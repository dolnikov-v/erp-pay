<?php

$params = require(__DIR__ . '/params.php');
$commonConfig = require(__DIR__ . '/common.php');


$localConfig = [];
if (file_exists(__DIR__ . DIRECTORY_SEPARATOR . 'test.dev.php')) {
    $localConfig = require(__DIR__ . DIRECTORY_SEPARATOR . 'test.dev.php');
}

return array_merge_recursive($commonConfig, [
    'id' => 'basic-tests',
    'language' => 'en-US',
    'basePath' => dirname(__DIR__),
    'components' => [
        'mailer' => [
            'useFileTransport' => true,
        ],
        'urlManager' => [
            'showScriptName' => true,
        ],
        'db' => require (__DIR__ . DIRECTORY_SEPARATOR . 'test_db.php')
    ],
], $localConfig);