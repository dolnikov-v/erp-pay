<?php


/** Список очередей и параметров для них, который необходим для автоматического создания очередей
 *  Очередь задается в виде:
 *  [
 * 'QueueName' => '<string>', // REQUIRED,
 * 'Attributes' => ['<string>', ...] // Описание списка атрибутов - https://docs.aws.amazon.com/aws-sdk-php/v3/api/api-sqs-2012-11-05.html#createqueue
 * ]
 *
 *  При включенном YII_ENV_DEV к названию очереди добавляется префикс 'dev_'
 */

/**
 * Пример:
 *
        [
        'QueueName' => 'CalculateDeliveryCharges',
        'Attributes' => [
        'VisibilityTimeout' => 600,
        'MessageRetentionPeriod' => 1209600,
        'ReceiveMessageWaitTimeSeconds' => 20,
        ]
        ],
 */

return [
    [
        'QueueName' => 'ClosingInvoice.fifo',
        'Attributes' => [
            'VisibilityTimeout' => 600,
            'MessageRetentionPeriod' => 1209600,
            'ReceiveMessageWaitTimeSeconds' => 20,
            'FifoQueue' => 'true',
            'ContentBasedDeduplication' => 'true'
        ]
    ],
    [
        'QueueName' => 'CalculateDeliveryCharges',
        'Attributes' => [
            'VisibilityTimeout' => 600,
            'MessageRetentionPeriod' => 1209600,
            'ReceiveMessageWaitTimeSeconds' => 20,
        ]
    ],
    [
        'QueueName' => 'ActionWithUserFromERP',
        'Attributes' => [
            'VisibilityTimeout' => 60,
            'MessageRetentionPeriod' => 1209600,
            'ReceiveMessageWaitTimeSeconds' => 20,
        ]
    ],
    [
        'QueueName' => 'LeadSendError',
        'Attributes' => [
            'VisibilityTimeout' => 600,
            'MessageRetentionPeriod' => 1209600,
            'ReceiveMessageWaitTimeSeconds' => 20,
        ]
    ],
    [
        'QueueName' => 'newLeadFromCCResponse',
        'Attributes' => [
            'VisibilityTimeout' => 60,
            'MessageRetentionPeriod' => 1209600,
            'ReceiveMessageWaitTimeSeconds' => 20,
        ]
    ],
    [
        'QueueName' => 'newLeadFromCCErrors',
        'Attributes' => [
            'VisibilityTimeout' => 60,
            'MessageRetentionPeriod' => 1209600,
            'ReceiveMessageWaitTimeSeconds' => 20,
        ]
    ],
    [
        'QueueName' => 'newLeadFromCC',
        'Attributes' => [
            'VisibilityTimeout' => 1200,
            'MessageRetentionPeriod' => 1209600,
            'ReceiveMessageWaitTimeSeconds' => 20,
        ]
    ],
    [
        'QueueName' => 'LeadErrorPay',
        'Attributes' => [
            'VisibilityTimeout' => 30,
            'MessageRetentionPeriod' => 1209600,
            'ReceiveMessageWaitTimeSeconds' => 0,
        ]
    ],
    [
        'QueueName' => 'LeadStatus',
        'Attributes' => [
            'VisibilityTimeout' => 600,
            'MessageRetentionPeriod' => 1209600,
            'ReceiveMessageWaitTimeSeconds' => 20,
        ]
    ],
    [
        'QueueName' => 'LeadStatus.fifo',
        'Attributes' => [
            'VisibilityTimeout' => 600,
            'MessageRetentionPeriod' => 1209600,
            'ReceiveMessageWaitTimeSeconds' => 20,
            'FifoQueue' => 'true',
            'ContentBasedDeduplication' => 'true'
        ]
    ],
    [
        'QueueName' => 'newLeads',
        'Attributes' => [
            'VisibilityTimeout' => 60,
            'MessageRetentionPeriod' => 1209600,
            'ReceiveMessageWaitTimeSeconds' => 20,
        ]
    ],
    [
        'QueueName' => 'CheckTrackingList',
        'Attributes' => [
            'VisibilityTimeout' => 3600,
            'MessageRetentionPeriod' => 1209600,
            'ReceiveMessageWaitTimeSeconds' => 20,
        ]
    ],
    [
        'QueueName' => 'OldCCUpdateList',
        'Attributes' => [
            'VisibilityTimeout' => 600,
            'MessageRetentionPeriod' => 1209600,
            'ReceiveMessageWaitTimeSeconds' => 20,
        ]
    ],
    [
        'QueueName' => 'CallCenterCheckingStatus',
        'Attributes' => [
            'VisibilityTimeout' => 300,
            'MessageRetentionPeriod' => 1209600,
            'ReceiveMessageWaitTimeSeconds' => 20,
        ]
    ],
    [
        'QueueName' => 'GoodsAccounting.fifo',
        'Attributes' => [
            'VisibilityTimeout' => 600,
            'MessageRetentionPeriod' => 1209600,
            'ReceiveMessageWaitTimeSeconds' => 20,
            'FifoQueue' => 'true',
            'ContentBasedDeduplication' => 'true'
        ]
    ],
    [
        'QueueName' => 'GoodsAccountingErrors.fifo',
        'Attributes' => [
            'VisibilityTimeout' => 600,
            'MessageRetentionPeriod' => 1209600,
            'ReceiveMessageWaitTimeSeconds' => 20,
            'FifoQueue' => 'true',
            'ContentBasedDeduplication' => 'true'
        ]
    ],
    [
        'QueueName' => 'DeliveryPayment.fifo',
        'Attributes' => [
            'VisibilityTimeout' => 600,
            'MessageRetentionPeriod' => 1209600,
            'ReceiveMessageWaitTimeSeconds' => 20,
            'FifoQueue' => 'true',
            'ContentBasedDeduplication' => 'true'
        ]
    ],
    [
        'QueueName' => 'DeliveryPaymentErrors.fifo',
        'Attributes' => [
            'VisibilityTimeout' => 600,
            'MessageRetentionPeriod' => 1209600,
            'ReceiveMessageWaitTimeSeconds' => 20,
            'FifoQueue' => 'true',
            'ContentBasedDeduplication' => 'true'
        ]
    ],
    [
        'QueueName' => 'MercuryQueue.fifo',
        'Attributes' => [
            'VisibilityTimeout' => 300,
            'MessageRetentionPeriod' => 1209600,
            'ReceiveMessageWaitTimeSeconds' => 20,
            'FifoQueue' => 'true',
            'ContentBasedDeduplication' => 'true'
        ]
    ],
    [
        'QueueName' => 'MercuryBarcodeRequest',
        'Attributes' => [
            'VisibilityTimeout' => 300,
            'MessageRetentionPeriod' => 1209600,
            'ReceiveMessageWaitTimeSeconds' => 20,
        ]
    ],
    [
        'QueueName' => 'MercuryBarcodeResponse',
        'Attributes' => [
            'VisibilityTimeout' => 300,
            'MessageRetentionPeriod' => 1209600,
            'ReceiveMessageWaitTimeSeconds' => 20,
        ]
    ],
    [
        'QueueName' => 'MercuryBarcodeResponseErrors',
        'Attributes' => [
            'VisibilityTimeout' => 300,
            'MessageRetentionPeriod' => 1209600,
            'ReceiveMessageWaitTimeSeconds' => 20,
        ]
    ],
];