<?php
use yii\helpers\ArrayHelper;

$params = require(__DIR__ . '/params.php');
$commonConfig = require(__DIR__. '/common.php');

$config = [
    'id' => 'basic',
    'language' => 'ru-RU',
    'basePath' => dirname(__DIR__),
    'bootstrap' => [],
    'defaultRoute' => 'home/index',
    'name' => $params['companyName'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            'class' => 'app\components\web\Request',
            'cookieValidationKey' => 'k9UL1hykVkvXNEtSJPFy8M8GZLcysaha',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser'
            ]
        ],
        'response' => [
            'class' => 'yii\web\Response',
            'formatters' => [
                yii\web\Response::FORMAT_JSON => [
                    'class' => 'yii\web\JsonResponseFormatter',
                    'prettyPrint' => false, // амазоновский CloudWatch разучился парсить многострочный джейсон
                ],
            ],
        ],
        'session' => [
            'class' => \yii\redis\Session::class,
            'redis' => array_merge(require(__DIR__.'/redis.php'), [
                'database' => 2
            ]),
            'keyPrefix' => '2wtrade-pay.session.',
            'timeout' => 3600 * 24 * 30,
            'cookieParams' => [
                'lifetime' => 3600 * 24 * 30,
            ],
        ],
        'user' => [
            'class' => 'app\components\web\User',
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'errors/index',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
                [
                    'class' => 'app\components\log\DbTarget',
                    'levels' => ['trace', 'info', 'error', 'warning'],
                    'categories' => ['yii\db\Command::execute'],
                ],
            ],
        ],
        'formatter' => [
            'class' => 'app\components\i18n\Formatter',
            'nullDisplay' => '—',
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'app\modules\i18n\components\DbMessageSource',
                    'sourceLanguage' => 'ru-RU',
                    'sourceMessageTable' => '{{%i18n_source_message}}',
                    'messageTable' => '{{%i18n_message}}',
                    'cachingDuration' => 86400,
                    'enableCaching' => true,
                    'on missingTranslation' => [\app\modules\i18n\components\DbMessageSource::class, 'handleMissingTranslation']
                ],
            ],
        ],
        'assetManager' => [
            'linkAssets' => YII_ENV_DEV ? true : false,
            'appendTimestamp' => true,
            'bundles' => [
                'yii\web\YiiAsset' => [
                    'depends' => [
                        'app\assets\vendor\JqueryAsset',
                        'app\assets\vendor\BootstrapAsset',
                    ],
                ],
                'yii\web\JqueryAsset' => [
                    'css' => [],
                    'js' => [],
                ],
                'yii\bootstrap\BootstrapAsset' => [
                    'css' => [],
                    'js' => [],
                ],
                'yii\bootstrap\BootstrapPluginAsset' => [
                    'css' => [],
                    'js' => [],
                ],
            ],
        ],
        'userActionLog' => [
            'class' => 'app\components\UserActionLog',
        ],
        'notifier' => [
            'class' => 'app\components\Notifier',
        ],
        'excelXML' => [
            'class' => 'app\components\Excel_XML',
        ],
        'apiUser' => [
            'class' => 'app\components\rest\User',
        ],
        'view' => [
            'theme' => [
                'basePath' => '@app/web/themes/' . $params['theme'],
                'baseUrl' => '@web/themes/' . $params['theme'],
                'pathMap' => [
                    '@app/views' => '@app/web/themes/' . $params['theme'],
                ],
            ],
        ],
        'skin' => '@app/web/themes/' . $params['theme'] . '/skins/' . $params['skin'],
    ],
    /* Модули размещать по алфавиту */
    'modules' => [
        'access' => [
            'class' => 'app\modules\access\Module',
        ],
        'administration' => [
            'class' => 'app\modules\administration\Module',
        ],
        'api' => [
            'class' => 'app\modules\api\Module',
        ],
        'apidelivery' => [
            'class' => 'app\modules\apidelivery\Module',
        ],
        'auth' => [
            'class' => 'app\modules\auth\Module',
        ],
        'bar' => [
            'class' => 'app\modules\bar\Module',
        ],
        'call-center' => [
            'class' => 'app\modules\callcenter\Module',
        ],
        'catalog' => [
            'class' => 'app\modules\catalog\Module',
        ],
        'checklist' => [
            'class' => 'app\modules\checklist\Module',
        ],
        'crocotime' => [
            'class' => 'app\modules\crocotime\Module',
        ],
        'delivery' => [
            'class' => 'app\modules\delivery\DeliveryModule',
        ],
        'deliveryreport' => [
            'class' => 'app\modules\deliveryreport\Module',
        ],
        'gridview' => [
            'class' => 'kartik\grid\Module',
        ],
        'home' => [
            'class' => 'app\modules\home\Module',
        ],
        'i18n' => [
            'class' => 'app\modules\i18n\Module',
        ],
        'media' => [
            'class' => 'app\modules\media\Module',
        ],
        'notification' => [
            'class' => 'app\modules\notification\Module',
        ],
        'order' => [
            'class' => 'app\modules\order\Module',
        ],
        'profile' => [
            'class' => 'app\modules\profile\Module',
        ],
        'rating' => [
            'class' => 'app\modules\rating\Module',
            'params' => [
                'count' => 2, //2 users per page
                'interval' => 10, //second
                'dollar' => true //use dollar on calculate
            ]
        ],
        'report' => [
            'class' => 'app\modules\report\Module',
        ],
        'reportoperational' => [
            'class' => 'app\modules\reportoperational\Module',
        ],
        'salary' => [
            'class' => 'app\modules\salary\Module',
        ],
        'smsnotification' => [
            'class' => 'app\modules\smsnotification\Module',
            'params' => [
                'countrySupportSmsSender' => require(__DIR__ . '/country_support_sms_sender.php'),
                'smsSender' => '2WTRADE',
            ],
        ],
        'storage' => [
            'class' => 'app\modules\storage\Module',
        ],
        'systemstatus' => [
            'class' => 'app\modules\systemstatus\Module',
        ],
        'telegram' => [
            'class' => 'app\modules\telegram\Module',
        ],
        'logger' => [
            'class' => 'app\modules\logger\Module',
            'logger' => 'processingLogger', // имя компонента с базой данных (yii/db/Connection)
        ],
        'ticket' => [
            'class' => 'app\modules\ticket\Module',
            'params' => [
                'siteTicket' => 'http://ticket.m2corp.ru/api/project/7/',
                'userTicket' => 'director:cucumber',
                'uploadTicket' => 'http://ticket.m2corp.ru/uploads/',
            ]
        ],
        'webhook' => [
            'class' => 'app\modules\webhook\Module',
            'params' => [
                'ninja' => [
                    'id' => [
                        'secret' => '3b383c4c40d8489295a01b43c9223ae2'
                    ],
                    'my' => [
                        'secret' => '2551a3e4fc574efc930b2b9446ca76f4'
                    ],
                    'ph' => [
                        'secret' => '9d292437b06f45ecafc83e8a924f66a0'
                    ],
                    'sg' => [
                        'secret' => '7316f43bd45c4481a171d4704d3344d4'
                    ],
                    'th' => [
                        'secret' => '0f56105f30e84fa3b480f9e5fdf435ec'
                    ],
                    'vn' => [
                        'secret' => ''
                    ]
                ],
            ]
        ],
        'widget' => [
            'class' => 'app\modules\widget\Module',
        ],
        'packager' => [
            'class' => 'app\modules\packager\Module',
        ],
        'finance' => [
            'class' => 'app\modules\finance\Module',
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        'allowedIPs' => ['127.0.0.1', '::1', '87.103.246.84', '172.21.0.1'],
        'panels' => [
            'mongodb' => [
                'class' => \yii\mongodb\debug\MongoDbPanel::class,
                 'db' => 'mongodb'
            ],
            'queue' => [
                'class' => \yii\queue\debug\Panel::class,
            ],
        ]
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'generators' => [
            'fixture' => [
                'class' => 'app\components\gii\fixture\LimitedGenerator',
            ],
        ],
    ];
}

$linkPagerDefinition = [
    'nextPageLabel' => false,
    'prevPageLabel' => false,
    'lastPageLabel' => '»',
    'firstPageLabel' => '«',
    'maxButtonCount' => 5,
    'options' => [
        'class' => 'pagination pagination-sm no-margin pull-right',
    ]
];

Yii::$container->set('yii\widgets\LinkPager', $linkPagerDefinition);
Yii::$container->set('app\widgets\LinkPager', $linkPagerDefinition);

// Дев конфиг, если есть то подгружаем
if (file_exists(__DIR__ . '/web.dev.php')) {
    $devCfg = require(__DIR__ . '/web.dev.php');
    $config = ArrayHelper::merge($config, $devCfg);
}

return ArrayHelper::merge($commonConfig, $config);
