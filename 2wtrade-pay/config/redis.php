<?php
/**
 * Настройки подключения к БД Redis
 */

$localConfig = [];

// Локальный конфиг, если есть то подгружаем
if (file_exists(__DIR__ . '/redis.local.php')) {
    $localConfig = require(__DIR__ . '/redis.local.php');
}

$config = [
    'class' => \yii\redis\Connection::class,
    'hostname' => 'redis',
    'port' => 6379,
    'retries' => 10,
    'password' => '321wwtradensk9956'
];

return array_merge($config, $localConfig);