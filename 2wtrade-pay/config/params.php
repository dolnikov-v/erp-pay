<?php

Yii::setAlias('@media', realpath(dirname(__FILE__) . '/../files/media'));
Yii::setAlias('@downloads', realpath(dirname(__FILE__) . '/../files/downloads'));
Yii::setAlias('@labels', realpath(dirname(__FILE__) . '/../files/downloads/labels'));
Yii::setAlias('@templates', realpath(dirname(__FILE__) . '/../templates'));
Yii::setAlias('@tickets', realpath(dirname(__FILE__) . '/../files/tickets'));
Yii::setAlias('@uploads', realpath(dirname(__FILE__) . '/../runtime/uploads'));
Yii::setAlias('@docs', realpath(dirname(__FILE__) . '/../files/docs'));
Yii::setAlias('@logs', realpath(dirname(__FILE__) . '/../runtime/logs'));
Yii::setAlias('@deliveryApiClasses', realpath(dirname(__FILE__) . '/../modules/delivery/vendor'));
Yii::setAlias('@files', realpath(dirname(__FILE__) . '/../files'));
Yii::setAlias('@certificatesFolder', realpath(dirname(__FILE__) . '/../files/downloads/certificates'));
Yii::setAlias('@export', realpath(dirname(__FILE__) . '/../files/export'));
Yii::setAlias('@tests', realpath(dirname(__FILE__) . '/../tests'));
Yii::setAlias('@new_templates', realpath(dirname(__FILE__) . '/../templates'));
Yii::setAlias('@documents', realpath(dirname(__FILE__) . '/../documents'));

$params = [
    'companyName' => '2WTRADE',
    'companyShortName' => 'WT',
    'noReplyEmail' => 'no-reply@2wtrade-pay.com',
    'infoMailAddress' => 'info@2wtrade.com',
    'domain' => 'http://2wtrade-pay.com',
    'theme' => 'basic',
    'skin' => 'basic',
    'jira' => [
        'credentials' =>
            [
                'username' => 'jibot@2wtrade.com',
                'password' => '3YeXHnmq'
            ],
        'notifiers' => [
            'smykov@2wtrade.com',
            'korzik.e@2wtrade.com'
        ],
        'host' => 'https://2wtrade-tasks.atlassian.net',
        'supervisor' => 'smykov',
        'monitor-delivery' => 'muravev.a'
    ],
    'jiraIssueCollector' => [
        'collectorId' => [
            'ru-RU' => '0a268f54',
            'en-US' => '786ab02f',
        ],
        'url' => 'https://2wtrade-tasks.atlassian.net/s/d41d8cd98f00b204e9800998ecf8427e-T/-aefvof/b/8/a44af77267a987a660377e5c46e0fb64/_/download/batch/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector.js?locale=ru-RU',
    ],
    'confluence' => [
        'credentials' =>
            [
                'username' => 'ratvien@gmail.com',
                'password' => '13Ct510$'
            ],
        'rest' => 'https://2wtrade.atlassian.net/wiki/rest/api/',
        'jsonRpcUrl' => 'https://2wtrade.atlassian.net/wiki/rpc/json-rpc/confluenceservice-v2',
    ],
    'crocotime' => [
        'token' => 'c7811adb9385a1b5d531b06408d10b78022a4683e8dd53f1f8417b0798dc4811',
        'url' => 'http://87.103.246.84:8085',
        'appVersion' => '5.7.3',
        'login' => 'ratvien@gmail.com',
        'password' => 'z0YpfvNJ',
        'notificationReceivers' => [
            'smykov@2wtrade.com',
            'korzik.e@2wtrade.com',
        ]
    ],
    'vpn' => [
        'notifiers' => [
            'smykov@2wtrade.com',
            'korzik.e@2wtrade.com'
        ]
    ],
    'telegram' => [
        'token' => '458898473:AAHhzSGnWbF6p4K8FQNw2W5Pv2u7Gf3Ir4c',
        'invite' => 'https://t.me/wwtrade_bot',
        'api_url' => 'https://api.telegram.org/bot'
    ],
    'telegramBot' => [
        'token' => '490778274:AAF2Pal760NlMTbPPsVmJ48M1ySTCZSzpaE',
    ],
    'skype' => [
        'token_url' => 'https://login.microsoftonline.com/botframework.com/oauth2/v2.0/token',
        'join_to_dot_url' => 'https://join.skype.com/bot/696937b2-2690-4c68-b8aa-8fd71473c5c9',
        'client_id' => '696937b2-2690-4c68-b8aa-8fd71473c5c9',
        'client_secret' => 'hehNTYN9932_@+xotvFYF5$',
        'scope' => 'https://api.botframework.com/.default',
        'bot_id' => '28:696937b2-2690-4c68-b8aa-8fd71473c5c9',
        'bot_name' => '2wtrade_Bot'
    ],
    'amazon' => [
        'apiKey' => 'AKIAJRTTMOU3QM6VYBOA',
        'secretKey' => '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',
        'accountId' => '636470419474',
        'region' => 'eu-west-1',
        'version' => 'latest',
        'sqs' => [
            // Алиасы для использования в коде, на случай необходимости смены названия очереди
            'queueNameAliases' => [
                'close_invoice' => 'ClosingInvoice.fifo',
                'calculate_delivery_charges' => 'CalculateDeliveryCharges',
                'amazonActionWithUserCC' => 'ActionWithUserFromERP',
                'amazonLeadSendErrorQueue' => 'LeadSendError',
                'amazonLeadNewLeadFromCCResponse' => 'newLeadFromCCResponse',
                'amazonLeadNewLeadFromCCErrors' => 'newLeadFromCCErrors',
                'amazonLeadNewLeadFromCC' => 'newLeadFromCC',
                'amazonLeadErrorPay' => 'LeadErrorPay',
                'amazonGetCallCenterStatusesError' => 'LeadErrorPay',
                'amazonGetCallCenterStatusesQueueUrl' => 'LeadStatus.fifo',
                'amazonCallCenterSendQueueUrl' => 'newLeads',
                'amazonCheckTrackingQueueUrl' => 'CheckTrackingList',
                'amazonOldCallCenterUpdateQueueUrl' => 'OldCCUpdateList',
                'amazonCallCenterCheckingStatusQueueUrl' => 'CallCenterCheckingStatus',
                'goodsAccounting' => 'GoodsAccounting.fifo',
                'goodsAccountingErrors' => 'GoodsAccountingErrors.fifo',
                'deliveryPayment' => 'DeliveryPayment.fifo',
                'deliveryPaymentErrors' => 'DeliveryPaymentErrors.fifo',
                'mercuryQueue' => 'MercuryQueue.fifo',
                'barcodeRequest' => 'MercuryBarcodeRequest',
                'barcodeResponse' => 'MercuryBarcodeResponse',
                'barcodeResponseErrors' => 'MercuryBarcodeResponseErrors',
            ]
        ],
    ],
    'api2wcallSendForeignId' => 'http://api.2wcall.com/v1/order/set-foreign-id-from-pay',
    'api2wcallIdentity' => '2wtrade-pay',
    'api2wcallToken' => '20b70f0af00562e63758b9ee42012ecc96c58590',
    'currencyLayerApiKey' => '94577bc776afbe85d054ce4ea9048b0d',
    'api2wcallUrl' => 'http://api.2wcall.com',
    'SmsService' => [
        'login' => 'smykov@2wtrade.com',
        'password' => '16csj78sl69as',
        'url' => 'https://my.zorra.com/api',
        'smsSender' => '2wtrade',
    ],
    'marketplace' => [
        'mercadolibreCL' => [
            'baseApiUrl' => 'http://api.mercadolibre.com/',
            'authorizationUrl' => 'https://api.mercadolibre.com/oauth/token',
            'apiKey' => '',
            'clientId' => '7032807430132059',
            'secretKey' => '',
            'redirectUri' => 'https://www.tophot.com',
            'sellerId' => '364172666',
        ],
        'mercadolibrePE' => [
            'baseApiUrl' => 'http://api.mercadolibre.com/',
            'authorizationUrl' => 'https://api.mercadolibre.com/oauth/token',
            'apiKey' => '',
            'clientId' => '8124024919985920',
            'secretKey' => '',
            'redirectUri' => 'https://www.tophot.com',
            'sellerId' => '374020438',
        ],
        'mercadolibreCO' => [
            'baseApiUrl' => 'http://api.mercadolibre.com/',
            'authorizationUrl' => 'https://api.mercadolibre.com/oauth/token',
            'apiKey' => '',
            'clientId' => '7728338173021219',
            'secretKey' => '',
            'redirectUri' => 'https://www.tophot.com',
            'sellerId' => '373181946',
        ],
        'mercadolibreMX' => [
            'baseApiUrl' => 'http://api.mercadolibre.com/',
            'authorizationUrl' => 'https://api.mercadolibre.com/oauth/token',
            'apiKey' => '',
            'clientId' => '2587937474559266',
            'secretKey' => '',
            'redirectUri' => 'https://www.tophot.com',
            'sellerId' => '371866701',
        ],
        'mercadolibreGT' => [
            'baseApiUrl' => 'http://api.mercadolibre.com/',
            'authorizationUrl' => 'https://api.mercadolibre.com/oauth/token',
            'apiKey' => '',
            'clientId' => '4203779349634253',
            'secretKey' => '',
            'redirectUri' => 'https://www.tophot.com',
            'sellerId' => '376321613',
        ],
        'lelong' => [
            'apiKey' => '',
        ],
        'elevenstreet' => [
            'baseApiUrl' => 'http://api.11street.my/rest/ordservices/complete/',
            'apiKey' => '',
        ],
        'youbeli' => [
            'baseUrl' => 'https://api.youbeli.com/3.0',
            'apiKey' => '',
            'storeId' => '',
        ]
    ]
];

if (file_exists(__DIR__ . '/params.local.php')) {
    $localParams = require(__DIR__ . '/params.local.php');
    $params = \yii\helpers\ArrayHelper::merge($params, $localParams);
}

return $params;
