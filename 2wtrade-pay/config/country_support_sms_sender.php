<?php

return [
    "AF" => false,
    "AL" => true,
    "DZ" => false,
    "AD" => true,
    "AO" => true,
    "AI" => true,
    "AG" => true,
    "AR" => false,
    "AM" => true,
    "AW" => true,
    "AU" => true,
    "AT" => true,
    "AZ" => false,
    "BS" => true,
    "BH" => true,
    "BD" => false,
    "BB" => true,
    "BY" => true,
    "BE" => false,
    "BZ" => true,
    "BJ" => true,
    "BM" => true,
    "BT" => true,
    "BO" => true,
    "BA" => true,
    "BW" => true,
    "BR" => false,
    "BN" => true,
    "BG" => true,
    "BF" => true,
    "BI" => true,
    "KH" => true,
    "CM" => true,
    "CA" => false,
    "CV" => true,
    "KY" => false,
    "CF" => true,
    "TD" => true,
    "CL" => false,
    "CN" => false,
    "CO" => false,
    "KM" => true,
    "CG" => false,
    "CD" => false,
    "CK" => true,
    "CR" => false,
    "HR" => false,
    "CU" => false,
    "CY" => true,
    "CZ" => true,
    "DK" => true,
    "IO" => false,
    "DJ" => true,
    "DM" => true,
    "DO" => false,
    "EC" => false,
    "EG" => false,
    "SV" => false,
    "GQ" => true,
    "EE" => true,
    "FK" => true,
    "FO" => true,
    "FJ" => true,
    "FI" => true,
    "FR" => true,
    "GF" => false,
    "PF" => true,
    "GA" => true,
    "GM" => true,
    "GE" => true,
    "DE" => true,
    "GH" => false,
    "GI" => true,
    "GR" => true,
    "GL" => true,
    "GD" => true,
    "GP" => true,
    "GU" => false,
    "GT" => false,
    "GG" => true,
    "GN" => true,
    "GW" => true,
    "GY" => true,
    "HT" => true,
    "HN" => false,
    "HK" => true,
    "HU" => false,
    "IS" => true,
    "IN" => true,
    "ID" => true,
    "IR" => false,
    "IQ" => false,
    "IE" => true,
    "IM" => true,
    "IL" => true,
    "IT" => true,
    "CI" => true,
    "JM" => true,
    "JP" => false,
    "JE" => true,
    "JO" => true,
    "KZ" => false,
    "KE" => false,
    "KW" => false,
    "KG" => false,
    "LA" => false,
    "LV" => true,
    "LB" => true,
    "LS" => true,
    "LR" => true,
    "LY" => true,
    "LI" => true,
    "LT" => true,
    "LU" => true,
    "MO" => true,
    "MK" => true,
    "MG" => true,
    "MW" => true,
    "MY" => false,
    "MV" => true,
    "ML" => false,
    "MT" => true,
    "MQ" => true,
    "MR" => true,
    "MU" => true,
    "YT" => true,
    "MX" => false,
    "MD" => true,
    "MC" => false,
    "MN" => true,
    "ME" => true,
    "MS" => true,
    "MA" => false,
    "MZ" => false,
    "MM" => false,
    "NA" => false,
    "NR" => false,
    "NP" => false,
    "NL" => true,
    "AN" => true,
    "NC" => true,
    "NZ" => false,
    "NI" => false,
    "NE" => true,
    "NG" => true,
    "NF" => true,
    "NO" => true,
    "OM" => true,
    "PK" => false,
    "PS" => true,
    "PA" => false,
    "PG" => true,
    "PY" => true,
    "PE" => true,
    "PH" => true,
    "PL" => true,
    "PT" => true,
    "PR" => false,
    "QA" => false,
    "RE" => true,
    "RO" => false,
    "RU" => true,
    "RW" => true,
    "WS" => true,
    "SM" => true,
    "ST" => true,
    "SA" => true,
    "SN" => true,
    "RS" => true,
    "SC" => true,
    "SL" => true,
    "SG" => true,
    "SK" => true,
    "SI" => true,
    "SB" => true,
    "SO" => true,
    "ZA" => false,
    "KP" => false,
    "SS" => true,
    "ES" => true,
    "LK" => false,
    "KN" => true,
    "LC" => true,
    "VC" => true,
    "SD" => true,
    "SR" => true,
    "SZ" => true,
    "SE" => true,
    "CH" => true,
    "SY" => false,
    "TW" => true,
    "TJ" => true,
    "TZ" => true,
    "TH" => true,
    "TL" => true,
    "TG" => true,
    "TO" => true,
    "TT" => true,
    "TN" => false,
    "TR" => false,
    "TM" => true,
    "TC" => true,
    "UG" => true,
    "UA" => true,
    "AE" => true,
    "GB" => true,
    "US" => false,
    "UY" => false,
    "UZ" => true,
    "VU" => true,
    "VE" => false,
    "VN" => false,
    "VG" => true,
    "YE" => true,
    "ZM" => true,
    "ZW" => true,
];
