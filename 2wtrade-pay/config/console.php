<?php

use yii\helpers\ArrayHelper;

$params = require(__DIR__ . '/params.php');
$commonConfig = require(__DIR__ . '/common.php');

$config = [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['dbEventWatcher', 'dbEventWatcherRedis'],
    'controllerNamespace' => 'app\commands',
    'controllerMap' => [
        'migrate' => [
            'class' => 'yii\console\controllers\MigrateController',
            'templateFile' => '@app/migrations/views/migration.php',
        ],
        'mongodb-migrate' => [
            'class' => 'yii\mongodb\console\controllers\MigrateController',
            'migrationPath' => 'migrations-mongo'
        ],
        'amazon' => [
            'class' => \app\controllers\console\AmazonController::class,
            'sqsQueueList' => require(__DIR__ . DIRECTORY_SEPARATOR . 'amazonSqsQueueList.php')
        ],
        'db' => [
            'class' => \app\controllers\console\DbController::class,
            'configurationValues' => require(__DIR__ . '/dbConfigurationValues.php'),
        ],
        'callCenter' => [
            'class' => \app\commands\crontab\CallCenterController::class,
            'requestSender' => [
                'class' => \app\modules\callcenter\components\Sender::class,
                'as notification' => \app\modules\callcenter\components\behaviors\NotificationBehavior::class,
                'as log' => [
                    'class' => \app\modules\callcenter\components\behaviors\LogBehavior::class,
                    'logger' => 'logger'
                ],
            ],
            'requestSenderQueue' => 'queueCallCenterSend'
        ]
    ],
    'components' => [
        'dbEventWatcher' => [
            'class' => \app\components\dbevent\db\EventWatcher::class,
            'eventHandlers' => require(__DIR__ . DIRECTORY_SEPARATOR . 'dbEventHandlers.php'),
            'mutex' => [
                'class' => \yii\redis\Mutex::class,
                'redis' => 'redis',
                'expire' => 120, // блокировка на 2 минуты
                'keyPrefix' => '2wtrade-pay.dbEventWatcher.'
            ]
        ],
        'dbEventWatcherRedis' => [
            'class' => \app\components\dbevent\redis\EventWatcher::class,
            'eventHandlers' => require(__DIR__ . DIRECTORY_SEPARATOR . 'dbEventHandlers.php'),
            'channel' => 'db_events',
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'urlManager' => [
            'baseUrl' => $params['domain'],
        ],
        'formatter' => [
            'class' => 'app\components\i18n\Formatter',
            'nullDisplay' => '—',
            'decimalSeparator' => ',',
            'thousandSeparator' => ' ',
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => \app\modules\i18n\components\DbMessageSource::class,
                    'sourceLanguage' => 'ru-RU',
                    'sourceMessageTable' => '{{%i18n_source_message}}',
                    'messageTable' => '{{%i18n_message}}',
                    'cachingDuration' => 86400,
                    'enableCaching' => true,
                    'on missingTranslation' => [
                        \app\modules\i18n\components\DbMessageSource::class,
                        'handleMissingTranslation'
                    ]
                ],
            ],
        ],
        'asterisk_db' => require(__DIR__ . '/asteriskcdr.php'),
        'adcombo' => [
            'class' => 'app\components\adcombo\AdComboParser',
            'params' => require(__DIR__ . '/adcombo.php'),
        ]
    ],
    'modules' => [
        'smsnotification' => [
            'class' => 'app\modules\smsnotification\Module',
            'params' => [
                'countrySupportSmsSender' => require(__DIR__ . '/country_support_sms_sender.php'),
                'smsSender' => '2WTRADE',
            ],
        ],
        'rating' => [
            'class' => 'app\modules\rating\Module',
            'params' => [
                'count' => 2, //2 users per page
                'interval' => 10, //second
                'dollar' => true //use dollar on calculate
            ]
        ]
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {

    $config['controllerMap']['fixture'] = [
        'class' => 'yii\faker\FixtureController',
        'fixtureDataPath' => '@app/tests/fixtures/data',
        'templatePath' => '@app/tests/templates',
        'namespace' => 'tests\fixtures',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

// Дев конфиг, если есть то подгружаем
if (file_exists(__DIR__ . '/console.dev.php')) {
    $devCfg = require(__DIR__ . '/console.dev.php');
    $config = ArrayHelper::merge($config, $devCfg);
}

return ArrayHelper::merge($commonConfig, $config);
