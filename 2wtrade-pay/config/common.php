<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'bootstrap' => ['log', 'queue', 'queueOrders', 'queueExport', 'queueTelegram', 'queueTransport', 'queueCallCenterSend', 'queueLog', 'queueInvoiceGenerate'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'components' => [
        'logger' => [
            'class' => \snapsuzun\yii2logger\file\Logger::class,
        ],
        'cache' => [
            'class' => \yii\redis\Cache::class,
            'redis' => array_merge(require(__DIR__ . '/redis.php'), [
                'database' => 1
            ])
        ],
        'db' => require(__DIR__ . '/local.php'),
        'vpnDb' => require(__DIR__ . '/vpnDb.php'),
        'mongodb' => require(__DIR__ . '/mongoDb.php'),
        'logger_db' => require(__DIR__ . '/logger.php'),
        'redis' => require(__DIR__ . '/redis.php'),
        // Общая очередь для выполнения отложенных заданий
        'queue' => [
            'class' => app\components\queue\BaseQueue::class,
            'as log' => \yii\queue\LogBehavior::class,
            'redis' => 'redis',
            'channel' => 'queueCommon',
            'commandClass' => \app\components\queue\redis\Command::class,
        ],
        // Очередь для выполнения отложенных действий над заказами (калькуляция расходов, создание заявок и т.д.)
        'queueOrders' => [
            'class' => app\components\queue\BaseQueue::class,
            'as log' => \yii\queue\LogBehavior::class,
            'redis' => 'redis',
            'channel' => 'queueOrders',
            'commandClass' => \app\components\queue\redis\Command::class,
        ],
        // Очередь для выполнения задач из телеграма
        'queueTelegram' => [
            'class' => app\components\queue\QueueTelegram::class,
            'as log' => \yii\queue\LogBehavior::class,
            'handled' => true,
            'redis' => 'redis',
            'channel' => 'queueTelegram',
            'commandClass' => \app\components\queue\redis\Command::class,
        ],
        // Очередь для генерации различного рода файлов и экспорта данных
        'queueExport' => [
            'class' => app\components\queue\BaseQueue::class,
            'as log' => \yii\queue\LogBehavior::class,
            'redis' => 'redis',
            'channel' => 'queueExport',
            'commandClass' => \app\components\queue\redis\Command::class,
        ],
        // Очередь для генерации инвойсов
        'queueInvoiceGenerate' => [
            'class' => app\components\queue\BaseQueue::class,
            'as log' => \yii\queue\LogBehavior::class,
            'redis' => 'redis',
            'channel' => 'queueInvoiceGenerate',
            'commandClass' => \app\components\queue\redis\Command::class,
        ],
        // Очередь для отправки различного рода сообщений (смс, телеграм, емейл)
        'queueTransport' => [
            'class' => \app\components\queue\BaseQueue::class,
            'as log' => \yii\queue\LogBehavior::class,
            'redis' => 'redis',
            'channel' => 'queueTransport',
            'commandClass' => \app\components\queue\redis\Command::class,
        ],
        // Очередь для создания заявок в КЦ и отправки их туда
        'queueCallCenterSend' => [
            'class' => \app\modules\callcenter\components\queue\CallCenterQueue::class,
            'channel' => 'queueCallCenterSend',
            'commandClass' => \app\components\queue\redis\Command::class,
            'requestSender' => [
                'class' => \app\modules\callcenter\components\Sender::class,
                'as notification' => [
                    'class' => \app\modules\callcenter\components\behaviors\NotificationBehavior::class,
                    'notification' => 'notification'
                ],
                'as log' => [
                    'class' => \app\modules\callcenter\components\behaviors\LogBehavior::class,
                    'logger' => 'logger'
                ],
            ]
        ],
        // Очередь для асинхронного создания логов
        'queueLog' => [
            'class' => \app\components\queue\BaseQueue::class,
            'channel' => 'queueLog',
            'commandClass' => \app\components\queue\redis\Command::class
        ],
        'authManager' => [
            'class' => 'app\components\rbac\DbManager',
        ],
        'mailer' => [
            'class' => \app\components\mailer\Mailer::class,
            'useFileTransport' => false,
            'htmlLayout' => '@app/mail/layouts/html',
            'messageClass' => \app\components\mailer\Message::class,
            'messageConfig' => [
                'maxCommonFileSizeLimit' => 7340032, // 7Mb
                'attachmentLinkLayout' => '@app/mail/layouts/html-attachment-links',
                'urlPathToDownloadFile' => '/webhook/file',
                'contentPath' => rtrim(Yii::getAlias('@files')) . DIRECTORY_SEPARATOR . 'mail',
                'allowForAccessDirs' => [Yii::getAlias('@files'), Yii::getAlias('@files')]
            ]
        ],
        'notification' => [
            'class' => 'app\components\Notification',
        ],
        'MetricTrendDetector' => [
            'class' => 'app\components\MetricTrendDetector'
        ],
        'processingLogger' => [
            'class' => 'app\modules\logger\components\log\Logger',
            'db' => 'logger_db',
        ],
        'urlManager' => [
            'class' => 'app\components\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => true,
            'suffix' => '',
            'rules' => [
                'http://rating.2wtrade-pay.com' => '/rating/country-curators/index',
                '' => 'home/index',
                '<action:[\w-]+>' => 'home/<action>',
                '<controller:[\w-]+>' => '<controller>/home',

                '<module:[\w-]+>/<controller:[\w-]+>/<action:download-ticket|download-excel|download-invoice|download-file>/<name:[\w]+[/.]+[\w]+>' => '<module>/<controller>/<action>',
                '<module:media>/<controller:[\w-]+>/<action:[\w-]+>/<fileName:[\w]+[/.]+[\w]+>' => '<module>/<controller>/<action>',
                '<module:webhook>/<controller:ninja>/<countryChar:[\w-]+>' => '<module>/<controller>/index',
                '<module:webhook>/<controller:boxme>/<countryChar:[\w-]+>' => '<module>/<controller>/index',
                '<module:webhook>/<controller:list>/<hash:[\w]+>' => '<module>/<controller>/index',
                '<module:webhook>/<controller:telegram>/<token:[\d+:\w+]+>' => '<module>/<controller>/index',
                //'<module:webhook>/<controller:jira>/<action:block-user>/<user:[\w-]+>' => '<module>/<controller>/<action>',
                '<module:[\w-]+>/<controller:[\w-]+>/<action:download-ticket|download-excel|download-invoice|download-file|download-tmp-file>/<name:[\w/._-]+[/.]+[\w/._-]+>' => '<module>/<controller>/<action>',
                '<module:media>/<controller:call-center-user>/<fileName:[\w]+[/.]+[\w]+>' => '<module>/<controller>/index',
                '<module:media>/<controller:salary-designation>/<action:job-description>/<fileName:[\w]+[/.]+[\w]+>' => '<module>/<controller>/<action>',
                '<module:media>/<controller:product>/<fileName:[\w]+[/.]+[\w]+>' => '<module>/<controller>/index',
                '<module:media>/<controller:delivery-contract>/<action:file>/<id:[\w]+[/.]+[\w]+>' => '<module>/<controller>/<action>',
                '<module:marketplace>/<controller:view|index>/<action:[\w-]+>/<marketplaceId:[\w\d-]+>/<id:[\w\d-]+>' => '<module>/<controller>/<action>',
                '<module:marketplace>/<controller:view|index>/<action:[\w-]+>/<marketplaceId:[\w\d-]+>' => '<module>/<controller>/<action>',

                '<module:[\w-]+>/<folder:[\w-]+>/<controller:[\w-]+>/<action:[\w-]+>/<id:\d+>' => '<module>/<folder>/<controller>/<action>',
                '<module:[\w-]+>/<controller:[\w-]+>/<action:[\w-]+>/<id:\d+>' => '<module>/<controller>/<action>',
                '<controller:[\w-]+>/<action:[\w-]+>/<id:\d+>' => '<controller>/<action>',

                '<module:[\w-]+>/<folder:[\w-]+>/<controller:[\w-]+>/<action:[\w-]+>' => '<module>/<folder>/<controller>/<action>',
                '<module:[\w-]+>/<controller:[\w-]+>/<action:[\w-]+>' => '<module>/<controller>/<action>',
                '<controller:[\w-]+>/<action:[\w-]+>' => '<controller>/<action>',


            ]
        ],
        'sqsClient' => function () use ($params) {
            $amazonParams = $params['amazon'];
            $client = new \app\components\amazon\sqs\SqsClient([
                'credentials' => [
                    'key' => $amazonParams['apiKey'],
                    'secret' => $amazonParams['secretKey']
                ],
                'region' => $amazonParams['region'],
                'version' => $amazonParams['version'],
                'accountId' => $amazonParams['accountId'],
                'queueNameAliases' => $amazonParams['sqs']['queueNameAliases']
            ]);
            return $client;
        },
        'sessionKey' => function () {
            return \app\helpers\Utils::uid();
        },
        'smsService' => [
            'class' => 'app\modules\smsnotification\components\ZorraService',
            'login' => $params['SmsService']['login'],
            'password' => $params['SmsService']['password'],
            'url' => $params['SmsService']['url'],
            'smsSender' => $params['SmsService']['smsSender'],
        ],
        'mutex' => [
            'class' => \yii\redis\Mutex::class,
            'redis' => 'redis',
            'keyPrefix' => '2wtrade-pay.blocks.'
        ],
        'sqsOrder' => [
            'class' => \app\components\integration1c\SqsOrder::class,
        ],
    ],
    'modules' => [
        'delivery' => [
            'class' => app\modules\delivery\DeliveryModule::class,
            'components' => [
                'broker' => [
                    'class' => \app\modules\delivery\components\broker\Broker::class,
                    'cacheTime' => 600
                ],
            ],
        ],
        'marketplace' => [
            'class' => app\modules\marketplace\MarketPlaceModule::class,
            'defaultMarketplaceConfiguration' => [
                'configurationClass' => [
                    'class' => \app\modules\marketplace\components\MarketPlaceConfiguration::class
                ],
                'dataConverterClass' => [
                    'class' => \app\modules\marketplace\components\DefaultDataTransmitter::class
                ],
                'productManagerClass' => [
                    'class' => \app\modules\marketplace\components\ProductManager::class,
                    'productClass' => \app\modules\marketplace\models\Product::class,
                ]
            ],
            'components' => [
                'defaultStorageService' => [
                    'class' => \app\modules\marketplace\components\StorageService::class,
                ],
                'defaultCacheService' => [
                    'class' => \app\modules\marketplace\components\CacheService::class
                ]
            ],
            'modules' => [
                'mercadolibreCL' => [
                    'class' => \app\modules\marketplace\modules\mercadolibre\MercadoLibre::class,
                    'nameSuffix' => 'CL',
                    'params' => [
                        'apiKey' => $params['marketplace']['mercadolibreCL']['apiKey']
                    ],
                    'components' => [
                        'apiAdapter' => [
                            'class' => \app\modules\marketplace\modules\mercadolibre\components\MercadoLibreApi::class,
                            'baseApiUrl' => $params['marketplace']['mercadolibreCL']['baseApiUrl'],
                            'authorizationUrl' => $params['marketplace']['mercadolibreCL']['authorizationUrl'],
                            'clientId' => $params['marketplace']['mercadolibreCL']['clientId'],
                            'secretKey' => $params['marketplace']['mercadolibreCL']['secretKey'],
                            'redirectUri' => $params['marketplace']['mercadolibreCL']['redirectUri'],
                            'sellerId' => $params['marketplace']['mercadolibreCL']['sellerId'],
                        ]
                    ]
                ],
                'mercadolibrePE' => [
                    'class' => \app\modules\marketplace\modules\mercadolibre\MercadoLibre::class,
                    'nameSuffix' => 'PE',
                    'params' => [
                        'apiKey' => $params['marketplace']['mercadolibrePE']['apiKey']
                    ],
                    'components' => [
                        'apiAdapter' => [
                            'class' => \app\modules\marketplace\modules\mercadolibre\components\MercadoLibreApi::class,
                            'baseApiUrl' => $params['marketplace']['mercadolibrePE']['baseApiUrl'],
                            'authorizationUrl' => $params['marketplace']['mercadolibrePE']['authorizationUrl'],
                            'clientId' => $params['marketplace']['mercadolibrePE']['clientId'],
                            'secretKey' => $params['marketplace']['mercadolibrePE']['secretKey'],
                            'redirectUri' => $params['marketplace']['mercadolibrePE']['redirectUri'],
                            'sellerId' => $params['marketplace']['mercadolibrePE']['sellerId'],
                        ]
                    ]
                ],
                'mercadolibreCO' => [
                    'class' => \app\modules\marketplace\modules\mercadolibre\MercadoLibre::class,
                    'nameSuffix' => 'CO',
                    'params' => [
                        'apiKey' => $params['marketplace']['mercadolibreCO']['apiKey']
                    ],
                    'components' => [
                        'apiAdapter' => [
                            'class' => \app\modules\marketplace\modules\mercadolibre\components\MercadoLibreApi::class,
                            'baseApiUrl' => $params['marketplace']['mercadolibreCO']['baseApiUrl'],
                            'authorizationUrl' => $params['marketplace']['mercadolibreCO']['authorizationUrl'],
                            'clientId' => $params['marketplace']['mercadolibreCO']['clientId'],
                            'secretKey' => $params['marketplace']['mercadolibreCO']['secretKey'],
                            'redirectUri' => $params['marketplace']['mercadolibreCO']['redirectUri'],
                            'sellerId' => $params['marketplace']['mercadolibreCO']['sellerId'],
                        ]
                    ]
                ],
                'mercadolibreMX' => [
                    'class' => \app\modules\marketplace\modules\mercadolibre\MercadoLibre::class,
                    'nameSuffix' => 'MX',
                    'params' => [
                        'apiKey' => $params['marketplace']['mercadolibreMX']['apiKey']
                    ],
                    'components' => [
                        'apiAdapter' => [
                            'class' => \app\modules\marketplace\modules\mercadolibre\components\MercadoLibreApi::class,
                            'baseApiUrl' => $params['marketplace']['mercadolibreMX']['baseApiUrl'],
                            'authorizationUrl' => $params['marketplace']['mercadolibreMX']['authorizationUrl'],
                            'clientId' => $params['marketplace']['mercadolibreMX']['clientId'],
                            'secretKey' => $params['marketplace']['mercadolibreMX']['secretKey'],
                            'redirectUri' => $params['marketplace']['mercadolibreMX']['redirectUri'],
                            'sellerId' => $params['marketplace']['mercadolibreMX']['sellerId'],
                        ]
                    ]
                ],
                'mercadolibreGT' => [
                    'class' => \app\modules\marketplace\modules\mercadolibre\MercadoLibre::class,
                    'nameSuffix' => 'GT',
                    'params' => [
                        'apiKey' => $params['marketplace']['mercadolibreGT']['apiKey']
                    ],
                    'components' => [
                        'apiAdapter' => [
                            'class' => \app\modules\marketplace\modules\mercadolibre\components\MercadoLibreApi::class,
                            'baseApiUrl' => $params['marketplace']['mercadolibreGT']['baseApiUrl'],
                            'authorizationUrl' => $params['marketplace']['mercadolibreGT']['authorizationUrl'],
                            'clientId' => $params['marketplace']['mercadolibreGT']['clientId'],
                            'secretKey' => $params['marketplace']['mercadolibreGT']['secretKey'],
                            'redirectUri' => $params['marketplace']['mercadolibreGT']['redirectUri'],
                            'sellerId' => $params['marketplace']['mercadolibreGT']['sellerId'],
                        ]
                    ]
                ],
                'lelong' => [
                    'class' => \app\modules\marketplace\modules\lelong\Lelong::class,
                    'params' => [
                        'apiKey' => $params['marketplace']['lelong']['apiKey']
                    ],
                    'components' => [
                        'apiAdapter' => [
                            'class' => \app\modules\marketplace\modules\lelong\components\LelongApi::class,
                            'apiKey' => $params['marketplace']['lelong']['apiKey'],
                        ]
                    ]
                ],
                'elevenstreet' => [
                    'class' => \app\modules\marketplace\modules\elevenstreet\ElevenStreet::class,
                    'params' => [
                        'apiKey' => $params['marketplace']['elevenstreet']['apiKey']
                    ],
                    'components' => [
                        'apiAdapter' => [
                            'class' => \app\modules\marketplace\modules\elevenstreet\components\ElevenStreetApi::class,
                            'baseApiUrl' => $params['marketplace']['elevenstreet']['baseApiUrl'],
                        ]
                    ]
                ],
                'youbeli' => [
                    'class' => \app\modules\marketplace\modules\youbeli\Youbeli::class,
                    'params' => [
                        'apiKey' => $params['marketplace']['youbeli']['apiKey']
                    ],
                    'components' => [
                        'apiAdapter' => [
                            'class' => \app\modules\marketplace\modules\youbeli\components\YoubeliApi::class,
                            'baseUrl' => $params['marketplace']['youbeli']['baseUrl'],
                            'apiKey' => $params['marketplace']['youbeli']['apiKey'],
                            'storeId' => $params['marketplace']['youbeli']['storeId'],
                        ]
                    ]
                ],
            ]
        ],
        'messenger' => [
            'class' => \app\modules\messenger\MessengerModule::class,
            'modules' => [
                'serviceSkype' => [
                    'class' => \app\modules\messenger\modules\skype\SkypeModule::class,
                    'components' => [
                        'transport' => [
                            'class' => \app\modules\messenger\modules\skype\transport\SkypeTransport::class,
                            'botID' => '',
                            'botName' => '',
                            'clientID' => '',
                            'clientSecret' => '',
                        ],
                        'auth' => [
                            'class' => \app\modules\messenger\modules\skype\components\AuthComponent::class,
                        ],
                    ],
                ],
            ],
        ],
    ]
];

// Дев конфиг, если есть то подгружаем
if (file_exists(__DIR__ . '/common.dev.php')) {
    $devCfg = require(__DIR__ . '/common.dev.php');
    $config = \yii\helpers\ArrayHelper::merge($config, $devCfg);
}

return $config;