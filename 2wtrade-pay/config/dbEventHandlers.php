<?php

/**
 * [
 *      'event_id' => callback(EventObject, EventWatcher)|EventHandlerInterface::class,
 *      'event_id' => [
 *         callback(EventObject, EventWatcher)|EventHandlerInterface::class,
 *         callback(EventObject, EventWatcher)|EventHandlerInterface::class,
 *         'event_type' => callback(EventObject, EventWatcher)|EventHandlerInterface::class,
 *         'event_type' => [
 *              callback(EventObject, EventWatcher)|EventHandlerInterface::class,
 *              callback(EventObject, EventWatcher)|EventHandlerInterface::class,
 *              ...
 *          ],
 *      ]
 * ]
 */

return [
    'order_finance_prediction' => \app\modules\order\dbeventhandlers\OrderFinancePredictionAndFactEventHandler::class,
    'order_finance_fact' => function ($eventObject) {
        $deliveryRequestId = (new \yii\db\Query())->from(['off' => \app\modules\order\models\OrderFinanceFact::tableName()])
            ->leftJoin(['dr' => \app\modules\delivery\models\DeliveryRequest::tableName()], 'dr.order_id = off.order_id')
            ->where(['off.id' => $eventObject->primaryKey])
            ->select('dr.id')
            ->scalar();
        Yii::$app->queueOrders->push(new \app\modules\order\jobs\OrderCalculateFinanceBalanceJob(['deliveryRequestId' => $deliveryRequestId]));
    },
    'order_status_id_was_changed' => \app\modules\order\dbeventhandlers\OrderFinancePredictionAndFactEventHandler::class,
];