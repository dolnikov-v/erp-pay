<?php

$config = [
    'redis_host' => 'redis',
    'redis_port' => 6379,
    'redis_password' => '321wwtradensk9956',
    'redis_db_events_channel' => 'db_events'
];

// Дев конфиг, если есть то подгружаем
if (file_exists(__DIR__ . '/dbConfigurationValues.local.php')) {
    $devCfg = require(__DIR__ . '/dbConfigurationValues.local.php');
    $config = \yii\helpers\ArrayHelper::merge($config, $devCfg);
}

return $config;