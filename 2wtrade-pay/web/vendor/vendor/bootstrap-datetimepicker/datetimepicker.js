var DateRangePicker = {
    initDateRangeFrom: function ($input) {
        $input.datetimepicker({
            locale: $input.data('locale'),
            format: $input.data('format'),
            minDate: $input.data('mindate'),
            maxDate: $input.data('maxdate')
        });

        $input.on("dp.change", DateRangePicker.changeDateRangeFrom);
        $input.on("dp.show", DateRangePicker.highlightDateRangeFrom);
        $input.on("dp.update", DateRangePicker.highlightDateRangeFrom);
    },
    initDateRangeTo: function ($input) {
        $input.datetimepicker({
            locale: $input.data('locale'),
            format: $input.data('format'),
            minDate: $input.data('mindate'),
            maxDate: $input.data('maxdate')
        });

        $input.on("dp.change", DateRangePicker.changeDateRangeTo);
        $input.on("dp.show", DateRangePicker.highlightDateRangeTo);
        $input.on("dp.update", DateRangePicker.highlightDateRangeTo);
    },

    changeDateRangeFrom: function (e) {
        $(this).data('partner').data("DateTimePicker").minDate(e.date);
    },
    changeDateRangeTo: function (e) {
        $(this).data('partner').data("DateTimePicker").maxDate(e.date);
    },

    highlightDateRangeFrom: function () {
        DateRangePicker.highlightDateRange($(this), $(this).data('partner'), 'add');
    },
    highlightDateRangeTo: function () {
        DateRangePicker.highlightDateRange($(this).data('partner'), $(this), 'subtract');
    },

    /**
     * Подсветка дней в календаре
     * @param $inputFrom
     * @param $inputTo
     * @param type
     */
    highlightDateRange: function ($inputFrom, $inputTo, type) {
        if ($inputFrom.val() && $inputTo.val()) {
            var $widget = null;

            var locale = $inputFrom.data('locale');
            var format = $inputFrom.data('format');

            if (type == 'add') {
                $widget = $inputFrom.parent().find('.bootstrap-datetimepicker-widget');
            } else {
                $widget = $inputTo.parent().find('.bootstrap-datetimepicker-widget');
            }

            moment.locale(locale);

            var momentFrom = moment($inputFrom.val(), format);
            var momentTo = moment($inputTo.val(), format);

            $widget.find('td').removeClass('range-start').removeClass('range-end');

            $widget.find('td[data-day="' + momentFrom.format('L') + '"]').addClass('range-start');
            $widget.find('td[data-day="' + momentTo.format('L') + '"]').addClass('range-end');

            while (momentFrom.format('x') < momentTo.format('x')) {
                var momentCurrent = null;

                if (type == 'add') {
                    momentCurrent = momentFrom.add(1, 'days').format('L');
                } else {
                    momentCurrent = momentTo.subtract(1, 'days').format('L');
                }

                var $td = $widget.find('td[data-day="' + momentCurrent + '"]');

                if ($td.hasClass('disabled')) {
                    break;
                }

                $td.addClass('ranged');
            }
        }
    },

    /**
     * Установка интервала для дат
     */
    changeRangeDates: function($inputFrom, $inputTo, range) {
        var locale = $inputFrom.data('locale');
        var format = $inputFrom.data('format');

        moment.locale(locale);

        var dateFrom = moment();
        var dateTo = moment();

        switch (range) {
            case 'today':

                break;
            case 'yesterday':
                dateFrom = moment().subtract(1, 'days');
                dateTo = moment().subtract(1, 'days');
                break;
            case 'current_week':
                dateFrom = moment().startOf('week');
                break;
            case 'before_week':
                dateFrom = moment().startOf('week').subtract(7, 'days');
                dateTo = moment().startOf('week').subtract(1, 'days');
                break;
            case 'current_month':
                dateFrom = moment().startOf('month');
                dateTo = moment().endOf('month');
                break;
            case 'before_month':
                dateFrom = moment().startOf('month').subtract(1, 'days').startOf('month');
                dateTo = moment().startOf('month').subtract(1, 'days');
                break;
            case 'tomorrow':
                dateFrom = moment().add(1, 'days');
                dateTo = moment().add(1, 'days');
                break;
            case 'next_week':
                dateFrom = moment().add(1, 'weeks').startOf('week');
                dateTo = moment().add(1, 'weeks').endOf('week');
                break;
            case 'next_two_weeks':
                dateFrom = moment().add(1, 'weeks').startOf('week');
                dateTo = moment().add(2, 'weeks').endOf('week');
                break;
            case 'next_month':
                dateFrom = moment().add(1, 'months').startOf('month');
                dateTo = moment().add(1, 'months').endOf('month');
                break;
            default:
                if (range.match(/before_month_/)) {
                    var p = range.split('before_month_');
                    dateFrom = moment().startOf('month').subtract(p[1], 'month').startOf('month');
                    dateTo = moment().startOf('month').subtract(p[1], 'month').endOf('month');
                }
                break;
        }

        var currentTimestampFrom = parseInt(moment($inputFrom.val(), format).format('x'));
        var currentTimestampTo = parseInt(moment($inputTo.val(), format).format('x'));
        var newTimestampTo = dateTo.format('x');

        if (isNaN(currentTimestampFrom)) {
            currentTimestampFrom = 0;
        }

        if (isNaN(currentTimestampTo)) {
            currentTimestampTo = 0;
        }

        if (currentTimestampFrom == 0) {
            $inputTo.data("DateTimePicker").date(dateTo);
            $inputFrom.data("DateTimePicker").date(dateFrom);
        } else if (currentTimestampTo == 0) {
            $inputFrom.data("DateTimePicker").date(dateFrom);
            $inputTo.data("DateTimePicker").date(dateTo);
        } else {
            if (newTimestampTo >= currentTimestampTo) {
                $inputTo.data("DateTimePicker").date(dateTo);
                $inputFrom.data("DateTimePicker").date(dateFrom);
            } else {
                $inputFrom.data("DateTimePicker").date(dateFrom);
                $inputTo.data("DateTimePicker").date(dateTo);
            }
        }
    }
};
