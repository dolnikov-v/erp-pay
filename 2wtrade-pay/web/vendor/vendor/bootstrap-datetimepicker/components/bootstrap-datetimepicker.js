$.components.register("datetimepicker", {
    mode: "init",
    defaults: {
        locale: 'ru',
        format: 'DD.MM.YYYY'
    },
    init: function (context) {
        var defaults = $.components.getDefaults("datetimepicker");

        $('[data-plugin="datetimepicker"]', context).each(function () {
            var options = {
                locale: $(this).data('locale'),
                format: $(this).data('format'),
                minDate: $(this).data('mindate'),
                maxDate: $(this).data('maxdate')
            };

            options = $.extend(defaults, options);

            $(this).datetimepicker(options);
        });
    }
});
