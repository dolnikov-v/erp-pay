$.components.register("daterangepicker", {
    mode: "init",
    defaults: {
        locale: 'ru',
        format: 'DD.MM.YYYY'
    },
    init: function (context) {
        var defaults = $.components.getDefaults("daterangepicker");

        $('[data-plugin="daterangepicker"]', context).each(function () {
            var options = {
                locale: $(this).data('locale'),
                format: $(this).data('format'),
                minDate: $(this).data('mindate'),
                maxDate: $(this).data('maxdate')
            };

            options = $.extend(defaults, options);

            var $inputFrom = $(this).find('.inputs-daterangepicker input.form-control:eq(0)');
            var $inputTo = $(this).find('.inputs-daterangepicker input.form-control:eq(1)');
            var $rangeCollection = $(this).find('.range-collection-daterangepicker select.form-control');

            $inputFrom.data('locale', options.locale).data('format', options.format).data('partner', $inputTo);
            $inputTo.data('locale', options.locale).data('format', options.format).data('partner', $inputFrom);
            $rangeCollection.data('locale', options.locale).data('format', options.format);

            DateRangePicker.initDateRangeFrom($inputFrom);
            DateRangePicker.initDateRangeTo($inputTo);

            $rangeCollection.on('change', function() {
                DateRangePicker.changeRangeDates($inputFrom, $inputTo, $(this).val());
            });
        });
    }
});
