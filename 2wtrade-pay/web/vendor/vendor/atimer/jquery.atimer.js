(function ($) {
    var methods = {
        init: function (options) {
            return this.each(function () {
                var $this = $(this);

                var settings = $.extend({
                    'format': 'DD.MM.YYYY HH:mm:ss'
                }, options);

                $this.data('settings', settings);

                var timestamp = parseInt(moment($this.text(), settings['format']).format('x'));
                var offset = moment().format('s') * 1000 - moment($this.text(), settings['format']).format('s') * 1000;

                if (offset != 0) {
                    timestamp = timestamp + offset;
                }

                $this.data('timestamp', timestamp);

                var timer = setInterval(function () {
                    $this.atimer('tick');
                }, 1000);

                $this.data('timer', timer);
                $this.atimer('tick');
            });
        },
        tick: function () {
            var $this = $(this);
            var settings = $this.data('settings');

            var timestamp = parseInt($this.data('timestamp')) + 1000;
            $this.text(moment(timestamp).format(settings['format']));

            $this.data('timestamp', timestamp);

            return this;
        },
        stop: function () {
            var $this = $(this);

            clearInterval($this.data('timer'));

            return this;
        }
    };

    $.fn.atimer = function (method) {
        if (typeof moment == 'undefined') {
            throw 'jQuery.atimer requires Moment.js to be loaded first';
        }

        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            throw 'The method with the name ' + method + ' does not exist for jQuery.atimer';
        }
    };
})(jQuery);

$(function() {
    $('.atimer-auto-init').each(function() {
        var options = {
            'format': $(this).data('format')
        };

        $(this).atimer(options);
    });
});
