$(function () {
    if ($('.select2-auto-ellipsis').length) {
        $('.select2-auto-ellipsis').on('select2:open', function() {
            setTimeout(function() {
                $('.select2-results__options li.select2-results__option').addClass('ellipsis');
            }, 0);
        });
    }
});
