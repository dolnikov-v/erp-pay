$(function () {
    TagFilter.init();
});

var TagFilter = {
    init: function () {
        $('.container-tag-filter .add-tag-filter').on('click', TagFilter.addInput);
        $('.container-tag-filter .delete-tag-filter').on('click', TagFilter.removeInput);

        if($('.add-tag-filter').length == 1){
            $('.delete-tag-filter').hide();
        }
    },
    addInput: function (e) {
        e.preventDefault();
        $('.container-tag-filter').append($(this).closest('.row-tag-filter').clone());
        $(this).hide();
        $(this).parent().find('.delete-tag-filter').show();
        $(this).parent().find('.delete-tag-filter').removeClass('hidden');
        TagFilter.init();
    },
    removeInput: function (e) {
        e.preventDefault();
        $(this).closest('.row-tag-filter').remove();
    }
};
