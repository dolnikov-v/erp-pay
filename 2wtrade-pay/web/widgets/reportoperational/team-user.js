$(function () {
    ManageTeamUsers.init();
    User.init();
});

var ManageTeamUsers = {
    init: function () {
        $('.btn-team-users-plus').off('click');
        $('.btn-team-users-minus').off('click');

        $('.btn-team-users-plus').on('click', ManageTeamUsers.addInput);
        $('.btn-team-users-minus').on('click', ManageTeamUsers.removeInput);

        if ($('.group-users-team-group .row-team-users').find('.btn-team-users-minus').size() == 1) {
            $('.btn-team-users-minus').hide();
        } else {
            $('.btn-team-users-minus').show();
            $('.btn-team-users-plus').hide();
            $('.btn-team-users-minus').last().hide();
            $('.btn-team-users-plus').last().show();
        }

    },
    addInput: function () {
        $('.group-users-team-group .row-team-users').last().clone().appendTo('.group-users-team-group');

        User.init();

        $('.group-users-team-group .row-team-users').last().find('input').each(function () {
            $(this).val('');
        })

        $('.group-users-team-group .row-team-users').last().find('select').each(function () {
            $(this).parent().find('.select2').remove();
            $(this).val(0);
            $(this).select2();
            $(this).parent().find('.select2').css('width', '100%');
        });

        ManageTeamUsers.init();
    },
    removeInput: function () {
        $(this).parents().each(function () {
            if ($(this).hasClass('row-team-users')) {
                $(this).remove();
            }
        });

        ManageTeamUsers.init();
    }
};

var User = {
    init: function () {
        $('.group-users-team-group .user select').on('change', User.change);
    },
    change: function () {
        $countriesSelect = $(this).closest('.row-team-users').find('.countries select');
        $nameCountriesSelect = $countriesSelect.attr('name');
        // $nameCountriesSelect must be "TeamCountryUser[country_ids_<число>][][]"
        $positionBegin = $nameCountriesSelect.indexOf('country_ids_');
        $positionEnd = $nameCountriesSelect.indexOf(']');
        $newNameCountriesSelect = $nameCountriesSelect.substr(0, $positionBegin) + 'country_ids_' + $(this).val() + $nameCountriesSelect.substr($positionEnd);
        $countriesSelect.attr('name', $newNameCountriesSelect);
    }
};
