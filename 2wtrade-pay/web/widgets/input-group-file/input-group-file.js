$(function () {
    InputGroupFile.init();
});

var InputGroupFile = {
    init: function () {
        $('.input-group-file input[type="file"]').on('change', InputGroupFile.change);
    },
    change: function () {
        var fileName = $(this).val().split('/').pop().split('\\').pop();

        $(this).closest('.input-group-file').find('input[type="text"]').val(fileName);
    }
};
