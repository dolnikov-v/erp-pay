$(function () {
    OrderFilters.init();
});

/**
 * Общий фильтр
 */
var OrderFilters = {
    $inputFilters: $('#filter_orders_selectable_filters'),

    init: function () {
        OrderFiltersDate.init();
        OrderFiltersTabs.init();
        OrderFilterCheckingType.init();

        $(document).on('click', '.add-selectable-filters', OrderFilters.addFilter);
        $(document).on('click', '.remove-selectable-filters', OrderFilters.removeFilter);
        $(document).on('click', '.not-selectable-filters', OrderFilters.toggleNot);
        $('.shower-selectable-filters').on('click', OrderFilters.showFilters);
    },

    addFilter: function () {
        var filter = OrderFilters.$inputFilters.val();

        $('#row_selectable_filters').before($('#container_empty_filters').find('.container-empty-filter[data-filter="' + filter + '"]').clone());
        $('#row_selectable_filters').prev().find('.container-selectable-filters select').select2({width: '100%'});
        OrderFilterCheckingType.init();
    },

    removeFilter: function () {
        var $form = $(this).closest('form');

        var $row = $(this).closest('.container-empty-filter[data-filter="' + $(this).data('filter') + '"]');
        $row.remove();

        $form.find('.shower-selectable-filters[data-filter="' + $(this).data('filter') + '"]').trigger('click');
    },

    toggleNot: function () {
        var $input = $(this).find('input');

        $(this).toggleClass('btn-default').toggleClass('btn-danger');

        if ($(this).hasClass('btn-default')) {
            $input.val('0');
        } else {
            $input.val('1');
        }
    },

    showFilters: function () {
        var $form = $(this).closest('form');
        var filter = $(this).data('filter');

        $form.find('.container-empty-filter[data-filter="' + filter + '"]').show()
            .find('.form-group.form-group-no-margin').removeClass('form-group-no-margin');

        $(this).closest('div.row').remove();

        return false;
    }
};

/**
 * По табам
 */
var OrderFiltersTabs = {
    $tabFilters: $('#tab0'),
    $tabOperations: $('#tab1'),

    init: function () {
        $('a[data-toggle="tab"]').on('shown.bs.tab', OrderFiltersTabs.toggle);

        OrderFiltersTabs.toggle();
    },

    toggle: function () {
        if (OrderFiltersTabs.$tabFilters.hasClass('active')) {
            OrderFiltersTabs.shownTabFilters();
        } else if (OrderFiltersTabs.$tabOperations.hasClass('active')) {
            OrderFiltersTabs.shownTabOperations();
        }
    },

    shownTabFilters: function () {

    },

    shownTabOperations: function () {

    }
};

/**
 * Фильтр по датам
 */
var OrderFiltersDate = {
    $inputDateType: $('select[name="DateFilter[dateType]"]'),
    $inputFrom: $('input[name="DateFilter[dateFrom]"]'),
    $inputTo: $('input[name="DateFilter[dateTo]"]'),
    $inputRange: $('.range-collection-daterangepicker select.form-control'),

    init: function () {
        OrderFiltersDate.$inputDateType.on('change', OrderFiltersDate.changeDateType);

        if (OrderFiltersDate.$inputDateType.val()) {
            OrderFiltersDate.setDisabledInputs(false);
        }
    },

    setDisabledInputs: function (value) {
        OrderFiltersDate.$inputFrom.prop('disabled', value);
        OrderFiltersDate.$inputTo.prop('disabled', value);
        OrderFiltersDate.$inputRange.prop('disabled', value);
    },

    changeDateType: function () {
        if (OrderFiltersDate.$inputDateType.val()) {
            OrderFiltersDate.setDisabledInputs(false);
            OrderFiltersDate.$inputRange.trigger('change');
        } else {
            OrderFiltersDate.setDisabledInputs(true);
            OrderFiltersDate.$inputFrom.val('');
            OrderFiltersDate.$inputTo.val('');
        }
    }
};

/**
 * Фильтр по типам очередей для проверки и типам возвращаемой инфы
 */
var OrderFilterCheckingType = {
    selectedCheckingType: null,
    init: function () {
        $('.checking-type-select').on('change', OrderFilterCheckingType.changeType);
    },
    changeType: function () {
        var $row = $(this).closest('.row');
        $row.find('.checking-type-information-select').addClass('hidden').find('select').attr('name', '');
        var $newSelect = $row.find('.checking-type-information-select[data-checking-type="' + $(this).val() + '"]');
        $newSelect.removeClass('hidden');
        $newSelect.find('select').attr('name', $(this).data('name-for-information'));
    }
};

