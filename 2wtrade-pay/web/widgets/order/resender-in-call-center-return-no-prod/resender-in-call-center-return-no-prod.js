var I18n = I18n || {};

$(function () {
    ResenderInCallCenterReturnNoProd.init();
});

var ResenderInCallCenterReturnNoProd = {
    ajax: null,
    count: 0,
    $modal: $('#modal_resend_in_call_center_return_no_prod'),
    $form: $('#modal_resend_in_call_center_return_no_prod').find('form'),

    init: function () {
        $('#start_resend_in_call_center_return_no_prod').on('click', ResenderInCallCenterReturnNoProd.start);
        $('#stop_resend_in_call_center_return_no_prod').on('click', ResenderInCallCenterReturnNoProd.stop);
    },

    start: function () {
        ResenderInCallCenterReturnNoProd.$modal.find('.sender-percent').text('0%');
        ResenderInCallCenterReturnNoProd.$modal.find('.progress-bar').css('width', '0%');

        ResenderInCallCenterReturnNoProd.$modal.find('.row-with-btn-start').hide();
        ResenderInCallCenterReturnNoProd.$modal.find('.row-with-text-start').hide();
        ResenderInCallCenterReturnNoProd.$modal.find('.row-with-btn-stop').show();
        ResenderInCallCenterReturnNoProd.$modal.find('.row-with-text-stop').show();

        ResenderInCallCenterReturnNoProd.count = ResenderInCallCenterReturnNoProd.$form.find('input[name="id[]"]').length;

        if (ResenderInCallCenterReturnNoProd.count) {
            ResenderInCallCenterReturnNoProd.next();
        } else {
            $.notify({message: I18n['Необходимо выбрать заказы.']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            ResenderInCallCenterReturnNoProd.stop();
        }
    },

    next: function () {
        var $id = ResenderInCallCenterReturnNoProd.$form.find('input[name="id[]"]:first');

        if ($id.length) {
            var percent = ResenderInCallCenterReturnNoProd.calculatePercent();

            ResenderInCallCenterReturnNoProd.$modal.find('.sender-percent').text(percent + '%');
            ResenderInCallCenterReturnNoProd.$modal.find('.progress-bar').css('width', percent + '%');

            $id.remove();

            var dataAjax = {
                id: $id.val()
            };

            var $checkbox = $('#orders_content input[name="id[]"][value="' + $id.val() + '"]');
            $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
            $checkbox.closest('tr').removeClass('text-danger');

            ResenderInCallCenterReturnNoProd.ajax = $.ajax({
                type: 'POST',
                url: ResenderInCallCenterReturnNoProd.$form.data('url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        $checkbox.closest('tr').remove();
                        ResenderInCallCenterReturnNoProd.next();
                    } else {
                        $checkbox.prop('checked', false);
                        $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                        $checkbox.closest('tr').addClass('text-danger');
                        ResenderInCallCenterReturnNoProd.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    $checkbox.prop('checked', false);
                    ResenderInCallCenterReturnNoProd.stop();
                    response = JSON.parse(response.responseText);

                    if (response.statusText != 'abort') {
                        if (typeof response.message != 'undefined') {
                            $.notify({message: response.message}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        } else {
                            $.notify({message: I18n['Не удалось отправить заказ в колл-центр.']}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }
                    }
                }
            });
        } else {
            ResenderInCallCenterReturnNoProd.ajax = null;
            ResenderInCallCenterReturnNoProd.$modal.modal('hide');

            $.notify({message: I18n['Отправка заказов в колл-центр завершена.']}, {
                type: "success dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
        }
    },

    stop: function () {
        if (ResenderInCallCenterReturnNoProd.ajax != null) {
            ResenderInCallCenterReturnNoProd.ajax.abort();
        }

        ResenderInCallCenterReturnNoProd.$modal.find('.row-with-btn-start').show();
        ResenderInCallCenterReturnNoProd.$modal.find('.row-with-text-start').show();
        ResenderInCallCenterReturnNoProd.$modal.find('.row-with-btn-stop').hide();
        ResenderInCallCenterReturnNoProd.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (ResenderInCallCenterReturnNoProd.count) {
            var part = 100 / ResenderInCallCenterReturnNoProd.count;
            var count = ResenderInCallCenterReturnNoProd.count - ResenderInCallCenterReturnNoProd.$form.find('input[name="id[]"]').length;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
