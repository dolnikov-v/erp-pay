var I18n = I18n || {};

$(function () {
    SendInformation.init();
});

var send_information = $('#modal_send_information'),
    SendInformation = {
    count: 0,
    currentCount: 0,
    $modal: send_information,
    $form: send_information.find('form'),
    timer: 0,

    init: function () {
        $('#start_send_information').on('click', SendInformation.start);
        $('#stop_send_information').on('click', SendInformation.stop);
    },

    start: function () {
        SendInformation.$modal.find('.sender-percent').text('0%');
        SendInformation.$modal.find('.progress-bar').css('width', '0%');

        SendInformation.$modal.find('.row-with-btn-start').hide();
        SendInformation.$modal.find('.row-with-text-start').hide();
        SendInformation.$modal.find('.row-with-btn-stop').show();
        SendInformation.$modal.find('.row-with-text-stop').show();

        SendInformation.count = SendInformation.$form.find('input[name="id[]"]').length;


        if ($('#send_information_comment').val() == '') {
            $.notify({message: I18n['Введите комментарий.']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            SendInformation.stop();
            return false;
        }

        if (SendInformation.count) {
            SendInformation.next();
        } else {
            $.notify({message: I18n['Необходимо выбрать заказы.']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            SendInformation.stop();
        }
    },

    next: function () {
        var $id = SendInformation.$form.find('input[name="id[]"]:first');

        if ($id.length) {
            var percent = SendInformation.calculatePercent();

            SendInformation.$modal.find('.sender-percent').text(percent + '%');
            SendInformation.$modal.find('.progress-bar').css('width', percent + '%');

            $id.remove();

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {
                id: $id.val(),
                no_repeat: $('#send_information_no_repeat').prop('checked'),
                comment: $('#send_information_comment').val()
            };

            dataAjax[csrfParam] = csrfToken;

            var $checkbox = $('#orders_content input[name="id[]"][value="' + $id.val() + '"]');
            $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
            $checkbox.closest('tr').removeClass('text-danger');

            SendInformation.ajax = $.ajax({
                type: 'POST',
                url: SendInformation.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    console.log(1);
                    if (response.status == 'success') {
                        if (response.notifyWarning) {
                            $.notify({message: response.notifyMessage}, {
                                type: "warning dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }

                        $checkbox.closest('tr').remove();
                        SendInformation.next();
                    } else {
                        $checkbox.prop('checked', false);
                        $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                        $checkbox.closest('tr').addClass('text-danger');
                        SendInformation.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    $checkbox.prop('checked', false);
                    SendInformation.stop();
                    response = JSON.parse(response.responseText);

                    if (response.statusText != 'abort') {
                        if (typeof response.message != 'undefined') {
                            $.notify({message: response.message}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        } else {
                            $.notify({message: I18n['Не удалось отправить заказ в колл-центр.']}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }
                    }
                }
            });
        } else {
            SendInformation.ajax = null;
            SendInformation.stop();
            SendInformation.$modal.modal('hide');

            $.notify({message: I18n['Отправка заказов в колл-центр завершена.']}, {
                type: "success dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
        }
    },

    stop: function () {
        if (SendInformation.ajax != null) {
            SendInformation.ajax.abort();
        }

        SendInformation.$modal.find('.row-with-btn-start').show();
        SendInformation.$modal.find('.row-with-text-start').show();
        SendInformation.$modal.find('.row-with-btn-stop').hide();
        SendInformation.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (SendInformation.count) {
            var part = 100 / SendInformation.count;
            var count = SendInformation.count - SendInformation.$form.find('input[name="id[]"]').length;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
