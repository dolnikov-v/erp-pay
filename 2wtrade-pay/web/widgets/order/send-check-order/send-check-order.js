var I18n = I18n || {};

$(function () {
    SendCheckOrder.init();
});

var send_check_order = $('#modal_send_check_order'),
    SendCheckOrder = {
    count: 0,
    currentCount: 0,
    $modal: send_check_order,
    $form: send_check_order.find('form'),
    timer: 0,

    init: function () {
        $('#start_send_check_order').on('click', SendCheckOrder.start);
        $('#stop_send_check_order').on('click', SendCheckOrder.stop);
    },

    start: function () {
        SendCheckOrder.$modal.find('.sender-percent').text('0%');
        SendCheckOrder.$modal.find('.progress-bar').css('width', '0%');

        SendCheckOrder.$modal.find('.row-with-btn-start').hide();
        SendCheckOrder.$modal.find('.row-with-text-start').hide();
        SendCheckOrder.$modal.find('.row-with-btn-stop').show();
        SendCheckOrder.$modal.find('.row-with-text-stop').show();

        SendCheckOrder.count = SendCheckOrder.$form.find('input[name="id[]"]').length;

        if (SendCheckOrder.count) {
            SendCheckOrder.next();
        } else {
            $.notify({message: I18n['Необходимо выбрать заказы.']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            SendCheckOrder.stop();
        }
    },

    next: function () {
        var $id = SendCheckOrder.$form.find('input[name="id[]"]:first');

        if ($id.length) {
            var percent = SendCheckOrder.calculatePercent();

            SendCheckOrder.$modal.find('.sender-percent').text(percent + '%');
            SendCheckOrder.$modal.find('.progress-bar').css('width', percent + '%');

            $id.remove();

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {
                id: $id.val(),
                delivery: $('#send_check_order').val()
            };

            dataAjax[csrfParam] = csrfToken;

            var $checkbox = $('#orders_content input[name="id[]"][value="' + $id.val() + '"]');
            $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
            $checkbox.closest('tr').removeClass('text-danger');

            SendCheckOrder.ajax = $.ajax({
                type: 'POST',
                url: SendCheckOrder.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    console.log(1);
                    if (response.status == 'success') {
                        if (response.notifyWarning) {
                            $.notify({message: response.notifyMessage}, {
                                type: "warning dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }

                        $checkbox.closest('tr').remove();
                        SendCheckOrder.next();
                    } else {
                        $checkbox.prop('checked', false);
                        $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                        $checkbox.closest('tr').addClass('text-danger');
                        SendCheckOrder.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    $checkbox.prop('checked', false);
                    SendCheckOrder.stop();
                    response = JSON.parse(response.responseText);

                    if (response.statusText != 'abort') {
                        if (typeof response.message != 'undefined') {
                            $.notify({message: response.message}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        } else {
                            $.notify({message: I18n['Не удалось передать заказ в службу доставки.']}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }
                    }
                }
            });
        } else {
            SendCheckOrder.ajax = null;
            SendCheckOrder.stop();
            SendCheckOrder.$modal.modal('hide');

            $.notify({message: I18n['Передача заказов в службу доставки завершена.']}, {
                type: "success dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
        }
    },

    stop: function () {
        if (SendCheckOrder.ajax != null) {
            SendCheckOrder.ajax.abort();
        }

        SendCheckOrder.$modal.find('.row-with-btn-start').show();
        SendCheckOrder.$modal.find('.row-with-text-start').show();
        SendCheckOrder.$modal.find('.row-with-btn-stop').hide();
        SendCheckOrder.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (SendCheckOrder.count) {
            var part = 100 / SendCheckOrder.count;
            var count = SendCheckOrder.count - SendCheckOrder.$form.find('input[name="id[]"]').length;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
