var I18n = I18n || {};

$(function () {
    ClarificationInDelivery.init();
});

var ClarificationInDelivery = {
    count: 0,
    currentCount: 0,
    $modal: $('#modal_clarification_in_delivery'),
    $form: $('#modal_clarification_in_delivery').find('form'),
    timer: 0,

    init: function () {
        $('#start_clarification_in_delivery').on('click', ClarificationInDelivery.start);
        $('#stop_clarification_in_delivery').on('click', ClarificationInDelivery.stop);
    },

    start: function () {
        ClarificationInDelivery.$modal.find('.clarification-percent').text('0%');
        ClarificationInDelivery.$modal.find('.progress-bar').css('width', '0%');

        ClarificationInDelivery.$modal.find('.row-with-btn-start').hide();
        ClarificationInDelivery.$modal.find('.row-with-text-start').hide();
        ClarificationInDelivery.$modal.find('.row-with-btn-stop').show();
        ClarificationInDelivery.$modal.find('.row-with-text-stop').show();

        ClarificationInDelivery.count = ClarificationInDelivery.$form.find('input[name="id[]"]').length;


        if (!$('.deliveryemails:checked').length) {
            $.notify({message: I18n['Выберите получателей.']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            ClarificationInDelivery.stop();
            return false;
        }

        if ($('#message_subject').val() == '') {
            $.notify({message: I18n['Введите тему.']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            ClarificationInDelivery.stop();
            return false;
        }

        if ($('#message_body').val() == '') {
            $.notify({message: I18n['Введите сообщение.']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            ClarificationInDelivery.stop();
            return false;
        }

        if (ClarificationInDelivery.count) {
            ClarificationInDelivery.next();
        } else {
            $.notify({message: I18n['Необходимо выбрать заказы.']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            ClarificationInDelivery.stop();
        }
    },

    next: function () {
        var order_ids = [];
        ClarificationInDelivery.$form.find('input[name="id[]"]').each(function () {
            order_ids.push($(this).val());
        });

        var emails = [];
        ClarificationInDelivery.$form.find('.deliveryemails:checked').each(function () {
            emails.push($(this).val());
        });

        if (order_ids.length) {
            var percent = ClarificationInDelivery.calculatePercent();

            ClarificationInDelivery.$modal.find('.clarification-percent').text(percent + '%');
            ClarificationInDelivery.$modal.find('.progress-bar').css('width', percent + '%');

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {
                id: order_ids.join(),
                subject: $('#message_subject').val(),
                body: $('#message_body').val(),
                emails: emails
            };

            dataAjax[csrfParam] = csrfToken;

            ClarificationInDelivery.ajax = $.ajax({
                type: 'POST',
                url: ClarificationInDelivery.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    ClarificationInDelivery.stop();
                    if (response.status == 'success') {
                        ClarificationInDelivery.ajax = null;
                        ClarificationInDelivery.$modal.modal('hide');

                        ClarificationInDelivery.$form.find('input[name="id[]"]').each(function () {
                            var $checkbox = $('#orders_content input[name="id[]"][value="' + $(this).val() + '"]');
                            $checkbox.closest('tr').removeClass('active');
                            $checkbox.prop('checked', false);
                        });

                        $.notify({message: I18n['Сформирована очередь отправки на уточнение.']}, {
                            type: "success dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    } else {
                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    $checkbox.prop('checked', false);
                    ClarificationInDelivery.stop();
                    response = JSON.parse(response.responseText);

                    if (response.statusText != 'abort') {
                        if (typeof response.message != 'undefined') {
                            $.notify({message: response.message}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        } else {
                            $.notify({message: I18n['Не удалось передать заказ в службу доставки.']}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }
                    }
                }
            });
        }
    },

    stop: function () {
        if (ClarificationInDelivery.ajax != null) {
            ClarificationInDelivery.ajax.abort();
        }

        ClarificationInDelivery.$modal.find('.row-with-btn-start').show();
        ClarificationInDelivery.$modal.find('.row-with-text-start').show();
        ClarificationInDelivery.$modal.find('.row-with-btn-stop').hide();
        ClarificationInDelivery.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (ClarificationInDelivery.count) {
            var part = 100 / ClarificationInDelivery.count;
            var count = ClarificationInDelivery.count - ClarificationInDelivery.$form.find('input[name="id[]"]').length;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
