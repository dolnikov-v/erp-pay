var I18n = I18n || {};

$(function () {
    DeliveryRequestStatusChanger.init();
});

var DeliveryRequestStatusChanger = {
    ajax: null,
    count: 0,
    $modal: $('#modal_delivery_request_status_changer'),
    $form: $('#modal_delivery_request_status_changer').find('form'),

    init: function () {
        $('#start_delivery_request_status_changer').on('click', DeliveryRequestStatusChanger.start);
        $('#stop_delivery_request_status_changer').on('click', DeliveryRequestStatusChanger.stop);
    },

    start: function () {
        DeliveryRequestStatusChanger.$modal.find('.sender-percent').text('0%');
        DeliveryRequestStatusChanger.$modal.find('.progress-bar').css('width', '0%');

        DeliveryRequestStatusChanger.$modal.find('.row-with-btn-start').hide();
        DeliveryRequestStatusChanger.$modal.find('.row-with-text-start').hide();
        DeliveryRequestStatusChanger.$modal.find('.row-with-btn-stop').show();
        DeliveryRequestStatusChanger.$modal.find('.row-with-text-stop').show();

        DeliveryRequestStatusChanger.count = DeliveryRequestStatusChanger.$form.find('input[name="id[]"]').length;

        if (DeliveryRequestStatusChanger.count) {
            DeliveryRequestStatusChanger.next();
        } else {
            $.notify({message: I18n['Необходимо выбрать заказы.']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            DeliveryRequestStatusChanger.stop();
        }
    },

    next: function () {
        var $id = DeliveryRequestStatusChanger.$form.find('input[name="id[]"]:first');

        if ($id.length) {
            var percent = DeliveryRequestStatusChanger.calculatePercent();

            DeliveryRequestStatusChanger.$modal.find('.sender-percent').text(percent + '%');
            DeliveryRequestStatusChanger.$modal.find('.progress-bar').css('width', percent + '%');

            $id.remove();

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {
                id: $id.val(),
                status: $('#delivery_request_status_changer_select').val(),
                comment: $('#delivery_request_status_changer_comment').val()
            };

            dataAjax[csrfParam] = csrfToken;

            var $checkbox = $('#orders_content input[name="id[]"][value="' + $id.val() + '"]');
            $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
            $checkbox.closest('tr').removeClass('text-danger');

            DeliveryRequestStatusChanger.ajax = $.ajax({
                type: 'POST',
                url: DeliveryRequestStatusChanger.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        $checkbox.closest('tr').remove();
                        DeliveryRequestStatusChanger.next();
                    } else {
                        $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                        $checkbox.closest('tr').addClass('text-danger');
                        DeliveryRequestStatusChanger.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    DeliveryRequestStatusChanger.stop();
                    response = JSON.parse(response.responseText);

                    if (response.statusText != 'abort') {
                        if (typeof response.message != 'undefined') {
                            $.notify({message: response.message}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        } else {
                            $.notify({message: I18n['Не удалось сменить статус.']}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }
                    }
                }
            });
        } else {
            DeliveryRequestStatusChanger.ajax = null;
            DeliveryRequestStatusChanger.$modal.modal('hide');

            $.notify({message: I18n['Смена статусов завершена.']}, {
                type: "success dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
        }
    },

    stop: function () {
        if (DeliveryRequestStatusChanger.ajax != null) {
            DeliveryRequestStatusChanger.ajax.abort();
        }

        DeliveryRequestStatusChanger.$modal.find('.row-with-btn-start').show();
        DeliveryRequestStatusChanger.$modal.find('.row-with-text-start').show();
        DeliveryRequestStatusChanger.$modal.find('.row-with-btn-stop').hide();
        DeliveryRequestStatusChanger.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (DeliveryRequestStatusChanger.count) {
            var part = 100 / DeliveryRequestStatusChanger.count;
            var count = DeliveryRequestStatusChanger.count - DeliveryRequestStatusChanger.$form.find('input[name="id[]"]').length;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
