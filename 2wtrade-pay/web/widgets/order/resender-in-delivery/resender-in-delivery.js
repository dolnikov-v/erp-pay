var I18n = I18n || {};

$(function () {
    ResenderInDelivery.init();
});

var ResenderInDelivery = {
    count: 0,
    currentCount: 0,
    $modal: $('#modal_resender_in_delivery'),
    $form: $('#modal_resender_in_delivery').find('form'),
    timer: 0,

    init: function () {
        $('#start_resender_in_delivery').on('click', ResenderInDelivery.start);
        $('#stop_resender_in_delivery').on('click', ResenderInDelivery.stop);
    },

    start: function () {
        ResenderInDelivery.$modal.find('.resender-percent').text('0%');
        ResenderInDelivery.$modal.find('.progress-bar').css('width', '0%');

        ResenderInDelivery.$modal.find('.row-with-btn-start').hide();
        ResenderInDelivery.$modal.find('.row-with-text-start').hide();
        ResenderInDelivery.$modal.find('.row-with-btn-stop').show();
        ResenderInDelivery.$modal.find('.row-with-text-stop').show();

        ResenderInDelivery.count = ResenderInDelivery.$form.find('input[name="id[]"]').length;

        if (ResenderInDelivery.count) {
            ResenderInDelivery.next();
        } else {
            $.notify({message: I18n['Необходимо выбрать заказы.']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            ResenderInDelivery.stop();
        }
    },

    next: function () {
        var $id = ResenderInDelivery.$form.find('input[name="id[]"]:first');

        if ($id.length) {
            var percent = ResenderInDelivery.calculatePercent();

            ResenderInDelivery.$modal.find('.resender-percent').text(percent + '%');
            ResenderInDelivery.$modal.find('.progress-bar').css('width', percent + '%');

            $id.remove();

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {
                id: $id.val(),
                delivery: $('#resender_in_delivery_select').val()
            };

            dataAjax[csrfParam] = csrfToken;

            var $checkbox = $('#orders_content input[name="id[]"][value="' + $id.val() + '"]');
            $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
            $checkbox.closest('tr').removeClass('text-danger');

            ResenderInDelivery.ajax = $.ajax({
                type: 'POST',
                url: ResenderInDelivery.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        $checkbox.closest('tr').remove();
                        ResenderInDelivery.next();
                    } else {
                        $checkbox.prop('checked', false);
                        $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                        $checkbox.closest('tr').addClass('text-danger');
                        ResenderInDelivery.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    $checkbox.prop('checked', false);
                    ResenderInDelivery.stop();
                    response = JSON.parse(response.responseText);

                    if (response.statusText != 'abort') {
                        if (typeof response.message != 'undefined') {
                            $.notify({message: response.message}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        } else {
                            $.notify({message: I18n['Не удалось передать заказ в службу доставки.']}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }
                    }
                }
            });
        } else {
            ResenderInDelivery.ajax = null;
            ResenderInDelivery.$modal.modal('hide');

            $.notify({message: I18n['Передача заказов в службу доставки завершена.']}, {
                type: "success dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
        }
    },

    stop: function () {
        if (ResenderInDelivery.ajax != null) {
            ResenderInDelivery.ajax.abort();
        }

        ResenderInDelivery.$modal.find('.row-with-btn-start').show();
        ResenderInDelivery.$modal.find('.row-with-text-start').show();
        ResenderInDelivery.$modal.find('.row-with-btn-stop').hide();
        ResenderInDelivery.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (ResenderInDelivery.count) {
            var part = 100 / ResenderInDelivery.count;
            var count = ResenderInDelivery.count - ResenderInDelivery.$form.find('input[name="id[]"]').length;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
