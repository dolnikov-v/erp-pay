var I18n = I18n || {};

$(function () {
    ResenderInCallCenterStillWaiting.init();
});

var ResenderInCallCenterStillWaiting = {
    ajax: null,
    count: 0,
    $modal: $('#modal_resend_in_call_center_still_waiting'),
    $form: $('#modal_resend_in_call_center_still_waiting').find('form'),

    init: function () {
        $('#start_resend_in_call_center_still_waiting').on('click', ResenderInCallCenterStillWaiting.start);
        $('#stop_resend_in_call_center_still_waiting').on('click', ResenderInCallCenterStillWaiting.stop);
    },

    start: function () {
        ResenderInCallCenterStillWaiting.$modal.find('.sender-percent').text('0%');
        ResenderInCallCenterStillWaiting.$modal.find('.progress-bar').css('width', '0%');

        ResenderInCallCenterStillWaiting.$modal.find('.row-with-btn-start').hide();
        ResenderInCallCenterStillWaiting.$modal.find('.row-with-text-start').hide();
        ResenderInCallCenterStillWaiting.$modal.find('.row-with-btn-stop').show();
        ResenderInCallCenterStillWaiting.$modal.find('.row-with-text-stop').show();

        ResenderInCallCenterStillWaiting.count = ResenderInCallCenterStillWaiting.$form.find('input[name="id[]"]').length;

        if (ResenderInCallCenterStillWaiting.count) {
            ResenderInCallCenterStillWaiting.next();
        } else {
            $.notify({message: I18n['Необходимо выбрать заказы.']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            ResenderInCallCenterStillWaiting.stop();
        }
    },

    next: function () {
        var $id = ResenderInCallCenterStillWaiting.$form.find('input[name="id[]"]:first');

        if ($id.length) {
            var percent = ResenderInCallCenterStillWaiting.calculatePercent();

            ResenderInCallCenterStillWaiting.$modal.find('.sender-percent').text(percent + '%');
            ResenderInCallCenterStillWaiting.$modal.find('.progress-bar').css('width', percent + '%');

            $id.remove();

            var dataAjax = {
                id: $id.val(),
                after_action_id: ResenderInCallCenterStillWaiting.$form.find('[name="after-action-id"]').val()
            };

            var $checkbox = $('#orders_content input[name="id[]"][value="' + $id.val() + '"]');
            $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
            $checkbox.closest('tr').removeClass('text-danger');

            ResenderInCallCenterStillWaiting.ajax = $.ajax({
                type: 'POST',
                url: ResenderInCallCenterStillWaiting.$form.data('url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        $checkbox.closest('tr').remove();
                        ResenderInCallCenterStillWaiting.next();
                    } else {
                        $checkbox.prop('checked', false);
                        $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                        $checkbox.closest('tr').addClass('text-danger');
                        ResenderInCallCenterStillWaiting.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    $checkbox.prop('checked', false);
                    ResenderInCallCenterStillWaiting.stop();
                    response = JSON.parse(response.responseText);

                    if (response.statusText != 'abort') {
                        if (typeof response.message != 'undefined') {
                            $.notify({message: response.message}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        } else {
                            $.notify({message: I18n['Не удалось отправить заказ в колл-центр.']}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }
                    }
                }
            });
        } else {
            ResenderInCallCenterStillWaiting.ajax = null;
            ResenderInCallCenterStillWaiting.$modal.modal('hide');

            $.notify({message: I18n['Отправка заказов в колл-центр завершена.']}, {
                type: "success dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
        }
    },

    stop: function () {
        if (ResenderInCallCenterStillWaiting.ajax != null) {
            ResenderInCallCenterStillWaiting.ajax.abort();
        }

        ResenderInCallCenterStillWaiting.$modal.find('.row-with-btn-start').show();
        ResenderInCallCenterStillWaiting.$modal.find('.row-with-text-start').show();
        ResenderInCallCenterStillWaiting.$modal.find('.row-with-btn-stop').hide();
        ResenderInCallCenterStillWaiting.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (ResenderInCallCenterStillWaiting.count) {
            var part = 100 / ResenderInCallCenterStillWaiting.count;
            var count = ResenderInCallCenterStillWaiting.count - ResenderInCallCenterStillWaiting.$form.find('input[name="id[]"]').length;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
