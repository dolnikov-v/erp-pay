var I18n = I18n || {};

$(function () {
    CallCenterRequestStatusChanger.init();
});

var CallCenterRequestStatusChanger = {
    ajax: null,
    count: 0,
    $modal: $('#modal_call_center_request_status_changer'),
    $form: $('#modal_call_center_request_status_changer').find('form'),

    init: function () {
        $('#start_call_center_request_status_changer').on('click', CallCenterRequestStatusChanger.start);
        $('#stop_call_center_request_status_changer').on('click', CallCenterRequestStatusChanger.stop);
    },

    start: function () {
        CallCenterRequestStatusChanger.$modal.find('.sender-percent').text('0%');
        CallCenterRequestStatusChanger.$modal.find('.progress-bar').css('width', '0%');

        CallCenterRequestStatusChanger.$modal.find('.row-with-btn-start').hide();
        CallCenterRequestStatusChanger.$modal.find('.row-with-text-start').hide();
        CallCenterRequestStatusChanger.$modal.find('.row-with-btn-stop').show();
        CallCenterRequestStatusChanger.$modal.find('.row-with-text-stop').show();

        CallCenterRequestStatusChanger.count = CallCenterRequestStatusChanger.$form.find('input[name="id[]"]').length;

        if (CallCenterRequestStatusChanger.count) {
            CallCenterRequestStatusChanger.next();
        } else {
            $.notify({message: I18n['Необходимо выбрать заказы.']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            CallCenterRequestStatusChanger.stop();
        }
    },

    next: function () {
        var $id = CallCenterRequestStatusChanger.$form.find('input[name="id[]"]:first');

        if ($id.length) {
            var percent = CallCenterRequestStatusChanger.calculatePercent();

            CallCenterRequestStatusChanger.$modal.find('.sender-percent').text(percent + '%');
            CallCenterRequestStatusChanger.$modal.find('.progress-bar').css('width', percent + '%');

            $id.remove();

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {
                id: $id.val(),
                status: $('#call_center_request_status_changer_select').val(),
                comment: $('#call_center_request_status_changer_comment').val()
            };

            dataAjax[csrfParam] = csrfToken;

            var $checkbox = $('#orders_content input[name="id[]"][value="' + $id.val() + '"]');
            $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
            $checkbox.closest('tr').removeClass('text-danger');

            CallCenterRequestStatusChanger.ajax = $.ajax({
                type: 'POST',
                url: CallCenterRequestStatusChanger.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        $checkbox.closest('tr').remove();
                        CallCenterRequestStatusChanger.next();
                    } else {
                        $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                        $checkbox.closest('tr').addClass('text-danger');
                        CallCenterRequestStatusChanger.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    CallCenterRequestStatusChanger.stop();
                    response = JSON.parse(response.responseText);

                    if (response.statusText != 'abort') {
                        if (typeof response.message != 'undefined') {
                            $.notify({message: response.message}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        } else {
                            $.notify({message: I18n['Не удалось сменить статус.']}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }
                    }
                }
            });
        } else {
            CallCenterRequestStatusChanger.ajax = null;
            CallCenterRequestStatusChanger.$modal.modal('hide');

            $.notify({message: I18n['Смена статусов завершена.']}, {
                type: "success dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
        }
    },

    stop: function () {
        if (CallCenterRequestStatusChanger.ajax != null) {
            CallCenterRequestStatusChanger.ajax.abort();
        }

        CallCenterRequestStatusChanger.$modal.find('.row-with-btn-start').show();
        CallCenterRequestStatusChanger.$modal.find('.row-with-text-start').show();
        CallCenterRequestStatusChanger.$modal.find('.row-with-btn-stop').hide();
        CallCenterRequestStatusChanger.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (CallCenterRequestStatusChanger.count) {
            var part = 100 / CallCenterRequestStatusChanger.count;
            var count = CallCenterRequestStatusChanger.count - CallCenterRequestStatusChanger.$form.find('input[name="id[]"]').length;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
