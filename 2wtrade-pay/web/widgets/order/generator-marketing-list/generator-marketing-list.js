var I18n = I18n || {};

$(function () {
    GeneratorMarketingList.init();
});

var GeneratorMarketingList = {
    count: 0,
    currentCount: 0,
    $modal: $('#modal_generate_marketing_list'),
    $form: $('#modal_generate_marketing_list').find('form'),
    timer: 0,

    init: function () {
        GeneratorMarketingList.$form.on('submit', GeneratorMarketingList.start);
    },

    start: function (event) {
        clearInterval(GeneratorMarketingList.timer);
        GeneratorMarketingList.count = GeneratorMarketingList.$form.find('input[name="id[]"]').length;
        GeneratorMarketingList.currentCount = 0;

        if (GeneratorMarketingList.count) {
            GeneratorMarketingList.$form.find('.row-with-text-start').hide();
            GeneratorMarketingList.$form.find('.row-with-btn-start').hide();
            GeneratorMarketingList.$form.find('.row-with-text-stop').show();

            GeneratorMarketingList.timer = setInterval(GeneratorMarketingList.tick, 300);
        } else {
            $.notify({message: I18n['Необходимо выбрать заказы.']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });

            event.stopImmediatePropagation();

            return false;
        }
    },

    stop: function () {
    },

    tick: function () {
        GeneratorMarketingList.currentCount++;

        var part = 100 / GeneratorMarketingList.count;
        var percent = parseInt(GeneratorMarketingList.currentCount * part);

        if (percent > 99) {
            percent = 99;
        }

        GeneratorMarketingList.$modal.find('.sender-percent').text(percent + '%');
        GeneratorMarketingList.$modal.find('.progress-bar').css('width', percent + '%');

        if (GeneratorMarketingList.currentCount >= GeneratorMarketingList.count) {
            clearInterval(GeneratorMarketingList.timer);
        }
    }
};
