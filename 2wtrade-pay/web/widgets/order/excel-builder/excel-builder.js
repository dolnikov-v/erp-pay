$(function () {
    ExcelBuilder.init();
});

var ExcelBuilder = {
    ajax: null,
    $modal: $('#modal_excel_builder'),
    $form: $('#modal_excel_builder form'),
    count: 0,
    currentCount: 0,
    timer: 0,

    init: function () {
        $('.build-excel-link').on('click', ExcelBuilder.modalShow);
        ExcelBuilder.$modal.on('hide.bs.modal', ExcelBuilder.modalHide);
        ExcelBuilder.$form.on('submit', ExcelBuilder.submit);
    },

    initStart: function () {
        ExcelBuilder.$form.find('.row-with-columns').show();
        ExcelBuilder.$form.find('.row-with-btn-start').show();
        ExcelBuilder.$form.find('.row-with-text-progress').hide();
        ExcelBuilder.$form.find('.row-with-text-finish').hide();
    },

    initProgress: function () {
        ExcelBuilder.$form.find('.row-with-columns').hide();
        ExcelBuilder.$form.find('.row-with-btn-start').hide();
        ExcelBuilder.$form.find('.row-with-text-progress').show();
        ExcelBuilder.$form.find('.row-with-text-finish').hide();
    },

    initFinish: function () {
        ExcelBuilder.$form.find('.row-with-columns').hide();
        ExcelBuilder.$form.find('.row-with-btn-start').hide();
        ExcelBuilder.$form.find('.row-with-text-progress').hide();
        ExcelBuilder.$form.find('.row-with-text-finish').show();
    },

    modalShow: function () {
        ExcelBuilder.initStart();

        $('#excel_builder_list_id').val($(this).data('list-id'));
        ExcelBuilder.count = $(this).data('count-orders');
        ExcelBuilder.currentCount = 0;

        ExcelBuilder.$modal.modal();

        return false;
    },

    modalHide: function () {
        if (ExcelBuilder.ajax != null) {
            ExcelBuilder.ajax.abort();
        }

        clearInterval(ExcelBuilder.timer);
        ExcelBuilder.setPercentProgress(0);
    },

    submit: function (event) {
        ExcelBuilder.initProgress();

        ExcelBuilder.timer = setInterval(ExcelBuilder.tick, 150);

        ExcelBuilder.ajax = $.ajax({
            type: 'POST',
            url: ExcelBuilder.$form.attr('action'),
            dataType: 'json',
            data: ExcelBuilder.$form.serialize(),
            success: function (response, textStatus) {
                if (response.status == 'success') {
                    ExcelBuilder.initFinish();
                    $('#excel_builder_link_download').attr('href', response.message);
                } else {
                    ExcelBuilder.initStart();
                    MainBootstrap.addNotificationDanger(response.message);
                }
            },
            error: function (response) {
                ExcelBuilder.initStart();

                response = JSON.parse(response.responseText);

                if (response.statusText != 'abort') {
                    if (typeof response.message != 'undefined') {
                        MainBootstrap.addNotificationDanger(response.message);
                    } else {
                        MainBootstrap.addNotificationDanger(I18n['Не удалось сгенерировать Excel-лист.']);
                    }
                }
            }
        });

        event.stopImmediatePropagation();

        return false;
    },

    tick: function () {
        ExcelBuilder.currentCount++;

        var part = 100 / ExcelBuilder.count;
        var percent = parseInt(ExcelBuilder.currentCount * part);

        if (percent > 99) {
            percent = 99;
        }

        ExcelBuilder.setPercentProgress(percent);

        if (ExcelBuilder.currentCount >= ExcelBuilder.count) {
            clearInterval(ExcelBuilder.timer);
        }
    },

    setPercentProgress: function (percent) {
        ExcelBuilder.$modal.find('.builder-percent').text(percent + '%');
        ExcelBuilder.$modal.find('.progress-bar').css('width', percent + '%');
    }
};
