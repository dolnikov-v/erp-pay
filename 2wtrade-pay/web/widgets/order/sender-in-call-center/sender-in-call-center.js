var I18n = I18n || {};

$(function () {
    SenderInCallCenter.init();
});

var SenderInCallCenter = {
    ajax: null,
    count: 0,
    $modal: $('#modal_send_in_call_center'),
    $form: $('#modal_send_in_call_center').find('form'),

    init: function () {
        $('#start_send_in_call_center').on('click', SenderInCallCenter.start);
        $('#stop_send_in_call_center').on('click', SenderInCallCenter.stop);
    },

    start: function () {
        SenderInCallCenter.$modal.find('.sender-percent').text('0%');
        SenderInCallCenter.$modal.find('.progress-bar').css('width', '0%');

        SenderInCallCenter.$modal.find('.row-with-btn-start').hide();
        SenderInCallCenter.$modal.find('.row-with-text-start').hide();
        SenderInCallCenter.$modal.find('.row-with-btn-stop').show();
        SenderInCallCenter.$modal.find('.row-with-text-stop').show();

        SenderInCallCenter.count = SenderInCallCenter.$form.find('input[name="id[]"]').length;

        if (SenderInCallCenter.count) {
            SenderInCallCenter.next();
        } else {
            $.notify({message: I18n['Необходимо выбрать заказы.']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            SenderInCallCenter.stop();
        }
    },

    next: function () {
        var $id = SenderInCallCenter.$form.find('input[name="id[]"]:first');

        if ($id.length) {
            var percent = SenderInCallCenter.calculatePercent();

            SenderInCallCenter.$modal.find('.sender-percent').text(percent + '%');
            SenderInCallCenter.$modal.find('.progress-bar').css('width', percent + '%');

            $id.remove();

            var dataAjax = {
                id: $id.val()
            };

            var $checkbox = $('#orders_content input[name="id[]"][value="' + $id.val() + '"]');
            $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
            $checkbox.closest('tr').removeClass('text-danger');

            SenderInCallCenter.ajax = $.ajax({
                type: 'POST',
                url: SenderInCallCenter.$form.data('url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        $checkbox.closest('tr').remove();
                        SenderInCallCenter.next();
                    } else {
                        $checkbox.prop('checked', false);
                        $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                        $checkbox.closest('tr').addClass('text-danger');
                        SenderInCallCenter.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    $checkbox.prop('checked', false);
                    SenderInCallCenter.stop();
                    response = JSON.parse(response.responseText);

                    if (response.statusText != 'abort') {
                        if (typeof response.message != 'undefined') {
                            $.notify({message: response.message}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        } else {
                            $.notify({message: I18n['Не удалось отправить заказ в колл-центр.']}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }
                    }
                }
            });
        } else {
            SenderInCallCenter.ajax = null;
            SenderInCallCenter.$modal.modal('hide');

            $.notify({message: I18n['Отправка заказов в колл-центр завершена.']}, {
                type: "success dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
        }
    },

    stop: function () {
        if (SenderInCallCenter.ajax != null) {
            SenderInCallCenter.ajax.abort();
        }

        SenderInCallCenter.$modal.find('.row-with-btn-start').show();
        SenderInCallCenter.$modal.find('.row-with-text-start').show();
        SenderInCallCenter.$modal.find('.row-with-btn-stop').hide();
        SenderInCallCenter.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (SenderInCallCenter.count) {
            var part = 100 / SenderInCallCenter.count;
            var count = SenderInCallCenter.count - SenderInCallCenter.$form.find('input[name="id[]"]').length;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
