$(function () {
    InvoiceBuilder.init();
});

var InvoiceBuilder = {
    ajax: null,
    $modal: $('#modal_invoice_builder'),
    $form: $('#modal_invoice_builder form'),
    count: 0,
    currentCount: 0,
    timer: 0,

    init: function () {
        $('.build-invoice-link').on('click', InvoiceBuilder.modalShow);
        InvoiceBuilder.$modal.on('hide.bs.modal', InvoiceBuilder.modalHide);
        InvoiceBuilder.$form.on('submit', InvoiceBuilder.submit);
    },

    initStart: function () {
        InvoiceBuilder.$form.find('.row-with-text-start').show();
        InvoiceBuilder.$form.find('.row-with-btn-start').show();
        InvoiceBuilder.$form.find('.row-with-text-progress').hide();
        InvoiceBuilder.$form.find('.row-with-text-finish').hide();
    },

    initProgress: function () {
        InvoiceBuilder.$form.find('.row-with-text-start').hide();
        InvoiceBuilder.$form.find('.row-with-btn-start').hide();
        InvoiceBuilder.$form.find('.row-with-text-progress').show();
        InvoiceBuilder.$form.find('.row-with-text-finish').hide();
    },

    initFinish: function () {
        InvoiceBuilder.$form.find('.row-with-text-start').hide();
        InvoiceBuilder.$form.find('.row-with-btn-start').hide();
        InvoiceBuilder.$form.find('.row-with-text-progress').hide();
        InvoiceBuilder.$form.find('.row-with-text-finish').show();
    },

    modalShow: function () {
        InvoiceBuilder.initStart();

        $('#invoice_builder_list_id').val($(this).data('list-id'));
        InvoiceBuilder.count = $(this).data('count-orders');
        InvoiceBuilder.currentCount = 0;

        InvoiceBuilder.$modal.modal();

        return false;
    },

    modalHide: function () {
        if (InvoiceBuilder.ajax != null) {
            InvoiceBuilder.ajax.abort();
        }

        clearInterval(InvoiceBuilder.timer);
        InvoiceBuilder.setPercentProgress(0);
    },

    submit: function (event) {
        InvoiceBuilder.initProgress();

        if ($('#orderlogisticlistinvoice-format').val() == 'docx') {
            InvoiceBuilder.timer = setInterval(InvoiceBuilder.tick, 150);
        } else {
            InvoiceBuilder.timer = setInterval(InvoiceBuilder.tick, 3000);
        }

        InvoiceBuilder.ajax = $.ajax({
            type: 'POST',
            url: InvoiceBuilder.$form.attr('action'),
            dataType: 'json',
            data: InvoiceBuilder.$form.serialize(),
            success: function (response, textStatus) {
                if (response.status == 'success') {
                    InvoiceBuilder.initFinish();
                    $('#invoice_builder_link_download').attr('href', response.message);
                } else {
                    InvoiceBuilder.initStart();
                    MainBootstrap.addNotificationDanger(response.message);
                }
            },
            error: function (response) {
                InvoiceBuilder.initStart();

                response = JSON.parse(response.responseText);

                if (response.statusText != 'abort') {
                    if (typeof response.message != 'undefined') {
                        MainBootstrap.addNotificationDanger(response.message);
                    } else {
                        MainBootstrap.addNotificationDanger(I18n['Не удалось сгенерировать накладные.']);
                    }
                }
            }
        });

        event.stopImmediatePropagation();

        return false;
    },

    tick: function () {
        InvoiceBuilder.currentCount++;

        var part = 100 / InvoiceBuilder.count;
        var percent = parseInt(InvoiceBuilder.currentCount * part);

        if (percent > 99) {
            percent = 99;
        }

        InvoiceBuilder.setPercentProgress(percent);

        if (InvoiceBuilder.currentCount >= InvoiceBuilder.count) {
            clearInterval(InvoiceBuilder.timer);
        }
    },

    setPercentProgress: function (percent) {
        InvoiceBuilder.$modal.find('.builder-percent').text(percent + '%');
        InvoiceBuilder.$modal.find('.progress-bar').css('width', percent + '%');
    }
};
