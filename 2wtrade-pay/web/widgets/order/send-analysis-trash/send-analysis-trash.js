var I18n = I18n || {};

$(function () {
    SendAnalysisTrash.init();
});

var send_analysis_trash = $('#modal_send_analysis_trash'),
    SendAnalysisTrash = {
    count: 0,
    currentCount: 0,
    $modal: send_analysis_trash,
    $form: send_analysis_trash.find('form'),
    timer: 0,

    init: function () {
        $('#start_send_analysis_trash').on('click', SendAnalysisTrash.start);
        $('#stop_send_analysis_trash').on('click', SendAnalysisTrash.stop);
    },

    start: function () {
        SendAnalysisTrash.$modal.find('.sender-percent').text('0%');
        SendAnalysisTrash.$modal.find('.progress-bar').css('width', '0%');

        SendAnalysisTrash.$modal.find('.row-with-btn-start').hide();
        SendAnalysisTrash.$modal.find('.row-with-text-start').hide();
        SendAnalysisTrash.$modal.find('.row-with-btn-stop').show();
        SendAnalysisTrash.$modal.find('.row-with-text-stop').show();

        SendAnalysisTrash.count = SendAnalysisTrash.$form.find('input[name="id[]"]').length;

        if (SendAnalysisTrash.count) {
            SendAnalysisTrash.next();
        } else {
            $.notify({message: I18n['Необходимо выбрать заказы.']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            SendAnalysisTrash.stop();
        }
    },

    next: function () {
        var $id = SendAnalysisTrash.$form.find('input[name="id[]"]:first');

        if ($id.length) {
            var percent = SendAnalysisTrash.calculatePercent();

            SendAnalysisTrash.$modal.find('.sender-percent').text(percent + '%');
            SendAnalysisTrash.$modal.find('.progress-bar').css('width', percent + '%');

            $id.remove();

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {
                id: $id.val(),
                delivery: $('#send_analysis_trash').val()
            };

            dataAjax[csrfParam] = csrfToken;

            var $checkbox = $('#orders_content input[name="id[]"][value="' + $id.val() + '"]');
            $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
            $checkbox.closest('tr').removeClass('text-danger');

            SendAnalysisTrash.ajax = $.ajax({
                type: 'POST',
                url: SendAnalysisTrash.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        if (response.notifyWarning) {
                            $.notify({message: response.notifyMessage}, {
                                type: "warning dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }

                        $checkbox.closest('tr').remove();
                        SendAnalysisTrash.next();
                    } else {
                        $checkbox.prop('checked', false);
                        $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                        $checkbox.closest('tr').addClass('text-danger');
                        SendAnalysisTrash.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    $checkbox.prop('checked', false);
                    SendAnalysisTrash.stop();
                    response = JSON.parse(response.responseText);

                    if (response.statusText != 'abort') {
                        if (typeof response.message != 'undefined') {
                            $.notify({message: response.message}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        } else {
                            $.notify({message: I18n['Не удалось передать заказ.']}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }
                    }
                }
            });
        } else {
            SendAnalysisTrash.ajax = null;
            SendAnalysisTrash.stop();
            SendAnalysisTrash.$modal.modal('hide');

            $.notify({message: I18n['Передача заказов завершена.']}, {
                type: "success dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
        }
    },

    stop: function () {
        if (SendAnalysisTrash.ajax != null) {
            SendAnalysisTrash.ajax.abort();
        }

        SendAnalysisTrash.$modal.find('.row-with-btn-start').show();
        SendAnalysisTrash.$modal.find('.row-with-text-start').show();
        SendAnalysisTrash.$modal.find('.row-with-btn-stop').hide();
        SendAnalysisTrash.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (SendAnalysisTrash.count) {
            var part = 100 / SendAnalysisTrash.count;
            var count = SendAnalysisTrash.count - SendAnalysisTrash.$form.find('input[name="id[]"]').length;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
