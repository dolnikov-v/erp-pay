var I18n = I18n || {};

$(function () {
    ResenderInCallCenterNewQueue.init();
});

var ResenderInCallCenterNewQueue = {
    ajax: null,
    count: 0,
    $modal: $('#modal_resend_in_call_center_new_queue'),
    $form: $('#modal_resend_in_call_center_new_queue').find('form'),

    init: function () {
        $('#start_resend_in_call_center_new_queue').on('click', ResenderInCallCenterNewQueue.start);
        $('#stop_resend_in_call_center_new_queue').on('click', ResenderInCallCenterNewQueue.stop);
    },

    start: function () {
        ResenderInCallCenterNewQueue.$modal.find('.sender-percent').text('0%');
        ResenderInCallCenterNewQueue.$modal.find('.progress-bar').css('width', '0%');

        ResenderInCallCenterNewQueue.$modal.find('.row-with-btn-start').hide();
        ResenderInCallCenterNewQueue.$modal.find('.row-with-text-start').hide();
        ResenderInCallCenterNewQueue.$modal.find('.row-with-btn-stop').show();
        ResenderInCallCenterNewQueue.$modal.find('.row-with-text-stop').show();

        ResenderInCallCenterNewQueue.count = ResenderInCallCenterNewQueue.$form.find('input[name="id[]"]').length;

        if (ResenderInCallCenterNewQueue.count) {
            ResenderInCallCenterNewQueue.next();
        } else {
            $.notify({message: I18n['Необходимо выбрать заказы.']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            ResenderInCallCenterNewQueue.stop();
        }
    },

    next: function () {
        var $id = ResenderInCallCenterNewQueue.$form.find('input[name="id[]"]:first');

        if ($id.length) {
            var percent = ResenderInCallCenterNewQueue.calculatePercent();

            ResenderInCallCenterNewQueue.$modal.find('.sender-percent').text(percent + '%');
            ResenderInCallCenterNewQueue.$modal.find('.progress-bar').css('width', percent + '%');

            $id.remove();

            var dataAjax = {
                id: $id.val()
            };

            var $checkbox = $('#orders_content input[name="id[]"][value="' + $id.val() + '"]');
            $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
            $checkbox.closest('tr').removeClass('text-danger');

            ResenderInCallCenterNewQueue.ajax = $.ajax({
                type: 'POST',
                url: ResenderInCallCenterNewQueue.$form.data('url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        $checkbox.closest('tr').remove();
                        ResenderInCallCenterNewQueue.next();
                    } else {
                        $checkbox.prop('checked', false);
                        $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                        $checkbox.closest('tr').addClass('text-danger');
                        ResenderInCallCenterNewQueue.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    $checkbox.prop('checked', false);
                    ResenderInCallCenterNewQueue.stop();
                    response = JSON.parse(response.responseText);

                    if (response.statusText != 'abort') {
                        if (typeof response.message != 'undefined') {
                            $.notify({message: response.message}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        } else {
                            $.notify({message: I18n['Не удалось отправить заказ в колл-центр.']}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }
                    }
                }
            });
        } else {
            ResenderInCallCenterNewQueue.ajax = null;
            ResenderInCallCenterNewQueue.$modal.modal('hide');

            $.notify({message: I18n['Отправка заказов в колл-центр завершена.']}, {
                type: "success dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
        }
    },

    stop: function () {
        if (ResenderInCallCenterNewQueue.ajax != null) {
            ResenderInCallCenterNewQueue.ajax.abort();
        }

        ResenderInCallCenterNewQueue.$modal.find('.row-with-btn-start').show();
        ResenderInCallCenterNewQueue.$modal.find('.row-with-text-start').show();
        ResenderInCallCenterNewQueue.$modal.find('.row-with-btn-stop').hide();
        ResenderInCallCenterNewQueue.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (ResenderInCallCenterNewQueue.count) {
            var part = 100 / ResenderInCallCenterNewQueue.count;
            var count = ResenderInCallCenterNewQueue.count - ResenderInCallCenterNewQueue.$form.find('input[name="id[]"]').length;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
