$(function () {
    ExportDropdown.init();
});

var ExportDropdown = {
    init: function () {
        $('#orders_export_dropdown a').on('click', ExportDropdown.export);
    },
    export: function () {
        $.ajax({
            type: 'GET',
            url: $(this).data('url'),
            success: function (response) {
                if (response.status == 'success') {
                    $.notify({message: response.message}, {
                        type: "warning dark",
                        animate: {exit: 'hide'},
                        z_index: 2031
                    });
                }
                else {
                    $.notify({message: response.message}, {
                        type: "danger dark",
                        animate: {exit: 'hide'},
                        z_index: 2031
                    });
                }
            },
            error: function (response) {
                $.notify({message: response.message}, {
                    type: "danger dark",
                    animate: {exit: 'hide'},
                    z_index: 2031
                });
            }
        });
    }
};
