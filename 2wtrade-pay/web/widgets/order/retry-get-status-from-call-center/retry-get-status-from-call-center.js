var I18n = I18n || {};

$(function () {
    RetryGetStatusFromCallCenter.init();
});

var RetryGetStatusFromCallCenter = {
    ajax: null,
    count: 0,
    $modal: $('#modal_retry_get_status_from_call_center'),
    $form: $('#modal_retry_get_status_from_call_center').find('form'),

    init: function () {
        $('#start_retry_get_status_from_call_center').on('click', RetryGetStatusFromCallCenter.start);
        $('#stop_retry_get_status_from_call_center').on('click', RetryGetStatusFromCallCenter.stop);
    },

    start: function () {
        RetryGetStatusFromCallCenter.$modal.find('.sender-percent').text('0%');
        RetryGetStatusFromCallCenter.$modal.find('.progress-bar').css('width', '0%');

        RetryGetStatusFromCallCenter.$modal.find('.row-with-btn-start').hide();
        RetryGetStatusFromCallCenter.$modal.find('.row-with-text-start').hide();
        RetryGetStatusFromCallCenter.$modal.find('.row-with-btn-stop').show();
        RetryGetStatusFromCallCenter.$modal.find('.row-with-text-stop').show();

        RetryGetStatusFromCallCenter.count = RetryGetStatusFromCallCenter.$form.find('input[name="id[]"]').length;

        if (RetryGetStatusFromCallCenter.count) {
            RetryGetStatusFromCallCenter.next();
        } else {
            $.notify({message: I18n['Необходимо выбрать заказы.']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            RetryGetStatusFromCallCenter.stop();
        }
    },

    next: function () {
        var $id = RetryGetStatusFromCallCenter.$form.find('input[name="id[]"]:first');

        if ($id.length) {
            var percent = RetryGetStatusFromCallCenter.calculatePercent();

            RetryGetStatusFromCallCenter.$modal.find('.sender-percent').text(percent + '%');
            RetryGetStatusFromCallCenter.$modal.find('.progress-bar').css('width', percent + '%');

            $id.remove();

            var dataAjax = {
                id: $id.val()
            };

            var $checkbox = $('#orders_content input[name="id[]"][value="' + $id.val() + '"]');
            $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
            $checkbox.closest('tr').removeClass('text-danger');

            RetryGetStatusFromCallCenter.ajax = $.ajax({
                type: 'POST',
                url: RetryGetStatusFromCallCenter.$form.data('url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        $checkbox.closest('tr').remove();
                        RetryGetStatusFromCallCenter.next();
                    } else {
                        $checkbox.prop('checked', false);
                        $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                        $checkbox.closest('tr').addClass('text-danger');
                        RetryGetStatusFromCallCenter.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    $checkbox.prop('checked', false);
                    RetryGetStatusFromCallCenter.stop();
                    response = JSON.parse(response.responseText);

                    if (response.statusText != 'abort') {
                        if (typeof response.message != 'undefined') {
                            $.notify({message: response.message}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        } else {
                            $.notify({message: I18n['Не удалось поставить заявку в очередь на обновление статуса.']}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }
                    }
                }
            });
        } else {
            RetryGetStatusFromCallCenter.ajax = null;
            RetryGetStatusFromCallCenter.$modal.modal('hide');

            $.notify({message: I18n['Поднятие заказов в очереди на обновление статусов из КЦ завершено.']}, {
                type: "success dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
        }
    },

    stop: function () {
        if (RetryGetStatusFromCallCenter.ajax != null) {
            RetryGetStatusFromCallCenter.ajax.abort();
        }

        RetryGetStatusFromCallCenter.$modal.find('.row-with-btn-start').show();
        RetryGetStatusFromCallCenter.$modal.find('.row-with-text-start').show();
        RetryGetStatusFromCallCenter.$modal.find('.row-with-btn-stop').hide();
        RetryGetStatusFromCallCenter.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (RetryGetStatusFromCallCenter.count) {
            var part = 100 / RetryGetStatusFromCallCenter.count;
            var count = RetryGetStatusFromCallCenter.count - RetryGetStatusFromCallCenter.$form.find('input[name="id[]"]').length;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
