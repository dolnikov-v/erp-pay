var I18n = I18n || {};

$(function () {
    SendSmsPoll.init();
});

var SendSmsPoll = {
    ajax: null,
    count: 0,
    $modal: $('#modal_send_sms_poll'),
    $form: $('#modal_send_sms_poll').find('form'),

    init: function () {
        $('#start_send_sms_poll').on('click', SendSmsPoll.start);
        $('#stop_send_sms_poll').on('click', SendSmsPoll.stop);
    },

    start: function () {

        SendSmsPoll.$modal.find('.sender-percent').text('0%');
        SendSmsPoll.$modal.find('.progress-bar').css('width', '0%');

        SendSmsPoll.$modal.find('.row-with-btn-start').hide();
        SendSmsPoll.$modal.find('.row-with-text-start').hide();
        SendSmsPoll.$modal.find('.row-with-btn-stop').show();
        SendSmsPoll.$modal.find('.row-with-text-stop').show();

        SendSmsPoll.count = SendSmsPoll.$form.find('input[name="id[]"]').length;

        if (SendSmsPoll.count) {
            SendSmsPoll.next();
        } else {
            $.notify({message: I18n['Необходимо выбрать заказы.']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            SendSmsPoll.stop();
        }
    },

    next: function () {
        var $id = SendSmsPoll.$form.find('input[name="id[]"]:first');

        if ($id.length) {
            var percent = SendSmsPoll.calculatePercent();

            SendSmsPoll.$modal.find('.sender-percent').text(percent + '%');
            SendSmsPoll.$modal.find('.progress-bar').css('width', percent + '%');

            $id.remove();

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {
                id: $id.val(),
                sms_poll: $('#send_sms_poll_select').val()
            };
            console.log(dataAjax, 1);
            dataAjax[csrfParam] = csrfToken;

            var $checkbox = $('#orders_content input[name="id[]"][value="' + $id.val() + '"]');
            $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
            $checkbox.closest('tr').removeClass('text-danger');

            SendSmsPoll.ajax = $.ajax({
                type: 'POST',
                url: SendSmsPoll.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        $checkbox.closest('tr').remove();
                        SendSmsPoll.next();
                    } else {
                        $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                        $checkbox.closest('tr').addClass('text-danger');
                        SendSmsPoll.stop();
                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    SendSmsPoll.stop();
                    response = JSON.parse(response.responseText);

                    if (response.statusText != 'abort') {
                        if (typeof response.message != 'undefined') {
                            $.notify({message: response.message}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        } else {
                            $.notify({message: I18n['Не удалось отправить СМС опрос.']}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }
                    }
                }
            });
        } else {
            SendSmsPoll.ajax = null;
            SendSmsPoll.$modal.modal('hide');

            $.notify({message: I18n['Отправка СМС опроса завершена.']}, {
                type: "success dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
        }
    },

    stop: function () {
        if (SendSmsPoll.ajax != null) {
            SendSmsPoll.ajax.abort();
        }

        SendSmsPoll.$modal.find('.row-with-btn-start').show();
        SendSmsPoll.$modal.find('.row-with-text-start').show();
        SendSmsPoll.$modal.find('.row-with-btn-stop').hide();
        SendSmsPoll.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (SendSmsPoll.count) {
            var part = 100 / SendSmsPoll.count;
            var count = SendSmsPoll.count - SendSmsPoll.$form.find('input[name="id[]"]').length;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
