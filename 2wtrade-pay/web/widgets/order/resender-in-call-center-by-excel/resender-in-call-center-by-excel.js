var I18n = I18n || {};

$(function () {
    ResenderInCallCenterByExcel.init();
});

var ResenderInCallCenterByExcel = {
    $modal: $('#modal_resend_in_call_center_by_excel'),
    $form: $('#modal_resend_in_call_center_by_excel').find('form'),

    init: function () {
        ResenderInCallCenterByExcel.$form.on('submit', ResenderInCallCenterByExcel.start);
    },

    start: function (event) {
        ResenderInCallCenterByExcel.$form.find('.row-with-text-start').hide();
        ResenderInCallCenterByExcel.$form.find('.row-with-btn-start').hide();
        ResenderInCallCenterByExcel.$form.find('.input-group-file').hide();
        ResenderInCallCenterByExcel.$form.find('.row-with-text-stop').show();
    },

    stop: function () {
    }
};
