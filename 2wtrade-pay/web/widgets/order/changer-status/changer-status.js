var I18n = I18n || {};

$(function () {
    ChangerStatus.init();
});

var ChangerStatus = {
    ajax: null,
    count: 0,
    $modal: $('#modal_changer_status'),
    $form: $('#modal_changer_status').find('form'),

    init: function () {
        $('#start_changer_status').on('click', ChangerStatus.start);
        $('#stop_changer_status').on('click', ChangerStatus.stop);
    },

    start: function () {
        ChangerStatus.$modal.find('.sender-percent').text('0%');
        ChangerStatus.$modal.find('.progress-bar').css('width', '0%');

        ChangerStatus.$modal.find('.row-with-btn-start').hide();
        ChangerStatus.$modal.find('.row-with-text-start').hide();
        ChangerStatus.$modal.find('.row-with-btn-stop').show();
        ChangerStatus.$modal.find('.row-with-text-stop').show();

        ChangerStatus.count = ChangerStatus.$form.find('input[name="id[]"]').length;

        if (ChangerStatus.count) {
            ChangerStatus.next();
        } else {
            $.notify({message: I18n['Необходимо выбрать заказы.']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            ChangerStatus.stop();
        }
    },

    next: function () {
        var $id = ChangerStatus.$form.find('input[name="id[]"]:first');

        if ($id.length) {
            var percent = ChangerStatus.calculatePercent();

            ChangerStatus.$modal.find('.sender-percent').text(percent + '%');
            ChangerStatus.$modal.find('.progress-bar').css('width', percent + '%');

            $id.remove();

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {
                id: $id.val(),
                status: $('#changer_status_select').val(),
                comment: $('#changer_status_comment').val(),
                ignore: $('#changer_status_ignore').prop('checked') ? 1 : null
            };

            dataAjax[csrfParam] = csrfToken;

            var $checkbox = $('#orders_content input[name="id[]"][value="' + $id.val() + '"]');
            $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
            $checkbox.closest('tr').removeClass('text-danger');

            ChangerStatus.ajax = $.ajax({
                type: 'POST',
                url: ChangerStatus.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        $checkbox.closest('tr').remove();
                        ChangerStatus.next();
                    } else {
                        $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                        $checkbox.closest('tr').addClass('text-danger');
                        ChangerStatus.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    ChangerStatus.stop();
                    response = JSON.parse(response.responseText);

                    if (response.statusText != 'abort') {
                        if (typeof response.message != 'undefined') {
                            $.notify({message: response.message}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        } else {
                            $.notify({message: I18n['Не удалось сменить статус.']}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }
                    }
                }
            });
        } else {
            ChangerStatus.ajax = null;
            ChangerStatus.$modal.modal('hide');

            $.notify({message: I18n['Смена статусов завершена.']}, {
                type: "success dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
        }
    },

    stop: function () {
        if (ChangerStatus.ajax != null) {
            ChangerStatus.ajax.abort();
        }

        ChangerStatus.$modal.find('.row-with-btn-start').show();
        ChangerStatus.$modal.find('.row-with-text-start').show();
        ChangerStatus.$modal.find('.row-with-btn-stop').hide();
        ChangerStatus.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (ChangerStatus.count) {
            var part = 100 / ChangerStatus.count;
            var count = ChangerStatus.count - ChangerStatus.$form.find('input[name="id[]"]').length;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
