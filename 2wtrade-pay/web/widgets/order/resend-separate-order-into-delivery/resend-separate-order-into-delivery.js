var I18n = I18n || {};

$(function () {
    ResenderSeparateOrderIntoDelivery.init();
});

var ResenderSeparateOrderIntoDelivery = {
    ajax: null,
    count: 0,
    $modal: $('#modal_resend_separate_order_into_delivery'),
    $form: $('#modal_resend_separate_order_into_delivery').find('form'),

    init: function () {
        $('#start_resend_separate_order_into_delivery').on('click', ResenderSeparateOrderIntoDelivery.start);
        $('#stop_resend_separate_order_into_delivery').on('click', ResenderSeparateOrderIntoDelivery.stop);
    },

    start: function () {

        ResenderSeparateOrderIntoDelivery.$modal.find('.sender-percent').text('0%');
        ResenderSeparateOrderIntoDelivery.$modal.find('.progress-bar').css('width', '0%');

        ResenderSeparateOrderIntoDelivery.$modal.find('.row-with-btn-start').hide();
        ResenderSeparateOrderIntoDelivery.$modal.find('.row-with-text-start').hide();
        ResenderSeparateOrderIntoDelivery.$modal.find('.row-with-btn-stop').show();
        ResenderSeparateOrderIntoDelivery.$modal.find('.row-with-text-stop').show();

        ResenderSeparateOrderIntoDelivery.count = ResenderSeparateOrderIntoDelivery.$form.find('input[name="id[]"]').length;

        if (ResenderSeparateOrderIntoDelivery.count) {
            ResenderSeparateOrderIntoDelivery.next();
        } else {
            $.notify({message: I18n['Необходимо выбрать заказы.']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            ResenderSeparateOrderIntoDelivery.stop();
        }
    },

    next: function () {
        var $id = ResenderSeparateOrderIntoDelivery.$form.find('input[name="id[]"]:first');

        if ($id.length) {
            var percent = ResenderSeparateOrderIntoDelivery.calculatePercent();

            ResenderSeparateOrderIntoDelivery.$modal.find('.sender-percent').text(percent + '%');
            ResenderSeparateOrderIntoDelivery.$modal.find('.progress-bar').css('width', percent + '%');

            $id.remove();

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {
                id: $id.val(),
                delivery: $('#resend_separate_order_into_delivery_select').val(),
                orderAgeIgnore: $('#order_age_ignore').prop('checked') ? 1 : null
            };

            dataAjax[csrfParam] = csrfToken;

            var $checkbox = $('#orders_content input[name="id[]"][value="' + $id.val() + '"]');
            $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
            $checkbox.closest('tr').removeClass('text-danger');

            ResenderSeparateOrderIntoDelivery.ajax = $.ajax({
                type: 'POST',
                url: ResenderSeparateOrderIntoDelivery.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        $checkbox.closest('tr').remove();
                        ResenderSeparateOrderIntoDelivery.next();
                    } else {
                        $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                        $checkbox.closest('tr').addClass('text-danger');
                        ResenderSeparateOrderIntoDelivery.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    ResenderSeparateOrderIntoDelivery.stop();
                    response = JSON.parse(response.responseText);

                    if (response.statusText != 'abort') {
                        if (typeof response.message != 'undefined') {
                            $.notify({message: response.message}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        } else {
                            $.notify({message: I18n['Не удалось создать копию отправки и отправить в службу доставки']}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }
                    }
                }
            });
        } else {
            ResenderSeparateOrderIntoDelivery.ajax = null;
            ResenderSeparateOrderIntoDelivery.$modal.modal('hide');

            $.notify({message: I18n['Создание копий и отправка в службу доставки завершено.']}, {
                type: "success dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
        }
    },

    stop: function () {
        if (ResenderSeparateOrderIntoDelivery.ajax != null) {
            ResenderSeparateOrderIntoDelivery.ajax.abort();
        }

        ResenderSeparateOrderIntoDelivery.$modal.find('.row-with-btn-start').show();
        ResenderSeparateOrderIntoDelivery.$modal.find('.row-with-text-start').show();
        ResenderSeparateOrderIntoDelivery.$modal.find('.row-with-btn-stop').hide();
        ResenderSeparateOrderIntoDelivery.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (ResenderSeparateOrderIntoDelivery.count) {
            var part = 100 / ResenderSeparateOrderIntoDelivery.count;
            var count = ResenderSeparateOrderIntoDelivery.count - ResenderSeparateOrderIntoDelivery.$form.find('input[name="id[]"]').length;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
