var I18n = I18n || {};

$(function () {
    GeneratorList.init();
});

var GeneratorList = {
    count: 0,
    currentCount: 0,
    $modal: $('#modal_generate_list'),
    $form: $('#modal_generate_list').find('form'),
    timer: 0,

    init: function () {
        GeneratorList.$form.on('submit', GeneratorList.start);
    },

    start: function (event) {
        clearInterval(GeneratorList.timer);
        GeneratorList.count = GeneratorList.$form.find('input[name="id[]"]').length;
        GeneratorList.currentCount = 0;

        if (GeneratorList.count) {
            GeneratorList.$form.find('.row-with-text-start').hide();
            GeneratorList.$form.find('.row-with-btn-start').hide();
            GeneratorList.$form.find('.row-with-text-stop').show();

            GeneratorList.timer = setInterval(GeneratorList.tick, 300);
        } else {
            $.notify({message: I18n['Необходимо выбрать заказы.']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });

            event.stopImmediatePropagation();

            return false;
        }
    },

    stop: function () {
        GeneratorList.$form.find('.input-group-file input[type="file"]').val('');
        GeneratorList.$form.find('.input-group-file input[type="text"]').val('');
    },

    tick: function () {
        GeneratorList.currentCount++;

        var part = 100 / GeneratorList.count;
        var percent = parseInt(GeneratorList.currentCount * part);

        if (percent > 99) {
            percent = 99;
        }

        GeneratorList.$modal.find('.sender-percent').text(percent + '%');
        GeneratorList.$modal.find('.progress-bar').css('width', percent + '%');

        if (GeneratorList.currentCount >= GeneratorList.count) {
            clearInterval(GeneratorList.timer);
        }
    }
};
