$(function () {
    GenerateAndSendList.init();
});

var GenerateAndSendList = {
    $modal: $('#modal_generate_and_send_list'),
    $form: $('#modal_generate_and_send_list form'),
    ajax: null,
    emailsCount: 0,
    currentCount: 0,
    hasApiGeneratingList: false,
    excelFilename: null,
    init: function () {
        $('.btn-generate-and-send-list').on('click', GenerateAndSendList.modalShow);
        $('#start_sending_list').on('click', GenerateAndSendList.startSendingList);
        $('#stop_sending_list').on('click', GenerateAndSendList.stopSendingList);
        GenerateAndSendList.$modal.on('hide.bs.modal', GenerateAndSendList.modalHide);
        GenerateAndSendList.$form.on('submit', GenerateAndSendList.startGeneratingList);
    },
    modalShow: function () {
        $('#generate_and_send_list_id').val($(this).data('list-id'));
        GenerateAndSendList.emailsCount = $(this).data('emails-count');
        $('#generate_and_send_mailing_list').text($(this).data('emails-list'));
        GenerateAndSendList.hasApiGeneratingList = $(this).data('has-api-generate-list');
        GenerateAndSendList.currentCount = 0;
        GenerateAndSendList.excelFilename = null;

        if ($(this).data('skip-generating-list') != '1') {
            GenerateAndSendList.showGeneratingList();
        } else {
            GenerateAndSendList.showSendingList();
        }

        GenerateAndSendList.$modal.modal();
        return false;
    },
    showSendingList: function () {
        GenerateAndSendList.$form.find('.generating-default-list').hide();
        GenerateAndSendList.$form.find('.required-generating-list').hide();
        GenerateAndSendList.$form.find('.btn-generating-list').hide();
        GenerateAndSendList.$form.find('.btn-sending-list').hide();
        GenerateAndSendList.$form.find('.btn-send-list').show();
        GenerateAndSendList.$form.find('.sending-list-progress').hide();
        GenerateAndSendList.$form.find('.generating-list-progress').hide();
        GenerateAndSendList.$form.find('.row-with-sending-text').show();
    },
    showGeneratingList: function () {
        GenerateAndSendList.$form.find('.required-generating-list').show();
        if (GenerateAndSendList.hasApiGeneratingList != '1') {
            GenerateAndSendList.$form.find('.generating-default-list').show();
        } else {
            GenerateAndSendList.$form.find('.generating-default-list').hide();
        }
        GenerateAndSendList.$form.find('.btn-generating-list').show();
        GenerateAndSendList.$form.find('.btn-sending-list').hide();
        GenerateAndSendList.$form.find('.btn-send-list').hide();
        GenerateAndSendList.$form.find('.sending-list-progress').hide();
        GenerateAndSendList.$form.find('.generating-list-progress').hide();
        GenerateAndSendList.$form.find('.row-with-sending-text').hide();
    },
    showGeneratingProgress: function () {
        GenerateAndSendList.$form.find('.required-generating-list').hide();
        GenerateAndSendList.$form.find('.generating-default-list').hide();
        GenerateAndSendList.$form.find('.btn-generating-list').hide();
        GenerateAndSendList.$form.find('.btn-sending-list').hide();
        GenerateAndSendList.$form.find('.btn-send-list').hide();
        GenerateAndSendList.$form.find('.sending-list-progress').hide();
        GenerateAndSendList.$form.find('.generating-list-progress').show();
        GenerateAndSendList.$form.find('.row-with-sending-text').hide();
    },
    showSendingProgress: function () {
        GenerateAndSendList.$form.find('.generating-default-list').hide();
        GenerateAndSendList.$form.find('.required-generating-list').hide();
        GenerateAndSendList.$form.find('.btn-generating-list').hide();
        GenerateAndSendList.$form.find('.btn-sending-list').show();
        GenerateAndSendList.$form.find('.btn-send-list').hide();
        GenerateAndSendList.$form.find('.sending-list-progress').show();
        GenerateAndSendList.$form.find('.generating-list-progress').hide();
        GenerateAndSendList.$form.find('.row-with-sending-text').hide();
    },
    modalHide: function () {
        if (GenerateAndSendList.ajax != null) {
            GenerateAndSendList.ajax.abort();
        }
        GenerateAndSendList.setPercentProgress(0);
    },
    startGeneratingList: function (event) {
        GenerateAndSendList.showGeneratingProgress();

        GenerateAndSendList.ajax = $.ajax({
            type: 'POST',
            url: GenerateAndSendList.$form.attr('action'),
            dataType: 'json',
            data: GenerateAndSendList.$form.serialize(),
            success: function (response, textStatus) {
                if (response.status == 'success') {
                    GenerateAndSendList.excelFilename = response.message;
                    GenerateAndSendList.showSendingList();
                } else {
                    GenerateAndSendList.showGeneratingList();
                    MainBootstrap.addNotificationDanger(response.message);
                }
            },
            error: function (response) {
                GenerateAndSendList.showGeneratingList();

                response = JSON.parse(response.responseText);

                if (response.statusText != 'abort') {
                    if (typeof response.message != 'undefined') {
                        MainBootstrap.addNotificationDanger(response.message);
                    } else {
                        MainBootstrap.addNotificationDanger(I18n['Не удалось сгенерировать Excel-лист.']);
                    }
                }
            }
        });

        event.stopImmediatePropagation();
        return false;
    },
    startSendingList: function () {
        GenerateAndSendList.currentCount = 0;
        GenerateAndSendList.setPercentProgress(0);
        GenerateAndSendList.showSendingProgress();
        GenerateAndSendList.sendingList();
    },
    sendingList: function () {
        if (GenerateAndSendList.currentCount < GenerateAndSendList.emailsCount) {
            var percent = GenerateAndSendList.calculatePercent();
            GenerateAndSendList.setPercentProgress(percent);

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {
                list_id: $('#generate_and_send_list_id').val(),
                sending: true,
                emailsOffset: GenerateAndSendList.currentCount,
                filename: GenerateAndSendList.excelFilename
            };

            dataAjax[csrfParam] = csrfToken;

            GenerateAndSendList.ajax = $.ajax({
                type: 'POST',
                url: GenerateAndSendList.$form.attr('action'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        GenerateAndSendList.currentCount += 1;
                        if (typeof response.emailsCount != 'undefined') {
                            GenerateAndSendList.emailsCount = response.emailsCount;
                        }
                        GenerateAndSendList.sendingList();
                    } else {
                        GenerateAndSendList.stopSendingList();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    GenerateAndSendList.stopSendingList();
                    console.log(response);
                    response = JSON.parse(response.responseText);

                    if (response.statusText != 'abort') {
                        if (typeof response.message != 'undefined') {
                            $.notify({message: response.message}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        } else {
                            $.notify({message: I18n['Не удалось отправить лист.']}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }
                    }
                }
            });
        } else {
            GenerateAndSendList.ajax = null;
            GenerateAndSendList.$modal.modal('hide');

            $.notify({message: I18n['Отправка листа завершена.']}, {
                type: "success dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
        }
    },
    stopSendingList: function () {
        if (GenerateAndSendList.ajax != null) {
            GenerateAndSendList.ajax.abort();
        }
        GenerateAndSendList.setPercentProgress(0);

        GenerateAndSendList.showSendingList();
    },
    setPercentProgress: function (percent) {
        GenerateAndSendList.$modal.find('.builder-percent').text(percent + '%');
        GenerateAndSendList.$modal.find('.progress-bar').css('width', percent + '%');
    },
    calculatePercent: function () {
        var percent = 0;

        if (GenerateAndSendList.currentCount) {
            var part = GenerateAndSendList.currentCount / GenerateAndSendList.emailsCount;

            percent = parseInt(part * 100);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
