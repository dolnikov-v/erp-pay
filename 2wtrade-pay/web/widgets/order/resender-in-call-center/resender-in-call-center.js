var I18n = I18n || {};

$(function () {
    ResenderInCallCenter.init();
});

var ResenderInCallCenter = {
    ajax: null,
    count: 0,
    $modal: $('#modal_resend_in_call_center'),
    $form: $('#modal_resend_in_call_center').find('form'),

    init: function () {
        $('#start_resend_in_call_center').on('click', ResenderInCallCenter.start);
        $('#stop_resend_in_call_center').on('click', ResenderInCallCenter.stop);
    },

    start: function () {
        ResenderInCallCenter.$modal.find('.sender-percent').text('0%');
        ResenderInCallCenter.$modal.find('.progress-bar').css('width', '0%');

        ResenderInCallCenter.$modal.find('.row-with-btn-start').hide();
        ResenderInCallCenter.$modal.find('.row-with-text-start').hide();
        ResenderInCallCenter.$modal.find('.row-with-btn-stop').show();
        ResenderInCallCenter.$modal.find('.row-with-text-stop').show();

        ResenderInCallCenter.count = ResenderInCallCenter.$form.find('input[name="id[]"]').length;

        if (ResenderInCallCenter.count) {
            ResenderInCallCenter.next();
        } else {
            $.notify({message: I18n['Необходимо выбрать заказы.']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            ResenderInCallCenter.stop();
        }
    },

    next: function () {
        var $id = ResenderInCallCenter.$form.find('input[name="id[]"]:first');

        if ($id.length) {
            var percent = ResenderInCallCenter.calculatePercent();

            ResenderInCallCenter.$modal.find('.sender-percent').text(percent + '%');
            ResenderInCallCenter.$modal.find('.progress-bar').css('width', percent + '%');

            $id.remove();

            var dataAjax = {
                id: $id.val()
            };

            var $checkbox = $('#orders_content input[name="id[]"][value="' + $id.val() + '"]');
            $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
            $checkbox.closest('tr').removeClass('text-danger');

            ResenderInCallCenter.ajax = $.ajax({
                type: 'POST',
                url: ResenderInCallCenter.$form.data('url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        $checkbox.closest('tr').remove();
                        ResenderInCallCenter.next();
                    } else {
                        $checkbox.prop('checked', false);
                        $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                        $checkbox.closest('tr').addClass('text-danger');
                        ResenderInCallCenter.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    $checkbox.prop('checked', false);
                    ResenderInCallCenter.stop();
                    response = JSON.parse(response.responseText);

                    if (response.statusText != 'abort') {
                        if (typeof response.message != 'undefined') {
                            $.notify({message: response.message}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        } else {
                            $.notify({message: I18n['Не удалось отправить заказ в колл-центр.']}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }
                    }
                }
            });
        } else {
            ResenderInCallCenter.ajax = null;
            ResenderInCallCenter.$modal.modal('hide');

            $.notify({message: I18n['Отправка заказов в колл-центр завершена.']}, {
                type: "success dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
        }
    },

    stop: function () {
        if (ResenderInCallCenter.ajax != null) {
            ResenderInCallCenter.ajax.abort();
        }

        ResenderInCallCenter.$modal.find('.row-with-btn-start').show();
        ResenderInCallCenter.$modal.find('.row-with-text-start').show();
        ResenderInCallCenter.$modal.find('.row-with-btn-stop').hide();
        ResenderInCallCenter.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (ResenderInCallCenter.count) {
            var part = 100 / ResenderInCallCenter.count;
            var count = ResenderInCallCenter.count - ResenderInCallCenter.$form.find('input[name="id[]"]').length;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
