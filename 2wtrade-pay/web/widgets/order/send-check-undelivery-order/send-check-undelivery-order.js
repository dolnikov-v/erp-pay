var I18n = I18n || {};

$(function () {
    SendCheckUndeliveryOrder.init();
});

var send_check_undelivery_order = $('#modal_send_check_undelivery_order'),
    SendCheckUndeliveryOrder = {
    remember: false,
    dont_ask : $('#dont_ask'),
    no_repeat : $('#no_repeat'),
    count: 0,
    currentCount: 0,
    $modal: send_check_undelivery_order,
    $form: send_check_undelivery_order.find('form'),
    timer: 0,

    init: function () {
        $('#start_send_check_undelivery_order').on('click', SendCheckUndeliveryOrder.start);
        $('#stop_send_check_undelivery_order').on('click', SendCheckUndeliveryOrder.stop);

        SendCheckUndeliveryOrder.dont_ask.on('click', function(){
            SendCheckUndeliveryOrder.remember = $(this).prop('checked');
        });
    },
    start: function () {
        SendCheckUndeliveryOrder.$modal.find('.sender-percent').text('0%');
        SendCheckUndeliveryOrder.$modal.find('.progress-bar').css('width', '0%');

        SendCheckUndeliveryOrder.$modal.find('.row-with-btn-start').hide();
        SendCheckUndeliveryOrder.$modal.find('.row-with-text-start').hide();
        SendCheckUndeliveryOrder.$modal.find('.row-with-btn-stop').show();
        SendCheckUndeliveryOrder.$modal.find('.row-with-text-stop').show();

        SendCheckUndeliveryOrder.count = SendCheckUndeliveryOrder.$form.find('input[name="id[]"]').length;

        if (SendCheckUndeliveryOrder.count) {
            SendCheckUndeliveryOrder.next();
        } else {
            $.notify({message: I18n['Необходимо выбрать заказы.']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            SendCheckUndeliveryOrder.stop();
        }
    },

    next: function () {
        var $id = SendCheckUndeliveryOrder.$form.find('input[name="id[]"]:first');

        if ($id.length) {
            var percent = SendCheckUndeliveryOrder.calculatePercent();

            SendCheckUndeliveryOrder.$modal.find('.sender-percent').text(percent + '%');
            SendCheckUndeliveryOrder.$modal.find('.progress-bar').css('width', percent + '%');

            $id.remove();

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {
                id: $id.val(),
                no_repeat: $('#no_repeat').prop('checked')
            };

            dataAjax[csrfParam] = csrfToken;

            var $checkbox = $('#orders_content input[name="id[]"][value="' + $id.val() + '"]');
            $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
            $checkbox.closest('tr').removeClass('text-danger');

            SendCheckUndeliveryOrder.ajax = $.ajax({
                type: 'POST',
                url: SendCheckUndeliveryOrder.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        if (response.notifyWarning) {
                            $.notify({message: response.notifyMessage}, {
                                type: "warning dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }

                        $checkbox.closest('tr').remove();
                        SendCheckUndeliveryOrder.next();
                    } else {
                        $checkbox.prop('checked', false);
                        $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                        $checkbox.closest('tr').addClass('text-danger');
                        SendCheckUndeliveryOrder.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    $checkbox.prop('checked', false);
                    if(SendCheckUndeliveryOrder.remember){
                        SendCheckUndeliveryOrder.next();
                    }else{
                        SendCheckUndeliveryOrder.stop();
                    }

                    response = JSON.parse(response.responseText);

                    if (response.statusText != 'abort') {
                        if (typeof response.message != 'undefined') {
                            $.notify({message: response.message}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        } else {
                            $.notify({message: I18n['Не удалось передать заказ в службу доставки.']}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }
                    }
                }
            });
        } else {
            SendCheckUndeliveryOrder.ajax = null;
            SendCheckUndeliveryOrder.$modal.modal('hide');

            $.notify({message: I18n['Передача заказов в службу доставки завершена.']}, {
                type: "success dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
        }
    },

    stop: function () {
        if (SendCheckUndeliveryOrder.ajax != null) {
            SendCheckUndeliveryOrder.ajax.abort();
        }

        SendCheckUndeliveryOrder.$modal.find('.row-with-btn-start').show();
        SendCheckUndeliveryOrder.$modal.find('.row-with-text-start').show();
        SendCheckUndeliveryOrder.$modal.find('.row-with-btn-stop').hide();
        SendCheckUndeliveryOrder.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (SendCheckUndeliveryOrder.count) {
            var part = 100 / SendCheckUndeliveryOrder.count;
            var count = SendCheckUndeliveryOrder.count - SendCheckUndeliveryOrder.$form.find('input[name="id[]"]').length;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
