var I18n = I18n || {};

$(function () {
    SendCheckAddress.init();
});

var send_check_address = $('#modal_send_check_address'),
    SendCheckAddress = {
    count: 0,
    currentCount: 0,
    $modal: send_check_address,
    $form: send_check_address.find('form'),
    timer: 0,

    init: function () {
        $('#start_send_check_address').on('click', SendCheckAddress.start);
        $('#stop_send_check_address').on('click', SendCheckAddress.stop);
    },

    start: function () {
        SendCheckAddress.$modal.find('.sender-percent').text('0%');
        SendCheckAddress.$modal.find('.progress-bar').css('width', '0%');

        SendCheckAddress.$modal.find('.row-with-btn-start').hide();
        SendCheckAddress.$modal.find('.row-with-text-start').hide();
        SendCheckAddress.$modal.find('.row-with-btn-stop').show();
        SendCheckAddress.$modal.find('.row-with-text-stop').show();

        SendCheckAddress.count = SendCheckAddress.$form.find('input[name="id[]"]').length;

        if (SendCheckAddress.count) {
            SendCheckAddress.next();
        } else {
            $.notify({message: I18n['Необходимо выбрать заказы.']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            SendCheckAddress.stop();
        }
    },

    next: function () {
        var $id = SendCheckAddress.$form.find('input[name="id[]"]:first');

        if ($id.length) {
            var percent = SendCheckAddress.calculatePercent();

            SendCheckAddress.$modal.find('.sender-percent').text(percent + '%');
            SendCheckAddress.$modal.find('.progress-bar').css('width', percent + '%');

            $id.remove();

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {
                id: $id.val(),
                delivery: $('#send_check_address').val()
            };

            dataAjax[csrfParam] = csrfToken;

            var $checkbox = $('#orders_content input[name="id[]"][value="' + $id.val() + '"]');
            $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
            $checkbox.closest('tr').removeClass('text-danger');

            SendCheckAddress.ajax = $.ajax({
                type: 'POST',
                url: SendCheckAddress.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    console.log(1);
                    if (response.status == 'success') {
                        if (response.notifyWarning) {
                            $.notify({message: response.notifyMessage}, {
                                type: "warning dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }

                        $checkbox.closest('tr').remove();
                        SendCheckAddress.next();
                    } else {
                        $checkbox.prop('checked', false);
                        $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                        $checkbox.closest('tr').addClass('text-danger');
                        SendCheckAddress.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    $checkbox.prop('checked', false);
                    SendCheckAddress.stop();
                    response = JSON.parse(response.responseText);

                    if (response.statusText != 'abort') {
                        if (typeof response.message != 'undefined') {
                            $.notify({message: response.message}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        } else {
                            $.notify({message: I18n['Не удалось отправить заказ в колл-центр.']}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }
                    }
                }
            });
        } else {
            SendCheckAddress.ajax = null;
            SendCheckAddress.$modal.modal('hide');

            $.notify({message: I18n['Отправка заказов в колл-центр завершена.']}, {
                type: "success dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
        }
    },

    stop: function () {
        if (SendCheckAddress.ajax != null) {
            SendCheckAddress.ajax.abort();
        }

        SendCheckAddress.$modal.find('.row-with-btn-start').show();
        SendCheckAddress.$modal.find('.row-with-text-start').show();
        SendCheckAddress.$modal.find('.row-with-btn-stop').hide();
        SendCheckAddress.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (SendCheckAddress.count) {
            var part = 100 / SendCheckAddress.count;
            var count = SendCheckAddress.count - SendCheckAddress.$form.find('input[name="id[]"]').length;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
