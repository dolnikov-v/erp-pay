var I18n = I18n || {};

$(function () {
    DeleteOrdersFromLists.init();
});

var DeleteOrdersFromLists = {
    count: 0,
    currentCount: 0,
    $modal: $('#modal_delete_orders_from_lists'),
    $form: $('#modal_delete_orders_from_lists').find('form'),
    timer: 0,

    init: function () {
        DeleteOrdersFromLists.$form.on('submit', DeleteOrdersFromLists.start);
    },

    start: function (event) {
        clearInterval(DeleteOrdersFromLists.timer);
        DeleteOrdersFromLists.count = DeleteOrdersFromLists.$form.find('input[name="id[]"]').length;
        DeleteOrdersFromLists.currentCount = 0;

        if (DeleteOrdersFromLists.count) {
            DeleteOrdersFromLists.$form.find('.row-with-text-start').hide();
            DeleteOrdersFromLists.$form.find('.row-with-btn-start').hide();
            DeleteOrdersFromLists.$form.find('.row-with-text-stop').show();

            DeleteOrdersFromLists.timer = setInterval(DeleteOrdersFromLists.tick, 2000);
        } else {
            $.notify({message: I18n['Необходимо выбрать заказы.']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });

            event.stopImmediatePropagation();

            return false;
        }
    },

    stop: function () {
    },

    tick: function () {
        DeleteOrdersFromLists.currentCount++;

        var part = 100 / DeleteOrdersFromLists.count;
        var percent = parseInt(DeleteOrdersFromLists.currentCount * part);

        if (percent > 99) {
            percent = 99;
        }

        DeleteOrdersFromLists.$modal.find('.sender-percent').text(percent + '%');
        DeleteOrdersFromLists.$modal.find('.progress-bar').css('width', percent + '%');

        if (DeleteOrdersFromLists.currentCount >= DeleteOrdersFromLists.count) {
            clearInterval(DeleteOrdersFromLists.timer);
        }
    }
};
