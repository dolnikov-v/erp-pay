var I18n = I18n || {};

$(function () {
    TransferToDistributor.init();
});

var transfer_to_distributor = $('#modal_transfer_to_distributor'),
    TransferToDistributor = {
        distributor: transfer_to_distributor.find("[name*=country_id]"),
        count: 0,
        currentCount: 0,
        $modal: transfer_to_distributor,
        $form: transfer_to_distributor.find('form'),
        timer: 0,

        init: function () {
            $('#start_transfer_to_distributor').on('click', TransferToDistributor.start);
            $('#stop_transfer_to_distributor').on('click', TransferToDistributor.stop);

            TransferToDistributor.$modal.on('shown.bs.modal', function () {
                $(this).find('.btn-success').prop('disabled', true);
            });

            TransferToDistributor.$modal.on('hide.bs.modal', function () {
                TransferToDistributor.distributor.val('');
                TransferToDistributor.distributor.trigger('change');
                TransferToDistributor.stop();
            });

            TransferToDistributor.distributor.on('change', TransferToDistributor.checkActive);
        },
        checkActive: function () {
            TransferToDistributor.$modal.find('.btn-success').prop('disabled', TransferToDistributor.distributor.val() == '');
        },
        start: function () {
            TransferToDistributor.$modal.find('.sender-percent').text('0%');
            TransferToDistributor.$modal.find('.progress-bar').css('width', '0%');

            TransferToDistributor.$modal.find('.row-with-btn-start').hide();
            TransferToDistributor.$modal.find('.row-with-text-start').hide();
            TransferToDistributor.$modal.find('.row-with-btn-stop').show();
            TransferToDistributor.$modal.find('.row-with-text-stop').show();

            TransferToDistributor.count = TransferToDistributor.$form.find('input[name="id[]"]').length;

            if (TransferToDistributor.count) {
                TransferToDistributor.next();
            } else {
                $.notify({message: I18n['Необходимо выбрать заказы.']}, {
                    type: "danger dark",
                    animate: {exit: 'hide'},
                    z_index: 2031
                });
                TransferToDistributor.stop();
            }
        },

        next: function () {
            var $id = TransferToDistributor.$form.find('input[name="id[]"]:first');

            if ($id.length) {
                var percent = TransferToDistributor.calculatePercent();

                TransferToDistributor.$modal.find('.sender-percent').text(percent + '%');
                TransferToDistributor.$modal.find('.progress-bar').css('width', percent + '%');

                $id.remove();

                var csrfParam = $('meta[name=csrf-param]').attr("content");
                var csrfToken = $('meta[name=csrf-token]').attr("content");

                var dataAjax = {
                    id: $id.val(),
                    country_id: TransferToDistributor.distributor.val()
                };

                dataAjax[csrfParam] = csrfToken;

                var $checkbox = $('#orders_content input[name="id[]"][value="' + $id.val() + '"]');
                $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
                $checkbox.closest('tr').removeClass('text-danger');

                TransferToDistributor.ajax = $.ajax({
                    type: 'POST',
                    url: TransferToDistributor.$form.data('sender-url'),
                    dataType: 'json',
                    data: dataAjax,
                    success: function (response, textStatus) {
                        if (response.status == 'success') {
                            $checkbox.closest('tr').remove();
                            TransferToDistributor.next();
                        } else {
                            $checkbox.prop('checked', false);
                            $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                            $checkbox.closest('tr').addClass('text-danger');
                            TransferToDistributor.stop();

                            $.notify({message: response.message}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }
                    },
                    error: function (response) {
                        $checkbox.prop('checked', false);
                        TransferToDistributor.stop();
                        response = JSON.parse(response.responseText);

                        if (response.statusText != 'abort') {
                            if (typeof response.message != 'undefined') {
                                $.notify({message: response.message}, {
                                    type: "danger dark",
                                    animate: {exit: 'hide'},
                                    z_index: 2031
                                });
                            } else {
                                $.notify({message: I18n['Не удалось отправить заказы дистребьютору\'']}, {
                                    type: "danger dark",
                                    animate: {exit: 'hide'},
                                    z_index: 2031
                                });
                            }
                        }
                    }
                });
            } else {
                TransferToDistributor.ajax = null;
                TransferToDistributor.$modal.modal('hide');

                $.notify({message: I18n['Передача заказов дистрибьютору завершена.']}, {
                    type: "success dark",
                    animate: {exit: 'hide'},
                    z_index: 2031
                });
            }
        },

        stop: function () {
            if (TransferToDistributor.ajax != null) {
                TransferToDistributor.ajax.abort();
            }

            TransferToDistributor.$modal.find('.row-with-btn-start').show();
            TransferToDistributor.$modal.find('.row-with-text-start').show();
            TransferToDistributor.$modal.find('.row-with-btn-stop').hide();
            TransferToDistributor.$modal.find('.row-with-text-stop').hide();
        },

        calculatePercent: function () {
            var percent = 0;

            if (TransferToDistributor.count) {
                var part = 100 / TransferToDistributor.count;
                var count = TransferToDistributor.count - TransferToDistributor.$form.find('input[name="id[]"]').length;

                percent = parseInt(count * part);

                if (percent > 100) {
                    percent = 100;
                }
            }

            return percent;
        }
    };
