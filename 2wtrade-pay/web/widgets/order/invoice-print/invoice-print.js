$(function () {
    InvoicePrint.init();
});

var InvoicePrint = {
    ajax: null,
    $modal: $('#modal_invoice_print'),
    $form: $('#modal_invoice_print form'),
    count: 0,
    currentCount: 0,
    timer: 0,

    init: function () {
        $('.print-invoice-link').on('click', InvoicePrint.modalShow);
        InvoicePrint.$modal.on('hide.bs.modal', InvoicePrint.modalHide);
        InvoicePrint.$form.on('submit', InvoicePrint.submit);
    },

    initStart: function () {
        InvoicePrint.$form.find('.row-with-text-start').show();
        InvoicePrint.$form.find('.row-with-btn-start').show();
        InvoicePrint.$form.find('.row-with-text-progress').hide();
        InvoicePrint.$form.find('.row-with-text-finish').hide();
    },

    initProgress: function () {
        InvoicePrint.$form.find('.row-with-text-start').hide();
        InvoicePrint.$form.find('.row-with-btn-start').hide();
        InvoicePrint.$form.find('.row-with-text-progress').show();
        InvoicePrint.$form.find('.row-with-text-finish').hide();
    },

    initFinish: function () {
        InvoicePrint.$form.find('.row-with-text-start').hide();
        InvoicePrint.$form.find('.row-with-btn-start').hide();
        InvoicePrint.$form.find('.row-with-text-progress').hide();
        InvoicePrint.$form.find('.row-with-text-finish').show();
    },

    modalShow: function () {
        InvoicePrint.initStart();

        $('#invoice_builder_list_id').val($(this).data('list-id'));
/*        InvoicePrint.count = $(this).data('count-orders');
        InvoicePrint.currentCount = 0;*/

        InvoicePrint.$modal.modal();

        return false;
    },

    modalHide: function () {
        if (InvoicePrint.ajax != null) {
            InvoicePrint.ajax.abort();
        }

        clearInterval(InvoicePrint.timer);
        InvoicePrint.setPercentProgress(0);
    },

    submit: function (event) {
        InvoicePrint.initProgress();

        if ($('#orderlogisticlistinvoice-format').val() == 'docx') {
            InvoicePrint.timer = setInterval(InvoicePrint.tick, 150);
        } else {
            InvoicePrint.timer = setInterval(InvoicePrint.tick, 3000);
        }

        InvoicePrint.ajax = $.ajax({
            type: 'POST',
            url: InvoicePrint.$form.attr('action'),
            dataType: 'json',
            data: InvoicePrint.$form.serialize(),
            success: function (response, textStatus) {
                if (response.status == 'success') {
                    InvoicePrint.initFinish();
                    //$('#invoice_builder_link_download').attr('href', response.message);
                } else {
                    InvoicePrint.initStart();
                    MainBootstrap.addNotificationDanger(response.message);
                }
            },
            error: function (response) {
                InvoicePrint.initStart();

                response = JSON.parse(response.responseText);

                if (response.statusText != 'abort') {
                    if (typeof response.message != 'undefined') {
                        MainBootstrap.addNotificationDanger(response.message);
                    } else {
                        MainBootstrap.addNotificationDanger(I18n['Не удалось сгенерировать накладные.']);
                    }
                }
            }
        });

        event.stopImmediatePropagation();

        return false;
    },

    tick: function () {
        InvoicePrint.currentCount++;

        var part = 100 / InvoicePrint.count;
        var percent = parseInt(InvoicePrint.currentCount * part);

        if (percent > 99) {
            percent = 99;
        }

        InvoicePrint.setPercentProgress(percent);

        if (InvoicePrint.currentCount >= InvoicePrint.count) {
            clearInterval(InvoicePrint.timer);
        }
    },

    setPercentProgress: function (percent) {
        InvoicePrint.$modal.find('.builder-percent').text(percent + '%');
        InvoicePrint.$modal.find('.progress-bar').css('width', percent + '%');
    }
};
