$(function () {
    var $modal = $('#modal_invoice_print'),
        $modalForm = $('#modal_invoice_print form'),
        $downloadLink = $('#invoice-download-link'),
        $orderId = $('#invoice_order_id');
    $('.print-invoice-anchor').click(function (e) {
        $('#invoice_order_id').val($(this).data('order_id'));
        $('#invoice_list_id').val($(this).data('list_id'));
        $modal.modal();
        e.preventDefault();
    });

    $modalForm.submit(function (e) {
        e.stopImmediatePropagation();

        $.ajax({
            type: 'POST',
            url: $(this).attr('action'),
            dataType: 'json',
            data: $(this).serialize(),
            success: function (response) {
                if (response.status == 'success') {
                    if(response.file) {
                        var $fileType = $('#select2-orderlogisticlistinvoice-format-container').text().toLowerCase();
                        console.log($fileType);
                        $downloadLink.attr("download", $orderId.val() + '.' + $fileType);
                        var file = 'data:application/octet-stream;base64,' + response.file;
                        $downloadLink.attr("href", file);
                        $downloadLink.show();
                        $downloadLink.click(function () {
                            $(this).hide();
                        })
                    }
                } else {
                    $.notify({message: response.message}, {
                        type: "danger dark",
                        animate: {exit: 'hide'},
                        z_index: 2031
                    });
                }
            },
            error: function (response) {
                console.log('error');
                console.log(response.responseText);
                $.notify({message: response.responseText}, {
                    type: "danger dark",
                    animate: {exit: 'hide'},
                    z_index: 2031
                });
            }
        });
        e.preventDefault();
        return false;
    })
});
