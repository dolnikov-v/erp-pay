var I18n = I18n || {};

$(function () {
    ResenderSeparateOrder.init();
});

var ResenderSeparateOrder = {
    ajax: null,
    count: 0,
    $modal: $('#modal_resend_separate_order'),
    $form: $('#modal_resend_separate_order').find('form'),

    init: function () {
        $('#start_resend_separate_delivery').on('click', ResenderSeparateOrder.start);
        $('#stop_resend_separate_delivery').on('click', ResenderSeparateOrder.stop);
    },

    start: function () {

        ResenderSeparateOrder.$modal.find('.sender-percent').text('0%');
        ResenderSeparateOrder.$modal.find('.progress-bar').css('width', '0%');

        ResenderSeparateOrder.$modal.find('.row-with-btn-start').hide();
        ResenderSeparateOrder.$modal.find('.row-with-text-start').hide();
        ResenderSeparateOrder.$modal.find('.row-with-btn-stop').show();
        ResenderSeparateOrder.$modal.find('.row-with-text-stop').show();

        ResenderSeparateOrder.count = ResenderSeparateOrder.$form.find('input[name="id[]"]').length;

        if (ResenderSeparateOrder.count) {
            ResenderSeparateOrder.next();
        } else {
            $.notify({message: I18n['Необходимо выбрать заказы.']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            ResenderSeparateOrder.stop();
        }
    },

    next: function () {
        var $id = ResenderSeparateOrder.$form.find('input[name="id[]"]:first');

        if ($id.length) {
            var percent = ResenderSeparateOrder.calculatePercent();

            ResenderSeparateOrder.$modal.find('.sender-percent').text(percent + '%');
            ResenderSeparateOrder.$modal.find('.progress-bar').css('width', percent + '%');

            $id.remove();

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {
                id: $id.val(),
                delivery: $('#modal_resend_separate_order').val()
            };

            dataAjax[csrfParam] = csrfToken;

            var $checkbox = $('#orders_content input[name="id[]"][value="' + $id.val() + '"]');
            $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
            $checkbox.closest('tr').removeClass('text-danger');

            ResenderSeparateOrder.ajax = $.ajax({
                type: 'POST',
                url: ResenderSeparateOrder.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        $checkbox.closest('tr').remove();
                        ResenderSeparateOrder.next();
                    } else {
                        $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                        $checkbox.closest('tr').addClass('text-danger');
                        ResenderSeparateOrder.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    ResenderSeparateOrder.stop();
                    response = JSON.parse(response.responseText);

                    if (response.statusText != 'abort') {
                        if (typeof response.message != 'undefined') {
                            $.notify({message: response.message}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        } else {
                            $.notify({message: I18n['Не удалось создать копию отправки и отправить в КЦ на обзвон']}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }
                    }
                }
            });
        } else {
            ResenderSeparateOrder.ajax = null;
            ResenderSeparateOrder.$modal.modal('hide');

            $.notify({message: I18n['Создание копий и отправка в КЦ на обзвон завершено.']}, {
                type: "success dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
        }
    },

    stop: function () {
        if (ResenderSeparateOrder.ajax != null) {
            ResenderSeparateOrder.ajax.abort();
        }

        ResenderSeparateOrder.$modal.find('.row-with-btn-start').show();
        ResenderSeparateOrder.$modal.find('.row-with-text-start').show();
        ResenderSeparateOrder.$modal.find('.row-with-btn-stop').hide();
        ResenderSeparateOrder.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (ResenderSeparateOrder.count) {
            var part = 100 / ResenderSeparateOrder.count;
            var count = ResenderSeparateOrder.count - ResenderSeparateOrder.$form.find('input[name="id[]"]').length;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
