var I18n = I18n || {};

$(function () {
    SecondDeliveryAttempt.init();
});

var SecondDeliveryAttempt = {
    ajax: null,
    count: 0,
    $modal: $('#modal_second_delivery_attempt'),
    $form: $('#modal_second_delivery_attempt').find('form'),

    init: function () {
        $('#start_second_delivery_attempt').on('click', SecondDeliveryAttempt.start);
        $('#stop_second_delivery_attempt').on('click', SecondDeliveryAttempt.stop);
    },

    start: function () {

        SecondDeliveryAttempt.$modal.find('.sender-percent').text('0%');
        SecondDeliveryAttempt.$modal.find('.progress-bar').css('width', '0%');

        SecondDeliveryAttempt.$modal.find('.row-with-btn-start').hide();
        SecondDeliveryAttempt.$modal.find('.row-with-text-start').hide();
        SecondDeliveryAttempt.$modal.find('.row-with-btn-stop').show();
        SecondDeliveryAttempt.$modal.find('.row-with-text-stop').show();

        SecondDeliveryAttempt.count = SecondDeliveryAttempt.$form.find('input[name="id[]"]').length;

        if (SecondDeliveryAttempt.count) {
            SecondDeliveryAttempt.next();
        } else {
            $.notify({message: I18n['Необходимо выбрать заказы.']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            SecondDeliveryAttempt.stop();
        }
    },

    next: function () {
        var $id = SecondDeliveryAttempt.$form.find('input[name="id[]"]:first');

        if ($id.length) {
            var percent = SecondDeliveryAttempt.calculatePercent();

            SecondDeliveryAttempt.$modal.find('.sender-percent').text(percent + '%');
            SecondDeliveryAttempt.$modal.find('.progress-bar').css('width', percent + '%');

            $id.remove();

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {
                id: $id.val(),
                date: $('#second_delivery_date').val()
            };

            dataAjax[csrfParam] = csrfToken;

            var $checkbox = $('#orders_content input[name="id[]"][value="' + $id.val() + '"]');
            $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
            $checkbox.closest('tr').removeClass('text-danger');

            SecondDeliveryAttempt.ajax = $.ajax({
                type: 'POST',
                url: SecondDeliveryAttempt.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        $checkbox.closest('tr').remove();
                        SecondDeliveryAttempt.next();
                    } else {
                        $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                        $checkbox.closest('tr').addClass('text-danger');
                        SecondDeliveryAttempt.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    SecondDeliveryAttempt.stop();
                    response = JSON.parse(response.responseText);

                    if (response.statusText != 'abort') {
                        if (typeof response.message != 'undefined') {
                            $.notify({message: response.message}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        } else {
                            $.notify({message: I18n['Не удалось изменить заказ.']}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }
                    }
                }
            });
        } else {
            SecondDeliveryAttempt.ajax = null;
            SecondDeliveryAttempt.$modal.modal('hide');

            $.notify({message: I18n['Изменение заказов завершено.']}, {
                type: "success dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
        }
    },

    stop: function () {
        if (SecondDeliveryAttempt.ajax != null) {
            SecondDeliveryAttempt.ajax.abort();
        }

        SecondDeliveryAttempt.$modal.find('.row-with-btn-start').show();
        SecondDeliveryAttempt.$modal.find('.row-with-text-start').show();
        SecondDeliveryAttempt.$modal.find('.row-with-btn-stop').hide();
        SecondDeliveryAttempt.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (SecondDeliveryAttempt.count) {
            var part = 100 / SecondDeliveryAttempt.count;
            var count = SecondDeliveryAttempt.count - SecondDeliveryAttempt.$form.find('input[name="id[]"]').length;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
