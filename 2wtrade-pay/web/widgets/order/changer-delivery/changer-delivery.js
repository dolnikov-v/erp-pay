var I18n = I18n || {};

$(function () {
    ChangerDelivery.init();
});

var ChangerDelivery = {
    ajax: null,
    count: 0,
    $modal: $('#modal_changer_delivery'),
    $form: $('#modal_changer_delivery').find('form'),

    init: function () {
        $('#start_changer_delivery').on('click', ChangerDelivery.start);
        $('#stop_changer_delivery').on('click', ChangerDelivery.stop);
    },

    start: function () {

        ChangerDelivery.$modal.find('.sender-percent').text('0%');
        ChangerDelivery.$modal.find('.progress-bar').css('width', '0%');

        ChangerDelivery.$modal.find('.row-with-btn-start').hide();
        ChangerDelivery.$modal.find('.row-with-text-start').hide();
        ChangerDelivery.$modal.find('.row-with-btn-stop').show();
        ChangerDelivery.$modal.find('.row-with-text-stop').show();

        ChangerDelivery.count = ChangerDelivery.$form.find('input[name="id[]"]').length;

        if (ChangerDelivery.count) {
            ChangerDelivery.next();
        } else {
            $.notify({message: I18n['Необходимо выбрать заказы.']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            ChangerDelivery.stop();
        }
    },

    next: function () {
        var $id = ChangerDelivery.$form.find('input[name="id[]"]:first');

        if ($id.length) {
            var percent = ChangerDelivery.calculatePercent();

            ChangerDelivery.$modal.find('.sender-percent').text(percent + '%');
            ChangerDelivery.$modal.find('.progress-bar').css('width', percent + '%');

            $id.remove();

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {
                id: $id.val(),
                delivery: $('#changer_delivery_select').val(),
                comment: $('#changer_delivery_comment').val(),
                ignore: $('#changer_delivery_ignore').prop('checked') ? 1 : null
            };

            dataAjax[csrfParam] = csrfToken;

            var $checkbox = $('#orders_content input[name="id[]"][value="' + $id.val() + '"]');
            $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
            $checkbox.closest('tr').removeClass('text-danger');

            ChangerDelivery.ajax = $.ajax({
                type: 'POST',
                url: ChangerDelivery.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        $checkbox.closest('tr').remove();
                        ChangerDelivery.next();
                    } else {
                        $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                        $checkbox.closest('tr').addClass('text-danger');
                        ChangerDelivery.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    ChangerDelivery.stop();
                    response = JSON.parse(response.responseText);

                    if (response.statusText != 'abort') {
                        if (typeof response.message != 'undefined') {
                            $.notify({message: response.message}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        } else {
                            $.notify({message: I18n['Не удалось сменить доставку.']}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }
                    }
                }
            });
        } else {
            ChangerDelivery.ajax = null;
            ChangerDelivery.$modal.modal('hide');

            $.notify({message: I18n['Смена доставки завершена.']}, {
                type: "success dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
        }
    },

    stop: function () {
        if (ChangerDelivery.ajax != null) {
            ChangerDelivery.ajax.abort();
        }

        ChangerDelivery.$modal.find('.row-with-btn-start').show();
        ChangerDelivery.$modal.find('.row-with-text-start').show();
        ChangerDelivery.$modal.find('.row-with-btn-stop').hide();
        ChangerDelivery.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (ChangerDelivery.count) {
            var part = 100 / ChangerDelivery.count;
            var count = ChangerDelivery.count - ChangerDelivery.$form.find('input[name="id[]"]').length;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
