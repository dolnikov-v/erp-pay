var I18n = I18n || {};

$(function () {
    SendPostSell.init();
});

var send_post_sale = $('#modal_send_post_sale'),
    SendPostSell = {
    count: 0,
    currentCount: 0,
    $modal: send_post_sale,
    $form: send_post_sale.find('form'),
    timer: 0,

    init: function () {
        $('#start_send_post_sale').on('click', SendPostSell.start);
        $('#stop_send_post_sale').on('click', SendPostSell.stop);
    },

    start: function () {
        SendPostSell.$modal.find('.sender-percent').text('0%');
        SendPostSell.$modal.find('.progress-bar').css('width', '0%');

        SendPostSell.$modal.find('.row-with-btn-start').hide();
        SendPostSell.$modal.find('.row-with-text-start').hide();
        SendPostSell.$modal.find('.row-with-btn-stop').show();
        SendPostSell.$modal.find('.row-with-text-stop').show();

        SendPostSell.count = SendPostSell.$form.find('input[name="id[]"]').length;

        if (SendPostSell.count) {
            SendPostSell.next();
        } else {
            $.notify({message: I18n['Необходимо выбрать заказы.']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            SendPostSell.stop();
        }
    },

    next: function () {
        var $id = SendPostSell.$form.find('input[name="id[]"]:first');

        if ($id.length) {
            var percent = SendPostSell.calculatePercent();

            SendPostSell.$modal.find('.sender-percent').text(percent + '%');
            SendPostSell.$modal.find('.progress-bar').css('width', percent + '%');

            $id.remove();

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {
                id: $id.val(),
                delivery: $('#send_check_address').val()
            };

            dataAjax[csrfParam] = csrfToken;

            var $checkbox = $('#orders_content input[name="id[]"][value="' + $id.val() + '"]');
            $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
            $checkbox.closest('tr').removeClass('text-danger');

            SendPostSell.ajax = $.ajax({
                type: 'POST',
                url: SendPostSell.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    console.log(1);
                    if (response.status == 'success') {
                        if (response.notifyWarning) {
                            $.notify({message: response.notifyMessage}, {
                                type: "warning dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }

                        $checkbox.closest('tr').remove();
                        SendPostSell.next();
                    } else {
                        $checkbox.prop('checked', false);
                        $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                        $checkbox.closest('tr').addClass('text-danger');
                        SendPostSell.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    $checkbox.prop('checked', false);
                    SendPostSell.stop();
                    response = JSON.parse(response.responseText);

                    if (response.statusText != 'abort') {
                        if (typeof response.message != 'undefined') {
                            $.notify({message: response.message}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        } else {
                            $.notify({message: I18n['Не удалось отправить заказ в колл-центр.']}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }
                    }
                }
            });
        } else {
            SendPostSell.ajax = null;
            SendPostSell.stop();
            SendPostSell.$modal.modal('hide');

            $.notify({message: I18n['Отправка заказов в колл-центр завершена.']}, {
                type: "success dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
        }
    },

    stop: function () {
        if (SendPostSell.ajax != null) {
            SendPostSell.ajax.abort();
        }

        SendPostSell.$modal.find('.row-with-btn-start').show();
        SendPostSell.$modal.find('.row-with-text-start').show();
        SendPostSell.$modal.find('.row-with-btn-stop').hide();
        SendPostSell.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (SendPostSell.count) {
            var part = 100 / SendPostSell.count;
            var count = SendPostSell.count - SendPostSell.$form.find('input[name="id[]"]').length;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
