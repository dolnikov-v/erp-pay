var I18n = I18n || {};

$(function () {
    GeneratorPretensionList.init();
});

var GeneratorPretensionList = {
    count: 0,
    currentCount: 0,
    $modal: $('#modal_generate_pretension_list'),
    $form: $('#modal_generate_pretension_list').find('form'),
    timer: 0,

    init: function () {
        GeneratorPretensionList.$form.on('submit', GeneratorPretensionList.start);
    },

    start: function (event) {
        clearInterval(GeneratorPretensionList.timer);
        GeneratorPretensionList.count = GeneratorPretensionList.$form.find('input[name="id[]"]').length;
        GeneratorPretensionList.currentCount = 0;

        if (GeneratorPretensionList.count) {
            GeneratorPretensionList.$form.find('.row-with-text-start').hide();
            GeneratorPretensionList.$form.find('.row-with-btn-start').hide();
            GeneratorPretensionList.$form.find('.row-with-text-stop').show();

            GeneratorPretensionList.timer = setInterval(GeneratorPretensionList.tick, 300);
        } else {
            $.notify({message: I18n['Необходимо выбрать заказы.']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });

            event.stopImmediatePropagation();

            return false;
        }
    },

    stop: function () {
        GeneratorPretensionList.$form.find('.input-group-file input[type="file"]').val('');
        GeneratorPretensionList.$form.find('.input-group-file input[type="text"]').val('');
    },

    tick: function () {
        GeneratorPretensionList.currentCount++;

        var part = 100 / GeneratorPretensionList.count;
        var percent = parseInt(GeneratorPretensionList.currentCount * part);

        if (percent > 99) {
            percent = 99;
        }

        GeneratorPretensionList.$modal.find('.sender-percent').text(percent + '%');
        GeneratorPretensionList.$modal.find('.progress-bar').css('width', percent + '%');

        if (GeneratorPretensionList.currentCount >= GeneratorPretensionList.count) {
            clearInterval(GeneratorPretensionList.timer);
        }
    }
};
