var I18n = I18n || {};

$(function () {
    ChangeStatusByLimitedUser.init();
});

var ChangeStatusByLimitedUser = {
    ajax: null,
    count: 0,
    $modal: $('#modal_change_status_by_limited_user'),
    $form: $('#modal_change_status_by_limited_user').find('form'),

    init: function () {
        $('#start_change_status_by_limited_user').on('click', ChangeStatusByLimitedUser.start);
        $('#stop_change_status_by_limited_user').on('click', ChangeStatusByLimitedUser.stop);
    },

    start: function () {
        ChangeStatusByLimitedUser.$modal.find('.sender-percent').text('0%');
        ChangeStatusByLimitedUser.$modal.find('.progress-bar').css('width', '0%');

        ChangeStatusByLimitedUser.$modal.find('.row-with-btn-start').hide();
        ChangeStatusByLimitedUser.$modal.find('.row-with-text-start').hide();
        ChangeStatusByLimitedUser.$modal.find('.row-with-btn-stop').show();
        ChangeStatusByLimitedUser.$modal.find('.row-with-text-stop').show();

        ChangeStatusByLimitedUser.count = ChangeStatusByLimitedUser.$form.find('input[name="id[]"]').length;

        if (ChangeStatusByLimitedUser.count) {
            ChangeStatusByLimitedUser.next();
        } else {
            $.notify({message: I18n['Необходимо выбрать заказы.']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            ChangeStatusByLimitedUser.stop();
        }
    },

    next: function () {
        var $id = ChangeStatusByLimitedUser.$form.find('input[name="id[]"]:first');

        if ($id.length) {
            var percent = ChangeStatusByLimitedUser.calculatePercent();

            ChangeStatusByLimitedUser.$modal.find('.sender-percent').text(percent + '%');
            ChangeStatusByLimitedUser.$modal.find('.progress-bar').css('width', percent + '%');

            $id.remove();

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {
                id: $id.val(),
                status: $('#changer_status_select').val()
            };

            dataAjax[csrfParam] = csrfToken;

            var $checkbox = $('#orders_content input[name="id[]"][value="' + $id.val() + '"]');
            $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
            $checkbox.closest('tr').removeClass('text-danger');

            ChangeStatusByLimitedUser.ajax = $.ajax({
                type: 'POST',
                url: ChangeStatusByLimitedUser.$form.data('sender-url'),
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        $checkbox.closest('tr').remove();
                        ChangeStatusByLimitedUser.next();
                    } else {
                        $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                        $checkbox.closest('tr').addClass('text-danger');
                        ChangeStatusByLimitedUser.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    ChangeStatusByLimitedUser.stop();
                    response = JSON.parse(response.responseText);

                    if (response.statusText != 'abort') {
                        if (typeof response.message != 'undefined') {
                            $.notify({message: response.message}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        } else {
                            $.notify({message: I18n['Не удалось сменить статус.']}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }
                    }
                }
            });
        } else {
            ChangeStatusByLimitedUser.ajax = null;
            ChangeStatusByLimitedUser.$modal.modal('hide');

            $.notify({message: I18n['Смена статусов завершена.']}, {
                type: "success dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
        }
    },

    stop: function () {
        if (ChangeStatusByLimitedUser.ajax != null) {
            ChangeStatusByLimitedUser.ajax.abort();
        }

        ChangeStatusByLimitedUser.$modal.find('.row-with-btn-start').show();
        ChangeStatusByLimitedUser.$modal.find('.row-with-text-start').show();
        ChangeStatusByLimitedUser.$modal.find('.row-with-btn-stop').hide();
        ChangeStatusByLimitedUser.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (ChangeStatusByLimitedUser.count) {
            var part = 100 / ChangeStatusByLimitedUser.count;
            var count = ChangeStatusByLimitedUser.count - ChangeStatusByLimitedUser.$form.find('input[name="id[]"]').length;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
