$(function () {
    ShowerColumns.init();
});

var ShowerColumns = {
    ajax: null,

    init: function () {
        /*$('.shower-columns-checkbox').on('change', ShowerColumns.toggleColumn);*/
        $('#apply_shower_columns').on('click', function () {
            console.log(this);
            $('#apply_shower_columns').prop('disabled', true);
            ShowerColumns.toggleColumn();
            return false;
        });
    },

    toggleColumn: function () {
        if (ShowerColumns.ajax) {
            ShowerColumns.ajax.abort();
        }

        var columns = [];
        var choices = [];

        $('.shower-columns-checkbox').map(function () {
            columns.push($(this).data('column'));
            choices.push($(this).prop('checked') ? 1 : 0);
        })

        var dataAjax = {
            columns: columns,
            choices: choices
        };

        $('#modal_shower_columns .block-loading').show();
        $('#modal_shower_columns .btn').focus();

        ShowerColumns.ajax = $.ajax({
            type: 'POST',
            url: location.href,
            dataType: 'json',
            data: dataAjax,
            success: function (response, textStatus) {
                $('#modal_shower_columns .block-loading').hide();

                if (typeof response.content != 'undefined') {
                    var $table = $('#orders_content');
                    var $panel = $table.closest('.panel');

                    $table.remove();
                    $panel.find('.panel-heading').after(response.content);
                }

                $('#apply_shower_columns').prop('disabled', false);
            }
        });
    }
};
