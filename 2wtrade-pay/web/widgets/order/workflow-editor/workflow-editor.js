var workflowScheme = workflowScheme || [];

$(function () {
    WorkflowEditor.init();
    WorkflowEditor.initActions();
});

var WorkflowEditor = {
    scheme: null,

    initActions: function () {
        $('.save-workflow-scheme').on('click', function () {
            WorkflowEditor.save();
        });

        $('.layout-workflow-scheme').on('click', function () {
            WorkflowEditor.layout();
        });
    },

    init: function () {
        var $ = go.GraphObject.make;

        WorkflowEditor.scheme = $(go.Diagram, "workflow_scheme",
            {
                initialContentAlignment: go.Spot.Center,
                layout: $(go.LayeredDigraphLayout, {isInitial: false, isOngoing: false, layerSpacing: 10}),
                initialAutoScale: go.Diagram.Uniform,
                "undoManager.isEnabled": true
            });

        WorkflowEditor.scheme.nodeTemplate =
            $(go.Node, "Auto",
                new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
                $(go.Shape, "RoundedRectangle", {strokeWidth: 0},
                    new go.Binding("fill", "color")),
                $(go.TextBlock,
                    {margin: 8},
                    new go.Binding("text", "text"))
            );

        WorkflowEditor.scheme.linkTemplate =
            $(go.Link,
                {curve: go.Link.Normal, toShortLength: 5},
                new go.Binding("smoothness", "smoothness"),
                $(go.Shape,
                    {stroke: "#2F4F4F", strokeWidth: 1}),
                $(go.Shape,
                    {toArrow: "kite", fill: "#2F4F4F", stroke: null, scale: 1})
            );
        WorkflowEditor.scheme.linkTemplateMap.add("Comment",
            $(go.Link, {selectable: false}));
        WorkflowEditor.scheme.model = go.Model.fromJson(workflowScheme);
    },

    layout: function () {
        WorkflowEditor.scheme.layoutDiagram(true);
    },

    save: function () {
        var dataAjax = {
            id: $('.wrap-editor').data('id-scheme'),
            value: WorkflowEditor.scheme.model.toJson()
        };

        var success = $(this).closest('table').data('i18n-success');
        var fail = $(this).closest('table').data('i18n-fail');

        $.ajax({
            type: 'POST',
            url: $('.wrap-editor').data('url'),
            dataType: 'json',
            data: dataAjax,
            success: function () {
                $.notify({message: I18n['Успешно сохранено.']}, {
                    type: "success dark",
                    animate: {exit: 'hide'},
                    z_index: 2031
                });
            },
            error: function () {
                $.notify({message: I18n['Не удалось сохранить.']}, {
                    type: "danger dark",
                    animate: {exit: 'hide'},
                    z_index: 2031
                });
            }
        });
    }
};
