$(function () {
    $('.container-countrydelivery .country').on('change', Delivery.change);
});

var Delivery = {
    change: function () {

        $.ajax({
            type: 'POST',
            url: '/report/debt/get-select-options-delivery',
            data: {
                country_id: $('.container-countrydelivery .country select').val()
            },
            success: function (response) {
                $('.container-countrydelivery .delivery select').empty().html(response);
                $('.container-countrydelivery .delivery select').removeAttr('disabled');
            },
            error: function () {
                $('.container-countrydelivery .delivery select').attr('disabled', '');

            }
        });

        $('.container-countrydelivery .delivery .selection span span').empty().html('—');
        $('.container-countrydelivery .delivery .selection span span').attr('title', '—');
    }
};
