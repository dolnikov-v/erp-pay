$(function () {
    ChangeableList.init();
});

var ChangeableList = {
    init: function () {
        $('.changeable-list').on('click', '.delete-changeable-element', ChangeableList.removeElement);
        $('.changeable-list').on('click', '.add-changeable-element', ChangeableList.addElement);
    },
    addElement: function (e) {
        const parent = $(this).closest('.changeable-list-element.changeable-list-template');
        var $newRow = parent.clone();
        $newRow.removeClass('changeable-list-template');
        $newRow.find('.add-changeable-element').addClass('hidden');
        $newRow.find('.delete-changeable-element').removeClass('hidden');
        $newRow.insertBefore(parent);
        e.preventDefault();
        e.stopImmediatePropagation();
        var callbackFunc = $(this).data('callback-func');
        if ((typeof callbackFunc) !== "undefined" && callbackFunc !== '') {
            var callbackFunctions = callbackFunc.split(';');
            for (var key in callbackFunctions) {
                if (callbackFunctions[key] !== '') {
                    Main.executeFunctionByName(callbackFunctions[key], window, $newRow, parent);
                }
            }
        }
    },
    removeElement: function (e) {
        $(this).parent().remove();
        e.preventDefault();
    },
    refreshSelect: function ($newRow, parent) {
        $($newRow).find('select[data-plugin="select2"]').each(function () {
            $(this).parent().find('.select2').remove();
            $(this).select2();
            $(this).parent().find('>span.select2').removeAttr('style');
        });
    },
    refreshDatepicker: function ($newRow, parent) {
        $($newRow).find('.container-daterangepicker').each(function () {
            // TODO: добавить свойства по мере необходимости
            var params = {
                locale: $(this).attr('data-locale'),
                format: $(this).attr('data-format'),
            };
            $(this).find("input").datetimepicker(params);
        });
    },
    setDefaultValue: function ($newRow, parent) {
        $($newRow).find('input[data-default]').each(function () {
            $(this).val($(this).attr('data-default'));
        });
    },
};