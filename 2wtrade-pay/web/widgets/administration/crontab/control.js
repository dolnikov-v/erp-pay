$(function () {
    $('input[data-action="crontab-active-switchery"]').on('change', CrontabActiveSwitchery.change);
});

var CrontabActiveSwitchery = {
    change: function () {
        var csrfParam = $('meta[name=csrf-param]').attr("content");
        var csrfToken = $('meta[name=csrf-token]').attr("content");

        var dataAjax = {
            task_id: $(this).data('task-id'),
            value: $(this).is(':checked') ? 'on' : 'off'
        };

        dataAjax[csrfParam] = csrfToken;

        $(this).closest('tr').toggleClass('text-muted');

        $.ajax({
            type: 'POST',
            url: '/administration/crontab/control/set-active',
            dataType: 'json',
            data: dataAjax,
            success: function (response, textStatus) {
                $.notify({message: 'Действие выполнено.'}, {type: "success dark", animate: {exit: 'hide'}, z_index: 2031});
            },
            error: function () {
                $.notify({message: 'Не удалось выполнить запрос.'}, {type: "danger dark", animate: {exit: 'hide'}, z_index: 2031});
            }
        });
    }
};
