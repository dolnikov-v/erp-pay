var I18n = I18n || {};

$(function () {
    NotapprovePersons.init();
});

var NotapprovePersons = {
    ajax: null,
    count: 0,
    $modal: $('#modal_notapprove_persons'),
    $form: $('#modal_notapprove_persons').find('form'),

    init: function () {
        $('#start_notapprove_persons').on('click', NotapprovePersons.start);
        $('#stop_notapprove_persons').on('click', NotapprovePersons.stop);
    },

    start: function () {
        NotapprovePersons.$modal.find('.sender-percent').text('0%');
        NotapprovePersons.$modal.find('.progress-bar').css('width', '0%');

        NotapprovePersons.$modal.find('.row-with-btn-start').hide();
        NotapprovePersons.$modal.find('.row-with-text-start').hide();
        NotapprovePersons.$modal.find('.row-with-btn-stop').show();
        NotapprovePersons.$modal.find('.row-with-text-stop').show();

        NotapprovePersons.count = NotapprovePersons.$form.find('input[name="id[]"]').length;

        if (NotapprovePersons.count) {
            NotapprovePersons.next();
        } else {
            $.notify({message: I18n['Необходимо выбрать сотрудников.']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            NotapprovePersons.stop();
        }
    },

    next: function () {
        var $id = NotapprovePersons.$form.find('input[name="id[]"]:first');

        if ($id.length) {
            var percent = NotapprovePersons.calculatePercent();

            NotapprovePersons.$modal.find('.sender-percent').text(percent + '%');
            NotapprovePersons.$modal.find('.progress-bar').css('width', percent + '%');

            $id.remove();

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {
                id: $id.val()
            };

            dataAjax[csrfParam] = csrfToken;

            var $checkbox = $('#persons_content input[name="id[]"][value="' + $id.val() + '"]');
            $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
            $checkbox.closest('tr').removeClass('text-danger');

            NotapprovePersons.ajax = $.ajax({
                type: 'POST',
                url: NotapprovePersons.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        $checkbox.closest('tr').addClass('text-muted');
                        var s = $checkbox.closest('tr').find('.approve-status span');
                        s.addClass('label-default').removeClass('label-success').text(I18n['Нет']);
                        NotapprovePersons.next();
                    } else {
                        $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                        $checkbox.closest('tr').addClass('text-danger');
                        NotapprovePersons.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    NotapprovePersons.stop();
                    response = JSON.parse(response.responseText);

                    if (response.statusText != 'abort') {
                        if (typeof response.message != 'undefined') {
                            $.notify({message: response.message}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        } else {
                            $.notify({message: I18n['Не удалось убрать подтверждение у сотрудников']}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }
                    }
                }
            });
        } else {
            NotapprovePersons.ajax = null;
            NotapprovePersons.$modal.modal('hide');

            $.notify({message: 'Операция выполнена успешно'}, {
                type: "success dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
        }
    },

    stop: function () {
        if (NotapprovePersons.ajax != null) {
            NotapprovePersons.ajax.abort();
        }

        NotapprovePersons.$modal.find('.row-with-btn-start').show();
        NotapprovePersons.$modal.find('.row-with-text-start').show();
        NotapprovePersons.$modal.find('.row-with-btn-stop').hide();
        NotapprovePersons.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (NotapprovePersons.count) {
            var part = 100 / NotapprovePersons.count;
            var count = NotapprovePersons.count - NotapprovePersons.$form.find('input[name="id[]"]').length;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
