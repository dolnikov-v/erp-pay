var I18n = I18n || {};

$(function () {
    DeleteBonuses.init();
});

var DeleteBonuses = {
    ajax: null,
    count: 0,
    $modal: $('#modal_delete_bonuses'),
    $form: $('#modal_delete_bonuses').find('form'),

    init: function () {
        $('#start_delete_bonuses').on('click', DeleteBonuses.start);
        $('#stop_delete_bonuses').on('click', DeleteBonuses.stop);
    },

    start: function () {
        DeleteBonuses.$modal.find('.sender-percent').text('0%');
        DeleteBonuses.$modal.find('.progress-bar').css('width', '0%');

        DeleteBonuses.$modal.find('.row-with-btn-start').hide();
        DeleteBonuses.$modal.find('.row-with-text-start').hide();
        DeleteBonuses.$modal.find('.row-with-btn-stop').show();
        DeleteBonuses.$modal.find('.row-with-text-stop').show();

        DeleteBonuses.count = DeleteBonuses.$form.find('input[name="id[]"]').length;

        if (DeleteBonuses.count) {
            DeleteBonuses.next();
        } else {
            $.notify({message: I18n['Необходимо выбрать бонусы']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            DeleteBonuses.stop();
        }
    },

    next: function () {
        var $id = DeleteBonuses.$form.find('input[name="id[]"]:first');

        if ($id.length) {
            var percent = DeleteBonuses.calculatePercent();

            DeleteBonuses.$modal.find('.sender-percent').text(percent + '%');
            DeleteBonuses.$modal.find('.progress-bar').css('width', percent + '%');

            $id.remove();

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {
                id: $id.val(),
                date: $('#bonuses_dismissal_date').val()
            };

            dataAjax[csrfParam] = csrfToken;

            var $checkbox = $('#bonuses_content input[name="id[]"][value="' + $id.val() + '"]');
            $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
            $checkbox.closest('tr').removeClass('text-danger');

            DeleteBonuses.ajax = $.ajax({
                type: 'POST',
                url: DeleteBonuses.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        $checkbox.closest('tr').remove();
                        DeleteBonuses.next();
                    } else {
                        $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                        $checkbox.closest('tr').addClass('text-danger');
                        DeleteBonuses.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    DeleteBonuses.stop();
                    response = JSON.parse(response.responseText);

                    if (response.statusText != 'abort') {
                        if (typeof response.message != 'undefined') {
                            $.notify({message: response.message}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        } else {
                            $.notify({message: I18n['Не удалось удалить бонусы']}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }
                    }
                }
            });
        } else {
            DeleteBonuses.ajax = null;
            DeleteBonuses.$modal.modal('hide');

            $.notify({message: 'Операция выполнена успешно'}, {
                type: "success dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
        }
    },

    stop: function () {
        if (DeleteBonuses.ajax != null) {
            DeleteBonuses.ajax.abort();
        }

        DeleteBonuses.$modal.find('.row-with-btn-start').show();
        DeleteBonuses.$modal.find('.row-with-text-start').show();
        DeleteBonuses.$modal.find('.row-with-btn-stop').hide();
        DeleteBonuses.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (DeleteBonuses.count) {
            var part = 100 / DeleteBonuses.count;
            var count = DeleteBonuses.count - DeleteBonuses.$form.find('input[name="id[]"]').length;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
