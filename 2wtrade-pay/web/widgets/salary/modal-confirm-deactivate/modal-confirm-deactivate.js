var I18n = I18n || {};

$(function () {
    ConfirmDeactivate.init();
});

var ConfirmDeactivate = {
    $modal: $('#modal_confirm_deactivate'),

    init: function () {
        $('#modal_confirm_deactivate_link').on('click', ConfirmDeactivate.start);
    },

    start: function () {
        if ($("#person_dismissal_date").val().length != 10) {
            $.notify({message: I18n['Введите дату увольнения']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            $("#person_dismissal_date").focus();
            return false;
        }
        if ($("#person_dismissal_comment").val().length == 0) {
            $.notify({message: I18n['Введите причину увольнения']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            $("#person_dismissal_comment").focus();
            return false;
        }
        return true;
    }

};
