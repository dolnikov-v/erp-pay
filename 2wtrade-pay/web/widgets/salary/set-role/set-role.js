var I18n = I18n || {};

$(function () {
    SetRole.init();
});

var SetRole = {
    ajax: null,
    count: 0,
    $modal: $('#modal_set_role'),
    $form: $('#modal_set_role').find('form'),

    init: function () {
        $('#start_set_role').on('click', SetRole.start);
        $('#stop_set_role').on('click', SetRole.stop);
    },

    start: function () {
        SetRole.$modal.find('.sender-percent').text('0%');
        SetRole.$modal.find('.progress-bar').css('width', '0%');

        SetRole.$modal.find('.row-with-btn-start').hide();
        SetRole.$modal.find('.row-with-text-start').hide();
        SetRole.$modal.find('.row-with-btn-stop').show();
        SetRole.$modal.find('.row-with-text-stop').show();

        SetRole.count = SetRole.$form.find('input[name="id[]"]').length;

        if (SetRole.count) {
            SetRole.next();
        } else {
            $.notify({message: I18n['Необходимо выбрать сотрудников.']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            SetRole.stop();
        }
    },

    next: function () {
        var $id = SetRole.$form.find('input[name="id[]"]:first');

        if ($id.length) {
            var percent = SetRole.calculatePercent();

            SetRole.$modal.find('.sender-percent').text(percent + '%');
            SetRole.$modal.find('.progress-bar').css('width', percent + '%');

            $id.remove();

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var designation = $("#set_role_select option:selected").text();

            var dataAjax = {
                id: $id.val(),
                role: $('#set_role_select').val()
            };

            dataAjax[csrfParam] = csrfToken;

            var $checkbox = $('#persons_content input[name="id[]"][value="' + $id.val() + '"]');
            $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
            $checkbox.closest('tr').removeClass('text-danger');

            SetRole.ajax = $.ajax({
                type: 'POST',
                url: SetRole.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        $checkbox.closest('tr').find('.designation').text(designation);
                        SetRole.next();
                    } else {
                        $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                        $checkbox.closest('tr').addClass('text-danger');
                        SetRole.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    SetRole.stop();
                    response = JSON.parse(response.responseText);

                    if (response.statusText != 'abort') {
                        if (typeof response.message != 'undefined') {
                            $.notify({message: response.message}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        } else {
                            $.notify({message: I18n['Не удалось установить должность']}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }
                    }
                }
            });
        } else {
            SetRole.ajax = null;
            SetRole.$modal.modal('hide');

            $.notify({message: 'Операция выполнена успешно'}, {
                type: "success dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
        }
    },

    stop: function () {
        if (SetRole.ajax != null) {
            SetRole.ajax.abort();
        }

        SetRole.$modal.find('.row-with-btn-start').show();
        SetRole.$modal.find('.row-with-text-start').show();
        SetRole.$modal.find('.row-with-btn-stop').hide();
        SetRole.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (SetRole.count) {
            var part = 100 / SetRole.count;
            var count = SetRole.count - SetRole.$form.find('input[name="id[]"]').length;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
