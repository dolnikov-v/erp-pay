var I18n = I18n || {};

$(function () {
    DeleteList.init();
});

var DeleteList = {
    ajax: null,
    count: 0,
    $modal: $('#modal_delete_list'),
    $form: $('#modal_delete_list').find('form'),

    init: function () {
        $('#start_delete_list').on('click', DeleteList.start);
        $('#stop_delete_list').on('click', DeleteList.stop);
    },

    start: function () {
        DeleteList.$modal.find('.sender-percent').text('0%');
        DeleteList.$modal.find('.progress-bar').css('width', '0%');

        DeleteList.$modal.find('.row-with-btn-start').hide();
        DeleteList.$modal.find('.row-with-text-start').hide();
        DeleteList.$modal.find('.row-with-btn-stop').show();
        DeleteList.$modal.find('.row-with-text-stop').show();

        DeleteList.count = DeleteList.$form.find('input[name="id[]"]').length;

        if (DeleteList.count) {
            DeleteList.next();
        } else {
            $.notify({message: I18n['Необходимо выбрать планы.']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            DeleteList.stop();
        }
    },

    next: function () {
        var $id = DeleteList.$form.find('input[name="id[]"]:first');

        if ($id.length) {
            var percent = DeleteList.calculatePercent();

            DeleteList.$modal.find('.sender-percent').text(percent + '%');
            DeleteList.$modal.find('.progress-bar').css('width', percent + '%');

            $id.remove();

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {
                id: $id.val(),
                date: $('#persons_dismissal_date').val()
            };

            dataAjax[csrfParam] = csrfToken;

            var $checkbox = $('#work-time-plan_content input[name="id[]"][value="' + $id.val() + '"]');
            $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
            $checkbox.closest('tr').removeClass('text-danger');

            DeleteList.ajax = $.ajax({
                type: 'POST',
                url: DeleteList.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        $checkbox.closest('tr').remove();
                        DeleteList.next();
                    } else {
                        $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                        $checkbox.closest('tr').addClass('text-danger');
                        DeleteList.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    DeleteList.stop();
                    response = JSON.parse(response.responseText);

                    if (response.statusText != 'abort') {
                        if (typeof response.message != 'undefined') {
                            $.notify({message: response.message}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        } else {
                            $.notify({message: I18n['Не удалось удалить планы']}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }
                    }
                }
            });
        } else {
            DeleteList.ajax = null;
            DeleteList.$modal.modal('hide');

            $.notify({message: 'Операция выполнена успешно'}, {
                type: "success dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
        }
    },

    stop: function () {
        if (DeleteList.ajax != null) {
            DeleteList.ajax.abort();
        }

        DeleteList.$modal.find('.row-with-btn-start').show();
        DeleteList.$modal.find('.row-with-text-start').show();
        DeleteList.$modal.find('.row-with-btn-stop').hide();
        DeleteList.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (DeleteList.count) {
            var part = 100 / DeleteList.count;
            var count = DeleteList.count - DeleteList.$form.find('input[name="id[]"]').length;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
