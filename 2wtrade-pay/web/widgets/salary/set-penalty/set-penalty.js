var I18n = I18n || {};

$(function () {
    SetPenalty.init();
});

var SetPenalty = {
    ajax: null,
    count: 0,
    $modal: $('#modal_set_penalty'),
    $form: $('#modal_set_penalty').find('form'),

    init: function () {
        $('#start_set_penalty').on('click', SetPenalty.start);
        $('#stop_set_penalty').on('click', SetPenalty.stop);
        $(document).on('change', '#set_penalty_type', SetPenalty.toggleLimitType);
    },

    toggleLimitType: function () {
        var v = $('#set_penalty_type').val();
        $(".limit").hide();
        if (v) {
            var type = penaltyTypes[v];
            if (type) {
                $(".limit_" + type.type).show();
                if (type.type == 'sum' || type.type == 'sum_work_hours') {
                    $('#set_penalty_sum_usd').val(eval(type.sum));
                    $('#set_penalty_sum_local').val(eval(type.sum_local));
                }
                if (type.type == 'percent') {
                    $(".limit_percent").find('input').val(eval(type.percent));
                }
                if (type.type == 'percent_calc_salary') {
                    $(".limit_percent").find('input').val(eval(type.percent));
                }
                if (type.type == 'other') {
                    $(".limit_other").find('input').val('').prop("readonly", false);
                }
            }
        }
    },

    start: function () {
        SetPenalty.$modal.find('.sender-percent').text('0%');
        SetPenalty.$modal.find('.progress-bar').css('width', '0%');

        SetPenalty.$modal.find('.row-with-btn-start').hide();
        SetPenalty.$modal.find('.row-with-text-start').hide();
        SetPenalty.$modal.find('.row-with-btn-stop').show();
        SetPenalty.$modal.find('.row-with-text-stop').show();

        SetPenalty.count = SetPenalty.$form.find('input[name="id[]"]').length;

        if (SetPenalty.count) {
            SetPenalty.next();
        } else {
            $.notify({message: I18n['Необходимо выбрать сотрудников.']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            SetPenalty.stop();
        }
    },

    next: function () {
        var $id = SetPenalty.$form.find('input[name="id[]"]:first');

        if ($id.length) {

            if (!$('#set_penalty_type').val()) {
                $.notify({message: I18n['Не удалось назначить штраф']}, {
                    type: "danger dark",
                    animate: {exit: 'hide'},
                    z_index: 2031
                });
                SetPenalty.stop();
            }

            var percent = SetPenalty.calculatePercent();

            SetPenalty.$modal.find('.sender-percent').text(percent + '%');
            SetPenalty.$modal.find('.progress-bar').css('width', percent + '%');

            $id.remove();

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");


            var dataAjax = {
                id: $id.val(),
                type: $('#set_penalty_type').val(),
                date: $('#set_penalty_date').val(),
                percent: $('#set_penalty_percent').val(),
                sum_usd: $('#set_penalty_sum_usd').val(),
                sum_local: $('#set_penalty_sum_local').val(),
                comment: $('#set_penalty_comment').val()
            };

            dataAjax[csrfParam] = csrfToken;

            var $checkbox = $('#persons_content input[name="id[]"][value="' + $id.val() + '"]');
            $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
            $checkbox.closest('tr').removeClass('text-danger');

            SetPenalty.ajax = $.ajax({
                type: 'POST',
                url: SetPenalty.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        $checkbox.closest('tr').remove();
                        SetPenalty.next();
                    } else {
                        $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                        $checkbox.closest('tr').addClass('text-danger');
                        SetPenalty.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    SetPenalty.stop();
                    response = JSON.parse(response.responseText);

                    if (response.statusText != 'abort') {
                        if (typeof response.message != 'undefined') {
                            $.notify({message: response.message}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        } else {
                            $.notify({message: I18n['Не удалось назначить штраф']}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }
                    }
                }
            });
        } else {
            SetPenalty.ajax = null;
            SetPenalty.$modal.modal('hide');

            $.notify({message: 'Операция выполнена успешно'}, {
                type: "success dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
        }
    },

    stop: function () {
        if (SetPenalty.ajax != null) {
            SetPenalty.ajax.abort();
        }

        SetPenalty.$modal.find('.row-with-btn-start').show();
        SetPenalty.$modal.find('.row-with-text-start').show();
        SetPenalty.$modal.find('.row-with-btn-stop').hide();
        SetPenalty.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (SetPenalty.count) {
            var part = 100 / SetPenalty.count;
            var count = SetPenalty.count - SetPenalty.$form.find('input[name="id[]"]').length;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
