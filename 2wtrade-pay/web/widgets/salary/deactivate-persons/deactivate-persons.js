var I18n = I18n || {};

$(function () {
    DeactivatePersons.init();
});

var DeactivatePersons = {
    ajax: null,
    count: 0,
    $modal: $('#modal_deactivate_persons'),
    $form: $('#modal_deactivate_persons').find('form'),

    init: function () {
        $('#start_deactivate_persons').on('click', DeactivatePersons.start);
        $('#stop_deactivate_persons').on('click', DeactivatePersons.stop);
    },

    start: function () {
        DeactivatePersons.$modal.find('.sender-percent').text('0%');
        DeactivatePersons.$modal.find('.progress-bar').css('width', '0%');

        DeactivatePersons.$modal.find('.row-with-btn-start').hide();
        DeactivatePersons.$modal.find('.row-with-text-start').hide();
        DeactivatePersons.$modal.find('.row-with-btn-stop').show();
        DeactivatePersons.$modal.find('.row-with-text-stop').show();

        DeactivatePersons.count = DeactivatePersons.$form.find('input[name="id[]"]').length;

        if (DeactivatePersons.count) {
            DeactivatePersons.next();
        } else {
            $.notify({message: I18n['Необходимо выбрать сотрудников.']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            DeactivatePersons.stop();
        }
    },

    next: function () {
        var $id = DeactivatePersons.$form.find('input[name="id[]"]:first');

        if ($id.length) {
            var percent = DeactivatePersons.calculatePercent();

            DeactivatePersons.$modal.find('.sender-percent').text(percent + '%');
            DeactivatePersons.$modal.find('.progress-bar').css('width', percent + '%');

            $id.remove();

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {
                id: $id.val(),
                date: $('#persons_dismissal_date').val()
            };

            dataAjax[csrfParam] = csrfToken;

            var $checkbox = $('#persons_content input[name="id[]"][value="' + $id.val() + '"]');
            $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
            $checkbox.closest('tr').removeClass('text-danger');

            DeactivatePersons.ajax = $.ajax({
                type: 'POST',
                url: DeactivatePersons.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        $checkbox.closest('tr').addClass('text-muted');
                        var s = $checkbox.closest('tr').find('.active-status span');
                        s.addClass('label-default').removeClass('label-success').text(I18n['Нет']);
                        DeactivatePersons.next();
                    } else {
                        $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                        $checkbox.closest('tr').addClass('text-danger');
                        DeactivatePersons.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    DeactivatePersons.stop();
                    response = JSON.parse(response.responseText);

                    if (response.statusText != 'abort') {
                        if (typeof response.message != 'undefined') {
                            $.notify({message: response.message}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        } else {
                            $.notify({message: I18n['Не удалось деактивировать сотрудников']}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }
                    }
                }
            });
        } else {
            DeactivatePersons.ajax = null;
            DeactivatePersons.$modal.modal('hide');

            $.notify({message: 'Операция выполнена успешно'}, {
                type: "success dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
        }
    },

    stop: function () {
        if (DeactivatePersons.ajax != null) {
            DeactivatePersons.ajax.abort();
        }

        DeactivatePersons.$modal.find('.row-with-btn-start').show();
        DeactivatePersons.$modal.find('.row-with-text-start').show();
        DeactivatePersons.$modal.find('.row-with-btn-stop').hide();
        DeactivatePersons.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (DeactivatePersons.count) {
            var part = 100 / DeactivatePersons.count;
            var count = DeactivatePersons.count - DeactivatePersons.$form.find('input[name="id[]"]').length;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
