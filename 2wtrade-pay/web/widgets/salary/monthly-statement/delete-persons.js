var I18n = I18n || {};

$(function () {
    DeletePersons.init();
});

var DeletePersons = {
    ajax: null,
    count: 0,
    $modal: $('#modal_delete_persons'),
    $form: $('#modal_delete_persons').find('form'),

    init: function () {
        $('#start_delete_persons').on('click', DeletePersons.start);
        $('#stop_delete_persons').on('click', DeletePersons.stop);
    },

    start: function () {
        DeletePersons.$modal.find('.sender-percent').text('0%');
        DeletePersons.$modal.find('.progress-bar').css('width', '0%');

        DeletePersons.$modal.find('.row-with-btn-start').hide();
        DeletePersons.$modal.find('.row-with-text-start').hide();
        DeletePersons.$modal.find('.row-with-btn-stop').show();
        DeletePersons.$modal.find('.row-with-text-stop').show();

        DeletePersons.count = DeletePersons.$form.find('input[name="data_id[]"]').length;

        if (DeletePersons.count) {
            DeletePersons.next();
        } else {
            $.notify({message: I18n['Необходимо выбрать сотрудников.']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            DeletePersons.stop();
        }
    },

    next: function () {
        var $id = DeletePersons.$form.find('input[name="data_id[]"]:first');

        if ($id.length) {
            var percent = DeletePersons.calculatePercent();

            DeletePersons.$modal.find('.sender-percent').text(percent + '%');
            DeletePersons.$modal.find('.progress-bar').css('width', percent + '%');

            $id.remove();

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {
                id: $id.val(),
                statement_id: $("#statement_id").val()
            };

            dataAjax[csrfParam] = csrfToken;

            var $checkbox = $('#data_content input[name="data_id[]"][value="' + $id.val() + '"]');
            $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
            $checkbox.closest('tr').removeClass('text-danger');

            DeletePersons.ajax = $.ajax({
                type: 'POST',
                url: DeletePersons.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        $checkbox.closest('tr').remove();
                        DeletePersons.next();
                    } else {
                        $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                        $checkbox.closest('tr').addClass('text-danger');
                        DeletePersons.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    DeletePersons.stop();
                    response = JSON.parse(response.responseText);

                    if (response.statusText != 'abort') {
                        if (typeof response.message != 'undefined') {
                            $.notify({message: response.message}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        } else {
                            $.notify({message: I18n['Не удалось деактивировать сотрудников']}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }
                    }
                }
            });
        } else {
            DeletePersons.ajax = null;
            DeletePersons.$modal.modal('hide');

            $.notify({message: 'Операция выполнена успешно'}, {
                type: "success dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
        }
    },

    stop: function () {
        if (DeletePersons.ajax != null) {
            DeletePersons.ajax.abort();
        }

        DeletePersons.$modal.find('.row-with-btn-start').show();
        DeletePersons.$modal.find('.row-with-text-start').show();
        DeletePersons.$modal.find('.row-with-btn-stop').hide();
        DeletePersons.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (DeletePersons.count) {
            var part = 100 / DeletePersons.count;
            var count = DeletePersons.count - DeletePersons.$form.find('input[name="data_id[]"]').length;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
