$(function () {
    ManageBonuses.init();
});

var ManageBonuses = {
    init: function () {
        $('.btn-staffing-bonuses-plus').off('click');
        $('.btn-staffing-bonuses-minus').off('click');

        $('.btn-staffing-bonuses-plus').on('click', ManageBonuses.addInput);
        $('.btn-staffing-bonuses-minus').on('click', ManageBonuses.removeInput);

        if ($('.group-bonuses-group .row-staffing-bonuses').find('.btn-staffing-bonuses-minus').size() == 1) {
            //$('.btn-staffing-bonuses-minus').hide();
        } else {
            //$('.btn-staffing-bonuses-minus').show();
            $('.btn-staffing-bonuses-plus').hide();
            //$('.btn-staffing-bonuses-minus').last().hide();
            $('.btn-staffing-bonuses-plus').last().show();
        }

    },
    addInput: function () {
        $('.group-bonuses-group .row-staffing-bonuses').last().clone().appendTo('.group-bonuses-group');

        $('.group-bonuses-group .row-staffing-bonuses').last().find('input').each(function () {
            $(this).val('');
        })

        $('.group-bonuses-group .row-staffing-bonuses').last().find('select').each(function () {
            $(this).parent().find('.select2').remove();
            $(this).val(0);
            $(this).select2();
            $(this).parent().find('.select2').css('width', '100%');
        });

        ManageBonuses.init();
    },
    removeInput: function () {
        $(this).parents().each(function () {
            if ($(this).hasClass('row-staffing-bonuses')) {
                $(this).remove();
            }
        });

        ManageBonuses.init();
    }
};
