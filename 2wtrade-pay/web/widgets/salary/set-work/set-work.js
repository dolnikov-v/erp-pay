var I18n = I18n || {};

$(function () {
    SetWork.init();
});

var SetWork = {
    ajax: null,
    count: 0,
    $modal: $('#modal_set_work'),
    $form: $('#modal_set_work').find('form'),

    init: function () {
        $('#start_set_work').on('click', SetWork.start);
        $('#stop_set_work').on('click', SetWork.stop);
    },

    start: function () {
        SetWork.$modal.find('.sender-percent').text('0%');
        SetWork.$modal.find('.progress-bar').css('width', '0%');

        SetWork.$modal.find('.row-with-btn-start').hide();
        SetWork.$modal.find('.row-with-text-start').hide();
        SetWork.$modal.find('.row-with-btn-stop').show();
        SetWork.$modal.find('.row-with-text-stop').show();

        SetWork.count = SetWork.$form.find('input[name="id[]"]').length;

        if (SetWork.count) {
            SetWork.next();
        } else {
            $.notify({message: I18n['Необходимо выбрать сотрудников.']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            SetWork.stop();
        }
    },

    next: function () {
        var $id = SetWork.$form.find('input[name="id[]"]:first');

        if ($id.length) {
            var percent = SetWork.calculatePercent();

            SetWork.$modal.find('.sender-percent').text(percent + '%');
            SetWork.$modal.find('.progress-bar').css('width', percent + '%');

            $id.remove();

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var work = $("#set_work_select option:selected").text();

            var dataAjax = {
                id: $id.val(),
                work: $('#set_work_select').val()
            };

            dataAjax[csrfParam] = csrfToken;

            var $checkbox = $('#persons_content input[name="id[]"][value="' + $id.val() + '"]');
            $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
            $checkbox.closest('tr').removeClass('text-danger');

            SetWork.ajax = $.ajax({
                type: 'POST',
                url: SetWork.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        $checkbox.closest('tr').find('.working_shift').text(work);
                        SetWork.next();
                    } else {
                        $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                        $checkbox.closest('tr').addClass('text-danger');
                        SetWork.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    SetWork.stop();
                    response = JSON.parse(response.responseText);

                    if (response.statusText != 'abort') {
                        if (typeof response.message != 'undefined') {
                            $.notify({message: response.message}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        } else {
                            $.notify({message: I18n['Не удалось установить рабочую смену']}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }
                    }
                }
            });
        } else {
            SetWork.ajax = null;
            SetWork.$modal.modal('hide');

            $.notify({message: 'Операция выполнена успешно'}, {
                type: "success dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
        }
    },

    stop: function () {
        if (SetWork.ajax != null) {
            SetWork.ajax.abort();
        }

        SetWork.$modal.find('.row-with-btn-start').show();
        SetWork.$modal.find('.row-with-text-start').show();
        SetWork.$modal.find('.row-with-btn-stop').hide();
        SetWork.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (SetWork.count) {
            var part = 100 / SetWork.count;
            var count = SetWork.count - SetWork.$form.find('input[name="id[]"]').length;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
