var I18n = I18n || {};

$(function () {
    DeletePenalties.init();
});

var DeletePenalties = {
    ajax: null,
    count: 0,
    $modal: $('#modal_delete_penalties'),
    $form: $('#modal_delete_penalties').find('form'),

    init: function () {
        $('#start_delete_penalties').on('click', DeletePenalties.start);
        $('#stop_delete_penalties').on('click', DeletePenalties.stop);
    },

    start: function () {
        DeletePenalties.$modal.find('.sender-percent').text('0%');
        DeletePenalties.$modal.find('.progress-bar').css('width', '0%');

        DeletePenalties.$modal.find('.row-with-btn-start').hide();
        DeletePenalties.$modal.find('.row-with-text-start').hide();
        DeletePenalties.$modal.find('.row-with-btn-stop').show();
        DeletePenalties.$modal.find('.row-with-text-stop').show();

        DeletePenalties.count = DeletePenalties.$form.find('input[name="id[]"]').length;

        if (DeletePenalties.count) {
            DeletePenalties.next();
        } else {
            $.notify({message: I18n['Необходимо выбрать штрафы']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            DeletePenalties.stop();
        }
    },

    next: function () {
        var $id = DeletePenalties.$form.find('input[name="id[]"]:first');

        if ($id.length) {
            var percent = DeletePenalties.calculatePercent();

            DeletePenalties.$modal.find('.sender-percent').text(percent + '%');
            DeletePenalties.$modal.find('.progress-bar').css('width', percent + '%');

            $id.remove();

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {
                id: $id.val(),
                date: $('#penalties_dismissal_date').val()
            };

            dataAjax[csrfParam] = csrfToken;

            var $checkbox = $('#penalties_content input[name="id[]"][value="' + $id.val() + '"]');
            $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
            $checkbox.closest('tr').removeClass('text-danger');

            DeletePenalties.ajax = $.ajax({
                type: 'POST',
                url: DeletePenalties.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        $checkbox.closest('tr').remove();
                        DeletePenalties.next();
                    } else {
                        $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                        $checkbox.closest('tr').addClass('text-danger');
                        DeletePenalties.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    DeletePenalties.stop();
                    response = JSON.parse(response.responseText);

                    if (response.statusText != 'abort') {
                        if (typeof response.message != 'undefined') {
                            $.notify({message: response.message}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        } else {
                            $.notify({message: I18n['Не удалось удалить штрафы']}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }
                    }
                }
            });
        } else {
            DeletePenalties.ajax = null;
            DeletePenalties.$modal.modal('hide');

            $.notify({message: 'Операция выполнена успешно'}, {
                type: "success dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
        }
    },

    stop: function () {
        if (DeletePenalties.ajax != null) {
            DeletePenalties.ajax.abort();
        }

        DeletePenalties.$modal.find('.row-with-btn-start').show();
        DeletePenalties.$modal.find('.row-with-text-start').show();
        DeletePenalties.$modal.find('.row-with-btn-stop').hide();
        DeletePenalties.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (DeletePenalties.count) {
            var part = 100 / DeletePenalties.count;
            var count = DeletePenalties.count - DeletePenalties.$form.find('input[name="id[]"]').length;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
