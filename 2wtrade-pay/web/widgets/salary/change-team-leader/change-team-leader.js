var I18n = I18n || {};

$(function () {
    ChangeTeamLeader.init();
});

var ChangeTeamLeader = {
    ajax: null,
    count: 0,
    $modal: $('#modal_change_team_leader'),
    $form: $('#modal_change_team_leader').find('form'),

    init: function () {
        $('#start_change_team_leader').on('click', ChangeTeamLeader.start);
        $('#stop_change_team_leader').on('click', ChangeTeamLeader.stop);
    },

    start: function () {
        ChangeTeamLeader.$modal.find('.sender-percent').text('0%');
        ChangeTeamLeader.$modal.find('.progress-bar').css('width', '0%');

        ChangeTeamLeader.$modal.find('.row-with-btn-start').hide();
        ChangeTeamLeader.$modal.find('.row-with-text-start').hide();
        ChangeTeamLeader.$modal.find('.row-with-btn-stop').show();
        ChangeTeamLeader.$modal.find('.row-with-text-stop').show();

        ChangeTeamLeader.count = ChangeTeamLeader.$form.find('input[name="id[]"]').length;

        if (ChangeTeamLeader.count) {
            ChangeTeamLeader.next();
        } else {
            $.notify({message: I18n['Необходимо выбрать пользователей.']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            ChangeTeamLeader.stop();
        }
    },

    next: function () {
        var $id = ChangeTeamLeader.$form.find('input[name="id[]"]:first');

        if ($id.length) {
            var percent = ChangeTeamLeader.calculatePercent();

            ChangeTeamLeader.$modal.find('.sender-percent').text(percent + '%');
            ChangeTeamLeader.$modal.find('.progress-bar').css('width', percent + '%');

            $id.remove();

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var parent = $("#change_team_leader_select option:selected").text();

            var dataAjax = {
                id: $id.val(),
                teamLeader: $('#change_team_leader_select').val()
            };

            dataAjax[csrfParam] = csrfToken;

            var $checkbox = $('#persons_content input[name="id[]"][value="' + $id.val() + '"]');
            $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
            $checkbox.closest('tr').removeClass('text-danger');

            ChangeTeamLeader.ajax = $.ajax({
                type: 'POST',
                url: ChangeTeamLeader.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        $checkbox.closest('tr').find('.parent').text(parent);
                        ChangeTeamLeader.next();
                    } else {
                        $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                        $checkbox.closest('tr').addClass('text-danger');
                        ChangeTeamLeader.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    ChangeTeamLeader.stop();
                    response = JSON.parse(response.responseText);

                    if (response.statusText != 'abort') {
                        if (typeof response.message != 'undefined') {
                            $.notify({message: response.message}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        } else {
                            $.notify({message: I18n['Не удалось сменить руководителя']}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }
                    }
                }
            });
        } else {
            ChangeTeamLeader.ajax = null;
            ChangeTeamLeader.$modal.modal('hide');

            $.notify({message: 'Операция выполнена успешно'}, {
                type: "success dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
        }
    },

    stop: function () {
        if (ChangeTeamLeader.ajax != null) {
            ChangeTeamLeader.ajax.abort();
        }

        ChangeTeamLeader.$modal.find('.row-with-btn-start').show();
        ChangeTeamLeader.$modal.find('.row-with-text-start').show();
        ChangeTeamLeader.$modal.find('.row-with-btn-stop').hide();
        ChangeTeamLeader.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (ChangeTeamLeader.count) {
            var part = 100 / ChangeTeamLeader.count;
            var count = ChangeTeamLeader.count - ChangeTeamLeader.$form.find('input[name="id[]"]').length;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
