var I18n = I18n || {};

$(function () {
    ApprovePersons.init();
});

var ApprovePersons = {
    ajax: null,
    count: 0,
    $modal: $('#modal_approve_persons'),
    $form: $('#modal_approve_persons').find('form'),

    init: function () {
        $('#start_approve_persons').on('click', ApprovePersons.start);
        $('#stop_approve_persons').on('click', ApprovePersons.stop);
    },

    start: function () {
        ApprovePersons.$modal.find('.sender-percent').text('0%');
        ApprovePersons.$modal.find('.progress-bar').css('width', '0%');

        ApprovePersons.$modal.find('.row-with-btn-start').hide();
        ApprovePersons.$modal.find('.row-with-text-start').hide();
        ApprovePersons.$modal.find('.row-with-btn-stop').show();
        ApprovePersons.$modal.find('.row-with-text-stop').show();

        ApprovePersons.count = ApprovePersons.$form.find('input[name="id[]"]').length;

        if (ApprovePersons.count) {
            ApprovePersons.next();
        } else {
            $.notify({message: I18n['Необходимо выбрать сотрудников.']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            ApprovePersons.stop();
        }
    },

    next: function () {
        var $id = ApprovePersons.$form.find('input[name="id[]"]:first');

        if ($id.length) {
            var percent = ApprovePersons.calculatePercent();

            ApprovePersons.$modal.find('.sender-percent').text(percent + '%');
            ApprovePersons.$modal.find('.progress-bar').css('width', percent + '%');

            $id.remove();

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {
                id: $id.val()
            };

            dataAjax[csrfParam] = csrfToken;

            var $checkbox = $('#persons_content input[name="id[]"][value="' + $id.val() + '"]');
            $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
            $checkbox.closest('tr').removeClass('text-danger');

            ApprovePersons.ajax = $.ajax({
                type: 'POST',
                url: ApprovePersons.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        $checkbox.closest('tr').removeClass('text-muted');
                        var s = $checkbox.closest('tr').find('.approve-status span');
                        s.removeClass('label-default').addClass('label-success').text(I18n['Да']);
                        ApprovePersons.next();
                    } else {
                        $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                        $checkbox.closest('tr').addClass('text-danger');
                        ApprovePersons.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    ApprovePersons.stop();
                    response = JSON.parse(response.responseText);

                    if (response.statusText != 'abort') {
                        if (typeof response.message != 'undefined') {
                            $.notify({message: response.message}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        } else {
                            $.notify({message: I18n['Не удалось подтвердить сотрудников']}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }
                    }
                }
            });
        } else {
            ApprovePersons.ajax = null;
            ApprovePersons.$modal.modal('hide');

            $.notify({message: 'Операция выполнена успешно'}, {
                type: "success dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
        }
    },

    stop: function () {
        if (ApprovePersons.ajax != null) {
            ApprovePersons.ajax.abort();
        }

        ApprovePersons.$modal.find('.row-with-btn-start').show();
        ApprovePersons.$modal.find('.row-with-text-start').show();
        ApprovePersons.$modal.find('.row-with-btn-stop').hide();
        ApprovePersons.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (ApprovePersons.count) {
            var part = 100 / ApprovePersons.count;
            var count = ApprovePersons.count - ApprovePersons.$form.find('input[name="id[]"]').length;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
