var I18n = I18n || {};

$(function () {
    SetBonus.init();
});

var SetBonus = {
    ajax: null,
    count: 0,
    $modal: $('#modal_set_bonus'),
    $form: $('#modal_set_bonus').find('form'),

    init: function () {
        $('#start_set_bonus').on('click', SetBonus.start);
        $('#stop_set_bonus').on('click', SetBonus.stop);
        $(document).on('change', '#set_bonus_type', SetBonus.toggleLimitType);
    },

    toggleLimitType: function () {
        var v = $('#set_bonus_type').val();
        $(".limit").hide();
        if (v) {
            var type = bonusTypes[v];
            if (type) {
                $(".limit_" + type.type).show();
                if (type.type == 'sum' || type.type == 'sum_work_hours') {
                    $('#set_bonus_sum_usd').val(eval(type.sum));
                    $('#set_bonus_sum_local').val(eval(type.sum_local));
                }
                if (type.type == 'percent') {
                    $(".limit_percent").find('input').val(eval(type.percent));
                }
                if (type.type == 'percent_calc_salary') {
                    $(".limit_percent").find('input').val(eval(type.percent));
                }
                if (type.type == 'percent_work_hours') {
                    $(".limit_percent_work_hours").find('input').val(eval(type.percent));
                }
                if (type.type == 'hour') {
                    $(".limit_hour").find('input').val('');
                }
                if (type.type == 'other') {
                    $(".limit_other").find('input').val('').prop("readonly", false);
                }
            }
        }
    },

    start: function () {
        SetBonus.$modal.find('.sender-percent').text('0%');
        SetBonus.$modal.find('.progress-bar').css('width', '0%');

        SetBonus.$modal.find('.row-with-btn-start').hide();
        SetBonus.$modal.find('.row-with-text-start').hide();
        SetBonus.$modal.find('.row-with-btn-stop').show();
        SetBonus.$modal.find('.row-with-text-stop').show();

        SetBonus.count = SetBonus.$form.find('input[name="id[]"]').length;

        if (SetBonus.count) {
            SetBonus.next();
        } else {
            $.notify({message: I18n['Необходимо выбрать сотрудников.']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            SetBonus.stop();
        }
    },

    next: function () {
        var $id = SetBonus.$form.find('input[name="id[]"]:first');

        if ($id.length) {

            if (!$('#set_bonus_type').val()) {
                $.notify({message: I18n['Не удалось назначить бонус']}, {
                    type: "danger dark",
                    animate: {exit: 'hide'},
                    z_index: 2031
                });
                SetBonus.stop();
            }

            var percent = SetBonus.calculatePercent();

            SetBonus.$modal.find('.sender-percent').text(percent + '%');
            SetBonus.$modal.find('.progress-bar').css('width', percent + '%');

            $id.remove();

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {
                id: $id.val(),
                type: $('#set_bonus_type').val(),
                date: $('#set_bonus_date').val(),
                percent: $('#set_bonus_percent').val(),
                sum_usd: $('#set_bonus_sum_usd').val(),
                sum_local: $('#set_bonus_sum_local').val(),
                hour: $('#set_bonus_hour').val(),
                percent_hour: $('#set_bonus_percent_hour').val(),
                comment: $('#set_bonus_comment').val()
            };

            dataAjax[csrfParam] = csrfToken;

            var $checkbox = $('#persons_content input[name="id[]"][value="' + $id.val() + '"]');
            $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
            $checkbox.closest('tr').removeClass('text-danger');

            SetBonus.ajax = $.ajax({
                type: 'POST',
                url: SetBonus.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        $checkbox.closest('tr').remove();
                        SetBonus.next();
                    } else {
                        $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                        $checkbox.closest('tr').addClass('text-danger');
                        SetBonus.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    SetBonus.stop();
                    response = JSON.parse(response.responseText);

                    if (response.statusText != 'abort') {
                        if (typeof response.message != 'undefined') {
                            $.notify({message: response.message}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        } else {
                            $.notify({message: I18n['Не удалось назначить бонус']}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }
                    }
                }
            });
        } else {
            SetBonus.ajax = null;
            SetBonus.$modal.modal('hide');

            $.notify({message: 'Операция выполнена успешно'}, {
                type: "success dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
        }
    },

    stop: function () {
        if (SetBonus.ajax != null) {
            SetBonus.ajax.abort();
        }

        SetBonus.$modal.find('.row-with-btn-start').show();
        SetBonus.$modal.find('.row-with-text-start').show();
        SetBonus.$modal.find('.row-with-btn-stop').hide();
        SetBonus.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (SetBonus.count) {
            var part = 100 / SetBonus.count;
            var count = SetBonus.count - SetBonus.$form.find('input[name="id[]"]').length;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
