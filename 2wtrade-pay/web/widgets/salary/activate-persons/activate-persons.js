var I18n = I18n || {};

$(function () {
    ActivatePersons.init();
});

var ActivatePersons = {
    ajax: null,
    count: 0,
    $modal: $('#modal_activate_persons'),
    $form: $('#modal_activate_persons').find('form'),

    init: function () {
        $('#start_activate_persons').on('click', ActivatePersons.start);
        $('#stop_activate_persons').on('click', ActivatePersons.stop);
    },

    start: function () {
        ActivatePersons.$modal.find('.sender-percent').text('0%');
        ActivatePersons.$modal.find('.progress-bar').css('width', '0%');

        ActivatePersons.$modal.find('.row-with-btn-start').hide();
        ActivatePersons.$modal.find('.row-with-text-start').hide();
        ActivatePersons.$modal.find('.row-with-btn-stop').show();
        ActivatePersons.$modal.find('.row-with-text-stop').show();

        ActivatePersons.count = ActivatePersons.$form.find('input[name="id[]"]').length;

        if (ActivatePersons.count) {
            ActivatePersons.next();
        } else {
            $.notify({message: I18n['Необходимо выбрать сотрудников.']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            ActivatePersons.stop();
        }
    },

    next: function () {
        var $id = ActivatePersons.$form.find('input[name="id[]"]:first');

        if ($id.length) {
            var percent = ActivatePersons.calculatePercent();

            ActivatePersons.$modal.find('.sender-percent').text(percent + '%');
            ActivatePersons.$modal.find('.progress-bar').css('width', percent + '%');

            $id.remove();

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {
                id: $id.val()
            };

            dataAjax[csrfParam] = csrfToken;

            var $checkbox = $('#persons_content input[name="id[]"][value="' + $id.val() + '"]');
            $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
            $checkbox.closest('tr').removeClass('text-danger');

            ActivatePersons.ajax = $.ajax({
                type: 'POST',
                url: ActivatePersons.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        $checkbox.closest('tr').removeClass('text-muted');
                        var s = $checkbox.closest('tr').find('.active-status span');
                        s.removeClass('label-default').addClass('label-success').text(I18n['Да']);
                        ActivatePersons.next();
                    } else {
                        $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                        $checkbox.closest('tr').addClass('text-danger');
                        ActivatePersons.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    ActivatePersons.stop();
                    response = JSON.parse(response.responseText);

                    if (response.statusText != 'abort') {
                        if (typeof response.message != 'undefined') {
                            $.notify({message: response.message}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        } else {
                            $.notify({message: I18n['Не удалось активировать сотрудников']}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }
                    }
                }
            });
        } else {
            ActivatePersons.ajax = null;
            ActivatePersons.$modal.modal('hide');

            $.notify({message: 'Операция выполнена успешно'}, {
                type: "success dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
        }
    },

    stop: function () {
        if (ActivatePersons.ajax != null) {
            ActivatePersons.ajax.abort();
        }

        ActivatePersons.$modal.find('.row-with-btn-start').show();
        ActivatePersons.$modal.find('.row-with-text-start').show();
        ActivatePersons.$modal.find('.row-with-btn-stop').hide();
        ActivatePersons.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (ActivatePersons.count) {
            var part = 100 / ActivatePersons.count;
            var count = ActivatePersons.count - ActivatePersons.$form.find('input[name="id[]"]').length;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
