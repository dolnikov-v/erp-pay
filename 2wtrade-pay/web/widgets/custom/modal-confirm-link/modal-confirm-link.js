$(function () {
    $(document).on('click', '.confirm-data-link', ModalConfirmLink.confirmLink);
});

var ModalConfirmLink = {
    confirmLink: function () {
        $('#modal_confirm_data_link').attr('href', $(this).data('href'));
        $('#modal_confirm_data').modal();
        return false;
    }
};
