$(function () {
    $('#autotrash-autotrash_status').on('change', function () {
        var csrfParam = $('meta[name=csrf-param]').attr("content");
        var csrfToken = $('meta[name=csrf-token]').attr("content");

        var dataAjax = {
            stateAutotrash: $(this).is(':checked') ? 1 : 0
        };

        dataAjax[csrfParam] = csrfToken;

        $.ajax({
            type: 'POST',
            url: '/catalog/autotrash/autotrash-switch',
            dataType: 'json',
            data: dataAjax,
            success: function (response, textStatus) {
                $.notify({message: response.message}, {
                    type: "success dark",
                    animate: {exit: 'hide'},
                    z_index: 2031
                });
            },
            error: function () {
                $.notify({message: response.message}, {
                    type: "danger dark",
                    animate: {exit: 'hide'},
                    z_index: 2031
                });
            }
        });
    });
});
