$(function () {
    $('input[data-widget="product-switchery"]').on('change', function () {
        var csrfParam = $('meta[name=csrf-param]').attr("content");
        var csrfToken = $('meta[name=csrf-token]').attr("content");
        var dataAjax = {
            model_id: $(this).closest('table').data('model-id'),
            product_id: $(this).closest('td').data('product-id'),
            value: $(this).is(':checked') ? 'on' : 'off'
        };
        var controller = $(this).closest('table').data('controller');
        var url = controller + 'set-product';
        console.log(url);
        dataAjax[csrfParam] = csrfToken;

        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            data: dataAjax,
            success: function () {
                $.notify({message: 'Действие выполнено.'}, {type: "success dark", animate: {exit: 'hide'}, z_index: 2031});
            },
            error: function (response) {
                $.notify({message: 'Не удалось выполнить запрос. ' + response.responseText}, {type: "danger dark", animate: {exit: 'hide'}, z_index: 2031});
            }
        });
    });
});
