var I18n = I18n || {};

$(function () {
    ActivateUsers.init();
});

var ActivateUsers = {
    ajax: null,
    count: 0,
    $modal: $('#modal_activate_users'),
    $form: $('#modal_activate_users').find('form'),

    init: function () {
        $('#start_activate_users').on('click', ActivateUsers.start);
        $('#stop_activate_users').on('click', ActivateUsers.stop);
    },

    start: function () {
        ActivateUsers.$modal.find('.sender-percent').text('0%');
        ActivateUsers.$modal.find('.progress-bar').css('width', '0%');

        ActivateUsers.$modal.find('.row-with-btn-start').hide();
        ActivateUsers.$modal.find('.row-with-text-start').hide();
        ActivateUsers.$modal.find('.row-with-btn-stop').show();
        ActivateUsers.$modal.find('.row-with-text-stop').show();

        ActivateUsers.count = ActivateUsers.$form.find('input[name="id[]"]').length;

        if (ActivateUsers.count) {
            ActivateUsers.next();
        } else {
            $.notify({message: I18n['Необходимо выбрать пользователей.']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            ActivateUsers.stop();
        }
    },

    next: function () {
        var $id = ActivateUsers.$form.find('input[name="id[]"]:first');

        if ($id.length) {
            var percent = ActivateUsers.calculatePercent();

            ActivateUsers.$modal.find('.sender-percent').text(percent + '%');
            ActivateUsers.$modal.find('.progress-bar').css('width', percent + '%');

            $id.remove();

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {
                id: $id.val()
            };

            dataAjax[csrfParam] = csrfToken;

            var $checkbox = $('#users_content input[name="id[]"][value="' + $id.val() + '"]');
            $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
            $checkbox.closest('tr').removeClass('text-danger');

            ActivateUsers.ajax = $.ajax({
                type: 'POST',
                url: ActivateUsers.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        $checkbox.closest('tr').remove();
                        ActivateUsers.next();
                    } else {
                        $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                        $checkbox.closest('tr').addClass('text-danger');
                        ActivateUsers.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    ActivateUsers.stop();
                    response = JSON.parse(response.responseText);

                    if (response.statusText != 'abort') {
                        if (typeof response.message != 'undefined') {
                            $.notify({message: response.message}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        } else {
                            $.notify({message: I18n['Не удалось активировать пользователей']}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }
                    }
                }
            });
        } else {
            ActivateUsers.ajax = null;
            ActivateUsers.$modal.modal('hide');

            $.notify({message: 'Операция выполнена успешно'}, {
                type: "success dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
        }
    },

    stop: function () {
        if (ActivateUsers.ajax != null) {
            ActivateUsers.ajax.abort();
        }

        ActivateUsers.$modal.find('.row-with-btn-start').show();
        ActivateUsers.$modal.find('.row-with-text-start').show();
        ActivateUsers.$modal.find('.row-with-btn-stop').hide();
        ActivateUsers.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (ActivateUsers.count) {
            var part = 100 / ActivateUsers.count;
            var count = ActivateUsers.count - ActivateUsers.$form.find('input[name="id[]"]').length;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
