var I18n = I18n || {};

$(function () {
    DeleteUsers.init();
});

var DeleteUsers = {
    ajax: null,
    count: 0,
    $modal: $('#modal_delete_users'),
    $form: $('#modal_delete_users').find('form'),

    init: function () {
        $('#start_delete_users').on('click', DeleteUsers.start);
        $('#stop_delete_users').on('click', DeleteUsers.stop);
    },

    start: function () {
        DeleteUsers.$modal.find('.sender-percent').text('0%');
        DeleteUsers.$modal.find('.progress-bar').css('width', '0%');

        DeleteUsers.$modal.find('.row-with-btn-start').hide();
        DeleteUsers.$modal.find('.row-with-text-start').hide();
        DeleteUsers.$modal.find('.row-with-btn-stop').show();
        DeleteUsers.$modal.find('.row-with-text-stop').show();

        DeleteUsers.count = DeleteUsers.$form.find('input[name="id[]"]').length;

        if (DeleteUsers.count) {
            DeleteUsers.next();
        } else {
            $.notify({message: I18n['Необходимо выбрать пользователей.']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            DeleteUsers.stop();
        }
    },

    next: function () {
        var $id = DeleteUsers.$form.find('input[name="id[]"]:first');

        if ($id.length) {
            var percent = DeleteUsers.calculatePercent();

            DeleteUsers.$modal.find('.sender-percent').text(percent + '%');
            DeleteUsers.$modal.find('.progress-bar').css('width', percent + '%');

            $id.remove();

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {
                id: $id.val()
            };

            dataAjax[csrfParam] = csrfToken;

            var $checkbox = $('#users_content input[name="id[]"][value="' + $id.val() + '"]');
            $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
            $checkbox.closest('tr').removeClass('text-danger');

            DeleteUsers.ajax = $.ajax({
                type: 'POST',
                url: DeleteUsers.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        $checkbox.closest('tr').remove();
                        DeleteUsers.next();
                    } else {
                        $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                        $checkbox.closest('tr').addClass('text-danger');
                        DeleteUsers.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    DeleteUsers.stop();
                    response = JSON.parse(response.responseText);

                    if (response.statusText != 'abort') {
                        if (typeof response.message != 'undefined') {
                            $.notify({message: response.message}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        } else {
                            $.notify({message: I18n['Не удалось удалить пользователей']}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }
                    }
                }
            });
        } else {
            DeleteUsers.ajax = null;
            DeleteUsers.$modal.modal('hide');

            $.notify({message: 'Операция выполнена успешно'}, {
                type: "success dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
        }
    },

    stop: function () {
        if (DeleteUsers.ajax != null) {
            DeleteUsers.ajax.abort();
        }

        DeleteUsers.$modal.find('.row-with-btn-start').show();
        DeleteUsers.$modal.find('.row-with-text-start').show();
        DeleteUsers.$modal.find('.row-with-btn-stop').hide();
        DeleteUsers.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (DeleteUsers.count) {
            var part = 100 / DeleteUsers.count;
            var count = DeleteUsers.count - DeleteUsers.$form.find('input[name="id[]"]').length;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
