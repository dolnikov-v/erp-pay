var I18n = I18n || {};

$(function () {
    DeactivateUsers.init();
});

var DeactivateUsers = {
    ajax: null,
    count: 0,
    $modal: $('#modal_deactivate_users'),
    $form: $('#modal_deactivate_users').find('form'),

    init: function () {
        $('#start_deactivate_users').on('click', DeactivateUsers.start);
        $('#stop_deactivate_users').on('click', DeactivateUsers.stop);
    },

    start: function () {
        DeactivateUsers.$modal.find('.sender-percent').text('0%');
        DeactivateUsers.$modal.find('.progress-bar').css('width', '0%');

        DeactivateUsers.$modal.find('.row-with-btn-start').hide();
        DeactivateUsers.$modal.find('.row-with-text-start').hide();
        DeactivateUsers.$modal.find('.row-with-btn-stop').show();
        DeactivateUsers.$modal.find('.row-with-text-stop').show();

        DeactivateUsers.count = DeactivateUsers.$form.find('input[name="id[]"]').length;

        if (DeactivateUsers.count) {
            DeactivateUsers.next();
        } else {
            $.notify({message: I18n['Необходимо выбрать пользователей.']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            DeactivateUsers.stop();
        }
    },

    next: function () {
        var $id = DeactivateUsers.$form.find('input[name="id[]"]:first');

        if ($id.length) {
            var percent = DeactivateUsers.calculatePercent();

            DeactivateUsers.$modal.find('.sender-percent').text(percent + '%');
            DeactivateUsers.$modal.find('.progress-bar').css('width', percent + '%');

            $id.remove();

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {
                id: $id.val()
            };

            dataAjax[csrfParam] = csrfToken;

            var $checkbox = $('#users_content input[name="id[]"][value="' + $id.val() + '"]');
            $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
            $checkbox.closest('tr').removeClass('text-danger');

            DeactivateUsers.ajax = $.ajax({
                type: 'POST',
                url: DeactivateUsers.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        $checkbox.closest('tr').remove();
                        DeactivateUsers.next();
                    } else {
                        $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                        $checkbox.closest('tr').addClass('text-danger');
                        DeactivateUsers.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    DeactivateUsers.stop();
                    response = JSON.parse(response.responseText);

                    if (response.statusText != 'abort') {
                        if (typeof response.message != 'undefined') {
                            $.notify({message: response.message}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        } else {
                            $.notify({message: I18n['Не удалось деактивировать пользователей']}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }
                    }
                }
            });
        } else {
            DeactivateUsers.ajax = null;
            DeactivateUsers.$modal.modal('hide');

            $.notify({message: 'Операция выполнена успешно'}, {
                type: "success dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
        }
    },

    stop: function () {
        if (DeactivateUsers.ajax != null) {
            DeactivateUsers.ajax.abort();
        }

        DeactivateUsers.$modal.find('.row-with-btn-start').show();
        DeactivateUsers.$modal.find('.row-with-text-start').show();
        DeactivateUsers.$modal.find('.row-with-btn-stop').hide();
        DeactivateUsers.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (DeactivateUsers.count) {
            var part = 100 / DeactivateUsers.count;
            var count = DeactivateUsers.count - DeactivateUsers.$form.find('input[name="id[]"]').length;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
