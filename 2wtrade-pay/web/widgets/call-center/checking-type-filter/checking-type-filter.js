$(function () {
    CheckingTypeWidget.init();
});

var CheckingTypeWidget = {
    currentTypeValue: $('#checking_type_select').val(),
    init: function () {
        $('#checking_type_select').on("change", CheckingTypeWidget.changeType);
        console.log(CheckingTypeWidget.currentTypeValue);
    },
    changeType: function () {
        var $oldSelect = $('.checking-type-information-select.checking-type-information-select-' + CheckingTypeWidget.currentTypeValue);
        var $newSelect = $('.checking-type-information-select.checking-type-information-select-' + $(this).val());
        $oldSelect.addClass('hidden');
        $newSelect.removeClass('hidden');
        $newSelect.find('select').attr('name', $oldSelect.find('select').attr('name'));
        $oldSelect.find('select').attr('name', '');
        CheckingTypeWidget.currentTypeValue = $(this).val();
    }
};
