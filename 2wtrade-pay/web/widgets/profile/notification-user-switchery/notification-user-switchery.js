$(function () {
    $('input[data-widget="notification-user-switchery"]').on('change', function () {
        var dataAjax = {
            trigger: $(this).closest('td').data('notification'),
            value: $(this).is(':checked') ? 'on' : 'off'
        };

        var success = $(this).closest('table').data('i18n-success');
        var fail = $(this).closest('table').data('i18n-fail');
        
        $.ajax({
            type: 'POST',
            url: '/profile/view/set-notification',
            dataType: 'json',
            data: dataAjax,
            success: function () {
                $.notify({message: success}, {type: "success dark", animate: {exit: 'hide'}, z_index: 2031});
            },
            error: function () {
                $.notify({message: fail}, {type: "danger dark", animate: {exit: 'hide'}, z_index: 2031});
            }
        });
    });
});
