$(function () {
    $(document).on('click', '.tr-notification .open .dropdown-menu li a', function () {
        var dataAjax = {
            interval: $(this).attr('href'),
            trigger: $(this).parents('tr').data('name')
        };

        var success = $(this).closest('table').data('i18n-success');
        var fail = $(this).closest('table').data('i18n-fail');

        $.ajax({
            type: 'POST',
            url: $(this).parents('.mail-interval').data('interval'),
            dataType: 'json',
            data: dataAjax,
            success: function () {
                $.notify({message: success}, {type: "success dark", animate: {exit: 'hide'}, z_index: 2031});
            },
            error: function () {
                $.notify({message: fail}, {type: "danger dark", animate: {exit: 'hide'}, z_index: 2031});
            }
        });

        var caret = '<span class="caret"></span>';

        $(this).parents('.dropdown-menu').siblings('button').html($(this).text() + caret);
        $(this).parents('.open').removeClass('open');

        return false;
    });
});
