$(function () {
    Cropper.init();
});

var Cropper = {
    options: {},
    cropperId: $('#cropper_image'),
    modalId: $('#modal_edit_profile_avatar'),
    init: function () {
        Cropper.initCropper();

        $("#upload_media").on('change', function (e) {
            Cropper.loadFile(e);
        });

        $(document).on('click', "#crop-data-save", function () {
            Cropper.saveCropData();
        });

        $(document).on('click', "#crop-data-cancel", function () {
            Cropper.modalId.modal('hide');
        });
    },
    initCropper: function () {
        var $previews = $('.preview');

        Cropper.cropperId.cropper({
            aspectRatio: Cropper.options.aspectRatio,
            zoomOnWheel: false,
            build: function () {
                var $clone = $(this).clone();

                $clone.css({
                    display: 'block',
                    width: '100%',
                    minWidth: 0,
                    minHeight: 0,
                    maxWidth: 'none',
                    maxHeight: 'none'
                });

                $previews.css({
                    width: '100%',
                    overflow: 'hidden'
                }).html($clone);
            },
            crop: function (e) {
                var imageData = $(this).cropper('getImageData');
                var previewAspectRatio = e.width / e.height;

                $previews.each(function () {
                    var $preview = $(this);
                    var previewWidth = $preview.width();
                    var previewHeight = previewWidth / previewAspectRatio;
                    var imageScaledRatio = e.width / previewWidth;

                    $preview.height(previewHeight).find('img').css({
                        width: imageData.naturalWidth / imageScaledRatio,
                        height: imageData.naturalHeight / imageScaledRatio,
                        marginLeft: -e.x / imageScaledRatio,
                        marginTop: -e.y / imageScaledRatio
                    });
                });
            }
        });

        Cropper.cropperId.on('built.cropper', Cropper.setCropData);
    },
    setCropData: function () {
        if (Cropper.options.cropData != "") {
            Cropper.cropperId.cropper('setData', JSON.parse(Cropper.options.cropData));
        }
    },
    saveCropData: function () {
        var urlCropSave = $('.cropper-actions').data('crop-save');

        var cropData = {
            cropData: Cropper.cropperId.cropper('getData'),
            currentFileName: Cropper.options.currentFileName,
            cropType: Cropper.options.cropType,
            linkId: Cropper.options.linkId
        };

        $.ajax({
            url: urlCropSave,
            method: "POST",
            data: cropData,
            cache: false,
            success: function (data) {
                if (data.status == "success") {
                    $.notify({message: Cropper.options.successNotify}, {
                        type: "success dark",
                        animate: {exit: 'hide'},
                        z_index: 2031
                    });
                    Cropper.options.currentFileName = data.answer.fileName;
                    $('.avatar img').attr('src', data.answer.pathFile);
                    Cropper.modalId.modal('hide');
                } else {
                    $.notify({message: Cropper.options.failNotify}, {
                        type: "danger dark",
                        animate: {exit: 'hide'},
                        z_index: 2031
                    });
                }
            },
        });
    },
    loadFile: function (e) {
        var urlUpload = $('.cropper-actions').data('upload-url');

        var formData = new FormData();
        var files = e.target.files;

        $.each(files, function (key, value) {
            formData.append(Cropper.options.nameInput, value);
        });

        $.ajax({
            url: urlUpload,
            type: "POST",
            data: formData,
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                Cropper.cropperId.cropper('replace', data['pathFile']);
                $('.preview img').removeClass('cropper-hidden');
                Cropper.options.currentFileName = data['file'];
            },
        });

        e.preventDefault();
    }
};

