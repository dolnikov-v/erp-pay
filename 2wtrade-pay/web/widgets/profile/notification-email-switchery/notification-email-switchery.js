$(function () {
    $('input[data-widget="notification-email-switchery"]').on('change', function () {
        var dataAjax = {
            trigger: $(this).closest('td').data('notification'),
            interval: $(this).is(':checked') ? 'always' : ''
        };

        var success = $(this).closest('table').data('i18n-success');
        var fail = $(this).closest('table').data('i18n-fail');
        
        $.ajax({
            type: 'POST',
            url: '/profile/view/set-mail-interval',
            dataType: 'json',
            data: dataAjax,
            success: function () {
                $.notify({message: success}, {type: "success dark", animate: {exit: 'hide'}, z_index: 2031});
            },
            error: function () {
                $.notify({message: fail}, {type: "danger dark", animate: {exit: 'hide'}, z_index: 2031});
            }
        });
    });
});
