var I18n = I18n || {};

$(function () {
    SenderInDelivery.init();
});

var SenderInDelivery = {
    ajax: null,
    count: 0,
    $modal: $('#modal_sender_in_delivery'),
    $modal: $('#modal_sender_in_delivery'),
    $form: $('#modal_sender_in_delivery').find('form'),
    lastResponseMessage: '',

    init: function () {
        $('#start_sender_in_delivery').on('click', SenderInDelivery.start);
        $('#stop_sender_in_delivery').on('click', SenderInDelivery.stop);
    },

    start: function () {
        SenderInDelivery.$modal.find('.sender-percent').text('0%');
        SenderInDelivery.$modal.find('.progress-bar').css('width', '0%');

        SenderInDelivery.$modal.find('.row-with-btn-start').hide();
        SenderInDelivery.$modal.find('.row-with-text-start').hide();
        SenderInDelivery.$modal.find('.row-with-btn-stop').show();
        SenderInDelivery.$modal.find('.row-with-text-stop').show();

        SenderInDelivery.count = SenderInDelivery.$form.find('input[name="id[]"]').length;

        if (SenderInDelivery.count) {
            SenderInDelivery.next();
        } else {
            $.notify({message: I18n['Необходимо выбрать заявки']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            SenderInDelivery.stop();
        }
    },

    next: function () {
        var $id = SenderInDelivery.$form.find('input[name="id[]"]:first');
        if ($id.length) {
            var percent = SenderInDelivery.calculatePercent();

            SenderInDelivery.$modal.find('.sender-percent').text(percent + '%');
            SenderInDelivery.$modal.find('.progress-bar').css('width', percent + '%');

            $id.remove();

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {
                id: $id.val(),
                delivery: $('#sender_in_delivery_select').val()
            };

            dataAjax[csrfParam] = csrfToken;

            var $checkbox = $('#packager-request-grid input[name="id[]"][value="' + $id.val() + '"]');
            $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
            $checkbox.closest('tr').removeClass('text-danger');

            SenderInDelivery.ajax = $.ajax({
                type: 'POST',
                url: SenderInDelivery.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        $checkbox.closest('tr').remove();
                        SenderInDelivery.lastResponseMessage = response.message;
                        SenderInDelivery.next();
                    } else {
                        $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                        $checkbox.closest('tr').addClass('text-danger');
                        SenderInDelivery.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    SenderInDelivery.stop();
                    response = JSON.parse(response.responseText);

                    if (response.statusText != 'abort') {
                        if (typeof response.message != 'undefined') {
                            $.notify({message: response.message}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        } else {
                            $.notify({message: I18n['Не удалось выполнить операцию']}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }
                    }
                }
            });
        } else {
            SenderInDelivery.ajax = null;
            SenderInDelivery.$modal.modal('hide');

            $.notify({message: SenderInDelivery.lastResponseMessage}, {
                type: "success dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
        }
    },

    stop: function () {
        if (SenderInDelivery.ajax != null) {
            SenderInDelivery.ajax.abort();
        }

        SenderInDelivery.$modal.find('.row-with-btn-start').show();
        SenderInDelivery.$modal.find('.row-with-text-start').show();
        SenderInDelivery.$modal.find('.row-with-btn-stop').hide();
        SenderInDelivery.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (SenderInDelivery.count) {
            var part = 100 / SenderInDelivery.count;
            var count = SenderInDelivery.count - SenderInDelivery.$form.find('input[name="id[]"]').length;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
