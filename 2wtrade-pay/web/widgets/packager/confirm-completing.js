var I18n = I18n || {};

$(function () {
    ConfirmCompleting.init();
});

var ConfirmCompleting = {
    ajax: null,
    count: 0,
    $modal: $('#modal_confirm_completing'),
    $modal: $('#modal_confirm_completing'),
    $form: $('#modal_confirm_completing').find('form'),
    lastResponseMessage: '',

    init: function () {
        $('#start_confirm_completing').on('click', ConfirmCompleting.start);
        $('#stop_confirm_completing').on('click', ConfirmCompleting.stop);
    },

    start: function () {
        ConfirmCompleting.$modal.find('.sender-percent').text('0%');
        ConfirmCompleting.$modal.find('.progress-bar').css('width', '0%');

        ConfirmCompleting.$modal.find('.row-with-btn-start').hide();
        ConfirmCompleting.$modal.find('.row-with-text-start').hide();
        ConfirmCompleting.$modal.find('.row-with-btn-stop').show();
        ConfirmCompleting.$modal.find('.row-with-text-stop').show();

        ConfirmCompleting.count = ConfirmCompleting.$form.find('input[name="id[]"]').length;

        if (ConfirmCompleting.count) {
            ConfirmCompleting.next();
        } else {
            $.notify({message: I18n['Необходимо выбрать заявки']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            ConfirmCompleting.stop();
        }
    },

    next: function () {
        var $id = ConfirmCompleting.$form.find('input[name="id[]"]:first');

        if ($id.length) {
            var percent = ConfirmCompleting.calculatePercent();

            ConfirmCompleting.$modal.find('.sender-percent').text(percent + '%');
            ConfirmCompleting.$modal.find('.progress-bar').css('width', percent + '%');

            $id.remove();

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {
                id: $id.val()
            };

            dataAjax[csrfParam] = csrfToken;

            var $checkbox = $('#packager-request-grid input[name="id[]"][value="' + $id.val() + '"]');
            $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
            $checkbox.closest('tr').removeClass('text-danger');

            ConfirmCompleting.ajax = $.ajax({
                type: 'POST',
                url: ConfirmCompleting.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        $checkbox.closest('tr').remove();
                        ConfirmCompleting.lastResponseMessage = response.message;
                        ConfirmCompleting.next();
                    } else {
                        $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                        $checkbox.closest('tr').addClass('text-danger');
                        ConfirmCompleting.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    ConfirmCompleting.stop();
                    response = JSON.parse(response.responseText);

                    if (response.statusText != 'abort') {
                        if (typeof response.message != 'undefined') {
                            $.notify({message: response.message}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        } else {
                            $.notify({message: I18n['Не удалось выполнить операцию']}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }
                    }
                }
            });
        } else {
            ConfirmCompleting.ajax = null;
            ConfirmCompleting.$modal.modal('hide');

            $.notify({message: ConfirmCompleting.lastResponseMessage}, {
                type: "success dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
        }
    },

    stop: function () {
        if (ConfirmCompleting.ajax != null) {
            ConfirmCompleting.ajax.abort();
        }

        ConfirmCompleting.$modal.find('.row-with-btn-start').show();
        ConfirmCompleting.$modal.find('.row-with-text-start').show();
        ConfirmCompleting.$modal.find('.row-with-btn-stop').hide();
        ConfirmCompleting.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (ConfirmCompleting.count) {
            var part = 100 / ConfirmCompleting.count;
            var count = ConfirmCompleting.count - ConfirmCompleting.$form.find('input[name="id[]"]').length;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
