var I18n = I18n || {};

$(function () {
    SetApprove.init();
});

var SetApprove = {
    ajax: null,
    count: 0,
    $modal: $('#modal_set_approve'),
    $modal: $('#modal_set_approve'),
    $form: $('#modal_set_approve').find('form'),
    lastResponseMessage: '',

    init: function () {
        $('#start_set_approve').on('click', SetApprove.start);
        $('#stop_set_approve').on('click', SetApprove.stop);
    },

    start: function () {
        SetApprove.$modal.find('.sender-percent').text('0%');
        SetApprove.$modal.find('.progress-bar').css('width', '0%');

        SetApprove.$modal.find('.row-with-btn-start').hide();
        SetApprove.$modal.find('.row-with-text-start').hide();
        SetApprove.$modal.find('.row-with-btn-stop').show();
        SetApprove.$modal.find('.row-with-text-stop').show();

        SetApprove.count = SetApprove.$form.find('input[name="id[]"]').length;

        if (SetApprove.count) {
            SetApprove.next();
        } else {
            $.notify({message: I18n['Необходимо выбрать заявки']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            SetApprove.stop();
        }
    },

    next: function () {
        var $id = SetApprove.$form.find('input[name="id[]"]:first');
        if ($id.length) {
            var percent = SetApprove.calculatePercent();

            SetApprove.$modal.find('.sender-percent').text(percent + '%');
            SetApprove.$modal.find('.progress-bar').css('width', percent + '%');

            $id.remove();

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {
                id: $id.val()
            };

            dataAjax[csrfParam] = csrfToken;

            var $checkbox = $('#packager-request-grid input[name="id[]"][value="' + $id.val() + '"]');
            $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
            $checkbox.closest('tr').removeClass('text-danger');

            SetApprove.ajax = $.ajax({
                type: 'POST',
                url: SetApprove.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        $checkbox.closest('tr').remove();
                        SetApprove.lastResponseMessage = response.message;
                        SetApprove.next();
                    } else {
                        $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                        $checkbox.closest('tr').addClass('text-danger');
                        SetApprove.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    SetApprove.stop();
                    response = JSON.parse(response.responseText);

                    if (response.statusText != 'abort') {
                        if (typeof response.message != 'undefined') {
                            $.notify({message: response.message}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        } else {
                            $.notify({message: I18n['Не удалось выполнить операцию']}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }
                    }
                }
            });
        } else {
            SetApprove.ajax = null;
            SetApprove.$modal.modal('hide');
            SetApprove.stop();
            $.notify({message: SetApprove.lastResponseMessage}, {
                type: "success dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
        }
    },

    stop: function () {
        if (SetApprove.ajax != null) {
            SetApprove.ajax.abort();
        }

        SetApprove.$modal.find('.row-with-btn-start').show();
        SetApprove.$modal.find('.row-with-text-start').show();
        SetApprove.$modal.find('.row-with-btn-stop').hide();
        SetApprove.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (SetApprove.count) {
            var part = 100 / SetApprove.count;
            var count = SetApprove.count - SetApprove.$form.find('input[name="id[]"]').length;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
