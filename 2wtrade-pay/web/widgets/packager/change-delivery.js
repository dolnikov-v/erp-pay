var I18n = I18n || {};

$(function () {
    ChangeDelivery.init();
});

var ChangeDelivery = {
    ajax: null,
    count: 0,
    $modal: $('#modal_change_delivery'),
    $modal: $('#modal_change_delivery'),
    $form: $('#modal_change_delivery').find('form'),
    lastResponseMessage: '',

    init: function () {
        $('#start_change_delivery').on('click', ChangeDelivery.start);
        $('#stop_change_delivery').on('click', ChangeDelivery.stop);
    },

    start: function () {
        ChangeDelivery.$modal.find('.sender-percent').text('0%');
        ChangeDelivery.$modal.find('.progress-bar').css('width', '0%');

        ChangeDelivery.$modal.find('.row-with-btn-start').hide();
        ChangeDelivery.$modal.find('.row-with-text-start').hide();
        ChangeDelivery.$modal.find('.row-with-btn-stop').show();
        ChangeDelivery.$modal.find('.row-with-text-stop').show();

        ChangeDelivery.count = ChangeDelivery.$form.find('input[name="id[]"]').length;

        if (ChangeDelivery.count) {
            ChangeDelivery.next();
        } else {
            $.notify({message: I18n['Необходимо выбрать заявки']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            ChangeDelivery.stop();
        }
    },

    next: function () {
        var $id = ChangeDelivery.$form.find('input[name="id[]"]:first');
        if ($id.length) {
            var percent = ChangeDelivery.calculatePercent();

            ChangeDelivery.$modal.find('.sender-percent').text(percent + '%');
            ChangeDelivery.$modal.find('.progress-bar').css('width', percent + '%');

            $id.remove();

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {
                id: $id.val(),
                delivery: $('#change_delivery_select').val()
            };

            dataAjax[csrfParam] = csrfToken;

            var $checkbox = $('#packager-request-grid input[name="id[]"][value="' + $id.val() + '"]');
            $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
            $checkbox.closest('tr').removeClass('text-danger');

            ChangeDelivery.ajax = $.ajax({
                type: 'POST',
                url: ChangeDelivery.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        $checkbox.closest('tr').remove();
                        ChangeDelivery.lastResponseMessage = response.message;
                        ChangeDelivery.next();
                    } else {
                        $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                        $checkbox.closest('tr').addClass('text-danger');
                        ChangeDelivery.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    ChangeDelivery.stop();
                    response = JSON.parse(response.responseText);

                    if (response.statusText != 'abort') {
                        if (typeof response.message != 'undefined') {
                            $.notify({message: response.message}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        } else {
                            $.notify({message: I18n['Не удалось выполнить операцию']}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }
                    }
                }
            });
        } else {
            ChangeDelivery.ajax = null;
            ChangeDelivery.$modal.modal('hide');

            $.notify({message: ChangeDelivery.lastResponseMessage}, {
                type: "success dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
        }
    },

    stop: function () {
        if (ChangeDelivery.ajax != null) {
            ChangeDelivery.ajax.abort();
        }

        ChangeDelivery.$modal.find('.row-with-btn-start').show();
        ChangeDelivery.$modal.find('.row-with-text-start').show();
        ChangeDelivery.$modal.find('.row-with-btn-stop').hide();
        ChangeDelivery.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (ChangeDelivery.count) {
            var part = 100 / ChangeDelivery.count;
            var count = ChangeDelivery.count - ChangeDelivery.$form.find('input[name="id[]"]').length;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
