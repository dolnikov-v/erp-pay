var I18n = I18n || {};

$(function () {
    RefusalCompleting.init();
});

var RefusalCompleting = {
    ajax: null,
    count: 0,
    $modal: $('#modal_refusal_completing'),
    $modal: $('#modal_refusal_completing'),
    $form: $('#modal_refusal_completing').find('form'),
    lastResponseMessage: '',

    init: function () {
        $('#start_refusal_completing').on('click', RefusalCompleting.start);
        $('#stop_refusal_completing').on('click', RefusalCompleting.stop);
    },

    start: function () {
        RefusalCompleting.$modal.find('.sender-percent').text('0%');
        RefusalCompleting.$modal.find('.progress-bar').css('width', '0%');

        RefusalCompleting.$modal.find('.row-with-btn-start').hide();
        RefusalCompleting.$modal.find('.row-with-text-start').hide();
        RefusalCompleting.$modal.find('.row-with-btn-stop').show();
        RefusalCompleting.$modal.find('.row-with-text-stop').show();

        RefusalCompleting.count = RefusalCompleting.$form.find('input[name="id[]"]').length;

        if (RefusalCompleting.count) {
            RefusalCompleting.next();
        } else {
            $.notify({message: I18n['Необходимо выбрать заявки']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            RefusalCompleting.stop();
        }
    },

    next: function () {
        var $id = RefusalCompleting.$form.find('input[name="id[]"]:first');

        if ($('#reason').val() == '') {
            $.notify({message: I18n['Введите причину отказа']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            RefusalCompleting.stop();
        }
        else if ($id.length) {
            var percent = RefusalCompleting.calculatePercent();

            RefusalCompleting.$modal.find('.sender-percent').text(percent + '%');
            RefusalCompleting.$modal.find('.progress-bar').css('width', percent + '%');

            $id.remove();

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {
                id: $id.val(),
                reason: $('#reason').val()
            };

            dataAjax[csrfParam] = csrfToken;

            var $checkbox = $('#packager-request-grid input[name="id[]"][value="' + $id.val() + '"]');
            $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
            $checkbox.closest('tr').removeClass('text-danger');

            RefusalCompleting.ajax = $.ajax({
                type: 'POST',
                url: RefusalCompleting.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        $checkbox.closest('tr').remove();
                        RefusalCompleting.lastResponseMessage = response.message;
                        RefusalCompleting.next();
                    } else {
                        $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                        $checkbox.closest('tr').addClass('text-danger');
                        RefusalCompleting.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    RefusalCompleting.stop();
                    response = JSON.parse(response.responseText);

                    if (response.statusText != 'abort') {
                        if (typeof response.message != 'undefined') {
                            $.notify({message: response.message}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        } else {
                            $.notify({message: I18n['Не удалось выполнить операцию']}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }
                    }
                }
            });
        } else {
            RefusalCompleting.ajax = null;
            RefusalCompleting.$modal.modal('hide');

            $.notify({message: RefusalCompleting.lastResponseMessage}, {
                type: "success dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
        }
    },

    stop: function () {
        if (RefusalCompleting.ajax != null) {
            RefusalCompleting.ajax.abort();
        }

        RefusalCompleting.$modal.find('.row-with-btn-start').show();
        RefusalCompleting.$modal.find('.row-with-text-start').show();
        RefusalCompleting.$modal.find('.row-with-btn-stop').hide();
        RefusalCompleting.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (RefusalCompleting.count) {
            var part = 100 / RefusalCompleting.count;
            var count = RefusalCompleting.count - RefusalCompleting.$form.find('input[name="id[]"]').length;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
