var I18n = I18n || {};

$(function () {
    CancelShipment.init();
});

var CancelShipment = {
    ajax: null,
    count: 0,
    $modal: $('#modal_cancel_shipment'),
    $modal: $('#modal_cancel_shipment'),
    $form: $('#modal_cancel_shipment').find('form'),
    lastResponseMessage: '',

    init: function () {
        $('#start_cancel_shipment').on('click', CancelShipment.start);
        $('#stop_cancel_shipment').on('click', CancelShipment.stop);
    },

    start: function () {
        CancelShipment.$modal.find('.sender-percent').text('0%');
        CancelShipment.$modal.find('.progress-bar').css('width', '0%');

        CancelShipment.$modal.find('.row-with-btn-start').hide();
        CancelShipment.$modal.find('.row-with-text-start').hide();
        CancelShipment.$modal.find('.row-with-btn-stop').show();
        CancelShipment.$modal.find('.row-with-text-stop').show();

        CancelShipment.count = CancelShipment.$form.find('input[name="id[]"]').length;

        if (CancelShipment.count) {
            CancelShipment.next();
        } else {
            $.notify({message: I18n['Необходимо выбрать заявки']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            CancelShipment.stop();
        }
    },

    next: function () {
        var $id = CancelShipment.$form.find('input[name="id[]"]:first');

        if ($id.length) {
            var percent = CancelShipment.calculatePercent();

            CancelShipment.$modal.find('.sender-percent').text(percent + '%');
            CancelShipment.$modal.find('.progress-bar').css('width', percent + '%');

            $id.remove();

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {
                id: $id.val()
            };

            dataAjax[csrfParam] = csrfToken;

            var $checkbox = $('#packager-request-grid input[name="id[]"][value="' + $id.val() + '"]');
            $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
            $checkbox.closest('tr').removeClass('text-danger');

            CancelShipment.ajax = $.ajax({
                type: 'POST',
                url: CancelShipment.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        $checkbox.closest('tr').remove();
                        CancelShipment.lastResponseMessage = response.message;
                        CancelShipment.next();
                    } else {
                        $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                        $checkbox.closest('tr').addClass('text-danger');
                        CancelShipment.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    CancelShipment.stop();
                    response = JSON.parse(response.responseText);

                    if (response.statusText != 'abort') {
                        if (typeof response.message != 'undefined') {
                            $.notify({message: response.message}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        } else {
                            $.notify({message: I18n['Не удалось выполнить операцию']}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }
                    }
                }
            });
        } else {
            CancelShipment.ajax = null;
            CancelShipment.$modal.modal('hide');

            $.notify({message: CancelShipment.lastResponseMessage}, {
                type: "success dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
        }
    },

    stop: function () {
        if (CancelShipment.ajax != null) {
            CancelShipment.ajax.abort();
        }

        CancelShipment.$modal.find('.row-with-btn-start').show();
        CancelShipment.$modal.find('.row-with-text-start').show();
        CancelShipment.$modal.find('.row-with-btn-stop').hide();
        CancelShipment.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (CancelShipment.count) {
            var part = 100 / CancelShipment.count;
            var count = CancelShipment.count - CancelShipment.$form.find('input[name="id[]"]').length;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
