$(function () {
    LabelPrinting.init();
});

var LabelPrinting = {
    count: 0,
    $modal: $('#modal_label_printing'),
    $form: $('#modal_label_printing').find('form'),

    init: function () {
        $('#start_label_printing').on('click', LabelPrinting.start);
        $('#stop_label_printing').on('click', LabelPrinting.stop);
    },

    start: function () {
        LabelPrinting.count = LabelPrinting.$form.find('input[name="id[]"]').length;

        if (LabelPrinting.count == 0 && $('#all_label_printing_warning').css('display') == 'none') {
            $('#all_label_printing_warning').show();
            return false;
        }

        LabelPrinting.$modal.find('.sender-percent').text('0%');
        LabelPrinting.$modal.find('.progress-bar').css('width', '0%');

        LabelPrinting.$modal.find('.row-with-btn-start').hide();
        LabelPrinting.$modal.find('.row-with-text-start').hide();
        LabelPrinting.$modal.find('.row-with-btn-stop').show();
        LabelPrinting.$modal.find('.row-with-text-stop').show();

        LabelPrinting.all();
    },

    all: function () {
        LabelPrinting.stop();
        LabelPrinting.$form.submit();
    },

    stop: function () {

        LabelPrinting.$modal.find('.row-with-btn-start').show();
        LabelPrinting.$modal.find('.row-with-text-start').show();
        LabelPrinting.$modal.find('.row-with-btn-stop').hide();
        LabelPrinting.$modal.find('.row-with-text-stop').hide();
    }
};
