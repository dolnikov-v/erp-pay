$(function () {
    ListGeneration.init();
});

var ListGeneration = {
    count: 0,
    $modal: $('#modal_list_generation'),
    $form: $('#modal_list_generation').find('form'),
    lastResponseMessage: '',

    init: function () {
        $('#start_list_generation').on('click', ListGeneration.start);
        $('#stop_list_generation').on('click', ListGeneration.stop);
    },

    start: function () {
        ListGeneration.$modal.find('.sender-percent').text('0%');
        ListGeneration.$modal.find('.progress-bar').css('width', '0%');

        ListGeneration.$modal.find('.row-with-btn-start').hide();
        ListGeneration.$modal.find('.row-with-text-start').hide();
        ListGeneration.$modal.find('.row-with-btn-stop').show();
        ListGeneration.$modal.find('.row-with-text-stop').show();

        ListGeneration.count = ListGeneration.$form.find('input[name="id[]"]').length;

        if (ListGeneration.count) {
            ListGeneration.all();
        } else {
            $.notify({message: I18n['Необходимо выбрать заявки']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            ConfirmCompleting.stop();
        }
    },

    all: function () {
        ListGeneration.stop();
        ListGeneration.$form.submit();
    },

    stop: function () {

        ListGeneration.$modal.find('.row-with-btn-start').show();
        ListGeneration.$modal.find('.row-with-text-start').show();
        ListGeneration.$modal.find('.row-with-btn-stop').hide();
        ListGeneration.$modal.find('.row-with-text-stop').hide();
    }
};
