$(function () {
    GetManifest.init();
});

var GetManifest = {
    count: 0,
    $modal: $('#modal_get_manifest'),
    $form: $('#modal_get_manifest').find('form'),

    init: function () {
        $('#start_get_manifest').on('click', GetManifest.start);
        $('#stop_get_manifest').on('click', GetManifest.stop);
    },

    start: function () {
        GetManifest.count = GetManifest.$form.find('input[name="id[]"]').length;

        GetManifest.$modal.find('.sender-percent').text('0%');
        GetManifest.$modal.find('.progress-bar').css('width', '0%');

        GetManifest.$modal.find('.row-with-btn-start').hide();
        GetManifest.$modal.find('.row-with-text-start').hide();
        GetManifest.$modal.find('.row-with-btn-stop').show();
        GetManifest.$modal.find('.row-with-text-stop').show();

        GetManifest.all();
    },

    all: function () {
        GetManifest.stop();
        GetManifest.$form.submit();
    },

    stop: function () {

        GetManifest.$modal.find('.row-with-btn-start').show();
        GetManifest.$modal.find('.row-with-text-start').show();
        GetManifest.$modal.find('.row-with-btn-stop').hide();
        GetManifest.$modal.find('.row-with-text-stop').hide();
    }
};
