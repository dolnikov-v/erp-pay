var I18n = I18n || {};

$(function () {
    SetReturn.init();
});

var SetReturn = {
    ajax: null,
    count: 0,
    $modal: $('#modal_set_return'),
    $modal: $('#modal_set_return'),
    $form: $('#modal_set_return').find('form'),
    lastResponseMessage: '',

    init: function () {
        $('#start_set_return').on('click', SetReturn.start);
        $('#stop_set_return').on('click', SetReturn.stop);
    },

    start: function () {
        SetReturn.$modal.find('.sender-percent').text('0%');
        SetReturn.$modal.find('.progress-bar').css('width', '0%');

        SetReturn.$modal.find('.row-with-btn-start').hide();
        SetReturn.$modal.find('.row-with-text-start').hide();
        SetReturn.$modal.find('.row-with-btn-stop').show();
        SetReturn.$modal.find('.row-with-text-stop').show();

        SetReturn.count = SetReturn.$form.find('input[name="id[]"]').length;

        if (SetReturn.count) {
            SetReturn.next();
        } else {
            $.notify({message: I18n['Необходимо выбрать заявки']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            SetReturn.stop();
        }
    },

    next: function () {
        var $id = SetReturn.$form.find('input[name="id[]"]:first');
        if ($id.length) {
            var percent = SetReturn.calculatePercent();

            SetReturn.$modal.find('.sender-percent').text(percent + '%');
            SetReturn.$modal.find('.progress-bar').css('width', percent + '%');

            $id.remove();

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {
                id: $id.val()
            };

            dataAjax[csrfParam] = csrfToken;

            var $checkbox = $('#packager-request-grid input[name="id[]"][value="' + $id.val() + '"]');
            $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
            $checkbox.closest('tr').removeClass('text-danger');

            SetReturn.ajax = $.ajax({
                type: 'POST',
                url: SetReturn.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        $checkbox.closest('tr').remove();
                        SetReturn.lastResponseMessage = response.message;
                        SetReturn.next();
                    } else {
                        $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                        $checkbox.closest('tr').addClass('text-danger');
                        SetReturn.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    SetReturn.stop();
                    response = JSON.parse(response.responseText);

                    if (response.statusText != 'abort') {
                        if (typeof response.message != 'undefined') {
                            $.notify({message: response.message}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        } else {
                            $.notify({message: I18n['Не удалось выполнить операцию']}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }
                    }
                }
            });
        } else {
            SetReturn.ajax = null;
            SetReturn.$modal.modal('hide');

            $.notify({message: SetReturn.lastResponseMessage}, {
                type: "success dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
        }
    },

    stop: function () {
        if (SetReturn.ajax != null) {
            SetReturn.ajax.abort();
        }

        SetReturn.$modal.find('.row-with-btn-start').show();
        SetReturn.$modal.find('.row-with-text-start').show();
        SetReturn.$modal.find('.row-with-btn-stop').hide();
        SetReturn.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (SetReturn.count) {
            var part = 100 / SetReturn.count;
            var count = SetReturn.count - SetReturn.$form.find('input[name="id[]"]').length;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
