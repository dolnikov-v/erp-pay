var I18n = I18n || {};

$(function () {
    SetShipped.init();
});

var SetShipped = {
    ajax: null,
    count: 0,
    $modal: $('#modal_set_shipped'),
    $modal: $('#modal_set_shipped'),
    $form: $('#modal_set_shipped').find('form'),
    lastResponseMessage: '',

    init: function () {
        $('#start_set_shipped').on('click', SetShipped.start);
        $('#stop_set_shipped').on('click', SetShipped.stop);
    },

    start: function () {
        SetShipped.$modal.find('.sender-percent').text('0%');
        SetShipped.$modal.find('.progress-bar').css('width', '0%');

        SetShipped.$modal.find('.row-with-btn-start').hide();
        SetShipped.$modal.find('.row-with-text-start').hide();
        SetShipped.$modal.find('.row-with-btn-stop').show();
        SetShipped.$modal.find('.row-with-text-stop').show();

        SetShipped.count = SetShipped.$form.find('input[name="id[]"]').length;

        if (SetShipped.count) {
            SetShipped.next();
        } else {
            $.notify({message: I18n['Необходимо выбрать заявки']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            SetShipped.stop();
        }
    },

    next: function () {
        var $id = SetShipped.$form.find('input[name="id[]"]:first');

        if ($id.length) {
            var percent = SetShipped.calculatePercent();

            SetShipped.$modal.find('.sender-percent').text(percent + '%');
            SetShipped.$modal.find('.progress-bar').css('width', percent + '%');

            $id.remove();

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {
                id: $id.val()
            };

            dataAjax[csrfParam] = csrfToken;

            var $checkbox = $('#packager-request-grid input[name="id[]"][value="' + $id.val() + '"]');
            $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
            $checkbox.closest('tr').removeClass('text-danger');

            SetShipped.ajax = $.ajax({
                type: 'POST',
                url: SetShipped.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        $checkbox.closest('tr').remove();
                        SetShipped.lastResponseMessage = response.message;
                        SetShipped.next();
                    } else {
                        $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                        $checkbox.closest('tr').addClass('text-danger');
                        SetShipped.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    SetShipped.stop();
                    response = JSON.parse(response.responseText);

                    if (response.statusText != 'abort') {
                        if (typeof response.message != 'undefined') {
                            $.notify({message: response.message}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        } else {
                            $.notify({message: I18n['Не удалось выполнить операцию']}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }
                    }
                }
            });
        } else {
            SetShipped.ajax = null;
            SetShipped.$modal.modal('hide');

            $.notify({message: SetShipped.lastResponseMessage}, {
                type: "success dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
        }
    },

    stop: function () {
        if (SetShipped.ajax != null) {
            SetShipped.ajax.abort();
        }

        SetShipped.$modal.find('.row-with-btn-start').show();
        SetShipped.$modal.find('.row-with-text-start').show();
        SetShipped.$modal.find('.row-with-btn-stop').hide();
        SetShipped.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (SetShipped.count) {
            var part = 100 / SetShipped.count;
            var count = SetShipped.count - SetShipped.$form.find('input[name="id[]"]').length;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
