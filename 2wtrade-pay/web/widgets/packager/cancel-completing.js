var I18n = I18n || {};

$(function () {
    CancelCompleting.init();
});

var CancelCompleting = {
    ajax: null,
    count: 0,
    $modal: $('#modal_cancel_completing'),
    $modal: $('#modal_cancel_completing'),
    $form: $('#modal_cancel_completing').find('form'),
    lastResponseMessage: '',

    init: function () {
        $('#start_cancel_completing').on('click', CancelCompleting.start);
        $('#stop_cancel_completing').on('click', CancelCompleting.stop);
    },

    start: function () {
        CancelCompleting.$modal.find('.sender-percent').text('0%');
        CancelCompleting.$modal.find('.progress-bar').css('width', '0%');

        CancelCompleting.$modal.find('.row-with-btn-start').hide();
        CancelCompleting.$modal.find('.row-with-text-start').hide();
        CancelCompleting.$modal.find('.row-with-btn-stop').show();
        CancelCompleting.$modal.find('.row-with-text-stop').show();

        CancelCompleting.count = CancelCompleting.$form.find('input[name="id[]"]').length;

        if (CancelCompleting.count) {
            CancelCompleting.next();
        } else {
            $.notify({message: I18n['Необходимо выбрать заявки']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            CancelCompleting.stop();
        }
    },

    next: function () {
        var $id = CancelCompleting.$form.find('input[name="id[]"]:first');

        if ($id.length) {
            var percent = CancelCompleting.calculatePercent();

            CancelCompleting.$modal.find('.sender-percent').text(percent + '%');
            CancelCompleting.$modal.find('.progress-bar').css('width', percent + '%');

            $id.remove();

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {
                id: $id.val()
            };

            dataAjax[csrfParam] = csrfToken;

            var $checkbox = $('#packager-request-grid input[name="id[]"][value="' + $id.val() + '"]');
            $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
            $checkbox.closest('tr').removeClass('text-danger');

            CancelCompleting.ajax = $.ajax({
                type: 'POST',
                url: CancelCompleting.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        $checkbox.closest('tr').remove();
                        CancelCompleting.lastResponseMessage = response.message;
                        CancelCompleting.next();
                    } else {
                        $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                        $checkbox.closest('tr').addClass('text-danger');
                        CancelCompleting.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    CancelCompleting.stop();
                    response = JSON.parse(response.responseText);

                    if (response.statusText != 'abort') {
                        if (typeof response.message != 'undefined') {
                            $.notify({message: response.message}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        } else {
                            $.notify({message: I18n['Не удалось выполнить операцию']}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }
                    }
                }
            });
        } else {
            CancelCompleting.ajax = null;
            CancelCompleting.$modal.modal('hide');

            $.notify({message: CancelCompleting.lastResponseMessage}, {
                type: "success dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
        }
    },

    stop: function () {
        if (CancelCompleting.ajax != null) {
            CancelCompleting.ajax.abort();
        }

        CancelCompleting.$modal.find('.row-with-btn-start').show();
        CancelCompleting.$modal.find('.row-with-text-start').show();
        CancelCompleting.$modal.find('.row-with-btn-stop').hide();
        CancelCompleting.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (CancelCompleting.count) {
            var part = 100 / CancelCompleting.count;
            var count = CancelCompleting.count - CancelCompleting.$form.find('input[name="id[]"]').length;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
