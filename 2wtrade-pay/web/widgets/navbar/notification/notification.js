$(function () {
    Notification.init();
});

var Notification = {
    ajax: null,

    end: false,
    offset: 0,

    templateLoaderNotifications: Handlebars.compile($('#template_loader_notifications').html()),
    templateEmptyNotifications: Handlebars.compile($('#template_empty_notifications').html()),
    templateNotification: Handlebars.compile($('#template_notification').html()),

    urlGetNotifications: $('.notification-widget .show-notification-profile').data('url-get-notifications'),
    urlRead: $('.notification-widget .show-notification-profile').data('url-read'),

    init: function () {
        $('.dropdown.notification-widget').on('show.bs.dropdown', Notification.initDropdown);
        $('.notification-widget .list-group').on('click', '.widget-notification-item', Notification.read);
        $('.notification-widget .list-group .container-notify').on('scroll', Notification.scroll);
    },

    /**
     * Инициализация выпадающего списка
     */
    initDropdown: function () {
        Notification.end = false;
        Notification.offset = 0;

        Notification.download(true);
    },

    /**
     * Подгрузка уведолмений
     */
    download: function (first) {
        if (Notification.end || Notification.ajax != null) {
            return;
        }

        if (first == true) {
            $('.notification-widget .container-notify .content-notify').html(Notification.templateLoaderNotifications());
            Notification.reInitScrollable();
        } else {
            $('.notification-widget .container-notify .content-notify').append(Notification.templateLoaderNotifications());
            $('.notification-widget .loader-notifications').addClass('in-progress');
        }

        var scrollTop = $('.notification-widget .list-group .container-notify').scrollTop();

        var csrfParam = $('meta[name=csrf-param]').attr("content");
        var csrfToken = $('meta[name=csrf-token]').attr("content");

        var dataAjax = {
            offset: Notification.offset
        };

        dataAjax[csrfParam] = csrfToken;

        Notification.ajax = $.ajax({
            type: 'GET',
            url: Notification.urlGetNotifications,
            data: dataAjax,
            dataType: 'json',
            success: function (response) {
                Notification.ajax = null;

                Notification.end = response.end;

                var countUnread = parseInt(response.countUnread);

                $('.notification-widget .count-notifications').text(countUnread);
                $('.notification-widget .count-notifications-inner').text(countUnread);

                if (countUnread) {
                    $('.notification-widget .loader-notifications').remove();

                    $.each(response.notifications, function (index, obj) {
                        Notification.offset++;
                        var htmlNotification = Notification.templateNotification(obj);
                        $('.notification-widget .container-notify .content-notify').append(htmlNotification);
                    });

                    Notification.reInitScrollable(scrollTop);
                } else {
                    $('.notification-widget .count-notifications').text('');
                    $('.notification-widget .container-notify .content-notify').html(Notification.templateEmptyNotifications());
                }
            },
            error: function (response) {
                response = JSON.parse(response.responseText);
                MainBootstrap.addNotificationDanger(response.message);
            }
        });
    },

    /**
     * Отметить оповещение, как прочитанное
     */
    read: function () {
        $(this).slideUp(200, function() { $(this).remove(); });

        var csrfParam = $('meta[name=csrf-param]').attr("content");
        var csrfToken = $('meta[name=csrf-token]').attr("content");

        var dataAjax = {
            id: $(this).data('id')
        };

        dataAjax[csrfParam] = csrfToken;

        var scrollTop = $('.notification-widget .list-group .container-notify').scrollTop();

        $.ajax({
            type: 'POST',
            url: $(this).data('url'),
            dataType: 'json',
            data: dataAjax,
            success: function (response) {
                Notification.offset--;

                var countUnread = parseInt(response.countUnread);

                $('.notification-widget .count-notifications').text(countUnread);
                $('.notification-widget .count-notifications-inner').text(countUnread);

                if (countUnread) {
                    $('.notification-widget .loader-notifications').remove();

                    Notification.scroll();
                } else {
                    $('.notification-widget .count-notifications').text('');
                    $('.notification-widget .container-notify .content-notify').html(Notification.templateEmptyNotifications());
                }
            }
        });

        Notification.reInitScrollable(scrollTop);

        return false;
    },

    /**
     * Прокрутка оповещений
     */
    scroll: function () {
        var toTopBlock = $('.notification-widget .list-group .container-notify').scrollTop();
        var heightBlock = $('.notification-widget .list-group .content-notify').height();

        if (heightBlock - toTopBlock < 300) {
            Notification.download(false);
        }
    },

    /**
     * Новая инициализация Scrollable
     */
    reInitScrollable: function (scrollTop) {
        scrollTop = scrollTop || 0;

        setTimeout(function () {
            $('.notification-widget .scrollable-container').css('height', '');
            $('.notification-widget .list-group').asScrollable('update');

            $('.notification-widget .list-group .container-notify').scrollTop(scrollTop);
        }, 0);
    }
};
