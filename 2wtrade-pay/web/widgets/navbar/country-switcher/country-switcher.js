$(function () {
    $('#country_switcher_menu').on('click', NavbarCountrySwitcher.toggleCountries);
    $('#country_switcher_searcher_input').on('keydown', NavbarCountrySwitcher.searchCountry);
});

var NavbarCountrySwitcher = {
    toggleCountries: function () {
        var $input = $('#country_switcher_searcher_input');

        $input.val('');
        $input.trigger('keydown');

        setTimeout(function () {
            $input.focus();
        }, 0);
    },

    searchCountry: function (e) {
        setTimeout(function () {
            if (e.keyCode == 27) {
                $('#country_switcher_dropdown').trigger('click');
            } else {
                var search = $('#country_switcher_searcher_input').val().toLowerCase();
                var $menu = $('#country_switcher_dropdown_menu .list-group');

                if (search == '') {
                    $menu.find('.list-group-item').show();
                } else {
                    $menu.find('.list-group-item').each(function () {
                        var text = $(this).data('name').toLowerCase();

                        if (text.indexOf(search) == -1) {
                            $(this).hide();
                        } else {
                            $(this).show();
                        }
                    });
                }
            }
        }, 10);
    }
};
