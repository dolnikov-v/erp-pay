$(function () {
    Efficiency.init();
});

var Efficiency = {
    $modal: $('#efficiency_modal'),

    init: function () {
        $('#efficiency_detail').on('click', Efficiency.open);
        //Efficiency.$modal.on('hide.bs.modal', Efficiency.close);
        //Efficiency.$modal.find('.close').on('click', Efficiency.close);
    },

    open: function () {
        Efficiency.$modal.modal();
    },

    close: function () {
        //Efficiency.$modal.hide();
        Efficiency.$modal.modal('hide');
    }
};
