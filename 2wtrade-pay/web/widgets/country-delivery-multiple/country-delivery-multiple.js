$(function () {
    $('.container-countrydeliverymultiple .countrymultiple select').on('change', Delivery.change);
});

var Delivery = {
    change: function () {
        var id = $(this).attr('id');
        $('#' + id).closest('.container-countrydeliverymultiple').find('.deliverymultiple select').attr('disabled', true);
        $.ajax({
            type: 'POST',
            url: '/report/filter/get-select-options-delivery',
            data: {
                country_ids: $(this).closest('.container-countrydeliverymultiple').find('.countrymultiple select').val()
            },
            success: function (response) {
                var select = $('#' + id).closest('.container-countrydeliverymultiple').find('.deliverymultiple select');
                var options = select.data('select2').options.options;
                select.find('.select2-selection ul').empty();
                select.html(response);
                select.select2(options);
                select.removeAttr('disabled');

            },
            error: function () {
                var select = $('#' + id).closest('.container-countrydeliverymultiple').find('.deliverymultiple select');
                select.attr('disabled', '');
            }
        });
    }
};
