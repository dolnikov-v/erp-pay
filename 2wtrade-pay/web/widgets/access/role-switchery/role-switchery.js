$(function () {
    $('input[data-widget="role-switchery"]').on('change', function () {
        var csrfParam = $('meta[name=csrf-param]').attr("content");
        var csrfToken = $('meta[name=csrf-token]').attr("content");

        var dataAjax = {
            permission: $(this).closest('table').data('permission'),
            role: $(this).closest('td').data('role'),
            value: $(this).is(':checked') ? 'on' : 'off'
        };

        var success = $(this).closest('table').data('i18n-success');
        var fail = $(this).closest('table').data('i18n-fail');

        dataAjax[csrfParam] = csrfToken;

        $.ajax({
            type: 'POST',
            url: '/access/role/set-permission',
            dataType: 'json',
            data: dataAjax,
            success: function (response, textStatus) {

                $.notify({message: success}, {type: "success dark", animate: {exit: 'hide'}, z_index: 2031});
            },
            error: function () {
                $.notify({message: fail}, {type: "danger dark", animate: {exit: 'hide'}, z_index: 2031});
            }
        });
    });
});
