$(function () {
    $('input[data-widget="country-switchery"]').on('change', function () {
        var csrfParam = $('meta[name=csrf-param]').attr("content");
        var csrfToken = $('meta[name=csrf-token]').attr("content");

        var dataAjax = {
            user_id: $(this).closest('table').data('user-id'),
            country_id: $(this).closest('td').data('country-id'),
            value: $(this).is(':checked') ? 'on' : 'off'
        };

        dataAjax[csrfParam] = csrfToken;

        $.ajax({
            type: 'POST',
            url: '/access/user/set-country',
            dataType: 'json',
            data: dataAjax,
            success: function (response, textStatus) {
                $.notify({message: 'Действие выполнено.'}, {type: "success dark", animate: {exit: 'hide'}, z_index: 2031});
            },
            error: function () {
                $.notify({message: 'Не удалось выполнить запрос.'}, {type: "danger dark", animate: {exit: 'hide'}, z_index: 2031});
            }
        });
    });
});
