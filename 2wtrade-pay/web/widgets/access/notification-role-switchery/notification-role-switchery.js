$(function () {
    $('input[data-widget="notification-role-switchery"]').on('change', function () {

        var urlSetNotification = $(this).parents('table').data('url-set-notification');
        var dataAjax = {
            role: $(this).closest('table').data('role'),
            trigger: $(this).parent().siblings().data('trigger'),
            value: $(this).is(':checked') ? 'on' : 'off'
        };

        var success = $(this).closest('table').data('i18n-success');
        var fail = $(this).closest('table').data('i18n-fail');

        $.ajax({
            type: 'POST',
            url: urlSetNotification,
            dataType: 'json',
            data: dataAjax,
            success: function () {
                $.notify({message: success}, {type: "success dark", animate: {exit: 'hide'}, z_index: 2031});
            },
            error: function () {
                $.notify({message: fail}, {type: "danger dark", animate: {exit: 'hide'}, z_index: 2031});
            }
        });
    });
});
