Vars = Vars || {};

$(function() {
    AccessRole.init();
});

var AccessRole = {
    init: function() {
        if ($('.permission-filter-section').length) {
            $('.permission-filter-section').on('click', AccessRole.togglePermissions);
        }
    },

    togglePermissions: function() {
        var section = $(this).data('name');
        var $dropdown = $(this).closest('.dropdown');
        var $label = $dropdown.find('.dropdown-toggle-label');

        if (typeof Vars['sections'][section] != 'undefined') {
            if (section == '') {
                $label.text($dropdown.data('default-name'));
                $('#table_allow_permissions .tr-permission').show();
            } else {
                $label.text($(this).text());
                $('#table_allow_permissions .tr-permission').hide();

                for (var i = 0; i < Vars['sections'][section].length; i++) {
                    $('tr.tr-permission[data-name^="' + Vars['sections'][section][i] + '"]').show();
                }
            }
        } else {
            $label.text($dropdown.data('default-name'));
            $('#table_allow_permissions .tr-permission').show();
        }

        $dropdown.removeClass('open');

        return false;
    }
};
