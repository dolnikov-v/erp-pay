$(function () {
    $(document).on('click', '[data-widget="password-generator"]', PasswordGenerate.generate);
    $(document).on('click', '[data-widget="password-showing"]', PasswordGenerate.toggle);
});

var PasswordGenerate = {
    generate: function () {
        var chars = '23456789ABCDEFGHIJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz'.split('');
        var $input = $(this).closest('.form-group').find('input');

        var passwordLength = parseInt($(this).data('password-generator-length'));

        if (isNaN(passwordLength) || passwordLength == 0) {
            passwordLength = 8;
        }

        var password = '';
        var rand = 0;
        var timeout = Math.floor(1000 / passwordLength);

        var timeoutGenerator = function () {
            rand = Math.floor(Math.random() * (chars.length));
            password = password + chars[rand];

            $input.val(password);

            if (password.length < passwordLength) {
                setTimeout(timeoutGenerator, timeout);
            }
        };

        timeoutGenerator();
    },

    toggle: function () {
        var $input = $(this).closest('.form-group').find('input');
        var $icon = $(this).find('i.icon');

        if ($input.attr('type') == 'text') {
            $input.attr('type', 'password');
            $icon.removeClass('fa-eye-slash').addClass('fa-eye');
        } else {
            $input.attr('type', 'text');
            $icon.removeClass('fa-eye').addClass('fa-eye-slash');
        }
    }
};
