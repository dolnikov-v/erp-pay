$(function () {
    $('input[data-widget="api-user-delivery-switchery"]').on('change', function () {
        var success = $(this).closest('.list-group-block').data('i18n-success');
        var fail = $(this).closest('.list-group-block').data('i18n-fail');

        var csrfParam = $('meta[name=csrf-param]').attr("content");
        var csrfToken = $('meta[name=csrf-token]').attr("content");

        var dataAjax = {
            deliveryId: $(this).closest('li').data('delivery-id'),
            userId: $(this).closest('.list-group-block').data('user-id'),
        };

        dataAjax[csrfParam] = csrfToken;

        $.ajax({
            type: 'POST',
            url: '/access/api-user/change-delivery',
            dataType: 'json',
            data: dataAjax,
            success: function () {
                $.notify({message: success}, {type: "success dark", animate: {exit: 'hide'}, z_index: 2031});
            },
            error: function () {
                $.notify({message: fail}, {type: "danger dark", animate: {exit: 'hide'}, z_index: 2031});
            }
        });
    });
});
