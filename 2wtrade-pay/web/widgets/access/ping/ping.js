var userPingInterval = 60000;
var userPingTimer = null;

$(function () {
    userPingTimer = setInterval(userPing, userPingInterval);
});

function userPing() {
    var csrfParam = $('meta[name=csrf-param]').attr("content");
    var csrfToken = $('meta[name=csrf-token]').attr("content");

    var ajaxData = {};

    ajaxData[csrfParam] = csrfToken;

    $.ajax({
        type: 'POST',
        url: '/access/user/ping',
        dataType: 'json',
        data: ajaxData,
        success: function (response, textStatus) {

        },
        error: function (response, textStatus) {
            clearInterval(userPingTimer);
        }
    });
}
