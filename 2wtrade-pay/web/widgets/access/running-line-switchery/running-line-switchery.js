$(function () {
    $('input[data-widget="running-line-switchery"]').on('change', function () {
        var csrfParam = $('meta[name=csrf-param]').attr("content");
        var csrfToken = $('meta[name=csrf-token]').attr("content");

        var controller_name = $(this).closest('table').data('controller-name');
        var dataAjax = {
            cotroller_name: $(this).closest('table').data('controller-name'),
            user_id: $(this).closest('table').data('user'),
            role_name: $(this).closest('table').data('role'),
            notification_name: $(this).closest('td').data('notification-name'),
            value: $(this).is(':checked') ? 'on' : 'off'
        };

        dataAjax[csrfParam] = csrfToken;

        $.ajax({
            type: 'POST',
            url: '/access/' + controller_name + '/set-running-line-notification',
            dataType: 'json',
            data: dataAjax,
            success: function (response, textStatus) {
                $.notify({message: 'Действие выполнено.'}, {type: "success dark", animate: {exit: 'hide'}, z_index: 2031});
            },
            error: function () {
                $.notify({message: 'Не удалось выполнить запрос'}, {type: "danger dark", animate: {exit: 'hide'}, z_index: 2031});
            }
        });
    });
});
