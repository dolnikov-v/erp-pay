$(function () {
    $( "#vpn-checkbox" ).change(function () {
        if(this.checked) {
            $( "#vpn-form" ). show();
        } else {
            $( "#vpn-form" ). hide();
        }
    });

    $( "#jira-bind" ).click(function () {
        var email = $( "#user-email" ).val();
        var username = $("#user-username");

        var dataAjax = {
            email: email
        };
        $.ajax({
            type: 'GET',
            url: 'jira-bind',
            dataType: 'json',
            data: dataAjax,
            success: function (response) {
                username.val(response.name);
                $.notify({message: response.message}, {type: "success dark", animate: {exit: 'hide'}, z_index: 2031});
            },
            error: function (response) {
                $.notify({message: response.responseJSON.message}, {type: "danger dark", animate: {exit: 'hide'}, z_index: 2031});
            }
        });
    });
});