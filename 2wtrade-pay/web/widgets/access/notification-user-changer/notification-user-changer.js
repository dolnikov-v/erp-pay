$(function () {
    NotificationUserChanger.init();
});

var NotificationUserChanger = {
    user: null,
    urlSetNotification: null,
    i18nSuccess: null,
    i18nFail: null,

    /**
     * Инициализация
     */
    init: function () {
        var $table = $('#table_allow_notifications');

        NotificationUserChanger.user = $table.data('user');
        NotificationUserChanger.urlSetNotification = $table.data('url-set-notification');
        NotificationUserChanger.i18nSuccess = $table.data('i18n-success');
        NotificationUserChanger.i18nFail = $table.data('i18n-fail');

        $('.notification-user-changer').on('change', 'input', NotificationUserChanger.change);
    },

    /**
     * Смена значения
     */
    change: function () {

        var dataAjax = {
            user: NotificationUserChanger.user,
            trigger: $(this).attr('name'),
            value: $(this).attr('value')
        };

        var success = $(this).closest('table').data('i18n-success');
        var fail = $(this).closest('table').data('i18n-fail');

        $.ajax({
            type: 'POST',
            url: NotificationUserChanger.urlSetNotification,
            dataType: 'json',
            data: dataAjax,
            success: function () {
                $.notify({message: NotificationUserChanger.i18nSuccess}, {type: "success dark", animate: {exit: 'hide'}, z_index: 2031});
            },
            error: function () {
                $.notify({message: NotificationUserChanger.i18nFail}, {type: "danger dark", animate: {exit: 'hide'}, z_index: 2031});
            }
        });
    }
};
