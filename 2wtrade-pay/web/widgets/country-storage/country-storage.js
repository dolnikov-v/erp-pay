$(function () {
    $('.container-countrystorage .country').on('change', Storage.change);
});

var Storage = {
    change: function () {
        var $self = $(this);
        var $selfParent = $(this).parent();
        $.ajax({
            type: 'POST',
            url: '/storage/country-storage/get-select-options-storage',
            data: {
                country_id: $self.find('select').val()
            },
            success: function (response) {
                $selfParent.find('.storage select').empty().html(response);
                $selfParent.find('.storage select').removeAttr('disabled');
            },
            error: function () {
                $selfParent.find('.storage select').attr('disabled', '');

            }
        });

        $selfParent.find('.storage .selection span span').empty().html('—');
        $selfParent.find('.storage .selection span span').attr('title', '—');
    }
};