$(function () {
    ProductList.init();
});

var ProductList = {
    disableMode: 1,
    init: function () {
        ProductList.disableMode = $('.container-product-list').data('disable');
        $('.container-product-list .add-product-element').on('click', ProductList.addRow);
        $('.container-product-list').on('click', '.delete-product-element', ProductList.deleteRow);
    },
    addRow: function (e) {
        e.preventDefault();
        var $parent = $(this).parent();
        var $row = $parent.next().clone();
        var $option = $parent.find('option:selected');
        var productId = $option.val();
        $row.data('id', productId);
        $row.removeClass('hidden');

        $row.find('input.product_id').prop('name', 'products[]').val($option.val());
        $row.find('input.product_name').val($option.text());
        $row.find('input.values').prop('name', 'values[]');
        $row.find('input.limits').prop('name', 'limits[]');

        var dates = $row.find('input.dates');
        if (dates) {
            dates.prop('name', 'dates[]');
            dates.datetimepicker({
                format: dates.data('format'),
                locale: dates.data('locale')
            });
        }

        var dates_from = $row.find('input.dates_from');
        if (dates_from) {
            dates_from.prop('name', 'dates_from[]');
            dates_from.datetimepicker({
                format: dates_from.data('format'),
                locale: dates_from.data('locale')
            });
        }
        var dates_to = $row.find('input.dates_to');
        if (dates_to) {
            dates_to.prop('name', 'dates_to[]');
            dates_to.datetimepicker({
                format: dates_to.data('format'),
                locale: dates_to.data('locale')
            });
        }
        if (ProductList.disableMode) {
            $option.prop('disabled', true);
            $option.next().prop('selected', true);
        }
        $parent.find('select').select2();
        $parent.before($row);
    },
    deleteRow: function (e) {
        e.preventDefault();
        var $parent = $(this).parent();
        var $option = $('.container-product-list').find('option[value="' + $parent.data('id') + '"]');
        if (ProductList.disableMode) {
            $option.prop('disabled', false);
            $option.next().prop('selected', false);
        }
        $option.parent().select2();
        $parent.remove();
    }
};
