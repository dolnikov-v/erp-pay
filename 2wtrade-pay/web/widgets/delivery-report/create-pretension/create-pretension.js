var I18n = I18n || {};

$(function () {
    CreatePretension.init();
});

var CreatePretension = {
    ajax: null,
    count: 0,
    offsetCount: null,
    $modal: $('#modal_create_pretension'),
    $form: $('#modal_create_pretension').find('form'),
    init: function () {
        $('#start_create_pretension').on('click', CreatePretension.start);
        $('#stop_create_pretension').on('click', CreatePretension.stop);
    },
    show: function () {
        CreatePretension.$modal.modal();
        CreatePretension.count = CreatePretension.$form.find('input[name="id[]"]').length;
        if (CreatePretension.count) {
            CreatePretension.$modal.find('.action-record-count').hide();
        }
        else {
            CreatePretension.$modal.find('.action-record-count').show();
        }
    },
    start: function () {
        CreatePretension.$modal.find('.sender-percent').text('0%');
        CreatePretension.$modal.find('.progress-bar').css('width', '0%');

        CreatePretension.$modal.find('.row-with-btn-start').hide();
        CreatePretension.$modal.find('.row-with-text-start').hide();
        CreatePretension.$modal.find('.row-with-btn-stop').show();
        CreatePretension.$modal.find('.row-with-text-stop').show();

        CreatePretension.count = CreatePretension.$form.find('input[name="id[]"]').length;

        if (CreatePretension.count) {
            CreatePretension.next();
        } else {
            CreatePretension.getOffsetCount();
        }
    },
    getOffsetCount: function () {
        var csrfParam = $('meta[name=csrf-param]').attr("content");
        var csrfToken = $('meta[name=csrf-token]').attr("content");
        var dataAjax = {
            getOffsetCount: '1',
            pretension: $('#create_pretension_select').val(),
            rewrite: CreatePretension.$form.find('input[name="rewrite"]').prop('checked') ? 1 : null
        };
        dataAjax[csrfParam] = csrfToken;

        CreatePretension.ajax = $.ajax({
            type: 'GET',
            url: CreatePretension.$form.data('sender-url'),
            dataType: 'json',
            data: dataAjax,
            success: function (response, textStatus) {
                if (response.status == 'success' && response.offsetCount > 0) {
                    CreatePretension.count = response.offsetCount;
                    CreatePretension.offsetCount = response.offsetCount;
                    CreatePretension.nextOffset();
                } else {
                    CreatePretension.stop();
                    $.notify({message: response.message}, {
                        type: "danger dark",
                        animate: {exit: 'hide'},
                        z_index: 2031
                    });
                }
            },
            error: function (response) {
                CreatePretension.queryError(response);
            }
        });
    },
    nextOffset: function () {
        if (CreatePretension.offsetCount > 0) {
            var percent = CreatePretension.calculatePercent();

            CreatePretension.$modal.find('.sender-percent').text(percent + '%');
            CreatePretension.$modal.find('.progress-bar').css('width', percent + '%');

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {
                pretension: $('#create_pretension_select').val(),
                rewrite: CreatePretension.$form.find('input[name="rewrite"]').prop('checked') ? 1 : null,
                offset: CreatePretension.count - CreatePretension.offsetCount
            };

            dataAjax[csrfParam] = csrfToken;

            CreatePretension.ajax = $.ajax({
                type: 'POST',
                url: CreatePretension.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        if (typeof response.errors != 'undefined') {
                            for (var i = 0; i < response.errors.length; i++) {
                                $.notify({message: response.errors[i]}, {
                                    type: "danger dark",
                                    animate: {exit: 'hide'},
                                    z_index: 2031
                                });
                            }
                        }
                        CreatePretension.offsetCount--;
                        CreatePretension.nextOffset();
                    } else {
                        CreatePretension.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    CreatePretension.queryError(response);
                }
            });
        } else {
            CreatePretension.updateContent();
        }
    },
    next: function () {
        var $id = CreatePretension.$form.find('input[name="id[]"]:first');

        if ($id.length) {
            var percent = CreatePretension.calculatePercent();

            CreatePretension.$modal.find('.sender-percent').text(percent + '%');
            CreatePretension.$modal.find('.progress-bar').css('width', percent + '%');

            $id.remove();

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {
                id: $id.val(),
                pretension: $('#create_pretension_select').val(),
                rewrite: CreatePretension.$form.find('input[name="rewrite"]').prop('checked') ? 1 : null
            };

            dataAjax[csrfParam] = csrfToken;

            var $checkbox = $('#records_content input[name="id[]"][value="' + $id.val() + '"]');
            $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
            $checkbox.closest('tr').removeClass('text-danger');

            CreatePretension.ajax = $.ajax({
                type: 'POST',
                url: CreatePretension.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        if (typeof response.errors != 'undefined') {
                            for (var i = 0; i < response.errors.length; i++) {
                                $.notify({message: response.errors[i]}, {
                                    type: "danger dark",
                                    animate: {exit: 'hide'},
                                    z_index: 2031
                                });
                            }
                        }
                        CreatePretension.next();
                    } else {
                        $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                        $checkbox.closest('tr').addClass('text-danger');
                        CreatePretension.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    CreatePretension.queryError(response);
                }
            });
        } else {
            CreatePretension.updateContent();
        }
    },
    updateContent: function () {
        CreatePretension.ajax = $.ajax({
            type: 'GET',
            url: CreatePretension.$form.data('updater-url'),
            success: function (response, textStatus) {
                if (typeof response.content != 'undefined') {
                    var $table = $('#records_content');

                    $table.replaceWith(response.content);
                    if ($('#records_content').length) {
                        ReportViewReport.init();
                    }
                }
                CreatePretension.end();
            },
            error: function (response) {
                CreatePretension.end();
                response = JSON.parse(response.responseText);

                if (response.statusText != 'abort') {
                    if (typeof response.message != 'undefined') {
                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    } else {
                        $.notify({message: I18n['Не удалось обновить данные на странице.']}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                }
            }
        });
    },
    queryError: function (response) {
        CreatePretension.stop();
        response = JSON.parse(response.responseText);

        if (response.statusText != 'abort') {
            if (typeof response.message != 'undefined') {
                $.notify({message: response.message}, {
                    type: "danger dark",
                    animate: {exit: 'hide'},
                    z_index: 2031
                });
            } else {
                $.notify({message: I18n['Не удалось обновить данные.']}, {
                    type: "danger dark",
                    animate: {exit: 'hide'},
                    z_index: 2031
                });
            }
        }
    },
    end: function () {
        CreatePretension.ajax = null;
        CreatePretension.$modal.modal('hide');

        $.notify({message: I18n['Обновление данных завершено.']}, {
            type: "success dark",
            animate: {exit: 'hide'},
            z_index: 2031
        });
    },
    stop: function () {
        if (CreatePretension.ajax != null) {
            CreatePretension.ajax.abort();
        }

        CreatePretension.$modal.find('.row-with-btn-start').show();
        CreatePretension.$modal.find('.row-with-text-start').show();
        CreatePretension.$modal.find('.row-with-btn-stop').hide();
        CreatePretension.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (CreatePretension.count) {
            var part = 100 / CreatePretension.count;
            var count = CreatePretension.count;
            if (CreatePretension.offsetCount == null)
                count -= CreatePretension.$form.find('input[name="id[]"]').length;
            else count -= CreatePretension.offsetCount;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
