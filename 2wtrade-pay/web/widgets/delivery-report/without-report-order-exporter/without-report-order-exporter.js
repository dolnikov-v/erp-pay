var I18n = I18n || {};

$(function () {
    WithoutReportOrderExporter.init();
});

var WithoutReportOrderExporter = {
    ajax: null,
    count: 0,
    offsetCount: null,
    filename: '',
    $modal: $('#modal_export_without_report'),
    $form: $('#modal_export_without_report').find('form'),
    init: function () {
        $('#start_export_without_report').on('click', WithoutReportOrderExporter.start);
        $('#stop_export_without_report').on('click', WithoutReportOrderExporter.stop);
    },
    show: function () {
        WithoutReportOrderExporter.$form.find('input[name="dateFrom"]').remove();
        WithoutReportOrderExporter.$form.find('input[name="dateTo"]').remove();
        WithoutReportOrderExporter.$form.find('select[name="delivery_id"]').remove();
        $('#export_without_report_link_download').attr('href', '#');

        WithoutReportOrderExporter.$modal.find('.row-with-btn-start').show();
        WithoutReportOrderExporter.$modal.find('.row-with-text-start').show();
        WithoutReportOrderExporter.$modal.find('.row-with-btn-stop').hide();
        WithoutReportOrderExporter.$modal.find('.row-with-text-progress').hide();
        WithoutReportOrderExporter.$modal.find('.row-with-text-finish').hide();
        WithoutReportOrderExporter.$modal.modal();
    },
    start: function () {
        WithoutReportOrderExporter.$modal.find('.sender-percent').text('0%');
        WithoutReportOrderExporter.$modal.find('.progress-bar').css('width', '0%');

        WithoutReportOrderExporter.$modal.find('.row-with-btn-start').hide();
        WithoutReportOrderExporter.$modal.find('.row-with-text-start').hide();
        WithoutReportOrderExporter.$modal.find('.row-with-text-finish').hide();
        WithoutReportOrderExporter.$modal.find('.row-with-btn-stop').show();
        WithoutReportOrderExporter.$modal.find('.row-with-text-progress').show();

        WithoutReportOrderExporter.getOffsetCount();
    },

    getOffsetCount: function () {
        var csrfParam = $('meta[name=csrf-param]').attr("content");
        var csrfToken = $('meta[name=csrf-token]').attr("content");
        var dataAjax = {
            getOffsetCount: '1',
            delivery: WithoutReportOrderExporter.$form.find('select[name="delivery_id"]').val(),
            dateFrom: WithoutReportOrderExporter.$form.find('input[name="dateFrom"]').val(),
            dateTo: WithoutReportOrderExporter.$form.find('input[name="dateTo"]').val()
        };
        dataAjax[csrfParam] = csrfToken;

        WithoutReportOrderExporter.ajax = $.ajax({
            type: 'GET',
            url: WithoutReportOrderExporter.$form.data('sender-url'),
            dataType: 'json',
            data: dataAjax,
            success: function (response, textStatus) {
                if (response.status == 'success' && response.offsetCount > 0) {
                    WithoutReportOrderExporter.count = response.offsetCount;
                    WithoutReportOrderExporter.offsetCount = response.offsetCount;
                    WithoutReportOrderExporter.filename = response.filename;
                    WithoutReportOrderExporter.nextOffset();
                } else {
                    WithoutReportOrderExporter.stop();
                    $.notify({message: response.message}, {
                        type: "danger dark",
                        animate: {exit: 'hide'},
                        z_index: 2031
                    });
                }
            },
            error: function (response) {
                WithoutReportOrderExporter.queryError(response);
            }
        });
    },
    nextOffset: function () {
        if (WithoutReportOrderExporter.offsetCount > 0) {
            var percent = WithoutReportOrderExporter.calculatePercent();

            WithoutReportOrderExporter.$modal.find('.sender-percent').text(percent + '%');
            WithoutReportOrderExporter.$modal.find('.progress-bar').css('width', percent + '%');

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {
                offset: WithoutReportOrderExporter.count - WithoutReportOrderExporter.offsetCount,
                filename: WithoutReportOrderExporter.filename,
                delivery: WithoutReportOrderExporter.$form.find('select[name="delivery_id"]').val(),
                dateFrom: WithoutReportOrderExporter.$form.find('input[name="dateFrom"]').val(),
                dateTo: WithoutReportOrderExporter.$form.find('input[name="dateTo"]').val()
            };

            dataAjax[csrfParam] = csrfToken;

            WithoutReportOrderExporter.ajax = $.ajax({
                type: 'POST',
                url: WithoutReportOrderExporter.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        WithoutReportOrderExporter.offsetCount--;
                        WithoutReportOrderExporter.nextOffset();
                    } else {
                        WithoutReportOrderExporter.stop();
                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    WithoutReportOrderExporter.queryError(response);
                }
            });
        } else {
            if (WithoutReportOrderExporter.filename != null && WithoutReportOrderExporter.count > 0) {
                $('#export_without_report_link_download').attr('href', WithoutReportOrderExporter.$form.data('download-url') + "/" + WithoutReportOrderExporter.filename);
            }
            WithoutReportOrderExporter.end();
        }
    },
    queryError: function (response) {
        WithoutReportOrderExporter.stop();
        response = JSON.parse(response.responseText);

        if (response.statusText != 'abort') {
            if (typeof response.message != 'undefined') {
                $.notify({message: response.message}, {
                    type: "danger dark",
                    animate: {exit: 'hide'},
                    z_index: 2031
                });
            } else {
                $.notify({message: I18n['Произошла ошибка при генерации файла.']}, {
                    type: "danger dark",
                    animate: {exit: 'hide'},
                    z_index: 2031
                });
            }
        }
    },
    end: function () {
        WithoutReportOrderExporter.ajax = null;
        WithoutReportOrderExporter.$modal.find('.row-with-btn-stop').hide();
        WithoutReportOrderExporter.$modal.find('.row-with-text-progress').hide();
        WithoutReportOrderExporter.$modal.find('.row-with-text-finish').show();

        $.notify({message: I18n['Генерация файла завершена.']}, {
            type: "success dark",
            animate: {exit: 'hide'},
            z_index: 2031
        });
    },
    stop: function () {
        if (WithoutReportOrderExporter.ajax != null) {
            WithoutReportOrderExporter.ajax.abort();
        }

        WithoutReportOrderExporter.$modal.find('.row-with-btn-start').show();
        WithoutReportOrderExporter.$modal.find('.row-with-text-start').show();
        WithoutReportOrderExporter.$modal.find('.row-with-btn-stop').hide();
        WithoutReportOrderExporter.$modal.find('.row-with-text-progress').hide();
        WithoutReportOrderExporter.$modal.find('.row-with-text-finish').hide();
    },
    calculatePercent: function () {
        var percent = 0;

        if (WithoutReportOrderExporter.count) {
            var part = 100 / WithoutReportOrderExporter.count;
            var count = WithoutReportOrderExporter.count - WithoutReportOrderExporter.offsetCount;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
