var I18n = I18n || {};

$(function () {
    RecordApprover.init();
});

var RecordApprover = {
    ajax: null,
    count: 0,
    offsetCount: null,
    offset: 0,
    $modal: $('#modal_set_approve'),
    $form: $('#modal_set_approve').find('form'),
    init: function () {
        $('#start_set_approve').on('click', RecordApprover.start);
        $('#stop_set_approve').on('click', RecordApprover.stop);
    },
    show: function () {
        RecordApprover.$modal.modal();
        RecordApprover.count = RecordApprover.$form.find('input[name="id[]"]').length;
        if (RecordApprover.count) {
            RecordApprover.$modal.find('.action-record-count').hide();
        }
    },
    start: function () {
        RecordApprover.$modal.find('.sender-percent').text('0%');
        RecordApprover.$modal.find('.progress-bar').css('width', '0%');

        RecordApprover.$modal.find('.row-with-btn-start').hide();
        RecordApprover.$modal.find('.row-with-text-start').hide();
        RecordApprover.$modal.find('.row-with-btn-stop').show();
        RecordApprover.$modal.find('.row-with-text-stop').show();

        RecordApprover.count = RecordApprover.$form.find('input[name="id[]"]').length;

        if (RecordApprover.count) {
            RecordApprover.next();
        } else {
            RecordApprover.getOffsetCount();
        }
    },
    getOffsetCount: function () {
        var csrfParam = $('meta[name=csrf-param]').attr("content");
        var csrfToken = $('meta[name=csrf-token]').attr("content");
        var dataAjax = {
            getOffsetCount: '1'
        };
        dataAjax[csrfParam] = csrfToken;

        RecordApprover.ajax = $.ajax({
            type: 'GET',
            url: RecordApprover.$form.data('sender-url'),
            dataType: 'json',
            data: dataAjax,
            success: function (response, textStatus) {
                if (response.status == 'success' && response.offsetCount > 0) {
                    RecordApprover.count = response.offsetCount;
                    RecordApprover.offsetCount = response.offsetCount;
                    RecordApprover.nextOffset();
                } else {
                    RecordApprover.stop();
                    $.notify({message: response.message}, {
                        type: "danger dark",
                        animate: {exit: 'hide'},
                        z_index: 2031
                    });
                }
            },
            error: function (response) {
                RecordApprover.queryError(response);
            }
        });
    },
    nextOffset: function () {
        if (RecordApprover.offsetCount > 0) {
            var percent = RecordApprover.calculatePercent();

            RecordApprover.$modal.find('.sender-percent').text(percent + '%');
            RecordApprover.$modal.find('.progress-bar').css('width', percent + '%');

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {};

            dataAjax[csrfParam] = csrfToken;
            dataAjax['offset'] = RecordApprover.offset;

            RecordApprover.ajax = $.ajax({
                type: 'POST',
                url: RecordApprover.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        RecordApprover.offsetCount--;
                        RecordApprover.offset = response.offset;
                        RecordApprover.nextOffset();
                    } else {
                        RecordApprover.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    RecordApprover.queryError(response);
                }
            });
        } else {
            RecordApprover.updateContent();
        }
    },
    next: function () {
        var $id = RecordApprover.$form.find('input[name="id[]"]:first');

        if ($id.length) {
            var percent = RecordApprover.calculatePercent();

            RecordApprover.$modal.find('.sender-percent').text(percent + '%');
            RecordApprover.$modal.find('.progress-bar').css('width', percent + '%');

            $id.remove();

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {
                id: $id.val()
            };

            dataAjax[csrfParam] = csrfToken;

            var $checkbox = $('#records_content input[name="id[]"][value="' + $id.val() + '"]');
            $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
            $checkbox.closest('tr').removeClass('text-danger');

            RecordApprover.ajax = $.ajax({
                type: 'POST',
                url: RecordApprover.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        RecordApprover.next();
                    } else {
                        $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                        $checkbox.closest('tr').addClass('text-danger');
                        RecordApprover.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    RecordApprover.queryError(response);
                }
            });
        } else {
            RecordApprover.updateContent();
        }
    },
    updateContent: function () {
        RecordApprover.ajax = $.ajax({
            type: 'GET',
            url: RecordApprover.$form.data('updater-url'),
            success: function (response, textStatus) {
                if (typeof response.content != 'undefined') {
                    var $table = $('#records_content');

                    $table.replaceWith(response.content);
                    if ($('#records_content').length) {
                        ReportViewReport.init();
                    }
                }
                RecordApprover.end();
            },
            error: function (response) {
                RecordApprover.end();
                response = JSON.parse(response.responseText);

                if (response.statusText != 'abort') {
                    if (typeof response.message != 'undefined') {
                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    } else {
                        $.notify({message: I18n['Не удалось обновить данные на странице.']}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                }
            }
        });
    },
    queryError: function (response) {
        RecordApprover.stop();
        response = JSON.parse(response.responseText);

        if (response.statusText != 'abort') {
            if (typeof response.message != 'undefined') {
                $.notify({message: response.message}, {
                    type: "danger dark",
                    animate: {exit: 'hide'},
                    z_index: 2031
                });
            } else {
                $.notify({message: I18n['Не удалось сделать подтверждение записи.']}, {
                    type: "danger dark",
                    animate: {exit: 'hide'},
                    z_index: 2031
                });
            }
        }
    },
    end: function () {
        RecordApprover.ajax = null;
        RecordApprover.$modal.modal('hide');

        $.notify({message: I18n['Подтверждение записей завершено.']}, {
            type: "success dark",
            animate: {exit: 'hide'},
            z_index: 2031
        });
    },
    stop: function () {
        if (RecordApprover.ajax != null) {
            RecordApprover.ajax.abort();
        }

        RecordApprover.$modal.find('.row-with-btn-start').show();
        RecordApprover.$modal.find('.row-with-text-start').show();
        RecordApprover.$modal.find('.row-with-btn-stop').hide();
        RecordApprover.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (RecordApprover.count) {
            var part = 100 / RecordApprover.count;
            var count = RecordApprover.count;
            if (RecordApprover.offsetCount == null)
                count -= RecordApprover.$form.find('input[name="id[]"]').length;
            else count -= RecordApprover.offsetCount;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
