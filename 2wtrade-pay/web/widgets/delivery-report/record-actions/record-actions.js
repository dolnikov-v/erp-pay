
$(function () {
    RecordFilters.init();
});

var RecordFilters = {
    $inputFilters: $('#filter_records_selectable_filters'),
    init: function () {
        $(document).on('click', '.add-selectable-filters', RecordFilters.addFilter);
        $(document).on('click', '.remove-selectable-filters', RecordFilters.removeFilter);
        $(document).on('click', '.not-selectable-filters', RecordFilters.toggleNot);
        $('.shower-selectable-filters').on('click', RecordFilters.showFilters);
    },

    addFilter: function () {
        var filter = RecordFilters.$inputFilters.val();

        $('#row_selectable_filters').before($('#container_empty_filters').find('.container-empty-filter[data-filter="' + filter +  '"]').clone());
        $('#row_selectable_filters').prev().find('.container-selectable-filters select').select2({ width: '100%' });
    },

    removeFilter: function () {
        var $form = $(this).closest('form');

        var $row = $(this).closest('.container-empty-filter[data-filter="' + $(this).data('filter') + '"]');
        $row.remove();

        $form.find('.shower-selectable-filters[data-filter="' + $(this).data('filter') + '"]').trigger('click');
    },

    toggleNot: function () {
        var $input = $(this).find('input');

        $(this).toggleClass('btn-default').toggleClass('btn-danger');

        if ($(this).hasClass('btn-default')) {
            $input.val('0');
        } else {
            $input.val('1');
        }
    },

    showFilters: function () {
        var $form = $(this).closest('form');
        var filter = $(this).data('filter');

        $form.find('.container-empty-filter[data-filter="' + filter + '"]').show()
            .find('.form-group.form-group-no-margin').removeClass('form-group-no-margin');

        $(this).closest('div.row').remove();

        return false;
    }
};
