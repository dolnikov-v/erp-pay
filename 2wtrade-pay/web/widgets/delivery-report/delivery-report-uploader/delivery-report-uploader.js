var I18n = I18n || {};

$(function () {
    DeliveryReportUploader.init();
});

var DeliveryReportUploader = {
    ajax: null,
    $modal: $('#modal_delivery_report_uploader'),
    $form: $('#modal_delivery_report_uploader').find('form'),

    init: function () {
        DeliveryReportUploader.$modal.on('hide.bs.modal', DeliveryReportUploader.modalHide);
        DeliveryReportUploader.$form.on('submit', DeliveryReportUploader.submit);
    },
    initStart: function () {
        DeliveryReportUploader.$form.find('.row-with-text-start').show();
        DeliveryReportUploader.$form.find('.row-with-btn-start').show();
        DeliveryReportUploader.$form.find('.row-with-text-progress').hide();
        DeliveryReportUploader.$form.find('.row-with-text-finish').hide();
        DeliveryReportUploader.ajax = null;
        if (typeof DeliveryReportUploader.$form.data('report-id') == 'undefined') {
            DeliveryReportUploader.initStartCreateReport();
        } else {
            DeliveryReportUploader.initStartUpdateReport();
        }
    },
    initStartCreateReport: function () {
        $('#delivery_report_uploader_title_row').hide();
        DeliveryReportUploader.$modal.find('.modal-header .modal-title').text(I18n['Загрузка отчета']);
    },
    initStartUpdateReport: function () {
        $('#delivery_report_uploader_title_row').show();
        DeliveryReportUploader.$modal.find('.modal-header .modal-title').text(I18n['Редактирование отчета']);
    },
    initProgress: function () {
        DeliveryReportUploader.$form.find('.row-with-text-start').hide();
        DeliveryReportUploader.$form.find('.row-with-btn-start').hide();
        DeliveryReportUploader.$form.find('.row-with-text-progress').show();
        DeliveryReportUploader.$form.find('.row-with-text-finish').hide();
    },
    fillValues: function (data) {
        $('#delivery_report_uploader_delivery_id').val(data['delivery']).change();
        $('#delivery_report_uploader_type').val(data['type']).change();
        $('#delivery_report_uploader_date_format').val(data['dateFormat']);
        $('#delivery_report_uploader_title').val(data['title']);
        DeliveryReportUploader.$form.find("input[name='period_from']").val(data['periodFrom']);
        DeliveryReportUploader.$form.find("input[name='period_to']").val(data['periodTo']);
        DeliveryReportUploader.$form.data('report-id', data['reportId']);
    },
    clearValues: function () {
        $('#delivery_report_uploader_delivery_id').val('').change();
        $('#delivery_report_uploader_type').val('').change();
        $('#delivery_report_uploader_date_format').val('');
        $('#delivery_report_uploader_title').val('');
        DeliveryReportUploader.$form.find("input[name='period_from']").val('');
        DeliveryReportUploader.$form.find("input[name='period_to']").val('');
        DeliveryReportUploader.$form.removeData('report-id');
    },
    show: function (clear) {
        if (clear) {
            DeliveryReportUploader.clearValues();
        }
        DeliveryReportUploader.initStart();
        DeliveryReportUploader.$modal.modal();
        return false;
    },

    stop: function () {
        if (DeliveryReportUploader.ajax != null) {
            return false;
        }
        return true;
    },
    modalHide: function () {
        return DeliveryReportUploader.stop();
    },

    submit: function (event) {
        DeliveryReportUploader.initProgress();

        var formData = new FormData();
        formData.append('file', DeliveryReportUploader.$form.find('input[name="file"]').prop('files')[0]);
        formData.append('delivery_id', $('#delivery_report_uploader_delivery_id').val());
        formData.append('type', $('#delivery_report_uploader_type').val());
        formData.append('date_format', $('#delivery_report_uploader_date_format').val());
        formData.append('period_from', DeliveryReportUploader.$form.find("input[name='period_from']").val());
        formData.append('period_to', DeliveryReportUploader.$form.find("input[name='period_to']").val());
        if (typeof DeliveryReportUploader.$form.data('report-id') != 'undefined') {
            formData.append('report_name', $('#delivery_report_uploader_title').val());
            formData.append('report_id', DeliveryReportUploader.$form.data('report-id'));
        }

        DeliveryReportUploader.ajax = $.ajax({
            type: 'POST',
            url: DeliveryReportUploader.$form.attr('action'),
            dataType: 'json',
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            success: function (response, textStatus) {
                if (response.status == 'success') {
                    $('#reports_content').replaceWith(response.content);
                    DeliveryReportIndex.init();
                    DeliveryReportUploader.end();
                } else {
                    DeliveryReportUploader.initStart();
                    MainBootstrap.addNotificationDanger(response.message);
                }
            },
            error: function (response) {
                DeliveryReportUploader.initStart();
                DeliveryReportUploader.ajax = null;
                response = JSON.parse(response.responseText);

                if (response.statusText != 'abort') {
                    if (typeof response.message != 'undefined') {
                        MainBootstrap.addNotificationDanger(response.message);
                    } else {
                        MainBootstrap.addNotificationDanger(I18n['Не удалось загрузить отчет.']);
                    }
                }
            }
        });

        event.stopImmediatePropagation();

        return false;
    },
    end: function () {
        DeliveryReportUploader.ajax = null;
        DeliveryReportUploader.$modal.modal('hide');

        $.notify({message: (typeof DeliveryReportUploader.$form.data('report-id') == 'undefined' ? I18n['Отчет успешно загружен и отправлен на сканирование.'] : I18n['Отчет успешно отредактирован.'])}, {
            type: "success dark",
            animate: {exit: 'hide'},
            z_index: 2031
        });
    }
};
