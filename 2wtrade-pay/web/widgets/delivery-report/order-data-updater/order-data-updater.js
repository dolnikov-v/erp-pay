var I18n = I18n || {};

$(function () {
    OrderDataUpdater.init();
});

var OrderDataUpdater = {
    ajax: null,
    count: 0,
    offsetCount: null,
    $modal: $('#modal_update_order_data'),
    $form: $('#modal_update_order_data').find('form'),
    init: function () {
        $('#start_update_order_data').on('click', OrderDataUpdater.start);
        $('#stop_update_order_data').on('click', OrderDataUpdater.stop);
    },
    show: function () {
        OrderDataUpdater.$modal.modal();
        OrderDataUpdater.count = OrderDataUpdater.$form.find('input[name="id[]"]').length;
        if (OrderDataUpdater.count) {
            OrderDataUpdater.$modal.find('.action-record-count').hide();
        }
    },
    start: function () {
        OrderDataUpdater.$modal.find('.sender-percent').text('0%');
        OrderDataUpdater.$modal.find('.progress-bar').css('width', '0%');

        OrderDataUpdater.$modal.find('.row-with-btn-start').hide();
        OrderDataUpdater.$modal.find('.row-with-text-start').hide();
        OrderDataUpdater.$modal.find('.row-with-btn-stop').show();
        OrderDataUpdater.$modal.find('.row-with-text-stop').show();

        OrderDataUpdater.count = OrderDataUpdater.$form.find('input[name="id[]"]').length;

        if (OrderDataUpdater.count) {
            OrderDataUpdater.next();
        } else {
            OrderDataUpdater.getOffsetCount();
        }
    },
    getOffsetCount: function () {
        var csrfParam = $('meta[name=csrf-param]').attr("content");
        var csrfToken = $('meta[name=csrf-token]').attr("content");
        var dataAjax = {
            getOffsetCount: '1',
            column: $('#update_order_data_column_select').val(),
            rewrite: OrderDataUpdater.$form.find('input[name="rewrite"]').prop('checked') ? 1 : null
        };
        dataAjax[csrfParam] = csrfToken;

        OrderDataUpdater.ajax = $.ajax({
            type: 'GET',
            url: OrderDataUpdater.$form.data('sender-url'),
            dataType: 'json',
            data: dataAjax,
            success: function (response, textStatus) {
                if (response.status == 'success' && response.offsetCount > 0) {
                    OrderDataUpdater.count = response.offsetCount;
                    OrderDataUpdater.offsetCount = response.offsetCount;
                    OrderDataUpdater.nextOffset();
                } else {
                    OrderDataUpdater.stop();
                    $.notify({message: response.message}, {
                        type: "danger dark",
                        animate: {exit: 'hide'},
                        z_index: 2031
                    });
                }
            },
            error: function (response) {
                OrderDataUpdater.queryError(response);
            }
        });
    },
    nextOffset: function () {
        if (OrderDataUpdater.offsetCount > 0) {
            var percent = OrderDataUpdater.calculatePercent();

            OrderDataUpdater.$modal.find('.sender-percent').text(percent + '%');
            OrderDataUpdater.$modal.find('.progress-bar').css('width', percent + '%');

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {
                column: $('#update_order_data_column_select').val(),
                rewrite: OrderDataUpdater.$form.find('input[name="rewrite"]').prop('checked') ? 1 : null,
                offset: OrderDataUpdater.count - OrderDataUpdater.offsetCount
            };

            dataAjax[csrfParam] = csrfToken;

            OrderDataUpdater.ajax = $.ajax({
                type: 'POST',
                url: OrderDataUpdater.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        if (typeof response.errors != 'undefined') {
                            for (var i = 0; i < response.errors.length; i++) {
                                $.notify({message: response.errors[i]}, {
                                    type: "danger dark",
                                    animate: {exit: 'hide'},
                                    z_index: 2031
                                });
                            }
                        }
                        OrderDataUpdater.offsetCount--;
                        OrderDataUpdater.nextOffset();
                    } else {
                        OrderDataUpdater.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    OrderDataUpdater.queryError(response);
                }
            });
        } else {
            OrderDataUpdater.updateContent();
        }
    },
    next: function () {
        var $id = OrderDataUpdater.$form.find('input[name="id[]"]:first');

        if ($id.length) {
            var percent = OrderDataUpdater.calculatePercent();

            OrderDataUpdater.$modal.find('.sender-percent').text(percent + '%');
            OrderDataUpdater.$modal.find('.progress-bar').css('width', percent + '%');

            $id.remove();

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {
                id: $id.val(),
                column: $('#update_order_data_column_select').val(),
                rewrite: OrderDataUpdater.$form.find('input[name="rewrite"]').prop('checked') ? 1 : null
            };

            dataAjax[csrfParam] = csrfToken;

            var $checkbox = $('#records_content input[name="id[]"][value="' + $id.val() + '"]');
            $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
            $checkbox.closest('tr').removeClass('text-danger');

            OrderDataUpdater.ajax = $.ajax({
                type: 'POST',
                url: OrderDataUpdater.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        if (typeof response.errors != 'undefined') {
                            for (var i = 0; i < response.errors.length; i++) {
                                $.notify({message: response.errors[i]}, {
                                    type: "danger dark",
                                    animate: {exit: 'hide'},
                                    z_index: 2031
                                });
                            }
                        }
                        OrderDataUpdater.next();
                    } else {
                        $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                        $checkbox.closest('tr').addClass('text-danger');
                        OrderDataUpdater.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    OrderDataUpdater.queryError(response);
                }
            });
        } else {
            OrderDataUpdater.updateContent();
        }
    },
    updateContent: function () {
        OrderDataUpdater.ajax = $.ajax({
            type: 'GET',
            url: OrderDataUpdater.$form.data('updater-url'),
            success: function (response, textStatus) {
                if (typeof response.content != 'undefined') {
                    var $table = $('#records_content');

                    $table.replaceWith(response.content);
                    if ($('#records_content').length) {
                        ReportViewReport.init();
                    }
                }
                OrderDataUpdater.end();
            },
            error: function (response) {
                OrderDataUpdater.end();
                response = JSON.parse(response.responseText);

                if (response.statusText != 'abort') {
                    if (typeof response.message != 'undefined') {
                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    } else {
                        $.notify({message: I18n['Не удалось обновить данные на странице.']}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                }
            }
        });
    },
    queryError: function (response) {
        OrderDataUpdater.stop();
        response = JSON.parse(response.responseText);

        if (response.statusText != 'abort') {
            if (typeof response.message != 'undefined') {
                $.notify({message: response.message}, {
                    type: "danger dark",
                    animate: {exit: 'hide'},
                    z_index: 2031
                });
            } else {
                $.notify({message: I18n['Не удалось обновить данные.']}, {
                    type: "danger dark",
                    animate: {exit: 'hide'},
                    z_index: 2031
                });
            }
        }
    },
    end: function () {
        OrderDataUpdater.ajax = null;
        OrderDataUpdater.$modal.modal('hide');

        $.notify({message: I18n['Обновление данных завершено.']}, {
            type: "success dark",
            animate: {exit: 'hide'},
            z_index: 2031
        });
    },
    stop: function () {
        if (OrderDataUpdater.ajax != null) {
            OrderDataUpdater.ajax.abort();
        }

        OrderDataUpdater.$modal.find('.row-with-btn-start').show();
        OrderDataUpdater.$modal.find('.row-with-text-start').show();
        OrderDataUpdater.$modal.find('.row-with-btn-stop').hide();
        OrderDataUpdater.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (OrderDataUpdater.count) {
            var part = 100 / OrderDataUpdater.count;
            var count = OrderDataUpdater.count;
            if (OrderDataUpdater.offsetCount == null)
                count -= OrderDataUpdater.$form.find('input[name="id[]"]').length;
            else count -= OrderDataUpdater.offsetCount;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
