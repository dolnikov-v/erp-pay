var I18n = I18n || {};

$(function () {
    OrderCopyCreator.init();
});

var OrderCopyCreator = {
    $modal: $('#modal_create_order_copy'),
    $form: $('#modal_create_order_copy').find('form'),
    count: null,
    $ajax: null,
    init: function () {
        $('#start_create_order_copy').on('click', OrderCopyCreator.start);
        $('#stop_create_order_copy').on('click', OrderCopyCreator.stop);
    },
    show: function () {
        OrderCopyCreator.$modal.modal();
    },
    stop: function () {
        if (OrderCopyCreator.ajax != null) {
            OrderCopyCreator.ajax.abort();
        }

        OrderCopyCreator.$modal.find('.row-with-btn-start').show();
        OrderCopyCreator.$modal.find('.row-with-text-start').show();
        OrderCopyCreator.$modal.find('.row-with-btn-stop').hide();
        OrderCopyCreator.$modal.find('.row-with-text-stop').hide();
    },
    start: function () {
        OrderCopyCreator.$modal.find('.sender-percent').text('0%');
        OrderCopyCreator.$modal.find('.progress-bar').css('width', '0%');

        OrderCopyCreator.$modal.find('.row-with-btn-start').hide();
        OrderCopyCreator.$modal.find('.row-with-text-start').hide();
        OrderCopyCreator.$modal.find('.row-with-btn-stop').show();
        OrderCopyCreator.$modal.find('.row-with-text-stop').show();

        OrderCopyCreator.count = OrderCopyCreator.$form.find('input[name="id[]"]').length;

        if (OrderCopyCreator.count) {
            OrderCopyCreator.next();
        } else {
            $.notify({message: I18n['Необходимо выбрать записи.']}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            OrderCopyCreator.stop();
        }
    },
    next: function () {
        var $id = OrderCopyCreator.$form.find('input[name="id[]"]:first');

        if ($id.length) {
            var percent = OrderCopyCreator.calculatePercent();

            OrderCopyCreator.$modal.find('.sender-percent').text(percent + '%');
            OrderCopyCreator.$modal.find('.progress-bar').css('width', percent + '%');

            $id.remove();

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {
                id: $id.val(),
            };

            dataAjax[csrfParam] = csrfToken;

            var $checkbox = $('#records_content input[name="id[]"][value="' + $id.val() + '"]');
            $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
            $checkbox.closest('tr').removeClass('text-danger');

            OrderCopyCreator.ajax = $.ajax({
                type: 'POST',
                url: OrderCopyCreator.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        if (typeof response.errors != 'undefined') {
                            for (var i = 0; i < response.errors.length; i++) {
                                $.notify({message: response.errors[i]}, {
                                    type: "danger dark",
                                    animate: {exit: 'hide'},
                                    z_index: 2031
                                });
                            }
                        }
                        OrderCopyCreator.removeRow($checkbox.closest('tr'));
                        OrderCopyCreator.next();
                    } else {
                        $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                        $checkbox.closest('tr').addClass('text-danger');
                        OrderCopyCreator.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    OrderCopyCreator.queryError(response);
                }
            });
        } else {
            OrderCopyCreator.updateContent();
        }
    },
    updateContent: function () {
        OrderCopyCreator.ajax = $.ajax({
            type: 'GET',
            url: OrderCopyCreator.$form.data('updater-url'),
            success: function (response, textStatus) {
                if (typeof response.content != 'undefined') {
                    var $table = $('#records_content');

                    $table.replaceWith(response.content);
                    if ($('#records_content').length) {
                        ReportViewReport.init();
                    }
                }
                OrderCopyCreator.end();
            },
            error: function (response) {
                OrderCopyCreator.end();
                response = JSON.parse(response.responseText);

                if (response.statusText != 'abort') {
                    if (typeof response.message != 'undefined') {
                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    } else {
                        $.notify({message: I18n['Не удалось обновить данные на странице.']}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                }
            }
        });
    },
    queryError: function (response) {
        OrderCopyCreator.stop();
        response = JSON.parse(response.responseText);

        if (response.statusText != 'abort') {
            if (typeof response.message != 'undefined') {
                $.notify({message: response.message}, {
                    type: "danger dark",
                    animate: {exit: 'hide'},
                    z_index: 2031
                });
            } else {
                $.notify({message: I18n['Не создать дубликат заказа.']}, {
                    type: "danger dark",
                    animate: {exit: 'hide'},
                    z_index: 2031
                });
            }
        }
    },
    end: function () {
        OrderCopyCreator.ajax = null;
        OrderCopyCreator.$modal.modal('hide');

        $.notify({message: I18n['Создание дубликатов завершено.']}, {
            type: "success dark",
            animate: {exit: 'hide'},
            z_index: 2031
        });
    },
    calculatePercent: function () {
        var percent = 0;

        if (OrderCopyCreator.count) {
            var part = 100 / OrderCopyCreator.count;
            var count = OrderCopyCreator.count;
            if (OrderCopyCreator.offsetCount == null)
                count -= OrderCopyCreator.$form.find('input[name="id[]"]').length;
            else count -= OrderCopyCreator.offsetCount;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    },
    removeRow: function ($tr) {
        $tr.next().remove();
        $tr.next().remove();
        $tr.remove();
    }
};
