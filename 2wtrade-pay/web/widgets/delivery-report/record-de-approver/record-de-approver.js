var I18n = I18n || {};

$(function () {
    RecordDeApprover.init();
});

var RecordDeApprover = {
    ajax: null,
    count: 0,
    offsetCount: null,
    $modal: $('#modal_unset_approve'),
    $form: $('#modal_unset_approve').find('form'),
    init: function () {
        $('#start_unset_approve').on('click', RecordDeApprover.start);
        $('#stop_unset_approve').on('click', RecordDeApprover.stop);
    },
    show: function () {
        RecordDeApprover.$modal.modal();
        RecordDeApprover.count = RecordDeApprover.$form.find('input[name="id[]"]').length;
        if (RecordDeApprover.count) {
            RecordDeApprover.$modal.find('.action-record-count').hide();
        }
    },
    start: function () {
        RecordDeApprover.$modal.find('.sender-percent').text('0%');
        RecordDeApprover.$modal.find('.progress-bar').css('width', '0%');

        RecordDeApprover.$modal.find('.row-with-btn-start').hide();
        RecordDeApprover.$modal.find('.row-with-text-start').hide();
        RecordDeApprover.$modal.find('.row-with-btn-stop').show();
        RecordDeApprover.$modal.find('.row-with-text-stop').show();

        RecordDeApprover.count = RecordDeApprover.$form.find('input[name="id[]"]').length;

        if (RecordDeApprover.count) {
            RecordDeApprover.next();
        } else {
            RecordDeApprover.getOffsetCount();
        }
    },
    getOffsetCount: function () {
        var csrfParam = $('meta[name=csrf-param]').attr("content");
        var csrfToken = $('meta[name=csrf-token]').attr("content");
        var dataAjax = {
            getOffsetCount: '1'
        };
        dataAjax[csrfParam] = csrfToken;

        RecordDeApprover.ajax = $.ajax({
            type: 'GET',
            url: RecordDeApprover.$form.data('sender-url'),
            dataType: 'json',
            data: dataAjax,
            success: function (response, textStatus) {
                if (response.status == 'success' && response.offsetCount > 0) {
                    RecordDeApprover.count = response.offsetCount;
                    RecordDeApprover.offsetCount = response.offsetCount;
                    RecordDeApprover.nextOffset();
                } else {
                    RecordDeApprover.stop();
                    $.notify({message: response.message}, {
                        type: "danger dark",
                        animate: {exit: 'hide'},
                        z_index: 2031
                    });
                }
            },
            error: function (response) {
                RecordDeApprover.queryError(response);
            }
        });
    },
    nextOffset: function () {
        if (RecordDeApprover.offsetCount > 0) {
            var percent = RecordDeApprover.calculatePercent();

            RecordDeApprover.$modal.find('.sender-percent').text(percent + '%');
            RecordDeApprover.$modal.find('.progress-bar').css('width', percent + '%');

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {};

            dataAjax[csrfParam] = csrfToken;

            RecordDeApprover.ajax = $.ajax({
                type: 'POST',
                url: RecordDeApprover.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        RecordDeApprover.offsetCount--;
                        RecordDeApprover.nextOffset();
                    } else {
                        RecordDeApprover.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    RecordDeApprover.queryError(response);
                }
            });
        } else {
            RecordDeApprover.updateContent();
        }
    },
    next: function () {
        var $id = RecordDeApprover.$form.find('input[name="id[]"]:first');

        if ($id.length) {
            var percent = RecordDeApprover.calculatePercent();

            RecordDeApprover.$modal.find('.sender-percent').text(percent + '%');
            RecordDeApprover.$modal.find('.progress-bar').css('width', percent + '%');

            $id.remove();

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {
                id: $id.val()
            };

            dataAjax[csrfParam] = csrfToken;

            var $checkbox = $('#records_content input[name="id[]"][value="' + $id.val() + '"]');
            $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
            $checkbox.closest('tr').removeClass('text-danger');

            RecordDeApprover.ajax = $.ajax({
                type: 'POST',
                url: RecordDeApprover.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        RecordDeApprover.next();
                    } else {
                        $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                        $checkbox.closest('tr').addClass('text-danger');
                        RecordDeApprover.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    RecordDeApprover.queryError(response);
                }
            });
        } else {
            RecordDeApprover.updateContent();
        }
    },
    updateContent: function () {
        RecordDeApprover.ajax = $.ajax({
            type: 'GET',
            url: RecordDeApprover.$form.data('updater-url'),
            success: function (response, textStatus) {
                if (typeof response.content != 'undefined') {
                    var $table = $('#records_content');

                    $table.replaceWith(response.content);
                    if ($('#records_content').length) {
                        ReportViewReport.init();
                    }
                }
                RecordDeApprover.end();
            },
            error: function (response) {
                RecordDeApprover.end();
                response = JSON.parse(response.responseText);

                if (response.statusText != 'abort') {
                    if (typeof response.message != 'undefined') {
                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    } else {
                        $.notify({message: I18n['Не удалось обновить данные на странице.']}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                }
            }
        });
    },
    queryError: function (response) {
        RecordDeApprover.stop();
        response = JSON.parse(response.responseText);

        if (response.statusText != 'abort') {
            if (typeof response.message != 'undefined') {
                $.notify({message: response.message}, {
                    type: "danger dark",
                    animate: {exit: 'hide'},
                    z_index: 2031
                });
            } else {
                $.notify({message: I18n['Не удалось отменить подтверждение записи.']}, {
                    type: "danger dark",
                    animate: {exit: 'hide'},
                    z_index: 2031
                });
            }
        }
    },
    end: function () {
        RecordDeApprover.ajax = null;
        RecordDeApprover.$modal.modal('hide');

        $.notify({message: I18n['Отмена подтверждения записей завершено.']}, {
            type: "success dark",
            animate: {exit: 'hide'},
            z_index: 2031
        });
    },
    stop: function () {
        if (RecordDeApprover.ajax != null) {
            RecordDeApprover.ajax.abort();
        }

        RecordDeApprover.$modal.find('.row-with-btn-start').show();
        RecordDeApprover.$modal.find('.row-with-text-start').show();
        RecordDeApprover.$modal.find('.row-with-btn-stop').hide();
        RecordDeApprover.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (RecordDeApprover.count) {
            var part = 100 / RecordDeApprover.count;
            var count = RecordDeApprover.count;
            if (RecordDeApprover.offsetCount == null)
                count -= RecordDeApprover.$form.find('input[name="id[]"]').length;
            else count -= RecordDeApprover.offsetCount;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
