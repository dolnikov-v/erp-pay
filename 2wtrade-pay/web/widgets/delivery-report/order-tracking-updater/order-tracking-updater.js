var I18n = I18n || {};

$(function () {
    OrderTrackingUpdater.init();
});

var OrderTrackingUpdater = {
    ajax: null,
    count: 0,
    offsetCount: null,
    $modal: $('#modal_update_tracking'),
    $form: $('#modal_update_tracking').find('form'),
    init: function () {
        $('#start_update_tracking').on('click', OrderTrackingUpdater.start);
        $('#stop_update_tracking').on('click', OrderTrackingUpdater.stop);
    },
    show: function () {
        OrderTrackingUpdater.$modal.modal();
        OrderTrackingUpdater.count = OrderTrackingUpdater.$form.find('input[name="id[]"]').length;
        if (OrderTrackingUpdater.count) {
            OrderTrackingUpdater.$modal.find('.action-record-count').hide();
        }
    },
    start: function () {
        OrderTrackingUpdater.$modal.find('.sender-percent').text('0%');
        OrderTrackingUpdater.$modal.find('.progress-bar').css('width', '0%');

        OrderTrackingUpdater.$modal.find('.row-with-btn-start').hide();
        OrderTrackingUpdater.$modal.find('.row-with-text-start').hide();
        OrderTrackingUpdater.$modal.find('.row-with-btn-stop').show();
        OrderTrackingUpdater.$modal.find('.row-with-text-stop').show();

        OrderTrackingUpdater.count = OrderTrackingUpdater.$form.find('input[name="id[]"]').length;

        if (OrderTrackingUpdater.count) {
            OrderTrackingUpdater.next();
        } else {
            OrderTrackingUpdater.getOffsetCount();
        }
    },
    getOffsetCount: function () {
        var csrfParam = $('meta[name=csrf-param]').attr("content");
        var csrfToken = $('meta[name=csrf-token]').attr("content");
        var dataAjax = {
            getOffsetCount: '1',
            rewrite: OrderTrackingUpdater.$form.find('input[name="rewrite"]').prop('checked') ? 1 : null
        };
        dataAjax[csrfParam] = csrfToken;

        OrderTrackingUpdater.ajax = $.ajax({
            type: 'GET',
            url: OrderTrackingUpdater.$form.data('sender-url'),
            dataType: 'json',
            data: dataAjax,
            success: function (response, textStatus) {
                if (response.status == 'success' && response.offsetCount > 0) {
                    OrderTrackingUpdater.count = response.offsetCount;
                    OrderTrackingUpdater.offsetCount = response.offsetCount;
                    OrderTrackingUpdater.nextOffset();
                } else {
                    OrderTrackingUpdater.stop();
                    $.notify({message: response.message}, {
                        type: "danger dark",
                        animate: {exit: 'hide'},
                        z_index: 2031
                    });
                }
            },
            error: function (response) {
                OrderTrackingUpdater.queryError(response);
            }
        });
    },
    nextOffset: function () {
        if (OrderTrackingUpdater.offsetCount > 0) {
            var percent = OrderTrackingUpdater.calculatePercent();

            OrderTrackingUpdater.$modal.find('.sender-percent').text(percent + '%');
            OrderTrackingUpdater.$modal.find('.progress-bar').css('width', percent + '%');

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {
                rewrite: OrderTrackingUpdater.$form.find('input[name="rewrite"]').prop('checked') ? 1 : null,
                offset: OrderTrackingUpdater.count - OrderTrackingUpdater.offsetCount
            };

            dataAjax[csrfParam] = csrfToken;

            OrderTrackingUpdater.ajax = $.ajax({
                type: 'POST',
                url: OrderTrackingUpdater.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        if (typeof response.errors != 'undefined') {
                            for (var i = 0; i < response.errors.length; i++) {
                                $.notify({message: response.errors[i]}, {
                                    type: "danger dark",
                                    animate: {exit: 'hide'},
                                    z_index: 2031
                                });
                            }
                        }
                        OrderTrackingUpdater.offsetCount--;
                        OrderTrackingUpdater.nextOffset();
                    } else {
                        OrderTrackingUpdater.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    OrderTrackingUpdater.queryError(response);
                }
            });
        } else {
            OrderTrackingUpdater.updateContent();
        }
    },
    next: function () {
        var $id = OrderTrackingUpdater.$form.find('input[name="id[]"]:first');

        if ($id.length) {
            var percent = OrderTrackingUpdater.calculatePercent();

            OrderTrackingUpdater.$modal.find('.sender-percent').text(percent + '%');
            OrderTrackingUpdater.$modal.find('.progress-bar').css('width', percent + '%');

            $id.remove();

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {
                id: $id.val(),
                rewrite: OrderTrackingUpdater.$form.find('input[name="rewrite"]').prop('checked') ? 1 : null
            };

            dataAjax[csrfParam] = csrfToken;

            var $checkbox = $('#records_content input[name="id[]"][value="' + $id.val() + '"]');
            $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
            $checkbox.closest('tr').removeClass('text-danger');

            OrderTrackingUpdater.ajax = $.ajax({
                type: 'POST',
                url: OrderTrackingUpdater.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        if (typeof response.errors != 'undefined') {
                            for (var i = 0; i < response.errors.length; i++) {
                                $.notify({message: response.errors[i]}, {
                                    type: "danger dark",
                                    animate: {exit: 'hide'},
                                    z_index: 2031
                                });
                            }
                        }
                        OrderTrackingUpdater.next();
                    } else {
                        $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                        $checkbox.closest('tr').addClass('text-danger');
                        OrderTrackingUpdater.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    OrderTrackingUpdater.queryError(response);
                }
            });
        } else {
            OrderTrackingUpdater.updateContent();
        }
    },
    updateContent: function () {
        OrderTrackingUpdater.ajax = $.ajax({
            type: 'GET',
            url: OrderTrackingUpdater.$form.data('updater-url'),
            success: function (response, textStatus) {
                if (typeof response.content != 'undefined') {
                    var $table = $('#records_content');

                    $table.replaceWith(response.content);
                    if ($('#records_content').length) {
                        ReportViewReport.init();
                    }
                }
                OrderTrackingUpdater.end();
            },
            error: function (response) {
                OrderTrackingUpdater.end();
                response = JSON.parse(response.responseText);

                if (response.statusText != 'abort') {
                    if (typeof response.message != 'undefined') {
                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    } else {
                        $.notify({message: I18n['Не удалось обновить данные на странице.']}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                }
            }
        });
    },
    queryError: function (response) {
        OrderTrackingUpdater.stop();
        response = JSON.parse(response.responseText);

        if (response.statusText != 'abort') {
            if (typeof response.message != 'undefined') {
                $.notify({message: response.message}, {
                    type: "danger dark",
                    animate: {exit: 'hide'},
                    z_index: 2031
                });
            } else {
                $.notify({message: I18n['Не удалось обновить статус.']}, {
                    type: "danger dark",
                    animate: {exit: 'hide'},
                    z_index: 2031
                });
            }
        }
    },
    end: function () {
        OrderTrackingUpdater.ajax = null;
        OrderTrackingUpdater.$modal.modal('hide');

        $.notify({message: I18n['Обновление трек номеров завершено.']}, {
            type: "success dark",
            animate: {exit: 'hide'},
            z_index: 2031
        });
    },
    stop: function () {
        if (OrderTrackingUpdater.ajax != null) {
            OrderTrackingUpdater.ajax.abort();
        }

        OrderTrackingUpdater.$modal.find('.row-with-btn-start').show();
        OrderTrackingUpdater.$modal.find('.row-with-text-start').show();
        OrderTrackingUpdater.$modal.find('.row-with-btn-stop').hide();
        OrderTrackingUpdater.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (OrderTrackingUpdater.count) {
            var part = 100 / OrderTrackingUpdater.count;
            var count = OrderTrackingUpdater.count;
            if (OrderTrackingUpdater.offsetCount == null)
                count -= OrderTrackingUpdater.$form.find('input[name="id[]"]').length;
            else count -= OrderTrackingUpdater.offsetCount;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
