$(function () {
    ShowerColumns.init();
});

var ShowerColumns = {
    ajax: null,

    init: function () {
        $('.shower-columns-checkbox').on('change', ShowerColumns.toggleColumn);
    },

    toggleColumn: function() {
        if (ShowerColumns.ajax) {
            ShowerColumns.ajax.abort();
        }

        var dataAjax = {
            column: $(this).data('column'),
            choose: $(this).prop('checked') ? 1 : 0
        };

        ShowerColumns.ajax = $.ajax({
            type: 'POST',
            url: location.href,
            dataType: 'json',
            data: dataAjax,
            success: function (response, textStatus) {
                if (typeof response.content != 'undefined') {
                    var $table = $('#records_content');

                    $table.replaceWith(response.content);
                    if ($('#records_content').length) {
                        ReportViewReport.init();
                    }
                }
            }
        });
    }
};
