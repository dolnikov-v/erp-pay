var I18n = I18n || {};

$(function () {
    OrderPriceUpdater.init();
});

var OrderPriceUpdater = {
    ajax: null,
    count: 0,
    offsetCount: null,
    $modal: $('#modal_update_prices'),
    $form: $('#modal_update_prices').find('form'),
    init: function () {
        $('#start_update_prices').on('click', OrderPriceUpdater.start);
        $('#stop_update_prices').on('click', OrderPriceUpdater.stop);
    },
    show: function () {
        OrderPriceUpdater.$modal.modal();
        OrderPriceUpdater.count = OrderPriceUpdater.$form.find('input[name="id[]"]').length;
        if (OrderPriceUpdater.count) {
            OrderPriceUpdater.$modal.find('.action-record-count').hide();
        }
    },
    start: function () {
        OrderPriceUpdater.$modal.find('.sender-percent').text('0%');
        OrderPriceUpdater.$modal.find('.progress-bar').css('width', '0%');

        OrderPriceUpdater.$modal.find('.row-with-btn-start').hide();
        OrderPriceUpdater.$modal.find('.row-with-text-start').hide();
        OrderPriceUpdater.$modal.find('.row-with-btn-stop').show();
        OrderPriceUpdater.$modal.find('.row-with-text-stop').show();

        OrderPriceUpdater.count = OrderPriceUpdater.$form.find('input[name="id[]"]').length;

        if (OrderPriceUpdater.count) {
            OrderPriceUpdater.next();
        } else {
            OrderPriceUpdater.getOffsetCount();
        }
    },
    getOffsetCount: function () {
        var csrfParam = $('meta[name=csrf-param]').attr("content");
        var csrfToken = $('meta[name=csrf-token]').attr("content");
        var dataAjax = {
            getOffsetCount: '1',
            rewrite: OrderPriceUpdater.$form.find('input[name="rewrite"]').prop('checked') ? 1 : null
        };
        dataAjax[csrfParam] = csrfToken;

        OrderPriceUpdater.ajax = $.ajax({
            type: 'GET',
            url: OrderPriceUpdater.$form.data('sender-url'),
            dataType: 'json',
            data: dataAjax,
            success: function (response, textStatus) {
                if (response.status == 'success' && response.offsetCount > 0) {
                    OrderPriceUpdater.count = response.offsetCount;
                    OrderPriceUpdater.offsetCount = response.offsetCount;
                    OrderPriceUpdater.nextOffset();
                } else {
                    OrderPriceUpdater.stop();
                    $.notify({message: response.message}, {
                        type: "danger dark",
                        animate: {exit: 'hide'},
                        z_index: 2031
                    });
                }
            },
            error: function (response) {
                OrderPriceUpdater.queryError(response);
            }
        });
    },
    nextOffset: function () {
        if (OrderPriceUpdater.offsetCount > 0) {
            var percent = OrderPriceUpdater.calculatePercent();

            OrderPriceUpdater.$modal.find('.sender-percent').text(percent + '%');
            OrderPriceUpdater.$modal.find('.progress-bar').css('width', percent + '%');

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {
                rewrite: OrderPriceUpdater.$form.find('input[name="rewrite"]').prop('checked') ? 1 : null,
                offset: OrderPriceUpdater.count - OrderPriceUpdater.offsetCount
            };

            dataAjax[csrfParam] = csrfToken;

            OrderPriceUpdater.ajax = $.ajax({
                type: 'POST',
                url: OrderPriceUpdater.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        if (typeof response.errors != 'undefined') {
                            for (var i = 0; i < response.errors.length; i++) {
                                $.notify({message: response.errors[i]}, {
                                    type: "danger dark",
                                    animate: {exit: 'hide'},
                                    z_index: 2031
                                });
                            }
                        }
                        OrderPriceUpdater.offsetCount--;
                        OrderPriceUpdater.nextOffset();
                    } else {
                        OrderPriceUpdater.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    OrderPriceUpdater.queryError(response);
                }
            });
        } else {
            OrderPriceUpdater.updateContent();
        }
    },
    next: function () {
        var $id = OrderPriceUpdater.$form.find('input[name="id[]"]:first');

        if ($id.length) {
            var percent = OrderPriceUpdater.calculatePercent();

            OrderPriceUpdater.$modal.find('.sender-percent').text(percent + '%');
            OrderPriceUpdater.$modal.find('.progress-bar').css('width', percent + '%');

            $id.remove();

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {
                id: $id.val(),
                rewrite: OrderPriceUpdater.$form.find('input[name="rewrite"]').prop('checked') ? 1 : null
            };

            dataAjax[csrfParam] = csrfToken;

            var $checkbox = $('#records_content input[name="id[]"][value="' + $id.val() + '"]');
            $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
            $checkbox.closest('tr').removeClass('text-danger');

            OrderPriceUpdater.ajax = $.ajax({
                type: 'POST',
                url: OrderPriceUpdater.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        if (typeof response.errors != 'undefined') {
                            for (var i = 0; i < response.errors.length; i++) {
                                $.notify({message: response.errors[i]}, {
                                    type: "danger dark",
                                    animate: {exit: 'hide'},
                                    z_index: 2031
                                });
                            }
                        }
                        OrderPriceUpdater.next();
                    } else {
                        $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                        $checkbox.closest('tr').addClass('text-danger');
                        OrderPriceUpdater.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    OrderPriceUpdater.queryError(response);
                }
            });
        } else {
            OrderPriceUpdater.updateContent();
        }
    },
    updateContent: function () {
        OrderPriceUpdater.ajax = $.ajax({
            type: 'GET',
            url: OrderPriceUpdater.$form.data('updater-url'),
            success: function (response, textStatus) {
                if (typeof response.content != 'undefined') {
                    var $table = $('#records_content');

                    $table.replaceWith(response.content);
                    if ($('#records_content').length) {
                        ReportViewReport.init();
                    }
                }
                OrderPriceUpdater.end();
            },
            error: function (response) {
                OrderPriceUpdater.end();
                response = JSON.parse(response.responseText);

                if (response.statusText != 'abort') {
                    if (typeof response.message != 'undefined') {
                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    } else {
                        $.notify({message: I18n['Не удалось обновить данные на странице.']}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                }
            }
        });
    },
    queryError: function (response) {
        OrderPriceUpdater.stop();
        response = JSON.parse(response.responseText);

        if (response.statusText != 'abort') {
            if (typeof response.message != 'undefined') {
                $.notify({message: response.message}, {
                    type: "danger dark",
                    animate: {exit: 'hide'},
                    z_index: 2031
                });
            } else {
                $.notify({message: I18n['Не удалось обновить цены.']}, {
                    type: "danger dark",
                    animate: {exit: 'hide'},
                    z_index: 2031
                });
            }
        }
    },
    end: function () {
        OrderPriceUpdater.ajax = null;
        OrderPriceUpdater.$modal.modal('hide');

        $.notify({message: I18n['Обновление цен завершено.']}, {
            type: "success dark",
            animate: {exit: 'hide'},
            z_index: 2031
        });
    },
    stop: function () {
        if (OrderPriceUpdater.ajax != null) {
            OrderPriceUpdater.ajax.abort();
        }

        OrderPriceUpdater.$modal.find('.row-with-btn-start').show();
        OrderPriceUpdater.$modal.find('.row-with-text-start').show();
        OrderPriceUpdater.$modal.find('.row-with-btn-stop').hide();
        OrderPriceUpdater.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (OrderPriceUpdater.count) {
            var part = 100 / OrderPriceUpdater.count;
            var count = OrderPriceUpdater.count;
            if (OrderPriceUpdater.offsetCount == null)
                count -= OrderPriceUpdater.$form.find('input[name="id[]"]').length;
            else count -= OrderPriceUpdater.offsetCount;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
