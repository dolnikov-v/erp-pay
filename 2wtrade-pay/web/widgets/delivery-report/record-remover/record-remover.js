var I18n = I18n || {};

$(function () {
    RecordRemover.init();
});

var RecordRemover = {
    ajax: null,
    count: 0,
    offsetCount: null,
    $modal: $('#modal_remove_records'),
    $form: $('#modal_remove_records').find('form'),
    init: function () {
        $('#start_remove_records').on('click', RecordRemover.start);
        $('#stop_remove_records').on('click', RecordRemover.stop);
    },
    show: function () {
        RecordRemover.$modal.modal();
        RecordRemover.count = RecordRemover.$form.find('input[name="id[]"]').length;
        if (RecordRemover.count) {
            RecordRemover.$modal.find('.action-record-count').hide();
        }
    },
    start: function () {
        RecordRemover.$modal.find('.sender-percent').text('0%');
        RecordRemover.$modal.find('.progress-bar').css('width', '0%');

        RecordRemover.$modal.find('.row-with-btn-start').hide();
        RecordRemover.$modal.find('.row-with-text-start').hide();
        RecordRemover.$modal.find('.row-with-btn-stop').show();
        RecordRemover.$modal.find('.row-with-text-stop').show();

        RecordRemover.count = RecordRemover.$form.find('input[name="id[]"]').length;

        if (RecordRemover.count) {
            RecordRemover.next();
        } else {
            RecordRemover.getOffsetCount();
        }
    },
    getOffsetCount: function () {
        var csrfParam = $('meta[name=csrf-param]').attr("content");
        var csrfToken = $('meta[name=csrf-token]').attr("content");
        var dataAjax = {
            getOffsetCount: '1'
        };
        dataAjax[csrfParam] = csrfToken;

        RecordRemover.ajax = $.ajax({
            type: 'GET',
            url: RecordRemover.$form.data('sender-url'),
            dataType: 'json',
            data: dataAjax,
            success: function (response, textStatus) {
                if (response.status == 'success' && response.offsetCount > 0) {
                    RecordRemover.count = response.offsetCount;
                    RecordRemover.offsetCount = response.offsetCount;
                    RecordRemover.nextOffset();
                } else {
                    RecordRemover.stop();
                    $.notify({message: response.message}, {
                        type: "danger dark",
                        animate: {exit: 'hide'},
                        z_index: 2031
                    });
                }
            },
            error: function (response) {
                RecordRemover.queryError(response);
            }
        });
    },
    nextOffset: function () {
        if (RecordRemover.offsetCount > 0) {
            var percent = RecordRemover.calculatePercent();

            RecordRemover.$modal.find('.sender-percent').text(percent + '%');
            RecordRemover.$modal.find('.progress-bar').css('width', percent + '%');

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {};

            dataAjax[csrfParam] = csrfToken;

            RecordRemover.ajax = $.ajax({
                type: 'POST',
                url: RecordRemover.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        RecordRemover.offsetCount--;
                        RecordRemover.nextOffset();
                    } else {
                        RecordRemover.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    RecordRemover.queryError(response);
                }
            });
        } else {
            RecordRemover.updateContent();
        }
    },
    next: function () {
        var $id = RecordRemover.$form.find('input[name="id[]"]:first');

        if ($id.length) {
            var percent = RecordRemover.calculatePercent();

            RecordRemover.$modal.find('.sender-percent').text(percent + '%');
            RecordRemover.$modal.find('.progress-bar').css('width', percent + '%');

            $id.remove();

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {
                id: $id.val()
            };

            dataAjax[csrfParam] = csrfToken;

            var $checkbox = $('#records_content input[name="id[]"][value="' + $id.val() + '"]');
            $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
            $checkbox.closest('tr').removeClass('text-danger');

            RecordRemover.ajax = $.ajax({
                type: 'POST',
                url: RecordRemover.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        $checkbox.closest('tr').remove();
                        RecordRemover.next();
                    } else {
                        $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                        $checkbox.closest('tr').addClass('text-danger');
                        RecordRemover.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    RecordRemover.queryError(response);
                }
            });
        } else {
            RecordRemover.end();
        }
    },
    updateContent: function () {
        RecordRemover.ajax = $.ajax({
            type: 'GET',
            url: RecordRemover.$form.data('updater-url'),
            success: function (response, textStatus) {
                if (typeof response.content != 'undefined') {
                    var $table = $('#records_content');

                    $table.replaceWith(response.content);
                    if ($('#records_content').length) {
                        ReportViewReport.init();
                    }
                }
                RecordRemover.end();
            },
            error: function (response) {
                RecordRemover.end();
                response = JSON.parse(response.responseText);

                if (response.statusText != 'abort') {
                    if (typeof response.message != 'undefined') {
                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    } else {
                        $.notify({message: I18n['Не удалось обновить данные на странице.']}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                }
            }
        });
    },
    queryError: function (response) {
        RecordRemover.stop();
        response = JSON.parse(response.responseText);

        if (response.statusText != 'abort') {
            if (typeof response.message != 'undefined') {
                $.notify({message: response.message}, {
                    type: "danger dark",
                    animate: {exit: 'hide'},
                    z_index: 2031
                });
            } else {
                $.notify({message: I18n['Не удалось удалить запись.']}, {
                    type: "danger dark",
                    animate: {exit: 'hide'},
                    z_index: 2031
                });
            }
        }
    },
    end: function () {
        RecordRemover.ajax = null;
        RecordRemover.$modal.modal('hide');

        $.notify({message: I18n['Удаление записей завершено.']}, {
            type: "success dark",
            animate: {exit: 'hide'},
            z_index: 2031
        });
    },
    stop: function () {
        if (RecordRemover.ajax != null) {
            RecordRemover.ajax.abort();
        }

        RecordRemover.$modal.find('.row-with-btn-start').show();
        RecordRemover.$modal.find('.row-with-text-start').show();
        RecordRemover.$modal.find('.row-with-btn-stop').hide();
        RecordRemover.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (RecordRemover.count) {
            var part = 100 / RecordRemover.count;
            var count = RecordRemover.count;
            if (RecordRemover.offsetCount == null)
                count -= RecordRemover.$form.find('input[name="id[]"]').length;
            else count -= RecordRemover.offsetCount;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
