var I18n = I18n || {};

$(function () {
    CostList.init();
});

var CostList = {
    init: function () {
        $("#modal_cost_list").on('click', 'form :input', function () {
            $(this).closest('form').find('#save-cost').get(0).disabled = false;
        });
        $("#modal_cost_list").on('click', '#delete-cost', this.deleteRow);
        $("#modal_cost_list").on('click', '#save-cost', this.saveCost);
    },
    addNewRow: function ($newRow, parent) {
        $newRow.find('#delete-cost').get(0).disabled = false;
        $newRow.find('#save-cost').get(0).disabled = true;
        $newRow.find('form').attr('id', null);
    },
    deleteRow: function () {
        var form = $(this).parents('form');
        var id = form.find('input[name="DeliveryReportCosts[id]"]').val();
        if (typeof id !== "undefined" && id !== '') {
            $.ajax({
                type: 'POST',
                url: form.attr('data-delete-url'),
                dataType: 'json',
                data: {
                    id: id,
                },
                success: function (response, textStatus) {
                    if (response.status === 'success') {
                        $.notify({message: I18n['Издержки успешно удалены.']}, {
                            type: "success dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                        if (typeof response.data.balance !== 'undefined') {
                            $('.report-balance').html(response.data.balance);
                        }
                        $(this).parents('.changeable-list-element').remove();
                    } else {
                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                }.bind(form),
                error: function (response) {this.printError(response)},
            });
        } else {
            $(this).parents('.changeable-list-element').remove();
        }
    },
    saveCost: function () {
        var form = $(this).parents('form');
        $.ajax({
            type: 'POST',
            url: form.attr('action'),
            dataType: 'json',
            data: form.serialize(),
            success: function (response, textStatus) {
                if (response.status === 'success') {
                    if (typeof response.data === 'object') {
                        if (typeof response.data.balance !== 'undefined') {
                            $('.report-balance').html(response.data.balance);
                        }
                        if (typeof response.data.cost === 'object') {
                            var answerData = response.data.cost;
                            $.each(['id', 'balance', 'sum', 'description'], function (key, val) {
                                if (typeof answerData[val] !== 'undefined') {
                                    form.find('input[name="DeliveryReportCosts[' + val + ']"]').val(answerData[val]);
                                }
                            }.bind(form));
                            if (typeof answerData.id !== 'undefined') {
                                form.attr('action').replace(/(\/\d+(|\/))$/, answerData.id);
                            }
                        }
                    }
                    $.notify({message: I18n['Издержки успешно сохранены.']}, {
                        type: "success dark",
                        animate: {exit: 'hide'},
                        z_index: 2031
                    });
                } else {
                    $.notify({message: response.message}, {
                        type: "danger dark",
                        animate: {exit: 'hide'},
                        z_index: 2031
                    });
                }
            }.bind(form),
            error: function (response) {this.printError(response)},
        });
    },

    //TODO: сделать шаблонным и вынести в общий JS
    printError: function (response) {
        try {
            response = JSON.parse(response.responseText);
            if (response.statusText !== 'abort') {
                if (typeof response.message !== 'undefined') {
                    $.notify({message: response.message}, {
                        type: "danger dark",
                        animate: {exit: 'hide'},
                        z_index: 2031
                    });
                } else {
                    $.notify({message: I18n['Не удалось сохранить информацию.']}, {
                        type: "danger dark",
                        animate: {exit: 'hide'},
                        z_index: 2031
                    });
                }
            }
        } catch (e) {
            $.notify({message: response.responseText}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
        }
    }
};