var I18n = I18n || {};

$(function () {
    OrderStatusUpdater.init();
});

var OrderStatusUpdater = {
    ajax: null,
    count: 0,
    offsetCount: null,
    offset: 0,
    $modal: $('#modal_update_status'),
    $form: $('#modal_update_status').find('form'),
    init: function () {
        $('#start_update_status').on('click', OrderStatusUpdater.start);
        $('#stop_update_status').on('click', OrderStatusUpdater.stop);
    },
    show: function () {
        OrderStatusUpdater.$modal.modal();
        OrderStatusUpdater.count = OrderStatusUpdater.$form.find('input[name="id[]"]').length;
        if (OrderStatusUpdater.count) {
            OrderStatusUpdater.$modal.find('.action-record-count').hide();
        }
    },
    start: function () {
        OrderStatusUpdater.$modal.find('.sender-percent').text('0%');
        OrderStatusUpdater.$modal.find('.progress-bar').css('width', '0%');

        OrderStatusUpdater.$modal.find('.row-with-btn-start').hide();
        OrderStatusUpdater.$modal.find('.row-with-text-start').hide();
        OrderStatusUpdater.$modal.find('.row-with-btn-stop').show();
        OrderStatusUpdater.$modal.find('.row-with-text-stop').show();
        OrderStatusUpdater.offsetCount = 0;
        OrderStatusUpdater.offset = 0;

        OrderStatusUpdater.count = OrderStatusUpdater.$form.find('input[name="id[]"]').length;

        if (OrderStatusUpdater.count) {
            OrderStatusUpdater.next();
        } else {
            OrderStatusUpdater.getOffsetCount();
        }
    },
    getOffsetCount: function () {
        var csrfParam = $('meta[name=csrf-param]').attr("content");
        var csrfToken = $('meta[name=csrf-token]').attr("content");
        var dataAjax = {
            getOffsetCount: '1',
            rewrite: OrderStatusUpdater.$form.find('input[name="rewrite"]').prop('checked') ? 1 : null,
        };
        dataAjax[csrfParam] = csrfToken;

        OrderStatusUpdater.ajax = $.ajax({
            type: 'GET',
            url: OrderStatusUpdater.$form.data('sender-url'),
            dataType: 'json',
            data: dataAjax,
            success: function (response, textStatus) {
                if (response.status == 'success' && response.offsetCount > 0) {
                    OrderStatusUpdater.count = response.offsetCount;
                    OrderStatusUpdater.offsetCount = response.offsetCount;
                    OrderStatusUpdater.nextOffset();
                } else {
                    OrderStatusUpdater.stop();
                    $.notify({message: response.message}, {
                        type: "danger dark",
                        animate: {exit: 'hide'},
                        z_index: 2031
                    });
                }
            },
            error: function (response) {
                OrderStatusUpdater.queryError(response);
            }
        });
    },
    nextOffset: function () {
        if (OrderStatusUpdater.offsetCount > 0) {
            var percent = OrderStatusUpdater.calculatePercent();

            OrderStatusUpdater.$modal.find('.sender-percent').text(percent + '%');
            OrderStatusUpdater.$modal.find('.progress-bar').css('width', percent + '%');

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {
                rewrite: OrderStatusUpdater.$form.find('input[name="rewrite"]').prop('checked') ? 1 : null,
                ignore_balance: OrderStatusUpdater.$form.find('input[name="ignore-balance"]').prop('checked') ? 1 : null,
                offset: OrderStatusUpdater.offset
            };

            dataAjax[csrfParam] = csrfToken;

            OrderStatusUpdater.ajax = $.ajax({
                type: 'POST',
                url: OrderStatusUpdater.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        OrderStatusUpdater.offsetCount--;
                        OrderStatusUpdater.offset = response.offset;
                        if (typeof response.errors != 'undefined') {
                            for (var i = 0; i < response.errors.length; i++) {
                                $.notify({message: response.errors[i]}, {
                                    type: "danger dark",
                                    animate: {exit: 'hide'},
                                    z_index: 2031
                                });
                            }
                        }
                        if(typeof response.report_balance != 'undefined') {
                            $('.report-balance').html(response.report_balance);
                        }
                        OrderStatusUpdater.nextOffset();
                    } else {
                        OrderStatusUpdater.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    OrderStatusUpdater.queryError(response);
                }
            });
        } else {
            OrderStatusUpdater.updateContent();
        }
    },
    next: function () {
        var $id = OrderStatusUpdater.$form.find('input[name="id[]"]:first');

        if ($id.length) {
            var percent = OrderStatusUpdater.calculatePercent();

            OrderStatusUpdater.$modal.find('.sender-percent').text(percent + '%');
            OrderStatusUpdater.$modal.find('.progress-bar').css('width', percent + '%');

            $id.remove();

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {
                id: $id.val(),
                rewrite: OrderStatusUpdater.$form.find('input[name="rewrite"]').prop('checked') ? 1 : null,
                ignore_balance: OrderStatusUpdater.$form.find('input[name="ignore-balance"]').prop('checked') ? 1 : null
            };

            dataAjax[csrfParam] = csrfToken;

            var $checkbox = $('#records_content input[name="id[]"][value="' + $id.val() + '"]');
            $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
            $checkbox.closest('tr').removeClass('text-danger');

            OrderStatusUpdater.ajax = $.ajax({
                type: 'POST',
                url: OrderStatusUpdater.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        if (typeof response.errors != 'undefined') {
                            for (var i = 0; i < response.errors.length; i++) {
                                $.notify({message: response.errors[i]}, {
                                    type: "danger dark",
                                    animate: {exit: 'hide'},
                                    z_index: 2031
                                });
                            }
                        }
                        if(typeof response.report_balance != 'undefined') {
                            $('.report-balance').html(response.report_balance);
                        }
                        OrderStatusUpdater.next();
                    } else {
                        $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                        $checkbox.closest('tr').addClass('text-danger');
                        OrderStatusUpdater.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    OrderStatusUpdater.queryError(response);
                }
            });
        } else {
            OrderStatusUpdater.updateContent();
        }
    },
    updateContent: function () {
        OrderStatusUpdater.ajax = $.ajax({
            type: 'GET',
            url: OrderStatusUpdater.$form.data('updater-url'),
            success: function (response, textStatus) {
                if (typeof response.content != 'undefined') {
                    var $table = $('#records_content');

                    $table.replaceWith(response.content);
                    if ($('#records_content').length) {
                        ReportViewReport.init();
                    }
                }
                OrderStatusUpdater.end();
            },
            error: function (response) {
                OrderStatusUpdater.end();
                response = JSON.parse(response.responseText);

                if (response.statusText != 'abort') {
                    if (typeof response.message != 'undefined') {
                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    } else {
                        $.notify({message: I18n['Не удалось обновить данные на странице.']}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                }
            }
        });
    },
    queryError: function (response) {
        OrderStatusUpdater.stop();
        response = JSON.parse(response.responseText);

        if (response.statusText != 'abort') {
            if (typeof response.message != 'undefined') {
                $.notify({message: response.message}, {
                    type: "danger dark",
                    animate: {exit: 'hide'},
                    z_index: 2031
                });
            } else {
                $.notify({message: I18n['Не удалось обновить статус.']}, {
                    type: "danger dark",
                    animate: {exit: 'hide'},
                    z_index: 2031
                });
            }
        }
    },
    end: function () {
        OrderStatusUpdater.ajax = null;
        OrderStatusUpdater.$modal.modal('hide');

        $.notify({message: I18n['Обновление статусов завершено.']}, {
            type: "success dark",
            animate: {exit: 'hide'},
            z_index: 2031
        });
    },
    stop: function () {
        if (OrderStatusUpdater.ajax != null) {
            OrderStatusUpdater.ajax.abort();
        }

        OrderStatusUpdater.$modal.find('.row-with-btn-start').show();
        OrderStatusUpdater.$modal.find('.row-with-text-start').show();
        OrderStatusUpdater.$modal.find('.row-with-btn-stop').hide();
        OrderStatusUpdater.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (OrderStatusUpdater.count) {
            var part = 100 / OrderStatusUpdater.count;
            var count = OrderStatusUpdater.count;
            if (OrderStatusUpdater.offsetCount == null)
                count -= OrderStatusUpdater.$form.find('input[name="id[]"]').length;
            else count -= OrderStatusUpdater.offsetCount;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
