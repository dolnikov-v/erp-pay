var I18n = I18n || {};

$(function () {
    PaymentIdUpdater.init();
});

var PaymentIdUpdater = {
    ajax: null,
    count: 0,
    offsetCount: null,
    $modal: $('#modal_update_payment_id'),
    $form: $('#modal_update_payment_id').find('form'),
    init: function () {
        $('#start_update_payment_id').on('click', PaymentIdUpdater.start);
        $('#stop_update_payment_id').on('click', PaymentIdUpdater.stop);
    },
    show: function () {
        PaymentIdUpdater.$modal.modal();
    },
    start: function () {
        PaymentIdUpdater.$modal.find('.sender-percent').text('0%');
        PaymentIdUpdater.$modal.find('.progress-bar').css('width', '0%');

        PaymentIdUpdater.$modal.find('.row-with-btn-start').hide();
        PaymentIdUpdater.$modal.find('.row-with-text-start').hide();
        PaymentIdUpdater.$modal.find('.row-with-btn-stop').show();
        PaymentIdUpdater.$modal.find('.row-with-text-stop').show();

        PaymentIdUpdater.getOffsetCount();
    },
    getOffsetCount: function () {
        var csrfParam = $('meta[name=csrf-param]').attr("content");
        var csrfToken = $('meta[name=csrf-token]').attr("content");
        var dataAjax = {
            getOffsetCount: '1',
            payment_id: $('#update_payment_id_input').val(),
            rewrite: PaymentIdUpdater.$form.find('input[name="rewrite"]').prop('checked') ? 1 : null
        };
        dataAjax[csrfParam] = csrfToken;

        PaymentIdUpdater.ajax = $.ajax({
            type: 'GET',
            url: PaymentIdUpdater.$form.data('sender-url'),
            dataType: 'json',
            data: dataAjax,
            success: function (response, textStatus) {
                if (response.status == 'success' && response.offsetCount > 0) {
                    PaymentIdUpdater.count = response.offsetCount;
                    PaymentIdUpdater.offsetCount = response.offsetCount;
                    PaymentIdUpdater.nextOffset();
                } else {
                    PaymentIdUpdater.stop();
                    $.notify({message: response.message}, {
                        type: "danger dark",
                        animate: {exit: 'hide'},
                        z_index: 2031
                    });
                }
            },
            error: function (response) {
                PaymentIdUpdater.queryError(response);
            }
        });
    },
    nextOffset: function () {
        var csrfParam = $('meta[name=csrf-param]').attr("content");
        var csrfToken = $('meta[name=csrf-token]').attr("content");

        var dataAjax = {};

        dataAjax[csrfParam] = csrfToken;

        if (PaymentIdUpdater.offsetCount > 0) {
            var percent = PaymentIdUpdater.calculatePercent();

            PaymentIdUpdater.$modal.find('.sender-percent').text(percent + '%');
            PaymentIdUpdater.$modal.find('.progress-bar').css('width', percent + '%');

            dataAjax['payment_id'] = $('#update_payment_id_input').val();
            dataAjax['offset'] = PaymentIdUpdater.count - PaymentIdUpdater.offsetCount;
            dataAjax['rewrite'] = PaymentIdUpdater.$form.find('input[name="rewrite"]').prop('checked') ? 1 : null;

            PaymentIdUpdater.ajax = $.ajax({
                type: 'POST',
                url: PaymentIdUpdater.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        PaymentIdUpdater.offsetCount--;
                        PaymentIdUpdater.nextOffset();
                    } else {
                        PaymentIdUpdater.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    PaymentIdUpdater.queryError(response);
                }
            });
        } else {

            dataAjax['payment_id'] = $('#update_payment_id_input').val();
            dataAjax['set_to_report'] = '1';

            PaymentIdUpdater.ajax = $.ajax({
                type: 'POST',
                url: PaymentIdUpdater.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        PaymentIdUpdater.end();
                    } else {
                        PaymentIdUpdater.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                    PaymentIdUpdater.updateContent();
                },
                error: function (response) {
                    PaymentIdUpdater.queryError(response);
                }
            });
        }
    },
    updateContent: function () {
        PaymentIdUpdater.ajax = $.ajax({
            type: 'GET',
            url: PaymentIdUpdater.$form.data('updater-url'),
            success: function (response, textStatus) {
                if (typeof response.content != 'undefined') {
                    var $table = $('#records_content');

                    $table.replaceWith(response.content);
                    if ($('#records_content').length) {
                        ReportViewReport.init();
                    }
                }
            },
            error: function (response) {
                response = JSON.parse(response.responseText);

                if (response.statusText != 'abort') {
                    if (typeof response.message != 'undefined') {
                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    } else {
                        $.notify({message: I18n['Не удалось обновить данные на странице.']}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                }
            }
        });
    },
    queryError: function (response) {
        PaymentIdUpdater.stop();
        response = JSON.parse(response.responseText);

        if (response.statusText != 'abort') {
            if (typeof response.message != 'undefined') {
                $.notify({message: response.message}, {
                    type: "danger dark",
                    animate: {exit: 'hide'},
                    z_index: 2031
                });
            } else {
                $.notify({message: I18n['Не удалось обновить номер платежа.']}, {
                    type: "danger dark",
                    animate: {exit: 'hide'},
                    z_index: 2031
                });
            }
        }
    },
    end: function () {
        PaymentIdUpdater.ajax = null;
        PaymentIdUpdater.$modal.modal('hide');

        $.notify({message: I18n['Обновление номера платежа завершено.']}, {
            type: "success dark",
            animate: {exit: 'hide'},
            z_index: 2031
        });
    },
    stop: function () {
        if (PaymentIdUpdater.ajax != null) {
            PaymentIdUpdater.ajax.abort();
        }

        PaymentIdUpdater.$modal.find('.row-with-btn-start').show();
        PaymentIdUpdater.$modal.find('.row-with-text-start').show();
        PaymentIdUpdater.$modal.find('.row-with-btn-stop').hide();
        PaymentIdUpdater.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (PaymentIdUpdater.count) {
            var part = 100 / PaymentIdUpdater.count;
            var count = PaymentIdUpdater.count;
            count -= PaymentIdUpdater.offsetCount;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
