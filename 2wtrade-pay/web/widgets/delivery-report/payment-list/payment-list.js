const USD = 1;
const EUR = 8;

var I18n = I18n || {};

$(function () {
    PaymentList.init();
});

$('#report_costs_currency').on('change', function(){
    $('#total_report_rate').prop('disabled', [USD,EUR].indexOf(parseInt($(this).val())) == -1);

    if([USD,EUR].indexOf(parseInt($(this).val())) == -1){
        $('#total_report_rate').val('');
    }
});

var PaymentList = {
    $ajax: null,
    $modal: $('#modal_payment_list'),
    $form: $('#modal_payment_list').find('form'),
    init: function () {
        $('#total_report_rate').prop('disabled', [USD,EUR].indexOf(parseInt($('#report_costs_currency').val())) == -1);
        $('#start_adding_payment').on('click', PaymentList.addPayment);
        $('.remove-payment').on('click', PaymentList.removePayment);
        $('#change_report_costs').on('click', PaymentList.changeReportCosts);
    },
    show: function () {
        $('#total_report_rate').val('');
        PaymentList.$modal.modal();
    },
    addPayment: function (e) {
        e.stopImmediatePropagation();
        $('.remove-payment').attr('disabled', '');
        $('#change_report_costs').attr('disabled', '');
        PaymentList.$ajax = $.ajax({
            type: 'POST',
            url: PaymentList.$form.data('sender-url'),
            dataType: 'json',
            data: PaymentList.$form.serialize(),
            success: function (response) {
                if (response.status == 'success') {
                    $('#paymentorder-name').val('');
                    $('#paymentorder-sum').val('');
                    PaymentList.$form.find('.changeable-list > .changeable-list-element.row:not(.changeable-list-template)').remove();
                    $('[name*="PaymentCurrencyRate"]').each(function (i, el) {
                        if ($(el).attr('data-plugin') == 'select2') {
                            $(el).val(1);
                            $(el).parent().find('.select2').remove();
                            $(el).select2();
                        } else {
                            $(el).val('');
                        }
                    });

                    if (typeof response.data.content != 'undefined') {
                        $('.payment-list-content').html(response.data.content);
                        PaymentList.init();
                    }
                    if (typeof response.data.report_balance != 'undefined') {
                        $('.report-balance').html(response.data.report_balance);
                    }
                    $.notify({message: I18n['Платеж прикреплен.']}, {
                        type: "success dark",
                        animate: {exit: 'hide'},
                        z_index: 2031
                    });
                }
                else {
                    $.notify({message: response.message}, {
                        type: "danger dark",
                        animate: {exit: 'hide'},
                        z_index: 2031
                    });
                }
                PaymentList.stop();
            },
            error: function (response) {
                PaymentList.stop();
                response = JSON.parse(response.responseText);

                if (response.statusText != 'abort') {
                    if (typeof response.message != 'undefined') {
                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    } else {
                        $.notify({message: I18n['Не удалось прикрепить платеж.']}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                }
            }
        });
    },
    removePayment: function (e) {
        $('.remove-payment').attr('disabled', '');
        $('#start_adding_payment').attr('disabled', '');
        $('#change_report_costs').attr('disabled', '');
        PaymentList.$ajax = $.ajax({
            type: 'GET',
            url: $(this).data('url'),
            dataType: 'json',
            success: function (response) {
                if (response.status == 'success') {
                    if (typeof response.data.content != 'undefined') {
                        $('.payment-list-content').html(response.data.content);
                        PaymentList.init();
                    }
                    if (typeof response.data.report_balance != 'undefined') {
                        $('.report-balance').html(response.data.report_balance);
                    }
                    $.notify({message: I18n['Платеж удален.']}, {
                        type: "success dark",
                        animate: {exit: 'hide'},
                        z_index: 2031
                    });
                } else {
                    $.notify({message: response.message}, {
                        type: "danger dark",
                        animate: {exit: 'hide'},
                        z_index: 2031
                    });
                }
                PaymentList.stopRemoving();
            },
            error: function (response) {
                PaymentList.stopRemoving();
                response = JSON.parse(response.responseText);

                if (response.statusText != 'abort') {
                    if (typeof response.message != 'undefined') {
                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    } else {
                        $.notify({message: I18n['Не удалось удалить платеж.']}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                }
            }
        });
    },
    changeReportCosts: function () {
        $('.remove-payment').attr('disabled', '');
        $('#start_adding_payment').attr('disabled', '');
        var data = {
            total_costs: $('#total_report_costs').val(),
            currency: $('#report_costs_currency').val(),
            rate: $('#total_report_rate').val()
        };
        var csrfParam = $('meta[name=csrf-param]').attr("content");
        data[csrfParam] = $('meta[name=csrf-token]').attr("content");
        PaymentList.$ajax = $.ajax({
            type: 'POST',
            url: PaymentList.$form.data('change-costs-url'),
            dataType: 'json',
            data: data,
            success: function (response) {
                if (response.status == 'success') {
                    if (typeof response.data.report_balance != 'undefined') {
                        $('.report-balance').html(response.data.report_balance);
                    }
                    if (typeof response.data.sum_total != 'undefined') {
                        $('#sum_total').val(response.data.sum_total)
                    }
                    $.notify({message: response.message}, {
                        type: "success dark",
                        animate: {exit: 'hide'},
                        z_index: 2031
                    });
                } else {
                    $.notify({message: response.message}, {
                        type: "danger dark",
                        animate: {exit: 'hide'},
                        z_index: 2031
                    });
                }
                PaymentList.stopChangingCosts();
            },
            error: function (response) {
                PaymentList.stopChangingCosts();
                response = JSON.parse(response.responseText);

                if (response.statusText != 'abort') {
                    if (typeof response.message != 'undefined') {
                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    } else {
                        $.notify({message: I18n['Не удалось изменить значения издержек.']}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                }
            }
        });
    },
    stop: function () {
        if (PaymentList.$ajax != null) {
            PaymentList.$ajax.abort();
        }
        $('#start_adding_payment').removeAttr('disabled');
        $('.remove-payment').removeAttr('disabled');
        $('#start_adding_payment').removeAttr('data-loading');
        $('#change_report_costs').removeAttr('disabled');
    },
    stopRemoving: function () {
        if (PaymentList.$ajax != null) {
            PaymentList.$ajax.abort();
        }
        $('#start_adding_payment').removeAttr('disabled');
        $('.remove-payment').removeAttr('disabled');
        $('.remove-payment').removeAttr('data-loading');
        $('#change_report_costs').removeAttr('disabled');
    },
    stopChangingCosts: function () {
        if (PaymentList.$ajax != null) {
            PaymentList.$ajax.abort();
        }
        $('#start_adding_payment').removeAttr('disabled');
        $('.remove-payment').removeAttr('disabled');
        $('#change_report_costs').removeAttr('disabled');
        $('#change_report_costs').removeAttr('data-loading');
    }
};
