var I18n = I18n || {};

$(function () {
    RemovePretension.init();
});

var RemovePretension = {
    ajax: null,
    count: 0,
    offsetCount: null,
    $modal: $('#modal_remove_pretension'),
    $form: $('#modal_remove_pretension').find('form'),
    init: function () {
        $('#start_remove_pretension').on('click', RemovePretension.start);
        $('#stop_remove_pretension').on('click', RemovePretension.stop);
    },
    show: function () {
        RemovePretension.$modal.modal();
        RemovePretension.count = RemovePretension.$form.find('input[name="id[]"]').length;
        if (RemovePretension.count) {
            RemovePretension.$modal.find('.action-record-count').hide();
        }
    },
    start: function () {
        RemovePretension.$modal.find('.sender-percent').text('0%');
        RemovePretension.$modal.find('.progress-bar').css('width', '0%');

        RemovePretension.$modal.find('.row-with-btn-start').hide();
        RemovePretension.$modal.find('.row-with-text-start').hide();
        RemovePretension.$modal.find('.row-with-btn-stop').show();
        RemovePretension.$modal.find('.row-with-text-stop').show();

        RemovePretension.count = RemovePretension.$form.find('input[name="id[]"]').length;

        if (RemovePretension.count) {
            RemovePretension.next();
        } else {
            RemovePretension.getOffsetCount();
        }
    },
    getOffsetCount: function () {
        var csrfParam = $('meta[name=csrf-param]').attr("content");
        var csrfToken = $('meta[name=csrf-token]').attr("content");
        var dataAjax = {
            getOffsetCount: '1'
        };
        dataAjax[csrfParam] = csrfToken;

        RemovePretension.ajax = $.ajax({
            type: 'GET',
            url: RemovePretension.$form.data('sender-url'),
            dataType: 'json',
            data: dataAjax,
            success: function (response, textStatus) {
                if (response.status == 'success' && response.offsetCount > 0) {
                    RemovePretension.count = response.offsetCount;
                    RemovePretension.offsetCount = response.offsetCount;
                    RemovePretension.nextOffset();
                } else {
                    RemovePretension.stop();
                    $.notify({message: response.message}, {
                        type: "danger dark",
                        animate: {exit: 'hide'},
                        z_index: 2031
                    });
                }
            },
            error: function (response) {
                RemovePretension.queryError(response);
            }
        });
    },
    nextOffset: function () {
        if (RemovePretension.offsetCount > 0) {
            var percent = RemovePretension.calculatePercent();

            RemovePretension.$modal.find('.sender-percent').text(percent + '%');
            RemovePretension.$modal.find('.progress-bar').css('width', percent + '%');

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {};

            dataAjax[csrfParam] = csrfToken;

            RemovePretension.ajax = $.ajax({
                type: 'POST',
                url: RemovePretension.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        RemovePretension.offsetCount--;
                        RemovePretension.nextOffset();
                    } else {
                        RemovePretension.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    RemovePretension.queryError(response);
                }
            });
        } else {
            RemovePretension.updateContent();
        }
    },
    next: function () {
        var $id = RemovePretension.$form.find('input[name="id[]"]:first');

        if ($id.length) {
            var percent = RemovePretension.calculatePercent();

            RemovePretension.$modal.find('.sender-percent').text(percent + '%');
            RemovePretension.$modal.find('.progress-bar').css('width', percent + '%');

            $id.remove();

            var csrfParam = $('meta[name=csrf-param]').attr("content");
            var csrfToken = $('meta[name=csrf-token]').attr("content");

            var dataAjax = {
                id: $id.val()
            };

            dataAjax[csrfParam] = csrfToken;

            var $checkbox = $('#records_content input[name="id[]"][value="' + $id.val() + '"]');
            $checkbox.closest('.checkbox-custom').removeClass('checkbox-danger');
            $checkbox.closest('tr').removeClass('text-danger');

            RemovePretension.ajax = $.ajax({
                type: 'POST',
                url: RemovePretension.$form.data('sender-url'),
                dataType: 'json',
                data: dataAjax,
                success: function (response, textStatus) {
                    if (response.status == 'success') {
                        $checkbox.closest('tr').removeClass('text-danger');
                        $checkbox.prop('checked', false);
                        RemovePretension.next();
                    } else {
                        $checkbox.closest('.checkbox-custom').addClass('checkbox-danger');
                        $checkbox.closest('tr').addClass('text-danger');
                        RemovePretension.stop();

                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                },
                error: function (response) {
                    RemovePretension.queryError(response);
                }
            });
        } else {
            RemovePretension.end();
        }
    },
    updateContent: function () {
        RemovePretension.ajax = $.ajax({
            type: 'GET',
            url: RemovePretension.$form.data('updater-url'),
            success: function (response, textStatus) {
                if (typeof response.content != 'undefined') {
                    var $table = $('#records_content');

                    $table.replaceWith(response.content);
                    if ($('#records_content').length) {
                        ReportViewReport.init();
                    }
                }
                RemovePretension.end();
            },
            error: function (response) {
                RemovePretension.end();
                response = JSON.parse(response.responseText);

                if (response.statusText != 'abort') {
                    if (typeof response.message != 'undefined') {
                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    } else {
                        $.notify({message: I18n['Не удалось обновить данные на странице.']}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                }
            }
        });
    },
    queryError: function (response) {
        RemovePretension.stop();
        response = JSON.parse(response.responseText);

        if (response.statusText != 'abort') {
            if (typeof response.message != 'undefined') {
                $.notify({message: response.message}, {
                    type: "danger dark",
                    animate: {exit: 'hide'},
                    z_index: 2031
                });
            } else {
                $.notify({message: I18n['Не удалось удалить запись.']}, {
                    type: "danger dark",
                    animate: {exit: 'hide'},
                    z_index: 2031
                });
            }
        }
    },
    end: function () {
        RemovePretension.ajax = null;
        RemovePretension.$modal.modal('hide');

        $.notify({message: I18n['Удаление записей завершено.']}, {
            type: "success dark",
            animate: {exit: 'hide'},
            z_index: 2031
        });
    },
    stop: function () {
        if (RemovePretension.ajax != null) {
            RemovePretension.ajax.abort();
        }

        RemovePretension.$modal.find('.row-with-btn-start').show();
        RemovePretension.$modal.find('.row-with-text-start').show();
        RemovePretension.$modal.find('.row-with-btn-stop').hide();
        RemovePretension.$modal.find('.row-with-text-stop').hide();
    },

    calculatePercent: function () {
        var percent = 0;

        if (RemovePretension.count) {
            var part = 100 / RemovePretension.count;
            var count = RemovePretension.count;
            if (RemovePretension.offsetCount == null)
                count -= RemovePretension.$form.find('input[name="id[]"]').length;
            else count -= RemovePretension.offsetCount;

            percent = parseInt(count * part);

            if (percent > 100) {
                percent = 100;
            }
        }

        return percent;
    }
};
