$(function () {
    $('.container-storageproduct .storage').on('change', Product.change);
    $('.container-storageproduct .product').on('change', ShelfLife.change);
});

var Product = {
    change: function () {
        var $self = $(this);
        var $selfParent = $(this).parent();
        $.ajax({
            type: 'POST',
            url: '/storage/storage-product/get-select-options-product',
            data: {
                storage_id: $self.find('select').val()
            },
            success: function (response) {
                $selfParent.find('.product select').empty().html(response);
                $selfParent.find('.product select').removeAttr('disabled');
            },
            error: function () {
                $selfParent.find('.product select').attr('disabled', '');

            }
        });

        $selfParent.find('.product .selection span span:first').empty().html('—');
        $selfParent.find('.product .selection span span:first').attr('title', '—');
    }
};

var ShelfLife = {
    change: function (event) {
        var $self = $(event.target);
        var $selfParent = $self.offsetParent();
        $.ajax({
            type: 'POST',
            url: '/storage/storage-product/get-select-options-shelf-life',
            data: {
                product_id: $selfParent.find('.product select').val(),
                storage_id: $selfParent.find('.storage select').val()
            },
            success: function (response) {
                $selfParent.find('.productShelfLife select').empty().html(response);
                $selfParent.find('.productShelfLife select').removeAttr('disabled');
                $selfParent.find('.productShelfLife .selection span span:first').empty().html($selfParent.find('.productShelfLife select option').get(0).innerHTML);
                $selfParent.find('.productShelfLife .selection span span:first').attr('title', $selfParent.find('.productShelfLife select option').get(0).innerHTML);
            },
            error: function () {
                $selfParent.find('.productShelfLife select').empty();
                $selfParent.find('.productShelfLife select').attr('disabled', '');
                $selfParent.find('.productShelfLife .selection span span:first').empty().html('—');
                $selfParent.find('.productShelfLife .selection span span:first').attr('title', '—');
            }
        });
    }
};