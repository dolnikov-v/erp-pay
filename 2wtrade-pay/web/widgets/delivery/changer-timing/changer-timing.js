$(function () {
    ChangerTiming.init();
});

var ChangerTiming = {
    init: function () {
        $('.input-timing').each(function () {
            $(this).datetimepicker({
                format: $(this).data('format')
            });
        });

        ChangerTiming.initDatetimepickers();

        $('.add-changer-timing').on('click', ChangerTiming.addTiming);
        $(document).on('click', '.delete-changer-timing', ChangerTiming.deleteTiming);
    },

    initDatetimepickers: function () {
        $('#row_delivery_timings .input-timing.no-init').each(function () {
            $(this).removeClass('no-init');

            $(this).datetimepicker({
                format: $(this).data('format')
            });
        });
    },

    addTiming: function () {
        var $root = $('#row_delivery_timings');
        $(this).closest('.row-delivery-timing').clone().appendTo($root);

        $root.find('.add-changer-timing').remove();
        $root.find('.delete-changer-timing').show();

        ChangerTiming.initDatetimepickers();
    },

    deleteTiming: function () {
        $(this).closest('.row-delivery-timing').remove();
    }
};
