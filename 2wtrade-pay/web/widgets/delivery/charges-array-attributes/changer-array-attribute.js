function changeStatusPercentBuyout() {
    var parentContainer = $(this).parents('div.row-delivery-charges-for-region').get(0);
    var rows = $(parentContainer).find('.charges-container');
    rows.toggleClass('hide');
}
function plusPercentBuyout() {
    var parentContainer = $(this).parents('div.row-delivery-charges-for-region').get(0);
    var firstContainer = $(parentContainer).find('div.charges-array-attr-default').first().clone();
    $(firstContainer).find('input, select').each(function (id, element) {
        if (element.name.charAt(0) == '_') {
            $(element).attr('name', element.name.substring(1))
        }
    });
    $(firstContainer).find('input').val(null);
    $(firstContainer).find('select[data-plugin="select2"]').each(function () {
        $(this).parent().find('.select2').remove();
        $(this).select2();
    });
    $(firstContainer).removeClass('hide');
    $(parentContainer).find('.charges-container .charges-panel').before(firstContainer);
}
function minusPercentBuyout() {
    var parentContainer = $(this).parents('div.row').get(0);
    parentContainer.remove();
}

$(function () {
    $('.group-delivery-charges-for-region').on('click', '.btn-percent-buyout-minus', minusPercentBuyout);
    $('.group-delivery-charges-for-region').on('click', '.btn-percent-buyout-plus', plusPercentBuyout);
    $('.group-delivery-charges-for-region').on('change', '[name*=enable_percent_buyout]', changeStatusPercentBuyout);
});
