$(function () {
    ManageRequisites.init();
});

var ManageRequisites = {
    init: function () {
        $('.btn-delivery-requisites-plus').off('click');
        $('.btn-delivery-requisites-minus').off('click');

        $('.btn-delivery-requisites-plus').on('click', ManageRequisites.addLine);
        $('.btn-delivery-requisites-minus').on('click', ManageRequisites.removeLine);

        if ($('.group-requisites .row').length > 1) {
            $('#no_requisites').hide();
        } else {
            $('#no_requisites').show();
        }
    },
    addLine: function () {
        var option = $('#select-delivery-requisites-plus option:selected');

        if (option.prop('disabled') == true) {
            return false;
        }

        var last = $('.group-requisites .row').last();
        var copy = $('.group-requisites .row').last().clone();
        copy.appendTo('.group-requisites').show();

        last.insertAfter(copy);

        option.prop('disabled', true);
        if (option.next()) {
            option.next().prop('selected', true);
        }

        copy.find('input').val(option.val());
        copy.find('.col-id').text(option.val());
        copy.find('.col-name').text(option.text());

        $("#select-delivery-requisites-plus").select2();

        ManageRequisites.init();
    },
    removeLine: function () {
        $(this).parents().each(function () {
            if ($(this).hasClass('row')) {
                var id = $(this).find('input').eq(0).val();
                $("#select-delivery-requisites-plus [value='" + id + "']").prop('disabled', false);
                $("#select-delivery-requisites-plus").select2();
                $(this).remove();
            }
        });

        ManageRequisites.init();
    }
};
