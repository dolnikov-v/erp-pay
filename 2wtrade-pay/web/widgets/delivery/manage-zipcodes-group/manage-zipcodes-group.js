$(function () {
    ManageZipcodesGroup.init();
    DeleteZipCodes.init();
    DeleteCities.init();
});

var ManageZipcodesGroup = {

    $modalDeleteZipCodes: $('#modal_delete_zip_codes'),
    $modalDeleteCities: $('#modal_delete_cities'),
    $selectedTextarea: null,
    $selectedZipCodesId: null,

    init: function () {
        $('.btn-delivery-zipcodes-plus').off('click');
        $('.btn-delivery-zipcodes-minus').off('click');
        $('.btn-delivery-zipcodes-open-modal').off('click');
        $('.btn-delivery-cities-open-modal').off('click');

        $('.btn-delivery-zipcodes-plus').on('click', ManageZipcodesGroup.addInput);
        $('.btn-delivery-zipcodes-minus').on('click', ManageZipcodesGroup.removeInput);
        $('.btn-delivery-zipcodes-open-modal').on('click', ManageZipcodesGroup.openModal);
        $('.btn-delivery-cities-open-modal').on('click', ManageZipcodesGroup.openModalCities);

        if ($('.group-zipcodes-group .row-delivery-zipcodes').find('.btn-delivery-zipcodes-minus').size() == 1) {
            $('.btn-delivery-zipcodes-minus').hide();
        } else {
            $('.btn-delivery-zipcodes-minus').show();
        }
    },
    addInput: function () {
        $('.group-zipcodes-group .row-delivery-zipcodes').last().clone().appendTo('.group-zipcodes-group');

        $('.group-zipcodes-group .row-delivery-zipcodes').last().find('input, textarea').each(function () {
            $(this).val('');
        });

        $('.group-zipcodes-group .row-delivery-zipcodes').last().find('select').each(function () {
            $(this).select2();
            $(this).parent().find('.select2').css('width', '100%');
        });
        ManageZipcodesGroup.init();
    },
    removeInput: function () {
        $(this).parents().each(function () {
            if ($(this).hasClass('row-delivery-zipcodes')) {
                $(this).remove();
            }
        });

        ManageZipcodesGroup.init();
    },
    openModal: function () {
        $(this).parents().each(function () {
            if ($(this).hasClass('row-delivery-zipcodes')) {
                $selectedTextarea = $(this).find('textarea[name^="DeliveryZipcodes[zipcodes]"]');
                $selectedZipCodesId = $(this).find('.delivery-zipcodes-id').val();
                $("#zip-codes-to-delete").val('');
                ManageZipcodesGroup.$modalDeleteZipCodes.modal();
            }
        });
    },
    openModalCities: function () {
        $(this).parents().each(function () {
            if ($(this).hasClass('row-delivery-zipcodes')) {
                $selectedTextarea = $(this).find('textarea[name^="DeliveryZipcodes[cities]"]');
                $selectedZipCodesId = $(this).find('.delivery-zipcodes-id').val();
                $("#cities-to-delete").val('');
                ManageZipcodesGroup.$modalDeleteCities.modal();
            }
        });
    }
};

var delete_zip_codes = $('#modal_delete_zip_codes'),
    DeleteZipCodes = {
        $modal: delete_zip_codes,
        $form: delete_zip_codes.find('form'),

        init: function () {
            $('#start_delete_zip_codes').on('click', DeleteZipCodes.start);
        },

        start: function () {

            DeleteZipCodes.$modal.find('.row-with-btn-start').hide();

            var newVal = $selectedTextarea.val();

            var toDel = $("#zip-codes-to-delete").val();
            if (toDel.length) {
                var str_array = toDel.split(',');
                for (var i = 0; i < str_array.length; i++) {
                    newVal = newVal.replace(new RegExp('(,|^)' + str_array[i].replace(' ', '') + '(,|$)', 'g'), ',');
                }
            }
            newVal = newVal.replace(new RegExp(',+', 'g'), ',');
            newVal = newVal.replace(new RegExp('(^,|\D|\s)'), '');

            $.ajax({
                type: 'POST',
                url: '/delivery/control/set-zip-codes',
                dataType: 'json',
                data: {
                    delivery_id: $("#zip-codes-to-delete").data('delivery'),
                    zipcodes_id: $selectedZipCodesId,
                    zip: newVal
                },
                success: function () {
                    DeleteZipCodes.$modal.find('.row-with-btn-start').show();
                    $selectedTextarea.val(newVal);
                    DeleteZipCodes.ajax = null;
                    DeleteZipCodes.$modal.modal('hide');
                    $.notify({message: I18n['Удаление индексов завершено.']}, {
                        type: "success dark",
                        animate: {exit: 'hide'},
                        z_index: 2031
                    });
                },
                error: function () {
                    DeleteZipCodes.$modal.find('.row-with-btn-start').show();
                    $.notify({message: I18n['Не удалось удалить индексы.']}, {
                        type: "danger dark",
                        animate: {exit: 'hide'},
                        z_index: 2031
                    });

                }
            });
        }
    };

var delete_cities = $('#modal_delete_cities'),
    DeleteCities = {
        $modal: delete_cities,
        $form: delete_cities.find('form'),

        init: function () {
            $('#start_delete_cities').on('click', DeleteCities.start);
        },
        start: function () {
            DeleteCities.$modal.find('.row-with-btn-start').hide();
            var newVal = $selectedTextarea.val();
            var toDel = $("#cities-to-delete").val();
            if (toDel.length) {
                var str_array = toDel.split(',');
                for (var i = 0; i < str_array.length; i++) {
                    newVal = newVal.replace(new RegExp('(,|^)' + str_array[i].replace(' ', '') + '(,|$)', 'g'), ',');
                }
            }
            newVal = newVal.replace(new RegExp(',+', 'g'), ',');
            newVal = newVal.replace(new RegExp('(^,|\D|\s)'), '');

            $.ajax({
                type: 'POST',
                url: '/delivery/control/set-cities',
                dataType: 'json',
                data: {
                    delivery_id: $("#cities-to-delete").data('delivery'),
                    zipcodes_id: $selectedZipCodesId,
                    cities: newVal
                },
                success: function () {
                    DeleteCities.$modal.find('.row-with-btn-start').show();
                    $selectedTextarea.val(newVal);
                    DeleteCities.ajax = null;
                    DeleteCities.$modal.modal('hide');
                    $.notify({message: I18n['Удаление городов завершено.']}, {
                        type: "success dark",
                        animate: {exit: 'hide'},
                        z_index: 2031
                    });
                },
                error: function () {
                    DeleteCities.$modal.find('.row-with-btn-start').show();
                    $.notify({message: I18n['Не удалось удалить города.']}, {
                        type: "danger dark",
                        animate: {exit: 'hide'},
                        z_index: 2031
                    });

                }
            });
        }
    };
