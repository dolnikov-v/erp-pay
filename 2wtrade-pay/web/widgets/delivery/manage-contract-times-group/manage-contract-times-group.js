$(function () {
    ManageContractTimesGroup.init();
});

var ManageContractTimesGroup = {

    $selectedTextarea: null,
    $selectedContractTimesId: null,

    init: function () {
        $('.btn-delivery-contract-times-plus').off('click');
        $('.btn-delivery-contract-times-minus').off('click');

        $('.btn-delivery-contract-times-plus').on('click', ManageContractTimesGroup.addInput);
        $('.btn-delivery-contract-times-minus').on('click', ManageContractTimesGroup.removeInput);

        if ($('.group-contract-times-group .row-delivery-contract-times').find('.btn-delivery-contract-times-minus').size() == 1) {
            $('.btn-delivery-contract-times-minus').hide();
        } else {
            $('.btn-delivery-contract-times-minus').show();
        }
    },
    addInput: function () {
        $('.group-contract-times-group .row-delivery-contract-times').last().clone().appendTo('.group-contract-times-group');

        $('.group-contract-times-group .row-delivery-contract-times').last().find('input, textarea').each(function () {
            $(this).val('');
        });

        $('.group-contract-times-group .row-delivery-contract-times').last().find('select').each(function () {
            var name = this.name;
            $(this).parent().find('.select2').remove();
            $(this).val('');
            $(this).select2();
            $(this).parent().find('.select2').css('width', '100%');
        });
        ManageContractTimesGroup.init();
    },
    removeInput: function () {
        $(this).parents().each(function () {
            if ($(this).hasClass('row-delivery-contract-times')) {
                $(this).remove();
            }
        });
        ManageContractTimesGroup.init();
    }
};