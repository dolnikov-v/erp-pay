var I18n = I18n || {};
$(function () {
    ManageDeliveryChargesForRegion.init();
});

var ManageDeliveryChargesForRegion = {
    $removeButtonClass: '.btn-delivery-charges-for-region-minus',
    $addButtonClass: '.btn-delivery-charges-by-region-plus',
    $groupClass: '.group-delivery-charges-for-region',
    $rowClass: '.row-delivery-charges-for-region',
    $headerClass: '.row-header-delivery-charges-for-region',
    $formClass: '.charges-form',
    $loadChargesButton: '#btn_upload_charges_for_region_from_file',
    $loadChargesModal: '#modal_upload_charges_for_region_from_file',
    $startLoadChargesButton: '#btn_start_upload_charges_for_region_from_file',
    modalLocked: false,

    init: function () {
        $(ManageDeliveryChargesForRegion.$addButtonClass).on('click', ManageDeliveryChargesForRegion.add);
        $(ManageDeliveryChargesForRegion.$removeButtonClass).on('click', ManageDeliveryChargesForRegion.remove);
        $(ManageDeliveryChargesForRegion.$loadChargesButton).on('click', ManageDeliveryChargesForRegion.showLoadChargesModal);
        $(ManageDeliveryChargesForRegion.$loadChargesModal).on('hide.bs.modal', ManageDeliveryChargesForRegion.hideLoadChargesModal);
        $(ManageDeliveryChargesForRegion.$startLoadChargesButton).on('click', ManageDeliveryChargesForRegion.startLoadChargesModal);
    },
    remove: function (event) {
        $(this).closest(ManageDeliveryChargesForRegion.$rowClass).remove();
        event.stopImmediatePropagation();
        ManageDeliveryChargesForRegion.updateZoneNumbers();
        ManageDeliveryChargesForRegion.autosaveCharges();
    },
    add: function (event) {
        ManageDeliveryChargesForRegion.autosaveCharges();
        $(ManageDeliveryChargesForRegion.$groupClass).append($(ManageDeliveryChargesForRegion.$groupClass).data('template'));
        ManageDeliveryChargesForRegion.init();
        event.stopImmediatePropagation();
        ManageDeliveryChargesForRegion.updateZoneNumbers();
    },
    updateZoneNumbers: function () {
        $(ManageDeliveryChargesForRegion.$headerClass).each(function (index) {
            var str = I18n['Регион #{number}'];
            str = str.replace('{number}', (index + 1));
            $(this).text(str);
        });
    },
    autosaveCharges: function () {
        var form = $(ManageDeliveryChargesForRegion.$formClass);
        $.ajax({
            url: form.attr('action'),
            type: 'POST',
            data: form.serialize(),
            success: function (response) {
                if (response.status == 'success') {
                    MainBootstrap.addNotificationSuccess(response.message);
                } else {
                    MainBootstrap.addNotificationDanger(response.message);
                }
            },
            error: function (response) {
                response = JSON.parse(response.responseText);

                if (response.statusText != 'abort') {
                    if (typeof response.message != 'undefined') {
                        MainBootstrap.addNotificationDanger(response.message);
                    } else {
                        MainBootstrap.addNotificationDanger(I18n['Не удалось произвести автосохранение тарифов.']);
                    }
                }
            }
        });
    },
    showLoadChargesModal: function () {
        $(ManageDeliveryChargesForRegion.$loadChargesModal).modal();
        $(ManageDeliveryChargesForRegion.$startLoadChargesButton).removeAttr('data-loading');
        $(ManageDeliveryChargesForRegion.$loadChargesModal).find('button').removeAttr('disabled');
        ManageDeliveryChargesForRegion.modalLocked = false;
    },
    hideLoadChargesModal: function (e) {
        if (ManageDeliveryChargesForRegion.modalLocked) {
            e.preventDefault();
            return;
        }
        $(ManageDeliveryChargesForRegion.$loadChargesButton).removeAttr('disabled');
        $(ManageDeliveryChargesForRegion.$loadChargesButton).removeAttr('data-loading')
    },
    startLoadChargesModal: function (event) {
        $(ManageDeliveryChargesForRegion.$loadChargesModal).find('button').attr('disabled', true);
        var form = new FormData($(ManageDeliveryChargesForRegion.$formClass));
        var formArray = $(ManageDeliveryChargesForRegion.$formClass).serializeArray();
        for (var key in formArray) {
            form.append(formArray[key]['name'], formArray[key]['value']);
        }

        var fileContainer = $(ManageDeliveryChargesForRegion.$loadChargesModal).find('.file-field-container').find('input[type="file"]');
        form.append(fileContainer.attr('name'), fileContainer.prop('files')[0]);
        form.append('load_from_file', true);

        ManageDeliveryChargesForRegion.modalLocked = true;
        $.ajax({
            url: $(ManageDeliveryChargesForRegion.$formClass).attr('action'),
            type: 'POST',
            data: form,
            dataType: 'json',
            cache: false,
            processData: false,
            contentType: false,
            success: function (response) {
                ManageDeliveryChargesForRegion.showLoadChargesModal();
                if (response.status == 'success') {
                    MainBootstrap.addNotificationSuccess(response.message);
                    location.reload();
                } else {
                    MainBootstrap.addNotificationDanger(response.message);
                }
                ManageDeliveryChargesForRegion.modalLocked = false;
            },
            error: function (response) {
                ManageDeliveryChargesForRegion.modalLocked = false;
                ManageDeliveryChargesForRegion.showLoadChargesModal();
                response = JSON.parse(response.responseText);

                if (response.statusText != 'abort') {
                    if (typeof response.message != 'undefined') {
                        MainBootstrap.addNotificationDanger(response.message);
                    } else {
                        MainBootstrap.addNotificationDanger(I18n['Не удалось произвести загрузку тарифов.']);
                    }
                }
            }
        });

        event.preventDefault();
    },
};