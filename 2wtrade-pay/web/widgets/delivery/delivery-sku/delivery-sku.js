$(function () {
    ManageSku.init();
});

var ManageSku = {
    init: function () {
        $('.btn-delivery-sku-plus').off('click');
        $('.btn-delivery-sku-minus').off('click');

        $('.btn-delivery-sku-plus').on('click', ManageSku.addInput);
        $('.btn-delivery-sku-minus').on('click', ManageSku.removeInput);

        if ($('.group-sku-group .row-delivery-sku').find('.btn-delivery-sku-minus').size() == 1) {
            $('.btn-delivery-sku-minus').hide();
        } else {
            $('.btn-delivery-sku-minus').show();
            $('.btn-delivery-sku-plus').hide();
            $('.btn-delivery-sku-minus').last().hide();
            $('.btn-delivery-sku-plus').last().show();
        }

    },
    addInput: function () {
        $('.group-sku-group .row-delivery-sku').last().clone().appendTo('.group-sku-group');

        $('.group-sku-group .row-delivery-sku').last().find('input').each(function () {
            $(this).val('');
        });

        $('.group-sku-group .row-delivery-sku').last().find('select').each(function () {
            $(this).parent().find('.select2').remove();
            $(this).val(0);
            $(this).select2();
            $(this).parent().find('.select2').css('width', '100%');
        });

        ManageSku.init();
    },
    removeInput: function () {
        $(this).parents().each(function () {
            if ($(this).hasClass('row-delivery-sku')) {
                $(this).remove();
            }
        });

        ManageSku.init();
    }
};
