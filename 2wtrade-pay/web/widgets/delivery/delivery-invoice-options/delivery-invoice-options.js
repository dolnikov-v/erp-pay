$(function () {
    InvoiceOptions.init();
});

var InvoiceOptions = {
    init: function () {

    },
    reinit: function () {
        $('.invoice-email-list input[data-plugin="datetimepicker"]').datetimepicker({
            format: 'HH:mm',
        });
        $('.invoice-email-list').find('select[data-plugin="select2"]').each(function () {
            $(this).parent().find('.select2').remove();
            $(this).select2();
            $(this).parent().find('>span.select2').removeAttr('style');
        });
    },
    callbackClearInput: function ($row, $template) {
        this.reinit()
    }
};