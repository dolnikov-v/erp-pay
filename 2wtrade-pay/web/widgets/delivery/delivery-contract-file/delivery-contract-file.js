$(function () {
    ManageContacts.init();
});

var ManageContacts = {
    init: function () {
        $('.btn-delivery-contract-file-plus').off('click');
        $('.btn-delivery-contract-file-minus').off('click');

        $('.btn-delivery-contract-file-plus').on('click', ManageContacts.addInput);
        $('.btn-delivery-contract-file-minus').on('click', ManageContacts.removeInput);

        if ($('.group-contract-file .row-delivery-contract-file').find('.btn-delivery-contract-file-minus').size() == 1) {
            $('.btn-delivery-contract-file-minus').hide();
        } else {
            $('.btn-delivery-contract-file-minus').show();
            $('.btn-delivery-contract-file-plus').hide();
            $('.btn-delivery-contract-file-minus').last().hide();
            $('.btn-delivery-contract-file-plus').last().show();
        }

    },
    addInput: function () {
        $('.group-contract-file .row-delivery-contract-file').last().clone().appendTo('.group-contract-file');

        $('.group-contract-file .row-delivery-contract-file').last().find('input').each(function () {
            $(this).val('');
        })

        ManageContacts.init();
        InputGroupFile.init();
    },
    removeInput: function () {
        $(this).parents().each(function () {
            if ($(this).hasClass('row-delivery-contract-file')) {
                $(this).remove();
            }
        });

        ManageContacts.init();
    }
};
