$(function () {
    EmailList.init();
});

var EmailList = {
    init: function () {
        $('.container-email-list .add-email-element').on('click', EmailList.addInput);
        $('.container-email-list .delete-email-element').on('click', EmailList.removeInput);
    },
    addInput: function (e) {
        e.preventDefault();
        $('.container-email-list').append($(this).parent().clone());
        $(this).hide();
        $(this).parent().find('.delete-email-element').show();
        EmailList.init();
    },
    removeInput: function (e) {
        e.preventDefault();
        $(this).parent().remove();
    },
    callbackClearInput: function ($row, $template) {
        $template.find('input').val('');
    }
};
