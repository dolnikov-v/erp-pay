$(function () {
    ManageAutoChangeReason.init();
});

var ManageAutoChangeReason = {
    init: function () {
        $('.btn-delivery-auto-change-reason-plus').off('click');
        $('.btn-delivery-auto-change-reason-minus').off('click');

        $('.btn-delivery-auto-change-reason-plus').on('click', ManageAutoChangeReason.addInput);
        $('.btn-delivery-auto-change-reason-minus').on('click', ManageAutoChangeReason.removeInput);

        if ($('.group-auto-change-reason-group .row-delivery-auto-change-reason').find('.btn-delivery-auto-change-reason-minus').size() == 1) {
            $('.btn-delivery-auto-change-reason-minus').hide();
        } else {
            $('.btn-delivery-auto-change-reason-minus').show();
            $('.btn-delivery-auto-change-reason-plus').hide();
            $('.btn-delivery-auto-change-reason-minus').last().hide();
            $('.btn-delivery-auto-change-reason-plus').last().show();
        }

    },
    addInput: function () {
        $('.group-auto-change-reason-group .row-delivery-auto-change-reason').last().clone().appendTo('.group-auto-change-reason-group');

        $('.group-auto-change-reason-group .row-delivery-auto-change-reason').last().find('input').each(function () {
            $(this).val('');
        });

        $('.group-auto-change-reason-group .row-delivery-auto-change-reason').last().find('select').each(function () {
            $(this).parent().find('.select2').remove();
            $(this).val(0);
            $(this).select2();
            $(this).parent().find('.select2').css('width', '100%');
        });

        ManageAutoChangeReason.init();
    },
    removeInput: function () {
        $(this).parents().each(function () {
            if ($(this).hasClass('row-delivery-auto-change-reason')) {
                $(this).remove();
            }
        });

        ManageAutoChangeReason.init();
    }
};
