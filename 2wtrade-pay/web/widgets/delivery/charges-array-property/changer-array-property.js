function plusRowInArray() {
    var parentContainer = $(this).parents('div.array-properties').get(0);
    var firstContainer = $(parentContainer).find('div.array-properties-default').first().clone();
    $(firstContainer).removeClass('array-properties-default');
    $(firstContainer).find('[type=hidden]').remove();
    $(firstContainer).find('input, select').each(function (id, element) {
        if (element.name.charAt(0) == '_') {
            $(element).attr('name', element.name.substring(1))
        }
    });
    var regex = /.+\[(\d+)\].+/;
    var matches;
    var indexRow = -1;
    var lastSelect = $(parentContainer).find('select:last').get(0).name;
    if (!lastSelect) {
        lastSelect = $(parentContainer).find('input:last').get(0).name;
    }
    if (lastSelect && (matches = regex.exec(lastSelect)) != null) {
        indexRow = matches[1];
    }
    $(firstContainer).find('select, input').each(function (id, element) {
        $(element).attr('name', element.name.replace('-1', +indexRow + 1));
    });
    $(firstContainer).find('select[data-plugin="select2"]').each(function () {
        $(this).parent().find('.select2').remove();
        $(this).select2();
    });
    $(parentContainer).find('.charges-panel').before(firstContainer);
}
function minusRowInArray() {
    var parentContainer = $(this).parents('div.row').get(0);
    parentContainer.remove();
}

$(function () {
    $('.array-properties').on('click', '.btn-minus', minusRowInArray);
    $('.array-properties').on('click', '.btn-plus', plusRowInArray);
});
