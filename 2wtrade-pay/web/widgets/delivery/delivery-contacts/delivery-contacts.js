$(function () {
    ManageContacts.init();
});

var ManageContacts = {
    init: function () {
        $('.btn-delivery-contacts-plus').off('click');
        $('.btn-delivery-contacts-minus').off('click');

        $('.btn-delivery-contacts-plus').on('click', ManageContacts.addInput);
        $('.btn-delivery-contacts-minus').on('click', ManageContacts.removeInput);

        if ($('.group-contacts-group .row-delivery-contacts').find('.btn-delivery-contacts-minus').size() == 1) {
            $('.btn-delivery-contacts-minus').hide();
        } else {
            $('.btn-delivery-contacts-minus').show();
            $('.btn-delivery-contacts-plus').hide();
            $('.btn-delivery-contacts-minus').last().hide();
            $('.btn-delivery-contacts-plus').last().show();
        }

    },
    addInput: function () {
        $('.group-contacts-group .row-delivery-contacts').last().clone().appendTo('.group-contacts-group');

        $('.group-contacts-group .row-delivery-contacts').last().find('input').each(function () {
            $(this).val('');
        })

        ManageContacts.init();
    },
    removeInput: function () {
        $(this).parents().each(function () {
            if ($(this).hasClass('row-delivery-contacts')) {
                $(this).remove();
            }
        });

        ManageContacts.init();
    }
};
