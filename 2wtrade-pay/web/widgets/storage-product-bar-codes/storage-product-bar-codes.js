$(function () {
    $('.container-storageproductbarcodes .storage').on('change', Product.change);
});

$(function () {
    $('.container-storageproductbarcodes .product').on('change', function (event) {
        BarList.change(event);
        ShelfLife.change(event)
    });
});

var Product = {
    change: function () {
        var $self = $(this);
        var $selfParent = $(this).parent();
        $.ajax({
            type: 'POST',
            url: '/storage/storage-product/get-select-options-product',
            data: {
                storage_id: $self.find('select').val()
            },
            success: function (response) {
                $selfParent.find('.product select').empty().html(response);
                $selfParent.find('.product select').removeAttr('disabled');
            },
            error: function () {
                $selfParent.find('.product select').attr('disabled', '');

            }
        });

        $selfParent.find('.product .selection span span:first').empty().html('—');
        $selfParent.find('.product .selection span span:first').attr('title', '—');
    }
};

var BarList = {
    change: function (event) {
        var $self = $(event.target);
        var $selfParent = $self.offsetParent();
        $.ajax({
            type: 'POST',
            url: '/storage/storage-product-bar-codes/get-select-options-bar-list',
            data: {
                product_id: $selfParent.find('.product select').val(),
                storage_id: $selfParent.find('.storage select').val()
            },
            success: function (response) {
                $selfParent.find('.bar-list').empty().html(response);
                $selfParent.find('.quantity input').attr('disabled', '');
            },
            error: function () {
                $selfParent.find('.bar-list').empty();
                $selfParent.find('.quantity input').removeAttr('disabled');
            }
        });
    }
};

var ShelfLife = {
    change: function (event) {
        var $self = $(event.target);
        var $selfParent = $self.offsetParent();
        $.ajax({
            type: 'POST',
            url: '/storage/storage-product/get-select-options-shelf-life',
            data: {
                product_id: $selfParent.find('.product select').val(),
                storage_id: $selfParent.find('.storage select').val()
            },
            success: function (response) {
                $selfParent.find('.productShelfLife select').empty().html(response);
                $selfParent.find('.productShelfLife select').removeAttr('disabled');
                $selfParent.find('.productShelfLife .selection span span:first').empty().html($selfParent.find('.productShelfLife select option').get(0).innerHTML);
                $selfParent.find('.productShelfLife .selection span span:first').attr('title', $selfParent.find('.productShelfLife select option').get(0).innerHTML);
            },
            error: function () {
                $selfParent.find('.productShelfLife select').empty();
                $selfParent.find('.productShelfLife select').attr('disabled', '');
                $selfParent.find('.productShelfLife .selection span span:first').empty().html('—');
                $selfParent.find('.productShelfLife .selection span span:first').attr('title', '—');
            }
        });
    }
};