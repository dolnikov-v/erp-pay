$(function () {
    $('.container-countrydeliverypartnersmultiple .countrymultiple select').on('change', Delivery.change);
});
$(function () {
    $('.container-countrydeliverypartnersmultiple .deliverymultiple select').on('change', Partners.change);
});


var Delivery = {
    change: function () {
        var id = $(this).attr('id');
        $('#' + id).closest('.container-countrydeliverypartnersmultiple').find('.deliverymultiple select').attr('disabled', true);
        $('#' + id).closest('.container-countrydeliverypartnersmultiple').find('.partnersmultiple select').attr('disabled', true);
        $.ajax({
            type: 'POST',
            url: '/report/filter/get-select-options-delivery',
            data: {
                country_ids: $('#' + id).closest('.container-countrydeliverypartnersmultiple').find('.countrymultiple select').val()
            },
            success: function (response) {
                var select = $('#' + id).closest('.container-countrydeliverypartnersmultiple').find('.deliverymultiple select');
                var options = select.data('select2').options.options;
                $('#' + id).closest('.container-countrydeliverypartnersmultiple').find('.deliverymultiple .select2-selection ul').empty();
                select.html(response);
                select.select2(options);
                select.removeAttr('disabled');
                $('.container-countrydeliverypartnersmultiple .partnersmultiple select').removeAttr('disabled');
            },
            error: function () {
                $('.container-countrydeliverypartnersmultiple .deliverymultiple select').attr('disabled', '');
                $('.container-countrydeliverypartnersmultiple .partnersmultiple select').attr('disabled', '');

            }
        });
    }
};

var Partners = {
    change: function () {
        var id = $(this).attr('id');
        $('#' + id).closest('.container-countrydeliverypartnersmultiple').find('.partnersmultiple select').attr('disabled', true);
        $.ajax({
            type: 'POST',
            url: '/report/filter/get-select-options-partners',
            data: {
                delivery_ids: $('#' + id).closest('.container-countrydeliverypartnersmultiple').find('.deliverymultiple select').val()
            },
            success: function (response) {
                var select = $('#' + id).closest('.container-countrydeliverypartnersmultiple').find('.partnersmultiple select');
                var options = select.data('select2').options.options;
                $('#' + id).closest('.container-countrydeliverypartnersmultiple').find('.partnersmultiple .select2-selection ul').empty();
                select.html(response);
                select.select2(options);
                select.removeAttr('disabled');
            },
            error: function () {
                $('#' + id).closest('.container-countrydeliverypartnersmultiple').find('.partnersmultiple select').attr('disabled', '');

            }
        });
    }
};