var I18n = I18n || {};

$(function () {
    SetBalance.init();
});

var SetBalance = {
    ajax: null,
    input: null,

    init: function () {
        $('.balance_sub').on('blur', SetBalance.sub);
        $('.balance_add').on('blur', SetBalance.add);
        $('<div id="loading"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span></div>').appendTo("body");

        $('.page-content').on('click', '#modal_confirm_delete_link', function ($action) {
            SetBalance.delete($action.target.href);
            $('#modal_confirm_delete').modal('hide');
            return false;
        })
    },

    sub: function () {
        SetBalance.input = $(this);
        SetBalance.change($(this).data('id'), -1 * $(this).val());
    },

    add: function () {
        SetBalance.input = $(this);
        SetBalance.change($(this).data('id'), $(this).val());
    },

    change: function (id, val) {
        if (id && val) {
            $("#loading").show();
            $.ajax({
                type: 'POST',
                url: '/storage/balance/change',
                dataType: 'json',
                data: {
                    id: id,
                    val: val
                },
                success: function (response, textStatus) {
                    var input = SetBalance.input;
                    if (response.status == 'success') {
                        input.val('');
                        var cur = input.closest('tr').find('.current-balance');
                        cur.html(response.balance);
                        var d = input.closest('tr').find('.updated_at div');
                        d.html(response.updated_at);
                    } else {
                        input.closest('tr').addClass('text-danger');
                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                    SetBalance.input = null;
                    $("#loading").hide();
                },
                error: function (response) {
                    SetBalance.input = null;
                    $("#loading").hide();
                    response = JSON.parse(response.responseText);
                    if (response.statusText != 'abort') {
                        if (typeof response.message != 'undefined') {
                            $.notify({message: response.message}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        } else {
                            $.notify({message: I18n['Не удалось изменить баланс']}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }
                    }
                }
            });
        }
    },

    delete: function (link) {
        if (link) {
            $("#loading").show();
            $.ajax({
                type: 'GET',
                url: link,
                dataType: 'json',
                success: function (response, textStatus) {
                    var input = SetBalance.input;
                    if (response.status == 'success') {
                        document.location.reload();
                    } else {
                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                    SetBalance.input = null;
                    $("#loading").hide();
                },
                error: function (response) {
                    SetBalance.input = null;
                    $("#loading").hide();
                    response = JSON.parse(response.responseText);
                    if (response.statusText != 'abort') {
                        if (typeof response.message != 'undefined') {
                            $.notify({message: response.message}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        } else {
                            $.notify({message: I18n['Не удалось удалить запись']}, {
                                type: "danger dark",
                                animate: {exit: 'hide'},
                                z_index: 2031
                            });
                        }
                    }
                }
            });
        }
    },
};
