$(function () {
    var $widgetStack = $('#widget-stack');

    $widgetStack.find('[data-toggle="panel-close"]').click(function () {

        var csrfParam = $('meta[name=csrf-param]').attr("content");
        var csrfToken = $('meta[name=csrf-token]').attr("content");

        var dataAjax = {
            id: $(this).closest('.panel-wrap-data').data('id')
        };

        dataAjax[csrfParam] = csrfToken;

        $.ajax({
            type: 'POST',
            url: '/widget/index/delete',
            dataType: 'json',
            data: dataAjax,
            success: function (response, textStatus) {
                $.notify({message: 'Действие выполнено.'}, {type: "success dark", animate: {exit: 'hide'}, z_index: 2031});
            },
            error: function () {
                $.notify({message: 'Не удалось выполнить запрос.'}, {type: "danger dark", animate: {exit: 'hide'}, z_index: 2031});
            }
        });

        $widgetStack.find('.grid-stack').data('gridstack').remove_widget($(this).closest('.grid-stack-item'));
    });

    $widgetStack.find('.grid-stack').on('change', function (e, items) {

        if (!items) return false;

        var csrfParam = $('meta[name=csrf-param]').attr("content");
        var csrfToken = $('meta[name=csrf-token]').attr("content");

        var dataAjax = {
            items: []
        };

        $.each(items, function () {

            var $el = $(this.el);
            var $panelWrap = $el.find('.panel-wrap-data');
            var $panel = $panelWrap.children('.panel');
            //не уловил зачем это? теперь у нас есть футер и с ним не получилось нормально
            //$panel.children('.panel-body').outerHeight(this.height * 100 - 40 - $panel.children('.panel-heading').height()- $panel.children('.panel-footer').height());

            dataAjax.items.push({
                id: $panelWrap.data('id'),
                options: {
                    height: this.height,
                    width: this.width,
                    x: this.x,
                    y: this.y
                }
            });
        });

        dataAjax[csrfParam] = csrfToken;

        $.ajax({
            type: 'POST',
            url: '/widget/index/change-place',
            dataType: 'json',
            data: dataAjax,
            success: function (response, textStatus) {
                // $.notify({message: 'Действие выполнено.'}, {type: "success dark", animate: {exit: 'hide'}, z_index: 2031});
            },
            error: function () {
                // $.notify({message: 'Не удалось выполнить запрос.'}, {type: "danger dark", animate: {exit: 'hide'}, z_index: 2031});
            }
        });

    });

    buyoutGoodsInit();
});

function buyoutGoodsInit() {

    var lastValue = 'all';
    var lastIndex = 0;

    $('#widget-stack .widget-buyout-goods').find('select').change(function () {
        var $this = $(this);
        var $parent = $this.closest('.widget-buyout-goods');
        var $items = $parent.find('.widget-buyout-goods-item');
        $items.filter('.visible').removeClass('visible').addClass('hidden');
        $items.filter('[data-id="' + $this.val() + '"]').removeClass('hidden').addClass('visible');

        var $export = $parent.closest('.panel').find('.panel-action-export');
        if ($export.length) {
            var index = $this.prop('selectedIndex');

            $export.children(':not(span)').appendTo($items.eq(lastIndex).find('.report_export_box'));
            $items.eq(index).find('.report_export_box').children('ul').appendTo($export);

            lastIndex = index;
            lastValue = $this.val();
        }
    });
}

function widget_refresh(done) {

    var $panelWrap = $(this).closest('.panel-wrap-data');

    var dataAjax = {
        id: $panelWrap.data('id')
    };

    var csrfParam = $('meta[name=csrf-param]').attr("content");
    var csrfToken = $('meta[name=csrf-token]').attr("content");
    dataAjax[csrfParam] = csrfToken;

    var body = $(this).closest('.grid-stack-item').find('.panel-body');
    var footer = $(this).closest('.grid-stack-item').find('.panel-footer');

    $.ajax({
        type: 'POST',
        url: '/widget/index/refresh',
        dataType: 'json',
        data: dataAjax,
        success: function (response, textStatus) {

            if (typeof response.data === 'string') {
                $panelWrap.find('table').resizableColumns('destroy');
                body.html(response.data);

                body.find('.grid-view .report_export_box').each(function() {
                    var $this = $(this);
                    var func = $this.parent().data('krajeeGrid') + '__export';
                    $.isFunction(window[func]) && window[func]();

                    var $action = $this.closest('.panel').find('.panel-action-export');
                    $action.children().not('.icon').remove();
                    $this.children().appendTo($action);
                });

                if ($panelWrap.data('code') == 'buy_out_goods') {
                    buyoutGoodsInit();
                }

                if (body.find('.kv-grid-group-header, kv-grid-group-filter').length) {
                    kvGridGroup(body.find('.grid-view').prop('id'));
                }

            } else if (response.data && typeof response.data === 'object') {

                if (response.data.scenario == 'chart') {
                    var chart = body.children().highcharts();
                    var options = response.data.options;
                    // Обновляем данные графика
                    var seriesLength = chart.series.length;
                    for(var i = seriesLength -1; i > -1; i--) {
                        chart.series[i].remove();
                    }
                    for (var i = 0; i < options.series.length; i++) {
                        chart.addSeries(options.series[i]);
                    }
                    chart.xAxis[0].setCategories(options.xAxis.categories, true, true);
                    chart.yAxis[0].setTitle(options.yAxis.title);
                }
            }

            footer.html(response.time);
            done();
        },
        error: function () {
            $.notify({message: 'Не удалось обновить виджет.'}, {type: "danger dark", animate: {exit: 'hide'}, z_index: 2031});
            done();
        }
    });
}