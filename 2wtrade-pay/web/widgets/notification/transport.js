$(function () {
    ManageTransports.init();
});

var ManageTransports = {
    init: function () {
        $('.btn-transport-plus').off('click');
        $('.btn-transport-minus').off('click');

        $('.btn-transport-plus').on('click', ManageTransports.addInput);
        $('.btn-transport-minus').on('click', ManageTransports.removeInput);

        if ($('.row-transport').find('.btn-transport-minus').size() == 1) {
            $('.btn-transport-minus').hide();
        } else {
            $('.btn-transport-minus').show();
            $('.btn-transport-plus').hide();
            $('.btn-transport-minus').last().hide();
            $('.btn-transport-plus').last().show();
        }

    },
    addInput: function () {
        $('.group-transport-group .row-transport').last().clone().appendTo('.group-transport-group');

        $('.group-transport-group .row-transport').last().find('input').each(function () {
            $(this).val('');
        })

        $('.group-transport-group .row-transport').last().find('select').each(function () {
            $(this).parent().find('.select2').remove();
            $(this).val(0);
            $(this).select2();
            $(this).parent().find('.select2').css('width', '100%');
        });

        ManageTransports.init();
    },
    removeInput: function () {
        $(this).parents().each(function () {
            if ($(this).hasClass('row-transport')) {
                $(this).remove();
            }
        });

        ManageTransports.init();
    }
};
