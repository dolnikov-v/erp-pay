$(function () {
    TimeRangePicker.init();
});

var TimeRangePicker = {
    init: function () {
        $('.input-timing').each(function () {
            $(this).datetimepicker({
                format: $(this).data('format')
            });
        });
    }
};
