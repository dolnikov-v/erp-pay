$(function () {
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var target = $(e.target).attr("href");
        var targetBlock = $(target);
        if (targetBlock.length) {
            var container = targetBlock.find('.highcharts-container').parent();
            if (container.length) {
                var chart=container.highcharts();
                chart.setSize(null)
            }
        }
    });
});