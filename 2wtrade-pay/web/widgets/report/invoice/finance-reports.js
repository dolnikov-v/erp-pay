$(function () {
    var form = $("#by-finance-report");

    var FinanceInvoiceFilter = {
        form: form,
        btn_success: form.find('.btn-success'),
        delivery_select: form.find("[name*=delivery_ids]"),
        country_select: form.find("[name*=country_ids]"),
        report_select: form.find("[name*=financeReportId]"),
        active_finance_report: $('[name*=activeFinanceReport]'),
        spinner: form.find('.load-finance-reports'),
        init: function () {
            FinanceInvoiceFilter.manageSuccessBtn();
            FinanceInvoiceFilter.getFinanceReports();
        },
        spinnerShow: function () {
            $(FinanceInvoiceFilter.spinner).show();
        },
        spinnerHide: function () {
            $(FinanceInvoiceFilter.spinner).hide();
        },
        clearReportsList: function () {

        },
        manageSuccessBtn: function () {
            $(FinanceInvoiceFilter.btn_success).prop('disabled', typeof $(FinanceInvoiceFilter.report_select) !== 'number');
        },
        getDeliveryByCountry: function () {
            $(FinanceInvoiceFilter.active_finance_report).val('');
            $(FinanceInvoiceFilter.btn_success).prop('disabled', true);

            $(FinanceInvoiceFilter.report_select).val('');
            $(FinanceInvoiceFilter.report_select).trigger('change');
            $(FinanceInvoiceFilter.report_select).prop('disabled', true);
            $(FinanceInvoiceFilter.delivery_select).prop('disabled', true);

            $.ajax({
                type: 'POST',
                url: '/report/filter/get-select-options-delivery',
                data: {
                    country_ids: $(FinanceInvoiceFilter.country_select).val()
                },
                success: function (response) {
                    var options = $(FinanceInvoiceFilter.delivery_select).find('option');
                    $(FinanceInvoiceFilter.delivery_select).find('option').remove();
                    $(FinanceInvoiceFilter.delivery_select).html(response);
                    $(FinanceInvoiceFilter.delivery_select).select2(options);
                    $(FinanceInvoiceFilter.delivery_select).removeAttr('disabled');
                    $(FinanceInvoiceFilter.delivery_select).trigger('change');
                },
                error: function () {
                    $(FinanceInvoiceFilter.delivery_select).attr('disabled', '');

                }
            });

        },
        getFinanceReports: function () {
            FinanceInvoiceFilter.spinnerShow();

            $(FinanceInvoiceFilter.btn_success).prop('disabled', true);

            $(FinanceInvoiceFilter.report_select).find('option').remove();
            $(FinanceInvoiceFilter.report_select).val('');
            $(FinanceInvoiceFilter.report_select).trigger('change');
            $(FinanceInvoiceFilter.report_select).prop('disabled', true);

            $.ajax({
                url: '/report/invoice/get-finance-reports',
                type: 'post',
                dataType: 'json',
                data: {
                    country_id: $(FinanceInvoiceFilter.country_select).val(),
                    delivery_id: $(FinanceInvoiceFilter.delivery_select).val()
                },
                success: function (data) {
                    FinanceInvoiceFilter.spinnerHide();

                    if (Object.keys(data).length > 0) {
                        for (var k in data) {
                            $(FinanceInvoiceFilter.report_select).append($("<option></option>").attr('value', k).text(data[k]));
                        }

                        $(FinanceInvoiceFilter.report_select).prop('disabled', false);

                        if ($(FinanceInvoiceFilter.active_finance_report).val() === '') {
                            $(FinanceInvoiceFilter.report_select).val($(FinanceInvoiceFilter.report_select).find('option').first().val());
                        } else {
                            $(FinanceInvoiceFilter.report_select).val($(FinanceInvoiceFilter.active_finance_report).val());
                        }

                        $(FinanceInvoiceFilter.report_select).trigger('change');

                        $(FinanceInvoiceFilter.btn_success).prop('disabled', false);

                    } else {
                        if($("a[href*=#tab1]").closest('li').hasClass('active')) {
                            MainBootstrap.addNotificationDanger(I18n['Финансовые отчёты не найдены']);
                        }
                    }
                },
                error: function (error) {
                    FinanceInvoiceFilter.spinnerHide();

                    if($("a[href*=#tab1]").closest('li').hasClass('active')) {
                        MainBootstrap.addNotificationDanger(data.responseText);
                    }
                }

            });
        }
    };

    FinanceInvoiceFilter.init();
    $(FinanceInvoiceFilter.country_select).on('change', FinanceInvoiceFilter.getDeliveryByCountry);
    $(FinanceInvoiceFilter.delivery_select).on('change', FinanceInvoiceFilter.getFinanceReports);
});
