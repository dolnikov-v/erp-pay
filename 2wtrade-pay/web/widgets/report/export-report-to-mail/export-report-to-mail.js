var I18n = I18n || {};

$(function () {
    ExportReportToMail.init();
});

var ExportReportToMail = {
    ajax: null,
    $modal: $('#modal_report_sent_to_mail'),
    $form: $('#modal_report_sent_to_mail').find('form'),
    $content: $('#ready_to_export').html(),

    init: function () {
        $('#start_export_report_to_mail').on('click', ExportReportToMail.start);
        $('#stop_export_report_to_mail').on('click', ExportReportToMail.stop);
    },

    start: function () {
        ExportReportToMail.$modal.find('.sender-percent').text('0%');
        ExportReportToMail.$modal.find('.progress-bar').css('width', '0%');

        ExportReportToMail.$modal.find('.row-with-btn-start').hide();
        ExportReportToMail.$modal.find('.row-with-text-start').hide();
        ExportReportToMail.$modal.find('.row-with-btn-stop').show();
        ExportReportToMail.$modal.find('.row-with-text-stop').show();

        ExportReportToMail.sent();
    },

    stop: function () {
        if (ExportReportToMail.ajax != null) {
            ExportReportToMail.ajax.abort();
        }

        ExportReportToMail.$modal.find('.row-with-btn-start').show();
        ExportReportToMail.$modal.find('.row-with-text-start').show();
        ExportReportToMail.$modal.find('.row-with-btn-stop').hide();
        ExportReportToMail.$modal.find('.row-with-text-stop').hide();
    },

    sent: function () {

        var url = ExportReportToMail.$form.attr('data-url');
        var report = $('.page-title').text();
        var list = ExportReportToMail.$form.find('.select2-selection__choice').text();

        if (list.length) {

            ExportReportToMail.$modal.find('.sender-percent').text('100%');
            ExportReportToMail.$modal.find('.progress-bar').css('width', '100%');

            $.ajax({
                url: url,
                type: 'POST',
                data: {
                    'export_content': ExportReportToMail.$content,
                    'mails': list,
                    'report': report
                },
                success: function(data) {
                    if (data == 'success') {
                        $.notify({message: 'Отчет успешно отослан на указанные адреса'}, {
                            type: "success dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                    else {
                        $.notify({message: 'Не удалось отправить отчет на следующие адреса: ' + data.toString()}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                    ExportReportToMail.stop();
                },
                error: function (data) {
                    confirm(JSON.stringify(data));
                    $.notify({message: 'Ошибка отправки отчета на почту'}, {
                        type: "danger dark",
                        animate: {exit: 'hide'},
                        z_index: 2031
                    });
                    ExportReportToMail.stop();
                }
            });
        }
        else {
            $.notify({message: "Ошибка: не указано ни одного почтового адреса"}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
            ExportReportToMail.stop();
        }
    }

};
