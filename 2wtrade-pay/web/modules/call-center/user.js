$(function () {
    $('.callcenter_holder select').on('change', CallCenter.change);
});

var CallCenter = {
    change: function () {
        $('.parent_holder').hide();
        $.ajax({
            type: 'GET',
            url: '/call-center/user/get-parents',
            data: {
                callcenter_id: $('.callcenter_holder select').val()
            },
            success: function (response) {
                $('.parent_holder select').empty();
                $('.parent_holder select').append('<option value="0">-</option>');
                $.each(response, function (key, value) {
                    $('.parent_holder select').append('<option value=' + key + '>' + value + '</option>');
                });
                $('.parent_holder').show();
            }
        });
    }
};
