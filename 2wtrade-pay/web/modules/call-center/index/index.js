$(function () {
    if ($('#users_content td').length) {
       CallCenterUserIndex.init();
    }
});

var CallCenterUserIndex = {
    $btnChangeTeamLeader: $('#btn_change_team_leader'),
    $modalChangeTeamLeader: $('#modal_change_team_leader'),

    $btnDeactivateUsers: $('#btn_deactivate_users'),
    $modalDeactivateUsers: $('#modal_deactivate_users'),

    $btnActivateUsers: $('#btn_activate_users'),
    $modalActivateUsers: $('#modal_activate_users'),

    $btnDeleteUsers: $('#btn_delete_users'),
    $modalDeleteUsers: $('#modal_delete_users'),
    
    $btnSetRole: $('#btn_set_role'),
    $modalSetRole: $('#modal_set_role'),

    init: function () {
        $(document).on('change', '#users_content .th-custom-checkbox input[type="checkbox"]', CallCenterUserIndex.toggleCheckboxes);
        $('#users_content .td-custom-checkbox input[type="checkbox"]').on('change', CallCenterUserIndex.toggleCheckbox);

        CallCenterUserIndex.$btnChangeTeamLeader.on('click', CallCenterUserIndex.initChangeTeamLeader);
        CallCenterUserIndex.$modalChangeTeamLeader.on('hide.bs.modal', CallCenterUserIndex.destroyChangeTeamLeader);

        CallCenterUserIndex.$btnDeactivateUsers.on('click', CallCenterUserIndex.initDeactivateUsers);
        CallCenterUserIndex.$modalDeactivateUsers.on('hide.bs.modal', CallCenterUserIndex.destroyDeactivateUsers);

        CallCenterUserIndex.$btnActivateUsers.on('click', CallCenterUserIndex.initActivateUsers);
        CallCenterUserIndex.$modalActivateUsers.on('hide.bs.modal', CallCenterUserIndex.destroyActivateUsers);

        CallCenterUserIndex.$btnDeleteUsers.on('click', CallCenterUserIndex.initDeleteUsers);
        CallCenterUserIndex.$modalDeleteUsers.on('hide.bs.modal', CallCenterUserIndex.destroyDeleteUsers);

        CallCenterUserIndex.$btnSetRole.on('click', CallCenterUserIndex.initSetRole);
        CallCenterUserIndex.$modalSetRole.on('hide.bs.modal', CallCenterUserIndex.destroySetRole);
    },

    toggleCheckboxes: function () {
        $('#users_content .td-custom-checkbox input[type="checkbox"]').prop('checked', $(this).prop('checked')).trigger('change');
    },

    toggleCheckbox: function () {
        var $tr = $(this).closest('tr');

        if ($(this).prop('checked')) {
            $tr.addClass('active');
        } else {
            $tr.removeClass('active');
        }
    },

    // Смена руководителя
    initChangeTeamLeader: function () {
        CallCenterUserIndex.$modalChangeTeamLeader.modal();
        CallCenterUserIndex.completeForm(CallCenterUserIndex.$modalChangeTeamLeader.find('form'));
    },
    destroyChangeTeamLeader: function () {
        CallCenterUserIndex.$btnChangeTeamLeader.removeAttr('disabled');
        CallCenterUserIndex.$btnChangeTeamLeader.removeAttr('data-loading');
        ChangeTeamLeader.stop();
    },

    // Деактивация пользователей
    initDeactivateUsers: function () {
        CallCenterUserIndex.$modalDeactivateUsers.modal();
        CallCenterUserIndex.completeForm(CallCenterUserIndex.$modalDeactivateUsers.find('form'));
    },
    destroyDeactivateUsers: function () {
        CallCenterUserIndex.$btnDeactivateUsers.removeAttr('disabled');
        CallCenterUserIndex.$btnDeactivateUsers.removeAttr('data-loading');
        DeactivateUsers.stop();
    },

    // Активация пользователей
    initActivateUsers: function () {
        CallCenterUserIndex.$modalActivateUsers.modal();
        CallCenterUserIndex.completeForm(CallCenterUserIndex.$modalActivateUsers.find('form'));
    },
    destroyActivateUsers: function () {
        CallCenterUserIndex.$btnActivateUsers.removeAttr('disabled');
        CallCenterUserIndex.$btnActivateUsers.removeAttr('data-loading');
        ActivateUsers.stop();
    },

    // Удаление пользователей
    initDeleteUsers: function () {
        CallCenterUserIndex.$modalDeleteUsers.modal();
        CallCenterUserIndex.completeForm(CallCenterUserIndex.$modalDeleteUsers.find('form'));
    },
    destroyDeleteUsers: function () {
        CallCenterUserIndex.$btnDeleteUsers.removeAttr('disabled');
        CallCenterUserIndex.$btnDeleteUsers.removeAttr('data-loading');
        DeleteUsers.stop();
    },

    // Установка роли
    initSetRole: function () {
        CallCenterUserIndex.$modalSetRole.modal();
        CallCenterUserIndex.completeForm(CallCenterUserIndex.$modalSetRole.find('form'));
    },
    destroySetRole: function () {
        CallCenterUserIndex.$btnSetRole.removeAttr('disabled');
        CallCenterUserIndex.$btnSetRole.removeAttr('data-loading');
        SetRole.stop();
    },

    completeForm: function ($form) {
        $form.find('input[name="id[]"]').remove();

        $('#users_content input[name="id[]"]:checked').each(function () {
            $(this).clone().attr('type', 'hidden').val($(this).val()).appendTo($form);
        });
    },
};
