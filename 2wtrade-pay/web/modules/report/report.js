$(document).ready(function () {

    $('#report_export_box a').on('click', function () {
        $(this).parents('.btn-group').removeClass('open');
    });

    $('#sale_all_countries input[type="checkbox"]').on('click', function () {
        if ($('#sale_all_countries input[type="checkbox"]').prop("checked")) {
            $('#sale_delivery select').attr('disabled', '');
        } else {
            $('#sale_delivery select').removeAttr('disabled')
        }
    });

    if ($('#sale_all_countries input[type="checkbox"]').prop("checked")) {
        $('#sale_delivery select').attr('disabled', '');
    }

});
