$(function () {
    var $modal = $('#payment-per-period-modal');
    var $modalBody = $modal.find(".modal-body");

    $(document).on("click", "[data-id=\"payment-per-period-modal\"]", function() {

        $modalBody.html('<div class="progress progress-striped bg-success active"><div class="progress-bar" style="width: 100%"></div></div>');

        $.ajax({
            url: "/report/delivery-closed-periods/payment-per-period",
            data: {
                month: $(this).data("month"),
                year: $(this).data("year")
            },
            method: 'post',
            success: function(html) {
                $modalBody.fadeOut(400, function() {
                    $modalBody.html(html);
                    $modalBody.fadeIn();
                });
            },
            error: function () {
                $modal.modal('hide');
            }
        })
    });
});


