var I18n = I18n || {};

$(document).ready(function() {
    var $officeSelect = $('#s-office');
    var $callcenterSelect = $('#s-callcenter');
    var $teamleadSelect = $('#s-teamlead');

    $officeSelect.select2().on('change', function() {
        $callcenterSelect.parent().hide();
        $.ajax({
            url: "/report/team/get-call-center",
            data: {
                'office': $officeSelect.val()
            },
            success: function(data) {
                if (data.status == false) {
                    return
                }
                $callcenterSelect.empty();
                $callcenterSelect.append('<option value="0">-</option>');
                $.each(data.message, function (key, value) {
                    $callcenterSelect.append('<option value=' + key + '>' + value + '</option>');
                });
                $callcenterSelect.parent().show();
                $callcenterSelect.select2();
            }
        });
        $teamleadSelect.parent().hide();
        $.ajax({
            url: "/report/team/get-team-lead",
            data: {
                'office': $officeSelect.val()
            },
            success: function(data) {
                if (data.status == false) {
                    return
                }
                $teamleadSelect.empty();
                $teamleadSelect.append('<option value="0">-</option>');
                $.each(data.message, function (key, value) {
                    $teamleadSelect.append('<option value=' + key + '>' + value + '</option>');
                });
                $teamleadSelect.parent().show();
                $teamleadSelect.select2();
            }
        });
    });

});

function customSumWithPercent(data) {
    var i, out = 0, n = data && data.length || 0;
    for (i = 0; i < n; i++) {
        out += data[i];
    }
    return out;
}