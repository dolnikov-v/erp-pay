$(function () {
    PaymentsList.init();
});

var PaymentsList = {
    $modal: $('#payments_modal'),
    $modalSum: $('#sum_modal'),

    init: function () {
        $('.view-payments-modal').on('click', function () {
            $("#payments-body").html('<div class="loader-notifications"><div class="loader loader-default"></div> Идёт загрузка данных</div>');
            PaymentsList.$modal.modal();

            $.ajax({
                type: 'GET',
                url: $(this).data('url'),
                success: function (response) {
                    $("#payments-body").html(response);
                },
                error: function (response) {
                    PaymentsList.$modal.modal('hide');
                    $.notify({message: response.message}, {
                        type: "danger dark",
                        animate: {exit: 'hide'},
                        z_index: 2031
                    });
                }
            });
        });

        $('.page-content').on('click', '.view-sum-modal', function () {
            var dataArray = $(this).data('popup');
            if (dataArray.length) {
                var table = $('#sum-popup-container').clone();
                table.removeClass('hidden');
                table.removeAttr('id');
                var defaultRow = table.find('.table-content>tr');
                var resultSum = 0;
                $.each(dataArray, function(key, item) {
                    if (item[2]) {
                        var newRow = defaultRow.clone();
                        newRow.find('td:first').html(item[0]);
                        newRow.find('td:nth-child(2)').html(item[1]);
                        newRow.find('td:nth-child(3)').html((item[2] / item[1]).toFixed(2));
                        newRow.find('td:nth-child(4)').html(item[2].toFixed(2));
                        newRow.insertAfter(defaultRow);
                        resultSum += item[2];
                    }
                });
                defaultRow.remove();
                table.find('.table-footer td:last').html(resultSum.toFixed(2));
                PaymentsList.$modalSum.find('.table-responsive').html(table);
                var link = PaymentsList.$modalSum.find('#link_to_orders');
                link.html($(this).data('count') ? $(this).data('count') : 0).attr('href', $(this).data('url') ? $(this).data('url') : '#');
                PaymentsList.$modalSum.modal();
            }
        });
    },

    close: function () {
        PaymentsList.$modal.modal('hide');
    }
};
