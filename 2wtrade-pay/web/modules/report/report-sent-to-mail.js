var I18n = I18n || {};

$(function () {
    ReportSentToMail.init();
});

var ReportSentToMail = {

    $btnReportSentToMail: $('#btn_send_report_to_mail'),
    $modalReportSentToMail: $('#modal_report_sent_to_mail'),

    init: function () {
        ReportSentToMail.$btnReportSentToMail.on('click', ReportSentToMail.initReportSentToMail);
        ReportSentToMail.$modalReportSentToMail.on('hide.bs.modal', ReportSentToMail.destroyReportSentToMail);
    },

    initReportSentToMail: function () {
        ReportSentToMail.$modalReportSentToMail.modal();
        ReportSentToMail.completeForm(ReportSentToMail.$modalReportSentToMail.find('form'));
    },

    destroyReportSentToMail: function () {
        ReportSentToMail.$btnReportSentToMail.removeAttr('disabled');
        ReportSentToMail.$btnReportSentToMail.removeAttr('data-loading');
        ExportReportToMail.stop();
    }
};

