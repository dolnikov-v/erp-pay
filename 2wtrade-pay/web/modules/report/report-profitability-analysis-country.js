$(function() {

    $('#report-grid-profitability-analysis').find('input.update-by-ajax')
        .each(function () {
            $(this).data('value', this.value);
        })
        .change(function () {
            var $this = $(this);
            var data = {
                identity: $this.data('identity'),
                param: $this.data('param'),
                value: $this.val()
            };

            $this.prop('disabled', true);
            $.ajax({
                type: "POST",
                url: "/report/profitability-analysis/set-param",
                data: data,
                dataType: 'json',
                success: function(data) {
                    if (data.status) {
                        $this.data('value', $this.val());
                    } else {
                        $this.val($this.data('value'));
                    }
                    $this.prop('disabled', false);
                },
                failure: function() {
                    $this.val($this.data('value'));
                    $this.prop('disabled', false);
                }
            });
        });
});