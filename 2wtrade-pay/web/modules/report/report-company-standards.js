$(function () {
    DeliveryList.init();
});

var DeliveryList = {
    $modal: $('#delivery_modal'),

    init: function () {
        $('#table-company-standard').find("td").each(function (key, val) {
            if (val.innerHTML.charAt(0) == '-' || $(val).find('span.parent-danger').get(0)) {
                $(val).addClass('bg-danger');
            } else if ($(val).find('span.parent-warning').get(0)) {
                $(val).addClass('custom-warning');
            }
        });

        $('.view-delivery-modal').on('click', function (el) {
            DeliveryList.$modal.find("#payments-body").html(el.target.getAttribute('data-content'));
            DeliveryList.$modal.find("#payments-body").find("td").each(function (key, val) {
                if (val.innerHTML.charAt(0) == '-') {
                    $(val).addClass('bg-danger');
                }
            });
            DeliveryList.$modal.modal();
        });
    },

    close: function () {
        DeliveryList.$modal.modal('hide');
    }
};
