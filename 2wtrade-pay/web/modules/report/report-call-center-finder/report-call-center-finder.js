$(function () {
    ReportCallCenterFinder.init();
});

var ReportCallCenterFinder = {
    ajax: null,
    init: function () {
        $('.shower-columns-checkbox').on('change', ReportCallCenterFinder.toggleColumn);
        $('.show-call-center-response').on('click', ReportCallCenterFinder.showCCResponse);
    },
    toggleColumn: function () {
        if (ReportCallCenterFinder.ajax) {
            ReportCallCenterFinder.ajax.abort();
        }

        var dataAjax = {
            column: $(this).data('column'),
            choose: $(this).prop('checked') ? 1 : 0
        };

        ReportCallCenterFinder.ajax = $.ajax({
            type: 'POST',
            url: location.href,
            dataType: 'json',
            data: dataAjax,
            success: function (response, textStatus) {
                if (typeof response.content != 'undefined') {
                    var $table = $('#records_content');

                    $table.replaceWith(response.content);
                    if ($('#records_content').length) {
                        ReportCallCenterFinder.init();
                    }
                }
            }
        });
    },
    showCCResponse: function (e) {
        e.preventDefault();
        var id = $(this).data('id');

        var text = $('.call-center-responses #call_center_response_'+id).text();
        $('#modal_report_call_center_response_viewer .report-response-viewer-text').text(text);
        $('#modal_report_call_center_response_viewer').modal();
    }
};
