$(function () {
    DebtsDynamic.init();
});

var DebtsDynamic = {

    init: function () {
        $('#debts_dynamic_reload').on('click', function () {
            $(this).hide();
            $.ajax({
                type: 'GET',
                url: $(this).data('url'),
                success: function (response) {
                    $.notify({message: response.message}, {
                        type: "success dark",
                        animate: {exit: 'hide'},
                        z_index: 2031
                    });
                },
                error: function (response) {
                    $.notify({message: response.message}, {
                        type: "danger dark",
                        animate: {exit: 'hide'},
                        z_index: 2031
                    });
                }
            });
        });
    }
};
