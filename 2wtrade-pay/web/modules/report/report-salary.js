var I18n = I18n || {};

$(document).ready(function () {
    var $officeSelect = $('#s-office');
    var $callcenterSelect = $('#s-callcenter');
    var $teamleadSelect = $('#s-teamlead');
    var $monthlyStatementSelect = $('#s-monthlystatement');

    $officeSelect.select2().on('change', function () {

        $callcenterSelect.parent().hide();
        $teamleadSelect.parent().hide();
        $monthlyStatementSelect.parent().hide();

        $.ajax({
            url: "/report/salary/get-call-center",
            data: {
                'office': $officeSelect.val()
            },
            success: function (data) {
                if (data.status == false) {
                    return
                }

                $callcenterSelect.empty();
                $teamleadSelect.empty();
                $monthlyStatementSelect.empty();

                $callcenterSelect.append('<option value="0">-</option>');
                $teamleadSelect.append('<option value="0">-</option>');
                $monthlyStatementSelect.append('<option value="0">-</option>');

                data.centers.forEach(function (entry) {
                    $callcenterSelect.append('<option value=' + entry.id + '>' + entry.name + '</option>');
                });
                data.leads.forEach(function (entry) {
                    $teamleadSelect.append('<option value=' + entry.id + '>' + entry.name + '</option>');
                });
                data.statements.forEach(function (entry) {
                    $monthlyStatementSelect.append('<option value=' + entry.id + '>' + entry.name + '</option>');
                });

                $callcenterSelect.parent().show();
                $teamleadSelect.parent().show();
                $monthlyStatementSelect.parent().show();

                $callcenterSelect.select2();
                $teamleadSelect.select2();
                $monthlyStatementSelect.select2();
            }
        });
    });
});