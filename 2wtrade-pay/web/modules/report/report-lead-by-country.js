var I18n = I18n || {};

$(document).ready(function() {
    var $countrySelect = $('#s-country_ids');
    var $userSelect = $('#s-user_ids');
    $userSelect.select2().on('change', function() {
        $.ajax({
            url: I18n.getCountryUrl,
            data: {
                'curator-users': $userSelect.val()
            },
            success: function(data) {
                if (data.status == false) {
                    return
                }
                $countrySelect.val(data.message);
                $countrySelect.select2();
            }
        });
    })
    $countrySelect.select2().on('change', function(e) {});
});