$('.lnk_delivery_get_zips').on('click', function(event){

    event.preventDefault();
    $('.modal-zip-code-title').attr("style", "");
    $('#delivery_modal_zip_codes_area').text("");
    var url = $(this).attr('data-url');

    $.ajax({
        url: url,
        type: 'POST',
        data: {},
        success: function(data) {
            $('.modal-zip-code-title').attr("style", "display: none;");
            $('#delivery_modal_zip_codes_area').text(data);
        }
    });

});


