var I18n = I18n || {};

$(document).ready(function () {
    var $officeSelect = $('#s-office');
    var $callcenterSelect = $('#s-callcenter');
    var $teamleadSelect = $('#s-teamlead');

    $officeSelect.select2().on('change', function () {

        $callcenterSelect.parent().hide();
        $teamleadSelect.parent().hide();

        $.ajax({
            url: "/report/call-center-operators-online/get-call-center",
            data: {
                'office': $officeSelect.val()
            },
            success: function (data) {
                if (data.status == false) {
                    return
                }

                $callcenterSelect.empty();
                $teamleadSelect.empty();

                $callcenterSelect.append('<option value="0">-</option>');
                $teamleadSelect.append('<option value="0">-</option>');

                data.centers.forEach(function (entry) {
                    $callcenterSelect.append('<option value=' + entry.id + '>' + entry.name + '</option>');
                });
                data.leads.forEach(function (entry) {
                    $teamleadSelect.append('<option value=' + entry.id + '>' + entry.name + '</option>');
                });

                $callcenterSelect.parent().show();
                $teamleadSelect.parent().show();

                $callcenterSelect.select2();
                $teamleadSelect.select2();
            }
        });
    });
});