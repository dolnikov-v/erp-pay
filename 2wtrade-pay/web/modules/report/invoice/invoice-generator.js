var I18n = I18n || {};

$(function () {
    InvoiceGenerator.init();
});

var InvoiceGenerator = {
    modal: $('#modal_create_invoice'),
    ajax: null,
    isBlocked: false,
    init: function () {
    },
    show: function () {
        InvoiceGenerator.modal.modal();
        $('#start_create_invoice').on('click', InvoiceGenerator.startGenerate);
        InvoiceGenerator.modal.on('hide.bs.modal', InvoiceGenerator.hide);
        InvoiceGenerator.initStartView();
    },
    initStartView: function () {
        InvoiceGenerator.modal.find('.row-with-text-progress').addClass('hidden');
        InvoiceGenerator.modal.find('.row-with-text-start').removeClass('hidden');
        InvoiceGenerator.modal.find('.row-with-btn-start').removeClass('hidden');
        InvoiceGenerator.modal.find('button.close').removeClass('hidden');
    },
    startGenerate: function (e) {
        InvoiceGenerator.modal.find('.row-with-text-progress').removeClass('hidden');
        InvoiceGenerator.modal.find('.row-with-text-start').addClass('hidden');
        InvoiceGenerator.modal.find('.row-with-btn-start').addClass('hidden');
        InvoiceGenerator.modal.find('button.close').addClass('hidden');

        InvoiceGenerator.isBlocked = true;
        var data = InvoiceGenerator.modal.find('form').data('query-params');
        var payment_date = InvoiceGenerator.modal.find('form input[name="payment_due_date"]').val();
        if(payment_date != '') {
            data['payment_due_date'] = payment_date;
        }

        data['requisite_delivery_id'] = $(InvoiceGenerator.modal).data('requisite_delivery_id');

        InvoiceGenerator.ajax = $.ajax({
            type: 'POST',
            url: InvoiceGenerator.modal.find('form').attr('action'),
            dataType: 'json',
            data: data,
            cache: false,
            success: function (response, textStatus) {
                InvoiceGenerator.isBlocked = false;
                if (response.status == 'success') {
                    MainBootstrap.addNotificationSuccess(I18n['Инвойс успешно создан, ожидайте генерации файлов.']);
                    if (typeof response.finance_table_content != 'undefined' || typeof response.invoice_table_content != 'undefined') {
                        if (typeof response.finance_table_content != 'undefined') {
                            $('#preview_invoice_finance_table').html(response.finance_table_content);
                        }
                        if (typeof response.invoice_table_content != 'undefined') {
                            $('#invoices_table_container').html(response.invoice_table_content);
                        }
                        InvoiceHandler.bindingEventHandlers();
                    }
                    InvoiceGenerator.modal.modal('hide');
                } else {
                    MainBootstrap.addNotificationDanger(response.message);
                }
                InvoiceGenerator.initStartView();
                InvoiceGenerator.ajax = null;
            },
            error: function (response) {
                InvoiceGenerator.initStartView();
                InvoiceGenerator.isBlocked = false;
                InvoiceGenerator.ajax = null;

                if (response.statusText != 'abort') {
                    if (typeof response.message != 'undefined') {
                        MainBootstrap.addNotificationDanger(response.message);
                    } else {
                        MainBootstrap.addNotificationDanger(I18n['Не удалось сгенерировать инвойс.']);
                    }
                }
            }
        });
        e.stopImmediatePropagation();
    },
    hide: function (e) {
        if (InvoiceGenerator.isBlocked) {
            e.preventDefault();
            return;
        }
    }
};