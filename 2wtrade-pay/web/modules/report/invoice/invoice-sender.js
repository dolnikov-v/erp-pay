var I18n = I18n || {};

$(function () {
    InvoiceSender.init();
});

var InvoiceSender = {
    modal: '#modal_invoice_modal_email_sender',
    buttonStartSendingInvoice: '#start_sending_invoice',
    isLocked: false,
    init: function () {
        $(InvoiceSender.buttonStartSendingInvoice).on('click', InvoiceSender.startSendingInvoice);
        $(InvoiceSender.modal).on('hide.bs.modal', InvoiceSender.hide);
    },
    showModal: function (button) {
        $(InvoiceSender.modal).modal();
        $(InvoiceSender.modal).find('.setting-block').hide();
        $(InvoiceSender.modal).find('.loading-block').show();
        $(InvoiceSender.buttonStartSendingInvoice).removeAttr('disabled');
        $(InvoiceSender.buttonStartSendingInvoice).removeAttr('data-loading');

        InvoiceSender.isLocked = false;

        $.ajax({
            url: button.data('href'),
            type: 'GET',
            data: {get_invoice_information: true},
            dataType: 'json',
            cache: false,
            success: function (response) {
                if (response.status == 'success' && typeof response.content != 'undefined') {
                    $(InvoiceSender.modal).find('.settings-block-settings').html(response.content);
                    $(InvoiceSender.modal).find('input[data-plugin="datetimepicker"]').each(function () {
                        $(this).datetimepicker({
                            format: $(this).data('format'),
                            locale: $(this).data('locale'),
                            maxDate: $(this).data('maxdate'),
                            minDate: $(this).data('mindate')
                        })
                    });
                    $(InvoiceSender.modal).find('select[data-plugin="select2"]').each(function () {
                        $(this).select2({
                            multiple: $(this).data('multiple'),
                            width:'style',
                            minimumInputLength: $(this).data('minimum-input-length')
                        });
                    });
                    InvoiceSender.showSettings();
                } else {
                    MainBootstrap.addNotificationDanger(response.message);
                    $(InvoiceSender.modal).modal('hide');
                }
            },
            error: function (response) {
                $(InvoiceSender.modal).modal('hide');
                if (response.statusText != 'abort') {
                    if (typeof response.message != 'undefined') {
                        MainBootstrap.addNotificationDanger(response.message);
                    } else {
                        MainBootstrap.addNotificationDanger(I18n['Не удалось получить информацию об инвойсе.']);
                    }
                }
            }

        });
    },
    showSettings: function () {
        $(InvoiceSender.modal).find('.setting-block').show();
        $(InvoiceSender.modal).find('.loading-block').hide();
    },
    startSendingInvoice: function (e) {

        InvoiceSender.isLocked = true;
        var form = $(InvoiceSender.modal).find('.form-invoice-data');

        $.ajax({
            url: form.attr('action'),
            type: 'POST',
            data: form.serialize(),
            dataType: 'json',
            cache: false,
            success: function (response) {
                InvoiceSender.isLocked = false;
                if (response.status == 'success') {
                    MainBootstrap.addNotificationSuccess(response.message);
                    InvoiceHandler.reloadInvoiceList();
                    $(InvoiceSender.modal).modal('hide');
                } else {
                    MainBootstrap.addNotificationDanger(response.message);
                    InvoiceSender.showSettings();
                }
                $(InvoiceSender.buttonStartSendingInvoice).removeAttr('disabled');
                $(InvoiceSender.buttonStartSendingInvoice).removeAttr('data-loading');
            },
            error: function (response) {
                $(InvoiceSender.buttonStartSendingInvoice).removeAttr('disabled');
                $(InvoiceSender.buttonStartSendingInvoice).removeAttr('data-loading');
                InvoiceSender.isLocked = false;
                InvoiceSender.showSettings();
                if (response.statusText != 'abort') {
                    if (typeof response.message != 'undefined') {
                        MainBootstrap.addNotificationDanger(response.message);
                    } else {
                        MainBootstrap.addNotificationDanger(I18n['Не удалось отправить инвойс.']);
                    }
                }
            }

        });
        e.preventDefault();
    },
    hide: function (e) {
        if (InvoiceSender.isLocked) {
            e.preventDefault();
            return;
        }
    },
};