var I18n = I18n || {};

$(function () {
    InvoiceCloser.init();
});

var InvoiceCloser = {
    modal: '#modal_invoice_modal_close',
    buttonStartClosingInvoice: '#start_closing_invoice',
    isLocked: false,
    init: function () {
        $(InvoiceCloser.buttonStartClosingInvoice).on('click', InvoiceCloser.startClosingInvoice);
        $(InvoiceCloser.modal).on('hide.bs.modal', InvoiceCloser.hide);
    },
    showModal: function (button) {
        $(InvoiceCloser.modal).modal();
        $(InvoiceCloser.modal).find('.setting-block').hide();
        $(InvoiceCloser.modal).find('.loading-block .closing-invoice').hide();
        $(InvoiceCloser.modal).find('.loading-block').show();
        $(InvoiceCloser.modal).find('.loading-block .loading-information').show();

        InvoiceCloser.isLocked = false;

        $.ajax({
            url: button.data('href'),
            type: 'GET',
            data: {get_invoice_information: true},
            dataType: 'json',
            cache: false,
            success: function (response) {
                if (response.status == 'success' && typeof response.content != 'undefined') {
                    $(InvoiceCloser.modal).find('.settings-block-settings').html(response.content);
                    $(InvoiceCloser.modal).find('input[data-plugin="datetimepicker"]').each(function () {
                        $(this).datetimepicker({
                            format: $(this).data('format'),
                            locale: $(this).data('locale'),
                            maxDate: $(this).data('maxdate'),
                            minDate: $(this).data('mindate')
                        })
                    });
                    $(InvoiceCloser.modal).find('select[data-plugin="select2"]').each(function () {
                        $(this).select2({
                            multiple: $(this).data('multiple'),
                            width:'style',
                            minimumInputLength: $(this).data('minimum-input-length')
                        });
                    });
                    InvoiceCloser.showSettings();
                } else {
                    MainBootstrap.addNotificationDanger(response.message);
                    $(InvoiceCloser.modal).modal('hide');
                }
            },
            error: function (response) {
                $(InvoiceCloser.modal).modal('hide');
                if (response.statusText != 'abort') {
                    if (typeof response.message != 'undefined') {
                        MainBootstrap.addNotificationDanger(response.message);
                    } else {
                        MainBootstrap.addNotificationDanger(I18n['Не удалось получить информацию об инвойсе.']);
                    }
                }
            }

        });
    },
    showSettings: function () {
        $(InvoiceCloser.modal).find('.setting-block').show();
        $(InvoiceCloser.modal).find('.loading-block').hide();
    },
    startClosingInvoice: function (e) {
        $(InvoiceCloser.modal).find('.setting-block').hide();
        $(InvoiceCloser.modal).find('.loading-block .closing-invoice').show();
        $(InvoiceCloser.modal).find('.loading-block').show();
        $(InvoiceCloser.modal).find('.loading-block .loading-information').hide();

        InvoiceCloser.isLocked = true;
        var form = $(InvoiceCloser.modal).find('.form-invoice-data');

        $.ajax({
            url: form.attr('action'),
            type: 'POST',
            data: form.serialize(),
            dataType: 'json',
            cache: false,
            success: function (response) {
                InvoiceCloser.isLocked = false;
                if (response.status == 'success') {
                    MainBootstrap.addNotificationSuccess(response.message);
                    InvoiceHandler.reloadInvoiceList();
                    $(InvoiceCloser.modal).modal('hide');
                } else {
                    MainBootstrap.addNotificationDanger(response.message);
                    InvoiceCloser.showSettings();
                }
            },
            error: function (response) {
                InvoiceCloser.isLocked = false;
                InvoiceCloser.showSettings();
                if (response.statusText != 'abort') {
                    if (typeof response.message != 'undefined') {
                        MainBootstrap.addNotificationDanger(response.message);
                    } else {
                        MainBootstrap.addNotificationDanger(I18n['Не удалось закрыть инвойс.']);
                    }
                }
            }

        });
        e.preventDefault();
        e.stopImmediatePropagation();
    },
    hide: function (e) {
        if (InvoiceCloser.isLocked) {
            e.preventDefault();
            return;
        }
    }
};