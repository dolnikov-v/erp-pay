var I18n = I18n || {};

$(function () {
    InvoiceHandler.init(true);
    InvoiceHandler.requisiteFunctionalInit();
});

var InvoiceHandler = {
    delayTime: 5000,
    updateInvoiceTable: null,
    setRequisiteModal: '#set-requisite-to-invoice',
    buttonGenerateInvoice: '.button-generate-invoice',
    closeRequisiteBtn: '#close-button',
    ActionRequisiteBtn: '#action-button',
    modalGenerateInvoice: '#modal_create_invoice',
    buttonCloseInvoice: '.close-invoice-link',
    buttonSendInvoice: '.send-invoice-link',
    requisiteFunctionalInit: function () {
        $(InvoiceHandler.setRequisiteModal).data('backdrop', "static");
        $(InvoiceHandler.setRequisiteModal).data('keyboard', false);

        $(InvoiceHandler.closeRequisiteBtn).on('click', InvoiceHandler.actionCloseModalRequisite);
        $(InvoiceHandler.ActionRequisiteBtn).on('click', InvoiceHandler.prepareModalToCreateInvoice);

        $(InvoiceHandler.setRequisiteModal).on('hide.bs.modal', function () {
            InvoiceHandler.resetGenerateInvoiceButton();
        });

    },
    init: function (sendRequest) {
        if (sendRequest) {
            InvoiceHandler.bindingEventHandlers();
            InvoiceHandler.updateInvoiceTable = setTimeout(InvoiceHandler.reloadInvoiceList, InvoiceHandler.delayTime);
        }
        InvoiceHandler.initCurrencyTriggers();
    },
    bindingEventHandlers: function () {
        Ladda.bind(InvoiceHandler.buttonGenerateInvoice);
        //oldAction
        //$(InvoiceHandler.buttonGenerateInvoice).on('click', InvoiceHandler.prepareModalToCreateInvoice);

        //new Action
        $(InvoiceHandler.buttonGenerateInvoice).off('click');
        $(InvoiceHandler.buttonGenerateInvoice).on('click', InvoiceHandler.showModalRequisite);

        $(InvoiceHandler.modalGenerateInvoice).on('hide.bs.modal', InvoiceHandler.resetGenerateInvoiceButton);
        $('.regenerate-invoice-button').on('click', InvoiceHandler.regenerateInvoice);
        $('.approve-invoice-button').on('click', InvoiceHandler.approveInvoice);
        $(InvoiceHandler.buttonCloseInvoice).on('click', InvoiceHandler.showCloseModal);
        $(InvoiceHandler.buttonSendInvoice).on('click', InvoiceHandler.showSendModal);
    },

    showModalRequisite: function () {
        InvoiceHandler.sendRequestToServer();
        InvoiceHandler.showSpinner();
        InvoiceHandler.killUpdateInvoiceTable();
        InvoiceHandler.clearRequisiteList();
        $(InvoiceHandler.setRequisiteModal).modal('show');
        $(InvoiceHandler.ActionRequisiteBtn).prop('disabled', true);
        InvoiceHandler.resetGenerateInvoiceButton();
    },
    actionCloseModalRequisite: function () {
        $(InvoiceHandler.setRequisiteModal).modal('hide');
        $(InvoiceHandler.ActionRequisiteBtn).prop('disabled', true);
        InvoiceHandler.init(true);
        InvoiceHandler.resetGenerateInvoiceButton();
        InvoiceHandler.clearRequisiteList();

        return false;
    },
    afterClosingModalRequisite: function () {

    },
    clearRequisiteList: function () {
        $('.list-requisites').html('');
    },
    killUpdateInvoiceTable: function () {
        if (InvoiceHandler.updateInvoiceTable) {
            clearTimeout(InvoiceHandler.updateInvoiceTable);
            InvoiceHandler.updateInvoiceTable = null;
        }

        InvoiceHandler.init(false);
    },
    hideSpinner: function () {
        $('.spinner').hide();
    },
    showSpinner: function () {
        $('.spinner').show();
    },
    /**
     * Запрос для получения данных по реквизитам для КС
     * @param callback
     */
    sendRequestToServer: function (callback) {
        $.ajax({
            url: '/catalog/requisite-delivery/get-requisite-by-invoice',
            type: 'POST',
            data: {
                delivery_id: $("[name*=delivery_ids]").val(),
                country_id: $("[name*=country_ids]").val(),
                from: $("[name*=from]").val(),
                to: $("[name*=to]").val()
            },
            dataType: 'json',
            cache: false,
            success: function (data) {
                InvoiceHandler.hideSpinner();
                if (data.success) {
                    InvoiceHandler.renderRequisitesList(data.message);
                    if (data.default_requisite_delivery_id) {
                        var radio = $(InvoiceHandler.setRequisiteModal + ' #requisite_' + data.default_requisite_delivery_id);
                        radio.closest('.clickable').trigger('click');
                        $(InvoiceHandler.ActionRequisiteBtn).prop('disabled', false);
                    }
                } else {
                    InvoiceHandler.renderRequisitesError(data.message);
                }

                if (typeof callback === 'function') {
                    callback();
                }
            },
            error: function (error) {
                InvoiceHandler.hideSpinner();
                InvoiceHandler.renderRequisitesError(error.responseText);

                if (typeof callback === 'function') {
                    callback();
                }
            }
        });
    },
    /**
     * Вывод ошибки в модальном окне
     * @param data
     */
    renderRequisitesError: function (data) {
        $('<div>', {class: 'alert alert-danger', html: data}).appendTo($('.list-requisites'));
        $('<input/>', {type: 'button', class: 'btn btn-info add_requisites', 'value': 'Добавить реквизиты'}).appendTo($('.list-requisites'));

        $('.add_requisites').on('click', function(){
            window.open(location.origin + '/catalog/requisite-delivery/index');
            $(InvoiceHandler.setRequisiteModal).modal('hide');
        });
    },
    /**
     * Парсинг реквизита
     * @param data
     */
    parseRequisite: function (data) {
        var tpl = $('.tpl_requisite').html();

        return tpl
            .replace(/{id}/g, data.id)
            .replace(/{name}/g, data.name)
            .replace(/{city}/g, data.city)
            .replace(/{address}/g, data.address)
            .replace(/{bank}/g, data.beneficiary_bank)
            .replace(/{bank_address}/g, data.bank_address)
            .replace(/{bank_account}/g, data.bank_account)
            .replace(/{swift_code}/g, data.swift_code)
            .replace(/{checked}/g, false);
    },
    /**
     * Отрисовка каждого реквизита в молаьное окно
     * @param data
     */
    renderRequisitesList: function (data) {

        var container = $('.list-requisites');

        for (var k in data) {
            container.append(InvoiceHandler.parseRequisite(data[k]));
        }

        container.find('[type=radio]').map(function () {
            $(this).on('click', function () {

                $(InvoiceHandler.ActionRequisiteBtn).prop('disabled', false);
                $('.clickable').css('background-color', 'white');

                if ($(this).prop('checked')) {
                    $(this).closest('.clickable').css('background-color', '#fcf9e3');
                }
            });
        });

        container.find('.toggle').on('click', function () {
            container.find('.info').addClass('hidden');
            container.find('.toggle_' + $(this).data('id')).removeClass('hidden');
        });

        container.find('.clickable').map(function () {
            var radio = $(this).find('[type=radio]');

            if (radio.prop('checked')) {
                return false;
            }

            $(this).css('cursor', 'pointer');

            $(this).on('click', function () {
                $(InvoiceHandler.ActionRequisiteBtn).prop('disabled', false);
                $('.clickable').css('background-color', 'white');

                radio.prop('checked', true);

                $(this).css('background-color', '#fcf9e3');
            });

        });
    },
    getSelectedRequisite: function () {
        $('.list-requisites').find("[type=radio]").map(function () {
            if ($(this).prop('checked')) {
                $(InvoiceHandler.modalGenerateInvoice).attr('data-requisite_delivery_id', $(this).val());
                return true;
            }
        });
    },
    initCurrencyTriggers: function () {
        $('.preview-invoice-currencies-row .preview-invoice-currencies-date input').on('dp.change', InvoiceHandler.getCurrencyRate);
        $('.preview-invoice-currencies-row .preview-invoice-currencies-currency-id select').on('change', InvoiceHandler.getCurrencyRate);
    },
    prepareModalToCreateInvoice: function (e) {
        $(InvoiceHandler.setRequisiteModal).modal('hide');
        InvoiceHandler.init(true);
        $(InvoiceHandler.modalGenerateInvoice).find('form').attr('action', $(InvoiceHandler.buttonGenerateInvoice).data('url'));
        InvoiceHandler.getSelectedRequisite();
        InvoiceGenerator.show();
    },
    resetGenerateInvoiceButton: function () {
        $(InvoiceHandler.buttonGenerateInvoice).removeAttr('disabled');
        $(InvoiceHandler.buttonGenerateInvoice).removeAttr('data-loading');
    },
    showCloseModal: function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        InvoiceCloser.showModal($(this));
    },
    showSendModal: function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        InvoiceSender.showModal($(this));
    },
    regenerateInvoice: function (e) {
        $.ajax({
            url: $(this).attr('href'),
            dataType: 'json',
            cache: false,
            success: function (response, textStatus) {
                if (response.status == 'success') {
                    if (typeof response.invoice_table_content != 'undefined') {
                        $('#invoices_table_container').html(response.invoice_table_content);
                        InvoiceHandler.bindingEventHandlers();
                    }
                } else {
                    MainBootstrap.addNotificationDanger(response.message);
                }
            },
            error: function (response) {
                if (response.statusText != 'abort') {
                    if (typeof response.message != 'undefined') {
                        MainBootstrap.addNotificationDanger(response.message);
                    } else {
                        MainBootstrap.addNotificationDanger(I18n['Не удалось отправить инвойс на генерацию.']);
                    }
                }
            }
        });
        e.preventDefault();
        e.stopImmediatePropagation();
    },
    approveInvoice: function (e) {
        $.ajax({
            url: $(this).attr('href'),
            dataType: 'json',
            cache: false,
            success: function (response, textStatus) {
                if (response.status == 'success') {
                    if (typeof response.invoice_table_content != 'undefined') {
                        $('#invoices_table_container').html(response.invoice_table_content);
                        InvoiceHandler.bindingEventHandlers();
                    }
                } else {
                    MainBootstrap.addNotificationDanger(response.message);
                }
            },
            error: function (response) {
                if (response.statusText != 'abort') {
                    if (typeof response.message != 'undefined') {
                        MainBootstrap.addNotificationDanger(response.message);
                    } else {
                        MainBootstrap.addNotificationDanger(I18n['Не удалось утвердить инвойс.']);
                    }
                }
            }
        });
        e.preventDefault();
        e.stopImmediatePropagation();
    },
    reloadInvoiceList: function () {
        $.ajax({
            url: $('#invoices_table_container').data('reload-url'),
            dataType: 'json',
            cache: false,
            success: function (response, textStatus) {
                if (response.status == 'success') {
                    if (typeof response.invoice_table_content != 'undefined') {

                        $('#invoices_table_container').html(response.invoice_table_content);
                        InvoiceHandler.bindingEventHandlers();
                    }
                } else {
                    MainBootstrap.addNotificationDanger(response.message);
                }
                InvoiceHandler.updateInvoiceTable = setTimeout(InvoiceHandler.reloadInvoiceList, InvoiceHandler.delayTime);
            },
            error: function (response) {
                InvoiceHandler.updateInvoiceTable = setTimeout(InvoiceHandler.reloadInvoiceList, InvoiceHandler.delayTime);
            }
        });
    },
    updateCurrencyListTriggers: function (newRow, templateRow) {
        var template = newRow.closest('.changeable-list').find('.changeable-list-template')
        var templateSelect2Options = template.find('.preview-invoice-currencies-currency-id select').data('select2').options.options;
        var templateDateOptions = template.find('.preview-invoice-currencies-date input').data('DateTimePicker').options();
        var select = newRow.find('.preview-invoice-currencies-currency-id select');
        newRow.find('.preview-invoice-currencies-currency-id .select2-container').remove();
        select.select2(templateSelect2Options);
        select.val(templateRow.find('.preview-invoice-currencies-currency-id select').val()).trigger('change')
        var dateInput = newRow.find('.preview-invoice-currencies-date input');
        dateInput.datetimepicker(templateDateOptions);
        templateRow.find('.preview-invoice-currencies-currency-id select').val('').trigger('change');
        templateRow.find('.preview-invoice-currencies-date input').data('DateTimePicker').date('');
        templateRow.find('.preview-invoice-currencies-date input').val('');
        templateRow.find('.preview-invoice-currencies-rate input').val('');

        // Инициализируем эвенты
        dateInput.on('dp.change', InvoiceHandler.getCurrencyRate);
        select.on('change', InvoiceHandler.getCurrencyRate);
    },
    getCurrencyRate: function () {
        // Чтобы не срабатывал datetimepicker при загрузке, ставим флаг
        if ((typeof $(this).data('init')) != 'undefined') {
            if ($(this).data('init') == 0) {
                $(this).data('init', 1);
                return;
            }
        }

        var parent = $(this).closest('.preview-invoice-currencies-row');
        var url = parent.data('url');
        const currencyId = parent.find('.preview-invoice-currencies-currency-id select').val();
        const date = parent.find('.preview-invoice-currencies-date input').val();
        const invoiceCurrencyId = $(this).closest('.preview-invoice-currency-settings').find('.preview-invoice-currency-settings-invoice-currency-id select').val()
        if (currencyId.length === 0 || invoiceCurrencyId.length === 0) {
            return;
        }
        var dataAjax = {
            currency_id: currencyId,
            date: date,
            invoice_currency_id: invoiceCurrencyId
        };
        $.ajax({
            type: 'GET',
            url: url,
            dataType: 'json',
            data: dataAjax,
            success: function (response, textStatus) {
                if (response.status == 'success') {
                    parent.find('.preview-invoice-currencies-rate input').val(response.rate);
                } else {
                    $.notify({message: response.message}, {
                        type: "danger dark",
                        animate: {exit: 'hide'},
                        z_index: 2031
                    });
                }
            },
            error: function (response) {

                if (response.statusText != 'abort') {
                    if (typeof response.message != 'undefined') {
                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    } else {
                        $.notify({message: I18n['Не удалось получить информацию о курсе валют.']}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                }
            }
        });
    }
};