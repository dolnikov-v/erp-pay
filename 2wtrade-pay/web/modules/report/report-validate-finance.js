$(function () {
    ValidateFinance.init();
});

var ValidateFinance = {
    ajax: null,
    $progress: $('#validate-finance'),
    $form: $('#validate-finance form'),
    $submit: $('#validate-finance form button[type="submit"]'),
    count: 0,
    currentCount: 0,
    timer: 0,

    init: function () {
        ValidateFinance.$form.on('submit', ValidateFinance.submit);
    },

    initStop: function () {
        ValidateFinance.$progress.find('.row-with-text-progress').hide();
        ValidateFinance.$progress.find('.row-with-text-finish').hide();
        ValidateFinance.$submit.removeAttr("disabled");
    },

    initProgress: function () {
        ValidateFinance.$progress.find('.row-with-text-finish').hide();
        ValidateFinance.$progress.find('.row-with-text-progress').show();
        ValidateFinance.$submit.attr("disabled", true);
    },

    initFinish: function (file) {
        ValidateFinance.$progress.find('.row-with-text-progress').hide();
        ValidateFinance.$progress.find('.finish-link').attr('href', '?file=' + file);
        ValidateFinance.$progress.find('.row-with-text-finish').show();
        ValidateFinance.$submit.removeAttr("disabled");
    },

    submit: function (event) {
        fileOnLoad = $("#loadFile")[0].files[0];
        ValidateFinance.count = fileOnLoad.size / 1000;
        ValidateFinance.currentCount = 0;
        ValidateFinance.initProgress();
        ValidateFinance.timer = setInterval(ValidateFinance.tick, 1000);
        event.preventDefault();
        var fd = new FormData();
        fd.append("formData", ValidateFinance.$form.serialize());
        fd.append("s[loadFile]", fileOnLoad);
        ValidateFinance.load(fd);
        event.stopImmediatePropagation();
        return false;
    },

    load: function (fd) {
        ValidateFinance.ajax = $.ajax({
            type: 'POST',
            url: ValidateFinance.$form.attr('action'),
            dataType: 'json',
            contentType: false,
            processData: false,
            enctype: 'multipart/form-data',
            data: fd,
            success: function (response, textStatus) {
                if (response.status == 'success') {
                    ValidateFinance.parse(response.file, 1);
                } else {
                    ValidateFinance.initStop();
                    MainBootstrap.addNotificationDanger(response.message);
                }
            },
            error: function (response) {
                ValidateFinance.initStop();
                MainBootstrap.addNotificationDanger(response.responseText);
            }
        });
    },

    parse: function (file, currentStr) {
        var fd = new FormData();
        fd.append("file", file);
        fd.append("currentStr", currentStr);
        ValidateFinance.ajax = $.ajax({
            type: 'POST',
            url: '/report/validate-finance/parse',
            dataType: 'json',
            contentType: false,
            processData: false,
            data: fd,
            success: function (response, textStatus) {
                if (response.status == 'success') {
                    ValidateFinance.initFinish(file);
                    MainBootstrap.addNotificationSuccess(I18n['Файл обработан']);
                } else if (response.status == 'fail') {
                    ValidateFinance.initStop();
                    MainBootstrap.addNotificationDanger(response.message);
                } else {
                    ValidateFinance.parse(file, response.currentStr);
                }
            },
            error: function (response) {
                ValidateFinance.initStop();
                MainBootstrap.addNotificationDanger(response.responseText);
            }
        });
    },

    tick: function () {
        ValidateFinance.currentCount++;

        var part = 100 / ValidateFinance.count;
        var percent = parseInt(ValidateFinance.currentCount * part);

        if (percent > 99) {
            percent = 99;
        }

        ValidateFinance.setPercentProgress(percent);

        if (ValidateFinance.currentCount >= ValidateFinance.count) {
            clearInterval(ValidateFinance.timer);
        }
    },

    setPercentProgress: function (percent) {
        ValidateFinance.$progress.find('.builder-percent').text(percent + '%');
        ValidateFinance.$progress.find('.progress-bar').css('width', percent + '%');
    }
};
