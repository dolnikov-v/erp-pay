$(function () {
    CheckList.init();
    $("#checklist_content .total_efficiency").insertAfter($("#checklist_content .countries"));
});

var CheckList = {
    $country: 0,
    $teamlead: 0,
    $office: 0,
    $item: 0,
    $date: $('input[name="CheckListSearch[date]"]').val(),
    $button: 0,
    $value: 1,
    init: function () {
        $(document).on('click', '.checkable', CheckList.checkList);
        $(document).on('click', '.viewable', CheckList.showDetails);
        $(document).on('click', '.more-info', CheckList.showInfo);
        $(document).on('click', '#modal_confirm_check_list_link', CheckList.doCheckList);
    },
    showInfo: function () {
        var d = $(this).parent().find('.check-description');
        var i = $(this).parent().find('.more-icon');
        if (d.is(":visible")) {
            i.html('+');
            d.slideUp();
        }
        else {
            i.html('-');
            d.slideDown();
        }
        return false;
    },
    showDetails: function () {
        var d = $(this).parent().find('.details_info');
        if (d.length) {
            var tr = $(this).closest("tr").next(".details_holder");
            if (tr.length) {
                var td = tr.find("td");
                if (td.length) {
                    var div = tr.find("div");
                    if ($(this).hasClass('activated')) {
                        div.slideUp().html('');
                        $('.activated').removeClass('activated');
                    }
                    else {
                        div.html(d.html()).slideDown();
                        $('.activated').removeClass('activated');
                        $(this).addClass('activated');
                    }
                }
            }
        }
    },
    checkList: function () {

        CheckList.$button = $(this);
        CheckList.$country = $(this).data("country_id");
        CheckList.$teamlead = $(this).data("teamlead_id");
        CheckList.$office = $(this).data("office_id");
        CheckList.$item = $(this).data("item");
        CheckList.$value = $(this).data('value');

        var $tr = $(this).closest('tr');

        $('#modal_confirm_check_list').modal();
        $('#modal_confirm_check_list_span').text($tr.find('.check-item').html());


        if (typeof CheckList.$country === 'undefined') {
            $('#modal_confirm_check_list_country').parent().hide();
        }
        else {
            $('#modal_confirm_check_list_country').parent().show();
            $('#modal_confirm_check_list_country').text($(this).data("country"));
        }
        $('#modal_confirm_check_list_date').text(CheckList.$date);

        if ($(this).hasClass('explainable')) {
            $('#modal_confirm_check_explain_wrap').show();
            $('#modal_confirm_check_explain_text').val($(this).data("comment"));
            CheckList.$value = 0;
        }
        else {
            $('#modal_confirm_check_explain_wrap').hide();
        }

        if (CheckList.$value == 1) {
            $('#modal_confirm_check_true').hide();
            $('#modal_confirm_check_false').show();
        }
        else {
            $('#modal_confirm_check_true').show();
            $('#modal_confirm_check_false').hide();
        }

        return false;
    },
    doCheckList: function (e) {
        e.preventDefault();

        $.ajax({
            type: 'POST',
            url: $('#modal_confirm_check_list').data('url'),
            dataType: 'json',
            data: {
                country_id: CheckList.$country,
                team_id: $('#checklistsearch-team_id').val(),
                item_id: CheckList.$item,
                teamlead_id: CheckList.$teamlead,
                office_id: CheckList.$office,
                date: CheckList.$date,
                value: CheckList.$value ? 0 : 1,
                text: $('#modal_confirm_check_explain_text').val()
            },
            success: function (response, textStatus) {
                $('#modal_confirm_check_list').modal('hide');
                $('#modal_confirm_check_explain_text').val('');

                var th = CheckList.$button;
                th.prop('title', $.trim(response.comment + ' ' + response.date));
                th.data('comment', response.comment);
                if (response.value == 'ok') {
                    th.addClass("btn-success").removeClass("btn-danger");
                    th.find('i').removeClass("wb-close").addClass("wb-check");
                    th.data('value', 1);
                }
                else {
                    th.removeClass("btn-success").addClass("btn-danger");
                    th.find('i').addClass("wb-close").removeClass("wb-check");
                    th.data('value', 0);
                }
                if (th.hasClass('autocheck')) {
                    th.find('i').removeClass('wb-help');
                }

                if (response.status != 'success' || th.hasClass('autocheck')) {

                    var type = 'info';
                    if (response.status != 'success') {
                        type = 'danger';
                    }
                    $.notify({message: response.message}, {
                        type: type + " dark",
                        animate: {exit: 'hide'},
                        z_index: 2031
                    });
                }
            }
        });

    }
};
