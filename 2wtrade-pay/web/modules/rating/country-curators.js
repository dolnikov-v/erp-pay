$(document).ready(function () {

    chunked = [];

    var length = $("#params_count").html();
    var interval = $("#params_interval").html()*1000;

    var data = [];

    $(".user").map(function () {
        data.push($(this)[0].id)
    });

    while (data.length > 0) {
        chunked.push(data.splice(0, length))
    }
    if(chunked.length > 0) {
        for (var i = 0; i < chunked[0].length; i++) {
            var id = chunked[0][i];
            $('#' + id).fadeIn(3000);
            $('#' + id.replace('u', 'c')).fadeIn(3000);
        }

        start = 0;

        window.setInterval(function () {
            start++;
            $(".row").css('display', 'none');
            $(".clear").css('display', 'none');

            if (typeof chunked[start] == 'undefined') {
                start = 0;
            }

            for (var i = 0; i < chunked[start].length; i++) {
                var id = chunked[start][i];
                $('#' + id).fadeIn(2000);
                $('#' + id.replace('u', 'c')).fadeIn(2000);
            }
        }, interval);
    }


});