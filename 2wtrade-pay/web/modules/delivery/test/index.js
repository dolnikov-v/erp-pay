$('.btn-success').on('click',function () {

    var form = $('form');
    var action = '/delivery/test/' + $(this).attr('id');

    var resultBody = $('div .panel-body');
    form.yiiActiveForm('data').submitting = true;
    form.yiiActiveForm('validate');
    $.ajax({
        type: 'POST',
        url: action,
        dataType: 'json',
        data: form.serialize(),
        success: function (response) {
            if (response.status == 'success') {
                console.log('success!!!');
                console.log(response);
                $('div .panel-body').text(response.message);
                resultBody.css( "background", 'rgba(0,204,0,0.3)' );
                resultBody.css( "color", 'black' );
            } else {
                resultBody.text(response.responseText || response.message);
                resultBody.css( "background", 'rgba(243,213,213,0.5)' );
                resultBody.css( "color", 'black' );
            }
        },
        error: function (response) {
            $('div .panel-body').text(response.responseText);
            resultBody.css( "background", 'rgba(243,213,213,0.5)' );
            resultBody.css( "color", 'black' );
        }
    });
});