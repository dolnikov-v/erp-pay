$(function () {

    var $widget = $('#widget-top-countries');


    $widget.on('click', '.sorting a', function () {

        $widget.animate({opacity: 0.7}, 500);

        var data =   {
            id: $widget.data('id'),
            sort: $(this).data('sort')
        };

        $.get('/widget/index/load', data, function (html) {
            $widget.html(html).stop().animate({opacity: 1}, 500);
        });

        return false;
    });
});
