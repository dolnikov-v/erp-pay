$(function () {
    if ($('#records_content').length) {
        ReportViewReport.init();
    }
});

var ReportViewReport = {

    $btnRecordApprover: $('#btn_set_approve'),
    $modalRecordApprover: $('#modal_set_approve'),
    $btnRecordDeApprover: $('#btn_unset_approve'),
    $modalRecordDeApprover: $('#modal_unset_approve'),
    $btnRecordRemover: $('#btn_remove_records'),
    $modalRecordRemover: $('#modal_remove_records'),
    $btnOrderStatusUpdater: $('#btn_update_status'),
    $modalOrderStatusUpdater: $('#modal_update_status'),
    $btnOrderTrackingUpdater: $('#btn_update_tracking'),
    $modalOrderTrackingUpdater: $('#modal_update_tracking'),
    $btnOrderDataUpdater: $('#btn_update_order_data'),
    $modalOrderDataUpdater: $('#modal_update_order_data'),
    $btnCreatePretension: $('#btn_create_pretension'),
    $btnRemovePretension: $('#btn_remove_pretension'),
    $modalCreatePretension: $('#modal_create_pretension'),
    $modalRemovePretension: $('#modal_remove_pretension'),
    $btnOrderPriceUpdater: $('#btn_update_prices'),
    $modalOrderPriceUpdater: $('#modal_update_prices'),
    $btnPaymentIdUpdater: $('#btn_update_payment_id'),
    $modalPaymentIdUpdater: $('#modal_update_payment_id'),
    $btnPaymentList: $('#btn_payment_list'),
    $modalPaymentList: $('#modal_payment_list'),

    $btnCreateOrderCopy: $('#btn_create_order_copy'),
    $modalCreateOrderCopy: $('#modal_create_order_copy'),

    init: function () {
        ReportViewReport.$btnRecordApprover.on('click', ReportViewReport.initRecordApprover);
        ReportViewReport.$modalRecordApprover.on('hide.bs.modal', ReportViewReport.destroyRecordApprover);

        ReportViewReport.$btnRecordDeApprover.on('click', ReportViewReport.initRecordDeApprover);
        ReportViewReport.$modalRecordDeApprover.on('hide.bs.modal', ReportViewReport.destroyRecordDeApprover);

        ReportViewReport.$btnRecordRemover.on('click', ReportViewReport.initRecordRemover);
        ReportViewReport.$modalRecordRemover.on('hide.bs.modal', ReportViewReport.destroyRecordRemover);

        ReportViewReport.$btnOrderStatusUpdater.on('click', ReportViewReport.initOrderStatusUpdater);
        ReportViewReport.$modalOrderStatusUpdater.on('hide.bs.modal', ReportViewReport.destroyOrderStatusUpdater);

        ReportViewReport.$btnOrderTrackingUpdater.on('click', ReportViewReport.initOrderTrackingUpdater);
        ReportViewReport.$modalOrderTrackingUpdater.on('hide.bs.modal', ReportViewReport.destroyOrderTrackingUpdater);

        ReportViewReport.$btnOrderDataUpdater.on('click', ReportViewReport.initOrderDataUpdater);
        ReportViewReport.$modalOrderDataUpdater.on('hide.bs.modal', ReportViewReport.destroyOrderDataUpdater);

        ReportViewReport.$btnCreatePretension.on('click', ReportViewReport.initCreatePretension);
        ReportViewReport.$modalCreatePretension.on('hide.bs.modal', ReportViewReport.destroyCreatePretension);

        ReportViewReport.$btnRemovePretension.on('click', ReportViewReport.initRemovePretension);
        ReportViewReport.$modalRemovePretension.on('hide.bs.modal', ReportViewReport.destroyRemovePretension);

        ReportViewReport.$btnOrderPriceUpdater.on('click', ReportViewReport.initOrderPriceUpdater);
        ReportViewReport.$modalOrderPriceUpdater.on('hide.bs.modal', ReportViewReport.destroyOrderPriceUpdater);

        ReportViewReport.$btnPaymentIdUpdater.on('click', ReportViewReport.initPaymentIdUpdater);
        ReportViewReport.$modalPaymentIdUpdater.on('hide.bs.modal', ReportViewReport.destroyPaymentIdUpdater);

        ReportViewReport.$btnPaymentList.on('click', ReportViewReport.initPaymentList);
        ReportViewReport.$modalPaymentList.on('hide.bs.modal', ReportViewReport.destroyPaymentList);

        ReportViewReport.$btnCreateOrderCopy.on('click', ReportViewReport.initOrderCopyCreator);
        ReportViewReport.$modalCreateOrderCopy.on('hide.bs.modal', ReportViewReport.destroyOrderCopyCreator);

        $('#records_content .th-custom-checkbox input[type="checkbox"]').on('change', ReportViewReport.toggleCheckboxes);
        $('#records_content .td-custom-checkbox input[type="checkbox"]').on('change', ReportViewReport.toggleCheckbox);
        $('a[name="error-type"]').on('click', ReportViewReport.addErrorTypeFilter);

        $('#finance_panel_date').on('dp.change', ReportViewReport.getCurrencyRate);

        $('#delivery_report_currency_form').on('click', ReportViewReport.setCurrencies);

        $('#payment_list_date_input').on('dp.change', ReportViewReport.getRates);
        $('[name="PaymentCurrencyRate[currency_id_from][]"]').on('change', ReportViewReport.getRates);
        $('[name="PaymentCurrencyRate[currency_id_to][]"]').on('change', ReportViewReport.getRates);
    },
    addErrorTypeFilter: function (e) {
        $object = $('#container_empty_filters').find('.container-empty-filter[data-filter="record_error"]').clone();
        if (typeof $object != 'undefined') {
            $('#row_selectable_filters').before($object);
            $object.find('select').val($(this).data('error-type'));
        }
        $(this).disabled = true;
        $('.delivery-report-view-actions .nav li').removeClass('active');
        $('.delivery-report-view-actions .nav li a[href=' + $(this).attr('href') + ']').parent().addClass('active');
        e.preventDefault();
    },
    toggleCheckboxes: function () {
        $('#records_content .td-custom-checkbox input[type="checkbox"]').prop('checked', $(this).prop('checked')).trigger('change');
    },

    toggleCheckbox: function () {
        var $tr = $(this).closest('tr');
        var id = $tr.data('key');
        if ($(this).prop('checked')) {
            $('tr[data-key="' + id + '"]').addClass('active');
        } else {
            $('tr[data-key="' + id + '"]').removeClass('active');
        }
    },
    initRecordApprover: function () {
        ReportViewReport.completeForm(ReportViewReport.$modalRecordApprover.find('form'));
        RecordApprover.show();
    },
    destroyRecordApprover: function () {
        ReportViewReport.$btnRecordApprover.removeAttr('disabled');
        ReportViewReport.$btnRecordApprover.removeAttr('data-loading');
        RecordApprover.stop();
    },
    initRecordDeApprover: function () {
        ReportViewReport.completeForm(ReportViewReport.$modalRecordDeApprover.find('form'));
        RecordDeApprover.show();
    },
    destroyRecordDeApprover: function () {
        ReportViewReport.$btnRecordDeApprover.removeAttr('disabled');
        ReportViewReport.$btnRecordDeApprover.removeAttr('data-loading');
        RecordDeApprover.stop();
    },
    initRecordRemover: function () {
        ReportViewReport.completeForm(ReportViewReport.$modalRecordRemover.find('form'));
        RecordRemover.show();
    },
    destroyRecordRemover: function () {
        ReportViewReport.$btnRecordRemover.removeAttr('disabled');
        ReportViewReport.$btnRecordRemover.removeAttr('data-loading');
        RecordRemover.stop();
    },
    initOrderStatusUpdater: function () {
        ReportViewReport.completeForm(ReportViewReport.$modalOrderStatusUpdater.find('form'));
        OrderStatusUpdater.show();
    },
    destroyOrderStatusUpdater: function () {
        ReportViewReport.$btnOrderStatusUpdater.removeAttr('disabled');
        ReportViewReport.$btnOrderStatusUpdater.removeAttr('data-loading');
        OrderStatusUpdater.stop();
    },
    initOrderTrackingUpdater: function () {
        ReportViewReport.completeForm(ReportViewReport.$modalOrderTrackingUpdater.find('form'));
        OrderTrackingUpdater.show();
    },
    destroyOrderTrackingUpdater: function () {
        ReportViewReport.$btnOrderTrackingUpdater.removeAttr('disabled');
        ReportViewReport.$btnOrderTrackingUpdater.removeAttr('data-loading');
        OrderTrackingUpdater.stop();
    },
    initOrderDataUpdater: function () {
        ReportViewReport.completeForm(OrderDataUpdater.$form);
        OrderDataUpdater.show();
    },
    destroyOrderDataUpdater: function () {
        ReportViewReport.$btnOrderDataUpdater.removeAttr('disabled');
        ReportViewReport.$btnOrderDataUpdater.removeAttr('data-loading');
        OrderDataUpdater.stop();
    },
    initCreatePretension: function () {
        ReportViewReport.completeForm(CreatePretension.$form);
        CreatePretension.show();
    },
    destroyCreatePretension: function () {
        ReportViewReport.$btnCreatePretension.removeAttr('disabled');
        ReportViewReport.$btnCreatePretension.removeAttr('data-loading');
        CreatePretension.stop();
    },
    initRemovePretension: function () {
        ReportViewReport.completeForm(ReportViewReport.$modalRemovePretension.find('form'));
        RemovePretension.show();
    },
    destroyRemovePretension: function () {
        ReportViewReport.$btnRemovePretension.removeAttr('disabled');
        ReportViewReport.$btnRemovePretension.removeAttr('data-loading');
        RemovePretension.stop();
    },
    initOrderPriceUpdater: function () {
        ReportViewReport.completeForm(OrderPriceUpdater.$form);
        OrderPriceUpdater.show();
    },
    destroyOrderPriceUpdater: function () {
        ReportViewReport.$btnOrderPriceUpdater.removeAttr('disabled');
        ReportViewReport.$btnOrderPriceUpdater.removeAttr('data-loading');
        OrderPriceUpdater.stop();
    },
    initPaymentIdUpdater: function () {
        PaymentIdUpdater.show();
    },
    destroyPaymentIdUpdater: function () {
        ReportViewReport.$btnPaymentIdUpdater.removeAttr('disabled');
        ReportViewReport.$btnPaymentIdUpdater.removeAttr('data-loading');
        PaymentIdUpdater.stop();
    },
    initPaymentList: function () {
        PaymentList.show();
    },
    destroyPaymentList: function () {
        ReportViewReport.$btnPaymentList.removeAttr('disabled');
        ReportViewReport.$btnPaymentList.removeAttr('data-loading');
    },
    initOrderCopyCreator: function () {
        ReportViewReport.completeForm(OrderCopyCreator.$form);
        OrderCopyCreator.show();
    },
    destroyOrderCopyCreator: function () {
        ReportViewReport.$btnCreateOrderCopy.removeAttr('disabled');
        ReportViewReport.$btnCreateOrderCopy.removeAttr('data-loading');
        OrderCopyCreator.stop();
    },
    completeForm: function ($form) {
        $form.find('input[name="id[]"]').remove();
        $form.find('input[name="offset[]"]').remove();

        $('#records_content input[name="id[]"]:checked').each(function () {
            $(this).clone().attr('type', 'hidden').val($(this).val()).appendTo($form);
        });
    },
    setCurrencies: function () {
        var form = $(this).parents('form');
        $.ajax({
            type: 'POST',
            url: form.attr('action'),
            dataType: 'json',
            data: form.serialize(),
            success: function (response, textStatus) {
                if (response.status == 'success') {
                    $.notify({message: 'Валюты успешно сохранены'}, {
                        type: "success dark",
                        animate: {exit: 'hide'},
                        z_index: 2031
                    });
                } else {
                    console.log(response);
                    $.notify({message: response.message}, {
                        type: "danger dark",
                        animate: {exit: 'hide'},
                        z_index: 2031
                    });
                }
            },
            error: function (response) {
                response = JSON.parse(response.responseText);
                if (response.statusText != 'abort') {
                    if (typeof response.message != 'undefined') {
                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    } else {
                        $.notify({message: I18n['Не удалось сохранить информацию.']}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                }
            }
        });
        return false;
    },
    getRates: function () {
        var form = $(this).parents('form');
        $.ajax({
            type: 'POST',
            url: form.attr('data-getrate'),
            dataType: 'json',
            data: form.serialize(),
            success: function (response, textStatus) {
                if (response.status == 'success') {
                    form.find('#payment_rates .changeable-list-element').each(function () {
                        var row = this;
                        $.each(response.answer, function () {
                            if ($(row).find('[name$="[currency_id_from][]"]').val() == this.from && $(row).find('[name$="[currency_id_to][]"]').val() == this.to) {
                                $(row).find('[name$="[rate][]"]').val(this.rate);
                            }
                        })
                    })
                }
            },
            error: function (response) {
                response = JSON.parse(response.responseText);
                console.log(response);
            }
        });
        return false;
    },
};
