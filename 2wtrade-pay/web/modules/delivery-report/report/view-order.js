var I18n = I18n || {};

$(function () {
    ViewOrder.init();
});

var ViewOrder = {
    init: function () {
        $('#show_fractional_orders').on('click', ViewOrder.showFractionalOrders);
    },
    showFractionalOrders: function (e) {
        $.ajax({
            type: 'GET',
            url: $(this).data('url'),
            dataType: 'json',
            success: function (response) {
                if (response.status == 'success' && typeof response.content != 'undefined') {
                    $('.record-info .fractional-orders').html(response.content);
                } else {
                    if (typeof response.message != 'undefined') {
                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    } else {
                        $.notify({message: I18n['Не удалось получить данные.']}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                }
            },
            error: function (response) {
                response = JSON.parse(response.responseText);

                if (response.statusText != 'abort') {
                    if (typeof response.message != 'undefined') {
                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    } else {
                        $.notify({message: I18n['Не удалось получить данные.']}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                }
            }
        })
    }
};
