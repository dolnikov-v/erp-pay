var I18n = I18n || {};

$(function () {
    DeliveryReportIndex.init();
});

var DeliveryReportIndex = {
    statusList: [
        'queue',
        'queue_checking',
        'parsing',
        'checking'
    ],
    delayTime: 1000,
    $btnWithoutReportOrderExporter: $('#btn_export_without_report'),
    $modalWithoutReportOrderExporter: $('#modal_export_without_report'),

    $btnDeliveryReportUploader: $('#btn_delivery_report_uploader'),
    $modalDeliveryReportUploader: $('#modal_delivery_report_uploader'),

    init: function () {
        setTimeout(DeliveryReportIndex.checkStatus, DeliveryReportIndex.delayTime);

        $('.export-without-report .choosen-delivery select[name="delivery_id"]').on('change', DeliveryReportIndex.ChangeParamExportWithoutReport);
        $('.export-without-report .choosen-period input[name="dateFrom"]').on('dp.change', DeliveryReportIndex.ChangeParamExportWithoutReport);
        $('.export-without-report .choosen-period input[name="dateTo"]').on('dp.change', DeliveryReportIndex.ChangeParamExportWithoutReport);

        DeliveryReportIndex.$btnWithoutReportOrderExporter.on('click', DeliveryReportIndex.initWithoutReportOrderExporter);
        DeliveryReportIndex.$modalWithoutReportOrderExporter.on('hide.bs.modal', DeliveryReportIndex.destroyWithoutReportOrderExporter);

        DeliveryReportIndex.$btnDeliveryReportUploader.on('click', DeliveryReportIndex.initDeliveryReportUploader);
        DeliveryReportIndex.$modalDeliveryReportUploader.on('hide.bs.modal', DeliveryReportIndex.destroyDeliveryReportUploader);

        $('.report-error-link').on('click', DeliveryReportIndex.initReportErrorViewer);
        $('.delivery-report-uploader-edit-btn').on('click', DeliveryReportIndex.initDeliveryReportUploaderEdit);
    },
    checkStatus: function () {
        var statuses = $("div[name='report_status']");
        var records = {};
        var size = 0;
        for (var i = 0; i < statuses.size(); i++) {
            if (DeliveryReportIndex.statusList.indexOf($(statuses[i]).data('report-status')) >= 0) {
                records[$(statuses[i]).data('report-id')] = $(statuses[i]).data('report-status');
                size++;
            }
        }

        if (size > 0) {
            $.ajax({
                method: "POST",
                url: location.href,
                dataType: 'json',
                data: {
                    data: records
                },
                success: function (response) {
                    if (typeof response.content != 'undefined') {
                        $('#reports_content').replaceWith(response.content);
                        DeliveryReportIndex.init();
                    }
                    else {
                        setTimeout(DeliveryReportIndex.checkStatus, DeliveryReportIndex.delayTime);
                    }
                },
                error: function (response) {
                    setTimeout(DeliveryReportIndex.checkStatus, DeliveryReportIndex.delayTime);
                }
            });
        }
    },
    ChangeParamExportWithoutReport: function () {
        var dateFrom = $('.export-without-report .choosen-period input[name="dateFrom"]').val();
        var dateTo = $('.export-without-report .choosen-period input[name="dateTo"]').val();
        var delivery = $('.export-without-report .choosen-delivery select[name="delivery_id"]').val();

        $.ajax({
            url: $('.export-without-report').data('action-url'),
            type: 'get',
            data: {
                date_from: dateFrom,
                date_to: dateTo,
                delivery: delivery
            },
            success: function (data) {
                $(".export-without-report .export-count").text(I18n['Число отсутствующих заказов согласно параметрам'] + ": " + data.count);
            }
        });
    },
    initWithoutReportOrderExporter: function () {
        WithoutReportOrderExporter.show();
        $delivery = $('.export-without-report .choosen-delivery select[name="delivery_id"]');
        $('.export-without-report .choosen-period input[name="dateFrom"]').clone().attr('type', 'hidden').appendTo(WithoutReportOrderExporter.$form);
        $('.export-without-report .choosen-period input[name="dateTo"]').clone().attr('type', 'hidden').appendTo(WithoutReportOrderExporter.$form);
        $delivery.clone().attr('type', 'hidden').val($delivery.val()).appendTo(WithoutReportOrderExporter.$form);
    },
    destroyWithoutReportOrderExporter: function () {
        DeliveryReportIndex.$btnWithoutReportOrderExporter.removeAttr('disabled');
        DeliveryReportIndex.$btnWithoutReportOrderExporter.removeAttr('data-loading');
        WithoutReportOrderExporter.stop();
    },
    initReportErrorViewer: function (e) {
        $modal = $('#modal_view_report_error');
        $text = $('#modal_view_report_error_text');
        $status = $('#modal_view_report_error_status');
        $status.text("");
        $text.text("");
        if (typeof $(this).data('error-message') != 'undefined' && $(this).data('error-message') != "") {
            $modal.find('.error-message').show();
            $modal.find('.common-error-message').hide();
            $text.text($(this).data('error-message'));
        } else {
            $modal.find('.common-error-message').show();
            $modal.find('.error-message').hide();
        }
        if (typeof $(this).data('error-status') != 'undefined' && $(this).data('error-status') != "") {
            $status.text($(this).data('error-status'));
            $modal.find('.last-correct-status').show();
        } else {
            $modal.find('.last-correct-status').hide();
        }
        $modal.modal();
        e.preventDefault();
    },
    initDeliveryReportUploader: function () {
        DeliveryReportUploader.show(true);
    },
    destroyDeliveryReportUploader: function () {
        if (DeliveryReportUploader.stop()) {
            DeliveryReportIndex.$btnDeliveryReportUploader.removeAttr('disabled');
            DeliveryReportIndex.$btnDeliveryReportUploader.removeAttr('data-loading');
        }
    },
    initDeliveryReportUploaderEdit: function (e) {
        e.preventDefault();
        DeliveryReportUploader.fillValues($(this).data());
        DeliveryReportUploader.show(false);
    }
};
