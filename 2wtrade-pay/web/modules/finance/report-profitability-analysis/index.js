$(function () {
    var $table = $('.finance-profitability-analysis-table');
    $table.find('.root-group')
        .each(function () {
            $table.addClass('close-group');
        })
        .click(function () {
            var $this = $(this);
            $this.toggleClass('close-group');
            $this.find('td:first .label:first').text(!$this.hasClass('close-group') ? '+' : '-');
            $this.find('td:first .label').toggleClass('label-default label-dark');
            $table.find('[data-group="' + $this.data('group') + '"]:not(.root-group)').toggleClass('hidden');
        });
});

$(function () {

    var $allCountryTable = $('#current-balance-all-country-table');
    var $allUpdateBtn = $allCountryTable.find('.btn-update-all');
    var updateQueue = [];
    var multiProcess = false;

    function resetButton($btn) {
        var $icon = $btn.find('i');
        if (!$btn.hasClass('btn-primary')) {
            $btn.removeClass('btn-danger btn-success btn-dark').addClass('btn-primary');
        }
        if (!$icon.hasClass('wb-refresh')) {
            $icon.removeClass('wb-check wb-close wb-arrow-down').addClass('wb-refresh');
        }
    }

    function run() {
        setTimeout(function () {
            if (!(multiProcess && updateQueue && updateQueue.length)) {
                $allUpdateBtn.toggleClass('btn-primary btn-danger');
                $allUpdateBtn.find('i').toggleClass('wb-close wb-refresh');
                multiProcess = false;

                for (var i in updateQueue) {
                    $btn = updateQueue[i];
                    resetButton($btn);
                }
                return;
            }
            var $btn = updateQueue.shift();
            var url = $btn.data('url');
            $.get(url, function (data) {
                if (data) {
                    $btn.closest('tr').find('[data-field]').each(function () {
                        var $td = $(this);
                        var field = $td.data('field');
                        var value = (data.hasOwnProperty(field) && data[field]) ? data[field] : '';
                        $td.text(value);
                    });
                    calcTotals();
                }
                $btn.find('i').toggleClass('wb-check wb-arrow-down');
                $btn.toggleClass('btn-success btn-dark');

                run();
            });
        }, 50);
    }

    function calcTotals() {
        $allCountryTable.find('tr:first th:not(:first,:last)').each(function () {
            var $td = $(this);
            var totals = [];
            $allCountryTable.find('tr:not(:first,.kv-page-summary)').each(function () {
                var f = $(this).children().eq($td.index());
                totals.push(asValue(f.text()));
            });
            var sum = totals.reduce(function (a, b) {
                return a + b;
            }, 0);
            var total = sum;
            if ($td.data('summary') == 'avg') {
                var count = 0;
                totals.forEach(function (x) {
                    if (x) {
                        count++;
                    }
                });
                if (count) {
                    total = sum / count;
                }
                else {
                    total = 0;
                }
            }
            $allCountryTable.find('tr.kv-page-summary').children().eq($td.index()).text(asRound(total));
        });
    }

    $allUpdateBtn.click(function () {
        if (!multiProcess) {
            var $needUpdate = $allCountryTable.find('.btn-update').not('.btn-success');
            updateQueue = [];
            if ($needUpdate.length) {
                $allUpdateBtn.toggleClass('btn-primary btn-danger');
                $allUpdateBtn.find('i').toggleClass('wb-close wb-refresh');
                $needUpdate.each(function () {
                    var $this = $(this);
                    updateQueue.push($this);
                    $this.toggleClass('btn-primary btn-dark');
                    $this.find('i').toggleClass('wb-refresh wb-arrow-down');
                });
                multiProcess = true;
                run();
            } else {
                $allCountryTable.find('.btn-update').each(function () {
                    resetButton($(this));
                });
                $allUpdateBtn.trigger('click');
            }
        } else {
            multiProcess = false;
        }
    });

    $allCountryTable.find('.btn-update').click(function () {
        if (multiProcess) {
            return;
        }
        var $btn = $(this);
        var url = $btn.data('url');
        resetButton($btn);
        $btn.find('i').removeClass('wb-refresh').addClass('wb-arrow-down');
        $btn.removeClass('btn-primary').addClass('btn-dark');
        $.get(url, function (data) {
            if (data) {
                $btn.closest('tr').find('[data-field]').each(function () {
                    var $td = $(this);
                    var field = $td.data('field');
                    var value = (data.hasOwnProperty(field) && data[field]) ? data[field] : '';
                    $td.text(value);
                });
                calcTotals();
            }
            $btn.find('i').removeClass('wb-arrow-down').addClass('wb-check');
            $btn.removeClass('btn-dark').addClass('btn-success');
        });
    });

    function asValue(val) {
        val = val.replace(/,/g, '.');
        val = val.replace(/ /g, '');
        val = val.replace(/—/g, '');
        val = val.replace(/&nbsp;/g, '');

        var comma = /\./g.test(val) ? val.match(/\./g).length : 0;
        if (comma > 1) val = val.replace('.', '');

        if (isNaN(val) || val == '') val = 0;
        val = parseFloat(val);
        return val;
    }

    function asRound(num) {
        return Number(num.toFixed(2)).toLocaleString();
    }
});