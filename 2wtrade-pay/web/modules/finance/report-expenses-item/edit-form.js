$(function() {

    var $form = $('#edit-form');
    var $type = $form.find('[name*=type]');
    var $value = $form.find('[name*=value]');
    var $formula = $form.find('[name*=formula]');
    $type.change(function () {
        var regular = ($(this).val() == 'regular');
        $value.prop('disabled', !regular);
        $formula.prop('disabled', regular);
        if ($formula.data('widget') == 'ckeditor') {
            var id = $formula.prop('id');
            if (CKEDITOR.instances.hasOwnProperty(id)  ) {
                CKEDITOR.instances[id].setReadOnly(regular);
            }
        }
    });
    $type.trigger('change');
});