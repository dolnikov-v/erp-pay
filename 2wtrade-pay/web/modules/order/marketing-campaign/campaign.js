$(function () {
    MarketingCampaignEdit.init();
    MarketingCampaignList.init();
    MarketingCampaignView.init();
});

var MarketingCampaignEdit = {
    init: function () {
        MarketingCampaignEdit.toggleLimitType();
        $(document).on('change', '#marketingcampaign-limit_type', MarketingCampaignEdit.toggleLimitType);
    },
    toggleLimitType: function () {
        var v = $('#marketingcampaign-limit_type').val();
        $(".limit").hide();
        $(".limit_"+v).show();
    }
};


var MarketingCampaignList = {
    init: function () {
        $(document).on('click', '.confirm-delete-campaign', MarketingCampaignList.deleteList);
    },
    deleteList: function () {
        $('#modal_confirm_delete_campaign').modal();
        $('#modal_confirm_delete_campaign_link').attr('href', $(this).data('href'));
        $('#modal_confirm_delete_campaign_span').text($(this).data('text'));
        return false;
    }
};

var MarketingCampaignView = {
    init: function () {
        $(document).on('click', '.confirm-delete-coupon', MarketingCampaignView.deleteList);
    },
    deleteList: function () {
        $('#modal_confirm_delete_coupon').modal();
        $('#modal_confirm_delete_coupon_link').attr('href', $(this).data('href'));
        $('#modal_confirm_delete_coupon_span').text($(this).data('text'));
        return false;
    }
};