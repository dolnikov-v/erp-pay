var url = null;
var orderListCols = [];

$(function () {
    $('#col-select').on('show.bs.modal', function (e) {
        url = $(e.relatedTarget).data('list-url');
    });

    $('#go-gen-list').on('click', function (e) {
        $('#col-select').modal('hide');
        orderListCols = [];

        $('#col-select input:checked').each(function (i, e) {
            orderListCols.push($(e).val());
        });

        window.location = url + '?fields=' + JSON.stringify(orderListCols);
    });

    ListDetails.init();
    List.init();
});

var ListDetails = {
    init: function () {
        $(document).on('click', '.confirm-delete-order', ListDetails.deleteOrder);
    },
    deleteOrder: function () {
        $('#modal_confirm_delete_order').modal();
        $('#modal_confirm_delete_order_link').attr('href', $(this).data('href'));
        return false;
    }
};

var List = {
    init: function () {
        $(document).on('click', '.confirm-delete-list', List.deleteList);
    },

    deleteList: function () {
        $('#modal_confirm_delete_list').modal();
        $('#modal_confirm_delete_list_link').attr('href', $(this).data('href'));
        $('#modal_confirm_delete_list_span').text($(this).data('text'));
        return false;
    }
};
