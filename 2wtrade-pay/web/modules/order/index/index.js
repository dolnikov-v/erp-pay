$(function () {
    if ($('#orders_content').length) {
        OrderIndexIndex.init();
    }
});

var OrderIndexIndex = {
    $btnChangerStatus: $('#btn_changer_status'),
    $modalChangerStatus: $('#modal_changer_status'),

    $btnSendCheckOrder: $('#btn_send_check_order'),
    $modalSendCheckOrder: $('#modal_send_check_order'),

    $btnSendCheckUndeliveryOrder: $('#btn_send_check_undelivery_order'),
    $modalSendCheckUndeliveryOrder: $('#modal_send_check_undelivery_order'),

    $btnChangerDelivery: $('#btn_changer_delivery'),
    $modalChangerDelivery: $('#modal_changer_delivery'),

    $btnResendSeparateOrder: $('#btn_resend_separate_order'),
    $modalResendSeparateOrder: $('#modal_resend_separate_order'),

    $btnResendSeparateOrderIntoDelivery: $('#btn_resend_separate_order_into_delivery'),
    $modalResendSeparateOrderIntoDelivery: $('#modal_resend_separate_order_into_delivery'),

    $btnSendInCallCenter: $('#btn_send_in_call_center'),
    $modalSendInCallCenter: $('#modal_send_in_call_center'),

    $btnResendInCallCenter: $('#btn_resend_in_call_center'),
    $modalResendInCallCenter: $('#modal_resend_in_call_center'),

    $btnResendInCallCenterByExcel: $('#btn_resend_in_call_center_by_excel'),
    $modalResendInCallCenterByExcel: $('#modal_resend_in_call_center_by_excel'),

    $btnResendInCallCenterNewQueue: $('#btn_resend_in_call_center_new_queue'),
    $modalResendInCallCenterNewQueue: $('#modal_resend_in_call_center_new_queue'),

    $btnResendInCallCenterStillWaiting: $('#btn_resend_in_call_center_still_waiting'),
    $modalResendInCallCenterStillWaiting: $('#modal_resend_in_call_center_still_waiting'),

    $btnResendInCallCenterReturnNoProd: $('#btn_resend_in_call_center_return_no_prod'),
    $modalResendInCallCenterReturnNoProd: $('#modal_resend_in_call_center_return_no_prod'),

    $btnSendInQueue: $('#btn_send_in_queue'),
    $modalSendInQueue: $('#modal_send_in_queue'),


    $btnGenerateList: $('#btn_generate_list'),
    $modalGenerateList: $('#modal_generate_list'),

    $btnGeneratePretensionList: $('#btn_generate_pretension_list'),
    $modalGeneratePretensionList: $('#modal_generate_pretension_list'),

    $btnDeleteOrdersFromLists: $('#btn_delete_orders_from_lists'),
    $modalDeleteOrdersFromLists: $('#modal_delete_orders_from_lists'),

    $btnGenerateMarketingList: $('#btn_generate_marketinglist'),
    $modalGenerateMarketingList: $('#modal_generate_marketing_list'),

    $btnSenderInDelivery: $('#btn_sender_in_delivery'),
    $modalSenderInDelivery: $('#modal_sender_in_delivery'),

    $btnResenderInDelivery: $('#btn_resender_in_delivery'),
    $modalResenderInDelivery: $('#modal_resender_in_delivery'),

    $btnClarificationInDelivery: $('#btn_clarification_in_delivery'),
    $modalClarificationInDelivery: $('#modal_clarification_in_delivery'),

    $btnSecondDeliveryAttempt: $('#btn_second_delivery_attempt'),
    $modalSecondDeliveryAttempt: $('#modal_second_delivery_attempt'),

    $btnRetryGetStatusFromCallCenter: $('#btn_retry_get_status_from_call_center'),
    $modalRetryGetStatusFromCallCenter: $('#modal_retry_get_status_from_call_center'),

    $btnCallCenterRequestStatusChanger: $('#btn_call_center_request_status_changer'),
    $modalCallCenterRequestStatusChanger: $('#modal_call_center_request_status_changer'),

    $btnDeliveryRequestStatusChanger: $('#btn_delivery_request_status_changer'),
    $modalDeliveryRequestStatusChanger: $('#modal_delivery_request_status_changer'),

    $btnSendSmsPoll: $('#btn_send_sms_poll'),
    $modalSendSmsPoll: $('#modal_send_sms_poll'),

    $btnSendAnalysisTrash: $('#btn_send_analysis_trash'),
    $modalSendAnalysisTrash: $('#modal_send_analysis_trash'),

    $btnChangeStatusByLimitedUser: $('#btn_change_status_by_limited_user'),
    $modalChangeStatusByLimitedUser: $('#modal_change_status_by_limited_user'),

    $btnSendCheckAddress: $('#btn_send_check_address'),
    $modalSendCheckAddress: $('#modal_send_check_address'),

    $btnTransferToDistributor: $('#btn_transfer_to_distributor'),
    $modalTransferToDistributor: $('#modal_transfer_to_distributor'),

    $btnSendPostSell: $('#btn_send_post_sale'),
    $modalSendPostSell: $('#modal_send_post_sale'),

    $btnSendInformation: $('#btn_send_information'),
    $modalSendInformation: $('#modal_send_information'),

    init: function () {
        $(document).on('change', '#orders_content .th-custom-checkbox input[type="checkbox"]', OrderIndexIndex.toggleCheckboxes);
        $('#orders_content .td-custom-checkbox input[type="checkbox"]').on('change', OrderIndexIndex.toggleCheckbox);

        OrderIndexIndex.$btnChangerDelivery.on('click', OrderIndexIndex.initChangerDelivery);
        OrderIndexIndex.$modalChangerDelivery.on('hide.bs.modal', OrderIndexIndex.destroyChangerDelivery);

        OrderIndexIndex.$btnResendSeparateOrder.on('click', OrderIndexIndex.initResendSeparateOrder);
        OrderIndexIndex.$modalResendSeparateOrder.on('hide.bs.modal', OrderIndexIndex.destroyResendSeparateOrder);

        OrderIndexIndex.$btnResendSeparateOrderIntoDelivery.on('click', OrderIndexIndex.initResendSeparateOrderIntoDelivery);
        OrderIndexIndex.$modalResendSeparateOrderIntoDelivery.on('hide.bs.modal', OrderIndexIndex.destroyResendSeparateOrderIntoDelivery);

        OrderIndexIndex.$btnChangerStatus.on('click', OrderIndexIndex.initChangerStatus);
        OrderIndexIndex.$modalChangerStatus.on('hide.bs.modal', OrderIndexIndex.destroyChangerStatus);

        OrderIndexIndex.$btnSendInCallCenter.on('click', OrderIndexIndex.initSenderInCallCenter);
        OrderIndexIndex.$modalSendInCallCenter.on('hide.bs.modal', OrderIndexIndex.destroySenderInCallCenter);

        OrderIndexIndex.$btnResendInCallCenter.on('click', OrderIndexIndex.initResenderInCallCenter);
        OrderIndexIndex.$modalResendInCallCenter.on('hide.bs.modal', OrderIndexIndex.destroyResenderInCallCenter);

        OrderIndexIndex.$btnResendInCallCenterByExcel.on('click', OrderIndexIndex.initResenderInCallCenterByExcel);
        OrderIndexIndex.$modalResendInCallCenterByExcel.on('hide.bs.modal', OrderIndexIndex.destroyResenderInCallCenterByExcel);

        OrderIndexIndex.$btnResendInCallCenterNewQueue.on('click', OrderIndexIndex.initResenderInCallCenterNewQueue);
        OrderIndexIndex.$modalResendInCallCenterNewQueue.on('hide.bs.modal', OrderIndexIndex.destroyResenderInCallCenterNewQueue);

        OrderIndexIndex.$btnResendInCallCenterStillWaiting.on('click', OrderIndexIndex.initResenderInCallCenterStillWaiting);
        //OrderIndexIndex.$btnResendInCallCenterStillWaiting.on('click', $('button .close').trigger());
        OrderIndexIndex.$modalResendInCallCenterStillWaiting.on('hide.bs.modal', OrderIndexIndex.destroyResenderInCallCenterStillWaiting);

        OrderIndexIndex.$btnResendInCallCenterReturnNoProd.on('click', OrderIndexIndex.initResenderInCallCenterReturnNoProd);
        OrderIndexIndex.$modalResendInCallCenterReturnNoProd.on('hide.bs.modal', OrderIndexIndex.destroyResenderInCallCenterReturnNoProd);

        OrderIndexIndex.$btnSendInQueue.on('click', OrderIndexIndex.initSendInQueue);
        OrderIndexIndex.$modalSendInQueue.on('hide.bs.modal', OrderIndexIndex.destroySendInQueue);

        OrderIndexIndex.$btnGenerateList.on('click', OrderIndexIndex.initGeneratorList);
        OrderIndexIndex.$modalGenerateList.on('hide.bs.modal', OrderIndexIndex.destroyGeneratorList);

        OrderIndexIndex.$btnGeneratePretensionList.on('click', OrderIndexIndex.initGeneratorPretensionList);
        OrderIndexIndex.$modalGeneratePretensionList.on('hide.bs.modal', OrderIndexIndex.destroyGeneratorPretensionList);

        OrderIndexIndex.$btnDeleteOrdersFromLists.on('click', OrderIndexIndex.initDeleteOrdersFromLists);
        OrderIndexIndex.$modalDeleteOrdersFromLists.on('hide.bs.modal', OrderIndexIndex.destroyDeleteOrdersFromLists);

        OrderIndexIndex.$btnGenerateMarketingList.on('click', OrderIndexIndex.initGeneratorMarketingList);
        OrderIndexIndex.$modalGenerateMarketingList.on('hide.bs.modal', OrderIndexIndex.destroyGeneratorMarketingList);

        OrderIndexIndex.$btnSenderInDelivery.on('click', OrderIndexIndex.initSenderInDelivery);
        OrderIndexIndex.$modalSenderInDelivery.on('hide.bs.modal', OrderIndexIndex.destroySenderInDelivery);

        OrderIndexIndex.$btnResenderInDelivery.on('click', OrderIndexIndex.initResenderInDelivery);
        OrderIndexIndex.$modalResenderInDelivery.on('hide.bs.modal', OrderIndexIndex.destroyResenderInDelivery);

        OrderIndexIndex.$btnClarificationInDelivery.on('click', OrderIndexIndex.initClarificationInDelivery);
        OrderIndexIndex.$modalClarificationInDelivery.on('hide.bs.modal', OrderIndexIndex.destroyClarificationInDelivery);

        OrderIndexIndex.$btnSecondDeliveryAttempt.on('click', OrderIndexIndex.initSecondDeliveryAttempt);
        OrderIndexIndex.$modalSecondDeliveryAttempt.on('hide.bs.modal', OrderIndexIndex.destroySecondDeliveryAttempt);

        OrderIndexIndex.$btnSendCheckOrder.on('click', OrderIndexIndex.initSendCheckOrder);
        OrderIndexIndex.$modalSendCheckOrder.on('hide.bs.modal', OrderIndexIndex.destroySendCheckOrder);

        OrderIndexIndex.$btnSendCheckUndeliveryOrder.on('click', OrderIndexIndex.initSendCheckUndeliveryOrder);
        OrderIndexIndex.$modalSendCheckUndeliveryOrder.on('hide.bs.modal', OrderIndexIndex.destroySendCheckUndeliveryOrder);

        OrderIndexIndex.$btnRetryGetStatusFromCallCenter.on('click', OrderIndexIndex.initRetryGetStatusFromCallCenter);
        OrderIndexIndex.$modalRetryGetStatusFromCallCenter.on('hide.bs.modal', OrderIndexIndex.destroyRetryGetStatusFromCallCenter);

        OrderIndexIndex.$btnCallCenterRequestStatusChanger.on('click', OrderIndexIndex.initCallCenterRequestStatusChanger);
        OrderIndexIndex.$modalCallCenterRequestStatusChanger.on('hide.bs.modal', OrderIndexIndex.destroyCallCenterRequestStatusChanger);

        OrderIndexIndex.$btnDeliveryRequestStatusChanger.on('click', OrderIndexIndex.initDeliveryRequestStatusChanger);
        OrderIndexIndex.$modalDeliveryRequestStatusChanger.on('hide.bs.modal', OrderIndexIndex.destroyDeliveryRequestStatusChanger);

        OrderIndexIndex.$btnSendSmsPoll.on('click', OrderIndexIndex.initSendSmsPoll);
        OrderIndexIndex.$modalSendSmsPoll.on('hide.bs.modal', OrderIndexIndex.destroySendSmsPoll);

        OrderIndexIndex.$btnSendAnalysisTrash.on('click', OrderIndexIndex.initSendAnalysisTrash);
        OrderIndexIndex.$modalSendAnalysisTrash.on('hide.bs.modal', OrderIndexIndex.destroySendAnalysisTrash);

        OrderIndexIndex.$btnChangeStatusByLimitedUser.on('click', OrderIndexIndex.initChangeStatusByLimitedUser);
        OrderIndexIndex.$modalChangeStatusByLimitedUser.on('hide.bs.modal', OrderIndexIndex.destroyChangeStatusByLimitedUser);

        OrderIndexIndex.$btnSendCheckAddress.on('click', OrderIndexIndex.initSendCheckAddress);
        OrderIndexIndex.$modalSendCheckAddress.on('hide.bs.modal', OrderIndexIndex.destroySendCheckAddress);

        OrderIndexIndex.$btnTransferToDistributor.on('click', OrderIndexIndex.initTransferToDistributor);
        OrderIndexIndex.$modalTransferToDistributor.on('hide.bs.modal', OrderIndexIndex.destroyTransferToDistributor);

        OrderIndexIndex.$btnSendPostSell.on('click', OrderIndexIndex.initSendPostSell);
        OrderIndexIndex.$modalSendPostSell.on('hide.bs.modal', OrderIndexIndex.destroySendPostSell);

        OrderIndexIndex.$btnSendInformation.on('click', OrderIndexIndex.initSendInformation);
        OrderIndexIndex.$modalSendInformation.on('hide.bs.modal', OrderIndexIndex.destroySendInformation);

    },

    toggleCheckboxes: function () {
        $('#orders_content .td-custom-checkbox input[type="checkbox"]').prop('checked', $(this).prop('checked')).trigger('change');
    },

    toggleCheckbox: function () {
        var $tr = $(this).closest('tr');

        if ($(this).prop('checked')) {
            $tr.addClass('active');
        } else {
            $tr.removeClass('active');
        }
    },

    // Доставка
    initChangerDelivery: function () {
        OrderIndexIndex.$modalChangerDelivery.modal();
        OrderIndexIndex.completeForm(OrderIndexIndex.$modalChangerDelivery.find('form'));
    },
    destroyChangerDelivery: function () {
        OrderIndexIndex.$btnChangerDelivery.removeAttr('disabled');
        OrderIndexIndex.$btnChangerDelivery.removeAttr('data-loading');
        ChangerDelivery.stop();
    },

    // Переотправить отдельным заказом
    initResendSeparateOrder: function () {
        OrderIndexIndex.$modalResendSeparateOrder.modal();
        OrderIndexIndex.completeForm(OrderIndexIndex.$modalResendSeparateOrder.find('form'));
    },
    destroyResendSeparateOrder: function () {
        OrderIndexIndex.$btnResendSeparateOrder.removeAttr('disabled');
        OrderIndexIndex.$btnResendSeparateOrder.removeAttr('data-loading');
        ResenderSeparateOrder.stop();
    },

    // Переотправить отдельным заказом в КС
    initResendSeparateOrderIntoDelivery: function () {
        OrderIndexIndex.$modalResendSeparateOrderIntoDelivery.modal();
        OrderIndexIndex.completeForm(OrderIndexIndex.$modalResendSeparateOrderIntoDelivery.find('form'));
    },
    destroyResendSeparateOrderIntoDelivery: function () {
        OrderIndexIndex.$btnResendSeparateOrderIntoDelivery.removeAttr('disabled');
        OrderIndexIndex.$btnResendSeparateOrderIntoDelivery.removeAttr('data-loading');
        ResenderSeparateOrderIntoDelivery.stop();
    },

    // Статус
    initChangerStatus: function () {
        OrderIndexIndex.$modalChangerStatus.modal();
        OrderIndexIndex.completeForm(OrderIndexIndex.$modalChangerStatus.find('form'));
    },
    destroyChangerStatus: function () {
        OrderIndexIndex.$btnChangerStatus.removeAttr('disabled');
        OrderIndexIndex.$btnChangerStatus.removeAttr('data-loading');
        ChangerStatus.stop();
    },

    // Статус заявки в КЦ
    initCallCenterRequestStatusChanger: function () {
        OrderIndexIndex.$modalCallCenterRequestStatusChanger.modal();
        OrderIndexIndex.completeForm(OrderIndexIndex.$modalCallCenterRequestStatusChanger.find('form'));
    },
    destroyCallCenterRequestStatusChanger: function () {
        OrderIndexIndex.$btnCallCenterRequestStatusChanger.removeAttr('disabled');
        OrderIndexIndex.$btnCallCenterRequestStatusChanger.removeAttr('data-loading');
        CallCenterRequestStatusChanger.stop();
    },

    // Статус заявки в КС
    initDeliveryRequestStatusChanger: function () {
        OrderIndexIndex.$modalDeliveryRequestStatusChanger.modal();
        OrderIndexIndex.completeForm(OrderIndexIndex.$modalDeliveryRequestStatusChanger.find('form'));
    },
    destroyDeliveryRequestStatusChanger: function () {
        OrderIndexIndex.$btnDeliveryRequestStatusChanger.removeAttr('disabled');
        OrderIndexIndex.$btnDeliveryRequestStatusChanger.removeAttr('data-loading');
        DeliveryRequestStatusChanger.stop();
    },

    //СМС опрос
    initSendSmsPoll: function () {
        OrderIndexIndex.$modalSendSmsPoll.modal();
        OrderIndexIndex.completeForm(OrderIndexIndex.$modalSendSmsPoll.find('form'));
    },
    destroySendSmsPoll: function () {
        OrderIndexIndex.$btnSendSmsPoll.removeAttr('disabled');
        OrderIndexIndex.$btnSendSmsPoll.removeAttr('data-loading');
        SendSmsPoll.stop();
    },

    // Колл-центр
    initSenderInCallCenter: function () {
        OrderIndexIndex.$modalSendInCallCenter.modal();
        OrderIndexIndex.completeForm(OrderIndexIndex.$modalSendInCallCenter.find('form'));
    },
    initResenderInCallCenter: function () {
        OrderIndexIndex.$modalResendInCallCenter.modal();
        OrderIndexIndex.completeForm(OrderIndexIndex.$modalResendInCallCenter.find('form'));
    },
    initResenderInCallCenterByExcel: function () {
        OrderIndexIndex.$modalResendInCallCenterByExcel.modal();
        OrderIndexIndex.completeForm(OrderIndexIndex.$modalResendInCallCenterByExcel.find('form'));
    },
    initResenderInCallCenterNewQueue: function () {
        OrderIndexIndex.$modalResendInCallCenterNewQueue.modal();
        OrderIndexIndex.completeForm(OrderIndexIndex.$modalResendInCallCenterNewQueue.find('form'));
        OrderIndexIndex.$modalSendInQueue.modal('hide');
    },
    initResenderInCallCenterStillWaiting: function () {
        OrderIndexIndex.$modalResendInCallCenterStillWaiting.modal();
        OrderIndexIndex.completeForm(OrderIndexIndex.$modalResendInCallCenterStillWaiting.find('form'));
        OrderIndexIndex.$modalSendInQueue.modal('hide');
    },

    initResenderInCallCenterReturnNoProd: function () {
        OrderIndexIndex.$modalResendInCallCenterReturnNoProd.modal();
        OrderIndexIndex.completeForm(OrderIndexIndex.$modalResendInCallCenterReturnNoProd.find('form'));
        OrderIndexIndex.$modalSendInQueue.modal('hide');
    },

    initSendInQueue: function () {
        OrderIndexIndex.$modalSendInQueue.modal();
    },
    destroySendInQueue: function () {
        OrderIndexIndex.$btnSendInQueue.removeAttr('disabled');
        OrderIndexIndex.$btnSendInQueue.removeAttr('data-loading');
    },

    initSendCheckOrder: function () {
        OrderIndexIndex.$modalSendCheckOrder.modal();
        OrderIndexIndex.completeForm(OrderIndexIndex.$modalSendCheckOrder.find('form'));
        OrderIndexIndex.$modalSendInQueue.modal('hide');
    },
    initSendCheckUndeliveryOrder: function () {
        OrderIndexIndex.$modalSendCheckUndeliveryOrder.modal();
        OrderIndexIndex.completeForm(OrderIndexIndex.$modalSendCheckUndeliveryOrder.find('form'));
        OrderIndexIndex.$modalSendInQueue.modal('hide');
    },
    destroySenderInCallCenter: function () {
        OrderIndexIndex.$btnSendInCallCenter.removeAttr('disabled');
        OrderIndexIndex.$btnSendInCallCenter.removeAttr('data-loading');
        SenderInCallCenter.stop();
    },
    destroyResenderInCallCenter: function () {
        OrderIndexIndex.$btnResendInCallCenter.removeAttr('disabled');
        OrderIndexIndex.$btnResendInCallCenter.removeAttr('data-loading');
        ResenderInCallCenterNewQueue.stop();
    },
    destroyResenderInCallCenterByExcel: function () {
        OrderIndexIndex.$btnResendInCallCenterByExcel.removeAttr('disabled');
        OrderIndexIndex.$btnResendInCallCenterByExcel.removeAttr('data-loading');
        ResenderInCallCenterByExcel.stop();
    },
    destroyResenderInCallCenterNewQueue: function () {
        OrderIndexIndex.$btnResendInCallCenterNewQueue.removeAttr('disabled');
        OrderIndexIndex.$btnResendInCallCenterNewQueue.removeAttr('data-loading');
        ResenderInCallCenterNewQueue.stop();
    },
    destroyResenderInCallCenterStillWaiting: function () {
        OrderIndexIndex.$btnResendInCallCenterStillWaiting.removeAttr('disabled');
        OrderIndexIndex.$btnResendInCallCenterStillWaiting.removeAttr('data-loading');
        ResenderInCallCenterStillWaiting.stop();
        OrderIndexIndex.destroySendInQueue();
    },

    destroyResenderInCallCenterReturnNoProd: function () {
        OrderIndexIndex.$btnResendInCallCenterReturnNoProd.removeAttr('disabled');
        OrderIndexIndex.$btnResendInCallCenterReturnNoProd.removeAttr('data-loading');
        ResenderInCallCenterReturnNoProd.stop();
    },

    destroySendCheckOrder: function () {
        OrderIndexIndex.$btnSendCheckOrder.removeAttr('disabled');
        OrderIndexIndex.$btnSendCheckOrder.removeAttr('data-loading');
        SendCheckOrder.stop();
    },
    destroySendCheckUndeliveryOrder: function () {
        OrderIndexIndex.$btnSendCheckUndeliveryOrder.removeAttr('disabled');
        OrderIndexIndex.$btnSendCheckUndeliveryOrder.removeAttr('data-loading');
        SendCheckUndeliveryOrder.stop();
    },

    // Курьерка
    initSenderInDelivery: function () {
        OrderIndexIndex.$modalSenderInDelivery.modal();
        OrderIndexIndex.completeForm(OrderIndexIndex.$modalSenderInDelivery.find('form'));
    },
    destroySenderInDelivery: function () {
        OrderIndexIndex.$btnSenderInDelivery.removeAttr('disabled');
        OrderIndexIndex.$btnSenderInDelivery.removeAttr('data-loading');
        GeneratorList.stop();
    },

    initResenderInDelivery: function () {
        OrderIndexIndex.$modalResenderInDelivery.modal();
        OrderIndexIndex.completeForm(OrderIndexIndex.$modalResenderInDelivery.find('form'));
    },
    destroyResenderInDelivery: function () {
        OrderIndexIndex.$btnResenderInDelivery.removeAttr('disabled');
        OrderIndexIndex.$btnResenderInDelivery.removeAttr('data-loading');
        GeneratorList.stop();
    },

    initClarificationInDelivery: function () {
        OrderIndexIndex.$modalClarificationInDelivery.modal();
        OrderIndexIndex.completeClarificationForm(OrderIndexIndex.$modalClarificationInDelivery.find('form'));
    },
    destroyClarificationInDelivery: function () {
        OrderIndexIndex.$btnClarificationInDelivery.removeAttr('disabled');
        OrderIndexIndex.$btnClarificationInDelivery.removeAttr('data-loading');
        GeneratorList.stop();
    },

    initSecondDeliveryAttempt: function () {
        OrderIndexIndex.$modalSecondDeliveryAttempt.modal();
        OrderIndexIndex.completeForm(OrderIndexIndex.$modalSecondDeliveryAttempt.find('form'));
    },
    destroySecondDeliveryAttempt: function () {
        OrderIndexIndex.$btnSecondDeliveryAttempt.removeAttr('disabled');
        OrderIndexIndex.$btnSecondDeliveryAttempt.removeAttr('data-loading');
        GeneratorList.stop();
    },

    initSendAnalysisTrash: function () {
        OrderIndexIndex.$modalSendAnalysisTrash.modal();
        OrderIndexIndex.completeForm(OrderIndexIndex.$modalSendAnalysisTrash.find('form'));
        OrderIndexIndex.$modalSendInQueue.modal('hide');
    },

    destroySendAnalysisTrash: function () {
        OrderIndexIndex.$btnSendAnalysisTrash.removeAttr('disabled');
        OrderIndexIndex.$btnSendAnalysisTrash.removeAttr('data-loading');
        GeneratorList.stop();
    },

    // Логистика
    initGeneratorList: function () {
        OrderIndexIndex.$modalGenerateList.modal();
        OrderIndexIndex.completeForm(OrderIndexIndex.$modalGenerateList.find('form'));
    },
    destroyGeneratorList: function () {
        OrderIndexIndex.$btnGenerateList.removeAttr('disabled');
        OrderIndexIndex.$btnGenerateList.removeAttr('data-loading');
        GeneratorList.stop();
    },
    initGeneratorPretensionList: function () {
        OrderIndexIndex.$modalGeneratePretensionList.modal();
        OrderIndexIndex.completeForm(OrderIndexIndex.$modalGeneratePretensionList.find('form'));
    },
    destroyGeneratorPretensionList: function () {
        OrderIndexIndex.$btnGeneratePretensionList.removeAttr('disabled');
        OrderIndexIndex.$btnGeneratePretensionList.removeAttr('data-loading');
        GeneratorPretensionList.stop();
    },
    initDeleteOrdersFromLists: function () {
        OrderIndexIndex.$modalDeleteOrdersFromLists.modal();
        OrderIndexIndex.completeForm(OrderIndexIndex.$modalDeleteOrdersFromLists.find('form'));
    },
    destroyDeleteOrdersFromLists: function () {
        OrderIndexIndex.$btnDeleteOrdersFromLists.removeAttr('disabled');
        OrderIndexIndex.$btnDeleteOrdersFromLists.removeAttr('data-loading');
        DeleteOrdersFromLists.stop();
    },

    initGeneratorMarketingList: function () {
        OrderIndexIndex.$modalGenerateMarketingList.modal();
        OrderIndexIndex.completeForm(OrderIndexIndex.$modalGenerateMarketingList.find('form'));
    },
    destroyGeneratorMarketingList: function () {
        OrderIndexIndex.$btnGenerateMarketingList.removeAttr('disabled');
        OrderIndexIndex.$btnGenerateMarketingList.removeAttr('data-loading');
        GeneratorMarketingList.stop();
    },

    initRetryGetStatusFromCallCenter: function () {
        OrderIndexIndex.$modalRetryGetStatusFromCallCenter.modal();
        OrderIndexIndex.completeForm(OrderIndexIndex.$modalRetryGetStatusFromCallCenter.find('form'));
    },
    destroyRetryGetStatusFromCallCenter: function () {
        OrderIndexIndex.$btnRetryGetStatusFromCallCenter.removeAttr('disabled');
        OrderIndexIndex.$btnRetryGetStatusFromCallCenter.removeAttr('data-loading');
        RetryGetStatusFromCallCenter.stop();
    },

    initChangeStatusByLimitedUser: function () {
        OrderIndexIndex.$modalChangeStatusByLimitedUser.modal();
        OrderIndexIndex.completeForm(OrderIndexIndex.$modalChangeStatusByLimitedUser.find('form'));
    },
    destroyChangeStatusByLimitedUser: function () {
        OrderIndexIndex.$btnChangeStatusByLimitedUser.removeAttr('disabled');
        OrderIndexIndex.$btnChangeStatusByLimitedUser.removeAttr('data-loading');
        ChangeStatusByLimitedUser.stop();
    },

    initSendCheckAddress: function () {
        OrderIndexIndex.$modalSendCheckAddress.modal();
        OrderIndexIndex.completeForm(OrderIndexIndex.$modalSendCheckAddress.find('form'));
        OrderIndexIndex.$modalSendInQueue.modal('hide');
    },
    destroySendCheckAddress: function () {
        OrderIndexIndex.$btnSendCheckAddress.removeAttr('disabled');
        OrderIndexIndex.$btnSendCheckAddress.removeAttr('data-loading');
        GeneratorList.stop();
    },
    //Передать дистрибьютору
    initTransferToDistributor: function () {
        OrderIndexIndex.$modalTransferToDistributor.modal();
        OrderIndexIndex.completeForm(OrderIndexIndex.$modalTransferToDistributor.find('form'));
    },
    destroyTransferToDistributor: function () {
        OrderIndexIndex.$btnTransferToDistributor.removeAttr('disabled');
        OrderIndexIndex.$btnTransferToDistributor.removeAttr('data-loading');
    },

    initSendPostSell: function () {
        OrderIndexIndex.$modalSendPostSell.modal();
        OrderIndexIndex.completeForm(OrderIndexIndex.$modalSendPostSell.find('form'));
        OrderIndexIndex.$modalSendInQueue.modal('hide');
    },
    destroySendPostSell: function () {
        OrderIndexIndex.$btnSendPostSell.removeAttr('disabled');
        OrderIndexIndex.$btnSendPostSell.removeAttr('data-loading');
    },

    initSendInformation: function () {
        OrderIndexIndex.$modalSendInformation.modal();
        OrderIndexIndex.completeForm(OrderIndexIndex.$modalSendInformation.find('form'));
        OrderIndexIndex.$modalSendInQueue.modal('hide');
    },
    destroySendInformation: function () {
        OrderIndexIndex.$btnSendInformation.removeAttr('disabled');
        OrderIndexIndex.$btnSendInformation.removeAttr('data-loading');
    },
    
    completeForm: function ($form) {
        $form.find('input[name="id[]"]').remove();

        $('#orders_content input[name="id[]"]:checked').each(function () {
            // Переназначаем value для клонированного элемента, иначе IE не видит val()
            $(this).clone().attr('type', 'hidden').val($(this).val()).appendTo($form);
        });
    },
    completeClarificationForm: function ($form) {
        $form.find('.row-with-btn-start').hide();
        $form.find('input[name="id[]"]').remove();
        var order_ids = [];
        $('#orders_content input[name="id[]"]:checked').each(function () {
            $(this).clone().attr('type', 'hidden').val($(this).val()).appendTo($form);
            order_ids.push($(this).val());
        });
        var holder = $form.find('.emails_holder');
        holder.html('');
        $.ajax({
            type: 'POST',
            url: $form.data('delivery-url'),
            dataType: 'json',
            data: {
                id: order_ids.join()
            },
            success: function (response, textStatus) {
                if (response.status == 'success') {
                    response.result.forEach(function (delivery) {
                        holder.append('<div class="h5">' + delivery.name + '</div>');
                        if (delivery.contacts.length) {
                            delivery.contacts.forEach(function (email) {
                                holder.append('<div class="checkbox-custom checkbox-primary"><input class="deliveryemails" checked="checked" type="checkbox" value="' + email.id + '" id="email_' + email.id + '"><label for="email_' + email.id + '">' + email.email + '</label></div>')
                            });
                        }
                        else {
                            holder.append('<div> - </div>');
                        }
                    });

                    $form.find('.row-with-btn-start').show();
                }
                else {
                    $.notify({message: response.message}, {
                        type: "danger dark",
                        animate: {exit: 'hide'},
                        z_index: 2031
                    });
                }
            }
        });
    }
};
