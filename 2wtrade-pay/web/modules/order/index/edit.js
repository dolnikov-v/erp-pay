$(function () {
    if ($('#order_products_content').length) {
        OrderIndexEdit.init();
    }
});

var OrderIndexEdit = {

    $btnSenderInDelivery: $('#btn_sender_in_delivery'),
    $modalSenderInDelivery: $('#modal_sender_in_delivery'),

    $btnResenderInDelivery: $('#btn_resender_in_delivery'),
    $modalResenderInDelivery: $('#modal_resender_in_delivery'),

    init: function () {
        $(document).on('click', '.btn-order-product-plus', OrderIndexEdit.plusLineProduct);
        $(document).on('click', '.btn-order-product-minus', OrderIndexEdit.minusLineProduct);

        OrderIndexEdit.$btnSenderInDelivery.on('click', OrderIndexEdit.initSenderInDelivery);
        OrderIndexEdit.$modalSenderInDelivery.on('hide.bs.modal', OrderIndexEdit.destroySenderInDelivery);

        OrderIndexEdit.$btnResenderInDelivery.on('click', OrderIndexEdit.initResenderInDelivery);
        OrderIndexEdit.$modalResenderInDelivery.on('hide.bs.modal', OrderIndexEdit.destroyResenderInDelivery);
    },

    plusLineProduct: function () {
        var html = $('#order_products_inputs').html();
        $('#order_products_content').append(html);

        $('#order_products_content .row-order-products-inputs:last select').select2();
    },

    minusLineProduct: function () {
        $(this).closest('.row-order-products-inputs').remove();
    },

    // Курьерка
    initSenderInDelivery: function () {
        OrderIndexEdit.$modalSenderInDelivery.modal();
        OrderIndexEdit.completeForm(OrderIndexEdit.$modalSenderInDelivery.find('form'));
    },
    destroySenderInDelivery: function () {
        OrderIndexEdit.$btnSenderInDelivery.removeAttr('disabled');
        OrderIndexEdit.$btnSenderInDelivery.removeAttr('data-loading');
    },

    initResenderInDelivery: function () {
        OrderIndexEdit.$modalResenderInDelivery.modal();
        OrderIndexEdit.completeForm(OrderIndexEdit.$modalResenderInDelivery.find('form'));
    },
    destroyResenderInDelivery: function () {
        OrderIndexEdit.$btnResenderInDelivery.removeAttr('disabled');
        OrderIndexEdit.$btnResenderInDelivery.removeAttr('data-loading');
    },

    completeForm: function ($form) {
        $form.find('input[name="id[]"]').remove();
        $form.append('<input type="hidden" name="id[]" value="'+$("#order-id").val()+'">');
        $("#sender_in_delivery_select").val($("#deliveryrequest-delivery_id").val()).trigger("change");
    }
};
