$(function () {
    PenaltyTypeEdit.init();
});

var PenaltyTypeEdit = {
    init: function () {
        PenaltyTypeEdit.toggleLimitType();
        $(document).on('change', '#penaltytype-type', PenaltyTypeEdit.toggleLimitType);
    },
    toggleLimitType: function () {
        var v = $('#penaltytype-type').val();

        $(".limit").hide();
        $(".limit").find('input').prop("disabled", true);
        $(".limit_" + v).show();
        $(".limit_" + v).find('input').prop("disabled", false);
    }
};