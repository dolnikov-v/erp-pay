$(function () {
    var parent = $('.external-roles');
    var DesignationExtensionRole = {
        checkboxes: parent.find('input[type="checkbox"]'),
        init: function () {
            this.checkboxes.map(function () {
                $(this).on('change', function () {
                    var data = {
                        external_source_id: $(this).data('external_source_id'),
                        external_source_role_id: $(this).data('external_source_role_id'),
                        designation_id: $(this).data('designation_id'),
                        state: $(this).prop('checked') ? 1 : 0
                    };
                    DesignationExtensionRole.sentToServer(data, $(this));
                });
            });
        },
        inAction: function (cbx) {
            cbx.closest('div').css('opacity', 0.4);
            cbx.closest('div').css('cursor', 'progress');
        },
        outOffAction: function (cbx) {
            cbx.closest('div').css('opacity', 1);
            cbx.closest('div').css('cursor', 'pointer');
        },
        sentToServer: function (data, cbx) {
            this.inAction(cbx);

            $.ajax({
                url: '/salary/designation/manage-external-roles',
                type: 'post',
                data: data,
                error: function (error) {
                    DesignationExtensionRole.outOffAction(cbx);

                    cbx.prop('checked', false);

                    $.notify({message: error.responseText}, {
                        type: "danger",
                        animate: {exit: 'hide'},
                        z_index: 2031
                    });
                },
                success: function (data) {
                    DesignationExtensionRole.outOffAction(cbx);

                    var response = JSON.parse(data);

                    if (response.success) {
                        type = 'success dark';
                    } else {
                        type = 'danger dark';
                        cbx.prop('checked', false);
                    }

                    $.notify({message: response.message}, {
                        type: type,
                        animate: {exit: 'hide'},
                        z_index: 2031
                    });
                }
            });
        }
    };

    DesignationExtensionRole.init();
});