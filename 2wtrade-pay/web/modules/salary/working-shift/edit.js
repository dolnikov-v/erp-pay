var I18n = I18n || {};

$(function () {
    WorkingShiftEdit.init();
});

var WorkingShiftEdit = {
    $confirmed: 0,
    $formOriginalData: [],
    init: function () {
        WorkingShiftEdit.$formOriginalData = WorkingShiftEdit.serialize();

        $('.working-shift-edit').on('submit', WorkingShiftEdit.openModalWorkingShift);
        $(document).on('click', '#modal_confirm_working_shift_ok', WorkingShiftEdit.saveWorkingShift);
    },
    serialize: function (e) {
        var tmp = [];
        $('.working-shift-edit input:text').each(function (i) {
            if ($(this).attr('name') !='WorkingShift[name]') {
                tmp[i] = $(this).val();
            }
        });
        return JSON.stringify(tmp);
    },
    saveWorkingShift: function (e) {
        WorkingShiftEdit.$confirmed = 1;
        $('#modal_confirm_working_shift').modal('hide');
        $('input[name="generateDateFrom"]').val($('input[name="working_shift_from"]').val());
        $('input[name="generateDateTo"]').val($('input[name="working_shift_to"]').val());
        $('.working-shift-edit').submit();
    },
    openModalWorkingShift: function () {
        if (WorkingShiftEdit.$formOriginalData != WorkingShiftEdit.serialize()) {
            if (WorkingShiftEdit.$confirmed === 0) {
                $('#modal_confirm_working_shift').modal();
                $('#modal_confirm_working_shift_person').parent().hide();
                $('#modal_confirm_working_shift_name').parent().hide();
                return false;
            }
            else {
                return true;
            }
        }
        else {
            return true;
        }
    }
};
