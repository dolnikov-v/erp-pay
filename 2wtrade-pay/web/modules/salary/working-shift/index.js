var I18n = I18n || {};

$(function () {
    WorkingShiftIndex.init();
});

var WorkingShiftIndex = {
    $selected: 0,
    init: function () {
        $('.person_working_shift').on('click', WorkingShiftIndex.openModalWorkingShift);
        $(document).on('click', '#modal_confirm_working_shift_ok', WorkingShiftIndex.setWorkingShift);
    },
    setWorkingShift: function (e) {
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: $('#persons_content').data('url'),
            dataType: 'json',
            data: {
                id: WorkingShiftIndex.$selected.data('id'),
                work: WorkingShiftIndex.$selected.val(),
                date_from: $('input[name="working_shift_from"]').val(),
                date_to: $('input[name="working_shift_to"]').val()
            },
            success: function (response, textStatus) {
                var type = "danger dark";
                if (response.status == 'success') {
                    $('.person' + WorkingShiftIndex.$selected.data('id')).prop("checked", false);
                    WorkingShiftIndex.$selected.prop('checked', true);
                    $('#modal_confirm_working_shift').modal('hide');
                    type = "success dark";
                }
                $.notify({message: response.message}, {
                    type: type,
                    animate: {exit: 'hide'},
                    z_index: 2031
                });
            },
            error: function (response) {
                response = JSON.parse(response.responseText);
                if (response.statusText != 'abort') {
                    if (typeof response.message != 'undefined') {
                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    } else {
                        $.notify({message: I18n['Не удалось установить рабочую смену']}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                    }
                }
            }
        });
    },
    openModalWorkingShift: function () {
        $('#modal_confirm_working_shift').modal();
        $('#modal_confirm_working_shift_person').text($(this).closest('tr').find('.person').text());
        $('#modal_confirm_working_shift_name').text($('.working_shift_' + $(this).val()).text());
        WorkingShiftIndex.$selected = $(this);
        return false;
    }
};
