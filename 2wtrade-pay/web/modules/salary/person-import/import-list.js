$(function () {
    if ($(PersonImport.rootSelector).length) {
        PersonImport.init();
    }
});

var PersonImport = {

    rootSelector: '#person-import-grid',
    actionsSelector: '#person-import-actions',

    init: function () {
        $(document).on('change', this.rootSelector + ' .th-custom-checkbox input[type="checkbox"]', this.toggleCheckboxes);
        $(this.rootSelector + ' .td-custom-checkbox input[type="checkbox"]').on('change', this.toggleCheckbox);

        this.initGroupActions();
    },

    toggleCheckboxes: function () {
        $(PersonImport.rootSelector + ' .td-custom-checkbox input[type="checkbox"]').prop('checked', $(this).prop('checked')).trigger('change');
    },

    toggleCheckbox: function () {
        var $tr = $(this).closest('tr');

        if ($(this).prop('checked')) {
            $tr.addClass('active');
        } else {
            $tr.removeClass('active');
        }

        PersonImport.checkGroupActions();
    },

    checkGroupActions: function () {
        var checked = $(this.rootSelector + ' .td-custom-checkbox input[type="checkbox"]').is(':checked');
        $(this.actionsSelector + ' button').prop('disabled', !checked).toggleClass('btn-primary', checked);
    },

    generateUrl: function (url, params) {
        var urlParams = [];
        for (var name in params) {
            var value = params[name];
            if ($.isArray(value)) {
                var tmp = [];
                for (var i in value) {
                    tmp.push(name + '[]=' + value[i]);
                }

                urlParams.push(tmp.join('&'));
            } else {
                urlParams.push(name + '=' + value);
            }
        }

        return encodeURI(url + ((url.indexOf("?") == -1) ? '?' : '&') + urlParams.join('&'));
    },

    redirect: function(url) {
        if ($.isArray(url)) {
            if (url.length == 2) {
                url = this.generateUrl(url[0], url[1])
            } else if (url.length == 1) {
                url = url[0];
            }
        }
        window.location.href = url;
    },

    getCheckedRows: function() {
        var rows = [];
        $(this.rootSelector + ' .td-custom-checkbox input[type="checkbox"]:checked').each(function () {
            rows.push(this.value);
        });
        return rows;
    },

    initGroupActions: function () {
        var th = this;
        th.checkGroupActions();

        $(th.actionsSelector + ' a').click(function () {
            var $link = $(this);
            var href = $link.attr('href');
            if (href.indexOf('#') == 0) {
                var $modal = $(href);
                $modal.modal('show');
                $modal.find('.btn-primary').click(function () {
                    var $select = $modal.find('select');
                    var params = {rows: th.getCheckedRows()};
                    params[$select.prop('name')] = $select.val();
                    th.redirect([$(this).data('url'), params]);
                });
            } else {
                th.redirect([this.href, {rows: th.getCheckedRows()}]);
            }

            return false;
        });
    }
};