$(function () {
    OfficeTaxEdit.init();
});

var OfficeTaxEdit = {
    init: function () {
        OfficeTaxEdit.toggleLimitType();
        $(document).on('change', '#officetax-type', OfficeTaxEdit.toggleLimitType);
    },
    toggleLimitType: function () {
        var v = $('#officetax-type').val();

        $(".limit").hide();
        $(".limit").find('input').prop("disabled", true);
        $(".limit_" + v).show();
        $(".limit_" + v).find('input').prop("disabled", false);
    }
};