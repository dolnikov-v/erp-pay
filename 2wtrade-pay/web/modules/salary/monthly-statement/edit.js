$(function () {
    if ($('#data_content td').length) {
        MonthlyStatementEdit.init();
    }
});

var MonthlyStatementEdit = {

    $btnDeletePersons: $('#btn_delete_persons'),
    $modalDeletePersons: $('#modal_delete_persons'),

    init: function () {
        $(document).on('change', '#data_content .th-custom-checkbox input[type="checkbox"]', MonthlyStatementEdit.toggleCheckboxes);
        $('#data_content .td-custom-checkbox input[type="checkbox"]').on('change', MonthlyStatementEdit.toggleCheckbox);

        MonthlyStatementEdit.$btnDeletePersons.on('click', MonthlyStatementEdit.initDeletePersons);
        MonthlyStatementEdit.$modalDeletePersons.on('hide.bs.modal', MonthlyStatementEdit.destroyDeletePersons);

    },

    toggleCheckboxes: function () {
        $('#data_content .td-custom-checkbox input[type="checkbox"]').prop('checked', $(this).prop('checked')).trigger('change');
    },

    toggleCheckbox: function () {
        var $tr = $(this).closest('tr');

        if ($(this).prop('checked')) {
            $tr.addClass('active');
        } else {
            $tr.removeClass('active');
        }
    },

    initDeletePersons: function () {
        MonthlyStatementEdit.$modalDeletePersons.modal();
        MonthlyStatementEdit.completeForm(MonthlyStatementEdit.$modalDeletePersons.find('form'));
    },
    destroyDeletePersons: function () {
        MonthlyStatementEdit.$btnDeletePersons.removeAttr('disabled');
        MonthlyStatementEdit.$btnDeletePersons.removeAttr('data-loading');
        DeletePersons.stop();
    },
    completeForm: function ($form) {
        $form.find('input[name="data_id[]"]').remove();

        $('#data_content input[name="data_id[]"]:checked').each(function () {
            $(this).clone().attr('type', 'hidden').val($(this).val()).appendTo($form);
        });
    }
};
