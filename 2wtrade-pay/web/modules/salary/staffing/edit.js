var I18n = I18n || {};

$(function () {
    $('select[name="Staffing[payment_type]').on('change', StaffingEdit.changePaymentType);
    $('select[name="Staffing[office_id]').on('change', StaffingEdit.changeOffice);
});

var StaffingEdit = {
    changeOffice: function () {
        $('select[name="Staffing[base_staffing_id]"]').parent().hide();
        $('select[name="Staffing[bonus_types][]"]').parent().hide();
        $.ajax({
            type: 'GET',
            url: '/salary/staffing/get-options-by-office',
            data: {
                office_id: $(this).val(),
                id: $(this).closest('form').data('id'),
            },
            success: function (response) {
                $('select[name="Staffing[base_staffing_id]"]').parent().show();
                $('select[name="Staffing[base_staffing_id]"]').empty();
                $('select[name="Staffing[base_staffing_id]"]').append('<option value="">-</option>');
                response.staffings.forEach(function (entry) {
                    $('select[name="Staffing[base_staffing_id]"]').append('<option value=' + entry.id + '>' + entry.name + '</option>');
                });
                $('select[name="Staffing[base_staffing_id]"]').select2();

                $('select[name="Staffing[bonus_types][]"]').parent().show();
                $('select[name="Staffing[bonus_types][]"]').empty();
                $('select[name="Staffing[bonus_types][]"]').append('<option value="">-</option>');
                response.bonus_types.forEach(function (entry) {
                    $('select[name="Staffing[bonus_types][]"]').append('<option value=' + entry.id + '>' + entry.name + '</option>');
                });
                $('select[name="Staffing[bonus_types][]"]').select2();
            }
        });
    },
    changePaymentType: function () {
        if ($(this).val() == 'fixed') {
            $('.payment_type_fixed').show();
        }
        else {
            $('.payment_type_fixed').hide();
        }
        if ($(this).val() == 'percent') {
            $('.payment_type_percent').show();
        }
        else {
            $('.payment_type_percent').hide();
        }
    }
};
