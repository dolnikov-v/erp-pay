var offices = offices || {};
var officesCol = officesCol || {};

$(function () {

    $('.jobdescription').on('click', JobDescription.showModal);

    if ($('#btn_show_tips').length) {
        ShowTips.init();
    }
    $('select[name="StaffingSearch[type]"]').on('change', OfficeFilter.changeType);
});

var ShowTips = {
    $btnShowTips: $('#btn_show_tips'),
    $modalShowTips: $('#modal_show_tips'),
    init: function () {
        ShowTips.$btnShowTips.on('click', ShowTips.initShowTips);
    },
    initShowTips: function () {
        ShowTips.$modalShowTips.modal();
    }
};

var OfficeFilter = {
    changeType: function () {
        var t = $(this).val();
        $('select[name="StaffingSearch[office_id]').empty();
        $('select[name="StaffingSearch[office_id]').append('<option value="">-</option>');
        if (typeof officesCol[t] !== 'undefined') {
            var cat = officesCol[t];
        }
        else {
            var cat = offices;
        }
        for (var i in cat) {
            if (cat.hasOwnProperty(i)) {
                $('select[name="StaffingSearch[office_id]').append('<option value=' + i + '>' + cat[i] + '</option>');
            }
        }
        $('select[name="StaffingSearch[office_id]').select2();
    }
};

var JobDescription = {
    $modalJobDescription: $('#modal_job_description'),
    showModal: function (e) {
        e.preventDefault();
        $.ajax({
            type: 'GET',
            url: $('#modal_job_description').find('.url_holder').data('url'),
            dataType: 'json',
            data: {
                id: $(this).data('id')
            },
            success: function (response, textStatus) {
                if (response.text == '' && response.file == '') {
                    $("#no_job_description").show();
                }
                else {
                    $("#no_job_description").hide();
                }
                $("#job_description_text").html(response.text);
                if (response.file !='') {
                    $("#job_description_file").show().attr('href', response.file);
                }
                else {
                    $("#job_description_file").hide();
                }

                JobDescription.$modalJobDescription.modal();
            },
            error: function (response) {
                $.notify({message: 'Error'}, {
                    type: "danger dark",
                    animate: {exit: 'hide'},
                    z_index: 2031
                });
            }
        });
    }
};