$(function () {
    BonusTypeEdit.init();
});

var BonusTypeEdit = {
    init: function () {
        BonusTypeEdit.toggleLimitType();
        $(document).on('change', '#bonustype-type', BonusTypeEdit.toggleLimitType);
    },
    toggleLimitType: function () {
        var v = $('#bonustype-type').val();

        $(".limit").hide();
        $(".limit").find('input').prop("disabled", true);
        $(".limit_" + v).show();
        $(".limit_" + v).find('input').prop("disabled", false);
    }
};