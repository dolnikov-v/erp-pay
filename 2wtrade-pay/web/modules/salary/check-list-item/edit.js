var itemCategories = itemCategories || {};

$(function () {
    $('select[name="CheckListItem[type]"]').on('change', CheckListItemEdit.changeType);
});

var CheckListItemEdit = {
    changeType: function () {
        var t = $(this).val();
        $('select[name="CheckListItem[category_id]"]').empty();
        $('select[name="CheckListItem[category_id]"]').append('<option value="">-</option>');
        if (typeof itemCategories[t] !== 'undefined') {
            var cat = itemCategories[t];
            for (var i in cat) {
                if (cat.hasOwnProperty(i)) {
                    $('select[name="CheckListItem[category_id]"]').append('<option value=' + i + '>' + cat[i] + '</option>');
                }
            }
            $('select[name="CheckListItem[category_id]"]').select2();
        }
    }
};
