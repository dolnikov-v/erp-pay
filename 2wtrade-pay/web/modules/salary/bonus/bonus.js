$(function () {
    BonusEdit.init();
});

var BonusEdit = {
    init: function () {
        $(document).on('change', '#bonus-bonus_type_id', BonusEdit.toggleLimitType);
        $(document).on('change', '#bonus-office_id', BonusEdit.changeOffice);
    },
    changeOffice: function () {
        var office_id = $('#bonus-office_id').val();
        $('.person_holder').hide();
        $.ajax({
            type: 'GET',
            url: '/salary/person/get-persons',
            data: {
                office_id: office_id
            },
            success: function (response) {
                $('#bonus-person_id').empty();
                $('#bonus-person_id').append('<option value="">-</option>');
                response.forEach(function(entry) {
                    $('#bonus-person_id').append('<option value=' + entry.id + '>' + entry.name + '</option>');
                });
                $('.person_holder').show();
                $('#bonus-person_id').select2();
            }
        });

        $('.bonus_type_holder').hide();
        $.ajax({
            type: 'GET',
            url: '/salary/bonus/get-types',
            data: {
                office_id: office_id
            },
            success: function (response) {
                $('#bonus-bonus_type_id').empty();
                $('#bonus-bonus_type_id').append('<option value="">-</option>');
                response.forEach(function(entry) {
                    $('#bonus-bonus_type_id').append('<option value=' + entry.id + '>' + entry.name + '</option>');
                    bonusTypes[entry.id] = {type: entry.type, sum: entry.sum, sum_local: entry.sum_local, percent: entry.percent};
                });
                $('.bonus_type_holder').show();
                $('#bonus-bonus_type_id').select2();
            }
        });
    },
    toggleLimitType: function () {
        var v = $('#bonus-bonus_type_id').val();
        $(".limit").hide();
        //$(".limit").find('input').prop("readonly", true);
        if (v) {
            var type = bonusTypes[v];
            if (type) {
                $(".limit_" + type.type).show();
                if (type.type == 'sum' || type.type == 'sum_work_hours') {
                    $('#bonus-bonus_sum_usd').val(eval(type.sum));
                    $('#bonus-bonus_sum_local').val(eval(type.sum_local));
                }
                if (type.type == 'percent') {
                    $(".limit_percent").find('input').val(eval(type.percent));
                }
                if (type.type == 'percent_calc_salary') {
                    $(".limit_percent").find('input').val(eval(type.percent));
                }
                if (type.type == 'percent_work_hours') {
                    $(".limit_percent_work_hours").find('input').val(eval(type.percent));
                }
                if (type.type == 'hour') {
                    $(".limit_hour").find('input').val('');
                }
                if (type.type == 'other') {
                    $(".limit_other").find('input').val('').prop("readonly", false);
                }
            }
        }
    }
};