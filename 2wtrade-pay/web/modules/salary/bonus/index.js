$(function () {
    if ($('#bonuses_content td').length) {
        BonusIndex.init();
    }
});

var BonusIndex = {
    $btnDeleteBonuses: $('#btn_delete_bonuses'),
    $modalDeleteBonuses: $('#modal_delete_bonuses'),

    init: function () {
        $(document).on('change', '#bonuses_content .th-custom-checkbox input[type="checkbox"]', BonusIndex.toggleCheckboxes);
        $('#bonuses_content .td-custom-checkbox input[type="checkbox"]').on('change', BonusIndex.toggleCheckbox);

        BonusIndex.$btnDeleteBonuses.on('click', BonusIndex.initDeleteBonuses);
        BonusIndex.$modalDeleteBonuses.on('hide.bs.modal', BonusIndex.destroyDeleteBonuses);
    },

    toggleCheckboxes: function () {
        $('#bonuses_content .td-custom-checkbox input[type="checkbox"]').prop('checked', $(this).prop('checked')).trigger('change');
    },

    toggleCheckbox: function () {
        var $tr = $(this).closest('tr');

        if ($(this).prop('checked')) {
            $tr.addClass('active');
        } else {
            $tr.removeClass('active');
        }
    },
    initDeleteBonuses: function () {
        BonusIndex.$modalDeleteBonuses.modal();
        BonusIndex.completeForm(BonusIndex.$modalDeleteBonuses.find('form'));
    },
    destroyDeleteBonuses: function () {
        BonusIndex.$btnDeleteBonuses.removeAttr('disabled');
        BonusIndex.$btnDeleteBonuses.removeAttr('data-loading');
        DeleteBonuses.stop();
    },
    completeForm: function ($form) {
        $form.find('input[name="id[]"]').remove();

        $('#bonuses_content input[name="id[]"]:checked').each(function () {
            $(this).clone().attr('type', 'hidden').val($(this).val()).appendTo($form);
        });
    },
};
