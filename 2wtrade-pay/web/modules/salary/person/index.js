$(function () {
    if ($('#persons_content td').length) {
        PersonIndex.init();
    }
});

var PersonIndex = {
    $btnChangeTeamLeader: $('#btn_change_team_leader'),
    $modalChangeTeamLeader: $('#modal_change_team_leader'),

    $btnDeactivatePersons: $('#btn_deactivate_persons'),
    $modalDeactivatePersons: $('#modal_deactivate_persons'),

    $btnActivatePersons: $('#btn_activate_persons'),
    $modalActivatePersons: $('#modal_activate_persons'),

    $btnNotapprovePersons: $('#btn_notapprove_persons'),
    $modalNotapprovePersons: $('#modal_notapprove_persons'),

    $btnApprovePersons: $('#btn_approve_persons'),
    $modalApprovePersons: $('#modal_approve_persons'),

    $btnSetRole: $('#btn_set_role'),
    $modalSetRole: $('#modal_set_role'),

    $btnSetWork: $('#btn_set_work'),
    $modalSetWork: $('#modal_set_work'),

    $btnSetBonus: $('#btn_set_bonus'),
    $modalSetBonus: $('#modal_set_bonus'),

    $btnSetPenalty: $('#btn_set_penalty'),
    $modalSetPenalty: $('#modal_set_penalty'),

    $lnkDeactivate: $('.confirm-deactivate-link'),

    init: function () {
        $(document).on('change', '#persons_content .th-custom-checkbox input[type="checkbox"]', PersonIndex.toggleCheckboxes);
        $('#persons_content .td-custom-checkbox input[type="checkbox"]').on('change', PersonIndex.toggleCheckbox);

        PersonIndex.$btnChangeTeamLeader.on('click', PersonIndex.initChangeTeamLeader);
        PersonIndex.$modalChangeTeamLeader.on('hide.bs.modal', PersonIndex.destroyChangeTeamLeader);

        PersonIndex.$btnDeactivatePersons.on('click', PersonIndex.initDeactivatePersons);
        PersonIndex.$modalDeactivatePersons.on('hide.bs.modal', PersonIndex.destroyDeactivatePersons);

        PersonIndex.$btnActivatePersons.on('click', PersonIndex.initActivatePersons);
        PersonIndex.$modalActivatePersons.on('hide.bs.modal', PersonIndex.destroyActivatePersons);

        PersonIndex.$btnNotapprovePersons.on('click', PersonIndex.initNotapprovePersons);
        PersonIndex.$modalNotapprovePersons.on('hide.bs.modal', PersonIndex.destroyNotapprovePersons);

        PersonIndex.$btnApprovePersons.on('click', PersonIndex.initApprovePersons);
        PersonIndex.$modalApprovePersons.on('hide.bs.modal', PersonIndex.destroyApprovePersons);

        PersonIndex.$btnSetRole.on('click', PersonIndex.initSetRole);
        PersonIndex.$modalSetRole.on('hide.bs.modal', PersonIndex.destroySetRole);

        PersonIndex.$btnSetWork.on('click', PersonIndex.initSetWork);
        PersonIndex.$modalSetWork.on('hide.bs.modal', PersonIndex.destroySetWork);

        PersonIndex.$btnSetBonus.on('click', PersonIndex.initSetBonus);
        PersonIndex.$modalSetBonus.on('hide.bs.modal', PersonIndex.destroySetBonus);

        PersonIndex.$btnSetPenalty.on('click', PersonIndex.initSetPenalty);
        PersonIndex.$modalSetPenalty.on('hide.bs.modal', PersonIndex.destroySetPenalty);

        PersonIndex.$lnkDeactivate.on('click', PersonIndex.initDeactivate);
    },

    toggleCheckboxes: function () {
        $('#persons_content .td-custom-checkbox input[type="checkbox"]').prop('checked', $(this).prop('checked')).trigger('change');
    },

    toggleCheckbox: function () {
        var $tr = $(this).closest('tr');

        if ($(this).prop('checked')) {
            $tr.addClass('active');
        } else {
            $tr.removeClass('active');
        }
    },

    // Смена руководителя
    initChangeTeamLeader: function () {
        PersonIndex.$modalChangeTeamLeader.modal();
        PersonIndex.completeForm(PersonIndex.$modalChangeTeamLeader.find('form'));
    },
    destroyChangeTeamLeader: function () {
        PersonIndex.$btnChangeTeamLeader.removeAttr('disabled');
        PersonIndex.$btnChangeTeamLeader.removeAttr('data-loading');
        ChangeTeamLeader.stop();
    },
    initDeactivatePersons: function () {
        PersonIndex.$modalDeactivatePersons.modal();
        PersonIndex.completeForm(PersonIndex.$modalDeactivatePersons.find('form'));
    },
    destroyDeactivatePersons: function () {
        PersonIndex.$btnDeactivatePersons.removeAttr('disabled');
        PersonIndex.$btnDeactivatePersons.removeAttr('data-loading');
        DeactivatePersons.stop();
    },
    initActivatePersons: function () {
        PersonIndex.$modalActivatePersons.modal();
        PersonIndex.completeForm(PersonIndex.$modalActivatePersons.find('form'));
    },
    destroyActivatePersons: function () {
        PersonIndex.$btnActivatePersons.removeAttr('disabled');
        PersonIndex.$btnActivatePersons.removeAttr('data-loading');
        ActivatePersons.stop();
    },
    initNotapprovePersons: function () {
        PersonIndex.$modalNotapprovePersons.modal();
        PersonIndex.completeForm(PersonIndex.$modalNotapprovePersons.find('form'));
    },
    destroyNotapprovePersons: function () {
        PersonIndex.$btnNotapprovePersons.removeAttr('disabled');
        PersonIndex.$btnNotapprovePersons.removeAttr('data-loading');
        NotapprovePersons.stop();
    },
    initApprovePersons: function () {
        PersonIndex.$modalApprovePersons.modal();
        PersonIndex.completeForm(PersonIndex.$modalApprovePersons.find('form'));
    },
    destroyApprovePersons: function () {
        PersonIndex.$btnApprovePersons.removeAttr('disabled');
        PersonIndex.$btnApprovePersons.removeAttr('data-loading');
        ApprovePersons.stop();
    },
    initSetRole: function () {
        PersonIndex.$modalSetRole.modal();
        PersonIndex.completeForm(PersonIndex.$modalSetRole.find('form'));
    },
    destroySetRole: function () {
        PersonIndex.$btnSetRole.removeAttr('disabled');
        PersonIndex.$btnSetRole.removeAttr('data-loading');
        SetRole.stop();
    },
    initSetWork: function () {
        PersonIndex.$modalSetWork.modal();
        PersonIndex.completeForm(PersonIndex.$modalSetWork.find('form'));
    },
    destroySetWork: function () {
        PersonIndex.$btnSetWork.removeAttr('disabled');
        PersonIndex.$btnSetWork.removeAttr('data-loading');
        SetWork.stop();
    },
    initSetBonus: function () {
        PersonIndex.$modalSetBonus.modal();
        PersonIndex.completeForm(PersonIndex.$modalSetBonus.find('form'));
    },
    destroySetBonus: function () {
        PersonIndex.$btnSetBonus.removeAttr('disabled');
        PersonIndex.$btnSetBonus.removeAttr('data-loading');
        SetBonus.stop();
    },
    initSetPenalty: function () {
        PersonIndex.$modalSetPenalty.modal();
        PersonIndex.completeForm(PersonIndex.$modalSetPenalty.find('form'));
    },
    destroySetPenalty: function () {
        PersonIndex.$btnSetPenalty.removeAttr('disabled');
        PersonIndex.$btnSetPenalty.removeAttr('data-loading');
        SetPenalty.stop();
    },
    initDeactivate: function () {
        $('#modal_confirm_deactivate').find('form').attr('action', $(this).data('href'));
        $('#modal_confirm_deactivate').modal();
        return false
    },
    completeForm: function ($form) {
        $form.find('input[name="id[]"]').remove();

        $('#persons_content input[name="id[]"]:checked').each(function () {
            $(this).clone().attr('type', 'hidden').val($(this).val()).appendTo($form);
        });
    },
};
