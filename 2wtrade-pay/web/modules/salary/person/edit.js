$(function () {
    PersonEdit.init();
});

var PersonEdit = {
    init: function () {
        $('select[name="Person[office_id]').on('change', PersonEdit.changeOffice);
    },
    changeOffice: function () {
        $('select[name="Person[working_shift_id]"]').parent().hide();
        $.ajax({
            type: 'GET',
            url: '/salary/person/get-shifts',
            data: {
                office_id: $(this).val()
            },
            success: function (response) {
                $('select[name="Person[working_shift_id]"]').parent().show();
                $('select[name="Person[working_shift_id]"]').empty();
                $('select[name="Person[working_shift_id]"]').append('<option value="">-</option>');
                response.shifts.forEach(function (entry) {
                    $('select[name="Person[working_shift_id]"]').append('<option value=' + entry.id + '>' + entry.name + '</option>');
                });
                $('select[name="Person[working_shift_id]"]').select2();
            }
        });
    }
};