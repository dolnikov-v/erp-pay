$(function () {
    $('.user_callcenter_holder select').on('change', LinkUserPerson.change);
});

var LinkUserPerson = {
    change: function () {
        $('.user_login_holder').hide();
        $.ajax({
            type: 'GET',
            url: '/call-center/user/get-users',
            data: {
                callcenter_id: $('.user_callcenter_holder select').val(),
                free_person_id: $('#callcenterusersearch-person_id').val()
            },
            success: function (response) {
                $('.user_login_holder select').empty();
                $('.user_login_holder select').append('<option value="0">-</option>');
                $.each(response, function (key, value) {
                    $('.user_login_holder select').append('<option value=' + key + '>' + value + '</option>');
                });
                $('.user_login_holder').show();
            }
        });
    }
};
