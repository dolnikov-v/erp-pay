var I18n = I18n || {};

$(function () {
    $('select[name="WorkTimePlan[person_id]"]').on('change', WorkTimePlanEdit.changePerson);
    $('select[name="WorkTimePlan[working_shift_id]"]').on('change', WorkTimePlanEdit.changeWorkingShift);
});

var WorkTimePlanEdit = {
    changePerson: function () {
        $('select[name="WorkTimePlan[working_shift_id]"]').parent().hide();
        $('input[name="WorkTimePlan[start_at]"]').hide();
        $('input[name="WorkTimePlan[end_at]"]').hide();
        $('input[name="WorkTimePlan[lunch_time]"]').hide();
        $.ajax({
            type: 'GET',
            url: '/salary/work-time-plan/get-shifts',
            data: {
                person_id: $(this).val()
            },
            success: function (response) {
                $('select[name="WorkTimePlan[working_shift_id]"]').parent().show();
                $('input[name="WorkTimePlan[start_at]"]').show().val('');
                $('input[name="WorkTimePlan[end_at]"]').show().val('');
                $('input[name="WorkTimePlan[lunch_time]"]').show().val('');
                $('select[name="WorkTimePlan[working_shift_id]"]').empty();
                $('select[name="WorkTimePlan[working_shift_id]"]').append('<option value="">-</option>');
                response.shifts.forEach(function (entry) {
                    $('select[name="WorkTimePlan[working_shift_id]"]').append('<option value=' + entry.id + '>' + entry.name + '</option>');
                });
                $('select[name="WorkTimePlan[working_shift_id]"]').select2();
            }
        });
    },
    changeWorkingShift: function () {
        $('input[name="WorkTimePlan[start_at]"]').hide();
        $('input[name="WorkTimePlan[end_at]"]').hide();
        $('input[name="WorkTimePlan[lunch_time]"]').hide();
        $.ajax({
            type: 'GET',
            url: '/salary/work-time-plan/get-times',
            data: {
                working_shift_id: $(this).val(),
                date: $('input[name="WorkTimePlan[date]"]').val()
            },
            success: function (response) {
                $('input[name="WorkTimePlan[start_at]"]').show().val(response.start_at);
                $('input[name="WorkTimePlan[end_at]"]').show().val(response.end_at);
                $('input[name="WorkTimePlan[lunch_time]"]').show().val(response.lunch_time);
            }
        });
    }
};
