$(function () {
    if ($('#work-time-plan_content td').length) {
        WorkTimePlanIndex.init();
    }
});

var WorkTimePlanIndex = {
    $btnDeleteList: $('#btn_delete_list'),
    $modalDeleteList: $('#modal_delete_list'),

    init: function () {
        $(document).on('change', '#work-time-plan_content .th-custom-checkbox input[type="checkbox"]', WorkTimePlanIndex.toggleCheckboxes);
        $('#work-time-plan_content .td-custom-checkbox input[type="checkbox"]').on('change', WorkTimePlanIndex.toggleCheckbox);

        WorkTimePlanIndex.$btnDeleteList.on('click', WorkTimePlanIndex.initDeleteList);
        WorkTimePlanIndex.$modalDeleteList.on('hide.bs.modal', WorkTimePlanIndex.destroyDeleteList);
    },

    toggleCheckboxes: function () {
        $('#work-time-plan_content .td-custom-checkbox input[type="checkbox"]').prop('checked', $(this).prop('checked')).trigger('change');
    },

    toggleCheckbox: function () {
        var $tr = $(this).closest('tr');

        if ($(this).prop('checked')) {
            $tr.addClass('active');
        } else {
            $tr.removeClass('active');
        }
    },

    initDeleteList: function () {
        WorkTimePlanIndex.$modalDeleteList.modal();
        WorkTimePlanIndex.completeForm(WorkTimePlanIndex.$modalDeleteList.find('form'));
    },
    destroyDeleteList: function () {
        WorkTimePlanIndex.$btnDeleteList.removeAttr('disabled');
        WorkTimePlanIndex.$btnDeleteList.removeAttr('data-loading');
        DeleteList.stop();
    },
    completeForm: function ($form) {
        $form.find('input[name="id[]"]').remove();

        $('#work-time-plan_content input[name="id[]"]:checked').each(function () {
            $(this).clone().attr('type', 'hidden').val($(this).val()).appendTo($form);
        });
    }
};
