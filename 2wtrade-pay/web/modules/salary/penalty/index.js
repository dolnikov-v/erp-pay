$(function () {
    if ($('#penalties_content td').length) {
        PenaltyIndex.init();
    }
});

var PenaltyIndex = {
    $btnDeletePenalties: $('#btn_delete_penalties'),
    $modalDeletePenalties: $('#modal_delete_penalties'),

    init: function () {
        $(document).on('change', '#penalties_content .th-custom-checkbox input[type="checkbox"]', PenaltyIndex.toggleCheckboxes);
        $('#penalties_content .td-custom-checkbox input[type="checkbox"]').on('change', PenaltyIndex.toggleCheckbox);

        PenaltyIndex.$btnDeletePenalties.on('click', PenaltyIndex.initDeletePenalties);
        PenaltyIndex.$modalDeletePenalties.on('hide.bs.modal', PenaltyIndex.destroyDeletePenalties);
    },

    toggleCheckboxes: function () {
        $('#penalties_content .td-custom-checkbox input[type="checkbox"]').prop('checked', $(this).prop('checked')).trigger('change');
    },

    toggleCheckbox: function () {
        var $tr = $(this).closest('tr');

        if ($(this).prop('checked')) {
            $tr.addClass('active');
        } else {
            $tr.removeClass('active');
        }
    },
    initDeletePenalties: function () {
        PenaltyIndex.$modalDeletePenalties.modal();
        PenaltyIndex.completeForm(PenaltyIndex.$modalDeletePenalties.find('form'));
    },
    destroyDeletePenalties: function () {
        PenaltyIndex.$btnDeletePenalties.removeAttr('disabled');
        PenaltyIndex.$btnDeletePenalties.removeAttr('data-loading');
        DeletePenalties.stop();
    },
    completeForm: function ($form) {
        $form.find('input[name="id[]"]').remove();

        $('#penalties_content input[name="id[]"]:checked').each(function () {
            $(this).clone().attr('type', 'hidden').val($(this).val()).appendTo($form);
        });
    },
};
