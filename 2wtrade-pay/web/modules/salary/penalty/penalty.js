$(function () {
    PenaltyEdit.init();
});

var PenaltyEdit = {
    init: function () {
        $(document).on('change', '#penalty-penalty_type_id', PenaltyEdit.toggleLimitType);
        $(document).on('change', '#penalty-office_id', PenaltyEdit.changeOffice);
        $('#penalty-order_id').on('keyup', PenaltyEdit.findPersonByOrderId);
    },
    changeOffice: function () {
        if ($('#penalty-order_id').val() === '') {
            var v = $('#penalty-office_id').val();
            $('.person_holder').hide();
            if (v !== '') {
                $.ajax({
                    type: 'GET',
                    url: '/salary/person/get-persons',
                    data: {
                        office_id: v
                    },
                    success: function (response) {
                        PenaltyEdit.changeCallCenterPersonList(response);
                        $('.person_holder').show();
                        $('#penalty-person_id').select2();
                    }
                });
            } else {
                $('#penalty-person_id').empty();
                $('.person_holder').show();
                $('#penalty-person_id').select2();
            }
        }
    },
    toggleLimitType: function () {
        var v = $('#penalty-penalty_type_id').val();
        $(".limit").hide();
        $(".limit").find('input').prop("readonly", true);
        if (v) {
            var type = penaltyTypes[v];
            if (type) {
                $(".limit_" + type.type).show();
                if (type.type == 'sum') {
                    $(".limit_sum").find('input').val(eval(type.sum));
                }
                if (type.type == 'percent') {
                    $(".limit_percent").find('input').val(eval(type.percent));
                }
                if (type.type == 'other') {
                    $(".limit_other").find('input').val('').prop("readonly", false);
                }
            }
        }
    },
    findPersonByOrderId: function () {
        var value = $(this).val();
        if (value === '') {
            $('.person_holder_clone').hide();
            $('.call_center_office_holder_clone').hide();
            $('.person_holder').removeClass('hidden').show();
            $('.call_center_office_holder').removeClass('hidden').show();
        } else {
            $('.person_holder').hide();
            $('.call_center_office_holder').hide();
            $('.person_holder_clone').removeClass('hidden').show();
            $('.call_center_office_holder_clone').removeClass('hidden').show();

            $('#penalty-person_id').val(false).trigger('change');

            $('#penalty-person_id_clone').val(false).trigger('change');

            $.ajax({
                type: 'GET',
                url: '/salary/person/get-person-by-order-id',
                data: {
                    order_id: value
                },
                success: function (response) {
                    if (response.status === 'success' && $('#penalty-order_id').val() !== '') {
                        $('#penalty-office_id').val(response.office_id).trigger('change');
                        $('#penalty-office_id_clone').val(response.office_id).trigger('change');
                        PenaltyEdit.changeCallCenterPersonList(response.office_persons);
                        PenaltyEdit.changeCallCenterPersonCloneList(response.office_persons);
                        $('#penalty-person_id').val(response.person_id).trigger('change');
                        $('#penalty-person_id_clone').val(response.person_id).trigger('change');
                    }
                }
            });
        }
    },
    changeCallCenterPersonList: function (list) {
        $('#penalty-person_id').empty();
        $('#penalty-person_id').append('<option value="">-</option>');
        list.forEach(function (entry) {
            $('#penalty-person_id').append('<option value=' + entry.id + '>' + entry.name + '</option>');
        });
    },
    changeCallCenterPersonCloneList: function (list) {
        $('#penalty-person_id_clone').empty();
        $('#penalty-person_id_clone').append('<option value="">-</option>');
        list.forEach(function (entry) {
            $('#penalty-person_id_clone').append('<option value=' + entry.id + '>' + entry.name + '</option>');
        });
    }
};