$(function () {
    prefix = '#panel_api_';
    prefixForm = '#form_';

    DeliveryApiTesting.init();
});

var DeliveryApiTesting = {
    options: {},
    init: function () {
        $(prefix + $('#delivery_api_actions').val()).show(0);

        $('#action_authorization').on('click', DeliveryApiTesting.authorization);
        $(document).on('click', '#execute_testing_action', DeliveryApiTesting.executeForm);
        $('#delivery_api_actions').on('change', DeliveryApiTesting.changeAction);
    },
    changeAction: function () {
        $('.modified-panel').hide(0);
        $(prefix + $(this).val()).show(0);
    },
    executeForm: function () {
        var data = {};
        var action = $('#delivery_api_actions').val();

        data = $(prefixForm + action).serializeArray();
        data.push({name: 'action', value: action});

        $.ajax({
            url: DeliveryApiTesting.options['linkExecute'],
            type: 'post',
            data: data,
            success: function (response) {
                $.notify({message: 'Действие выполнено.'}, {
                    type: "success dark",
                    animate: {exit: 'hide'},
                    z_index: 2031
                });
                $('.answer-area').jJsonViewer(response);
                Ladda.stopAll();
            },
            error: function () {
                Ladda.stopAll();
            }
        });

        return false;
    },
    authorization: function () {
        var data = {};
        var action = $(this).data('action');

        data = $(prefixForm + $(this).data('action')).serializeArray();
        data.push({name: 'action', value: action});

        $.ajax({
            url: DeliveryApiTesting.options['linkExecute'],
            type: 'post',
            data: data,
            success: function (response) {
                $.notify({message: 'Действие выполнено.'}, {
                    type: "success dark",
                    animate: {exit: 'hide'},
                    z_index: 2031
                });
                $('.answer-area').jJsonViewer(response.data);
                $('input#deliveryapi_token').val(response.token);
                $('input#deliveryapi_base64_token').val(response.tokenBase64);
                Ladda.stopAll();
            },
            error: function () {
                Ladda.stopAll();
            }
        });

        return false;
    },
};
