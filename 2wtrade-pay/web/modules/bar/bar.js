$(function () {
    BarScanner.init();
});

var BarScanner = {
    barCodeLength: 15,
    ajax: null,
    $form: $('form[name="bar-field"]'),
    $barInput: $('form[name="bar-field"] .bar-input'),
    init: function () {
        BarScanner.$form.on('submit', BarScanner.checkBarCode);
    },
    checkBarCode: function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        var text = BarScanner.$barInput.val();
        text = text.replace(/_/g, '');
        if(text.length == BarScanner.barCodeLength) {
            BarScanner.ajax = $.ajax({
                type: 'POST',
                url: BarScanner.$form.attr('action'),
                dataType: 'json',
                data: {Bar:{bar_code:text}},
                success: function (response) {
                    if (response.status == 'success') {
                        $.notify({message: response.message}, {
                            type: "success dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                        BarScanner.$barInput.val('');
                    } else {
                        $.notify({message: response.message}, {
                            type: "danger dark",
                            animate: {exit: 'hide'},
                            z_index: 2031
                        });
                        BarScanner.$barInput.val('');
                        BarScanner.$barInput.focus();
                    }
                },
                error: function (response) {
                    BarScanner.queryError(response);
                }
            });
        }
    },
    queryError: function (response) {
        $.notify({message: response}, {
            type: "danger dark",
            animate: {exit: 'hide'},
            z_index: 2031
        });
    }
};