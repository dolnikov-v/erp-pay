/**
 * Created by ratvien on 19.12.16.
 */
$(function () {
    BarPack.init();
});

var BarPack = {
    ajax: null,
    $orderForm: $('form[name="order-form"]'),
    $orderInput: $('form[name="order-form"] .order-input'),
    init: function () {
        BarPack.$orderForm.on('submit', BarPack.findOrder);
    },
    findOrder: function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        BarPack.ajax = $.ajax({
            type: 'POST',
            url: $(this).attr('action'),
            dataType: 'json',
            data: {order_id: BarPack.$orderInput.val()},
            success: function (response) {
                if (response.status == 'success') {
                    $.notify({message: response.message}, {
                        type: "success dark",
                        animate: {exit: 'hide'},
                        z_index: 2031
                    });
                    if (typeof response.content != 'undefined') {
                        var $table = $('#products');
                        $table.html(response.content);
                        BarPack.initProducts();
                    }
                } else {
                    $.notify({message: response.message}, {
                        type: "danger dark",
                        animate: {exit: 'hide'},
                        z_index: 2031
                    });
                    BarPack.$orderInput.val('');
                }
            },
            error: function (response) {
                BarPack.queryError(response);
            }
        });
    },
    initProducts: function () {
        var productInput = $('.product-input');
        var productForm = $('form[name="product-form"]');
        var productSubmitButton = $('form[name="product-form"] button[type=submit]');
        var isFiled;

        productSubmitButton.attr('disabled', 'disabled');
        productInput.first().focus();
        productInput.keyup(function(a) {
            isFiled = true;
            productInput.each(function () {
                if(this.value.length<this.maxLength){
                    isFiled = false;
                }
            });
            if(isFiled){
                productSubmitButton.removeAttr('disabled');
            }
            else productSubmitButton.attr('disabled', 'disabled');

            if (a.which == 13) {
                a.preventDefault();
                var index = $(this).index('.product-input');
                productInput.eq(index+1).focus();
            }
        });
        productForm.on('submit', BarPack.changeStatus)
    },
    changeStatus: function (e) {
        var productInput = $('.product-input');
        var productArr = {};
        productInput.each(function(index) {
            productArr[index] = {
                bar_code: $(this).val(),
                order_id: $(this).data('order_id'),
                product_id: $(this).data('product_id')
            }
        });
        e.preventDefault();
        e.stopImmediatePropagation();
        BarPack.ajax = $.ajax({
            type: 'POST',
            url: $(this).attr('action'),
            data: productArr,
            dataType: 'json',
            encode:true,
            success: function (response) {
                if (response.status == 'success') {
                    BarPack.$orderInput.val('');
                    BarPack.$orderInput.focus();
                    $.notify({message: response.message}, {
                        type: "success dark",
                        animate: {exit: 'hide'},
                        z_index: 2031
                    });
                }
                else {
                    $.notify({message: response.message}, {
                        type: "danger dark",
                        animate: {exit: 'hide'},
                        z_index: 2031
                    });
                }
            },
            error: function (response) {
                BarPack.queryError(response);
            }
        });
    },
    queryError: function (response) {
        $.notify({message: response}, {
            type: "danger dark",
            animate: {exit: 'hide'},
            z_index: 2031
        });
    }
};