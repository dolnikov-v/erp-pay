/**
 * Created by ratvien on 19.12.16.
 */
$(function () {
    BarReturn.init();
});

var BarReturn = {
    ajax: null,
    $orderForm: $('form[name="order-form"]'),
    $orderInput: $('form[name="order-form"] .order-input'),
    init: function () {
        BarReturn.$orderForm.submit(BarReturn.findOrder);
    },
    findOrder: function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        BarReturn.ajax = $.ajax({
            type: 'POST',
            url: $(this).attr('action'),
            dataType: 'json',
            data: {
                    id : BarReturn.$orderInput.val()
            },
            success: function (response) {
                if (response.status == 'success') {
                    BarReturn.$orderInput.val('');
                    BarReturn.$orderInput.focus();
                    $.notify({message: response.message}, {
                        type: "success dark",
                        animate: {exit: 'hide'},
                        z_index: 2031
                    });
                } else {
                    $.notify({message: response.message}, {
                        type: "danger dark",
                        animate: {exit: 'hide'},
                        z_index: 2031
                    });
                }
            },
            error: function (response) {
                BarReturn.queryError(response);
            }
        });
        return false;
    },
    queryError: function (response) {
        $.notify({message: response}, {
            type: "danger dark",
            animate: {exit: 'hide'},
            z_index: 2031
        });
    }
};