$(function () {
    const _CHANGE_ = 'change';
    const _OPEN_ = 'open';
    const _ADD_ = 'add';
    const _REMOVE_ = 'remove';

    var delivery_data = $('.delivery_data');

    var Rqst = {
        parent: $('.requisite-delivery-links'),
        data: JSON.parse(delivery_data.text()),
        viewMode: delivery_data.data('view'),
        freeDelivery: [],
        freeCountry: [],
        usedData: {
            contracts: []
        },
        index: 0,
        //инициализация при ренндере страницы
        init: function (type) {
            var parent = Rqst.parent;
            var rows = parent.find('.requisite-delivery-link');

            if (rows.length > 1) {
                rows.map(function () {
                    $(this).find('.btn-requisite-delivery-link-plus').hide();
                });
            }

            rows.map(function () {
                Rqst.fillSelects($(this));
            });

            if (Rqst.getCountContracts() > rows.length) {
                rows.last().find('.btn-requisite-delivery-link-plus').show();
            }

            if (type !== _REMOVE_) {
                $('.btn-requisite-delivery-link-plus').map(function () {
                    $(this).unbind('click');
                    $(this).on('click', function () {
                        Rqst.add();
                    });
                });

                $('.btn-requisite-delivery-link-minus').map(function () {
                    $(this).on('click', function () {
                        $(this).unbind('click');
                        Rqst.remove($(this));
                    });
                });

                var selects = $('select');

                selects.on('change', function () {
                    Rqst.changeControl($(this), _CHANGE_);
                    Rqst.control($(this), _ADD_);

                    var parent = $(this).closest('.requisite-delivery-link');
                    if (parent !== 'undefined') {
                        var is_empty = false;
                        parent.find('select').map(function () {
                            if ($(this).val() === '') {
                                is_empty = true;
                            }
                        });

                        if (!is_empty) {
                            parent.find("[type=radio]").prop('disabled', false);
                        } else {
                            parent.find("[type=radio]").prop('disabled', true);
                            parent.find("[type=radio]").prop('checked', false);
                        }
                    }
                });

                selects.on('select2:open', function () {
                    Rqst.changeControl($(this), _OPEN_);
                });
                //сбросить именя для использовании в группе
                Rqst.clearNamesRadioButtons();
            }
            Rqst.getContracts();
        },
        getCountContracts: function () {
            var contractsByCountry = Rqst.getContractsByCountry();
            var count = 0;

            for (var k in contractsByCountry) {
                count += contractsByCountry[k].length;
            }

            return count;
        },
        //собрать все прорендеренные контракты, пометить как занятые
        getContracts: function () {
            $('select').map(function () {
                if (/contract_id/.test($(this).prop('name'))) {
                    Rqst.control($(this), _ADD_);
                }
            });
        },
        changeControl: function (select, type) {
            if (type === _CHANGE_) {
                Rqst.addBlockedContract(select);
            } else if (type === _OPEN_) {
                Rqst.blockDelivery(select)
            } else if (type === _ADD_) {
                Rqst.addBlockedCountry();
            }
        },
        addBlockedContract: function (select) {
            select.find('option').each(function () {
                var parent = '.requisite-delivery-link';
                //определить страну
                var country = $(this).closest(parent).find("[name*=country_id]").val();
                //определить курьерку
                var delivery = $(this).closest(parent).find("[name*=delivery_id]").val();

                if ($(this).val() !== '' && typeof $(this) === 'object') {
                    var isContract = /contract_id/.test($(this).closest('select').prop('name'));
                    //проверить - какие контракты уже использованы
                    if (isContract) {
                        if ($.inArray($(this).val(), Rqst.usedData.contracts) !== -1) {
                            $(this).attr('disabled', true);
                        }
                    }

                    var isCountry = /country_id/.test($(this).closest('select').prop('name'));
                    //Проверить - какие курьерки уже не имеют свободных контрактов
                    if (isCountry) {
                        //список уже использованных контрактов
                        var blocked_contracts = Rqst.usedData.contracts;

                        if (country !== '') {
                            var deliveries = Object.keys(Rqst.data.delivery[country]);
                            var free = [];

                            for (var k in deliveries) {
                                free[deliveries[k]] = true;
                            }

                            for (var k in deliveries) {
                                var target_delivery = deliveries[k];
                                var target_contracts = Object.keys(Rqst.data.contract[country][target_delivery]);

                                for (var j in target_contracts) {
                                    if ($.inArray(target_contracts[j], blocked_contracts) === -1) {
                                        free[target_delivery] = false;
                                    }
                                }
                            }

                            Rqst.freeDelivery = free;
                        }
                    }
                }
            });
        },
        blockDelivery: function (select) {
            var isDelivery = /delivery_id/.test(select.closest('select').prop('name'));

            if (isDelivery) {
                select.find('option').map(function () {
                    for (var k in Rqst.freeDelivery) {
                        if ($(this).val() === k && Rqst.freeDelivery[k]) {
                            $(this).attr('disabled', true);
                        }
                    }
                });
            }
        },
        addBlockedCountry: function () {
            var countries = Rqst.getCountries();
            var contracts = Rqst.getContractsByCountry();
            var freeCountry = [];

            for (var k in countries) {
                freeCountry[countries[k]] = [];
            }

            for (var k in Rqst.usedData.contracts) {
                for (var j in contracts) {
                    if ($.inArray(Rqst.usedData.contracts[k], contracts[j]) > -1) {
                        freeCountry[j].push(Rqst.usedData.contracts[k]);

                        if (freeCountry[j].length === contracts[j].length) {
                            Rqst.freeCountry.push(j);
                        }
                    }
                }
            }
        },
        getContractsByCountry: function () {
            var contracts = [];

            for (var k in Rqst.data.contract) {
                if (typeof contracts[k] === 'undefined') {
                    contracts[k] = [];
                }

                for (var j in Rqst.data.contract[k]) {
                    for (var l in Rqst.data.contract[k][j]) {
                        contracts[k].push(l);
                    }
                }
            }

            return contracts;
        },
        //доступные страны
        getCountries: function () {
            return Object.keys(Rqst.data.contract);
        },
        //выставим данные селектов из data атрибута родительского div (с сервера)
        setValue: function (select) {
            var parent = select.closest('.requisite-delivery-link');
            var parentData = parent.data();

            if (typeof parentData !== 'object' || select.val() !== '') {
                return;
            }

            var name = select.prop('name');

            if (/country_id/.test(name)) {
                select.val(parentData.country_id);
            } else if (/delivery_id/.test(name)) {
                select.val(parentData.delivery_id);
            } else if (/contract_id/.test(name)) {
                select.val(parentData.delivery_contract_id);
                parent.find("[type=radio]").prop('disabled', false);
            }

            select.trigger('change');
        },
        control: function (select, type) {
            var parent = Rqst.parent;

            if (type === _ADD_) {
                parent.find("[name*=contract_id]").map(function () {
                    if ($(this).val() !== '' && $(this).val() !== null) {

                        if ($.inArray($(this).val(), Rqst.usedData.contracts) < 0) {
                            Rqst.usedData.contracts.push($(this).val());
                        }
                    }
                });
            } else if (_REMOVE_) {
                for (var k in Rqst.usedData.contracts) {
                    if (Rqst.usedData.contracts[k] === select.val()) {
                        delete Rqst.usedData.contracts[k];
                    }
                }
            }
        },
        //добавить позицию
        add: function () {
            var parent = Rqst.parent;
            var rows = parent.find('.requisite-delivery-link');
            var row = rows.last().clone();
            var index = rows.length * 1000;

            var isEmpty = false;

            parent.find('.requisite-delivery-link').last().find('select').map(function () {
                if ($(this).val() === '') {
                    isEmpty = true;
                }
            });

            if (!isEmpty) {
                row.removeAttr('data-country_id');
                row.removeAttr('data-delivery_id');
                row.removeAttr('data-delivery_contract_id');
                row.find("[type=radio]").prop('disabled', true);
                row.find("[type=radio]").prop('name', 'is_default[]');
                row.find("[type=radio]").prop('checked', false);
                row.find('select').map(function () {
                    var name = $(this).prop('name');
                    $(this).prop('name', name.replace(/\d{1,}/, index));
                });

                row.find('span').remove();
                row.find('select').select2();
                row.appendTo(parent);
                row.attr('data-index', index);

                Rqst.changeControl(row.find("[name*=country_id]"), _ADD_);

                Rqst.init();
            }
        },
        //удалить позицию
        remove: function (btn) {
            var parent = Rqst.parent;
            var rows = parent.find('.requisite-delivery-link');
            var row = btn.closest('.requisite-delivery-link');

            if (rows.length > 1) {
                row.remove();
            } else {
                row.find('select').map(function () {
                    $(this).val('');
                    $(this).trigger('change');
                });
            }
            Rqst.control(row.find("[name*=contract_id]"), 'remove');
            Rqst.init(_REMOVE_);
        },
        //блокировать зависимые списки если у парента нет значения
        fillSelects: function (row) {
            if (row.find("[name*=delivery_id]").val() !== '') {
                return;
            }

            row.find("[name*=delivery_id]").prop('disabled', true);
            row.find("[name*=contract_id]").prop('disabled', true);

            Rqst.generateOptions(row.find("[name*=country_id]"), Rqst.data.country);
        },
        //сформировать options из json на странице
        generateOptions: function (select, data) {
            $(select).find('option').map(function () {
                if ($(this).text() !== '—') {
                    $(this).remove()
                }
            });

            for (var k in data) {
                var option = new Option(data[k].name, data[k].id, false, false);
                $(select).append(option);
            }

            if (/country_id/.test($(select).prop('name'))) {
                $(select).find('option').map(function () {
                    if ($.inArray($(this).val(), Rqst.freeCountry) > -1) {
                        $(this).attr('disabled', true);
                    }
                });
            }

            $(select).on('change', function () {
                $(Rqst.manageSelect($(this)));
            });

            Rqst.setValue($(select));
        },
        //управление зависимыми списками
        manageSelect: function (select) {
            var name = select.prop('name');
            var selects = $(select).closest('.requisite-delivery-link').find('select');
            var names = [];

            selects.each(function (idx) {
                names.push($(this).prop('name').replace('_id[]', ''));
                if ($(this).prop('name') === name) {
                    Rqst.index = idx;
                }
            });

            if (select.val() === '') {
                Rqst.manageNextSelects(selects, true);
            } else {
                Rqst.manageNextSelects(selects, Rqst.viewMode);

                if (/country/.test($(select).prop('name'))) {
                    Rqst.index = 0;
                }else if (/delivery/.test($(select).prop('name'))) {
                    Rqst.index = 1;
                }else if (/contract/.test($(select).prop('name'))) {
                    Rqst.index = 2;
                }

                var nextIndex = Rqst.index + 1;
                var nextSelect = selects[nextIndex];
                var temp_data;

                //определяем зависимый select
                if (typeof nextSelect !== 'undefined') {
                    if (Rqst.viewMode === 0) {
                        $(nextSelect).prop('disabled', false);
                    }

                    $(nextSelect).val('');
                    $(nextSelect).trigger('change');

                    var data = [];

                    //подтягиваем курьерки
                    if (/country/.test(select.prop('name'))) {
                        temp_data = Rqst.data.delivery[select.val()];
                        for (var k in temp_data) {
                            data.push(temp_data[k]);
                        }
                    } else if (/delivery/.test(select.prop('name'))) {
                        //подтягиваем контракты
                        var country_id = select.closest('.requisite-delivery-link').find("[name*=country_id]").val();
                        var delivery_id = select.closest('.requisite-delivery-link').find("[name*=delivery_id]").val();

                        temp_data = Rqst.data.contract[country_id][delivery_id];

                        for (var k in temp_data) {
                            data.push(temp_data[k]);
                        }
                    }

                    Rqst.generateOptions(nextSelect, data);
                }
            }
        },
        manageNextSelects: function (selects, disabled) {
            selects.each(function (idx) {
                if (idx > Rqst.index) {
                    $(this).val('');
                    $(this).trigger('change');
                    $(this).prop('disabled', disabled);
                }
            });
        },
        //сбросить именя для использовании в группе
        clearNamesRadioButtons: function () {
            var parent = Rqst.parent;

            parent.find("[type=radio]").map(function () {
                $(this).prop('name', 'is_default[]');
            });
        }
    };

    Rqst.init();

    //пронумеровать radio
    $("[type=submit]").on('click', function () {
        var parent = Rqst.parent;
        var rows = parent.find('.requisite-delivery-link');
        // var index = 0;

        rows.find("[type=radio]").map(function () {
            //index++;
            var index = $(this).closest('.requisite-delivery-link').attr('data-index')
            var name = $(this).prop('name');
            $(this).prop('name', name.replace('[]', '[' + index + ']'));
        });
    });
});