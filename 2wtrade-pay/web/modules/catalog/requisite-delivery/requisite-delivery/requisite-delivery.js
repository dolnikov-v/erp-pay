$(function () {
    var InvoiceTPLManager = {
        select_template: $("[name*=invoice_tpl]"),
        image_block: $('div.stamp'),
        image_class: '.stamp',
        image_prefix_class: '.index-',
        tpl_collection: JSON.parse($('.requisite-data').html()),
        data_fields: [
            'name',
            'address',
            'city',
            'beneficiary_bank',
            'bank_address',
            'bank_account',
            'swift_code'
        ],
        init: function () {
            $(InvoiceTPLManager.select_template).on('change', function () {
                $(InvoiceTPLManager.image_block).find(InvoiceTPLManager.image_class).addClass('hidden');
                $(InvoiceTPLManager.image_block).find(InvoiceTPLManager.image_prefix_class + $(this).val()).removeClass('hidden')

                InvoiceTPLManager.manageStamp($(this).val());
                InvoiceTPLManager.useTPLCollection($(this).val());
            });
        },
        useTPLCollection: function (tpl) {
            for (var k in InvoiceTPLManager.data_fields) {
                if (typeof InvoiceTPLManager.tpl_collection[tpl] != 'undefined') {
                    var empty = tpl === '',
                        value = empty ? '' : InvoiceTPLManager.tpl_collection[tpl][InvoiceTPLManager.data_fields[k]],
                        field = $("[name*=" + InvoiceTPLManager.data_fields[k] + "]");


                    field.val(value);
                    field.blur();
                }
            }
        },
        manageStamp: function (tpl) {
            if (tpl === '') {
                $(InvoiceTPLManager.image_class).find('i').removeClass('hidden');
            } else {
                $(InvoiceTPLManager.image_class).find('i').addClass('hidden');
            }
        }
    };

    InvoiceTPLManager.init();
    InvoiceTPLManager.useTPLCollection($(InvoiceTPLManager.select_template).val());
    InvoiceTPLManager.manageStamp($(InvoiceTPLManager.select_template).val());

});

