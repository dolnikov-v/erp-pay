$(document).ready(function () {
    $('#certificate_not_needed').on('change', function(){
       if($(this).is(':checked')){
           $('#certificate-name').val($('#hidden').val());
           $('#certificateName').focus();
           $('#CertificateFiles').prop('disabled', true);
           $('#is_partner').prop( "checked", false );
           $('#is_partner').prop('disabled', true);
       }
       else{
           $('#certificate-name').val('');
           $('#CertificateFiles').prop('disabled', false);
           $('#is_partner').prop('disabled', false);
       }
    });
});