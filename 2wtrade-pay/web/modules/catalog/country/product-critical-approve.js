$(function () {
    var CriticalApprove = {
        parent: $('.products_percent'),
        rowsClass: '.product_percent',
        plus: '.btn-critical-approve-plus',
        minus: '.btn-critical-approve-minus',
        typeApprove: $('[name*=approve_type]'),
        type_country: 'country',
        type_product: 'product',
        class_by_country: '.by-country',
        class_by_product: '.by-products',
        critical_approve_percent : $('[name*=critical_approve_percent]'),
        rows: [],
        changeType: function(){
            CriticalApprove.typeApprove.on('change', function(){
                if($(this).val() === CriticalApprove.type_country){
                    $(CriticalApprove.class_by_product).addClass('hidden');
                    $(CriticalApprove.class_by_country).removeClass('hidden');

                    $(CriticalApprove.class_by_product).find(CriticalApprove.rowsClass).map(function(){
                        if($(CriticalApprove.class_by_product).find(CriticalApprove.rowsClass).length > 1){
                            $(this).remove();
                        }else{
                            $(this).find("[name*=product_id]").val('');
                            $(this).find("[name*=product_id]").trigger('change')
                            $(this).find("[name*=percent]").val('');
                        }
                    });
                }else if($(this).val() === CriticalApprove.type_product){
                    CriticalApprove.critical_approve_percent.val('');
                    $(CriticalApprove.class_by_country).addClass('hidden')
                    $(CriticalApprove.class_by_product).removeClass('hidden')
                }
            });
        },
        add: function () {
            add();
            CriticalApprove.init();
        },
        remove: function (btn) {
            remove(btn);
        },
        init: function () {
            CriticalApprove.rows = CriticalApprove.parent.find(CriticalApprove.rowsClass);
            if ($(CriticalApprove.rowsClass).length > 1) {
                $(CriticalApprove.plus).hide();
                $(CriticalApprove.plus).last().show();
                $(CriticalApprove.minus).show();
                $(CriticalApprove.minus).last().hide();
            } else {
                $(CriticalApprove.minus).hide();
            }

            $(CriticalApprove.plus).map(function (e) {
                $(this).off('click');
                $(this).on('click', CriticalApprove.add);
            });

            $(CriticalApprove.minus).map(function () {
                $(this).on('click', function () {
                    CriticalApprove.remove($(this));
                });
            });

            CriticalApprove.changeType();
        }
    };

    CriticalApprove.init();

    $(CriticalApprove.minus).on('click', function () {
        CriticalApprove.remove($(this));
    });

    function add() {
        var clone = $(CriticalApprove.parent).find(CriticalApprove.rowsClass).last().clone();

        clone.find('span').remove();
        clone.find('select').select2().val('').trigger('change');
        $(clone).appendTo(CriticalApprove.parent);
    }

    function remove(btn) {
        $(btn).closest(CriticalApprove.rowsClass).remove()
    }
});