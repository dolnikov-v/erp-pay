$(function () {
    if ($(PackagerRequestIndex.rootSelector).length) {
        PackagerRequestIndex.init();
    }
});

var PackagerRequestIndex = {

    $btnLabelPrinting: $('#btn_label_printing'),
    $modalLabelPrinting: $('#modal_label_printing'),

    $btnConfirmCompleting: $('#btn_confirm_completing'),
    $modalConfirmCompleting: $('#modal_confirm_completing'),

    $btnCancelCompleting: $('#btn_cancel_completing'),
    $modalCancelCompleting: $('#modal_cancel_completing'),

    $btnRefusalCompleting: $('#btn_refusal_completing'),
    $modalRefusalCompleting: $('#modal_refusal_completing'),

    $btnListGeneration: $('#btn_list_generation'),
    $modalListGeneration: $('#modal_list_generation'),

    $btnCancelShipment: $('#btn_cancel_shipment'),
    $modalCancelShipment: $('#modal_cancel_shipment'),

    $btnSetShipped: $('#btn_set_shipped'),
    $modalSetShipped: $('#modal_set_shipped'),

    $btnSetReturn: $('#btn_set_return'),
    $modalSetReturn: $('#modal_set_return'),

    $btnSetApprove: $('#btn_set_approve'),
    $modalSetApprove: $('#modal_set_approve'),

    $btnChangeDelivery: $('#btn_change_delivery'),
    $modalChangeDelivery: $('#modal_change_delivery'),

    $btnSenderInDelivery: $('#btn_sender_in_delivery'),
    $modalSenderInDelivery: $('#modal_sender_in_delivery'),

    $btnGetManifest: $('#btn_get_manifest'),
    $modalGetManifest: $('#modal_get_manifest'),

    rootSelector: '#packager-request-grid',

    init: function () {
        $(document).on('change', this.rootSelector + ' .th-custom-checkbox input[type="checkbox"]', this.toggleCheckboxes);
        $(this.rootSelector + ' .td-custom-checkbox input[type="checkbox"]').on('change', this.toggleCheckbox);

        PackagerRequestIndex.$btnLabelPrinting.on('click', PackagerRequestIndex.initLabelPrinting);
        PackagerRequestIndex.$modalLabelPrinting.on('hide.bs.modal', PackagerRequestIndex.destroyLabelPrinting);

        PackagerRequestIndex.$btnConfirmCompleting.on('click', PackagerRequestIndex.initConfirmCompleting);
        PackagerRequestIndex.$modalConfirmCompleting.on('hide.bs.modal', PackagerRequestIndex.destroyConfirmCompleting);

        PackagerRequestIndex.$btnCancelCompleting.on('click', PackagerRequestIndex.initCancelCompleting);
        PackagerRequestIndex.$modalCancelCompleting.on('hide.bs.modal', PackagerRequestIndex.destroyCancelCompleting);

        PackagerRequestIndex.$btnRefusalCompleting.on('click', PackagerRequestIndex.initRefusalCompleting);
        PackagerRequestIndex.$modalRefusalCompleting.on('hide.bs.modal', PackagerRequestIndex.destroyRefusalCompleting);

        PackagerRequestIndex.$btnListGeneration.on('click', PackagerRequestIndex.initListGeneration);
        PackagerRequestIndex.$modalListGeneration.on('hide.bs.modal', PackagerRequestIndex.destroyListGeneration);

        PackagerRequestIndex.$btnCancelShipment.on('click', PackagerRequestIndex.initCancelShipment);
        PackagerRequestIndex.$modalCancelShipment.on('hide.bs.modal', PackagerRequestIndex.destroyCancelShipment);

        PackagerRequestIndex.$btnSetShipped.on('click', PackagerRequestIndex.initSetShipped);
        PackagerRequestIndex.$modalSetShipped.on('hide.bs.modal', PackagerRequestIndex.destroySetShipped);

        PackagerRequestIndex.$btnSetReturn.on('click', PackagerRequestIndex.initSetReturn);
        PackagerRequestIndex.$modalSetReturn.on('hide.bs.modal', PackagerRequestIndex.destroySetReturn);

        PackagerRequestIndex.$btnSetApprove.on('click', PackagerRequestIndex.initSetApprove);
        PackagerRequestIndex.$modalSetApprove.on('hide.bs.modal', PackagerRequestIndex.destroySetApprove);

        PackagerRequestIndex.$btnChangeDelivery.on('click', PackagerRequestIndex.initChangeDelivery);
        PackagerRequestIndex.$modalChangeDelivery.on('hide.bs.modal', PackagerRequestIndex.destroyChangeDelivery);

        PackagerRequestIndex.$btnSenderInDelivery.on('click', PackagerRequestIndex.initSenderInDelivery);
        PackagerRequestIndex.$modalSenderInDelivery.on('hide.bs.modal', PackagerRequestIndex.destroySenderInDelivery);

        PackagerRequestIndex.$btnGetManifest.on('click', PackagerRequestIndex.initGetManifest);
        PackagerRequestIndex.$modalGetManifest.on('hide.bs.modal', PackagerRequestIndex.destroyGetManifest);

    },

    initLabelPrinting: function () {
        LabelPrinting.count = LabelPrinting.$form.find('input[name="id[]"]').length;
        PackagerRequestIndex.$modalLabelPrinting.modal();
        PackagerRequestIndex.completeForm(PackagerRequestIndex.$modalLabelPrinting.find('form'), 1);
    },
    destroyLabelPrinting: function () {
        PackagerRequestIndex.$btnLabelPrinting.removeAttr('disabled');
        PackagerRequestIndex.$btnLabelPrinting.removeAttr('data-loading');
        LabelPrinting.stop();
    },

    initConfirmCompleting: function () {
        PackagerRequestIndex.$modalConfirmCompleting.modal();
        PackagerRequestIndex.completeForm(PackagerRequestIndex.$modalConfirmCompleting.find('form'));
    },
    destroyConfirmCompleting: function () {
        PackagerRequestIndex.$btnConfirmCompleting.removeAttr('disabled');
        PackagerRequestIndex.$btnConfirmCompleting.removeAttr('data-loading');
        ConfirmCompleting.stop();
    },

    initCancelCompleting: function () {
        PackagerRequestIndex.$modalCancelCompleting.modal();
        PackagerRequestIndex.completeForm(PackagerRequestIndex.$modalCancelCompleting.find('form'));
    },
    destroyCancelCompleting: function () {
        PackagerRequestIndex.$btnCancelCompleting.removeAttr('disabled');
        PackagerRequestIndex.$btnCancelCompleting.removeAttr('data-loading');
        CancelCompleting.stop();
    },

    initRefusalCompleting: function () {
        PackagerRequestIndex.$modalRefusalCompleting.modal();
        PackagerRequestIndex.completeForm(PackagerRequestIndex.$modalRefusalCompleting.find('form'));
    },
    destroyRefusalCompleting: function () {
        PackagerRequestIndex.$btnRefusalCompleting.removeAttr('disabled');
        PackagerRequestIndex.$btnRefusalCompleting.removeAttr('data-loading');
        RefusalCompleting.stop();
    },

    initListGeneration: function () {
        PackagerRequestIndex.$modalListGeneration.modal();
        PackagerRequestIndex.completeForm(PackagerRequestIndex.$modalListGeneration.find('form'));
    },
    destroyListGeneration: function () {
        PackagerRequestIndex.$btnListGeneration.removeAttr('disabled');
        PackagerRequestIndex.$btnListGeneration.removeAttr('data-loading');
        ListGeneration.stop();
    },

    initCancelShipment: function () {
        PackagerRequestIndex.$modalCancelShipment.modal();
        PackagerRequestIndex.completeForm(PackagerRequestIndex.$modalCancelShipment.find('form'));
    },
    destroyCancelShipment: function () {
        PackagerRequestIndex.$btnCancelShipment.removeAttr('disabled');
        PackagerRequestIndex.$btnCancelShipment.removeAttr('data-loading');
        CancelShipment.stop();
    },

    initSetShipped: function () {
        PackagerRequestIndex.$modalSetShipped.modal();
        PackagerRequestIndex.completeForm(PackagerRequestIndex.$modalSetShipped.find('form'));
    },
    destroySetShipped: function () {
        PackagerRequestIndex.$btnSetShipped.removeAttr('disabled');
        PackagerRequestIndex.$btnSetShipped.removeAttr('data-loading');
        SetShipped.stop();
    },

    initSetReturn: function () {
        PackagerRequestIndex.$modalSetReturn.modal();
        PackagerRequestIndex.completeForm(PackagerRequestIndex.$modalSetReturn.find('form'));
    },
    destroySetReturn: function () {
        PackagerRequestIndex.$btnSetReturn.removeAttr('disabled');
        PackagerRequestIndex.$btnSetReturn.removeAttr('data-loading');
        SetReturn.stop();
    },

    initSetApprove: function () {
        PackagerRequestIndex.$modalSetApprove.modal();
        PackagerRequestIndex.completeForm(PackagerRequestIndex.$modalSetApprove.find('form'));
    },
    destroySetApprove: function () {
        PackagerRequestIndex.$btnSetApprove.removeAttr('disabled');
        PackagerRequestIndex.$btnSetApprove.removeAttr('data-loading');
        SetReturn.stop();
    },

    initChangeDelivery: function () {
        PackagerRequestIndex.$modalChangeDelivery.modal();
        PackagerRequestIndex.completeForm(PackagerRequestIndex.$modalChangeDelivery.find('form'));
    },
    destroyChangeDelivery: function () {
        PackagerRequestIndex.$btnChangeDelivery.removeAttr('disabled');
        PackagerRequestIndex.$btnChangeDelivery.removeAttr('data-loading');
        ChangeDelivery.stop();
    },

    initSenderInDelivery: function () {
        PackagerRequestIndex.$modalSenderInDelivery.modal();
        PackagerRequestIndex.completeForm(PackagerRequestIndex.$modalSenderInDelivery.find('form'));
    },
    destroySenderInDelivery: function () {
        PackagerRequestIndex.$btnSenderInDelivery.removeAttr('disabled');
        PackagerRequestIndex.$btnSenderInDelivery.removeAttr('data-loading');
        SenderInDelivery.stop();
    },

    initGetManifest: function () {
        PackagerRequestIndex.$modalGetManifest.modal();
        PackagerRequestIndex.completeForm(PackagerRequestIndex.$modalGetManifest.find('form'));
    },
    destroyGetManifest: function () {
        PackagerRequestIndex.$btnGetManifest.removeAttr('disabled');
        PackagerRequestIndex.$btnGetManifest.removeAttr('data-loading');
        GetManifest.stop();
    },

    completeForm: function ($form, hideWarning) {
        $form.find('input[name="id[]"]').remove();

        var anySelect = 0;
        $(PackagerRequestIndex.rootSelector + ' input[name="id[]"]:checked').each(function () {
            $(this).clone().attr('type', 'hidden').val($(this).val()).appendTo($form);
            anySelect = 1;
        });

        if (hideWarning && anySelect === 1) {
            $('#all_label_printing_warning').hide();
        }
    },

    toggleCheckboxes: function () {
        $(PackagerRequestIndex.rootSelector + ' .td-custom-checkbox input[type="checkbox"]').prop('checked', $(this).prop('checked')).trigger('change');
    },

    toggleCheckbox: function () {
        var $tr = $(this).closest('tr');

        if ($(this).prop('checked')) {
            $tr.addClass('active');
        } else {
            $tr.removeClass('active');
        }
    }
};


