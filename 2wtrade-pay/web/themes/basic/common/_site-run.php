<script type="text/javascript">
    // @todo: Перенести в отдельный js-файл
    (function (document, window, $) {
        'use strict';
        var Site = window.Site;
        $(document).ready(function () {
            Site.run();
            window.ATL_JQ_PAGE_PROPS = {
                "triggerFunction": function(showCollectorDialog) {
                    jQuery("#sendErrorToJira").click(function(e) {
                        e.preventDefault();
                        showCollectorDialog();
                    });
                },
                fieldValues: {
                    description: window.location.href + '\n',
                    components: '10209',
                    email: '<?= (isset(Yii::$app->user->identity->email) ? Yii::$app->user->identity->email : '') ?>'
                }
            };
            jQuery.ajax({
                url: "<?= Yii::$app->params['jiraIssueCollector']['url'] ?>&collectorId=<?= (isset(Yii::$app->params['jiraIssueCollector']['collectorId'][Yii::$app->language]) ? Yii::$app->params['jiraIssueCollector']['collectorId'][Yii::$app->language] : Yii::$app->params['jiraIssueCollector']['collectorId']['en-US']) ?>",
                type: "get",
                cache: true,
                dataType: "script"
            });
        });
    })(document, window, jQuery);
</script>
