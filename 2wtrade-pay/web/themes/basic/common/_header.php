<?php
use app\helpers\WbIcon;
use app\widgets\navbar\Clock;
use app\widgets\navbar\Notification;
use app\widgets\navbar\CountrySwitcher;
use app\widgets\navbar\LanguageSwitcher;
use app\widgets\navbar\RunningLine;
use app\widgets\navbar\Profile;
use app\widgets\navbar\Efficiency;
use yii\helpers\Url;
?>

<nav class="site-navbar navbar navbar-default navbar-fixed-top navbar-mega" role="navigation">
    <div class="navbar-header">
        <button type="button"
                class="navbar-toggle hamburger hamburger-close navbar-toggle-left hided"
                data-toggle="menubar">
            <span class="sr-only">Toggle navigation</span>
            <span class="hamburger-bar"></span>
        </button>
        <button type="button" class="navbar-toggle collapsed" data-target="#site-navbar-collapse"
                data-toggle="collapse">
            <i class="icon wb-more-horizontal" aria-hidden="true"></i>
        </button>
        <div class="navbar-brand navbar-brand-center site-gridmenu-toggle" data-toggle="gridmenu">
            <a href="<?= Url::toRoute('/home/index'); ?>">
                <img class="navbar-brand-logo" src="<?= $this->theme->getUrl('/images/logo.png'); ?>">
                <span class="navbar-brand-text"><?= Yii::$app->params['companyName'] ?></span>
            </a>
        </div>
    </div>
    <div class="navbar-container container-fluid">
        <div class="collapse navbar-collapse navbar-collapse-toolbar" id="site-navbar-collapse">

            <ul class="nav navbar-toolbar">
                <li id="toggleMenubar" class="hidden-float">
                    <a role="button" href="#" data-toggle="menubar">
                        <i class="icon hamburger hamburger-arrow-left hided unfolded">
                            <span class="sr-only">Toggle menubar</span>
                            <span class="hamburger-bar"></span>
                        </i>
                    </a>
                </li>
            </ul>

            <ul class="nav navbar-toolbar navbar-right navbar-toolbar-right">

                <?= LanguageSwitcher::widget() ?>

                <?= Profile::widget([
                    'user' => Yii::$app->user,
                    'links' => [
                        [
                            'url' => Url::toRoute(['/profile/view']),
                            'label' => Yii::t('common', 'Мой профиль'),
                            'icon' => WbIcon::USER
                        ],
                        [
                            'url' => Url::toRoute('/auth/logout'),
                            'label' => Yii::t('common', 'Выйти'),
                            'icon' => WbIcon::POWER
                        ]
                    ],
                ]) ?>

                <?php if (Yii::$app->user->can('ticket.ticket.index') && !empty(Yii::$app->user->country->slug)) : ?>

                    <li>
                        <a class="navbar-ticket" href="<?= Url::toRoute(['/ticket/ticket/index']) ?>">
                            <span>
                                <i class="icon <?= WbIcon::HELP_CIRCLE ?>" aria-hidden="true"></i>
                            </span>
                        </a>
                    </li>
                <?php endif; ?>

                <?= Notification::widget() ?>

                <?= Clock::widget(); ?>
            </ul>

            <?= CountrySwitcher::widget() ?>

            <div class="nav navbar-toolbar navbar-right sendErrorToJira">
                <div class="jira-btn btn btn-primary" id="sendErrorToJira">
                    <div class="square">
                        <div class="sphere"></div>
                    </div>
                    <span>&nbsp;<?= yii::t("common", "Нашли ошибку?"); ?></span>
                </div>
            </div>

            <div class="nav navbar-toolbar hidden-sm col-sm-2 col-md-3 col-lg-4 col-xl-6">
                <?= RunningLine::widget([
                    'paddingTop' => '24',
                    'paddingTopType' => 'px',
                    'marginLeft' => '24',
                    'marginLeftType' => 'px',
                    'fontWeight' => 'bold',
                    'text' => Yii::$app->user->getRunningLineNotifications(),
                ]);
                ?>
            </div>

        </div>
    </div>
</nav>
