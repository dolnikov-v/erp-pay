<?php
use app\components\grid\GridViewExtention as GridView;
use app\widgets\navbar\Efficiency;
use app\modules\reportoperational\models\TeamCountry;
?>

<?php if($dataProvider = Efficiency::$cachedDataProvider) : ?>
<div class="table-responsive">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'options' => ['class' => 'text-nowrap'],
        'showPageSummary' => true,
        'positionPageSummary' => 'top',
        'pageSummaryContainer' => ['class' => 'page-summary text-right'],
        'layout' => "{items}",
        'columns' => [
            [
                'attribute' => 'team_name',
                'label' => ' ',
                'group' => true,
                'groupedRow' => true,
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'groupFooter' => function () {
                    $response = [
                        'content' => [
                            3 => GridView::F_AVG,
                            4 => GridView::F_AVG,
                            5 => GridView::F_AVG,
                        ],
                        'contentFormats' => [
                            3 => ['format' => 'number', 'decimals' => 2, 'decPoint'=>',', 'thousandSep'=>' '],
                            4 => ['format' => 'number', 'decimals' => 2, 'decPoint'=>',', 'thousandSep'=>' '],
                            5 => ['format' => 'number', 'decimals' => 2, 'decPoint'=>',', 'thousandSep'=>' '],
                        ],
                        'contentOptions' => [
                            3 => ['class' => 'text-right'],
                            4 => ['class' => 'text-right'],
                            5 => ['class' => 'text-right'],

                        ],
                        'options' => ['class' => 'warning', 'style' => 'font-weight:bold;'],
                    ];
                    return $response;
                }
            ],
            [
                'attribute' => 'country_name',
                'label' => Yii::t('common', 'Страна')
            ],
            [
                'attribute' => 'percent_approve',
                'format'=>['decimal', 2],
                'label' => Yii::t('common', '% Аппрува'),
                'contentOptions' => ['class' => 'text-right'],
                'pageSummary' => true,
                'pageSummaryFunc' => GridView::F_AVG,
                'pageSummaryOptions' => ['class' => 'warning', 'style' => 'font-weight:bold;'],
            ],
            [
                'attribute' => 'avg_check_approve',
                'format'=>['decimal', 2],
                'label' => Yii::t('common', 'Средний чек, $'),
                'contentOptions' => ['class' => 'text-right'],
                'pageSummary' => true,
                'pageSummaryFunc' => GridView::F_AVG,
                'pageSummaryOptions' => ['class' => 'warning', 'style' => 'font-weight:bold;'],
                'value' => function ($model) {
                    return (TeamCountry::$currencies[$model['country_id']] ? $model['avg_check_approve'] / TeamCountry::$currencies[$model['country_id']] : 0);
                }
            ],
            [
                'attribute' => 'efficiency_check_all',
                'format'=>['decimal', 2],
                'label' => Yii::t('common', 'Выполнение чек-листа, %'),
                'contentOptions' => ['class' => 'text-right'],
                'pageSummary' => true,
                'pageSummaryFunc' => GridView::F_AVG,
                'pageSummaryOptions' => ['class' => 'warning', 'style' => 'font-weight:bold;'],
                'value' => function ($model) {
                    return ($model['efficiency_check_all'] ? $model['efficiency_check_ok'] / $model['efficiency_check_all'] * 100 : 0);
                }
            ],
        ]
    ]) ?>
</div>
<?php endif; ?>