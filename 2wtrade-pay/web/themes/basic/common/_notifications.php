<?php
$notifications = Yii::$app->notifier->getNotifications();
?>

<?php if (!empty($notifications) && is_array($notifications)) { ?>
    <?php foreach ($notifications as $notification) { ?>
        <script type="application/javascript">
            var $notify = $.notify(
                {
                    message: "<?= htmlspecialchars($notification['message']) ?>"
                }, {
                    type: "<?= $notification['type'] ?>" + " dark",
                    animate: {
                        exit: 'hide'
                    },
                    z_index: 2031
                }
            );
        </script>
    <?php } ?>
<?php } ?>
