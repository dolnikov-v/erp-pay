<?php
use yii\helpers\Url;
use app\widgets\Modal;

?>
<footer class="site-footer">
    <div class="site-footer-legal">
        © <?= date('Y') ?> <a href="<?= Url::toRoute('/') ?>"><?= Yii::$app->params['companyName'] ?></a>
    </div>
</footer>
