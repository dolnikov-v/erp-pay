<?php

use app\helpers\FontAwesome;
use app\helpers\GlyphIcon;
use app\helpers\WbIcon;

$controllerId = Yii::$app->controller->id;
$moduleId = Yii::$app->controller->module->id;
$country = !Yii::$app->user->country->isNewRecord;
$actionId = Yii::$app->controller->action->id;
?>

<div class="site-menubar">
    <div class="site-menubar-body">
        <div>
            <div>
                <?= \app\widgets\Menu::widget(
                    [
                        'options' => ['class' => 'site-menu'],
                        'items' => [
                            [
                                'label' => Yii::t('common', 'Главное меню'),
                                'visible' => Yii::$app->user->can('home.index.index'),
                                'options' => ['class' => 'site-menu-category']
                            ],
                            [
                                'label' => Yii::t('common', 'Домашняя страница'),
                                'visible' => Yii::$app->user->can('home.index.index'),
                                'icon' => WbIcon::HOME,
                                'url' => ['/home/index'],
                                'active' => $moduleId == 'home',
                            ],
                            [
                                'label' => Yii::t('common', 'Менеджмент'),
                                'options' => ['class' => 'site-menu-category'],
                                'visible' => (
                                    Yii::$app->user->can('checklist.check.index') ||
                                    Yii::$app->user->can('checklist.call.index') ||
                                    Yii::$app->user->can('checklist.auditor.index') ||
                                    Yii::$app->user->can('checklist.tm.index') ||
                                    Yii::$app->user->can('report.approvebycountry.index')
                                ),
                            ],
                            [
                                'label' => Yii::t('common', 'Чек-лист'),
                                'visible' => (
                                    Yii::$app->user->can('checklist.check.index') ||
                                    Yii::$app->user->can('checklist.call.index') ||
                                    Yii::$app->user->can('checklist.auditor.index') ||
                                    Yii::$app->user->can('checklist.tm.index')
                                ),
                                'icon' => WbIcon::CHECK_MINI,
                                'options' => ['class' => 'site-menu-item has-sub'],
                                'url' => 'javascript:void(0);',
                                'active' => $moduleId == 'checklist' && (in_array($controllerId, ['check','call', 'auditor', 'tm'])),
                                'items' => [
                                    [
                                        'label' => Yii::t('common', 'Зам руководителя группы стран'),
                                        'visible' => Yii::$app->user->can('checklist.check.index'),
                                        'url' => ['/checklist/check/index'],
                                        'active' => $moduleId == 'checklist' && $controllerId == 'check',
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Супервайзера КЦ'),
                                        'visible' => Yii::$app->user->can('checklist.call.index'),
                                        'url' => ['/checklist/call/index'],
                                        'active' => $moduleId == 'checklist' && $controllerId == 'call',
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Аудитора КЦ'),
                                        'visible' => Yii::$app->user->can('checklist.auditor.index'),
                                        'url' => ['/checklist/auditor/index'],
                                        'active' => $moduleId == 'checklist' && $controllerId == 'auditor',
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Руководителя группы стран'),
                                        'visible' => Yii::$app->user->can('checklist.tm.index'),
                                        'url' => ['/checklist/tm/index'],
                                        'active' => $moduleId == 'checklist' && $controllerId == 'tm',
                                    ],
                                ]
                            ],
                            [
                                'label' => Yii::t('common', 'Листы эффективности'),
                                'visible' => Yii::$app->user->can('#'),
                                'url' => ['/#'],
                                'icon' => WbIcon::LIBRARY,
                            ],
                            [
                                'label' => Yii::t('common', 'Выполнение стандартов'),
                                'visible' => Yii::$app->user->can('report.companystandards.index'),
                                'url' => ['/report/company-standards/index'],
                                'icon' => FontAwesome::CHART_AREA,
                            ],
                            [
                                'label' => Yii::t('common', 'Аналитика'),
                                'options' => ['class' => 'site-menu-category'],
                                'visible' => (
                                    Yii::$app->user->can('finance.reportprofitabilityanalysis.index') ||
                                    Yii::$app->user->can('report.buyoutbycountry.index') ||
                                    Yii::$app->user->can('report.approvebycountry.index') ||
                                    Yii::$app->user->can('report.averagecheckbycountry.index') ||
                                    Yii::$app->user->can('report.leadbycountry.index') ||
                                    Yii::$app->user->can('report.consolidatesale.index') ||
                                    Yii::$app->user->can('report.team.index') ||
                                    Yii::$app->user->can('report.topproduct.index')
                                ),
                            ],
                            [
                                'label' => Yii::t('common', 'Операционный баланс'),
                                'visible' => Yii::$app->user->can('finance.reportprofitabilityanalysis.index'),
                                'active' => $moduleId == 'finance' && ($controllerId == 'report-profitability-analysis'),
                                'url' => ['/finance/report-profitability-analysis/index'],
                                'icon' => FontAwesome::MONEY,
                            ],
                            [
                                'label' => Yii::t('common', 'Аналитика по качеству Лидов'),
                                'visible' => Yii::$app->user->can('#'),
                                'url' => ['/#'],
                                'active' => $moduleId == 'report' && ($controllerId == 'approved'),
                                'icon' => WbIcon::USER_ADD,
                            ],
                            [
                                'label' => Yii::t('common', 'Аналитика по Гео'),
                                'visible' => (
                                        Yii::$app->user->can('report.buyoutbycountry.index') ||
                                        Yii::$app->user->can('report.approvebycountry.index') ||
                                        Yii::$app->user->can('report.averagecheckbycountry.index') ||
                                        Yii::$app->user->can('report.leadbycountry.index')
                                ),
                                'url' => 'javascript:void(0);',
                                'active' => $moduleId == 'report' && (in_array($controllerId, ['buyout-by-country', 'approve-by-country', 'average-check-by-country', 'lead-by-country'])),
                                'icon' => WbIcon::MAP,
                                'items' => [
                                    [
                                        'label' => Yii::t('common', 'Выкуп по группам стран'),
                                        'visible' => Yii::$app->user->can('report.buyoutbycountry.index'),
                                        'url' => ['/report/buyout-by-country/index'],
                                        'active' => $moduleId == 'report' && ($controllerId == 'buyout-by-country'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Апрув по группам стран'),
                                        'visible' => Yii::$app->user->can('report.approvebycountry.index'),
                                        'url' => ['/report/approve-by-country/index'],
                                        'active' => $moduleId == 'report' && ($controllerId == 'approve-by-country'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Средний чек по группам стран'),
                                        'visible' => Yii::$app->user->can('report.averagecheckbycountry.index'),
                                        'url' => ['/report/average-check-by-country/index'],
                                        'active' => $moduleId == 'report' && ($controllerId == 'average-check-by-country'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Лиды по группам стран'),
                                        'visible' => Yii::$app->user->can('report.leadbycountry.index'),
                                        'url' => ['/report/lead-by-country/index'],
                                        'active' => $moduleId == 'report' && ($controllerId == 'lead-by-country'),
                                    ],

                                ]
                            ],

                            [
                                'label' => Yii::t('common', 'Аналитика по эффективности'),
                                'visible' => (
                                    Yii::$app->user->can('report.team.index')
                                ),
                                'active' => $moduleId == 'report' && $controllerId == 'team',
                                'url' => 'javascript:void(0);',
                                'options' => ['class' => 'site-menu-item has-sub'],
                                'icon' => WbIcon::BOOK,
                                'items' => [
                                    [
                                        'label' => Yii::t('common', 'Эффектность работы операционистов'),
                                        'visible' => Yii::$app->user->can('report.team.index'),
                                        'url' => ['/report/team/index'],
                                        'active' => $moduleId == 'report' && ($controllerId == 'team'),
                                    ],
                                ]
                            ],
                            [
                                'label' => Yii::t('common', 'Результаты тестирования новых товаров'),
                                'visible' => Yii::$app->user->can('#'),
                                'url' => ['/#'],
                                'active' => $moduleId == 'report' && ($controllerId == 'top-product'),
                                'icon' => WbIcon::PIE_CHART,
                            ],
                            [
                                'label' => Yii::t('common', 'Финансы'),
                                'options' => ['class' => 'site-menu-category'],
                                'visible' => (
                                    Yii::$app->user->can('report.salary.index') ||
                                    Yii::$app->user->can('finance.reportexpensesitem.index') ||
                                    Yii::$app->user->can('finance.reportexpensescountryitem.index') ||
                                    Yii::$app->user->can('finance.reportexpensescountry.index') ||
                                    Yii::$app->user->can('finance.reportexpensescategory.index') ||
                                    Yii::$app->user->can('finance.reportexpenses.index') ||
                                    Yii::$app->user->can('report.finance.index') ||
                                    Yii::$app->user->can('report.finance.index') ||
                                    Yii::$app->user->can('report.debts.indextwo') ||
                                    Yii::$app->user->can('report.debts.index') ||
                                    Yii::$app->user->can('report.invoice.index') ||
                                    Yii::$app->user->can('report.deliverypayment.index')
                                ),
                            ],
                            [
                                'label' => Yii::t('common', 'Расчет ЗП'),
                                'visible' => Yii::$app->user->can('report.salary.index'),
                                'url' => ['/report/salary/index'],
                                'active' => $moduleId == 'report' && ($controllerId == 'salary'),
                                'icon' => FontAwesome::MONEY,
                            ],
                            [
                                'label' => Yii::t('common', 'Справочники'),
                                'visible' => (
                                    Yii::$app->user->can('finance.reportexpensesitem.index') ||
                                    Yii::$app->user->can('finance.reportexpensescountryitem.index') ||
                                    Yii::$app->user->can('finance.reportexpensescountry.index') ||
                                    Yii::$app->user->can('finance.reportexpensescategory.index')
                                ),
                                'active' => $moduleId == 'finance' && in_array($controllerId, [
                                        'report-expenses-item',
                                        'report-expenses-country-item',
                                        'report-expenses-country',
                                        'report-expenses-category'
                                    ]),
                                'url' => 'javascript:void(0);',
                                'options' => ['class' => 'site-menu-item has-sub'],
                                'icon' => WbIcon::BOOK,
                                'items' => [
                                    [
                                        'label' => Yii::t('common', 'Статьи расхода'),
                                        'visible' => Yii::$app->user->can('finance.reportexpensesitem.index'),
                                        'active' => $moduleId == 'finance' && ($controllerId == 'report-expenses-item'),
                                        'url' => ['/finance/report-expenses-item/index'],
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Статьи расхода по стране'),
                                        'visible' => Yii::$app->user->can('finance.reportexpensescountryitem.index'),
                                        'active' => $moduleId == 'finance' && ($controllerId == 'report-expenses-country-item'),
                                        'url' => ['/finance/report-expenses-country-item/index'],
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Страны'),
                                        'visible' => Yii::$app->user->can('finance.reportexpensescountry.index'),
                                        'active' => $moduleId == 'finance' && ($controllerId == 'report-expenses-country'),
                                        'url' => ['/finance/report-expenses-country/index'],
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Категории'),
                                        'visible' => Yii::$app->user->can('finance.reportexpensescategory.index'),
                                        'active' => $moduleId == 'finance' && ($controllerId == 'report-expenses-category'),
                                        'url' => ['/finance/report-expenses-category/index'],
                                    ],
                                ]
                            ],
                            [
                                'label' => Yii::t('common', 'Анализ расходов'),
                                'visible' => Yii::$app->user->can('finance.reportexpenses.index'),
                                'active' => $moduleId == 'finance' && ($controllerId == 'report-expenses'),
                                'url' => ['/finance/report-expenses/index'],
                                'icon' => WbIcon::PIE_CHART,
                            ],
                            [
                                'label' => Yii::t('common', 'Финансовый отчет'),
                                'visible' => Yii::$app->user->can('report.finance.index'),
                                'url' => ['/report/finance/index'],
                                'active' => $moduleId == 'report' && $controllerId == 'finance',
                                'icon' => WbIcon::PLUSE,
                            ],
                            [
                                'label' => Yii::t('common', 'Взаиморасчеты с КС (по дате передачи в КС)'),
                                'visible' => Yii::$app->user->can('report.debts.indextwo'),
                                'active' => $moduleId == 'report' && ($controllerId == 'debts') && $actionId == 'index-two',
                                'url' => ['/report/debts/index-two'],
                                'icon' => WbIcon::LOOP,
                            ],
                            [
                                'label' => Yii::t('common', 'Дебиторская задолжность'),
                                'visible' => Yii::$app->user->can('report.debts.index'),
                                'active' => $moduleId == 'report' && ($controllerId == 'debts') && $actionId == 'index',
                                'url' => ['/report/debts/index'],
                                'icon' => WbIcon::GRID_4,
                            ],
                            [
                                'label' => Yii::t('common', 'Динамика ДЗ'),
                                'visible' => Yii::$app->user->can('report.debts.dynamic'),
                                'active' => $moduleId == 'report' && ($controllerId == 'debts') && $actionId == 'dynamic',
                                'url' => ['/report/debts/dynamic'],
                                'icon' => FontAwesome::BAR_CHART,
                            ],
                            [
                                'label' => Yii::t('common', 'Инвойсы'),
                                'visible' => Yii::$app->user->can('report.invoice.index'),
                                'active' => $moduleId == 'report' && ($controllerId == 'invoice') && $actionId == 'index',
                                'url' => ['/report/invoice/index'],
                                'icon' => WbIcon::COPY,
                            ],
                            [
                                'label' => Yii::t('common', 'Поступления ДС'),
                                'visible' => Yii::$app->user->can('report.deliverypayment.index'),
                                'url' => ['/report/delivery-payment/index'],
                                'active' => $moduleId == 'report' && ($controllerId == 'delivery-payment'),
                                'icon' => WbIcon::PAYMENT,
                            ],
                            [
                                'label' => Yii::t('common', 'Планирование продаж'),
                                'options' => ['class' => 'site-menu-category'],
                                'visible' => (
                                    Yii::$app->user->can('rating.ratingproduct.index') ||
                                    Yii::$app->user->can('rating.ratingbuyoutcheck.index') ||
                                    Yii::$app->user->can('report.forecastlead.index') ||
                                    Yii::$app->user->can('report.trends.lead')||
                                    Yii::$app->user->can('report.trends.approve')||
                                    Yii::$app->user->can('report.trends.buyout')
                                ),
                            ],
                            [
                                'label' => Yii::t('common', 'Рейтинг товаров по выкупу'),
                                'visible' => Yii::$app->user->can('rating.ratingproduct.index'),
                                'url' => ['/rating/rating-product/index'],
                                'active' => $moduleId == 'rating' && ($controllerId == 'rating-product'),
                                'icon' => WbIcon::PAYMENT,
                            ],
                            [
                                'label' => Yii::t('common', 'Рейтинг товаров по вык. чеку'),
                                'visible' => Yii::$app->user->can('rating.ratingbuyoutcheck.index'),
                                'url' => ['/rating/rating-buyout-check/index'],
                                'active' => $moduleId == 'rating' && ($controllerId == 'rating-buyout-check'),
                                'icon' => WbIcon::ORDER,
                            ],
                            [
                                'label' => Yii::t('common', 'Прогнозирование лидов'),
                                'visible' => Yii::$app->user->can('report.forecastlead.index'),
                                'url' => ['/report/forecast-lead/index'],
                                'active' => $moduleId == 'report' && ($controllerId == 'forecast-lead'),
                                'icon' => WbIcon::GRAPH_UP,
                            ],


                            [
                                'label' => Yii::t('common', 'Тренды'),
                                'icon' => WbIcon::PIE_CHART,
                                'options' => ['class' => 'site-menu-item has-sub'],
                                'visible' => (
                                    Yii::$app->user->can('report.trends.lead')||
                                    Yii::$app->user->can('report.trends.approve')||
                                    Yii::$app->user->can('report.trends.buyout')
                                ),
                                'active' => $moduleId == 'report' && $controllerId == 'trends',
                                'url' => 'javascript:void(0);',
                                'items' => [
                                    [
                                        'label' => Yii::t('common', 'По лидам'),
                                        'visible' => Yii::$app->user->can('report.trends.lead'),
                                        'url' => ['/report/trends/lead'],
                                        'active' => $controllerId == 'trends' && $actionId == 'lead',
                                    ],
                                    [
                                        'label' => Yii::t('common', 'По апруву'),
                                        'visible' => Yii::$app->user->can('report.trends.approve'),
                                        'url' => ['/report/trends/approve'],
                                        'active' =>  $controllerId == 'trends' && $actionId == 'approve',
                                    ],
                                    [
                                        'label' => Yii::t('common', 'По выкупу'),
                                        'visible' => Yii::$app->user->can('report.trends.buyout'),
                                        'url' => ['/report/trends/buyout'],
                                        'active' => $controllerId == 'trends' && $actionId == 'buyout',
                                    ],
                                ]
                            ],
                            [
                                'label' => Yii::t('common', 'Управление продажами'),
                                'options' => ['class' => 'site-menu-category'],
                                'visible' => (
                                    Yii::$app->user->can('order.index.index') ||
                                    Yii::$app->user->can('order.list.index') ||
                                    Yii::$app->user->can('order.notificationrequest.index') ||
                                    Yii::$app->user->can('order.notification.index') ||
                                    Yii::$app->user->can('report.team.index') ||
                                    Yii::$app->user->can('finance.reportprofitabilityanalysis.index') ||
                                    Yii::$app->user->can('report.status.index') ||
                                    Yii::$app->user->can('report.delivery.index') ||
                                    Yii::$app->user->can('report.unshippingreasons.index') ||
                                    Yii::$app->user->can('report.callcenter.index') ||
                                    Yii::$app->user->can('report.reportworkflow.index') ||
                                    Yii::$app->user->can('storage.balance.index') ||
                                    Yii::$app->user->can('report.webmaster.index') ||
                                    Yii::$app->user->can('report.webmaster.analytics') ||
                                    Yii::$app->user->can('report.callcenteroperatorsonline.index') ||
                                    Yii::$app->user->can('report.approved.index') ||
                                    Yii::$app->user->can('report.reportstandartdelivery.index') ||
                                    Yii::$app->user->can('report.callcentercheckrequest.index') ||
                                    Yii::$app->user->can('reportoperational.buyouts.index') ||
                                    Yii::$app->user->can('reportoperational.shippings.index') ||
                                    Yii::$app->user->can('storage.add.index') ||
                                    Yii::$app->user->can('storage.control.index') ||
                                    Yii::$app->user->can('storage.balance.index') ||
                                    Yii::$app->user->can('storage.document.index') ||
                                    Yii::$app->user->can('storage.move.index') ||
                                    Yii::$app->user->can('storage.correction.index') ||
                                    Yii::$app->user->can('storage.purchase.index') ||
                                    Yii::$app->user->can('report.team.index') ||
                                    Yii::$app->user->can('callcenter.request.index') ||
                                    Yii::$app->user->can('callcenter.checkingrequest.index') ||
                                    Yii::$app->user->can('callcenter.control.index') ||
                                    Yii::$app->user->can('callcenter.user.index') ||
                                    Yii::$app->user->can('delivery.request.index') ||
                                    Yii::$app->user->can('delivery.control.index') ||
                                    Yii::$app->user->can('delivery.apiclass.index') ||
                                    Yii::$app->user->can('delivery.chargescalculator.index') ||
                                    Yii::$app->user->can('delivery.test.index') ||
                                    Yii::$app->user->can('report.chargestest.index') ||
                                    Yii::$app->user->can('packager.request.index') ||
                                    Yii::$app->user->can('packager.control.index') ||
                                    Yii::$app->user->can('marketplace.index.index') ||
                                    Yii::$app->user->can('catalog.country.index') ||
                                    Yii::$app->user->can('catalog.currencyrate.index') ||
                                    Yii::$app->user->can('catalog.product.index') ||
                                    Yii::$app->user->can('catalog.productcategory.index') ||
                                    Yii::$app->user->can('catalog.certificate.index') ||
                                    Yii::$app->user->can('catalog.smspollquestions.index') ||
                                    Yii::$app->user->can('catalog.landing.index') ||
                                    Yii::$app->user->can('catalog.autotrash.index') ||
                                    Yii::$app->user->can('catalog.operatorspenalty.index') ||
                                    Yii::$app->user->can('catalog.unbuyoutreason.index') ||
                                    Yii::$app->user->can('catalog.requisitedelivery.index') ||
                                    Yii::$app->user->can('order.status.index') ||
                                    Yii::$app->user->can('order.financefact.index') ||
                                    Yii::$app->user->can('deliveryreport.report.index')
                                ),
                            ],
                            [
                                'label' => Yii::t('common', 'Заказы'),
                                'icon' => FontAwesome::SHOPPING_CART,
                                'options' => ['class' => 'site-menu-item has-sub'],
                                'visible' => (
                                    Yii::$app->user->can('order.index.index')||
                                    Yii::$app->user->can('order.list.index')),
                                'active' => $moduleId == 'order' && ($controllerId == 'index' || $controllerId == 'list'),
                                'url' => 'javascript:void(0);',
                                'items' => [
                                    [
                                        'label' => Yii::t('common', 'Список заказов'),
                                        'visible' => Yii::$app->user->can('order.index.index'),
                                        'url' => ['/order/index/index'],
                                        'active' => $moduleId == 'order' && ($controllerId == 'index'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Список листов'),
                                        'visible' => Yii::$app->user->can('order.list.index'),
                                        'url' => ['/order/list/index'],
                                        'active' => $moduleId == 'order' && ($controllerId == 'list'),
                                    ],
                                ]
                            ],
                            [
                                'label' => Yii::t('common', 'Уведомление клиентов'),
                                'icon' => FontAwesome::LIST,
                                'options' => ['class' => 'site-menu-item has-sub'],
                                'visible' => Yii::$app->user->can('order.notification.index'),
                                'active' => $moduleId == 'order' && ($controllerId == 'notification-request' || $controllerId == 'notification'),
                                'url' => 'javascript:void(0);',
                                'items' => [
                                    [
                                        'label' => Yii::t('common', 'Уведомления'),
                                        'visible' => Yii::$app->user->can('order.notificationrequest.index'),
                                        'url' => ['/order/notification-request/index'],
                                        'active' => $moduleId == 'order' && ($controllerId == 'notification-request'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Шаблоны'),
                                        'visible' => Yii::$app->user->can('order.notification.index'),
                                        'url' => ['/order/notification/index'],
                                        'active' => $moduleId == 'order' && ($controllerId == 'notification'),
                                    ],
                                ]
                            ],
                            [
                                'label' => Yii::t('common', 'Отчеты'),
                                'icon' => WbIcon::PIE_CHART,
                                'options' => ['class' => 'site-menu-item has-sub'],
                                'visible' => (
                                    Yii::$app->user->can('report.team.index') ||
                                    Yii::$app->user->can('finance.reportprofitabilityanalysis.index') ||
                                    Yii::$app->user->can('report.status.index') ||
                                    Yii::$app->user->can('report.delivery.index') ||
                                    Yii::$app->user->can('report.unshippingreasons.index') ||
                                    Yii::$app->user->can('report.callcenter.index') ||
                                    Yii::$app->user->can('report.reportworkflow.index') ||
                                    Yii::$app->user->can('storage.balance.index') ||
                                    Yii::$app->user->can('report.webmaster.index') ||
                                    Yii::$app->user->can('report.webmaster.analytics') ||
                                    Yii::$app->user->can('report.callcenteroperatorsonline.index') ||
                                    Yii::$app->user->can('report.approved.index') ||
                                    Yii::$app->user->can('report.reportstandartdelivery.index') ||
                                    Yii::$app->user->can('report.callcentercheckrequest.index') ||
                                    Yii::$app->user->can('reportoperational.settingscountry.index') ||
                                    Yii::$app->user->can('reportoperational.settings.index') ||
                                    Yii::$app->user->can('reportoperational.buyouts.index') ||
                                    Yii::$app->user->can('reportoperational.shippings.index')
                                ) && $country,
                                'active' => in_array($moduleId, ['finance', 'reportoperational','report']) && !in_array($controllerId, ['salary', 'team', 'profitability-analysis', 'time-sheet', 'debts', 'debts-invoice', 'invoice', 'finance', 'charges-test', 'delivery-payment', 'trends', 'forecast-lead']),
                                'url' => 'javascript:void(0);',
                                'items' => [
                                    [
                                        'label' => Yii::t('common', 'Операционный баланс'),
                                        'visible' => Yii::$app->user->can('finance.reportprofitabilityanalysis.index'),
                                        'active' => $moduleId == 'finance' && ($controllerId == 'report-profitability-analysis'),
                                        'url' => ['/finance/report-profitability-analysis/index'],
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Статистика по операторам КЦ'),
                                        'visible' => Yii::$app->user->can('report.team.index'),
                                        'active' => $moduleId == 'report' && ($controllerId == 'team'),
                                        'url' => ['/report/team/index'],
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Полный'),
                                        'visible' => Yii::$app->user->can('report.status.index'),
                                        'url' => ['/report/status/index'],
                                        'active' => $moduleId == 'report' && ($controllerId == 'status'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Доставка'),
                                        'visible' => Yii::$app->user->can('report.delivery.index'),
                                        'url' => ['/report/delivery/index'],
                                        'active' => $moduleId == 'report' && ($controllerId == 'delivery'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Анализ невыкупов'),
                                        'visible' => Yii::$app->user->can('report.unshippingreasons.index'),
                                        'url' => ['/report/unshipping-reasons/index'],
                                        'active' => $moduleId == 'report' && ($controllerId == 'unshipping-reasons'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Колл-центры'),
                                        'visible' => Yii::$app->user->can('report.callcenter.index'),
                                        'url' => ['/report/call-center/index'],
                                        'active' => $moduleId == 'report' && ($controllerId == 'call-center'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Отчет по workflow'),
                                        'visible' => Yii::$app->user->can('report.reportworkflow.index'),
                                        'active' => $moduleId == 'report' && ($controllerId == 'report-workflow'),
                                        'url' => ['/report/report-workflow/index'],
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Складские остатки'),
                                        'visible' => Yii::$app->user->can('storage.balance.index'),
                                        'url' => ['/storage/balance/index'],
                                        'active' => $moduleId == 'storage' && ($controllerId == 'balance'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Рейтинг вебмастеров'),
                                        'visible' => Yii::$app->user->can('report.webmaster.index'),
                                        'active' => $moduleId == 'report' && $controllerId == 'webmaster' && $actionId == 'index',
                                        'url' => ['/report/webmaster/index'],
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Аналитика вебмастеров'),
                                        'visible' => Yii::$app->user->can('report.webmaster.analytics'),
                                        'active' => $moduleId == 'report' && $controllerId == 'webmaster' && $actionId == 'analytics',
                                        'url' => ['/report/webmaster/analytics'],
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Операторы КЦ онлайн'),
                                        'visible' => Yii::$app->user->can('report.callcenteroperatorsonline.index'),
                                        'active' => $moduleId == 'report' && ($controllerId == 'call-center-operators-online'),
                                        'url' => ['/report/call-center-operators-online/index']
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Новые лиды и одобренные по часам'),
                                        'visible' => Yii::$app->user->can('report.approved.index'),
                                        'url' => ['/report/approved/index'],
                                        'active' => $moduleId == 'report' && ($controllerId == 'approved'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Сроки доставки'),
                                        'visible' => Yii::$app->user->can('report.reportstandartdelivery.index'),
                                        'url' => ['/report/report-standart-delivery/index']
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Проверка Курьерских служб'),
                                        'visible' => Yii::$app->user->can('report.callcentercheckrequest.index'),
                                        'url' => ['/report/call-center-check-request/index']
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Операционные отчеты'),
                                        'visible' => (
                                            Yii::$app->user->can('reportoperational.settingscountry.index') ||
                                            Yii::$app->user->can('reportoperational.settingsteam.index') ||
                                            Yii::$app->user->can('reportoperational.buyouts.index') ||
                                            Yii::$app->user->can('reportoperational.shippings.index')
                                        ),
                                        'icon' => WbIcon::INBOX,
                                        'options' => ['class' => 'site-menu-item has-sub', 'style' => 'left: 28px;'],
                                        'url' => 'javascript:void(0);',
                                        'active' => $moduleId == 'reportoperational',
                                        'items' => [
                                            [
                                                'label' => Yii::t('common', 'Настройки'),
                                                'visible' => (
                                                    Yii::$app->user->can('reportoperational.settingscountry.index') ||
                                                    Yii::$app->user->can('reportoperational.settingsteam.index')
                                                ),
                                                'icon' => WbIcon::SETTINGS,
                                                'options' => ['class' => 'site-menu-item has-sub'],
                                                'url' => 'javascript:void(0);',
                                                'active' => $moduleId == 'reportoperational' && (in_array($controllerId, ['settings-country', 'settings-team'])),
                                                'items' => [
                                                    [
                                                        'label' => Yii::t('common', 'Страны'),
                                                        'visible' => Yii::$app->user->can('reportoperational.settingscountry.index'),
                                                        'url' => ['/reportoperational/settings-country/index'],
                                                        'active' => $moduleId == 'reportoperational' && ($controllerId == 'settings-country'),
                                                    ],
                                                    [
                                                        'label' => Yii::t('common', 'Регионы'),
                                                        'visible' => Yii::$app->user->can('reportoperational.settingsteam.index'),
                                                        'url' => ['/reportoperational/settings-team/index'],
                                                        'active' => $moduleId == 'reportoperational' && ($controllerId == 'settings-team'),
                                                    ],
                                                ]
                                            ],
                                            [
                                                'label' => Yii::t('common', 'По выкупам'),
                                                'visible' => Yii::$app->user->can('reportoperational.buyouts.index'),
                                                'url' => ['/reportoperational/buyouts/index'],
                                                'active' => $moduleId == 'reportoperational' && ($controllerId == 'buyouts'),
                                                'icon' => WbIcon::SHOPPING_CART,
                                            ],
                                            [
                                                'label' => Yii::t('common', 'По отгрузкам'),
                                                'visible' => Yii::$app->user->can('reportoperational.shippings.index'),
                                                'url' => ['/reportoperational/shippings/index'],
                                                'active' => $moduleId == 'reportoperational' && ($controllerId == 'shippings'),
                                                'icon' => FontAwesome::PAPER_PLANE_O,
                                            ],
                                        ],
                                    ],
                                ]
                            ],
                            [
                                'label' => Yii::t('common', 'Склады'),
                                'visible' => (Yii::$app->user->can('storage.balance.index')
                                        || Yii::$app->user->can('storage.control.index')
                                        || Yii::$app->user->can('storage.document.index')
                                        || Yii::$app->user->can('storage.move.index')
                                        || Yii::$app->user->can('storage.correction.index')
                                        || Yii::$app->user->can('storage.add.index')
                                        || Yii::$app->user->can('storage.purchase.index')
                                        || Yii::$app->user->can('storage.part.index')
                                    )
                                    && $country,
                                'icon' => FontAwesome::CLOUD,
                                'options' => ['class' => 'site-menu-item has-sub'],
                                'url' => 'javascript:void(0);',
                                'active' => $moduleId == 'storage',
                                'items' => [
                                    [
                                        'label' => Yii::t('common', 'Добавить товар'),
                                        'visible' => Yii::$app->user->can('storage.add.index'),
                                        'url' => ['/storage/add/index'],
                                        'active' => $moduleId == 'storage' && ($controllerId == 'add'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Управление'),
                                        'visible' => Yii::$app->user->can('storage.control.index'),
                                        'url' => ['/storage/control/index'],
                                        'active' => $moduleId == 'storage' && ($controllerId == 'control' || $controllerId == 'products'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Складские партии'),
                                        'visible' => Yii::$app->user->can('storage.part.index'),
                                        'url' => ['/storage/part/index'],
                                        'active' => $moduleId == 'storage' && $controllerId == 'part',
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Складские остатки'),
                                        'visible' => Yii::$app->user->can('storage.balance.index'),
                                        'url' => ['/storage/balance/index'],
                                        'active' => $moduleId == 'storage' && ($controllerId == 'balance'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Движения'),
                                        'visible' => Yii::$app->user->can('storage.document.index'),
                                        'url' => ['/storage/document/index'],
                                        'active' => $moduleId == 'storage' && ($controllerId == 'document'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Переместить'),
                                        'visible' => Yii::$app->user->can('storage.move.index'),
                                        'url' => ['/storage/move/index'],
                                        'active' => $moduleId == 'storage' && ($controllerId == 'move'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Корректировка'),
                                        'visible' => Yii::$app->user->can('storage.correction.index'),
                                        'url' => ['/storage/correction/index'],
                                        'active' => $moduleId == 'storage' && ($controllerId == 'correction'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Закупки'),
                                        'visible' => Yii::$app->user->can('storage.purchase.index'),
                                        'url' => ['/storage/purchase/index'],
                                        'active' => $moduleId == 'storage' && ($controllerId == 'purchase'),
                                    ],
                                ],
                            ],
                            [
                                'label' => Yii::t('common', 'Зарплата'),
                                'visible' => (
                                        Yii::$app->user->can('salary.penalty.index')
                                        || Yii::$app->user->can('salary.penaltytype.index')
                                        || Yii::$app->user->can('salary.person.index')
                                        || Yii::$app->user->can('salary.personimport.index')
                                        || Yii::$app->user->can('salary.designation.index')
                                        || Yii::$app->user->can('salary.bonustype.index')
                                        || Yii::$app->user->can('salary.bonus.index')
                                        || Yii::$app->user->can('salary.personimport.index')
                                        || Yii::$app->user->can('salary.office.index')
                                        || Yii::$app->user->can('salary.holiday.index')
                                        || Yii::$app->user->can('salary.monthlystatement.index')
                                        || Yii::$app->user->can('salary.worktimeplan.index')
                                        || Yii::$app->user->can('report.salary.index')
                                        || Yii::$app->user->can('report.team.index')
                                        || Yii::$app->user->can('report.timesheet.index')
                                    ) && $country,
                                'icon' => FontAwesome::MONEY,
                                'options' => ['class' => 'site-menu-item has-sub'],
                                'url' => 'javascript:void(0);',
                                'active' => $moduleId == 'salary' || ($moduleId == 'report' && ($controllerId == 'time-sheet')),
                                'items' => [
                                    [
                                        'label' => Yii::t('common', 'Офисы'),
                                        'visible' => Yii::$app->user->can('salary.office.index'),
                                        'url' => ['/salary/office/index'],
                                        'active' => $moduleId == 'salary' &&
                                            ($controllerId == 'office' ||
                                                ($controllerId == 'working-shift' && $actionId != 'index') ||
                                                ($controllerId == 'office-tax' && $actionId != 'index') ||
                                                ($controllerId == 'staffing' && $actionId != 'index')),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Рабочие смены'),
                                        'visible' => Yii::$app->user->can('salary.workingshift.index'),
                                        'url' => ['/salary/working-shift/index'],
                                        'active' => $moduleId == 'salary' && ($controllerId == 'working-shift' && $actionId == 'index'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Штатное расписание'),
                                        'visible' => Yii::$app->user->can('salary.staffing.index'),
                                        'url' => ['/salary/staffing/index'],
                                        'active' => $moduleId == 'salary' && ($controllerId == 'staffing'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Сотрудники'),
                                        'visible' => Yii::$app->user->can('salary.person.index'),
                                        'url' => ['/salary/person/index'],
                                        'active' => $moduleId == 'salary' && ($controllerId == 'person' || $controllerId == 'person-import'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Должности'),
                                        'visible' => Yii::$app->user->can('salary.designation.index'),
                                        'url' => ['/salary/designation/index'],
                                        'active' => $moduleId == 'salary' && $controllerId == 'designation',
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Типы штрафов'),
                                        'visible' => Yii::$app->user->can('salary.penaltytype.index'),
                                        'url' => ['/salary/penalty-type/index'],
                                        'active' => $moduleId == 'salary' && $controllerId == 'penalty-type',
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Типы бонусов'),
                                        'visible' => Yii::$app->user->can('salary.bonustype.index'),
                                        'url' => ['/salary/bonus-type/index'],
                                        'active' => $moduleId == 'salary' && $controllerId == 'bonus-type',
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Штрафы'),
                                        'visible' => Yii::$app->user->can('salary.penalty.index'),
                                        'url' => ['/salary/penalty/index'],
                                        'active' => $moduleId == 'salary' && $controllerId == 'penalty',
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Бонусы'),
                                        'visible' => Yii::$app->user->can('salary.bonus.index'),
                                        'url' => ['/salary/bonus/index'],
                                        'active' => $moduleId == 'salary' && $controllerId == 'bonus',
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Праздники'),
                                        'visible' => Yii::$app->user->can('salary.holiday.index'),
                                        'url' => ['/salary/holiday/index'],
                                        'active' => $moduleId == 'salary' && $controllerId == 'holiday',
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Графики'),
                                        'visible' => Yii::$app->user->can('salary.monthlystatement.index'),
                                        'url' => ['/salary/monthly-statement/index'],
                                        'active' => $moduleId == 'salary' && ($controllerId == 'monthly-statement' || $controllerId == 'monthly-statement-data'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Рабочий план'),
                                        'visible' => Yii::$app->user->can('salary.worktimeplan.index'),
                                        'url' => ['/salary/work-time-plan/index'],
                                        'active' => $moduleId == 'salary' && $controllerId == 'work-time-plan',
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Табель'),
                                        'visible' => Yii::$app->user->can('report.timesheet.index'),
                                        'url' => ['/report/time-sheet/index'],
                                        'active' => $moduleId == 'report' && ($controllerId == 'time-sheet'),
                                    ],
                                ]
                            ],
                            [
                                'label' => Yii::t('common', 'Колл-центры'),
                                'visible' => (
                                        Yii::$app->user->can('report.team.index') ||
                                        Yii::$app->user->can('callcenter.request.index') ||
                                        Yii::$app->user->can('callcenter.checkingrequest.index') ||
                                        Yii::$app->user->can('callcenter.control.index') ||
                                        Yii::$app->user->can('callcenter.user.index')
                                    ) && $country,
                                'icon' => GlyphIcon::EARPHONE,
                                'options' => ['class' => 'site-menu-item has-sub'],
                                'url' => 'javascript:void(0);',
                                'active' => $moduleId == 'c' || ($moduleId == 'report' && $controllerId == 'team'),
                                'items' => [
                                    [
                                        'label' => Yii::t('common', 'Статистика по операторам КЦ'),
                                        'visible' => Yii::$app->user->can('report.team.index'),
                                        'active' => $moduleId == 'report' && ($controllerId == 'team'),
                                        'url' => ['/report/team/index'],
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Заявки'),
                                        'visible' => Yii::$app->user->can('callcenter.request.index'),
                                        'url' => ['/call-center/request/index'],
                                        'active' => $moduleId == 'call-center' && $controllerId == 'request',
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Заявки на проверку'),
                                        'visible' => Yii::$app->user->can('callcenter.checkingrequest.index'),
                                        'url' => ['/call-center/checking-request/index'],
                                        'active' => $moduleId == 'call-center' && $controllerId == 'checking-request',
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Управление'),
                                        'visible' => Yii::$app->user->can('callcenter.control.index'),
                                        'url' => ['/call-center/control/index'],
                                        'active' => $moduleId == 'call-center' && $controllerId == 'control',
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Пользователи'),
                                        'visible' => Yii::$app->user->can('callcenter.user.index'),
                                        'url' => ['/call-center/user/index'],
                                        'active' => $moduleId == 'call-center' && $controllerId == 'user',
                                    ],
                                ],
                            ],
                            [
                                'label' => Yii::t('common', 'Службы доставки'),
                                'visible' => (
                                    (Yii::$app->user->can('delivery.control.index') && $country) ||
                                    Yii::$app->user->can('delivery.request.index') ||
                                    Yii::$app->user->can('delivery.apiclass.index') ||
                                    Yii::$app->user->can('delivery.chargescalculator.index') ||
                                    Yii::$app->user->can('delivery.test.index') ||
                                    Yii::$app->user->can('report.chargestest.index')
                                ),
                                'icon' => FontAwesome::PAPER_PLANE_O,
                                'options' => ['class' => 'site-menu-item has-sub'],
                                'url' => 'javascript:void(0);',
                                'active' => $moduleId == 'delivery' || ($moduleId == 'report' && $controllerId == 'charges-test'),
                                'items' => [
                                    [
                                        'label' => Yii::t('common', 'Заявки'),
                                        'visible' => Yii::$app->user->can('delivery.request.index'),
                                        'url' => ['/delivery/request/index'],
                                        'active' => $moduleId == 'delivery' && $controllerId == 'request',
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Управление'),
                                        'visible' => Yii::$app->user->can('delivery.control.index'),
                                        'url' => ['/delivery/control/index'],
                                        'active' => $moduleId == 'delivery' && $controllerId == 'control',
                                    ],
                                    [
                                        'label' => Yii::t('common', 'API-классы'),
                                        'visible' => Yii::$app->user->can('delivery.apiclass.index'),
                                        'url' => ['/delivery/api-class/index'],
                                        'active' => $moduleId == 'delivery' && $controllerId == 'api-class',
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Шаблоны расчета расходов на доставку'),
                                        'visible' => Yii::$app->user->can('delivery.chargescalculator.index'),
                                        'url' => ['/delivery/charges-calculator/index'],
                                        'active' => $moduleId == 'delivery' && $controllerId == 'charges-calculator',
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Тестирование апи курьерских служб'),
                                        'visible' => Yii::$app->user->can('delivery.test.index'),
                                        'url' => ['/delivery/test/index'],
                                        'active' => $moduleId == 'delivery' && $controllerId == 'test',
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Тестирование калькулятора доставки'),
                                        'visible' => Yii::$app->user->can('report.chargestest.index'),
                                        'url' => ['/report/charges-test/index'],
                                        'active' => $moduleId == 'report' && $controllerId == 'charges-test',
                                    ]
                                ],
                            ],
                            [
                                'label' => Yii::t('common', 'Службы упаковки'),
                                'visible' => (
                                        Yii::$app->user->can('packager.request.index') ||
                                        Yii::$app->user->can('packager.control.index')
                                    ) && $country,
                                'icon' => FontAwesome::CUBE,
                                'options' => ['class' => 'site-menu-item has-sub'],
                                'url' => 'javascript:void(0);',
                                'active' => $moduleId == 'packager',
                                'items' => [
                                    [
                                        'label' => Yii::t('common', 'Заявки'),
                                        'visible' => Yii::$app->user->can('packager.request.index'),
                                        'url' => ['/packager/request/index'],
                                        'active' => $moduleId == 'packager' && $controllerId == 'request',
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Управление'),
                                        'visible' => Yii::$app->user->can('packager.control.index'),
                                        'url' => ['/packager/control/index'],
                                        'active' => $moduleId == 'packager' && $controllerId == 'control',
                                    ],
                                ],
                            ],
                            [
                                'label' => Yii::t('common', 'Маркеты'),
                                'visible' => Yii::$app->user->can('marketplace.index.index'),
                                'icon' => FontAwesome::SHOPPING_CART,
                                'options' => ['class' => 'site-menu-item has-sub'],
                                'url' => 'javascript:void(0);',
                                'active' => $moduleId == 'marketplace',
                                'items' => [
                                    [
                                        'label' => Yii::t('common', 'Управление'),
                                        'visible' => Yii::$app->user->can('marketplace.index.index'),
                                        'url' => ['/marketplace/index/index'],
                                        'active' => $moduleId == 'marketplace' && ($controllerId == 'index' || $controllerId == 'view'),
                                    ],
                                ]
                            ],
                            [
                                'label' => Yii::t('common', 'Справочники'),
                                'visible' => (
                                    Yii::$app->user->can('catalog.country.index') ||
                                    Yii::$app->user->can('catalog.currencyrate.index') ||
                                    Yii::$app->user->can('catalog.product.index') ||
                                    Yii::$app->user->can('catalog.productcategory.index') ||
                                    Yii::$app->user->can('catalog.certificate.index') ||
                                    Yii::$app->user->can('catalog.smspollquestions.index') ||
                                    Yii::$app->user->can('catalog.landing.index') ||
                                    Yii::$app->user->can('catalog.autotrash.index') ||
                                    Yii::$app->user->can('catalog.operatorspenalty.index') ||
                                    Yii::$app->user->can('catalog.unbuyoutreason.index') ||
                                    Yii::$app->user->can('catalog.requisitedelivery.index') ||
                                    Yii::$app->user->can('order.status.index')
                                ),
                                'icon' => WbIcon::LIBRARY,
                                'options' => ['class' => 'site-menu-item has-sub'],
                                'url' => 'javascript:void(0);',
                                'active' => (in_array($moduleId, ['catalog','order'])) && (in_array($controllerId, ['country','currency-rate','product','product-category','certificate','sms-poll-questions','landing','autotrash','operators-penalty', 'un-buyout-reason','requisite-delivery', 'status'])),
                                'items' => [
                                    [
                                        'label' => Yii::t('common', 'Страны'),
                                        'visible' => Yii::$app->user->can('catalog.country.index'),
                                        'url' => ['/catalog/country/index'],
                                        'active' => $moduleId == 'catalog' && ($controllerId == 'country'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Курсы валют'),
                                        'visible' => Yii::$app->user->can('catalog.currencyrate.index'),
                                        'url' => ['/catalog/currency-rate/index'],
                                        'active' => $moduleId == 'catalog' && ($controllerId == 'currency-rate'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Товары'),
                                        'visible' => Yii::$app->user->can('catalog.product.index'),
                                        'url' => ['/catalog/product/index'],
                                        'active' => $moduleId == 'catalog' && ($controllerId == 'product' || $controllerId == 'product-translate' || $controllerId == 'product-price'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Категории товаров'),
                                        'visible' => Yii::$app->user->can('catalog.productcategory.index'),
                                        'url' => ['/catalog/product-category/index'],
                                        'active' => $moduleId == 'catalog' && ($controllerId == 'product-category'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Сертификаты'),
                                        'visible' => Yii::$app->user->can('catalog.certificate.index'),
                                        'url' => ['/catalog/certificate/index'],
                                        'active' => $moduleId == 'catalog' && ($controllerId == 'certificate'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'СМС опрос'),
                                        'visible' => Yii::$app->user->can('catalog.smspollquestions.index'),
                                        'url' => ['/catalog/sms-poll-questions/index'],
                                        'active' => $moduleId == 'catalog' && ($controllerId == 'sms-poll-questions'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Лендинги'),
                                        'visible' => Yii::$app->user->can('catalog.landing.index'),
                                        'url' => ['/catalog/landing/index'],
                                        'active' => $moduleId == 'catalog' && ($controllerId == 'landing'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Автотреш'),
                                        'visible' => Yii::$app->user->can('catalog.autotrash.index'),
                                        'url' => ['/catalog/autotrash/index'],
                                        'active' => $moduleId == 'catalog' && ($controllerId == 'autotrash'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Штрафы операторов'),
                                        'visible' => Yii::$app->user->can('catalog.operatorspenalty.index'),
                                        'url' => ['/catalog/operators-penalty/index'],
                                        'active' => $moduleId == 'catalog' && ($controllerId == 'operators-penalty'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Причины невыкупа'),
                                        'visible' => Yii::$app->user->can('catalog.unbuyoutreason.index'),
                                        'url' => ['/catalog/un-buyout-reason/index'],
                                        'active' => $moduleId == 'catalog' && $controllerId == 'un-buyout-reason',
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Реквизиты'),
                                        'visible' => Yii::$app->user->can('catalog.requisitedelivery.index'),
                                        'url' => ['/catalog/requisite-delivery/index'],
                                        'active' => $moduleId == 'catalog' && $controllerId == 'requisite-delivery',
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Статусы'),
                                        'visible' => Yii::$app->user->can('order.status.index'),
                                        'url' => ['/order/status/index'],
                                        'active' => $moduleId == 'order' && ($controllerId == 'status'),
                                    ],
                                ],
                            ],
                            [
                                'label' => Yii::t('common', 'Фактические расходы'),
                                'visible' => Yii::$app->user->can('order.financefact.index'),
                                'url' => ['/order/finance-fact/index'],
                                'active' => $moduleId == 'order' && ($controllerId == 'finance-fact'),
                                'icon' => WbIcon::STATS_BARS,
                            ],
                            [
                                'label' => Yii::t('common', 'Сверка'),
                                'visible' => Yii::$app->user->can('deliveryreport.report.index'),
                                'url' => ['/deliveryreport/report/index'],
                                'active' => $moduleId == 'deliveryreport',
                                'icon' => WbIcon::PAYMENT,
                            ],
                            [
                                'label' => Yii::t('common', 'Архив'),
                                'options' => ['class' => 'site-menu-category'],
                                'visible' => (
                                        Yii::$app->user->can('report.short.index') ||
                                        Yii::$app->user->can('report.deliveryterms.index') ||
                                        Yii::$app->user->can('report.deliveryzipcodesstats.index') ||
                                        Yii::$app->user->can('report.deliveryclosedperiods.index') ||
                                        Yii::$app->user->can('report.product.index') ||
                                        Yii::$app->user->can('report.topproduct.index') ||
                                        Yii::$app->user->can('report.inprogress.index') ||
                                        Yii::$app->user->can('report.topmanager.index') ||
                                        Yii::$app->user->can('report.advertising.index') ||
                                        Yii::$app->user->can('report.daily.index') ||
                                        Yii::$app->user->can('report.approved.index') ||
                                        Yii::$app->user->can('report.reportadcombo.index') ||
                                        Yii::$app->user->can('report.reportcallcenter.index') ||
                                        Yii::$app->user->can('report.reportcallcenter.index') ||
                                        Yii::$app->user->can('report.consolidatebycountry.index') ||
                                        Yii::$app->user->can('report.consolidatesale.index') ||
                                        Yii::$app->user->can('report.approvebycountry.index') ||
                                        Yii::$app->user->can('report.buyoutbycountry.index') ||
                                        Yii::$app->user->can('report.buyoutbycountry.index') ||
                                        Yii::$app->user->can('report.averagecheckbycountry.index') ||
                                        Yii::$app->user->can('report.leadbycountry.index') ||
                                        Yii::$app->user->can('report.forecastleadbyweekdays.index') ||
                                        Yii::$app->user->can('report.forecastworkloadoperatorsbyhours.index') ||
                                        Yii::$app->user->can('report.delivery.delaybycountry.index') ||
                                        Yii::$app->user->can('report.sale.index') ||
                                        Yii::$app->user->can('report.shipment.index') ||
                                        Yii::$app->user->can('report.foreignoperator.index') ||
                                        Yii::$app->user->can('report.operatorworkdays.index') ||
                                        Yii::$app->user->can('report.operatorspenalty.index') ||
                                        Yii::$app->user->can('report.reportstandartsales.index') ||
                                        Yii::$app->user->can('report.reportstandartdelivery.index') ||
                                        Yii::$app->user->can('report.accountingcosts.index') ||
                                        Yii::$app->user->can('report.activelanding.index') ||
                                        Yii::$app->user->can('report.validatefinance.index') ||
                                        Yii::$app->user->can('report.checkundelivery.index') ||
                                        Yii::$app->user->can('report.smspollhistory.index') ||
                                        Yii::$app->user->can('report.smsnotification.index') ||
                                        Yii::$app->user->can('report.forecastdelivery.index') ||
                                        Yii::$app->user->can('report.purchasestorage.index')
                                    ) && $country,
                            ],
                            [
                                'label' => Yii::t('common', 'Отчеты'),
                                'visible' => (
                                        Yii::$app->user->can('report.short.index') ||
                                        Yii::$app->user->can('report.deliveryterms.index') ||
                                        Yii::$app->user->can('report.deliveryzipcodesstats.index') ||
                                        Yii::$app->user->can('report.deliveryclosedperiods.index') ||
                                        Yii::$app->user->can('report.product.index') ||
                                        Yii::$app->user->can('report.topproduct.index') ||
                                        Yii::$app->user->can('report.inprogress.index') ||
                                        Yii::$app->user->can('report.topmanager.index') ||
                                        Yii::$app->user->can('report.advertising.index') ||
                                        Yii::$app->user->can('report.daily.index') ||
                                        Yii::$app->user->can('report.approved.index') ||
                                        Yii::$app->user->can('report.reportadcombo.index') ||
                                        Yii::$app->user->can('report.reportcallcenter.index') ||
                                        Yii::$app->user->can('report.reportcallcenter.index') ||
                                        Yii::$app->user->can('report.consolidatebycountry.index') ||
                                        Yii::$app->user->can('report.consolidatesale.index') ||
                                        Yii::$app->user->can('report.approvebycountry.index') ||
                                        Yii::$app->user->can('report.buyoutbycountry.index') ||
                                        Yii::$app->user->can('report.buyoutbycountry.index') ||
                                        Yii::$app->user->can('report.averagecheckbycountry.index') ||
                                        Yii::$app->user->can('report.leadbycountry.index') ||
                                        Yii::$app->user->can('report.forecastleadbyweekdays.index') ||
                                        Yii::$app->user->can('report.forecastworkloadoperatorsbyhours.index') ||
                                        Yii::$app->user->can('report.delivery.delaybycountry.index') ||
                                        Yii::$app->user->can('report.sale.index') ||
                                        Yii::$app->user->can('report.shipment.index') ||
                                        Yii::$app->user->can('report.foreignoperator.index') ||
                                        Yii::$app->user->can('report.operatorworkdays.index') ||
                                        Yii::$app->user->can('report.operatorspenalty.index') ||
                                        Yii::$app->user->can('report.reportstandartsales.index') ||
                                        Yii::$app->user->can('report.reportstandartdelivery.index') ||
                                        Yii::$app->user->can('report.accountingcosts.index') ||
                                        Yii::$app->user->can('report.activelanding.index') ||
                                        Yii::$app->user->can('report.validatefinance.index') ||
                                        Yii::$app->user->can('report.checkundelivery.index') ||
                                        Yii::$app->user->can('report.smspollhistory.index') ||
                                        Yii::$app->user->can('report.smsnotification.index') ||
                                        Yii::$app->user->can('report.forecastdelivery.index') ||
                                        Yii::$app->user->can('report.purchasestorage.index')
                                    ) && $country,
                                'icon' => WbIcon::PAPERCLIP,
                                'options' => ['class' => 'site-menu-item has-sub'],
                                'url' => 'javascript:void(0);',
                                'active' => $moduleId == 'report' && !in_array($controllerId, ['salary', 'team', 'profitability-analysis', 'time-sheet', 'debts', 'invoice', 'finance', 'trends', 'forecast-lead']),
                                'items' => [
                                    [
                                        'label' => Yii::t('common', 'Заказы'),
                                        'visible' => Yii::$app->user->can('report.short.index'),
                                        'url' => ['/report/short/index'],
                                        'active' => $moduleId == 'report' && ($controllerId == 'short'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Стандартные сроки доставки'),
                                        'visible' => Yii::$app->user->can('report.deliveryterms.index'),
                                        'url' => ['/report/delivery-terms/index'],
                                        'active' => $moduleId == 'report' && ($controllerId == 'delivery-terms'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Статистика по группам индексов доставки'),
                                        'visible' => Yii::$app->user->can('report.deliveryzipcodesstats.index'),
                                        'url' => ['/report/delivery-zipcodes-stats/index'],
                                        'active' => $moduleId == 'report' && ($controllerId == 'delivery-zipcodes-stats'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Закрытые периоды'),
                                        'visible' => Yii::$app->user->can('report.deliveryclosedperiods.index'),
                                        'url' => ['/report/delivery-closed-periods/index'],
                                        'active' => $moduleId == 'report' && ($controllerId == 'delivery-closed-periods'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Товары'),
                                        'visible' => Yii::$app->user->can('report.product.index'),
                                        'url' => ['/report/product/index'],
                                        'active' => $moduleId == 'report' && ($controllerId == 'product'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Топ товаров'),
                                        'visible' => Yii::$app->user->can('report.topproduct.index'),
                                        'url' => ['/report/top-product/index'],
                                        'active' => $moduleId == 'report' && ($controllerId == 'top-product'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Заказы "В процессе"'),
                                        'visible' => Yii::$app->user->can('report.inprogress.index'),
                                        'url' => ['/report/in-progress/index'],
                                        'active' => $moduleId == 'report' && ($controllerId == 'in-progress'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Топ менеджеров'),
                                        'visible' => Yii::$app->user->can('report.topmanager.index'),
                                        'url' => ['/report/top-manager/index'],
                                        'active' => $moduleId == 'report' && ($controllerId == 'top-manager'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Затраты на рекламу'),
                                        'visible' => Yii::$app->user->can('report.advertising.index'),
                                        'url' => ['/report/advertising/index'],
                                        'active' => $moduleId == 'report' && ($controllerId == 'advertising'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Ежедневная сверка заказов'),
                                        'visible' => Yii::$app->user->can('report.daily.index'),
                                        'url' => ['/report/daily/index'],
                                        'active' => $moduleId == 'report' && ($controllerId == 'daily'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Новые лиды и одобренные по часам'),
                                        'visible' => Yii::$app->user->can('report.approved.index'),
                                        'url' => ['/report/approved/index'],
                                        'active' => $moduleId == 'report' && ($controllerId == 'approved'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Сверка с AdCombo'),
                                        'visible' => Yii::$app->user->can('report.reportadcombo.index'),
                                        'active' => $moduleId == 'report' && ($controllerId == 'report-adcombo'),
                                        'url' => ['/report/report-adcombo/index'],
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Сверка с КЦ'),
                                        'visible' => Yii::$app->user->can('report.reportcallcenter.index'),
                                        'active' => $moduleId == 'report' && ($controllerId == 'report-call-center'),
                                        'url' => ['/report/report-call-center/index']
                                    ],
                                    [
                                        'label' => Yii::t('common',
                                            'КЦ: задержка обновления статусов по группам стран'),
                                        'visible' => Yii::$app->user->can('report.reportcallcenter.index'),
                                        'active' => $moduleId == 'report' && ($controllerId == 'call-center-delay-by-country'),
                                        'url' => ['/report/call-center-delay-by-country/index']
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Сводный отчет по стране'),
                                        'visible' => Yii::$app->user->can('report.consolidatebycountry.index'),
                                        'active' => $moduleId == 'report' && ($controllerId == 'consolidate-by-country'),
                                        'url' => ['/report/consolidate-by-country/index']
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Сводный отчет по продажам'),
                                        'visible' => Yii::$app->user->can('report.consolidatesale.index'),
                                        'active' => $moduleId == 'report' && ($controllerId == 'consolidate-sale'),
                                        'url' => ['/report/consolidate-sale/index']
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Апрув по группам стран'),
                                        'visible' => Yii::$app->user->can('report.approvebycountry.index'),
                                        'active' => $moduleId == 'report' && ($controllerId == 'approve-by-country'),
                                        'url' => ['/report/approve-by-country/index']
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Выкуп по группам стран'),
                                        'visible' => Yii::$app->user->can('report.buyoutbycountry.index'),
                                        'active' => $moduleId == 'report' && ($controllerId == 'buyout-by-country'),
                                        'url' => ['/report/buyout-by-country/index']
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Выкуп и апрув по странам'),
                                        'visible' => Yii::$app->user->can('report.buyoutbycountry.index'),
                                        'active' => $moduleId == 'report' && ($controllerId == 'buyout-and-approve-by-country'),
                                        'url' => ['/report/buyout-and-approve-by-country/index']
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Средний чек по группам стран'),
                                        'visible' => Yii::$app->user->can('report.averagecheckbycountry.index'),
                                        'active' => $moduleId == 'report' && ($controllerId == 'average-check-by-country'),
                                        'url' => ['/report/average-check-by-country/index']
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Лиды по группам стран'),
                                        'visible' => Yii::$app->user->can('report.leadbycountry.index'),
                                        'active' => $moduleId == 'report' && ($controllerId == 'lead-by-country'),
                                        'url' => ['/report/lead-by-country/index']
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Прогноз лидов на неделю'),
                                        'visible' => Yii::$app->user->can('report.forecastleadbyweekdays.index'),
                                        'active' => $moduleId == 'report' && ($controllerId == 'forecast-lead-by-week-days'),
                                        'url' => ['/report/forecast-lead-by-week-days/index']
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Прогноз загруженности операторов'),
                                        'visible' => Yii::$app->user->can('report.forecastworkloadoperatorsbyhours.index'),
                                        'active' => $moduleId == 'report' && ($controllerId == 'forecast-workload-operators-by-hours'),
                                        'url' => ['/report/forecast-workload-operators-by-hours/index']
                                    ],
                                    [
                                        'label' => Yii::t('common',
                                            'КС: задержка обновления статусов по группам стран'),
                                        'visible' => Yii::$app->user->can('report.delivery.delaybycountry.index'),
                                        'active' => $moduleId == 'report' && ($controllerId == 'delivery-delay-by-country'),
                                        'url' => ['/report/delivery-delay-by-country/index']
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Отчет по продажам'),
                                        'visible' => Yii::$app->user->can('report.sale.index'),
                                        'active' => $moduleId == 'report' && ($controllerId == 'sale'),
                                        'url' => ['/report/sale/index'],
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Отчет по отгрузкам'),
                                        'visible' => Yii::$app->user->can('report.shipment.index'),
                                        'active' => $moduleId == 'report' && ($controllerId == 'shipment'),
                                        'url' => ['/report/shipment/index'],
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Выкуп по операторам'),
                                        'visible' => Yii::$app->user->can('report.foreignoperator.index'),
                                        'active' => $moduleId == 'report' && ($controllerId == 'foreign-operator'),
                                        'url' => ['/report/foreign-operator/index'],
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Рабочие дни операторов'),
                                        'visible' => Yii::$app->user->can('report.operatorworkdays.index'),
                                        'active' => $moduleId == 'report' && ($controllerId == 'operator-workdays'),
                                        'url' => ['/report/operator-workdays/index'],
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Штрафы операторов'),
                                        'visible' => Yii::$app->user->can('report.operatorspenalty.index'),
                                        'active' => $moduleId == 'report' && ($controllerId == 'operators-penalty'),
                                        'url' => ['/report/operators-penalty/index'],
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Эталонные продажи'),
                                        'visible' => Yii::$app->user->can('report.reportstandartsales.index'),
                                        'url' => ['/report/report-standart-sales/index']
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Эталонные доставки'),
                                        'visible' => Yii::$app->user->can('report.reportstandartdelivery.index'),
                                        'url' => ['/report/report-standart-delivery/index']
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Расходы из 1С'),
                                        'visible' => Yii::$app->user->can('report.accountingcosts.index'),
                                        'active' => $moduleId == 'report' && ($controllerId == 'accounting-costs'),
                                        'url' => ['/report/accounting-costs/index'],
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Активные лендинги'),
                                        'visible' => Yii::$app->user->can('report.activelanding.index'),
                                        'active' => $moduleId == 'report' && ($controllerId == 'active-landing'),
                                        'url' => ['/report/active-landing/index'],
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Сверка финансового отчета'),
                                        'visible' => Yii::$app->user->can('report.validatefinance.index'),
                                        'active' => $moduleId == 'report' && ($controllerId == 'validate-finance'),
                                        'url' => ['/report/validate-finance/index'],
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Результаты не выкупа'),
                                        'visible' => Yii::$app->user->can('report.checkundelivery.index'),
                                        'active' => $moduleId == 'report' && ($controllerId == 'check-undelivery'),
                                        'url' => ['/report/check-undelivery/index'],
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Отчёт по смс опросам'),
                                        'visible' => Yii::$app->user->can('report.smspollhistory.index'),
                                        'active' => $moduleId == 'report' && ($controllerId == 'sms-poll-history'),
                                        'url' => ['/report/sms-poll-history/index'],
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Отчёт по смс/email уведомлениям'),
                                        'visible' => Yii::$app->user->can('report.smsnotification.index'),
                                        'active' => $moduleId == 'report' && ($controllerId == 'smsnotification'),
                                        'url' => ['/report/sms-notification/index'],
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Прогнозирование доставки'),
                                        'visible' => Yii::$app->user->can('report.forecastdelivery.index'),
                                        'active' => $moduleId == 'report' && ($controllerId == 'forecast-delivery'),
                                        'url' => ['/report/forecast-delivery/index'],
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Прогнозирование закупок'),
                                        'visible' => Yii::$app->user->can('report.purchasestorage.index'),
                                        'active' => $moduleId == 'report' && ($controllerId == 'purchase-storage'),
                                        'url' => ['/report/purchase-storage/index'],
                                    ],
                                ],
                            ],
                            [
                                'label' => Yii::t('common', 'Администрирование'),
                                'options' => ['class' => 'site-menu-category'],
                                'visible' => (
                                    Yii::$app->user->can('administration.system.index') ||
                                    Yii::$app->user->can('access.user.index') ||
                                    Yii::$app->user->can('access.role.index') ||
                                    Yii::$app->user->can('access.permission.index') ||
                                    Yii::$app->user->can('access.ticket.index') ||
                                    Yii::$app->user->can('access.apiuser.index') ||
                                    Yii::$app->user->can('widget.type.index') ||
                                    Yii::$app->user->can('i18n.language.index') ||
                                    Yii::$app->user->can('i18n.phrase.index') ||
                                    Yii::$app->user->can('i18n.translation.index') ||
                                    Yii::$app->user->can('notification.chat.index') ||
                                    Yii::$app->user->can('notification.chatgroup.index') ||
                                    Yii::$app->user->can('bar.create.index') ||
                                    Yii::$app->user->can('bar.initiate.index') ||
                                    Yii::$app->user->can('bar.pack.index') ||
                                    Yii::$app->user->can('bar.return.index') ||
                                    Yii::$app->user->can('administration.system.index') ||
                                    Yii::$app->user->can('logger.browser.index') ||
                                    Yii::$app->user->can('logger.sqlerror.index') ||
                                    Yii::$app->user->can('administration.crontab.control.index') ||
                                    Yii::$app->user->can('administration.crontab.logs.index') ||
                                    Yii::$app->user->can('catalog.source.index') ||
                                    Yii::$app->user->can('catalog.externalsource.index') ||
                                    Yii::$app->user->can('catalog.externalsourcerole.index') ||
                                    Yii::$app->user->can('catalog.partner.index') ||
                                    Yii::$app->user->can('catalog.timezone.index') ||
                                    Yii::$app->user->can('catalog.currency.index') ||
                                    Yii::$app->user->can('catalog.language.index') ||
                                    Yii::$app->user->can('catalog.notification.index') ||
                                    Yii::$app->user->can('checklist.admin.index') ||
                                    Yii::$app->user->can('checklist.admincategory.index') ||
                                    Yii::$app->user->can('order.workflow.index')
                                ),
                            ],
                            [
                                'label' => Yii::t('common', 'Справочники'),
                                'visible' => (
                                    Yii::$app->user->can('catalog.source.index') ||
                                    Yii::$app->user->can('catalog.externalsource.index') ||
                                    Yii::$app->user->can('catalog.externalsourcerole.index') ||
                                    Yii::$app->user->can('catalog.partner.index') ||
                                    Yii::$app->user->can('catalog.timezone.index') ||
                                    Yii::$app->user->can('catalog.currency.index') ||
                                    Yii::$app->user->can('catalog.language.index') ||
                                    Yii::$app->user->can('catalog.notification.index') ||
                                    Yii::$app->user->can('checklist.admin.index') ||
                                    Yii::$app->user->can('checklist.admincategory.index') ||
                                    Yii::$app->user->can('order.workflow.index')
                                ),
                                'icon' => WbIcon::LIBRARY,
                                'options' => ['class' => 'site-menu-item has-sub'],
                                'url' => 'javascript:void(0);',
                                'active' => $moduleId == 'catalog' && (in_array($controllerId, ['source','external-source','external-source-role','partner','timezone','currency','language','notification','admin'])) ||
                                    ($moduleId == 'checklist' && (in_array($controllerId, ['admin','admin-category']))) ||
                                    ($moduleId == 'order' && $controllerId == 'workflow'),
                                'items' => [
                                    [
                                        'label' => Yii::t('common', 'Источники'),
                                        'visible' => Yii::$app->user->can('catalog.source.index'),
                                        'url' => ['/catalog/source/index'],
                                        'active' => $moduleId == 'catalog' && ($controllerId == 'source'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Сторонние источники'),
                                        'visible' => Yii::$app->user->can('catalog.externalsource.index'),
                                        'url' => ['/catalog/external-source/index'],
                                        'active' => $moduleId == 'catalog' && ($controllerId == 'external-source'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Роли сторонних источников'),
                                        'visible' => Yii::$app->user->can('catalog.externalsourcerole.index'),
                                        'url' => ['/catalog/external-source-role/index'],
                                        'active' => $moduleId == 'catalog' && ($controllerId == 'external-source-role'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Партнеры'),
                                        'visible' => Yii::$app->user->can('catalog.partner.index'),
                                        'url' => ['/catalog/partner/index'],
                                        'active' => $moduleId == 'catalog' && ($controllerId == 'partner'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Временные зоны'),
                                        'visible' => Yii::$app->user->can('catalog.timezone.index'),
                                        'url' => ['/catalog/timezone/index'],
                                        'active' => $moduleId == 'catalog' && ($controllerId == 'timezone'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Валюты'),
                                        'visible' => Yii::$app->user->can('catalog.currency.index'),
                                        'url' => ['/catalog/currency/index'],
                                        'active' => $moduleId == 'catalog' && ($controllerId == 'currency'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Список языков'),
                                        'visible' => Yii::$app->user->can('catalog.language.index'),
                                        'url' => ['/catalog/language/index'],
                                        'active' => $moduleId == 'catalog' && ($controllerId == 'language'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Оповещения'),
                                        'visible' => Yii::$app->user->can('catalog.notification.index'),
                                        'url' => ['/catalog/notification/index'],
                                        'active' => $moduleId == 'catalog' && ($controllerId == 'notification'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Чек-лист'),
                                        'icon' => FontAwesome::LIST,
                                        'options' => ['class' => 'site-menu-item has-sub', 'style' => 'left: 28px;'],
                                        'visible' => (
                                            Yii::$app->user->can('checklist.admin.index') ||
                                            Yii::$app->user->can('checklist.admincategory.index')
                                        ),
                                        'active' => $moduleId == 'checklist' && (in_array($controllerId, ['admin','admin-category'])),
                                        'url' => 'javascript:void(0);',
                                        'items' => [
                                            [
                                                'label' => Yii::t('common', 'Категории'),
                                                'visible' => Yii::$app->user->can('checklist.admincategory.index'),
                                                'url' => ['/checklist/admin-category/index'],
                                                'active' => $moduleId == 'checklist' && $controllerId == 'admin-category',
                                            ],
                                            [
                                                'label' => Yii::t('common', 'Пункты'),
                                                'visible' => Yii::$app->user->can('checklist.admin.index'),
                                                'url' => ['/checklist/admin/index'],
                                                'active' => $moduleId == 'checklist' && $controllerId == 'admin',
                                            ],
                                        ]
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Workflows'),
                                        'visible' => Yii::$app->user->can('order.workflow.index'),
                                        'url' => ['/order/workflow/index'],
                                        'active' => $moduleId == 'order' && ($controllerId == 'workflow'),
                                    ],
                                ],
                            ],
                            [
                                'label' => Yii::t('common', 'Доступы'),
                                'visible' => (
                                    Yii::$app->user->can('access.user.index') ||
                                    Yii::$app->user->can('access.role.index') ||
                                    Yii::$app->user->can('access.permission.index') ||
                                    Yii::$app->user->can('access.ticket.index') ||
                                    Yii::$app->user->can('access.apiuser.index') ||
                                    Yii::$app->user->can('widget.type.index')
                                ),
                                'icon' => WbIcon::USER,
                                'options' => ['class' => 'site-menu-item has-sub'],
                                'url' => 'javascript:void(0);',
                                'active' => $moduleId == 'access' || $moduleId == 'widget',
                                'items' => [
                                    [
                                        'label' => Yii::t('common', 'Список пользователей'),
                                        'visible' => Yii::$app->user->can('access.user.index'),
                                        'url' => ['/access/user/index'],
                                        'active' => $moduleId == 'access' && ($controllerId == 'user'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Список ролей'),
                                        'visible' => Yii::$app->user->can('access.role.index'),
                                        'url' => ['/access/role/index'],
                                        'active' => $moduleId == 'access' && ($controllerId == 'role'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Список операций'),
                                        'visible' => Yii::$app->user->can('access.permission.index'),
                                        'url' => ['/access/permission/index'],
                                        'active' => $moduleId == 'access' && ($controllerId == 'permission'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Список тикетных групп'),
                                        'visible' => Yii::$app->user->can('access.ticket.index'),
                                        'url' => ['/access/ticket/index'],
                                        'active' => $moduleId == 'access' && ($controllerId == 'ticket'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Пользователи API'),
                                        'visible' => Yii::$app->user->can('access.apiuser.index'),
                                        'url' => ['/access/api-user/index'],
                                        'active' => $moduleId == 'access' && ($controllerId == 'apiuser'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Список виджетов'),
                                        'visible' => Yii::$app->user->can('widget.type.index'),
                                        'url' => ['/widget/type/index'],
                                        'active' => $moduleId == 'widget' && ($controllerId == 'type'),
                                    ]
                                ],
                            ],
                            [
                                'label' => Yii::t('common', 'Интернационализация'),
                                'visible' => (
                                    Yii::$app->user->can('i18n.language.index') ||
                                    Yii::$app->user->can('i18n.phrase.index') ||
                                    Yii::$app->user->can('i18n.translation.index')
                                ),
                                'icon' => FontAwesome::LANGUAGE,
                                'options' => ['class' => 'site-menu-item has-sub'],
                                'url' => 'javascript:void(0);',
                                'active' => $moduleId == 'i18n',
                                'items' => [
                                    [
                                        'label' => Yii::t('common', 'Список языков'),
                                        'visible' => Yii::$app->user->can('i18n.language.index'),
                                        'url' => ['/i18n/language/index'],
                                        'active' => $moduleId == 'i18n' && ($controllerId == 'language'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Список фраз'),
                                        'visible' => Yii::$app->user->can('i18n.phrase.index'),
                                        'url' => ['/i18n/phrase/index'],
                                        'active' => $moduleId == 'i18n' && ($controllerId == 'phrase'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Список переводов'),
                                        'visible' => Yii::$app->user->can('i18n.translation.index'),
                                        'url' => ['/i18n/translation/index'],
                                        'active' => $moduleId == 'i18n' && ($controllerId == 'translation'),
                                    ],
                                ],
                            ],
                            [
                                'label' => Yii::t('common', 'Уведомления'),
                                'icon' => WbIcon::CHAT,
                                'options' => ['class' => 'site-menu-item has-sub'],
                                'url' => 'javascript:void(0);',
                                'active' => $moduleId == 'notification',
                                'visible' => (
                                    Yii::$app->user->can('notification.chat.index') ||
                                    Yii::$app->user->can('notification.chatgroup.index')
                                )
                                ,
                                'items' =>[
                                    [
                                        'label' => Yii::t('common', 'Чаты'),
                                        'visible' => Yii::$app->user->can('notification.chat.index'),
                                        'url' => ['/notification/chat/index'],
                                        'active' => $moduleId == 'notification' && ($controllerId == 'chat'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Группы чатов'),
                                        'visible' => Yii::$app->user->can('notification.chat-group.index'),
                                        'url' => ['/notification/chat-group/index'],
                                        'active' => $moduleId == 'notification' && ($controllerId == 'chat-group'),
                                    ],
                                ]
                            ],
                            [
                                'label' => Yii::t('common', 'Штрих кодирование'),
                                'visible' =>
                                    (
                                        Yii::$app->user->can('bar.create.index') ||
                                        Yii::$app->user->can('bar.initiate.index') ||
                                        Yii::$app->user->can('bar.pack.index') ||
                                        Yii::$app->user->can('bar.return.index')
                                    ),
                                'icon' => FontAwesome::BAR_CODE,
                                'url' => 'javascript:void(0);',
                                'active' => $moduleId == 'bar' && ($controllerId == 'create' || $controllerId == 'initiate'
                                        || $controllerId == 'pack' || $controllerId == 'return'),
                                'options' => ['class' => 'site-menu-item has-sub'],
                                'items' => [
                                    [
                                        'label' => Yii::t('common', 'Создание'),
                                        'visible' => Yii::$app->user->can('bar.create.index'),
                                        'url' => ['/bar/create/index'],
                                        'active' => $moduleId == 'bar' && ($controllerId == 'create'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Инициация'),
                                        'visible' => Yii::$app->user->can('bar.initiate.index'),
                                        'url' => ['/bar/initiate/index'],
                                        'active' => $moduleId == 'bar' && ($controllerId == 'initiate'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Упаковка'),
                                        'visible' => Yii::$app->user->can('bar.pack.index'),
                                        'url' => ['/bar/pack/index'],
                                        'active' => $moduleId == 'bar' && ($controllerId == 'pack'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Возврат'),
                                        'visible' => Yii::$app->user->can('bar.return.index'),
                                        'url' => ['/bar/return/index'],
                                        'active' => $moduleId == 'bar' && ($controllerId == 'return'),
                                    ],
                                ],
                            ],
                            [
                                'label' => Yii::t('common', 'Система'),
                                'visible' => Yii::$app->user->can('administration.system.index'),
                                'icon' => WbIcon::SETTINGS,
                                'url' => ['/administration/system/index'],
                                'active' => $moduleId == 'administration' && $controllerId == 'system',
                            ],
                            [
                                'label' => Yii::t('common', 'Почтовые оповещения'),
                                'visible' => Yii::$app->user->can('administration.system.index'),
                                'icon' => WbIcon::EDIT,
                                'url' => ['/administration/system-mailing/index'],
                                'active' => $moduleId == 'administration' && $controllerId == 'systemmailing',
                            ],
                            [
                                'label' => Yii::t('common', 'Логи процессинга'),
                                'visible' => Yii::$app->user->can('logger.browser.index'),
                                'icon' => WbIcon::STATS_BARS,
                                'url' => ['/logger/browser/index'],
                                'active' => $moduleId == 'logger' && $controllerId == 'browser',
                            ],
                            [
                                'label' => Yii::t('common', 'Ошибки SQL запросов'),
                                'visible' => Yii::$app->user->can('logger.sqlerror.index'),
                                'icon' => FontAwesome::DATABASE,
                                'url' => ['/logger/sql-error/index'],
                                'active' => $moduleId == 'logger' && $controllerId == 'sql-error',
                            ],
                            [
                                'label' => Yii::t('common', 'Crontab'),
                                'visible' => (
                                    Yii::$app->user->can('administration.crontab.control.index') ||
                                    Yii::$app->user->can('administration.crontab.logs.index')
                                ),
                                'icon' => WbIcon::CLIPBOARD,
                                'url' => 'javascript:void(0);',
                                'active' => $moduleId == 'administration' && ($controllerId == 'crontab/control' || $controllerId == 'crontab/logs'),
                                'items' => [
                                    [
                                        'label' => Yii::t('common', 'Управление'),
                                        'visible' => Yii::$app->user->can('administration.crontab.control.index'),
                                        'url' => ['/administration/crontab/control/index'],
                                        'active' => $moduleId == 'administration' && ($controllerId == 'crontab/control'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Логи'),
                                        'visible' => Yii::$app->user->can('administration.crontab.logs.index'),
                                        'url' => ['/administration/crontab/logs/index'],
                                        'active' => $moduleId == 'administration' && ($controllerId == 'crontab/logs'),
                                    ],
                                ],
                            ],
                            [
                                'label' => Yii::t('common', 'API'),
                                'options' => ['class' => 'site-menu-category'],
                                'visible' =>  (
                                    Yii::$app->user->can('api.logs.lead.index') ||
                                    Yii::$app->user->can('api.logs.callcenter.index') ||
                                    Yii::$app->user->can('api.logs.delivery.index') ||
                                    Yii::$app->user->can('api.testing.deliveryapi.index')
                                ),
                            ],
                            [
                                'label' => Yii::t('common', 'Логирование'),
                                'visible' => (
                                    Yii::$app->user->can('api.logs.lead.index') ||
                                    Yii::$app->user->can('api.logs.callcenter.index') ||
                                    Yii::$app->user->can('api.logs.delivery.index')
                                ) ,
                                'icon' => WbIcon::ALIGN_JUSTIFY,
                                'url' => 'javascript:void(0);',
                                'active' => $moduleId == 'api' &&
                                    (
                                        $controllerId == 'logs/lead'
                                        || $controllerId == 'logs/call-center'
                                        || $controllerId == 'logs/delivery'
                                    ),
                                'items' => [
                                    [
                                        'label' => Yii::t('common', 'Лиды'),
                                        'visible' => Yii::$app->user->can('api.logs.lead.index'),
                                        'url' => ['/api/logs/lead'],
                                        'active' => $moduleId == 'api' && ($controllerId == 'logs/lead'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Колл-центр'),
                                        'visible' => Yii::$app->user->can('api.logs.callcenter.index'),
                                        'url' => ['/api/logs/call-center'],
                                        'active' => $moduleId == 'api' && ($controllerId == 'logs/call-center'),
                                    ],
                                    [
                                        'label' => Yii::t('common', 'Служба доставки'),
                                        'visible' => Yii::$app->user->can('api.logs.delivery.index'),
                                        'url' => ['/api/logs/delivery'],
                                        'active' => $moduleId == 'api' && ($controllerId == 'logs/delivery'),
                                    ],
                                ],
                            ],
                            [
                                'label' => Yii::t('common', 'Тестирование'),
                                'visible' => Yii::$app->user->can('api.testing.deliveryapi.index'),
                                'icon' => WbIcon::HAMMER,
                                'url' => 'javascript:void(0);',
                                'active' => $moduleId == 'api' &&
                                    (
                                        $controllerId == 'testing/delivery-api'
                                    ),
                                'items' => [
                                    [
                                        'label' => Yii::t('common', 'Служба доставки API'),
                                        'visible' => Yii::$app->user->can('api.testing.deliveryapi.index'),
                                        'url' => ['/api/testing/delivery-api'],
                                        'active' => $moduleId == 'api' && ($controllerId == 'testing/delivery-api'),
                                    ],
                                ],
                            ],
                        ],
                    ]
                ) ?>
            </div>
        </div>
    </div>
</div>
