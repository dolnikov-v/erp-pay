<?php
use app\widgets\ButtonLink;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var string $name */
/** @var string $message */
/** @var \Exception $exception */

$this->title = $name;
?>

<header>
    <h1 class="animation-slide-top"><?= $exception->statusCode ?></h1>
    <p><?= nl2br(Html::encode($message)) ?></p>
</header>
<p class="error-advise"><?= Yii::t('common', 'При обработке вашего запроса произошла ошибка.') ?></p>
<p class="error-advise"><?= Yii::t('common', 'Пожалуйста, свяжитесь с нами, если эта ошибка будет повторяться.') ?></p>

<p><?= ButtonLink::widget(['label' => Yii::t('common', 'На главную'), 'url' => Url::to('/'), 'style' => ButtonLink::STYLE_PRIMARY]); ?></p>
<?php if ($exception->statusCode == 403): ?>
    <p><?= ButtonLink::widget(['label' => Yii::t('common', 'Выйти'), 'url' => Url::to('/auth/logout'), 'style' => ButtonLink::STYLE_DANGER]); ?></p>
<?php endif; ?>
