function StickInit(target = '.stick > thead > tr', stopPanel = 'nav'){
    var stick = $(target);
    var stopObj = $(stopPanel);
    if (stick.get(0) && stopObj.get(0)) {
        var h_hght = stopObj.outerHeight(false);
        var h_nav = stick.outerHeight();
        var offset = stick.offset().top;
        var wh = stick.parent().width();
        var top;
        $(window).scroll(function () {
            top = $(this).scrollTop();
            if ((offset - top) <= h_nav && $(window).width() > 1000) {
                stick.css('top', h_hght);
                stick.css('position', 'fixed');
                stick.css('width', wh);
            } else {
                stick.css({'top': '', 'position': '', 'width': ''});
            }
        });
        $(window).resize(function () {
            $('.stick-hidden').remove();
            var dd = stick.clone();
            dd.addClass('stick-hidden');
            dd.removeAttr( 'style' );
            stick.parent().append(dd);
            h_hght = stopObj.outerHeight();
            h_nav = stick.outerHeight();
            offset = stick.offset().top;
            wh = stick.width();
        });
    }
}
(function(window, document, $) {
    StickInit();
})(window, document, jQuery);