<?php
use app\assets\ErrorsAsset;
use app\assets\Ie10Asset;
use app\assets\Ie9Asset;
use app\assets\ScriptAsset;
use app\assets\SectionsAsset;
use app\assets\vendor\AnimsitionAsset;
use app\assets\vendor\AsHoverScrollAsset;
use app\assets\vendor\AsScrollableAsset;
use app\assets\vendor\AsScrollAsset;
use app\assets\vendor\BootstrapNotifyAsset;
use app\assets\vendor\IntroJsAsset;
use app\assets\vendor\MousewheelAsset;
use app\assets\vendor\ScreenFullAsset;
use app\assets\vendor\SlidePanelAsset;
use app\assets\vendor\SwitcheryAsset;
use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var string $content */

ErrorsAsset::register($this);

AnimsitionAsset::register($this);
AsScrollableAsset::register($this);
SwitcheryAsset::register($this);
SlidePanelAsset::register($this);

AsHoverScrollAsset::register($this);
AsScrollAsset::register($this);
MousewheelAsset::register($this);
IntroJsAsset::register($this);
ScreenFullAsset::register($this);
BootstrapNotifyAsset::register($this);

SectionsAsset::register($this);

Ie9Asset::register($this);
Ie10Asset::register($this);

ScriptAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="no-js css-menubar">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Yii::t('common', 'Ошибка') ?> :: <?= Yii::$app->name ?></title>
    <?php $this->head() ?>
    <?= $this->render('@app/views/common/_breakpoints') ?>
</head>
<body class="page-error page-error-404 layout-full">
<?php $this->beginBody(); ?>

<div class="page animsition vertical-align text-center" data-animsition-in="fade-in" data-animsition-out="fade-out">
    <div class="page-content vertical-align-middle">
        <?= $content ?>
    </div>
</div>

<?php $this->endBody(); ?>

<?= $this->render('@app/views/common/_site-run') ?>
</body>
</html>
<?php $this->endPage() ?>
