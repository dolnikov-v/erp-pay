<?php

use app\assets\Ie10Asset;
use app\assets\Ie9Asset;
use app\assets\ScriptAsset;
use app\assets\SectionsAsset;
use app\assets\SiteAsset;
use app\assets\SkinAsset;
use app\assets\vendor\AnimsitionAsset;
use app\assets\vendor\AsHoverScrollAsset;
use app\assets\vendor\AsScrollableAsset;
use app\assets\vendor\AsScrollAsset;
use app\assets\vendor\BootstrapNotifyAsset;
use app\assets\vendor\HandlebarsAsset;
use app\assets\vendor\IntroJsAsset;
use app\assets\vendor\MousewheelAsset;
use app\assets\vendor\ScreenFullAsset;
use app\assets\vendor\SlidePanelAsset;
use app\assets\vendor\SwitcheryAsset;
use app\modules\access\assets\PingAsset;
use app\widgets\Breadcrumbs;
use yii\helpers\Html;

/** @var string $content */

PingAsset::register($this);

SiteAsset::register($this);
SkinAsset::register($this);

AnimsitionAsset::register($this);
AsScrollableAsset::register($this);
SwitcheryAsset::register($this);
SlidePanelAsset::register($this);

AsHoverScrollAsset::register($this);
AsScrollAsset::register($this);
MousewheelAsset::register($this);
IntroJsAsset::register($this);
ScreenFullAsset::register($this);
BootstrapNotifyAsset::register($this);

SectionsAsset::register($this);

HandlebarsAsset::register($this);

Ie9Asset::register($this);
Ie10Asset::register($this);

ScriptAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="no-js css-menubar">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?> :: <?= Yii::$app->name ?></title>
    <?php $this->head() ?>
    <?= $this->render('@app/views/common/_breakpoints') ?>
</head>
<body>
<?php $this->beginBody(); ?>

<?= $this->render('@app/views/common/_header') ?>
<?= $this->render('@app/views/common/_aside') ?>

<div class="page animsition">
    <div class="page-header">
        <?php if (!empty($this->params['customTitle'])): ?>
            <?= $this->params['customTitle'] ?>
        <?php else: ?>
            <h1 class="page-title"><?= Html::encode($this->title) ?></h1>
        <?php endif; ?>
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            'fixed' => $this->params['breadcrumbs_fixed'] ?? false,
        ]) ?>
    </div>

    <div class="page-content">
        <?= $content ?>
    </div>
</div>

<?= $this->render('@app/views/common/_footer') ?>

<?php $this->endBody(); ?>

<?= $this->render('@app/views/common/_notifications') ?>
<?= $this->render('@app/views/common/_site-run') ?>
</body>
</html>
<?php $this->endPage() ?>
