<?php

use app\components\CustomMigration as Migration;

/**
 * Handles adding deliveries to table `order_notification`.
 */
class m170807_114503_add_deliveries_to_order_notification extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('order_notification', 'deliveries', $this->string(255)->after('order_statuses'));
        $this->addColumn('order_notification_request', 'delivery_id', $this->integer()->after('order_status_id'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('order_notification', 'deliveries');
        $this->dropColumn('order_notification_request', 'delivery_id');
    }
}
