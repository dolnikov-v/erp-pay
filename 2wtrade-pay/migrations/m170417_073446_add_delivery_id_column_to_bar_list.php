<?php

use app\components\CustomMigration as Migration;
use app\modules\bar\models\BarList;
use app\modules\delivery\models\Delivery;

/**
 * Handles adding delivery_id_column to table `bar_list`.
 */
class m170417_073446_add_delivery_id_column_to_bar_list extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn(BarList::tableName(), 'delivery_id', $this->integer(4)->after('product_id'));
        $this->addForeignKey(null, BarList::tableName(), 'delivery_id', Delivery::tableName(), 'id',  self::NO_ACTION, self::NO_ACTION);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn(BarList::tableName(), 'delivery_id');
    }
}
