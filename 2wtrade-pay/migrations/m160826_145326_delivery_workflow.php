<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160826_145326_delivery_workflow
 */
class m160826_145326_delivery_workflow extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('delivery', 'workflow_id', $this->integer()->defaultValue(null)->after('auto_sending'));
        $this->addForeignKey(null, 'delivery', 'workflow_id', 'order_workflow', 'id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey($this->getFkName('delivery', 'workflow_id'), 'delivery');
        $this->dropColumn('delivery', 'workflow_id');
    }
}
