<?php
use app\components\CustomMigration as Migration;

/**
 * Class m161217_075446_addCallCenterTypeForOrder
 */
class m161217_075446_addCallCenterTypeForOrder extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('order', 'call_center_type', "ENUM('order', 'new_return', 'return_noprod', 'order_rejected', 'return') NOT NULL DEFAULT 'order' AFTER `call_center_again`");
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('order', 'call_center_type', "ENUM('order', 'new_return', 'return_noprod', 'order_rejected') NOT NULL DEFAULT 'order' AFTER `call_center_again`");
    }
}
