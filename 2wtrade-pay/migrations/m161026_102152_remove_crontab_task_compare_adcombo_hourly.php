<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m161026_102152_remove_crontab_task_compare_adcombo_hourly
 */
class m161026_102152_remove_crontab_task_compare_adcombo_hourly extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $task = CrontabTask::find()->where(['name' => 'report_compare_with_adcombo_hourly'])->one();
        if ($task) {
            $task->delete();
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $task = CrontabTask::find()->where(['name' => 'report_compare_with_adcombo_hourly'])->one();
        if (!$task) {
            $crontabTask = new CrontabTask();
            $crontabTask->name = 'report_compare_with_adcombo_hourly';
            $crontabTask->description = 'Ежечасная сверка с AdCombo';
            $crontabTask->save();
        }
    }
}
