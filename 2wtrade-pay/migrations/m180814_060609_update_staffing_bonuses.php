<?php
use app\components\CustomMigration as Migration;
use app\modules\salary\models\Staffing;
use app\modules\salary\models\BonusType;

/**
 * Class m180814_060609_update_staffing_bonuses
 */
class m180814_060609_update_staffing_bonuses extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropTable('salary_staffing_bonus');

        $this->createTable('salary_staffing_bonus', [
            'id' => $this->primaryKey(),
            'day' => $this->integer(),
            'staffing_id' => $this->integer()->notNull(),
            'bonus_type_id' => $this->integer()->notNull(),
        ]);

        $this->createIndex(null, 'salary_staffing_bonus', 'staffing_id');
        $this->createIndex(null, 'salary_staffing_bonus', 'bonus_type_id');
        $this->addForeignKey(null, 'salary_staffing_bonus', 'staffing_id', Staffing::tableName(), 'id', self::CASCADE, self::CASCADE);
        $this->addForeignKey(null, 'salary_staffing_bonus', 'bonus_type_id', BonusType::tableName(), 'id', self::CASCADE, self::CASCADE);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('salary_staffing_bonus');

        $this->createTable('salary_staffing_bonus', [
            'staffing_id' => $this->integer()->notNull(),
            'bonus_type_id' => $this->integer()->notNull(),
        ]);
    }
}
