<?php
use app\components\CustomMigration as Migration;
use app\modules\order\models\OrderStatus;

/**
 * Class m160721_105821_fix_statuses
 */
class m160721_105821_fix_statuses extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        /** @var OrderStatus $status */
        $status = OrderStatus::findOne(OrderStatus::STATUS_CC_FAIL_CALL);

        if ($status) {
            $status->type = OrderStatus::TYPE_MIDDLE;
            $status->save(true, ['type']);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        /** @var OrderStatus $status */
        $status = OrderStatus::findOne(OrderStatus::STATUS_CC_FAIL_CALL);

        if ($status) {
            $status->type = OrderStatus::TYPE_FINAL;
            $status->save(true, ['type']);
        }
    }
}
