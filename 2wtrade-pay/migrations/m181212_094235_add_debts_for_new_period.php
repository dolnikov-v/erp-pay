<?php

/**
 * Class m181212_094235_add_debts_for_new_period
 */
class m181212_094235_add_debts_for_new_period extends \app\components\PermissionMigration
{
    protected $permissions = [
        'report.debts.indextwo'
    ];
    protected $roles = [];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $roles = \app\models\AuthItemChild::find()->where(['child' => 'report.debts.index'])->select('parent')->distinct()->column();
        foreach ($roles as $role) {
            $this->roles[$role] = ['report.debts.indextwo'];
        }
        parent::safeUp();
    }
}
