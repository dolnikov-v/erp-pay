<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryApiClass;

/**
 * Class m171004_063141_add_bex_api
 */
class m171004_063141_add_bex_api extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $apiClass = new DeliveryApiClass();
        $apiClass->name = 'Bex';
        $apiClass->class_path = '/bex-api/src/BexApi.php';
        $apiClass->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        DeliveryApiClass::findOne(['name' => "Bex"])->delete();
    }
}
