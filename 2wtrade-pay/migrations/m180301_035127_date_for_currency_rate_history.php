<?php
use app\components\CustomMigration as Migration;

/**
 * Class m180301_035127_date_for_currency_rate_history
 */
class m180301_035127_date_for_currency_rate_history extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('currency_rate_history', 'date', $this->date()->after('bid'));
        $this->createIndex($this->getIdxName('currency_rate_history', [
            'currency_id',
            'created_at'
        ]), 'currency_rate_history', ['currency_id', 'created_at']);
        $this->createIndex($this->getIdxName('currency_rate_history', [
            'currency_id',
            'date'
        ]), 'currency_rate_history', ['currency_id', 'date']);
        $this->update('currency_rate_history', ['date' => new \yii\db\Expression('DATE(FROM_UNIXTIME(`created_at`))')]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropIndex($this->getIdxName('currency_rate_history', [
            'currency_id',
            'created_at'
        ]), 'currency_rate_history');
        $this->dropIndex($this->getIdxName('currency_rate_history', [
            'currency_id',
            'date'
        ]), 'currency_rate_history');
        $this->dropColumn('currency_rate_history', 'date');
    }
}
