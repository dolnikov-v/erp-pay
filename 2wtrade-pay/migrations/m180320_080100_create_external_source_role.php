<?php

use app\modules\catalog\models\ExternalSource;
use app\modules\catalog\models\ExternalSourceRole;
use app\components\CustomMigration as Migration;

/**
 * Handles the creation for table `external_source_role`.
 */
class m180320_080100_create_external_source_role extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable(ExternalSourceRole::tableName(), [
            'id' => $this->primaryKey(),
            'external_source_id' => $this->integer(),
            'name' => $this->string(),
            'description' => $this->string(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ],  $this->tableOptions);

        $this->addForeignKey(null, ExternalSourceRole::tableName(), 'external_source_id', ExternalSource::tableName(), 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable(ExternalSourceRole::tableName());
    }
}