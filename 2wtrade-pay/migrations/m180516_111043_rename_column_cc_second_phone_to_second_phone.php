<?php
use app\components\CustomMigration as Migration;
use app\modules\order\models\Lead;

/**
 * Class m180516_111043_rename_column_cc_second_phone_to_second_phone
 */
class m180516_111043_rename_column_cc_second_phone_to_second_phone extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropColumn(Lead::tableName(), 'cc_second_phone');
        $this->addColumn(Lead::tableName(), 'second_phone', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Lead::tableName(), 'second_phone');
        $this->addColumn(Lead::tableName(), 'cc_second_name', $this->string());
    }
}
