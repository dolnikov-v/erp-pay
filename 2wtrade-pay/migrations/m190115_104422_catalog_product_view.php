<?php

use app\components\PermissionMigration as Migration;

/**
 * Class m190115_104422_catalog_product_view
 */
class m190115_104422_catalog_product_view extends Migration
{
    protected $permissions = ['catalog.product.view'];

    protected $roles = [
        'admin' => ['catalog.product.view'],
        'controller.analyst' => ['catalog.product.view'],
        'project.manager' => ['catalog.product.view'],
        'storage.manager' => ['catalog.product.view'],
        'support.manager' => ['catalog.product.view'],
        'translator' => ['catalog.product.view'],
    ];
}

