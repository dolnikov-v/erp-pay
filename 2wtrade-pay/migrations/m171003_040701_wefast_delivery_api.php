<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryApiClass;
use app\modules\delivery\models\Delivery;
use app\models\Country;

/**
 * Class m171003_040701_wefast_delivery_api
 */
class m171003_040701_wefast_delivery_api extends Migration
{

    protected $api_name = 'WefastApi';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        $india = Country::find()->byCharCode('IN')->one();
        if (!$india) {
            echo "Error: Create country India";
            die();
        }

        $class = DeliveryApiClass::find()
            ->where([
                'name' => $this->api_name,
            ])
            ->one();

        if (!$class) {
            $class = new DeliveryApiClass();
            $class->name = $this->api_name;
            $class->class_path = '/wefast-api/src/wefastApi.php';
            $class->active = 1;
            $class->save();
        }

        $delivery = Delivery::find()
            ->where([
                'name' => 'Wefast',
            ])
            ->one();

        if (!$delivery) {
            $delivery = new Delivery();
            $delivery->name = 'Wefast';
            $delivery->char_code = 'WEF';
            $delivery->active = 1;
            $delivery->api_class_id = $class->id;
            $delivery->country_id = $india->id;
            $delivery->save();
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $delivery = Delivery::find()
            ->where([
                'name' => 'Wefast',
            ])
            ->one();

        if ($delivery) {
            $delivery->delete();
        }

        $class = DeliveryApiClass::find()
            ->where([
                'name' => $this->api_name,
            ])
            ->one();
        if ($class) {
            $class->delete();
        }
    }
}
