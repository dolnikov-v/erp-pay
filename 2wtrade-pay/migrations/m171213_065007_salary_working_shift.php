<?php
use app\components\PermissionMigration as Migration;

/**
 * Class m171213_065007_salary_working_shift
 */
class m171213_065007_salary_working_shift extends Migration
{
    /**
     * @var array
     */
    protected $permissions = [
        'salary.workingshift.index',
    ];

    /**
     * @var array
     */
    protected $roles = [
/* пока не даем доступ для суперов КЦ
        'callcenter.leader' => [
            'salary.workingshift.index',
        ],
*/
        'project.manager' => [
            'salary.workingshift.index',
        ],
        'admin' => [
            'salary.workingshift.index',
        ],
        'salaryproject.clerk' => [
            'salary.workingshift.index',
        ],
    ];
}
