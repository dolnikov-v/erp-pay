<?php
use app\components\CustomMigration as Migration;

/**
 * Class m161224_084138_add_cc_type_for_order
 */
class m161224_084138_add_cc_type_for_order extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('order', 'call_center_type', "ENUM('order', 'new_return', 'return_noprod', 'jeempo', 'return') NOT NULL DEFAULT 'order' AFTER `call_center_again`");
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('order', 'call_center_type', "ENUM('order', 'new_return', 'return_noprod', 'return') NOT NULL DEFAULT 'order' AFTER `call_center_again`");
    }
}
