<?php

use yii\db\Migration;
use yii\db\Query;

/**
 * Handles adding translate to table `mercury_functional`.
 */
class m180323_084740_add_translate_to_mercury_functional extends Migration
{
    const TEXT = [
        'Роли сторонних источников' => 'Third-party sources',
        'Наименование' => 'Name',
        'Url источника' => 'Url source',
        'Описание' => 'Description',
        'Источник' => 'Source',
        'Наименование роли' => 'Role title',
        'Список сторонних источников' => 'Third-party sources list',
        'Таблица с источниками' => 'Source table',
        'Добавить источник' => 'Add source',
        'Сохранить источник' => 'Save Source',
        'Редактирование стороннего источника' => 'Editing a third-party source',
        'Добавление стороннего источника' => 'Add a third-party source',
        'Сторонний источник успешно добавлен.' => 'Third-party source was successfully added.',
        'Сторонний источник успешно отредактирован.' => 'The third-party source was successfully edited.',
        'Список ролей сторонних источников' => 'The list of third-party sources',
        'Таблица с ролями источников' => 'Table with source roles',
        'Сторонний источник успешно удален' => 'Third-party source deleted successfully',
        'При работе с ролями произошла ошибка' => 'An error occurred while working with roles',
        'Роль успешно привязана' => 'Role successfully attached',
        'Ошибка привязки роли' => 'Role binding error',
        'Ошибка : {error}' => 'Error: {error}',
        'Для данного источника роли не были получены' => 'For this source roles were not received',
        'Операция выполнена успешно' => 'Operation completed successfully',
    ];

    /**
     * @inheritdoc
     */
    public function up()
    {
        $query = new Query();

        foreach ($this::TEXT as $ruText => $enText) {

            $is = $query->select('id')
                ->from('i18n_source_message')
                ->where(['category' => 'common', 'message' => $ruText])->one();

            if (!$is) {
                $this->insert('i18n_source_message', ['category' => 'common', 'message' => $ruText]);
                $id = Yii::$app->db->getLastInsertID();
                $this->insert('i18n_message', ['id' => $id, 'language' => 'en-US', 'translation' => $enText]);
            }
        }

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        foreach ($this::TEXT as $ruText => $enText) {
            $this->delete('i18n_source_message', ['category' => 'common', 'message' => $ruText]);
        }

    }
}
