<?php
use app\components\CustomMigration as Migration;
use app\models\Country;

/**
 * Class m170209_075813_country_table_add_column_cc_operator_daily_approve_plan
 */
class m170209_075813_country_table_add_column_cc_operator_daily_approve_plan extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Country::tableName(), 'cc_operator_daily_approve_plan', $this->integer()->defaultValue(null));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Country::tableName(), 'cc_operator_daily_approve_plan');
    }
}
