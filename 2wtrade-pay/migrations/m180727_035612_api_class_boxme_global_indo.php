<?php
use app\components\CustomMigration as Migration;

use app\modules\delivery\models\DeliveryApiClass;

/**
 * Class m180727_035612_api_class_boxme_global_indo
 */
class m180727_035612_api_class_boxme_global_indo extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $apiClass = new DeliveryApiClass();
        $apiClass->name = 'boxMeApi';
        $apiClass->active = 1;
        $apiClass->can_tracking = 0;
        $apiClass->class_path = '/boxme-api/src/boxMeApi.php';
        $apiClass->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        DeliveryApiClass::find()
            ->where([
                'name' => 'boxMeApi',
            ])
            ->one()
            ->delete();
    }
}