<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryRequest;


/**
 * Class m170601_080229_update_tracking_delivery_request
 */
class m170601_080229_update_tracking_delivery_request extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn(DeliveryRequest::tableName(), 'tracking', $this->string(255)->defaultValue(null));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn(DeliveryRequest::tableName(), 'tracking', $this->string(30));
    }
}
