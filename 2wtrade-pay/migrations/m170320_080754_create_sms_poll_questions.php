<?php

use app\components\CustomMigration as Migration;
use app\models\Country;
use app\modules\catalog\models\SmsPollQuestions;
use app\models\AuthItem;

/**
 * Handles the creation for table `sms_poll_questions`.
 */
class m170320_080754_create_sms_poll_questions extends Migration
{
    const CASCADE = 'CASCADE';
    /**
     * @inheritdoc
     */
    public function up()
    {

        $this->createTable(SmsPollQuestions::tableName(), [
            'id' => $this->primaryKey(),
            'country_id' => $this->integer()->notNull(),
            'question_text' => $this->text(),
            'is_active' => $this->boolean()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull()
        ]);

        $this->addForeignKey(null, SmsPollQuestions::tableName(), 'country_id', Country::tableName(), 'id', self::CASCADE, self::CASCADE);

        $list = [
            'CMC опрос',
            'Сохранить вопрос',
            'Редактировать вопрос',
            'Добавить вопрос',
            'Удалить вопрос',
            'СМС опрос',
            'Таблица вопросов',
            'Вопрос успешно добавлен.',
            'Вопрос успешно отредактирован.',
            'Вопрос не найден',
            'Список вопросов',
            'Сведения о вопросе',
            'Просмотр вопроса',
            'Текст вопроса',
        ];

        foreach ($list as $k => $v) {
            $this->insert('i18n_source_message', ['category' => 'common', 'message' => $v]);
        }

        $this->insert(AuthItem::tableName(),array(
            'name'=>'catalog.smspollquestions.index',
            'type' => '2',
            'description' => 'catalog.smspollquestions.index',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'country.curator',
            'child' => 'catalog.smspollquestions.index'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'callcenter.manager',
            'child' => 'catalog.smspollquestions.index'
        ));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable(SmsPollQuestions::tableName());
        $this->delete($this->authManager->itemChildTable, ['child' => 'catalog.smspollquestions.index', 'parent' => 'country.curator']);
        $this->delete($this->authManager->itemChildTable, ['child' => 'catalog.smspollquestions.index', 'parent' => 'callcenter.manager']);
        $this->delete(AuthItem::tableName(), ['name' => 'catalog.smspollquestions.index']);
    }
}
