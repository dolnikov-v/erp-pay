<?php

use app\components\CustomMigration as Migration;

/**
 * Class m190204_102602_refactoring_source
 */
class m190204_102602_refactoring_source extends Migration
{
    protected $tokens = [
        'a77cf421eee4938ef83a89de92f3f65214e99bec' => 'adcombo',
        '21afc774aa5e50b232977870305795553f2e0a70' => 'jeempo',
        '20b70f0af00562e63758b9ee42012ecc96c58590' => 'call_center',
        'c851e902148a4fdcf99c9d7a541285d9135cf500' => 'dina',

        'dgnv20s64l21v9br46he33fdn0n4bd53Gik0fju4' => '2wstore',
        'f2e0a7e6f59d6fa0c9d653fdn0n4bd53Gik0fju4' => 'diavita',
        '72c46677f59d6fa0c9d6581f632d4746cf28bb36' => 'sms_poll_history',
        '310ec4e6213b134f6c2813bed00192195a756ee0' => 'notification',
        'e4938ef83a89dem6f59d6fa03fdn0n4bd53Gik0f' => 'zabbix',
        '4608532b542fa2b0ff78d97c2fee8498a03fdn0n' => 'mercury',
        '8603fc9f6f5e7ac50f75e7680f72cf17c4162392' => '2wDistr',
        '653355e7032146e08b85e53de142b355b1317301' => 'wtrade_post_sale',

        '5526eca61e078a2e97063fe86d5fd8d3d234f19a' => 'test_1',
        '655204e9c26c82e11fb2c7bb5a91a74455c3acf1' => 'my_epc',
        'b3061ed2070c813a97e67c06d8258b060f0a3c21' => 'test_3',
        '44ca55def9c93b3843359599d27834f47b876a5d' => 'test_4',
        '3ed2e00b58148a23f21cbbafa62765d621c0ac2b' => 'test_5',

        'deab196b508fdfee5f8b723d39846a345c20180a' => 'tophot',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('api_user', 'source_id', $this->integer()->after('status'));
        $this->addForeignKey($this->getFkName('api_user', 'source_id'), 'api_user', 'source_id', 'source', 'id', self::RESTRICT, self::CASCADE);
        $this->alterColumn('api_user_token', 'type', $this->string(20));
        $this->addColumn('partner', 'source_id', $this->integer()->after('source'));
        $this->addForeignKey($this->getFkName('partner', 'source_id'), 'partner', 'source_id', 'source', 'id', self::CASCADE, self::CASCADE);

        $partners = (new \yii\db\Query())->from('partner')->select(['id', 'source'])->all($this->db);

        // Переделываем связку партнер-источник
        foreach ($partners as $partner) {
            $sourceId = (new \yii\db\Query())->from('source')
                ->where(['name' => $partner['source']])
                ->select('id')
                ->scalar($this->db);
            $this->update('partner', ['source_id' => $sourceId], ['id' => $partner['id']]);
        }

        $this->dropColumn('partner', 'source');
        $this->addColumn('source', 'unique_system_name', $this->string(32)->unique()->after('name'));

        // Создаем токены и пользователей АПИ для источников
        foreach ($this->tokens as $token => $service) {
            $userId = (new \yii\db\Query())->from('api_user')
                ->where(['username' => $service])
                ->select('id')
                ->scalar($this->db);
            $sourceId = (new \yii\db\Query())->from('source')
                ->where(['name' => $service])
                ->select('id')
                ->scalar($this->db);
            if (!$userId) {
                $this->insert('api_user', [
                    'username' => $service,
                    'password' => Yii::$app->security->generatePasswordHash(Yii::$app->security->generateRandomString(16)),
                    'status' => 1,
                    'source_id' => $sourceId ?: null,
                    'created_at' => time(),
                    'updated_at' => time()
                ]);
                $userId = $this->db->lastInsertID;
            } else {
                $this->update('api_user', ['source_id' => $sourceId ?: null], ['id' => $userId]);
            }
            if (!(new \yii\db\Query())->from('api_user_token')
                ->where(['user_id' => $userId, 'auth_token' => $token])
                ->exists($this->db)) {
                $this->insert('api_user_token', [
                    'user_id' => $userId,
                    'type' => 'source',
                    'auth_token' => $token,
                    'lifetime' => 0,
                    'created_at' => time(),
                    'updated_at' => time()
                ]);
            } else {
                $this->update('api_user_token', ['lifetime' => 0], ['user_id' => $userId, 'auth_token' => $token]);
            }
        }

        $this->addColumn('source', 'default_call_center_type', $this->string(50)
            ->defaultValue('order')
            ->after('unique_system_name'));

        $this->update('source', ['unique_system_name' => new \yii\db\Expression('name')]);

        // Переделываем связки КЦ-источники и Пользователь-источники
        $this->addColumn('call_center_source', 'source_id', $this->integer()->after('source'));
        $this->addColumn('user_source', 'source_id', $this->integer()->after('source'));
        $ccSources = (new \yii\db\Query())->from('call_center_source')->distinct()->select('source')->column($this->db);
        foreach ($ccSources as $source) {
            $this->update('call_center_source', [
                'source_id' => (new \yii\db\Query())->from('source')
                    ->where(['unique_system_name' => $source])
                    ->select('id')
                    ->scalar($this->db)
            ], ['source' => $source]);
        }
        $userSources = (new \yii\db\Query())->from('user_source')->distinct()->select('source')->column($this->db);
        foreach ($userSources as $source) {
            $this->update('user_source', [
                'source_id' => (new \yii\db\Query())->from('source')
                    ->where(['unique_system_name' => $source])
                    ->select('id')
                    ->scalar($this->db)
            ], ['source' => $source]);
        }

        $this->alterColumn('call_center_source', 'source_id', $this->integer()->notNull());
        $this->alterColumn('user_source', 'source_id', $this->integer()->notNull());

        $this->dropPrimaryKey($this->getPkName('call_center_source', [
            'call_center_id',
            'source'
        ]), 'call_center_source');
        $this->dropPrimaryKey($this->getPkName('user_source', ['user_id', 'source']), 'user_source');
        $this->addPrimaryKey($this->getPkName('call_center_source', [
            'call_center_id',
            'source_id'
        ]), 'call_center_source', ['call_center_id', 'source_id']);
        $this->addPrimaryKey($this->getPkName('user_source', ['user_id', 'source_id']), 'user_source', [
            'user_id',
            'source_id'
        ]);
        $this->addForeignKey($this->getFkName('call_center_source', 'source_id'), 'call_center_source', 'source_id', 'source', 'id', self::CASCADE, self::CASCADE);
        $this->addForeignKey($this->getFkName('user_source', 'source_id'), 'user_source', 'source_id', 'source', 'id', self::CASCADE, self::CASCADE);
        $this->dropColumn('user_source', 'source');
        $this->dropColumn('call_center_source', 'source');

         // Переделываем источники в лидах и заказах
        $this->addColumn('lead', 'source_id', $this->integer()->after('source'));
        $this->addColumn('order', 'source_id', $this->integer()->after('source')->defaultValue(null));

        $sources = (new \yii\db\Query())->from('source')->select(['id', 'unique_system_name'])->all($this->db);
        foreach ($sources as $source) {
            $this->update('lead', ['source_id' => $source['id']], ['source' => $source['unique_system_name']]);
            $this->update('order', ['source_id' => $source['id']], ['source' => $source['unique_system_name']]);
        }
        $this->update('lead', ['source_id' => 1], ['source_id' => 0]);
        $this->dropPrimaryKey($this->getPkName('lead', ['source', 'foreign_id']), 'lead');
        $this->addPrimaryKey($this->getPkName('lead', ['source_id', 'foreign_id']), 'lead', [
            'source_id',
            'foreign_id'
        ]);
        $this->addForeignKey($this->getFkName('lead', 'source_id'), 'lead', 'source_id', 'source', 'id', self::RESTRICT, self::CASCADE);
        $this->addForeignKey($this->getFkName('order', 'source_id'), 'order', 'source_id', 'source', 'id', self::RESTRICT, self::CASCADE);

        $this->createIndex($this->getIdxName('order', ['source_id', 'foreign_id']), 'order', [
            'source_id',
            'foreign_id'
        ], true);
        $this->dropIndex('uni_order_sourceforeign_id', 'order');
        $this->dropIndex($this->getIdxName('lead', ['source', 'country', 'source_status', 'ts_spawn']), 'lead');
        $this->createIndex($this->getIdxName('lead', ['source_id', 'country', 'source_status', 'ts_spawn']), 'lead', ['source_id', 'country', 'source_status', 'ts_spawn']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropIndex($this->getIdxName('lead', ['source_id', 'country', 'source_status', 'ts_spawn']), 'lead');
        $this->createIndex($this->getIdxName('lead', ['source', 'country', 'source_status', 'ts_spawn']), 'lead', ['source', 'country', 'source_status', 'ts_spawn']);
        $this->dropIndex($this->getIdxName('order', ['source_id', 'foreign_id']), 'order');
        $this->createIndex('uni_order_sourceforeign_id', 'order', ['source', 'foreign_id'], true);
        $this->dropForeignKey($this->getFkName('order', 'source_id'), 'order');
        $this->dropForeignKey($this->getFkName('lead', 'source_id'), 'lead');
        $this->dropPrimaryKey($this->getPkName('lead', ['source_id', 'foreign_id']), 'lead');
        $this->addPrimaryKey($this->getPkName('lead', ['source', 'foreign_id']), 'lead', ['source', 'foreign_id']);
        $this->dropColumn('order', 'source_id');
        $this->dropColumn('lead', 'source_id');

        $this->addColumn('user_source', 'source', $this->string(64)->after('call_center_id'));
        $this->addColumn('call_center_source', 'source', $this->string(64)->after('user_id'));

        $ccSources = (new \yii\db\Query())->from('call_center_source')
            ->distinct()
            ->select('source_id')
            ->column($this->db);
        foreach ($ccSources as $source) {
            $this->update('call_center_source', [
                'source' => (new \yii\db\Query())->from('source')
                    ->where(['id' => $source])
                    ->select('unique_system_name')
                    ->scalar($this->db)
            ], ['source_id' => $source]);
        }
        $userSources = (new \yii\db\Query())->from('user_source')->distinct()->select('source_id')->column($this->db);
        foreach ($userSources as $source) {
            $this->update('user_source', [
                'source' => (new \yii\db\Query())->from('source')
                    ->where(['id' => $source])
                    ->select('unique_system_name')
                    ->scalar($this->db)
            ], ['source_id' => $source]);
        }

        $this->alterColumn('user_source', 'source', $this->string(64)->notNull());
        $this->alterColumn('cal_center_source', 'source', $this->string(64)->notNull());

        $this->dropForeignKey($this->getFkName('user_source', 'source_id'), 'user_source');
        $this->dropForeignKey($this->getFkName('call_center_source', 'source_id'), 'call_center_source');

        $this->dropPrimaryKey($this->getPkName('call_center_source', [
            'call_center_id',
            'source_id'
        ]), 'call_center_source');
        $this->dropPrimaryKey($this->getPkName('user_source', ['user_id', 'source_id']), 'user_source');
        $this->addPrimaryKey($this->getPkName('call_center_source', [
            'call_center_id',
            'source'
        ]), 'call_center_source', ['call_center_id', 'source']);
        $this->addPrimaryKey($this->getPkName('user_source', ['user_id', 'source']), 'user_source', [
            'user_id',
            'source'
        ]);
        $this->dropColumn('user_source', 'source_id');
        $this->dropColumn('call_center_source', 'source_id');


        $this->dropColumn('source', 'unique_system_name');
        $this->dropColumn('source', 'default_call_center_type');
        $this->addColumn('partner', 'source', $this->string(50)->after('source_id'));
        $partners = (new \yii\db\Query())->from('partner')->select(['id', 'source_id'])->all($this->db);

        foreach ($partners as $partner) {
            $sourceName = (new \yii\db\Query())->from('source')
                ->where(['id' => $partner['source_id']])
                ->select('name')
                ->scalar($this->db);
            $this->update('partner', ['source' => $sourceName], ['id' => $partner['id']]);
        }

        $this->dropForeignKey($this->getFkName('partner', 'source_id'), 'partner', 'source_id');
        $this->dropColumn('partner', 'source_id');

        $this->dropForeignKey($this->getFkName('api_user', 'source_id'), 'api_user', 'source_id');
        $this->dropColumn('api_user', 'source_id');
        $this->delete('api_user_token', ['AND', ['!=', 'type', 'delivery'], ['!=', 'type', 'call_center']]);
        $this->alterColumn('api_user_token', 'type', $this->enum(['delivery', 'call_center']));
    }
}
