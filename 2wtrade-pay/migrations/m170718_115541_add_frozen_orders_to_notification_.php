<?php

use app\models\Notification;
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Handles adding frozen_orders to table `notification_`.
 */
class m170718_115541_add_frozen_orders_to_notification_ extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $model = new Notification([
            'description' => '{country_name}. {amount} заказов находится в статусе {status_id} более {time_norm}' . PHP_EOL .
                             'попробуйте исправить ситуацию одним из предложенных способов:' . PHP_EOL .
                             'https://2wtrade.atlassian.net/wiki/spaces/SPC/pages/18716335 .' . PHP_EOL,
            'trigger' => Notification::TRIGGER_REPORT_ORDERS_FROZEN,
            'type' => Notification::TYPE_DANGER,
            'group' => Notification::GROUP_SYSTEM,
            'active' => Notification::ACTIVE,
        ]);

        $model->save(false);

        $model = new Notification([
            'description' => '{country_name}. {amount} заказов находится в статусе {status_id} более {time_norm}' . PHP_EOL .
                'в течении 2 часов ситуация не изменилась:' . PHP_EOL,
            'trigger' => Notification::TRIGGER_REPORT_ORDERS_STILL_FROZEN,
            'type' => Notification::TYPE_DANGER,
            'group' => Notification::GROUP_SYSTEM,
            'active' => Notification::ACTIVE,
        ]);

        $model->save(false);

        $crontabTask = new CrontabTask();
        $crontabTask->name = CrontabTask::TASK_REPORT_ORDERS_FROZEN;
        $crontabTask->description = "Скопление заказов в 1,2,6,9,12,14,15,16,19,27,31,32 колонке";
        $crontabTask->save();

        $crontabTask = new CrontabTask();
        $crontabTask->name = CrontabTask::TASK_REPORT_ORDERS_STILL_FROZEN;
        $crontabTask->description = "Скопление заказов в 1,2,6,9,12,14,15,16,19,27,31,32 колонке без улучшений в течении 2 часов";
        $crontabTask->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete(Notification::tableName(), ['trigger' => Notification::TRIGGER_REPORT_ORDERS_FROZEN]);
        $this->delete(Notification::tableName(), ['trigger' => Notification::TRIGGER_REPORT_ORDERS_STILL_FROZEN]);
        $this->delete(CrontabTask::tableName(), ['name' => CrontabTask::TASK_REPORT_ORDERS_FROZEN]);
        $this->delete(CrontabTask::tableName(), ['name' => CrontabTask::TASK_REPORT_ORDERS_STILL_FROZEN]);
    }
}
