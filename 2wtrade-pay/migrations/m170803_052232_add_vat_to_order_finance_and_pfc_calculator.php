<?php

use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryChargesCalculator;
use app\modules\order\models\OrderFinance;

/**
 * Handles adding vat to table `order_finance_and_pfc_calculator`.
 */
class m170803_052232_add_vat_to_order_finance_and_pfc_calculator extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(OrderFinance::tableName(), 'price_vat', $this->double()->after('price_cod_service'));
        $this->insert(DeliveryChargesCalculator::tableName(), [
            'name' => 'PfcCommon',
            'class_path' => 'app\modules\delivery\components\charges\pfc\PfcCommon',
            'active' => 1,
            'created_at' => time(),
        ]);
        $this->insert(DeliveryChargesCalculator::tableName(), [
            'name' => 'PfcCN',
            'class_path' => 'app\modules\delivery\components\charges\pfc\PfcCN',
            'active' => 1,
            'created_at' => time(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(OrderFinance::tableName(), 'price_vat');
        $this->delete(DeliveryChargesCalculator::tableName(), ['name' => 'PfcCommon']);
        $this->delete(DeliveryChargesCalculator::tableName(), ['name' => 'PfcCN']);
    }
}
