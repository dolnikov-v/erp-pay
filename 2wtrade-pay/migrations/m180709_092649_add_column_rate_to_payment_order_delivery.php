<?php

use app\modules\deliveryreport\models\PaymentOrder;
use yii\db\Migration;

/**
 * Handles adding column_rate to table `payment_order_delivery`.
 */
class m180709_092649_add_column_rate_to_payment_order_delivery extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn(PaymentOrder::tableName(), 'rate', $this->double()->after('currency_id'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn(PaymentOrder::tableName(), 'rate');
    }
}
