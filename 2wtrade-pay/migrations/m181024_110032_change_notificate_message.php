<?php
use app\components\CustomMigration as Migration;

/**
 * Class m181024_110032_change_notificate_message
 */
class m181024_110032_change_notificate_message extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->update('notification', [
                'description' => 'В {country}: {delivery} {num} {num, plural, =0{все листы отправлены} =1{не отправлен # лист} one{не отправлен # лист} few{не отправлено # листа} many{не отправлено # листов} other{не отправлено # листов}}.' . PHP_EOL .
                    'Последняя дата создания листа {last}.',
            ],
            [
                'trigger' => 'order.logistic.check.sending.list',
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->update('notification', ['description' => 'В {country}:{delivery} не отправлено {num}.\r\nПоследняя дата создания листа {last}.'],
            ['trigger' => 'order.logistic.check.sending.list',]
        );
    }
}
