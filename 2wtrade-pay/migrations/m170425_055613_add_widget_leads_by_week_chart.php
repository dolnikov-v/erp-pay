<?php

use app\components\CustomMigration as Migration;
use app\modules\widget\models\WidgetType;
use app\modules\widget\models\WidgetRole;

/**
 * Class m170425_055613_add_widget_leads_by_week_chart
 */
class m170425_055613_add_widget_leads_by_week_chart extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $code = 'leads_by_week_chart';

        $roles = [
            'business_analyst',
            'country.curator',
            'callcenter.manager',
        ];

        $this->insert(WidgetType::tableName(), [
            'code' => $code,
            'name' => 'Лиды по неделям',
            'status' => WidgetType::STATUS_ACTIVE,
            'by_country' => WidgetType::BY_ALL_COUNTRIES,
        ]);

        $type = WidgetType::find()
            ->where(['code' => $code])
            ->one();

        foreach ($roles as $role) {
            $this->insert(WidgetRole::tableName(), [
                'role' => $role,
                'type_id' => $type->id
            ]);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $code = 'leads_by_week_chart';
        $roles = [
            'business_analyst',
            'country.curator',
            'callcenter.manager',
        ];

        $type = WidgetType::find()
            ->where(['code' => $code])
            ->one();

        foreach ($roles as $role) {
            $this->delete(WidgetRole::tableName(), [
                'role' => $role,
                'type_id' => $type->id
            ]);
        }

        $this->delete(WidgetType::tableName(), ['code' => $code]);
    }
}
