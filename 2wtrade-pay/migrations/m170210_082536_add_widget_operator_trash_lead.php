<?php
use app\components\CustomMigration as Migration;
use app\modules\widget\models\WidgetType;
use app\modules\widget\models\WidgetRole;
/**
 * Class m170210_082536_add_widget_operator_trash_lead
 */
class m170210_082536_add_widget_operator_trash_lead extends Migration
{
    /**
     * cоздание записи о виджете, добавление роли пользователя к виджету
     */
    public function safeUp()
    {
        $this->insert(WidgetType::tableName(), [
            'code' => 'operator_trash_lead',
            'name' => 'Треши операторов',
            'status' => WidgetType::STATUS_ACTIVE
        ]);

        $type = WidgetType::find()
            ->where(['code' => 'operator_trash_lead'])
            ->one();

        $this->insert(WidgetRole::tableName(), [
            'role' => 'business_analyst',
            'type_id' => $type->id
        ]);

        $this->insert(WidgetRole::tableName(), [
            'role' => 'callcenter.manager',
            'type_id' => $type->id
        ]);
    }


    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $type = WidgetType::find()
            ->where(['code' => 'operator_trash_lead'])
            ->one();

        $this->delete(WidgetRole::tableName(), [
            'role' => 'business_analyst',
            'type_id' => $type->id,
        ]);

        $this->delete(WidgetRole::tableName(), [
            'role' => 'callcenter.manager',
            'type_id' => $type->id,
        ]);

        $this->delete(WidgetType::tableName(), ['code' => 'operator_trash_lead']);
    }
}
