<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160930_045206_report_debts_add_column_created
 */
class m160930_045206_report_debts_add_column_created extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('report_debts', 'created_at', $this->integer()->defaultValue(null));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('report_debts', 'created_at');
    }
}
