<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m170516_100242_add_crontab_task_for_sms_poll_history
 */
class m170516_100242_add_crontab_task_for_sms_poll_history extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = new CrontabTask();
        $crontabTask->name = 'take_sms_poll_history';
        $crontabTask->description = 'Подготовка смс опросов для отправки по API';
        $crontabTask->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = CrontabTask::find()
            ->byName('take_sms_poll_history')
            ->one();

        if ($crontabTask) {
            $crontabTask->delete();
        }
    }
}
