<?php
use app\components\PermissionMigration as Migration;

/**
 * Class m181029_064619_add_roles_from_see_logs
 */
class m181029_064619_add_roles_from_see_logs extends Migration
{
    protected $permissions = ['callcenter.control.log',
        'storage.control.view',
        'salary.office.log',
        'access.role.log',
        'access.user.log',
        'delivery.control.contractedit',

    ];

    protected $roles = [
        'admin' => [],
        'technical.director' => [],
        'support.manager' => [],
    ];

    /**
     * заполнение ролей
     */
    private function loadRoles ()
    {
        foreach ($this->roles as $key => $val) {
            $this->roles[$key] = $this->permissions;
        }
    }

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
//        DELIVERY
        $groups = $this->getDb()->createCommand('
            SELECT group_id FROM delivery_log group by group_id
        ')->queryAll();
        foreach ($groups as $group) {
            $data = $this->getDb()->createCommand('
                SELECT * FROM delivery_log WHERE group_id = :group
            ', [':group' => $group['group_id']])->queryAll();
            $log = [
                'table' => 'delivery',
                'id' => (int)$data[0]['delivery_id'],
                'user_id' => (int)$data[0]['user_id'] ?? null,
                'type' => 'update',
                'created_at' => (int)$data[0]['created_at'] ?? time(),
            ];
            foreach ($data as $item) {
                $log['data'][] = [
                    'field' => $item['field'],
                    'old' => $item['old'],
                    'new' => $item['new']
                ];
            }
            Yii::$app->mongodb->createCommand()->insert('table_log', $log);
        }

//        ORDER_LOGISTIC_LIST
        $groups = $this->getDb()->createCommand('
            SELECT group_id FROM order_logistic_list_log group by group_id
        ')->queryAll();
        foreach ($groups as $group) {
            $data = $this->getDb()->createCommand('
                SELECT * FROM order_logistic_list_log WHERE group_id = :group
            ', [':group' => $group['group_id']])->queryAll();
            $log = [
                'table' => 'order_logistic_list',
                'id' => (int)$data[0]['list_id'],
                'user_id' => (int)$data[0]['user_id'] ?? null,
                'type' => 'update',
                'created_at' => (int)$data[0]['created_at'] ?? time(),
            ];
            foreach ($data as $item) {
                $log['data'][] = [
                    'field' => $item['field'],
                    'old' => $item['old'],
                    'new' => $item['new']
                ];
            }
            Yii::$app->mongodb->createCommand()->insert('table_log', $log);
        }

//        TODO: удалить delivery_log & order_logistic_list_log после всех проверок

        $this->loadRoles();
        parent::safeUp();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $lastData = (new \yii\db\Query())->select('created_at')->from('order_logistic_list_log')->orderBy(['created_at' => SORT_DESC])->one();

        $data = new \yii\mongodb\Query();
        $data->from('table_log');
        $data->where(['table' => 'order_logistic_list'])->andWhere(['>', 'created_at', $lastData['created_at'] ?? 0]);
        foreach ($data->batch(500) as $val) {
            $arr = [];
            foreach ($val as $item) {
                $uid = \app\helpers\Utils::uid();
                foreach ($item['data'] as $change) {
                    $arr[] = ['list_id' => $item['id'], 'user_id' => $item['user_id'] ?? null, 'group_id' => $uid,
                        'created_at' => $item['created_at'], 'field' => $change['field'], 'old' => $change['old'] ?? null,
                        'new' => $change['new'] ?? null];
                }
            }
            $this->batchInsert('order_logistic_list_log', [
                'list_id', 'user_id', 'group_id', 'created_at', 'field', 'old', 'new'
            ], $arr);
        }

        $lastData = (new \yii\db\Query())->select('created_at')->from('delivery_log')->orderBy(['created_at' => SORT_DESC])->one();

        $data = new \yii\mongodb\Query();
        $data->from('table_log');
        $data->where(['table' => 'delivery'])->andWhere(['>', 'created_at', $lastData['created_at'] ?? 0]);
        foreach ($data->batch(500) as $val) {
            $arr = [];
            foreach ($val as $item) {
                $uid = \app\helpers\Utils::uid();
                foreach ($item['data'] as $change) {
                    $arr[] = ['delivery_id' => $item['id'], 'user_id' => $item['user_id'] ?? null, 'group_id' => $uid,
                        'created_at' => $item['created_at'], 'field' => $change['field'], 'old' => $change['old'] ?? null,
                        'new' => $change['new'] ?? null];
                }
            }
            $this->batchInsert('delivery_log', [
                'delivery_id', 'user_id', 'group_id', 'created_at', 'field', 'old', 'new'
            ], $arr);
        }

        $this->loadRoles();
        parent::safeDown();
    }
}