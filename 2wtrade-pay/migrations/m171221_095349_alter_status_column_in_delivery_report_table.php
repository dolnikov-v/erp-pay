<?php
use app\components\CustomMigration as Migration;
use app\modules\deliveryreport\models\DeliveryReport;

/**
 * Class m171221_095349_alter_status_column_in_delivery_report_table
 */
class m171221_095349_alter_status_column_in_delivery_report_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn(DeliveryReport::tableName(), 'status', $this->string(50)->defaultValue('queue'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn(DeliveryReport::tableName(), 'status', $this->enum([
            'queue',
            'parsing',
            'checking',
            'error',
            'complete',
            'in_work',
            'queue_checking'])->notNull()->defaultValue('queue'));
    }
}
