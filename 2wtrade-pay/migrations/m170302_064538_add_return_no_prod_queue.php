<?php
use app\components\CustomMigration as Migration;
use app\models\AuthItem;

/**
 * Class m170302_064538_add_return_no_prod_queue
 */
class m170302_064538_add_return_no_prod_queue extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('order', 'call_center_type',
            "ENUM('order', 'new_return', 'return_no_prod', 'jeempo', 'return', 'repeat', 'still_waiting') NOT NULL DEFAULT 'order' AFTER `call_center_again`");
        $this->insert('{{%auth_item}}', array(
            'name' => 'order.index.resendincallcenterreturnnoprod',
            'type' => '2',
            'description' => 'order.index.resendincallcenterreturnnoprod',
            'created_at' => time(),
            'updated_at' => time()
        ));

        if (AuthItem::find()->where(['name' => 'country.curator'])->exists()) {
            $this->insert($this->authManager->itemChildTable, array(
                'parent' => 'country.curator',
                'child' => 'order.index.resendincallcenterreturnnoprod'
            ));
        }
        if (AuthItem::find()->where(['name' => 'project.manager'])->exists()) {
            $this->insert($this->authManager->itemChildTable, array(
                'parent' => 'project.manager',
                'child' => 'order.index.resendincallcenterreturnnoprod'
            ));
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('order', 'call_center_type',
            "ENUM('order', 'new_return', 'return_noprod', 'jeempo', 'return', 'repeat', 'still_waiting') NOT NULL DEFAULT 'order' AFTER `call_center_again`");
        if (AuthItem::find()->where(['name' => 'country.curator'])->exists()) {
            $this->delete($this->authManager->itemChildTable,
                ['child' => 'order.index.resendincallcenterreturnnoprod', 'parent' => 'country.curator']);
        }
        if (AuthItem::find()->where(['name' => 'project.manager'])->exists()) {
            $this->delete($this->authManager->itemChildTable,
                ['child' => 'order.index.resendincallcenterreturnnoprod', 'parent' => 'project.manager']);
        }
        $this->delete('{{%auth_item}}', ['name' => 'order.index.resendincallcenterreturnnoprod']);
    }
}
