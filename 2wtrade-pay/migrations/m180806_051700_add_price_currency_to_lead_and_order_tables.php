<?php

use yii\db\Migration;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;

/**
 * Handles adding price_currency to table `lead_and_order_tables`.
 */
class m180806_051700_add_price_currency_to_lead_and_order_tables extends Migration
{
    const BATCH_SIZE = 2000;
    const COUNTRY_COMBODIA = 'KH';
    const CURRENCY_USD = 'USD';
    const PRICE_LIMIT = 2000;

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        //Для комбоджийских лидов и заказов при цене < 1000 выставить price_currency = 1 (USD)
        $usdCurrency = (new \yii\db\Query())->from('currency')->where(['char_code' => self::CURRENCY_USD])->one();
        $leadQuery = (new \yii\db\Query())->from('lead')->where(['is', 'price_currency', null])->andWhere([
            'is not',
            'country',
            null
        ])->orderBy(['country' => SORT_ASC]);

        $orderIds = $leadQuery->select('order_id')->column();

        $totalCount = count($orderIds);
        $counter = 0;
        Console::startProgress($counter, $totalCount);

        foreach (array_chunk($orderIds, self::BATCH_SIZE) as $chunkIds) {
            $leads = (new \yii\db\Query())->from('lead')->where(['order_id' => $chunkIds])->all();
            $currencyIdForOrders = (new \yii\db\Query())->from('order')
                ->leftJoin('country', 'country.id = order.country_id')
                ->select(['id' => 'order.id', 'currency_id' => 'country.currency_id'])
                ->where(['order.id' => $chunkIds])
                ->all();
            $currencyIdForOrders = ArrayHelper::map($currencyIdForOrders, 'id', 'currency_id');
            $priceCurrencyIds = [];
            foreach ($leads as $lead) {
                if (isset($currencyIdForOrders[$lead['order_id']])) {
                    $price_currency = $lead['country'] == self::COUNTRY_COMBODIA && $lead['total_price'] < self::PRICE_LIMIT ? $usdCurrency['id'] : $currencyIdForOrders[$lead['order_id']];
                    if (!isset($priceCurrencyIds[$price_currency])) {
                        $priceCurrencyIds[$price_currency] = [];
                    }
                    $priceCurrencyIds[$price_currency][] = $lead['order_id'];
                }
            }
            foreach ($priceCurrencyIds as $priceCurrencyId => $ids) {
                $this->db->createCommand()->update('lead', ['price_currency' => $priceCurrencyId], ['order_id' => $ids]);
            }
            $counter += count($chunkIds);
            Console::updateProgress($counter, $totalCount);
        }
        Console::endProgress('All price_currency in `order` was updated.');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->update('lead', ['price_currency' => null], ['is not', 'price_currency', null]);
    }
}
