<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryApiClass;
use app\modules\delivery\models\Delivery;
use app\models\Country;

/**
 * Class m170623_085918_add_delivery_caex_logistics
 */
class m170623_085918_add_delivery_caex_logistics extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $params = [
            'name' => 'CaexLogisticsApi',
            'class_path' => '/caexlogistics-api/src/CaexLogisticsApi.php'
        ];
        $apiClass = DeliveryApiClass::findOne($params) ?: new DeliveryApiClass($params);
        if ($apiClass->isNewRecord) {
            $apiClass->active = 1;
            if (!$apiClass->save()) {
                return false;
            }
        }

        $country = Country::findOne([
            'name' => 'Гватемала',
            'char_code' => 'GT'
        ]);
        if (!$country) {
            return false;
        }

        $params = [
            'name' => 'CaexLogistics',
            'char_code' => 'CXL',
            'country_id' => $country->id,
        ];
        $delivery = Delivery::findOne($params) ?: new Delivery($params);
        $delivery->api_class_id = $apiClass->id;
        $delivery->active = 1;
        $delivery->brokerage_active = 0;
        $delivery->auto_sending = 0;
        return $delivery->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $country = Country::findOne([
            'name' => 'Гватемала',
            'char_code' => 'GT'
        ]);
        if (!$country) {
            return false;
        }

        $delivery = Delivery::findOne([
            'name' => 'CaexLogistics',
            'char_code' => 'CXL',
            'country_id' => $country->id,
        ]);
        $delivery && $delivery->delete();

        $apiClass = DeliveryApiClass::findOne([
            'name' => 'CaexLogisticsApi',
            'class_path' => '/caexlogistics-api/src/CaexLogisticsApi.php'
        ]);
        $apiClass && $apiClass->delete();

        return true;
    }
}
