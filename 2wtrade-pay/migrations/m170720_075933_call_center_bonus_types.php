<?php
use app\components\CustomMigration as Migration;
use app\modules\salary\models\Bonus;
use app\modules\salary\models\BonusType;
use app\modules\salary\models\Office;

/**
 * Class m170720_075933_call_center_bonus_types
 */
class m170720_075933_call_center_bonus_types extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->update(Bonus::tableName(), ['bonus_percent' => 0, 'bonus_sum_usd' => 0], ['bonus_type_id' => 2]);

        $this->addColumn(Bonus::tableName(), 'bonus_hour', $this->float());
        $this->addColumn(Bonus::tableName(), 'bonus_percent_hour', $this->float());
        $this->addColumn(BonusType::tableName(), 'office_id', $this->integer());
        $this->addForeignKey('fk_call_center_bonus_type_office_id', BonusType::tableName(), 'office_id', Office::tableName(), 'id', self::SET_NULL, self::NO_ACTION);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Bonus::tableName(), 'bonus_hour');
        $this->dropColumn(Bonus::tableName(), 'bonus_percent_hour');
        $this->dropForeignKey('fk_call_center_bonus_type_office_id', BonusType::tableName());
        $this->dropColumn(BonusType::tableName(), 'office_id');
    }
}
