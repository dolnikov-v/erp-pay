<?php
use app\components\CustomMigration as Migration;

/**
 * Class m181016_035533_add_charge_DifferentDeliveryChargeByCity
 */
class m181016_035533_add_charge_DifferentDeliveryChargeByCity extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('delivery_charges_calculator',
            [
                'name' => 'DifferentDeliveryChargeByCity',
                'class_path' => 'app\modules\delivery\components\charges\DifferentDeliveryChargeByCity',
                'active' => 1,
                'created_at' => time()
            ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete('delivery_charges_calculator', ['name' => 'DifferentDeliveryChargeByCity']);
    }
}
