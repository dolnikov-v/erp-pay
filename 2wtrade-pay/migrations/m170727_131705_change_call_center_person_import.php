<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170727_131705_change_call_center_person_import
 */
class m170727_131705_change_call_center_person_import extends Migration
{
    /**
     * @var string
     */
    protected $permission = 'callcenter.personimport.delete';

    /**
     * @var string
     */
    protected $role = 'project.manager';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropColumn('call_center_person_import', 'shift_time');
        $this->dropColumn('call_center_person_import', 'lunch_time');

        $this->addColumn('call_center_person_import', 'cell_employment_date', $this->string(100)->after('cell_skype'));
        $this->addColumn('call_center_person_import', 'cell_termination_date', $this->string(100)->after('cell_employment_date'));
        $this->addColumn('call_center_person_import', 'cell_termination_reason', $this->string(100)->after('cell_termination_date'));

        $this->addColumn('call_center_person_import_data', 'person_employment_date', $this->string(100)->after('person_skype'));
        $this->addColumn('call_center_person_import_data', 'person_termination_date', $this->string(100)->after('person_employment_date'));
        $this->addColumn('call_center_person_import_data', 'person_termination_reason', $this->string(255)->after('person_termination_date'));

        $permission = $this->authManager->createPermission($this->permission);
        $permission->description = $this->permission;
        $this->authManager->add($permission);

        $role = $this->authManager->getRole($this->role);
        if ($this->authManager->canAddChild($role, $permission)) {
            $this->authManager->addChild($role, $permission);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('call_center_person_import', 'cell_employment_date');
        $this->dropColumn('call_center_person_import', 'cell_termination_date');
        $this->dropColumn('call_center_person_import', 'cell_termination_reason');

        $this->dropColumn('call_center_person_import_data', 'person_employment_date');
        $this->dropColumn('call_center_person_import_data', 'person_termination_date');
        $this->dropColumn('call_center_person_import_data', 'person_termination_reason');

        $this->addColumn('call_center_person_import', 'shift_time', $this->string(255));
        $this->addColumn('call_center_person_import', 'lunch_time', $this->string(255));

        $role = $this->authManager->getRole($this->role);
        $permission = $this->authManager->getPermission($this->permission);
        $this->authManager->removeChild($role, $permission);
        $this->authManager->remove($permission);
    }
}
