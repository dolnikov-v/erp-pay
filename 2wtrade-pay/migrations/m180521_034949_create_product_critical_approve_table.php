<?php

use app\models\Country;
use app\models\Product;
use app\modules\catalog\models\ProductCriticalApprove;
use app\components\CustomMigration as Migration;

/**
 * Class m180521_034949_create_product_critical_approve_table
 */
class m180521_034949_create_product_critical_approve_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable(ProductCriticalApprove::tableName(), [
            'id' => $this->primaryKey(),
            'country_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
            'percent' => $this->integer()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);

        $this->addForeignKey(null, ProductCriticalApprove::tableName(), 'country_id', Country::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, ProductCriticalApprove::tableName(), 'product_id', Product::tableName(), 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable(ProductCriticalApprove::tableName());
    }
}
