<?php
use app\components\CustomMigration as Migration;

/**
 * Class m181206_090759_add_export_fact
 */
class m181206_090759_add_export_fact extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('order_export', 'type', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('order_export', 'type', $this->enum(['csv', 'excel', 'zip']));
    }
}
