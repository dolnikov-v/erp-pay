<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160629_101806_add_field_email_interval_for_notification
 */
class m160629_101806_add_field_email_interval_for_notification extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('user_notification_setting', 'email_interval',
            "ENUM('always', 'hourly', 'daily') DEFAULT NULL AFTER `active`");
        $this->createIndex(null, 'user_notification_setting', 'email_interval');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('user_notification_setting', 'email_interval');
    }
}
