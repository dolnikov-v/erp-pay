<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `order_finance_balance`.
 */
class m180808_114519_add_columns_to_order_finance_balance extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        // 'price_fulfilment', 'price_packing', 'price_package', 'price_delivery', 'price_redelivery', 'price_delivery_back', 'price_delivery_return', 'price_address_correction', 'price_cod_service', 'price_vat'
        $this->addColumn('order_finance_balance', 'prediction_price_cod', $this->float(2)->defaultValue(0)->after('price_cod'));
        $this->addColumn('order_finance_balance', 'prediction_price_storage', $this->float(2)->defaultValue(0)->after('price_storage'));
        $this->addColumn('order_finance_balance', 'prediction_price_fulfilment', $this->float(2)->defaultValue(0)->after('price_fulfilment'));
        $this->addColumn('order_finance_balance', 'prediction_price_packing', $this->float(2)->defaultValue(0)->after('price_packing'));
        $this->addColumn('order_finance_balance', 'prediction_price_package', $this->float(2)->defaultValue(0)->after('price_package'));
        $this->addColumn('order_finance_balance', 'prediction_price_delivery', $this->float(2)->defaultValue(0)->after('price_delivery'));
        $this->addColumn('order_finance_balance', 'prediction_price_redelivery', $this->float(2)->defaultValue(0)->after('price_redelivery'));
        $this->addColumn('order_finance_balance', 'prediction_price_delivery_back', $this->float(2)->defaultValue(0)->after('price_delivery_back'));
        $this->addColumn('order_finance_balance', 'prediction_price_delivery_return', $this->float(2)->defaultValue(0)->after('price_delivery_return'));
        $this->addColumn('order_finance_balance', 'prediction_price_address_correction', $this->float(2)->defaultValue(0)->after('price_address_correction'));
        $this->addColumn('order_finance_balance', 'prediction_price_cod_service', $this->float(2)->defaultValue(0)->after('price_cod_service'));
        $this->addColumn('order_finance_balance', 'prediction_price_vat', $this->float(2)->defaultValue(0)->after('price_vat'));

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('order_finance_balance', 'prediction_price_cod');
        $this->dropColumn('order_finance_balance', 'prediction_price_storage');
        $this->dropColumn('order_finance_balance', 'prediction_price_fulfilment');
        $this->dropColumn('order_finance_balance', 'prediction_price_packing');
        $this->dropColumn('order_finance_balance', 'prediction_price_package');
        $this->dropColumn('order_finance_balance', 'prediction_price_delivery');
        $this->dropColumn('order_finance_balance', 'prediction_price_redelivery');
        $this->dropColumn('order_finance_balance', 'prediction_price_delivery_back');
        $this->dropColumn('order_finance_balance', 'prediction_price_delivery_return');
        $this->dropColumn('order_finance_balance', 'prediction_price_address_correction');
        $this->dropColumn('order_finance_balance', 'prediction_price_cod_service');
        $this->dropColumn('order_finance_balance', 'prediction_price_vat');
    }
}
