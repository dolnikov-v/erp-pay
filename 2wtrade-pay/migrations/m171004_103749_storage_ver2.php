<?php
use app\components\CustomMigration as Migration;

/**
 * Class m171004_103749_storage_ver2
 */
class m171004_103749_storage_ver2 extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->renameTable('bar_codes', 'bar_code');

        $this->addColumn('storage', 'use_bar_code', $this->integer(1)->defaultValue(0));

        $this->createTable('storage_product', [
               'id' => $this->primaryKey(),
               'storage_id' => $this->integer()->notNull(),
               'product_id' => $this->integer()->notNull(),
               'balance' => $this->integer()->notNull()->defaultValue(0),
               'created_at' => $this->integer()->notNull(),
               'updated_at' => $this->integer()->notNull(),
            ]);
        $this->createIndex(null, 'storage_product', 'storage_id');
        $this->createIndex(null, 'storage_product', 'product_id');

        $this->createTable('storage_document', [
                'id' => $this->primaryKey(),
                'type' => $this->string(50)->notNull(),
                'product_id' => $this->integer()->notNull(),
                'user_id' => $this->integer(),
                'quantity' => $this->integer()->notNull(),
                'storage_id_from' => $this->integer()->defaultValue(null),
                'storage_id_to' => $this->integer()->defaultValue(null),
                'order_id' => $this->integer()->defaultValue(null),
                'bar_list_id' => $this->integer()->defaultValue(null),
                'bar_code' => $this->string(500)->defaultValue(null),
                'comment' => $this->text()->defaultValue(null),
                'created_at' => $this->integer()->notNull(),
                'updated_at' => $this->integer()->notNull(),
            ]);

        $this->createIndex(null, 'storage_document', 'product_id');
        $this->createIndex(null, 'storage_document', 'storage_id_from');
        $this->createIndex(null, 'storage_document', 'storage_id_to');
        $this->createIndex(null, 'storage_document', 'order_id');
        $this->createIndex(null, 'storage_document', 'bar_list_id');

        $this->addColumn('order_product', 'storage_id', $this->integer()->defaultValue(null));
        $this->createIndex(null, 'order_product', 'storage_id');

        $this->addColumn('bar_code', 'storage_document_id', $this->integer()->defaultValue(null));
        $this->addColumn('bar_code', 'storage_id', $this->integer()->defaultValue(null));
        $this->addColumn('bar_code', 'bar_list_id', $this->integer()->defaultValue(null));
        $this->createIndex(null, 'bar_code', 'storage_document_id');
        $this->createIndex(null, 'bar_code', 'storage_id');
        $this->createIndex(null, 'bar_code', 'bar_list_id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->renameTable('bar_code', 'bar_codes');

        $this->dropColumn('storage', 'use_bar_code');

        $this->dropTable('storage_product');
        $this->dropTable('storage_document');

        $this->dropColumn('order_product', 'storage_id');

        $this->dropColumn('bar_code', 'storage_document_id');
        $this->dropColumn('bar_code', 'storage_id');
        $this->dropColumn('bar_code', 'bar_list_id');
    }
}
