<?php
use app\components\CustomMigration as Migration;

use app\modules\delivery\models\Delivery;



/**
 * Class m161103_164907_add_cc_type_for_order
 */
class m161108_033906_delivery_payment_notification_settings extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Delivery::tableName(), 'payment_reminder_schedule', $this->enum(["never", "end-of-week", "twice-a-month", "end-of-month"])->notNull()->defaultValue("never"));
        $this->addColumn(Delivery::tableName(), 'payment_reminder_email_to', $this->string(2048)->notNull()->defaultValue(""));
        $this->addColumn(Delivery::tableName(), 'payment_reminder_email_reply_to', $this->string(2048)->notNull()->defaultValue(""));
        $this->addColumn(Delivery::tableName(), 'payment_reminder_sent_at', $this->integer(11));

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Delivery::tableName(), 'payment_reminder_schedule');
        $this->dropColumn(Delivery::tableName(), 'payment_reminder_email_to');
        $this->dropColumn(Delivery::tableName(), 'payment_reminder_email_reply_to');
        $this->dropColumn(Delivery::tableName(), 'payment_reminder_sent_at');
    }
}
