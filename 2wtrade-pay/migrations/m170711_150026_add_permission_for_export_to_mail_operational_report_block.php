<?php

use app\components\CustomMigration as Migration;

/**
 * Class m170711_150026_add_permission_for_export_to_mail_operational_report_block
 */
class m170711_150026_add_permission_for_export_to_mail_operational_report_block extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        // Доступ к экспорту операционных отчетов на почту
        $this->insert('{{%auth_item}}',array(
            'name'=>'reportoperational.exporttomail.sent',
            'type' => '2',
            'description' => 'reportoperational.exporttomail.sent',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'finance.director',
            'child' => 'reportoperational.exporttomail.sent'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'business_analyst',
            'child' => 'reportoperational.exporttomail.sent'
        ));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->authManager->itemChildTable, ['child' => 'reportoperational.exporttomail.sent', 'parent' => 'business_analyst']);
        $this->delete($this->authManager->itemChildTable, ['child' => 'reportoperational.exporttomail.sent', 'parent' => 'finance.director']);
        $this->delete('{{%auth_item}}', ['name' => 'reportoperational.exporttomail.sent']);
    }
}
