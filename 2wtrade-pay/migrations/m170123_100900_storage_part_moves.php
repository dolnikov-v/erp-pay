<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170123_100900_storage_part_moves
 */
class m170123_100900_storage_part_moves extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        /*
         * Новая таблица
         * storage_part_moves - движения в партиях
         */
        $this->createTable('storage_part_moves', [
            'id' => $this->primaryKey(),
            'type' => $this->enum(['move', 'remove'])->defaultValue('move'),
            'quantity' => $this->integer(11)->notNull(),
            'product_id' => $this->integer(11)->notNull(),
            'storage_part_from' => $this->integer(11)->notNull(),
            'storage_part_to' => $this->integer(11)->defaultValue(null),
            'user_from' => $this->integer(11)->defaultValue(null),
            'user_to' => $this->integer(11)->defaultValue(null),
            'storage_to' => $this->integer(11)->defaultValue(null),
            'comment' => $this->text()->defaultValue(null),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11),
        ]);

        $this->addForeignKey(null, 'storage_part_moves', 'product_id', 'product', 'id', Migration::CASCADE);
        $this->addForeignKey(null, 'storage_part_moves', 'storage_part_from', 'storage_part', 'id', Migration::CASCADE);
        $this->addForeignKey(null, 'storage_part_moves', 'storage_part_to', 'storage_part', 'id', Migration::CASCADE);
        $this->addForeignKey(null, 'storage_part_moves', 'user_from', 'user', 'id', Migration::CASCADE);
        $this->addForeignKey(null, 'storage_part_moves', 'user_to', 'user', 'id', Migration::CASCADE);
        $this->addForeignKey(null, 'storage_part_moves', 'storage_to', 'storage', 'id', Migration::CASCADE);
        $this->createIndex(null, 'storage_part_moves', 'storage_part_from');
        $this->createIndex(null, 'storage_part_moves', 'storage_part_to');
        $this->createIndex(null, 'storage_part_moves', 'storage_to');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('storage_part_moves');
    }
}
