<?php
use app\components\CustomMigration as Migration;
use app\models\Notification;
use app\modules\administration\models\CrontabTask;

/**
 * Class m180125_095928_check_day_approve
 */
class m180125_095928_check_day_approve extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $notification = Notification::findOne(['trigger' => Notification::TRIGGER_CALL_CENTER_CHECK_DAY_APPROVE]);
        if (!$notification instanceof Notification) {
            $notification = new Notification();
        }

        $notification->description = 'В {officeName} ({workTime}), новых лидов сегодня по направлениям: {text}.';
        $notification->trigger = Notification::TRIGGER_CALL_CENTER_CHECK_DAY_APPROVE;
        $notification->type = Notification::TYPE_INFO;
        $notification->group = Notification::GROUP_SYSTEM;
        $notification->active = Notification::ACTIVE;
        $notification->save(false);

        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_CALL_CENTER_CHECK_DAY_APPROVE]);
        if (!$crontabTask) {
            $crontabTask = new CrontabTask();
            $crontabTask->name = CrontabTask::TASK_CALL_CENTER_CHECK_DAY_APPROVE;
            $crontabTask->description = "Уведомление о проценте апрува с начала дня";
            $crontabTask->save();
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $notification = Notification::findOne(['trigger' => Notification::TRIGGER_CALL_CENTER_CHECK_DAY_APPROVE]);
        $notification->delete();
        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_CALL_CENTER_CHECK_DAY_APPROVE]);
        $crontabTask->delete();

    }
}
