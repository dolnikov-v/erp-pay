<?php
use app\components\CustomMigration as Migration;

/**
 * Class m161222_091833_add_sms_notification
 */
class m161222_091833_add_sms_notification extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('sms_notification', [
            'id' => $this->primaryKey(),
            'text' => $this->string(500),
            'trigger' => $this->string(100)->unique(),
            'active' => $this->smallInteger()->defaultValue(0),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ], $this->tableOptions);


        $this->createTable('sms_workflow', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'comment' => $this->text(),
            'active' => $this->smallInteger()->defaultValue(0),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ], $this->tableOptions);

        $this->createTable('sms_workflow_status', [
            'id' => $this->primaryKey(),
            'sms_workflow_id' => $this->integer()->notNull(),
            'sms_notification_id' => $this->integer()->notNull(),
            'status_ids' => $this->string(100),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ], $this->tableOptions);

        $this->addForeignKey(null, 'sms_workflow_status', 'sms_workflow_id', 'sms_workflow', 'id', self::CASCADE, self::CASCADE);
        $this->addForeignKey(null, 'sms_workflow_status', 'sms_notification_id', 'sms_notification', 'id', self::CASCADE, self::CASCADE);

        $this->createTable('country_sms_workflow', [
            'id' => $this->primaryKey(),
            'country_id' => $this->integer()->notNull(),
            'sms_workflow_id' => $this->integer()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ], $this->tableOptions);

        $this->addForeignKey(null, 'country_sms_workflow', 'country_id', 'country', 'id', self::CASCADE, self::CASCADE);
        $this->addForeignKey(null, 'country_sms_workflow', 'sms_workflow_id', 'sms_workflow', 'id', self::CASCADE, self::CASCADE);

        $this->createTable('order_sms_notification_request', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->notNull(),
            'order_status_id' => $this->integer(),
            'sms_notification_id' => $this->integer()->notNull(),
            'status' => "ENUM ('pending', 'in_progress', 'done', 'error') default 'pending' not null",
            'phone_to' => $this->string(100),
            'foreign_id' => $this->string(100),
            'foreign_status' => $this->string(100),
            'price' => $this->decimal(11, 5),
            'date_created' => $this->integer(),
            'date_updated' => $this->integer(),
            'date_sent' => $this->integer(),
            'uri' => $this->string(1000),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ], $this->tableOptions);

        $this->addForeignKey(null, 'order_sms_notification_request', 'order_id', 'order', 'id', self::CASCADE, self::CASCADE);
        $this->addForeignKey(null, 'order_sms_notification_request', 'order_status_id', 'order_status', 'id', self::CASCADE, self::NO_ACTION);
        $this->addForeignKey(null, 'order_sms_notification_request', 'sms_notification_id', 'sms_notification', 'id', self::CASCADE, self::NO_ACTION);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('order_sms_notification_request');
        $this->dropTable('country_sms_workflow');
        $this->dropTable('sms_workflow_status');
        $this->dropTable('sms_workflow');
        $this->dropTable('sms_notification');
    }
}
