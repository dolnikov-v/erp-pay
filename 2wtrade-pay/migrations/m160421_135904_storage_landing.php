<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160421_135904_storage_landing
 */
class m160421_135904_storage_landing extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('storage_product', 'landing_id', $this->integer()->defaultValue(null) . ' AFTER `product_id`');
        $this->addForeignKey(null, 'storage_product', 'landing_id', 'landing', 'id', self::SET_NULL, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey($this->getFkName('storage_product', 'landing_id'), 'storage_product');
        $this->dropColumn('storage_product', 'landing_id');
    }
}
