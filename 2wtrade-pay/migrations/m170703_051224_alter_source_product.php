<?php
use app\components\CustomMigration as Migration;
use app\models\SourceProduct;

/**
 * Class m170703_051224_alter_source_product
 */
class m170703_051224_alter_source_product extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropColumn(SourceProduct::tableName(), 'id');
        $this->dropColumn(SourceProduct::tableName(), 'active');
        $this->addPrimaryKey(null, SourceProduct::tableName(), ['source_id', 'product_id']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropPrimaryKey($this->getPkName(SourceProduct::tableName(), ['source_id', 'product_id']), SourceProduct::tableName());
        $this->addColumn(SourceProduct::tableName(), 'id', $this->primaryKey()->first());
        $this->addColumn(SourceProduct::tableName(), 'active', $this->integer(1)->defaultValue(1)->after('product_id'));
    }
}
