<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryApiClass;

/**
 * Class m161129_093316_add_delivery_api_row_pcpexpress
 */
class m161129_093316_add_delivery_api_row_pcpexpress extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $class = new DeliveryApiClass();
        $class->name = 'pcpExpressApi';
        $class->class_path = '/pcpexpress-api/src/pcpexpressApi.php';
        $class->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        DeliveryApiClass::find()
            ->where([
                'name' => 'pcpExpressApi',
                'class_path' => '/pcpexpress-api/src/pcpexpressApi.php'
            ])
            ->one()
            ->delete();
    }
}
