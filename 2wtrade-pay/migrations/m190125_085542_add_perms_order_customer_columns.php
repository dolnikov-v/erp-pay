<?php
use app\components\PermissionMigration as Migration;

/**
 * Class m190125_085542_add_perms_order_customer_columns
 */
class m190125_085542_add_perms_order_customer_columns extends Migration
{
    protected $permissions = [
        'order.customer_full_name',
        'order.customer_address_add',
    ];

    protected $roles = [
        'admin' => [
            'order.customer_full_name',
            'order.customer_address_add',
        ],
        'controller.analyst' => [
            'order.customer_full_name',
            'order.customer_address_add',
        ],
        'country.curator' => [
            'order.customer_full_name',
            'order.customer_address_add',
        ],
        'finance.director' => [
            'order.customer_full_name',
            'order.customer_address_add',
        ],
        'logist' => [
            'order.customer_full_name',
            'order.customer_address_add',
        ],
        'operational.director' => [
            'order.customer_full_name',
            'order.customer_address_add',
        ],
        'project.manager' => [
            'order.customer_full_name',
            'order.customer_address_add',
        ],
        'support.manager' => [
            'order.customer_full_name',
            'order.customer_address_add',
        ],
        'technical.director' => [
            'order.customer_full_name',
            'order.customer_address_add',
        ]
    ];
}
