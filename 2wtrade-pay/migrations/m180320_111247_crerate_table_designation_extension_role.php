<?php
use app\components\CustomMigration as Migration;
use app\modules\catalog\models\ExternalSource;
use app\modules\catalog\models\ExternalSourceRole;
use app\modules\salary\models\Designation;
use app\modules\salary\models\DesignationExtensionRole;

/**
 * Class m180320_111247_crerate_table_designation_extension_role
 */
class m180320_111247_crerate_table_designation_extension_role extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(DesignationExtensionRole::tableName(), [
            'id' => $this->primaryKey(),
            'designation_id' => $this->integer()->notNull(),
            'external_source_id' => $this->integer()->notNull(),
            'external_source_role_id' => $this->integer()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $this->tableOptions);

        $this->addForeignKey(null, DesignationExtensionRole::tableName(), 'designation_id', Designation::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, DesignationExtensionRole::tableName(), 'external_source_id', ExternalSource::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, DesignationExtensionRole::tableName(), 'external_source_role_id', ExternalSourceRole::tableName(), 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable(DesignationExtensionRole::tableName());
    }
}