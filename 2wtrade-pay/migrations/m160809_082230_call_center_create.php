<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160809_082230_call_center_create
 */
class m160809_082230_call_center_create extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('order', 'call_center_create', $this->smallInteger()->defaultValue(0)->after('adcombo_confirmed'));
        $this->addColumn('call_center_request', 'approved_at', $this->integer()->defaultValue(null)->after('sent_at'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('call_center_request', 'approved_at');
        $this->dropColumn('order', 'call_center_create');
    }
}
