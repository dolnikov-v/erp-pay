<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m171108_041739_clear_storage
 */
class m171108_041739_clear_storage extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        CrontabTask::deleteAll(['name' => 'storage_history_order']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = new CrontabTask();
        $crontabTask->name = 'storage_history_order';
        $crontabTask->description = 'Склады: движения товаров';
        $crontabTask->save();
    }
}
