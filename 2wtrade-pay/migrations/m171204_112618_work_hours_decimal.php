<?php
use app\components\CustomMigration as Migration;
use app\modules\salary\models\WorkTimePlan;

/**
 * Class m171204_112618_work_hours_decimal
 */
class m171204_112618_work_hours_decimal extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn(WorkTimePlan::tableName(), 'work_hours', $this->decimal(4, 2));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn(WorkTimePlan::tableName(), 'work_hours', $this->decimal(10));
    }
}
