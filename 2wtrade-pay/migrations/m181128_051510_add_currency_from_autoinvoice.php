<?php
use app\components\CustomMigration as Migration;

/**
 * Class m181128_051510_add_currency_from_autoinvoice
 */
class m181128_051510_add_currency_from_autoinvoice extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('delivery', 'invoice_currency_id', $this->integer(2)->defaultValue(\app\models\Currency::getUSD()->id ?? null));
        $this->addColumn('delivery', 'invoice_round_precision', $this->integer(2)->defaultValue(2));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('delivery', 'invoice_round_precision');
        $this->dropColumn('delivery', 'invoice_currency_id');
    }
}
