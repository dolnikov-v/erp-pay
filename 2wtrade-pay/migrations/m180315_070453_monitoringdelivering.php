<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m180315_070453_monitoringdelivering
 */
class m180315_070453_monitoringdelivering extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        CrontabTask::findOne(['name' => 'monitoring_delivery_date'])->delete();
        $crontabTask = new CrontabTask();
        $crontabTask->name = 'monitoring_delivering';
        $crontabTask->description = "Мониторинг долгой доставки и создание задач на исправление ситуации в Jira";
        $crontabTask->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        CrontabTask::findOne(['name' => 'monitoring_delivery_date'])->delete();
        CrontabTask::findOne(['name' => 'monitoring_delivering'])->delete();
    }
}
