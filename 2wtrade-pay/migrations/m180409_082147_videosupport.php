<?php
use app\components\PermissionMigration;

/**
 * Class m180409_082147_videosupport
 */
class m180409_082147_videosupport extends PermissionMigration
{
    protected $roles = [
        'support.video' => [
            'salary.person.index',
            'salary.person.view',
            'salary.penalty.index',
            'salary.penalty.edit',
            'home.index.index',
        ]
    ];
}
