<?php
use app\components\CustomMigration as Migration;
use app\modules\salary\models\Office;


/**
 * Class m171024_110418_office_address
 */
class m171024_110418_office_address extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Office::tableName(), 'address', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Office::tableName(), 'address');
    }
}
