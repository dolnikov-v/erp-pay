<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m160928_093733_new_crontab_task
 */
class m161024_132502_new_crontab_task_local_stats extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = new CrontabTask();
        $crontabTask->name = 'register_stats_local';
        $crontabTask->description = 'Запись локальной ежеминутной статистики';
        $crontabTask->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = CrontabTask::find()
            ->byName('register_stats_local')
            ->one();

        if ($crontabTask) {
            $crontabTask->delete();
        }
    }
}
