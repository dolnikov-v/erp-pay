<?php

use app\components\CustomMigration as Migration;
use app\modules\salary\models\WorkingShift;

/**
 * Handles adding crocotime_id to table `salary_working_shift_table`.
 */
class m171211_111029_add_crocotime_id_to_salary_working_shift_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
	    $this->addColumn( WorkingShift::tableName(), 'crocotime_id', $this->integer(4)->after('office_id'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
	    $this->dropColumn( WorkingShift::tableName(), 'crocotime_id' );
    }
}
