<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryChargesCalculator;
use  app\modules\order\models\OrderFinancePrediction;
use app\modules\order\models\OrderFinanceFact;

/**
 * Class m180816_123000_add_kerry_express_cambodia_charges
 */
class m180816_123000_add_kerry_express_cambodia_charges extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert(DeliveryChargesCalculator::tableName(), ['name' => 'KerryExpressCambodia', 'active' => 1, 'class_path' => 'app\modules\delivery\components\charges\kerryexpress\KerryExpressCambodia']);
        $this->addColumn(OrderFinanceFact::tableName(), 'oversea_handle', $this->float()->defaultValue(0));
        $this->addColumn(OrderFinancePrediction::tableName(), 'oversea_handle', $this->float()->defaultValue(0));
        $this->addColumn(OrderFinanceFact::tableName(), 'oversea_handle_currency_id', $this->integer());
        $this->addColumn(OrderFinancePrediction::tableName(), 'oversea_handle_currency_id', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(OrderFinancePrediction::tableName(), 'oversea_handle_currency_id');
        $this->dropColumn(OrderFinanceFact::tableName(), 'oversea_handle_currency_id');
        $this->dropColumn(OrderFinancePrediction::tableName(), 'oversea_handle');
        $this->dropColumn(OrderFinanceFact::tableName(), 'oversea_handle');
        $this->delete(DeliveryChargesCalculator::tableName(), ['name' => 'KerryExpressCambodia']);
    }
}