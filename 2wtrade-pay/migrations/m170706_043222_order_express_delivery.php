<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\Delivery;
use yii\db\Query;

/**
 * Class m170706_043222_order_express_delivery
 */
class m170706_043222_order_express_delivery extends Migration
{
    const ROLES = [
        'country.curator',
        'project.manager',
    ];

    const RULES = [
        'delivery.control.activateexpress',
        'delivery.control.deactivateexpress',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Delivery::tableName(), 'express_delivery', $this->integer(1)->defaultValue(0));

        $this->createTable('order_express_delivery', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->notNull()
        ]);

        $this->addForeignKey(null, 'order_express_delivery', 'order_id', 'order', 'id', self::CASCADE, self::RESTRICT);
        $this->createIndex(null, 'order_express_delivery', 'order_id', true);

        $query = new Query();

        foreach (self::RULES as $rule) {

            $is_rule = $query->select('*')->from($this->authManager->itemTable)
                ->where(['name' => $rule, 'type' => 2])
                ->one();

            if (!$is_rule) {
                $this->insert($this->authManager->itemTable, [
                    'name' => $rule,
                    'description' => $rule,
                    'type' => 2,
                    'created_at' => time(),
                    'updated_at' => time()]);
            }

            foreach (self::ROLES as $role) {
                $is_can = $query->select('*')->from($this->authManager->itemChildTable)->where([
                    'parent' => $role,
                    'child' => $rule
                ])->one();

                if (!$is_can) {
                    $this->insert($this->authManager->itemChildTable, [
                        'parent' => $role,
                        'child' => $rule
                    ]);
                }
            }
        }

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Delivery::tableName(), 'express_delivery');

        $this->dropTable('order_express_delivery');

        foreach (self::RULES as $rule) {
            foreach (self::ROLES as $role) {
                $this->delete($this->authManager->itemChildTable, [
                    'parent' => $role,
                    'child' => $rule
                ]);
            }
        }
    }
}
