<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160511_094738_fix_delivery
 */
class m160511_094738_fix_delivery extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('delivery', 'char_code', $this->string(3)->notNull() . ' AFTER `name`');
        $this->addColumn('delivery', 'active', $this->smallInteger()->notNull()->defaultValue(0) . ' AFTER `comment`');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('delivery', 'active');
        $this->dropColumn('delivery', 'char_code');
    }
}
