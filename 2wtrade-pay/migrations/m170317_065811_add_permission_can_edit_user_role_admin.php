<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170317_065811_add_permission_can_edit_user_role_admin
 */
class m170317_065811_add_permission_can_edit_user_role_admin extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $role = 'admin';
        $rule = 'can-edit-user-role-admin';

        $this->insert($this->authManager->itemTable, [
            'name' => $rule,
            'type' => 2,
            'description' => $rule,
            'created_at' => time(),
            'updated_at' => time()
        ]);

        $this->insert($this->authManager->itemChildTable, [
            'parent' => $role,
            'child' => $rule
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $role = 'admin';
        $rule = 'can-edit-user-role-admin';

        $this->delete($this->authManager->itemChildTable, [
            'parent' => $role,
            'child' => $rule
        ]);

        $this->delete($this->authManager->itemTable, [
            'name' => $rule,
            'type' => 2
        ]);
    }
}
