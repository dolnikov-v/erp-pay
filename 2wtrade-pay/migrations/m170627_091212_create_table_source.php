<?php

use app\components\CustomMigration as Migration;
use app\models\Source;

/**
 * Handles the creation for table `table_source`.
 */
class m170627_091212_create_table_source extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(Source::tableName(), [
            'id' => $this->primaryKey(),
            'name' => $this->string(64),
            'active' => $this->integer(1)->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $sources = [
            Source::SOURCE_ADCOMBO => 'adcombo',
            Source::SOURCE_JEEMPO => 'jeempo',
            Source::SOURCE_2WSTORE => '2wstore',
            Source::SOURCE_DIAVITA => 'diavita',
            Source::SOURCE_WTRADE_TRASH => 'wtrade_trash',
            Source::SOURCE_2WCALL => '2wcall',
            Source::SOURCE_2WDISTR => '2wDistr',
            Source::SOURCE_WTRADE_POST_SALE => 'wtrade_post_sale',
            Source::SOURCE_TEST_1 => 'test_1',
            Source::SOURCE_MY_EPC => 'myEPC',
            Source::SOURCE_TEST_3 => 'test_3',
            Source::SOURCE_TEST_4 => 'test_4',
            Source::SOURCE_TEST_5 => 'test_5',
            Source::SOURCE_MERCADOLIBRE => 'mercadolibre',
            Source::SOURCE_CALL_CENTER => 'call_center',
        ];

        foreach ($sources as $sourceName => $value) {
            $source = new Source();
            $source->name = $sourceName;
            $source->save();
        };
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('source');
    }
}
