<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\Delivery;

/**
 * Class m170118_123009_add_limit_for_list_autogeneration
 */
class m170118_123009_add_limit_for_list_autogeneration extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn(Delivery::tableName(), 'auto_list_generation_size', $this->integer()->defaultValue(200));
        Delivery::updateAll(['auto_list_generation_size' => 200], ['is', 'auto_list_generation_size', null]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn(Delivery::tableName(), 'auto_list_generation_size', $this->integer());
        Delivery::updateAll(['auto_list_generation_size' => null], ['auto_list_generation_size' => 200]);
    }
}
