<?php

use app\components\PermissionMigration as Migration;

/**
 * Class m190305_121219_send_to_check_undelivery
 */
class m190305_121219_send_to_check_undelivery extends Migration
{
    protected $permissions = [
        'delivery.control.editpretension',
        'order.index.generatepretensionlist'
    ];

    protected $roleList = [
        'admin',
        'country.curator',
        'finance.manager',
        'operational.director',
        'partners.manager',
        'project.manager',
        'support.manager',
        'technical.director',
        'product.manager',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('delivery', 'send_to_check_undelivery', $this->smallInteger());
        $this->addColumn('delivery', 'auto_pretension_generation', $this->smallInteger());
        $this->addColumn('delivery', 'auto_pretension_send', $this->smallInteger());
        $this->addColumn('delivery', 'pretension_send_offset_from', $this->integer());
        $this->addColumn('delivery', 'pretension_send_offset_to', $this->integer());

        $this->addColumn('user', 'fullname', $this->string()->after('username'));

        $this->renameColumn('order_logistic_list', 'correction', 'type');


        $this->insert('crontab_task', [
            'name' => 'delivery_generate_pretension_lists',
            'description' => 'Генерация и отправка листов претензий',
            'active' => 1,
            'created_at' => time(),
            'updated_at' => time(),
        ]);

        parent::safeUp();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('delivery', 'send_to_check_undelivery');
        $this->dropColumn('delivery', 'auto_pretension_generation');
        $this->dropColumn('delivery', 'auto_pretension_send');
        $this->dropColumn('delivery', 'pretension_send_offset_from');
        $this->dropColumn('delivery', 'pretension_send_offset_to');

        $this->dropColumn('user', 'fullname');

        $this->renameColumn('order_logistic_list', 'type', 'correction');

        $this->delete('crontab_task', ['name' => 'delivery_generate_pretension_lists']);

        parent::safeDown();
    }
}
