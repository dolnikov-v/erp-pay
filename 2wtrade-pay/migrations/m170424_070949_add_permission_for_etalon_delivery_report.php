<?php

use app\components\CustomMigration as Migration;

/**
 * Class m170424_070949_add_permission_for_etalon_delivery_report
 */
class m170424_070949_add_permission_for_etalon_delivery_report extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}',array(
            'name'=>'report.reportstandartdelivery.index',
            'type' => '2',
            'description' => 'report.reportstandartdelivery.index',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'country.curator',
            'child' => 'report.reportstandartdelivery.index'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'business_analyst',
            'child' => 'report.reportstandartdelivery.index'
        ));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->authManager->itemChildTable, ['child' => 'report.reportstandartdelivery.index', 'parent' => 'country.curator']);
        $this->delete($this->authManager->itemChildTable, ['child' => 'report.reportstandartdelivery.index', 'parent' => 'business_analyst']);
        $this->delete('{{%auth_item}}', ['name' => 'report.reportstandartdelivery.index']);
    }
}
