<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m160928_093733_new_crontab_task
 */
class m161021_160132_new_crontab_task_stats extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = new CrontabTask();
        $crontabTask->name = 'send_stats_newrelic';
        $crontabTask->description = 'Отправка статистики в newrelic.';
        $crontabTask->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = CrontabTask::find()
            ->byName('send_stats_newrelic')
            ->one();

        if ($crontabTask) {
            $crontabTask->delete();
        }
    }
}
