<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\Delivery;

/**
 * Class m161224_061134_add_auto_list_generation_size
 */
class m161224_061134_add_auto_list_generation_size extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Delivery::tableName(), 'auto_list_generation_size', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Delivery::tableName(), 'auto_list_generation_size');
    }
}
