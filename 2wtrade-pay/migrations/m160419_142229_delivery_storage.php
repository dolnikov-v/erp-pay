<?php
use app\components\CustomMigration as Migration;

class m160419_142229_delivery_storage extends Migration
{
    public function safeUp()
    {
        $this->createTable('delivery_storage', [
            'id' => $this->primaryKey(),
            'delivery_id' => $this->integer()->notNull(),
            'storage_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(null, 'delivery_storage', 'delivery_id', 'delivery', 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, 'delivery_storage', 'storage_id', 'storage', 'id', self::CASCADE, self::RESTRICT);
    }

    public function safeDown()
    {
        $this->dropTable('delivery_storage');
    }
}
