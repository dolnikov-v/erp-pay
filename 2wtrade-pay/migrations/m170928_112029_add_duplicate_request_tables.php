<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170928_112029_add_duplicate_request_tables
 */
class m170928_112029_add_duplicate_request_tables extends Migration
{
    const deliveryRequestIndices = [
        'order_id' => true,
        'status' => false,
        'tracking' => false,
        'last_update_info_at' => false,
        'api_error' => false,
        'api_error_type' => false,
        'created_at' => false,
        'updated_at' => false,
        'cron_launched_at' => false,
        'sent_at' => false,
        'second_sent_at' => false,
        'sent_clarification_at' => false,
        'returned_at' => false,
        'approved_at' => false,
        'accepted_at' => false,
        'paid_at' => false,
        'done_at' => false,
        'finance_tracking' => false,
        'partner_name' => false,
        'cs_send_response_at' => false,
        'unshipping_reason' => false,
    ];

    const callCenterRequestIndices = [
        'order_id' => true,
        'foreign_id' => false,
        'status' => false,
        'api_error' => false,
        'delivery_error' => false,
        'foreign_status' => false,
        'foreign_waiting' => false,
        'autocheck_address' => false,
        'created_at' => false,
        'updated_at' => false,
        'cron_launched_at' => false,
        'sent_at' => false,
        'approved_at' => false,
        'last_foreign_operator' => false,
        'cc_send_response_at' => false,
        'cc_update_response_at' => false,
        'analysis_trash_at' => false,
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('delivery_request_reserve', [
            'id' => $this->primaryKey(),
            'delivery_id' => $this->integer()->notNull(),
            'order_id' => $this->integer()->notNull(),
            'status' => $this->string(255)->defaultValue('pending'),
            'tracking' => $this->string(255),
            'foreign_info' => $this->text(),
            'last_update_info' => $this->string(2000),
            'last_update_info_at' => $this->integer(),
            'response_info' => $this->string(255),
            'comment' => $this->string(100),
            'api_error' => $this->string(255),
            'api_error_type' => $this->string(50)->notNull()->defaultValue('system'),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'cron_launched_at' => $this->integer(),
            'sent_at' => $this->integer(),
            'second_sent_at' => $this->integer(),
            'sent_clarification_at' => $this->integer(),
            'returned_at' => $this->integer(),
            'approved_at' => $this->integer(),
            'accepted_at' => $this->integer(),
            'paid_at' => $this->integer(),
            'done_at' => $this->integer(),
            'finance_tracking' => $this->string(30),
            'partner_name' => $this->string(100),
            'cs_send_response' => $this->text(),
            'cs_send_response_at' => $this->integer(),
            'unshipping_reason' => $this->string(32)
        ], $this->tableOptions);

        $this->addForeignKey(null, 'delivery_request_reserve', 'delivery_id', 'delivery', 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, 'delivery_request_reserve', 'order_id', 'order', 'id', self::CASCADE, self::RESTRICT);

        foreach (self::deliveryRequestIndices as $index => $unique) {
            $this->createIndex(null, 'delivery_request_reserve', $index, $unique);
        }

        $this->createTable('call_center_request_reserve', [
            'id' => $this->primaryKey(),
            'call_center_id' => $this->integer()->notNull(),
            'order_id' => $this->integer()->notNull(),
            'foreign_id' => $this->integer()->notNull(),
            'status' => $this->string(50)->defaultValue('pending'),
            'api_error' => $this->string(255),
            'delivery_error' => $this->string(255),
            'foreign_status' => $this->integer()->notNull(),
            'foreign_waiting' => $this->smallInteger(6)->defaultValue(0),
            'comment' => $this->string(1000),
            'autocheck_address' => $this->string(50),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'cron_launched_at' => $this->integer(),
            'sent_at' => $this->integer(),
            'approved_at' => $this->integer(),
            'last_foreign_operator' => $this->integer(),
            'cc_send_response' => $this->text(),
            'cc_send_response_at' => $this->integer(),
            'cc_update_response' => $this->text(),
            'cc_update_response_at' => $this->integer(),
            'analysis_trash_at' => $this->integer(),
            'analysis_trash_comment' => $this->string(255)
        ], $this->tableOptions);

        $this->addForeignKey(null, 'call_center_request_reserve', 'call_center_id', 'call_center', 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, 'call_center_request_reserve', 'order_id', 'order', 'id', self::CASCADE, self::RESTRICT);

        foreach (self::callCenterRequestIndices as $index => $unique) {
            $this->createIndex(null, 'call_center_request_reserve', $index, $unique);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('delivery_request_reserve');
        $this->dropTable('call_center_request_reserve');
    }
}
