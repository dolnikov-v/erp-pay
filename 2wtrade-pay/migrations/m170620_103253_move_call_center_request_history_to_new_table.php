<?php
use app\components\CustomMigration as Migration;
use app\modules\callcenter\models\CallCenterRequest;

/**
 * Class m170620_103253_move_call_center_request_history_to_new_table
 */
class m170620_103253_move_call_center_request_history_to_new_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->truncateTable('call_center_request_history');
        $query = CallCenterRequest::find()->select(['cc_update_response', 'id', 'call_center_id'])->asArray();
        foreach ($query->batch(1000) as $requests) {
            $rows = [];
            foreach ($requests as $request) {
                if (is_null($request['cc_update_response'])) {
                    continue;
                }
                $response = json_decode($request['cc_update_response'], true);
                $history = $response['history'];
                unset($response);
                foreach ($history as $historyItem) {
                    $rows[] = [
                        'call_center_request_id' => $request['id'],
                        'call_center_id' => $request['call_center_id'],
                        'operator_id' => $historyItem['oper'],
                        'foreign_status' => $historyItem['status_id'],
                        'called_at' => strtotime($historyItem['oe_changed']),
                    ];
                }
                if (count($rows) > 1000) {
                    $this->batchInsertIntoCallCenterRequestHistory($rows);
                    $rows = [];
                }
            }
            if (count($rows) > 0) {
                $this->batchInsertIntoCallCenterRequestHistory($rows);
            }
        }
    }

    protected function batchInsertIntoCallCenterRequestHistory($rows)
    {
        $this->batchInsert('call_center_request_history', ['call_center_request_id', 'call_center_id', 'operator_id', 'foreign_status', 'called_at'], $rows);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->truncateTable('call_center_request_history');
    }
}
