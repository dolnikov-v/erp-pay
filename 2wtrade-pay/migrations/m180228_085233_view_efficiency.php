<?php
use app\components\CustomMigration as Migration;

/**
 * Class m180228_085233_view_efficiency
 */
class m180228_085233_view_efficiency extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert($this->authManager->itemTable, [
            'name' => 'view_efficiency',
            'description' => 'view_efficiency',
            'type' => 2,
            'created_at' => time(),
            'updated_at' => time()]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->authManager->itemChildTable, [
            'child' => 'view_efficiency'
        ]);

        $this->delete($this->authManager->itemTable, [
            'name' => 'view_efficiency'
        ]);
    }
}
