<?php
use app\components\CustomMigration as Migration;

/**
 * Class m181107_034503_change_comment_to_text_in_lead
 */
class m181107_034503_change_comment_to_text_in_lead extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('lead', 'comment', $this->text());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('lead', 'comment', $this->string(200));
    }
}
