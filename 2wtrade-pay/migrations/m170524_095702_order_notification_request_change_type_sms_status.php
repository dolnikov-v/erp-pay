<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170524_095702_order_notification_request_change_type_sms_status
 */
class m170524_095702_order_notification_request_change_type_sms_status extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('{{%order_notification_request}}', 'sms_status', $this->string(50)->defaultValue('pending')->notNull());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('{{%order_notification_request}}', 'sms_status', "ENUM ('inactive', 'pending', 'in_progress', 'done', 'error') default 'pending' not null");
    }
}
