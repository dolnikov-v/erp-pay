<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170911_091239_task_clean_cron_process_pid
 */
class m170911_091239_task_clean_cron_process_pid extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('cron_process_list', 'pid', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('cron_process_list', 'pid');
    }
}
