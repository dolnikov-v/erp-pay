<?php
use app\components\CustomMigration as Migration;
use app\models\Partner;
use app\models\PartnerCountry;
use app\models\Source;

/**
 * Class m170711_100138_add_2wstore_partner
 */
class m170711_100138_add_2wstore_partner extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $wstorePartner = new Partner();
        $wstorePartner->source = '2wstore';
        $wstorePartner->foreign_id = "3";
        $wstorePartner->active = 1;
        $wstorePartner->save();

        $partnerCountries = PartnerCountry::find()
            ->leftJoin(Partner::tableName(), Partner::tableName() . '.id=' . PartnerCountry::tableName() . '.partner_id')
            ->leftJoin(Source::tableName(), Partner::tableName() . '.source =' . Source::tableName() . '.name')
            ->select(['country_id', 'source_id' => Source::tableName() . '.id'])
            ->where(['source' => 'adcombo'])
            ->distinct()
            ->asArray()
            ->all();

        foreach ($partnerCountries as $partnerCountry) {
            $wstoreCountry = new PartnerCountry();
            $wstoreCountry->country_id = $partnerCountry['country_id'];
            $wstoreCountry->partner_id = $wstorePartner->id;
            $wstoreCountry->active = 1;
            $wstoreCountry->save();
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $wstoreId = Partner::findOne(['foreign_id' => 3])->id;
        PartnerCountry::deleteAll(['partner_id' => $wstoreId]);
        Partner::deleteAll(['foreign_id' => 3]);
    }
}
