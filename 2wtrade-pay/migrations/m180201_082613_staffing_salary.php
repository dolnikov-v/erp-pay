<?php
use app\components\PermissionMigration as Migration;
use app\modules\salary\models\Staffing;
use app\modules\salary\models\BonusType;

/**
 * Class m180201_082613_staffing_salary
 */
class m180201_082613_staffing_salary extends Migration
{

    protected $permissions = ['salary.staffing.getoptionsbyoffice'];

    protected $roles = [
        'project.manager' => [
            'salary.staffing.getoptionsbyoffice'
        ],
        'general.director' => [
            'salary.staffing.getoptionsbyoffice'
        ]
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Staffing::tableName(), 'payment_type', $this->string(10)
            ->defaultValue(Staffing::PAYMENT_TYPE_FIXED)
            ->after('comment'));
        $this->addColumn(Staffing::tableName(), 'percent_base', $this->decimal(5, 2)->after('payment_type'));
        $this->addColumn(Staffing::tableName(), 'base_staffing_id', $this->integer()->after('percent_base'));

        $this->addForeignKey($this->getFkName(Staffing::tableName(), 'base_staffing_id'),
            Staffing::tableName(), 'base_staffing_id', Staffing::tableName(), 'id', self::SET_NULL, self::NO_ACTION);

        $this->createTable('salary_staffing_bonus', [
            'staffing_id' => $this->integer()->notNull(),
            'bonus_type_id' => $this->integer()->notNull(),
        ]);
        $this->addPrimaryKey(null, 'salary_staffing_bonus', ['staffing_id', 'bonus_type_id']);
        $this->createIndex(null,'salary_staffing_bonus','staffing_id');
        $this->createIndex(null,'salary_staffing_bonus','bonus_type_id');
        $this->addForeignKey(null, 'salary_staffing_bonus', 'staffing_id', Staffing::tableName(), 'id', self::CASCADE, self::CASCADE);
        $this->addForeignKey(null, 'salary_staffing_bonus', 'bonus_type_id', BonusType::tableName(), 'id', self::CASCADE, self::CASCADE);

        parent::safeUp();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey($this->getFkName(Staffing::tableName(), 'base_staffing_id'), Staffing::tableName());
        $this->dropColumn(Staffing::tableName(), 'payment_type');
        $this->dropColumn(Staffing::tableName(), 'percent_base');
        $this->dropColumn(Staffing::tableName(), 'base_staffing_id');

        $this->dropTable('salary_staffing_bonus');

        parent::safeDown();
    }
}
