<?php

use app\components\CustomMigration as Migration;
use app\models\Product;

class m160404_143731_product extends Migration
{
    public function up()
    {
        $this->dropColumn(Product::tableName(), 'foreign_id');
    }

    public function down()
    {
        $this->addColumn(Product::tableName(), 'foreign_id', 'integer(11) AFTER `id`');
    }
}
