<?php
use app\components\CustomMigration as Migration;
use app\modules\salary\models\Person;
use app\modules\callcenter\models\CallCenterWorkTimePlan;
use yii\db\Query;

/**
 * Class m170801_070742_call_center_work_time_plan
 */
class m170801_070742_call_center_work_time_plan extends Migration
{
    const ROLES = [
        'finance.director',
        'callcenter.hr',
        'project.manager',
        'salaryproject.clerk',
    ];

    const RULES = [
        'callcenter.worktimeplan.index',
        'callcenter.worktimeplan.edit',
        'callcenter.worktimeplan.delete',
        'callcenter.worktimeplan.generate',
        'callcenter.worktimeplan.deletelist',
    ];


    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $query = new Query();

        foreach (self::RULES as $rule) {

            $is_rule = $query->select('*')->from($this->authManager->itemTable)
                ->where(['name' => $rule, 'type' => 2])
                ->one();

            if (!$is_rule) {
                $this->insert($this->authManager->itemTable, [
                    'name' => $rule,
                    'description' => $rule,
                    'type' => 2,
                    'created_at' => time(),
                    'updated_at' => time()]);
            }

            foreach (self::ROLES as $role) {
                $is_can = $query->select('*')->from($this->authManager->itemChildTable)->where([
                    'parent' => $role,
                    'child' => $rule
                ])->one();

                if (!$is_can) {
                    $this->insert($this->authManager->itemChildTable, [
                        'parent' => $role,
                        'child' => $rule
                    ]);
                }
            }
        }

        $this->createTable(CallCenterWorkTimePlan::tableName(), [
            'id' => $this->primaryKey(),
            'person_id' => $this->integer(),
            'date' => $this->date(),
            'work_hours' => $this->decimal(),
            'lunch_time' => $this->string(),
            'start_at' => $this->integer(),
            'end_at' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addForeignKey(null, CallCenterWorkTimePlan::tableName(), 'person_id', Person::tableName(), 'id', self::CASCADE, self::RESTRICT);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach (self::RULES as $rule) {
            foreach (self::ROLES as $role) {
                $this->delete($this->authManager->itemChildTable, [
                    'parent' => $role,
                    'child' => $rule
                ]);
            }
        }

        $this->dropTable(CallCenterWorkTimePlan::tableName());
    }
}
