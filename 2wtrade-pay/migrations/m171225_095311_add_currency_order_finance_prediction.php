<?php
use app\components\CustomMigration as Migration;

/**
 * Class m171225_095311_add_currency_order_finance_prediction
 */
class m171225_095311_add_currency_order_finance_prediction extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('order_finance_prediction', 'currency_rate_id', $this->integer()->after('additional_prices'));

        $this->addColumn('order_finance_prediction', 'price_cod_original', $this->double()->after('price_vat'));
        $this->addColumn('order_finance_prediction', 'price_storage_original', $this->double()->after('price_cod_original'));
        $this->addColumn('order_finance_prediction', 'price_fulfilment_original', $this->double()->after('price_storage_original'));
        $this->addColumn('order_finance_prediction', 'price_packing_original', $this->double()->after('price_fulfilment_original'));
        $this->addColumn('order_finance_prediction', 'price_package_original', $this->double()->after('price_packing_original'));
        $this->addColumn('order_finance_prediction', 'price_delivery_original', $this->double()->after('price_package_original'));
        $this->addColumn('order_finance_prediction', 'price_redelivery_original', $this->double()->after('price_delivery_original'));
        $this->addColumn('order_finance_prediction', 'price_delivery_back_original', $this->double()->after('price_redelivery_original'));
        $this->addColumn('order_finance_prediction', 'price_delivery_return_original', $this->double()->after('price_delivery_back_original'));
        $this->addColumn('order_finance_prediction', 'price_address_correction_original', $this->double()->after('price_delivery_return_original'));
        $this->addColumn('order_finance_prediction', 'price_cod_service_original', $this->double()->after('price_address_correction_original'));
        $this->addColumn('order_finance_prediction', 'price_vat_original', $this->double()->after('price_cod_service_original'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('order_finance_prediction', 'currency_rate_id');

        $this->dropColumn('order_finance_prediction', 'price_cod_original');
        $this->dropColumn('order_finance_prediction', 'price_storage_original');
        $this->dropColumn('order_finance_prediction', 'price_fulfilment_original');
        $this->dropColumn('order_finance_prediction', 'price_packing_original');
        $this->dropColumn('order_finance_prediction', 'price_package_original');
        $this->dropColumn('order_finance_prediction', 'price_delivery_original');
        $this->dropColumn('order_finance_prediction', 'price_redelivery_original');
        $this->dropColumn('order_finance_prediction', 'price_delivery_back_original');
        $this->dropColumn('order_finance_prediction', 'price_delivery_return_original');
        $this->dropColumn('order_finance_prediction', 'price_address_correction_original');
        $this->dropColumn('order_finance_prediction', 'price_cod_service_original');
        $this->dropColumn('order_finance_prediction', 'price_vat_original');
    }
}
