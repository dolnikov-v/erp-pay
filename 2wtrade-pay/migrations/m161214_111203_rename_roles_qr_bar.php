<?php
use app\components\CustomMigration as Migration;

/**
 * Class m161214_111203_rename_roles_qr_bar
 */
class m161214_111203_rename_roles_qr_bar extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $auth = $this->authManager;
        $role = $auth->getRole('factory');
        $permissions = $auth->getChildren('factory');
        foreach ($permissions as $permission) {
            $auth->remove($permission);
        }
        $createBarCodes = $auth->createPermission('bar.create.index');
        $createBarCodes->description = 'Bar codes creating';
        $auth->add($createBarCodes);
        $initiateBarCodes = $auth->createPermission('bar.initiate.index');
        $initiateBarCodes->description = 'Bar codes initiating';
        $auth->add($initiateBarCodes);
        $auth->addChild($role,$createBarCodes);
        $auth->addChild($role,$initiateBarCodes);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $auth = $this->authManager;
        $role = $auth->getRole('factory');
        $permissions = $auth->getChildren('factory');
        foreach ($permissions as $permission) {
            $auth->remove($permission);
        }
        $createQrCodes = $auth->createPermission('qr.create.index');
        $createQrCodes->description = 'QR codes creating';
        $auth->add($createQrCodes);
        $initiateQrCodes = $auth->createPermission('qr.initiate.index');
        $initiateQrCodes->description = 'QR codes initiating';
        $auth->add($initiateQrCodes);
        $auth->addChild($role,$createQrCodes);
        $auth->addChild($role,$initiateQrCodes);
    }
}
