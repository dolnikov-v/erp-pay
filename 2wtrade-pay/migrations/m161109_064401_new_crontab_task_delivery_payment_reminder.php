<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m161102_123201_new_crontab_task_report_rejected
 */
class m161109_064401_new_crontab_task_delivery_payment_reminder extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = new CrontabTask();
        $crontabTask->name = 'mailer_send_delivery_payment_reminders';
        $crontabTask->description = 'Напоминания курьеркам о платежах';
        $crontabTask->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = CrontabTask::find()
            ->byName('mailer_send_delivery_payment_reminders')
            ->one();

        if ($crontabTask) {
            $crontabTask->delete();
        }
    }
}
