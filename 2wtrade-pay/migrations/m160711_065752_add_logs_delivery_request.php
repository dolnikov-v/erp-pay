<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160711_065752_add_logs_delivery_request
 */
class m160711_065752_add_logs_delivery_request extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('delivery_request_log', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->notNull(),
            'group_id' => $this->integer()->notNull(),
            'field' => $this->string()->notNull(),
            'old' => $this->string()->notNull(),
            'new' => $this->string()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(null, 'delivery_request_log', 'order_id', 'order', 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('delivery_request_log');
    }
}
