<?php
use app\components\CustomMigration as Migration;
use app\modules\widget\models\WidgetType;
use app\modules\widget\models\WidgetRole;


/**
 * Class m170421_042710_add_widget_trash
 */
class m170421_042710_add_widget_trash extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $code = 'trash';

        $roles = [
            'business_analyst',
            'country.curator'
        ];

        $this->insert(WidgetType::tableName(), [
            'code' => $code,
            'name' => 'Статистика трешей',
            'status' => WidgetType::STATUS_ACTIVE,
            'by_country' => WidgetType::BY_ALL_COUNTRIES,
        ]);

        $type = WidgetType::find()
            ->where(['code' => $code])
            ->one();

        foreach ($roles as $role) {
            $this->insert(WidgetRole::tableName(), [
                'role' => $role,
                'type_id' => $type->id
            ]);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $code = 'trash';

        $roles = [
            'business_analyst',
            'country.curator',
        ];

        $type = WidgetType::find()
            ->where(['code' => $code])
            ->one();

        foreach ($roles as $role) {
            $this->delete(WidgetRole::tableName(), [
                'role' => $role,
                'type_id' => $type->id
            ]);
        }
        $this->delete(WidgetType::tableName(), ['code' => $code]);
    }
}
