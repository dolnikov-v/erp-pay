<?php

use app\components\CustomMigration as Migration;

class m160414_084821_fix_order_fields extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('order', 'customer_ip', $this->string(100)->defaultValue(null));
        $this->alterColumn('order', 'customer_code', $this->string(100)->defaultValue(null));
        $this->alterColumn('order', 'customer_phone', $this->string(100)->defaultValue(null));
        $this->alterColumn('order', 'customer_phone_add', $this->string(100)->defaultValue(null));
        $this->alterColumn('order', 'customer_city', $this->string(100)->defaultValue(null));
        $this->alterColumn('order', 'customer_zip', $this->string(100)->defaultValue(null));
    }

    public function safeDown()
    {
        $this->alterColumn('order', 'customer_ip', $this->string(15)->defaultValue(null));
        $this->alterColumn('order', 'customer_code', $this->string(50)->defaultValue(null));
        $this->alterColumn('order', 'customer_phone', $this->string(15)->defaultValue(null));
        $this->alterColumn('order', 'customer_phone_add', $this->string(15)->defaultValue(null));
        $this->alterColumn('order', 'customer_city', $this->string(15)->defaultValue(null));
        $this->alterColumn('order', 'customer_zip', $this->string(15)->defaultValue(null));
    }
}
