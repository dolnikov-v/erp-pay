<?php

use app\components\CustomMigration as Migration;

/**
 * Handles adding permission  `report.forecast.lead.by.weekdays.index`.
 */
class m170222_130722_add_new_permission_for_forecast_lead_report extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'business_analyst',
            'child' => 'report.forecast.lead.by.weekdays.index'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'callcenter.manager',
            'child' => 'report.forecast.lead.by.weekdays.index'
        ));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->authManager->itemChildTable, ['child' => 'report.forecast.lead.by.weekdays.index', 'parent' => 'callcenter.manager']);
        $this->delete($this->authManager->itemChildTable, ['child' => 'report.forecast.lead.by.weekdays.index', 'parent' => 'business_analyst']);
    }
}
