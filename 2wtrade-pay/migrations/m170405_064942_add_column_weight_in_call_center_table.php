<?php
use app\components\CustomMigration as Migration;
use app\modules\callcenter\models\CallCenter;
/**
 * Class m170405_064942_add_column_weight_in_call_center_table
 */
class m170405_064942_add_column_weight_in_call_center_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn(CallCenter::tableName(), 'weight', 'integer(11) DEFAULT 100 AFTER api_pid');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn(CallCenter::tableName(), 'weight');
    }
}
