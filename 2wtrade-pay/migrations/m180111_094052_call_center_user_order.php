<?php
use app\components\CustomMigration as Migration;

/**
 * Class m180111_094052_call_center_user_order
 */
class m180111_094052_call_center_user_order extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('call_center_user_order', [
            'id' => $this->primaryKey(),
            'user_id' => $this->string(),
            'order_id' => $this->integer(),
            'number_of_calls' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $this->tableOptions);

        $this->createTable('call_center_product_call', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer(),
            'number_of_calls' => $this->integer(),
            'date' => $this->date(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $this->tableOptions);
        $this->addForeignKey(null, 'call_center_product_call', 'product_id', 'product', 'id', self::CASCADE, self::CASCADE);
        $this->createIndex(null, 'call_center_product_call', 'date');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('call_center_user_order');
        $this->dropTable('call_center_product_call');
    }
}
