<?php

use app\components\CustomMigration as Migration;

class m160414_131834_call_centers extends Migration
{
    public function safeUp()
    {
        $this->createTable('call_center', [
            'id' => $this->primaryKey(),
            'country_id' => $this->integer()->notNull(),
            'name' => $this->string(100)->notNull(),
            'comment' => $this->text()->defaultValue(null),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(null, 'call_center', 'country_id', 'country', 'id', self::CASCADE, self::RESTRICT);
    }

    public function safeDown()
    {
        $this->dropTable('call_center');
    }
}
