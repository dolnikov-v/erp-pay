<?php

use app\components\CustomMigration as Migration;
use app\modules\widget\models\WidgetType;
use app\modules\widget\models\WidgetRole;

/**
 * Class m170330_094201_add_widget_leads_chart
 */
class m170330_094201_add_widget_leads_chart extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $code = 'leads_chart';

        $roles = [
            'business_analyst',
            'country.curator',
            'callcenter.manager',
        ];

        $this->insert(WidgetType::tableName(), [
            'code' => $code,
            'name' => 'Лиды',
            'status' => WidgetType::STATUS_ACTIVE,
            'by_country' => WidgetType::BY_COUNTRY,
        ]);

        $type = WidgetType::find()
            ->where(['code' => $code])
            ->one();

        foreach ($roles as $role) {
            $this->insert(WidgetRole::tableName(), [
                'role' => $role,
                'type_id' => $type->id
            ]);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $code = 'leads_chart';
        $roles = [
            'business_analyst',
            'country.curator',
            'callcenter.manager',
        ];

        $type = WidgetType::find()
            ->where(['code' => $code])
            ->one();

        foreach ($roles as $role) {
            $this->delete(WidgetRole::tableName(), [
                'role' => $role,
                'type_id' => $type->id
            ]);
        }

        $this->delete(WidgetType::tableName(), ['code' => $code]);
    }
}
