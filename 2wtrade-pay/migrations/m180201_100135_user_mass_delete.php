<?php
use app\components\PermissionMigration as Migration;

/**
 * Class m180201_100135_user_mass_delete
 */
class m180201_100135_user_mass_delete extends Migration
{
    protected $permissions = ['callcenter.user.deleteusers'];
    protected $roles = [
        'salaryproject.clerk' => [
            'callcenter.user.delete',
            'callcenter.user.deleteusers'
        ]
    ];
}
