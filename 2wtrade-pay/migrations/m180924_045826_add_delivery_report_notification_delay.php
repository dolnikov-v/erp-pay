<?php

use app\components\CustomMigration as Migration;

/**
 * Class m180924_045826_add_delivery_report_notification_delay
 */
class m180924_045826_add_delivery_report_notification_delay extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('crontab_task', [
            'name' => 'delivery_report_notification_delay',
            'description' => 'Оповещение КС о 4-х дневной задержке отчетов',
            'active' => 1,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete('crontab_task', ['name' => 'delivery_report_notification_delay']);
    }
}
