<?php

use app\components\CustomMigration as Migration;

/**
 * Class m181210_083428_add_indices_to_call_center_request
 */
class m181210_083428_add_indices_to_call_center_request extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $sql = "ALTER TABLE `call_center_request` 
ADD INDEX `idx_call_center_request_call_center_id_status_cron_launched_at` (`call_center_id`, `status`, `cron_launched_at`),
ADD INDEX `idx_call_center_request_cron_launched_at` (`cron_launched_at`)
";
        $this->execute($sql);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $sql = "ALTER TABLE `call_center_request`
DROP INDEX `idx_call_center_request_call_center_id_status_cron_launched_at`,
DROP INDEX `idx_call_center_request_cron_launched_at`";
        $this->execute($sql);
    }
}
