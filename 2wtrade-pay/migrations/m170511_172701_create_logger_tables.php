<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170410_053402_add_permission_product_rating_report
 */
class m170511_172701_create_logger_tables extends Migration
{


    public function init()
    {
        // пишем в отдельную базу для логгера
        $this->db = 'logger_db';
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('log', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer(11)->notNull(),
            'message' => $this->string(4096)->notNull(),
            'tags' => $this->string(4096)->notNull(),
        ]);

        $this->createTable('tag', [
            'id'=>$this->primaryKey(),
            'name'=>$this->string(64)->notNull(),
            'value'=>$this->string(255)->notNull(),
            'log_id'=>$this->integer(11)->notNull(),
        ]);

        $this->addForeignKey(null, 'tag', 'log_id', 'log', 'id');

        $this->createIndex(null, 'tag', ['name', 'value']);


    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('tag');
        $this->dropTable('log');
    }
}
