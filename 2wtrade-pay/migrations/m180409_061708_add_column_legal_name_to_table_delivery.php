<?php

use app\components\CustomMigration as Migration;
use app\modules\delivery\models\Delivery;

/**
 * Handles adding column_legal_name to table `table_delivery`.
 */
class m180409_061708_add_column_legal_name_to_table_delivery extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn(Delivery::tableName(), 'legal_name', $this->string()->notNull()->after('name'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn(Delivery::tableName(), 'legal_name');
    }
}
