<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160816_051513_user_notification_add_column_params
 */
class m160816_051513_user_notification_add_column_params extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('user_notification','params',$this->text()->after('read'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('user_notification','params');
    }
}
