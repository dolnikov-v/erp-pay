<?php
use app\components\CustomMigration as Migration;
use app\modules\order\models\OrderFinancePrediction;
use app\modules\order\models\OrderFinanceFact;

/**
 * Class m180827_045132_delete_excess_column_in_finance
 */
class m180827_045132_delete_excess_column_in_finance extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropColumn(OrderFinancePrediction::tableName(), 'oversea_handle_currency_id');
        $this->dropColumn(OrderFinanceFact::tableName(), 'oversea_handle_currency_id');
        $this->dropColumn(OrderFinancePrediction::tableName(), 'oversea_handle');
        $this->dropColumn(OrderFinanceFact::tableName(), 'oversea_handle');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->addColumn(OrderFinanceFact::tableName(), 'oversea_handle', $this->float()->defaultValue(0));
        $this->addColumn(OrderFinancePrediction::tableName(), 'oversea_handle', $this->float()->defaultValue(0));
        $this->addColumn(OrderFinanceFact::tableName(), 'oversea_handle_currency_id', $this->integer());
        $this->addColumn(OrderFinancePrediction::tableName(), 'oversea_handle_currency_id', $this->integer());
    }
}
