<?php

use app\models\Notification;
use app\components\CustomMigration as Migration;

/**
 * Class m170713_075237_call_center_api_error_notifications
 */
class m170713_075237_call_center_api_error_notifications extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $model = new Notification([
            'description' => 'Произошла ошибка при импорте отработанного времени оператаров из Колл-центра по API . Ошибка: {error}',
            'trigger' => Notification::TRIGGER_CALL_CENTER_API_GET_OPERATOR_WORK_TIME_ERROR,
            'type' => Notification::TYPE_DANGER,
            'group' => Notification::GROUP_SYSTEM,
            'active' => Notification::ACTIVE,
        ]);
        $model->save(false);

        $model = new Notification([
            'description' => 'Произошла ошибка при импорте кол-ва звонков оператаров из Колл-центра по API. Ошибка: {error}',
            'trigger' => Notification::TRIGGER_CALL_CENTER_API_GET_OPERATOR_NUMBER_OF_CALL_ERROR,
            'type' => Notification::TYPE_DANGER,
            'group' => Notification::GROUP_SYSTEM,
            'active' => Notification::ACTIVE,
        ]);
        $model->save(false);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $models = Notification::findAll([
            'trigger' => [
                Notification::TRIGGER_CALL_CENTER_API_GET_OPERATOR_WORK_TIME_ERROR,
                Notification::TRIGGER_CALL_CENTER_API_GET_OPERATOR_NUMBER_OF_CALL_ERROR,
            ]
        ]);

        foreach ($models as $model) {
            $model->delete();
        }
    }
}
