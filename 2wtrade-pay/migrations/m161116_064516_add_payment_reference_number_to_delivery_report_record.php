<?php

use app\components\CustomMigration;
use app\modules\deliveryreport\models\DeliveryReportRecord;

/**
 * Class m161116_064516_add_payment_reference_number_to_delivery_report_record
 */
class m161116_064516_add_payment_reference_number_to_delivery_report_record extends CustomMigration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(DeliveryReportRecord::tableName(), 'payment_reference_number', $this->string(255));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(DeliveryReportRecord::tableName(), 'payment_reference_number');
    }
}
