<?php
use app\components\CustomMigration as Migration;
use app\modules\api\models\ApiLog;

class m160419_095145_fix_api_logs extends Migration
{
    public function safeUp()
    {
        $this->renameColumn('api_lead_log', 'status', 'old_status');
        $this->addColumn('api_lead_log', 'status', "ENUM('fail', 'success') DEFAULT NULL");
        $this->createIndex('idx_api_lead_log_status', 'api_lead_log', 'status');

        $logs = ApiLog::find()
            ->from('api_lead_log')
            ->all();

        foreach ($logs as $log) {
            if ($log->old_status == 0) {
                $log->status = 'fail';
            } else {
                $log->status = 'success';
            }

            $log->save(false, ['status']);
        }

        $this->dropColumn('api_lead_log', 'old_status');
    }

    public function safeDown()
    {
        $this->renameColumn('api_lead_log', 'status', 'old_status');
        $this->addColumn('api_lead_log', 'status', $this->smallInteger()->defaultValue(0));

        $logs = ApiLog::find()
            ->from('api_lead_log')
            ->all();

        foreach ($logs as $log) {
            if ($log->old_status == 'fail') {
                $log->status = 0;
            } else {
                $log->status = 1;
            }

            $log->save(false, ['status']);
        }

        $this->dropColumn('api_lead_log', 'old_status');
    }
}
