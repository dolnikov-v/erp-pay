<?php

use yii\db\Migration;

/**
 * Class m170711_120406_add_columns_to_report_operational_settings_team
 */
class m170711_120406_add_columns_to_report_operational_settings_team extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('report_operational_settings_team', 'leader_id', 'INT(11) AFTER name');
        $this->addColumn('report_operational_settings_team', 'leader_name', 'VARCHAR(255) AFTER leader_id');
        $this->alterColumn('report_operational_settings_team', 'name', $this->string(255));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('report_operational_settings_team', 'name', $this->string(64));
        $this->dropColumn('report_operational_settings_team', 'leader_name');
        $this->dropColumn('report_operational_settings_team', 'leader_id');
    }
}
