<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m160712_132034_fix_delivery_status
 */
class m160712_132034_fix_delivery_status extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('delivery_request', 'status', "ENUM('pending', 'in_progress', 'done', 'error') DEFAULT 'pending'");
        $this->addColumn('delivery_request', 'foreign_id', $this->integer()->defaultValue(null)->after('id'));
        $this->addColumn('delivery_request', 'foreign_status', $this->string(30)->defaultValue(null)->after('status'));
        $this->addColumn('delivery_request', 'tracking', $this->string(30)->defaultValue(null)->after('foreign_status'));
        $this->addColumn('order', 'customer_email', $this->string(100)->defaultValue(null)->after('customer_full_name'));

        $this->createIndex(null, 'order', 'foreign_id');
        $this->createIndex(null, 'delivery_request', 'foreign_id');

        /** @var CrontabTask $crontabTask */
        $crontabTask = CrontabTask::find()
            ->byName('delivery_get_order_info')
            ->one();

        if ($crontabTask) {
            $crontabTask->name = 'delivery_get_orders_info';
            $crontabTask->save();
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        /** @var CrontabTask $crontabTask */
        $crontabTask = CrontabTask::find()
            ->byName('delivery_get_orders_info')
            ->one();

        if ($crontabTask) {
            $crontabTask->name = 'delivery_get_order_info';
            $crontabTask->save();
        }

        $this->dropIndex($this->getIdxName('order', 'foreign_id'), 'order');
        $this->dropIndex($this->getIdxName('delivery_request', 'foreign_id'), 'delivery_request');

        $this->dropColumn('order', 'customer_email');
        $this->dropColumn('delivery_request', 'tracking');
        $this->dropColumn('delivery_request', 'foreign_status');
        $this->dropColumn('delivery_request', 'foreign_id');
        $this->alterColumn('delivery_request', 'status', "ENUM('in_progress', 'done') DEFAULT 'in_progress'");
    }
}
