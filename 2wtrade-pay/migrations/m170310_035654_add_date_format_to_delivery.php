<?php

use app\components\CustomMigration as Migration;
use app\modules\delivery\models\Delivery;

/**
 * Handles adding date_format to table `delivery`.
 */
class m170310_035654_add_date_format_to_delivery extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Delivery::tableName(), 'date_format', $this->string(50));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Delivery::tableName(), 'date_format');
    }
}
