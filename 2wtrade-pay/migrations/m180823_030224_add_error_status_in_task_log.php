<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTaskLog;

/**
 * Class m180823_030224_add_error_status_in_task_log
 */
class m180823_030224_add_error_status_in_task_log extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn(CrontabTaskLog::tableName(), 'status', $this->enum([
            CrontabTaskLog::STATUS_DONE,
            CrontabTaskLog::STATUS_IN_PROGRESS,
            CrontabTaskLog::STATUS_ERROR
        ])->defaultValue(CrontabTaskLog::STATUS_IN_PROGRESS));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn(CrontabTaskLog::tableName(), 'status', $this->enum([
            CrontabTaskLog::STATUS_DONE,
            CrontabTaskLog::STATUS_IN_PROGRESS
        ])->defaultValue(CrontabTaskLog::STATUS_IN_PROGRESS));
    }
}
