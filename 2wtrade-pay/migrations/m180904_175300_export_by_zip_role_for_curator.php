<?php
use app\components\PermissionMigration as Migration;

/**
 * Class m180904_175300_export_by_zip_role_for_curator
 */
class m180904_175300_export_by_zip_role_for_curator extends Migration
{

    protected $permissions = ['order.index.export.zip'];

    protected $roles = [
        'country.curator' => ['order.index.export.zip']
    ];
}
