<?php
use app\components\CustomMigration as Migration;
use app\models\Notification;
use app\models\UserNotificationSetting;
use app\models\NotificationUser;

/**
 * Class m180316_120637_add_productonstorage
 * миграция для задачи:
 * https://2wtrade-tasks.atlassian.net/browse/ERP-496
 */
class m180316_120637_add_productonstorage extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $notification = Notification::findOne(['trigger' => Notification::TRIGGER_PRODUCT_ON_STORAGE]);
        if (!($notification instanceof Notification)) {
            $notification = new Notification([
                'description' => 'Products into storages: {text}',
                'trigger' => Notification::TRIGGER_PRODUCT_ON_STORAGE,
                'type' => Notification::TYPE_INFO,
                'group' => Notification::GROUP_REPORT,
                'active' => Notification::ACTIVE,
            ]);
            $notification->save(false);
        } else {
            $notification->description = 'Products into storages: {text}';
            $notification->save(false);
        }

        try {
            $record = new UserNotificationSetting();
            $record->user_id = 5; // джефф
            $record->trigger = Notification::TRIGGER_PRODUCT_ON_STORAGE;
            $record->active = Notification::ACTIVE;
            $record->telegram_active = 1;
            $record->save();
        } catch (Exception $e) {
        }

        try {
            $record = new NotificationUser();
            $record->user_id = 5; // джефф
            $record->trigger = Notification::TRIGGER_PRODUCT_ON_STORAGE;
            $record->active = Notification::ACTIVE;
            $record->save();
        } catch (Exception $e) {
        }

        try {
            $record = new UserNotificationSetting();
            $record->user_id = 199; // кураев
            $record->trigger = Notification::TRIGGER_PRODUCT_ON_STORAGE;
            $record->active = Notification::ACTIVE;
            $record->telegram_active = 1;
            $record->save();
        } catch (Exception $e) {
        }

        try {
            $record = new NotificationUser();
            $record->user_id = 199; // кураев
            $record->trigger = Notification::TRIGGER_PRODUCT_ON_STORAGE;
            $record->active = Notification::ACTIVE;
            $record->save();
        } catch (Exception $e) {
        }

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

    }
}
