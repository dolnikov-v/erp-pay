<?php
use app\components\CustomMigration as Migration;
use app\modules\finance\models\ReportExpensesCountryItem;
use app\modules\finance\models\ReportExpensesCountryItemProduct;


/**
 * Class m180330_035225_report_expenses_country_item_lead_limit
 */
class m180330_035225_report_expenses_country_item_lead_limit extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropColumn(ReportExpensesCountryItem::tableName(), 'lead_limit');
        $this->addColumn(ReportExpensesCountryItemProduct::tableName(), 'lead_limit', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->addColumn(ReportExpensesCountryItem::tableName(), 'lead_limit', $this->integer());
        $this->dropColumn(ReportExpensesCountryItemProduct::tableName(), 'lead_limit');
    }
}
