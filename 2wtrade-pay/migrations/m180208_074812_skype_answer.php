<?php
use app\components\CustomMigration as Migration;

/**
 * Class m180208_074812_skype_answer
 */
class m180208_074812_skype_answer extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('skype_answer', [
            'id' => $this->primaryKey(),
            'answer' => $this->text(),
            'created_at' => $this->integer()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('skype_answer');
    }
}
