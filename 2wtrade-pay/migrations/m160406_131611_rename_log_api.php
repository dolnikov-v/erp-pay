<?php

use app\components\CustomMigration as Migration;

class m160406_131611_rename_log_api extends Migration
{
    public function up()
    {
        $this->renameTable('log_api', 'api_lead_log');
    }

    public function down()
    {
        $this->renameTable('api_lead_log', 'log_api');
    }
}
