<?php

use app\components\CustomMigration as Migration;

/**
 * Class m170417_112602_add_permission_for_get_orders_from_excel
 */
class m170417_112602_add_permission_for_get_orders_from_excel extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}',array(
            'name'=>'order.index.resendincallcenterbyexcel',
            'type' => '2',
            'description' => 'order.index.resendincallcenterbyexcel',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'country.curator',
            'child' => 'order.index.resendincallcenterbyexcel'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'business_analyst',
            'child' => 'order.index.resendincallcenterbyexcel'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'callcenter.manager',
            'child' => 'order.index.resendincallcenterbyexcel'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'project.manager',
            'child' => 'order.index.resendincallcenterbyexcel'
        ));


        $this->insert('{{%auth_item}}',array(
            'name'=>'order.index.excelorderlistclear',
            'type' => '2',
            'description' => 'order.index.excelorderlistclear',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'country.curator',
            'child' => 'order.index.excelorderlistclear'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'business_analyst',
            'child' => 'order.index.excelorderlistclear'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'callcenter.manager',
            'child' => 'order.index.excelorderlistclear'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'project.manager',
            'child' => 'order.index.excelorderlistclear'
        ));

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->authManager->itemChildTable, ['child' => 'order.index.resendincallcenterbyexcel', 'parent' => 'country.curator']);
        $this->delete($this->authManager->itemChildTable, ['child' => 'order.index.resendincallcenterbyexcel', 'parent' => 'business_analyst']);
        $this->delete($this->authManager->itemChildTable, ['child' => 'order.index.resendincallcenterbyexcel', 'parent' => 'callcenter.manager']);
        $this->delete($this->authManager->itemChildTable, ['child' => 'order.index.resendincallcenterbyexcel', 'parent' => 'project.manager']);
        $this->delete('{{%auth_item}}', ['name' => 'order.index.resendincallcenterbyexcel']);

        $this->delete($this->authManager->itemChildTable, ['child' => 'order.index.excelorderlistclear', 'parent' => 'country.curator']);
        $this->delete($this->authManager->itemChildTable, ['child' => 'order.index.excelorderlistclear', 'parent' => 'business_analyst']);
        $this->delete($this->authManager->itemChildTable, ['child' => 'order.index.excelorderlistclear', 'parent' => 'callcenter.manager']);
        $this->delete($this->authManager->itemChildTable, ['child' => 'order.index.excelorderlistclear', 'parent' => 'project.manager']);
        $this->delete('{{%auth_item}}', ['name' => 'order.index.excelorderlistclear']);
    }
}
