<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;
use app\modules\callcenter\models\CallCenterRequest;
use \yii\db\Query;


/**
 * Class m170206_060631_analysis_of_trash
 */
class m170206_060631_analysis_of_trash extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        $rules = [
            'order.index.sendanalysistrash',
        ];

        $roles = [
            'business_analyst',
            'callcenter.manager'
        ];

        $query = new Query();

        foreach ($rules as $rule) {

            $is_rule = $query->select('*')->from($this->authManager->itemTable)
                ->where([
                    'name' => $rule,
                    'type' => 2])
                ->one();

            if (!$is_rule) {
                $this->insert($this->authManager->itemTable, [
                    'name' => $rule,
                    'type' => 2,
                    'description' => $rule,
                    'created_at' => time(),
                    'updated_at' => time()]);
            }


            foreach ($roles as $role) {

                $is_role = $query->select('*')->from($this->authManager->itemTable)
                    ->where(['name' => $role, 'type' => 1])
                    ->one();

                if (!$is_role) {
                    echo "Error: No ".$role." exists";
                    die();
                }

                $is_br = $query->select('*')->from($this->authManager->itemChildTable)->where([
                    'parent' => $role,
                    'child' => $rule
                ])->one();

                if (!$is_br) {
                    $this->insert($this->authManager->itemChildTable, [
                        'parent' => $role,
                        'child' => $rule
                    ]);
                }
            }
        }


        $crontabTask = new CrontabTask();
        $crontabTask->name = 'call_center_send_analysis_trash';
        $crontabTask->description = 'Отправка трэшей на анализиз в колл-центр';
        $crontabTask->save();

        $crontabTask = new CrontabTask();
        $crontabTask->name = 'call_center_get_analysis_trash';
        $crontabTask->description = 'Получение результатов анализиза трэшей из колл-центра';
        $crontabTask->save();

        $this->addColumn(CallCenterRequest::tableName(), 'analysis_trash_at', $this->integer());
        $this->addColumn(CallCenterRequest::tableName(), 'analysis_trash_comment', $this->string());


    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

        $rules = [
            'order.index.sendanalysistrash',
        ];

        $roles = [
            'business_analyst',
            'callcenter.manager'
        ];

        foreach ($rules as $rule) {

            foreach ($roles as $role) {
                $this->delete($this->authManager->itemChildTable, [
                    'parent' => $role,
                    'child' => $rule
                ]);
            }

            $this->delete($this->authManager->itemTable, [
                'name' => $rule,
                'type' => 2]);
        }

        $crontabTask = CrontabTask::find()
            ->byName('call_center_send_analysis_trash')
            ->one();

        if ($crontabTask) {
            $crontabTask->delete();
        }

        $crontabTask = CrontabTask::find()
            ->byName('call_center_get_analysis_trash')
            ->one();

        if ($crontabTask) {
            $crontabTask->delete();
        }

        $this->dropColumn(CallCenterRequest::tableName(), 'analysis_trash_at');
        $this->dropColumn(CallCenterRequest::tableName(), 'analysis_trash_comment');
    }
}
