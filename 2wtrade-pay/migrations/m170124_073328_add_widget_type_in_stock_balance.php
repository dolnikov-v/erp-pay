<?php
use app\components\CustomMigration as Migration;
use app\modules\widget\models\WidgetType;
use app\modules\widget\models\WidgetRole;

/**
 * Class m170124_073328_add_widget_type_in_stock_balance
 */
class m170124_073328_add_widget_type_in_stock_balance extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert(WidgetType::tableName(), [
            'code' => 'in_stock_balance',
            'name' => 'Остатки товаров',
            'status' => WidgetType::STATUS_ACTIVE,
        ]);

        $type = WidgetType::find()
            ->where(['code' => 'in_stock_balance'])
            ->one();

        $this->insert(WidgetRole::tableName(), [
            'role' => 'business_analyst',
            'type_id' => $type->id,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $type = WidgetType::find()
            ->where(['code' => 'in_stock_balance'])
            ->one();

        $this->delete(WidgetRole::tableName(), [
            'role' => 'business_analyst',
            'type_id' => $type->id,
        ]);

        $this->delete(WidgetType::tableName(), ['code' => 'in_stock_balance']);
    }
}
