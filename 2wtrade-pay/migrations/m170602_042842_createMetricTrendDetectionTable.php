<?php
use app\components\CustomMigration as Migration;
use app\models\Metric;

/**
 * Class m170602_042842_createMetricTrendDetectionTable
 */
class m170602_042842_createMetricTrendDetectionTable extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

        $this->createTable(Metric::tableName(), [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'count' => $this->double(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ], $tableOptions);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable(Metric::tableName());
    }
}
