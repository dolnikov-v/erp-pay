<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m170428_041048_add_cron_for_get_info_for_checking_requests
 */
class m170428_041048_add_cron_for_get_info_for_checking_requests extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = new CrontabTask();
        $crontabTask->name = CrontabTask::TASK_CC_GET_CHECKING_ORDER_INFO;
        $crontabTask->description = 'Получение информации о заявках, которые на проверке в КЦ';
        $crontabTask->active = 1;
        $crontabTask->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        CrontabTask::deleteAll(['name' => CrontabTask::TASK_CC_GET_CHECKING_ORDER_INFO]);
    }
}
