<?php

use yii\db\Migration;

/**
 * Handles adding permissions_an_translate to table `auto_approve`.
 */
class m180521_055307_add_permissions_an_translate_to_auto_approve extends Migration
{
    private $translate = [
        'Основные настройки' => 'Main settings',
        'Список workflows' => 'List workflows',
        'Автоаппрув' => 'Auto approve',
        'Настройка автоапрува в стране' => 'Set up auto-adjust in the country',
        'Ошибка сохранения данных критического апрува по продуктам' => 'Failed to store critical product data for products',
        'Если указан критичный апрув по стране, данные по товарам будут игнорироваться. Удалите значение апрува для страны, чтобы использовать апрув по товарам' =>
        'If you specify a critical approve for the country, the product data will be ignored. Remove the value of the percent approve for the country in order to use the goods by product'
    ];

    /**
     * @inheritdoc
     */
    public function up()
    {
        foreach ($this->translate as $ruText => $enText) {
            $this->insert('i18n_source_message', ['category' => 'common', 'message' => $ruText]);
            $id = Yii::$app->db->getLastInsertID();
            $this->insert('i18n_message', ['id' => $id, 'language' => 'en-US', 'translation' => $enText]);
        }

        $this->insert('{{%auth_item}}', [
            'name' => 'catalog.country.autoapprove',
            'type' => '2',
            'description' => 'Настройка автоаппрува в стране',
            'created_at' => time(),
            'updated_at' => time()
        ]);

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        foreach ($this->translate as $ruText => $enText) {
            $this->delete('i18n_source_message', ['category' => 'common', 'message' => $ruText]);
        }

        $this->delete('{{%auth_item}}', ['name' => 'catalog.country.autoapprove']);

    }
}
