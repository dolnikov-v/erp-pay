<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170929_092011_order_logistic_list_request
 */
class m170929_092011_order_logistic_list_request extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('order_logistic_list_request', [
            'id' => $this->primaryKey(),
            'list_id' => $this->integer()->notNull(),
            'email_to' => $this->string(255),
            'status' => $this->string(255),
            'hash' => $this->string(255),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
        $this->addForeignKey(null, 'order_logistic_list_request', 'list_id', 'order_logistic_list', 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('order_logistic_list_request');
    }
}
