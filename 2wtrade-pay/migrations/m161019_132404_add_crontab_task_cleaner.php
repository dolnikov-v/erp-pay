<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m161019_132404_add_crontab_task_cleaner
 */
class m161019_132404_add_crontab_task_cleaner extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = new CrontabTask();
        $crontabTask->name = 'clean_notifications';
        $crontabTask->description = 'Очистка старых оповещений.';
        $crontabTask->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = CrontabTask::find()
            ->byName('clean_notifications')
            ->one();

        if ($crontabTask) {
            $crontabTask->delete();
        }
    }
}
