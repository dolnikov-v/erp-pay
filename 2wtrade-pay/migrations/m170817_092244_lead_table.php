<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170817_092244_lead_table
 */
class m170817_092244_lead_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('lead', [
            'order_id' => $this->integer()->unique()->notNull(),
            'foreign_id' => $this->integer()->unique()->notNull(),
            'comment' => $this->string(200)->defaultValue(null),
            'revenue' => $this->double()->notNull(),
            'ip' => $this->string(100),
            'site' => $this->string()->notNull(),
            'street' => $this->string(300),
            'postal_code' => $this->string(50),
            'address_detail' => $this->string(500),
            'name_detail' => $this->string(300),
            'city' => $this->string(100),
            'district' => $this->string(100),
            'price' => $this->double(),
            'delivery' => $this->double(),
            'phone' => $this->string(20)->notNull(),
            'is_validated_int' => $this->integer(1),
            'address' => $this->string(500),
            'prefecture' => $this->string(100),
            'name' => $this->string()->notNull(),
            'total_price' => $this->double(),
            'goods_id' => $this->string(200),
            'goods_array_json' => $this->text(),
            'ts_spawn' => $this->integer(),
            'country' => $this->string()->notNull(),
            'currency_local' => $this->string(),
            'num' => $this->integer(),
            'sub_district' => $this->string(100),
            'with_address' => $this->string(200),
            'package_id' => $this->string(50),
            'web_id' => $this->string(50),
            'partner_id' => $this->string(50),
            'email' => $this->string(50),
            'payment_type' => $this->string(20),
            'payment_amount' => $this->string(),
            'source' => $this->string(50)->defaultValue(null),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->createIndex(null, 'lead', 'foreign_id', true);
        $this->addForeignKey(null, 'lead', 'order_id', 'order', 'id');

        $this->createTable('lead_product', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
            'price' => $this->double()->notNull(),
            'quantity' => $this->integer()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $this->tableOptions);

        $this->addForeignKey(null, 'lead_product', 'order_id', 'order', 'id', self::NO_ACTION, self::NO_ACTION);
        $this->addForeignKey(null, 'lead_product', 'product_id', 'product', 'id', self::NO_ACTION, self::NO_ACTION);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('lead');
        $this->dropTable('lead_product');
    }
}
