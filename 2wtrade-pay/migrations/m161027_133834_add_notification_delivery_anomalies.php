<?php
use app\components\CustomMigration as Migration;
use app\models\Notification;

/**
 * Class m161002_071128_add_notification_product_unaccounted
 */
class m161027_133834_add_notification_delivery_anomalies extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $model = new Notification([
            'description' => '{country}: несовпадения терминальных статусов заказов с КС. Всего: {count}. Заказы: {transitions}',
            'trigger' => 'delivery.status.anomaly',
            'type' => Notification::TYPE_WARNING,
            'group' => Notification::GROUP_REPORT,
            'active' => Notification::ACTIVE,
        ]);

        $model->save(false);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $model = Notification::findOne(['trigger' => 'delivery.status.anomaly']);

        if ($model instanceof Notification) {
            $model->delete();
        }
    }
}
