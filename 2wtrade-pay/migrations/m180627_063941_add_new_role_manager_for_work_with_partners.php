<?php

use app\components\CustomMigration as Migration;
use app\models\AuthItem;
use yii\helpers\ArrayHelper;

/**
 * Class m180627_063941_add_new_role_manager_for_work_with_partners
 */
class m180627_063941_add_new_role_manager_for_work_with_partners extends Migration
{
    public $new_permissions = [
        'order.index.transfertodistributor' => 'Заказы / Групповые операции / Передать дистрибьютору'
    ];

    public $translate = [
        'Заказы / Групповые операции / Передать дистрибьютору' => 'Orders / Group operations / Transfer to distributor',
        'Менеджер по работе с партнерами' => 'Manager for work with partners',
        'Дистрибьютор' => 'Distributor',
        'Передать дистрибьютору' => 'Transfer to distributor',
        'Выбранные заказы будут переданы дистрибьютору.' => 'Selected orders will be transferred to the distributor. ',
        'Передача заказов дистрибьютору завершена.' => 'The transfer of orders to the distributor has been completed.',
        'Ошибка передачи заказа дестребьютору. Заказ должен быть в статусах "выкуп" или "Деньги получены"' => 'An error occurred while transferring the order to the distributor. The order must be in the status of "Buyout" or "Money received"',
        'Заказ #{id} не найден' => 'Order # {id} not found',
        'Страна  #{id} не найден' => 'Country # {id} not found',
        'Новый заказ #{id} не найден' => 'New order # {id} not found',
        'Ошибка передачи заказа дестребьютору. Ошибка определения токена дистрибьютора' => 'An error occurred while transferring the order to the destructor. Error determining distributor token',
    ];


    public $role = 'partners.manager';


    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        foreach ($this->translate as $ruText => $enText) {
            $this->insert('i18n_source_message', ['category' => 'common', 'message' => $ruText]);
            $id = Yii::$app->db->getLastInsertID();
            $this->insert('i18n_message', ['id' => $id, 'language' => 'en-US', 'translation' => $enText]);
        }

        $managerForWorkWithPartners = new AuthItem();
        $managerForWorkWithPartners->name = $this->role;
        $managerForWorkWithPartners->type = 1;
        $managerForWorkWithPartners->description = 'Менеджер по работе с партнерами';
        $managerForWorkWithPartners->save();

        foreach($this->new_permissions as $permission => $description) {
            $this->insert('{{%auth_item}}',array(
                'name'=>$permission,
                'type' => '2',
                'description' => $description
            ));
        }


    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->authManager->itemChildTable, ['parent' => $this->role]);

        foreach($this->new_permissions as $permission=>$description){
            $this->delete('{{%auth_item}}', ['name' => $permission]);
        }

        foreach ($this->translate as $ruText => $enText) {
            $this->delete('i18n_source_message', ['category' => 'common', 'message' => $ruText]);
        }

    }
}
