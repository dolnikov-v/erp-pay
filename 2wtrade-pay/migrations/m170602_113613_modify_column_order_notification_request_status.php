<?php
use app\components\CustomMigration as Migration;
use app\modules\order\models\OrderNotificationRequest;
/**
 * Class m170602_113613_modify_column_order_notification_request_status
 */
class m170602_113613_modify_column_order_notification_request_status extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn(OrderNotificationRequest::tableName(), 'status', $this->string(50)->defaultValue(OrderNotificationRequest::SMS_STATUS_READY));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn(OrderNotificationRequest::tableName(), 'status', $this->string(50)->defaultValue(OrderNotificationRequest::SMS_STATUS_NEW));
    }
}
