<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160815_114946_report_advertising
 */
class m160815_114946_report_advertising extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('report_advertising', [
            'id' => $this->primaryKey(),
            'foreign_id' => $this->integer(11),
            'foreign_id_file' => $this->string(11),
            'country_code' => $this->string(3),
            'country_code_file' => $this->string(3),
            'created_at' => $this->integer(11),
            'created_at_file' => $this->string(11),
            'status_id' => $this->string(5),
            'status_id_file' => $this->string(5),
            'price' => $this->double(),
            'price_file' => $this->string(50),
            'difference' => $this->string(50),
            'error' => $this->boolean(),
            'day' => $this->integer(11)
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable("report_advertising");
    }
}
