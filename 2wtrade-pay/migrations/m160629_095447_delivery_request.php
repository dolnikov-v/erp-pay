<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160629_095447_delivery_request
 */
class m160629_095447_delivery_request extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('delivery_request', [
            'id' => $this->primaryKey(),
            'delivery_id' => $this->integer()->notNull(),
            'order_id' => $this->integer()->notNull(),
            'status' => "ENUM('in_progress', 'done') DEFAULT 'in_progress'",
            'comment' => $this->string(100)->defaultValue(null),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(null, 'delivery_request', 'delivery_id', 'delivery', 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, 'delivery_request', 'order_id', 'order', 'id', self::CASCADE, self::RESTRICT);
        $this->createIndex(null, 'delivery_request', 'status');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('delivery_request');
    }
}
