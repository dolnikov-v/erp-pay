<?php

use app\components\CustomMigration as Migration;

/**
 * Class m170526_073102_add_permission_for_operators_penalty
 */
class m170526_073102_add_permission_for_operators_penalty extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        // Доступ к агрегирующему пункту меню в "заказах" по работе с Excel
        $this->insert('{{%auth_item}}',array(
            'name'=>'order.ordersbyexcel.index',
            'type' => '2',
            'description' => 'order.ordersbyexcel.index',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'country.curator',
            'child' => 'order.ordersbyexcel.index'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'business_analyst',
            'child' => 'order.ordersbyexcel.index'
        ));

        // Доступ к штрафам операторов в заказах
        $this->insert('{{%auth_item}}',array(
            'name'=>'order.unshipping.index',
            'type' => '2',
            'description' => 'order.unshipping.index',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'country.curator',
            'child' => 'order.unshipping.index'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'business_analyst',
            'child' => 'order.unshipping.index'
        ));

        // Доступ к справочнику штрафов операторов
        $this->insert('{{%auth_item}}',array(
            'name'=>'catalog.operatorspenalty.index',
            'type' => '2',
            'description' => 'catalog.operatorspenalty.index',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'country.curator',
            'child' => 'catalog.operatorspenalty.index'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'business_analyst',
            'child' => 'catalog.operatorspenalty.index'
        ));

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->authManager->itemChildTable, ['child' => 'order.ordersbyexcel.index', 'parent' => 'country.curator']);
        $this->delete($this->authManager->itemChildTable, ['child' => 'order.ordersbyexcel.index', 'parent' => 'business_analyst']);
        $this->delete('{{%auth_item}}', ['name' => 'order.ordersbyexcel.index']);

        $this->delete($this->authManager->itemChildTable, ['child' => 'order.unshipping.index', 'parent' => 'country.curator']);
        $this->delete($this->authManager->itemChildTable, ['child' => 'order.unshipping.index', 'parent' => 'business_analyst']);
        $this->delete('{{%auth_item}}', ['name' => 'order.unshipping.index']);

        $this->delete($this->authManager->itemChildTable, ['child' => 'catalog.operatorspenalty.index', 'parent' => 'country.curator']);
        $this->delete($this->authManager->itemChildTable, ['child' => 'catalog.operatorspenalty.index', 'parent' => 'business_analyst']);
        $this->delete('{{%auth_item}}', ['name' => 'catalog.operatorspenalty.index']);
    }
}
