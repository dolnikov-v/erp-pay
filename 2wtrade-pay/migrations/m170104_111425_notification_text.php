<?php
use app\components\CustomMigration as Migration;
use app\models\Notification;

/**
 * Class m170104_111425_notification_text
 */
class m170104_111425_notification_text extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $model = Notification::findOne(['trigger' => 'product.on.storage']);
        $model->description = 'Товары на складах: {text}.';
        $model->save(false);

        $model = Notification::findOne(['trigger' => 'no.products.for.order']);
        $model->description = 'Заказам требуются товары: {text}.';
        $model->save(false);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $model = Notification::findOne(['trigger' => 'product.on.storage']);
        $model->description = '{text}.';
        $model->save(false);

        $model = Notification::findOne(['trigger' => 'no.products.for.order']);
        $model->description = '{text}.';
        $model->save(false);
    }
}
