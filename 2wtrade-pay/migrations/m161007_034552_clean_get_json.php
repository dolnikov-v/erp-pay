<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m161007_034552_clean_get_json
 */
class m161007_034552_clean_get_json extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = CrontabTask::find()
            ->byName('get_json')
            ->one();

        if ($crontabTask) {
            $crontabTask->delete();
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = new CrontabTask();
        $crontabTask->name = 'get_json';
        $crontabTask->description = 'Фигня.';
        $crontabTask->save();
    }
}
