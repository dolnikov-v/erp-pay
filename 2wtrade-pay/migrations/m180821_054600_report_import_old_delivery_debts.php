<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;
use app\modules\report\models\ReportDeliveryDebts;

/**
 * Class m180821_054600_report_import_old_delivery_debts
 */
class m180821_054600_report_import_old_delivery_debts extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = CrontabTask::find()
            ->byName(CrontabTask::TASK_REPORT_IMPORT_OLD_DELIVERY_DEBTS)
            ->one();

        if (!$crontabTask) {
            $crontabTask = new CrontabTask();
            $crontabTask->name = CrontabTask::TASK_REPORT_IMPORT_OLD_DELIVERY_DEBTS;
            $crontabTask->description = 'Импорт данных дебиторской задолжности из tracking.m2corp.ru';
            $crontabTask->save();
        }

        $this->addColumn(ReportDeliveryDebts::tableName(), 'is_old_data', $this->boolean()->defaultValue(0));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = CrontabTask::find()
            ->byName(CrontabTask::TASK_REPORT_IMPORT_OLD_DELIVERY_DEBTS)
            ->one();

        if ($crontabTask) {
            $crontabTask->delete();
        }

        $this->dropColumn(ReportDeliveryDebts::tableName(), 'is_old_data');
    }
}
