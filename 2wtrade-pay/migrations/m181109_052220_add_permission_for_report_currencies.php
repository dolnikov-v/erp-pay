<?php
use app\components\PermissionMigration as Migration;

/**
 * Class m181109_052220_add_permission_for_report_currencies
 */
class m181109_052220_add_permission_for_report_currencies extends Migration
{
    protected $permissions = ['delivery.control.setcurrencies'];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $query = new \yii\db\Query();
        $permissions = $query->from($this->authManager->itemChildTable)->where([
            'child' => 'deliveryreport.report.view'
        ])->all();
        foreach ($permissions as $permission) {
            $this->roles[$permission['parent']] = $this->permissions;
        }

        parent::safeUp();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

        parent::safeDown();
    }
}
