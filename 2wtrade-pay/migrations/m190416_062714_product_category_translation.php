<?php
use app\components\CustomMigration as Migration;

/**
 * Class m190416_062714_product_category_translation
 */
class m190416_062714_product_category_translation extends Migration
{
    private static $translations = [
        "Категория товаров" => "Product category",
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        foreach (self::$translations as $ruText => $enText) {
            $this->insert('i18n_source_message', ['category' => 'common', 'message' => $ruText]);
            $id = Yii::$app->db->getLastInsertID();
            $this->insert('i18n_message', ['id' => $id, 'language' => 'en-US', 'translation' => $enText]);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach (self::$translations as $ruText => $enText) {
            $this->delete('i18n_source_message', ['category' => 'common', 'message' => $ruText]);
        }
    }
}
