<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170614_054416_add_delivery_sku_table
 */
class m170614_054416_add_delivery_sku_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('delivery_sku', [
            'id' => $this->primaryKey(),
            'delivery_id' => $this->integer(),
            'product_id' => $this->integer(),
            'sku' => $this->string(255),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $this->tableOptions);

        $this->addForeignKey(null, 'delivery_sku', 'delivery_id', 'delivery', 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, 'delivery_sku', 'product_id', 'product', 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('delivery_sku');
    }
}
