<?php

use yii\db\Migration;

/**
 * Handles the dropping for table `columns_delivery`.
 */
class m180910_053941_drop_columns_delivery extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropColumn('delivery', 'fulfillment_cost');
        $this->dropColumn('delivery', 'nds');
        $this->dropColumn('delivery', 'fulfillment_nds');
        $this->dropColumn('delivery', 'delivery_cost_percent');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->addColumn('delivery', 'fulfillment_cost', $this->double()->defaultValue(0));
        $this->addColumn('delivery', 'nds', $this->double()->defaultValue(0));
        $this->addColumn('delivery', 'fulfillment_nds', $this->double()->defaultValue(0));
        $this->addColumn('delivery', 'delivery_cost_percent', $this->double()->defaultValue(0));
    }
}
