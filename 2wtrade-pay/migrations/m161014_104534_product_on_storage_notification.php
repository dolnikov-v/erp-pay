<?php
use app\components\CustomMigration as Migration;
use app\models\Notification;

/**
 * Class m161014_104534_product_on_storage_notification
 */
class m161014_104534_product_on_storage_notification extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $model = new Notification([
            'description' => '{text}.',
            'trigger' => 'product.on.storage',
            'type' => Notification::TYPE_INFO,
            'group' => Notification::GROUP_REPORT,
            'active' => Notification::ACTIVE,
        ]);

        $model->save(false);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $model = Notification::findOne(['trigger' => 'product.on.storage']);

        if ($model instanceof Notification) {
            $model->delete();
        }
    }
}
