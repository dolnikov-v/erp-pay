<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170717_104656_add_permission_for_call_center_person_import
 */
class m170717_104656_add_permission_for_call_center_person_import extends Migration
{
    protected $permissions = [
        'callcenter.personimport.index',
        'callcenter.personimport.import',
        'callcenter.personimport.process',
        'callcenter.personimport.recordedit',
        'callcenter.personimport.recordinclude',
        'callcenter.personimport.recordexclude',
        'callcenter.personimport.recordgroupinclude',
        'callcenter.personimport.recordgroupexclude',
        'callcenter.personimport.recordgroupapprovedhead',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $auth = $this->authManager;
        foreach ($this->permissions as $permission) {
            $authPermission = $auth->createPermission($permission);
            $authPermission->description = $permission;
            $auth->add($authPermission);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach ($this->permissions as $permission) {
            $this->delete($this->authManager->itemTable, ['name' => $permission]);
        }
    }
}