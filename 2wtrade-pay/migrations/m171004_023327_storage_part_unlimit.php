<?php
use app\components\CustomMigration as Migration;

/**
 * Class m171004_023327_storage_part_unlimit
 */
class m171004_023327_storage_part_unlimit extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('storage_part', 'unlimit', $this->boolean()->defaultValue(false));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('storage_part', 'unlimit');
    }
}
