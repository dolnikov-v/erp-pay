<?php
use app\components\PermissionMigration as Migration;
use app\modules\salary\models\Person;

/**
 * Class m171117_055259_person_link
 */
class m171117_055259_person_link extends Migration
{

    protected $permissions = [
        'salary.person.deletelink'
    ];

    protected $roles = [
        'admin' => [
            'salary.person.deletelink'
        ],
        'callcenter.hr' => [
            'salary.person.deletelink'
        ],
        'callcenter.leader' => [
            'salary.person.deletelink'
        ],
        'callcenter.manager' => [
            'salary.person.deletelink'
        ],
        'project.manager' => [
            'salary.person.deletelink'
        ],
        'salaryproject.clerk' => [
            'salary.person.deletelink'
        ],
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('salary_person_link', [
            'id' => $this->primaryKey(),
            'person_id' => $this->integer()->notNull(),
            'type' => $this->string(),
            'foreign_id' => $this->string(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $this->tableOptions);
        $this->addForeignKey(null, 'salary_person_link', 'person_id', 'salary_person', 'id', self::CASCADE, self::NO_ACTION);

        $persons = Person::find()
            ->where(['is not', 'crocotime_employee_id', null])
            ->all();

        $data = [];
        foreach ($persons as $person) {
            $data[] = [
                'person_id' => $person->id,
                'type' => 'crocotime',
                'foreign_id' => $person->crocotime_employee_id,
                'created_at' => time(),
                'updated_at' => time(),
            ];
        }

        if ($data) {
            Yii::$app->db->createCommand()->batchInsert(
                'salary_person_link',
                [
                    'person_id',
                    'type',
                    'foreign_id',
                    'created_at',
                    'updated_at',
                ],
                $data
            )->execute();
        }

        parent::safeUp();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('salary_person_link');
        parent::safeDown();
    }
}
