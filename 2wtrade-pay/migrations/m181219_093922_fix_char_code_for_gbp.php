<?php
use app\components\CustomMigration as Migration;

/**
 * Class m181219_093922_fix_char_code_for_gbp
 */
class m181219_093922_fix_char_code_for_gbp extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->update('currency', ['char_code' => 'GBP'], ['char_code' => 'GBR']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->update('currency', ['char_code' => 'GBR'], ['char_code' => 'GBP']);
    }
}
