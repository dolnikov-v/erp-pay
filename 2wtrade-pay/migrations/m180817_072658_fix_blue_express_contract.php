<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryContract;
use app\modules\delivery\models\DeliveryChargesCalculator;

/**
 * Class m180817_072658_fix_blue_express_contract
 *
 * меняем шаблон для одной КС
 *
 */
class m180817_072658_fix_blue_express_contract extends Migration
{
    const CONTRACT_ID = 13;
    const OLD_CALC = 'DeliveryChargeByZip';
    const NEW_CALC = 'DeliveryChargeByCity';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $contract = DeliveryContract::findOne(self::CONTRACT_ID);

        $calcOld = DeliveryChargesCalculator::findOne(['name' => self::OLD_CALC]);
        $calcNew = DeliveryChargesCalculator::findOne(['name' => self::NEW_CALC]);

        if ($contract && $calcNew && $calcOld) {
            $contract->charges_calculator_id = $calcNew->id;
            $contract->charges_values = str_replace($calcOld->name, $calcNew->name, $contract->charges_values);
            $contract->save(false, ['charges_calculator_id', 'charges_values']);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $contract = DeliveryContract::findOne(self::CONTRACT_ID);

        $calcOld = DeliveryChargesCalculator::findOne(['name' => self::OLD_CALC]);
        $calcNew = DeliveryChargesCalculator::findOne(['name' => self::NEW_CALC]);

        if ($contract && $calcNew && $calcOld) {
            $contract->charges_calculator_id = $calcOld->id;
            $contract->charges_values = str_replace($calcNew->name, $calcOld->name, $contract->charges_values);
            $contract->save(false, ['charges_calculator_id', 'charges_values']);
        }
    }
}
