<?php
use app\components\CustomMigration as Migration;

/**
 * Class m180221_032430_translate_new_rating_titles
 */
class m180221_032430_translate_new_rating_titles extends Migration
{

    private static $translations = [
        "Рейтинг товаров по выкупу" => "Product rating on buyout",
        "Рейтинг товаров по вык. чеку" => "Product rating on buyout check"
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        foreach(self::$translations as $ruText => $enText) {
            $this->insert('i18n_source_message', ['category' => 'common', 'message' => $ruText]);
            $id = Yii::$app->db->getLastInsertID();
            $this->insert('i18n_message', ['id' => $id, 'language' => 'en-US', 'translation' => $enText]);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach (self::$translations as $ruText => $enText) {
            $this->delete('i18n_source_message', ['category' => 'common', 'message' => $ruText]);
        }
    }
}
