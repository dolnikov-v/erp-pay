<?php

use yii\db\Migration;
use app\modules\order\models\Order;
/**
 * Handles adding column customer_address_addition to table `order`.
 */
class m170414_065638_add_column_customer_address_additional_to_order extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn(Order::tableName(), 'customer_address_additional', $this->string('255')->defaultValue(NULL)->after('customer_address'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn(Order::tableName(), 'customer_address_additional');
    }
}
