<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170317_052215_add_user_role_securitymanager
 */
class m170317_052215_add_user_role_securitymanager extends Migration
{
    public function safeUp()
    {
        $role = 'security.manager';
        $rules = [
            'home.index.index',
            'access.user.index',
            'access.user.ping',
            'access.user.view',
            'access.user.edit',
            'access.user.block',
            'access.user.unblock',
            'access.user.setcountry',
        ];

        $this->insert($this->authManager->itemTable, [
            'name' => $role,
            'type' => 1,
            'description' => 'Менеджер по безопасности',
            'created_at' => time(),
            'updated_at' => time()
        ]);

        foreach ($rules as $rule) {

            $this->insert($this->authManager->itemChildTable, [
                'parent' => $role,
                'child' => $rule
            ]);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

        $role = 'security.manager';
        $rules = [
            'home.index.index',
            'access.user.index',
            'access.user.ping',
            'access.user.view',
            'access.user.edit',
            'access.user.blocked',
            'access.user.unblocked',
            'access.user.setcountry',
        ];

        foreach ($rules as $rule) {

            $this->delete($this->authManager->itemChildTable, [
                'parent' => $role,
                'child' => $rule
            ]);
        }

        $this->delete($this->authManager->itemTable, [
            'name' => $role,
            'type' => 1
        ]);
    }
}
