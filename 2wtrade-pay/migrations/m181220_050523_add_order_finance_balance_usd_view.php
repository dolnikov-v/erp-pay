<?php

use app\components\CustomMigration as Migration;

/**
 * Class m181220_050523_add_order_finance_balance_usd_view
 */
class m181220_050523_add_order_finance_balance_usd_view extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('db_events', 'primary_key', $this->string(1000));
        $this->createIndex($this->getIdxName('db_events', 'created_at'), 'db_events', 'created_at');
        $sql = <<<SQL
CREATE OR REPLACE VIEW `order_finance_balance_usd` AS
  SELECT
    DISTINCT `order_finance_balance`.delivery_request_id,
    `order_finance_balance`.order_id,
    CASE WHEN `order_finance_balance`.price_cod IS NOT NULL AND `order_finance_balance`.price_cod != 0
      THEN CASE WHEN price_cod_currency_rate_temp.rate IS NOT NULL AND price_cod_currency_rate_temp.rate != 0
        THEN `order_finance_balance`.price_cod / price_cod_currency_rate_temp.rate
           ELSE convertToCurrencyByCountry(`order_finance_balance`.price_cod,
                                           COALESCE(`order_finance_balance`.price_cod_currency_id,
                                                    `order`.price_currency,
                                                    `country`.currency_id), `usd_currency`.id) END
    ELSE 0 END AS `price_cod`,
    CASE WHEN `order_finance_balance`.price_storage IS NOT NULL AND `order_finance_balance`.price_storage != 0
      THEN CASE WHEN price_storage_currency_rate_temp.rate IS NOT NULL AND price_storage_currency_rate_temp.rate != 0
        THEN `order_finance_balance`.price_storage / price_storage_currency_rate_temp.rate
           ELSE convertToCurrencyByCountry(`order_finance_balance`.price_storage,
                                           COALESCE(`order_finance_balance`.price_storage_currency_id,
                                                    `order`.price_currency,
                                                    `country`.currency_id), `usd_currency`.id) END
    ELSE 0 END AS `price_storage`,
    CASE WHEN `order_finance_balance`.price_fulfilment IS NOT NULL AND `order_finance_balance`.price_fulfilment != 0
      THEN CASE WHEN price_fulfilment_currency_rate_temp.rate IS NOT NULL AND
                     price_fulfilment_currency_rate_temp.rate != 0
        THEN `order_finance_balance`.price_fulfilment / price_fulfilment_currency_rate_temp.rate
           ELSE convertToCurrencyByCountry(`order_finance_balance`.price_fulfilment,
                                           COALESCE(`order_finance_balance`.price_fulfilment_currency_id,
                                                    `order`.price_currency,
                                                    `country`.currency_id), `usd_currency`.id) END
    ELSE 0 END AS `price_fulfilment`,
    CASE WHEN `order_finance_balance`.price_packing IS NOT NULL AND `order_finance_balance`.price_packing != 0
      THEN CASE WHEN price_packing_currency_rate_temp.rate IS NOT NULL AND price_packing_currency_rate_temp.rate != 0
        THEN `order_finance_balance`.price_packing / price_packing_currency_rate_temp.rate
           ELSE convertToCurrencyByCountry(`order_finance_balance`.price_packing,
                                           COALESCE(`order_finance_balance`.price_packing_currency_id,
                                                    `order`.price_currency,
                                                    `country`.currency_id), `usd_currency`.id) END
    ELSE 0 END AS `price_packing`,
    CASE WHEN `order_finance_balance`.price_package IS NOT NULL AND `order_finance_balance`.price_package != 0
      THEN CASE WHEN price_package_currency_rate_temp.rate IS NOT NULL AND price_package_currency_rate_temp.rate != 0
        THEN `order_finance_balance`.price_package / price_package_currency_rate_temp.rate
           ELSE convertToCurrencyByCountry(`order_finance_balance`.price_package,
                                           COALESCE(`order_finance_balance`.price_package_currency_id,
                                                    `order`.price_currency,
                                                    `country`.currency_id), `usd_currency`.id) END
    ELSE 0 END AS `price_package`,
    CASE WHEN `order_finance_balance`.price_delivery IS NOT NULL AND `order_finance_balance`.price_delivery != 0
      THEN CASE WHEN price_delivery_currency_rate_temp.rate IS NOT NULL AND price_delivery_currency_rate_temp.rate != 0
        THEN `order_finance_balance`.price_delivery / price_delivery_currency_rate_temp.rate
           ELSE convertToCurrencyByCountry(`order_finance_balance`.price_delivery,
                                           COALESCE(`order_finance_balance`.price_delivery_currency_id,
                                                    `order`.price_currency,
                                                    `country`.currency_id), `usd_currency`.id) END
    ELSE 0 END AS `price_delivery`,
    CASE WHEN `order_finance_balance`.price_redelivery IS NOT NULL AND `order_finance_balance`.price_redelivery != 0
      THEN CASE WHEN price_redelivery_currency_rate_temp.rate IS NOT NULL AND
                     price_redelivery_currency_rate_temp.rate != 0
        THEN `order_finance_balance`.price_redelivery / price_redelivery_currency_rate_temp.rate
           ELSE convertToCurrencyByCountry(`order_finance_balance`.price_redelivery,
                                           COALESCE(`order_finance_balance`.price_redelivery_currency_id,
                                                    `order`.price_currency,
                                                    `country`.currency_id), `usd_currency`.id) END
    ELSE 0 END AS `price_redelivery`,
    CASE WHEN `order_finance_balance`.price_delivery_back IS NOT NULL AND
              `order_finance_balance`.price_delivery_back != 0
      THEN CASE WHEN price_delivery_back_currency_rate_temp.rate IS NOT NULL AND
                     price_delivery_back_currency_rate_temp.rate != 0
        THEN `order_finance_balance`.price_delivery_back / price_delivery_back_currency_rate_temp.rate
           ELSE convertToCurrencyByCountry(`order_finance_balance`.price_delivery_back,
                                           COALESCE(`order_finance_balance`.price_delivery_back_currency_id,
                                                    `order`.price_currency,
                                                    `country`.currency_id), `usd_currency`.id) END
    ELSE 0 END AS `price_delivery_back`,
    CASE WHEN `order_finance_balance`.price_delivery_return IS NOT NULL AND
              `order_finance_balance`.price_delivery_return != 0
      THEN CASE WHEN price_delivery_return_currency_rate_temp.rate IS NOT NULL AND
                     price_delivery_return_currency_rate_temp.rate != 0
        THEN `order_finance_balance`.price_delivery_return / price_delivery_return_currency_rate_temp.rate
           ELSE convertToCurrencyByCountry(`order_finance_balance`.price_delivery_return,
                                           COALESCE(`order_finance_balance`.price_delivery_return_currency_id,
                                                    `order`.price_currency,
                                                    `country`.currency_id), `usd_currency`.id) END
    ELSE 0 END AS `price_delivery_return`,
    CASE WHEN `order_finance_balance`.price_address_correction IS NOT NULL AND
              `order_finance_balance`.price_address_correction != 0
      THEN CASE WHEN price_address_correction_currency_rate_temp.rate IS NOT NULL AND
                     price_address_correction_currency_rate_temp.rate != 0
        THEN `order_finance_balance`.price_address_correction / price_address_correction_currency_rate_temp.rate
           ELSE convertToCurrencyByCountry(`order_finance_balance`.price_address_correction,
                                           COALESCE(`order_finance_balance`.price_address_correction_currency_id,
                                                    `order`.price_currency,
                                                    `country`.currency_id), `usd_currency`.id) END
    ELSE 0 END AS `price_address_correction`,
    CASE WHEN `order_finance_balance`.price_cod_service IS NOT NULL AND `order_finance_balance`.price_cod_service != 0
      THEN CASE WHEN price_cod_service_currency_rate_temp.rate IS NOT NULL AND
                     price_cod_service_currency_rate_temp.rate != 0
        THEN `order_finance_balance`.price_cod_service / price_cod_service_currency_rate_temp.rate
           ELSE convertToCurrencyByCountry(`order_finance_balance`.price_cod_service,
                                           COALESCE(`order_finance_balance`.price_cod_service_currency_id,
                                                    `order`.price_currency,
                                                    `country`.currency_id), `usd_currency`.id) END
    ELSE 0 END AS `price_cod_service`,
    CASE WHEN `order_finance_balance`.price_vat IS NOT NULL AND `order_finance_balance`.price_vat != 0
      THEN CASE WHEN price_vat_currency_rate_temp.rate IS NOT NULL AND price_vat_currency_rate_temp.rate != 0
        THEN `order_finance_balance`.price_vat / price_vat_currency_rate_temp.rate
           ELSE convertToCurrencyByCountry(`order_finance_balance`.price_vat,
                                           COALESCE(`order_finance_balance`.price_vat_currency_id,
                                                    `order`.price_currency,
                                                    `country`.currency_id), `usd_currency`.id) END
    ELSE 0 END AS `price_vat`,
    `order_finance_balance`.additional_prices,
    `order_finance_balance`.created_at,
    `order_finance_balance`.updated_at
  FROM `order_finance_balance`
    LEFT JOIN `order` ON `order`.id = `order_finance_balance`.order_id
    LEFT JOIN `country` ON `country`.id = `order`.country_id
    INNER JOIN (SELECT `id`
                FROM `currency`
                WHERE char_code = 'USD'
                LIMIT 1) `usd_currency`
    LEFT JOIN `invoice_order` ON `order_finance_balance`.`order_id` = `invoice_order`.`order_id`
    LEFT JOIN `invoice_currency` `price_cod_currency_rate_temp`
      ON price_cod_currency_rate_temp.invoice_id = `invoice_order`.invoice_id AND
         price_cod_currency_rate_temp.invoice_currency_id = usd_currency.id AND
         price_cod_currency_rate_temp.currency_id = `order_finance_balance`.price_cod_currency_id
    LEFT JOIN `invoice_currency` `price_storage_currency_rate_temp`
      ON price_storage_currency_rate_temp.invoice_id = `invoice_order`.invoice_id AND
         price_storage_currency_rate_temp.invoice_currency_id = usd_currency.id AND
         price_storage_currency_rate_temp.currency_id = `order_finance_balance`.price_storage_currency_id
    LEFT JOIN `invoice_currency` `price_fulfilment_currency_rate_temp`
      ON price_fulfilment_currency_rate_temp.invoice_id = `invoice_order`.invoice_id AND
         price_fulfilment_currency_rate_temp.invoice_currency_id = usd_currency.id AND
         price_fulfilment_currency_rate_temp.currency_id = `order_finance_balance`.price_fulfilment_currency_id
    LEFT JOIN `invoice_currency` `price_packing_currency_rate_temp`
      ON price_packing_currency_rate_temp.invoice_id = `invoice_order`.invoice_id AND
         price_packing_currency_rate_temp.invoice_currency_id = usd_currency.id AND
         price_packing_currency_rate_temp.currency_id = `order_finance_balance`.price_packing_currency_id
    LEFT JOIN `invoice_currency` `price_package_currency_rate_temp`
      ON price_package_currency_rate_temp.invoice_id = `invoice_order`.invoice_id AND
         price_package_currency_rate_temp.invoice_currency_id = usd_currency.id AND
         price_package_currency_rate_temp.currency_id = `order_finance_balance`.price_package_currency_id
    LEFT JOIN `invoice_currency` `price_address_correction_currency_rate_temp`
      ON price_address_correction_currency_rate_temp.invoice_id = `invoice_order`.invoice_id AND
         price_address_correction_currency_rate_temp.invoice_currency_id = usd_currency.id AND
         price_address_correction_currency_rate_temp.currency_id =
         `order_finance_balance`.price_address_correction_currency_id
    LEFT JOIN `invoice_currency` `price_delivery_currency_rate_temp`
      ON price_delivery_currency_rate_temp.invoice_id = `invoice_order`.invoice_id AND
         price_delivery_currency_rate_temp.invoice_currency_id = usd_currency.id AND
         price_delivery_currency_rate_temp.currency_id = `order_finance_balance`.price_delivery_currency_id
    LEFT JOIN `invoice_currency` `price_redelivery_currency_rate_temp`
      ON price_redelivery_currency_rate_temp.invoice_id = `invoice_order`.invoice_id AND
         price_redelivery_currency_rate_temp.invoice_currency_id = usd_currency.id AND
         price_redelivery_currency_rate_temp.currency_id = `order_finance_balance`.price_redelivery_currency_id
    LEFT JOIN `invoice_currency` `price_delivery_back_currency_rate_temp`
      ON price_delivery_back_currency_rate_temp.invoice_id = `invoice_order`.invoice_id AND
         price_delivery_back_currency_rate_temp.invoice_currency_id = usd_currency.id AND
         price_delivery_back_currency_rate_temp.currency_id = `order_finance_balance`.price_delivery_back_currency_id
    LEFT JOIN `invoice_currency` `price_delivery_return_currency_rate_temp`
      ON price_delivery_return_currency_rate_temp.invoice_id = `invoice_order`.invoice_id AND
         price_delivery_return_currency_rate_temp.invoice_currency_id = usd_currency.id AND
         price_delivery_return_currency_rate_temp.currency_id =
         `order_finance_balance`.price_delivery_return_currency_id
    LEFT JOIN `invoice_currency` `price_cod_service_currency_rate_temp`
      ON price_cod_service_currency_rate_temp.invoice_id = `invoice_order`.invoice_id AND
         price_cod_service_currency_rate_temp.invoice_currency_id = usd_currency.id AND
         price_cod_service_currency_rate_temp.currency_id = `order_finance_balance`.price_cod_service_currency_id
    LEFT JOIN `invoice_currency` `price_vat_currency_rate_temp`
      ON price_vat_currency_rate_temp.invoice_id = `invoice_order`.invoice_id AND
         price_vat_currency_rate_temp.invoice_currency_id = usd_currency.id AND
         price_vat_currency_rate_temp.currency_id = `order_finance_balance`.price_vat_currency_id;
SQL;
        $this->execute($sql);

        $sql = <<<SQL
CREATE OR REPLACE VIEW `order_finance_fact_usd` AS
  SELECT
    `order_finance_fact`.`id`,
    `order_finance_fact`.`order_id`,
    `order_finance_fact`.`payment_id`,
    `order_finance_fact`.`pretension`,
    CASE WHEN `order_finance_fact`.price_cod IS NOT NULL AND `order_finance_fact`.price_cod != 0
      THEN `order_finance_fact`.price_cod / COALESCE(`order_finance_fact`.price_cod_currency_rate,
                                                     get_currency_rate_for_date(
                                                         COALESCE(`order_finance_fact`.price_cod_currency_id,
                                                                  `country`.currency_id),
                                                         FROM_UNIXTIME(
                                                             `payment_order_delivery`.paid_at,
                                                             '%Y-%m-%d')))
    ELSE 0 END AS `price_cod`,
    CASE WHEN `order_finance_fact`.price_storage IS NOT NULL AND `order_finance_fact`.price_storage != 0
      THEN `order_finance_fact`.price_storage / COALESCE(`order_finance_fact`.price_storage_currency_rate,
                                                         get_currency_rate_for_date(
                                                             COALESCE(`order_finance_fact`.price_storage_currency_id,
                                                                      `country`.currency_id),
                                                             FROM_UNIXTIME(
                                                                 `payment_order_delivery`.paid_at, '%Y-%m-%d')))
    ELSE 0 END AS `price_storage`,
    CASE WHEN `order_finance_fact`.price_fulfilment IS NOT NULL AND `order_finance_fact`.price_fulfilment != 0
      THEN `order_finance_fact`.price_fulfilment / COALESCE(`order_finance_fact`.price_fulfilment_currency_rate,
                                                            get_currency_rate_for_date(
                                                                COALESCE(
                                                                    `order_finance_fact`.price_fulfilment_currency_id,
                                                                    `country`.currency_id),
                                                                FROM_UNIXTIME(
                                                                    `payment_order_delivery`.paid_at,
                                                                    '%Y-%m-%d')))
    ELSE 0 END AS `price_fulfilment`,
    CASE WHEN `order_finance_fact`.price_packing IS NOT NULL AND `order_finance_fact`.price_packing != 0
      THEN `order_finance_fact`.price_packing / COALESCE(`order_finance_fact`.price_packing_currency_rate,
                                                         get_currency_rate_for_date(
                                                             COALESCE(`order_finance_fact`.price_packing_currency_id,
                                                                      `country`.currency_id),
                                                             FROM_UNIXTIME(
                                                                 `payment_order_delivery`.paid_at, '%Y-%m-%d')))
    ELSE 0 END AS `price_packing`,
    CASE WHEN `order_finance_fact`.price_package IS NOT NULL AND `order_finance_fact`.price_package != 0
      THEN `order_finance_fact`.price_package / COALESCE(`order_finance_fact`.price_package_currency_rate,
                                                         get_currency_rate_for_date(
                                                             COALESCE(`order_finance_fact`.price_package_currency_id,
                                                                      `country`.currency_id),
                                                             FROM_UNIXTIME(
                                                                 `payment_order_delivery`.paid_at, '%Y-%m-%d')))
    ELSE 0 END AS `price_package`,
    CASE WHEN `order_finance_fact`.price_address_correction IS NOT NULL AND
              `order_finance_fact`.price_address_correction != 0
      THEN `order_finance_fact`.price_address_correction /
           COALESCE(`order_finance_fact`.price_address_correction_currency_rate,
                    get_currency_rate_for_date(
                        COALESCE(`order_finance_fact`.price_address_correction_currency_id, `country`.currency_id),
                        FROM_UNIXTIME(`payment_order_delivery`.paid_at,
                                      '%Y-%m-%d')))
    ELSE 0 END AS `price_address_correction`,
    CASE WHEN `order_finance_fact`.price_delivery IS NOT NULL AND `order_finance_fact`.price_delivery != 0
      THEN `order_finance_fact`.price_delivery / COALESCE(`order_finance_fact`.price_delivery_currency_rate,
                                                          get_currency_rate_for_date(
                                                              COALESCE(`order_finance_fact`.price_delivery_currency_id,
                                                                       `country`.currency_id), FROM_UNIXTIME(
                                                                  `payment_order_delivery`.paid_at, '%Y-%m-%d')))
    ELSE 0 END AS `price_delivery`,
    CASE WHEN `order_finance_fact`.price_redelivery IS NOT NULL AND `order_finance_fact`.price_redelivery != 0
      THEN `order_finance_fact`.price_redelivery / COALESCE(`order_finance_fact`.price_redelivery_currency_rate,
                                                            get_currency_rate_for_date(
                                                                COALESCE(
                                                                    `order_finance_fact`.price_redelivery_currency_id,
                                                                    `country`.currency_id),
                                                                FROM_UNIXTIME(
                                                                    `payment_order_delivery`.paid_at,
                                                                    '%Y-%m-%d')))
    ELSE 0 END AS `price_redelivery`,
    CASE WHEN `order_finance_fact`.price_delivery_back IS NOT NULL AND `order_finance_fact`.price_delivery_back != 0
      THEN `order_finance_fact`.price_delivery_back / COALESCE(`order_finance_fact`.price_delivery_back_currency_rate,
                                                               get_currency_rate_for_date(
                                                                   COALESCE(
                                                                       `order_finance_fact`.price_delivery_back_currency_id,
                                                                       `country`.currency_id),
                                                                   FROM_UNIXTIME(
                                                                       `payment_order_delivery`.paid_at,
                                                                       '%Y-%m-%d')))
    ELSE 0 END AS `price_delivery_back`,
    CASE WHEN `order_finance_fact`.price_delivery_return IS NOT NULL AND `order_finance_fact`.price_delivery_return != 0
      THEN `order_finance_fact`.price_delivery_return /
           COALESCE(`order_finance_fact`.price_delivery_return_currency_rate,
                    get_currency_rate_for_date(
                        COALESCE(`order_finance_fact`.price_delivery_return_currency_id, `country`.currency_id),
                        FROM_UNIXTIME(
                            `payment_order_delivery`.paid_at,
                            '%Y-%m-%d')))
    ELSE 0 END AS `price_delivery_return`,
    CASE WHEN `order_finance_fact`.price_cod_service IS NOT NULL AND `order_finance_fact`.price_cod_service != 0
      THEN `order_finance_fact`.price_cod_service / COALESCE(`order_finance_fact`.price_cod_service_currency_rate,
                                                             get_currency_rate_for_date(
                                                                 COALESCE(
                                                                     `order_finance_fact`.price_cod_service_currency_id,
                                                                     `country`.currency_id),
                                                                 FROM_UNIXTIME(
                                                                     `payment_order_delivery`.paid_at,
                                                                     '%Y-%m-%d')))
    ELSE 0 END AS `price_cod_service`,
    CASE WHEN `order_finance_fact`.price_vat IS NOT NULL AND `order_finance_fact`.price_vat != 0
      THEN `order_finance_fact`.price_vat / COALESCE(`order_finance_fact`.price_vat_currency_rate,
                                                     get_currency_rate_for_date(
                                                         COALESCE(`order_finance_fact`.price_vat_currency_id,
                                                                  `country`.currency_id),
                                                         FROM_UNIXTIME(
                                                             `payment_order_delivery`.paid_at,
                                                             '%Y-%m-%d')))
    ELSE 0 END AS `price_vat`,
    `order_finance_fact`.additional_prices,
    `order_finance_fact`.delivery_report_id,
    `order_finance_fact`.delivery_report_record_id,
    `order_finance_fact`.created_at,
    `order_finance_fact`.updated_at
  FROM `order_finance_fact`
    LEFT JOIN `payment_order_delivery` ON `order_finance_fact`.`payment_id` = `payment_order_delivery`.`id`
    LEFT JOIN `order` ON `order`.id = `order_finance_fact`.order_id
    LEFT JOIN `country` ON `country`.id = `order`.country_id
SQL;

        $this->execute($sql);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropIndex($this->getIdxName('db_events', 'created_at'), 'db_events');
        $this->alterColumn('db_events', 'primary_key', $this->text());
        $this->execute("DROP VIEW `order_finance_balance_usd`");
        $this->execute("DROP VIEW `order_finance_fact_usd`");
    }
}
