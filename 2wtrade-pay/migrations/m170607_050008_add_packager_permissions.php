<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170607_050008_add_packager_permissions
 */
class m170607_050008_add_packager_permissions extends Migration
{
    protected $permissions = [
        'packager.control.index',
        'packager.control.edit',
        'packager.control.delete',
        'packager.control.activate',
        'packager.control.deactivate',
        'packager.control.activateautosend',
        'packager.control.deactivateautosend',
        'packager.request.index',
        'packager.request.downloadlabel',
        'packager.request.send',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        foreach ($this->permissions as $permission) {
            $this->insert('{{%auth_item}}', [
                'name' => $permission,
                'type' => '2',
                'description' => $permission,
            ]);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach ($this->permissions as $permission) {
            $this->delete('{{%auth_item}}', ['name' => $permission]);
        }
    }
}
