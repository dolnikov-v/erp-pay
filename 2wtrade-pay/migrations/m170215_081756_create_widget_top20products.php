<?php

use yii\db\Migration;
use app\modules\widget\models\WidgetType;
use app\modules\widget\models\WidgetRole;

/**
 * Handles the creation for table `widget_top_products`.
 */
class m170215_081756_create_widget_top20products extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert(WidgetType::tableName(), [
            'code' => 'top20products',
            'name' => 'ТОП 20 товаров городах',
            'status' => WidgetType::STATUS_ACTIVE
        ]);


        $type = WidgetType::find()
            ->where(['code' => 'top20products'])
            ->one();

        $this->insert(WidgetRole::tableName(), [
            'role' => 'business_analyst',
            'type_id' => $type->id
        ]);

        $this->insert(WidgetRole::tableName(), [
            'role' => 'country.curator',
            'type_id' => $type->id
        ]);

        $this->insert('i18n_source_message', ['category' => 'common', 'message' => 'ТОП 20 товаров в городах']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $type = WidgetType::find()
            ->where(['code' => 'top20products'])
            ->one();

        $this->delete(WidgetRole::tableName(), [
            'role' => 'business_analyst',
            'type_id' => $type->id,
        ]);

        $this->delete(WidgetRole::tableName(), [
            'role' => 'country.curator',
            'type_id' => $type->id,
        ]);

        $this->delete(WidgetType::tableName(), ['code' => 'top20products']);
    }
}
