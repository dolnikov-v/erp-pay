<?php

use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;
use app\modules\delivery\models\Delivery;

/**
 * Handles adding cron_send_orders to table `update_queue`.
 */
class m170622_084112_add_cron_send_orders_to_update_queue extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Delivery::tableName(), 'amazon_queue_url', $this->string(255));

        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_DELIVERY_SEND_ORDERS_TO_UPDATE_QUEUE]);

        if (!($crontabTask instanceof CrontabTask)) {
            $crontabTask = new CrontabTask();
            $crontabTask->name = CrontabTask::TASK_DELIVERY_SEND_ORDERS_TO_UPDATE_QUEUE;
            $crontabTask->description = "Отправка заказов в очередь на обновление статусов";
            $crontabTask->save();
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Delivery::tableName(), 'amazon_queue_url');
        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_DELIVERY_SEND_ORDERS_TO_UPDATE_QUEUE]);

        if ($crontabTask instanceof CrontabTask) {
            $crontabTask->delete();
        }
    }
}
