<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;
use app\models\Notification;

/**
 * Class m161019_052421_add_adcombo_hourly
 */
class m161019_052421_add_adcombo_hourly extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = new CrontabTask();
        $crontabTask->name = 'report_compare_with_adcombo_hourly';
        $crontabTask->description = 'Ежечасная сверка с AdCombo';
        $crontabTask->save();

        $notification = new Notification();
        $notification->description = "Ежечасная сверка с AdCombo с '{dateFrom} по {dateTo}': AdCombo - {totalAdComboCount}; Заказы - {totalOrderCount}; Колл-Центр - {totalCallCount};";
        $notification->trigger = "report.adcombo.hourly.info";
        $notification->type = Notification::TYPE_INFO;
        $notification->group = Notification::GROUP_REPORT;
        $notification->active = 1;
        $notification->save();
        unset($notification);

        $notification1 = new Notification();
        $notification1->description = "Ежечасная сверка с AdCombo с '{dateFrom} по {dateTo}' имеются несовпадения: AdCombo - {totalAdComboCount}; Заказы - {totalOrderCount}({differenceOrder}); Колл-Центр - {totalCallCount}({differenceCallCenter});";
        $notification1->trigger = "report.adcombo.hourly.danger";
        $notification1->type = Notification::TYPE_DANGER;
        $notification1->group = Notification::GROUP_REPORT;
        $notification1->active = 1;
        $notification1->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = CrontabTask::find()->where(['name' => 'report_compare_with_adcombo_hourly'])->one();
        if ($crontabTask) {
            $crontabTask->delete();
        }
        $notification = Notification::find()->where(['trigger' => "report.adcombo.hourly.info"])->one();
        if ($notification) {
            $notification->delete();
        }
        $notification = Notification::find()->where(['trigger' => "report.adcombo.hourly.danger"])->one();
        if ($notification) {
            $notification->delete();
        }
    }
}
