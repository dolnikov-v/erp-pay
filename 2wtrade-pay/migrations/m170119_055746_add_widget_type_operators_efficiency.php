<?php

use app\components\CustomMigration as Migration;
use app\modules\widget\models\WidgetType;
use app\modules\widget\models\WidgetRole;

/**
 * Class m170119_055746_add_widget_type_operators_efficiency
 */
class m170119_055746_add_widget_type_operators_efficiency extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert(WidgetType::tableName(), [
            'code' => 'operators_efficiency',
            'name' => 'Эффективность операторов',
            'status' => WidgetType::STATUS_ACTIVE,
        ]);

        $type = WidgetType::find()
            ->where(['code' => 'operators_efficiency'])
            ->one();

        $this->insert(WidgetRole::tableName(), [
            'role' => 'callcenter.manager',
            'type_id' => $type->id,
        ]);

        $this->insert(WidgetRole::tableName(), [
            'role' => 'business_analyst',
            'type_id' => $type->id,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $type = WidgetType::find()
            ->where(['code' => 'operators_efficiency'])
            ->one();

        $this->delete(WidgetRole::tableName(), [
            'role' => 'business_analyst',
            'type_id' => $type->id,
        ]);

        $this->delete(WidgetType::tableName(), ['code' => 'operators_efficiency']);
    }
}
