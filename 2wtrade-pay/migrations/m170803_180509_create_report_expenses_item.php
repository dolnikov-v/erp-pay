<?php

use app\components\CustomMigration as Migration;

/**
 * Handles the creation for table `report_expenses_item`.
 */
class m170803_180509_create_report_expenses_item extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('report_expenses_item', [
            'id' => $this->primaryKey(),
            'code' => $this->string(255)->notNull()->unique(),
            'name' => $this->string(255)->notNull(),
            'type' => $this->string(255)->notNull(),
            'value' => $this->string(255),
            'formula' => $this->string(255),
            'active' => $this->smallInteger(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $rows = [
            ['account_services',     'variable', 'Бухгалтерские услуги'],
            ['administrator',        'variable', 'Административные расходы'],
            ['advert',               'regular',  'Реклама'],
            ['bank_charges',         'regular',  'Банковские комиссии'],
            ['cash',                 'variable', 'Наличные'],
            ['certification',        'variable', 'Сертификация'],
            ['cleaning',             'variable', 'Уборка'],
            ['communication',        'variable', 'Связь'],
            ['courier_services',     'variable', 'Курьрская доставка'],
            ['delivery',             'variable', 'Доставка'],
            ['electricity',          'regular',  'Электричество'],
            ['entity_expenses',      'variable', 'Существенные расходы'],
            ['goods',                'variable', 'Товары (закупка)'],
            ['internet',             'regular',  'Интернет'],
            ['legal_expenses',       'variable', 'Юридические услуги'],
            ['maintenance',          'variable', 'Обслуживание'],
            ['material',             'variable', 'Материалы'],
            ['office_equipment',     'variable', 'Офисное оборудование'],
            ['office_expenses',      'variable', 'Офисные расходы'],
            ['other',                'variable', 'Другое'],
            ['packing',              'variable', 'Упаковка'],
            ['production',           'variable', 'Производство'],
            ['receiving',            'variable', 'Поступления'],
            ['recruitment',          'variable', 'Подбор персонала'],
            ['registration_fee',     'variable', 'Регистрация товара'],
            ['rent',                 'regular',  'Аренда'],
            ['salary',               'regular',  'Зарплата'],
            ['security',             'regular',  'Охрана'],
            ['social_security',      'variable', 'Социальное страхование'],
            ['stationery',           'variable', 'Канц товары'],
            ['tax',                  'regular',  'Налоги'],
            ['telephony',            'regular',  'Телефония'],
            ['tickets_fly_taxi',     'variable', 'Билеты (по командировкам)'],
            ['transport_cost',       'variable', 'Транспортные расходы'],
            ['utilities',            'regular',  'Коммунальные услуги'],
            ['warehouse',            'variable', 'Склад'],
            ['warehouse_expenses',   'variable', 'Складские расходы'],
        ];

        foreach ($rows as $row) {
            $time = time();
            $this->insert('report_expenses_item', [
                'code' => $row[0],
                'name' => $row[2],
                'type' => $row[1],
                'active' => 1,
                'created_at' => $time,
                'updated_at' => $time,
            ]);
        }

        $this->createTable('report_expenses_country', [
            'id' => $this->primaryKey(),
            'name' =>  $this->string(255)->notNull(),
            'country_id' => $this->integer(),
            'active' => $this->smallInteger(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $countries = [
            'Cambodia', 'China', 'China1', 'China2', 'Colombia', 'India',
            'Indonesia', 'Malaysia', 'Mexico', 'Pakistan', 'Peru',
            'Philippines', 'Russia',  'Thailand 1', 'Thailand 2', 'Tunisia'
        ];

        foreach ($countries as $country) {
            $time = time();
            $this->insert('report_expenses_country', [
                'name' => $country,
                'active' => 1,
                'created_at' => $time,
                'updated_at' => $time,
            ]);
        }

        $this->createTable('report_expenses_country_item', [
            'id' => $this->primaryKey(),
            'country_id' => $this->integer()->notNull(),
            'parent_id' => $this->integer()->notNull(),
            'type' => $this->string(255)->notNull(),
            'value' => $this->string(255),
            'formula' => $this->string(255),
            'active' => $this->smallInteger(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addForeignKey(null, 'report_expenses_country_item', 'country_id', 'report_expenses_country', 'id');
        $this->addForeignKey(null, 'report_expenses_country_item', 'parent_id', 'report_expenses_item', 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('report_expenses_country_item');
        $this->dropTable('report_expenses_item');
        $this->dropTable('report_expenses_country');
    }
}
