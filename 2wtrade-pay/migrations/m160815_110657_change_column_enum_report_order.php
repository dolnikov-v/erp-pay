<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160815_110657_change_column_enum_report_order
 */
class m160815_110657_change_column_enum_report_order extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('report_order', 'type', 'ENUM("debts", "potential_revenue", "revenue", "refund")');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('report_order', 'type', $this->string(50));
    }
}
