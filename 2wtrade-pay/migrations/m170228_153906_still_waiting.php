<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170228_153906_still_waiting
 */
class m170228_153906_still_waiting extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('order', 'call_center_type', "ENUM('order', 'new_return', 'return_noprod', 'jeempo', 'return', 'repeat', 'still_waiting') NOT NULL DEFAULT 'order' AFTER `call_center_again`");

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('order', 'call_center_type', "ENUM('order', 'new_return', 'return_noprod', 'jeempo', 'return', 'repeat') NOT NULL DEFAULT 'order' AFTER `call_center_again`");
    }
}
