<?php
use app\components\CustomMigration as Migration;

/**
 * Class m161216_045348_add_order_customer_subdistrict
 */
class m161216_045348_add_order_customer_subdistrict extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('order', 'customer_subdistrict', 'VARCHAR(200) DEFAULT NULL AFTER customer_district');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('order', 'customer_subdistrict');
    }
}
