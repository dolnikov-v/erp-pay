<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170608_062510_change_type_column_email_text_to_order_notification
 */
class m170608_062510_change_type_column_email_text_to_order_notification extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('order_notification', 'email_text', $this->text());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('order_notification', 'email_text', $this->string(500));
    }
}
