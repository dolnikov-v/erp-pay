<?php
use app\components\CustomMigration as Migration;
use app\modules\widget\models\WidgetType;
use app\modules\widget\models\WidgetRole;

/**
 * Class m170214_081845_add_widget_buyout_by_product_country_delivery
 */
class m170214_081845_add_widget_buyout_by_product_country_delivery extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn(WidgetType::tableName(), 'code', $this->string(255)->notNull());

        $this->insert(WidgetType::tableName(), [
            'code' => 'buyout_by_product_country_delivery',
            'name' => 'Выкуп по товару/стране/курьерке',
            'status' => WidgetType::STATUS_ACTIVE
        ]);


        $type = WidgetType::find()
            ->where(['code' => 'buyout_by_product_country_delivery'])
            ->one();

        $this->insert(WidgetRole::tableName(), [
            'role' => 'business_analyst',
            'type_id' => $type->id
        ]);

        $this->insert(WidgetRole::tableName(), [
            'role' => 'country.curator',
            'type_id' => $type->id
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

        $type = WidgetType::find()
            ->where(['code' => 'buyout_by_product_country_delivery'])
            ->one();

        $this->delete(WidgetRole::tableName(), [
            'role' => 'business_analyst',
            'type_id' => $type->id,
        ]);

        $this->delete(WidgetRole::tableName(), [
            'role' => 'country.curator',
            'type_id' => $type->id,
        ]);

        $this->delete(WidgetType::tableName(), ['code' => 'buyout_by_product_country_delivery']);

        $this->alterColumn(WidgetType::tableName(), 'code', $this->string(30)->notNull());
    }
}
