<?php
use app\components\CustomMigration as Migration;
use app\modules\callcenter\models\CallCenterUser;
use app\modules\callcenter\models\CallCenterUserPenalty;
use app\modules\callcenter\models\CallCenter;
use app\models\User;
use app\models\Country;
use yii\db\Query;

/**
 * Class m170609_035652_call_center_user_salary_demo
 */
class m170609_035652_call_center_user_salary_demo extends Migration
{

    const ROLES = [
        'country.curator',
        'callcenter.manager',
        'callcenter.hr',
    ];

    const RULES = [
        'callcenter.penalty.index',
        'callcenter.penalty.edit',
        'callcenter.penalty.delete',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(CallCenterUser::tableName(), 'salary_scheme', $this->string(255)->after('photo'));
        $this->addColumn(CallCenterUser::tableName(), 'salary_sum', $this->float()->after('salary_scheme'));

        $this->createTable(CallCenterUserPenalty::tableName(), [
            'id' => $this->primaryKey(),
            'country_id' => $this->integer()->notNull(),
            'call_center_id' => $this->integer()->notNull(),
            'call_center_user_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'penalty_type' => $this->integer()->notNull(),
            'penalty_percent' => $this->integer(),
            'penalty_sum' => $this->float(),
            'comment' => $this->string(255),
            'penalty_at' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addForeignKey(null, CallCenterUserPenalty::tableName(), 'country_id', Country::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, CallCenterUserPenalty::tableName(), 'call_center_id', CallCenter::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, CallCenterUserPenalty::tableName(), 'call_center_user_id', CallCenterUser::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, CallCenterUserPenalty::tableName(), 'user_id', User::tableName(), 'id', self::CASCADE, self::RESTRICT);

        $query = new Query();

        foreach (self::RULES as $rule) {

            $is_rule = $query->select('*')->from($this->authManager->itemTable)
                ->where(['name' => $rule, 'type' => 2])
                ->one();

            if (!$is_rule) {
                $this->insert($this->authManager->itemTable, [
                    'name' => $rule,
                    'description' => $rule,
                    'type' => 2,
                    'created_at' => time(),
                    'updated_at' => time()]);
            }

            foreach (self::ROLES as $role) {
                $is_can = $query->select('*')->from($this->authManager->itemChildTable)->where([
                    'parent' => $role,
                    'child' => $rule
                ])->one();

                if (!$is_can) {
                    $this->insert($this->authManager->itemChildTable, [
                        'parent' => $role,
                        'child' => $rule
                    ]);
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(CallCenterUser::tableName(), 'salary_scheme');
        $this->dropColumn(CallCenterUser::tableName(), 'salary_sum');

        $this->dropTable(CallCenterUserPenalty::tableName());

        foreach (self::RULES as $rule) {
            foreach (self::ROLES as $role) {
                $this->delete($this->authManager->itemChildTable, [
                    'parent' => $role,
                    'child' => $rule
                ]);
            }
        }
    }
}