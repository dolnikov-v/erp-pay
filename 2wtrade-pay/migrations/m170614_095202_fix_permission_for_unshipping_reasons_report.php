<?php

use app\components\CustomMigration as Migration;

/**
 * Class m170614_095202_fix_permission_for_unshipping_reasons_report
 */
class m170614_095202_fix_permission_for_unshipping_reasons_report extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->delete($this->authManager->itemChildTable, ['child' => 'report.unshipping-reasons.index', 'parent' => 'country.curator']);
        $this->delete($this->authManager->itemChildTable, ['child' => 'report.unshipping-reasons.index', 'parent' => 'business_analyst']);
        $this->delete('{{%auth_item}}', ['name' => 'report.unshipping-reasons.index']);

        // Доступ к отчету о причинах недоставленных заказов
        $this->insert('{{%auth_item}}',array(
            'name'=>'report.unshippingreasons.index',
            'type' => '2',
            'description' => 'report.unshippingreasons.index',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'country.curator',
            'child' => 'report.unshippingreasons.index'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'callcenter.manager',
            'child' => 'report.unshippingreasons.index'
        ));

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->authManager->itemChildTable, ['child' => 'report.unshippingreasons.index', 'parent' => 'country.curator']);
        $this->delete($this->authManager->itemChildTable, ['child' => 'report.unshippingreasons.index', 'parent' => 'callcenter.manager']);
        $this->delete('{{%auth_item}}', ['name' => 'report.unshippingreasons.index']);
    }
}
