<?php
use app\components\CustomMigration as Migration;

use app\modules\widget\models\WidgetType;

/**
 * Class m170112_073643_add_widget_type
 */
class m170112_073643_add_widget_type extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('{{%widget_type}}', [
            'code' => 'call_center_rating',
            'name' => 'Рейтинг Call-center',
            'status' => WidgetType::STATUS_ACTIVE,
        ]);

        $this->insert('{{%widget_type}}', [
            'code' => 'payback_advertising',
            'name' => 'Окупаемость рекламы',
            'status' => WidgetType::STATUS_ACTIVE,

        ]);

        $this->insert('{{%widget_type}}', [
            'code' => 'buy_out_goods',
            'name' => 'Выкупы по товарам',
            'status' => WidgetType::STATUS_ACTIVE,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete('{{%widget_type}}', ['code' => 'call_center_rating']);
        $this->delete('{{%widget_type}}', ['code' => 'payback_advertising']);
        $this->delete('{{%widget_type}}', ['code' => 'buy_out_goods']);
    }
}
