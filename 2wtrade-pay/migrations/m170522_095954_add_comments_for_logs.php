<?php
use app\components\CustomMigration as Migration;
use app\modules\callcenter\models\CallCenterRequestLog;
use app\modules\delivery\models\DeliveryRequestLog;
use app\modules\order\models\OrderLog;
use app\modules\order\models\OrderLogStatus;

/**
 * Class m170522_095954_add_comments_for_logs
 */
class m170522_095954_add_comments_for_logs extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(OrderLog::tableName(), 'comment', $this->string(500)->after('new'));
        $this->addColumn(OrderLogStatus::tableName(), 'comment', $this->string(500)->after('new'));
        $this->addColumn(CallCenterRequestLog::tableName(), 'comment', $this->string(500)->after('new'));
        $this->addColumn(DeliveryRequestLog::tableName(), 'comment', $this->string(500)->after('new'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(OrderLog::tableName(), 'comment');
        $this->dropColumn(OrderLogStatus::tableName(), 'comment');
        $this->dropColumn(CallCenterRequestLog::tableName(), 'comment');
        $this->dropColumn(DeliveryRequestLog::tableName(), 'comment');
    }
}
