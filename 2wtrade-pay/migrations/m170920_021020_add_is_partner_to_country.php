<?php

use app\models\Country;
use yii\db\Migration;

/**
 * Handles adding is_partner to table `country`.
 */
class m170920_021020_add_is_partner_to_country extends Migration
{

    /**
     * @inheritdoc
     */
    public function up()
    {
        $model = new Country();

        if (!$model->hasAttribute('is_partner')) {
            $this->addColumn(Country::tableName(), 'is_partner', $this->boolean()->defaultValue(0));
            $partnerCountries = Country::find()->where(['LIKE', 'slug', 2])->all();
            foreach ($partnerCountries as $partnerCountry) {
                $partnerCountry->is_partner = 1;
                $partnerCountry->save();
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn(Country::tableName(), 'is_partner');
    }
}
