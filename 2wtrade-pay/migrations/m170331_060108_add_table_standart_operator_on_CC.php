<?php
use app\components\CustomMigration as Migration;
use app\modules\callcenter\models\CallCenter;
use app\modules\callcenter\models\CallCenterStandartOperator;

/**
 * Class m170331_060108_add_table_standart_operator_on_CC
 */
class m170331_060108_add_table_standart_operator_on_CC extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

        $this->createTable(CallCenterStandartOperator::tableName(), [
            'id' => $this->primaryKey(),
            'call_center_id' => $this->integer(11)->notNull(),
            'operator_id' => $this->integer(11)->notNull(),
            'created_at' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)->notNull()
        ], $tableOptions);


        $this->addForeignKey(null, CallCenterStandartOperator::tableName(), 'call_center_id', CallCenter::tableName(), 'id', self::CASCADE, self::CASCADE);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable(CallCenterStandartOperator::tableName());
    }
}
