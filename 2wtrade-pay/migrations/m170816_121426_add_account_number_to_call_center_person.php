<?php

use yii\db\Migration;

/**
 * Handles adding account_number to table `call_center_person`.
 */
class m170816_121426_add_account_number_to_call_center_person extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('call_center_person', 'account_number', $this->string(100));
        $this->addColumn('call_center_person_import', 'cell_account_number', $this->integer());
        $this->addColumn('call_center_person_import_data', 'person_account_number', $this->string(100));

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('call_center_person', 'account_number');
        $this->dropColumn('call_center_person_import', 'cell_account_number');
        $this->dropColumn('call_center_person_import_data', 'person_account_number');
    }
}
