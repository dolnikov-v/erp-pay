<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160913_093653_add_country_in_notification
 */
class m160913_093653_add_country_in_notification extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('user_notification', 'country_id', $this->integer()->defaultValue(null)->after('trigger'));
        $this->addForeignKey(null, 'user_notification', 'country_id', 'country', 'id', self::CASCADE, self::RESTRICT);

        $this->dropForeignKey($this->getFkName('user_notification_queue', 'user_id'), 'user_notification_queue');
        $this->dropForeignKey($this->getFkName('user_notification_queue', 'trigger'), 'user_notification_queue');

        $this->dropColumn('user_notification_queue', 'trigger');
        $this->dropColumn('user_notification_queue', 'user_id');
        $this->dropColumn('user_notification_queue', 'read');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->addColumn('user_notification_queue', 'read', $this->smallInteger()->notNull()->defaultValue(0)->after('interval'));
        $this->addColumn('user_notification_queue', 'user_id', $this->integer()->after('notification_id'));
        $this->addColumn('user_notification_queue', 'trigger', $this->string(255)->after('user_id'));

        $this->addForeignKey(null, 'user_notification_queue', 'trigger', 'notification', 'trigger', self::CASCADE, self::SET_NULL);
        $this->addForeignKey(null, 'user_notification_queue', 'user_id', 'user', 'id', self::CASCADE, self::SET_NULL);

        $this->dropForeignKey($this->getFkName('user_notification', 'country_id'), 'user_notification');
        $this->dropColumn('user_notification', 'country_id');
    }
}
