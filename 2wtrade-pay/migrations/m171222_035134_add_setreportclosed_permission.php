<?php
use app\components\CustomMigration as Migration;

/**
 * Class m171222_035134_add_setreportclosed_permission
 */
class m171222_035134_add_setreportclosed_permission extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $auth = $this->authManager;
        $permission = $auth->createPermission('deliveryreport.report.setreportclosed');
        $permission->type = 2;
        $permission->description = 'Completely close delivery report';
        $auth->add($permission);

        $role = $auth->getRole('deliveryreport.clerk');
        $auth->addChild($role, $permission);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $permission = $this->authManager->getPermission('deliveryreport.report.setreportclosed');
        $this->authManager->remove($permission);
    }
}
