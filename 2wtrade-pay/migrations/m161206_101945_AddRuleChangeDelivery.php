<?php
use app\components\CustomMigration as Migration;

/**
 * Class m161206_101945_AddRuleChangeDelivery
 */
class m161206_101945_AddRuleChangeDelivery extends Migration
{
    public function up()
    {
        $this->insert('{{%auth_item}}',array(
            'name'=>'order.index.changedelivery',
            'type' => '2',
            'description' => 'order.index.changedelivery'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'country.curator',
            'child' => 'order.index.changedelivery'
        ));

    }

    public function down()
    {
        $this->delete($this->authManager->itemChildTable, ['child' => 'order.index.changedelivery', 'parent' => 'country.curator']);
        $this->delete('{{%auth_item}}', ['name' => 'order.index.changedelivery']);
    }
}
