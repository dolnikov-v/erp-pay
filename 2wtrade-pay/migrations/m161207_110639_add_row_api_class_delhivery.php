<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryApiClass;

/**
 * Class m161207_110639_add_row_api_class_delhivery
 */
class m161207_110639_add_row_api_class_delhivery extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $class = new DeliveryApiClass();
        $class->name = 'delhiveryApi';
        $class->class_path = '/delhivery-api/src/delhiveryApi.php';
        $class->save();

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        DeliveryApiClass::find()
            ->where([
                'name' => 'delhiveryApi',
                'class_path' => '/delhivery-api/src/delhiveryApi.php'
            ])
            ->one()
            ->delete();

    }
}
