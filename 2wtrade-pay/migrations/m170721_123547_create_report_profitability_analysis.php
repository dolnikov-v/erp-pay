<?php

use yii\db\Migration;

/**
 * Handles the creation for table `report_profitability_analysis`.
 */
class m170721_123547_create_report_profitability_analysis extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('report_profitability_analysis', [
            'id' => $this->primaryKey(),
            'country_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
            'month' => $this->smallInteger()->notNull(),
            'year' => $this->smallInteger()->notNull(),
            'data_json' => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('report_profitability_analysis');
    }
}
