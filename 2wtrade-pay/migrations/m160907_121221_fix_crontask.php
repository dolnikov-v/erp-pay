<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m160907_121221_fix_crontask
 */
class m160907_121221_fix_crontask extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        /** @var CrontabTask $task */
        $task = CrontabTask::find()
            ->byName('clean_api_lead_log')
            ->one();

        if ($task) {
            $task->name = 'clean_api_log';
            $task->save();
        }

        $task = CrontabTask::find()
            ->byName('clean_cron_task_log')
            ->one();

        if ($task) {
            $task->name = 'clean_crontab_task_log';
            $task->description = 'Очистка логов Crontab.';
            $task->save();
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        /** @var CrontabTask $task */
        $task = CrontabTask::find()
            ->byName('clean_crontab_task_log')
            ->one();

        if ($task) {
            $task->name = 'clean_cron_task_log';
            $task->description = 'Очистка логов по Crontab.';
            $task->save();
        }

        $task = CrontabTask::find()
            ->byName('clean_api_log')
            ->one();

        if ($task) {
            $task->name = 'clean_api_lead_log';
            $task->save();
        }
    }
}
