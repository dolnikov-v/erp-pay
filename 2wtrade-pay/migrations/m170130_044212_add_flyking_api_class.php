<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryApiClass;

/**
 * Class m170130_044212_add_flyking_api_class
 */
class m170130_044212_add_flyking_api_class extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $class = new DeliveryApiClass();
        $class->name = 'flyKingApi';
        $class->class_path = '/flyking-api/src/flyKingApi.php';
        $class->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        DeliveryApiClass::find()
            ->where([
                'name' => 'flyKingApi',
                'class_path' => '/flyking-api/src/flyKingApi.php'
            ])
            ->one()
            ->delete();
    }
}
