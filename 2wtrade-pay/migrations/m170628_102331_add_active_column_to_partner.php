<?php

use app\models\Partner;
use yii\db\Migration;

/**
 * Handles adding active_column to table `partner`.
 */
class m170628_102331_add_active_column_to_partner extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn(Partner::tableName(), 'active', $this->integer(1)->defaultValue(1)->after('default'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn(Partner::tableName(), 'active');
    }
}
