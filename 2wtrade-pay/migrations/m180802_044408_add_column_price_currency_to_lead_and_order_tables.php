<?php

use app\models\Currency;
use app\modules\order\models\Lead;
use app\modules\order\models\Order;
use app\components\CustomMigration as Migration;

/**
 * Handles adding column_price_currency to table `lead_and_order_tables`.
 */
class m180802_044408_add_column_price_currency_to_lead_and_order_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn(Lead::tableName(), 'price_currency', $this->integer()->defaultValue(null)->after('total_price'));
        $this->addColumn(Order::tableName(), 'price_currency', $this->integer()->defaultValue(null)->after('price_total'));

        $this->addForeignKey(null, Lead::tableName(), 'price_currency', Currency::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, Order::tableName(), 'price_currency', Currency::tableName(), 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn(Lead::tableName(), 'price_currency');
        $this->dropColumn(Order::tableName(), 'price_currency');
    }
}
