<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryApiClass;
use app\modules\delivery\models\Delivery;
use app\models\Country;


/**
 * Class m170405_042229_add_delivery_DIANTA_for_Indonesia
 */
class m170405_042229_add_delivery_DIANTA_for_Indonesia extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        if (DeliveryApiClass::find()->where(
            ['name' => 'DiantaApi', 'class_path' => '/dianta-api/src/DiantaApi.php']
        )->exists()
        ) {
            $apiClass = DeliveryApiClass::findOne(
                ['name' => 'DiantaApi', 'class_path' => '/ark-api/src/DiantaApi.php']
            );
        } else {
            $apiClass = new DeliveryApiClass();
        }
        $apiClass->name = 'DiantaApi';
        $apiClass->class_path = '/dianta-api/src/DiantaApi.php';
        $apiClass->active = '1';
        $apiClass->created_at = time();
        $apiClass->updated_at = time();
        $apiClass->save();

        if (Country::find()->where(
            [
                'name' => 'Индонезия 1',
                'char_code' => 'ID'
            ]
        )->exists()
        ) {
            $country = Country::find()->where(
                [
                    'name' => 'Индонезия 1',
                    'char_code' => 'ID'
                ]
            )->select('id')->one();
        } else {
            //нет страны
            return false;
        }

        if (Delivery::find()->where(
            [
                'name' => 'Dianta',
                'char_code' => 'DIANTA'
            ]
        )->exists()
        ) {
            $delivery = Delivery::findOne(
                [
                    'name' => 'Dianta',
                    'char_code' => 'DIANTA'
                ]
            );
        } else {
            $delivery = new Delivery();
        }
        $delivery->country_id = $country->id;
        $delivery->name = 'Dianta';
        $delivery->char_code = 'DIANTA';
        $delivery->api_class_id = $apiClass->id;
        $delivery->active = '1';
        $delivery->auto_sending = '0';
        $delivery->save(false);
        return true;
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        if (Country::find()->where(
            [
                'name' => 'Индонезия 1',
                'char_code' => 'ID'
            ]
        )->exists()
        ) {
            $country = Country::find()->where(
                [
                    'name' => 'Индонезия 1',
                    'char_code' => 'ID'
                ]
            )->select('id')->one();
        } else {
            //нет страны
            return false;
        }

        DeliveryApiClass::find()
            ->where([
                'name' => 'DiantaApi',
                'class_path' => '/dianta-api/src/DiantaApi.php'
            ])
            ->one()
            ->delete();

        Delivery::findOne(
            [
                'name' => 'Dianta',
                'country_id' => $country->id
            ]
        )->delete();
        return true;
    }
}
