<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170825_124236_add_salary_hourly_to_call_center_person_import
 */
class m170825_124236_add_salary_hourly_to_call_center_person_import extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('call_center_person_import', 'cell_salary_hourly', $this->string(100));
        $this->addColumn('call_center_person_import', 'cell_salary_hourly_usd', $this->string(100));
        $this->addColumn('call_center_person_import_data', 'person_salary_hourly', $this->string(100));
        $this->addColumn('call_center_person_import_data', 'person_salary_hourly_usd', $this->string(100));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('call_center_person_import', 'cell_salary_hourly');
        $this->dropColumn('call_center_person_import', 'cell_salary_hourly_usd');
        $this->dropColumn('call_center_person_import_data', 'person_salary_hourly');
        $this->dropColumn('call_center_person_import_data', 'person_salary_hourly_usd');
    }
}
