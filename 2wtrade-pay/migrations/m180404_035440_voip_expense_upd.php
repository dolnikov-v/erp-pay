<?php
use app\components\CustomMigration as Migration;

/**
 * Class m180404_035440_voip_expense_upd
 */
class m180404_035440_voip_expense_upd extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('voip_expense', 'asr', $this->float()->after('amount')->defaultValue(0));
        $this->addColumn('voip_expense', 'date_stat', $this->date()->after('date'));

        $sql = "update voip_expense set date_stat = from_unixtime(date, '%Y-%m-%d')";
        $this->execute($sql);

        $this->createIndex($this->getIdxName('voip_expense', ['country_id', 'date_stat']), 'voip_expense', ['country_id', 'date_stat']);

        $this->dropColumn('voip_expense', 'date');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropIndex($this->getIdxName('voip_expense', ['country_id', 'date_stat']), 'voip_expense');
        $this->dropColumn('voip_expense', 'asr');
        $this->dropColumn('voip_expense', 'date_stat');
        $this->addColumn('voip_expense', 'date', $this->integer());
    }
}
