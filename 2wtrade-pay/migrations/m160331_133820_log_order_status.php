<?php

use app\components\CustomMigration as Migration;
use app\modules\order\models\Order;
use app\modules\order\models\OrderLogStatus;
use app\modules\order\models\OrderStatus;

class m160331_133820_log_order_status extends Migration
{
    public function safeUp()
    {
        $this->createTable(OrderLogStatus::tableName(), [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->notNull(),
            'group_id' => $this->string()->notNull(),
            'old' => $this->integer()->notNull(),
            'new' => $this->integer()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $this->tableOptions);

        $this->addForeignKey(null, OrderLogStatus::tableName(), 'order_id', Order::tableName(), 'id', self::NO_ACTION, self::NO_ACTION);
        $this->addForeignKey(null, OrderLogStatus::tableName(), 'old', OrderStatus::tableName(), 'id', self::NO_ACTION, self::NO_ACTION);
        $this->addForeignKey(null, OrderLogStatus::tableName(), 'new', OrderStatus::tableName(), 'id', self::NO_ACTION, self::NO_ACTION);
    }

    public function safeDown()
    {
        $this->dropTable(OrderLogStatus::tableName());
    }
}
