<?php

use app\models\User;
use app\modules\order\models\OrderFinancePretension;
use app\components\CustomMigration as Migration;

/**
 * Handles adding column_user_id to table `order_finance_pretension`.
 */
class m180803_111656_add_column_user_id_to_order_finance_pretension extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn(OrderFinancePretension::tableName(), 'user_id', $this->integer()->defaultValue(null));
        $this->addForeignKey(null, OrderFinancePretension::tableName(), 'user_id', User::tableName(), 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey($this->getFkName(OrderFinancePretension::tableName(), 'user_id'), OrderFinancePretension::tableName(), 'user_id');
        $this->dropColumn(OrderFinancePretension::tableName(), 'user_id');
    }
}
