<?php
use app\components\CustomMigration as Migration;

/**
 * Class m180307_044533_report_profitability_opt
 */
class m180307_044533_report_profitability_opt extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createIndex($this->getIdxName('salary_work_time_plan', ['person_id']), 'salary_work_time_plan', ['person_id']);

        $this->execute('UPDATE salary_person 
          LEFT JOIN salary_working_shift on salary_person.working_shift_id = salary_working_shift.id
          SET salary_person.working_shift_id = null
          WHERE salary_person.office_id != salary_working_shift.office_id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropIndex($this->getIdxName('salary_work_time_plan', ['person_id']), 'salary_work_time_plan');
    }
}
