<?php

use app\components\CustomMigration;
use app\modules\order\models\OrderFinance;

/**
 * Handles adding fulfilment to table `order_finance`.
 */
class m170815_042042_add_fulfilment_to_order_finance extends CustomMigration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(OrderFinance::tableName(), 'price_fulfilment', $this->double()->after('price_storage'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(OrderFinance::tableName(), 'price_fulfilment');
    }
}
