<?php
use app\components\CustomMigration as Migration;
use app\modules\widget\models\WidgetType;
use app\modules\widget\models\WidgetRole;

/**
 * Class m170522_094027_add_widget_large_balances
 */
class m170522_094027_add_widget_large_balances extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $code = 'large_balances';

        $roles = [
            'business_analyst',
            'country.curator'
        ];

        $this->insert(WidgetType::tableName(), [
            'code' => $code,
            'name' => 'Большие остатки',
            'status' => WidgetType::STATUS_ACTIVE,
            'by_country' => WidgetType::BY_ALL_COUNTRIES,
        ]);

        $type = WidgetType::find()
            ->where(['code' => $code])
            ->one();

        foreach ($roles as $role) {
            $this->insert(WidgetRole::tableName(), [
                'role' => $role,
                'type_id' => $type->id
            ]);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $code = 'large_balances';
        $roles = [
            'business_analyst',
            'country.curator',
        ];

        $type = WidgetType::find()
            ->where(['code' => $code])
            ->one();

        foreach ($roles as $role) {
            $this->delete(WidgetRole::tableName(), [
                'role' => $role,
                'type_id' => $type->id
            ]);
        }

        $this->delete(WidgetType::tableName(), ['code' => $code]);
    }
}
