<?php
use app\components\CustomMigration as Migration;

/**
 * Class m161109_165756_alter_table_order_column_adress_add
 */
class m161116_101604_delivery_brokerage_columns extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('delivery', 'brokerage_daily_minimum', $this->integer(11)->defaultValue(0)->after('payment_reminder_sent_at'));
        $this->addColumn('delivery', 'brokerage_daily_maximum', $this->integer(11)->defaultValue(0)->after('brokerage_daily_minimum'));
        $this->addColumn('delivery', 'brokerage_stats_cutoff', $this->integer(11)->defaultValue(31)->after('brokerage_daily_maximum'));
        $this->addColumn('delivery', 'brokerage_enabled', $this->boolean()->defaultValue(false)->after('brokerage_stats_cutoff'));
        $this->addColumn('delivery', 'zipcodes', $this->string(10240)->defaultValue('')->after('brokerage_enabled'));

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('delivery', 'brokerage_daily_minimum');
        $this->dropColumn('delivery', 'brokerage_daily_maximum');
        $this->dropColumn('delivery', 'brokerage_stats_cutoff');
        $this->dropColumn('delivery', 'brokerage_enabled');
        $this->dropColumn('delivery', 'zipcodes');
    }
}
