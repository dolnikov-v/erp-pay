<?php
use app\components\CustomMigration as Migration;
use app\models\CurrencyRate;
use app\modules\order\models\OrderFinancePrediction;
use app\modules\report\models\Invoice;

/**
 * Class m180207_105930_invoice_sum_column
 */
class m180207_105930_invoice_sum_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('invoice', 'total', $this->double()->after('name'));
        $this->addColumn('invoice', 'additional_charges', $this->double()->after('total'));
        $query = Invoice::find()
            ->joinWith(['delivery.country.currencyRate', 'orders.financePrediction'], false)
            ->with(['delivery.country'])
            ->groupBy([Invoice::tableName() . '.id']);

        $query->orderBy([Invoice::tableName() . '.id' => SORT_DESC]);
        $query->joinWith([
            'orders.financePrediction.priceCodCurrencyRate',
            'orders.financePrediction.priceStorageCurrencyRate',
            'orders.financePrediction.priceFulfilmentCurrencyRate',
            'orders.financePrediction.pricePackingCurrencyRate',
            'orders.financePrediction.pricePackageCurrencyRate',
            'orders.financePrediction.priceAddressCorrectionCurrencyRate',
            'orders.financePrediction.priceDeliveryCurrencyRate',
            'orders.financePrediction.priceRedeliveryCurrencyRate',
            'orders.financePrediction.priceDeliveryReturnCurrencyRate',
            'orders.financePrediction.priceDeliveryBackCurrencyRate',
            'orders.financePrediction.priceCodServiceCurrencyRate',
            'orders.financePrediction.priceVatCurrencyRate',
        ], false);

        $select = [
            'income' => [],
            'costs' => [],
        ];
        $currencyFields = OrderFinancePrediction::getCurrencyFields();
        $currencyRateRelationMap = OrderFinancePrediction::currencyRateRelationMap();

        foreach (array_keys($currencyFields) as $key) {
            $relationName = $currencyRateRelationMap[$currencyFields[$key]];
            $query->leftJoin("invoice_currency as {$relationName}_temp", "{$relationName}_temp.invoice_id = " . Invoice::tableName() . ".id and {$relationName}_temp.currency_id = " . OrderFinancePrediction::tableName() . ".{$currencyFields[$key]}");
            $type = OrderFinancePrediction::getFinanceTypeOfField($key) == OrderFinancePrediction::FINANCE_TYPE_INCOME ? 'income' : 'costs';
            $select[$type][] = "IFNULL(" . OrderFinancePrediction::tableName() . ".{$key} / COALESCE({$relationName}_temp.rate, {$relationName}.rate, " . CurrencyRate::tableName() . '.rate), 0)';
        }

        $query->select([
            'sumTotal' => 'SUM((' . implode('+', $select['income']) . ') - (' . implode('+', $select['costs']) . '))',
            'id' => Invoice::tableName() . '.id'
        ]);

        $data = $query->createCommand()->queryAll();

        foreach ($data as $invoice) {
            $this->update('invoice', ['total' => $invoice['sumTotal']], ['id' => $invoice['id']]);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('invoice', 'total');
        $this->dropColumn('invoice', 'additional_charges');
    }
}
