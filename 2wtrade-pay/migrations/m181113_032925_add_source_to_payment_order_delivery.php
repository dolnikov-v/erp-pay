<?php
use app\components\CustomMigration as Migration;

/**
 * Class m181113_032925_add_source_to_payment_order_delivery
 */
class m181113_032925_add_source_to_payment_order_delivery extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('payment_order_delivery', 'source', "tinyint unsigned not null DEFAULT 0 AFTER `name`");
        $this->update('payment_order_delivery', ['source' => 1]);
        $this->addColumn('payment_order_delivery', 'created_by', $this->integer());
        $this->addForeignKey($this->getFkName('payment_order_delivery', 'created_by'), 'payment_order_delivery', 'created_by', 'user', 'id', self::SET_NULL, self::CASCADE);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey($this->getFkName('payment_order_delivery', 'created_by'), 'payment_order_delivery', 'created_by');
        $this->dropColumn('payment_order_delivery', 'source');
        $this->dropColumn('payment_order_delivery', 'created_by');
    }
}
