<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryChargesCalculator;

/**
 * Class m170804_100041_add_charges_calculators_bi_and_ninja
 */
class m170804_100041_add_charges_calculators_bi_and_ninja extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropForeignKey('fk_delivery_charges_calculator_id', Delivery::tableName());
        $this->addForeignKey('fk_delivery_charges_calculator_id', Delivery::tableName(), 'charges_calculator_id', 'delivery_charges_calculator', 'id', self::SET_NULL, self::CASCADE);
        $this->insert(DeliveryChargesCalculator::tableName(), [
            'name' => 'NinjaID',
            'class_path' => 'app\modules\delivery\components\charges\ninja\NinjaID',
            'active' => 1,
            'created_at' => time(),
        ]);
        $this->insert(DeliveryChargesCalculator::tableName(), [
            'name' => 'NinjaMY',
            'class_path' => 'app\modules\delivery\components\charges\ninja\NinjaMY',
            'active' => 1,
            'created_at' => time(),
        ]);
        $this->insert(DeliveryChargesCalculator::tableName(), [
            'name' => 'NinjaPH',
            'class_path' => 'app\modules\delivery\components\charges\ninja\NinjaPH',
            'active' => 1,
            'created_at' => time(),
        ]);
        $this->insert(DeliveryChargesCalculator::tableName(), [
            'name' => 'NinjaTH',
            'class_path' => 'app\modules\delivery\components\charges\ninja\NinjaTH',
            'active' => 1,
            'created_at' => time(),
        ]);
        $this->insert(DeliveryChargesCalculator::tableName(), [
            'name' => 'NinjaSG',
            'class_path' => 'app\modules\delivery\components\charges\ninja\NinjaSG',
            'active' => 1,
            'created_at' => time(),
        ]);
        $this->insert(DeliveryChargesCalculator::tableName(), [
            'name' => 'BI',
            'class_path' => 'app\modules\delivery\components\charges\bi\BI',
            'active' => 1,
            'created_at' => time(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_delivery_charges_calculator_id', Delivery::tableName());
        $this->addForeignKey('fk_delivery_charges_calculator_id', Delivery::tableName(), 'charges_calculator_id', 'delivery_charges_calculator', 'id');
        $this->delete(DeliveryChargesCalculator::tableName(), ['name' => 'NinjaID']);
        $this->delete(DeliveryChargesCalculator::tableName(), ['name' => 'NinjaMY']);
        $this->delete(DeliveryChargesCalculator::tableName(), ['name' => 'NinjaPH']);
        $this->delete(DeliveryChargesCalculator::tableName(), ['name' => 'NinjaTH']);
        $this->delete(DeliveryChargesCalculator::tableName(), ['name' => 'NinjaSG']);
        $this->delete(DeliveryChargesCalculator::tableName(), ['name' => 'BI']);
    }
}
