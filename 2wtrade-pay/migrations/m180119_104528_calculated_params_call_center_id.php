<?php
use app\components\CustomMigration as Migration;

/**
 * Class m180119_104528_calculated_params_call_center_id
 */
class m180119_104528_calculated_params_call_center_id extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('calculated_param', 'call_center_id', $this->integer());
        $this->addForeignKey(null, 'calculated_param', 'call_center_id', 'call_center', 'id', self::CASCADE, self::NO_ACTION);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('calculated_param', 'call_center_id');
        $this->dropForeignKey('calculated_param', 'call_center_id');
    }
}
