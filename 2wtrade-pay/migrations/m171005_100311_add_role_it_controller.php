<?php
use app\components\CustomMigration as Migration;

/**
 * Class m171005_100311_add_role_it_controller
 */
class m171005_100311_add_role_it_controller extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $role = $this->authManager->createRole('it_controller');
        $role->description = 'IT control';
        $this->authManager->add($role);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $role = $this->authManager->getRole('it_controller');
        $this->authManager->remove($role);
    }
}
