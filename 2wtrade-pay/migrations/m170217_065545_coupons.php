<?php
use app\components\CustomMigration as Migration;
use app\modules\order\models\MarketingCampaign;
use app\modules\order\models\MarketingCampaignCoupon;
use app\models\Country;
use app\models\User;
use \yii\db\Query;

/**
 * Class m170217_065545_coupons
 */
class m170217_065545_coupons extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        $query = new Query();

        $rules = [
            'order.marketingcampaign.index',
            'order.marketingcampaign.deletecoupon',
            'order.marketingcampaign.deletecampaign',
            'order.marketingcampaign.details',
            'order.marketingcampaign.edit',
            'order.marketingcampaign.print',
        ];

        $roles = [
            'callcenter.manager',
            'project.manager'
        ];

        foreach ($rules as $rule) {
            $is_report = $query->select('*')->from($this->authManager->itemTable)
                ->where([
                    'name' => $rule,
                    'type' => 2])
                ->one();

            if (!$is_report) {
                $this->insert($this->authManager->itemTable, [
                    'name' => $rule,
                    'type' => 2,
                    'description' => $rule,
                    'created_at' => time(),
                    'updated_at' => time()]);
            }

            foreach ($roles as $role) {

                $is_role = $query->select('*')->from($this->authManager->itemTable)
                    ->where(['name' => $role, 'type' => 1])
                    ->one();

                if (!$is_role) {
                    $this->insert($this->authManager->itemTable, [
                        'name' => $role,
                        'type' => 1,
                        'description' => $role,
                        'created_at' => time(),
                        'updated_at' => time()]);
                }

                $is_br = $query->select('*')->from($this->authManager->itemChildTable)->where([
                    'parent' => $role,
                    'child' => $rule
                ])->one();

                if (!$is_br) {
                    $this->insert($this->authManager->itemChildTable, [
                        'parent' => $role,
                        'child' => $rule
                    ]);
                }
            }
        }


        $this->createTable(MarketingCampaign::tableName(), [
            'id' => $this->primaryKey(),
            'country_id' => $this->integer()->notNull(),
            'name' => $this->string(64),
            'user_id' => $this->integer()->notNull(),
            'limit_type' => $this->enum(['no', 'at', 'days']),
            'limit_use_days' => $this->integer(),
            'limit_use_at' => $this->integer(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(null, MarketingCampaign::tableName(), 'country_id', Country::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, MarketingCampaign::tableName(), 'user_id', User::tableName(), 'id', self::CASCADE, self::RESTRICT);

        $this->createTable(MarketingCampaignCoupon::tableName(), [
            'id' => $this->primaryKey(),
            'marketing_campaign_id' => $this->integer()->notNull(),
            'coupon' => $this->string(10)->notNull()->unique(),
            'printed_at' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
        $this->addForeignKey(null, MarketingCampaignCoupon::tableName(), 'marketing_campaign_id', MarketingCampaign::tableName(), 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

        $rules = [
            'order.marketingcampaign.index',
            'order.marketingcampaign.deletecoupon',
            'order.marketingcampaign.deletecampaign',
            'order.marketingcampaign.details',
            'order.marketingcampaign.edit',
            'order.marketingcampaign.print',
        ];

        $roles = [
            'callcenter.manager',
            'project.manager'
        ];

        foreach ($rules as $rule) {

            foreach ($roles as $role) {
                $this->delete($this->authManager->itemChildTable, [
                    'parent' => $role,
                    'child' => $rule
                ]);
            }

            $this->delete($this->authManager->itemTable, [
                'name' => $rule,
                'type' => 2]);
        }

        $this->dropTable(MarketingCampaignCoupon::tableName());
        $this->dropTable(MarketingCampaign::tableName());
    }
}
