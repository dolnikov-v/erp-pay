<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryContract;

/**
 * Class m180609_034555_delivery_contract_charge_size
 */
class m180609_034555_delivery_contract_charge_size extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn(DeliveryContract::tableName(), 'charges_values', 'MEDIUMTEXT');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn(DeliveryContract::tableName(), 'charges_values', $this->text());
    }
}
