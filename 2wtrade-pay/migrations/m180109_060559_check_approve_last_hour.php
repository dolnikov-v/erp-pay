<?php
use app\components\CustomMigration as Migration;
use app\models\Notification;
use app\modules\administration\models\CrontabTask;

/**
 * Class m180109_060559_check_approve_last_hour
 */
class m180109_060559_check_approve_last_hour extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $notification = Notification::findOne(['trigger' => Notification::TRIGGER_CALL_CENTER_NO_APPROVE_LAST_HOUR]);
        if (!$notification instanceof Notification) {
            $notification = new Notification();
        }

        $notification->description = 'В офисе {officeName} ({workTime}), новых лидов сегодня {countLead}. За последний час {countApprove} апрувов.';
        $notification->trigger = Notification::TRIGGER_CALL_CENTER_NO_APPROVE_LAST_HOUR;
        $notification->type = Notification::TYPE_DANGER;
        $notification->group = Notification::GROUP_SYSTEM;
        $notification->active = Notification::ACTIVE;
        $notification->save(false);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $notification = Notification::findOne(['trigger' => Notification::TRIGGER_CALL_CENTER_NO_APPROVE_LAST_HOUR]);
        if ($notification instanceof Notification) {
            $notification->description = 'За последний час в офисе {officeName} не было апрувов по новым лидам';
            $notification->save(false);
        }
    }
}
