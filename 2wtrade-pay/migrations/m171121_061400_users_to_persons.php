<?php
use app\components\CustomMigration as Migration;
use app\modules\salary\models\PersonLink;
use app\models\User;

/**
 * Class m171121_061400_users_to_persons
 */
class m171121_061400_users_to_persons extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $users = User::find()
            ->where(['is not', 'person_id', null])
            ->all();

        $data = [];
        foreach ($users as $user) {
            $data[] = [
                'person_id' => $user->person_id,
                'type' => PersonLink::TYPE_ERP,
                'foreign_id' => $user->id,
                'created_at' => time(),
                'updated_at' => time(),
            ];
        }

        if ($data) {
            Yii::$app->db->createCommand()->batchInsert(
                PersonLink::tableName(),
                [
                    'person_id',
                    'type',
                    'foreign_id',
                    'created_at',
                    'updated_at',
                ],
                $data
            )->execute();
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        PersonLink::deleteAll([
            'type' => PersonLink::TYPE_ERP
        ]);
    }
}