<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160917_103500_storages_tables
 */
class m160917_103500_storages_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        /*
         * Новая таблица
         * storage_part - партии на складе
         */
        $this->createTable('storage_part', [
            'id' => $this->primaryKey(),
            'price_purchase' => $this->decimal(15, 2)->defaultValue(0),
            'logistics_delivery_storage' => $this->decimal(15, 2)->defaultValue(0),
            'logistics_customs' => $this->decimal(15, 2)->defaultValue(0),
            'logistics_certification' => $this->decimal(15, 2)->defaultValue(0),
            'logistics_other' => $this->decimal(15, 2)->defaultValue(0),
            'number' => $this->integer(11)->notNull(),
            'quantity' => $this->integer(11)->notNull(),
            'product_id' => $this->integer(11)->notNull(),
            'storage_id' => $this->integer(11)->notNull(),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11),
            'active' => $this->integer(1)->defaultValue(1),
            'balance' => $this->integer(11)
        ]);

        $this->addForeignKey(null, 'storage_part', 'product_id', 'product', 'id');
        $this->addForeignKey(null, 'storage_part', 'storage_id', 'storage', 'id');
        $this->createIndex(null, 'storage_part', 'product_id');
        $this->createIndex(null, 'storage_part', 'storage_id');

        /*
         * Новая таблица
         * storage_part_history - движение товара в партиях
         */
        $this->createTable('storage_part_history', [
            'id' => $this->primaryKey(),
            'storage_part_id' => $this->integer(11)->notNull(),
            'type' => $this->enum(['plus', 'minus'])->defaultValue('plus'),
            'amount' => $this->integer(11)->notNull(),
            'order_id' => $this->integer(11)->notNull(),
            'created_at' => $this->integer(11)->notNull()
        ]);

        $this->addForeignKey(null, 'storage_part_history', 'storage_part_id', 'storage_part', 'id');
        $this->createIndex(null, 'storage_part_history', 'storage_part_id');

        /*
         * Новая таблица
         * storage_product_unaccounted - неучтенные движения товара
         */
        $this->createTable('storage_product_unaccounted', [
            'id' => $this->primaryKey(),
            'amount' => $this->integer(11),
            'product_id' => $this->integer(11)->notNull(),
            'storage_id' => $this->integer(11)->defaultValue(null),
            'country_id' => $this->integer(11)->defaultValue(null),
            'order_id' => $this->integer(11)->notNull()
        ]);

        $this->addForeignKey(null, 'storage_product_unaccounted', 'product_id', 'product', 'id');
        $this->addForeignKey(null, 'storage_product_unaccounted', 'storage_id', 'storage', 'id');
        $this->addForeignKey(null, 'storage_product_unaccounted', 'country_id', 'country', 'id');
        $this->addForeignKey(null, 'storage_product_unaccounted', 'order_id', 'order', 'id');
        $this->createIndex(null, 'storage_product_unaccounted', 'product_id');

        /*
         * удалим таблицы из первой версии складов
         */
        $this->dropTable('storage_product_history');
        $this->dropTable('storage_product');

        /*
         * добавление колонок и индексов в уже имеющиеся таблицы
         */
        $this->addColumn('order_product', 'storage_part_id', $this->integer(11)->defaultValue(null));
        $this->addForeignKey(null, 'order_product', 'storage_part_id', 'storage_part', 'id');
        $this->addColumn('storage', 'delivery_id', $this->integer(11)->defaultValue(null));
        $this->addForeignKey(null, 'storage', 'delivery_id', 'delivery', 'id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('storage_product_unaccounted');
        $this->dropTable('storage_part_history');

        $this->createTable('storage_product_history', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(11)->notNull(),
            'storage_product_id' => $this->integer(11)->notNull(),
            'type' => $this->enum(['plus', 'minus'])->defaultValue('plus'),
            'amount' => $this->integer(11)->notNull(),
            'comment' => $this->string(100)->defaultValue(null),
            'created_at' => $this->integer(11)->notNull()
        ]);

        $this->createIndex(null, 'storage_product_history', 'storage_product_id');
        $this->createIndex(null, 'storage_product_history', 'user_id');

        $this->createTable('storage_product', [
            'id' => $this->primaryKey(),
            'storage_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
            'active' => $this->smallInteger()->notNull()->defaultValue(1),
            'balance' => $this->integer()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
        $this->addForeignKey(null, 'storage_product', 'storage_id', 'storage', 'id');
        $this->addForeignKey(null, 'storage_product', 'product_id', 'product', 'id');

        $this->dropForeignKey($this->getFkName('storage', 'delivery_id'), 'storage');
        $this->dropColumn('storage', 'delivery_id');
        $this->dropForeignKey($this->getFkName('order_product', 'storage_part_id'), 'order_product');
        $this->dropColumn('order_product', 'storage_part_id');

        $this->dropTable('storage_part');
    }
}
