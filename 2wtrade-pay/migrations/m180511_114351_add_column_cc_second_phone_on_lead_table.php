<?php

use app\components\CustomMigration as Migration;
use app\modules\order\models\Lead;


/**
 * Class m180511_114351_add_column_cc_second_phone_on_lead_table
 */
class m180511_114351_add_column_cc_second_phone_on_lead_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Lead::tableName(), 'cc_second_phone', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Lead::tableName(), 'cc_second_phone');
    }
}
