<?php
use app\components\CustomMigration as Migration;

/**
 * Class m190314_075816_call_center_request_call_data
 */
class m190314_075816_call_center_request_call_data extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('call_center_request_call_data', [
            'call_center_request_id' => $this->integer(),
            'operator_id' => $this->integer(),
            'called_at' => $this->integer(),
        ]);

        $this->addPrimaryKey('', 'call_center_request_call_data', [
            'call_center_request_id',
            'called_at'
        ]);

        $this->addForeignKey(null, 'call_center_request_call_data', 'call_center_request_id', 'call_center_request', 'id', self::CASCADE, self::CASCADE);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('call_center_request_call_data');
    }
}
