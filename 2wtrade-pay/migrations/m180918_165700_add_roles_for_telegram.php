<?php
use app\components\PermissionMigration as Migration;

/**
 * Class m180918_165700_add_roles_for_telegram
 */
class m180918_165700_add_roles_for_telegram extends Migration
{

    protected $permissions = ['telegram.voip.expense', 'telegram.delivery.calculate.charges'];

    protected $roles = [
        'superadmin' => ['telegram.voip.expense', 'telegram.delivery.calculate.charges'],
        'technical.director' => ['telegram.voip.expense', 'telegram.delivery.calculate.charges']
    ];
}
