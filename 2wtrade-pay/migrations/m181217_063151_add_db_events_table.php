<?php
use app\components\CustomMigration as Migration;

/**
 * Class m181217_063151_add_db_events_table
 */
class m181217_063151_add_db_events_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('db_events', [
            'id' => $this->string(160)->notNull(),
            'event_id' => $this->string(255)->notNull()->comment('Identifier of triggered event. Used to find executable actions in handler. May be name of table or other unique identifier.'),
            'type' => $this->enum(['insert', 'update', 'delete'])->notNull()->comment('Type of triggered event.'),
            'primary_key' => $this->text()->notNull()->comment('Primary key(s) of changed record. May be JSON object or scalar.'),
            'values' => $this->text()->comment("Value(s) of changed record. May be custom JSON object(array) or scalar. Used only in event handler. Not required."),
            'created_at' => $this->dateTime()->defaultValue(new \yii\db\Expression('NOW()'))
        ]);

        $this->addPrimaryKey(null, 'db_events', 'id');

        $this->execute('DROP TRIGGER IF EXISTS `db_events_generate_unique_id`');
        $createTriggerSql = <<<SQL
CREATE TRIGGER `db_events_generate_unique_id` BEFORE INSERT ON `db_events`
  FOR EACH ROW BEGIN
  IF NEW.id IS NULL THEN
    SET @custom_uuid=SHA(CONCAT(UUID(), NEW.event_id, NEW.type, NEW.primary_key, IFNULL(NEW.values, '')));
    SET NEW.id=@custom_uuid;
  END IF;
END;
SQL;
        $this->execute($createTriggerSql);


        $this->execute('DROP TRIGGER IF EXISTS `order_finance_prediction_insert_event_log`');
        $createTriggerSql = <<<SQL
CREATE TRIGGER `order_finance_prediction_insert_event_log` AFTER INSERT ON `order_finance_prediction`
    FOR EACH ROW BEGIN
        INSERT INTO `db_events` SET `event_id`='order_finance_prediction', `type`='insert', `primary_key`=NEW.order_id;
    END;
SQL;
        $this->execute($createTriggerSql);

        $this->execute('DROP TRIGGER IF EXISTS `order_finance_prediction_update_event_log`');
        $createTriggerSql = <<<SQL
CREATE TRIGGER `order_finance_prediction_update_event_log` AFTER UPDATE ON `order_finance_prediction`
    FOR EACH ROW BEGIN
        INSERT INTO `db_events` SET `event_id`='order_finance_prediction', `type`='update', `primary_key`=NEW.order_id;
    END;
SQL;
        $this->execute($createTriggerSql);

        $this->execute('DROP TRIGGER IF EXISTS `order_finance_prediction_delete_event_log`');
        $createTriggerSql = <<<SQL
CREATE TRIGGER `order_finance_prediction_delete_event_log` AFTER DELETE ON `order_finance_prediction`
    FOR EACH ROW BEGIN
        INSERT INTO `db_events` SET `event_id`='order_finance_prediction', `type`='delete', `primary_key`=OLD.order_id;
    END;
SQL;
        $this->execute($createTriggerSql);


        $this->execute('DROP TRIGGER IF EXISTS `order_finance_fact_insert_event_log`');
        $createTriggerSql = <<<SQL
CREATE TRIGGER `order_finance_fact_insert_event_log` AFTER INSERT ON `order_finance_fact`
    FOR EACH ROW BEGIN
        INSERT INTO `db_events` SET `event_id`='order_finance_fact', `type`='insert', `primary_key`=NEW.id;
    END;
SQL;
        $this->execute($createTriggerSql);

        $this->execute('DROP TRIGGER IF EXISTS `order_finance_fact_update_event_log`');
        $createTriggerSql = <<<SQL
CREATE TRIGGER `order_finance_fact_update_event_log` AFTER UPDATE ON `order_finance_fact`
    FOR EACH ROW BEGIN
        INSERT INTO `db_events` SET `event_id`='order_finance_fact', `type`='update', `primary_key`=NEW.id;
    END;
SQL;
        $this->execute($createTriggerSql);

        $this->execute('DROP TRIGGER IF EXISTS `order_finance_fact_delete_event_log`');
        $createTriggerSql = <<<SQL
CREATE TRIGGER `order_finance_fact_delete_event_log` AFTER DELETE ON `order_finance_fact`
    FOR EACH ROW BEGIN
        INSERT INTO `db_events` SET `event_id`='order_finance_fact', `type`='delete', `primary_key`=OLD.id;
    END;
SQL;
        $this->execute($createTriggerSql);

        $this->execute('DROP TRIGGER IF EXISTS `order_status_id_was_changed_event_log`');
        $createTriggerSql = <<<SQL
CREATE TRIGGER `order_status_id_was_changed_event_log` AFTER UPDATE ON `order`
    FOR EACH ROW BEGIN
        IF OLD.status_id != NEW.status_id THEN
          INSERT INTO `db_events` SET `event_id`='order_status_id_was_changed', `type`='update', `primary_key`=NEW.id, `values`=JSON_OBJECT('old', OLD.status_id, 'new', NEW.status_id);
        END IF; 
    END;
SQL;
        $this->execute($createTriggerSql);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->execute('DROP TRIGGER IF EXISTS `order_status_id_was_changed_event_log`');
        $this->execute('DROP TRIGGER IF EXISTS `order_finance_prediction_insert_event_log`');
        $this->execute('DROP TRIGGER IF EXISTS `order_finance_prediction_update_event_log`');
        $this->execute('DROP TRIGGER IF EXISTS `order_finance_prediction_delete_event_log`');
        $this->execute('DROP TRIGGER IF EXISTS `order_finance_fact_insert_event_log`');
        $this->execute('DROP TRIGGER IF EXISTS `order_finance_fact_update_event_log`');
        $this->execute('DROP TRIGGER IF EXISTS `order_finance_fact_delete_event_log`');
        $this->execute('DROP TRIGGER IF EXISTS `db_events_generate_unique_id`');
        $this->dropTable('db_events');
    }
}
