<?php

use app\components\CustomMigration as Migration;
use app\modules\delivery\models\Delivery;

/**
 * Handles adding broker_active to table `delivery`.
 */
class m170228_114448_add_broker_active_to_delivery extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn(Delivery::tableName(), 'brokerage_active', $this->smallInteger()->defaultValue(1)->after('brokerage_stats_cutoff'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn(Delivery::tableName(), 'brokerage_active');
    }
}
