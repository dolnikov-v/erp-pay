<?php
use app\components\CustomMigration as Migration;
use app\modules\callcenter\models\CallCenter;

/**
 * Class m171204_044424_call_center_issue_collectors
 */
class m171204_044424_call_center_issue_collectors extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(CallCenter::tableName(), 'jira_issue_collector', $this->string());

        $data = [
            'http://pakistan-call.com/' => '14b99092',
//            'Indonesia' => '1e5dbe26',
            'http://malaysia-call.com' => '82768e5b',
            'http://indonesia-call.com' => '013b6796',
            'http://philippines-call.com/malaysya' => '08d4f8a8',
            'http://indi-call.com/shri' => 'c279ff0f',
            'http://thai2-call.com' => '7104fa3a',
            'http://pakistan-call.com/kuwait' => 'fb16319c',
//            'Laos' => '32d0fd0d',
            'http://pakistan-call.com/nepal/' => '9704ff8c',
            'http://pakistan-call.com/qatar' => '19cf8abf',
            'http://pakistan-call.com/saudiarabia' => '8b0e3c1d',
            'http://indi-call.com/uae' => '7ca83161',
            'http://pakistan-call.com/south_africa' => '82cca4ba',
        ];

        foreach ($data as $key => $val) {
            $this->update(CallCenter::tableName(), ['jira_issue_collector' => $val], ['url' => $key]);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(CallCenter::tableName(), 'jira_issue_collector');
    }
}
