<?php
use app\components\CustomMigration as Migration;
use yii\db\Query;

/**
 * Class m181121_045924_add_order_notifier_check_notifications_cron
 */
class m181121_045924_add_order_notifier_check_notifications_cron extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $cronTaskId = (new Query())->select('id')
            ->from('crontab_task')
            ->where(['name' => \app\modules\administration\models\CrontabTask::TASK_ORDER_NOTIFIER_CHECK_NOTIFICATIONS])
            ->scalar();

        $data = [
            'name' => \app\modules\administration\models\CrontabTask::TASK_ORDER_NOTIFIER_CHECK_NOTIFICATIONS,
            'description' => 'Получение информации о статусе доставки СМС оповещений клиентам',
            'updated_at' => time(),
        ];

        if (!$cronTaskId) {
            $this->insert('crontab_task', $data);
        } else {
            $data['created_at'] = time();
            $this->update('crontab_task', $data, ['id' => $cronTaskId]);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete('crontab_task', ['name' => \app\modules\administration\models\CrontabTask::TASK_ORDER_NOTIFIER_CHECK_NOTIFICATIONS]);
    }
}
