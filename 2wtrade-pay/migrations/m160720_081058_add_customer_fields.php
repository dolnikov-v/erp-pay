<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160720_081058_add_customer_fields
 */
class m160720_081058_add_customer_fields extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('order', 'customer_province', $this->string(100)->defaultValue(null)->after('customer_mobile'));
        $this->addColumn('order', 'customer_street', $this->string(100)->defaultValue(null)->after('customer_city'));
        $this->addColumn('order', 'customer_house_number', $this->string(100)->defaultValue(null)->after('customer_street'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('order', 'customer_house_number');
        $this->dropColumn('order', 'customer_street');
        $this->dropColumn('order', 'customer_province');
    }
}
