<?php
use app\components\CustomMigration as Migration;
use app\modules\callcenter\models\CallCenterUser;
use app\modules\callcenter\models\CallCenterWorkTime;
use app\models\Country;
use app\modules\administration\models\CrontabTask;
use app\modules\salary\models\Designation;
use yii\db\Query;

/**
 * Class m170626_051344_call_center_auto_calc_salary
 */
class m170626_051344_call_center_auto_calc_salary extends Migration
{

    const ROLES = [
        'finance.director',
        'callcenter.hr',
    ];

    const RULES = [
        'report.salary.index',
    ];


    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        $query = new Query();

        foreach (self::RULES as $rule) {

            $is_rule = $query->select('*')->from($this->authManager->itemTable)
                ->where(['name' => $rule, 'type' => 2])
                ->one();

            if (!$is_rule) {
                $this->insert($this->authManager->itemTable, [
                    'name' => $rule,
                    'description' => $rule,
                    'type' => 2,
                    'created_at' => time(),
                    'updated_at' => time()]);
            }

            foreach (self::ROLES as $role) {
                $is_can = $query->select('*')->from($this->authManager->itemChildTable)->where([
                    'parent' => $role,
                    'child' => $rule
                ])->one();

                if (!$is_can) {
                    $this->insert($this->authManager->itemChildTable, [
                        'parent' => $role,
                        'child' => $rule
                    ]);
                }
            }
        }

        $this->addColumn(Designation::tableName(), 'team_lead', $this->integer(1)->defaultValue(0));
        $this->addColumn(Designation::tableName(), 'calc_work_time', $this->integer(1)->defaultValue(0));

        $this->update(Designation::tableName(), ['team_lead' => 1], ['id' => [3, 4]]);
        $this->update(Designation::tableName(), ['calc_work_time' => 1], ['id' => [5]]);


        $this->createTable(CallCenterWorkTime::tableName(), [
            'id' => $this->primaryKey(),
            'call_center_user_id' => $this->integer()->notNull(),
            'date' => $this->date(),
            'time' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $this->tableOptions);

        $this->addForeignKey(null, CallCenterWorkTime::tableName(), 'call_center_user_id', CallCenterUser::tableName(), 'id', self::CASCADE, self::RESTRICT);

        $this->createTable('call_center_holiday', [
            'id' => $this->primaryKey(),
            'country_id' => $this->integer()->notNull(),
            'date' => $this->date(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $this->tableOptions);

        $this->addForeignKey(null, 'call_center_holiday', 'country_id', Country::tableName(), 'id', self::CASCADE, self::RESTRICT);

        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_CC_GET_OPERS_ACTIVITY_DATA]);

        if (!($crontabTask instanceof CrontabTask)) {
            $crontabTask = new CrontabTask();
            $crontabTask->name = CrontabTask::TASK_CC_GET_OPERS_ACTIVITY_DATA;
            $crontabTask->description = "Получение отработанных часов из КЦ по пользователям";
            $crontabTask->save();
        }

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

        foreach (self::RULES as $rule) {
            foreach (self::ROLES as $role) {
                $this->delete($this->authManager->itemChildTable, [
                    'parent' => $role,
                    'child' => $rule
                ]);
            }
        }

        $this->dropColumn(Designation::tableName(), 'team_lead');
        $this->dropColumn(Designation::tableName(), 'calc_work_time');

        $schemaWT = Yii::$app->db->schema->getTableSchema(CallCenterWorkTime::tableName());

        if ($schemaWT !== null) {
            $this->dropTable(CallCenterWorkTime::tableName());
        }

        $schemaCH = Yii::$app->db->schema->getTableSchema('call_center_holiday');

        if ($schemaCH !== null) {
            $this->dropTable('call_center_holiday');
        }

        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_CC_GET_OPERS_ACTIVITY_DATA]);

        if ($crontabTask instanceof CrontabTask) {
            $crontabTask->delete();
        }

    }
}
