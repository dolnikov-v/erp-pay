<?php
use app\components\CustomMigration as Migration;

class m000000_000001_auth_item extends Migration
{
    public function up()
    {
        $this->createTable($this->authManager->itemTable, [
            'name' => $this->string(64)->notNull(),
            'type' => $this->integer()->notNull(),
            'description' => $this->text(),
            'rule_name' => $this->string(64),
            'data' => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            $this->includePrimaryKey('name'),
        ], $this->tableOptions);

        $this->createIndex(null, $this->authManager->itemTable, 'type');

        $this->addForeignKey(null, $this->authManager->itemTable, 'rule_name', $this->authManager->ruleTable, 'name', self::SET_NULL, self::CASCADE);
    }

    public function down()
    {
        $this->dropTable($this->authManager->itemTable);
    }
}
