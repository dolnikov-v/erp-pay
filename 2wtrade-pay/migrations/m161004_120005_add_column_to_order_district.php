<?php

use yii\db\Migration;
use app\modules\order\models\Order;

/**
 * Handles adding column to table `order_district`.
 */
class m161004_120005_add_column_to_order_district extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn(Order::tableName(),'customer_district', $this->string(100)->defaultValue(NULL)->after('customer_province'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn(Order::tableName(), 'customer_district');
    }
}
