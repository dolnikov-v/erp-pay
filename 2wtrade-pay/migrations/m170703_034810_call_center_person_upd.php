<?php
use app\components\CustomMigration as Migration;
use app\modules\callcenter\models\CallCenterUser;
use app\modules\salary\models\Person;
use yii\db\Expression;
use yii\db\Query;

/**
 * Class m170703_034810_call_center_person_upd
 */
class m170703_034810_call_center_person_upd extends Migration
{

    const ROLE = 'callcenter.teamlead';
        
    const ROLE_NAME = 'Супервайзер продаж КЦ';

    const RULES = [
        'home.index.index',
        'report.salary.index',
    ];


    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $query = new Query();

        $is_role = $query->select('*')->from($this->authManager->itemTable)
            ->where(['name' => self::ROLE, 'type' => 1])
            ->one();

        if (!$is_role) {
            $this->insert($this->authManager->itemTable, [
                'name' => self::ROLE,
                'type' => 1,
                'description' => self::ROLE_NAME,
                'created_at' => time(),
                'updated_at' => time()]);
        }

        foreach (self::RULES as $rule) {

            $is_rule = $query->select('*')->from($this->authManager->itemTable)
                ->where(['name' => $rule, 'type' => 2])
                ->one();

            if (!$is_rule) {
                $this->insert($this->authManager->itemTable, [
                    'name' => $rule,
                    'description' => $rule,
                    'type' => 2,
                    'created_at' => time(),
                    'updated_at' => time()]);
            }


            $is_can = $query->select('*')->from($this->authManager->itemChildTable)->where([
                'parent' => self::ROLE,
                'child' => $rule
            ])->one();

            if (!$is_can) {
                $this->insert($this->authManager->itemChildTable, [
                    'parent' => self::ROLE,
                    'child' => $rule
                ]);
            }
        }

        $this->dropForeignKey('fk_call_center_user_person_id', CallCenterUser::tableName());
        $this->addForeignKey('fk_call_center_user_person_id', CallCenterUser::tableName(), 'person_id', Person::tableName(), 'id', self::SET_NULL, self::NO_ACTION);

        $this->alterColumn(Person::tableName(), 'working_mon', $this->string());
        $this->alterColumn(Person::tableName(), 'working_tue', $this->string());
        $this->alterColumn(Person::tableName(), 'working_wed', $this->string());
        $this->alterColumn(Person::tableName(), 'working_thu', $this->string());
        $this->alterColumn(Person::tableName(), 'working_fri', $this->string());
        $this->alterColumn(Person::tableName(), 'working_sat', $this->string());
        $this->alterColumn(Person::tableName(), 'working_sun', $this->string());

        $this->update(Person::tableName(), ['working_mon' => new Expression('shift_time')], ['working_mon' => 1]);
        $this->update(Person::tableName(), ['working_tue' => new Expression('shift_time')], ['working_tue' => 1]);
        $this->update(Person::tableName(), ['working_wed' => new Expression('shift_time')], ['working_wed' => 1]);
        $this->update(Person::tableName(), ['working_thu' => new Expression('shift_time')], ['working_thu' => 1]);
        $this->update(Person::tableName(), ['working_fri' => new Expression('shift_time')], ['working_fri' => 1]);
        $this->update(Person::tableName(), ['working_sat' => new Expression('shift_time')], ['working_sat' => 1]);
        $this->update(Person::tableName(), ['working_sun' => new Expression('shift_time')], ['working_sun' => 1]);

        $this->update(Person::tableName(), ['working_mon' => ''], ['working_mon' => 0]);
        $this->update(Person::tableName(), ['working_tue' => ''], ['working_tue' => 0]);
        $this->update(Person::tableName(), ['working_wed' => ''], ['working_wed' => 0]);
        $this->update(Person::tableName(), ['working_thu' => ''], ['working_thu' => 0]);
        $this->update(Person::tableName(), ['working_fri' => ''], ['working_fri' => 0]);
        $this->update(Person::tableName(), ['working_sat' => ''], ['working_sat' => 0]);
        $this->update(Person::tableName(), ['working_sun' => ''], ['working_sun' => 0]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

        foreach (self::RULES as $rule) {
            $this->delete($this->authManager->itemChildTable, [
                'parent' => self::ROLE,
                'child' => $rule
            ]);
        }

        $this->delete($this->authManager->itemTable, [
            'name' => self::ROLE,
            'type' => 1
        ]);

        $this->dropForeignKey('fk_call_center_user_person_id', CallCenterUser::tableName());
        $this->addForeignKey('fk_call_center_user_person_id', CallCenterUser::tableName(), 'person_id', Person::tableName(), 'id', self::CASCADE, self::RESTRICT);

        $this->alterColumn(Person::tableName(), 'working_mon', $this->integer(1)->defaultValue(1));
        $this->alterColumn(Person::tableName(), 'working_tue', $this->integer(1)->defaultValue(1));
        $this->alterColumn(Person::tableName(), 'working_wed', $this->integer(1)->defaultValue(1));
        $this->alterColumn(Person::tableName(), 'working_thu', $this->integer(1)->defaultValue(1));
        $this->alterColumn(Person::tableName(), 'working_fri', $this->integer(1)->defaultValue(1));
        $this->alterColumn(Person::tableName(), 'working_sat', $this->integer(1)->defaultValue(0));
        $this->alterColumn(Person::tableName(), 'working_sun', $this->integer(1)->defaultValue(0));
    }
}
