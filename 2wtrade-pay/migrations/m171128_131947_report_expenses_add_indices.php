<?php
use app\components\CustomMigration as Migration;

/**
 * Class m171128_131947_report_expenses_add_indices
 */
class m171128_131947_report_expenses_add_indices extends Migration
{
    protected $tables = [
        'report_expenses_category' => [
            'code',
            'name',
            'active',
            'created_at',
            'updated_at',
        ],
        'report_expenses_country' => [
            'name',
            'active',
            'created_at',
            'updated_at',
        ],
        'report_expenses_country_category' => [
            'active',
            'created_at',
            'updated_at',
        ],
        'report_expenses_country_item' => [
            'type',
            'active',
            'created_at',
            'updated_at',
        ],
        'report_expenses_item' => [
            'code',
            'name',
            'type',
            'active',
            'created_at',
            'updated_at',
            'detailed',
            'is_expense',
        ]

    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addForeignKey($this->getFkName('report_expenses_country', 'country_id'), 'report_expenses_country', 'country_id', 'country', 'id', self::SET_NULL, self::CASCADE);

        $this->addForeignKey($this->getFkName('report_expenses_country_category', 'country_id'), 'report_expenses_country_category', 'country_id', 'report_expenses_country', 'id', self::CASCADE, self::CASCADE);
        $this->addForeignKey($this->getFkName('report_expenses_country_category', 'category_id'), 'report_expenses_country_category', 'category_id', 'report_expenses_category', 'id', self::CASCADE, self::CASCADE);
        $this->addForeignKey($this->getFkName('report_expenses_country_category', 'office_id'), 'report_expenses_country_category', 'office_id', 'salary_office', 'id', self::CASCADE, self::CASCADE);
        $this->addForeignKey($this->getFkName('report_expenses_country_category', 'delivery_id'), 'report_expenses_country_category', 'delivery_id', 'delivery', 'id', self::CASCADE, self::CASCADE);

        $this->addForeignKey($this->getFkName('report_expenses_country_item', 'category_id'), 'report_expenses_country_item', 'category_id', 'report_expenses_country_category', 'id', self::CASCADE, self::CASCADE);

        $this->addForeignKey($this->getFkName('report_expenses_country_item_product', 'item_id'), 'report_expenses_country_item_product', 'item_id', 'report_expenses_item', 'id', self::CASCADE, self::CASCADE);
        $this->addForeignKey($this->getFkName('report_expenses_country_item_product', 'product_id'), 'report_expenses_country_item_product', 'product_id', 'product', 'id', self::CASCADE, self::CASCADE);

        $this->addForeignKey($this->getFkName('report_expenses_item', 'category_id'), 'report_expenses_item', 'category_id', 'report_expenses_country_category', 'id', self::CASCADE, self::CASCADE);

        foreach ($this->tables as $table => $columns) {
            foreach ($columns as $column) {
                $this->createIndex($this->getIdxName($table, $column), $table, $column);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey($this->getFkName('report_expenses_country', 'country_id'), 'report_expenses_country', 'country_id');

        $this->dropForeignKey($this->getFkName('report_expenses_country_category', 'country_id'), 'report_expenses_country_category', 'country_id');
        $this->dropForeignKey($this->getFkName('report_expenses_country_category', 'category_id'), 'report_expenses_country_category', 'category_id');
        $this->dropForeignKey($this->getFkName('report_expenses_country_category', 'office_id'), 'report_expenses_country_category', 'office_id');
        $this->dropForeignKey($this->getFkName('report_expenses_country_category', 'delivery_id'), 'report_expenses_country_category', 'delivery_id');

        $this->dropForeignKey($this->getFkName('report_expenses_country_item', 'category_id'), 'report_expenses_country_item', 'category_id');

        $this->dropForeignKey($this->getFkName('report_expenses_country_item_product', 'item_id'), 'report_expenses_country_item_product', 'item_id');
        $this->dropForeignKey($this->getFkName('report_expenses_country_item_product', 'product_id'), 'report_expenses_country_item_product', 'product_id');

        $this->dropForeignKey($this->getFkName('report_expenses_item', 'category_id'), 'report_expenses_item', 'category_id');

        foreach ($this->tables as $table => $columns) {
            foreach ($columns as $column) {
                $this->createIndex($this->getIdxName($table, $column), $table, $column);
            }
        }
    }
}
