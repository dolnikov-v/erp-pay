<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryApiClass;
use app\modules\delivery\models\Delivery;
use app\models\Country;

/**
 * Class m170531_141419_add_delivery_correosdemexico
 */
class m170531_141419_add_delivery_correosdemexico extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        if (DeliveryApiClass::find()->where(
            ['name' => 'CorreosDeMexicoApi', 'class_path' => '/correosdemexico-api/src/correosdemexicoApi.php']
        )->exists()
        ) {
            $apiClass = DeliveryApiClass::findOne(
                ['name' => 'CorreosDeMexicoApi', 'class_path' => '/correosdemexico-api/src/correosdemexicoApi.php']
            );
        } else {
            $apiClass = new DeliveryApiClass();
        }
        $apiClass->name = 'CorreosDeMexicoApi';
        $apiClass->class_path = '/correosdemexico-api/src/correosdemexicoApi.php';
        $apiClass->active = '1';
        $apiClass->created_at = time();
        $apiClass->updated_at = time();
        $apiClass->save();

        if (Country::find()->where(
            [
                'name' => 'Мексика',
                'char_code' => 'MX'
            ]
        )->exists()
        ) {
            $country = Country::find()->where(
                [
                    'name' => 'Мексика',
                    'char_code' => 'MX'
                ]
            )->select('id')->one();
        } else {
            //нет страны
            return false;
        }

        if (Delivery::find()->where(
            [
                'name' => 'CorreosDeMexico',
                'char_code' => 'CDM'
            ]
        )->exists()
        ) {
            $delivery = Delivery::findOne(
                [
                    'name' => 'CorreosDeMexico',
                    'char_code' => 'CDM'
                ]
            );
        } else {
            $delivery = new Delivery();
        }
        $delivery->country_id = $country->id;
        $delivery->name = 'CorreosDeMexico';
        $delivery->char_code = 'CDM';
        $delivery->api_class_id = $apiClass->id;
        $delivery->active = '1';
        $delivery->auto_sending = '0';
        $delivery->save(false);
        return true;
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        if (Country::find()->where(
            [
                'name' => 'Мексика',
                'char_code' => 'MX'
            ]
        )->exists()
        ) {
            $country = Country::find()->where(
                [
                    'name' => 'Мексика',
                    'char_code' => 'MX'
                ]
            )->select('id')->one();
        } else {
            //нет страны
            return false;
        }

        DeliveryApiClass::find()
            ->where([
                'name' => 'CorreosDeMexicoApi',
                'class_path' => '/correosdemexico-api/src/correosdemexicoApi.php'
            ])
            ->one()
            ->delete();

        Delivery::findOne(
            [
                'name' => 'CorreosDeMexico',
                'country_id' => $country->id
            ]
        )->delete();
        return true;
    }
}
