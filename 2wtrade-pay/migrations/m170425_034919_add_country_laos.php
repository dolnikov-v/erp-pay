<?php
use app\components\CustomMigration as Migration;
use app\models\Country;
use app\models\Currency;
use app\models\Timezone;

/**
 * Class m170425_034919_add_country_laos
 */
class m170425_034919_add_country_laos extends Migration
{

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $result = Country::find()
            ->where(['char_code' => 'LA'])
            ->exists();

        if (!$result) {
            $currency = Currency::find()
                ->where([
                    'char_code' => 'LAK',
                    'num_code' => 418,
                ])
                ->one();

            if (!$currency) {
                $currency = new Currency;
                $currency->name = 'Лаосский кип';
                $currency->char_code = 'LAK';
                $currency->num_code = 418;
                $currency->save();
            }

            $timeZone = Timezone:: find()
                ->where(['timezone_id' => 'Asia/Bangkok'])
                ->one();

            if (!$currency) {
                $currency = new Currency;
                $currency->char_code = 'LAK';
                $currency->num_code = 418;
                $currency->save();
            }

            if ($timeZone && $currency->id) {
                $country = new Country();
                $country->name = 'Лаос';
                $country->slug = 'la';
                $country->char_code = 'LA';
                $country->currency_id = $currency->id;
                $country->timezone_id = $timeZone->id;
                $result = $country->save();
            }
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $country = Country::find()
            ->where(['char_code' => 'LA'])
            ->one();

        if ($country) {
            $currency = Currency::find()
                ->where([
                    'char_code' => 'LAK',
                    'num_code' => 418,
                ])
                ->one();

            if ($currency) {
                $currency->delete();
            }
            $country->delete();
        }
    }
}
