<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170523_092945_add_permission_for_group_operations_cc_users
 */
class m170523_092945_add_permission_for_group_operations_cc_users extends Migration
{
    public   $permissions = [
        'callcenter.user.changeteamleader',
        'callcenter.user.deactivateusers',
        'callcenter.user.activateusers'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        foreach($this->permissions as $v) {
            $this->insert('{{%auth_item}}', array(
                'name' => $v,
                'type' => '2',
                'description' => $v
            ));

            $this->insert($this->authManager->itemChildTable, array(
                'parent' => 'country.curator',
                'child' => $v
            ));
        }

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach($this->permissions as $v) {
            $this->delete($this->authManager->itemChildTable, ['child' => $v, 'parent' => 'country.curator']);
            $this->delete('{{%auth_item}}', ['name' => $v]);
        }

    }
}
