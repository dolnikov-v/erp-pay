<?php
use app\components\CustomMigration as Migration;
use app\models\User;

class m000000_000005_user extends Migration
{
    public function safeUp()
    {
        $this->createTable(User::tableName(), [
            'id' => $this->primaryKey(),
            'username' => $this->string(32)->notNull(),
            'password' => $this->string(128)->notNull(),
            'auth_key' => $this->string(32)->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
            'ping_at' => $this->integer()->defaultValue(null),
            'lastact_at' => $this->integer()->defaultValue(null),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $this->tableOptions);

        $user = new User();
        $user->username = 'superadmin';
        $user->setPassword('superadmin');
        $user->generateAuthKey();
        $user->status = User::STATUS_DEFAULT;
        $user->save(false);

        $this->authManager->removeAll();

        $role = $this->authManager->createRole(User::ROLE_SUPERADMIN);
        $role->description = 'Суперадминистратор';
        $this->authManager->add($role);
        $this->authManager->assign($role, $user->id);
    }

    public function safeDown()
    {
        $this->dropTable(User::tableName());
    }
}
