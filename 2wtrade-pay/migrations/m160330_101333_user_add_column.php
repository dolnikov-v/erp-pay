<?php

use app\components\CustomMigration as Migration;
use app\models\User;

class m160330_101333_user_add_column extends Migration
{
    public function up()
    {
        $this->addColumn(User::tableName(), 'timezone_id', 'integer(11) DEFAULT NULL AFTER `password`');
    }

    public function down()
    {
        $this->dropColumn(User::tableName(), 'timezone_id');
    }
}
