<?php
use app\components\CustomMigration as Migration;

/**
 * Class m180209_041142_skype_user
 */
class m180209_041142_skype_user extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropTable('skype_answer');
        $this->createTable('skype_user', [
            'id' => $this->primaryKey(),
            'skype_id' => $this->string(50),
            'skype_name' => $this->string(50),
            'conversation_id' => $this->string(50),
            'bot_id' => $this->string(50),
            'bot_name' => $this->string(50),
            'service_url' => $this->string(50),
            'answer' => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->createTable('skype_answer', [
            'id' => $this->primaryKey(),
            'answer' => $this->text(),
            'created_at' => $this->integer()
        ]);
        $this->dropTable('skype_user');
    }
}
