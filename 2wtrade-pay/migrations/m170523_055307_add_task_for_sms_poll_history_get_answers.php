<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m170523_055307_add_task_for_sms_poll_history_get_answers
 */
class m170523_055307_add_task_for_sms_poll_history_get_answers extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = new CrontabTask();
        $crontabTask->name = 'get_answers_for_sms_poll_history';
        $crontabTask->description = 'Получение ответов смс опроса с 2wstore.com';
        $crontabTask->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = CrontabTask::find()
            ->byName('get_answers_for_sms_poll_history')
            ->one();

        if ($crontabTask) {
            $crontabTask->delete();
        }

    }
}
