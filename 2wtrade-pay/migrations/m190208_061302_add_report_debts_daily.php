<?php
use app\components\PermissionMigration as Migration;

/**
 * Class m190208_061302_add_report_debts_daily
 */
class m190208_061302_add_report_debts_daily extends Migration
{

    protected $permissions = [
        'report.debts.dynamic',
        'report.debts.dynamicreload'
    ];

    protected $roles = [
        'admin' => [
            'report.debts.dynamic',
            'report.debts.dynamicreload'
        ],
        'country.curator' => [
            'report.debts.dynamic',
            'report.debts.dynamicreload'
        ],
        'finance.director' => [
            'report.debts.dynamic',
            'report.debts.dynamicreload'
        ],
        'finance.manager' => [
            'report.debts.dynamic',
            'report.debts.dynamicreload'
        ],
        'operational.director' => [
            'report.debts.dynamic',
            'report.debts.dynamicreload'
        ],
        'project.manager' => [
            'report.debts.dynamic',
            'report.debts.dynamicreload'
        ],
        'sales.director' => [
            'report.debts.dynamic',
            'report.debts.dynamicreload'
        ],
        'support.manager' => [
            'report.debts.dynamic',
            'report.debts.dynamicreload'
        ],
        'technical.director' => [
            'report.debts.dynamic',
            'report.debts.dynamicreload'
        ],
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('report_debts_daily', [
            'delivery_id' => $this->integer(),
            'date' => $this->date(),
            'amount' => $this->decimal(15, 2),
            'created_at' => $this->integer(),
        ]);

        $this->addPrimaryKey('', 'report_debts_daily', [
            'delivery_id',
            'date'
        ]);

        $this->addForeignKey(null, 'report_debts_daily', 'delivery_id', 'delivery', 'id', self::CASCADE, self::CASCADE);

        $this->insert('crontab_task', [
            'name' => 'report_save_delivery_debts_daily',
            'description' => 'Дебиторская задолженность сохранение ежедневных данных',
            'active' => 1
        ]);

        $this->insert('notification', [
            'description' => 'Завершен пересчет данных по выбранному фильтру динамики дебиторской задолженности, <a href="{link}">обновите страницу</a>',
            'trigger' => 'delivery.debts.dynamic.reload',
            'type' => 'info',
            'group' => 'report',
            'active' => 1
        ]);

        parent::safeUp();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('report_debts_daily');
        $this->delete('crontab_task', ['name' => 'report_save_delivery_debts_daily']);
        $this->delete('notification', ['trigger' => 'delivery.debts.dynamic.reload']);

        parent::safeDown();
    }
}
