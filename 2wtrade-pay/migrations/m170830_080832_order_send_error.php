<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170830_080832_order_send_error
 */
class m170830_080832_order_send_error extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('order_send_error', [
            'type' => $this->string(100),
            'order_id' => $this->integer(),
            'error' => $this->text(),
            'response' => $this->text(),
            'cron_launched_at' => $this->integer(),
            $this->includePrimaryKey(['type', 'order_id']),
        ], $this->tableOptions);

        $this->addForeignKey(null, 'order_send_error', 'order_id', 'order', 'id', self::CASCADE, self::CASCADE);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('order_send_error');
    }
}
