<?php

use app\components\CustomMigration as Migration;

/**
 * Class m190203_103631_add_skype_from_notifications
 */
class m190203_103631_add_skype_from_notifications extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('user_notification_setting', 'skype_active', $this->integer(1)->defaultValue(0));

        $this->insert('notification', [
            'description' => "Дебеторская задолжность.\n{text}",
            'trigger' => 'debts.notification.three.month',
            'type' => 'info',
            'group' => 'report',
            'active' => 1
        ]);

        $this->insert('crontab_task', [
            'name' => 'send_skype_notifications',
            'description' => 'Уведомления. Отправка в Skype.',
            'active' => 1
        ]);

        $this->insert('crontab_task', [
            'name' => 'debts_notification_three_month',
            'description' => 'Уведомления. ДЗ за 3 последних месяца.',
            'active' => 1
        ]);

        $this->insert('notification_role', [
            'role' => 'admin',
            'trigger' => 'debts.notification.three.month'
        ]);

        $this->getDb()->createCommand("INSERT INTO `user_notification_setting` (`user_id`, `trigger`, `active`, `skype_active`) SELECT id, 'debts.notification.three.month', 1, 1 FROM `user` WHERE username in ('yulia', 'a.lebedeva')")->execute();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete('user_notification_setting', ['trigger' => 'debts.notification.three.month']);
        $this->delete('notification_role', ['role' => 'admin', 'trigger' => 'debts.notification.three.month']);
        $this->delete('crontab_task', ['name' => 'debts_notification_three_month']);
        $this->delete('crontab_task', ['name' => 'send_skype_notifications']);
        $this->delete('notification', ['trigger' => 'debts.notification.three.month']);
        $this->dropColumn('user_notification_setting', 'skype_active');
    }
}
