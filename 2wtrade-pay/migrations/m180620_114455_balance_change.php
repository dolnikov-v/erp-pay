<?php
use app\components\PermissionMigration as Migration;

/**
 * Class m180620_114455_balance_change
 */
class m180620_114455_balance_change extends Migration
{
    protected $permissions = ['storage.balance.change'];

    protected $roles = [
        'project.manager' => [
            'storage.balance.change'
        ],
        'admin' => [
            'storage.balance.change'
        ],
        'controller.analyst' => [
            'storage.balance.change'
        ],
        'country.curator' => [
            'storage.balance.change'
        ],
        'partner' => [
            'storage.balance.change'
        ],
        'storage.manager' => [
            'storage.balance.change'
        ],
        'support.manager' => [
            'storage.balance.change'
        ],
        'technical.director' => [
            'storage.balance.change'
        ],
    ];
}
