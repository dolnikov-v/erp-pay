<?php
use app\models\Notification;
use app\components\CustomMigration as Migration;
use yii\db\Query;

/**
 * Class m181106_082742_notification_pedning_requests_error
 */
class m181106_082742_notification_pedning_requests_error extends Migration
{

    protected $notifications = [
        'call.center.requests.error.summary' => '{count} заказов с ошибкой заявок в КЦ, {message}',
        'delivery.requests.error.summary' => '{count} заказов с ошибкой заявок в КС, {message}',
    ];

    protected $crons = [
        'requests_error_summary' => 'Уведомление об ошибках заявок в КЦ и КС',
    ];

    protected $roles = ['technical.director', 'support.manager', 'country.curator', 'project.manager'];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        foreach ($this->notifications as $trigger => $description) {

            $notificationId = (new Query())->select('id')
                ->from('notification')
                ->where(['trigger' => $trigger])
                ->scalar();

            $data = [
                'description' => $description,
                'trigger' => $trigger,
                'type' => Notification::TYPE_WARNING,
                'group' => Notification::GROUP_REPORT,
                'active' => Notification::ACTIVE,
            ];

            if (!$notificationId) {
                $this->insert('notification', $data);
            } else {
                $this->update('notification', $data, ['id' => $notificationId]);
            }
        }

        foreach ($this->crons as $name => $description) {
            $cronTaskId = (new Query())->select('id')
                ->from('crontab_task')
                ->where(['name' => $name])
                ->scalar();

            $data = [
                'name' => $name,
                'description' => $description,
                'updated_at' => time(),
            ];

            if (!$cronTaskId) {
                $this->insert('crontab_task', $data);
            } else {
                $data['created_at'] = time();
                $this->update('crontab_task', $data, ['id' => $cronTaskId]);
            }
        }

        foreach ($this->roles as $role) {
            foreach ($this->notifications as $trigger => $description) {
                $this->insert('notification_role', [
                    'role' => $role,
                    'trigger' => $trigger,
                    'created_at' => time(),
                    'updated_at' => time(),
                ]);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach ($this->notifications as $trigger => $description) {
            $this->delete('user_notification_setting', ['trigger' => $trigger]);
            $this->delete('notification', ['trigger' => $trigger]);
        }

        foreach ($this->crons as $name => $description) {
            $this->delete('crontab_task', ['name' => $name]);
        }
    }
}