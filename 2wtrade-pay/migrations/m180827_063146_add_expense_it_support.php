<?php
use app\components\CustomMigration as Migration;
use app\modules\salary\models\Office;


/**
 * Class m180827_063146_add_expense_it_support
 */
class m180827_063146_add_expense_it_support extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('salary_office', 'expense_it_support', $this->double()->after('expense_courier_services'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('salary_office', 'expense_it_support');
    }
}
