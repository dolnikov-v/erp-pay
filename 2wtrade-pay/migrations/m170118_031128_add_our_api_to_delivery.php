<?php

use app\components\CustomMigration;
use app\modules\delivery\models\Delivery;

/**
 * Handles adding our_api to table `delivery`.
 */
class m170118_031128_add_our_api_to_delivery extends CustomMigration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Delivery::tableName(), 'our_api', $this->integer(1)->defaultValue(0));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Delivery::tableName(), 'our_api');
    }
}
