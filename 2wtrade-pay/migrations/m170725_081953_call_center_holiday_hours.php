<?php
use app\components\CustomMigration as Migration;
use app\modules\salary\models\Office;

/**
 * Class m170725_081953_call_center_holiday_hours
 */
class m170725_081953_call_center_holiday_hours extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Office::tableName(), 'holiday_hours', $this->float()->defaultValue(9));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Office::tableName(), 'holiday_hours');
    }
}
