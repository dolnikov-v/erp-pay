<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160923_130625_delivery_order_logistic_list
 */
class m160923_130625_delivery_order_logistic_list extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('order_logistic_list', 'delivery_id', $this->integer()->defaultValue(null)->after('country_id'));

        $this->addForeignKey(null, 'order_logistic_list', 'delivery_id', 'delivery', 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey($this->getFkName('order_logistic_list', 'delivery_id'), 'order_logistic_list');

        $this->dropColumn('order_logistic_list', 'delivery_id');
    }
}
