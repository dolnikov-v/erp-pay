<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\models\Order;
use app\modules\callcenter\models\CallCenterRequest;


/**
 * Class m170202_041828_add_swift_api
 */
class m170203_133560_add_indexes_to_some_fields extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createIndex(null, DeliveryRequest::tableName(), 'created_at');
        $this->createIndex(null, DeliveryRequest::tableName(), 'cron_launched_at');
        $this->createIndex(null, DeliveryRequest::tableName(), 'sent_at');
        $this->createIndex(null, DeliveryRequest::tableName(), 'approved_at');
        $this->createIndex(null, DeliveryRequest::tableName(), 'accepted_at');
        $this->createIndex(null, DeliveryRequest::tableName(), 'done_at');

        $this->createIndex(null, Order::tableName(), 'created_at');

        $this->createIndex(null, CallCenterRequest::tableName(), 'created_at');
        $this->createIndex(null, CallCenterRequest::tableName(), 'sent_at');
        $this->createIndex(null, CallCenterRequest::tableName(), 'approved_at');




    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropIndex($this->getIdxName(DeliveryRequest::tableName(), 'created_at'), DeliveryRequest::tableName());
        $this->dropIndex($this->getIdxName(DeliveryRequest::tableName(), 'cron_launched_at'), DeliveryRequest::tableName());
        $this->dropIndex($this->getIdxName(DeliveryRequest::tableName(), 'sent_at'), DeliveryRequest::tableName());
        $this->dropIndex($this->getIdxName(DeliveryRequest::tableName(), 'approved_at'), DeliveryRequest::tableName());
        $this->dropIndex($this->getIdxName(DeliveryRequest::tableName(), 'accepted_at'), DeliveryRequest::tableName());
        $this->dropIndex($this->getIdxName(DeliveryRequest::tableName(), 'done_at'), DeliveryRequest::tableName());

        $this->dropIndex($this->getIdxName(Order::tableName(), 'created_at'), Order::tableName());

        $this->dropIndex($this->getIdxName(CallCenterRequest::tableName(), 'created_at'), CallCenterRequest::tableName());
        $this->dropIndex($this->getIdxName(CallCenterRequest::tableName(), 'sent_at'), CallCenterRequest::tableName());
        $this->dropIndex($this->getIdxName(CallCenterRequest::tableName(), 'approved_at'), CallCenterRequest::tableName());

    }
}
