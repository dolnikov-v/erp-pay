<?php
use app\components\CustomMigration as Migration;

use app\modules\administration\models\CrontabTask;

/**
 * Class m180815_051417_cron_monthly_staffing_bonuses
 */
class m180815_051417_cron_monthly_staffing_bonuses extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_MAKE_MONTHLY_BONUS]);

        if (!($crontabTask instanceof CrontabTask)) {
            $crontabTask = new CrontabTask();
            $crontabTask->name = CrontabTask::TASK_MAKE_MONTHLY_BONUS;
            $crontabTask->description = 'Ежемесячные бонусы в ЗП проект';
            $crontabTask->save();
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_MAKE_MONTHLY_BONUS]);

        if ($crontabTask) {
            $crontabTask->delete();
        }
    }
}
