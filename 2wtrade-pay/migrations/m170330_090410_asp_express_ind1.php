<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryApiClass;
use app\models\Country;

/**
 * Class m170330_090410_asp_express_ind1
 */
class m170330_090410_asp_express_ind1 extends Migration
{

    /**
     * @return Country
     */
    private function getCountry($number)
    {
        $country = Country::find()
            ->select('id')
            ->where([
                'char_code' => 'ID',
                'name' => 'Индонезия '.$number,
            ])
            ->one();

        if (!$country) {
            $country = new Country();
            $country->char_code = 'ID';
            $country->name = 'Индонезия '.$number;
            $country->save();
        }
        return $country;
    }

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $apiClass = DeliveryApiClass::findOne(['name' => 'aspExpressApi']);

        if (!($apiClass instanceof DeliveryApiClass)) {
            $apiClass = new DeliveryApiClass();
        }
        $apiClass->name = 'aspExpressApi2';
        $apiClass->class_path = '/aspexpress-api/src/ID2/aspExpressApiID2.php';
        $apiClass->active = 1;
        $apiClass->save();


        $country = $this->getCountry(2);
        $delivery = Delivery::findOne(['char_code' => 'ASP', 'country_id' => $country->id]);
        if (!$delivery) {
            $delivery = new Delivery();
            $delivery->country_id = $country->id;
            $delivery->name = 'ASPEXPRESS';
            $delivery->char_code = 'ASP';
            $delivery->auto_sending = 0;
        }
        if (!$delivery->created_at) {
            $delivery->created_at = time();
        }
        if (!$delivery->updated_at) {
            $delivery->updated_at = time();
        }
        $delivery->api_class_id = $apiClass->id;
        $delivery->active = 1;
        $delivery->save();



        $apiClass = DeliveryApiClass::findOne(['name' => 'aspExpressApi1']);

        if (!($apiClass instanceof DeliveryApiClass)) {
            $apiClass = new DeliveryApiClass();
        }
        $apiClass->name = 'aspExpressApi1';
        $apiClass->class_path = '/aspexpress-api/src/ID1/aspExpressApiID1.php';
        $apiClass->active = 1;
        $apiClass->save();

        $country = $this->getCountry(1);
        $delivery = Delivery::findOne(['char_code' => 'ASP', 'country_id' => $country->id]);
        if (!$delivery) {
            $delivery = new Delivery();
            $delivery->country_id = $country->id;
            $delivery->name = 'ASPEXPRESS';
            $delivery->char_code = 'ASP';
            $delivery->auto_sending = 0;
        }
        if (!$delivery->created_at) {
            $delivery->created_at = time();
        }
        if (!$delivery->updated_at) {
            $delivery->updated_at = time();
        }
        $delivery->api_class_id = $apiClass->id;
        $delivery->active = 1;
        $delivery->save();

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $apiClass = DeliveryApiClass::findOne(['name' => 'aspExpressApi2']);
        if ($apiClass instanceof DeliveryApiClass) {
            $apiClass->name = 'aspExpressApi';
            $apiClass->class_path = '/aspexpress-api/src/aspExpressApi.php';
            $apiClass->save();
        }

        $apiClass = DeliveryApiClass::findOne(['name' => 'aspExpressApi1']);
        if ($apiClass instanceof DeliveryApiClass) {
            $apiClass->delete();
        }
    }
}
