<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170216_030744_delivery_new_fields
 */
class m170216_030744_delivery_new_fields extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('delivery', 'delivery_cost_percent', $this->double()->defaultValue(0));
        $this->addColumn('delivery', 'fulfillment_cost', $this->double()->defaultValue(0));
        $this->addColumn('delivery', 'nds', $this->double()->defaultValue(0));
        $this->addColumn('delivery', 'fulfillment_nds', $this->double()->defaultValue(0));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('delivery', 'delivery_cost_percent');
        $this->dropColumn('delivery', 'fulfillment_cost');
        $this->dropColumn('delivery', 'nds');
        $this->dropColumn('delivery', 'fulfillment_nds');
    }
}
