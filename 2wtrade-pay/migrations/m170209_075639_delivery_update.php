<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170209_075639_delivery_update
 */
class m170209_075639_delivery_update extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        /*
         * Новая таблица
         * delivery_zipcodes - индексы для служб доставки
         */
        $this->createTable('delivery_zipcodes', [
            'id' => $this->primaryKey(),
            'name' => $this->string(200)->notNull(),
            'zipcodes' => $this->text(),
            'price' => $this->double()->defaultValue(0),
            'delivery_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull()
        ]);

        $this->addForeignKey(null, 'delivery_zipcodes', 'delivery_id', 'delivery', 'id', self::CASCADE, self::CASCADE);

        $this->truncateTable('delivery_zipcodes');
        $this->execute('insert into delivery_zipcodes(name, zipcodes, delivery_id) 
        select "default", delivery.zipcodes, delivery.id from delivery');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('delivery_zipcodes');
    }
}
