<?php
use app\components\CustomMigration as Migration;
use app\models\CountryOrderWorkflow;

/**
 * Class m170307_062647_delete_in_country_order_workflow_by_workflow_id_13_16
 */
class m170307_062647_delete_in_country_order_workflow_by_workflow_id_13_16 extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->delete(CountryOrderWorkflow::tableName(), ['workflow_id' => 13]);
        $this->delete(CountryOrderWorkflow::tableName(), ['workflow_id' => 16]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->insert(CountryOrderWorkflow::tableName(), ['country_id' => 9, 'workflow_id' => 13]);
        $this->insert(CountryOrderWorkflow::tableName(), ['country_id' => 6, 'workflow_id' => 13]);
    }
}
