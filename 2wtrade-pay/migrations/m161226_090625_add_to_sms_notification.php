<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;
use app\models\Country;

/**
 * Class m161226_090625_add_to_sms_notification
 */
class m161226_090625_add_to_sms_notification extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('order_sms_notification_request', 'api_error', $this->string(255));
        $this->addColumn('order_sms_notification_request', 'api_code', $this->integer());
        $this->addColumn(Country::tableName(), 'sms_notifier_phone_from', $this->string(50));

        $crontabTask = new CrontabTask();
        $crontabTask->name = 'sms_notifier_send_sms_for_order';
        $crontabTask->description = 'Рассылка СМС уведомлений клиентам';
        $crontabTask->save();

        $crontabTask = new CrontabTask();
        $crontabTask->name = 'sms_notifier_check_sms_status_for_order';
        $crontabTask->description = 'Обновление статусов заявок смс уведомлений';
        $crontabTask->save();

        $this->createTable('order_sms_notification_request_log', [
            'id' => $this->primaryKey(),
            'order_sms_notification_request_id' => $this->integer()->notNull(),
            'user_id' => $this->integer(),
            'route' => $this->string(255),
            'group_id' => $this->string(255),
            'field' => $this->string(255),
            'old' => $this->string(255),
            'new' => $this->string(255),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $this->tableOptions);

        $this->addForeignKey(null, 'order_sms_notification_request_log', 'order_sms_notification_request_id',
            'order_sms_notification_request', 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, 'order_sms_notification_request_log', 'user_id', 'user', 'id', self::CASCADE,
            self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('order_sms_notification_request_log');
        $crontabTask = CrontabTask::find()
            ->byName('sms_notifier_send_sms_for_order')
            ->one();

        if ($crontabTask) {
            $crontabTask->delete();
        }

        $crontabTask = CrontabTask::find()
            ->byName('sms_notifier_check_sms_status_for_order')
            ->one();

        if ($crontabTask) {
            $crontabTask->delete();
        }

        $this->dropColumn(Country::tableName(), 'sms_notifier_phone_from');
        $this->dropColumn('order_sms_notification_request', 'api_error');
        $this->dropColumn('order_sms_notification_request', 'api_code');
    }
}
