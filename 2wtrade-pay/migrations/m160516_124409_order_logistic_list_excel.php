<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160516_124409_order_logistic_list_excel
 */
class m160516_124409_order_logistic_list_excel extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropColumn('order_logistic_list', 'list');

        $this->createTable('order_logistic_list_excel', [
            'id' => $this->primaryKey(),
            'list_id' => $this->integer()->notNull(),
            'format' => $this->string(20)->notNull(),
            'columns_hash' => $this->string(32)->notNull(),
            'excel' => $this->string(100)->notNull(),
            'excel_local' => $this->string(100)->notNull(),
            'user_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(null, 'order_logistic_list_excel', 'list_id', 'order_logistic_list', 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, 'order_logistic_list_excel', 'user_id', 'user', 'id', self::CASCADE, self::RESTRICT);
        $this->createIndex(null, 'order_logistic_list_excel', 'format');
        $this->createIndex(null, 'order_logistic_list_excel', 'columns_hash');
        $this->createIndex(null, 'order_logistic_list_excel', 'excel');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('order_logistic_list_excel');

        $this->addColumn('order_logistic_list', 'list', $this->string(100)->defaultValue(null) . ' AFTER `ticket`');
    }
}
