<?php
use app\components\CustomMigration as Migration;

/**
 * Class m181212_045248_add_create_source_for_call_centers
 */
class m181212_045248_add_create_source_for_call_centers extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        if (!(new \yii\db\Query())->from('source')->where(['name' => 'call_center'])->exists($this->getDb())) {
            $this->insert('source', [
                'name' => 'call_center',
                'active' => 1,
                'use_pseudo_approve' => 0,
                'created_at' => time(),
                'updated_at' => time()
            ]);
        }
        if (!(new \yii\db\Query())->from('source')->where(['name' => '2wcall'])->exists($this->getDb())) {
            $this->insert('source', [
                'name' => '2wcall',
                'active' => 1,
                'use_pseudo_approve' => 0,
                'created_at' => time(),
                'updated_at' => time()
            ]);
        }
        $newCallCenterSourceId = (new \yii\db\Query())->from('source')->where(['name' => '2wcall'])->select('id')->scalar($this->getDb());
        $oldCallCenterSourceId = (new \yii\db\Query())->from('source')->where(['name' => 'call_center'])->select('id')->scalar($this->getDb());

        $this->addColumn('call_center', 'source_id', $this->integer()->after('country_id'));
        $this->addForeignKey($this->getFkName('call_center', 'source_id'), 'call_center', 'source_id', 'source', 'id', self::SET_NULL, self::CASCADE);

        $this->update('call_center', ['source_id' => $newCallCenterSourceId], ['is_amazon_query' => 1]);
        $this->update('call_center', ['source_id' => $oldCallCenterSourceId], ['is_amazon_query' => 0]);

        $this->update('notification',
            ['description'=>'Не удалось добавить нового лида #{foreign_id} от {source}. Ошибка: {exception}'],
            ['trigger' => 'lead.not.added']
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey($this->getFkName('call_center', 'source_id'), 'call_center', 'source_id');
        $this->dropColumn('call_center', 'source_id');
        $this->update('notification',
            ['description'=>'Не удалось добавить нового лида от {source}. Ошибка: {exception}'],
            ['trigger' => 'lead.not.added']
        );
    }
}
