<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170209_043021_widget_top_goods_add_role
 */

use app\modules\widget\models\WidgetType;
use app\modules\widget\models\WidgetRole;

class m170209_043021_widget_top_goods_add_role extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $type = WidgetType::find()
            ->where(['code' => 'top_goods'])
            ->one();

        $this->insert(WidgetRole::tableName(), [
            'role' => 'country.curator',
            'type_id' => $type->id,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $type = WidgetType::find()
            ->where(['code' => 'top_goods'])
            ->one();

        $this->delete(WidgetRole::tableName(), [
            'role' => 'country.curator',
            'type_id' => $type->id,
        ]);
    }
}
