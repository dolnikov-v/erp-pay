<?php
use app\components\CustomMigration as Migration;
use app\modules\callcenter\models\CallCenterUser;
use app\modules\media\models\Media;
use yii\db\Query;
use app\modules\administration\models\CrontabTask;
use app\modules\callcenter\models\CallCenter;


/**
 * Class m170321_060657_call_center_user
 */
class m170321_060657_call_center_user extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(CallCenterUser::tableName(), [
            'id' => $this->primaryKey(),
            'callcenter_id' => $this->integer(),
            'user_id' => $this->string(),
            'user_login' => $this->string(),
            'user_role' => $this->string(),
            'name' => $this->string(),
            'active' => $this->integer(1),
            'photo' => $this->string(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $this->tableOptions);

        $this->addForeignKey(null, CallCenterUser::tableName(), 'callcenter_id', CallCenter::tableName(), 'id', self::CASCADE, self::RESTRICT);

        $role = 'callcenter.hr';
        $rules = [
            'home.index.index',
            'callcenter.user.index',
            'callcenter.user.view',
            'callcenter.user.edit',
            'callcenter.user.activate',
            'callcenter.user.deactivate',
            'callcenter.user.deletephoto',
        ];

        $query = new Query();

        $is_role = $query->select('*')->from($this->authManager->itemTable)
            ->where(['name' => $role, 'type' => 1])
            ->one();

        if (!$is_role) {
            $this->insert($this->authManager->itemTable, [
                'name' => $role,
                'type' => 1,
                'description' => 'HR Коллцентра',
                'created_at' => time(),
                'updated_at' => time()]);
        }

        foreach ($rules as $rule) {


            $is_rule = $query->select('*')->from($this->authManager->itemTable)
                ->where(['name' => $rule, 'type' => 2])
                ->one();

            if (!$is_rule) {
                $this->insert($this->authManager->itemTable, [
                    'name' => $rule,
                    'description' => $rule,
                    'type' => 2,
                    'created_at' => time(),
                    'updated_at' => time()]);
            }

            $is_can = $query->select('*')->from($this->authManager->itemChildTable)->where([
                'parent' => $role,
                'child' => $rule
            ])->one();

            if (!$is_can) {
                $this->insert($this->authManager->itemChildTable, [
                    'parent' => $role,
                    'child' => $rule
                ]);
            }
        }

        $tasks = [
            CrontabTask::TASK_CALL_CENTER_GET_USERS => 'Получение списка пользователей Колл Центра',
        ];

        foreach ($tasks as $name => $description) {

            $crontabTask = CrontabTask::findOne(['name' => $name]);

            if (!($crontabTask instanceof CrontabTask)) {
                $crontabTask = new CrontabTask();
                $crontabTask->name = $name;
                $crontabTask->description = $description;
                $crontabTask->save();
            }
        }

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable(CallCenterUser::tableName());

        $role = 'callcenter.hr';
        $rules = [
            'callcenter.user.index',
            'callcenter.user.view',
            'callcenter.user.edit',
            'callcenter.user.activate',
            'callcenter.user.deactivate',
            'callcenter.user.deletephoto',
        ];

        foreach ($rules as $rule) {

            $this->delete($this->authManager->itemChildTable, [
                'parent' => $role,
                'child' => $rule
            ]);

            $this->delete($this->authManager->itemTable, [
                'name' => $rule,
                'type' => 2
            ]);

        }

        $this->delete($this->authManager->itemTable, [
            'name' => $role,
            'type' => 1
        ]);

        $tasks = [
            CrontabTask::TASK_CALL_CENTER_GET_USERS
        ];

        foreach ($tasks as $task) {

            $crontabTask = CrontabTask::findOne(['name' => $task]);

            if ($crontabTask instanceof CrontabTask) {
                $crontabTask->delete();
            }
        }
    }
}
