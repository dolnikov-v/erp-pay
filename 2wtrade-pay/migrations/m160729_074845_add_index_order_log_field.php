<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160729_074845_add_index_order_log_field
 */
class m160729_074845_add_index_order_log_field extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createIndex(null, 'order_log', 'field');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropIndex($this->getIdxName('order_log', 'field'), 'order_log');
    }
}
