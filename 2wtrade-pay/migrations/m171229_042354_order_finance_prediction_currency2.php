<?php
use app\components\CustomMigration as Migration;

/**
 * Class m171229_042354_order_finance_prediction_currency2
 */
class m171229_042354_order_finance_prediction_currency2 extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->renameColumn('order_finance_prediction', 'price_cod_original', 'price_cod_currency_id');
        $this->renameColumn('order_finance_prediction', 'price_storage_original', 'price_storage_currency_id');
        $this->renameColumn('order_finance_prediction', 'price_fulfilment_original', 'price_fulfilment_currency_id');
        $this->renameColumn('order_finance_prediction', 'price_packing_original', 'price_packing_currency_id');
        $this->renameColumn('order_finance_prediction', 'price_package_original', 'price_package_currency_id');
        $this->renameColumn('order_finance_prediction', 'price_delivery_original', 'price_delivery_currency_id');
        $this->renameColumn('order_finance_prediction', 'price_redelivery_original', 'price_redelivery_currency_id');
        $this->renameColumn('order_finance_prediction', 'price_delivery_back_original', 'price_delivery_back_currency_id');
        $this->renameColumn('order_finance_prediction', 'price_delivery_return_original', 'price_delivery_return_currency_id');
        $this->renameColumn('order_finance_prediction', 'price_address_correction_original', 'price_address_correction_currency_id');
        $this->renameColumn('order_finance_prediction', 'price_cod_service_original', 'price_cod_service_currency_id');
        $this->renameColumn('order_finance_prediction', 'price_vat_original', 'price_vat_currency_id');
        $this->dropColumn('order_finance_prediction', 'currency_id');

        $this->alterColumn('order_finance_prediction', 'price_cod_currency_id', $this->integer());
        $this->alterColumn('order_finance_prediction', 'price_storage_currency_id', $this->integer());
        $this->alterColumn('order_finance_prediction', 'price_fulfilment_currency_id', $this->integer());
        $this->alterColumn('order_finance_prediction', 'price_packing_currency_id', $this->integer());
        $this->alterColumn('order_finance_prediction', 'price_package_currency_id', $this->integer());
        $this->alterColumn('order_finance_prediction', 'price_delivery_currency_id', $this->integer());
        $this->alterColumn('order_finance_prediction', 'price_redelivery_currency_id', $this->integer());
        $this->alterColumn('order_finance_prediction', 'price_delivery_back_currency_id', $this->integer());
        $this->alterColumn('order_finance_prediction', 'price_delivery_return_currency_id', $this->integer());
        $this->alterColumn('order_finance_prediction', 'price_address_correction_currency_id', $this->integer());
        $this->alterColumn('order_finance_prediction', 'price_cod_service_currency_id', $this->integer());
        $this->alterColumn('order_finance_prediction', 'price_vat_currency_id', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('order_finance_prediction', 'price_cod_currency_id', $this->double());
        $this->alterColumn('order_finance_prediction', 'price_storage_currency_id', $this->double());
        $this->alterColumn('order_finance_prediction', 'price_fulfilment_currency_id', $this->double());
        $this->alterColumn('order_finance_prediction', 'price_packing_currency_id', $this->double());
        $this->alterColumn('order_finance_prediction', 'price_package_currency_id', $this->double());
        $this->alterColumn('order_finance_prediction', 'price_delivery_currency_id', $this->double());
        $this->alterColumn('order_finance_prediction', 'price_redelivery_currency_id', $this->double());
        $this->alterColumn('order_finance_prediction', 'price_delivery_back_currency_id', $this->double());
        $this->alterColumn('order_finance_prediction', 'price_delivery_return_currency_id', $this->double());
        $this->alterColumn('order_finance_prediction', 'price_address_correction_currency_id', $this->double());
        $this->alterColumn('order_finance_prediction', 'price_cod_service_currency_id', $this->double());
        $this->alterColumn('order_finance_prediction', 'price_vat_currency_id', $this->double());

        $this->addColumn('order_finance_prediction', 'currency_id', $this->integer()->after('additional_prices'));

        $this->renameColumn('order_finance_prediction', 'price_cod_currency_id', 'price_cod_original');
        $this->renameColumn('order_finance_prediction', 'price_storage_currency_id', 'price_storage_original');
        $this->renameColumn('order_finance_prediction', 'price_fulfilment_currency_id', 'price_fulfilment_original');
        $this->renameColumn('order_finance_prediction', 'price_packing_currency_id', 'price_packing_original');
        $this->renameColumn('order_finance_prediction', 'price_package_currency_id', 'price_package_original');
        $this->renameColumn('order_finance_prediction', 'price_delivery_currency_id', 'price_delivery_original');
        $this->renameColumn('order_finance_prediction', 'price_redelivery_currency_id', 'price_redelivery_original');
        $this->renameColumn('order_finance_prediction', 'price_delivery_back_currency_id', 'price_delivery_back_original');
        $this->renameColumn('order_finance_prediction', 'price_delivery_return_currency_id', 'price_delivery_return_original');
        $this->renameColumn('order_finance_prediction', 'price_address_correction_currency_id', 'price_address_correction_original');
        $this->renameColumn('order_finance_prediction', 'price_cod_service_currency_id', 'price_cod_service_original');
        $this->renameColumn('order_finance_prediction', 'price_vat_currency_id', 'price_vat_original');
    }
}
