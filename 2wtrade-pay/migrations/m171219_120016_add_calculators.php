<?php

/**
 * Class m171219_120016_add_calculators
 */
class m171219_120016_add_calculators extends \app\components\DeliveryChargesCalculatorMigration
{
    /**
     * @return array
     */
    protected static function getCalculators()
    {
        return [
            'IPS' => 'app\modules\delivery\components\charges\ips\IPS',
            'Ark' => 'app\modules\delivery\components\charges\ark\Ark'
        ];
    }
}
