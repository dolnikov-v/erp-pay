<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160420_134305_crontab_task_log
 */
class m160420_134305_crontab_task_log extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('crontab_task_log', [
            'id' => $this->primaryKey(),
            'task_id' => $this->integer()->notNull(),
            'status' => "ENUM('in_progress', 'done') DEFAULT 'in_progress'",
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(null, 'crontab_task_log', 'task_id', 'crontab_task', 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('crontab_task_log');
    }
}
