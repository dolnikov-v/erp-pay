<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m170830_165646_remove_cron_for_update_tracking_info_from_amazon_queue
 */
class m170830_165646_remove_cron_for_update_tracking_info_from_amazon_queue extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = CrontabTask::findOne(['name' => 'delivery_get_orders_from_update_queue']);

        if ($crontabTask instanceof CrontabTask) {
            $crontabTask->delete();
        }

        $this->addColumn('delivery_api_class', 'can_tracking', $this->boolean()->after('active')->defaultValue(1));
        $this->addColumn('delivery', 'can_tracking', $this->boolean()->after('auto_sending')->defaultValue(1));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = CrontabTask::findOne(['name' => 'delivery_get_orders_from_update_queue']);

        if (!($crontabTask instanceof CrontabTask)) {
            $crontabTask = new CrontabTask();
            $crontabTask->name = 'delivery_get_orders_from_update_queue';
            $crontabTask->description = "Забор заказов из амазоновской очереди обновления статусов КС";
            $crontabTask->save();
        }

        $this->dropColumn('delivery_api_class', 'can_tracking');
        $this->dropColumn('delivery', 'can_tracking');
    }
}
