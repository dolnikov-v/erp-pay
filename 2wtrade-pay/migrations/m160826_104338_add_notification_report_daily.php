<?php
use app\components\CustomMigration as Migration;
use app\models\Notification;

/**
 * Class m160826_104338_add_notification_report_daily
 */
class m160826_104338_add_notification_report_daily extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $model = new Notification([
            'description' => 'Ежедневная сверка заказов c {dateFrom} по {dateTo}, страна {country}: Создано {created}; Отправлено в колл-центр {call_sent}; Одобрено колл-центром {call_approved}; Принято курьерской службой {delivery_sent}; Отклонено курьерской службой {delivery_error}',
            'trigger' => 'report.order.daily',
            'type' => Notification::TYPE_INFO,
            'group' => Notification::GROUP_REPORT,
            'active' => Notification::ACTIVE,
        ]);

        $model->save();
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        /** @var Notification $model */
        $model = Notification::findOne(['trigger' => 'report.order.daily']);

        if ($model) {
            $model->delete();
        }
    }
}
