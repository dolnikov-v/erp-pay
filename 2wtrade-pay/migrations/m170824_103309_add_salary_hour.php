<?php
use app\components\CustomMigration as Migration;
use app\modules\salary\models\Person;
use app\modules\salary\models\MonthlyStatementData;

/**
 * Class m170824_103309_add_salary_hour
 */
class m170824_103309_add_salary_hour extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Person::tableName(), 'salary_hour_local', $this->float());
        $this->addColumn(Person::tableName(), 'salary_hour_usd', $this->float());
        $this->addColumn(MonthlyStatementData::tableName(), 'salary_hour_local', $this->float());
        $this->addColumn(MonthlyStatementData::tableName(), 'salary_hour_usd', $this->float());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Person::tableName(), 'salary_hour_local');
        $this->dropColumn(Person::tableName(), 'salary_hour_usd');
        $this->dropColumn(MonthlyStatementData::tableName(), 'salary_hour_local');
        $this->dropColumn(MonthlyStatementData::tableName(), 'salary_hour_usd');
    }
}
