<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;
/**
 * Class m160916_090452_add_cleaner_check_unlinked_files
 */
class m160916_090452_add_cleaner_check_unlinked_files extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = new CrontabTask();
        $crontabTask->name = 'check_crontab_log';
        $crontabTask->description = 'Проверка несвязанных файлов логов Crontab';
        $crontabTask->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = CrontabTask::find()
            ->byName('check_crontab_log')
            ->one();

        if ($crontabTask) {
            $crontabTask->delete();
        }
    }
}
