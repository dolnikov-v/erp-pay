<?php

use app\components\CustomMigration as Migration;

/**
 * Handles adding permission report_webmaster_index
 */
class m170113_100107_add_permission_report_webmaster_index extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}',array(
            'name'=>'report.webmaster.index',
            'type' => '2',
            'description' => 'report.webmaster.index',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'country.curator',
            'child' => 'report.webmaster.index'
        ));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->authManager->itemChildTable, ['child' => 'report.webmaster.index', 'parent' => 'country.curator']);
        $this->delete('{{%auth_item}}', ['name' => 'report.webmaster.index']);
    }
}
