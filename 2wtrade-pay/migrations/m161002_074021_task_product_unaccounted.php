<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m161002_074021_task_product_unaccounted
 */
class m161002_074021_task_product_unaccounted extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = new CrontabTask();
        $crontabTask->name = 'product_unaccounted';
        $crontabTask->description = 'Отправка оповещения о неучтенных на складе товарах.';
        $crontabTask->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = CrontabTask::find()
            ->byName('product_unaccounted')
            ->one();

        if ($crontabTask) {
            $crontabTask->delete();
        }
    }
}
