<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160422_101913_order_delivery
 */
class m160422_101913_order_delivery extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('order', 'delivery', $this->double()->defaultValue(null) . ' AFTER `income`');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('order', 'delivery');
    }
}
