<?php
use app\components\CustomMigration as Migration;

/**
 * Class m180904_084457_add_column_dismissal_pay_salary_bonus_type
 */
class m180904_084457_add_column_dismissal_pay_salary_bonus_type extends Migration
{

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('salary_bonus_type', 'is_dismissal_pay', $this->boolean()->defaultValue(false));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('salary_bonus_type', 'is_dismissal_pay');
    }
}
