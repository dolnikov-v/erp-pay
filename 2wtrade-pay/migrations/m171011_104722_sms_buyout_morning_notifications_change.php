<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;
use app\models\Notification;

/**
 * Class m171011_104722_sms_buyout_morning_notifications_change
 */
class m171011_104722_sms_buyout_morning_notifications_change extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $model = Notification::findOne(['trigger' => Notification::TRIGGER_SMS_BUYOUTS_NOTIFICATION]);
        $model->description = 'Доброе утро! {text}';
        $model->save(false);

        $crontabTask = CrontabTask::find()
            ->byName('sms_buyouts_notification')
            ->one();

        $crontabTask->description = 'Утреннее SMS уведомление о выкупах по циклу 10 дней';
        $crontabTask->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $model = Notification::findOne(['trigger' => Notification::TRIGGER_SMS_BUYOUTS_NOTIFICATION]);
        $model->description = 'Доброе утро! По состоянию на {date} средний выкуп 2wtrade составляет {vikup_percent}';
        $model->save(false);

        $crontabTask = CrontabTask::find()
            ->byName('sms_buyouts_notification')
            ->one();

        $crontabTask->description = 'Утреннее SMS уведомление о выкупах по циклу 15 дней';
        $crontabTask->save();
    }
}
