<?php
use app\components\CustomMigration as Migration;
use app\modules\widget\models\WidgetUser;
use app\modules\administration\models\CrontabTask;
use app\models\Country;
use app\modules\widget\models\WidgetType;

/**
 * Class m170307_035841_widget_cache
 */
class m170307_035841_widget_cache extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(WidgetType::tableName(), 'by_country', $this->smallInteger());
        $this->addColumn(WidgetType::tableName(), 'no_cache', $this->smallInteger());

        $codes_by_all_countries = [
            'buy_out_goods',
            'operators_efficiency',
            'top_goods',
            'top_deliveries',
            'in_stock_balance',
            'products_certificates',
            'buyout_delivery',
            'top_countries',
            'top20cities'
        ];

        foreach ($codes_by_all_countries as $code) {
            $widget = WidgetType::find()->where(['code' => $code])->one();
            if ($widget) {
                $widget->by_country = WidgetType::BY_COUNTRY;
                $widget->save();
            }
        }

        $widget = WidgetType::find()->where(['code' => 'delivery_debts'])->one();
        if ($widget) {
            $widget->no_cache = WidgetType::NO_CACHE;
            $widget->save();
        }

        $this->createTable('widget_cache', [
            'id' => $this->primaryKey(),
            'widgetuser_id' => $this->integer(),
            'country_id' => $this->integer(),
            'data' => 'MEDIUMTEXT',
            'cached_at' => $this->integer(),
        ], $this->tableOptions);

        $this->addForeignKey(null, 'widget_cache', 'widgetuser_id', WidgetUser::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, 'widget_cache', 'country_id', Country::tableName(), 'id', self::CASCADE, self::RESTRICT);

        $tasks = [
            CrontabTask::TASK_CACHE_WIDGETS => 'Кеширование данных для виджетов',
        ];

        foreach ($tasks as $name => $description) {

            $crontabTask = CrontabTask::findOne(['name' => $name]);

            if (!($crontabTask instanceof CrontabTask)) {
                $crontabTask = new CrontabTask();
                $crontabTask->name = $name;
                $crontabTask->description = $description;
                $crontabTask->save();
            }
        }

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(WidgetType::tableName(), 'by_country');
        $this->dropColumn(WidgetType::tableName(), 'no_cache');
        $this->dropTable('widget_cache');

        $tasks = [
            CrontabTask::TASK_CACHE_WIDGETS
        ];

        foreach ($tasks as $task) {

            $crontabTask = CrontabTask::findOne(['name' => $task]);

            if ($crontabTask instanceof CrontabTask) {
                $crontabTask->delete();
            }
        }

    }
}
