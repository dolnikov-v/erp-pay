<?php
use app\components\PermissionMigration as Migration;

/**
 * Class m171130_122327_get_select_option_partners
 */
class m171130_122327_get_select_option_partners extends Migration
{
    /**
     * @var array
     */
    protected $permissions = [
        'report.filter.getselectoptionspartners',
    ];

    /**
     * @var array
     */
    protected $roles = [
        'admin' => [
            'report.filter.getselectoptionspartners',
        ],
        'auditor' => [
            'report.filter.getselectoptionspartners',
        ],
        'auditor.helper' => [
            'report.filter.getselectoptionspartners',
        ],
        'callcenter.manager' => [
            'report.filter.getselectoptionspartners',
        ],
        'controller.analyst' => [
            'report.filter.getselectoptionspartners',
        ],
        'country.curator' => [
            'report.filter.getselectoptionspartners',
        ],
        'finance.director' => [
            'report.filter.getselectoptionspartners',
        ],
        'moderator_cc' => [
            'report.filter.getselectoptionspartners',
        ]
    ];
}
