<?php
use app\components\CustomMigration as Migration;
use app\modules\salary\models\Office;
/**
 * Class m170717_052225_call_center_office_holidays_extra
 */
class m170717_052225_call_center_office_holidays_extra extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Office::tableName(), 'holidays_coefficient', $this->float()->defaultValue(2));
        $this->addColumn(Office::tableName(), 'extra_days_coefficient', $this->float()->defaultValue(1));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Office::tableName(), 'holidays_coefficient');
        $this->dropColumn(Office::tableName(), 'extra_days_coefficient');
    }
}
