<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Handles the creation for table `voip_expense`.
 */
class m170809_044354_create_voip_expense extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('voip_expense', [
            'id' => $this->primaryKey(),
            'amount' => $this->float()->notNull(),
            'date' => $this->integer(11),
            'country_id' => $this->integer(3),
            'launched_at' => $this->integer(11),
        ]);
        $this->addForeignKey(null, 'voip_expense', 'country_id', 'country', 'id', self::CASCADE, self::RESTRICT);

        $crontabTask = new CrontabTask();
        $crontabTask->name = 'voip_expense';
        $crontabTask->active = 1;
        $crontabTask->description = 'Получение расходов за voip телефонию.';
        $crontabTask->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('voip_expense');
        $this->delete(CrontabTask::tableName(), ['name' => 'voip_expense']);
    }
}
