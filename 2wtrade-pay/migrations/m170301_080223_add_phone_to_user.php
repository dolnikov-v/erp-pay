<?php

use app\components\CustomMigration as Migration;
use app\models\User;
use app\models\UserNotificationSetting;
use app\models\Notification;
use app\modules\administration\models\CrontabTask;
use app\models\NotificationRole;

/**
 * Handles adding phone to table `user`.
 */
class m170301_080223_add_phone_to_user extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn(User::tableName(), 'phone', $this->string(50));
        $this->addColumn(UserNotificationSetting::tableName(), 'sms_active', $this->smallInteger()->defaultValue(0)->after('email_interval'));

        $triggers = [
            Notification::TRIGGER_REPORT_ORDERS_STATUS_IN_1_2 => 'Скопление в колонках 1 и 2 в сумме по всем странам {count} заказов',
            Notification::TRIGGER_REPORT_ORDERS_STATUS_IN_31 => 'Скопление в колонке 31 в сумме по всем странам {count} заказов',
            Notification::TRIGGER_REPORT_NO_ANSWER_FROM_CC => 'Не получали более часа статусов из КЦ по всем странам {time}',
        ];

        foreach ($triggers as $trigger => $description) {

            $notification = Notification::findOne(['trigger' => $trigger]);

            if (!($notification instanceof Notification)) {
                $notification = new Notification([
                    'description' => $description,
                    'trigger' => $trigger,
                    'type' => Notification::TYPE_WARNING,
                    'group' => Notification::GROUP_REPORT,
                    'active' => Notification::ACTIVE,
                ]);
                $notification->save(false);
            }
        }


        $tasks = [
            CrontabTask::TASK_REPORT_STATUS_IN_1_2 => 'Скопление в колонках 1 и 2 в сумме по всем странам более 500 заказов',
            CrontabTask::TASK_REPORT_STATUS_IN_31 => 'Скопление в колонке 31 в сумме по всем странам более 500 заказов',
            CrontabTask::TASK_REPORT_NO_ANSWER_FROM_CC => 'Не получали больше часа статусов из КЦ по всем странам',
            CrontabTask::TASK_SMS_NOTIFIER_SEND_NOTIFICATIONS => 'Отправка SMS нотификаций пользователям'
        ];

        foreach ($tasks as $name => $description) {

            $crontabTask = CrontabTask::findOne(['name' => $name]);

            if (!($crontabTask instanceof CrontabTask)) {
                $crontabTask = new CrontabTask();
                $crontabTask->name = $name;
                $crontabTask->description = $description;
                $crontabTask->save();
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn(User::tableName(), 'phone');
        $this->dropColumn(UserNotificationSetting::tableName(), 'sms_active');


        $triggers = [
            Notification::TRIGGER_REPORT_ORDERS_STATUS_IN_1_2,
            Notification::TRIGGER_REPORT_ORDERS_STATUS_IN_31,
            Notification::TRIGGER_REPORT_NO_ANSWER_FROM_CC
        ];

        foreach ($triggers as $trigger) {

            $notification = Notification::findOne(['trigger' => $trigger]);

            if ($notification instanceof Notification) {
                $notification->delete();
            }
        }

        $tasks = [
            CrontabTask::TASK_REPORT_STATUS_IN_1_2,
            CrontabTask::TASK_REPORT_STATUS_IN_31,
            CrontabTask::TASK_REPORT_NO_ANSWER_FROM_CC,
            CrontabTask::TASK_SMS_NOTIFIER_SEND_NOTIFICATIONS
        ];

        foreach ($tasks as $task) {

            $crontabTask = CrontabTask::findOne(['name' => $task]);

            if ($crontabTask instanceof CrontabTask) {
                $crontabTask->delete();
            }
        }
    }
}
