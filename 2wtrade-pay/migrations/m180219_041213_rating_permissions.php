<?php
use app\components\CustomMigration as Migration;

/**
 * Class m180219_041213_rating_permissions
 */
class m180219_041213_rating_permissions extends Migration
{
    private $roles = [
        'general.director',
        'development.director',
        'country.curator',
        'finance.director',
        'executive.director',
        'auditor',
        'project.manager'
    ];
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->delete('{{%auth_item}}', ['name' => 'rating.ratingproduct.index']);
        $this->insert('{{%auth_item}}', [
            'name' => 'rating.ratingproduct.index',
            'type' => '2',
            'description' => 'rating.ratingproduct.index',
            'created_at' => time(),
            'updated_at' => time()
        ]);

        $this->delete('{{%auth_item}}', ['name' => 'rating.ratingbuyoutcheck.index']);
        $this->insert('{{%auth_item}}', [
            'name' => 'rating.ratingbuyoutcheck.index',
            'type' => '2',
            'description' => 'rating.ratingbuyoutcheck.index',
            'created_at' => time(),
            'updated_at' => time()
        ]);

        foreach ($this->roles as $role) {
            $this->delete($this->authManager->itemChildTable, ['child' => 'rating.ratingproduct.index', 'parent' => $role]);
            $this->delete($this->authManager->itemChildTable, ['child' => 'rating.ratingbuyoutcheck.index', 'parent' => $role]);

            $this->insert($this->authManager->itemChildTable, array(
                'parent' => $role,
                'child' => 'rating.ratingproduct.index'
            ));
            $this->insert($this->authManager->itemChildTable, array(
                'parent' => $role,
                'child' => 'rating.ratingbuyoutcheck.index'
            ));
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach ($this->roles as $role) {
            $this->delete($this->authManager->itemChildTable, ['child' => 'rating.ratingproduct.index', 'parent' => $role]);
            $this->delete($this->authManager->itemChildTable, ['child' => 'rating.ratingbuyoutcheck.index', 'parent' => $role]);
        }

        $this->delete('{{%auth_item}}', ['name' => 'rating.ratingproduct.index']);
        $this->delete('{{%auth_item}}', ['name' => 'rating.ratingbuyoutcheck.index']);
    }
}
