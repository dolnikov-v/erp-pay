<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160802_131320_add_column_who_changed
 */
class m160802_131320_add_column_who_changed extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('order_log', 'user_id', $this->integer(11)->defaultValue(null)->after('order_id'));
        $this->addColumn('order_log', 'route', $this->string(255)->defaultValue(null)->after('user_id'));
        $this->addColumn('delivery_request_log', 'user_id', $this->integer(11)->defaultValue(null)->after('delivery_request_id'));
        $this->addColumn('delivery_request_log', 'route', $this->string(255)->defaultValue(null)->after('user_id'));
        $this->addColumn('call_center_request_log', 'user_id', $this->integer(11)->defaultValue(null)->after('call_center_request_id'));
        $this->addColumn('call_center_request_log', 'route', $this->string(255)->defaultValue(null)->after('user_id'));

        $this->addForeignKey(null, 'order_log', 'user_id', 'user', 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, 'delivery_request_log', 'user_id', 'user', 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, 'call_center_request_log', 'user_id', 'user', 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey($this->getFkName('call_center_request_log', 'user_id'), 'call_center_request_log');
        $this->dropForeignKey($this->getFkName('delivery_request_log', 'user_id'), 'delivery_request_log');
        $this->dropForeignKey($this->getFkName('order_log', 'user_id'), 'order_log');

        $this->dropColumn('call_center_request_log', 'route');
        $this->dropColumn('call_center_request_log', 'user_id');
        $this->dropColumn('delivery_request_log', 'route');
        $this->dropColumn('delivery_request_log', 'user_id');
        $this->dropColumn('order_log', 'route');
        $this->dropColumn('order_log', 'user_id');
    }
}
