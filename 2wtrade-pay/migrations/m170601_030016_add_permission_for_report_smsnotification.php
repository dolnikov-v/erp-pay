<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170601_030016_add_permission_for_report_smsnotification
 */
class m170601_030016_add_permission_for_report_smsnotification extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}', array(
            'name' => 'report.smsnotification.index',
            'type' => '2',
            'description' => 'report.smsnotification.index'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'business_analyst',
            'child' => 'report.smsnotification.index'
        ));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->authManager->itemChildTable, ['child' => 'report.smsnotification.index', 'parent' => 'business_analyst']);
        $this->delete('{{%auth_item}}', ['name' => 'report.smsnotification.index']);
    }
}
