<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryApiClass;

/**
 * Class m170406_091217_add_bluedart_fulfillment_api
 */
class m170406_091217_add_bluedart_fulfillment_api extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $class = new DeliveryApiClass();
        $class->name = 'BluedartFulfillment';
        $class->class_path = '/bluedart-fulfillment-api/src/BluedartFulfillmentApi.php';
        $class->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        DeliveryApiClass::find()
            ->where([
                'name' => 'BluedartFulfillment',
                'class_path' => '/bluedart-fulfillment-api/src/BluedartFulfillmentApi.php'
            ])
            ->one()
            ->delete();
    }
}
