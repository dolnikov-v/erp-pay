<?php
use app\components\CustomMigration as Migration;
use app\modules\salary\models\Person;
use yii\db\Query;
use app\modules\callcenter\models\CallCenterUser;
use app\modules\salary\models\Designation;
use app\models\Currency;
use app\models\Country;

/**
 * Class m170616_081606_call_center_person
 */
class m170616_081606_call_center_person extends Migration
{
    const ROLES = [
        'country.curator',
        'callcenter.manager',
        'callcenter.hr',
    ];

    const RULES = [
        'callcenter.person.index',
        'callcenter.person.edit',
        'callcenter.person.delete',
        'callcenter.person.activate',
        'callcenter.person.deactivate',
        'callcenter.person.deletephoto',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $CallCenterPersonSchema = Yii::$app->db->schema->getTableSchema(Person::tableName());

        if ($CallCenterPersonSchema !== null) {
            $this->dropTable(Person::tableName());
        }

        $this->createTable(Designation::tableName(), [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'active' => $this->integer(1)->defaultValue(1),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->createTable(Person::tableName(), [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'designation_id' => $this->integer(),
            'parent_id' => $this->integer(),
            'birth_date' => $this->date(),
            'start_date' => $this->date(),
            'shift_time' => $this->string(),
            'lunch_time' => $this->string(),
            'country_id' => $this->integer()->notNull(),
            'photo' => $this->string(),
            'salary_scheme' => $this->string(),
            'salary_sum' => $this->float(),
            'currency_id' => $this->integer(),
            'working_mon' => $this->integer(1)->defaultValue(1),
            'working_tue' => $this->integer(1)->defaultValue(1),
            'working_wed' => $this->integer(1)->defaultValue(1),
            'working_thu' => $this->integer(1)->defaultValue(1),
            'working_fri' => $this->integer(1)->defaultValue(1),
            'working_sat' => $this->integer(1)->defaultValue(0),
            'working_sun' => $this->integer(1)->defaultValue(0),
            'active' => $this->integer(1)->defaultValue(1),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addColumn(CallCenterUser::tableName(), 'person_id', $this->integer()->after('salary_sum'));
        $this->dropColumn(CallCenterUser::tableName(), 'salary_scheme');
        $this->dropColumn(CallCenterUser::tableName(), 'salary_sum');
        $this->dropColumn(CallCenterUser::tableName(), 'photo');
        $this->dropColumn(CallCenterUser::tableName(), 'name');

        $this->addForeignKey('fk_call_center_user_person_id', CallCenterUser::tableName(), 'person_id', Person::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, Person::tableName(), 'parent_id', Person::tableName(), 'id', self::SET_NULL, self::NO_ACTION);
        $this->addForeignKey(null, Person::tableName(), 'country_id', Country::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, Person::tableName(), 'currency_id', Currency::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, Person::tableName(), 'designation_id', Designation::tableName(), 'id', self::CASCADE, self::RESTRICT);

        $arrayForInsert = [];
        $arrayForInsert[] = ['name' => 'Административный директор'];
        $arrayForInsert[] = ['name' => 'Супервайзер'];
        $arrayForInsert[] = ['name' => 'Тимлид продаж'];
        $arrayForInsert[] = ['name' => 'Тимлид доставки'];
        $arrayForInsert[] = ['name' => 'Оператор'];
        $arrayForInsert[] = ['name' => 'HR'];
        $arrayForInsert[] = ['name' => 'IT'];

        foreach ($arrayForInsert as &$a) {
            $a['active'] = 1;
            $a['created_at'] = time();
            $a['updated_at'] = time();
        }

        Yii::$app->db->createCommand()->batchInsert(
            'call_center_designation',
            [
                'name',
                'active',
                'created_at',
                'updated_at'
            ],
            $arrayForInsert
        )->execute();


        $query = new Query();

        foreach (self::RULES as $rule) {

            $is_rule = $query->select('*')->from($this->authManager->itemTable)
                ->where(['name' => $rule, 'type' => 2])
                ->one();

            if (!$is_rule) {
                $this->insert($this->authManager->itemTable, [
                    'name' => $rule,
                    'description' => $rule,
                    'type' => 2,
                    'created_at' => time(),
                    'updated_at' => time()]);
            }

            foreach (self::ROLES as $role) {
                $is_can = $query->select('*')->from($this->authManager->itemChildTable)->where([
                    'parent' => $role,
                    'child' => $rule
                ])->one();

                if (!$is_can) {
                    $this->insert($this->authManager->itemChildTable, [
                        'parent' => $role,
                        'child' => $rule
                    ]);
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_call_center_user_person_id', CallCenterUser::tableName());
        $this->dropColumn(CallCenterUser::tableName(), 'person_id');

        $CallCenterPersonSchema = Yii::$app->db->schema->getTableSchema(Person::tableName());

        if ($CallCenterPersonSchema !== null) {
            $this->dropTable(Person::tableName());
        }

        $CallCenterPersonSchema = Yii::$app->db->schema->getTableSchema(Designation::tableName());

        if ($CallCenterPersonSchema !== null) {
            $this->dropTable(Designation::tableName());
        }


        foreach (self::RULES as $rule) {
            foreach (self::ROLES as $role) {
                $this->delete($this->authManager->itemChildTable, [
                    'parent' => $role,
                    'child' => $rule
                ]);
            }
        }

    }
}
