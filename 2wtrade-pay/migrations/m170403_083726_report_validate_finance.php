<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170403_083726_report_validate_finance
 */
class m170403_083726_report_validate_finance extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('report_validate_finance', [
            'id' => $this->primaryKey(),
            'file_name' => $this->string(200)->notNull(),
            'order_id' => $this->string(50),
            'order_id_file' => $this->string(50),
            'tracking' => $this->string(50),
            'tracking_file' => $this->string(50),
            'delivery_cost_percent' => $this->string(50),
            'delivery_cost_percent_file' => $this->string(50),
            'fulfillment_cost' => $this->string(50),
            'fulfillment_cost_file' => $this->string(50),
            'nds' => $this->string(50),
            'nds_file' => $this->string(50),
            'fulfillment_nds' => $this->string(50),
            'fulfillment_nds_file' => $this->string(50),
            'error' => $this->string(500)->defaultValue(null),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11),
        ]);

        $this->createIndex(null, 'report_validate_finance', 'file_name');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable("report_validate_finance");
    }
}
