<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170410_053402_add_permission_product_rating_report
 */
class m170410_053402_add_permission_product_rating_report extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $auth = $this->authManager;
        $role = $auth->getRole('country.curator');

        $permission = $auth->createPermission('report.productrating.index');
        $permission->description = 'Report product rating';
        $auth->add($permission);
        $auth->addChild($role, $permission);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $auth = $this->authManager;
        $permission = $auth->getPermission('report.productrating.index');
        $auth->remove($permission);
    }
}
