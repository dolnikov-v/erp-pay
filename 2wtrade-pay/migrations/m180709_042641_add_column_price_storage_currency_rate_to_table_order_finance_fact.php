<?php

use app\modules\order\models\OrderFinanceFact;
use yii\db\Migration;

/**
 * Handles adding column_price_storage_currency_rate to table `table_order_finance_fact`.
 */
class m180709_042641_add_column_price_storage_currency_rate_to_table_order_finance_fact extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $table = Yii::$app->db->schema->getTableSchema(OrderFinanceFact::tableName());
        if (!isset($table->columns['price_storage_currency_rate'])) {
            $this->addColumn(OrderFinanceFact::tableName(), 'price_storage_currency_rate', $this->double()->after('price_storage_currency_id'));
        }
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
    }
}
