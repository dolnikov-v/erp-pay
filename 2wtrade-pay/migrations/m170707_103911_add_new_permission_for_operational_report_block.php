<?php

use app\components\CustomMigration as Migration;

/**
 * Class m170707_103911_add_new_permission_for_operational_report_block
 */
class m170707_103911_add_new_permission_for_operational_report_block extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        $this->delete($this->authManager->itemChildTable, ['child' => 'report.dailyoperationalreports.index', 'parent' => 'finance.director']);
        $this->delete('{{%auth_item}}', ['name' => 'report.dailyoperationalreports.index']);

        $this->delete($this->authManager->itemChildTable, ['child' => 'report.dailyoperationalbybuyouts.index', 'parent' => 'finance.director']);
        $this->delete('{{%auth_item}}', ['name' => 'report.dailyoperationalbybuyouts.index']);

        $this->delete($this->authManager->itemChildTable, ['child' => 'report.dailyoperationalbyproducts.index', 'parent' => 'finance.director']);
        $this->delete('{{%auth_item}}', ['name' => 'report.dailyoperationalbyproducts.index']);

        $this->delete($this->authManager->itemChildTable, ['child' => 'report.dailyoperationalbyshippings.index', 'parent' => 'finance.director']);
        $this->delete('{{%auth_item}}', ['name' => 'report.dailyoperationalbyshippings.index']);


        // Доступ к новому блоку операционных отчетов
        $this->insert('{{%auth_item}}',array(
            'name'=>'reportoperational',
            'type' => '2',
            'description' => 'reportoperational',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'finance.director',
            'child' => 'reportoperational'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'business_analyst',
            'child' => 'reportoperational'
        ));

        // Доступ к блоку настроек операционных отчетов
        $this->insert('{{%auth_item}}',array(
            'name'=>'reportoperational.settings.index',
            'type' => '2',
            'description' => 'reportoperational.settings.index',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'finance.director',
            'child' => 'reportoperational.settings.index'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'business_analyst',
            'child' => 'reportoperational.settings.index'
        ));

        // Доступ к настройкам стран операционных отчетов
        $this->insert('{{%auth_item}}',array(
            'name'=>'reportoperational.settingscountries.index',
            'type' => '2',
            'description' => 'reportoperational.settingscountries.index',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'finance.director',
            'child' => 'reportoperational.settingscountries.index'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'business_analyst',
            'child' => 'reportoperational.settingscountries.index'
        ));

        // Доступ к настройкам команд операционных отчетов
        $this->insert('{{%auth_item}}',array(
            'name'=>'reportoperational.settingsteam.index',
            'type' => '2',
            'description' => 'reportoperational.settingsteam.index',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'finance.director',
            'child' => 'reportoperational.settingsteam.index'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'business_analyst',
            'child' => 'reportoperational.settingsteam.index'
        ));


        // Доступ к операционному отчету по выкупам
        $this->insert('{{%auth_item}}',array(
            'name'=>'reportoperational.buyouts.index',
            'type' => '2',
            'description' => 'reportoperational.buyouts.index',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'finance.director',
            'child' => 'reportoperational.buyouts.index'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'business_analyst',
            'child' => 'reportoperational.buyouts.index'
        ));

        // Доступ к операционному отчету по продуктам
        $this->insert('{{%auth_item}}',array(
            'name'=>'reportoperational.products.index',
            'type' => '2',
            'description' => 'reportoperational.products.index',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'finance.director',
            'child' => 'reportoperational.products.index'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'business_analyst',
            'child' => 'reportoperational.products.index'
        ));

        // Доступ к операционному отчету по доставкам
        $this->insert('{{%auth_item}}',array(
            'name'=>'reportoperational.shippings.index',
            'type' => '2',
            'description' => 'reportoperational.shippings.index',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'finance.director',
            'child' => 'reportoperational.shippings.index'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'business_analyst',
            'child' => 'reportoperational.shippings.index'
        ));

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->authManager->itemChildTable, ['child' => 'reportoperational', 'parent' => 'business_analyst']);
        $this->delete($this->authManager->itemChildTable, ['child' => 'reportoperational', 'parent' => 'finance.director']);
        $this->delete('{{%auth_item}}', ['name' => 'reportoperational']);

        $this->delete($this->authManager->itemChildTable, ['child' => 'reportoperational.settings.index', 'parent' => 'business_analyst']);
        $this->delete($this->authManager->itemChildTable, ['child' => 'reportoperational.settings.index', 'parent' => 'finance.director']);
        $this->delete('{{%auth_item}}', ['name' => 'reportoperational.settings.index']);

        $this->delete($this->authManager->itemChildTable, ['child' => 'reportoperational.settingscountries.index', 'parent' => 'business_analyst']);
        $this->delete($this->authManager->itemChildTable, ['child' => 'reportoperational.settingscountries.index', 'parent' => 'finance.director']);
        $this->delete('{{%auth_item}}', ['name' => 'reportoperational.settingscountries.index']);

        $this->delete($this->authManager->itemChildTable, ['child' => 'reportoperational.settingsteam.index', 'parent' => 'business_analyst']);
        $this->delete($this->authManager->itemChildTable, ['child' => 'reportoperational.settingsteam.index', 'parent' => 'finance.director']);
        $this->delete('{{%auth_item}}', ['name' => 'reportoperational.settingsteam.index']);

        $this->delete($this->authManager->itemChildTable, ['child' => 'reportoperational.buyouts.index', 'parent' => 'business_analyst']);
        $this->delete($this->authManager->itemChildTable, ['child' => 'reportoperational.buyouts.index', 'parent' => 'finance.director']);
        $this->delete('{{%auth_item}}', ['name' => 'reportoperational.buyouts.index.index']);

        $this->delete($this->authManager->itemChildTable, ['child' => 'reportoperational.products.index', 'parent' => 'business_analyst']);
        $this->delete($this->authManager->itemChildTable, ['child' => 'reportoperational.products.index', 'parent' => 'finance.director']);
        $this->delete('{{%auth_item}}', ['name' => 'reportoperational.products.index']);

        $this->delete($this->authManager->itemChildTable, ['child' => 'reportoperational.shippings.index', 'parent' => 'business_analyst']);
        $this->delete($this->authManager->itemChildTable, ['child' => 'reportoperational.shippings.index', 'parent' => 'finance.director']);
        $this->delete('{{%auth_item}}', ['name' => 'reportoperational.shippings.index']);


        // Доступ к старому блоку операционных отчетов
        $this->insert('{{%auth_item}}',array(
            'name'=>'report.dailyoperationalreports.index',
            'type' => '2',
            'description' => 'report.dailyoperationalreports.index',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'finance.director',
            'child' => 'report.dailyoperationalreports.index'
        ));

        // Доступ к ежедневному операционному отчету по выкупам
        $this->insert('{{%auth_item}}',array(
            'name'=>'report.dailyoperationalbybuyouts.index',
            'type' => '2',
            'description' => 'report.dailyoperationalbybuyouts.index',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'finance.director',
            'child' => 'report.dailyoperationalbybuyouts.index'
        ));

        // Доступ к ежедневному операционному отчету по продуктам
        $this->insert('{{%auth_item}}',array(
            'name'=>'report.dailyoperationalbyproducts.index',
            'type' => '2',
            'description' => 'report.dailyoperationalbyproducts.index',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'finance.director',
            'child' => 'report.dailyoperationalbyproducts.index'
        ));

        // Доступ к ежедневному операционному отчету по выкупам
        $this->insert('{{%auth_item}}',array(
            'name'=>'report.dailyoperationalbyshippings.index',
            'type' => '2',
            'description' => 'report.dailyoperationalbyshippings.index',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'finance.director',
            'child' => 'report.dailyoperationalbyshippings.index'
        ));
    }
}
