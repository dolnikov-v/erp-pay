<?php
use app\components\PermissionMigration as Migration;

/**
 * Class m180613_020136_order_finance_fact
 */
class m180613_020136_order_finance_fact extends Migration
{

    protected $permissions = ['deliveryreport.report.createpretension'];

    protected $roles = [
        'project.manager' => [
            'deliveryreport.report.createpretension'
        ],
        'deliveryreport.clerk' => [
            'deliveryreport.report.createpretension'
        ],
        'admin' => [
            'deliveryreport.report.createpretension'
        ],
        'technical.director' => [
            'deliveryreport.report.createpretension'
        ],
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('order_finance_fact', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(),
            'payment_id' => $this->integer(),
            'pretension' => $this->smallInteger(),
            'price_cod' => $this->double(),
            'price_storage' => $this->double(),
            'price_fulfilment' => $this->double(),
            'price_packing' => $this->double(),
            'price_package' => $this->double(),
            'price_delivery' => $this->double(),
            'price_redelivery' => $this->double(),
            'price_delivery_back' => $this->double(),
            'price_delivery_return' => $this->double(),
            'price_address_correction' => $this->double(),
            'price_cod_service' => $this->double(),
            'price_vat' => $this->double(),
            'price_cod_currency_id' => $this->integer(),
            'price_storage_currency_id' => $this->integer(),
            'price_fulfilment_currency_id' => $this->integer(),
            'price_packing_currency_id' => $this->integer(),
            'price_package_currency_id' => $this->integer(),
            'price_delivery_currency_id' => $this->integer(),
            'price_redelivery_currency_id' => $this->integer(),
            'price_delivery_back_currency_id' => $this->integer(),
            'price_delivery_return_currency_id' => $this->integer(),
            'price_address_correction_currency_id' => $this->integer(),
            'price_cod_service_currency_id' => $this->integer(),
            'price_vat_currency_id' => $this->integer(),
            'additional_prices' => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $this->tableOptions);

        $this->addForeignKey(null, 'order_finance_fact', 'order_id', 'order', 'id', self::CASCADE, self::CASCADE);
        $this->addForeignKey(null, 'order_finance_fact', 'payment_id', 'payment_order_delivery', 'id', self::SET_NULL, self::NO_ACTION);
        $this->createIndex(null, 'order_finance_fact', 'pretension');
        $this->createIndex(null, 'order_finance_fact', 'created_at');
        $this->createIndex(null, 'order_finance_fact', 'updated_at');

        parent::safeUp();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('order_finance_fact');

        parent::safeDown();
    }
}
