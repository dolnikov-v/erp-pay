<?php
use app\components\CustomMigration as Migration;
use app\modules\salary\models\MonthlyStatementData;

/**
 * Class m170724_101910_salary_filters_multiple
 */
class m170724_101910_salary_filters_multiple extends Migration
{

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(MonthlyStatementData::tableName(), 'office', $this->string());

        $this->delete($this->authManager->itemChildTable, [
            'parent' => 'project.manager',
            'child' => 'limit_call_center_persons_by_office'
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(MonthlyStatementData::tableName(), 'office');
    }
}
