<?php
use app\components\CustomMigration as Migration;
use yii\db\Query;
use app\modules\salary\models\Office;

/**
 * Class m170705_032916_call_center_time_sheet
 */
class m170705_032916_call_center_time_sheet extends Migration
{
    const ROLES = [
        'finance.director',
        'callcenter.hr',
        'callcenter.manager',
        'project.manager'
    ];

    const RULES = [
        'report.timesheet.index',
    ];


    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Office::tableName(), 'calc_salary_local', $this->integer(1)->defaultValue(1));
        $this->addColumn(Office::tableName(), 'calc_salary_usd', $this->integer(1)->defaultValue(0));

        $query = new Query();

        foreach (self::RULES as $rule) {

            $is_rule = $query->select('*')->from($this->authManager->itemTable)
                ->where(['name' => $rule, 'type' => 2])
                ->one();

            if (!$is_rule) {
                $this->insert($this->authManager->itemTable, [
                    'name' => $rule,
                    'description' => $rule,
                    'type' => 2,
                    'created_at' => time(),
                    'updated_at' => time()]);
            }

            foreach (self::ROLES as $role) {
                $is_can = $query->select('*')->from($this->authManager->itemChildTable)->where([
                    'parent' => $role,
                    'child' => $rule
                ])->one();

                if (!$is_can) {
                    $this->insert($this->authManager->itemChildTable, [
                        'parent' => $role,
                        'child' => $rule
                    ]);
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Office::tableName(), 'calc_salary_local');
        $this->dropColumn(Office::tableName(), 'calc_salary_usd');

        foreach (self::RULES as $rule) {
            foreach (self::ROLES as $role) {
                $this->delete($this->authManager->itemChildTable, [
                    'parent' => $role,
                    'child' => $rule
                ]);
            }
        }
    }
}
