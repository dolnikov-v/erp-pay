<?php
use app\components\CustomMigration as Migration;
use app\modules\salary\models\Office;

/**
 * Class m170704_035714_call_center_office_week_hours
 */
class m170704_035714_call_center_office_week_hours extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Office::tableName(), 'working_hours_per_week', $this->integer()->after('active'));

        $this->update(Office::tableName(), ['working_hours_per_week' => 48], ['name' => 'Колл-центр Индия']);
        $this->update(Office::tableName(), ['working_hours_per_week' => 42], ['name' => 'Колл-центр Пакистан']);
        $this->update(Office::tableName(), ['working_hours_per_week' => 40], ['name' => 'Колл-центр Тунис']);
        $this->update(Office::tableName(), ['working_hours_per_week' => 40], ['name' => 'Колл-центр Индонезия']);
        $this->update(Office::tableName(), ['working_hours_per_week' => 40], ['name' => 'Колл-центр Филиппины']);
        $this->update(Office::tableName(), ['working_hours_per_week' => 48], ['name' => 'Колл-центр Камбоджа']);
        $this->update(Office::tableName(), ['working_hours_per_week' => 40], ['name' => 'Колл-центр Китай']);
        $this->update(Office::tableName(), ['working_hours_per_week' => 48], ['name' => 'Колл-центр Колумбия']);
        $this->update(Office::tableName(), ['working_hours_per_week' => 44], ['name' => 'Колл-центр Гватемала']);
        $this->update(Office::tableName(), ['working_hours_per_week' => 48], ['name' => 'Колл-центр Тайланд']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Office::tableName(), 'working_hours_per_week');
    }
}
