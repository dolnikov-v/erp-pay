<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170719_133455_add_permission_new_export_block_operational_report
 */
class m170719_133455_add_permission_new_export_block_operational_report extends Migration
{

    protected $roles  = [
        'finance.director',
        'business_analyst',
    ];

    protected $permissions = [
        'reportoperational.exporttoexcel.index',
        'reportoperational.buyoutstoexcel.index',
        'reportoperational.productstoexcel.index',
        'reportoperational.shippingstoexcel.index',
        'reportoperational.buyoutstoexcel.exporttoexcel',
        'reportoperational.productstoexcel.exporttoexcel',
        'reportoperational.shippingstoexcel.exporttoexcel',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        foreach ($this->permissions as $permission) {
            $this->insert('{{%auth_item}}',
                [
                    'name' => $permission,
                    'type' => '2',
                    'description' => $permission,
                    'created_at' => time(),
                    'updated_at' => time()
                ]);
            foreach ($this->roles as $role) {
                $this->insert($this->authManager->itemChildTable,
                    [
                        'parent' => $role,
                        'child' => $permission
                    ]);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach ($this->permissions as $permission) {
            $this->delete($this->authManager->itemTable, ['name' => $permission]);
        }
    }
}