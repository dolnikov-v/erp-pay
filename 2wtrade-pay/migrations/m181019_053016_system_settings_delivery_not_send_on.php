<?php
use app\components\CustomMigration as Migration;
use yii\db\Query;

/**
 * Class m181019_053016_system_settings_delivery_not_send_on
 */
class m181019_053016_system_settings_delivery_not_send_on extends Migration
{

    protected $notifications = [
        'delivery.no.send.by.debts',
        'delivery.no.send.by.process'
    ];

    protected $users = [
        'yulia_s',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        foreach ($this->users as $user) {
            $userId = (new Query())->select('id')
                ->from('user')
                ->where(['username' => $user])
                ->scalar();

            if ($userId) {
                foreach ($this->notifications as $trigger) {

                    $userNotificationSettingId = (new Query())->select('id')
                        ->from('user_notification_setting')
                        ->where([
                            'user_id' => $userId,
                            'trigger' => $trigger
                        ])
                        ->scalar();

                    $data = [
                        'user_id' => $userId,
                        'trigger' => $trigger,
                        'active' => 1,
                        'email_interval' => 'always',
                        'sms_active' => 0,
                        'telegram_active' => 1,
                        'updated_at' => time(),
                    ];

                    if (!$userNotificationSettingId) {
                        $data['created_at'] = time();
                        $this->insert('user_notification_setting', $data);
                    } else {
                        $this->update('notification', $data, ['id' => $userNotificationSettingId]);
                    }
                }
            }
        }

        $this->update('delivery', ['not_send_if_has_debts' => 1, 'not_send_if_in_process' => 1]);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach ($this->users as $user) {
            $userId = (new Query())->select('id')
                ->from('user')
                ->where(['username' => $user])
                ->scalar();

            if ($userId) {
                foreach ($this->notifications as $trigger) {
                    $this->delete('user_notification_setting', [
                        'user_id' => $userId,
                        'trigger' => $trigger,
                    ]);
                }
            }
        }

        $this->update('delivery', ['not_send_if_has_debts' => 0, 'not_send_if_in_process' => 0]);
    }
}
