<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160819_083234_edit_report_advertising
 */
class m160819_083234_edit_report_advertising extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('report_advertising', 'foreign_id', $this->string(11));
        $this->alterColumn('report_advertising', 'created_at', $this->string(11));
        $this->alterColumn('report_advertising', 'price', $this->string(50));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('report_advertising', 'foreign_id', $this->integer(11));
        $this->alterColumn('report_advertising', 'created_at', $this->integer(11));
        $this->alterColumn('report_advertising', 'price', $this->double());
    }
}
