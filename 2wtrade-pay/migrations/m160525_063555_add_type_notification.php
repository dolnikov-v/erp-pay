<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160525_063555_add_type_notification
 */
class m160525_063555_add_type_notification extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('notification', 'type', "ENUM('primary', 'success', 'info', 'warning', 'danger') DEFAULT 'primary' AFTER `trigger`");
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('notification', 'type');
    }
}
