<?php
use app\components\CustomMigration as Migration;
use yii\db\Query;

/**
 * Class m170817_103639_call_center_leader
 */
class m170817_103639_call_center_leader extends Migration
{
    const ROLE = 'callcenter.leader';
    const ROLE_NAME = 'Руководитель КЦ';

    const RULES = [
        'home.index.index',
        'limit_call_center_persons_by_office',
        'callcenter.person.activate',
        'callcenter.person.activatepersons',
        'callcenter.person.changeteamleader',
        'callcenter.person.deactivate',
        'callcenter.person.deactivatepersons',
        'callcenter.person.delete',
        'callcenter.person.deletephoto',
        'callcenter.person.edit',
        'callcenter.person.export',
        'callcenter.person.getparents',
        'callcenter.person.getpersons',
        'callcenter.person.index',
        'callcenter.person.link',
        'callcenter.person.setbonus',
        'callcenter.person.setpenalty',
        'callcenter.person.setrole',
        'callcenter.person.setwork',
        'callcenter.person.unlink',
        'callcenter.user.activate',
        'callcenter.user.activateusers',
        'callcenter.user.changeteamleader',
        'callcenter.user.deactivate',
        'callcenter.user.deactivateusers',
        'callcenter.user.deletephoto',
        'callcenter.user.edit',
        'callcenter.user.getparents',
        'callcenter.user.getusers',
        'callcenter.user.index',
        'callcenter.user.setrole',
        'callcenter.user.view',
        'callcenter.designation.activate',
        'callcenter.designation.deactivate',
        'callcenter.designation.edit',
        'callcenter.designation.index',
        'callcenter.bonus.delete',
        'callcenter.bonus.edit',
        'callcenter.bonus.gettypes',
        'callcenter.bonus.index',
        'callcenter.bonustype.activate',
        'callcenter.bonustype.deactivate',
        'callcenter.bonustype.delete',
        'callcenter.bonustype.edit',
        'callcenter.bonustype.index',
        'callcenter.penalty.delete',
        'callcenter.penalty.edit',
        'callcenter.penalty.index',
        'callcenter.penaltytype.activate',
        'callcenter.penaltytype.deactivate',
        'callcenter.penaltytype.delete',
        'callcenter.penaltytype.edit',
        'callcenter.penaltytype.index',
        'callcenter.holiday.index',
        'report.salary.getcallcenter',
        'report.salary.getteamlead',
        'report.salary.index',
        'report.team.getcallcenter',
        'report.team.getteamlead',
        'report.team.index',
        'report.timesheet.index',
    ];


    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $query = new Query();

        $is_role = $query->select('*')->from($this->authManager->itemTable)
            ->where(['name' => self::ROLE, 'type' => 1])
            ->one();

        if (!$is_role) {
            $this->insert($this->authManager->itemTable, [
                'name' => self::ROLE,
                'type' => 1,
                'description' => self::ROLE_NAME,
                'created_at' => time(),
                'updated_at' => time()]);
        }

        foreach (self::RULES as $rule) {

            $is_rule = $query->select('*')->from($this->authManager->itemTable)
                ->where(['name' => $rule, 'type' => 2])
                ->one();

            if (!$is_rule) {
                $this->insert($this->authManager->itemTable, [
                    'name' => $rule,
                    'description' => $rule,
                    'type' => 2,
                    'created_at' => time(),
                    'updated_at' => time()]);
            }


            $is_can = $query->select('*')->from($this->authManager->itemChildTable)->where([
                'parent' => self::ROLE,
                'child' => $rule
            ])->one();

            if (!$is_can) {
                $this->insert($this->authManager->itemChildTable, [
                    'parent' => self::ROLE,
                    'child' => $rule
                ]);
            }

        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach (self::RULES as $rule) {
            $this->delete($this->authManager->itemChildTable, [
                'parent' => self::ROLE,
                'child' => $rule
            ]);
        }

        $this->delete($this->authManager->itemTable, [
            'name' => self::ROLE,
            'type' => 1
        ]);
    }
}
