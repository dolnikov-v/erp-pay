<?php
use app\components\CustomMigration as Migration;

use app\modules\salary\models\WorkingShift;
/**
 * Class m170728_095209_call_center_working_shift_default
 */
class m170728_095209_call_center_working_shift_default extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(WorkingShift::tableName(), 'default', $this->boolean()->defaultValue(0)->after('working_sun'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(WorkingShift::tableName(), 'default');
    }
}
