<?php
use app\components\CustomMigration as Migration;

/**
 * Class m180131_064628_invoice_round_precision
 */
class m180131_064628_invoice_round_precision extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('invoice', 'round_precision', $this->integer(2)->after('user_id'));
        $this->update('invoice', ['round_precision' => 2]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('invoice', 'round_precision');
    }
}
