<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryApiClass;
use app\modules\delivery\models\Delivery;
use app\models\Country;

/**
 * Class m170413_065612_add_delivery_JNE_for_Indonesia
 */
class m170413_065612_add_delivery_JNE_for_Indonesia extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        if (DeliveryApiClass::find()->where(
            ['name' => 'JneApi', 'class_path' => '/jne-api/src/jneApi.php']
        )->exists()
        ) {
            $apiClass = DeliveryApiClass::findOne(
                ['name' => 'JneApi', 'class_path' => '/jne-api/src/jneApi.php']
            );
        } else {
            $apiClass = new DeliveryApiClass();
        }
        $apiClass->name = 'JneApi';
        $apiClass->class_path = '/jne-api/src/jneApi.php';
        $apiClass->active = '1';
        $apiClass->created_at = time();
        $apiClass->updated_at = time();
        $apiClass->save();

        if (Country::find()->where(
            [
                'name' => 'Индонезия 1',
                'char_code' => 'ID'
            ]
        )->exists()
        ) {
            $country = Country::find()->where(
                [
                    'name' => 'Индонезия 1',
                    'char_code' => 'ID'
                ]
            )->select('id')->one();
        } else {
            //нет страны
            return false;
        }

        if (Delivery::find()->where(
            [
                'name' => 'Jne',
                'char_code' => 'JNE'
            ]
        )->exists()
        ) {
            $delivery = Delivery::findOne(
                [
                    'name' => 'Jne',
                    'char_code' => 'JNE'
                ]
            );
        } else {
            $delivery = new Delivery();
        }
        $delivery->country_id = $country->id;
        $delivery->name = 'Jne';
        $delivery->char_code = 'JNE';
        $delivery->api_class_id = $apiClass->id;
        $delivery->active = '1';
        $delivery->auto_sending = '0';
        $delivery->save(false);
        return true;
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        if (Country::find()->where(
            [
                'name' => 'Индонезия 1',
                'char_code' => 'ID'
            ]
        )->exists()
        ) {
            $country = Country::find()->where(
                [
                    'name' => 'Индонезия 1',
                    'char_code' => 'ID'
                ]
            )->select('id')->one();
        } else {
            //нет страны
            return false;
        }

        DeliveryApiClass::find()
            ->where([
                'name' => 'jneApi',
                'class_path' => '/jne-api/src/jneApi.php'
            ])
            ->one()
            ->delete();

        Delivery::findOne(
            [
                'name' => 'Jne',
                'country_id' => $country->id
            ]
        )->delete();
        return true;
    }
}
