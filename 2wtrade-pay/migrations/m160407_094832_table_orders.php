<?php

use app\components\CustomMigration as Migration;

class m160407_094832_table_orders extends Migration
{
    public function safeUp()
    {
        $this->createTable('landing', [
            'id' => $this->primaryKey(),
            'url' => $this->string(500)->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $this->tableOptions);

        // order.site_id to order.landing_id
        $this->dropForeignKey('fk_order_site_id', 'order');
        $this->dropColumn('order', 'site_id');
        $this->addColumn('order', 'landing_id', 'INTEGER(11) DEFAULT NULL AFTER `country_id`');
        $this->addForeignKey('fk_order_landing_id', 'order', 'landing_id', 'landing', 'id', self::NO_ACTION, self::NO_ACTION);

        // product_site to product_landing
        $this->renameTable('product_site', 'product_landing');
        $this->dropColumn('product_landing', 'url');
        $this->addColumn('product_landing', 'landing_id', 'INTEGER(11) DEFAULT NULL AFTER `product_id`');
        $this->addForeignKey('fk_product_landing_landing_id', 'product_landing', 'landing_id', 'landing', 'id', self::NO_ACTION, self::NO_ACTION);

        $this->renameColumn('order', 'total', 'price_total');
    }

    public function safeDown()
    {
        $this->renameColumn('order', 'price_total', 'total');

        // product_landing to product_site
        $this->dropForeignKey('fk_product_landing_landing_id', 'product_landing');
        $this->dropColumn('product_landing', 'landing_id');
        $this->addColumn('product_landing', 'url', 'VARCHAR(100) NOT NULL');
        $this->renameTable('product_landing', 'product_site');

        // order.landing_id to order.site_id
        $this->dropForeignKey('fk_order_landing_id', 'order');
        $this->dropColumn('order', 'landing_id');
        $this->addColumn('order', 'site_id', 'INTEGER(11) DEFAULT NULL AFTER `country_id`');
        $this->addForeignKey('fk_order_site_id', 'order', 'site_id', 'product_site', 'id', self::NO_ACTION, self::NO_ACTION);

        $this->dropTable('landing');
    }
}
