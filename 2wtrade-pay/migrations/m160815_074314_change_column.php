<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160815_074314_change_column
 */
class m160815_074314_change_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('order', 'foreign_id', $this->integer()->defaultValue(null));
        $this->execute('UPDATE `order` SET `foreign_id` = NULL WHERE `foreign_id` = 0');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('order', 'foreign_id', $this->integer()->notNull());
    }
}
