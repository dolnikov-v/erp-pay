<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryApiClass;

/**
 * Class m170202_041828_add_swift_api
 */
class m170202_041828_add_swift_api extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $apiClass = $apiClass = DeliveryApiClass::findOne(
            ['name' => 'swiftApi', 'class_path' => '/swift-api/src/SwiftApi.php']
        );
        if (is_null($apiClass)) {
            $apiClass = new DeliveryApiClass();
        }
        $apiClass->name = 'swiftApi';
        $apiClass->class_path = '/swift-api/src/SwiftApi.php';
        $apiClass->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $apiClass = $apiClass = DeliveryApiClass::findOne(
            ['name' => 'swiftApi', 'class_path' => '/swift-api/src/SwiftApi.php']
        );

        if (!is_null($apiClass)) {
            $apiClass->delete();
        }
    }
}
