<?php
use app\components\CustomMigration as Migration;

class m161025_190102_add_delivery_request_done_at extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        $this->addColumn('delivery_request', 'done_at', $this->integer(11)->defaultValue(null)->after("paid_at"));

        Yii::$app->db->createCommand("UPDATE delivery_request SET done_at = updated_at WHERE status='done'")
            ->execute();


    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('delivery_request', 'done_at');
    }
}
