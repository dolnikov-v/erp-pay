<?php
use app\components\PermissionMigration as Migration;

/**
 * Class m180518_132619_callcentercheckrequest
 */
class m180518_132619_callcentercheckrequest extends Migration
{

    protected $permissions = ['report.callcentercheckrequest.index'];

    protected $roles = [
        'project.manager' => ['report.callcentercheckrequest.index']
    ];
}
