<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryApiClass;

/**
 * Class m160929_062750_correct_path_to_class_blueex_api
 */
class m160929_062750_correct_path_to_class_blueex_api extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $class = DeliveryApiClass::find()
            ->where([
                        'name' => 'BlueexApi',
                        'class_path' => '/blueEx-api/src/blueExApi.php'
                    ])
            ->one();
        if($class instanceof DeliveryApiClass){
            $class->class_path = '/blueex-api/src/blueExApi.php';
            $class->save(false, ['class_path']);
        }

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $class = DeliveryApiClass::find()
            ->where([
                        'name' => 'BlueexApi',
                        'class_path' => '/blueex-api/src/blueExApi.php'
                    ])
            ->one();
        if($class instanceof DeliveryApiClass){
            $class->class_path = '/blueEx-api/src/blueExApi.php';
            $class->save(false, ['class_path']);
        }
    }
}
