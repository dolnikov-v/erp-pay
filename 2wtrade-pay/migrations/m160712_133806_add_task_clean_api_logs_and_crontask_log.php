<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m160712_133806_add_task_clean_api_logs_and_crontask_log
 */
class m160712_133806_add_task_clean_api_logs_and_crontask_log extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = new CrontabTask();
        $crontabTask->name = 'clean_api_lead_log';
        $crontabTask->description = 'Очистка логов по запросам API.';
        $crontabTask->save();

        $crontabTask = new CrontabTask();
        $crontabTask->name = 'clean_cron_task_log';
        $crontabTask->description = 'Очистка логов по Crontab.';
        $crontabTask->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $cleanApi = CrontabTask::find()
            ->byName('clean_api_lead_log')
            ->one();

        $cronTaskLog = CrontabTask::find()
            ->byName('clean_cron_task_log')
            ->one();

        if ($cleanApi) {
            $cleanApi->delete();
        }

        if ($cronTaskLog) {
            $cronTaskLog->delete();
        }
    }
}
