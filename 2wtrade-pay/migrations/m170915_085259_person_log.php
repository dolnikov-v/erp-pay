<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170915_085259_person_log
 */
class m170915_085259_person_log extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('salary_person_log', [
            'id' => $this->primaryKey(),
            'person_id' => $this->integer()->notNull(),
            'user_id' => $this->integer(),
            'group_id' => $this->string(255),
            'field' => $this->string(255),
            'old' => $this->string(255),
            'new' => $this->string(255),
            'created_at' => $this->integer(),
        ]);
        $this->createIndex(null, 'salary_person_log', 'field');
        $this->addForeignKey(null, 'salary_person_log', 'person_id', 'salary_person', 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, 'salary_person_log', 'user_id', 'user', 'id', self::CASCADE, self::RESTRICT);

        $this->insert('{{%auth_item}}',array(
            'name'=>'salary.person.logs',
            'type' => '2',
            'description' => 'salary.person.logs'
        ));

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('salary_person_log');
        $this->delete('{{%auth_item}}', ['name' => 'salary.person.logs']);
    }
}
