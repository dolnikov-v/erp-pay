<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160711_102217_call_center_request_log
 */
class m160711_102217_call_center_request_log extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('call_center_request_log', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->notNull(),
            'group_id' => $this->integer()->notNull(),
            'field' => $this->string()->notNull(),
            'old' => $this->string()->notNull(),
            'new' => $this->string()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(null, 'call_center_request_log', 'order_id', 'order', 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('call_center_request_log');
    }
}
