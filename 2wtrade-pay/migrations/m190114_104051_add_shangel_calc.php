<?php
use app\components\CustomMigration as Migration;

/**
 * Class m190114_104051_add_shangel_calc
 */
class m190114_104051_add_shangel_calc extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('delivery_charges_calculator', [
            'name' => 'ShangelPE',
            'class_path' => 'app\modules\delivery\components\charges\shangel\ShangelPE',
            'active' => 1,
            'created_at' => time(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete('delivery_charges_calculator', ['name' => 'ShangelPE']);
    }
}
