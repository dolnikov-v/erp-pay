<?php

use app\models\Country;
use app\models\Currency;
use app\modules\order\models\Lead;
use app\modules\order\models\Order;
use yii\db\Migration;
use yii\helpers\Console;

/**
 * Handles adding price_currency to table `lead_and_order_tables`.
 */
class m180813_075755_add_price_currency_to_order extends Migration
{
    const BATCH_SIZE = 100;
    const COUNTRY_COMBODIA = 'KH';
    const CURRENCY_USD = 'USD';
    const PRICE_LIMIT = 1000;

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $usdCurrency = Currency::findOne(['char_code' => self::CURRENCY_USD]);

        $countries = Country::find()->where(['is not', 'currency_id', null])->all();

        foreach ($countries as $country) {
            if ($country->char_code == self::COUNTRY_COMBODIA) {
                //Обновляем на USD те у которых меньше 1000 прайс
                var_dump(Order::updateAll(
                    ['price_currency' => $usdCurrency->id],
                    ['AND', ['=', 'country_id', $country->id], ['<', 'price_total', self::PRICE_LIMIT]]
                ));

                var_dump(Order::updateAll(
                    ['price_currency' => $country->currency_id],
                    ['AND', ['=', 'country_id', $country->id], ['>=', 'price_total', self::PRICE_LIMIT]]
                ));
            } else {
                var_dump(Order::updateAll(
                    ['price_currency' => $country->currency_id],
                    ['AND', ['=', 'country_id', $country->id], ['is', 'price_currency', null]]
                ));
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        Order::updateAll(['price_currency' => null], ['is not', 'price_currency', null]);
    }
}
