<?php

use app\components\CustomMigration as Migration;
use app\modules\delivery\models\Delivery;
use app\modules\order\models\Order;

/**
 * Handles the creation for table `table_order_finance_balance`.
 */
class m180807_093632_create_table_order_finance_balance extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('order_finance_balance', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->notNull(),
            'delivery_id' => $this->integer()->notNull(),
            'price_cod' => $this->float()->defaultValue(0),
            'price_cod_currency_id' => $this->integer()->defaultValue(null),
            'price_storage' => $this->float()->defaultValue(0),
            'price_storage_currency_id' => $this->integer()->defaultValue(null),
            'price_fulfilment' => $this->float()->defaultValue(0),
            'price_fulfilment_currency_id' => $this->integer()->defaultValue(null),
            'price_packing' => $this->float()->defaultValue(0),
            'price_packing_currency_id' => $this->integer()->defaultValue(null),
            'price_package' => $this->float()->defaultValue(0),
            'price_package_currency_id' => $this->integer()->defaultValue(null),
            'price_delivery' => $this->float()->defaultValue(0),
            'price_delivery_currency_id' => $this->integer()->defaultValue(null),
            'price_redelivery' => $this->float()->defaultValue(0),
            'price_redelivery_currency_id' => $this->integer()->defaultValue(null),
            'price_delivery_back' => $this->float()->defaultValue(0),
            'price_delivery_back_currency_id' => $this->integer()->defaultValue(null),
            'price_delivery_return' => $this->float()->defaultValue(0),
            'price_delivery_return_currency_id' => $this->integer()->defaultValue(null),
            'price_address_correction' => $this->float()->defaultValue(0),
            'price_address_correction_currency_id' => $this->integer()->defaultValue(null),
            'price_cod_service' => $this->float()->defaultValue(0),
            'price_cod_service_currency_id' => $this->integer()->defaultValue(null),
            'price_vat' => $this->float()->defaultValue(0),
            'price_vat_currency_id' => $this->integer()->defaultValue(null),
            'last_error' => $this->string()->defaultValue(null),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);

        $this->addForeignKey(null, 'order_finance_balance', 'order_id', Order::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, 'order_finance_balance', 'delivery_id', Delivery::tableName(), 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('order_finance_balance');
    }
}
