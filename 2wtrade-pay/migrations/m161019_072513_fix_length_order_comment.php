<?php
use app\components\CustomMigration as Migration;

/**
 * Class m161019_072513_fix_length_order_comment
 */
class m161019_072513_fix_length_order_comment extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('order', 'comment', $this->string(1000)->defaultValue(null));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('order', 'comment', $this->string(200)->defaultValue(null));
    }
}
