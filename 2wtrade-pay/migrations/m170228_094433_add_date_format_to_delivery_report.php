<?php

use app\components\CustomMigration as Migration;
use app\modules\deliveryreport\models\DeliveryReport;

/**
 * Class m170228_094433_add_date_format_to_delivery_report
 */
class m170228_094433_add_date_format_to_delivery_report extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(DeliveryReport::tableName(), 'date_format', $this->string(20));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(DeliveryReport::tableName(), 'date_format');
    }
}
