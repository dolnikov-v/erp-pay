<?php
use app\components\PermissionMigration as Migration;

/**
 * Class m181214_114548_add_cron_for_calculate_company_standards
 */
class m181214_114548_add_cron_for_calculate_company_standards extends Migration
{
    protected $permissions = [
        'report.companystandards.index',
    ];

    protected $roleList = [
        'finance.manager',
        'finance.director',
        'admin',
        'project.manager',
        'technical.director',
        'support.manager'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('crontab_task', [
            'name' => 'report_calculate_company_standards',
            'description' => 'Отчёт. Генерация отчета "Стандарты работы Компании".',
            'active' => 1
        ]);
        $this->createTable('report_company_standards', [
            'country_id' => $this->integer()->notNull(),
            'month' => $this->string()->notNull(),
            'admission_lead' => $this->float(2),
            'first_sms' => $this->float(2),
            'send_to_delivery' => $this->float(2),
            'first_call' => $this->float(2),
            'try_call_count' => $this->float(2),
            'try_call_days' => $this->float(2),
            'approve_percent' => $this->float(2),
            'avg_approve_check' => $this->float(2),
            'costs' => $this->float(2),
            'income' => $this->float(2),
            'lead_cost' => $this->float(2),
            'call_center_costs' => $this->float(2),
            'product_costs' => $this->float(2),
            'error_in_address' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_at' => $this->integer(),
        ]);
        $this->addPrimaryKey(null, 'report_company_standards', ['country_id', 'month']);
        $this->addForeignKey(null, 'report_company_standards', 'country_id', 'country', 'id');

        $this->createTable('report_company_standards_by_delivery', [
            'country_id' => $this->integer()->notNull(),
            'month' => $this->string()->notNull(),
            'delivery_id' => $this->integer()->notNull(),
            'accepted_time' => $this->float(2),
            'payment_time' => $this->float(2),
            'updated_at' => $this->integer(),
            'created_at' => $this->integer(),
        ]);
        $this->addPrimaryKey('pk_report_company_standards_by_delivery', 'report_company_standards_by_delivery', ['country_id', 'month', 'delivery_id']);
        $this->addForeignKey('fk_report_company_standards_by_delivery_country', 'report_company_standards_by_delivery', 'country_id', 'country', 'id');
        $this->addForeignKey('fk_report_company_standards_by_delivery_delivery', 'report_company_standards_by_delivery', 'delivery_id', 'delivery', 'id');

        parent::safeUp();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('report_company_standards_by_delivery');
        $this->dropTable('report_company_standards');
        $this->delete('crontab_task', ['name' => 'report_calculate_company_standards']);

        parent::safeDown();
    }
}
