<?php

use app\components\CustomMigration as Migration;
use app\modules\callcenter\models\CallCenterRequest;
/**
 * Handles adding column `last_foreign_operator` to table `call_center_request`.
 */
class m170119_034707_add_column_last_foreign_operator_to_call_center_request extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(CallCenterRequest::tableName(), 'last_foreign_operator', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(CallCenterRequest::tableName(), 'last_foreign_operator');
    }
}
