<?php
use app\components\CustomMigration as Migration;

/**
 * Class m181201_145657_add_task_from_profit_stat_to_skype
 */
class m181201_145657_add_task_from_profit_stat_to_skype extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('crontab_task', [
            'name' => 'profit_stat_to_skype',
            'description' => 'Skype. Сумарная статистика по прибыли в странах.',
            'active' => 1
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete('crontab_task', ['name' => 'profit_stat_to_skype']);
    }
}
