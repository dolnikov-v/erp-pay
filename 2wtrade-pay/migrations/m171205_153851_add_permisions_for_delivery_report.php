<?php
use app\components\PermissionMigration as Migration;
use yii\db\Query;

/**
 * Class m171205_153851_add_permisions_for_delivery_report
 */
class m171205_153851_add_permisions_for_delivery_report extends Migration
{
    protected $permissions = [
        'deliveryreport.report.getrateforcurrencydate'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $query = new Query();
        $permissions = $query->from($this->authManager->itemChildTable)->where([
            'child' => 'deliveryreport.report.view'
        ])->all();
        foreach ($permissions as $permission) {
            $this->roles[$permission['parent']] = ['deliveryreport.report.getrateforcurrencydate'];
        }
        parent::safeUp();
    }
}
