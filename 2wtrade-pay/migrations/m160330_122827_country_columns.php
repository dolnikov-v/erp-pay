<?php

use app\components\CustomMigration as Migration;
use app\models\Country;

class m160330_122827_country_columns extends Migration
{
    public function up()
    {
        $this->alterColumn(Country::tableName(), 'char_code', $this->string(3)->notNull());
    }

    public function down()
    {
        $this->alterColumn(Country::tableName(), 'char_code', $this->string(10)->defaultValue(null));
    }
}
