<?php
use app\components\CustomMigration as Migration;

/**
 * Class m171121_131103_add_calculators
 */
class m171121_131103_add_calculators extends Migration
{
    protected static $calculators = [
        'Tunisia Express' => 'app\modules\delivery\components\charges\tunisiaexpress\TunisiaExpress',
        'Express Business Peru' => 'app\modules\delivery\components\charges\expressbusinessperu\ExpressBusinessPeru',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        foreach (self::$calculators as $name => $route) {
            $this->insert('delivery_charges_calculator', [
                'name' => $name,
                'class_path' => $route,
                'active' => 1,
                'created_at' => time(),
            ]);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach (self::$calculators as $name => $route) {
            $this->delete('delivery_charges_calculator', ['name' => $name]);
        }
    }
}
