<?php
use app\components\PermissionMigration as Migration;

/**
 * Class m181019_114228_salary_designation_view
 */
class m181019_114228_salary_designation_view extends Migration
{
    protected $permissions = ['salary.designation.view'];

    protected $roles = [
        'admin' => ['salary.designation.view'],
        'callcenter.hr' => ['salary.designation.view'],
        'controller.analyst' => ['salary.designation.view'],
        'finance.director' => ['salary.designation.view'],
        'partners.manager' => ['salary.designation.view'],
        'project.manager' => ['salary.designation.view'],
        'salaryproject.clerk' => ['salary.designation.view'],
        'support.manager' => ['salary.designation.view'],
        'technical.director' => ['salary.designation.view'],
        'country.curator' => ['salary.designation.view'],
        'finance.manager' => ['salary.designation.view'],
    ];
}
