<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m180508_044400_trash_holds
 */
class m180508_044400_trash_holds extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = new CrontabTask();
        $crontabTask->name = CrontabTask::TASK_SOURCE_TRASH_HOLDS;
        $crontabTask->description = 'Трешить "протухшие" холды для Адкомбо';
        $crontabTask->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = CrontabTask::find()
            ->byName(CrontabTask::TASK_SOURCE_TRASH_HOLDS)
            ->one();

        if ($crontabTask) {
            $crontabTask->delete();
        }
    }
}
