<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m160909_134637_add_table_media
 */
class m160909_134637_add_table_media extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('media', [
            'id' => $this->primaryKey(),
            'filename' => $this->string(255)->defaultValue(null),
            'type' => $this->string(255)->notNull(),
            'link_id' => $this->integer()->defaultValue(null),
            'link_type' => $this->string(255)->defaultValue(null),
            'crop_file' => $this->string(255)->defaultValue(null),
            'crop_data' => $this->text()->defaultValue(null),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->createIndex(null, 'media', 'filename');
        $this->createIndex(null, 'media', 'type');
        $this->createIndex(null, 'media', 'link_type');

        $this->addColumn('user', 'photo_id', $this->integer(11)->defaultValue(null)->after('email'));
        $this->addForeignKey(null, 'user', 'photo_id', 'media', 'id', self::SET_NULL, self::RESTRICT);

        $crontabTask = new CrontabTask();
        $crontabTask->name = 'clean_media';
        $crontabTask->description = 'Очистка непривязанных файлов Media';
        $crontabTask->save(false);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = CrontabTask::find()->where(['name' => 'clean_media'])->one();

        if ($crontabTask) {
            $crontabTask->delete();
        }

        $this->dropForeignKey($this->getFkName('user', 'photo_id'), 'user');
        $this->dropColumn('user', 'photo_id');
        $this->dropTable('media');
    }
}
