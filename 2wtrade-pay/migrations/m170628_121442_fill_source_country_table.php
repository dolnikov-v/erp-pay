<?php
use app\components\CustomMigration as Migration;
use app\models\Country;
use app\models\Partner;
use app\models\PartnerCountry;
use app\models\Source;
use app\models\SourceCountry;

/**
 * Class m170628_121442_fill_source_country_table
 */
class m170628_121442_fill_source_country_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        //Добавляем стрны по партнерам
        $partnerCountries = PartnerCountry::find()
            ->leftJoin(Partner::tableName(), Partner::tableName() . '.id=' . PartnerCountry::tableName() . '.partner_id')
            ->leftJoin(Source::tableName(), Partner::tableName() . '.source =' . Source::tableName() . '.name')
            ->select(['country_id', 'source_id' => Source::tableName() . '.id'])
            ->distinct()
            ->asArray()
            ->all();
        foreach ($partnerCountries as $partnerCountry) {
            $sourceCountry = new SourceCountry();
            $sourceCountry->country_id = $partnerCountry['country_id'];
            $sourceCountry->source_id = $partnerCountry['source_id'];
            $sourceCountry->save();
        }
        //Добавляем все страны в 2wstore
        $wstoreId = Source::find()->select('id')->where(['name' => '2wstore'])->scalar();
        $countries = Country::find()->all();
        foreach ($countries as $country) {
            $sourceCountry = new SourceCountry();
            $sourceCountry->country_id = $country->id;
            $sourceCountry->source_id = $wstoreId;
            $sourceCountry->save();
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->truncateTable(SourceCountry::tableName());
    }
}
