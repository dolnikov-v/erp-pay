<?php
use app\components\CustomMigration as Migration;
use app\modules\salary\models\Person;
use app\models\Language;

/**
 * Class m171108_062734_person_address
 */
class m171108_062734_person_address extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Person::tableName(), 'address', $this->string());
        $this->addColumn(Person::tableName(), 'gender', $this->string());

        $this->createTable('salary_person_language', [
            'id' => $this->primaryKey(),
            'person_id' => $this->integer(),
            'language_id' => $this->integer(),
        ], $this->tableOptions);

        $this->addForeignKey(null, 'salary_person_language', 'person_id', Person::tableName(), 'id', self::CASCADE, self::NO_ACTION);
        $this->addForeignKey(null, 'salary_person_language', 'language_id', Language::tableName(), 'id', self::CASCADE, self::NO_ACTION);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Person::tableName(), 'address');
        $this->dropColumn(Person::tableName(), 'gender');
        $this->dropTable('salary_person_language');
    }
}
