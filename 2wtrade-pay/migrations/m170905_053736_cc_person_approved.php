<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170905_053736_cc_person_approved
 */
class m170905_053736_cc_person_approved extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('call_center_person', 'approved', $this->integer(1)->defaultValue(0)->after('active'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('call_center_person', 'approved');
    }
}
