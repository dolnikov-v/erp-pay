<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170721_133536_add_permission_report_profitability_analysis
 */
class m170721_133536_add_permission_report_profitability_analysis extends Migration
{
    /**
     * @var array
     */
    protected $roles  = [
        'finance.director',
    ];

    /**
     * @var array
     */
    protected $permissions = [
        'report.profitabilityanalysis.index',
        'report.profitabilityanalysis.setparam',

    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        foreach ($this->permissions as $permission) {
            $this->insert($this->authManager->itemTable, [
                'name' => $permission,
                'type' => '2',
                'description' => $permission,
                'created_at' => time(),
                'updated_at' => time()
            ]);

            foreach ($this->roles as $role) {
                $this->insert($this->authManager->itemChildTable, [
                    'parent' => $role,
                    'child' => $permission
                ]);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach ($this->permissions as $permission) {
            $this->delete($this->authManager->itemChildTable, ['child' => $permission]);
            $this->delete($this->authManager->itemTable, ['name' => $permission]);
        }
    }
}
