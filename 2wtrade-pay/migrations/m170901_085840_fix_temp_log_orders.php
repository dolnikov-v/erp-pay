<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170901_085840_fix_temp_log_orders
 */
class m170901_085840_fix_temp_log_orders extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('temp_log_orders', 'log_answer_id', $this->integer());
        $this->addForeignKey(null, 'temp_log_orders', 'log_answer_id', 'crontab_task_log_answer', 'id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('temp_log_orders', 'log_answer_id');
    }
}
