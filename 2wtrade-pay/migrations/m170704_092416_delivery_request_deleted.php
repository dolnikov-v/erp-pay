<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170704_092416_delivery_request_deleted
 */
class m170704_092416_delivery_request_deleted extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('delivery_request_deleted', [
            'id' => $this->primaryKey(),
            'delivery_id' => $this->integer()->notNull(),
            'order_id' => $this->integer()->notNull(),
            'status' => $this->enum(['pending', 'in_progress', 'done', 'error', 'done_error'])->defaultValue('pending'),
            'tracking' => $this->string(255)->defaultValue(null),
            'foreign_info' => $this->text()->defaultValue(null),
            'last_update_info' => 'VARCHAR(2000) DEFAULT NULL',
            'last_update_info_at' => 'INTEGER(11) DEFAULT NULL',
            'response_info' => 'VARCHAR(255) DEFAULT NULL',
            'comment' => $this->string(100)->defaultValue(null),
            'api_error' => $this->string(255)->defaultValue(null),
            'api_error_type' => $this->enum(['system', 'courier'])->defaultValue('system'),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'cron_launched_at' => $this->integer()->defaultValue(null),
            'sent_at' => $this->integer()->defaultValue(null),
            'second_sent_at' => $this->integer()->defaultValue(null),
            'sent_clarification_at' => $this->integer()->defaultValue(null),
            'returned_at' => $this->integer()->defaultValue(null),
            'approved_at' => $this->integer()->defaultValue(null),
            'accepted_at' => $this->integer()->defaultValue(null),
            'paid_at' => $this->integer()->defaultValue(null),
            'done_at' => $this->integer(11)->defaultValue(null),
            'finance_tracking' => $this->string(30)->defaultValue(null),
            'partner_name' => $this->string(100)->defaultValue(null),
            'cs_send_response' => $this->text()->defaultValue(null),
            'cs_send_response_at' => $this->integer(11)->defaultValue(null),
            'unshipping_reason' => $this->string(32)->defaultValue(null),
        ]);

        $this->addCommentOnColumn('delivery_request_deleted', 'last_update_info_at', 'Time from last response Courier Service on update status delivery');
        $this->addCommentOnColumn('delivery_request_deleted', 'last_update_info', 'Last response update delivery status info from Courier Service');

        $this->addForeignKey(null, 'delivery_request_deleted', 'delivery_id', 'delivery', 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, 'delivery_request_deleted', 'order_id', 'order', 'id', self::CASCADE, self::RESTRICT);
        $this->createIndex(null, 'delivery_request_deleted', 'status');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('delivery_request_deleted');
    }
}
