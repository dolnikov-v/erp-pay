<?php
use app\components\PermissionMigration as Migration;

/**
 * Class m180201_055816_alter_chat_table
 */
class m180201_055816_create_chat_table extends Migration
{

    protected $permissions = [
        'notification.chat.index',
        'notification.chat-group.index',
        'notification.chat.edit',
        'notification.chat.activate',
        'notification.chat.deactivate',
        'notification.chat.delete',
    ];

    protected $roles = [
        'admin' => [
            'notification.chat.index',
            'notification.chat-group.index',
            'notification.chat.edit',
            'notification.chat.activate',
            'notification.chat.deactivate',
            'notification.chat.delete',
        ],
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('chat', [
            'id' => $this->primaryKey(),
            'chat_id' => $this->string(256)->notNull(),
            'messenger' => $this->string(32)->notNull(),
            'name' => $this->string(256)->notNull(),
            'active' => $this->boolean()->defaultValue(true),
        ],
            $this->tableOptions);

        $this->createTable('chat_group', [
            'id' => $this->primaryKey(),
            'name' => $this->string(256)->notNull()
        ],
            $this->tableOptions);

        /**                                 chat_link_group                                      */

        $this->createTable('chat_link_group', [
            'chat_id' => $this->integer()->notNull(),
            'group_id' => $this->integer()->notNull(),
        ], $this->tableOptions);

        $this->addPrimaryKey(null, 'chat_link_group', ['chat_id', 'group_id']);

        // add foreign key for table `chat`
        $this->addForeignKey(
            null,
            'chat_link_group',
            'chat_id',
            'chat',
            'id',
            self::CASCADE
        );

        // add foreign key for table `chat_group`
        $this->addForeignKey(
            null,
            'chat_link_group',
            'group_id',
            'chat_group',
            'id',
            self::CASCADE
        );

        /**                                 chat_country                                      */

        $this->createTable('chat_country', [
            'chat_id' => $this->integer()->notNull(),
            'country_id' => $this->integer()->notNull(),
        ],
            $this->tableOptions);

        $this->addPrimaryKey(null, 'chat_country', ['chat_id', 'country_id']);

        // add foreign key for table `chat`
        $this->addForeignKey(
            null,
            'chat_country',
            'chat_id',
            'chat',
            'id',
            self::CASCADE
        );

        // add foreign key for table `country`
        $this->addForeignKey(
            null,
            'chat_country',
            'country_id',
            'country',
            'id',
            self::CASCADE
        );
        parent::safeUp();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('chat_country');
        $this->dropTable('chat_link_group');
        $this->dropTable('chat_group');
        $this->dropTable('chat');

        parent::safeDown();
    }
}
