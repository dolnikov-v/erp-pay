<?php
use app\components\CustomMigration as Migration;

/**
 * Class m180301_061114_add_last_fin_report_update_translate
 */
class m180301_061114_add_last_fin_report_update_translate extends Migration
{
    private static $translations = [
        "Дата последнего обновления фин. отчета" => "Date of the last update of the financial report",
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        foreach(self::$translations as $ruText => $enText) {
            $this->insert('i18n_source_message', ['category' => 'common', 'message' => $ruText]);
            $id = Yii::$app->db->getLastInsertID();
            $this->insert('i18n_message', ['id' => $id, 'language' => 'en-US', 'translation' => $enText]);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach (self::$translations as $ruText => $enText) {
            $this->delete('i18n_source_message', ['category' => 'common', 'message' => $ruText]);
        }
    }
}
