<?php

use app\components\CustomMigration as Migration;

/**
 * Handles adding permission  `report.forecastleadbyweekdays.index`.
 */
class m170227_093522_fix_permission_for_forecast_lead_report extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->delete($this->authManager->itemChildTable, ['child' => 'report.forecast.lead.by.weekdays.index', 'parent' => 'country.curator']);
        $this->delete($this->authManager->itemChildTable, ['child' => 'report.forecast.lead.by.weekdays.index', 'parent' => 'callcenter.manager']);
        $this->delete($this->authManager->itemChildTable, ['child' => 'report.forecast.lead.by.weekdays.index', 'parent' => 'business_analyst']);
        $this->delete('{{%auth_item}}', ['name' => 'report.forecast.lead.by.weekdays.index']);

        $this->insert('{{%auth_item}}',array(
            'name'=>'report.forecastleadbyweekdays.index',
            'type' => '2',
            'description' => 'report.forecastleadbyweekdays.index',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'country.curator',
            'child' => 'report.forecastleadbyweekdays.index'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'callcenter.manager',
            'child' => 'report.forecastleadbyweekdays.index'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'business_analyst',
            'child' => 'report.forecastleadbyweekdays.index'
        ));

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->authManager->itemChildTable, ['child' => 'report.forecastleadbyweekdays.index', 'parent' => 'country.curator']);
        $this->delete($this->authManager->itemChildTable, ['child' => 'report.forecastleadbyweekdays.index', 'parent' => 'callcenter.manager']);
        $this->delete($this->authManager->itemChildTable, ['child' => 'report.forecastleadbyweekdays.index', 'parent' => 'business_analyst']);
        $this->delete('{{%auth_item}}', ['name' => 'report.forecastleadbyweekdays.index']);

        $this->insert('{{%auth_item}}',array(
            'name'=>'report.forecast.lead.by.weekdays.index',
            'type' => '2',
            'description' => 'report.forecast.lead.by.weekdays.index',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'country.curator',
            'child' => 'report.forecast.lead.by.weekdays.index'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'callcenter.manager',
            'child' => 'report.forecast.lead.by.weekdays.index'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'business_analyst',
            'child' => 'report.forecast.lead.by.weekdays.index'
        ));

    }
}
