<?php

use app\components\CustomMigration as Migration;

/**
 * Handles adding permission  `report.forecast.lead.by.weekdays.index`.
 */
class m170217_101517_add_permission_for_forecast_lead_report extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}',array(
            'name'=>'report.forecast.lead.by.weekdays.index',
            'type' => '2',
            'description' => 'report.forecast.lead.by.weekdays.index',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'country.curator',
            'child' => 'report.forecast.lead.by.weekdays.index'
        ));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->authManager->itemChildTable, ['child' => 'report.forecast.lead.by.weekdays.index', 'parent' => 'country.curator']);
        $this->delete('{{%auth_item}}', ['name' => 'report.forecast.lead.by.weekdays.index']);
    }
}
