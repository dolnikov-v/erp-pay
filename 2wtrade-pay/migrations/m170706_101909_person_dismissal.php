<?php
use app\components\CustomMigration as Migration;
use app\modules\salary\models\Person;

/**
 * Class m170706_101909_person_dismissal
 */
class m170706_101909_person_dismissal extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Person::tableName(), 'dismissal_date', $this->date());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Person::tableName(), 'dismissal_date');
    }
}
