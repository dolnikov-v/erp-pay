<?php
use app\components\CustomMigration as Migration;

/**
 * Class m171113_062646_call_center_request_extra
 */
class m171113_062646_call_center_request_extra extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('call_center_request_extra', [
            'call_center_request_id' => $this->integer()->notNull(),
            'calls_count' => $this->integer(),
        ], $this->tableOptions);
        $this->addPrimaryKey(null, 'call_center_request_extra', 'call_center_request_id');
        $this->addForeignKey(null, 'call_center_request_extra', 'call_center_request_id', 'call_center_request', 'id', self::CASCADE, self::NO_ACTION);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('call_center_request_extra');
    }
}
