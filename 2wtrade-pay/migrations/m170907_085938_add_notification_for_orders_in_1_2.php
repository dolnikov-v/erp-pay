<?php
use app\components\CustomMigration as Migration;
use app\models\Notification;
use app\modules\administration\models\CrontabTask;

/**
 * Class m170907_085938_add_notification_for_orders_in_1_2
 */
class m170907_085938_add_notification_for_orders_in_1_2 extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $notification = Notification::findOne(['trigger' => Notification::TRIGGER_REPORT_ORDERS_STATUS_IN_1_2_BY_COUNTRY]);

        if (!($notification instanceof Notification)) {
            $notification = new Notification([
                'description' => '{country}: в колонках 1 и 2 скопилось {count} заказов',
                'trigger' => Notification::TRIGGER_REPORT_ORDERS_STATUS_IN_1_2_BY_COUNTRY,
                'type' => Notification::TYPE_WARNING,
                'group' => Notification::GROUP_REPORT,
                'active' => Notification::ACTIVE,
            ]);
            $notification->save(false);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $notification = Notification::findOne(['trigger' => Notification::TRIGGER_REPORT_ORDERS_STATUS_IN_1_2_BY_COUNTRY]);

        if ($notification instanceof Notification) {
            $notification->delete();
        }
    }
}
