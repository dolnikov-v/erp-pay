<?php
use app\components\CustomMigration as Migration;

/**
 * Class m171208_042132_add_indices_for_voip_expense
 */
class m171208_042132_add_indices_for_voip_expense extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createIndex($this->getIdxName('voip_expense', 'date'), 'voip_expense', 'date');
        $this->createIndex($this->getIdxName('voip_expense', 'launched_at'), 'voip_expense', 'launched_at');
        $this->createIndex($this->getIdxName('voip_expense', ['country_id', 'date']), 'voip_expense', ['country_id', 'date']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropIndex($this->getIdxName('voip_expense', 'date'), 'voip_expense');
        $this->dropIndex($this->getIdxName('voip_expense', 'launched_at'), 'voip_expense');
        $this->dropIndex($this->getIdxName('voip_expense', ['country_id', 'date']), 'voip_expense');
    }
}
