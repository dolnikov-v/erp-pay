<?php
use app\components\CustomMigration as Migration;

/**
 * Class m180205_085020_report_expenses_foreign_keys
 */
class m180205_085020_report_expenses_foreign_keys extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropForeignKey($this->getFkName('report_expenses_country_item', 'country_id'), 'report_expenses_country_item');
        $this->dropForeignKey($this->getFkName('report_expenses_country_item', 'parent_id'), 'report_expenses_country_item');
        $this->addForeignKey($this->getFkName('report_expenses_country_item', 'country_id'), 'report_expenses_country_item', 'country_id', 'report_expenses_country', 'id', self::CASCADE, self::CASCADE);
        $this->addForeignKey($this->getFkName('report_expenses_country_item', 'parent_id'), 'report_expenses_country_item', 'parent_id', 'report_expenses_item', 'id', self::CASCADE, self::CASCADE);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey($this->getFkName('report_expenses_country_item', 'country_id'), 'report_expenses_country_item');
        $this->dropForeignKey($this->getFkName('report_expenses_country_item', 'parent_id'), 'report_expenses_country_item');
        $this->addForeignKey($this->getFkName('report_expenses_country_item', 'country_id'), 'report_expenses_country_item', 'country_id', 'report_expenses_country', 'id');
        $this->addForeignKey($this->getFkName('report_expenses_country_item', 'parent_id'), 'report_expenses_country_item', 'parent_id', 'report_expenses_item', 'id');
    }
}
