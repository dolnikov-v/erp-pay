<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160520_135620_user_notification
 */
class m160520_135620_user_notification extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('user_notification', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'trigger' => $this->string(255)->notNull(),
            'read'=> $this->smallInteger()->notNull()->defaultValue(0),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $this->tableOptions);
        $this->addForeignKey(
            null, 'user_notification', 'user_id', 'user', 'id', 'CASCADE'
        );
        $this->addForeignKey(
            null, 'user_notification', 'trigger', 'notification', 'trigger', 'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('user_notification');
    }
}
