<?php

use yii\db\Migration;

/**
 * Handles adding cell_user_id to table `call_center_person_import`.
 */
class m170704_045324_add_cell_user_id_to_call_center_person_import extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('call_center_person_import', 'cell_id', $this->integer());
        $this->addColumn('call_center_person_import_data', 'user_id', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('call_center_person_import', 'cell_id');
        $this->dropColumn('call_center_person_import_data', 'user_id');
    }
}
