<?php

use app\modules\deliveryreport\models\DeliveryReport;
use yii\db\Migration;

/**
 * Handles adding column_rate_costs to table `table_delivery_report`.
 */
class m180709_102701_add_column_rate_costs_to_table_delivery_report extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn(DeliveryReport::tableName(), 'rate_costs', $this->double()->after('additional_costs'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn(DeliveryReport::tableName(), 'rate_costs');
    }
}
