<?php

use app\components\CustomMigration as Migration;
use app\modules\order\models\OrderStatus;

class m000000_000022_order_status extends Migration
{
    public function safeUp()
    {
        $this->createTable(OrderStatus::tableName(), [
            'id' => $this->primaryKey(),
            'parent' => $this->string(50)->defaultValue(null),
            'children' => $this->string(50)->defaultValue(null),
            'group' => $this->integer()->defaultValue(null),
            'sort' => $this->integer()->defaultValue(0),
            'name' => $this->string()->notNull(),
            'full_name' => $this->string()->defaultValue(null),
            'comment' => $this->text()
        ], $this->tableOptions);

        foreach ($this->orderStatusFixture() as $item) {
            $status = new OrderStatus($item);
            $status->save(false);
        }
    }

    public function safeDown()
    {
        $this->dropTable(OrderStatus::tableName());
    }

    private function orderStatusFixture()
    {
        return [
            [
                'id' => 1,
                'name' => 'Adcombo (заказ создан с длинной формы)',
                'parent' => null,
                'children' => '8',
                'group' => 1,
                'sort' => 1,
            ],
            [
                'id' => 2,
                'name' => 'Adcombo (заказ создан с короткой формы)',
                'parent' => null,
                'children' => '3',
                'group' => 1,
                'sort' => 2,
            ],
            [
                'id' => 3,
                'name' => 'Колл-центр (отправлен в КЦ на обзвон)',
                'parent' => '2,30',
                'children' => '7,4,25',
                'group' => 2,
                'sort' => 4,
            ],
            [
                'id' => 4,
                'name' => 'Колл-центр (перезвон КЦ)',
                'parent' => '3,5',
                'children' => '5,6,7,26',
                'group' => 2,
                'sort' => 5,
            ],
            [
                'id' => 5,
                'name' => 'Колл-центр (недозвон КЦ)',
                'parent' => '4,6',
                'children' => '6,7',
                'group' => 2,
                'sort' => 6,
            ],
            [
                'id' => 6,
                'name' => 'Колл-центр (одобрен в КЦ)',
                'parent' => '4,5',
                'children' => '8,12,19,23',
                'group' => 2,
                'sort' => 10,
            ],
            [
                'id' => 7,
                'name' => 'Колл-центр (отклонен в КЦ)',
                'parent' => '3,5,26',
                'children' => null,
                'group' => 2,
                'sort' => 7,
            ],
            [
                'id' => 8,
                'name' => 'Логистика (принято, ожидается генерация листа)',
                'parent' => '1,6',
                'children' => '9',
                'group' => 3,
                'sort' => 13,
            ],
            [
                'id' => 9,
                'name' => 'Логистика (лист сгенерирован, ожидаются этикетки)',
                'parent' => '8,28',
                'children' => '10',
                'group' => 3,
                'sort' => 14,
            ],
            [
                'id' => 10,
                'name' => 'Логистика (этикетки созданы)',
                'parent' => '9',
                'children' => '11',
                'group' => 3,
                'sort' => 15,
            ],
            [
                'id' => 11,
                'name' => 'Логистика (этикетки наклеены)',
                'parent' => '10',
                'children' => '13',
                'group' => 3,
                'sort' => 16,
            ],
            [
                'id' => 12,
                'name' => 'Курьерка (принято)',
                'parent' => '1,6,20',
                'children' => '14,22,27',
                'group' => 4,
                'sort' => 17,
            ],
            [
                'id' => 13,
                'name' => 'Курьерка (покинуло склад)',
                'parent' => '11',
                'children' => '12',
                'group' => 4,
                'sort' => 19,
            ],
            [
                'id' => 14,
                'name' => 'Курьерка (ожидает вручения)',
                'parent' => '12',
                'children' => null,
                'group' => 4,
                'sort' => 23,
            ],
            [
                'id' => 15,
                'name' => 'Курьерка (выкуп)',
                'parent' => '22,24',
                'children' => '18',
                'group' => 4,
                'sort' => 29,
            ],
            [
                'id' => 16,
                'name' => 'Курьерка (отказ)',
                'parent' => '22,24',
                'children' => null,
                'group' => 4,
                'sort' => 24,
            ],
            [
                'id' => 17,
                'name' => 'Курьерка (вернулось отправителю)',
                'parent' => '27',
                'children' => '28',
                'group' => 4,
                'sort' => 26,
            ],
            [
                'id' => 18,
                'name' => 'Финансы (деньги получены)',
                'parent' => '15',
                'children' => null,
                'group' => 5,
                'sort' => 30,
            ],
            [
                'id' => 19,
                'name' => 'Курьерка (отклонено)',
                'parent' => '6',
                'children' => null,
                'group' => 4,
                'sort' => 18,
            ],
            [
                'id' => 20,
                'name' => 'Курьерка (покинуло наш склад когда-то)',
                'parent' => null,
                'children' => '12',
                'group' => 4,
                'sort' => 20,
            ],
            [
                'id' => 21,
                'name' => 'Курьерка (отправлено на передоставку)',
                'parent' => '22',
                'children' => '22',
                'group' => 4,
                'sort' => 27,
            ],
            [
                'id' => 22,
                'name' => 'Курьерка (покинуло их склад)',
                'parent' => '12',
                'children' => '15,16,24',
                'group' => 4,
                'sort' => 21,
            ],
            [
                'id' => 23,
                'name' => 'Логистика (отклонено)',
                'parent' => '6',
                'children' => null,
                'group' => 3,
                'sort' => 11,
            ],
            [
                'id' => 24,
                'name' => 'Курьерка (задерживается)',
                'parent' => '22',
                'children' => '15,16',
                'group' => 4,
                'sort' => 22,
            ],
            [
                'id' => 25,
                'name' => 'Колл-центр (треш)',
                'parent' => '3',
                'children' => null,
                'group' => 2,
                'sort' => 8,
            ],
            [
                'id' => 26,
                'name' => 'Колл-центр (дубль)',
                'parent' => '3,4',
                'children' => null,
                'group' => 2,
                'sort' => 9,
            ],
            [
                'id' => 27,
                'name' => 'Курьерка (отправлено на возврат)',
                'parent' => '12',
                'children' => '17',
                'group' => 4,
                'sort' => 25,
            ],
            [
                'id' => 28,
                'name' => 'Логистика (возврат принят)',
                'parent' => '17',
                'children' => '9',
                'group' => 3,
                'sort' => 12,
            ],
            [
                'id' => 29,
                'name' => 'Курьерка (потерян)',
                'parent' => '12',
                'children' => null,
                'group' => 4,
                'sort' => 28,
            ],
            [
                'id' => 30,
                'name' => 'Колл-центр (заказ создан в КЦ из входящей очереди)',
                'parent' => null,
                'children' => '3',
                'group' => 2,
                'sort' => 3,
            ],
        ];
    }
}
