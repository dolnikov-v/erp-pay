<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160527_103742_rename_notification_user_setting
 */
class m160527_103742_rename_notification_user_setting extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->renameTable('notification_user_setting', 'user_notification_setting');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->renameTable('user_notification_setting','notification_user_setting');
    }
}
