<?php
use app\components\CustomMigration as Migration;
use app\models\UserActionLog;
use app\models\User;

class m000000_000009_user_action_log extends Migration
{
    public function safeUp()
    {
        $this->createTable(UserActionLog::tableName(), [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'url' => $this->string(1024)->notNull(),
            'http_status' => $this->integer()->defaultValue(null),
            'get_data' => $this->string(1024)->notNull(),
            'post_data' => $this->string(1024)->notNull(),
            'created_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(null, UserActionLog::tableName(), 'user_id', User::tableName(), 'id', 'CASCADE', 'RESTRICT');
    }

    public function safeDown()
    {
        $this->dropTable(UserActionLog::tableName());
    }
}
