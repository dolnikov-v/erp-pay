<?php
use app\components\CustomMigration as Migration;
use app\modules\order\models\OrderLogisticList;
use yii\db\Query;

/**
 * Class m170927_105339_add_status_order_logistic_list
 */
class m170927_105339_add_status_order_logistic_list extends Migration
{
    const RULES = [
        'order.list.confirmreceived',
        'order.list.confirmsent',
    ];

    const ROLES = [
        'project.manager',
        'logist',
        'junior.logist',
        'implant',
        'country.curator',
        'controller.analyst',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(OrderLogisticList::tableName(), 'status', $this->string()->after('user_id'));

        // старые листы статус в sent
        $this->update(OrderLogisticList::tableName(), ['status' => OrderLogisticList::STATUS_SENT]);


        $this->createTable('order_logistic_list_log', [
            'id' => $this->primaryKey(),
            'list_id' => $this->integer()->notNull(),
            'user_id' => $this->integer(),
            'group_id' => $this->string(255),
            'field' => $this->string(255),
            'old' => $this->text(),
            'new' => $this->text(),
            'created_at' => $this->integer(),
        ]);
        $this->createIndex(null, 'order_logistic_list_log', 'field');
        $this->addForeignKey(null, 'order_logistic_list_log', 'list_id', 'order_logistic_list', 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, 'order_logistic_list_log', 'user_id', 'user', 'id', self::CASCADE, self::RESTRICT);

        $this->insert('{{%auth_item}}', array(
            'name' => 'order.list.logs',
            'type' => '2',
            'description' => 'order.list.logs'
        ));

        $query = new Query();

        foreach (self::RULES as $rule) {

            $is_rule = $query->select('*')->from($this->authManager->itemTable)
                ->where(['name' => $rule, 'type' => 2])
                ->one();

            if (!$is_rule) {
                $this->insert($this->authManager->itemTable, [
                    'name' => $rule,
                    'description' => $rule,
                    'type' => 2,
                    'created_at' => time(),
                    'updated_at' => time()]);
            }

            foreach (self::ROLES as $role) {

                $is_role = $query->select('*')->from($this->authManager->itemTable)
                    ->where(['name' => $role, 'type' => 1])
                    ->one();

                if (!$is_role) {
                    $this->insert($this->authManager->itemTable, [
                        'name' => $role,
                        'type' => 1,
                        'description' => $role,
                        'created_at' => time(),
                        'updated_at' => time()]);
                }

                $is_can = $query->select('*')->from($this->authManager->itemChildTable)->where([
                    'parent' => $role,
                    'child' => $rule
                ])->one();

                if (!$is_can) {
                    $this->insert($this->authManager->itemChildTable, [
                        'parent' => $role,
                        'child' => $rule
                    ]);
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(OrderLogisticList::tableName(), 'status');

        $this->dropTable('order_logistic_list_log');
        $this->delete('{{%auth_item}}', ['name' => 'order.list.logs']);

        foreach (self::RULES as $rule) {
            foreach (self::ROLES as $role) {
                $this->delete($this->authManager->itemChildTable, [
                    'parent' => $role,
                    'child' => $rule
                ]);
            }
        }
    }
}
