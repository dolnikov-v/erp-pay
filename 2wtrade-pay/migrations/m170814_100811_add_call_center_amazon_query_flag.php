<?php

use app\components\CustomMigration as Migration;
use app\modules\callcenter\models\CallCenter;

/**
 * Class m170814_100811_add_call_center_amazon_query_flag
 */
class m170814_100811_add_call_center_amazon_query_flag extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(CallCenter::tableName(), 'is_amazon_query', $this->boolean()->defaultValue(0));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(CallCenter::tableName(), 'is_amazon_query');
    }
}
