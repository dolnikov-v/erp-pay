<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryApiClass;
use app\models\Country;

/**
 * Class m170327_075954_asp_express_ind2
 */
class m170327_075954_asp_express_ind2 extends Migration
{

    /**
     * @return Country
     */
    private function getCountry()
    {
        $country = Country::find()
            ->select('id')
            ->where([
                'char_code' => 'ID',
                'name' => 'Индонезия 2',
            ])
            ->one();

        if (!$country) {
            $country = new Country();
            $country->char_code = 'ID';
            $country->name = 'Индонезия 2';
            $country->save();
        }
        return $country;
    }

    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        $apiClass = DeliveryApiClass::findOne(['name' => 'aspExpressApi']);

        if (!($apiClass instanceof DeliveryApiClass)) {
            $apiClass = new DeliveryApiClass();
            $apiClass->name = 'aspExpressApi';
            // потом поменяем на путь с учетом страны
            $apiClass->class_path = '/aspexpress-api/src/aspExpressApi.php';
            $apiClass->save();
        }

        $country = $this->getCountry();

        $delivery = Delivery::findOne(['char_code' => 'ASP', 'country_id' => $country->id]);

        if (!$delivery) {
            $delivery = new Delivery();
            $delivery->country_id = $country->id;
            $delivery->name = 'ASPEXPRESS';
            $delivery->char_code = 'ASP';
            $delivery->auto_sending = 0;
        }

        if (!$delivery->created_at) {
            $delivery->created_at = time();
        }
        if (!$delivery->updated_at) {
            $delivery->updated_at = time();
        }

        $delivery->api_class_id = $apiClass->id;
        $delivery->active = 1;

        $delivery->save();

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $country = $this->getCountry();

        $delivery = Delivery::findOne(['char_code' => 'ASP', 'country_id' => $country->id]);

        if ($delivery) {
            $delivery->api_class_id = null;
            $delivery->save();
        }

        $apiClass = DeliveryApiClass::find()
            ->where([
                'name' => 'AspExpressApi',
                'class_path' => '/aspexpress-api/src/aspExpressApi.php'
            ])
            ->one();

        $apiClass->delete();

    }
}
