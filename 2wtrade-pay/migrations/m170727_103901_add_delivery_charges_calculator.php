<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\Delivery;

/**
 * Class m170727_103901_add_delivery_charges_template
 */
class m170727_103901_add_delivery_charges_calculator extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('delivery_charges_calculator', [
            'id' => $this->primaryKey(),
            'name' => $this->string(100),
            'class_path' => $this->string(100),
            'active' => $this->boolean()->defaultValue(0),
            'created_at' => $this->integer(),
        ], $this->tableOptions);

        $this->addColumn(Delivery::tableName(), 'charges_calculator_id', $this->integer()->after('api_class_id'));
        $this->addColumn(Delivery::tableName(), 'charges_values', $this->text());
        $this->addForeignKey('fk_delivery_charges_calculator_id', Delivery::tableName(), 'charges_calculator_id', 'delivery_charges_calculator', 'id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Delivery::tableName(), 'charges_calculator_id');
        $this->dropColumn(Delivery::tableName(), 'charges_values');

        $this->dropTable('delivery_charges_calculator');
    }
}
