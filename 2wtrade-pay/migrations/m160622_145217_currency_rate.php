<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m160622_145217_currency_rate
 */
class m160622_145217_currency_rate extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = new CrontabTask();
        $crontabTask->name = 'update_currencies_rates';
        $crontabTask->description = 'Обновление курса валют.';
        $crontabTask->save();

        $this->dropTable('currency_rate_task');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->createTable('currency_rate_task', [
            'id' => $this->primaryKey(),
            'status' => "ENUM('in_progress', 'done', 'fail') NOT NULL DEFAULT 'in_progress'",
            'query' => $this->string(1000)->notNull(),
            'answer' => $this->string(2000)->defaultValue(null),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $crontabTask = CrontabTask::find()
            ->byName('update_currencies_rates')
            ->one();

        if ($crontabTask) {
            $crontabTask->delete();
        }
    }
}
