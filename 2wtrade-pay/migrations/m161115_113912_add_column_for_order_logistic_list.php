<?php
use app\components\CustomMigration as Migration;

/**
 * Class m161115_113912_add_column_for_order_logistic_list
 */
class m161115_113912_add_column_for_order_logistic_list extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('order_logistic_list', 'label', 'VARCHAR(100) DEFAULT NULL AFTER ticket');

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('order_logistic_list', 'label');

    }
}
