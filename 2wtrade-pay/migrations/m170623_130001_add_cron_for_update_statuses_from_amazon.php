<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m170623_130001_add_cron_for_update_statuses_from_amazon
 */
class m170623_130001_add_cron_for_update_statuses_from_amazon extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = CrontabTask::findOne(['name' => 'delivery_get_orders_from_update_queue']);

        if (!($crontabTask instanceof CrontabTask)) {
            $crontabTask = new CrontabTask();
            $crontabTask->name = 'delivery_get_orders_from_update_queue';
            $crontabTask->description = "Забор заказов из амазоновской очереди обновления статусов КС";
            $crontabTask->save();
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = CrontabTask::findOne(['name' => 'delivery_get_orders_from_update_queue']);

        if ($crontabTask instanceof CrontabTask) {
            $crontabTask->delete();
        }
    }
}
