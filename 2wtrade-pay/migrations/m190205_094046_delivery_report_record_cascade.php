<?php
use app\components\CustomMigration as Migration;

/**
 * Class m190205_094046_delivery_report_record_cascade
 */
class m190205_094046_delivery_report_record_cascade extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropForeignKey(
            $this->getFkName('delivery_report_record', 'delivery_report_id'),
            'delivery_report_record'
        );
        
        $this->addForeignKey(
            $this->getFkName('delivery_report_record', 'delivery_report_id'),
            'delivery_report_record',
            'delivery_report_id',
            'delivery_report',
            'id',
            self::CASCADE,
            self::CASCADE
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            $this->getFkName('delivery_report_record', 'delivery_report_id'),
            'delivery_report_record'
        );

        $this->addForeignKey(
            $this->getFkName('delivery_report_record', 'delivery_report_id'),
            'delivery_report_record',
            'delivery_report_id',
            'delivery_report',
            'id',
            self::RESTRICT,
            self::RESTRICT
        );
    }
}
