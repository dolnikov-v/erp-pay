<?php
use app\components\PermissionMigration as Migration;

/**
 * Class m181017_050038_add_delivery_permission
 */
class m181017_050038_add_delivery_permission extends Migration
{
    protected $permissions = ['delivery.control.add'];

    protected $roles = [
        'security.manager' => ['delivery.control.add'],
        'technical.director' => ['delivery.control.add'],
        'support.manager' => ['delivery.control.add'],
    ];
}
