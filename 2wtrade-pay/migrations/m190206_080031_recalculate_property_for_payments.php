<?php
use app\components\CustomMigration as Migration;

/**
 * Class m190206_080031_recalculate_property_for_payments
 */
class m190206_080031_recalculate_property_for_payments extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('temp_recalculated_balance_payments', [
           'report_id' => $this->integer()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('temp_recalculated_balance_payments');
    }
}
