<?php
use app\components\CustomMigration as Migration;
use app\modules\salary\models\Office;
use app\models\Currency;

/**
 * Class m171005_044432_office_currency
 */
class m171005_044432_office_currency extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Office::tableName(), 'currency_id', $this->integer());
        $this->addForeignKey('fk_salary_office_currency_id', Office::tableName(), 'currency_id', 'currency', 'id', self::SET_NULL, self::NO_ACTION);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_salary_office_currency_id', Office::tableName());
        $this->dropColumn(Office::tableName(), 'currency_id');
    }
}
