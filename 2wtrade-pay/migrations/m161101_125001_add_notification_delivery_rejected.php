<?php
use app\components\CustomMigration as Migration;
use app\models\Notification;

/**
 * Class m161101_125001_add_notification_delivery_rejected
 */
class m161101_125001_add_notification_delivery_rejected extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $model = new Notification([
            'description' => 'За последние 31 день найдено {count} заказов отклонённых в доставке (19 колонка)',
            'trigger' => 'report.delivery.rejected',
            'type' => Notification::TYPE_WARNING,
            'group' => Notification::GROUP_REPORT,
            'active' => Notification::ACTIVE,
        ]);

        $model->save(false);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $model = Notification::findOne(['trigger' => 'report.delivery.rejected']);

        if ($model instanceof Notification) {
            $model->delete();
        }
    }
}
