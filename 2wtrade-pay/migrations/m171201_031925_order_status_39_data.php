<?php
use app\components\CustomMigration as Migration;
use app\modules\order\models\OrderWorkflow;
use app\modules\order\models\OrderWorkflowStatus;
use app\modules\order\models\OrderStatus;

/**
 * Class m171201_031925_order_status_39_data
 */
class m171201_031925_order_status_39_data extends Migration
{

    const DATA = [
        OrderStatus::STATUS_CC_CHECKING => [
            'from' => [
                6, 19
            ],
            'to' => [
                6, 31, 36
            ]
        ],
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $orderWorkflows = OrderWorkflow::find()->active()->all();

        foreach ($orderWorkflows as $workflow) {

            foreach (self::DATA as $status_id => $data) {

                $status = self::getStatus($status_id);

                if (isset($data['to'])) {
                    $orderWorkflowStatus = new OrderWorkflowStatus();
                    $orderWorkflowStatus->workflow_id = $workflow->id;
                    $orderWorkflowStatus->status_id = $status->id;
                    $orderWorkflowStatus->parents = $data['to'];
                    $orderWorkflowStatus->created_at = time();
                    $orderWorkflowStatus->updated_at = time();
                    $orderWorkflowStatus->save();
                }

                foreach ($data['from'] as $from) {
                    $orderWorkflowStatus = OrderWorkflowStatus::findOne([
                        'workflow_id' => $workflow->id,
                        'status_id' => $from,
                    ]);
                    if ($orderWorkflowStatus) {
                        $parents = explode(",", $orderWorkflowStatus->parents);
                        if (!in_array($status->id, $parents)) {
                            $parents[] = $status->id;
                            $orderWorkflowStatus->parents = $parents;
                            $orderWorkflowStatus->save();
                        }
                    }
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach (self::DATA as $status_id => $data) {

            $status = self::getStatus($status_id);

            $this->delete(OrderWorkflowStatus::tableName(), [
                'status_id' => $status->id
            ]);
        }

        $orderWorkflows = OrderWorkflow::find()->active()->all();

        foreach ($orderWorkflows as $workflow) {

            foreach (self::DATA as $status_id => $data) {

                $status = self::getStatus($status_id);

                foreach ($data['from'] as $from) {

                    $orderWorkflowStatus = OrderWorkflowStatus::findOne([
                        'workflow_id' => $workflow->id,
                        'status_id' => $from,
                    ]);
                    if ($orderWorkflowStatus) {
                        $parents = explode(",", $orderWorkflowStatus->parents);
                        $key = array_search($status->id, $parents);
                        if ($key !== false) {
                            unset($parents[$key]);
                            $orderWorkflowStatus->parents = $parents;
                            $orderWorkflowStatus->save();
                        }
                    }
                }
            }
        }
    }

    public static function getStatus($sort)
    {
        $status = OrderStatus::find()->where(['sort' => $sort])->one();
        if ($status) {
            return $status;
        } else {
            echo "No status in base " . $sort . PHP_EOL;
            die();
        }
    }
}
