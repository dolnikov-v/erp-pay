<?php
use app\components\PermissionMigration as Migration;

/**
 * Class m180130_025711_delivery_invoice_emails
 */
class m180130_025711_delivery_invoice_emails extends Migration
{
    protected $permissions = [
        'report.invoice.sendinvoice',
    ];

    protected $roles = [
        'finance.director' => [
            'report.invoice.sendinvoice',
        ],
        'finance.manager' => [
            'report.invoice.sendinvoice',
        ],
        'admin' => [
            'report.invoice.sendinvoice',
        ],
        'project.manager' => [
            'report.invoice.sendinvoice',
        ],
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('delivery', 'invoice_email_list', $this->text()->after('auto_list_generation_size'));
        parent::safeUp();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('delivery', 'invoice_email_list');
        parent::safeDown();
    }
}
