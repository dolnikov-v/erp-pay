<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m180426_035445_add_crontab_task_adcombo_parser
 */
class m180426_035445_add_crontab_task_adcombo_parser extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = new CrontabTask();
        $crontabTask->name = CrontabTask::TASK_ADCOMBO_PARSER;
        $crontabTask->description = 'Парсер ЛК AdCombo';
        $crontabTask->save();

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $cronTask = CrontabTask::find()
            ->byName(CrontabTask::TASK_ADCOMBO_PARSER)
            ->one();

        if ($cronTask) {
            $cronTask->delete();
        }

    }
}
