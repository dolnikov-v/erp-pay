<?php
use app\components\CustomMigration as Migration;
use app\models\Currency;

class m000000_000012_currency extends Migration
{
    public function safeUp()
    {
        $this->createTable(Currency::tableName(), [
            'id' => $this->primaryKey(),
            'name' => $this->string(128)->notNull(),
            'num_code' => $this->string(3)->notNull(),
            'char_code' => $this->string(3)->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        foreach ($this->currenciesFixture() as $key => $fixture) {
            $currency = new Currency($fixture);
            $currency->save(false);
        }
    }

    public function safeDown()
    {
        $this->dropTable(Currency::tableName());
    }

    /**
     * @return array
     */
    private function currenciesFixture()
    {
        return [
            [
                'name' => 'Доллар США',
                'num_code' => 840,
                'char_code' => 'USD',
            ],
            [
                'name' => 'Индийская рупия',
                'num_code' => 356,
                'char_code' => 'INR',
            ],
            [
                'name' => 'Тайский бат',
                'num_code' => 764,
                'char_code' => 'THB',
            ],
            [
                'name' => 'Мексиканский песо',
                'num_code' => 484,
                'char_code' => 'MXN',
            ],
        ];
    }
}
