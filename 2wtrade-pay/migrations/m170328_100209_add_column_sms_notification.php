<?php
use app\components\CustomMigration as Migration;
/**
 * Class m170328_100209_add_column_sms_notification
 */
class m170328_100209_add_column_sms_notification extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('sms_notification', 'is_just_one_time', $this->boolean()->defaultValue(0));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('sms_notification', 'is_just_one_time');
    }
}
