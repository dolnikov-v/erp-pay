<?php
use app\components\CustomMigration as Migration;

/**
 * Class m171103_105043_fix_product_category_table
 */
class m171103_105043_fix_product_category_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropTable('product_link_category');
        $this->dropTable('product_category');
        $this->createTable('product_category', [
            'id' => $this->primaryKey(),
            'name' => $this->string(128)->notNull()->unique(),
        ], $this->tableOptions);

        $this->createTable('product_link_category', [
            'product_id' => $this->integer()->notNull(),
            'category_id' => $this->integer()->notNull(),
        ], $this->tableOptions);

        $this->addPrimaryKey(null, 'product_link_category', ['product_id', 'category_id']);

        // creates index for column `product_id`
        $this->createIndex(
            null,
            'product_link_category',
            'product_id'
        );

        // add foreign key for table `product`
        $this->addForeignKey(
            null,
            'product_link_category',
            'product_id',
            'product',
            'id',
            self::CASCADE
        );

        // creates index for column `category_id`
        $this->createIndex(
            null,
            'product_link_category',
            'category_id'
        );

        // add foreign key for table `product_category`
        $this->addForeignKey(
            null,
            'product_link_category',
            'category_id',
            'product_category',
            'id',
            self::CASCADE
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('product_link_category');
        $this->dropTable('product_category');
        $this->createTable('product_category', [
            'id' => $this->primaryKey(),
            'name' => $this->string(128)->notNull()->unique(),
        ]);

        $this->createTable('product_link_category', [
            'product_id' => $this->integer()->notNull(),
            'category_id' => $this->integer()->notNull(),
        ]);

        $this->addPrimaryKey(null, 'product_link_category', ['product_id', 'category_id']);

        // creates index for column `product_id`
        $this->createIndex(
            null,
            'product_link_category',
            'product_id'
        );

        // add foreign key for table `product`
        $this->addForeignKey(
            null,
            'product_link_category',
            'product_id',
            'product',
            'id',
            self::CASCADE
        );

        // creates index for column `category_id`
        $this->createIndex(
            null,
            'product_link_category',
            'category_id'
        );

        // add foreign key for table `product_category`
        $this->addForeignKey(
            null,
            'product_link_category',
            'category_id',
            'product_category',
            'id',
            self::CASCADE
        );
    }
}
