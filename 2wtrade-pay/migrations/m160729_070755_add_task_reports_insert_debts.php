<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m160729_070755_add_task_reports_insert_debts
 */
class m160729_070755_add_task_reports_insert_debts extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = new CrontabTask();
        $crontabTask->name = 'reports_insert_debts';
        $crontabTask->description = 'Заполнение таблицы отчетов report_debts';
        $crontabTask->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = CrontabTask::find()
            ->byName('reports_insert_debts')
            ->one();

        if ($crontabTask) {
            $crontabTask->delete();
        }
    }
}
