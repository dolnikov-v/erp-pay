<?php

use app\components\CustomMigration as Migration;
use app\modules\order\models\Order;
use app\modules\order\models\OrderLog;

class m160331_133813_log_order extends Migration
{
    public function safeUp()
    {
        $this->createTable(OrderLog::tableName(), [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->notNull(),
            'group_id' => $this->string()->notNull(),
            'field' => $this->string()->notNull(),
            'old' => $this->string()->notNull(),
            'new' => $this->string()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $this->tableOptions);

        $this->addForeignKey(null, OrderLog::tableName(), 'order_id', Order::tableName(), 'id', self::NO_ACTION, self::NO_ACTION);
    }

    public function safeDown()
    {
        $this->dropTable(OrderLog::tableName());
    }
}
