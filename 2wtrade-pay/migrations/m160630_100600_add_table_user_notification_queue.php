<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160630_100600_add_table_user_notification_queue
 */
class m160630_100600_add_table_user_notification_queue extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('user_notification_queue', [
            'id' => $this->primaryKey(),
            'notification_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'trigger' => $this->string(255)->notNull(),
            'type_sending' => 'ENUM("email", "skype", "sms") DEFAULT NULL',
            'interval' => 'ENUM("always", "hourly", "daily") DEFAULT NULL',
            'read' => $this->smallInteger()->notNull()->defaultValue(0),
            'status_send' => $this->smallInteger()->notNull()->defaultValue(0),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $this->tableOptions);
        $this->addForeignKey(null, 'user_notification_queue', 'notification_id', 'user_notification', 'id', 'CASCADE');
        $this->addForeignKey(null, 'user_notification_queue', 'user_id', 'user', 'id', 'CASCADE');
        $this->addForeignKey(null, 'user_notification_queue', 'trigger', 'notification', 'trigger', 'CASCADE');

        $this->createIndex(null, 'user_notification_queue', 'type_sending');
        $this->createIndex(null, 'user_notification_queue', 'interval');
        $this->createIndex(null, 'user_notification_queue', 'read');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('user_notification_queue');
    }
}
