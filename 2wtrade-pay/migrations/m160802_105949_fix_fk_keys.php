<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160802_105949_fix_fk_keys
 */
class m160802_105949_fix_fk_keys extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropForeignKey($this->getFkName('order_product', 'order_id'), 'order_product');
        $this->addForeignKey(null, 'order_product', 'order_id', 'order', 'id', self::CASCADE, self::RESTRICT);

        $this->dropForeignKey($this->getFkName('order_product', 'product_id'), 'order_product');
        $this->addForeignKey(null, 'order_product', 'product_id', 'product', 'id', self::CASCADE, self::RESTRICT);

        $this->dropForeignKey($this->getFkName('order_log', 'order_id'), 'order_log');
        $this->addForeignKey(null, 'order_log', 'order_id', 'order', 'id', self::CASCADE, self::RESTRICT);

        $this->dropForeignKey($this->getFkName('order_log_status', 'order_id'), 'order_log_status');
        $this->addForeignKey(null, 'order_log_status', 'order_id', 'order', 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey($this->getFkName('order_product', 'order_id'), 'order_product');
        $this->addForeignKey(null, 'order_product', 'order_id', 'order', 'id', self::NO_ACTION, self::NO_ACTION);

        $this->dropForeignKey($this->getFkName('order_product', 'product_id'), 'order_product');
        $this->addForeignKey(null, 'order_product', 'product_id', 'product', 'id', self::NO_ACTION, self::NO_ACTION);

        $this->dropForeignKey($this->getFkName('order_log', 'order_id'), 'order_log');
        $this->addForeignKey(null, 'order_log', 'order_id', 'order', 'id', self::NO_ACTION, self::NO_ACTION);

        $this->dropForeignKey($this->getFkName('order_log_status', 'order_id'), 'order_log_status');
        $this->addForeignKey(null, 'order_log_status', 'order_id', 'order', 'id', self::NO_ACTION, self::NO_ACTION);
    }
}
