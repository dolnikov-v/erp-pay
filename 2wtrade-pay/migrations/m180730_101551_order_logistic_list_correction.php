<?php
use app\components\CustomMigration as Migration;
use app\modules\order\models\OrderLogisticList;
use app\modules\administration\models\CrontabTask;
use app\modules\callcenter\models\CallCenterCheckRequest;

/**
 * Class m180730_101551_order_logistic_list_correction
 */
class m180730_101551_order_logistic_list_correction extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(OrderLogisticList::tableName(), 'correction', $this->boolean()->after('status')->defaultValue(false));
        $this->addColumn(CallCenterCheckRequest::tableName(), 'need_correction', $this->boolean()->after('status_id')->defaultValue(false));
        $this->createIndex(null, CallCenterCheckRequest::tableName(), 'need_correction');

        $crontabTask = new CrontabTask();
        $crontabTask->name = CrontabTask::TASK_DELIVERY_GENERATE_CORRECTION_LISTS;
        $crontabTask->description = 'Генерация и отправка корректирующих листов';
        $crontabTask->save();

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(OrderLogisticList::tableName(), 'correction');
        $this->dropColumn(CallCenterCheckRequest::tableName(), 'need_correction');


        $crontabTask = CrontabTask::find()
            ->byName(CrontabTask::TASK_DELIVERY_GENERATE_CORRECTION_LISTS)
            ->one();

        if ($crontabTask) {
            $crontabTask->delete();
        }
    }
}
