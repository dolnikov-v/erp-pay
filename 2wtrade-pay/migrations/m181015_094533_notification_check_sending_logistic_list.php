<?php
use app\components\CustomMigration as Migration;

/**
 * Class m181015_094533_notification_check_sending_logistic_list
 */
class m181015_094533_notification_check_sending_logistic_list extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('crontab_task', [
            'name' => 'order_logistic_check_sending_list',
            'description' => 'Проверка часовой задержки отправки листов',
            'active' => 1,
            'created_at' => time(),
            'updated_at' => time(),
        ]);
        $this->insert('notification', [
            'trigger' => 'order.logistic.check.sending.list',
            'description' => 'В {country}:{delivery} не отправлено {num}.\r\nПоследняя дата создания листа {last}.',
            'type' => 'danger',
            'group' => 'system',
            'active' => 1,
        ]);
        foreach (['technical.director', 'support.manager', 'country.curator', 'project.manager'] as $role) {
            $this->insert('notification_role', [
                'role' => $role,
                'trigger' => 'order.logistic.check.sending.list',
                'created_at' => time(),
                'updated_at' => time(),
            ]);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete('notification', ['trigger' => 'order.logistic.check.sending.list']);
        $this->delete('crontab_task', ['name' => 'order_logistic_check_sending_list']);
    }
}
