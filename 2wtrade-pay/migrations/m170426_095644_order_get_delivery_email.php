<?php
use app\components\CustomMigration as Migration;
use yii\db\Query;
use app\modules\order\components\exporter\Exporter;
use app\modules\order\models\OrderClarification;
use app\models\User;
use app\models\Country;
use app\modules\delivery\models\Delivery;
use app\modules\administration\models\CrontabTask;
use app\models\Notification;

/**
 * Class m170426_095644_order_get_delivery_email
 */
class m170426_095644_order_get_delivery_email extends Migration
{
    const ROLES = [
        'business_analyst',
        'callcenter.manager',
        'country.curator',
        'project.manager',
    ];

    const RULES = [
        'order.index.getdeliveryemail',
        'order.index.clarificationindelivery',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $query = new Query();

        foreach (self::ROLES as $role) {
            $is_role = $query->select('*')->from($this->authManager->itemTable)
                ->where(['name' => $role, 'type' => 1])
                ->one();

            if (!$is_role) {
                $this->insert($this->authManager->itemTable, [
                    'name' => $role,
                    'type' => 1,
                    'description' => $role,
                    'created_at' => time(),
                    'updated_at' => time()]);
            }

            foreach (self::RULES as $rule) {

                $is_rule = $query->select('*')->from($this->authManager->itemTable)
                    ->where(['name' => $rule, 'type' => 2])
                    ->one();

                if (!$is_rule) {
                    $this->insert($this->authManager->itemTable, [
                        'name' => $rule,
                        'description' => $rule,
                        'type' => 2,
                        'created_at' => time(),
                        'updated_at' => time()]);
                }

                $is_can = $query->select('*')->from($this->authManager->itemChildTable)->where([
                    'parent' => $role,
                    'child' => $rule
                ])->one();

                if (!$is_can) {
                    $this->insert($this->authManager->itemChildTable, [
                        'parent' => $role,
                        'child' => $rule
                    ]);
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach (self::ROLES as $role) {
            foreach (self::RULES as $rule) {
                $this->delete($this->authManager->itemChildTable, [
                    'parent' => $role,
                    'child' => $rule
                ]);
            }
        }
    }
}
