<?php
use app\components\CustomMigration as Migration;

use yii\db\Query;

use app\modules\salary\models\Holiday;

/**
 * Class m170725_034223_call_center_holiday
 */
class m170725_034223_call_center_holiday extends Migration
{
    const ROLES = [
        'finance.director',
        'callcenter.hr',
        'project.manager'
    ];

    const RULES = [
        'callcenter.holiday.edit',
        'callcenter.holiday.delete',
        'callcenter.holiday.index'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $query = new Query();

        foreach (self::RULES as $rule) {

            $is_rule = $query->select('*')->from($this->authManager->itemTable)
                ->where(['name' => $rule, 'type' => 2])
                ->one();

            if (!$is_rule) {
                $this->insert($this->authManager->itemTable, [
                    'name' => $rule,
                    'description' => $rule,
                    'type' => 2,
                    'created_at' => time(),
                    'updated_at' => time()]);
            }

            foreach (self::ROLES as $role) {
                $is_can = $query->select('*')->from($this->authManager->itemChildTable)->where([
                    'parent' => $role,
                    'child' => $rule
                ])->one();

                if (!$is_can) {
                    $this->insert($this->authManager->itemChildTable, [
                        'parent' => $role,
                        'child' => $rule
                    ]);
                }
            }
        }

        $this->addColumn(Holiday::tableName(), 'name', $this->string()->after('date'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach (self::RULES as $rule) {
            foreach (self::ROLES as $role) {
                $this->delete($this->authManager->itemChildTable, [
                    'parent' => $role,
                    'child' => $rule
                ]);
            }
        }

        $this->dropColumn(Holiday::tableName(), 'name');
    }
}
