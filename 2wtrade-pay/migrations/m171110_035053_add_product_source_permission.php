<?php
use app\components\PermissionMigration;

/**
 * Class m171110_035053_add_product_source_permission
 */
class m171110_035053_add_product_source_permission extends PermissionMigration
{
    /**
     * @var array
     */
    public $permissions = [
        'catalog.product.edit.source',
    ];
}
