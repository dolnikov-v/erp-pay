<?php

use app\components\CustomMigration as Migration;

/**
 * Class m170608_105202_add_permission_for_unshipping_reasons_report
 */
class m170608_105202_add_permission_for_unshipping_reasons_report extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        // Доступ к отчету о причинах недоставленных заказов
        $this->insert('{{%auth_item}}',array(
            'name'=>'report.unshipping-reasons.index',
            'type' => '2',
            'description' => 'report.unshipping-reasons.index',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'country.curator',
            'child' => 'report.unshipping-reasons.index'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'business_analyst',
            'child' => 'report.unshipping-reasons.index'
        ));

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->authManager->itemChildTable, ['child' => 'report.unshipping-reasons.index', 'parent' => 'country.curator']);
        $this->delete($this->authManager->itemChildTable, ['child' => 'report.unshipping-reasons.index', 'parent' => 'business_analyst']);
        $this->delete('{{%auth_item}}', ['name' => 'report.unshipping-reasons.index']);
    }
}
