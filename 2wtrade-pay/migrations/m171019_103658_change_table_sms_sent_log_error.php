<?php
use app\components\CustomMigration as Migration;
use app\models\UserNotification;
/**
 * Class m171019_103658_change_table_sms_sent_log_error
 */
class m171019_103658_change_table_sms_sent_log_error extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropTable('sms_sent_error_log');

        $this->createTable('transport_log', [
            'id' =>$this->primaryKey(),
            'notification_id' => $this->integer(),
            'to' => $this->string(),
            'from' => $this->string(),
            'request' => $this->text(),
            'response' => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);

        $this->addForeignKey(null, 'transport_log', 'notification_id', UserNotification::tableName(), 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        //нет смысла
    }
}
