<?php
use app\components\CustomMigration as Migration;

/**
 * Class m180112_065833_deivery_report_rate
 */
class m180112_065833_deivery_report_rate extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('delivery_report_rate', [
            'id' => $this->primaryKey(),
            'delivery_report_id' => $this->integer()->notNull(),
            'currency_id' => $this->integer()->notNull(),
            'rate' => $this->decimal(10, 4)->defaultValue(null),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addForeignKey(null, 'delivery_report_rate', 'delivery_report_id', 'delivery_report', 'id', self::CASCADE, self::CASCADE);
        $this->addForeignKey(null, 'delivery_report_rate', 'currency_id', 'currency', 'id', self::CASCADE, self::CASCADE);

        $this->dropColumn('delivery_report', 'currency_rates');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('delivery_report_rate');

        $this->addColumn('delivery_report', 'currency_rates', $this->string(200));
    }
}
