<?php
use app\components\CustomMigration as Migration;
use app\modules\order\models\OrderStatus;
use app\modules\order\models\OrderWorkflow;
use app\modules\order\models\OrderWorkflowStatus;
use yii\base\Exception;

/**
 * Class m160705_071333_japan_workflow
 */
class m160705_071333_japan_workflow extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $workflow = new OrderWorkflow();
        $workflow->name = 'Fulfillment - Japan';
        $workflow->comment = 'Создано специально для Японии.';
        $workflow->active = 1;

        if ($workflow->save(false)) {
            foreach ($this->getFixture($workflow) as $data) {
                $model = new OrderWorkflowStatus($data);
                $model->scenario = OrderWorkflowStatus::SCENARIO_IGNORE_PARENTS;
                $model->save(false);
            }
        } else {
            throw new Exception("Не удалось добавить workflow.", 400);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

    }

    /**
     * @param OrderWorkflow $workflow
     * @return array
     */
    private function getFixture($workflow)
    {
        return [
            ['workflow_id' => $workflow->id, 'status_id' => OrderStatus::STATUS_SOURCE_LONG_FORM, 'parents' => [6, 7, 26, 25, 4, 12, 15, 16]],
            ['workflow_id' => $workflow->id, 'status_id' => OrderStatus::STATUS_CC_APPROVED, 'parents' => [1, 7, 26, 25, 4, 12, 15, 16]],
            ['workflow_id' => $workflow->id, 'status_id' => OrderStatus::STATUS_CC_REJECTED, 'parents' => [1, 6, 26, 25, 4, 12, 15, 16]],
            ['workflow_id' => $workflow->id, 'status_id' => OrderStatus::STATUS_CC_DOUBLE, 'parents' => [1, 6, 7, 25, 4, 12, 15, 16]],
            ['workflow_id' => $workflow->id, 'status_id' => OrderStatus::STATUS_CC_TRASH, 'parents' => [1, 6, 7, 26, 4, 12, 15, 16]],
            ['workflow_id' => $workflow->id, 'status_id' => OrderStatus::STATUS_CC_RECALL, 'parents' => [1, 6, 7, 26, 25, 12, 15, 16]],
            ['workflow_id' => $workflow->id, 'status_id' => OrderStatus::STATUS_DELIVERY_ACCEPTED, 'parents' => [1, 6, 7, 26, 25, 4, 15, 16]],
            ['workflow_id' => $workflow->id, 'status_id' => OrderStatus::STATUS_DELIVERY_BUYOUT, 'parents' => [1, 6, 7, 26, 25, 4, 12, 16]],
            ['workflow_id' => $workflow->id, 'status_id' => OrderStatus::STATUS_DELIVERY_DENIAL, 'parents' => [1, 6, 7, 26, 25, 4, 12, 15]],
        ];
    }
}
