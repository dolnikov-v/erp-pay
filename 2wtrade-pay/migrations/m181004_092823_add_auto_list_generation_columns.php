<?php
use app\components\CustomMigration as Migration;

/**
 * Class m181004_092823_add_auto_list_generation_columns
 */
class m181004_092823_add_auto_list_generation_columns extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('delivery', 'auto_list_generation_columns', $this->text()->after('auto_list_generation_size'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('delivery', 'auto_list_generation_columns');
    }
}
