<?php
use app\components\CustomMigration as Migration;
use app\modules\callcenter\models\CallCenterWorkTime;

/**
 * Class m170822_114802_call_center_work_time
 */
class m170822_114802_call_center_work_time extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(CallCenterWorkTime::tableName(), 'last_action', $this->integer()->after('number_of_calls'));
        $this->addColumn(CallCenterWorkTime::tableName(), 'first_action', $this->integer()->after('number_of_calls'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(CallCenterWorkTime::tableName(), 'last_action');
        $this->dropColumn(CallCenterWorkTime::tableName(), 'first_action');
    }
}
