<?php

use app\components\CustomMigration;
use app\modules\callcenter\models\CallCenterFullRequest;

/**
 * Handles adding response_message to table `report_call_center`.
 */
class m161109_080613_add_response_message_to_report_call_center extends CustomMigration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(CallCenterFullRequest::tableName(), 'response_message', $this->text());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(CallCenterFullRequest::tableName(), 'response_message');
    }
}
