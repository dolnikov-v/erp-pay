<?php
use app\components\PermissionMigration as Migration;

/**
 * Class m190228_053707_packager_approve
 */
class m190228_053707_packager_approve extends Migration
{
    protected $permissions = [
        'packager.request.approve',
    ];
    
    protected $roleList = [
        'logist',
        'admin',
        'controller.analyst',
        'country.curator',
        'implant',
        'junior.logist',
        'operational.director',
        'project.manager',
        'support.manager',
        'technical.director'
    ];
}
