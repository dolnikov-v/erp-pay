<?php


use app\components\CustomMigration as Migration;
use app\models\Country;
use app\modules\catalog\models\RequisiteDelivery;
use app\modules\catalog\models\RequisiteDeliveryLink;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryContract;


/**
 * Handles the creation for table `requisite_delivery_link`.
 */
class m180329_034736_create_requisite_delivery_link extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(RequisiteDeliveryLink::tableName(), [
            'id' => $this->primaryKey(),
            'requisite_delivery_id' => $this->integer()->notNull(),
            'country_id' => $this->integer()->notNull(),
            'delivery_id' => $this->integer()->notNull(),
            'delivery_contract_id' => $this->integer()->notNull(),
            'is_default' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);

        $this->addForeignKey(null, RequisiteDeliveryLink::tableName(), 'requisite_delivery_id', RequisiteDelivery::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, RequisiteDeliveryLink::tableName(), 'delivery_id', Delivery::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, RequisiteDeliveryLink::tableName(), 'delivery_contract_id', DeliveryContract::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, RequisiteDeliveryLink::tableName(), 'country_id', Country::tableName(), 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable(RequisiteDeliveryLink::tableName());
    }
}
