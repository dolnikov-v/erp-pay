<?php

use app\components\PermissionMigration as Migration;

/**
 * Class m181121_084745_office_expense_days
 */
class m181121_084745_office_expense_days extends Migration
{
    protected $permissions = [
        'salary.officeexpense.edit',
        'salary.officeexpense.delete',
        'salary.officeexpense.deletefile',
        'media.salaryexpense.filename',
    ];

    protected $roles = [
        'admin' => [
            'salary.officeexpense.edit',
            'salary.officeexpense.delete',
            'salary.officeexpense.deletefile',
            'media.salaryexpense.filename',
        ],
        'controller.analyst' => [
            'salary.officeexpense.edit',
            'salary.officeexpense.delete',
            'salary.officeexpense.deletefile',
            'media.salaryexpense.filename',
        ],
        'country.curator' => [
            'salary.officeexpense.edit',
            'salary.officeexpense.delete',
            'salary.officeexpense.deletefile',
            'media.salaryexpense.filename',
        ],
        'finance.manager' => [
            'salary.officeexpense.edit',
            'salary.officeexpense.delete',
            'salary.officeexpense.deletefile',
            'media.salaryexpense.filename',
        ],
        'partners.manager' => [
            'salary.officeexpense.edit',
            'salary.officeexpense.delete',
            'salary.officeexpense.deletefile',
            'media.salaryexpense.filename',
        ],
        'salaryproject.clerk' => [
            'salary.officeexpense.edit',
            'salary.officeexpense.delete',
            'salary.officeexpense.deletefile',
            'media.salaryexpense.filename',
        ],
        'support.manager' => [
            'salary.officeexpense.edit',
            'salary.officeexpense.delete',
            'salary.officeexpense.deletefile',
            'media.salaryexpense.filename',
        ],
        'technical.director' => [
            'salary.officeexpense.edit',
            'salary.officeexpense.delete',
            'salary.officeexpense.deletefile',
            'media.salaryexpense.filename',
        ],
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('salary_office_expense', [
            'id' => $this->primaryKey(),
            'office_id' => $this->integer(),
            'type' => $this->string(),
            'expense' => $this->double(),
            'currency_id' => $this->integer()->defaultValue(1),
            'filename' => $this->string(),
            'date_from' => $this->date(),
            'date_to' => $this->date(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);

        $this->addForeignKey(null, 'salary_office_expense', 'office_id', 'salary_office', 'id', self::CASCADE, self::CASCADE);

        $this->addColumn('salary_office', 'expense_rent_currency_id', $this->integer()
            ->after('expense_rent')
            ->defaultValue(1));
        $this->addColumn('salary_office', 'expense_account_services_currency_id', $this->integer()
            ->after('expense_account_services')
            ->defaultValue(1));
        $this->addColumn('salary_office', 'expense_internet_currency_id', $this->integer()
            ->after('expense_internet')
            ->defaultValue(1));
        $this->addColumn('salary_office', 'expense_stationery_currency_id', $this->integer()
            ->after('expense_stationery')
            ->defaultValue(1));
        $this->addColumn('salary_office', 'expense_utilities_currency_id', $this->integer()
            ->after('expense_utilities')
            ->defaultValue(1));
        $this->addColumn('salary_office', 'expense_tax_currency_id', $this->integer()
            ->after('expense_tax')
            ->defaultValue(1));
        $this->addColumn('salary_office', 'expense_office_supplies_currency_id', $this->integer()
            ->after('expense_office_supplies')
            ->defaultValue(1));
        $this->addColumn('salary_office', 'expense_office_expenses_currency_id', $this->integer()
            ->after('expense_office_expenses')
            ->defaultValue(1));
        $this->addColumn('salary_office', 'expense_telephony_office_currency_id', $this->integer()
            ->after('expense_telephony_office')
            ->defaultValue(1));
        $this->addColumn('salary_office', 'expense_security_currency_id', $this->integer()
            ->after('expense_security')
            ->defaultValue(1));
        $this->addColumn('salary_office', 'expense_other_currency_id', $this->integer()
            ->after('expense_other')
            ->defaultValue(1));
        $this->addColumn('salary_office', 'expense_cleaning_currency_id', $this->integer()
            ->after('expense_cleaning')
            ->defaultValue(1));
        $this->addColumn('salary_office', 'expense_electricity_currency_id', $this->integer()
            ->after('expense_electricity')
            ->defaultValue(1));
        $this->addColumn('salary_office', 'expense_legal_expenses_currency_id', $this->integer()
            ->after('expense_legal_expenses')
            ->defaultValue(1));
        $this->addColumn('salary_office', 'expense_courier_services_currency_id', $this->integer()
            ->after('expense_courier_services')
            ->defaultValue(1));
        $this->addColumn('salary_office', 'expense_it_support_currency_id', $this->integer()
            ->after('expense_it_support')
            ->defaultValue(1));
        $this->addColumn('salary_office', 'expense_legal_service_currency_id', $this->integer()
            ->after('expense_legal_service')
            ->defaultValue(1));
        $this->addColumn('salary_office', 'expense_bank_fees_currency_id', $this->integer()
            ->after('expense_bank_fees')
            ->defaultValue(1));
        $this->addColumn('salary_office', 'expense_representation_currency_id', $this->integer()
            ->after('expense_representation')
            ->defaultValue(1));

        parent::safeUp();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('salary_office_expense');

        $this->dropColumn('salary_office', 'expense_rent_currency_id');
        $this->dropColumn('salary_office', 'expense_account_services_currency_id');
        $this->dropColumn('salary_office', 'expense_internet_currency_id');
        $this->dropColumn('salary_office', 'expense_stationery_currency_id');
        $this->dropColumn('salary_office', 'expense_utilities_currency_id');
        $this->dropColumn('salary_office', 'expense_tax_currency_id');
        $this->dropColumn('salary_office', 'expense_office_supplies_currency_id');
        $this->dropColumn('salary_office', 'expense_office_expenses_currency_id');
        $this->dropColumn('salary_office', 'expense_telephony_office_currency_id');
        $this->dropColumn('salary_office', 'expense_security_currency_id');
        $this->dropColumn('salary_office', 'expense_other_currency_id');
        $this->dropColumn('salary_office', 'expense_cleaning_currency_id');
        $this->dropColumn('salary_office', 'expense_electricity_currency_id');
        $this->dropColumn('salary_office', 'expense_legal_expenses_currency_id');
        $this->dropColumn('salary_office', 'expense_courier_services_currency_id');
        $this->dropColumn('salary_office', 'expense_it_support_currency_id');
        $this->dropColumn('salary_office', 'expense_legal_service_currency_id');
        $this->dropColumn('salary_office', 'expense_bank_fees_currency_id');
        $this->dropColumn('salary_office', 'expense_representation_currency_id');

        parent::safeDown();
    }
}
