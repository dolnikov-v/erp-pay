<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m180131_035324_add_buyouts_by_country_notification
 */
class m180131_035324_add_buyouts_by_country_notification extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_BUYOUTS_BY_COUNTRY]);
        if (!$crontabTask) {
            $crontabTask = new CrontabTask();
            $crontabTask->name = CrontabTask::TASK_BUYOUTS_BY_COUNTRY;
            $crontabTask->description = "Уведомление о выкупе по странам 7/15/30/45 дней назад";
            $crontabTask->save();
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_BUYOUTS_BY_COUNTRY]);
        $crontabTask->delete();
    }
}
