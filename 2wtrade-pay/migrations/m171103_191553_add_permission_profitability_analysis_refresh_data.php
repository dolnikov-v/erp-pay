<?php
use app\components\CustomMigration as Migration;

/**
 * Class m171103_191553_add_permission_profitability_analysis_refresh_data
 */
class m171103_191553_add_permission_profitability_analysis_refresh_data extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}', [
            'name' => 'finance.reportprofitabilityanalysis.refreshdata',
            'type' => '2',
            'description' => 'finance.reportprofitabilityanalysis.refreshdata',
            'created_at' => time(),
            'updated_at' => time()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete('{{%auth_item}}', ['name' => 'finance.reportprofitabilityanalysis.refreshdata']);
    }
}
