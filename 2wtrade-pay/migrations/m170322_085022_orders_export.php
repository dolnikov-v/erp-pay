<?php
use app\components\CustomMigration as Migration;
use app\models\User;
use app\models\Country;
use app\modules\order\models\OrderExport;
use app\modules\administration\models\CrontabTask;
use app\models\Notification;
use app\modules\order\components\exporter\Exporter;

/**
 * Class m170322_085022_orders_export
 */
class m170322_085022_orders_export extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(OrderExport::tableName(), [
            'id' => $this->primaryKey(),
            'country_id' => $this->integer(),
            'user_id' => $this->integer(),
            'type' => $this->enum([Exporter::TYPE_CSV, Exporter::TYPE_EXCEL]),
            'query' => $this->text(),
            'url' => $this->text(),
            'columns' => $this->text(),
            'lines' => $this->integer(),
            'filename' => $this->string(),
            'date_format' => $this->string(),
            'language' => $this->string(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'generated_at' => $this->integer(),
            'sent_at' => $this->integer(),
        ], $this->tableOptions);

        $this->addForeignKey(null, OrderExport::tableName(), 'user_id', User::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, OrderExport::tableName(), 'country_id', Country::tableName(), 'id', self::CASCADE, self::RESTRICT);

        $tasks = [
            CrontabTask::TASK_GENERATE_EXPORT_FILES => 'Генерация заказанных файлов экспорта',
        ];

        foreach ($tasks as $name => $description) {

            $crontabTask = CrontabTask::findOne(['name' => $name]);

            if (!($crontabTask instanceof CrontabTask)) {
                $crontabTask = new CrontabTask();
                $crontabTask->name = $name;
                $crontabTask->description = $description;
                $crontabTask->save();
            }
        }

        $triggers = [
            Notification::TRIGGER_GENERATE_EXPORT_FILE => 'Закончена генерация заказанного файла экспорта {file}',
        ];

        foreach ($triggers as $trigger => $description) {

            $notification = Notification::findOne(['trigger' => $trigger]);

            if (!($notification instanceof Notification)) {
                $notification = new Notification([
                    'description' => $description,
                    'trigger' => $trigger,
                    'type' => Notification::TYPE_SUCCESS,
                    'group' => Notification::GROUP_SYSTEM,
                    'active' => Notification::ACTIVE,
                ]);
                $notification->save(false);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable(OrderExport::tableName());

        $tasks = [
            CrontabTask::TASK_GENERATE_EXPORT_FILES
        ];

        foreach ($tasks as $task) {

            $crontabTask = CrontabTask::findOne(['name' => $task]);

            if ($crontabTask instanceof CrontabTask) {
                $crontabTask->delete();
            }
        }

        $triggers = [
            Notification::TRIGGER_GENERATE_EXPORT_FILE
        ];

        foreach ($triggers as $trigger) {

            $notification = Notification::findOne(['trigger' => $trigger]);

            if ($notification instanceof Notification) {
                $notification->delete();
            }
        }
    }
}
