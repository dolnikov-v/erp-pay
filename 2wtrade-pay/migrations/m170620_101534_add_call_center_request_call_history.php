<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170620_101534_add_call_center_request_call_history
 */
class m170620_101534_add_call_center_request_call_history extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('call_center_request_history', [
            'id' => $this->primaryKey(),
            'call_center_request_id' => $this->integer()->notNull(),
            'call_center_id' => $this->integer()->notNull(),
            'operator_id' => $this->integer()->notNull(),
            'foreign_status' => $this->integer(),
            'called_at' => $this->integer(),
        ], $this->tableOptions);

        $this->addForeignKey(null, 'call_center_request_history', 'call_center_request_id', 'call_center_request', 'id');
        $this->addForeignKey(null, 'call_center_request_history', 'call_center_id', 'call_center', 'id');
        $this->createIndex(null, 'call_center_request_history', ['operator_id', 'called_at']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('call_center_request_history');
    }
}
