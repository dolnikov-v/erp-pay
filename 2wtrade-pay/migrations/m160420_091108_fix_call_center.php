<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160420_091108_fix_call_center
 */
class m160420_091108_fix_call_center extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('call_center', 'url', $this->string(100)->defaultValue(null) . ' AFTER `name`');
        $this->addColumn('call_center', 'active', $this->smallInteger()->notNull()->defaultValue(0) . ' AFTER `comment`');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('call_center', 'active');
        $this->dropColumn('call_center', 'url');
    }
}
