<?php
use app\components\CustomMigration as Migration;
use app\models\CurrencyRateTask;

class m000000_000015_currency_rate_task extends Migration
{
    public function safeUp()
    {
        $this->createTable('currency_rate_task', [
            'id' => $this->primaryKey(),
            'status' => "ENUM('in_progress', 'done', 'fail') NOT NULL DEFAULT 'in_progress'",
            'query' => $this->string(1000)->notNull(),
            'answer' => $this->string(2000)->defaultValue(null),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('currency_rate_task');
    }
}
