<?php

use app\components\CustomMigration as Migration;
use yii\db\Query as MySqlQuery;
use yii\helpers\ArrayHelper;

/**
 * Class m190409_030821_add_roles_to_widget_role
 */
class m190409_030821_add_roles_to_widget_role extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $widgets = (new MySqlQuery())->select(['id'])
            ->from('widget_type')
            ->column();

        $roles = (new MySqlQuery())->select('item_name')
            ->from('auth_assignment')
            ->where(['like', 'item_name', 'director'])
            ->orWhere(['like', 'item_name', 'curator'])
            ->orWhere(['item_name' => 'auditor'])
            ->distinct()
            ->column();

        $data = [];

        foreach ($widgets as $widget) {
            foreach ($roles as $role) {
                $data[] = [
                    'role' => $role,
                    'type_id' => $widget,
                ];
            }
        }

        if (!empty($data)) {
            $this->delete(
                'widget_role',
                ['IN', 'role', $roles]
            );

            $this->batchInsert(
                'widget_role',
                [
                    'role',
                    'type_id'
                ],
                $data);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

    }
}
