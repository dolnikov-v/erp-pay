<?php
use app\components\PermissionMigration as Migration;
use app\modules\salary\models\Person;

/**
 * Class m180111_090645_person_experience
 */
class m180111_090645_person_experience extends Migration
{

    protected $permissions = [
        'salary.person.view',
    ];

    protected $roles = [
        'admin' => ['salary.person.view'],
        'auditor' => ['salary.person.view'],
        'callcenter.hr' => ['salary.person.view'],
        'callcenter.leader' => ['salary.person.view'],
        'callcenter.manager' => ['salary.person.view'],
        'controller.analyst' => ['salary.person.view'],
        'country.curator' => ['salary.person.view'],
        'project.manager' => ['salary.person.view'],
        'salaryproject.clerk' => ['salary.person.view'],
        'storage.manager' => ['salary.person.view']
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Person::tableName(), 'experience', $this->string());

        $this->delete($this->authManager->itemChildTable, [
            'child' => 'salary.person.delete'
        ]);

        parent::safeUp();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Person::tableName(), 'experience');

        parent::safeDown();
    }
}
