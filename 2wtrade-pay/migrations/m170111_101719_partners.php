<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170111_101719_partners
 */
class m170111_101719_partners extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('partner', [
            'id' => $this->primaryKey(),
            'source' => $this->enum(['adcombo', 'jeempo'])->defaultValue(null),
            'foreign_id' => $this->string(100)->notNull()->unique(),
            'default' => $this->boolean()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $this->tableOptions);

        $this->createIndex(null, 'partner', 'source');
        $this->createIndex(null, 'partner', 'foreign_id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('partner');
    }
}
