<?php
use app\components\PermissionMigration as Migration;

/**
 * Class m180118_055149_invoice_currency
 */
class m180118_055149_invoice_currency extends Migration
{
    protected $permissions = [
        'report.invoice.createinvoice',
        'report.invoice.regenerateinvoice',
        'report.invoice.deleteinvoice',
        'report.invoice.downloadinvoice',
        'report.invoice.currencyrate',
        'report.invoice.index.invoicelist',
        'report.invoice.index',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('invoice_currency', [
            'invoice_id' => $this->integer(),
            'currency_id' => $this->integer(),
            'rate' => $this->decimal(10, 4)
        ], $this->tableOptions);

        $this->addPrimaryKey($this->getPkName('invoice_currency', [
            'invoice_id',
            'currency_id'
        ]), 'invoice_currency', ['invoice_id', 'currency_id']);
        $this->addForeignKey(null, 'invoice_currency', 'invoice_id', 'invoice', 'id', self::CASCADE, self::CASCADE);
        $this->addForeignKey(null, 'invoice_currency', 'currency_id', 'currency', 'id', self::CASCADE, self::CASCADE);
        parent::safeUp();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('invoice_currency');
        parent::safeDown();
    }
}
