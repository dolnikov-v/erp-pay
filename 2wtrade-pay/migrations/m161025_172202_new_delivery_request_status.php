<?php
use app\components\CustomMigration as Migration;

class m161025_172202_new_delivery_request_status extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('delivery_request', 'status', $this->enum(['pending', 'in_progress', 'done', 'error', 'done_error'])->defaultValue('pending'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('delivery_request', 'status', $this->enum(['pending', 'in_progress', 'done', 'error'])->defaultValue('pending'));
    }
}
