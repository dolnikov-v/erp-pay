<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m171009_110230_storage_v2_upd1
 */
class m171009_110230_storage_v2_upd1 extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        CrontabTask::deleteAll(['name' => 'free_storage_product_unaccounted']);

        $this->addColumn('storage_product', 'unlimit', $this->boolean()->defaultValue(false));

        $this->createTable('storage_history_order', [
            'id' => $this->primaryKey(),
            'type' => $this->string(50)->notNull(),
            'product_id' => $this->integer()->notNull(),
            'quantity' => $this->integer()->notNull(),
            'order_id' => $this->integer()->notNull(),
            'country_id' => $this->integer()->notNull(),
            'cron_launched_at' => $this->integer()->defaultValue(0),
        ]);

        $crontabTask = new CrontabTask();
        $crontabTask->name = CrontabTask::TASK_FREE_STORAGE_HISTORY_ORDER;
        $crontabTask->description = 'Склады: движения товаров без штрих-кодов';
        $crontabTask->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = new CrontabTask();
        $crontabTask->name = 'free_storage_product_unaccounted';
        $crontabTask->description = 'Списание товаров, не нашедших партию';
        $crontabTask->save();

        $this->dropColumn('storage_part', 'unlimit');

        $this->dropTable('storage_history_order');

        CrontabTask::deleteAll(['name' => CrontabTask::TASK_FREE_STORAGE_HISTORY_ORDER]);
    }
}
