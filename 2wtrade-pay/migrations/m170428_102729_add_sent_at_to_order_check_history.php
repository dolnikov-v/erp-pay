<?php

use app\components\CustomMigration as Migration;
use app\modules\callcenter\models\CallCenterCheckRequest;

/**
 * Handles adding sent_at to table `order_check_history`.
 */
class m170428_102729_add_sent_at_to_order_check_history extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(CallCenterCheckRequest::tableName(), 'sent_at', $this->integer()->after('cron_launched_at'));
        $this->addColumn(CallCenterCheckRequest::tableName(), 'done_at', $this->integer()->after('sent_at'));

        CallCenterCheckRequest::updateAll(['sent_at' => new \yii\db\Expression('created_at')]);

        $auth = $this->authManager;
        $permission = $auth->createPermission('report.checkundelivery.index');
        $permission->description = 'report.checkundelivery.index';
        $auth->add($permission);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(CallCenterCheckRequest::tableName(), 'sent_at');
        $this->dropColumn(CallCenterCheckRequest::tableName(), 'done_at');

        $auth = $this->authManager;

        $permission = $auth->getPermission('report.checkundelivery.index');
        $auth->remove($permission);
    }
}
