<?php

use app\components\CustomMigration as Migration;
use app\models\Product;
use app\modules\order\models\Order;
use app\modules\order\models\OrderProduct;

class m000000_000024_order_product extends Migration
{
    public function up()
    {
        $this->createTable(OrderProduct::tableName(), [
            'id' => $this->primaryKey(),

            'order_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),

            'price' => $this->double()->notNull(),
            'quantity' => $this->integer()->notNull(),

            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $this->tableOptions);

        $this->addForeignKey(null, OrderProduct::tableName(), 'order_id', Order::tableName(), 'id', self::NO_ACTION, self::NO_ACTION);
        $this->addForeignKey(null, OrderProduct::tableName(), 'product_id', Product::tableName(), 'id', self::NO_ACTION, self::NO_ACTION);
    }

    public function down()
    {
        $this->dropTable(OrderProduct::tableName());
    }
}
