<?php

use yii\helpers\Console;

/**
 * Class m180912_104327_transfer_responses_from_delivery_and_call_center_request
 */
class m180912_104327_transfer_responses_from_delivery_and_call_center_request extends \app\components\CustomMigration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->transferCallCenterRequestResponseToTable();
        $this->transferDeliveryRequestResponseToTable();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->transferCallCenterRequestResponseFromTable();
        $this->transferDeliveryRequestResponseFromTable();
    }

    protected function transferCallCenterRequestResponseFromTable($batchSize = 500)
    {
        $query = (new \yii\db\Query())->from('call_center_request')->select(['id'])->where(['cc_send_response'=> null])->orWhere(['cc_update_response' => null])->orderBy(['id' => SORT_ASC]);

        $requestIds = $query->column();
        $totalCount = count($requestIds);
        $counter = 0;
        Console::startProgress($counter, $totalCount);
        foreach (array_chunk($requestIds, $batchSize) as $chunkIds) {

            foreach ($chunkIds as $requestId) {
                $sendResponse = (new \yii\mongodb\Query())->from('call_center_request_send_response')
                    ->where(['call_center_request_id' => $requestId])->orderBy(['created_at' => SORT_DESC])->one();
                $updateResponse = (new \yii\mongodb\Query())->from('call_center_request_update_response')
                    ->where(['call_center_request_id' => $requestId])->orderBy(['created_at' => SORT_DESC])->one();
                $data = [];
                if ($sendResponse) {
                    $data['cc_send_response'] = (isset($sendResponse['request']) ? '__Request__' . PHP_EOL . $sendResponse['request'] . PHP_EOL . '__Response__' . PHP_EOL : '') . $sendResponse['response'];
                    $data['cc_send_response_at'] = $sendResponse['created_at'];
                }

                if ($updateResponse) {
                    $data['cc_update_response'] = (isset($updateResponse['request']) ? '__Request__' . PHP_EOL . $updateResponse['request'] . PHP_EOL . '__Response__' . PHP_EOL : '') . $updateResponse['response'];
                    $data['cc_update_response_at'] = $updateResponse['created_at'];
                }

                if (!empty($data)) {
                    $this->db->createCommand()->update('call_center_request', $data, ['id' => $requestId]);
                }
            }
            $counter += count($chunkIds);
            Console::updateProgress($counter, $totalCount);
        }

        Console::endProgress("All CallCenterRequest responses was moved from MongoDB" . PHP_EOL);
    }

    protected function transferDeliveryRequestResponseFromTable($batchSize = 500)
    {
        $query = (new \yii\db\Query())->from('delivery_request')->select(['id'])->orderBy(['id' => SORT_ASC]);

        $requestIds = $query->column();
        $totalCount = count($requestIds);
        $counter = 0;
        Console::startProgress($counter, $totalCount);
        foreach (array_chunk($requestIds, $batchSize) as $chunkIds) {
            foreach ($chunkIds as $requestId) {
                $sendResponse = (new \yii\mongodb\Query())->from('delivery_request_send_response')
                    ->where(['delivery_request_id' => $requestId])->orderBy(['created_at' => SORT_DESC])->one();
                $updateResponse = (new \yii\mongodb\Query())->from('delivery_request_update_response')
                    ->where(['delivery_request_id' => $requestId])->orderBy(['created_at' => SORT_DESC])->one();
                $data = [];
                if ($sendResponse) {
                    $data['cs_send_response'] = (isset($sendResponse['request']) ? '__Request__' . PHP_EOL . $sendResponse['request'] . PHP_EOL . '__Response__' . PHP_EOL : '') . $sendResponse['response'];
                    $data['cs_send_response_at'] = $sendResponse['created_at'];
                }

                if ($updateResponse) {
                    $data['last_update_info'] = (isset($updateResponse['request']) ? '__Request__' . PHP_EOL . $updateResponse['request'] . PHP_EOL . '__Response__' . PHP_EOL : '') . $updateResponse['response'];
                    $data['last_update_info_at'] = $updateResponse['created_at'];
                }

                if (!empty($data)) {
                    $this->db->createCommand()->update('delivery_request', $data, ['id' => $requestId]);
                }
            }
            $counter += count($chunkIds);
            Console::updateProgress($counter, $totalCount);
        }

        Console::endProgress("All DeliveryRequest responses was moved from MongoDB" . PHP_EOL);
    }

    protected function transferCallCenterRequestResponseToTable($batchSize = 500)
    {
        $query = (new \yii\db\Query())->from('call_center_request')->select(['id'])
            ->where([
                'is not',
                'cc_send_response',
                null
            ])->orWhere([
                'is not',
                'cc_update_response',
                null
            ])->orderBy(['id' => SORT_ASC]);

        $requestIds = $query->column();
        $totalCount = count($requestIds);
        $counter = 0;
        Console::startProgress($counter, $totalCount);

        foreach (array_chunk($requestIds, $batchSize) as $chunkIds) {
            $chunkIds = array_map('intval', $chunkIds);
            $existedSendResponses = (new \yii\mongodb\Query())->from('call_center_request_send_response')
                ->where(['call_center_request_id' => $chunkIds])
                ->select(['call_center_request_id'])
                ->column();
            $existedUpdateResponses = (new \yii\mongodb\Query())->from('call_center_request_update_response')
                ->where(['call_center_request_id' => $chunkIds])
                ->select(['call_center_request_id'])
                ->column();
            if (!empty($requestIds)) {
                $requestIds = array_diff($chunkIds, array_intersect($existedUpdateResponses, $existedSendResponses));
                $requests = (new \yii\db\Query())->from('call_center_request')->select([
                    'id',
                    'cc_send_response',
                    'cc_send_response_at',
                    'cc_update_response',
                    'cc_update_response_at'
                ])->orderBy(['id' => SORT_ASC])->where(['id' => $requestIds])->all();
                $sendResponsesBatch = [];
                $updateResponsesBatch = [];
                foreach ($requests as $request) {
                    if (!in_array($request['id'], $existedSendResponses) && $request['cc_send_response']) {
                        $sendResponsesBatch[] = [
                            'call_center_request_id' => (int)$request['id'],
                            'response' => $request['cc_send_response'],
                            'created_at' => (int)$request['cc_send_response_at']
                        ];
                    }

                    if (!in_array($request['id'], $existedUpdateResponses) && $request['cc_update_response']) {
                        $updateResponsesBatch[] = [
                            'call_center_request_id' => (int)$request['id'],
                            'response' => $request['cc_update_response'],
                            'created_at' => (int)$request['cc_update_response_at']
                        ];
                    }
                }

                if (!empty($sendResponsesBatch)) {
                    Yii::$app->mongodb->createCommand()
                        ->batchInsert('call_center_request_send_response', $sendResponsesBatch);
                }
                if (!empty($updateResponsesBatch)) {
                    Yii::$app->mongodb->createCommand()
                        ->batchInsert('call_center_request_update_response', $updateResponsesBatch);
                }
            }

            $counter += count($chunkIds);
            Console::updateProgress($counter, $totalCount);
        }

        Console::endProgress("All CallCenterRequest responses was moved to MongoDB" . PHP_EOL);
    }

    protected function transferDeliveryRequestResponseToTable($batchSize = 500)
    {
        $query = (new \yii\db\Query())->from('delivery_request')->select([
            'id'
        ])->where(['is not', 'cs_send_response', null])->orWhere([
            'is not',
            'last_update_info',
            null
        ])->orderBy(['id' => SORT_ASC]);

        $requestIds = $query->column();
        $totalCount = count($requestIds);
        $counter = 0;
        Console::startProgress($counter, $totalCount);

        foreach (array_chunk($requestIds, $batchSize) as $chunkIds) {
            $chunkIds = array_map('intval', $chunkIds);
            $existedSendResponses = (new \yii\mongodb\Query())->from('delivery_request_send_response')
                ->where(['delivery_request_id' => $chunkIds])
                ->select(['delivery_request_id'])
                ->column();
            $existedUpdateResponses = (new \yii\mongodb\Query())->from('delivery_request_update_response')
                ->where(['delivery_request_id' => $chunkIds])
                ->select(['delivery_request_id'])
                ->column();
            $requestIds = array_diff($chunkIds, array_intersect($existedUpdateResponses, $existedSendResponses));
            if (!empty($requestIds)) {
                $requests = (new \yii\db\Query())->from('delivery_request')->select([
                    'id',
                    'cs_send_response',
                    'cs_send_response_at',
                    'last_update_info',
                    'last_update_info_at'
                ])->where(['id' => $requestIds])->orderBy(['id' => SORT_ASC])->all();
                $sendResponsesBatch = [];
                $updateResponsesBatch = [];
                foreach ($requests as $request) {
                    if (!in_array($request['id'], $existedSendResponses) && $request['cs_send_response']) {
                        $sendResponsesBatch[] = [
                            'delivery_request_id' => (int)$request['id'],
                            'response' => $request['cs_send_response'],
                            'created_at' => (int)$request['cs_send_response_at']
                        ];
                    }

                    if (!in_array($request['id'], $existedUpdateResponses) && $request['last_update_info']) {
                        $updateResponsesBatch[] = [
                            'delivery_request_id' => (int)$request['id'],
                            'response' => $request['last_update_info'],
                            'created_at' => (int)$request['last_update_info_at']
                        ];
                    }
                }

                if (!empty($sendResponsesBatch)) {
                    Yii::$app->mongodb->createCommand()
                        ->batchInsert('delivery_request_send_response', $sendResponsesBatch);
                }
                if (!empty($updateResponsesBatch)) {
                    Yii::$app->mongodb->createCommand()
                        ->batchInsert('delivery_request_update_response', $updateResponsesBatch);
                }
            }

            $counter += count($chunkIds);
            Console::updateProgress($counter, $totalCount);
        }

        Console::endProgress("All DeliveryRequest responses was moved to MongoDB" . PHP_EOL);
    }
}
