<?php
use app\components\CustomMigration as Migration;
use app\modules\order\models\OrderStatus;
use yii\db\Query;

/**
 * Class m170926_051452_order_statuses_36_37_38
 */
class m170926_051452_order_statuses_36_37_38 extends Migration
{
    const TEXT = [
        'Отказ (клиент)' => 'Client (refused)',
        'Курьерка (лист отправлен)' => 'Logistics (list sent)',
        'Курьерка (лист принят)' => 'Logistics (list received)',
    ];


    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $query = new Query();

        $this->insert(OrderStatus::tableName(), ['sort' => 36, 'name' => 'Отказ (клиент)', 'comment' => null, 'group' => 'source', 'type' => 'final']);
        $this->insert(OrderStatus::tableName(), ['sort' => 37, 'name' => 'Курьерка (лист отправлен)', 'comment' => null, 'group' => 'logistic', 'type' => 'middle']);
        $this->insert(OrderStatus::tableName(), ['sort' => 38, 'name' => 'Курьерка (лист принят)', 'comment' => null, 'group' => 'logistic', 'type' => 'middle']);


        foreach ($this::TEXT as $ruText => $enText) {

            $is = $query->select('id')
                ->from('i18n_source_message')
                ->where(['category' => 'common', 'message' => $ruText])->one();

            if (!$is) {
                $this->insert('i18n_source_message', ['category' => 'common', 'message' => $ruText]);
                $id = Yii::$app->db->getLastInsertID();
                $this->insert('i18n_message', ['id' => $id, 'language' => 'en-US', 'translation' => $enText]);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete(OrderStatus::tableName(), ['name' => 'Отказ (клиент)']);
        $this->delete(OrderStatus::tableName(), ['name' => 'Курьерка (лист отправлен)']);
        $this->delete(OrderStatus::tableName(), ['name' => 'Курьерка (лист принят)']);

        foreach ($this::TEXT as $ruText => $enText) {
            $this->delete('i18n_source_message', ['category' => 'common', 'message' => $ruText]);
        }
    }
}
