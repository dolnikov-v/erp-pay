<?php
use app\components\CustomMigration as Migration;
use app\modules\order\models\OrderStatus;

/**
 * Class m170530_100500_rename_6_order_status
 */
class m170530_100500_rename_6_order_status extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->update(OrderStatus::tableName(), ['name' => 'Одобрен'], ['id' => 6]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->update(OrderStatus::tableName(), ['name' => 'Колл-центр (одобрен в КЦ)'], ['id' => 6]);
    }
}
