<?php
use app\components\CustomMigration as Migration;

/**
 * Class m180227_072548_add_lead_ts_spawn_index
 */
class m180227_072548_add_lead_ts_spawn_index extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createIndex(
            $this->getIdxName("lead", ["ts_spawn"]),
            "lead",
            ["ts_spawn"]
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropIndex(
            $this->getIdxName("lead", ["ts_spawn"]),
            "lead"
        );
    }
}
