<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m170911_035928_task_clean_cron_process
 */
class m170911_035928_task_clean_cron_process extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = new CrontabTask();
        $crontabTask->name = 'clean_cron_process_list';
        $crontabTask->description = 'Очистка зависших заявок крона';
        $crontabTask->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        CrontabTask::deleteAll(['name' => 'clean_cron_process_list']);
    }
}
