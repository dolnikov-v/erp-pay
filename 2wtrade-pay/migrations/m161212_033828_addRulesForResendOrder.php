<?php
use app\components\CustomMigration as Migration;

/**
 * Class m161212_033828_addRulesForResendOrder
 */
class m161212_033828_addRulesForResendOrder extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}',array(
            'name'=>'order.index.resendseparateorder',
            'type' => '2',
            'description' => 'order.index.resendseparateorder'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'country.curator',
            'child' => 'order.index.resendseparateorder'
        ));


        $this->addColumn('order', 'duplicate_order_id', $this->integer()->defaultValue(null) . ' AFTER `id`'); 

        $this->addForeignKey(null, 'order', 'duplicate_order_id', 'order', 'id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->authManager->itemChildTable, ['child' => 'order.index.resendseparateorder', 'parent' => 'country.curator']);
        $this->delete('{{%auth_item}}', ['name' => 'order.index.resendseparateorder']);
        $this->dropColumn('order', 'duplicate_order_id');
    }
}
