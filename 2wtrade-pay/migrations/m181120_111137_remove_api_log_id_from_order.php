<?php
use app\components\CustomMigration as Migration;

/**
 * Class m181120_111137_remove_api_log_id_from_order
 */
class m181120_111137_remove_api_log_id_from_order extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropForeignKey($this->getFkName('order', 'api_log_id'), 'order', 'api_log_id');
        $this->dropColumn('order', 'api_log_id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->addColumn('order', 'api_log_id', $this->integer(16)->after('source_confirmed'));
        $this->addForeignKey(null, 'order', 'api_log_id', 'api_log', 'id');
    }
}
