<?php
use app\components\CustomMigration as Migration;
use app\models\User;
use app\models\UserDelivery;
use app\modules\delivery\models\Delivery;


/**
 * Class m170124_045640_add_role_courier_user_implant
 */
class m170124_045640_add_role_courier_user_implant extends Migration
{

    const AUTH_CHILDS = [
        'home.index.index',
        'order.index.index',
        'order.index.view',
        'order.index.generatelist',
        'order.list.index',
        'order.list.downloadlabel',
        'order.list.buildexcel',
        'order.list.buildinvoice',
        'order.list.download',
        'order.list.downloadinvoice',
        'order.list.downloadexcel',
        'order.list.downloadticket',
        'order.list.details',
        'order.list.deletelist',
        'order.list.deleteorder',
        'order.list.label',
        'order.list.send',
        'old_orders_access',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}', [
            'name'=> 'implant',
            'type' => '1',
            'description' => 'Пользователь курьерской службы',
            'created_at' => time(),
            'updated_at' => time()
        ]);
        foreach (self::AUTH_CHILDS as $child) {
            $this->insert($this->authManager->itemChildTable, [
                'parent' => 'implant',
                'child' => $child
            ]);
        }
        $this->createTable(UserDelivery::tableName(), [
            'user_id' => $this->integer()->notNull(),
            'delivery_id' => $this->integer()->notNull(),
            'active' => $this->boolean()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $this->tableOptions);
        $this->addPrimaryKey(null, UserDelivery::tableName(), ['user_id', 'delivery_id']);
        $this->addForeignKey(null, UserDelivery::tableName(), 'user_id', User::tableName(), 'id', Migration::CASCADE, Migration::CASCADE);
        $this->addForeignKey(null, UserDelivery::tableName(), 'delivery_id', Delivery::tableName(), 'id', Migration::CASCADE, Migration::CASCADE);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach (self::AUTH_CHILDS as $child) {
            $this->delete($this->authManager->itemChildTable, [
                'parent' => 'implant',
                'child' => $child,
            ]);
        }
        $this->delete('{{%auth_item}}', ['name' => 'implant']);

        $this->dropTable(UserDelivery::tableName());
    }
}
