<?php

use app\components\CustomMigration as Migration;
use app\modules\salary\models\Office;
use app\models\Notification;

/**
 * Handles adding auto_penalty to table `office`.
 */
class m171017_094612_add_auto_penalty_to_office extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Office::tableName(), 'auto_penalty', $this->integer(1)->defaultValue(0));

        $notification = Notification::findOne(['trigger' => Notification::TRIGGER_MAKE_AUTO_PENALTY]);
        if (!($notification instanceof Notification)) {
            $notification = new Notification([
                'description' => 'A penalty was given to an employee {person} from the office {office} for {penalty} {date}',
                'trigger' => Notification::TRIGGER_MAKE_AUTO_PENALTY,
                'type' => Notification::TYPE_INFO,
                'group' => Notification::GROUP_SYSTEM,
                'active' => Notification::ACTIVE,
            ]);
            $notification->save(false);
        } else {
            $notification->description = 'A penalty was given to an employee {person} from the office {office} for {penalty} {date}';
            $notification->save(false);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Office::tableName(), 'auto_penalty');

        $notification = Notification::findOne(['trigger' => Notification::TRIGGER_MAKE_AUTO_PENALTY]);
        if (($notification instanceof Notification)) {
            $notification->description = 'Выписан автоматический штраф сотруднику {person} за опоздание {date}';
            $notification->save(false);
        }
    }
}
