<?php
use app\components\CustomMigration as Migration;

/**
 * Class m180302_032227_translate_cannot_delete_delivery_service_message
 */
class m180302_032227_translate_cannot_delete_delivery_service_message extends Migration
{
    private static $translations = [
        "Служба доставки использовалась. Она не может быть удалена." => "The delivery service was used. It can't be deleted",
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        foreach(self::$translations as $ruText => $enText) {
            $this->insert('i18n_source_message', ['category' => 'common', 'message' => $ruText]);
            $id = Yii::$app->db->getLastInsertID();
            $this->insert('i18n_message', ['id' => $id, 'language' => 'en-US', 'translation' => $enText]);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach (self::$translations as $ruText => $enText) {
            $this->delete('i18n_source_message', ['category' => 'common', 'message' => $ruText]);
        }
    }
}
