<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160629_085647_add_mail_field_for_user
 */
class m160629_085647_add_mail_field_for_user extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('user', 'email', $this->string()->after('password'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'email');
    }
}
