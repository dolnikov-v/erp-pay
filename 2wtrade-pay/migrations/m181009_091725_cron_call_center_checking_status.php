<?php
use app\components\CustomMigration as Migration;
use yii\db\Query;

/**
 * Class m181009_091725_cron_call_center_checking_status
 */
class m181009_091725_cron_call_center_checking_status extends Migration
{

    const name = 'call_center_checking_status';
    const description = 'Запрос статусов обзвона в КЦ';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $cronTaskId = (new Query())->select('id')
            ->from('crontab_task')
            ->where(['name' => self::name])
            ->scalar();

        $data = [
            'name' => self::name,
            'description' => self::description,
            'updated_at' => time(),
        ];

        if (!$cronTaskId) {
            $this->insert('crontab_task', $data);
        } else {
            $data['created_at'] = time();
            $this->update('crontab_task', $data, ['id' => $cronTaskId]);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete('crontab_task', ['name' => self::name]);
    }
}
