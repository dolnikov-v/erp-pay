<?php
use app\components\PermissionMigration as Migration;
use app\modules\order\models\OrderFinancePretension;

/**
 * Class m180815_094624_ERP_706_disabled_options_for_pretensions
 */
class m180815_094624_ERP_706_disabled_options_for_pretensions extends Migration
{
    protected $permissions = ['deliveryreport.report.removepretension'];

    protected $roles = [
        'deliveryreport.clerk' => ['deliveryreport.report.removepretension'],
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(OrderFinancePretension::tableName(), 'is_delete', $this->boolean()->defaultValue(0));
        parent::safeUp();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(OrderFinancePretension::tableName(), 'is_delete');
        parent::safeDown();
    }
}
