<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160505_102817_order_invoice
 */
class m160505_102817_order_invoice extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropColumn('order_logistic_list', 'invoice');

        $this->createTable('order_logistic_list_invoice', [
            'id' => $this->primaryKey(),
            'list_id' => $this->integer()->notNull(),
            'order_id' => $this->integer()->notNull(),
            'template' => $this->string(20)->notNull(),
            'format' => $this->string(20)->notNull(),
            'invoice' => $this->string(100)->notNull(),
            'invoice_local' => $this->string(100)->notNull(),
            'user_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(null, 'order_logistic_list_invoice', 'list_id', 'order_logistic_list', 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, 'order_logistic_list_invoice', 'order_id', 'order', 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, 'order_logistic_list_invoice', 'user_id', 'user', 'id', self::CASCADE, self::RESTRICT);
        $this->createIndex(null, 'order_logistic_list_invoice', 'template');
        $this->createIndex(null, 'order_logistic_list_invoice', 'format');

        $this->createTable('order_logistic_list_invoice_archive', [
            'id' => $this->primaryKey(),
            'list_id' => $this->integer()->notNull(),
            'template' => $this->string(20)->notNull(),
            'format' => $this->string(20)->notNull(),
            'archive' => $this->string(100)->notNull(),
            'archive_local' => $this->string(100)->notNull(),
            'user_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(null, 'order_logistic_list_invoice_archive', 'list_id', 'order_logistic_list', 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, 'order_logistic_list_invoice_archive', 'user_id', 'user', 'id', self::CASCADE, self::RESTRICT);
        $this->createIndex(null, 'order_logistic_list_invoice_archive', 'template');
        $this->createIndex(null, 'order_logistic_list_invoice_archive', 'format');
        $this->createIndex(null, 'order_logistic_list_invoice_archive', 'archive');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('order_logistic_list_invoice_archive');
        $this->dropTable('order_logistic_list_invoice');

        $this->addColumn('order_logistic_list', 'invoice', $this->string(100)->defaultValue(null) . ' AFTER `list`');
    }
}
