<?php

use app\components\CustomMigration as Migration;

/**
 * Handles adding permission `order.index.deleteordersfromlists` for PM & CC users.
 */
class m170303_132817_add_permission_for_mass_delete_orders_button extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}',array(
            'name'=>'order.index.deleteordersfromlists',
            'type' => '2',
            'description' => 'order.index.deleteordersfromlists',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'country.curator',
            'child' => 'order.index.deleteordersfromlists'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'project.manager',
            'child' => 'order.index.deleteordersfromlists'
        ));

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->authManager->itemChildTable, ['child' => 'order.index.deleteordersfromlists', 'parent' => 'country.curator']);
        $this->delete($this->authManager->itemChildTable, ['child' => 'order.index.deleteordersfromlists', 'parent' => 'project.manager']);
        $this->delete('{{%auth_item}}', ['name' => 'order.index.deleteordersfromlists']);
    }
}
