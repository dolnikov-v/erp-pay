<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m160928_093733_new_crontab_task
 */
class m161026_180402_new_crontab_task_report_delivered_status_anomalies extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = new CrontabTask();
        $crontabTask->name = 'delivery_anomalies_daily_notifications';
        $crontabTask->description = 'Ежедневные нотификации об аномалиях в статусах доставки';
        $crontabTask->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = CrontabTask::find()
            ->byName('delivery_anomalies_daily_notifications')
            ->one();

        if ($crontabTask) {
            $crontabTask->delete();
        }
    }
}
