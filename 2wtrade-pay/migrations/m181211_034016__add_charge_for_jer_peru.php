<?php
use app\components\CustomMigration as Migration;

/**
 * Class m181211_034016__add_charge_for_jer_peru
 */
class m181211_034016__add_charge_for_jer_peru extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('delivery_charges_calculator', [
            'name' => 'JerCurrierPE',
            'class_path' => 'app\modules\delivery\components\charges\jercurrier\JerCurrierPE',
            'active' => 1,
            'created_at' => time(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete('delivery_charges_calculator', ['name' => 'JerCurrierPE']);
    }
}
