<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m180112_021745_calls_oper_products_tasks
 */
class m180112_021745_calls_oper_products_tasks extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = CrontabTask::findOne(['name' => 'get_call_center_user_order']);
        if (!($crontabTask instanceof CrontabTask)) {
            $crontabTask = new CrontabTask();
            $crontabTask->name = 'get_call_center_user_order';
            $crontabTask->description = 'Получение кол-ва звонков по заказам';
            $crontabTask->save();
        }

        $crontabTask = CrontabTask::findOne(['name' => 'get_call_center_product_call']);
        if (!($crontabTask instanceof CrontabTask)) {
            $crontabTask = new CrontabTask();
            $crontabTask->name = 'get_call_center_product_call';
            $crontabTask->description = 'Получение кол-ва звонков по продуктам';
            $crontabTask->save();
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = CrontabTask::findOne(['name' => 'get_call_center_user_order']);
        if ($crontabTask instanceof CrontabTask) {
            $crontabTask->delete();
        }

        $crontabTask = CrontabTask::findOne(['name' => 'get_call_center_product_call']);
        if ($crontabTask instanceof CrontabTask) {
            $crontabTask->delete();
        }
    }
}
