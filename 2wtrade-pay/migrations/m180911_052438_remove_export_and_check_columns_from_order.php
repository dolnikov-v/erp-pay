<?php
use app\components\CustomMigration as Migration;

/**
 * Class m180911_052438_remove_export_and_check_columns_from_order
 */
class m180911_052438_remove_export_and_check_columns_from_order extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $sql = <<<SQL
      ALTER TABLE `order` DROP COLUMN `export_history`, DROP COLUMN `check`, DROP COLUMN `date_check`, DROP COLUMN `call_center_check_type`;
SQL;
        $this->execute($sql);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $sql = <<<SQL
      ALTER TABLE `order` ADD COLUMN `export_history` TEXT AFTER `report_status_id`, ADD COLUMN `date_check` INTEGER(11) NOT NULL, ADD COLUMN `check` ENUM('pending', 'done'), ADD COLUMN `call_center_check_type` VARCHAR(50);
SQL;
        $this->execute($sql);
    }
}
