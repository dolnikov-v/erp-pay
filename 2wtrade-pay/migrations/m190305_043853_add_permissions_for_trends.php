<?php
use app\components\PermissionMigration as Migration;

/**
 * Class m190305_043853_add_permissions_for_trends
 */
class m190305_043853_add_permissions_for_trends extends Migration
{
    protected $permissions = [
        'report.trends.lead',
        'report.trends.approve',
        'report.trends.buyout',
        'rating.ratingproduct.index',
        'rating.ratingbuyoutcheck.index'
    ];

    protected $roles = [
        'admin' => [
            'report.trends.lead',
            'report.trends.approve',
            'report.trends.buyout',
            'rating.ratingproduct.index',
            'rating.ratingbuyoutcheck.index',
            'report.forecastlead.index'
        ],
        'controller.analyst' => [
            'report.trends.lead',
            'report.trends.approve',
            'report.trends.buyout',
            'rating.ratingproduct.index',
            'rating.ratingbuyoutcheck.index',
            'report.forecastlead.index'
        ],
        'country.curator' => [
            'report.trends.lead',
            'report.trends.approve',
            'report.trends.buyout',
            'rating.ratingproduct.index',
            'rating.ratingbuyoutcheck.index',
            'report.forecastlead.index'
        ],
        'finance.director' => [
            'report.trends.lead',
            'report.trends.approve',
            'report.trends.buyout',
            'rating.ratingproduct.index',
            'rating.ratingbuyoutcheck.index',
            'report.forecastlead.index'
        ],
        'logist' => [
            'report.trends.lead',
            'report.trends.approve',
            'report.trends.buyout',
            'rating.ratingproduct.index',
            'rating.ratingbuyoutcheck.index',
            'report.forecastlead.index'
        ],
        'operational.director' => [
            'report.trends.lead',
            'report.trends.approve',
            'report.trends.buyout',
            'rating.ratingproduct.index',
            'rating.ratingbuyoutcheck.index',
            'report.forecastlead.index'
        ],
        'project.manager' => [
            'report.trends.lead',
            'report.trends.approve',
            'report.trends.buyout',
            'rating.ratingproduct.index',
            'rating.ratingbuyoutcheck.index',
            'report.forecastlead.index'
        ],
        'support.manager' => [
            'report.trends.lead',
            'report.trends.approve',
            'report.trends.buyout',
            'rating.ratingproduct.index',
            'rating.ratingbuyoutcheck.index',
            'report.forecastlead.index'
        ],
        'technical.director' => [
            'report.trends.lead',
            'report.trends.approve',
            'report.trends.buyout',
            'rating.ratingproduct.index',
            'rating.ratingbuyoutcheck.index',
            'report.forecastlead.index'
        ]
    ];
}
