<?php
use app\components\PermissionMigration as Migration;

/**
 * Class m181214_062306_add_new_perms_for_finance_fact
 */
class m181214_062306_add_new_perms_for_finance_fact extends Migration
{
    protected $permissions = [
        'order.financefact.index',
        'order.financefact.export',
    ];

    protected $roles = [];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $query = new \yii\db\Query();
        $permissions = $query->from($this->authManager->itemChildTable)->where([
            'child' => 'report.debts.index'
        ])->all();
        foreach ($permissions as $permission) {
            $this->roles[$permission['parent']] = $this->permissions;
        }

        parent::safeUp();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        parent::safeDown();
    }
}
