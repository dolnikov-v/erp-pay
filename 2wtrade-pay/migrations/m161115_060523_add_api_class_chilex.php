<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryApiClass;

/**
 * Class m161115_060523_add_api_class_chilex
 */
class m161115_060523_add_api_class_chilex extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $class = new DeliveryApiClass();
        $class->name = 'chilexpressApi';
        $class->class_path = '/chilexpress-api/src/chilexpressApi.php';
        $class->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        DeliveryApiClass::find()
            ->where([
                'name' => 'chilexpressApi',
                'class_path' => '/chilexpress-api/src/chilexpressApi.php'
            ])
            ->one()
            ->delete();
    }
}