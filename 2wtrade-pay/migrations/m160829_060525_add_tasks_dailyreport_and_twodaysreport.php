<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m160829_060525_add_tasks_dailyreport_and_twodaysreport
 */
class m160829_060525_add_tasks_dailyreport_and_twodaysreport extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = new CrontabTask();
        $crontabTask->name = 'report_order_daily';
        $crontabTask->description = 'Отчет по ежедневной сверке заказов';
        $crontabTask->save();

        $crontabTask = new CrontabTask();
        $crontabTask->name = 'report_order_two_days_error';
        $crontabTask->description = 'Отчет о заказах в ошибке более 1-го дня';
        $crontabTask->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTasks = CrontabTask::find()
            ->where(['in', 'name', ['report_order_daily', 'report_order_two_days_error']])
            ->all();

        foreach ($crontabTasks as $crontabTask) {
            $crontabTask->delete();
        }
    }
}
