<?php
use app\models\Product;
use app\components\CustomMigration;

class m000000_000016_product extends CustomMigration
{
    public function safeUp()
    {
        $this->createTable(Product::tableName(), [
            'id' => $this->primaryKey(),
            'name' => $this->string(128)->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $this->tableOptions);

        foreach ($this->productFixture() as $key => $fixture) {
            $country = new Product($fixture);
            $country->save(false);
        }
    }

    public function safeDown()
    {
        $this->dropTable(Product::tableName());
    }

    /**
     * @return array
     */
    private function productFixture()
    {
        return [
            ['name' => 'Goji cream'],
            ['name' => 'Titan Gel'],
            ['name' => 'Goji berry'],
            ['name' => 'Don Vigaron'],
            ['name' => 'Brutalin'],
            ['name' => 'Vigaron hammer'],
            ['name' => 'Green coffee'],
            ['name' => 'Power balance'],
            ['name' => 'Valgustep'],
            ['name' => 'Cruz de Toretto'],
            ['name' => 'Corazon de Ocean'],
            ['name' => 'Provocative Gel'],
            ['name' => 'Hammer of Thor'],
            ['name' => 'Carrot mask'],
            ['name' => 'White mask'],
            ['name' => 'Snore clip'],
            ['name' => 'Antihrap'],
            ['name' => 'Eye mask'],
            ['name' => 'Teeth whitening'],
            ['name' => 'Turboslim'],
            ['name' => 'Goji Cream Hendel'],
            ['name' => 'Miracle glow'],
        ];
    }
}
