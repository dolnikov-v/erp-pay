<?php
use app\components\CustomMigration as Migration;

/**
 * Class m171219_054428_table_delivery_contract
 */
class m171219_054428_table_delivery_contract extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('delivery_contract', [
            'id' => $this->primaryKey(),
            'date_from' => $this->date(),
            'date_to' => $this->date(),
            'charges_values' => $this->text(),
            'delivery_id' => $this->integer()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);
        $this->createTable('delivery_contract_file', [
            'id' => $this->primaryKey(),
            'file_name' => $this->string(100),
            'delivery_contract_id' => $this->integer()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);

        $this->addForeignKey(null, 'delivery_contract', 'delivery_id', 'delivery', 'id', self::CASCADE, self::CASCADE);
        $this->addForeignKey(null, 'delivery_contract_file', 'delivery_contract_id', 'delivery_contract', 'id', self::CASCADE, self::CASCADE);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('delivery_contract_file');
        $this->dropTable('delivery_contract');
    }
}
