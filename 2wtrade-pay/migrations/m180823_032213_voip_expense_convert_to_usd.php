<?php
use app\components\CustomMigration as Migration;
use app\models\VoipExpense;

/**
 * Class m180823_032213_voip_expense_convert_to_usd
 */
class m180823_032213_voip_expense_convert_to_usd extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(VoipExpense::tableName(), 'amount_usd', $this->float()->defaultValue(null)->after('amount'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(VoipExpense::tableName(), 'amount_usd');
    }
}
