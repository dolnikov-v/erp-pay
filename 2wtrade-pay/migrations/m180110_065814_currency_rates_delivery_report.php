<?php
use app\components\CustomMigration as Migration;

/**
 * Class m180110_065814_currency_rates_delivery_report
 */
class m180110_065814_currency_rates_delivery_report extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('delivery_report', 'currency_rates', $this->string(200));
        $this->addColumn('delivery_report', 'currency_id', $this->integer());
        $this->addColumn('delivery_report', 'currency_date', $this->date());

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('delivery_report', 'currency_rates');
        $this->dropColumn('delivery_report', 'currency_id');
        $this->dropColumn('delivery_report', 'currency_date');
    }
}
