<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160516_121004_add_user_unique
 */
class m160516_121004_add_user_unique extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createIndex(null, 'user', 'username', true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropIndex($this->getUniName('user', 'username'), 'user');
    }
}
