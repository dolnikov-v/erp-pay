<?php

use app\components\CustomMigration as Migration;
use app\modules\catalog\models\RequisiteDelivery;

/**
 * Handles the creation for table `table_requisite_delivery`.
 */
class m180328_120732_create_table_requisite_delivery extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable(RequisiteDelivery::tableName(), [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'address' => $this->string()->notNull(),
            'city' => $this->string()->notNull(),
            'bank_address' => $this->string()->notNull(),
            'beneficiary_bank' => $this->string()->notNull(),
            'bank_account' => $this->string()->notNull(),
            'swift_code' => $this->string()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),

        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable(RequisiteDelivery::tableName());
    }
}
