<?php
use app\components\CustomMigration as Migration;
use yii\db\Query;
use app\modules\salary\models\MonthlyStatementData;

/**
 * Class m170719_043546_call_center_monthly_statement_data
 */
class m170719_043546_call_center_monthly_statement_data extends Migration
{
    const ROLES = [
        'finance.director',
        'callcenter.hr',
        'callcenter.manager',
        'project.manager'
    ];

    const RULES = [
        'callcenter.monthlystatementdata.delete',
        'callcenter.monthlystatementdata.edit',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        $this->renameColumn(MonthlyStatementData::tableName(), 'fio', 'name');

        $query = new Query();

        foreach (self::RULES as $rule) {

            $is_rule = $query->select('*')->from($this->authManager->itemTable)
                ->where(['name' => $rule, 'type' => 2])
                ->one();

            if (!$is_rule) {
                $this->insert($this->authManager->itemTable, [
                    'name' => $rule,
                    'description' => $rule,
                    'type' => 2,
                    'created_at' => time(),
                    'updated_at' => time()]);
            }

            foreach (self::ROLES as $role) {
                $is_can = $query->select('*')->from($this->authManager->itemChildTable)->where([
                    'parent' => $role,
                    'child' => $rule
                ])->one();

                if (!$is_can) {
                    $this->insert($this->authManager->itemChildTable, [
                        'parent' => $role,
                        'child' => $rule
                    ]);
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->renameColumn(MonthlyStatementData::tableName(), 'name', 'fio');

        foreach (self::RULES as $rule) {
            foreach (self::ROLES as $role) {
                $this->delete($this->authManager->itemChildTable, [
                    'parent' => $role,
                    'child' => $rule
                ]);
            }
        }
    }
}
