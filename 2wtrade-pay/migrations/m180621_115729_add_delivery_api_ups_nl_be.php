<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryApiClass;

/**
 * Class m180621_115729_add_delivery_api_ups_nl_be
 */
class m180621_115729_add_delivery_api_ups_nl_be extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $apiClass = new DeliveryApiClass();
        $apiClass->name = 'UPS-NLD';
        $apiClass->class_path = '/ups-api/src/NLD/upsApiNLD.php';
        $apiClass->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        DeliveryApiClass::find()
            ->where([
                'name' => 'UPS-NLD',
            ])
            ->one()
            ->delete();
    }
}
