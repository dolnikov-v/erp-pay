<?php
use app\components\PermissionMigration as Migration;

/**
 * Class m171120_063246_call_center_check_limits_view
 */
class m171120_063246_call_center_check_limits_view extends Migration
{
    protected $roles = [
        'controller.analyst' => [
            'salary.office.index',
            'salary.person.index',
            'salary.penalty.index',
            'salary.bonus.index',
            'salary.holiday.index',
            'report.timesheet.index',
        ],
        'country.curator' => [
            'salary.office.index',
            'salary.person.index',
            'salary.penalty.index',
            'salary.bonus.index',
            'salary.holiday.index',
            'report.timesheet.index',
        ],
    ];
}
