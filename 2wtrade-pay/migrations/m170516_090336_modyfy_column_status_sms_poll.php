<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170516_090336_modyfy_column_status_sms_poll
 */
class m170516_090336_modyfy_column_status_sms_poll extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('sms_poll_history', 'status', $this->string(50),  'new');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('sms_poll_history', 'status', "ENUM('error','success') NOT NULL DEFAULT 'error'");
    }
}
