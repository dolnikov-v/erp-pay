<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;
use app\models\Notification;

/**
 * Class m161026_065634_new_task_no_products_for_order
 */
class m161026_065634_new_task_no_products_for_order extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = new CrontabTask();
        $crontabTask->name = 'no_products_for_order';
        $crontabTask->description = 'Отправка оповещения о товарах, которые требуются заказам';
        $crontabTask->save();

        $model = new Notification([
            'description' => '{text}.',
            'trigger' => 'no.products.for.order',
            'type' => Notification::TYPE_WARNING,
            'group' => Notification::GROUP_REPORT,
            'active' => Notification::ACTIVE,
        ]);

        $model->save(false);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = CrontabTask::find()
            ->byName('no_products_for_order')
            ->one();

        if ($crontabTask) {
            $crontabTask->delete();
        }

        $model = Notification::findOne(['trigger' => 'no.products.for.order']);

        if ($model instanceof Notification) {
            $model->delete();
        }
    }
}
