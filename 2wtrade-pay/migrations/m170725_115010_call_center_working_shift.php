<?php
use app\components\CustomMigration as Migration;
use app\modules\salary\models\Person;

/**
 * Class m170725_115010_call_center_working_shift
 */
class m170725_115010_call_center_working_shift extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropColumn(Person::tableName(), 'shift_time');
        $this->dropColumn(Person::tableName(), 'lunch_time');
        $this->dropColumn(Person::tableName(), 'working_mon');
        $this->dropColumn(Person::tableName(), 'working_tue');
        $this->dropColumn(Person::tableName(), 'working_wed');
        $this->dropColumn(Person::tableName(), 'working_thu');
        $this->dropColumn(Person::tableName(), 'working_fri');
        $this->dropColumn(Person::tableName(), 'working_sat');
        $this->dropColumn(Person::tableName(), 'working_sun');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->addColumn(Person::tableName(), 'shift_time', $this->string());
        $this->addColumn(Person::tableName(), 'lunch_time', $this->string());
        $this->addColumn(Person::tableName(), 'working_mon', $this->string());
        $this->addColumn(Person::tableName(), 'working_tue', $this->string());
        $this->addColumn(Person::tableName(), 'working_wed', $this->string());
        $this->addColumn(Person::tableName(), 'working_thu', $this->string());
        $this->addColumn(Person::tableName(), 'working_fri', $this->string());
        $this->addColumn(Person::tableName(), 'working_sat', $this->string());
        $this->addColumn(Person::tableName(), 'working_sun', $this->string());
    }
}
