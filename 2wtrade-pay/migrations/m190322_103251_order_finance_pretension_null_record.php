<?php
use app\components\CustomMigration as Migration;

/**
 * Class m190322_103251_order_finance_pretension_null_record
 */
class m190322_103251_order_finance_pretension_null_record extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('order_finance_pretension', 'delivery_report_record_id', $this->integer()->null());
        $this->alterColumn('order_finance_pretension', 'delivery_report_id', $this->integer()->null());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('order_finance_pretension', 'delivery_report_record_id', $this->integer()->notNull());
        $this->alterColumn('order_finance_pretension', 'delivery_report_id', $this->integer()->notNull());
    }
}
