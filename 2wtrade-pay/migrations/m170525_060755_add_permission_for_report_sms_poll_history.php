<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170525_060755_add_permission_for_report_sms_poll_history
 */
class m170525_060755_add_permission_for_report_sms_poll_history extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'country.curator',
            'child' => 'report.smspollhistory.index'
        ));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->authManager->itemChildTable, ['child' => 'report.smspollhistory.index', 'parent' => 'country.curator']);
    }
}
