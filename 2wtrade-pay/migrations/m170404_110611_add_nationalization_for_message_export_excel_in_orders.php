<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170404_110611_add_nationalization_for_message_export_excel_in_orders
 */
class m170404_110611_add_nationalization_for_message_export_excel_in_orders extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('i18n_source_message', ['category' => 'common', 'message' => 'Превышено количество строк XLS файла (более 65 000). Используйте формат CSV.']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {


    }
}
