<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160908_084935_drop_subanswers
 */
class m160908_084935_drop_subanswers extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropTable('crontab_task_log_subanswer');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->createTable('crontab_task_log_subanswer', [
            'id' => $this->primaryKey(),
            'answer_id' => $this->integer()->notNull(),
            'data' => $this->text(),
            'answer' => $this->text(),
            'file_answer' => $this->string()->defaultValue(null),
            'status' => $this->enum(['fail', 'success'])->defaultValue('fail'),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(null, 'crontab_task_log_subanswer', 'answer_id', 'crontab_task_log_answer', 'id', self::CASCADE, self::RESTRICT);
        $this->createIndex(null, 'crontab_task_log_subanswer', 'status');
    }
}
