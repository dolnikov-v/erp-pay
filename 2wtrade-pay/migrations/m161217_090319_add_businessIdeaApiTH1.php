<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryApiClass;

/**
 * Class m161217_090319_add_businessIdeaApiTH1
 */
class m161217_090319_add_businessIdeaApiTH1 extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $class = new DeliveryApiClass();
        $class->name = 'businessideaApiTH1';
        $class->class_path = '/businessidea-api/src/TH1/businessideaApiTH1.php';
        $class->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        DeliveryApiClass::find()
            ->where([
                'name' => 'businessideaApiTH1',
                'class_path' => '/businessidea-api/src/TH1/businessideaApiTH1.php'
            ])
            ->one()
            ->delete();
    }
}
