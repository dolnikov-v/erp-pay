<?php
use app\components\CustomMigration as Migration;

/**
 * Class m190115_034915_add_japan_post_api_class
 */
class m190115_034915_add_japan_post_api_class extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('delivery_api_class', [
            'name' => 'JapanPostApi',
            'class_path' => '/japan-post-api/src/japanPostApi.php',
            'active' => 1,
            'can_tracking' => 0,
            'use_amazon_queue' => 0,
            'created_at' => time(),
            'updated_at' => time(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete('delivery_api_class', ['name' => 'JapanPostApi']);
    }
}
