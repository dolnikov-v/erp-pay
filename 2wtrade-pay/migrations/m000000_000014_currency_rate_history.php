<?php
use app\components\CustomMigration as Migration;
use app\models\Currency;
use app\models\CurrencyRateHistory;

class m000000_000014_currency_rate_history extends Migration
{
    public function safeUp()
    {
        $this->createTable(CurrencyRateHistory::tableName(), [
            'id' => $this->primaryKey(),
            'currency_id' => $this->integer()->notNull(),
            'rate' => $this->decimal(10, 4)->defaultValue(null),
            'ask' => $this->decimal(10, 4)->defaultValue(null),
            'bid' => $this->decimal(10, 4)->defaultValue(null),
            'created_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(null, CurrencyRateHistory::tableName(), 'currency_id', Currency::tableName(), 'id', self::CASCADE, self::RESTRICT);
    }

    public function safeDown()
    {
        $this->dropTable(CurrencyRateHistory::tableName());
    }
}
