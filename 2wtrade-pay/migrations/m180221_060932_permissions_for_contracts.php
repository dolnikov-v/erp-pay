<?php
use app\components\PermissionMigration as Migration;

/**
 * Class m180221_060932_permissions_for_contracts
 */
class m180221_060932_permissions_for_contracts extends Migration
{
    protected $permissions = [
        'delivery.control.contractview'
    ];

    protected $roles = [
        'country.curator' => [
            'delivery.control.contractview'
        ],
        'controller.analyst' => [
            'delivery.control.contractview'
        ],
        'auditor' => [
            'delivery.control.contractview'
        ],
        'admin' => [
            'delivery.control.contractview'
        ],
        'project.manager' => [
            'delivery.control.contractview'
        ]
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->delete($this->authManager->itemChildTable, [
            'parent' => 'auditor',
            'child' => 'delivery.control.contractedit'
        ]);
        parent::safeUp();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->insert($this->authManager->itemChildTable, [
            'parent' => 'auditor',
            'child' => 'delivery.control.contractedit'
        ]);
        parent::safeDown();
    }
}
