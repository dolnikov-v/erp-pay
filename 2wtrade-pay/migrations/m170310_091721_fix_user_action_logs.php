<?php

use app\components\CustomMigration as Migration;

class m170310_091721_fix_user_action_logs extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('user_action_log', 'get_data', $this->text());
        $this->alterColumn('user_action_log', 'post_data', $this->text());
    }

    public function safeDown()
    {
        $this->alterColumn('user_action_log', 'post_data', $this->string(1024));
        $this->alterColumn('user_action_log', 'get_data', $this->string(1024));
    }
}
