<?php
use app\components\CustomMigration as Migration;
use app\modules\salary\models\Office;
use app\modules\callcenter\models\CallCenter;
use app\modules\callcenter\models\CallCenterToOffice;



/**
 * Class m170712_023659_call_center_2_office
 */
class m170712_023659_call_center_2_office extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        $this->createTable(CallCenterToOffice::tableName(), [
            'id' => $this->primaryKey(),
            'call_center_id' => $this->integer(),
            'office_id' => $this->integer()
        ]);

        $this->addForeignKey(null, CallCenterToOffice::tableName(), 'call_center_id', CallCenter::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, CallCenterToOffice::tableName(), 'office_id', Office::tableName(), 'id', self::CASCADE, self::RESTRICT);

        $arrayForInsert = [];
        $callCenters = Yii::$app->db->createCommand('SELECT * FROM call_center')->queryAll();
        foreach ($callCenters as $callCenter) {
            $arrayForInsert[] = [
                'call_center_id' => $callCenter['id'],
                'office_id' => $callCenter['office_id']
            ];
        }

        Yii::$app->db->createCommand()->batchInsert(
            CallCenterToOffice::tableName(),
            [
                'call_center_id',
                'office_id',
            ],
            $arrayForInsert
        )->execute();

        $this->dropForeignKey('fk_call_center_office_id', CallCenter::tableName());
        $this->dropColumn(CallCenter::tableName(), 'office_id');

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable(CallCenterToOffice::tableName());
        $this->addColumn(CallCenter::tableName(), 'office_id', $this->integer());
        $this->addForeignKey('fk_call_center_office_id', CallCenter::tableName(), 'office_id', Office::tableName(), 'id', self::SET_NULL, self::NO_ACTION);
    }
}
