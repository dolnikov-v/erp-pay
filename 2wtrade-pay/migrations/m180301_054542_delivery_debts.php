<?php
use app\components\CustomMigration as Migration;

/**
 * Class m180301_054542_delivery_debts
 */
class m180301_054542_delivery_debts extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropColumn('report_delivery_debts', 'id');
        $this->dropColumn('report_delivery_debts', 'time_from');
        $this->dropColumn('report_delivery_debts', 'time_to');
        $this->dropColumn('report_delivery_debts', 'sum_total');
        $this->dropColumn('report_delivery_debts', 'sum_total_bucks');

        $this->addColumn('report_delivery_debts', 'month', $this->date());
        $this->addColumn('report_delivery_debts', 'data', $this->text());

        $this->delete('report_delivery_debts');
        $this->dropForeignKey($this->getFkName('report_delivery_debts', 'delivery_id'), 'report_delivery_debts');
        $this->dropIndex($this->getFkName('report_delivery_debts', 'delivery_id'), 'report_delivery_debts');
        $this->addPrimaryKey($this->getPkName('report_delivery_debts', [
            'delivery_id',
            'month'
        ]), 'report_delivery_debts', ['delivery_id', 'month']);
        $this->addForeignKey($this->getFkName('report_delivery_debts', 'delivery_id'), 'report_delivery_debts', 'delivery_id', 'delivery', 'id', self::CASCADE, self::CASCADE);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey($this->getFkName('report_delivery_debts', 'delivery_id'), 'report_delivery_debts');
        $this->dropPrimaryKey($this->getPkName('report_delivery_debts', [
            'delivery_id',
            'month'
        ]), 'report_delivery_debts');
        $this->dropColumn('report_delivery_debts', 'month');
        $this->dropColumn('report_delivery_debts', 'data');

        $this->addColumn('report_delivery_debts', 'id', $this->primaryKey());
        $this->alterColumn('report_delivery_debts', 'delivery_id', $this->integer()->after('id'));
        $this->addForeignKey($this->getFkName('report_delivery_debts', 'delivery_id'), 'report_delivery_debts', 'delivery_id', 'delivery', 'id', self::CASCADE, self::CASCADE);
        $this->addColumn('report_delivery_debts', 'time_from', $this->integer());
        $this->addColumn('report_delivery_debts', 'time_to', $this->integer());
        $this->addColumn('report_delivery_debts', 'sum_total', $this->double());
        $this->addColumn('report_delivery_debts', 'sum_total_bucks', $this->double());
    }
}
