<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m160705_094910_add_task_mailer_send_notifications
 */
class m160705_094910_add_task_mailer_send_notifications extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = new CrontabTask();
        $crontabTask->name = 'mailer_send_notifications';
        $crontabTask->description = 'Отправка писем по оповещениям.';
        $crontabTask->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = CrontabTask::find()
            ->byName('mailer_send_notifications')
            ->one();

        if ($crontabTask) {
            $crontabTask->delete();
        }
    }
}
