<?php

use app\components\CustomMigration as Migration;
use app\modules\order\models\Order;
use app\modules\api\models\ApiLog;

/**
 * Handles adding column_api_log_id to table `order_table`.
 */
class m170208_104151_add_column_api_log_id_to_order_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn(Order::tableName(), 'api_log_id', $this->integer(16)->after('source_confirmed'));
        $this->addForeignKey(null, Order::tableName(), 'api_log_id', ApiLog::tableName(), 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn(Order::tableName(), 'api_log_id');
    }
}
