<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryApiClass;

/**
 * Class m170307_052009_add_tcc_api_class
 */
class m170307_052009_add_tcc_api_class extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $apiClass = new DeliveryApiClass();
        $apiClass->name = 'TCC';
        $apiClass->class_path = '/tcc-api/src/tccApi.php';
        $apiClass->save();

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        DeliveryApiClass::find()
            ->where([
                'name' => 'TCC',
            ])
            ->one()
            ->delete();
    }
}
