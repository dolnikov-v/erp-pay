<?php
use app\components\CustomMigration as Migration;

/**
 * Class m180205_054822_add_call_center_request_hold_to
 */
class m180205_054822_add_call_center_request_hold_to extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('call_center_request_extra', 'hold_to', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('call_center_request_extra', 'hold_to');
    }
}
