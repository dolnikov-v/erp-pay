<?php

use app\components\CustomMigration as Migration;
use app\modules\report\models\SmsPollHistory;
use app\modules\order\models\Order;
use app\modules\catalog\models\SmsPollQuestions;
use app\models\AuthItem;

/**
 * Handles the creation for table `sms_poll_history`.
 */
class m170320_101829_create_sms_poll_history extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable(SmsPollHistory::tableName(), [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(11)->notNull(),
            'question_id' => $this->integer(11)->notNull(),
            'question_text' => $this->string(255)->notNull(),
            'status' => "ENUM('error','success') NOT NULL DEFAULT 'success'",
            'api_code' => $this->integer(11),
            'api_error' => $this->text(),
            'customer_mobile' => $this->string(255)->notNull(),
            'answer_text' => $this->text(),
            'answered_at' => $this->integer(11)->notNull(),
            'created_at' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)->notNull()

        ]);

        $this->addForeignKey(null, SmsPollHistory::tableName(), 'order_id', Order::tableName(), 'id', self::CASCADE, self::CASCADE);
        $this->addForeignKey(null, SmsPollHistory::tableName(), 'question_id', SmsPollQuestions::tableName(), 'id', self::CASCADE, self::CASCADE);

        $this->insert(AuthItem::tableName(), array(

            'name' => 'report.smspollhistory.index',
            'type' => '2',
            'description' => 'report.smspollhistory.index',
            'created_at' => time(),
            'updated_at' => time()
        ));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable(SmsPollHistory::tableName());
        $this->delete(AuthItem::tableName(), ['name' => 'report.smspollhistory.index']);
    }
}
