<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170919_021759_broker_package
 */
class m170919_021759_broker_package extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('delivery_package', [
            'id' => $this->primaryKey(),
            'delivery_id' => $this->integer()->notNull(),
            'package_id' => $this->integer()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $this->tableOptions);

        $this->addForeignKey(null, 'delivery_package', 'delivery_id', 'delivery', 'id', self::CASCADE, self::RESTRICT);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('delivery_package');
    }
}
