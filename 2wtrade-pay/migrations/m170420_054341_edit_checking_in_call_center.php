<?php
use app\components\CustomMigration as Migration;
use app\modules\callcenter\models\CallCenter;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\callcenter\models\CallCenterCheckRequest;

/**
 * Class m170420_054341_edit_checking_in_call_center
 */
class m170420_054341_edit_checking_in_call_center extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(CallCenterCheckRequest::tableName(), 'status',
            $this->string(50)->after('order_id')->defaultValue('pending'));
        $this->addColumn(CallCenterCheckRequest::tableName(), 'foreign_id', $this->integer()->after('status'));
        $this->addColumn(CallCenterCheckRequest::tableName(), 'foreign_status', $this->integer()->after('foreign_id'));
        $this->addColumn(CallCenterCheckRequest::tableName(), 'foreign_information',
            $this->string(100)->after('foreign_status'));
        $this->addColumn(CallCenterCheckRequest::tableName(), 'comment', $this->string(1000)->after('foreign_information'));
        $this->addColumn(CallCenterCheckRequest::tableName(), 'api_error', $this->string(255)->after('comment'));
        $this->addColumn(CallCenterCheckRequest::tableName(), 'cron_launched_at', $this->integer()->after('status_id'));
        $this->addColumn(CallCenterCheckRequest::tableName(), 'updated_at', $this->integer());
        $this->addColumn(CallCenterCheckRequest::tableName(), 'call_center_id', $this->integer()->after('id'));

        $this->createIndex(null, CallCenterCheckRequest::tableName(),
            ['foreign_id']);
        $this->createIndex(null, CallCenterCheckRequest::tableName(),
            ['foreign_status']);
        $this->createIndex(null, CallCenterCheckRequest::tableName(),
            ['status']);
        $this->createIndex(null, CallCenterCheckRequest::tableName(),
            ['foreign_information']);
        $this->addForeignKey('fk_order_check_history_call_center_id', CallCenterCheckRequest::tableName(), 'call_center_id',
            CallCenter::tableName(), 'id',
            self::CASCADE, self::RESTRICT);

        $checkingTable = CallCenterCheckRequest::tableName();
        $callCenterRequestTable = CallCenterRequest::tableName();

        $updateSql = "UPDATE {$checkingTable} LEFT JOIN {$callCenterRequestTable} ON {$callCenterRequestTable}.order_id = {$checkingTable}.order_id SET {$checkingTable}.call_center_id = {$callCenterRequestTable}.call_center_id, {$checkingTable}.status = 'done', {$checkingTable}.updated_at = {$checkingTable}.created_at";
        $this->execute($updateSql);

        $auth = $this->authManager;
        $permission = $auth->createPermission('callcenter.checkingrequest.index');
        $permission->description = 'callcenter.checkingrequest.index';
        $auth->add($permission);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(CallCenterCheckRequest::tableName(), 'foreign_id');
        $this->dropColumn(CallCenterCheckRequest::tableName(), 'status');
        $this->dropColumn(CallCenterCheckRequest::tableName(), 'foreign_status');
        $this->dropColumn(CallCenterCheckRequest::tableName(), 'foreign_information');
        $this->dropColumn(CallCenterCheckRequest::tableName(), 'api_error');
        $this->dropColumn(CallCenterCheckRequest::tableName(), 'cron_launched_at');
        $this->dropColumn(CallCenterCheckRequest::tableName(), 'comment');
        $this->dropColumn(CallCenterCheckRequest::tableName(), 'updated_at');
        $this->dropForeignKey('fk_order_check_history_call_center_id', CallCenterCheckRequest::tableName());
        $this->dropColumn(CallCenterCheckRequest::tableName(), 'call_center_id');

        $auth = $this->authManager;

        $permission = $auth->getPermission('callcenter.checkingrequest.index');
        $auth->remove($permission);
    }
}
