<?php
use app\components\CustomMigration as Migration;
use app\modules\salary\models\Office;
use yii\db\Query;

/**
 * Class m171109_111946_office_tax
 */
class m171109_111946_office_tax extends Migration
{

    const RULES = [
        'salary.officetax.edit',
        'salary.officetax.delete',
    ];

    const ROLES = [
        'admin',
        'callcenter.hr',
        'controller.analyst',
        'project.manager',
        'salaryproject.clerk',
    ];

    protected $textData = [
        'ЗП в местной валюте "на руки"' => 'Payments in local currency on hands',
        'ЗП в местной валюте с учетом налогов' => 'Payments in local currency, including taxes',
        'ЗП в USD "на руки"' => 'Payment in USD on hands',
        'ЗП в USD с учетом налогов' => 'Payment in USD, including taxes',
    ];


    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('salary_office_tax', [
            'id' => $this->primaryKey(),
            'office_id' => $this->integer(),
            'sum_from' => $this->double(),
            'sum_to' => $this->double(),
            'rate' => $this->double(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $this->tableOptions);

        $this->addForeignKey(null, 'salary_office_tax', 'office_id', Office::tableName(), 'id', self::CASCADE, self::NO_ACTION);

        $arrayForInsert = [];
        foreach (Office::find()->all() as $office) {
            $arrayForInsert[] = [
                'office_id' => $office->id,
                'sum_from' => 0,
                'rate' => 20,
                'created_at' => time(),
                'updated_at' => time(),
            ];
        }

        Yii::$app->db->createCommand()->batchInsert(
            'salary_office_tax',
            [
                'office_id',
                'sum_from',
                'rate',
                'created_at',
                'updated_at',
            ],
            $arrayForInsert
        )->execute();


        $query = new Query();

        foreach (self::RULES as $rule) {

            $is_rule = $query->select('*')->from($this->authManager->itemTable)
                ->where(['name' => $rule, 'type' => 2])
                ->one();

            if (!$is_rule) {
                $this->insert($this->authManager->itemTable, [
                    'name' => $rule,
                    'description' => $rule,
                    'type' => 2,
                    'created_at' => time(),
                    'updated_at' => time()]);
            }

            foreach (self::ROLES as $role) {

                $is_role = $query->select('*')->from($this->authManager->itemTable)
                    ->where(['name' => $role, 'type' => 1])
                    ->one();

                if (!$is_role) {
                    $this->insert($this->authManager->itemTable, [
                        'name' => $role,
                        'type' => 1,
                        'description' => $role,
                        'created_at' => time(),
                        'updated_at' => time()]);
                }

                $is_can = $query->select('*')->from($this->authManager->itemChildTable)->where([
                    'parent' => $role,
                    'child' => $rule
                ])->one();

                if (!$is_can) {
                    $this->insert($this->authManager->itemChildTable, [
                        'parent' => $role,
                        'child' => $rule
                    ]);
                }
            }
        }


        $query = new Query();
        foreach ($this->textData as $ruText => $enText) {

            $is = $query->select('id')
                ->from('i18n_source_message')
                ->where(['category' => 'common', 'message' => $ruText])->one();

            if (!$is) {
                $this->insert('i18n_source_message', ['category' => 'common', 'message' => $ruText]);
                $id = Yii::$app->db->getLastInsertID();
                $this->insert('i18n_message', ['id' => $id, 'language' => 'en-US', 'translation' => $enText]);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('salary_office_tax');

        foreach (self::RULES as $rule) {
            foreach (self::ROLES as $role) {
                $this->delete($this->authManager->itemChildTable, [
                    'parent' => $role,
                    'child' => $rule
                ]);
            }
        }
    }
}
