<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m180329_063306_lead_source_status
 */
class m180329_063306_lead_source_status extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('lead', 'source_status', $this->string(50)->after('source'));
        $this->addColumn('country', 'critical_approve_percent', $this->double()->after('brokerage_enabled'));
        $this->addColumn('source', 'use_pseudo_approve', $this->boolean()->after('active'));
        $this->dropIndex('foreign_id', 'lead');
        $this->dropIndex('uni_lead_foreign_id', 'lead');
        $this->addPrimaryKey($this->getPkName('lead', ['source', 'foreign_id']), 'lead', ['source', 'foreign_id']);
        $this->createIndex($this->getIdxName('lead', ['goods_id', 'ts_spawn']), 'lead', ['goods_id', 'ts_spawn']);
        $this->dropForeignKey($this->getFkName('lead', 'order_id'), 'lead');
        $this->addForeignKey($this->getFkName('lead', 'order_id'), 'lead', 'order_id', 'order', 'id', self::CASCADE, self::CASCADE);
        $this->createIndex($this->getIdxName('lead', 'source_status'), 'lead', 'source_status');
        $this->insertCrontabTask(CrontabTask::TASK_SOURCE_PSEUDO_APPROVE, 'Ложный апрув холдов для достижения гаранта аппрува');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->removeCrontabTask(CrontabTask::TASK_SOURCE_PSEUDO_APPROVE);
        $this->dropIndex($this->getIdxName('lead', 'source_status'), 'lead');
        $this->dropForeignKey($this->getFkName('lead', 'order_id'), 'lead');
        $this->addForeignKey($this->getFkName('lead', 'order_id'), 'lead', 'order_id', 'order', 'id');
        $this->dropIndex($this->getIdxName('lead', ['goods', 'ts_spawn']), 'lead');
        $this->dropPrimaryKey($this->getPkName('lead', ['source', 'foreign_id']), 'lead');
        $this->createIndex('uni_lead_foreign_id', 'lead', 'foreign_id', true);
        $this->createIndex('foreign_id', 'lead', 'foreign_id', true);
        $this->dropColumn('source', 'use_pseudo_approve');
        $this->dropColumn('country', 'critical_approve_percent');
        $this->dropColumn('lead', 'source_status');
    }
}
