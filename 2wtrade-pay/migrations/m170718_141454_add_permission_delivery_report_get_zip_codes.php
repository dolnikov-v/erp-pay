<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170718_141454_add_permission_delivery_report_get_zip_codes
 */
class m170718_141454_add_permission_delivery_report_get_zip_codes extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}',
        [
            'name'=>'report.delivery.zip',
            'type' => '2',
            'description' => 'report.delivery.zip',
            'created_at' => time(),
            'updated_at' => time()
        ]);

        $roles = Yii::$app->authManager->getRoles();
        foreach ($roles as $role) {
            $this->insert($this->authManager->itemChildTable,
            [
                'parent' => $role->name,
                'child' => 'report.delivery.zip'
            ]);
        }

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->authManager->itemTable, ['name' => 'report.delivery.zip']);
    }
}