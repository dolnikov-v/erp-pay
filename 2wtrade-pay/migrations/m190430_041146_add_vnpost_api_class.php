<?php
use app\components\CustomMigration as Migration;

/**
 * Class m190430_041146_add_vnpost_api_class
 */
class m190430_041146_add_vnpost_api_class extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('delivery_api_class', [
            'name' => 'VNPostApi',
            'class_path' => '/vnpost-api/src/VNPostApi.php',
            'active' => 1,
            'can_tracking' => 0,
            'use_amazon_queue' => 0,
            'created_at' => time(),
            'updated_at' => time(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete('delivery_api_class', ['name' => 'VNPostApi']);
    }
}
