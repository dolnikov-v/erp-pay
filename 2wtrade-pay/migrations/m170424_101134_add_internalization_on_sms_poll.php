<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170424_101134_add_internalization_on_sms_poll
 */
class m170424_101134_add_internalization_on_sms_poll extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $list = [
            'Отправка СМС опроса не возможна для данной страны, отсутствует номер.',
            'Не удалось сохранить историю смс опроса. Отправка смс отменена.',
            'Ссылка для ответа : {url}'
        ];

        foreach ($list as $phrase) {
            $this->insert('i18n_source_message', ['category' => 'common', 'message' => $phrase]);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

    }
}
