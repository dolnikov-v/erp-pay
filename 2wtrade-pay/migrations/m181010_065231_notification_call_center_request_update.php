<?php
use app\components\CustomMigration as Migration;

/**
 * Class m181010_065231_notification_call_center_request_update
 */
class m181010_065231_notification_call_center_request_update extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('notification', [
            'description' => 'По стране {country} в КЦ отсутствуют обновления статусов более {minute} минут у {count} заказов.',
            'trigger' => 'ccr.actuality',
            'type' => 'warning',
            'group' => 'system',
            'active' => 1
        ]);
        $this->insert('crontab_task', [
            'name' => 'check_call_center_request_actuality',
            'description' => 'КЦ Статусы. Предупреждение об отсутствии обновления.',
            'active' => 1
        ]);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete('crontab_task', ['name' => 'check_call_center_request_actuality']);
        $this->delete('notification', ['trigger' => 'ccr.actuality']);
    }
}
