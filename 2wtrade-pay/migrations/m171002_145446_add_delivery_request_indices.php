<?php
use app\components\CustomMigration as Migration;

/**
 * Class m171002_145446_add_delivery_request_indices
 */
class m171002_145446_add_delivery_request_indices extends Migration
{
    protected static $indices = [
        'tracking',
        'last_update_info_at',
        'api_error',
        'updated_at',
        'second_sent_at',
        'sent_clarification_at',
        'returned_at',
        'paid_at',
        'finance_tracking',
        'partner_name',
        'cs_send_response_at',
        'unshipping_reason',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        foreach (self::$indices as $index) {
            $this->createIndex($this->getIdxName('delivery_request', $index), 'delivery_request', $index);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach (self::$indices as $index) {
            $this->dropIndex($this->getIdxName('delivery_request', $index), 'delivery_request');
        }
    }
}
