<?php
use app\components\CustomMigration as Migration;

/**
 * Class m181210_102748_add_indices_to_call_center_request
 */
class m181210_102748_add_indices_to_call_center_request extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createIndex('idx_call_center_request_call_center_id_foreign_id_status', 'call_center_request', ['call_center_id', 'foreign_id', 'status']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropIndex('idx_call_center_request_call_center_id_foreign_id_status', 'call_center_request');
    }
}
