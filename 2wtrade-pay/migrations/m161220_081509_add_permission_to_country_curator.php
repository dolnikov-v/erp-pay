<?php

use app\components\CustomMigration as Migration;

/**
 * Handles adding permission to table `country_curator`.
 */
class m161220_081509_add_permission_to_country_curator extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}',array(
            'name'=>'access_warning_stale_delivery_reject',
            'type' => '2',
            'description' => 'access.warning.stale.delivery.reject'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'country.curator',
            'child' => 'access_warning_stale_delivery_reject'
        ));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->authManager->itemChildTable, ['child' => 'access_warning_stale_delivery_reject', 'parent' => 'country.curator']);
        $this->delete('{{%auth_item}}', ['name' => 'access_warning_stale_delivery_reject']);
    }
}
