<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160909_091437_crontab_delete_column_answer
 */
class m160909_091437_crontab_delete_column_answer extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropColumn('crontab_task_log_answer', 'answer');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->addColumn('crontab_task_log_answer', 'answer', $this->text()->after('data')->defaultValue(null));
        $this->alterColumn('crontab_task_log_answer', 'answer', 'MEDIUMTEXT');
    }
}
