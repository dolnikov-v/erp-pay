<?php
use app\models\Notification;

use yii\db\Query;

use app\components\PermissionMigration as Migration;

/**
 * Class m181017_055014_system_settings_delivery_permission
 */
class m181017_055014_system_settings_delivery_permission extends Migration
{
    protected $permissions = ['delivery.control.editsystemsettings'];

    protected $roles = [
        'security.manager' => ['delivery.control.editsystemsettings'],
    ];

    protected $notifications = [
        'delivery.no.send.by.debts' => 'Заказы не могут быть отправлены в КС {delivery}. Есть ее не погашенная ДЗ более {days} дней.',
        'delivery.no.send.by.process' => 'Заказы не могут быть отправлены в КС {delivery}. {orders} заказов в процессе более {days} дней'
    ];

    protected $users = [
        'smykov'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('delivery', 'not_send_if_no_contract', $this->boolean()->defaultValue(1));
        $this->addColumn('delivery', 'not_send_if_has_debts', $this->boolean()->defaultValue(1));
        $this->addColumn('delivery', 'not_send_if_has_debts_days', $this->integer()->defaultValue(45));
        $this->addColumn('delivery', 'not_send_if_in_process', $this->boolean()->defaultValue(1));
        $this->addColumn('delivery', 'not_send_if_in_process_days', $this->integer()->defaultValue(45));
        $this->addColumn('delivery', 'sent_notification_has_debts_at', $this->integer()->after('sent_notification_no_contract_at'));
        $this->addColumn('delivery', 'sent_notification_in_process_at', $this->integer()->after('sent_notification_has_debts_at'));


        foreach ($this->notifications as $trigger => $description) {

            $notificationId = (new Query())->select('id')
                ->from('notification')
                ->where(['trigger' => $trigger])
                ->scalar();

            $data = [
                'description' => $description,
                'trigger' => $trigger,
                'type' => Notification::TYPE_WARNING,
                'group' => Notification::GROUP_REPORT,
                'active' => Notification::ACTIVE,
            ];

            if (!$notificationId) {
                $this->insert('notification', $data);
            } else {
                $this->update('notification', $data, ['id' => $notificationId]);
            }
        }

        foreach ($this->users as $user) {
            $userId = (new Query())->select('id')
                ->from('user')
                ->where(['username' => $user])
                ->scalar();

            if ($userId) {
                foreach ($this->notifications as $trigger => $description) {

                    $userNotificationSettingId = (new Query())->select('id')
                        ->from('user_notification_setting')
                        ->where([
                            'user_id' => $userId,
                            'trigger' => $trigger
                        ])
                        ->scalar();

                    $data = [
                        'user_id' => $userId,
                        'trigger' => $trigger,
                        'active' => 1,
                        'email_interval' => 'always',
                        'sms_active' => 0,
                        'telegram_active' => 1,
                        'updated_at' => time(),
                    ];

                    if (!$userNotificationSettingId) {
                        $data['created_at'] = time();
                        $this->insert('user_notification_setting', $data);
                    } else {
                        $this->update('notification', $data, ['id' => $userNotificationSettingId]);
                    }
                }
            }
        }

        $this->update('delivery', ['not_send_if_has_debts' => 0, 'not_send_if_in_process' => 0]);

        parent::safeUp();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('delivery', 'not_send_if_no_contract');
        $this->dropColumn('delivery', 'not_send_if_has_debts');
        $this->dropColumn('delivery', 'not_send_if_has_debts_days');
        $this->dropColumn('delivery', 'not_send_if_in_process');
        $this->dropColumn('delivery', 'not_send_if_in_process_days');
        $this->dropColumn('delivery', 'sent_notification_has_debts_at');
        $this->dropColumn('delivery', 'sent_notification_in_process_at');

        foreach ($this->notifications as $trigger => $description) {
            $this->delete('user_notification_setting', ['trigger' => $trigger]);
            $this->delete('notification', ['trigger' => $trigger]);
        }

        parent::safeDown();
    }
}
