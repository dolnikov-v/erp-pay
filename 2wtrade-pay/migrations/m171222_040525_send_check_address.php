<?php
use app\components\PermissionMigration as Migration;

/**
 * Class m171222_040525_send_check_address
 */
class m171222_040525_send_check_address extends Migration
{
    protected $permissions = [
        'order.index.sendcheckaddress'
    ];

    protected $roles = [
        'country.curator' => [
            'order.index.sendcheckaddress'
        ],
        'project.manager' => [
            'order.index.sendcheckaddress'
        ],
        'controller.analyst' => [
            'order.index.sendcheckaddress'
        ],
    ];
}
