<?php
use app\components\CustomMigration as Migration;

/**
 * Class m161025_093828_api_delivery
 */
class m161025_093828_api_delivery extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('api_user', [
            'id' => $this->primaryKey(),
            'username' => $this->string(64)->notNull(),
            'password' => $this->string(64)->notNull(),
            'status' => $this->smallInteger()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $this->tableOptions);

        $this->createTable('api_user_delivery', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'delivery_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $this->tableOptions);

        $this->addForeignKey(null, 'api_user_delivery', 'user_id', 'api_user', 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, 'api_user_delivery', 'delivery_id', 'delivery', 'id', self::CASCADE, self::RESTRICT);

        $this->createTable('api_user_token', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'type' => $this->enum(['delivery', 'call_center']),
            'auth_token' => $this->string(100)->defaultValue(null),
            'lifetime' => $this->integer(11)->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $this->tableOptions);

        $this->addForeignKey(null, 'api_user_token', 'user_id', 'api_user', 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('api_user_token');
        $this->dropTable('api_user_delivery');
        $this->dropTable('api_user');
    }
}
