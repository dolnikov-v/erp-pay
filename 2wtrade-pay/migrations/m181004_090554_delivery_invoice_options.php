<?php
use app\components\CustomMigration as Migration;

/**
 * Class m181004_090554_delivery_invoice_options
 */
class m181004_090554_delivery_invoice_options extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('delivery_invoice_options', [
            'id' => $this->primaryKey(),
            'delivery_id' => $this->integer(),
            'day' => $this->integer(2)->notNull(),
            'time' => $this->string(5)->notNull()->defaultValue('00:00'),
            'from' => $this->integer(2)->notNull()->defaultValue(1),
            'to' => $this->integer(2)->notNull()->defaultValue(31),
        ]);

        $this->createIndex(null, 'delivery_invoice_options', 'delivery_id');
        $this->createIndex(null, 'delivery_invoice_options', 'day');
        $this->createIndex(null, 'delivery_invoice_options', 'time');

        $this->insert('crontab_task', ['name' => 'auto_invoice_sender', 'description' => 'Генерация и отправка автоинвойсов по параметрам (КС->инвойсирование)']);

        $this->addColumn('delivery', 'auto_send_invoice', $this->boolean()->notNull());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('delivery', 'auto_send_invoice');
        $this->delete('crontab_task', ['name' => 'auto_invoice_sender']);
        $this->dropTable('delivery_invoice_options');
    }
}