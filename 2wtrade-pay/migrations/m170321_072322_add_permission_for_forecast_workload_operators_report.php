<?php

use app\components\CustomMigration as Migration;

/**
 * Handles adding permission  `report.forecastworkloadoperatorsbyhours.index`.
 */
class m170321_072322_add_permission_for_forecast_workload_operators_report extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}',array(
            'name'=>'report.forecastworkloadoperatorsbyhours.index',
            'type' => '2',
            'description' => 'report.forecastworkloadoperatorsbyhours.index',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'country.curator',
            'child' => 'report.forecastworkloadoperatorsbyhours.index'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'callcenter.manager',
            'child' => 'report.forecastworkloadoperatorsbyhours.index'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'business_analyst',
            'child' => 'report.forecastworkloadoperatorsbyhours.index'
        ));

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->authManager->itemChildTable, ['child' => 'report.forecastworkloadoperatorsbyhours.index', 'parent' => 'country.curator']);
        $this->delete($this->authManager->itemChildTable, ['child' => 'report.forecastworkloadoperatorsbyhours.index', 'parent' => 'callcenter.manager']);
        $this->delete($this->authManager->itemChildTable, ['child' => 'report.forecastworkloadoperatorsbyhours.index', 'parent' => 'business_analyst']);
        $this->delete('{{%auth_item}}', ['name' => 'report.forecastworkloadoperatorsbyhours.index']);
    }
}
