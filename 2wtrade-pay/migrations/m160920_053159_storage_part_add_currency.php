<?php
use app\components\CustomMigration as Migration;
use app\models\Currency;
use app\modules\storage\models\StoragePart;
use app\components\Currency as CurrencyComponent;

/**
 * Class m160920_053159_storage_part_add_currency
 */
class m160920_053159_storage_part_add_currency extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('storage_part', 'currency_id', $this->integer(11)->after('storage_id'));
        $this->addForeignKey(null, 'storage_part', 'currency_id', 'currency', 'id');

        $usd = Currency::findOne(['char_code' => CurrencyComponent::DEFAULT_CURRENCY]);
        $usd = $usd->id;

        $parts = StoragePart::find()->all();
        foreach ($parts as $part) {
            $part->currency_id = $usd;
            $part->save(false);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey($this->getFkName('storage_part', 'currency_id'), 'storage_part');
        $this->dropColumn('storage_part', 'currency_id');
    }
}
