<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160628_122317_add_show_country
 */
class m160628_122317_add_show_country extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('country', 'active', $this->smallInteger()->notNull()->defaultValue(1)->after('currency_id'));
        $this->createIndex('idx_active_country', 'country', 'active');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('country', 'active');
    }
}
