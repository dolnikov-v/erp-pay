<?php

use app\components\CustomMigration as Migration;

/**
 * Handles adding roles to table `view_widgets`.
 */
class m170118_054134_add_roles_to_view_widgets extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}', [
            'name'=> 'business_analyst',
            'type' => '1',
            'description' => 'Бизнес-аналитик',
            'created_at' => time(),
            'updated_at' => time()
        ]);

        $this->insert($this->authManager->itemChildTable, [
            'parent' => 'business_analyst',
            'child' => 'home.index.index'
        ]);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->authManager->itemChildTable, [
            'parent' => 'business_analyst',
            'child' => 'home.index.index',
        ]);
        $this->delete('{{%auth_item}}', ['name' => 'business_analyst']);
    }
}
