<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryApiClass;

/**
 * Class m170403_114630_add_delivery_api_fullphilippines
 */
class m170403_114630_add_delivery_api_fullphilippines extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $class = new DeliveryApiClass();
        $class->name = 'Fullphilippines';
        $class->class_path = '/fullphilippines-api/src/FullphilippinesApi.php';
        $class->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        DeliveryApiClass::find()
            ->where([
                'name' => 'Fullphilippines',
                'class_path' => '/fullphilippines-api/src/FullphilippinesApi.php'
            ])
            ->one()
            ->delete();
    }
}
