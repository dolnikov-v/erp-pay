<?php
use app\components\CustomMigration as Migration;
use app\modules\report\models\ReportDeliveryDebts;

/**
 * Class m180523_075749_report_delivery_debts
 */
class m180523_075749_report_delivery_debts extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        $this->dropForeignKey($this->getFkName(ReportDeliveryDebts::tableName(), 'delivery_id'), ReportDeliveryDebts::tableName());

        $this->dropPrimaryKey($this->getPkName(ReportDeliveryDebts::tableName(), [
            'delivery_id',
            'month'
        ]), ReportDeliveryDebts::tableName());

        $this->addColumn(ReportDeliveryDebts::tableName(), 'id', $this->primaryKey());
        $this->addColumn(ReportDeliveryDebts::tableName(), 'created_at', $this->integer());

        $this->update(ReportDeliveryDebts::tableName(), [
            'created_at' => time(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(ReportDeliveryDebts::tableName(), 'id');
        $this->addPrimaryKey($this->getPkName(ReportDeliveryDebts::tableName(), [
            'delivery_id',
            'month'
        ]), 'report_delivery_debts', ['delivery_id', 'month']);
        $this->dropColumn(ReportDeliveryDebts::tableName(), 'created_at');
    }
}
