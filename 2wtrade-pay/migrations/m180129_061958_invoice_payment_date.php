<?php
use app\components\CustomMigration as Migration;

/**
 * Class m180129_061958_invoice_payment_date
 */
class m180129_061958_invoice_payment_date extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('invoice', 'payment_date', $this->integer()->after('period_to'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('invoice', 'payment_date');
    }
}
