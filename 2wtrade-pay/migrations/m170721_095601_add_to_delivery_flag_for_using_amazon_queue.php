<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryApiClass;

/**
 * Class m170721_095601_add_to_deliery_flag_for_using_amazon_queue
 */
class m170721_095601_add_to_delivery_flag_for_using_amazon_queue extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(DeliveryApiClass::tableName(), 'use_amazon_queue', $this->boolean()->after('active')->defaultValue(0));

        $this->insert('{{%auth_item}}',
            [
                'name' => 'delivery.apiclass.activateamazonqueue',
                'type' => '2',
                'description' => 'delivery.apiclass.activateamazonqueue',
                'created_at' => time(),
                'updated_at' => time()
            ]);

        $this->insert('{{%auth_item}}',
            [
                'name' => 'delivery.apiclass.deactivateamazonqueue',
                'type' => '2',
                'description' => 'delivery.apiclass.deactivateamazonqueue',
                'created_at' => time(),
                'updated_at' => time()
            ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(DeliveryApiClass::tableName(), 'use_amazon_queue');
        $this->delete($this->authManager->itemTable, ['name' => 'delivery.apiclass.activateamazonqueue']);
        $this->delete($this->authManager->itemTable, ['name' => 'delivery.apiclass.deactivateamazonqueue']);
    }
}
