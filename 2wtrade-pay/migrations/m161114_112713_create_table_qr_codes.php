<?php

use app\components\CustomMigration as Migration;

/**
 * Handles the creation for table `qr_codes`.
 */
class m161114_112713_create_table_qr_codes extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('qr_codes', [
            'id' => $this->primaryKey(),
            'qr_code' => $this->string(500)->notNull(),
            'created_at' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)->notNull(),
            'user_id' => $this->integer(4),
            'product_id' => $this->integer(4),
            'order_id' => $this->integer(16),
            'status' => $this->integer(2),
        ], $this->tableOptions);

        $this->addForeignKey('fk_user_qr_id', 'qr_codes', 'user_id', 'user', 'id',  self::NO_ACTION, self::NO_ACTION);
        $this->addForeignKey('fk_product_qr_id', 'qr_codes', 'product_id', 'product', 'id',  self::NO_ACTION, self::NO_ACTION);
        $this->addForeignKey('fk_order_qr_id', 'qr_codes', 'order_id', 'order', 'id',  self::NO_ACTION, self::NO_ACTION);
    }
    
    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_user_qr_id', 'qr_codes');
        $this->dropForeignKey('fk_product_qr_id', 'qr_codes');
        $this->dropForeignKey('fk_order_qr_id', 'qr_codes');
        $this->dropTable('qr_codes');
    }
}
