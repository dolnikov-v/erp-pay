<?php
use app\components\CustomMigration as Migration;
use app\modules\order\models\OrderStatus;
use app\modules\order\models\OrderWorkflow;
use app\modules\order\models\OrderWorkflowStatus;

/**
 * Class m170913_053613_update_order_workflow_status
 */
class m170913_053613_update_order_workflow_status extends Migration
{
    const CHANGING_STATUSES_WORKFLOW = [OrderStatus::STATUS_CC_REJECTED, OrderStatus::STATUS_CC_DOUBLE];
    const STATUS_RELATION = [
        OrderStatus::STATUS_CC_APPROVED,
        OrderStatus::STATUS_CC_TRASH,
        OrderStatus::STATUS_CC_RECALL,
        OrderStatus::STATUS_CC_POST_CALL,
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $orderWorkFlows = OrderWorkflow::find()->joinWith('statuses')->where(['NOT IN',OrderWorkflowStatus::tableName() . '.status_id', self::CHANGING_STATUSES_WORKFLOW])->all();
        foreach (self::CHANGING_STATUSES_WORKFLOW as $orderWorkflowStatusId) {
            foreach ($orderWorkFlows as $orderWorkFlow) {
                $orderWorkFlowStatus = new OrderWorkflowStatus();
                $orderWorkFlowStatus->workflow_id = $orderWorkFlow->id;
                $orderWorkFlowStatus->status_id = $orderWorkflowStatusId;
                $orderWorkFlowStatus->parents = self::STATUS_RELATION;
                $orderWorkFlowStatus->setScenario(OrderWorkflowStatus::SCENARIO_IGNORE_PARENTS);
                $orderWorkFlowStatus->save(false);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $orderWorkFlowStatuses = OrderWorkflowStatus::find()
            ->joinWith('workflow')
            ->where(['status_id' => self::CHANGING_STATUSES_WORKFLOW])
            ->andWhere(['<>', OrderWorkflow::tableName() . '.name', 'Fulfillment - Japan'])->all();
        foreach ($orderWorkFlowStatuses as $orderWorkFlowStatus) {
            $orderWorkFlowStatus->delete();
        }
    }
}
