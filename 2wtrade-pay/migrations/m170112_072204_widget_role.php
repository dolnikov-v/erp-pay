<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170112_072204_widget_rule
 */
class m170112_072204_widget_role extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('widget_role', [
            'id' => $this->primaryKey(),
            'role' => $this->string(64)->notNull(),
            'type_id' => $this->integer()->notNull(),
        ], $this->tableOptions);

        $this->addForeignKey(null, 'widget_role', 'type_id', 'widget_type', 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('widget_role');
    }
}
