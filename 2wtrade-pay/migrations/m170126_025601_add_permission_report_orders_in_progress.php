<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170126_025601_add_permission_report_orders_in_progress
 */
class m170126_025601_add_permission_report_orders_in_progress extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}', array(
            'name' => 'report.inprogress.index',
            'type' => '2',
            'description' => 'report.inprogress.index',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'country.curator',
            'child' => 'report.inprogress.index'
        ));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->authManager->itemChildTable, ['child' => 'report.inprogress.index', 'parent' => 'country.curator']);
        $this->delete('{{%auth_item}}', ['name' => 'report.inprogress.index']);
    }

}
