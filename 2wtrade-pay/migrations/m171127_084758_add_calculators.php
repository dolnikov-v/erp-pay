<?php
use app\components\DeliveryChargesCalculatorMigration as Migration;

/**
 * Class m171127_084758_add_calculators
 */
class m171127_084758_add_calculators extends Migration
{
    /**
     * @return array
     */
    protected static function getCalculators()
    {
        return [
            'AipexIN' => 'app\modules\delivery\components\charges\aipex\AipexIN',
            'Atri' => 'app\modules\delivery\components\charges\atri\Atri',
            'DTDC_IN' => 'app\modules\delivery\components\charges\dtdc\DTDCIN',
            'Kobo' => 'app\modules\delivery\components\charges\kobo\Kobo',
            'NinjaVN' => 'app\modules\delivery\components\charges\ninja\NinjaVN'
        ];
    }
}
