<?php

use app\components\CustomMigration as Migration;
use app\models\Product;

/**
 * Handles adding weight_and_size to table `product`.
 */
class m170801_103930_add_weight_and_size_to_product extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Product::tableName(), 'weight', $this->integer()->after('source'));
        $this->addColumn(Product::tableName(), 'width', $this->integer()->after('weight'));
        $this->addColumn(Product::tableName(), 'height', $this->integer()->after('width'));
        $this->addColumn(Product::tableName(), 'length', $this->integer()->after('height'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Product::tableName(), 'weight');
        $this->dropColumn(Product::tableName(), 'width');
        $this->dropColumn(Product::tableName(), 'height');
        $this->dropColumn(Product::tableName(), 'length');
    }
}
