<?php
use app\components\CustomMigration as Migration;
use yii\db\Query;
use app\modules\order\components\exporter\Exporter;
use app\modules\order\models\OrderClarification;
use app\models\User;
use app\models\Country;
use app\modules\delivery\models\Delivery;
use app\modules\administration\models\CrontabTask;
use app\models\Notification;

/**
 * Class m170418_031208_clarification_in_delivery
 */
class m170418_031208_clarification_in_delivery extends Migration
{
    const ROLES = [
        'business_analyst',
        'callcenter.manager',
        'country.curator',
        'project.manager',
    ];

    const RULES = [
        'order.index.clarificationdelivery'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $query = new Query();

        foreach (self::ROLES as $role) {
            $is_role = $query->select('*')->from($this->authManager->itemTable)
                ->where(['name' => $role, 'type' => 1])
                ->one();

            if (!$is_role) {
                $this->insert($this->authManager->itemTable, [
                    'name' => $role,
                    'type' => 1,
                    'description' => $role,
                    'created_at' => time(),
                    'updated_at' => time()]);
            }

            foreach (self::RULES as $rule) {

                $is_rule = $query->select('*')->from($this->authManager->itemTable)
                    ->where(['name' => $rule, 'type' => 2])
                    ->one();

                if (!$is_rule) {
                    $this->insert($this->authManager->itemTable, [
                        'name' => $rule,
                        'description' => $rule,
                        'type' => 2,
                        'created_at' => time(),
                        'updated_at' => time()]);
                }

                $is_can = $query->select('*')->from($this->authManager->itemChildTable)->where([
                    'parent' => $role,
                    'child' => $rule
                ])->one();

                if (!$is_can) {
                    $this->insert($this->authManager->itemChildTable, [
                        'parent' => $role,
                        'child' => $rule
                    ]);
                }
            }
        }

        $this->createTable(OrderClarification::tableName(), [
            'id' => $this->primaryKey(),
            'country_id' => $this->integer(),
            'user_id' => $this->integer(),
            'order_ids' => $this->text(),
            'delivery_id' => $this->integer(),
            'delivery_emails' => $this->string(),
            'subject' => $this->string(),
            'message' => $this->text(),
            'type' => $this->enum([Exporter::TYPE_CSV, Exporter::TYPE_EXCEL]),
            'columns' => $this->text(),
            'lines' => $this->integer(),
            'filename' => $this->string(),
            'date_format' => $this->string(),
            'language' => $this->string(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'generated_at' => $this->integer(),
            'sent_at' => $this->integer(),
        ], $this->tableOptions);

        $this->addForeignKey(null, OrderClarification::tableName(), 'user_id', User::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, OrderClarification::tableName(), 'country_id', Country::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, OrderClarification::tableName(), 'delivery_id', Delivery::tableName(), 'id', self::CASCADE, self::RESTRICT);

        $tasks = [
            CrontabTask::TASK_SEND_ORDER_CLARIFICATION => 'Отправка на уточнение в КС писем с выгрузкой заказов в файл'
        ];

        foreach ($tasks as $name => $description) {

            $crontabTask = CrontabTask::findOne(['name' => $name]);

            if (!($crontabTask instanceof CrontabTask)) {
                $crontabTask = new CrontabTask();
                $crontabTask->name = $name;
                $crontabTask->description = $description;
                $crontabTask->save();
            }
        }

        $triggers = [
            Notification::TRIGGER_SEND_ORDER_CLARIFICATION => 'Отправлены файлы с заказами на уточнение в КС {emails}',
        ];

        foreach ($triggers as $trigger => $description) {

            $notification = Notification::findOne(['trigger' => $trigger]);

            if (!($notification instanceof Notification)) {
                $notification = new Notification([
                    'description' => $description,
                    'trigger' => $trigger,
                    'type' => Notification::TYPE_SUCCESS,
                    'group' => Notification::GROUP_SYSTEM,
                    'active' => Notification::ACTIVE,
                ]);
                $notification->save(false);
            }
        }

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach (self::ROLES as $role) {
            foreach (self::RULES as $rule) {
                $this->delete($this->authManager->itemChildTable, [
                    'parent' => $role,
                    'child' => $rule
                ]);
            }
        }

        $this->dropTable(OrderClarification::tableName());

        $tasks = [
            CrontabTask::TASK_SEND_ORDER_CLARIFICATION
        ];

        foreach ($tasks as $task) {

            $crontabTask = CrontabTask::findOne(['name' => $task]);

            if ($crontabTask instanceof CrontabTask) {
                $crontabTask->delete();
            }
        }

        $triggers = [
            Notification::TRIGGER_SEND_ORDER_CLARIFICATION
        ];

        foreach ($triggers as $trigger) {

            $notification = Notification::findOne(['trigger' => $trigger]);

            if ($notification instanceof Notification) {
                $notification->delete();
            }
        }

    }
}
