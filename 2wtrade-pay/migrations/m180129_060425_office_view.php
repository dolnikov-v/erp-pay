<?php
use app\components\PermissionMigration as Migration;

/**
 * Class m180129_060425_office_view
 */
class m180129_060425_office_view extends Migration
{
    protected $permissions = ['salary.office.view'];

    protected $roles = [
        'project.manager' => [
            'salary.office.view'
        ],
        'admin' => [
            'salary.office.view'
        ],
        'callcenter.hr' => [
            'salary.office.view'
        ],
        'controller.analyst' => [
            'salary.office.view'
        ],
        'country.curator' => [
            'salary.office.view'
        ],
        'salaryproject.clerk' => [
            'salary.office.view'
        ],
        'callcenter.leader' => [
            'salary.office.view',
            'salary.office.index',
        ],
    ];
}
