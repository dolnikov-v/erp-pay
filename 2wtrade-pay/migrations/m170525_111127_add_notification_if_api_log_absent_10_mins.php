<?php
use app\components\CustomMigration as Migration;
use app\models\Notification;

/**
 * Class m170525_111127_add_notification_if_api_log_absent_10_mins
 */
class m170525_111127_add_notification_if_api_log_absent_10_mins extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $model = new Notification([
            'description' => 'За последние 10 мин не было логов success на создание лидов, последний лог {text}.',
            'trigger' => 'lead.absent.10minuts',
            'type' => Notification::TYPE_DANGER,
            'group' => Notification::GROUP_REPORT,
            'active' => Notification::ACTIVE,
        ]);

        $model->save(false);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $model = Notification::findOne(['trigger' => 'lead.absent.10minuts']);

        if ($model instanceof Notification) {
            $model->delete();
        }
    }
}
