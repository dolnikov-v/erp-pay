<?php
use app\components\PermissionMigration as Migration;

/**
 * Class m190116_110309_permission_for_delete_storage_balance
 */
class m190116_110309_permission_for_delete_storage_balance extends Migration
{
    protected $permissions = ['storage.balance.delete'];

    protected $roles = [
        'project.manager' => [
            'storage.balance.delete'
        ],
        'admin' => [
            'storage.balance.delete'
        ],
        'controller.analyst' => [
            'storage.balance.delete'
        ],
        'country.curator' => [
            'storage.balance.delete'
        ],
        'partner' => [
            'storage.balance.delete'
        ],
        'storage.manager' => [
            'storage.balance.delete'
        ],
        'support.manager' => [
            'storage.balance.delete'
        ],
        'technical.director' => [
            'storage.balance.delete'
        ],
    ];
}
