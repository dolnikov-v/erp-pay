<?php

use app\components\CustomMigration as Migration;

/**
 * Class m180424_063017_add_permission_get_requisite_by_invoice
 */
class m180424_063017_add_permission_get_requisite_by_invoice extends Migration
{
    private $translate = [
        'Справочники / Реквизиты (выбор в инвойсах)' => ''
    ];

    private $permissions = [
        'name' => 'catalog.requisitedelivery.getrequisitebyinvoice',
        'desc' => 'Directories / Requisites (choice in invoices)',
    ];


    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        foreach ($this->translate as $ruText => $enText) {
            $this->insert('i18n_source_message', ['category' => 'common', 'message' => $ruText]);
            $id = Yii::$app->db->getLastInsertID();
            $this->insert('i18n_message', ['id' => $id, 'language' => 'en-US', 'translation' => $enText]);
        }

        $this->insert('{{%auth_item}}', [
            'name' => $this->permissions['name'],
            'type' => '2',
            'description' => $this->permissions['desc'],
            'created_at' => time(),
            'updated_at' => time()
        ]);

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'finance.director',
            'child' => $this->permissions['name']
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'finance.manager',
            'child' => $this->permissions['name']
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'country.curator',
            'child' => $this->permissions['name']
        ));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach ($this->translate as $ruText => $enText) {
            $this->delete('i18n_source_message', ['category' => 'common', 'message' => $ruText]);
        }


        $this->delete($this->authManager->itemChildTable, ['child' => $this->permissions['name'], 'parent' => 'finance.director']);
        $this->delete($this->authManager->itemChildTable, ['child' => $this->permissions['name'], 'parent' => 'finance.manager']);
        $this->delete($this->authManager->itemChildTable, ['child' => $this->permissions['name'], 'parent' => 'country.curator']);

        $this->delete('{{%auth_item}}', ['name' => $this->permissions['name']]);


    }
}
