<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160425_091009_call_center_request
 */
class m160425_091009_call_center_request extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropColumn('order', 'call_center_sent_at');

        $this->dropForeignKey($this->getFkName('order', 'call_center_id'), 'order');
        $this->dropColumn('order', 'call_center_id');

        $this->createTable('call_center_request', [
            'id' => $this->primaryKey(),
            'call_center_id' => $this->integer()->notNull(),
            'order_id' => $this->integer()->notNull(),
            'foreign_id' => $this->integer()->notNull(),
            'status' => "ENUM('in_progress', 'done') DEFAULT 'in_progress'",
            'foreign_status' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(null, 'call_center_request', 'call_center_id', 'call_center', 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, 'call_center_request', 'order_id', 'order', 'id', self::CASCADE, self::RESTRICT);
        $this->createIndex(null, 'call_center_request', 'order_id', true);
        $this->createIndex(null, 'call_center_request', 'foreign_id');
        $this->createIndex(null, 'call_center_request', 'status');
        $this->createIndex(null, 'call_center_request', 'foreign_status');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey($this->getFkName('call_center_request', 'order_id'), 'call_center_request');
        $this->dropForeignKey($this->getFkName('call_center_request', 'call_center_id'), 'call_center_request');

        $this->dropTable('call_center_request');

        $this->addColumn('order', 'call_center_id', $this->integer()->defaultValue(null) . ' AFTER `country_id`');
        $this->addForeignKey(null, 'order', 'call_center_id', 'call_center', 'id', self::SET_NULL, self::SET_NULL);

        $this->addColumn('order', 'call_center_sent_at', $this->integer()->defaultValue(null) . ' AFTER `updated_at`');
    }
}
