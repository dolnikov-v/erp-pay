<?php
use app\components\CustomMigration as Migration;
use app\modules\salary\models\OfficeTax;

/**
 * Class m180413_052823_office_tax_type
 */
class m180413_052823_office_tax_type extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(OfficeTax::tableName(), 'type', $this->string()->after('sum_to')->defaultValue(OfficeTax::TYPE_PERCENT));
        $this->addColumn(OfficeTax::tableName(), 'amount', $this->float()->after('rate'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(OfficeTax::tableName(), 'type');
        $this->dropColumn(OfficeTax::tableName(), 'amount');
    }
}
