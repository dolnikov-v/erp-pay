<?php
use app\components\CustomMigration as Migration;
use app\modules\order\models\OrderStatus;

/**
 * Class m170622_080453_add_status_reclamation
 */
class m170622_080453_add_status_reclamation extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert(OrderStatus::tableName(), ['sort' => 34, 'name' => 'Рекламация', 'comment' => null, 'group' => 'delivery', 'type' => 'middle']);
        $this->insert('i18n_source_message', ['category' => 'common', 'message' => 'Рекламация']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete(OrderStatus::tableName(), ['name' => 'Рекламация']);
    }
}
