<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;
/**
 * Class m170607_050007_new_crontab_packager
 */
class m170607_050007_new_crontab_packager extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = new CrontabTask();
        $crontabTask->name = 'packager_send_orders_to_packaging';
        $crontabTask->description = 'Отправка этикеток заказов в службу упаковки.';
        $crontabTask->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = CrontabTask::find()
            ->byName('packager_send_orders_to_packaging')
            ->one();

        if ($crontabTask) {
            $crontabTask->delete();
        }
    }
}
