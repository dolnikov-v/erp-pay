<?php
use app\components\CustomMigration as Migration;
use yii\db\Query;
use app\modules\widget\models\WidgetType;
use app\modules\widget\models\WidgetRole;


/**
 * Class m170526_041233_finance_director
 */
class m170526_041233_finance_director extends Migration
{
    const ROLE = 'finance.director';

    // будет иметь полный доступ к финансовой информации
    const ROLE_NAME = 'Финансовый директор';

    // скопируем действущие операции от finance.manager
    const RULES = [
        'view_finance_director',
        'home.index.index',
        'order.index.changedelivery',
        'order.index.changestatus',
        'order.index.clarificationdelivery',
        'order.index.clarificationindelivery',
        'order.index.createcallcenterrequest',
        'order.index.deleteordersfromlists',
        'order.index.edit',
        'order.index.excelorderlistclear',
        'order.index.generatelist',
        'order.index.generatemarketinglist',
        'order.index.getdeliveryemail',
        'order.index.index',
        'order.index.printinvoice',
        'order.index.resendincallcenter',
        'order.index.resendincallcenterbyexcel',
        'order.index.resendincallcenternewqueue',
        'order.index.resendincallcenterreturnnoprod',
        'order.index.resendincallcenterstillwaiting',
        'order.index.resendindelivery',
        'order.index.resendseparateorder',
        'order.index.resendseparateorderintodelivery',
        'order.index.retrygetstatusfromcallcenter',
        'order.index.seconddeliveryattempt',
        'order.index.sendanalysistrash',
        'order.index.sendcheckundeliveryorder',
        'report.accountingcosts.index',
        'report.advertising.index',
        'report.callcenter.index',
        'report.daily.index',
        'report.debt.index',
        'report.debts.export',
        'report.debts.index',
        'report.debts.showorders',
        'report.debts.showpayments',
        'report.finance.index',
        'report.receivable.index',
        'report.short.index',
        'report.status.index',
        'report.topmanager.index'
    ];

    // эти виджеты будут доступны только для роли Финансовый директор
    const WIDGETS = [
        'top_countries',
        'delivery_debts',
        'global_top_goods',
        'payback_advertising',
        'top20cities',
        'top20products',
        'top_deliveries',
        'top_сс_managers',
        'top_country_curators',
        'top_goods',
        'operators_efficiency',
    ];

    // эти репорты доступны только для Финансовый директор, для остальный убираем
    const REPORTS = [
        'report.topmanager.index',
        'report.accountingcosts.index',
        'report.debts.index'
    ];

    //top_countries

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $query = new Query();

        $is_role = $query->select('*')->from($this->authManager->itemTable)
            ->where(['name' => self::ROLE, 'type' => 1])
            ->one();

        if (!$is_role) {
            $this->insert($this->authManager->itemTable, [
                'name' => self::ROLE,
                'type' => 1,
                'description' => self::ROLE_NAME,
                'created_at' => time(),
                'updated_at' => time()]);
        }

        foreach (self::RULES as $rule) {

            $is_rule = $query->select('*')->from($this->authManager->itemTable)
                ->where(['name' => $rule, 'type' => 2])
                ->one();

            if (!$is_rule) {
                $this->insert($this->authManager->itemTable, [
                    'name' => $rule,
                    'description' => $rule,
                    'type' => 2,
                    'created_at' => time(),
                    'updated_at' => time()]);
            }

            $is_can = $query->select('*')->from($this->authManager->itemChildTable)->where([
                'parent' => self::ROLE,
                'child' => $rule
            ])->one();

            if (!$is_can) {
                $this->insert($this->authManager->itemChildTable, [
                    'parent' => self::ROLE,
                    'child' => $rule
                ]);
            }
        }


        foreach (self::WIDGETS as $widget) {

            $widgetType = WidgetType::find()
                ->where(['code' => $widget])
                ->one();

            if ($widgetType) {

                $this->delete(WidgetRole::tableName(), [
                    'type_id' => $widgetType->id
                ]);

                $this->insert(WidgetRole::tableName(), [
                    'role' => self::ROLE,
                    'type_id' => $widgetType->id
                ]);
            }
        }

        foreach (self::REPORTS as $report) {
            $this->delete($this->authManager->itemChildTable,
                [
                    'and',
                    ['!=', 'parent', self::ROLE],
                    ['=', 'child', $report]
                ]
            );
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach (self::RULES as $rule) {
            $this->delete($this->authManager->itemChildTable, [
                'parent' => self::ROLE,
                'child' => $rule
            ]);
        }

        $this->delete($this->authManager->itemTable, [
            'name' => self::ROLE,
            'type' => 1
        ]);


        foreach (self::WIDGETS as $widget) {

            $widgetType = WidgetType::find()
                ->where(['code' => $widget])
                ->one();

            if ($widgetType) {

                $this->delete(WidgetRole::tableName(), [
                    'role' => self::ROLE,
                    'type_id' => $widgetType->id
                ]);

            }
        }
    }
}
