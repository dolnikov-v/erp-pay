<?php

use app\components\CustomMigration as Migration;
use app\models\Product;

class m000000_000025_product_add_column extends Migration
{
    public function up()
    {
        $this->addColumn(Product::tableName(), 'foreign_id', 'integer(11) AFTER `id`');
    }

    public function down()
    {
        $this->dropColumn(Product::tableName(), 'foreign_id');
    }
}
