<?php

use app\components\CustomMigration;
use app\modules\administration\models\CrontabTask;

/**
 * Handles adding delivery_report_parse to table `crontab`.
 */
class m161007_053319_add_delivery_report_parse_to_crontab extends CustomMigration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = new CrontabTask();
        $crontabTask->name = 'delivery_report_parse_reports';
        $crontabTask->description = 'Сканирование и проверка на ошибки отчетов от служб доставки.';
        $crontabTask->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = CrontabTask::find()
            ->byName('delivery_report_parse_reports')
            ->one();

        if ($crontabTask) {
            $crontabTask->delete();
        }
    }
}
