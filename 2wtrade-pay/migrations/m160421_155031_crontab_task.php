<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m160421_155031_crontab_task
 */
class m160421_155031_crontab_task extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = new CrontabTask();
        $crontabTask->name = 'send_lead_in_call_center';
        $crontabTask->description = 'Отправка лидов в колл-центр на обзвон.';
        $crontabTask->save();

        $crontabTask = new CrontabTask();
        $crontabTask->name = 'get_status_lead_from_call_center';
        $crontabTask->description = 'Принятие результатов прозвона из в колл-центра.';
        $crontabTask->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = CrontabTask::find()
            ->byName('send_lead_in_call_center')
            ->one();

        if ($crontabTask) {
            $crontabTask->delete();
        }

        $crontabTask = CrontabTask::find()
            ->byName('get_status_lead_from_call_center')
            ->one();

        if ($crontabTask) {
            $crontabTask->delete();
        }
    }
}
