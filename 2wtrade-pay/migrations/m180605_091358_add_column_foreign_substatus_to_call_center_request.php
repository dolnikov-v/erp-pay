<?php

use app\modules\callcenter\models\CallCenterRequest;
use yii\db\Migration;

/**
 * Handles adding column_foreign_substatus to table `call_center_request`.
 */
class m180605_091358_add_column_foreign_substatus_to_call_center_request extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn(CallCenterRequest::tableName(), 'foreign_substatus', $this->integer()->after('foreign_status')->defaultValue(null));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn(CallCenterRequest::tableName(), 'foreign_substatus');
    }
}
