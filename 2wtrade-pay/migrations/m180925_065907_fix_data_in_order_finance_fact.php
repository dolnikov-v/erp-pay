<?php

use app\components\CustomMigration as Migration;

/**
 * Class m180925_065907_fix_data_in_order_finance_fact
 */
class m180925_065907_fix_data_in_order_finance_fact extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->execute("DELETE `order_finance_fact` FROM `order_finance_fact` LEFT JOIN `delivery_report_record` ON `delivery_report_record`.id = `order_finance_fact`.delivery_report_record_id WHERE `delivery_report_record`.record_status != 'status_updated'");
        $this->execute("UPDATE order_finance_fact INNER JOIN order_finance ON order_finance.order_id = order_finance_fact.order_id AND order_finance.payment IS NOT NULL INNER JOIN payment_order_delivery ON payment_order_delivery.name = order_finance.payment SET order_finance_fact.payment_id = payment_order_delivery.id WHERE order_finance_fact.payment_id IS NULL AND payment_order_delivery.id IS NOT NULL");

        $usdCurrencyId = (new \yii\db\Query())->from('currency')
            ->where(['char_code' => 'USD'])
            ->select('id')
            ->scalar($this->getDb());
        $rateFields = [
            'price_cod',
            'price_storage',
            'price_fulfilment',
            'price_packing',
            'price_package',
            'price_delivery',
            'price_redelivery',
            'price_delivery_back',
            'price_delivery_return',
            'price_address_correction',
            'price_cod_service',
            'price_vat'
        ];
        foreach ($rateFields as $field) {
            $this->update('order_finance_fact', ["{$field}_currency_rate" => null], [
                'OR',
                [
                    'AND',
                    [
                        '!=',
                        "{$field}_currency_id",
                        $usdCurrencyId
                    ],
                    ["{$field}_currency_rate" => 1]
                ],
                ["{$field}_currency_rate" => 0]
            ]);
        }

        $currencyIdFields = [
            'price_cod' => 'price_cod_currency_id',
            'price_storage' => 'price_storage_currency_id',
            'price_fulfilment' => 'price_fulfilment_currency_id',
            'price_packing' => 'price_packing_currency_id',
            'price_package' => 'price_package_currency_id',
            'price_address_correction' => 'price_address_correction_currency_id',
            'price_delivery' => 'price_delivery_currency_id',
            'price_redelivery' => 'price_redelivery_currency_id',
            'price_delivery_back' => 'price_delivery_back_currency_id',
            'price_delivery_return' => 'price_delivery_return_currency_id',
            'price_cod_service' => 'price_cod_service_currency_id',
            'price_vat' => 'price_vat_currency_id',
        ];
        $currencyRateRelationMap = [
            'price_cod_currency_id' => 'price_cod_currency_rate',
            'price_storage_currency_id' => 'price_storage_currency_rate',
            'price_fulfilment_currency_id' => 'price_fulfilment_currency_rate',
            'price_packing_currency_id' => 'price_packing_currency_rate',
            'price_package_currency_id' => 'price_package_currency_rate',
            'price_address_correction_currency_id' => 'price_address_correction_currency_rate',
            'price_delivery_currency_id' => 'price_delivery_currency_rate',
            'price_redelivery_currency_id' => 'price_redelivery_currency_rate',
            'price_delivery_return_currency_id' => 'price_delivery_return_currency_rate',
            'price_delivery_back_currency_id' => 'price_delivery_back_currency_rate',
            'price_cod_service_currency_id' => 'price_cod_service_currency_rate',
            'price_vat_currency_id' => 'price_vat_currency_rate',
        ];

        $lastCurrencyId = (new \yii\db\Query())->from('currency')->select('MAX(id)')->scalar($this->getDb());
        $countryIds = (new \yii\db\Query())->from('country')->select('id')->orderBy('id')->column($this->getDb());
        foreach ($countryIds as $countryId) {
            $this->execute("UPDATE `order_finance_prediction` LEFT JOIN `order` ON `order`.id = `order_finance_prediction`.order_id LEFT JOIN `country` ON `country`.id = `order`.country_id SET `order_finance_prediction`.price_cod_currency_id = COALESCE(`order`.price_currency, `country`.currency_id) WHERE `order`.country_id = {$countryId} AND (`order_finance_prediction`.price_cod_currency_id = 0 OR `order_finance_prediction`.price_cod_currency_id > $lastCurrencyId OR `order_finance_prediction`.price_cod_currency_id = `order_finance_prediction`.price_cod OR `order_finance_prediction`.price_cod_currency_id IS NULL)");
            foreach ($currencyIdFields as $valueField => $field) {
                $this->execute("UPDATE `order_finance_fact` LEFT JOIN `order` on `order`.id = `order_finance_fact`.order_id INNER JOIN `order_finance_prediction` on `order_finance_prediction`.order_id = `order_finance_fact`.order_id SET `order_finance_fact`.{$field} = NULL, `order_finance_fact`.{$currencyRateRelationMap[$field]} = NULL WHERE `order`.country_id = {$countryId} AND (`order_finance_fact`.{$field} = 0 OR `order_finance_fact`.{$field} > $lastCurrencyId OR `order_finance_fact`.{$field} = `order_finance_prediction`.{$valueField})");
                $this->execute("UPDATE `order_finance_fact` LEFT JOIN `order` on `order`.id = `order_finance_fact`.order_id INNER JOIN `order_finance_prediction` on `order_finance_prediction`.order_id = `order_finance_fact`.order_id SET `order_finance_fact`.{$field} = `order_finance_prediction`.{$field} WHERE `order`.country_id = {$countryId} AND `order_finance_fact`.{$field} IS NULL AND (`order_finance_prediction`.{$field} IS NOT NULL AND `order_finance_prediction`.{$field} != 0 AND `order_finance_prediction`.{$field} <= {$lastCurrencyId})");
            }
        }

        foreach ($currencyIdFields as $valueField => $field) {
            $this->update('order_finance_fact', [$currencyRateRelationMap[$field] => 1], [$field => $usdCurrencyId]);
        }

        $transaction = $this->getDb()->beginTransaction();
        try {
            // Восстанавливаем currency_rates
            foreach ($countryIds as $countryId) {
                $query = (new \yii\db\Query())->from('order_finance_fact')
                    ->innerJoin('order', '`order`.id = `order_finance_fact`.order_id')
                    ->leftJoin('payment_order_delivery', '`payment_order_delivery`.id = `order_finance_fact`.payment_id')
                    ->leftJoin('delivery_request', '`delivery_request`.order_id = `order`.id')
                    ->where("`payment_order_delivery`.paid_at IS NULL AND `order`.country_id = {$countryId}")
                    ->groupBy(['order_finance_fact.order_id'])
                    ->select([
                        'paid_date' => new \yii\db\Expression("FROM_UNIXTIME(COALESCE(`delivery_request`.paid_at, `delivery_request`.done_at, `delivery_request`.updated_at), '%Y-%m-%d')"),
                        'order_id' => 'order_finance_fact.order_id'
                    ]);
                $condition = ['OR'];
                foreach ($currencyIdFields as $valueField => $field) {
                    $cond = "`order_finance_fact`.{$field} IS NOT NULL AND `order_finance_fact`.{$currencyRateRelationMap[$field]} IS NULL";
                    $condition[] = $cond;
                    $query->addSelect([$field => new \yii\db\Expression($cond)]);
                }
                $query->andWhere($condition);


                $this->execute("CREATE TEMPORARY TABLE order_finance_fact_temp " . $query->createCommand()->rawSql);

                foreach ($currencyIdFields as $valueField => $field) {
                    $query = (new \yii\db\Query())->from('order_finance_fact')
                        ->innerJoin('order', '`order`.id = `order_finance_fact`.order_id')
                        ->innerJoin('payment_order_delivery', '`payment_order_delivery`.id = `order_finance_fact`.payment_id AND payment_order_delivery.paid_at is not null')
                        ->where("`order_finance_fact`.{$field} IS NOT NULL AND `order`.country_id = {$countryId} AND (`order_finance_fact`.{$currencyRateRelationMap[$field]} IS NULL)")
                        ->groupBy(['order_finance_fact.payment_id'])
                        ->select([
                            'payment_id' => 'order_finance_fact.payment_id',
                            'paid_at' => 'payment_order_delivery.paid_at',
                            'currencies' => new \yii\db\Expression("GROUP_CONCAT(DISTINCT `order_finance_fact`.{$field})")
                        ]);

                    $payments = $query->all();

                    foreach ($payments as $payment) {
                        $currencies = explode(',', $payment['currencies']);
                        $paymentDate = date('Y-m-d', $payment['paid_at']);
                        foreach ($currencies as $currencyId) {
                            if ($currencyId == $usdCurrencyId) {
                                continue;
                            }
                            $currencyRate = $this->getDb()
                                ->createCommand("SELECT get_currency_rate_for_date({$currencyId}, '{$paymentDate}')")
                                ->queryScalar();
                            $this->execute("UPDATE `order_finance_fact` SET `order_finance_fact`.{$currencyRateRelationMap[$field]} = {$currencyRate} WHERE `order_finance_fact`.payment_id = {$payment['payment_id']} AND `order_finance_fact`.{$field} = {$currencyId} AND (`order_finance_fact`.{$currencyRateRelationMap[$field]} IS NULL)");
                        }
                    }

                    $query = (new \yii\db\Query())->from('order_finance_fact')
                        ->innerJoin('order_finance_fact_temp', '`order_finance_fact_temp`.order_id = `order_finance_fact`.order_id')
                        ->where("`order_finance_fact_temp`.{$field} = 1")
                        ->groupBy(['order_finance_fact_temp.paid_date'])
                        ->select([
                            'paid_date' => "order_finance_fact_temp.paid_date",
                            'currencies' => new \yii\db\Expression("GROUP_CONCAT(DISTINCT `order_finance_fact`.{$field})")
                        ]);

                    $paidDates = $query->all();

                    foreach ($paidDates as $paidDate) {
                        $currencies = explode(',', $paidDate['currencies']);
                        $paymentDate = empty($paidDate['paid_date']) ? null : $paidDate['paid_date'];
                        foreach ($currencies as $currencyId) {
                            if ($currencyId == $usdCurrencyId) {
                                continue;
                            }
                            $currencyRate = $this->getDb()
                                ->createCommand("SELECT get_currency_rate_for_date({$currencyId}, :param)", [':param' => $paymentDate])
                                ->queryScalar();
                            $sql = "UPDATE `order_finance_fact` INNER JOIN `order_finance_fact_temp` on `order_finance_fact_temp`.order_id = `order_finance_fact`.order_id SET `order_finance_fact`.{$currencyRateRelationMap[$field]} = {$currencyRate} WHERE `order_finance_fact_temp`.{$field} = 1 AND `order_finance_fact`.{$field} = {$currencyId} AND ";
                            if (!$paymentDate) {
                                $sql .= "`order_finance_fact_temp`.paid_date IS NULL";
                            } else {
                                $sql .= "`order_finance_fact_temp`.paid_date = '{$paymentDate}'";
                            }
                            $this->execute($sql);
                        }
                    }
                    $this->execute("UPDATE `order_finance_fact` LEFT JOIN `order` on `order`.id = `order_finance_fact`.order_id LEFT JOIN `delivery_request` ON `delivery_request`.order_id = `order`.id LEFT JOIN `payment_order_delivery` ON `payment_order_delivery`.id = `order_finance_fact`.payment_id SET `order_finance_fact`.{$currencyRateRelationMap[$field]} = get_currency_rate_for_date(`order_finance_fact`.{$field}, FROM_UNIXTIME(COALESCE (`delivery_request`.paid_at, `delivery_request`.done_at, `delivery_request`.updated_at), '%Y-%m-%d')) WHERE `payment_order_delivery`.paid_at IS NULL AND `order_finance_fact`.{$field} IS NOT NULL AND `order`.country_id = {$countryId} AND (`order_finance_fact`.{$currencyRateRelationMap[$field]} IS NULL)");
                }
                $this->execute("DROP TABLE order_finance_fact_temp");
            }
            $transaction->commit();
        } catch (Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
}
