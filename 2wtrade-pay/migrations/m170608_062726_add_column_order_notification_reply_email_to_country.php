<?php

use yii\db\Migration;

/**
 * Handles adding column_order_notification_reply_email to table `country`.
 */
class m170608_062726_add_column_order_notification_reply_email_to_country extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('country', 'order_notification_reply_email', $this->string(100));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('country', 'order_notification_reply_email');
    }
}
