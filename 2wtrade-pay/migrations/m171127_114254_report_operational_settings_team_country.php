<?php
use app\components\CustomMigration as Migration;

/**
 * Class m171127_114254_report_operational_settings_team_country
 */
class m171127_114254_report_operational_settings_team_country extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('report_operational_settings_team_country', [
            'team_id' => $this->integer()->notNull(),
            'country_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $this->tableOptions);

        $this->createTable('report_operational_settings_team_country_user', [
            'user_id' => $this->integer()->notNull(),
            'team_id' => $this->integer()->notNull(),
            'country_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $this->tableOptions);

        $this->addPrimaryKey('pk_team_country', 'report_operational_settings_team_country', ['team_id', 'country_id']);
        $this->addForeignKey('fk_team', 'report_operational_settings_team_country', 'team_id', 'report_operational_settings_team', 'id', Migration::CASCADE, Migration::CASCADE);
        $this->addForeignKey('fk_country', 'report_operational_settings_team_country', 'country_id', 'country', 'id', Migration::CASCADE, Migration::CASCADE);

        $this->addPrimaryKey('pk_team_country_user', 'report_operational_settings_team_country_user', ['user_id', 'team_id', 'country_id']);
        $this->addForeignKey('fk_team_id', 'report_operational_settings_team_country_user', 'team_id', 'report_operational_settings_team', 'id', Migration::CASCADE, Migration::CASCADE);
        $this->addForeignKey('fk_country_id', 'report_operational_settings_team_country_user', 'country_id', 'country', 'id', Migration::CASCADE, Migration::CASCADE);
        $this->addForeignKey('fk_user_id', 'report_operational_settings_team_country_user', 'user_id', 'user', 'id', Migration::CASCADE, Migration::CASCADE);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('report_operational_settings_team_country');
        $this->dropTable('report_operational_settings_team_country_user');
    }
}
