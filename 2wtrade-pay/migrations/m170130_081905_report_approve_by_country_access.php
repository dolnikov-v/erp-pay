<?php
use app\components\CustomMigration as Migration;
use \yii\db\Query;

/**
 * Class m170130_081905_report_approve_by_country_access
 */
class m170130_081905_report_approve_by_country_access extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        $query = new Query();

        $is_businessanalyst = $query->select('*')->from($this->authManager->itemTable)
            ->where(['name' => 'business_analyst', 'type' => 1])
            ->one();

        if (!$is_businessanalyst) {
            echo "Error: No business_analyst exists";
            die();
        }

        $is_callcentermanager = $query->select('*')->from($this->authManager->itemTable)
            ->where(['name' => 'callcenter.manager', 'type' => 1])
            ->one();

        if (!$is_callcentermanager) {
            echo "Error: No callcenter.manager exists";
            die();
        }

        $is_report = $query->select('*')->from($this->authManager->itemTable)
            ->where(['name' => 'report.approvebycity.index', 'type' => 2])
            ->one();

        if (!$is_report) {
            $this->insert($this->authManager->itemTable, [
                'name' => 'report.approvebycity.index',
                'type' => 2,
                'description' => 'report.approvebycity.index',
                'created_at' => time(),
                'updated_at' => time()]);
        }

        $is_br = $query->select('*')->from($this->authManager->itemChildTable)->where([
            'parent' => 'business_analyst',
            'child' => 'report.approvebycity.index'
        ])->one();

        if (!$is_br) {
            $this->insert($this->authManager->itemChildTable, [
                'parent' => 'business_analyst',
                'child' => 'report.approvebycity.index'
            ]);
        }

        $is_cm = $query->select('*')->from($this->authManager->itemChildTable)->where([
            'parent' => 'callcenter.manager',
            'child' => 'report.approvebycity.index'
        ])->one();

        if (!$is_cm) {
            $this->insert($this->authManager->itemChildTable, [
                'parent' => 'callcenter.manager',
                'child' => 'report.approvebycity.index'
            ]);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->authManager->itemChildTable, [
            'parent' => 'business_analyst',
            'child' => 'report.approvebycity.index'
        ]);

        $this->delete($this->authManager->itemChildTable, [
            'parent' => 'callcenter.manager',
            'child' => 'report.approvebycity.index'
        ]);
    }
}
