<?php
use app\components\CustomMigration as Migration;
use app\models\Notification;

/**
 * Class m170529_045040_edit_notification_
 */
class m170529_045040_edit_notification_ extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->update(Notification::tableName(), ['description' => 'Не удалось добавить нового лида от Adcombo. Ошибка: {exeption}'], ['trigger' => 'lead.not.added']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->update(Notification::tableName(), ['description' => 'Не удалось добавить нового лида от Adcombo'], ['trigger' => 'lead.not.added']);
    }
}
