<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170629_082231_packager_permission_for_country_curator_fix
 */
class m170629_082231_packager_permission_for_country_curator_fix extends Migration
{

    protected $permissions = [
        'packager.control.activateautosend' => 'packager.control.activateautosending',
        'packager.control.deactivateautosend' => 'packager.control.deactivateautosending',
    ];

    public function safeUp()
    {
        foreach ($this->permissions as $old => $new) {
            $this->update($this->authManager->itemTable,
                [
                    'name' => $new,
                    'description' => $new,
                ],
                [
                    'name' => $old,
                    'description' => $old,
                ]
            );

            $this->update($this->authManager->itemChildTable,
                [
                    'child' => $new,
                ],
                [
                    'parent' => 'country.curator',
                    'child' => $old,
                ]
            );
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach ($this->permissions as $old => $new) {
            $this->update($this->authManager->itemTable,
                [
                    'name' => $old,
                    'description' => $old,
                ],
                [
                    'name' => $new,
                    'description' => $new,
                ]
            );

            $this->update($this->authManager->itemChildTable,
                [
                    'child' => $old,
                ],
                [
                    'parent' => 'country.curator',
                    'child' => $new,
                ]
            );
        }
    }
}
