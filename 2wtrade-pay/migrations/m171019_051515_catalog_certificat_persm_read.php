<?php
use app\components\CustomMigration as Migration;
use yii\db\Query;

/**
 * Class m171019_051515_catalog_certificat_persm_read
 */
class m171019_051515_catalog_certificat_persm_read extends Migration
{
    const RULES = [
        'catalog.certificate.index',
        'catalog.certificate.view',
        'catalog.certificate.download',
    ];

    const ROLES = [
        'auditor',
        'auditor.helper',
    ];


    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        $query = new Query();

        foreach (self::RULES as $rule) {

            $is_rule = $query->select('*')->from($this->authManager->itemTable)
                ->where(['name' => $rule, 'type' => 2])
                ->one();

            if (!$is_rule) {
                $this->insert($this->authManager->itemTable, [
                    'name' => $rule,
                    'description' => $rule,
                    'type' => 2,
                    'created_at' => time(),
                    'updated_at' => time()]);
            }

            foreach (self::ROLES as $role) {

                $is_role = $query->select('*')->from($this->authManager->itemTable)
                    ->where(['name' => $role, 'type' => 1])
                    ->one();

                if (!$is_role) {
                    $this->insert($this->authManager->itemTable, [
                        'name' => $role,
                        'type' => 1,
                        'description' => $role,
                        'created_at' => time(),
                        'updated_at' => time()]);
                }

                $is_can = $query->select('*')->from($this->authManager->itemChildTable)->where([
                    'parent' => $role,
                    'child' => $rule
                ])->one();

                if (!$is_can) {
                    $this->insert($this->authManager->itemChildTable, [
                        'parent' => $role,
                        'child' => $rule
                    ]);
                }
            }
        }
    }


    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach (self::RULES as $rule) {
            foreach (self::ROLES as $role) {
                $this->delete($this->authManager->itemChildTable, [
                    'parent' => $role,
                    'child' => $rule
                ]);
            }
        }
    }
}
