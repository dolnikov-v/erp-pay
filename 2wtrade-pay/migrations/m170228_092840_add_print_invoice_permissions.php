<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170228_092840_add_print_invoice_permissions
 */
class m170228_092840_add_print_invoice_permissions extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        $permission = $auth->createPermission('order.index.print-invoice');
        $permission->description = 'Print one invoice from list';
        $permission->type = 2;
        $auth->add($permission);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;
        $permission = $auth->getPermission('order.index.print-invoice');
        $auth->remove($permission);
    }
}
