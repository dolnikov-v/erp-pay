<?php

use yii\db\Migration;

/**
 * Handles adding get_operator_data_by_api to table `call_center`.
 */
class m170713_065632_add_get_operator_data_by_api_to_call_center extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('call_center', 'get_operator_work_time_by_api', $this->smallInteger()->defaultValue(1)->after('active'));
        $this->addColumn('call_center', 'get_operator_number_of_call_by_api', $this->smallInteger()->defaultValue(1)->after('active'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('call_center', 'get_operator_work_time_by_api');
        $this->dropColumn('call_center', 'get_operator_number_of_call_by_api');
    }
}
