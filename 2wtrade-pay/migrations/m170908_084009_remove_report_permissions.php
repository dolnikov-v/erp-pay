<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170908_084009_remove_report_permissions
 */
class m170908_084009_remove_report_permissions extends Migration
{
    /**
     * @var array
     */
    protected $permissions = [
        'report.short.index',
        'report.deliveryterms.index',
        'report.deliveryzipcodesstats.index',
        'report.deliveryclosedperiods.index',
        'report.product.index',
        'report.topproduct.index',
        'report.finance.index',
        'report.inprogress.index',
        'report.topmanager.index',
        'report.advertising.index',
        'report.daily.index',
        'report.approved.index',
        'report.reportadcombo.index',
        'report.reportcallcenter.index',
        'report.reportcallcenter.index',
        'report.consolidatebycountry.index',
        'report.consolidatesale.index',
        'report.callcenteroperatorsonline.index',
        'report.approvebycountry.index',
        'report.buyoutbycountry.index',
        'report.buyoutbycountry.index',
        'report.averagecheckbycountry.index',
        'report.leadbycountry.index',
        'report.forecastleadbyweekdays.index',
        'report.forecastworkloadoperatorsbyhours.index',
        'report.delivery.delaybycountry.index',
        'report.sale.index',
        'report.shipment.index',
        'report.costprice.index',
        'report.webmaster.index',
        'report.foreignoperator.index',
        'report.operatorworkdays.index',
        'report.operatorspenalty.index',
        'report.reportstandartsales.index',
        'report.reportstandartdelivery.index',
        'report.accountingcosts.index',
        'report.productrating.index',
        'report.activelanding.index',
        'report.validatefinance.index',
        'report.checkundelivery.index',
        'report.smspollhistory.index',
        'report.smsnotification.index',
        'report.forecastdelivery.index',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        foreach ($this->permissions as $permission) {
            $this->delete('auth_item_child', ['child' => $permission]);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        // Восстановливать вручную
    }
}
