<?php
use app\components\CustomMigration as Migration;
use app\models\Notification;
use app\modules\delivery\models\Delivery;

/**
 * Class m180720_115505_check_contract_data
 */
class m180720_115505_check_contract_data extends Migration
{

    public $translate = [
        'Заказы не могут быть отправлены в КС. Заполните данные контракта.' => 'Distributor',
    ];


    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        $this->addColumn(Delivery::tableName(), 'sent_notification_no_contract_at', $this->integer());

        $notification = NotificatioN::findOne(['trigger' => Notification::TRIGGER_DELIVERY_HAS_NO_CONTRACT]);

        if (!($notification instanceof Notification)) {
            $notification = new Notification([
                'description' => 'Заказы не могут быть отправлены в КС {delivery}. Данные контракта не заполнены.',
                'trigger' => Notification::TRIGGER_DELIVERY_HAS_NO_CONTRACT,
                'type' => Notification::TYPE_WARNING,
                'group' => Notification::GROUP_REPORT,
                'active' => Notification::ACTIVE,
            ]);
            $notification->save(false);
        }

        foreach ($this->translate as $ruText => $enText) {
            $this->insert('i18n_source_message', ['category' => 'common', 'message' => $ruText]);
            $id = Yii::$app->db->getLastInsertID();
            $this->insert('i18n_message', ['id' => $id, 'language' => 'en-US', 'translation' => $enText]);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

        $this->dropColumn(Delivery::tableName(), 'sent_notification_no_contract_at');

        $notification = Notification::findOne(['trigger' => Notification::TRIGGER_DELIVERY_HAS_NO_CONTRACT]);

        if ($notification instanceof Notification) {
            $notification->delete();
        }

        foreach ($this->translate as $ruText => $enText) {
            $this->delete('i18n_source_message', ['category' => 'common', 'message' => $ruText]);
        }
    }
}
