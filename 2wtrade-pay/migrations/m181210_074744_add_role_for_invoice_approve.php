<?php
use app\components\PermissionMigration as Migration;

/**
 * Class m181210_074744_add_role_for_invoice_approve
 */
class m181210_074744_add_role_for_invoice_approve extends Migration
{
    protected $permissions = [
        'report.invoice.approveinvoice',
    ];

    protected $roles = [
        'finance.manager' => [
            'report.invoice.approveinvoice',
        ],
        'finance.director' => [
            'report.invoice.approveinvoice',
        ],
        'admin' => [
            'report.invoice.approveinvoice',
        ],
        'project.manager' => [
            'report.invoice.approveinvoice',
        ],
    ];
}
