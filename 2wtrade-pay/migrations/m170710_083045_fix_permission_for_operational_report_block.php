<?php

use app\components\CustomMigration as Migration;

/**
 * Class m170710_083045_fix_permission_for_operational_report_block
 */
class m170710_083045_fix_permission_for_operational_report_block extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        $this->delete($this->authManager->itemChildTable, ['child' => 'reportoperational.settingscountries.index', 'parent' => 'business_analyst']);
        $this->delete($this->authManager->itemChildTable, ['child' => 'reportoperational.settingscountries.index', 'parent' => 'finance.director']);
        $this->delete('{{%auth_item}}', ['name' => 'reportoperational.settingscountries.index']);

        // Доступ к настройкам стран операционных отчетов
        $this->insert('{{%auth_item}}',array(
            'name'=>'reportoperational.settingscountry.index',
            'type' => '2',
            'description' => 'reportoperational.settingscountry.index',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'finance.director',
            'child' => 'reportoperational.settingscountry.index'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'business_analyst',
            'child' => 'reportoperational.settingscountry.index'
        ));

        // Доступ к редактированию стран операционных отчетов
        $this->insert('{{%auth_item}}',array(
            'name'=>'reportoperational.settingscountry.update',
            'type' => '2',
            'description' => 'reportoperational.settingscountry.update',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'finance.director',
            'child' => 'reportoperational.settingscountry.update'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'business_analyst',
            'child' => 'reportoperational.settingscountry.update'
        ));

        // Доступ к удалению стран операционных отчетов
        $this->insert('{{%auth_item}}',array(
            'name'=>'reportoperational.settingscountry.delete',
            'type' => '2',
            'description' => 'reportoperational.settingscountry.delete',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'finance.director',
            'child' => 'reportoperational.settingscountry.delete'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'business_analyst',
            'child' => 'reportoperational.settingscountry.delete'
        ));

        // Доступ к активации стран операционных отчетов
        $this->insert('{{%auth_item}}',array(
            'name'=>'reportoperational.settingscountry.activate',
            'type' => '2',
            'description' => 'reportoperational.settingscountry.activate',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'finance.director',
            'child' => 'reportoperational.settingscountry.activate'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'business_analyst',
            'child' => 'reportoperational.settingscountry.activate'
        ));

        // Доступ к деактивации стран операционных отчетов
        $this->insert('{{%auth_item}}',array(
            'name'=>'reportoperational.settingscountry.deactivate',
            'type' => '2',
            'description' => 'reportoperational.settingscountry.deactivate',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'finance.director',
            'child' => 'reportoperational.settingscountry.deactivate'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'business_analyst',
            'child' => 'reportoperational.settingscountry.deactivate'
        ));


        // Доступ к созданию команд операционных отчетов
        $this->insert('{{%auth_item}}',array(
            'name'=>'reportoperational.settingsteam.create',
            'type' => '2',
            'description' => 'reportoperational.settingsteam.create',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'finance.director',
            'child' => 'reportoperational.settingsteam.create'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'business_analyst',
            'child' => 'reportoperational.settingsteam.create'
        ));

        // Доступ к редактированию команд операционных отчетов
        $this->insert('{{%auth_item}}',array(
            'name'=>'reportoperational.settingsteam.update',
            'type' => '2',
            'description' => 'reportoperational.settingsteam.update',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'finance.director',
            'child' => 'reportoperational.settingsteam.update'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'business_analyst',
            'child' => 'reportoperational.settingsteam.update'
        ));

        // Доступ к удалению команд операционных отчетов
        $this->insert('{{%auth_item}}',array(
            'name'=>'reportoperational.settingsteam.delete',
            'type' => '2',
            'description' => 'reportoperational.settingsteam.delete',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'finance.director',
            'child' => 'reportoperational.settingsteam.delete'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'business_analyst',
            'child' => 'reportoperational.settingsteam.delete'
        ));

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->authManager->itemChildTable, ['child' => 'reportoperational.settingscountry.index', 'parent' => 'business_analyst']);
        $this->delete($this->authManager->itemChildTable, ['child' => 'reportoperational.settingscountry.index', 'parent' => 'finance.director']);
        $this->delete('{{%auth_item}}', ['name' => 'reportoperational.settingscountry.index']);

        $this->delete($this->authManager->itemChildTable, ['child' => 'reportoperational.settingscountry.update', 'parent' => 'business_analyst']);
        $this->delete($this->authManager->itemChildTable, ['child' => 'reportoperational.settingscountry.update', 'parent' => 'finance.director']);
        $this->delete('{{%auth_item}}', ['name' => 'reportoperational.settingscountry.update']);

        $this->delete($this->authManager->itemChildTable, ['child' => 'reportoperational.settingscountry.delete', 'parent' => 'business_analyst']);
        $this->delete($this->authManager->itemChildTable, ['child' => 'reportoperational.settingscountry.delete', 'parent' => 'finance.director']);
        $this->delete('{{%auth_item}}', ['name' => 'reportoperational.settingscountry.delete']);

        $this->delete($this->authManager->itemChildTable, ['child' => 'reportoperational.settingscountry.activate', 'parent' => 'business_analyst']);
        $this->delete($this->authManager->itemChildTable, ['child' => 'reportoperational.settingscountry.activate', 'parent' => 'finance.director']);
        $this->delete('{{%auth_item}}', ['name' => 'reportoperational.settingscountry.activate']);

        $this->delete($this->authManager->itemChildTable, ['child' => 'reportoperational.settingscountry.deactivate', 'parent' => 'business_analyst']);
        $this->delete($this->authManager->itemChildTable, ['child' => 'reportoperational.settingscountry.deactivate', 'parent' => 'finance.director']);
        $this->delete('{{%auth_item}}', ['name' => 'reportoperational.settingscountry.deactivate']);


        $this->delete($this->authManager->itemChildTable, ['child' => 'reportoperational.settingsteam.create', 'parent' => 'business_analyst']);
        $this->delete($this->authManager->itemChildTable, ['child' => 'reportoperational.settingsteam.create', 'parent' => 'finance.director']);
        $this->delete('{{%auth_item}}', ['name' => 'reportoperational.settingsteam.create']);

        $this->delete($this->authManager->itemChildTable, ['child' => 'reportoperational.settingsteam.update', 'parent' => 'business_analyst']);
        $this->delete($this->authManager->itemChildTable, ['child' => 'reportoperational.settingsteam.update', 'parent' => 'finance.director']);
        $this->delete('{{%auth_item}}', ['name' => 'reportoperational.settingsteam.update']);

        $this->delete($this->authManager->itemChildTable, ['child' => 'reportoperational.settingsteam.delete', 'parent' => 'business_analyst']);
        $this->delete($this->authManager->itemChildTable, ['child' => 'reportoperational.settingsteam.delete', 'parent' => 'finance.director']);
        $this->delete('{{%auth_item}}', ['name' => 'reportoperational.settingsteam.delete']);

        // Доступ к настройкам стран операционных отчетов
        $this->insert('{{%auth_item}}',array(
            'name'=>'reportoperational.settingscountries.index',
            'type' => '2',
            'description' => 'reportoperational.settingscountries.index',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'finance.director',
            'child' => 'reportoperational.settingscountries.index'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'business_analyst',
            'child' => 'reportoperational.settingscountries.index'
        ));
    }
}
