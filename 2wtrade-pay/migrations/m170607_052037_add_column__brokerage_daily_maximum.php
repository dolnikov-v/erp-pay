<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryZipcodes;

/**
 * Class m170607_052037_add_column__brokerage_daily_maximum
 */
class m170607_052037_add_column__brokerage_daily_maximum extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn(DeliveryZipcodes::tableName(), "brokerage_daily_maximum", "integer(11) default '0' after delivery_id");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn(DeliveryZipcodes::tableName(), "brokerage_daily_maximum");
    }
}
