<?php

use app\components\CustomMigration as Migration;
use app\modules\deliveryreport\models\DeliveryReport;
use app\modules\deliveryreport\models\DeliveryReportRecord;
use app\modules\order\models\Order;
use app\modules\order\models\OrderFinancePretension;

/**
 * Class m180801_045317_add_table_order_finance_pretension
 */
class m180801_045317_add_table_order_finance_pretension extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(OrderFinancePretension::tableName(), [
            'id' => $this->primaryKey(),
            'type' => $this->integer()->notNull(),
            'status' => $this->string()->notNull()->defaultValue(OrderFinancePretension::PRETENSION_STATUS_IN_PROGRESS),
            'comment' => $this->text(),
            'order_id' => $this->integer()->notNull(),
            'delivery_report_record_id' => $this->integer()->notNull(),
            'delivery_report_id' => $this->integer()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);

        $this->addForeignKey(null, OrderFinancePretension::tableName(), 'order_id', Order::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, OrderFinancePretension::tableName(), 'delivery_report_record_id', DeliveryReportRecord::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, OrderFinancePretension::tableName(), 'delivery_report_id', DeliveryReport::tableName(), 'id', self::CASCADE, self::RESTRICT);


    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable(OrderFinancePretension::tableName());
    }
}
