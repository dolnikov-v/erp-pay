<?php

use yii\db\Migration;
use app\modules\widget\models\WidgetType;
use app\modules\widget\models\WidgetRole;

/**
 * Handles the creation for table `widget_range_products_from_countries`.
 */
class m170216_034401_create_widget_range_products_from_countries extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert(WidgetType::tableName(), [
            'code' => 'range_products_from_countries',
            'name' => 'Ассортимент',
            'status' => WidgetType::STATUS_ACTIVE
        ]);


        $type = WidgetType::find()
            ->where(['code' => 'range_products_from_countries'])
            ->one();

        $this->insert(WidgetRole::tableName(), [
            'role' => 'business_analyst',
            'type_id' => $type->id
        ]);

        $this->insert('i18n_source_message', ['category' => 'common', 'message' => 'Ассортимент']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $type = WidgetType::find()
            ->where(['code' => 'range_products_from_countries'])
            ->one();

        $this->delete(WidgetRole::tableName(), [
            'role' => 'business_analyst',
            'type_id' => $type->id,
        ]);

        $this->delete(WidgetType::tableName(), ['code' => 'range_products_from_countries']);
    }
}
