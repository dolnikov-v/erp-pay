<?php
use app\components\CustomMigration as Migration;

/**
 * Class m181217_041615_add_connect_user_and_skype_user
 */
class m181217_041615_add_connect_user_and_skype_user extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('skype_user_link', [
            'user_id' => $this->integer()->notNull(),
            'skype_user_id' => $this->integer()->notNull(),
        ]);
        $this->addForeignKey(null, 'skype_user_link', 'user_id', 'user', 'id', self::CASCADE, self::CASCADE);
        $this->addForeignKey(null, 'skype_user_link', 'skype_user_id', 'skype_user', 'id', self::CASCADE, self::CASCADE);
        $this->addPrimaryKey(null, 'skype_user_link', ['user_id', 'skype_user_id']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('skype_user_link');
    }
}
