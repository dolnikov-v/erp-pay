<?php
use app\components\CustomMigration as Migration;

/**
 * Class m180821_111152_delivery_not_products_by_zip
 */
class m180821_111152_delivery_not_products_by_zip extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('delivery_not_products_by_zip', [
            'zipcodes_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
            'created_at' => $this->integer(),
        ], $this->tableOptions);
        $this->addPrimaryKey(null, 'delivery_not_products_by_zip', ['zipcodes_id', 'product_id']);
        $this->addForeignKey(null, 'delivery_not_products_by_zip', 'zipcodes_id', 'delivery_zipcodes', 'id', self::CASCADE, self::CASCADE);
        $this->addForeignKey(null, 'delivery_not_products_by_zip', 'product_id', 'product', 'id', self::CASCADE, self::CASCADE);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('delivery_not_products_by_zip');
    }
}
