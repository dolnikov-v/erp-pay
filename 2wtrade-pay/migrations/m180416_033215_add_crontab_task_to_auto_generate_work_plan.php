<?php

use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Handles adding crontab_task to table `auto_generate_work_plan`.
 */
class m180416_033215_add_crontab_task_to_auto_generate_work_plan extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = new CrontabTask();
        $crontabTask->name = 'auto_generate_work_plan';
        $crontabTask->description = 'Автогенерация рабочего плана 25 числа на следующий месяц';
        $crontabTask->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = CrontabTask::find()
            ->byName('auto_generate_work_plan')
            ->one();

        if ($crontabTask) {
            $crontabTask->delete();
        }

    }
}
