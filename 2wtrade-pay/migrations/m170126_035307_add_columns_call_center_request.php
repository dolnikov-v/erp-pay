<?php
use app\components\CustomMigration as Migration;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\delivery\models\DeliveryRequest;
/**
 * Class m170126_035307_add_columns_call_center_request
 */
class m170126_035307_add_columns_call_center_request extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(CallCenterRequest::tableName(),'cc_send_response',$this->text());
        $this->addCommentOnColumn(CallCenterRequest::tableName(), 'cc_send_response', 'Last call center response when sending lead');
        $this->addColumn(CallCenterRequest::tableName(),'cc_send_response_at',$this->integer(11));
        $this->addCommentOnColumn(CallCenterRequest::tableName(), 'cc_send_response_at', 'Time call center response when sending lead');
        $this->addColumn(CallCenterRequest::tableName(),'cc_update_response',$this->text());
        $this->addCommentOnColumn(CallCenterRequest::tableName(), 'cc_update_response', 'Last call center response when update status');
        $this->addColumn(CallCenterRequest::tableName(),'cc_update_response_at',$this->integer(11));
        $this->addCommentOnColumn(CallCenterRequest::tableName(), 'cc_update_response_at', 'Time call center response when update status');
        $this->addColumn(DeliveryRequest::tableName(),'cs_send_response',$this->text());
        $this->addCommentOnColumn(DeliveryRequest::tableName(), 'cs_send_response', 'Last courier service response when sending order');
        $this->addColumn(DeliveryRequest::tableName(),'cs_send_response_at',$this->integer(11));
        $this->addCommentOnColumn(DeliveryRequest::tableName(), 'cs_send_response_at', 'Time courier service response when sending order');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(CallCenterRequest::tableName(),'cc_send_response');
        $this->dropColumn(CallCenterRequest::tableName(),'cc_send_response_at');
        $this->dropColumn(CallCenterRequest::tableName(),'cc_update_response');
        $this->dropColumn(CallCenterRequest::tableName(),'cc_update_response_at');
        $this->dropColumn(DeliveryRequest::tableName(),'cs_send_response');
        $this->dropColumn(DeliveryRequest::tableName(),'cs_send_response_at');
    }
}
