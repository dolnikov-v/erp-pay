<?php

use app\components\CustomMigration as Migration;

/**
 * Class m170717_075719_add_permission_export_for_operational_report_block
 */
class m170717_075719_add_permission_export_for_operational_report_block extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        // Доступ к экспорту в Excel отчетов по выкупам
        $this->insert('{{%auth_item}}',array(
            'name'=>'reportoperational.buyouts.exporttoexcel',
            'type' => '2',
            'description' => 'reportoperational.buyouts.exporttoexcel',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'finance.director',
            'child' => 'reportoperational.buyouts.exporttoexcel'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'business_analyst',
            'child' => 'reportoperational.buyouts.exporttoexcel'
        ));

        // Доступ к экспорту в Excel отчетов по продуктам
        $this->insert('{{%auth_item}}',array(
            'name'=>'reportoperational.products.exporttoexcel',
            'type' => '2',
            'description' => 'reportoperational.products.exporttoexcel',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'finance.director',
            'child' => 'reportoperational.products.exporttoexcel'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'business_analyst',
            'child' => 'reportoperational.products.exporttoexcel'
        ));

        // Доступ к экспорту в Excel отчетов по доставкам
        $this->insert('{{%auth_item}}',array(
            'name'=>'reportoperational.shippings.exporttoexcel',
            'type' => '2',
            'description' => 'reportoperational.shippings.exporttoexcel',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'finance.director',
            'child' => 'reportoperational.shippings.exporttoexcel'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'business_analyst',
            'child' => 'reportoperational.shippings.exporttoexcel'
        ));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->authManager->itemChildTable, ['child' => 'reportoperational.buyouts.exporttoexcel', 'parent' => 'business_analyst']);
        $this->delete($this->authManager->itemChildTable, ['child' => 'reportoperational.buyouts.exporttoexcel', 'parent' => 'finance.director']);
        $this->delete('{{%auth_item}}', ['name' => 'reportoperational.buyouts.exporttoexcel']);

        $this->delete($this->authManager->itemChildTable, ['child' => 'reportoperational.products.exporttoexcel', 'parent' => 'business_analyst']);
        $this->delete($this->authManager->itemChildTable, ['child' => 'reportoperational.products.exporttoexcel', 'parent' => 'finance.director']);
        $this->delete('{{%auth_item}}', ['name' => 'reportoperational.products.exporttoexcel']);

        $this->delete($this->authManager->itemChildTable, ['child' => 'reportoperational.shippings.exporttoexcel', 'parent' => 'business_analyst']);
        $this->delete($this->authManager->itemChildTable, ['child' => 'reportoperational.shippings.exporttoexcel', 'parent' => 'finance.director']);
        $this->delete('{{%auth_item}}', ['name' => 'reportoperational.shippings.exporttoexcel']);
    }
}
