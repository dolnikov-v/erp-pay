<?php
use app\components\CustomMigration as Migration;
use app\models\Notification;

/**
 * Class m160829_044628_add_notification_two_days
 */
class m160829_044628_add_notification_two_days extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $model = new Notification([
            'description' => 'Заказы с ошибкой от 1 до 2-х дней: созданные с {dateFrom} по {dateTo}, страна {country}, Отклонено курьерской службой {delivery_error}',
            'trigger' => 'report.order.two.days.error',
            'type' => Notification::TYPE_DANGER,
            'group' => Notification::GROUP_REPORT,
            'active' => Notification::ACTIVE,
        ]);

        $model->save();
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        /** @var Notification $model */
        $model = Notification::findOne(['trigger' => 'report.order.two.days.error']);

        if ($model) {
            $model->delete();
        }
    }
}
