<?php
use app\components\CustomMigration as Migration;
use yii\db\Query;

/**
 * Class m170410_103055_report_teams
 */
class m170410_103055_report_teams extends Migration
{
    const ROLE = 'callcenter.manager';

    const ROLE_NAME = 'Менеджер КЦ';

    const RULES = [
        'report.team.index'
    ];


    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $query = new Query();

        $is_role = $query->select('*')->from($this->authManager->itemTable)
            ->where(['name' => self::ROLE, 'type' => 1])
            ->one();

        if (!$is_role) {
            $this->insert($this->authManager->itemTable, [
                'name' => self::ROLE,
                'type' => 1,
                'description' => self::ROLE_NAME,
                'created_at' => time(),
                'updated_at' => time()]);
        }


        foreach (self::RULES as $rule) {

            $is_rule = $query->select('*')->from($this->authManager->itemTable)
                ->where(['name' => $rule, 'type' => 2])
                ->one();

            if (!$is_rule) {
                $this->insert($this->authManager->itemTable, [
                    'name' => $rule,
                    'description' => $rule,
                    'type' => 2,
                    'created_at' => time(),
                    'updated_at' => time()]);
            }

            $is_can = $query->select('*')->from($this->authManager->itemChildTable)->where([
                'parent' => self::ROLE,
                'child' => $rule
            ])->one();

            if (!$is_can) {
                $this->insert($this->authManager->itemChildTable, [
                    'parent' => self::ROLE,
                    'child' => $rule
                ]);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

        foreach (self::RULES as $rule) {
            $this->delete($this->authManager->itemChildTable, [
                'parent' => self::ROLE,
                'child' => $rule
            ]);
        }

        $this->delete($this->authManager->itemTable, [
            'name' => self::ROLE,
            'type' => 1
        ]);

    }
}
