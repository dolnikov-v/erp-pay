<?php

use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryApiClass;

/**
 * Class m170411_083109_add_delivery_fedex
 */
class m170411_083109_add_delivery_api_fedex extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $class = new DeliveryApiClass();
        $class->name = 'FedExApi';
        $class->active = 1;
        $class->class_path = '/fedex-api/src/FedExApi.php';
        $class->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        DeliveryApiClass::find()
            ->where([
                'name' => 'FedExApi',
                'class_path' => '/fedex-api/src/FedExApi.php'
            ])
            ->one()
            ->delete();
    }
}
