<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160802_040426_report_order
 */
class m160802_040426_report_order extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('report_order', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(11)->notNull(),
            'report_id' => $this->integer(11)->notNull(),
            'type' => $this->string(50)->notNull()
        ]);
        $this->createIndex(null, 'report_order', 'report_id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable("report_order");
    }
}
