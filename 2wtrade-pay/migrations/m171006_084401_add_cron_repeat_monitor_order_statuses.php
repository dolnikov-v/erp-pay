<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m171006_084401_add_cron_repeat_monitor_order_statuses
 */
class m171006_084401_add_cron_repeat_monitor_order_statuses extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = new CrontabTask();
        $crontabTask->name = 'repeat_monitor_order_statuses';
        $crontabTask->description = "Повторный мониторинг статусов заказов на скопление больше определенного времени и создание задач на исправление ситуации в Jira";
        $crontabTask->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        CrontabTask::findOne(['name' => 'repeat_monitor_order_statuses'])->delete();
    }
}
