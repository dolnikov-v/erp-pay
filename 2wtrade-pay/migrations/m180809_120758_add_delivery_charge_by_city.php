<?php
use app\components\CustomMigration as Migration;

/**
 * Class m180809_120758_add_delivery_charge_by_city
 */
class m180809_120758_add_delivery_charge_by_city extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('delivery_charges_calculator', [
            'name' => 'DeliveryChargeByCity',
            'class_path' => 'app\modules\delivery\components\charges\DeliveryChargeByCity',
            'active' => 1,
            'created_at' => time(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete('delivery_charges_calculator', ['name' => 'DeliveryChargeByCity']);
    }
}
