<?php

use yii\db\Migration;
use app\modules\order\models\Order;

/**
 * Handles adding columns to table `order_delivery_time_from_to`.
 */
class m161002_071843_add_columns_to_order_delivery_time_from_to extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn(Order::tableName(), 'delivery_time_to', $this->integer(11)->defaultValue(null)->after('updated_at'));
        $this->addColumn(Order::tableName(), 'delivery_time_from', $this->integer(11)->defaultValue(null)->after('updated_at'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn(Order::tableName(), 'delivery_time_to');
        $this->dropColumn(Order::tableName(), 'delivery_time_from');
    }
}
