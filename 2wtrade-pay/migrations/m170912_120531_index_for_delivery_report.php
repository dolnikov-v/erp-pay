<?php
use app\components\CustomMigration as Migration;
use app\modules\deliveryreport\models\DeliveryReport;
use app\modules\deliveryreport\models\DeliveryReportRecord;

/**
 * Class m170912_120531_index_for_delivery_report
 */
class m170912_120531_index_for_delivery_report extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createIndex('idx_delivery_report_record_tracking', DeliveryReportRecord::tableName(), 'tracking');
        $this->createIndex('idx_delivery_report_record_status', DeliveryReportRecord::tableName(), 'status');
        $this->createIndex('idx_delivery_report_record_record_status', DeliveryReportRecord::tableName(), 'record_status');
        $this->createIndex('idx_delivery_report_record_created_at', DeliveryReportRecord::tableName(), 'created_at');


        $this->createIndex('idx_delivery_report_payment_id', DeliveryReport::tableName(), 'payment_id');
        $this->createIndex('idx_delivery_report_status', DeliveryReport::tableName(), 'status');
        $this->createIndex('idx_delivery_report_type', DeliveryReport::tableName(), 'type');
        $this->createIndex('idx_delivery_report_created_at', DeliveryReport::tableName(), 'created_at');
        $this->createIndex('idx_delivery_report_priority', DeliveryReport::tableName(), 'priority');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropIndex('idx_delivery_report_record_tracking', DeliveryReportRecord::tableName());
        $this->dropIndex('idx_delivery_report_record_status', DeliveryReportRecord::tableName());
        $this->dropIndex('idx_delivery_report_record_record_status', DeliveryReportRecord::tableName());
        $this->dropIndex('idx_delivery_report_record_created_at', DeliveryReportRecord::tableName());

        $this->dropIndex('idx_delivery_report_payment_id', DeliveryReport::tableName());
        $this->dropIndex('idx_delivery_report_status', DeliveryReport::tableName());
        $this->dropIndex('idx_delivery_report_type', DeliveryReport::tableName());
        $this->dropIndex('idx_delivery_report_created_at', DeliveryReport::tableName());
        $this->dropIndex('idx_delivery_report_priority', DeliveryReport::tableName());
    }
}
