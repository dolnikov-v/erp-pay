<?php
use app\components\CustomMigration as Migration;
use app\models\Country;
use app\models\Currency;
use app\models\Timezone;

class m000000_000017_country extends Migration
{
    public function safeUp()
    {
        $this->createTable(Country::tableName(), [
            'id' => $this->primaryKey(),
            'name' => $this->string(32)->notNull(),
            'slug' => $this->string(3)->notNull(),
            'char_code' => $this->string(10)->defaultValue(null),
            'timezone_id' => $this->integer()->defaultValue(null),
            'currency_id' => $this->integer()->defaultValue(null),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $this->tableOptions);

        $this->addForeignKey(null, Country::tableName(), 'timezone_id', Timezone::tableName(), 'id', Migration::CASCADE, Migration::RESTRICT);
        $this->addForeignKey(null, Country::tableName(), 'currency_id', Currency::tableName(), 'id', Migration::CASCADE, Migration::RESTRICT);

        foreach ($this->countryFixture() as $key => $fixture) {
            $country = new Country($fixture);
            $country->save(false);
        }
    }

    public function safeDown()
    {
        $this->dropTable(Country::tableName());
    }

    /**
     * @return array
     */
    private function countryFixture()
    {
        return [
            [
                'name' => 'Индия',
                'slug' => 'in',
                'char_code' => 'IN',
            ],
            [
                'name' => 'Таиланд',
                'slug' => 'th',
                'char_code' => 'TH',
            ],
            [
                'name' => 'Мексика',
                'slug' => 'mx',
                'char_code' => 'MX',
            ],
        ];
    }
}
