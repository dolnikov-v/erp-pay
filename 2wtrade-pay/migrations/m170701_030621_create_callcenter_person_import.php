<?php

use yii\db\Migration;

/**
 * Handles the creation for table `callcenter_person_import`.
 */
class m170701_030621_create_callcenter_person_import extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('call_center_person_import', [
            'id' => $this->primaryKey(),
            'file_name' => $this->string(200)->notNull(),
            'original_file_name' => $this->string(200)->notNull(),
            'row_begin' => $this->integer(),
            'cell_login' => $this->integer(),
            'cell_full_name' => $this->integer(),
            'cell_country' => $this->integer(),
            'cell_position' => $this->integer(),
            'cell_salary' => $this->integer(),
            'cell_salary_usd' => $this->integer(),
            'position_map' => $this->text(),
            'country_map' => $this->text(),
            'office_id' => $this->integer(),
            'status' => $this->smallInteger()->defaultValue(0)->notNull(),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('call_center_person_import');
    }
}
