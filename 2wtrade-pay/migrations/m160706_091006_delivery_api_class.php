<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryApiClass;

/**
 * Class m160706_091006_delivery_api_class
 */
class m160706_091006_delivery_api_class extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('delivery_api_class', [
            'id' => $this->primaryKey(),
            'name' => $this->string(100)->notNull(),
            'class_path' => $this->string(100)->notNull(),
            'active' => $this->smallInteger()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->createIndex(null, 'delivery_api_class', 'active');

        $this->addColumn('delivery', 'api_class_id', $this->integer()->defaultValue(null)->after('char_code'));
        $this->addForeignKey(null, 'delivery', 'api_class_id', 'delivery_api_class', 'id', self::CASCADE, self::RESTRICT);

        foreach ($this->getFixture() as $data) {
            $apiClass = new DeliveryApiClass($data);
            $apiClass->save();
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey($this->getFkName('delivery', 'api_class_id'), 'delivery');
        $this->dropColumn('delivery', 'api_class_id');
        $this->dropTable('delivery_api_class');
    }

    /**
     * @return array
     */
    private function getFixture()
    {
        return [
            [
                'name' => 'KikaApi',
                'class_path' => '/kika-api/src/KikaApi.php',
                'active' => 1,
            ],
            [
                'name' => 'BlueExpressApi',
                'class_path' => '/blue-express-api/src/BlueExpressApi.php',
                'active' => 1,
            ]
        ];
    }
}
