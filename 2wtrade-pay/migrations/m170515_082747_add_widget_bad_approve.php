<?php
use app\components\CustomMigration as Migration;
use app\modules\widget\models\WidgetType;
use app\modules\widget\models\WidgetRole;
/**
 * Class m170515_082747_add_widget_bad_approve
 */
class m170515_082747_add_widget_bad_approve extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $code = 'bad_approve';

        $roles = [
            'business_analyst',
            'country.curator',
            'callcenter.manager',
        ];

        $this->insert(WidgetType::tableName(), [
            'code' => $code,
            'name' => 'Плохой апрув',
            'status' => WidgetType::STATUS_ACTIVE,
            'by_country' => WidgetType::BY_ALL_COUNTRIES,
        ]);

        $type = WidgetType::find()
            ->where(['code' => $code])
            ->one();

        foreach ($roles as $role) {
            $this->insert(WidgetRole::tableName(), [
                'role' => $role,
                'type_id' => $type->id
            ]);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $code = 'bad_approve';
        $roles = [
            'business_analyst',
            'country.curator',
            'callcenter.manager',
        ];

        $type = WidgetType::find()
            ->where(['code' => $code])
            ->one();

        foreach ($roles as $role) {
            $this->delete(WidgetRole::tableName(), [
                'role' => $role,
                'type_id' => $type->id
            ]);
        }

        $this->delete(WidgetType::tableName(), ['code' => $code]);
    }
}
