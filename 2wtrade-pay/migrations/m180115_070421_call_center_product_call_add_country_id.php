<?php
use app\components\CustomMigration as Migration;

/**
 * Class m180115_070421_call_center_product_call_add_country_id
 */
class m180115_070421_call_center_product_call_add_country_id extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('call_center_product_call', 'country_id', $this->integer()->notNull()->after('product_id'));
        $this->dropForeignKey(null, 'call_center_product_call', 'product_id');
        $this->alterColumn('call_center_product_call', 'product_id', $this->integer()->notNull());
        $this->addForeignKey(null, 'call_center_product_call', 'product_id', 'product', 'id', self::CASCADE, self::CASCADE);
        $this->addForeignKey(null, 'call_center_product_call', 'country_id', 'country', 'id', self::CASCADE, self::CASCADE);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('call_center_product_call', 'country_id');
        $this->alterColumn('call_center_product_call', 'product_id', $this->integer());
    }
}
