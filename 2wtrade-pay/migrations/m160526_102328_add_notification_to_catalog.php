<?php

use yii\db\Migration;
use app\models\Notification;

/**
 * Handles adding notification to table `catalog`.
 */
class m160526_102328_add_notification_to_catalog extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->db->createCommand()->checkIntegrity(false)->execute();
        $this->truncateTable('notification');
        $this->db->createCommand()->checkIntegrity(true)->execute();

        foreach ($this->getNotifications() as $data) {
            $model = new Notification($data);
            $model->save(false);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->db->createCommand()->checkIntegrity(false)->execute();
        $this->truncateTable('notification');
        $this->db->createCommand()->checkIntegrity(true)->execute();
    }

    /**
     * @return array
     */
    private function getNotifications()
    {
        return [
            [
                'description' => 'Успешное добавление нового лида от Adcombo',
                'trigger' => 'lead.added',
                'type' => Notification::TYPE_SUCCESS,
                'group' => Notification::GROUP_SYSTEM,
                'active' => Notification::ACTIVE,
            ], [
                'description' => 'Не удалось добавить нового лида от Adcombo',
                'trigger' => 'lead.not.added',
                'type' => Notification::TYPE_DANGER,
                'group' => Notification::GROUP_SYSTEM,
                'active' => Notification::ACTIVE,
            ],
        ];
    }
}
