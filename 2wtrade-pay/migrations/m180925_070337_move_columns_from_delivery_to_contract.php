<?php
use app\components\CustomMigration as Migration;

/**
 * Class m180925_070337_move_columns_from_delivery_to_contract
 */
class m180925_070337_move_columns_from_delivery_to_contract extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('delivery_contract', 'bank_name', $this->string());
        $this->addColumn('delivery_contract', 'bank_account', $this->string());
        $this->addColumn('delivery_contract', 'max_delivery_period', $this->integer());

        $sqlCopy = <<<SQL
      UPDATE delivery_contract left join delivery on delivery_contract.delivery_id = delivery.id 
      SET 
      delivery_contract.bank_name = delivery.bank_name, 
      delivery_contract.bank_account = delivery.bank_account,
      delivery_contract.max_delivery_period = delivery.max_delivery_period;
SQL;
        $this->execute($sqlCopy);

        $this->dropColumn('delivery', 'bank_name');
        $this->dropColumn('delivery', 'bank_account');
        $this->dropColumn('delivery', 'max_delivery_period');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->addColumn('delivery', 'bank_name', $this->string());
        $this->addColumn('delivery', 'bank_account', $this->string());
        $this->addColumn('delivery', 'max_delivery_period', $this->integer());

        $sql = <<<SQL
      UPDATE delivery left join delivery_contract on delivery_contract.delivery_id = delivery.id 
      SET 
      delivery.bank_name = delivery_contract.bank_name, 
      delivery.bank_account = delivery_contract.bank_account,
      delivery.max_delivery_period = delivery_contract.max_delivery_period;
SQL;
        $this->execute($sql);

        $this->dropColumn('delivery_contract', 'bank_name');
        $this->dropColumn('delivery_contract', 'bank_account');
        $this->dropColumn('delivery_contract', 'max_delivery_period');
    }
}
