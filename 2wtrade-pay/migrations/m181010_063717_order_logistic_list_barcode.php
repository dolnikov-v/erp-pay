<?php
use app\components\PermissionMigration as Migration;

/**
 * Class m181010_063717_order_logistic_list_barcode
 */
class m181010_063717_order_logistic_list_barcode extends Migration
{
    protected $permissions = ['order.list.downloadbarcodes'];

    protected $roles = [
        'admin' => ['order.list.downloadbarcodes'],
        'country.curator' => ['order.list.downloadbarcodes'],
        'project.manager' => ['order.list.downloadbarcodes'],
        'technical.director' => ['order.list.downloadbarcodes'],
        'support.manager' => ['order.list.downloadbarcodes'],
        'logist' => ['order.list.downloadbarcodes'],
        'junior.logist' => ['order.list.downloadbarcodes'],
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('order_logistic_list_barcode', [
            'id' => $this->primaryKey(),
            'list_id' => $this->integer()->notNull(),
            'order_id' => $this->integer()->notNull(),
            'barcode' => $this->string(100),
            'barcode_filename' => $this->string(),
        ]);
        $this->addForeignKey(null, 'order_logistic_list_barcode', 'list_id', 'order_logistic_list', 'id', self::CASCADE, self::CASCADE);
        $this->addForeignKey(null, 'order_logistic_list_barcode', 'order_id', 'order', 'id', self::CASCADE, self::CASCADE);
        $this->createIndex(null, 'order_logistic_list_barcode', ['list_id', 'order_id'], true);

        $this->addColumn('delivery', 'auto_generating_barcode', $this->boolean()->after('auto_generating_invoice'));
        $this->addColumn('order_logistic_list', 'barcodes', $this->string()->after('correction'));

        parent::safeUp();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('order_logistic_list_barcode');

        $this->dropColumn('delivery', 'auto_generating_barcode');
        $this->dropColumn('order_logistic_list', 'barcodes');

        parent::safeDown();
    }
}
