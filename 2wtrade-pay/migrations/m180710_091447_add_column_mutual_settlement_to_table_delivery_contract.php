<?php

use app\modules\delivery\models\DeliveryContract;
use yii\db\Migration;

/**
 * Handles adding column_mutual_settlement to table `table_delivery_contract`.
 */
class m180710_091447_add_column_mutual_settlement_to_table_delivery_contract extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn(DeliveryContract::tableName(), 'mutual_settlement', $this->boolean()
            ->defaultValue(DeliveryContract::MUTUAL_SETTLEMENT_OFF)
            ->after('delivery_id'));
        $this->update(DeliveryContract::tableName(), ['mutual_settlement' => DeliveryContract::MUTUAL_SETTLEMENT_ON]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn(DeliveryContract::tableName(), 'mutual_settlement');
    }
}
