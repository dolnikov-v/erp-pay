<?php
use app\components\CustomMigration as Migration;

/**
 * Class m181030_044501_delivery_request_deleted_structure
 */
class m181030_044501_delivery_request_deleted_structure extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->execute("ALTER TABLE `delivery_request_deleted` DROP COLUMN `cs_send_response`, DROP COLUMN `last_update_info`, ADD COLUMN `delivery_partner_id` INT(11) AFTER `partner_name`");
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->execute("ALTER TABLE `delivery_request_deleted` DROP COLUMN `delivery_partner_id`, ADD COLUMN `last_update_info` VARCHAR (2000) AFTER `foreign_info`, ADD COLUMN `cs_send_response` TEXT AFTER `partner_name`");
    }
}
