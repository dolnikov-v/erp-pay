<?php
use app\components\CustomMigration as Migration;

/**
 * Class m161205_100629_add_column_update_at
 */
class m161205_100629_add_column_update_at extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('qr_list', 'updated_at', 'VARCHAR(11) DEFAULT NULL AFTER created_at');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('qr_list', 'updated_at');
    }
}
