<?php
use app\components\CustomMigration as Migration;
use app\models\Country;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryApiClass;

/**
 * Class m170524_040712_add_skynet_qatar_oae
 */
class m170524_040712_add_skynet_qatar_oae extends Migration
{

    /**
     * @return array
     */
    private function getCountries()
    {
        $countries = Country::find()
            ->select('id')
            ->where(['slug' => ['qa','ae']])
            ->asArray()->all();
        return $countries;
    }

    /**
     * @return integer $apiClassId
     */
    private function getApiClassId()
    {
        $apiClassId = DeliveryApiClass::find()
            ->select('id')
            ->where(['name' => 'SkyNetApi'])
            ->scalar();

        return $apiClassId;
    }

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $countries = $this->getCountries();
        $apiClassId = $this->getApiClassId();
        foreach ($countries as $country) {
            $delivery = new Delivery();
            $delivery->country_id = $country['id'];
            $delivery->name = 'Sky Net';
            $delivery->char_code = 'SKY';
            $delivery->auto_sending = 0;
            $delivery->api_class_id = $apiClassId;
            $delivery->brokerage_active = 0;
            $delivery->active = 0;
            $delivery->save();
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $countries = $this->getCountries();
        foreach ($countries as $country) {
            Delivery::find()
                ->where([
                    'country_id' => $country['id'],
                    'name' => 'Sky Net',
                ])
                ->one()
                ->delete();
        }
    }
}
