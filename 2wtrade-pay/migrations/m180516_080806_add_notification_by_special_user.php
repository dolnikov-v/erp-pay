<?php

use app\components\CustomMigration as Migration;
use app\models\Notification;
use app\models\NotificationUser;
use app\models\User;
use app\models\UserNotificationSetting;

/**
 * Class m180516_080806_add_notification_by_special_user
 */
class m180516_080806_add_notification_by_special_user extends Migration
{
    private $user_ids = [
        145, //yulia
        285, //biryukov.a,
        5, //j3f7
    ];
    private $notification = Notification::TRIGGER_REPORT_ORDERS_STATUS_IN_12_9_37_38;

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        foreach ($this->user_ids as $user_id) {
            $user = User::findOne(['id' => $user_id]);

            if ($user) {
                $notification = NotificationUser::findOne([
                    'user_id' => $user->id,
                    'trigger' => $this->notification
                ]);
                //сама нотификация
                if ($notification) {
                    $this->update(NotificationUser::tableName(), ['active' => NotificationUser::ACTIVE], [
                        'id' => $notification->id
                    ]);
                } else {
                    $this->insert(NotificationUser::tableName(), [
                        'user_id' => $user_id,
                        'trigger' => $this->notification,
                        'active' => NotificationUser::ACTIVE,
                        'running_line_active' => 0,
                        'created_at' => time(),
                        'updated_at' => time()
                    ]);
                }

                //Настройки нотификации
                $settings = UserNotificationSetting::findOne([
                    'user_id' => $user_id,
                    'trigger' => $this->notification
                ]);

                if ($settings) {
                    $this->update(UserNotificationSetting::tableName(), [
                        'active' => UserNotificationSetting::ACTIVE,
                        'email_interval' => UserNotificationSetting::INTERVAL_ALWAYS,
                        'sms_active' => UserNotificationSetting::ACTIVE,
                        'telegram_active' => UserNotificationSetting::ACTIVE
                    ], [
                        'id' => $settings->id,
                    ]);
                } else {
                    $this->insert(UserNotificationSetting::tableName(), [
                        'user_id' => $user_id,
                        'trigger' => $this->notification,
                        'active' => UserNotificationSetting::ACTIVE,
                        'email_interval' => UserNotificationSetting::INTERVAL_ALWAYS,
                        'sms_active' => UserNotificationSetting::ACTIVE,
                        'telegram_active' => UserNotificationSetting::ACTIVE,
                        'created_at' => time(),
                        'updated_at' => time()
                    ]);
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach ($this->user_ids as $user_id) {
            $this->update(NotificationUser::tableName(),
                [
                    'active' => NotificationUser::NOT_ACTIVE
                ], [
                    'user_id' => $user_id,
                    'trigger' => $this->notification
                ]);

            $this->update(UserNotificationSetting::tableName(), [
                'active' => UserNotificationSetting::NOT_ACTIVE,
                'email_interval' => null,
                'sms_active' => UserNotificationSetting::NOT_ACTIVE,
                'telegram_active' => UserNotificationSetting::NOT_ACTIVE
            ], [
                'user_id' => $user_id,
                'trigger' => $this->notification
            ]);
        }
    }
}
