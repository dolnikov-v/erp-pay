<?php

use yii\db\Migration;

/**
 * Handles adding period to table `delivery_report`.
 */
class m171016_080501_add_period_to_delivery_report extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('delivery_report', 'period_from', $this->integer()->after('priority'));
        $this->addColumn('delivery_report', 'period_to', $this->integer()->after('period_from'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('delivery_report', 'period_from');
        $this->dropColumn('delivery_report', 'period_to');
    }
}
