<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;
use app\models\Notification;

/**
 * Class m161102_123201_new_crontab_task_report_rejected
 */
class m161110_064102_stale_orders_reporting_tasks_and_notifications extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = new CrontabTask();
        $crontabTask->name = 'report_stale_cc_approved';
        $crontabTask->description = 'Ежедневные нотификации о заказах, подвисших в 6-й колонке (одобрено КЦ) больше чем на сутки';
        $crontabTask->save();

        $crontabTask = new CrontabTask();
        $crontabTask->name = 'report_stale_delivery_pending';
        $crontabTask->description = 'Ежедневные нотификации заказах, подвисших в 31-й колонке (отправлен в доставку) больше чем на двое суток';
        $crontabTask->save();


        $model = new Notification([
            'description' => 'Найдены заказы, более суток висящие в статусе "Одобрено КЦ". Всего заказов: {count}',
            'trigger' => Notification::TRIGGER_REPORT_STALE_CC_APPROVED,
            'type' => Notification::TYPE_WARNING,
            'group' => Notification::GROUP_REPORT,
            'active' => Notification::ACTIVE,
        ]);

        $model->save(false);

        $model = new Notification([
            'description' => 'Найдены заказы, более двух суток висящие в статусе "отправлен в доставку". Всего заказов: {count}',
            'trigger' => Notification::TRIGGER_REPORT_STALE_DELIVERY_PENDING,
            'type' => Notification::TYPE_WARNING,
            'group' => Notification::GROUP_REPORT,
            'active' => Notification::ACTIVE,
        ]);

        $model->save(false);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = CrontabTask::find()
            ->byName('report_stale_cc_approved')
            ->one();

        if ($crontabTask instanceof CrontabTask) {
            $crontabTask->delete();
        }

        $crontabTask = CrontabTask::find()
            ->byName('report_stale_delivery_pending')
            ->one();

        if ($crontabTask instanceof CrontabTask) {
            $crontabTask->delete();
        }

        $model = Notification::findOne(['trigger' => Notification::TRIGGER_REPORT_STALE_CC_APPROVED]);

        if ($model instanceof Notification) {
            $model->delete();
        }

        $model = Notification::findOne(['trigger' => Notification::TRIGGER_REPORT_STALE_DELIVERY_PENDING]);

        if ($model instanceof Notification) {
            $model->delete();
        }
    }
}
