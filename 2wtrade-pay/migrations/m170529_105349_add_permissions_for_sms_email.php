<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170529_105349_add_permissions_for_sms_email
 */
class m170529_105349_add_permissions_for_sms_email extends Migration
{

    public $permissions = [
        'order.notification.index',
        'order.notificationrequest.index',
        'report.smspollhistory.index',
        'catalog.smspollquestions.index'
    ];
    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        $this->insert('{{%auth_item}}',array(
            'name'=>'order.notification.index',
            'type' => '2',
            'description' => 'order.notification.index'
        ));

        $this->insert('{{%auth_item}}',array(
            'name'=>'order.notificationrequest.index',
            'type' => '2',
            'description' => 'order.notificationrequest.index'
        ));

        foreach($this->permissions as $permission) {
            $this->insert($this->authManager->itemChildTable, array(
                'parent' => 'business_analyst',
                'child' => $permission
            ));
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach($this->permissions as $permission) {
            $this->delete($this->authManager->itemChildTable, ['child' => $permission, 'parent' => 'business_analyst']);
        }

    }
}
