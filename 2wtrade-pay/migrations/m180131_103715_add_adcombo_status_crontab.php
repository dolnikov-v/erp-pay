<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m180131_103715_add_adcombo_status_crontab
 */
class m180131_103715_add_adcombo_status_crontab extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_REPORT_ADCOMBO_STATUS]);
        if (!$crontabTask) {
            $crontabTask = new CrontabTask();
            $crontabTask->name = CrontabTask::TASK_REPORT_ADCOMBO_STATUS;
            $crontabTask->description = "Уведомление в чаты адкомбо о статусах сегодняшних лидов";
            $crontabTask->save();
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_REPORT_ADCOMBO_STATUS]);
        $crontabTask->delete();
    }
}
