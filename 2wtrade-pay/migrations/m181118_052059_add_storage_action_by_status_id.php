<?php
use app\components\CustomMigration as Migration;
use app\modules\order\models\OrderStatus;

/**
 * Class m181118_052059_add_storage_action_by_status_id
 */
class m181118_052059_add_storage_action_by_status_id extends Migration
{
    /**
     * @var array - статусы заказа для списания на складе
     */
    private $statusesMinus = [
        OrderStatus::STATUS_DELIVERY_ACCEPTED,
        OrderStatus::STATUS_DELIVERY_LEFT_WAREHOUSE,
        OrderStatus::STATUS_DELIVERY,
        OrderStatus::STATUS_DELIVERY_BUYOUT,
        OrderStatus::STATUS_FINANCE_MONEY_RECEIVED,
        OrderStatus::STATUS_DELIVERY_LEFT_OUR_WAREHOUSE,
        OrderStatus::STATUS_DELIVERY_REDELIVERY,
        OrderStatus::STATUS_DELIVERY_LEFT_THEIR_WAREHOUSE,
        OrderStatus::STATUS_DELIVERY_DELAYED,
        OrderStatus::STATUS_DELIVERY_LOST,
        OrderStatus::STATUS_DELIVERY_PENDING,
        OrderStatus::STATUS_LOG_SENT,
        OrderStatus::STATUS_LOG_RECEIVED,
    ];

    /**
     * @var array - статусы заказа для поступления на складе
     */
    private $statusesPlus = [
        OrderStatus::STATUS_DELIVERY_DENIAL,
        OrderStatus::STATUS_DELIVERY_RETURNED,
        OrderStatus::STATUS_DELIVERY_REJECTED,
        OrderStatus::STATUS_LOGISTICS_REJECTED,
        OrderStatus::STATUS_DELIVERY_REFUND,
        OrderStatus::STATUS_LOGISTICS_ACCEPTED,
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('storage_action_by_status_id', [
            'status_id' => $this->integer()->notNull(),
            'action_id' => $this->smallInteger()->notNull()
        ], $this->tableOptions);
        $this->addPrimaryKey($this->getPkName('storage_action_by_status_id', 'status_id'), 'storage_action_by_status_id', 'status_id');
        $this->addForeignKey(null, 'storage_action_by_status_id', 'status_id', 'order_status', 'id', self::CASCADE, self::CASCADE);

        $data = [];

        foreach ($this->statusesMinus as $status) {
            $data[] = ['status_id' => $status, 'action_id' => 0];
        }
        foreach ($this->statusesPlus as $status) {
            $data[] = ['status_id' => $status, 'action_id' => 1];
        }
        $this->batchInsert('storage_action_by_status_id', ['status_id', 'action_id'], $data);

        $this->execute('DROP TRIGGER IF EXISTS `order_change_status_id`');

        $createTriggerSql = "
CREATE TRIGGER `order_change_status_id` AFTER UPDATE ON `order`
    FOR EACH ROW BEGIN
        DECLARE v_order_product_id INT;
        DECLARE v_product_id INT;
        DECLARE v_quantity INT;
        DECLARE cursor_done integer default 0;
        
        DECLARE products_cur CURSOR FOR SELECT id, product_id, quantity
                                    FROM order_product
                                    WHERE order_id = NEW.id;
        DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET cursor_done=1;
        
        IF NEW.status_id != OLD.status_id and NEW.status_id IN (SELECT `status_id` FROM `storage_action_by_status_id`) THEN 
            OPEN products_cur;
            SET cursor_done = 0;
            FETCH products_cur INTO v_order_product_id, v_product_id, v_quantity;
            WHILE cursor_done = 0 DO 
                
                IF NEW.status_id IN (SELECT `status_id` FROM `storage_action_by_status_id` WHERE `action_id` = 0) and  OLD.status_id NOT IN (SELECT `status_id` FROM `storage_action_by_status_id` WHERE `action_id` = 0) THEN 
                    INSERT INTO storage_history_order SET type='minus', product_id=v_product_id, quantity=v_quantity, order_id=NEW.id, country_id=NEW.country_id;
                END IF;
                
                IF NEW.status_id IN (SELECT `status_id` FROM `storage_action_by_status_id` WHERE `action_id` = 1) and OLD.status_id NOT IN (SELECT `status_id` FROM `storage_action_by_status_id` WHERE `action_id` = 1) THEN 
                    INSERT INTO storage_history_order SET type='plus', product_id=v_product_id, quantity=v_quantity, order_id=NEW.id, country_id=NEW.country_id;
                END IF;
                
                SET cursor_done = 0;
                FETCH products_cur INTO v_order_product_id, v_product_id, v_quantity;
            
            END WHILE;
            CLOSE products_cur;
        END IF;
    END;";

        $this->execute($createTriggerSql);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('storage_action_by_status_id');

        $this->execute('DROP TRIGGER IF EXISTS `order_change_status_id`');

        $createTriggerSql = "
CREATE TRIGGER `order_change_status_id` AFTER UPDATE ON `order`
    FOR EACH ROW BEGIN
        DECLARE v_order_product_id INT;
        DECLARE v_product_id INT;
        DECLARE v_quantity INT;
        DECLARE cursor_done integer default 0;
        
        DECLARE products_cur CURSOR FOR SELECT id, product_id, quantity
                                    FROM order_product
                                    WHERE order_id = NEW.id;
        DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET cursor_done=1;
        
        IF NEW.status_id != OLD.status_id and NEW.status_id IN (" . implode(",", $this->statusesMinus) . "," . implode(",", $this->statusesPlus) . ") THEN 
            OPEN products_cur;
            SET cursor_done = 0;
            FETCH products_cur INTO v_order_product_id, v_product_id, v_quantity;
            WHILE cursor_done = 0 DO 
                
                IF NEW.status_id IN (" . implode(",", $this->statusesMinus) . ") and  OLD.status_id NOT IN (" . implode(",", $this->statusesMinus) . ") THEN 
                    INSERT INTO storage_history_order SET type='minus', product_id=v_product_id, quantity=v_quantity, order_id=NEW.id, country_id=NEW.country_id;
                END IF;
                
                IF NEW.status_id IN (" . implode(",", $this->statusesPlus) . ") and OLD.status_id NOT IN (" . implode(",", $this->statusesPlus) . ") THEN 
                    INSERT INTO storage_history_order SET type='plus', product_id=v_product_id, quantity=v_quantity, order_id=NEW.id, country_id=NEW.country_id;
                END IF;
                
                SET cursor_done = 0;
                FETCH products_cur INTO v_order_product_id, v_product_id, v_quantity;
            
            END WHILE;
            CLOSE products_cur;
        END IF;
    END;";

        $this->execute($createTriggerSql);
    }
}
