<?php
use app\components\CustomMigration as Migration;
use yii\db\Query;
use app\models\Language;
use app\models\Country;

/**
 * Class m170926_013147_language_data
 */
class m170926_013147_language_data extends Migration
{
    const LANG = [
        'af-ZA' => 'Afrikaans - South Africa',
        'sq-AL' => 'Albanian - Albania',
        'ar-DZ' => 'Arabic - Algeria',
        'ar-BH' => 'Arabic - Bahrain',
        'ar-EG' => 'Arabic - Egypt',
        'ar-IQ' => 'Arabic - Iraq',
        'ar-JO' => 'Arabic - Jordan',
        'ar-KW' => 'Arabic - Kuwait',
        'ar-LB' => 'Arabic - Lebanon',
        'ar-LY' => 'Arabic - Libya',
        'ar-MA' => 'Arabic - Morocco',
        'ar-OM' => 'Arabic - Oman',
        'ar-QA' => 'Arabic - Qatar',
        'ar-SA' => 'Arabic - Saudi Arabia',
        'ar-SY' => 'Arabic - Syria',
        'ar-TN' => 'Arabic - Tunisia',
        'ar-AE' => 'Arabic - United Arab Emirates',
        'ar-YE' => 'Arabic - Yemen',
        'hy-AM' => 'Armenian - Armenia',
        'Cy-az-AZ' => 'Azeri (Cyrillic) - Azerbaijan',
        'Lt-az-AZ' => 'Azeri (Latin) - Azerbaijan',
        'eu-ES' => 'Basque - Basque',
        'be-BY' => 'Belarusian - Belarus',
        'bg-BG' => 'Bulgarian - Bulgaria',
        'ca-ES' => 'Catalan - Catalan',
        'zh-CN' => 'Chinese - China',
        'zh-HK' => 'Chinese - Hong Kong SAR',
        'zh-MO' => 'Chinese - Macau SAR',
        'zh-SG' => 'Chinese - Singapore',
        'zh-TW' => 'Chinese - Taiwan',
        'zh-CHS' => 'Chinese (Simplified)',
        'zh-CHT' => 'Chinese (Traditional)',
        'hr-HR' => 'Croatian - Croatia',
        'cs-CZ' => 'Czech - Czech Republic',
        'da-DK' => 'Danish - Denmark',
        'div-MV' => 'Dhivehi - Maldives',
        'nl-BE' => 'Dutch - Belgium',
        'nl-NL' => 'Dutch - The Netherlands',
        'en-AU' => 'English - Australia',
        'en-BZ' => 'English - Belize',
        'en-CA' => 'English - Canada',
        'en-CB' => 'English - Caribbean',
        'en-IE' => 'English - Ireland',
        'en-JM' => 'English - Jamaica',
        'en-NZ' => 'English - New Zealand',
        'en-PH' => 'English - Philippines',
        'en-ZA' => 'English - South Africa',
        'en-TT' => 'English - Trinidad and Tobago',
        'en-GB' => 'English - United Kingdom',
        'en-US' => 'English - United States',
        'en-ZW' => 'English - Zimbabwe',
        'et-EE' => 'Estonian - Estonia',
        'fo-FO' => 'Faroese - Faroe Islands',
        'fa-IR' => 'Farsi - Iran',
        'fi-FI' => 'Finnish - Finland',
        'fr-BE' => 'French - Belgium',
        'fr-CA' => 'French - Canada',
        'fr-FR' => 'French - France',
        'fr-LU' => 'French - Luxembourg',
        'fr-MC' => 'French - Monaco',
        'fr-CH' => 'French - Switzerland',
        'gl-ES' => 'Galician - Galician',
        'ka-GE' => 'Georgian - Georgia',
        'de-AT' => 'German - Austria',
        'de-DE' => 'German - Germany',
        'de-LI' => 'German - Liechtenstein',
        'de-LU' => 'German - Luxembourg',
        'de-CH' => 'German - Switzerland',
        'el-GR' => 'Greek - Greece',
        'gu-IN' => 'Gujarati - India',
        'he-IL' => 'Hebrew - Israel',
        'hi-IN' => 'Hindi - India',
        'hu-HU' => 'Hungarian - Hungary',
        'is-IS' => 'Icelandic - Iceland',
        'id-ID' => 'Indonesian - Indonesia',
        'it-IT' => 'Italian - Italy',
        'it-CH' => 'Italian - Switzerland',
        'ja-JP' => 'Japanese - Japan',
        'kn-IN' => 'Kannada - India',
        'kk-KZ' => 'Kazakh - Kazakhstan',
        'kok-IN' => 'Konkani - India',
        'ko-KR' => 'Korean - Korea',
        'ky-KZ' => 'Kyrgyz - Kazakhstan',
        'lv-LV' => 'Latvian - Latvia',
        'lt-LT' => 'Lithuanian - Lithuania',
        'mk-MK' => 'Macedonian (FYROM)',
        'ms-BN' => 'Malay - Brunei',
        'ms-MY' => 'Malay - Malaysia',
        'mr-IN' => 'Marathi - India',
        'mn-MN' => 'Mongolian - Mongolia',
        'nb-NO' => 'Norwegian (Bokmål) - Norway',
        'nn-NO' => 'Norwegian (Nynorsk) - Norway',
        'pl-PL' => 'Polish - Poland',
        'pt-BR' => 'Portuguese - Brazil',
        'pt-PT' => 'Portuguese - Portugal',
        'pa-IN' => 'Punjabi - India',
        'ro-RO' => 'Romanian - Romania',
        'ru-RU' => 'Russian - Russia',
        'sa-IN' => 'Sanskrit - India',
        'Cy-sr-SP' => 'Serbian (Cyrillic) - Serbia',
        'Lt-sr-SP' => 'Serbian (Latin) - Serbia',
        'sk-SK' => 'Slovak - Slovakia',
        'sl-SI' => 'Slovenian - Slovenia',
        'es-AR' => 'Spanish - Argentina',
        'es-BO' => 'Spanish - Bolivia',
        'es-CL' => 'Spanish - Chile',
        'es-CO' => 'Spanish - Colombia',
        'es-CR' => 'Spanish - Costa Rica',
        'es-DO' => 'Spanish - Dominican Republic',
        'es-EC' => 'Spanish - Ecuador',
        'es-SV' => 'Spanish - El Salvador',
        'es-GT' => 'Spanish - Guatemala',
        'es-HN' => 'Spanish - Honduras',
        'es-MX' => 'Spanish - Mexico',
        'es-NI' => 'Spanish - Nicaragua',
        'es-PA' => 'Spanish - Panama',
        'es-PY' => 'Spanish - Paraguay',
        'es-PE' => 'Spanish - Peru',
        'es-PR' => 'Spanish - Puerto Rico',
        'es-ES' => 'Spanish - Spain',
        'es-UY' => 'Spanish - Uruguay',
        'es-VE' => 'Spanish - Venezuela',
        'sw-KE' => 'Swahili - Kenya',
        'sv-FI' => 'Swedish - Finland',
        'sv-SE' => 'Swedish - Sweden',
        'syr-SY' => 'Syriac - Syria',
        'ta-IN' => 'Tamil - India',
        'tt-RU' => 'Tatar - Russia',
        'te-IN' => 'Telugu - India',
        'th-TH' => 'Thai - Thailand',
        'tr-TR' => 'Turkish - Turkey',
        'uk-UA' => 'Ukrainian - Ukraine',
        'ur-PK' => 'Urdu - Pakistan',
        'Cy-uz-UZ' => 'Uzbek (Cyrillic) - Uzbekistan',
        'Lt-uz-UZ' => 'Uzbek (Latin) - Uzbekistan',
        'vi-VN' => 'Vietnamese - Vietnam'
    ];

    const COUNTRY_LANG = [
        'AR' => 'es-AR',
        'BH' => 'ar-BH',
        'NL' => 'nl-NL',
        'BG' => 'bg-BG',
        'BO' => 'es-BO',
        'GB' => 'en-GB',
        'VN' => 'vi-VN',
        'GT' => 'es-GT',
        'HK' => 'zh-HK',
        'GR' => 'el-GR',
        'DO' => 'es-DO',
        'EG' => 'ar-EG',
        'IN' => 'hi-IN',
        'ID' => 'id-ID',
        'JO' => 'ar-JO',
        'IR' => 'fa-IR',
        'ES' => 'es-ES',
        'IT' => 'it-IT',
        'QA' => 'ar-QA',
        'KE' => 'sw-KE',
        'CN' => 'zh-CN',
        'CO' => 'es-CO',
        'CR' => 'es-CR',
        'KW' => 'ar-KW',
        'MY' => 'ms-MY',
        'MX' => 'es-MX',
        'NLD' => 'nl-NL',
        'NI' => 'es-NI',
        'AE' => 'ar-AE',
        'PK' => 'ur-PK',
        'PA' => 'es-PA',
        'PY' => 'es-PY',
        'PE' => 'es-PE',
        'PT' => 'pt-PT',
        'RO' => 'ro-RO',
        'SA' => 'ar-SA',
        'SG' => 'zh-SG',
        'SK' => 'sk-SK',
        'US' => 'en-US',
        'TH' => 'th-TH',
        'TW' => 'zh-TW',
        'TN' => 'ar-TN',
        'TR' => 'tr-TR',
        'UY' => 'es-UY',
        'PH' => 'en-PH',
        'FR' => 'fr-FR',
        'CZ' => 'cs-CZ',
        'CL' => 'es-CL',
        'EC' => 'es-EC',
        'ZA' => 'en-ZA',
        'KR' => 'ko-KR',
        'JP' => 'ja-JP',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $query = new Query();

        foreach (self::LANG as $lang => $name) {

            $is_lang = $query->select('*')->from(Language::tableName())
                ->where(['locale' => $lang])
                ->one();

            if (!$is_lang) {
                $this->insert(Language::tableName(), [
                    'locale' => $lang,
                    'name' => $name,
                    'active' => 1,
                    'created_at' => time(),
                    'updated_at' => time()]);
            }
        }

        foreach (self::COUNTRY_LANG as $code => $lang) {

            $country = $query->select('*')->from(Country::tableName())
                ->where(['char_code' => $code])
                ->one();

            $language = $query->select('*')->from(Language::tableName())
                ->where(['locale' => $lang])
                ->one();

            if ($country && $language) {
                $this->update(Country::tableName(), [
                    'language_id' => $language['id']
                ], [
                    'id' => $country['id']
                ]);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $query = new Query();

        foreach (self::COUNTRY_LANG as $code => $lang) {

            $country = $query->select('*')->from(Country::tableName())
                ->where(['char_code' => $code])
                ->one();

            $language = $query->select('*')->from(Language::tableName())
                ->where(['locale' => $lang])
                ->one();

            if ($country && $language) {
                $this->update(Country::tableName(), [
                    'language_id' => null
                ], [
                    'id' => $country['id']
                ]);
            }
        }

        foreach (self::LANG as $lang => $name) {

            $is_lang = $query->select('*')->from(Language::tableName())
                ->where(['locale' => $lang])
                ->one();

            if ($is_lang) {
                $this->delete(Language::tableName(), [
                    'id' => $is_lang['id']
                ]);
            }
        }
    }
}