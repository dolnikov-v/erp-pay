<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryRequest;

/**
 * Class m170109_090148_add_finance_tracking
 */
class m170109_090148_add_finance_tracking extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(DeliveryRequest::tableName(), 'finance_tracking', $this->string(30));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(DeliveryRequest::tableName(), 'finance_tracking');
    }
}
