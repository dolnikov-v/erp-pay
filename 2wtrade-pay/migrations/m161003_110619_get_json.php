<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m161003_110619_get_json
 */
class m161003_110619_get_json extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = new CrontabTask();
        $crontabTask->name = 'get_json';
        $crontabTask->description = 'Фигня.';
        $crontabTask->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = CrontabTask::find()
            ->byName('get_json')
            ->one();

        if ($crontabTask) {
            $crontabTask->delete();
        }
    }
}
