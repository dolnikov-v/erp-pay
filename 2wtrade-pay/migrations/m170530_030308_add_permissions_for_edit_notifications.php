<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170530_030308_add_permissions_for_edit_notifications
 */
class m170530_030308_add_permissions_for_edit_notifications extends Migration
{
    public $permissions = [
        'order.notification.activate',
        'order.notification.deactivate',
        'order.notification.delete'
    ];
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        foreach($this->permissions as $permission) {
            $this->insert('{{%auth_item}}',array(
                'name'=>$permission,
                'type' => '2',
                'description' => $permission
            ));

            $this->insert($this->authManager->itemChildTable, array(
                'parent' => 'business_analyst',
                'child' => $permission
            ));
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach($this->permissions as $permission) {
            $this->delete($this->authManager->itemChildTable, ['child' => $permission, 'parent' => 'business_analyst']);
            $this->delete('{{%auth_item}}', ['name' => $permission]);
        }

    }
}
