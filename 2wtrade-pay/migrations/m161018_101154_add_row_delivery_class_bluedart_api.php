<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryApiClass;

/**
 * Class m161018_101154_add_row_delivery_class_bluedart_api
 */
class m161018_101154_add_row_delivery_class_bluedart_api extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $class = new DeliveryApiClass();
        $class->name = 'BluedartApi';
        $class->class_path = '/bluedart-api/src/BluedartApi.php';
        $class->active = 1;
        $class->save();

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        DeliveryApiClass::find()
            ->where([
                    'name' => 'BluedartApi',
                    'class_path' => '/bluedart-api/src/BluedartApi.php'
                ])
            ->one()
            ->delete();
    }
}
