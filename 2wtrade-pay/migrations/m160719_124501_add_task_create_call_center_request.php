<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m160719_124501_add_task_create_call_center_request
 */
class m160719_124501_add_task_create_call_center_request extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = new CrontabTask();
        $crontabTask->name = 'call_center_create_requests';
        $crontabTask->description = 'Создание заявок для колл-центра.';
        $crontabTask->save();

        $crontabTask = new CrontabTask();
        $crontabTask->name = 'call_center_send_requests';
        $crontabTask->description = 'Отправка заявок в колл-центр на обзвон.';
        $crontabTask->save();

        $crontabTask = new CrontabTask();
        $crontabTask->name = 'call_center_get_statuses';
        $crontabTask->description = 'Принятие результатов прозвона из колл-центра.';
        $crontabTask->save();

        $crontabTask = CrontabTask::find()
            ->byName('get_status_lead_from_call_center')
            ->one();

        if ($crontabTask) {
            $crontabTask->delete();
        }

        $crontabTask = CrontabTask::find()
            ->byName('send_lead_in_call_center')
            ->one();

        if ($crontabTask) {
            $crontabTask->delete();
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = new CrontabTask();
        $crontabTask->name = 'send_lead_in_call_center';
        $crontabTask->description = 'Отправка лидов в колл-центр на обзвон.';
        $crontabTask->save();

        $crontabTask = new CrontabTask();
        $crontabTask->name = 'get_status_lead_from_call_center';
        $crontabTask->description = 'Отправка лидов в колл-центр на обзвон.';
        $crontabTask->save();

        $crontabTask = CrontabTask::find()
            ->byName('call_center_get_statuses')
            ->one();

        if ($crontabTask) {
            $crontabTask->delete();
        }

        $crontabTask = CrontabTask::find()
            ->byName('call_center_send_requests')
            ->one();

        if ($crontabTask) {
            $crontabTask->delete();
        }

        $crontabTask = CrontabTask::find()
            ->byName('call_center_create_requests')
            ->one();

        if ($crontabTask) {
            $crontabTask->delete();
        }
    }
}
