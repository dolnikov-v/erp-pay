<?php
use app\components\CustomMigration as Migration;
use app\modules\callcenter\models\CallCenterUser;

/**
 * Class m180327_075748_add_column_password_in_table_call_center_user
 */
class m180327_075748_add_column_password_in_table_call_center_user extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(CallCenterUser::tableName(), 'user_password', $this->string()->after('user_login'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(CallCenterUser::tableName(), 'user_password');
    }
}