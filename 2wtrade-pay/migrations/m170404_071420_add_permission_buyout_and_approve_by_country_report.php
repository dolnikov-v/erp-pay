<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170404_071420_add_permission_buyout_and_approve_by_country_report
 */
class m170404_071420_add_permission_buyout_and_approve_by_country_report extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $auth = $this->authManager;
        $role = $auth->getRole('business_analyst');

        $buyoutAndApproveByCountry = $auth->createPermission('report.buyoutandapprovebycountry.index');
        $buyoutAndApproveByCountry->description = 'Report Buyout and approve by country';
        $auth->add($buyoutAndApproveByCountry);
        $auth->addChild($role,$buyoutAndApproveByCountry);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $auth = $this->authManager;
        $buyoutAndApproveByCountry = $auth->getPermission('report.buyoutandapprovebycountry.index');
        $auth->remove($buyoutAndApproveByCountry);
    }
}
