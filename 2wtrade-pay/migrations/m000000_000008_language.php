<?php

use app\components\CustomMigration as Migration;
use app\modules\i18n\models\Language;

class m000000_000008_language extends Migration
{
    public function safeUp()
    {
        $this->createTable(Language::tableName(), [
            'id' => $this->primaryKey(),
            'name' => $this->string(32)->notNull(),
            'icon' => $this->string(20)->notNull(),
            'locale' => $this->string(8)->notNull(),
            'source_language' => $this->smallInteger()->notNull()->defaultValue(0),
            'active' => $this->smallInteger()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $this->tableOptions);

        foreach ($this->languageFixture() as $key => $fixture) {
            $lang = new Language($fixture);
            $lang->save(false);
        }
    }

    public function safeDown()
    {
        $this->dropTable(Language::tableName());
    }

    /**
     * @return array
     */
    private function languageFixture()
    {
        return [
            [
                'name' => 'Русский',
                'icon' => 'flag-icon-ru',
                'locale' => 'ru-RU',
                'source_language' => 1,
                'active' => 1,
            ],
            [
                'name' => 'English',
                'icon' => 'flag-icon-gb',
                'locale' => 'en-US',
                'source_language' => 0,
                'active' => 1,
            ],
            [
                'name' => 'Español',
                'icon' => 'flag-icon-es',
                'locale' => 'es-ES',
                'source_language' => 0,
                'active' => 1,
            ],
            [
                'name' => 'Türkçe',
                'icon' => 'flag-icon-tr',
                'locale' => 'tr-TR',
                'source_language' => 0,
                'active' => 1,
            ],
        ];
    }
}
