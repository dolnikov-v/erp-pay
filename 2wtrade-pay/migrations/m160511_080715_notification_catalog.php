<?php
use app\components\CustomMigration as Migration;
use app\models\Notification;

/**
 * Class m160511_080715_notification_catalog
 */
class m160511_080715_notification_catalog extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('notification', [
            'id' => $this->primaryKey(),
            'description' => $this->string()->notNull(),
            'trigger' => $this->string(255)->unique()->notNull(),
            'group' => $this->string(255)->defaultValue(null),
            'active' => $this->boolean()->defaultValue(0),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $this->tableOptions);

        foreach ($this->notificationList() as $key => $notification) {
            $notice = new Notification($notification);
            $notice->save(false);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('notification');
    }

    /**
     * @return array
     */
    private function notificationList()
    {
        return [
            [
                'description' => 'Пользователь вошёл в систему',
                'trigger' => 'user.login',
                'group' => 'Обычное',
            ],
            [
                'description' => 'Пользователь вышел из системы',
                'trigger' => 'user.logout',
                'group' => 'Обычное',
            ],
        ];
    }
}
