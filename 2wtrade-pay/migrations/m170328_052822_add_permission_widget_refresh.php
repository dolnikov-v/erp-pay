<?php
use app\components\CustomMigration as Migration;
use app\models\AuthItem;
use yii\db\Query;

/**
 * Class m170328_052822_add_permission_widget_refresh
 */
class m170328_052822_add_permission_widget_refresh extends Migration
{

    /**
     * @param string $rule
     * @return array|null|\yii\db\ActiveRecord
     */

    private function findAuthItem($rule)
    {
        return AuthItem::find()
            ->where(['name' => $rule])
            ->one();
    }

    /**
     * @param string $role
     * @param string $rule
     * @return array|bool
     */

    private function findItemChild($role, $rule)
    {
        return (new Query())
            ->from($this->authManager->itemChildTable)
            ->where([
                'parent' => $role,
                'child' => $rule
            ])
            ->one();
    }

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $rule = 'widget.index.refresh';
        $roles = [
            'business_analyst',
            'country.curator',
            'callcenter.manager',
        ];

        if (!$this->findAuthItem($rule)) {
            $this->insert($this->authManager->itemTable, [
                'name' => $rule,
                'type' => 2,
                'description' => $rule,
                'created_at' => time(),
                'updated_at' => time()
            ]);
        }

        foreach ($roles as $role) {
            if (!$this->findItemChild($role, $rule)) {
                $this->insert($this->authManager->itemChildTable, [
                    'parent' => $role,
                    'child' => $rule
                ]);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $rule = 'widget.index.refresh';
        $roles = [
            'business_analyst',
            'country.curator',
            'callcenter.manager',
        ];

        foreach ($roles as $role) {
            $this->delete($this->authManager->itemChildTable, [
                'parent' => $role,
                'child' => $rule
            ]);
        }

        $this->delete($this->authManager->itemTable, [
            'name' => $rule,
            'type' => 2
        ]);
    }
}
