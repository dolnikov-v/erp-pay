<?php

use app\components\CustomMigration;
use app\modules\order\models\Order;

/**
 * Class m170110_072724_add_partner_name_to_order
 */
class m170110_072724_add_partner_name_to_order extends CustomMigration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Order::tableName(), 'delivery_partner_name', $this->string(100));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Order::tableName(), 'delivery_partner_name');
    }
}
