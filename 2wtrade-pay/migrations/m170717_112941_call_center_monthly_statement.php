<?php
use app\components\CustomMigration as Migration;
use app\modules\salary\models\Office;
use yii\db\Query;

/**
 * Class m170717_112941_call_center_monthly_statement
 */
class m170717_112941_call_center_monthly_statement extends Migration
{
    const ROLES = [
        'finance.director',
        'callcenter.hr',
        'callcenter.manager',
        'project.manager'
    ];

    const RULES = [
        'callcenter.monthlystatement.index',
        'callcenter.monthlystatement.edit',
        'callcenter.monthlystatement.delete',
        'callcenter.monthlystatement.activate',
        'callcenter.monthlystatement.deactivate',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        $query = new Query();

        foreach (self::RULES as $rule) {

            $is_rule = $query->select('*')->from($this->authManager->itemTable)
                ->where(['name' => $rule, 'type' => 2])
                ->one();

            if (!$is_rule) {
                $this->insert($this->authManager->itemTable, [
                    'name' => $rule,
                    'description' => $rule,
                    'type' => 2,
                    'created_at' => time(),
                    'updated_at' => time()]);
            }

            foreach (self::ROLES as $role) {
                $is_can = $query->select('*')->from($this->authManager->itemChildTable)->where([
                    'parent' => $role,
                    'child' => $rule
                ])->one();

                if (!$is_can) {
                    $this->insert($this->authManager->itemChildTable, [
                        'parent' => $role,
                        'child' => $rule
                    ]);
                }
            }
        }

        $this->createTable('call_center_monthly_statement', [
            'id' => $this->primaryKey(),
            'date_from' => $this->date(),
            'date_to' => $this->date(),
            'active' => $this->integer(1),
            'office_id' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addForeignKey(null, 'call_center_monthly_statement', 'office_id', Office::tableName(), 'id', self::CASCADE, self::RESTRICT);

        $this->createTable('call_center_monthly_statement_data', [
            'id' => $this->primaryKey(),
            'statement_id' => $this->integer(),
            'person_id' => $this->integer(),
            'fio' => $this->string(),
            'lunch_time' => $this->string(),
            'start_date' => $this->string(),
            'dismissal_date' => $this->string(),
            'active' => $this->integer(1),
            'parent_id' => $this->integer(),
            'parent_name' => $this->string(),
            'parent_designation' => $this->string(),
            'designation' => $this->string(),
            'designation_id' => $this->integer(),
            'designation_team_lead' => $this->integer(),
            'designation_calc_work_time' => $this->integer(),
            'working_1' => $this->string(),
            'working_2' => $this->string(),
            'working_3' => $this->string(),
            'working_4' => $this->string(),
            'working_5' => $this->string(),
            'working_6' => $this->string(),
            'working_7' => $this->string(),
            'call_center_user_logins' => $this->string(),
            'call_center_user_ids' => $this->string(),
            'call_center_oper_ids' => $this->string(),
            'call_center_id_oper_id' => $this->string(),
            'salary_local' => $this->float(),
            'salary_usd' => $this->float(),
            'salary_scheme' => $this->string(),
            'country_names' => $this->string(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addForeignKey(null, 'call_center_monthly_statement_data', 'statement_id', 'call_center_monthly_statement', 'id', self::CASCADE, self::RESTRICT);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach (self::RULES as $rule) {
            foreach (self::ROLES as $role) {
                $this->delete($this->authManager->itemChildTable, [
                    'parent' => $role,
                    'child' => $rule
                ]);
            }
        }

        $this->dropTable('call_center_monthly_statement_data');
        $this->dropTable('call_center_monthly_statement');
    }
}
