<?php
use app\components\CustomMigration as Migration;

/**
 * Class m190128_041533_add_ifhat_utama_api
 */
class m190128_041533_add_ifhat_utama_api extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('delivery_api_class', [
            'name' => 'IfhatExpressApi',
            'class_path' => '/ifhat-express-api/src/ifhatExpressApi.php',
            'active' => 1,
            'can_tracking' => 0,
            'use_amazon_queue' => 0,
            'created_at' => time(),
            'updated_at' => time(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete('delivery_api_class', ['name' => 'IfhatExpressApi']);
    }
}
