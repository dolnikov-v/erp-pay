<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m170406_092918_cron_free_storage_product_unaccounted
 */
class m170406_092918_cron_free_storage_product_unaccounted extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = new CrontabTask();
        $crontabTask->name = CrontabTask::TASK_FREE_STORAGE_PRODUCT_UNACCOUNTED;
        $crontabTask->description = 'Списание товаров, не нашедших партию';
        $crontabTask->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        CrontabTask::deleteAll(['name' => CrontabTask::TASK_FREE_STORAGE_PRODUCT_UNACCOUNTED]);
    }
}
