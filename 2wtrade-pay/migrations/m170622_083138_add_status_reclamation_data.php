<?php
use app\components\CustomMigration as Migration;
use app\modules\order\models\OrderWorkflow;
use app\modules\order\models\OrderWorkflowStatus;
use app\modules\order\models\OrderStatus;

/**
 * Class m170622_083138_add_status_reclamation_data
 */
class m170622_083138_add_status_reclamation_data extends Migration
{

    const STATUS_FROM = [12, 13, 14, 15, 16, 17];
    const STATUS_TO = [15, 17];


    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $orderWorkflows = OrderWorkflow::find()->active()->all();

        foreach ($orderWorkflows as $workflow) {

            $orderWorkflowStatus = new OrderWorkflowStatus();
            $orderWorkflowStatus->workflow_id = $workflow->id;
            $orderWorkflowStatus->status_id = OrderStatus::STATUS_RECLAMATION;
            $orderWorkflowStatus->parents = self::STATUS_TO;
            $orderWorkflowStatus->created_at = time();
            $orderWorkflowStatus->updated_at = time();
            $orderWorkflowStatus->save();

            foreach (self::STATUS_FROM as $from) {
                $orderWorkflowStatus = OrderWorkflowStatus::findOne([
                    'workflow_id' => $workflow->id,
                    'status_id' => $from,
                ]);
                if ($orderWorkflowStatus) {
                    $parents = explode(",", $orderWorkflowStatus->parents);
                    if (!in_array(OrderStatus::STATUS_RECLAMATION, $parents)) {
                        $parents[] = OrderStatus::STATUS_RECLAMATION;
                        $orderWorkflowStatus->parents = $parents;
                        $orderWorkflowStatus->save();
                    }
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete(OrderWorkflowStatus::tableName(), [
            'status_id' => OrderStatus::STATUS_RECLAMATION
        ]);

        $orderWorkflows = OrderWorkflow::find()->active()->all();

        foreach ($orderWorkflows as $workflow) {

            foreach (self::STATUS_FROM as $from) {

                $orderWorkflowStatus = OrderWorkflowStatus::findOne([
                    'workflow_id' => $workflow->id,
                    'status_id' => $from,
                ]);
                if ($orderWorkflowStatus) {
                    $parents = explode(",", $orderWorkflowStatus->parents);
                    $key = array_search(OrderStatus::STATUS_RECLAMATION, $parents);
                    if ($key !== false) {
                        unset($parents[$key]);
                        $orderWorkflowStatus->parents = $parents;
                        $orderWorkflowStatus->save();
                    }
                }
            }
        }
    }
}
