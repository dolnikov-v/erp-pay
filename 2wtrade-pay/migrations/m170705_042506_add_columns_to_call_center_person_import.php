<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `call_center_person_import`.
 */
class m170705_042506_add_columns_to_call_center_person_import extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('call_center_person_import', 'cell_passport', $this->integer());
        $this->addColumn('call_center_person_import', 'cell_head', $this->integer());
        $this->addColumn('call_center_person_import_data', 'person_passport', $this->string(100));
        $this->addColumn('call_center_person_import_data', 'person_head', $this->string(100));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('call_center_person_import', 'cell_passport');
        $this->dropColumn('call_center_person_import', 'cell_head');
        $this->dropColumn('call_center_person_import_data', 'person_passport');
        $this->dropColumn('call_center_person_import_data', 'person_head');
    }
}
