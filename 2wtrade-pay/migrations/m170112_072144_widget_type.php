<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170112_072144_widget_type
 */
class m170112_072144_widget_type extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('widget_type', [
            'id' => $this->primaryKey(),
            'code' => $this->string(30)->unique(),
            'name' => $this->string(255),
            'status' => $this->integer()->notNull(),
        ], $this->tableOptions);

        $this->createIndex(null, 'widget_type', 'code');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('widget_type');
    }

}
