<?php
use app\components\CustomMigration as Migration;

/**
 * Class m181203_080119_add_charge_ups_for_mexico
 */
class m181203_080119_add_charge_ups_for_mexico extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('delivery_charges_calculator', [
            'name' => 'Ups',
            'class_path' => 'app\modules\delivery\components\charges\ups\Ups',
            'active' => 1,
            'created_at' => time(),
        ]);
        $this->insert('delivery_charges_calculator', [
            'name' => 'UpsMX',
            'class_path' => 'app\modules\delivery\components\charges\ups\UpsMX',
            'active' => 1,
            'created_at' => time(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete('delivery_charges_calculator', ['name' => 'Ups']);
        $this->delete('delivery_charges_calculator', ['name' => 'UpsMX']);
    }
}
