<?php

use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;
use app\models\Notification;
use app\models\NotificationRole;

/**
 * Add new warning about orders sended to delivery without tracking
 */
class m161224_111542_add_notification_order_daily_fluctuation extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
		$crontabTask = new CrontabTask();
        $crontabTask->name = 'report_order_dayly_fluctuation';
        $crontabTask->description = 'Суточные колебания заказов относительно среднего за месяц';
        $crontabTask->save();

        $model = new Notification([
            'description' => 'Количество вчерашних заказов {sign} чем их среднее число за предыдущий месяц на {delta}%',
            'trigger' => Notification::TRIGGER_REPORT_ORDER_DAILY_FLUCTUATION,
            'type' => Notification::TYPE_WARNING,
            'group' => Notification::GROUP_REPORT,
            'active' => Notification::ACTIVE,
        ]);

        $model->save(false);
		
		$model = new NotificationRole([
			'role'			=> 'country.curator',
			'trigger'		=> 'report.order.daily.fluctuation',
			]);

		$model->save(false);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
		$crontabTask = CrontabTask::find()
            ->byName('report_order_dayly_fluctuation')
            ->one();

        if ($crontabTask instanceof CrontabTask) {
            $crontabTask->delete();
        }

        $model = Notification::findOne(['trigger' => Notification::TRIGGER_REPORT_ORDER_DAILY_FLUCTUATION]);

        if ($model instanceof Notification) {
            $model->delete();
        }
		
		$model = NotificationRole::findOne(['trigger' => Notification::TRIGGER_REPORT_ORDER_DAILY_FLUCTUATION]);
		
		if ($model instanceof NotificationRole) {
			$model->delete();
		}
		
    }
}
