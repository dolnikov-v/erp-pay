<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryChargesCalculator;

/**
 * Class m170801_081208_add_bms_charges
 */
class m170801_081208_add_bms_charges extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert(DeliveryChargesCalculator::tableName(), [
            'name' => 'BmsTH',
            'class_path' => 'app\modules\delivery\components\charges\bms\BmsTH',
            'active' => 1,
            'created_at' => time(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete(DeliveryChargesCalculator::tableName(), ['name' => 'BmsTH']);
    }
}
