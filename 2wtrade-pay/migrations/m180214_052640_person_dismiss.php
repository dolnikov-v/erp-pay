<?php
use app\components\PermissionMigration as Migration;

use app\modules\salary\models\Person;

/**
 * Class m180214_052640_person_dismiss
 */
class m180214_052640_person_dismiss extends Migration
{

    protected $permissions = ['media.salaryperson.dismissalfile'];

    protected $roles = [
        'callcenter.manager' => [
            'media.salaryperson.dismissalfile'
        ],
        'salaryproject.clerk' => [
            'media.salaryperson.dismissalfile'
        ],
        'project.manager' => [
            'media.salaryperson.dismissalfile',
            'salary.person.deactivate'
        ]
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Person::tableName(), 'dismissal_comment', $this->string()->after('dismissal_date'));
        $this->addColumn(Person::tableName(), 'dismissal_file', $this->string()->after('dismissal_date'));

        parent::safeUp();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Person::tableName(), 'dismissal_comment');
        $this->dropColumn(Person::tableName(), 'dismissal_file');

        parent::safeDown();
    }
}
