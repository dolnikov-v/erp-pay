<?php

use yii\db\Migration;

/**
 * Handles adding permission to table `order_clarification`.
 */
class m170623_063950_add_permission_to_order_clarification extends Migration
{
    private $rules = [
        'order.clarification.index',
        'order.clarification.delete',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        foreach ($this->rules as $rule) {
            $time = time();
            $this->insert('{{%auth_item}}', array(
                'name' => $rule,
                'type' => 2,
                'description' => $rule,
                'created_at' => $time,
                'updated_at' => $time,
            ));
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach ($this->rules as $rule) {
            $this->delete('{{%auth_item}}', ['name' => $rule]);
        }
    }

}
