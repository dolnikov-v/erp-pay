<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160425_071607_fix_tables
 */
class m160425_071607_fix_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('crontab_task_log_answer', 'url', $this->text());

        $this->addColumn('order', 'call_center_id', $this->integer()->defaultValue(null) . ' AFTER `country_id`');
        $this->addForeignKey(null, 'order', 'call_center_id', 'call_center', 'id', self::SET_NULL, self::SET_NULL);

        $this->addColumn('order', 'call_center_sent_at', $this->integer()->defaultValue(null) . ' AFTER `updated_at`');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('order', 'call_center_sent_at');

        $this->dropForeignKey($this->getFkName('order', 'call_center_id'), 'order');
        $this->dropColumn('order', 'call_center_id');

        $this->alterColumn('crontab_task_log_answer', 'url', $this->string(1000));
    }
}
