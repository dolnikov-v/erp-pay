<?php

use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Handles the creation for table `crocotime_work_time`.
 */
class m170821_073510_create_crocotime_work_time extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('crocotime_work_time', [
            'id' => $this->primaryKey(),
            'crocotime_employee_id' => $this->integer(6),
            'date' => $this->integer(6),
            'effective_time' => $this->integer(6),
            'start_at' => $this->integer(6),
            'end_at' => $this->integer(6),
        ]);

        $crontabTask = new CrontabTask();
        $crontabTask->name = 'work_time';
        $crontabTask->active = 1;
        $crontabTask->description = 'Забирает статистику по рабочему времени crocotime.';
        $crontabTask->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('crocotime_work_time');
        $this->delete(CrontabTask::tableName(), ['name' => ['work_time']]);
    }
}
