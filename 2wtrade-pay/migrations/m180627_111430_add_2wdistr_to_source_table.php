<?php

use app\models\Source;
use app\modules\api\components\filters\auth\HttpBearerAuth;
use yii\db\Migration;

/**
 * Handles adding 2wdistr to table `source_table`.
 */
class m180627_111430_add_2wdistr_to_source_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $source = new Source();
        $source->name = HttpBearerAuth::ACCESS_TYPE_2WDISTR;
        $source->active = 1;
        $source->save();
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        Source::deleteAll(['name' => HttpBearerAuth::ACCESS_TYPE_2WDISTR]);
    }
}
