<?php
use app\components\PermissionMigration as Migration;

/**
 * Class m190225_045814_packager_send_in_delivery
 */
class m190225_045814_packager_send_in_delivery extends Migration
{
    protected $permissions = [
        'packager.request.sendindelivery',
        'packager.request.setapprove',
        'packager.request.getmanifest'
    ];

    protected $roles = [
        'logist' => [
            'packager.request.sendindelivery',
            'packager.request.setapprove',
            'packager.request.getmanifest'
        ],
        'admin' => [
            'packager.request.sendindelivery',
            'packager.request.setapprove',
            'packager.request.getmanifest'
        ],
        'controller.analyst' => [
            'packager.request.sendindelivery',
            'packager.request.setapprove',
            'packager.request.getmanifest'
        ],
        'country.curator' => [
            'packager.request.sendindelivery',
            'packager.request.setapprove',
            'packager.request.getmanifest'
        ],
        'implant' => [
            'packager.request.sendindelivery',
            'packager.request.setapprove',
            'packager.request.getmanifest'
        ],
        'junior.logist' => [
            'packager.request.sendindelivery',
            'packager.request.setapprove',
            'packager.request.getmanifest'
        ],
        'operational.director' => [
            'packager.request.sendindelivery',
            'packager.request.setapprove',
            'packager.request.getmanifest'
        ],
        'project.manager' => [
            'packager.request.sendindelivery',
            'packager.request.setapprove',
            'packager.request.getmanifest'
        ],
        'support.manager' => [
            'packager.request.sendindelivery',
            'packager.request.setapprove',
            'packager.request.getmanifest'
        ],
        'technical.director' => [
            'packager.request.sendindelivery',
            'packager.request.setapprove',
            'packager.request.getmanifest'
        ],
    ];
}
