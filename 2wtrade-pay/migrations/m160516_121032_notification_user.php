<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160516_121032_notification_user
 */
class m160516_121032_notification_user extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('notification_user', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'trigger' => $this->string(255)->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $this->tableOptions);
        $this->addForeignKey(
            null, 'notification_user', 'user_id', 'user', 'id', 'CASCADE'
        );
        $this->addForeignKey(
            null, 'notification_user', 'trigger', 'notification', 'trigger', 'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('notification_user');
    }
}
