<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m180416_060854_cron_notification_chat_coordination
 */
class m180416_060854_cron_notification_chat_coordination extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_SKYPE_BUYOUTS_COORDINATION]);
        if (!$crontabTask) {
            $crontabTask = new CrontabTask();
            $crontabTask->name = CrontabTask::TASK_SKYPE_BUYOUTS_COORDINATION;
            $crontabTask->description = "Сообщение в чат Координация о выкупах по командам";
            $crontabTask->save();
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_SKYPE_BUYOUTS_COORDINATION]);
        if ($crontabTask) {
            $crontabTask->delete();
        }
    }
}
