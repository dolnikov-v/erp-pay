<?php
use app\components\CustomMigration as Migration;

/**
 * Class m171116_090659_calculated_params
 */
class m171116_090659_calculated_params extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('calculated_param', [
            'id' => $this->primaryKey(),
            'name' => $this->string(50)->notNull(),
            'value' => $this->decimal(15,2)->defaultValue(null),
            'product_id' => $this->integer(),
            'country_id' => $this->integer(),
            'params' => $this->string(300),
            'cron_launched_at' => $this->integer(),
        ]);

        $this->addForeignKey(null, 'calculated_param', 'country_id', 'country', 'id', self::CASCADE, self::NO_ACTION);
        $this->addForeignKey(null, 'calculated_param', 'product_id', 'product', 'id', self::CASCADE, self::NO_ACTION);
        $this->createIndex(null, 'calculated_param', 'name');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('calculated_param');
    }
}
