<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryApiClass;
use app\modules\delivery\models\Delivery;
use app\models\Country;

/**
 * Class m170116_102530_add_leopardApi
 */
class m170116_102530_add_leopardApi extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        if (DeliveryApiClass::find()->where(
            ['name' => 'leopardApi', 'class_path' => '/leopard-api/src/leopardApi.php']
        )->exists()
        ) {
            $apiClass = DeliveryApiClass::findOne(
                ['name' => 'leopardApi', 'class_path' => '/leopard-api/src/leopardApi.php']
            );
        } else {
            $apiClass = new DeliveryApiClass();
        }
        $apiClass->name = 'leopardApi';
        $apiClass->class_path = '/leopard-api/src/leopardApi.php';
        $apiClass->active = '1';
        $apiClass->created_at = time();
        $apiClass->updated_at = time();
        $apiClass->save();

        if (Country::find()->where(
            [
                'name' => 'Пакистан',
                'char_code' => 'PK'
            ]
        )->exists()
        ) {
            $country = Country::find()->where(
                [
                    'name' => 'Пакистан',
                    'char_code' => 'PK'
                ]
            )->select('id')->one();
        } else {
            //нет страны
            return false;
        }

        if (Delivery::find()->where(
            [
                'name' => 'Leopard',
                'char_code' => 'LEO'
            ]
        )->exists()
        ) {
            $delivery = Delivery::findOne(
                [
                    'name' => 'Leopard',
                    'char_code' => 'LEO'
                ]
            );
        } else {
            $delivery = new Delivery();
        }
        $delivery->country_id = $country->id;
        $delivery->name = 'Leopard';
        $delivery->char_code = 'LEO';
        $delivery->api_class_id = $apiClass->id;
        $delivery->active = '1';
        $delivery->auto_sending = '0';
        $delivery->save(false);
        return true;
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        if (Country::find()->where(
            [
                'name' => 'Пакистан',
                'char_code' => 'PK'
            ]
        )->exists()
        ) {
            $country = Country::find()->where(
                [
                    'name' => 'Пакистан',
                    'char_code' => 'PK'
                ]
            )->select('id')->one();
        } else {
            //нет страны
            return false;
        }

        DeliveryApiClass::find()
            ->where([
                'name' => 'leopardApi',
                'class_path' => '/leopard-api/src/leopardApi.php'
            ])
            ->one()
            ->delete();

        Delivery::findOne(
            [
                'name' => 'Leopard',
                'country_id' => $country->id
            ]
        )->delete();
        return true;
    }
}
