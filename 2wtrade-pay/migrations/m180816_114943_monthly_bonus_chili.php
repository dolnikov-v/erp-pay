<?php
use app\components\CustomMigration as Migration;
use app\modules\salary\models\BonusType;
use app\modules\salary\models\Office;
use app\models\Country;
use app\modules\salary\models\Staffing;
use app\modules\salary\models\StaffingBonus;


/**
 * Class m180816_114943_monthly_bonus_chili
 *
 *
 * Миграция для заполнения данными, можно пропустить на локальном
 * Создать бонус 25% от оклада для Чили
 *
 */
class m180816_114943_monthly_bonus_chili extends Migration
{

    const CHAR_CODE = 'CL';
    const BONUS_NAME = '25% от оклада Чили';
    const BONUS_PERCENT = 25;

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $offices = Office::find()
            ->joinWith('country')
            ->where([Country::tableName() . '.char_code' => self::CHAR_CODE])
            ->all();

        foreach ($offices as $office) {

            $bonusType = BonusType::find()
                ->where(['office_id' => $office->id])
                ->andWhere(['name' => self::BONUS_NAME])
                ->andWhere(['type' => BonusType::BONUS_TYPE_PERCENT])
                ->andWhere(['percent' => self::BONUS_PERCENT])
                ->andWhere(['active' => 1])
                ->one();

            if (!$bonusType instanceof BonusType) {
                $bonusType = new BonusType();
                $bonusType->office_id = $office->id;
                $bonusType->name = self::BONUS_NAME;
                $bonusType->type = BonusType::BONUS_TYPE_PERCENT;
                $bonusType->percent = self::BONUS_PERCENT;
                $bonusType->active = 1;
                $bonusType->save();
            }

            $staffings = Staffing::find()
                ->byOfficeId($office->id)
                ->all();

            foreach ($staffings as $staffing) {

                if (!StaffingBonus::find()
                    ->where(['staffing_id' => $staffing->id])
                    ->andWhere(['bonus_type_id' => $bonusType->id])
                    ->exists()
                ) {
                    $staffingBonus = new StaffingBonus();
                    $staffingBonus->staffing_id = $staffing->id;
                    $staffingBonus->bonus_type_id = $bonusType->id;
                    $staffingBonus->day = StaffingBonus::DAY_LAST;
                    $staffingBonus->save();
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

    }
}
