<?php
use app\components\CustomMigration as Migration;

/**
 * Class m171024_091031_permission_to_current_balance_report
 */
class m171024_161031_permission_to_current_balance_report extends Migration
{
    /**
     * @var array
     */
    public $permissions = [
        'finance.reportexpensescategory.edit',
        'finance.reportexpensescategory.index',
        'finance.reportexpensescategory.delete',
        'finance.reportexpensescountryitem.editcategory',
        'finance.reportexpensescountryitem.deletecategory',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        foreach ($this->permissions as $permission) {
            $this->insert('{{%auth_item}}', [
                'name' => $permission,
                'type' => '2',
                'description' => $permission,
                'created_at' => time(),
                'updated_at' => time()
            ]);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach ($this->permissions as $permission) {
            $this->delete('{{%auth_item}}', ['name' => $permission]);
        }
    }
}
