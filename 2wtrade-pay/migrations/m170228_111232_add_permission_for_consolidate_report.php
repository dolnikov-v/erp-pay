<?php

use app\components\CustomMigration as Migration;

/**
 * Handles adding permission `report.consolidatereport.index` for BA & CC users.
 */
class m170228_111232_add_permission_for_consolidate_report extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}',array(
            'name'=>'report.consolidatereport.index',
            'type' => '2',
            'description' => 'report.consolidatereport.index',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'country.curator',
            'child' => 'report.consolidatereport.index'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'business_analyst',
            'child' => 'report.consolidatereport.index'
        ));

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->authManager->itemChildTable, ['child' => 'report.consolidatereport.index', 'parent' => 'country.curator']);
        $this->delete($this->authManager->itemChildTable, ['child' => 'report.consolidatereport.index', 'parent' => 'business_analyst']);
        $this->delete('{{%auth_item}}', ['name' => 'report.consolidatereport.index']);
    }
}
