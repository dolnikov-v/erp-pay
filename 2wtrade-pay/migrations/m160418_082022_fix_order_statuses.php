<?php
use app\components\CustomMigration as Migration;
use app\modules\order\models\OrderStatus;

class m160418_082022_fix_order_statuses extends Migration
{
    private static $groupsMap = [
        1 => OrderStatus::GROUP_SOURCE,
        2 => OrderStatus::GROUP_CALL_CENTER,
        3 => OrderStatus::GROUP_LOGISTIC,
        4 => OrderStatus::GROUP_DELIVERY,
        5 => OrderStatus::GROUP_FINANCE,
    ];

    private static $oldGroupsMap = [
        OrderStatus::GROUP_SOURCE => 1,
        OrderStatus::GROUP_CALL_CENTER => 2,
        OrderStatus::GROUP_LOGISTIC => 3,
        OrderStatus::GROUP_DELIVERY => 4,
        OrderStatus::GROUP_FINANCE => 5,
    ];

    public function safeUp()
    {
        $this->dropColumn('order_status', 'parent');
        $this->dropColumn('order_status', 'children');

        $this->renameColumn('order_status', 'group', 'old_group');
        $this->addColumn('order_status', 'group', "ENUM('adcombo', 'call_center', 'logistic', 'delivery', 'finance') DEFAULT NULL");

        /** @var OrderStatus[] $statuses */
        $statuses = OrderStatus::find()->all();

        foreach ($statuses as $status) {
            $status->group = self::$groupsMap[$status->old_group];
            $status->save(false, ['group']);
        }

        $this->dropColumn('order_status', 'old_group');
        $this->dropColumn('order_status', 'full_name');

        $this->createIndex('idx_order_status_group', 'order_status', 'group');
    }

    public function safeDown()
    {
        $this->addColumn('order_status', 'parent', $this->string(50)->defaultValue(null));
        $this->addColumn('order_status', 'children', $this->string(50)->defaultValue(null));

        $this->renameColumn('order_status', 'group', 'old_group');
        $this->addColumn('order_status', 'group', $this->integer()->defaultValue(null));

        /** @var OrderStatus[] $statuses */
        $statuses = OrderStatus::find()->all();

        foreach ($statuses as $status) {
            $status->group = self::$oldGroupsMap[$status->old_group];
            $status->save(false, ['group']);
        }

        $this->dropColumn('order_status', 'old_group');
        $this->addColumn('order_status', 'full_name', $this->string()->defaultValue(null));
    }
}
