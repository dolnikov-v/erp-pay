<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170209_041159_report_delivery_closed_periods_add_roles
 */
class m170209_041159_report_delivery_closed_periods_add_roles extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $rules = [
            'report.deliveryclosedperiods.index',
            'report.deliveryclosedperiods.paymentperperiod',
        ];

        $roles = [
            'business_analyst',
            'country.curator',
            'project.manager'
        ];

        foreach ($rules as $rule) {

            $this->insert($this->authManager->itemTable, [
                'name' => $rule,
                'type' => 2,
                'description' => $rule,
                'created_at' => time(),
                'updated_at' => time()
            ]);

            foreach ($roles as $role) {

                $this->insert($this->authManager->itemChildTable, [
                    'parent' => $role,
                    'child' => $rule
                ]);
            }
        }

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

        $rules = [
            'report.deliveryclosedperiods.index',
            'report.deliveryclosedperiods.paymentperperiod',
        ];

        $roles = [
            'business_analyst',
            'country.curator',
            'project.manager'
        ];

        foreach ($rules as $rule) {

            foreach ($roles as $role) {
                $this->delete($this->authManager->itemChildTable, [
                    'parent' => $role,
                    'child' => $rule
                ]);
            }

            $this->delete($this->authManager->itemTable, [
                'name' => $rule,
                'type' => 2
            ]);
        }
    }
}
