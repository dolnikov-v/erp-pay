<?php
use app\components\CustomMigration as Migration;

/**
 * Class m180903_082759_telegram_auth
 */
class m180903_082759_telegram_auth extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('telegram_bot', [
            'id' => $this->primaryKey()->notNull(),
            'token' => $this->string(255)->notNull(),
            'name' => $this->string(255),
            'description' => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);
        $this->insert('telegram_bot',['id' => 490778274, 'token' => 'AAF2Pal760NlMTbPPsVmJ48M1ySTCZSzpaE', 'name' => '2trade']);

        $this->createTable('telegram_connection', [
            'bot_id' => $this->integer(),
            'user_id' => $this->integer(),
            'chat_id' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);
        $this->addPrimaryKey(null, 'telegram_connection', ['bot_id', 'user_id']);
        $this->addForeignKey(null, 'telegram_connection', 'bot_id', 'telegram_bot', 'id', self::CASCADE, self::CASCADE);
        $this->addForeignKey(null, 'telegram_connection', 'user_id', '{{%user}}', 'id', self::CASCADE, self::CASCADE);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('telegram_connection');
        $this->dropTable('telegram_bot');
    }
}