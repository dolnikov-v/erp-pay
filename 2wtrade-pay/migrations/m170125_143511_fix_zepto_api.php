<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryApiClass;

/**
 * Class m170125_143511_fix_zepto_api
 */
class m170125_143511_fix_zepto_api extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $apiClass = DeliveryApiClass::findOne(
            ['name' => 'zeptoApi', 'class_path' => '/zepto-api/src/zeptoApi.php']
        );
        if(!is_null($apiClass)) {
            $apiClass->class_path = '/zepto-api/src/ZeptoApi.php';
            $apiClass->save();
        }
        return true;
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $apiClass = DeliveryApiClass::findOne(
            ['name' => 'zeptoApi', 'class_path' => '/zepto-api/src/ZeptoApi.php']
        );
        if(!is_null($apiClass)) {
            $apiClass->class_path = '/zepto-api/src/zeptoApi.php';
            $apiClass->save();
        }
        return true;
    }
}
