<?php
use app\components\CustomMigration as Migration;
use app\modules\catalog\models\RequisiteDelivery;
use app\modules\delivery\models\DeliveryContract;

/**
 * Class m180405_054119_add_requisite_delivery_id
 */
class m180405_054119_add_requisite_delivery_id extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(DeliveryContract::tableName(), 'requisite_delivery_id', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(DeliveryContract::tableName(), 'requisite_delivery_id');
    }
}
