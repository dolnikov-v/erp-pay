<?php
use app\components\CustomMigration as Migration;
use \yii\db\Query;

/**
 * Class m170206_040113_report_buyout_by_country_access
 */
class m170206_040113_report_buyout_by_country_access extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $report = "report.buyoutbycountry.index";
        $role = "business_analyst";

        $query = new Query();

        $is_businessanalyst = $query->select('*')->from($this->authManager->itemTable)
            ->where(['name' => $role, 'type' => 1])
            ->one();

        if (!$is_businessanalyst) {
            echo "Error: No ".$role." exists";
            die();
        }

        $is_report = $query->select('*')->from($this->authManager->itemTable)
            ->where([
                'name' => $report,
                'type' => 2])
            ->one();

        if (!$is_report) {
            $this->insert($this->authManager->itemTable, [
                'name' => $report,
                'type' => 2,
                'description' => $report,
                'created_at' => time(),
                'updated_at' => time()]);
        }

        $is_cm = $query->select('*')->from($this->authManager->itemChildTable)->where([
            'parent' => $role,
            'child' => $report
        ])->one();

        if (!$is_cm) {
            $this->insert($this->authManager->itemChildTable, [
                'parent' => $role,
                'child' => $report
            ]);
        }

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

        $report = "report.buyoutbycountry.index";
        $role = "business_analyst";
        
        $this->delete($this->authManager->itemChildTable, [
            'parent' => $role,
            'child' => $report
        ]);
    }
}
