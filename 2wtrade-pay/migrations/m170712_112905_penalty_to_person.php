<?php
use app\components\CustomMigration as Migration;
use app\modules\salary\models\Person;
use app\modules\salary\models\Penalty;
use app\modules\salary\models\PenaltyType;
use app\models\User;
use yii\db\Query;

/**
 * Class m170712_112905_penalty_to_person
 */
class m170712_112905_penalty_to_person extends Migration
{

    const ROLES = [
        'country.curator',
        'callcenter.manager',
        'callcenter.hr',
    ];

    const RULES = [
        'callcenter.person.getparents',
        'callcenter.person.getpersons',
    ];


    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        $query = new Query();

        foreach (self::RULES as $rule) {

            $is_rule = $query->select('*')->from($this->authManager->itemTable)
                ->where(['name' => $rule, 'type' => 2])
                ->one();

            if (!$is_rule) {
                $this->insert($this->authManager->itemTable, [
                    'name' => $rule,
                    'description' => $rule,
                    'type' => 2,
                    'created_at' => time(),
                    'updated_at' => time()]);
            }

            foreach (self::ROLES as $role) {
                $is_can = $query->select('*')->from($this->authManager->itemChildTable)->where([
                    'parent' => $role,
                    'child' => $rule
                ])->one();

                if (!$is_can) {
                    $this->insert($this->authManager->itemChildTable, [
                        'parent' => $role,
                        'child' => $rule
                    ]);
                }
            }
        }

        $tableSchema = Yii::$app->db->schema->getTableSchema('call_center_user_penalty');
        if ($tableSchema !== null) {
            $this->dropTable('call_center_user_penalty');
        }

        $tableSchema = Yii::$app->db->schema->getTableSchema('call_center_penalty');
        if ($tableSchema !== null) {
            $this->dropTable('call_center_penalty');
        }

        $this->createTable(Penalty::tableName(), [
            'id' => $this->primaryKey(),
            'person_id' => $this->integer()->notNull(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'penalty_type_id' => $this->integer(),
            'penalty_percent' => $this->integer(),
            'penalty_sum_usd' => $this->float(),
            'penalty_sum_local' => $this->float(),
            'comment' => $this->string(255),
            'penalty_at' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addForeignKey(null, Penalty::tableName(), 'person_id', Person::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, Penalty::tableName(), 'penalty_type_id', PenaltyType::tableName(), 'id', self::SET_NULL, self::NO_ACTION);
        $this->addForeignKey(null, Penalty::tableName(), 'created_by', User::tableName(), 'id', self::SET_NULL, self::NO_ACTION);
        $this->addForeignKey(null, Penalty::tableName(), 'updated_by', User::tableName(), 'id', self::SET_NULL, self::NO_ACTION);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach (self::RULES as $rule) {
            foreach (self::ROLES as $role) {
                $this->delete($this->authManager->itemChildTable, [
                    'parent' => $role,
                    'child' => $rule
                ]);
            }
        }

        $tableSchema = Yii::$app->db->schema->getTableSchema('call_center_penalty');
        if ($tableSchema !== null) {
            $this->dropTable('call_center_penalty');
        }
    }
}
