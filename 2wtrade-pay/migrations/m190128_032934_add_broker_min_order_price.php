<?php
use app\components\CustomMigration as Migration;

/**
 * Class m190128_032934_add_broker_min_order_price
 */
class m190128_032934_add_broker_min_order_price extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('delivery', 'brokerage_min_order_price', $this->integer()->after('brokerage_active'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('delivery', 'brokerage_min_order_price');
    }
}
