<?php
use app\components\PermissionMigration as Migration;

/**
 * Class m180507_062837_fix_permission_for_fin_manager
 */
class m180507_062837_fix_permission_for_fin_manager extends Migration
{
    protected $permissions = [
        'report.invoice.getfinancereports',
        'catalog.requisitedelivery.getrequisitebyinvoice'
    ];



    protected $roles = [
        'finance.manager' => [
            'report.invoice.getfinancereports',
        ],
        'project.manager' => [
            'report.invoice.getfinancereports',
            'catalog.requisitedelivery.getrequisitebyinvoice',
        ],
        'business_analyst' => [
            'report.invoice.getfinancereports',
            'catalog.requisitedelivery.getrequisitebyinvoice',
        ],
        'controller.analyst' => [
            'report.invoice.getfinancereports',
            'catalog.requisitedelivery.getrequisitebyinvoice',
        ],
        'development.director' => [
            'report.invoice.getfinancereports',
            'catalog.requisitedelivery.getrequisitebyinvoice',
        ],
        'implant' => [
            'report.invoice.getfinancereports',
            'catalog.requisitedelivery.getrequisitebyinvoice',
        ],
        'technical.director' => [
            'report.invoice.getfinancereports',
            'catalog.requisitedelivery.getrequisitebyinvoice',
        ],
        'support.manager' => [
            'report.invoice.getfinancereports',
            'catalog.requisitedelivery.getrequisitebyinvoice',
        ],
        'logist' => [
            'report.invoice.getfinancereports',
            'catalog.requisitedelivery.getrequisitebyinvoice',
        ],
        'partner' => [
            'report.invoice.getfinancereports',
            'catalog.requisitedelivery.getrequisitebyinvoice',
        ],
    ];
}
