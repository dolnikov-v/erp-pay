<?php

use app\components\CustomMigration as Migration;

/**
 * Handles adding category to table `report_expenses_item`.
 */
class m170809_122044_add_category_to_report_expenses_item extends Migration
{
    /**
     * @var array
     */
    public $permissions = [
        'report.profitabilityanalysis.index' => 'finance.reportprofitabilityanalysis.index',
        'report.profitabilityanalysis.setparam' => 'finance.reportprofitabilityanalysis.setparam',
    ];

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('report_expenses_item', 'category', $this->string(255));
        try {
            $this->dropIndex('code', 'report_expenses_item');  // Необратимая миграция, заключена в исключение чтобы можно было перенакатить миграцию без ошибки
        } catch (Exception $exception ) {}

        $this->createTable('report_expenses_country_item_product', [
            'id' => $this->primaryKey(),
            'item_id' => $this->integer(),
            'product_id' => $this->integer(),
            'value' => $this->string(255),
        ]);

        $role = $this->authManager->getRole('finance.director');
        foreach ($this->permissions as $old => $new) {
            $authPermission = $this->authManager->getPermission($old);
            $this->authManager->removeChild($role, $authPermission);
            $this->authManager->remove($authPermission);

            $authPermission = $this->authManager->createPermission($new);
            $authPermission->description = $new;
            $this->authManager->add($authPermission);
            if ($this->authManager->canAddChild($role, $authPermission)) {
                $this->authManager->addChild($role, $authPermission);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('report_expenses_country_item_product');
        $this->dropColumn('report_expenses_item', 'category');

        $role = $this->authManager->getRole('finance.director');
        foreach ($this->permissions as $old => $new) {
            $authPermission = $this->authManager->getPermission($new);
            $this->authManager->removeChild($role, $authPermission);
            $this->authManager->remove($authPermission);

            $authPermission = $this->authManager->createPermission($old);
            $authPermission->description = $old;
            $this->authManager->add($authPermission);
            if ($this->authManager->canAddChild($role, $authPermission)) {
                $this->authManager->addChild($role, $authPermission);
            }
        }
    }
}
