<?php

use app\components\CustomMigration as Migration;

/**
 * Class m181120_102929_jer_currirer_charges_calculator
 */
class m181120_102929_jer_currirer_charges_calculator extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('delivery_charges_calculator', [
            'name' => 'JerCurrierMX',
            'class_path' => 'app\modules\delivery\components\charges\jercurrier\JerCurrierMX',
            'active' => 1,
            'created_at' => time(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete('delivery_charges_calculator', [
            'name' => 'JerCurrierMX',
        ]);
    }
}
