<?php
use app\components\CustomMigration as Migration;
use app\models\Product;
use yii\db\Query;

/**
 * Class m170921_100506_product_catalog
 */
class m170921_100506_product_catalog extends Migration
{

    const RULES = [
        'catalog.producttranslate.edit',
        'catalog.producttranslate.delete',
        'catalog.product.deleteimage',
    ];

    const ROLES = [
        'admin',
        'project.manager',
        'storage.manager',
        'translator',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Product::tableName(), 'description', $this->text()->after('length'));
        $this->addColumn(Product::tableName(), 'image', 'MEDIUMTEXT after description');

        $this->createTable('product_translation', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer()->notNull(),
            'description' => $this->text(),
            'language' => $this->string(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $this->tableOptions);

        $this->addForeignKey(null, 'product_translation', 'product_id', 'product', 'id', self::CASCADE, self::RESTRICT);

        $query = new Query();

        foreach (self::RULES as $rule) {

            $is_rule = $query->select('*')->from($this->authManager->itemTable)
                ->where(['name' => $rule, 'type' => 2])
                ->one();

            if (!$is_rule) {
                $this->insert($this->authManager->itemTable, [
                    'name' => $rule,
                    'description' => $rule,
                    'type' => 2,
                    'created_at' => time(),
                    'updated_at' => time()]);
            }

            foreach (self::ROLES as $role) {
                $is_can = $query->select('*')->from($this->authManager->itemChildTable)->where([
                    'parent' => $role,
                    'child' => $rule
                ])->one();

                if (!$is_can) {
                    $this->insert($this->authManager->itemChildTable, [
                        'parent' => $role,
                        'child' => $rule
                    ]);
                }
            }
        }

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Product::tableName(), 'description');
        $this->dropColumn(Product::tableName(), 'image');

        $this->dropTable('product_translation');

        foreach (self::RULES as $rule) {
            foreach (self::ROLES as $role) {
                $this->delete($this->authManager->itemChildTable, [
                    'parent' => $role,
                    'child' => $rule
                ]);
            }
        }
    }
}
