<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m170914_111901_del_temp
 */
class m170914_111901_del_temp extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropTable('temp_log_orders');

        CrontabTask::deleteAll(['name' => 'clean_cron_process_list']);

        $this->dropTable('cron_process_list');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->createTable('temp_log_orders', [
            'order_id' => $this->integer()->notNull(), // заказ
            'begin' => $this->integer()->notNull(), // старт крона
            'end' => $this->integer()->notNull(), // конец крона
            'block_file' => $this->string(300), // файл блокировки
            'string_params_cron' => $this->string(300), // строка с параметрами запуска крона
        ]);

        $this->addForeignKey(null, 'temp_log_orders', 'order_id', 'order', 'id');


        $crontabTask = new CrontabTask();
        $crontabTask->name = 'clean_cron_process_list';
        $crontabTask->description = 'Очистка зависших заявок крона';
        $crontabTask->save();


        $this->createTable('cron_process_list', [
            'request_id' => $this->integer()->notNull(), // заявка
            'cron_launched_at' => $this->integer()->notNull(), // старт крона
            'type_cron' => $this->string(300)->notNull(), // тип крона
        ]);

        $this->createIndex(null, 'cron_process_list', 'request_id');
        $this->createIndex(null, 'cron_process_list', 'type_cron');

        $this->addColumn('cron_process_list', 'pid', $this->integer());
    }
}
