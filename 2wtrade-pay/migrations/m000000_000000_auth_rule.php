<?php
use app\components\CustomMigration as Migration;

class m000000_000000_auth_rule extends Migration
{
    public function up()
    {
        $this->createTable($this->authManager->ruleTable, [
            'name' => $this->string(64)->notNull(),
            'data' => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            $this->includePrimaryKey('name'),
        ], $this->tableOptions);
    }

    public function down()
    {
        $this->dropTable($this->authManager->ruleTable);
    }
}
