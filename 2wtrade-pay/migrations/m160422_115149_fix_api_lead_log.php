<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160422_115149_fix_api_lead_log
 */
class m160422_115149_fix_api_lead_log extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->renameColumn('api_lead_log', 'message', 'output_data');
        $this->addColumn('api_lead_log', 'input_data', $this->text() . ' AFTER `id`');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('api_lead_log', 'input_data');
        $this->renameColumn('api_lead_log', 'output_data', 'message');
    }
}
