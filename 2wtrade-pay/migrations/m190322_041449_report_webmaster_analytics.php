<?php

use app\components\PermissionMigration as Migration;

/**
 * Class m190322_041449_report_webmaster_analytics
 */
class m190322_041449_report_webmaster_analytics extends Migration
{

    protected $permissions = ['report.webmaster.analytics'];

    protected $roleList = [
        'admin',
        'auditor',
        'business_analyst',
        'controller.analyst',
        'country.curator',
        'Demand & Supply Planner',
        'finance.director',
        'operational.director',
        'project.manager',
        'quality.manager',
        'sales.director',
        'support.manager',
        'technical.director'
    ];

}
