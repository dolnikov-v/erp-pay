<?php
use app\components\CustomMigration as Migration;
use app\models\AuthItem;
use yii\helpers\ArrayHelper;

/**
 * Class m180627_113251_add_permission_for_partners_manager
 */
class m180627_113251_add_permission_for_partners_manager extends Migration
{
    public $role = 'partners.manager';

    public $permissions = [
        'order.index.transfertodistributor',
        'old_orders_access',
        'home.index.index',
        'order.index.index',
        'report.delivery.index',
        'report.callcentercheckrequest.index',
        'report.reportstandartdelivery.index',
        'report.callcenteroperatorsonline.index',
        'report.callcenter.index',
        'storage.control.default',
        'storage.control.edit',
        'storage.control.index',
        'callcenter.control.activate',
        'callcenter.control.deactivate',
        'callcenter.control.edit',
        'callcenter.control.index',
        'callcenter.control.delete',
        'delivery.control.activate',
        'delivery.control.activateautolistgeneration',
        'delivery.control.activateautosending',
        'delivery.control.activateexpress',
        'delivery.control.activatebrokerage',
        'delivery.control.contractdelete',
        'delivery.control.contractedit',
        'delivery.control.contractfiledelete',
        'delivery.control.contracts',
        'delivery.control.contractview',
        'delivery.control.deactivate',
        'delivery.control.deactivateautolistgeneration',
        'delivery.control.deactivateautosending',
        'delivery.control.deactivatebrokerage',
        'delivery.control.deactivateexpress',
        'delivery.control.delete',
        'delivery.control.edit',
        'delivery.control.editchargesvalues',
        'delivery.control.index',
        'delivery.control.logs',
        'delivery.control.setstorage',
        'delivery.control.setzipcodes',
        'delivery.control.sort',
        'report.salary.index',
        'report.timesheet.index'
    ];
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $salaryPermissions = ArrayHelper::getColumn(AuthItem::find()
            ->where(['like', 'name', 'salary.'])
            ->andWhere(['type' => 2])
            ->all(), 'name');
        $permissions = array_unique(array_merge($this->permissions, $salaryPermissions));
        foreach ($permissions as $permission) {
            $this->insert($this->authManager->itemChildTable, [
                'parent' => $this->role,
                'child' => $permission
            ]);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach ($this->new_permissions as $permission => $description) {
            $permission = $this->authManager->getPermission($permission);
            $this->authManager->remove($permission);
        }

    }
}
