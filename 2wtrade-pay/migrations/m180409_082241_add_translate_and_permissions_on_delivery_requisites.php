<?php

use app\components\CustomMigration as Migration;

/**
 * Class m180409_082241_add_translate_and_permissions_on_delivery_requisites
 */
class m180409_082241_add_translate_and_permissions_on_delivery_requisites extends Migration
{
    private static $translations = [
        'Наименование компании' => 'Company name',
        'Адрес' => 'Address',
        'Город' => 'City',
        'Адрес банка' => 'Bank Address',
        '№ счета' => 'Bank account',
        'Банк получателя' => "Beneficiary's bank",
        'SWIFT код' => 'SWIFT Code',
        'Таблица с реквизитами' => 'Table with requisites',
        'Реквизиты' => 'Requisites',
        'Добавление реквизита' => 'Adding requisite',
        'Редактирование реквизита' => 'Edit requisite',
        'Добавить реквизит' => 'Add requisite',
        'Сохранить реквизит' => 'Save requisite',
        'Реквизит успешно добавлен.' => 'Requisite have been successfully added.',
        'Реквизит успешно отредактирован.' => 'Requisite were successfully edited.',
        'Реквизит успешно удален.' => 'Requisite were successfully deleted.',
        'Реквизит' => 'Requisite',
        'Ошибка обновления прикреплённых КС' => 'Error updating the attached delivery services',
        'Ошибка добавления КС: {error}' => 'Error adding delivery service: {error}',
        'Список контрактов курьерских служб' => 'List of delivery services contracts',
        'По умолчанию' => 'Default',
        'Контракты' => 'Contracts',
        'Бессрочный' => 'Unlimited',
        'Инвойс #{id} не найден' => 'Invoice # {id} not found',
        'Реквизиты для КС не найдены' => 'Requisites for delivery services not found',
        'Контракты для реквизитов у КС: {delivery} в стране: {country} не найдены' => 'Contracts for the requisites of the delivery: {delivery} in the country: {country} not found',
        'Укажите реквизиты' => 'Specify the requisites',
        'Реквизиты не обнаружены. Необходимо добавить в систему реквизиты: Справочники->Реквизиты' => 'Requisites not found. It is necessary to add the following requisites to the system: Directories-> Requisites',
        'Реквизит по умочанию' => 'Requisite by default',
        'Продолжить' => 'Continue',
        'Невозможно сгенерировать инвойс, отсутствуют реквизиты.' => 'It is impossible to generate an invoice, there are no requisites.',
        'Юридическое название' => 'Legal name',
        'Невозможно сгенерировать инвойс, отсутствует юридическое название.' => 'It is impossible to generate an invoice, there is no legal name.',
        'Справочники / Реквизиты (список)' => 'Directories / Requisites (List)',
        'Справочники / Реквизиты (редактированеи)' => 'Directories / Requisites (editing)',
        'Справочники / Реквизиты (удаление)' => 'Directories / Requisites (deletion)',
        'Справочники / Реквизиты (просмотр)' => 'Directories / Requisites (view)',
    ];


    private $editPermissions = [
        'catalog.requisitedelivery.index' => 'Справочники / Реквизиты (список)',
        'catalog.requisitedelivery.edit' => 'Справочники / Реквизиты (редактированеи)',
        'catalog.requisitedelivery.delete' => 'Справочники / Реквизиты (удаление)',
        'catalog.requisitedelivery.view' => 'Справочники / Реквизиты (просмотр)'
    ];

    private $viewPermissions = [
        'catalog.requisitedelivery.index' => '',
        'catalog.requisitedelivery.view' => ''
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        foreach (self::$translations as $ruText => $enText) {
            $this->insert('i18n_source_message', ['category' => 'common', 'message' => $ruText]);
            $id = Yii::$app->db->getLastInsertID();
            $this->insert('i18n_message', ['id' => $id, 'language' => 'en-US', 'translation' => $enText]);
        }

        //добавить пермишены
        foreach (array_merge($this->editPermissions, $this->viewPermissions) as $name=>$desc) {
            $this->insert('{{%auth_item}}', [
                'name' => $name,
                'type' => '2',
                'description' => $desc,
                'created_at' => time(),
                'updated_at' => time()
            ]);
        }

        //добавить для ролей (управление)
        foreach ($this->editPermissions as $permission=>$desc) {
            $this->insert($this->authManager->itemChildTable, array(
                'parent' => 'finance.director',
                'child' => $permission
            ));

            $this->insert($this->authManager->itemChildTable, array(
                'parent' => 'finance.manager',
                'child' => $permission
            ));
        }


        //добавить для ролей (просмотр)
        foreach ($this->viewPermissions as $permission => $desc) {
            $this->insert($this->authManager->itemChildTable, array(
                'parent' => 'country.curator',
                'child' => $permission
            ));
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach (self::$translations as $ruText => $enText) {
            $this->delete('i18n_source_message', ['category' => 'common', 'message' => $ruText]);
        }
        //грохаем пермишену у ролей
        foreach (array_merge($this->editPermissions, $this->viewPermissions) as $name=>$desc) {
            $this->delete($this->authManager->itemChildTable, ['child' => $name, 'parent' => 'finance.director']);
        }

        foreach (array_merge($this->editPermissions, $this->viewPermissions) as $name=>$desc) {
            $this->delete($this->authManager->itemChildTable, ['child' => $name, 'parent' => 'finance.manager']);
        }

        foreach ($this->viewPermissions as $permission => $desc) {
            $this->delete($this->authManager->itemChildTable, ['child' => $permission, 'parent' => 'country.curator']);
        }

        //грохнуть сами пермишены

        foreach (array_merge($this->editPermissions, $this->viewPermissions) as $name=>$desc) {
            $this->delete('{{%auth_item}}', ['name' => $name]);
        }
    }
}
