<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryApiClass;

/**
 * Class m170502_104835_add_api_focusonexpress
 */
class m170502_104835_add_api_focusonexpress extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $apiClass = new DeliveryApiClass();
        $apiClass->name = 'Focus On Express';
        $apiClass->class_path = '/focusonexpress-api/src/focusonexpressApi.php';
        $apiClass->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        DeliveryApiClass::find()
            ->where([
                'name' => 'Focus On Express',
            ])
            ->one()
            ->delete();
    }
}
