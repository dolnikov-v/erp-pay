<?php
use app\components\CustomMigration as Migration;
use app\models\AuthItem;

/**
 * Class m170322_152338_add_check_undelivery_queue
 */
class m170322_152338_add_check_undelivery_queue extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('order', 'call_center_type', $this->string(50)->notNull()->defaultValue('order')->after('call_center_again'));
        $this->addColumn('order', 'call_center_check_type', $this->string(50)->after('check'));
        $this->addColumn('order_check_history', 'type', $this->string(50)->defaultValue('return')->notNull()->after('id'));
        $this->insert('{{%auth_item}}', array(
            'name' => 'order.index.sendcheckundeliveryorder',
            'type' => '2',
            'description' => 'order.index.sendcheckundeliveryorder',
            'created_at' => time(),
            'updated_at' => time()
        ));

        if (AuthItem::find()->where(['name' => 'country.curator'])->exists()) {
            $this->insert($this->authManager->itemChildTable, array(
                'parent' => 'country.curator',
                'child' => 'order.index.sendcheckundeliveryorder'
            ));
        }
        if (AuthItem::find()->where(['name' => 'project.manager'])->exists()) {
            $this->insert($this->authManager->itemChildTable, array(
                'parent' => 'project.manager',
                'child' => 'order.index.sendcheckundeliveryorder'
            ));
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('order', 'call_center_type',
            "ENUM('order', 'new_return', 'return_no_prod', 'jeempo', 'return', 'repeat', 'still_waiting') NOT NULL DEFAULT 'order' AFTER `call_center_again`");
        $this->dropColumn('order', 'call_center_check_type');
        $this->dropColumn('order_check_history', 'type');
        if (AuthItem::find()->where(['name' => 'country.curator'])->exists()) {
            $this->delete($this->authManager->itemChildTable,
                ['child' => 'order.index.sendcheckundeliveryorder', 'parent' => 'country.curator']);
        }
        if (AuthItem::find()->where(['name' => 'project.manager'])->exists()) {
            $this->delete($this->authManager->itemChildTable,
                ['child' => 'order.index.sendcheckundeliveryorder', 'parent' => 'project.manager']);
        }
        $this->delete('{{%auth_item}}', ['name' => 'order.index.sendcheckundeliveryorder']);
    }
}
