<?php
use app\components\CustomMigration as Migration;
use app\modules\callcenter\models\CallCenter;

/**
 * Class m160725_141034_call_center_secret_key
 */
class m160725_141034_call_center_secret_key extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('call_center', 'api_key', $this->string('84')->defaultValue(null)->after('url'));
        $this->addColumn('call_center', 'api_pid', $this->string('12')->defaultValue(null)->after('api_key'));

        $callCenters = CallCenter::find()->all();

        foreach ($callCenters as $callCenter) {
            $callCenter->api_key = 'a77cf421eee4938ef83a89de92f3f65214e99bec';
            $callCenter->api_pid = '052d829b';
            $callCenter->save();
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('call_center', 'api_pid');
        $this->dropColumn('call_center', 'api_key');
    }
}
