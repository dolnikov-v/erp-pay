<?php
use app\components\CustomMigration as Migration;

/**
 * Class m161017_101400_text_utf8_utf16
 */
class m161017_101400_text_utf8_utf16 extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('api_log', 'input_data', "TEXT NULL COLLATE 'utf16_unicode_ci'");

        $this->alterColumn('crontab_task_log_answer', 'data', "TEXT NULL COLLATE 'utf16_unicode_ci'");

        $this->alterColumn('order', 'customer_full_name', "VARCHAR(200) NOT NULL COLLATE 'utf16_unicode_ci'");
        $this->alterColumn('order', 'customer_address', "VARCHAR(500) NULL DEFAULT NULL COLLATE 'utf16_unicode_ci'");
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('order', 'customer_address', "VARCHAR(500) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci'");
        $this->alterColumn('order', 'customer_full_name', "VARCHAR(200) NOT NULL COLLATE 'utf8_unicode_ci'");

        $this->alterColumn('crontab_task_log_answer', 'data', "TEXT NULL COLLATE 'utf8_unicode_ci'");

        $this->alterColumn('api_log', 'input_data', "TEXT NULL COLLATE 'utf8_unicode_ci'");
    }
}
