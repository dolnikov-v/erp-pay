<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryApiClass;
use app\modules\delivery\models\Delivery;
use app\models\Country;

/**
 * Class m170327_093014_add_delivery_ARK_for_Indonesia
 */
class m170327_093014_add_delivery_ARK_for_Indonesia extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        if (DeliveryApiClass::find()->where(
            ['name' => 'ArkApi', 'class_path' => '/ark-api/src/ArkApi.php']
        )->exists()
        ) {
            $apiClass = DeliveryApiClass::findOne(
                ['name' => 'ArkApi', 'class_path' => '/ark-api/src/ArkApi.php']
            );
        } else {
            $apiClass = new DeliveryApiClass();
        }
        $apiClass->name = 'ArkApi';
        $apiClass->class_path = '/ark-api/src/ArkApi.php';
        $apiClass->active = '1';
        $apiClass->created_at = time();
        $apiClass->updated_at = time();
        $apiClass->save();

        if (Country::find()->where(
            [
                'name' => 'Индонезия 1',
                'char_code' => 'ID'
            ]
        )->exists()
        ) {
            $country = Country::find()->where(
                [
                    'name' => 'Индонезия 1',
                    'char_code' => 'ID'
                ]
            )->select('id')->one();
        } else {
            //нет страны
            return false;
        }

        if (Delivery::find()->where(
            [
                'name' => 'Ark',
                'char_code' => 'ARK'
            ]
        )->exists()
        ) {
            $delivery = Delivery::findOne(
                [
                    'name' => 'Ark',
                    'char_code' => 'ARK'
                ]
            );
        } else {
            $delivery = new Delivery();
        }
        $delivery->country_id = $country->id;
        $delivery->name = 'Ark';
        $delivery->char_code = 'ARK';
        $delivery->api_class_id = $apiClass->id;
        $delivery->active = '1';
        $delivery->auto_sending = '0';
        $delivery->save(false);
        return true;
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        if (Country::find()->where(
            [
                'name' => 'Индонезия 1',
                'char_code' => 'ID'
            ]
        )->exists()
        ) {
            $country = Country::find()->where(
                [
                    'name' => 'Индонезия 1',
                    'char_code' => 'ID'
                ]
            )->select('id')->one();
        } else {
            //нет страны
            return false;
        }

        DeliveryApiClass::find()
            ->where([
                'name' => 'ArkApi',
                'class_path' => '/ark-api/src/ArkApi.php'
            ])
            ->one()
            ->delete();

        Delivery::findOne(
            [
                'name' => 'Ark',
                'country_id' => $country->id
            ]
        )->delete();
        return true;
    }
}
