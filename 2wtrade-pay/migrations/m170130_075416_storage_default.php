<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170130_075416_storage_default
 */
class m170130_075416_storage_default extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('storage', 'default', $this->boolean()->defaultValue(0));

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('storage', 'default');

    }
}
