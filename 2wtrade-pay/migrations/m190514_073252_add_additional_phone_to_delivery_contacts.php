<?php
use app\components\CustomMigration as Migration;

/**
 * Class m190514_073252_add_additional_phone_to_delivery_contacts
 */
class m190514_073252_add_additional_phone_to_delivery_contacts extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('delivery_contacts', 'additional_phone', $this->string()->after('mobile_phone'));

        $this->insert('i18n_source_message', ['category' => 'common', 'message' => 'Дополнительный телефон']);
        $id = Yii::$app->db->getLastInsertID();
        $this->insert('i18n_message', ['id' => $id, 'language' => 'en-US', 'translation' => 'Additional phone']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('delivery_contacts', 'additional_phone');

        $this->delete('i18n_source_message', ['category' => 'common', 'message' => 'Дополнительный телефон']);
    }
}
