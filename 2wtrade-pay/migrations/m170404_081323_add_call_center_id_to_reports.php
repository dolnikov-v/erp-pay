<?php

use app\components\CustomMigration as Migration;
use app\modules\deliveryreport\models\DeliveryReportRecord;

/**
 * Handles adding call_center_id to table `reports`.
 */
class m170404_081323_add_call_center_id_to_reports extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(DeliveryReportRecord::tableName(), 'call_center_id', $this->string(50)->after('order_id'));
        $this->createIndex(null, DeliveryReportRecord::tableName(), 'call_center_id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(DeliveryReportRecord::tableName(), 'call_center_id');
    }
}
