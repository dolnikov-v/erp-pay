<?php

use app\components\PermissionMigration as Migration;

/**
 * Class m181206_045150_storage_packing_service
 */
class m181206_045150_storage_packing_service extends Migration
{
    protected $roles = [
        'logist' => [
            'storage.part.delete',
            'storage.part.index',
            'storage.part.edit',
            'storage.part.move',
            'storage.part.remove',
            'packager.request.labelprinting',
            'packager.request.refusalcompleting',
            'packager.request.confirmcompleting',
            'packager.request.cancelcompleting',
            'packager.request.listgeneration',
            'packager.request.cancelshipment',
            'packager.request.setshipped',
        ],
        'admin' => [
            'packager.request.labelprinting',
            'packager.request.refusalcompleting',
            'packager.request.confirmcompleting',
            'packager.request.cancelcompleting',
            'packager.request.listgeneration',
            'packager.request.cancelshipment',
            'packager.request.setshipped',
            'packager.request.setreturn',
        ],
        'controller.analyst' => [
            'packager.request.labelprinting',
            'packager.request.refusalcompleting',
            'packager.request.confirmcompleting',
            'packager.request.cancelcompleting',
            'packager.request.listgeneration',
            'packager.request.cancelshipment',
            'packager.request.setshipped',
            'packager.request.setreturn',
        ],
        'country.curator' => [
            'packager.request.labelprinting',
            'packager.request.refusalcompleting',
            'packager.request.confirmcompleting',
            'packager.request.cancelcompleting',
            'packager.request.listgeneration',
            'packager.request.cancelshipment',
            'packager.request.setshipped',
            'packager.request.setreturn',
        ],
        'implant' => [
            'packager.request.labelprinting',
            'packager.request.refusalcompleting',
            'packager.request.confirmcompleting',
            'packager.request.cancelcompleting',
            'packager.request.listgeneration',
            'packager.request.cancelshipment',
            'packager.request.setshipped',
            'packager.request.setreturn',
        ],
        'junior.logist' => [
            'packager.request.labelprinting',
            'packager.request.refusalcompleting',
            'packager.request.confirmcompleting',
            'packager.request.cancelcompleting',
            'packager.request.listgeneration',
            'packager.request.cancelshipment',
            'packager.request.setshipped',
            'packager.request.setreturn',
        ],
        'operational.director' => [
            'packager.request.labelprinting',
            'packager.request.refusalcompleting',
            'packager.request.confirmcompleting',
            'packager.request.cancelcompleting',
            'packager.request.listgeneration',
            'packager.request.cancelshipment',
            'packager.request.setshipped',
            'packager.request.setreturn',
        ],
        'project.manager' => [
            'packager.request.labelprinting',
            'packager.request.refusalcompleting',
            'packager.request.confirmcompleting',
            'packager.request.cancelcompleting',
            'packager.request.listgeneration',
            'packager.request.cancelshipment',
            'packager.request.setshipped',
            'packager.request.setreturn',
        ],
        'support.manager' => [
            'packager.request.labelprinting',
            'packager.request.refusalcompleting',
            'packager.request.confirmcompleting',
            'packager.request.cancelcompleting',
            'packager.request.listgeneration',
            'packager.request.cancelshipment',
            'packager.request.setshipped',
            'packager.request.setreturn',
        ],
        'technical.director' => [
            'packager.request.labelprinting',
            'packager.request.refusalcompleting',
            'packager.request.confirmcompleting',
            'packager.request.cancelcompleting',
            'packager.request.listgeneration',
            'packager.request.cancelshipment',
            'packager.request.setshipped',
            'packager.request.setreturn',
        ],
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('storage', 'type', $this->smallInteger());
        $this->addColumn('storage_product', 'reserve', $this->integer());

        $this->alterColumn('delivery_request', 'status', $this->string(20)->defaultValue('pending'));
        $this->alterColumn('delivery_request_deleted', 'status', $this->string(20)->defaultValue('pending'));

        $this->addColumn('order_packaging', 'reason', $this->string());

        $this->addColumn('order_packaging', 'list_id', $this->integer());

        $this->addForeignKey(
            $this->getFkName('order_packaging', 'list_id'), 'order_packaging', 'list_id', 'order_logistic_list', 'id',
            self::SET_NULL,
            self::RESTRICT
        );

        $this->createIndex($this->getIdxName('order_packaging', 'status'), 'order_packaging', 'status');

        parent::safeUp();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('storage', 'type');
        $this->dropColumn('storage_product', 'reserve');

        $this->alterColumn('delivery_request', 'status', $this->enum([
            'pending',
            'in_progress',
            'done',
            'error',
            'done_error',
            'in_list'
        ])->defaultValue('pending'));
        $this->alterColumn('delivery_request_deleted', 'status', $this->enum([
            'pending',
            'in_progress',
            'done',
            'error',
            'done_error',
            'in_list'
        ])->defaultValue('pending'));

        $this->dropColumn('order_packaging', 'reason');

        $this->dropForeignKey($this->getFkName('order_packaging', 'list_id'), 'order_packaging');

        $this->dropColumn('order_packaging', 'list_id');

        $this->dropIndex($this->getIdxName('order_packaging', 'status'), 'order_packaging');

        parent::safeDown();
    }
}
