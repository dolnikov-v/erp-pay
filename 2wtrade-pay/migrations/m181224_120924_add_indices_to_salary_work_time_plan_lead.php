<?php
use app\components\CustomMigration as Migration;

/**
 * Class m181224_120924_add_indices_to_salary_work_time_plan_lead
 */
class m181224_120924_add_indices_to_salary_work_time_plan_lead extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createIndex($this->getIdxName('salary_work_time_plan', ['person_id', 'date']), 'salary_work_time_plan', ['person_id', 'date']);
        $this->createIndex($this->getIdxName('lead', ['source', 'country', 'source_status', 'ts_spawn']), 'lead', ['source', 'country', 'source_status', 'ts_spawn']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropIndex($this->getIdxName('salary_work_time_plan', ['person_id', 'date']), 'salary_work_time_plan');
        $this->dropIndex($this->getIdxName('lead', ['source', 'country', 'source_status', 'ts_spawn']), 'lead');
    }
}
