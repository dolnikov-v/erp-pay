<?php

use app\components\CustomMigration as Migration;

/**
 * Class m180827_104502_alter_function_price_currency
 */
class m180827_104502_alter_function_price_currency extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $sql = 'DROP FUNCTION IF EXISTS convertToCurrencyByCountry;';
        yii::$app->db->createCommand($sql)->execute();


        $sql = <<<SQL
        CREATE FUNCTION convertToCurrencyByCountry(sum FLOAT, currency_from INTEGER,  currency_to INTEGER) RETURNS FLOAT NOT DETERMINISTIC
        BEGIN 
		  DECLARE converted_sum FLOAT DEFAULT 0;  		
	  
		  	 		 	
          SET converted_sum = CASE WHEN  currency_from = currency_to THEN sum ELSE
			 CASE WHEN currency_to IS NOT NULL
			      THEN sum / COALESCE ((SELECT rate FROM currency_rate WHERE currency_id = currency_from),0) * COALESCE ((SELECT rate FROM currency_rate WHERE currency_id = currency_to), 0)
			      ELSE sum
			      END
			 END;
		  
        RETURN converted_sum;
        END; 
SQL;

        yii::$app->db->createCommand($sql)->execute();

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $sql = 'DROP FUNCTION IF EXISTS convertToCurrencyByCountry;';
        yii::$app->db->createCommand($sql)->execute();

        $sql = <<<SQL
        CREATE FUNCTION convertToCurrencyByCountry(sum FLOAT, currency_from INTEGER,  currency_to INTEGER) RETURNS FLOAT NOT DETERMINISTIC
        BEGIN 
		  DECLARE converted_sum FLOAT DEFAULT 0;  		
		  	 	
          SET converted_sum = 
			 CASE WHEN currency_to IS NOT NULL
			      THEN sum / COALESCE ((SELECT rate FROM currency_rate WHERE currency_id = currency_from),0) * COALESCE ((SELECT rate FROM currency_rate WHERE currency_id = currency_to), 0)
			      ELSE sum
			      END;
		  
        RETURN converted_sum;
        END; 
SQL;

        yii::$app->db->createCommand($sql)->execute();

    }
}
