<?php
use app\components\CustomMigration as Migration;
use app\modules\widget\models\WidgetType;
use app\modules\widget\models\WidgetRole;

/**
 * Class m170214_092639_add_widget_top_manager
 */
class m170214_092639_add_widget_top_manager extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert(WidgetType::tableName(), [
            'code' => 'top_сс_managers',
            'name' => 'Топ по менеджерам колл центров',
            'status' => WidgetType::STATUS_ACTIVE
        ]);

        $type = WidgetType::findOne(['code' => 'top_сс_managers']);

        $this->insert(WidgetRole::tableName(), [
            'role' => 'business_analyst',
            'type_id' => $type->id
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $type = WidgetType::findOne(['code' => 'top_сс_managers']);
        $this->delete(WidgetRole::tableName(), ['type_id' => $type->id]);
        $this->delete(WidgetType::tableName(), ['code' => 'top_сс_managers']);
    }
}
