<?php
use app\components\CustomMigration as Migration;
use app\modules\order\models\Order;

/**
 * Class m170120_112005_edit_customer_province_length
 */
class m170120_112005_edit_customer_province_length extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn(Order::tableName(), 'customer_province', $this->string(150));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn(Order::tableName(), 'customer_province', $this->string(100));
    }
}
