<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m171120_044355_task_calculated_param
 */
class m171120_044355_task_calculated_param extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = CrontabTask::findOne(['name' => 'calculated_param']);
        if (!$crontabTask) {
            $crontabTask = new CrontabTask();
            $crontabTask->name = 'calculated_param';
            $crontabTask->description = "Вычисление параметров для отчетов";
            $crontabTask->save();
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = CrontabTask::findOne(['name' => 'calculated_param']);
        if ($crontabTask instanceof CrontabTask) {
            $crontabTask->delete();
        }
    }
}
