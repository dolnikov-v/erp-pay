<?php

use app\components\CustomMigration as Migration;
use app\modules\order\models\IssueCollector;
use app\modules\order\models\OrderNotification;

/**
 * Handles create issue_collector table.
 */
class m170726_092153_create_issue_collector extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropIndex('trigger', OrderNotification::tableName());
        $this->renameColumn(OrderNotification::tableName(), 'trigger', 'name');
        $this->addColumn(OrderNotification::tableName(), 'issue_collector_id', $this->integer(3));
        $this->createTable('issue_collector', [
            'id' => $this->primaryKey(),
            'name' => $this->string(256),
            'jira_link' => $this->string(512),
            'form_link' => $this->string(32),
            'country_id' => $this->integer()->notNull(),
        ], $this->tableOptions);
        $this->createIndex(null, OrderNotification::tableName(),['name', 'country_id'], true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable(IssueCollector::tableName());
        $this->dropColumn(OrderNotification::tableName(), 'issue_collector_id');
        $this->renameColumn(OrderNotification::tableName(), 'name', 'trigger');
        $this->createIndex('trigger', OrderNotification::tableName(), 'trigger', true);
    }
}
