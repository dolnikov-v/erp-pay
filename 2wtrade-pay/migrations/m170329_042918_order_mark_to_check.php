<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\models\OrderExport;

/**
 * Class m170329_042918_order_mark_to_check
 */
class m170329_042918_order_mark_to_check extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(DeliveryRequest::tableName(), 'sent_clarification_at', $this->integer()->after('sent_at'));
        $this->addColumn(OrderExport::tableName(), 'after_action', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(DeliveryRequest::tableName(), 'sent_clarification_at');
        $this->dropColumn(OrderExport::tableName(), 'after_action');
    }
}
