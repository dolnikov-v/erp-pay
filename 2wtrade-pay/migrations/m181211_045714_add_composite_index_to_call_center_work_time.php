<?php
use app\components\CustomMigration as Migration;

/**
 * Class m181211_045714_add_composite_index_to_call_center_work_time
 */
class m181211_045714_add_composite_index_to_call_center_work_time extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createIndex('idx_call_center_work_time_call_center_user_id_date', 'call_center_work_time', ['call_center_user_id', 'date']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropIndex('idx_call_center_work_time_call_center_user_id_date', 'call_center_work_time');
    }
}
