<?php
use app\components\CustomMigration as Migration;
use app\modules\salary\models\Person;
use app\modules\salary\models\PersonImport;
use app\modules\salary\models\PersonImportData;

/**
 * Class m170915_043138_salary_person_phone
 */
class m170915_043138_salary_person_phone extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->renameColumn(Person::tableName(), 'skype', 'phone');
        $this->addColumn(Person::tableName(), 'skype', $this->string(255)->after('phone'));

        $this->renameColumn(PersonImport::tableName(), 'cell_skype', 'cell_phone');
        $this->addColumn(PersonImport::tableName(), 'cell_skype', $this->string(255)->after('cell_phone'));

        $this->renameColumn(PersonImportData::tableName(), 'person_skype', 'person_phone');
        $this->addColumn(PersonImportData::tableName(), 'person_skype', $this->string(255)->after('person_phone'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Person::tableName(), 'skype');
        $this->renameColumn(Person::tableName(), 'phone', 'skype');

        $this->dropColumn(PersonImport::tableName(), 'cell_skype');
        $this->renameColumn(PersonImport::tableName(), 'cell_phone', 'cell_skype');

        $this->dropColumn(PersonImportData::tableName(), 'person_skype');
        $this->renameColumn(PersonImportData::tableName(), 'person_phone', 'person_skype');
    }
}
