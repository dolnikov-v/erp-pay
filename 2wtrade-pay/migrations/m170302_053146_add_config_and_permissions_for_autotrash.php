<?php
use app\components\CustomMigration as Migration;
use app\models\AuthItem;
use app\models\Config;

/**
 * Class m170302_053146_add_config_and_permissions_for_autotrash
 */
class m170302_053146_add_config_and_permissions_for_autotrash extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert(Config::tableName(), ['name' => 'STATE_AUTOTRASH_ORDERS', 'description' => 'Вкл/откл автотреша заказов', 'value' => 0]);
        $this->insert(AuthItem::tableName(), ['name' => 'catalog.autotrash.index', 'type' => 2, 'description' => 'catalog.autotrash.index']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete(Config::tableName(), ['name' => 'STATE_AUTOTRASH_ORDERS']);
        $this->delete(AuthItem::tableName(), ['name' => 'catalog.autotrash.index']);
    }
}
