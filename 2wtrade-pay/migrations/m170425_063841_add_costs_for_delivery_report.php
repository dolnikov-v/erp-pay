<?php
use app\components\CustomMigration as Migration;
use app\modules\deliveryreport\models\DeliveryReport;

/**
 * Class m170425_063841_add_costs_for_delivery_report
 */
class m170425_063841_add_costs_for_delivery_report extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(DeliveryReport::tableName(), 'total_costs',
            $this->double()->after('sum_total')->defaultValue(0));
        $this->addColumn(DeliveryReport::tableName(), 'additional_costs',
            $this->double()->after('total_costs')->defaultValue(0));

        $this->insert('{{%auth_item}}', array(
            'name' => 'deliveryreport.report.changecosts',
            'type' => '2',
            'description' => 'deliveryreport.report.changecosts',
            'created_at' => time(),
            'updated_at' => time()
        ));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(DeliveryReport::tableName(), 'total_costs');
        $this->dropColumn(DeliveryReport::tableName(), 'additional_costs');

        $this->delete('{{%auth_item}}', ['name' => 'deliveryreport.report.changecosts']);
    }
}
