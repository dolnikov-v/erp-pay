<?php
use app\components\CustomMigration as Migration;
use app\models\Notification;
use app\modules\administration\models\CrontabTask;

/**
 * Class m171109_063817_fix_orders_in_lead_status_notification
 */
class m171109_063817_fix_orders_in_lead_status_notification extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $notification = Notification::findOne(['trigger' => 'report.orders.in.lead.status']);

        if ($notification instanceof Notification) {
            $notification->delete();
        }

        $crontabTask = CrontabTask::findOne(['name' => 'orders_in_lead_status_new']);
        $crontabTask->name = 'orders_in_hold_status';
        $crontabTask->description = 'Уведомление о холдах по командам и странам';
        $crontabTask->save();
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $notification = new Notification([
            'description' => 'Лидов по странам: {text}',
            'trigger' => 'report.orders.in.lead.status',
            'type' => Notification::TYPE_INFO,
            'group' => Notification::GROUP_REPORT,
            'active' => Notification::ACTIVE,
        ]);
        $notification->save();

        $crontabTask = CrontabTask::findOne(['name' => 'orders_in_hold_status']);
        $crontabTask->name = 'orders_in_lead_status_new';
        $crontabTask->description = 'Уведомление о лидах с тенденциями по странам';
        $crontabTask->save();
    }
}
