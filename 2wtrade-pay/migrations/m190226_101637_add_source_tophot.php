<?php
use app\components\CustomMigration as Migration;

/**
 * Class m190226_101637_add_source_tophot
 */
class m190226_101637_add_source_tophot extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('source', ['name' => 'tophot', 'active' => 1]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete('source', ['name' => 'tophot']);
    }
}
