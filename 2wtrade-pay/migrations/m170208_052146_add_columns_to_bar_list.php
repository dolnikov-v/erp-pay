<?php

use app\components\CustomMigration as Migration;
use app\modules\bar\models\BarList;

/**
 * Handles adding columns to table `bar_list`.
 */
class m170208_052146_add_columns_to_bar_list extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(BarList::tableName(), 'quantity', $this->integer(4)->after('user_id'));
        $this->addColumn(BarList::tableName(), 'product_id', $this->integer(3)->after('quantity'));
        $this->addForeignKey(null, BarList::tableName(), 'product_id', 'product', 'id',  self::NO_ACTION, self::NO_ACTION);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_bar_list_product_id', BarList::tableName());
        $this->dropColumn(BarList::tableName(), 'quantity');
        $this->dropColumn(BarList::tableName(), 'product_id');
    }
}
