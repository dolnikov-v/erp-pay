<?php
use app\components\CustomMigration as Migration;
use app\modules\order\models\OrderStatus;

/**
 * Class m161107_131002_change_order_statuses
 */
class m161107_131002_change_order_statuses extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('order_status', 'group', $this->enum(['source', 'call_center', 'logistic', 'delivery', 'finance'])->defaultValue(null));

        /** @var OrderStatus $status */
        $status = OrderStatus::findOne(1);

        if ($status) {
            $status->name = 'Источник (заказ создан с длинной формы)';
            $status->group = 'source';
            $status->save(false);
        }

        $status = OrderStatus::findOne(2);

        if ($status) {
            $status->name = 'Источник (заказ создан с короткой формы)';
            $status->group = 'source';
            $status->save(false);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('order_status', 'group', $this->enum(['adcombo', 'call_center', 'logistic', 'delivery', 'finance'])->defaultValue(null));

        /** @var OrderStatus $status */
        $status = OrderStatus::findOne(1);

        if ($status) {
            $status->name = 'Adcombo (заказ создан с длинной формы)';
            $status->group = 'adcombo';
            $status->save(false);
        }

        $status = OrderStatus::findOne(2);

        if ($status) {
            $status->name = 'Adcombo (заказ создан с короткой формы)';
            $status->group = 'adcombo';
            $status->save(false);
        }
    }
}
