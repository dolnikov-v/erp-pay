<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170925_075612_add_running_line_active_to_table
 */
class m170925_075612_add_running_line_active_to_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('notification_user', 'running_line_active', $this->integer(1)->defaultValue(0) . ' AFTER `active`');
        $this->addColumn('notification_role', 'running_line_active', $this->integer(1)->defaultValue(0) . ' AFTER `trigger`');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('notification_user', 'running_line_active');
        $this->dropColumn('notification_role', 'running_line_active');
    }
}
