<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m160928_093733_new_crontab_task
 */
class m161025_181802_new_crontab_task_monitor_delivered_anomalies extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = new CrontabTask();
        $crontabTask->name = 'monitor_delivered_anomalies';
        $crontabTask->description = "Мониторинг доставленных DeliveryRequesto'в на предмет смены статуса";
        $crontabTask->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = CrontabTask::find()
            ->byName('monitor_delivered_anomalies')
            ->one();

        if ($crontabTask) {
            $crontabTask->delete();
        }
    }
}
