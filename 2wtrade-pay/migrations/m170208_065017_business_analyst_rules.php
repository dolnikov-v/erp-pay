<?php
use app\components\CustomMigration as Migration;
use \yii\db\Query;

/**
 * Class m170208_065017_business_analyst_rules
 */
class m170208_065017_business_analyst_rules extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        $query = new Query();

        $is_businessanalyst = $query->select('*')->from($this->authManager->itemTable)
            ->where(['name' => 'business_analyst', 'type' => 1])
            ->one();

        if (!$is_businessanalyst) {
            echo "Error: No business_analyst exists";
            die();
        }


        $reports = [
            'report.approvebycountry.getcountry',
            'report.averagecheckbycountry.getcountry'
        ];

        foreach ($reports as $report) {
            $is_report = $query->select('*')->from($this->authManager->itemTable)
                ->where([
                    'name' => $report,
                    'type' => 2])
                ->one();

            if (!$is_report) {
                $this->insert($this->authManager->itemTable, [
                    'name' => $report,
                    'type' => 2,
                    'description' => $report,
                    'created_at' => time(),
                    'updated_at' => time()]);
            }

            $is_br = $query->select('*')->from($this->authManager->itemChildTable)->where([
                'parent' => 'business_analyst',
                'child' => $report
            ])->one();

            if (!$is_br) {
                $this->insert($this->authManager->itemChildTable, [
                    'parent' => 'business_analyst',
                    'child' => $report
                ]);
            }
        }

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $reports = [
            'report.approvebycountry.getcountry',
            'report.averagecheckbycountry.getcountry'
        ];

        foreach ($reports as $child) {
            $this->delete($this->authManager->itemChildTable, [
                'parent' => 'business_analyst',
                'child' => $child
            ]);
        }
    }
}
