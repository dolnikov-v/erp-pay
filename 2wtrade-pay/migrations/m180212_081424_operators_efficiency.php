<?php
use app\components\CustomMigration as Migration;

use app\modules\salary\models\Office;
use app\models\Notification;
use app\modules\administration\models\CrontabTask;

/**
 * Class m180212_081424_operators_efficiency
 */
class m180212_081424_operators_efficiency extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Office::tableName(), 'norm_hours_per_workplace', $this->integer()->defaultValue(8));

        $notification = Notification::findOne(['trigger' => Notification::TRIGGER_CALL_CENTER_EFFICIENCY_OPERATORS]);
        if (!$notification instanceof Notification) {
            $notification = new Notification();
        }

        $notification->description = 'Отчет работы {officeName}: {text}';
        $notification->trigger = Notification::TRIGGER_CALL_CENTER_EFFICIENCY_OPERATORS;
        $notification->type = Notification::TYPE_INFO;
        $notification->group = Notification::GROUP_SYSTEM;
        $notification->active = Notification::ACTIVE;
        $notification->save(false);

        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_CALL_CENTER_EFFICIENCY_OPERATORS]);
        if (!$crontabTask) {
            $crontabTask = new CrontabTask();
            $crontabTask->name = CrontabTask::TASK_CALL_CENTER_EFFICIENCY_OPERATORS;
            $crontabTask->description = "Уведомление эффективности операторов";
            $crontabTask->save();
        }


    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Office::tableName(), 'norm_hours_per_workplace');

        $notification = Notification::findOne(['trigger' => Notification::TRIGGER_CALL_CENTER_EFFICIENCY_OPERATORS]);
        $notification->delete();
        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_CALL_CENTER_EFFICIENCY_OPERATORS]);
        $crontabTask->delete();
    }
}
