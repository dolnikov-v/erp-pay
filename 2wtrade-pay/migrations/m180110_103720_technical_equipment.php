<?php
use app\components\CustomMigration as Migration;
use app\modules\salary\models\Office;

/**
 * Class m180110_103720_technical_equipment
 */
class m180110_103720_technical_equipment extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->renameColumn(Office::tableName(), 'technical_equipment', 'technical_equipment_mikrotik');
        $this->addColumn(Office::tableName(), 'technical_equipment_video', $this->integer(1)->defaultValue(0));
        $this->addColumn(Office::tableName(), 'technical_equipment_internet', $this->integer(1)->defaultValue(0));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->renameColumn(Office::tableName(), 'technical_equipment_mikrotik', 'technical_equipment');
        $this->dropColumn(Office::tableName(), 'technical_equipment_video');
        $this->dropColumn(Office::tableName(), 'technical_equipment_internet');
    }
}
