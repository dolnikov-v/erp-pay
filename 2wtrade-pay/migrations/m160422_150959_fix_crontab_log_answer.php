<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160422_150959_fix_crontab_log_answer
 */
class m160422_150959_fix_crontab_log_answer extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('crontab_task_log_answer', 'url', $this->string(1000) . ' AFTER `log_id`');
        $this->addColumn('crontab_task_log_answer', 'input_data', $this->text() . ' AFTER `url`');
        $this->renameColumn('crontab_task_log_answer', 'answer', 'output_data');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->renameColumn('crontab_task_log_answer', 'output_data', 'answer');
        $this->dropColumn('crontab_task_log_answer', 'input_data');
        $this->dropColumn('crontab_task_log_answer', 'url');
    }
}
