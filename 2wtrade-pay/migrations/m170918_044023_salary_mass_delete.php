<?php
use app\components\CustomMigration as Migration;
use yii\db\Query;

/**
 * Class m170918_044023_salary_mass_delete
 */
class m170918_044023_salary_mass_delete extends Migration
{

    const RULES = [
        'salary.penalty.deletepenalties',
        'salary.bonus.deletebonuses',
    ];

    const ROLES = [
        'salaryproject.clerk',
        'callcenter.hr',
        'callcenter.manager',
        'finance.director',
        'project.manager'
    ];

    const TEXT = [
        'Необходимо выбрать штрафы' => 'You must select penalties',
        'Не удалось удалить штрафы' => 'Could not delete penalties',
        'Необходимо выбрать бонусы' => 'You must select bonuses',
        'Не удалось удалить бонусы' => 'Could not delete bonuses',
        'Выбранные бонусы будут удалены' => 'Selected bonuses will be deleted',
        'Выбранные штрафы будут удалены' => 'Selected penalties will be deleted',
        'Удаление бонусов' => 'Delete bonuses',
        'Удаление штрафов' => 'Delete penalties',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $query = new Query();

        foreach (self::RULES as $rule) {

            $is_rule = $query->select('*')->from($this->authManager->itemTable)
                ->where(['name' => $rule, 'type' => 2])
                ->one();

            if (!$is_rule) {
                $this->insert($this->authManager->itemTable, [
                    'name' => $rule,
                    'description' => $rule,
                    'type' => 2,
                    'created_at' => time(),
                    'updated_at' => time()]);
            }

            foreach (self::ROLES as $role) {
                $is_can = $query->select('*')->from($this->authManager->itemChildTable)->where([
                    'parent' => $role,
                    'child' => $rule
                ])->one();

                if (!$is_can) {
                    $this->insert($this->authManager->itemChildTable, [
                        'parent' => $role,
                        'child' => $rule
                    ]);
                }
            }
        }

        foreach ($this::TEXT as $ruText => $enText) {

            $is = $query->select('id')
                ->from('i18n_source_message')
                ->where(['category' => 'common', 'message' => $ruText])->one();

            if (!$is) {
                $this->insert('i18n_source_message', ['category' => 'common', 'message' => $ruText]);
                $id = Yii::$app->db->getLastInsertID();
                $this->insert('i18n_message', ['id' => $id, 'language' => 'en-US', 'translation' => $enText]);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach (self::RULES as $rule) {
            foreach (self::ROLES as $role) {
                $this->delete($this->authManager->itemChildTable, [
                    'parent' => $role,
                    'child' => $rule
                ]);
            }
        }
    }
}
