<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;


/**
 * Class m170530_054203_add_task_for_notification_get_answers
 */
class m170530_054203_add_task_for_notification_get_answers extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = new CrontabTask();
        $crontabTask->name = 'get_answers_for_notification';
        $crontabTask->description = 'Получение ответов смс,email ответов на уведомления с 2wstore.com';
        $crontabTask->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = CrontabTask::find()
            ->byName('get_answers_for_notification')
            ->one();

        if ($crontabTask) {
            $crontabTask->delete();
        }

    }

}
