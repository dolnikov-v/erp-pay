<?php
use app\components\CustomMigration as Migration;
use \yii\db\Query;

/**
 * Class m170131_031929_reports_access
 */
class m170131_031929_reports_access extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        $this->delete($this->authManager->itemChildTable, [
            'child' => 'report.approvebycity.index'
        ]);

        $this->delete($this->authManager->itemTable, [
            'name' => 'report.approvebycity.index',
        ]);


        $query = new Query();

        $is_businessanalyst = $query->select('*')->from($this->authManager->itemTable)
            ->where(['name' => 'business_analyst', 'type' => 1])
            ->one();

        if (!$is_businessanalyst) {
            echo "Error: No business_analyst exists";
            die();
        }

        $is_callcentermanager = $query->select('*')->from($this->authManager->itemTable)
            ->where(['name' => 'callcenter.manager', 'type' => 1])
            ->one();

        if (!$is_callcentermanager) {
            echo "Error: No callcenter.manager exists";
            die();
        }

        $is_countrycurator = $query->select('*')->from($this->authManager->itemTable)
            ->where(['name' => 'country.curator', 'type' => 1])
            ->one();

        if (!$is_countrycurator) {
            echo "Error: No country.curator exists";
            die();
        }


        $reports = [
            'report.approvebycountry.index',
            'report.averagecheckbycountry.index'
        ];

        foreach ($reports as $report) {
            $is_report = $query->select('*')->from($this->authManager->itemTable)
                ->where([
                    'name' => $report,
                    'type' => 2])
                ->one();

            if (!$is_report) {
                $this->insert($this->authManager->itemTable, [
                    'name' => $report,
                    'type' => 2,
                    'description' => $report,
                    'created_at' => time(),
                    'updated_at' => time()]);
            }
        }


        $is_cm = $query->select('*')->from($this->authManager->itemChildTable)->where([
            'parent' => 'callcenter.manager',
            'child' => 'report.averagecheckbycountry.index'
        ])->one();

        if (!$is_cm) {
            $this->insert($this->authManager->itemChildTable, [
                'parent' => 'callcenter.manager',
                'child' => 'report.averagecheckbycountry.index'
            ]);
        }


        $country_curator_rules = [
            "report.approvebycountry.index",
            "report.averagecheckbycountry.index"
        ];

        foreach ($country_curator_rules as $child) {
            $is_cc = $query->select('*')->from($this->authManager->itemChildTable)->where([
                'parent' => 'country.curator',
                'child' => $child
            ])->one();

            if (!$is_cc) {
                $this->insert($this->authManager->itemChildTable, [
                    'parent' => 'country.curator',
                    'child' => $child
                ]);
            }
        }


        $business_analyst_rules = [
            'order.index.index',
            'order.index.view',
            'old_orders_access',
            'report.averagecheckbycountry.index',
            'report.approvebycountry.index',
        ];

        foreach ($business_analyst_rules as $child) {
            $is_br = $query->select('*')->from($this->authManager->itemChildTable)->where([
                'parent' => 'business_analyst',
                'child' => $child
            ])->one();

            if (!$is_br) {
                $this->insert($this->authManager->itemChildTable, [
                    'parent' => 'business_analyst',
                    'child' => $child
                ]);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

        $this->delete($this->authManager->itemChildTable, [
            'parent' => 'callcenter.manager',
            'child' => 'report.averagecheckbycountry.index'
        ]);


        $country_curator_rules = [
            "report.approvebycountry.index",
            "report.averagecheckbycountry.index"
        ];

        foreach ($country_curator_rules as $child) {
            $this->delete($this->authManager->itemChildTable, [
                'parent' => 'country.curator',
                'child' => $child
            ]);
        }

        $business_analyst_rules = [
            'order.index.index',
            'order.index.view',
            'old_orders_access',
            'report.averagecheckbycountry.index',
            'report.approvebycountry.index',
        ];

        foreach ($business_analyst_rules as $child) {
            $this->delete($this->authManager->itemChildTable, [
                'parent' => 'business_analyst',
                'child' => $child
            ]);
        }
    }

}
