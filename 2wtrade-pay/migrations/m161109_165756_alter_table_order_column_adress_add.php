<?php
use app\components\CustomMigration as Migration;

/**
 * Class m161109_165756_alter_table_order_column_adress_add
 */
class m161109_165756_alter_table_order_column_adress_add extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('order', 'customer_address_add', $this->string(800)->defaultValue(null));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('order', 'customer_address_add', $this->string(500)->defaultValue(null));
    }
}
