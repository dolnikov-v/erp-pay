<?php
use app\components\CustomMigration as Migration;

/**
 * Class m190326_061018_order_foreign_id_format
 */
class m190326_061018_order_foreign_id_format extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('order', 'foreign_id', $this->string(32));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('order', 'foreign_id', $this->integer(11));
    }
}
