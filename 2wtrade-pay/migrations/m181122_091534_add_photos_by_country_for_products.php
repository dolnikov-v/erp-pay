<?php
use app\components\PermissionMigration as Migration;

/**
 * Class m181122_091534_add_photos_by_country_for_products
 */
class m181122_091534_add_photos_by_country_for_products extends Migration
{
    protected $permissions = [
        'catalog.product.prices',
        'catalog.product.translate',
        'catalog.product.photo',
        'catalog.productphoto.edit',
        'catalog.productphoto.delete',
    ];

    protected $roles = [];

    private function loadRoles()
    {
        $query = new \yii\db\Query();
        $data = $query->from($this->authManager->itemChildTable)->where([
            'child' => 'catalog.product.edit'
        ])->all();
        foreach ($data as $permission) {
            $this->roles[$permission['parent']] = $this->permissions;
        }
    }

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('product_photo', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer()->notNull(),
            'country_id' => $this->integer()->notNull(),
            'image' => $this->string(255)->notNull(),
            'updated_at' => $this->integer(),
            'created_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(null, 'product_photo', 'product_id', 'product', 'id', self::CASCADE, self::CASCADE);
        $this->addForeignKey(null, 'product_photo', 'country_id', 'country', 'id', self::CASCADE, self::CASCADE);
        $this->createIndex(null, 'product_photo', ['product_id', 'country_id'], true);

        $this->addColumn('product_price', 'country_id', $this->integer());
        $this->getDb()->createCommand('UPDATE `product_price` pp INNER JOIN `country` c on c.currency_id = pp.currency_id SET pp.country_id = c.id')->execute();
        $this->addForeignKey(null, 'product_price', 'country_id', 'country', 'id', self::CASCADE, self::CASCADE);
        $this->createIndex('uni_product_price_product_idcountry_idcurrency_id', 'product_price', ['product_id', 'country_id', 'currency_id'], true);

        $this->addColumn('product_translation', 'country_id', $this->integer());
        $this->addForeignKey(null, 'product_translation', 'country_id', 'country', 'id', self::CASCADE, self::CASCADE);
        $this->getDb()->createCommand('UPDATE `product_translation` pt INNER JOIN `country` c on c.language_id = pt.language_id SET pt.country_id = c.id')->execute();
        $this->createIndex('uni_product_translation_product_idcountry_idcurrency_id', 'product_translation', ['product_id', 'country_id', 'language_id'], true);

        $this->loadRoles();

        parent::safeUp();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropIndex('uni_product_translation_product_idcountry_idcurrency_id', 'product_translation');
        $this->dropColumn('product_translation', 'country_id');

        $this->dropIndex('uni_product_price_product_idcountry_idcurrency_id', 'product_price');
        $this->dropColumn('product_price', 'country_id');

        $this->dropTable('product_photo');

        $this->loadRoles();

        parent::safeDown();
    }
}
