<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160811_072447_fix_enum_api_log
 */
class m160811_072447_fix_enum_api_log extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('api_log', 'type', 'ENUM ("lead", "callcenter", "delivery") DEFAULT "lead"');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('api_log', 'type', 'ENUM ("lead", "callcenter") DEFAULT "lead"');
    }
}
