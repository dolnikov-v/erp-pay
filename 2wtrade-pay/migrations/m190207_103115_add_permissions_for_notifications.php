<?php

use app\components\CustomMigration as Migration;

/**
 * Class m190207_103115_add_permissions_for_notifications
 */
class m190207_103115_add_permissions_for_notifications extends Migration
{
    private $roles = ['finance.director', 'finance.manager', 'technical.director', 'sales.director',
        'operational.director', 'general.director', 'leader'];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        foreach ($this->roles as $role) {
            $this->insert('notification_role', [
                'role' => $role,
                'trigger' => 'debts.notification.three.month'
            ]);
        }
        $command = $this->getDb()->createCommand("INSERT INTO `user_notification_setting` (`user_id`, `trigger`, `active`, `skype_active`) SELECT id, 'debts.notification.three.month', 1, 1 FROM `user` WHERE username in ('glushko.v', 'kravchenko.i')");
        $command->setRawSql($command->getSql() . ' on duplicate key update `skype_active` = 1');
        $command->execute();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach ($this->roles as $role) {
            $this->delete('notification_role', ['role' => $role, 'trigger' => 'debts.notification.three.month']);
        }
    }
}
