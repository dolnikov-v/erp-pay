<?php
use app\components\CustomMigration as Migration;
use app\models\Notification;
use app\modules\administration\models\CrontabTask;
use app\models\NotificationRole;
use app\models\UserNotificationSetting;
use app\models\User;

/**
 * Class m170127_064716_add_notification_unsent_orders
 */
class m170127_064716_add_notification_unsent_orders extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $notification = Notification::findOne(['trigger' => Notification::TRIGGER_REPORT_ORDERS_UNSENT]);

        if (!($notification instanceof Notification)) {
            $notification = new Notification([
                'description' => 'Неотправленных заказов: {count}',
                'trigger' => Notification::TRIGGER_REPORT_ORDERS_UNSENT,
                'type' => Notification::TYPE_WARNING,
                'group' => Notification::GROUP_REPORT,
                'active' => Notification::ACTIVE,
            ]);
            $notification->save(false);
        }

        $notificationRole = new NotificationRole([
            'role' => 'country.curator',
            'trigger' => Notification::TRIGGER_REPORT_ORDERS_UNSENT,
        ]);

        $notificationRole->save(false);


        $notificationRole = new NotificationRole([
            'role' => 'business_analyst',
            'trigger' => Notification::TRIGGER_REPORT_ORDERS_UNSENT,
        ]);

        $notificationRole->save(false);


        $users = User::find()
            ->join('JOIN', $this->authManager->assignmentTable, $this->authManager->assignmentTable . '.user_id = user.id')
            ->where([$this->authManager->assignmentTable . '.item_name' => [
                'country.curator',
                'business_analyst']])
            ->all();

        foreach ($users as $user) {

            $userNotificationSetting = UserNotificationSetting::find()
                ->where(['user_id' => $user->id, 'trigger' => Notification::TRIGGER_REPORT_ORDERS_UNSENT])
                ->one();

            if ($userNotificationSetting) {
                $userNotificationSetting->active = 0;
                $userNotificationSetting->email_interval = 'daily';
                $userNotificationSetting->save();

            } else {
                $userNotificationSetting = new UserNotificationSetting();
                $userNotificationSetting->user_id = $user->id;
                $userNotificationSetting->trigger = Notification::TRIGGER_REPORT_ORDERS_UNSENT;
                $userNotificationSetting->email_interval = 'daily';
                $userNotificationSetting->active = 0;
                $userNotificationSetting->save();
            }
        }

        $crontabTask = CrontabTask::findOne(['name' => 'report_orders_unsent']);

        if (!($crontabTask instanceof CrontabTask)) {
            $crontabTask = new CrontabTask();
            $crontabTask->name = 'report_orders_unsent';
            $crontabTask->description = 'Ежедневные нотификации о неотправленных заказах (1, 2, 6, 19, 31 колонка)';
            $crontabTask->save();
        }

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $notification = Notification::findOne(['trigger' => 'report.orders.unsent']);

        if ($notification instanceof Notification) {
            $notification->delete();
        }

        $crontabTask = CrontabTask::findOne(['name' => 'report_orders_unsent']);

        if ($crontabTask instanceof CrontabTask) {
            $crontabTask->delete();
        }
    }
}
