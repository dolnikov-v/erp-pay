<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryApiClass;

/**
 * Class m171026_100952_gt_express_api
 */
class m171026_100952_gt_express_api extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $class = new DeliveryApiClass();
        $class->name = 'gtExpress';
        $class->class_path = '/gtexpress-api/src/gtExpressApi.php';
        $class->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        DeliveryApiClass::find()
            ->where([
                'name' => 'gtExpress',
            ])
            ->one()
            ->delete();
    }
}
