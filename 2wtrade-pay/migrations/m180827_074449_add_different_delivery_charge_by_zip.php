<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryContract;
use app\modules\delivery\models\DeliveryChargesCalculator;

/**
 * Class m180827_074449_add_dufferent_delivery_charge_by_zip
 */
class m180827_074449_add_different_delivery_charge_by_zip extends Migration
{
    const CONTRACT_ID = 145;
    const OLD_CALC = 'DeliveryChargeByZip';
    const NEW_CALC = 'DifferentDeliveryChargeByZip';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('delivery_charges_calculator', [
            'name' => self::NEW_CALC,
            'class_path' => 'app\modules\delivery\components\charges\DifferentDeliveryChargeByZip',
            'active' => 1,
            'created_at' => time(),
        ]);

        $contract = DeliveryContract::findOne(self::CONTRACT_ID);
        $calcOld = DeliveryChargesCalculator::findOne(['name' => self::OLD_CALC]);
        $calcNew = DeliveryChargesCalculator::findOne(['name' => self::NEW_CALC]);
        if ($contract && $calcNew && $calcOld) {
            $this->update(DeliveryContract::tableName(), ['charges_calculator_id' => $calcNew->id,
                'charges_values' => str_replace($calcOld->name, $calcNew->name, $contract->charges_values)],
                ['id' => $contract->id]);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $contract = DeliveryContract::findOne(self::CONTRACT_ID);
        $calcOld = DeliveryChargesCalculator::findOne(['name' => self::OLD_CALC]);
        $calcNew = DeliveryChargesCalculator::findOne(['name' => self::NEW_CALC]);
        if ($contract && $calcNew && $calcOld) {
            $this->update(DeliveryContract::tableName(), ['charges_calculator_id' => $calcOld->id,
                'charges_values' => str_replace($calcNew->name, $calcOld->name, $contract->charges_values)],
                ['id' => $contract->id]);
        }

        $this->delete('delivery_charges_calculator', ['name' => self::NEW_CALC]);
    }
}