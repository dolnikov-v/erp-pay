<?php

use app\components\CustomMigration as Migration;

/**
 * Handles adding expenses to table `salary_office`.
 */
class m171030_060623_add_expenses_to_salary_office extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('salary_office', 'expense_rent', $this->decimal());
        $this->addColumn('salary_office', 'expense_account_services', $this->decimal());
        $this->addColumn('salary_office', 'expense_internet', $this->decimal());
        $this->addColumn('salary_office', 'expense_stationery', $this->decimal());
        $this->addColumn('salary_office', 'expense_utilities', $this->decimal());
        $this->addColumn('salary_office', 'expense_tax', $this->decimal());
        $this->addColumn('salary_office', 'expense_office_supplies', $this->decimal());
        $this->addColumn('salary_office', 'expense_office_expenses', $this->decimal());
        $this->addColumn('salary_office', 'expense_telephony_office', $this->decimal());
        $this->addColumn('salary_office', 'expense_security', $this->decimal());
        $this->addColumn('salary_office', 'expense_other', $this->decimal());
        $this->addColumn('salary_office', 'expense_cleaning', $this->decimal());
        $this->addColumn('salary_office', 'expense_electricity', $this->decimal());
        $this->addColumn('salary_office', 'expense_legal_expenses', $this->decimal());
        $this->addColumn('salary_office', 'expense_courier_services', $this->decimal());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('salary_office', 'expense_rent');
        $this->dropColumn('salary_office', 'expense_account_services');
        $this->dropColumn('salary_office', 'expense_internet');
        $this->dropColumn('salary_office', 'expense_stationery');
        $this->dropColumn('salary_office', 'expense_utilities');
        $this->dropColumn('salary_office', 'expense_tax');
        $this->dropColumn('salary_office', 'expense_office_supplies');
        $this->dropColumn('salary_office', 'expense_office_expenses');
        $this->dropColumn('salary_office', 'expense_telephony_office');
        $this->dropColumn('salary_office', 'expense_security');
        $this->dropColumn('salary_office', 'expense_other');
        $this->dropColumn('salary_office', 'expense_cleaning');
        $this->dropColumn('salary_office', 'expense_electricity');
        $this->dropColumn('salary_office', 'expense_legal_expenses');
        $this->dropColumn('salary_office', 'expense_courier_services');
    }
}
