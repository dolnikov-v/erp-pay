<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160824_073358_delivery_auto_sending
 */
class m160824_073358_delivery_auto_sending extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('delivery', 'auto_sending', $this->smallInteger()->notNull()->defaultValue(0)->after('active'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('delivery', 'auto_sending');
    }
}
