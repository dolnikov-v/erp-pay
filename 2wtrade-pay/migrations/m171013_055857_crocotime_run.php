<?php
use app\components\CustomMigration as Migration;

/**
 * Class m171013_055857_crocotime_run
 */
class m171013_055857_crocotime_run extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('crocotime_work_time', 'date', $this->date());
        $this->addColumn('crocotime_work_time', 'forbidden_time', $this->integer());
        $this->addColumn('crocotime_work_time', 'unknown_time', $this->integer());
        $this->addColumn('crocotime_work_time', 'late_time', $this->integer());
        $this->addColumn('crocotime_work_time', 'early_end_time', $this->integer());
        $this->addColumn('crocotime_work_time', 'summary_time', $this->integer());
        $this->addColumn('crocotime_work_time', 'norm', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('crocotime_work_time', 'date', $this->integer(6));
        $this->dropColumn('crocotime_work_time', 'forbidden_time');
        $this->dropColumn('crocotime_work_time', 'unknown_time');
        $this->dropColumn('crocotime_work_time', 'late_time');
        $this->dropColumn('crocotime_work_time', 'early_end_time');
        $this->dropColumn('crocotime_work_time', 'summary_time');
        $this->dropColumn('crocotime_work_time', 'norm');
    }
}
