<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160919_083804_add_idx_file_anser_crontab
 */
class m160919_083804_add_idx_file_anser_crontab extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createIndex(null, 'crontab_task_log_answer', 'file_answer');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropIndex($this->getIdxName('crontab_task_log_answer', 'file_answer'), 'crontab_task_log_answer');
    }
}
