<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160721_102657_answer_big_text
 */
class m160721_102657_answer_big_text extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('crontab_task_log_answer', 'answer', 'MEDIUMTEXT');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('crontab_task_log_answer', 'answer', $this->text());
    }
}
