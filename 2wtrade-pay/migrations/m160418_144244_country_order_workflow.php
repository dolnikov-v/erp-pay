<?php
use app\components\CustomMigration as Migration;

class m160418_144244_country_order_workflow extends Migration
{
    public function safeUp()
    {
        $this->createTable('country_order_workflow', [
            'id' => $this->primaryKey(),
            'country_id' => $this->integer()->notNull(),
            'workflow_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(null, 'country_order_workflow', 'country_id', 'country', 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, 'country_order_workflow', 'workflow_id', 'order_workflow', 'id', self::CASCADE, self::RESTRICT);
    }

    public function safeDown()
    {
        $this->dropTable('country_order_workflow');
    }
}
