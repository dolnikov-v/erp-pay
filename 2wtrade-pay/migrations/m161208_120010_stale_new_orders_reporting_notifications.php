<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;
use app\models\Notification;

/**
 * Class m161208_120010_stale_new_orders_reporting_notifications
 */
class m161208_120010_stale_new_orders_reporting_notifications extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
		$crontabTask = new CrontabTask();
        $crontabTask->name = 'report_stale_new_order';
        $crontabTask->description = 'Ежечасные нотификации о новых заказах, подвисших в 1-й или 2-й колонке больше чем на час и на сутки';
        $crontabTask->save();

        $model = new Notification([
            'description' => 'Найдено новых заказов висящих в статусе "заказ создан с длинной формы" или "заказ создан с короткой формы" более часа ({overhour}) и более суток ({overday})',
            'trigger' => Notification::TRIGGER_REPORT_STALE_NEW_ORDER,
            'type' => Notification::TYPE_WARNING,
            'group' => Notification::GROUP_REPORT,
            'active' => Notification::ACTIVE,
        ]);

        $model->save(false);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
		$crontabTask = CrontabTask::find()
            ->byName('report_stale_new_order')
            ->one();

        if ($crontabTask instanceof CrontabTask) {
            $crontabTask->delete();
        }

        $model = Notification::findOne(['trigger' => Notification::TRIGGER_REPORT_STALE_NEW_ORDER]);

        if ($model instanceof Notification) {
            $model->delete();
        }
    }
}
