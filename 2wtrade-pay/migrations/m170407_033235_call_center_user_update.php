<?php
use app\components\CustomMigration as Migration;
use app\modules\callcenter\models\CallCenterUser;
use yii\db\Query;

/**
 * Class m170407_033235_call_center_user_update
 */
class m170407_033235_call_center_user_update extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        $this->addColumn(CallCenterUser::tableName(), 'parent_id', $this->integer()->after('id'));
        $this->addForeignKey('fk_call_center_user_parent_id', CallCenterUser::tableName(), 'parent_id', CallCenterUser::tableName(), 'id', self::SET_NULL, self::NO_ACTION);

        $role = 'callcenter.manager';
        $rules = [
            'callcenter.user.index',
            'callcenter.user.view',
            'callcenter.user.edit',
            'callcenter.user.activate',
            'callcenter.user.deactivate',
            'callcenter.user.deletephoto',
        ];

        $query = new Query();

        $is_role = $query->select('*')->from($this->authManager->itemTable)
            ->where(['name' => $role, 'type' => 1])
            ->one();

        if (!$is_role) {
            $this->insert($this->authManager->itemTable, [
                'name' => $role,
                'type' => 1,
                'description' => 'Менеджер КЦ',
                'created_at' => time(),
                'updated_at' => time()]);
        }

        foreach ($rules as $rule) {

            $is_rule = $query->select('*')->from($this->authManager->itemTable)
                ->where(['name' => $rule, 'type' => 2])
                ->one();

            if (!$is_rule) {
                $this->insert($this->authManager->itemTable, [
                    'name' => $rule,
                    'description' => $rule,
                    'type' => 2,
                    'created_at' => time(),
                    'updated_at' => time()]);
            }

            $is_can = $query->select('*')->from($this->authManager->itemChildTable)->where([
                'parent' => $role,
                'child' => $rule
            ])->one();

            if (!$is_can) {
                $this->insert($this->authManager->itemChildTable, [
                    'parent' => $role,
                    'child' => $rule
                ]);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_call_center_user_parent_id', CallCenterUser::tableName());
        $this->dropColumn(CallCenterUser::tableName(), 'parent_id');

        $role = 'callcenter.manager';
        $rules = [
            'callcenter.user.index',
            'callcenter.user.view',
            'callcenter.user.edit',
            'callcenter.user.activate',
            'callcenter.user.deactivate',
            'callcenter.user.deletephoto',
        ];

        foreach ($rules as $rule) {

            $this->delete($this->authManager->itemChildTable, [
                'parent' => $role,
                'child' => $rule
            ]);

        }
    }
}
