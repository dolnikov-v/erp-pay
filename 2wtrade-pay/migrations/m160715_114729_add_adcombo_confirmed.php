<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160715_114729_add_adcombo_confirmed
 */
class m160715_114729_add_adcombo_confirmed extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('order', 'adcombo_confirmed', $this->smallInteger()->defaultValue(0)->after('delivery'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('order', 'adcombo_confirmed');
    }
}
