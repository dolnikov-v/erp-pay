<?php
use app\components\CustomMigration as Migration;

/**
 * Class m171219_124405_refactor_order_check_history
 */
class m171219_124405_refactor_order_check_history extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->renameTable('order_check_history', 'call_center_check_request');
        $this->createIndex($this->getIdxName('call_center_check_request', 'type'), 'call_center_check_request', 'type');

        $this->createIndex('idx_call_center_check_request_status_cc_id_type_launch', 'call_center_check_request', [
            'status',
            'call_center_id',
            'type',
            'cron_launched_at'
        ]);

        $this->createIndex('idx_call_center_check_request_status_cc_id_type_launch_crea', 'call_center_check_request', [
            'status',
            'call_center_id',
            'type',
            'cron_launched_at',
            'created_at'
        ]);

        $this->createIndex('idx_call_center_check_request_status_cc_id_launch_crea', 'call_center_check_request', [
            'status',
            'call_center_id',
            'cron_launched_at',
            'created_at'
        ]);

        $this->createIndex('idx_call_center_check_request_status_type_launch_crea', 'call_center_check_request', [
            'status',
            'type',
            'cron_launched_at',
            'created_at',
        ]);

        $this->createIndex('idx_call_center_check_request_status_launch_crea', 'call_center_check_request', [
            'status',
            'cron_launched_at',
            'created_at',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropIndex($this->getIdxName('call_center_check_request', 'type'), 'call_center_check_request');
        $this->dropIndex('idx_call_center_check_request_status_cc_id_type_launch', 'call_center_check_request');
        $this->dropIndex('idx_call_center_check_request_status_cc_id_type_launch_crea', 'call_center_check_request');
        $this->dropIndex('idx_call_center_check_request_status_cc_id_launch_crea', 'call_center_check_request');
        $this->dropIndex('idx_call_center_check_request_status_type_launch_crea', 'call_center_check_request');
        $this->dropIndex('idx_call_center_check_request_status_launch_crea', 'call_center_check_request');
        $this->renameTable('call_center_check_request', 'order_check_history');
    }
}
