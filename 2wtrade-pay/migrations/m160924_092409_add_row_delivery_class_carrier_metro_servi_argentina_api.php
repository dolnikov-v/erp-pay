<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryApiClass;
/**
 * Class m160924_092409_add_row_delivery_class_carrier_metro_servi_argentina_api
 */
class m160924_092409_add_row_delivery_class_carrier_metro_servi_argentina_api extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $class = new DeliveryApiClass();
        $class->name = 'ArgentinaApi';
        $class->class_path = '/argentina-api/src/ArgentinaApi.php';
        $class->save();

        $class = new DeliveryApiClass();
        $class->name = 'CarrierApi';
        $class->class_path = '/carrier-api/src/carrierApi.php';
        $class->save();

        $class = new DeliveryApiClass();
        $class->name = 'ServiApi';
        $class->class_path = '/servi-api/src/serviApi.php';
        $class->save();

        $class = new DeliveryApiClass();
        $class->name = 'MetropolitanExpressApi';
        $class->class_path = '/metropolitanexpress-api/src/metropolitanExpressApi.php';
        $class->save();

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        DeliveryApiClass::find()
            ->where([
                        'name' => 'ArgentinaApi',
                        'class_path' => '/argentina-api/src/ArgentinaApi.php'
                    ])
            ->one()
            ->delete();

        DeliveryApiClass::find()
            ->where([
                        'name' => 'CarrierApi',
                        'class_path' => '/carrier-api/src/carrierApi.php'
                    ])
            ->one()
            ->delete();

        DeliveryApiClass::find()
            ->where([
                        'name' => 'ServiApi',
                        'class_path' => '/servi-api/src/serviApi.php'
                    ])
            ->one()
            ->delete();

        DeliveryApiClass::find()
            ->where([
                        'name' => 'MetropolitanExpressApi',
                        'class_path' => '/metropolitanexpress-api/src/metropolitanExpressApi.php'
                    ])
            ->one()
            ->delete();
    }
}
