<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160519_125906_api_lead_log_country_id
 */
class m160519_125906_api_lead_log_country_id extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('api_lead_log', 'country_id', $this->integer()->defaultValue(null) . ' AFTER `id`');
        $this->addForeignKey(null, 'api_lead_log', 'country_id', 'country', 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey($this->getFkName('api_lead_log', 'country_id'), 'api_lead_log');
        $this->dropColumn('api_lead_log', 'country_id');
    }
}
