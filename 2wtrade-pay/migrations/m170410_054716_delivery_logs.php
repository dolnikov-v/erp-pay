<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170410_054716_delivery_logs
 */
class m170410_054716_delivery_logs extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('delivery_log', [
            'id' => $this->primaryKey(),
            'delivery_id' => $this->integer()->notNull(),
            'user_id' => $this->integer(),
            'group_id' => $this->string(255),
            'field' => $this->string(255),
            'old' => $this->string(255),
            'new' => $this->string(255),
            'created_at' => $this->integer(),
        ]);
        $this->createIndex(null, 'delivery_log', 'field');
        $this->addForeignKey(null, 'delivery_log', 'delivery_id', 'delivery', 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, 'delivery_log', 'user_id', 'user', 'id', self::CASCADE, self::RESTRICT);

        $this->insert('{{%auth_item}}',array(
            'name'=>'delivery.control.logs',
            'type' => '2',
            'description' => 'delivery.control.logs'
        ));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('delivery_log');
        $this->delete('{{%auth_item}}', ['name' => 'delivery.control.logs']);
    }
}
