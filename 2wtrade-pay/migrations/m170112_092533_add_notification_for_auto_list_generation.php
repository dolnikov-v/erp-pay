<?php
use app\components\CustomMigration as Migration;
use app\models\Notification;
use app\models\NotificationRole;

/**
 * Class m170112_092533_add_notification_for_auto_list_generation
 */
class m170112_092533_add_notification_for_auto_list_generation extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $notification = new Notification([
            'description' => 'Ошибка при автоматической генерации листа для {delivery}: {error}',
            'trigger' => Notification::TRIGGER_ORDER_LIST_AUTO_GENERATION_ERROR,
            'type' => Notification::TYPE_DANGER,
            'group' => Notification::GROUP_SYSTEM,
            'active' => Notification::ACTIVE,
        ]);
        $notification->save();

        $model = new NotificationRole([
            'role'			=> 'country.curator',
            'trigger'		=> Notification::TRIGGER_ORDER_LIST_AUTO_GENERATION_ERROR,
        ]);

        $model->save(false);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $model = Notification::findOne(['trigger' => Notification::TRIGGER_ORDER_LIST_AUTO_GENERATION_ERROR]);

        if ($model instanceof Notification) {
            $model->delete();
        }
    }
}
