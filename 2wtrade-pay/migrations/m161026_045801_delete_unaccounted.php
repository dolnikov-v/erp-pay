<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;
use app\models\Notification;

/**
 * Class m161026_045801_delete_unaccounted
 */
class m161026_045801_delete_unaccounted extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = CrontabTask::find()
            ->byName('product_unaccounted')
            ->one();

        if ($crontabTask) {
            $crontabTask->delete();
        }

        $model = Notification::findOne(['trigger' => 'product.unaccounted']);

        if ($model instanceof Notification) {
            $model->delete();
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = new CrontabTask();
        $crontabTask->name = 'product_unaccounted';
        $crontabTask->description = 'Отправка оповещения о неучтенных на складе товарах.';
        $crontabTask->save();

        $model = new Notification([
            'description' => 'Склады: {count} товаров {product_name} не привязано к {object} {name}.',
            'trigger' => 'product.unaccounted',
            'type' => Notification::TYPE_WARNING,
            'group' => Notification::GROUP_REPORT,
            'active' => Notification::ACTIVE,
        ]);

        $model->save(false);
    }
}
