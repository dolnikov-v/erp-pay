<?php
use app\components\CustomMigration as Migration;
use app\modules\salary\models\Person;
use app\modules\salary\models\Office;

/**
 * Class m170627_053940_center_person_to_office
 */
class m170627_053940_center_person_to_office extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Person::tableName(), 'office_id', $this->integer());
        $this->renameColumn(Person::tableName(), 'salary_sum', 'salary_local');
        $this->addColumn(Person::tableName(), 'salary_usd', $this->float());
        $this->addForeignKey('fk_call_center_person_office_id', Person::tableName(), 'office_id', Office::tableName(), 'id', self::SET_NULL, self::NO_ACTION);
        $this->dropForeignKey('fk_call_center_person_currency_id', Person::tableName());
        $this->dropColumn(Person::tableName(), 'currency_id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_call_center_person_office_id', Person::tableName());
        $this->dropColumn(Person::tableName(), 'office_id');
        $this->renameColumn(Person::tableName(), 'salary_local', 'salary_sum');
        $this->dropColumn(Person::tableName(), 'salary_usd');
    }
}
