<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m161102_123201_new_crontab_task_report_rejected
 */
class m161102_123201_new_crontab_task_report_rejected extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = new CrontabTask();
        $crontabTask->name = 'report_rejected_by_delivery';
        $crontabTask->description = 'Ежедневные нотификации об отклоненных в доставке заказах за последние 31 день';
        $crontabTask->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = CrontabTask::find()
            ->byName('report_rejected_by_delivery')
            ->one();

        if ($crontabTask) {
            $crontabTask->delete();
        }
    }
}
