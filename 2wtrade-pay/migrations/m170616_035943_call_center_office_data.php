<?php
use app\components\CustomMigration as Migration;
use app\models\Country;
use app\modules\salary\models\Office;
use app\modules\callcenter\models\CallCenter;

/**
 * Class m170616_035943_call_center_office_data
 */
class m170616_035943_call_center_office_data extends Migration
{

    const DATA = [
        'Индия' => 'Индия, Сингапур, Нигерия, Шри-Ланка',
        'Пакистан' => 'Пакистан',
        'Таиланд' => 'Таиланд, Филиппины',
        'Тунис' => 'Тунис, ОАЭ, Саудовская Аравия',
        'Индонезия' => 'Индонезия, Малайзия',
        'Филиппины' => 'Филиппины',
        'Камбоджа' => 'Камбоджа, Вьетнам, Сингапур',
        'Китай' => 'Китай, Гонконг, Тайвань',
        'Колумбия' => 'Чили, Перу, Колумбия, Аргентина',
        'Гватемала' => 'Гватемала, Мексика'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->update(CallCenter::tableName(), ['office_id' => null]);
        $this->delete(Office::tableName());

        foreach (self::DATA as $office => $line) {
            $callCenterOffice = new Office();
            $callCenterOffice->name = 'Колл-центр ' . $office;

            $country = self::getCountry($office);

            if ($country) {

                $callCenterOffice->country_id = $country->id;
                $callCenterOffice->active = 1;
                $callCenterOffice->created_at = time();
                $callCenterOffice->updated_at = time();
                if ($callCenterOffice->save()) {

                    foreach (explode(',', $line) as $direction) {

                        $callCenter = self::getCallCenter(trim($direction));

                        if ($callCenter) {
                            $callCenter->office_id = $callCenterOffice->id;
                            $callCenter->save();
                        }
                    }
                }
            }
        }
    }

    /**
     * @param string $countryName
     * @return Country|false
     */
    public static function getCountry($countryName)
    {
        // партнеры так
        if ($countryName == 'Таиланд') $countryName = 'Таиланд 2';
        if ($countryName == 'Индонезия') $countryName = 'Индонезия 1';
        if ($countryName == 'Малайзия') $countryName = 'Малайзия 1';

        $country = Country::findOne(['name' => $countryName]);
        if ($country) {
            return $country;
        }
        return false;
    }

    /**
     * @param string $countryName
     * @return CallCenter|false
     */
    public static function getCallCenter($countryName)
    {
        $country = self::getCountry($countryName);
        if ($country) {
            return CallCenter::find()->byCountryId($country->id)->one();
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->update(CallCenter::tableName(), ['office_id' => null]);
        $this->delete(Office::tableName());
    }
}
