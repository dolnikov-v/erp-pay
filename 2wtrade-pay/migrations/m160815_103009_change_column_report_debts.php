<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160815_103009_change_column_report_debts
 */
class m160815_103009_change_column_report_debts extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('report_debts', 'debts', $this->decimal(15, 2)->defaultValue(null));
        $this->alterColumn('report_debts', 'potential_revenue', $this->decimal(15, 2)->defaultValue(null));
        $this->alterColumn('report_debts', 'revenue', $this->decimal(15, 2)->defaultValue(null));
        $this->alterColumn('report_debts', 'refund', $this->decimal(15, 2)->defaultValue(null));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('report_debts', 'debts', $this->decimal(11, 2)->defaultValue(null));
        $this->alterColumn('report_debts', 'potential_revenue', $this->decimal(11, 2)->defaultValue(null));
        $this->alterColumn('report_debts', 'revenue', $this->decimal(11, 2)->defaultValue(null));
        $this->alterColumn('report_debts', 'refund', $this->decimal(11, 2)->defaultValue(null));
    }
}
