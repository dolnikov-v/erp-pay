<?php
use app\components\CustomMigration as Migration;

/**
 * Class m161224_041955_add_delivery_request_column_last_update_info
 */
class m161224_041955_add_delivery_request_column_last_update_info extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('delivery_request', 'last_update_info', 'VARCHAR(2000) DEFAULT NULL AFTER foreign_info');
        $this->addCommentOnColumn('delivery_request', 'last_update_info', 'Last response update delivery status info from Courier Service');
        $this->addColumn('delivery_request', 'last_update_info_at', 'INTEGER(11) DEFAULT NULL AFTER last_update_info');
        $this->addCommentOnColumn('delivery_request', 'last_update_info_at', 'Time from last response Courier Service on update status delivery');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('delivery_request', 'last_update_info');
        $this->dropColumn('delivery_request', 'last_update_info_at');

    }
}
