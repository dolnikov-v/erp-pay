<?php
use app\components\PermissionMigration as Migration;

/**
 * Class m171218_040400_order_export
 */
class m171218_040400_order_export extends Migration
{
    protected $permissions = [
        'order.index.export'
    ];

    protected $roles = [
        'accountant' => ['order.index.export'],
        'admin' => ['order.index.export'],
        'auditor' => ['order.index.export'],
        'auditor.helper' => ['order.index.export'],
        'business_analyst' => ['order.index.export'],
        'callcenter.leader' => ['order.index.export'],
        'callcenter.manager' => ['order.index.export'],
        'controller.analyst' => ['order.index.export'],
        'country.curator' => ['order.index.export'],
        'delivery.manager' => ['order.index.export'],
        'deliveryreport.clerk' => ['order.index.export'],
        'finance.director' => ['order.index.export'],
        'foreign.deliveryreport.clerk' => ['order.index.export'],
        'it_controller' => ['order.index.export'],
        'junior.logist' => ['order.index.export'],
        'logist' => ['order.index.export'],
        'moderator_cc' => ['order.index.export'],
        'monika.role' => ['order.index.export'],
        'project.manager' => ['order.index.export'],
        'translator' => ['order.index.export']
    ];
}
