<?php
use app\components\CustomMigration as Migration;

/**
 * Class m190514_083813_delivery_change_default_values
 */
class m190514_083813_delivery_change_default_values extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('delivery', 'not_send_if_has_debts', $this->boolean()->defaultValue(0));

        $this->alterColumn('delivery', 'not_send_if_in_process', $this->boolean()->defaultValue(0));

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('delivery', 'not_send_if_has_debts', $this->boolean()->defaultValue(1));

        $this->alterColumn('delivery', 'not_send_if_in_process', $this->boolean()->defaultValue(1));
    }
}
