<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryApiClass;
use app\models\Country;

/**
 * Class m170306_092941_add_delivery_api_dtds_for_singapore
 */
class m170306_092941_add_delivery_api_dtds_for_singapore extends Migration
{
    /**
     * @return Country
     */
    private function getCountry()
    {
        $country = Country::find()
            ->select('id')
            ->where([
                'char_code' => 'SG',
                'name' => 'Сингапур',
            ])
            ->one();

        return $country;
    }

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $apiClass = DeliveryApiClass::find()
            ->where(['name' => 'DTDCApi'])
            ->one();

        if ($apiClass && $country = $this->getCountry()) {

            $delivery = new Delivery();
            $delivery->country_id = $country->id;
            $delivery->name = 'DTDC';
            $delivery->char_code = 'DT';
            $delivery->api_class_id = $apiClass->id;
            $delivery->active = 1;
            $delivery->auto_sending = 0;
            $delivery->created_at = time();
            $delivery->updated_at = time();

            return $delivery->save();
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        if ($country = $this->getCountry()) {

            $delivery = Delivery::find()
                ->where([
                    'name' => 'DTDC',
                    'country_id' => $country->id
                ])
                ->one();

            return $delivery->delete();
        }

        return false;
    }
}
