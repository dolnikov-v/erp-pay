<?php
use app\components\CustomMigration as Migration;

/**
 * Class m181018_082834_rename_columns_with_amazon_queue_url
 */
class m181018_082834_rename_columns_with_amazon_queue_url extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->renameColumn('external_source', 'incoming_queue_amazon_url', 'incoming_amazon_queue_name');
        $this->renameColumn('external_source', 'outbound_queue_amazon_url', 'outbound_amazon_queue_name');
        $this->update('external_source', ['incoming_amazon_queue_name' => new \yii\db\Expression('REPLACE(`incoming_amazon_queue_name`, \'https://sqs.eu-west-1.amazonaws.com/636470419474/\', \'\')')]);
        $this->update('external_source', ['outbound_amazon_queue_name' => new \yii\db\Expression('REPLACE(`outbound_amazon_queue_name`, \'https://sqs.eu-west-1.amazonaws.com/636470419474/\', \'\')')]);

        $this->renameColumn('delivery', 'amazon_queue_url', 'amazon_queue_name');
        $this->renameColumn('delivery_api_class', 'amazon_queue_url', 'amazon_queue_name');
        $this->update('delivery', ['amazon_queue_name' => new \yii\db\Expression('REPLACE(`amazon_queue_name`, \'https://sqs.eu-west-1.amazonaws.com/636470419474/\', \'\')')]);
        $this->update('delivery_api_class', ['amazon_queue_name' => new \yii\db\Expression('REPLACE(`amazon_queue_name`, \'https://sqs.eu-west-1.amazonaws.com/636470419474/\', \'\')')]);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->update('external_source', ['incoming_amazon_queue_name' => new \yii\db\Expression('CONCAT(\'https://sqs.eu-west-1.amazonaws.com/636470419474/\', `incoming_amazon_queue_name`)')]);
        $this->update('external_source', ['outbound_amazon_queue_name' => new \yii\db\Expression('CONCAT(\'https://sqs.eu-west-1.amazonaws.com/636470419474/\', `outbound_amazon_queue_name`)')]);
        $this->update('delivery', ['amazon_queue_name' => new \yii\db\Expression('CONCAT(\'https://sqs.eu-west-1.amazonaws.com/636470419474/\', `amazon_queue_name`)')]);
        $this->update('delivery_api_class', ['amazon_queue_name' => new \yii\db\Expression('CONCAT(\'https://sqs.eu-west-1.amazonaws.com/636470419474/\', `amazon_queue_name`)')]);
        $this->renameColumn('delivery', 'amazon_queue_name', 'amazon_queue_url');
        $this->renameColumn('delivery_api_class', 'amazon_queue_name', 'amazon_queue_url');
        $this->renameColumn('external_source', 'incoming_amazon_queue_name', 'incoming_queue_amazon_url');
        $this->renameColumn('external_source', 'outbound_amazon_queue_name', 'outbound_queue_amazon_url');
    }
}
