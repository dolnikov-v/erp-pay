<?php
use app\components\CustomMigration as Migration;
use app\modules\salary\models\PenaltyType;

/**
 * Class m170614_083432_call_center_user_salary_penalty_data
 */
class m170614_083432_call_center_user_salary_penalty_data extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $penaltyData = [
            'ID-card loss' => 20,
            '10 minutes delay for work' => 10,
            'Using social media sites during work' => 10,
            'Smoking in not appropriate places (WC, in the office, in the building)' => 5,
            'Using corporate mail for personal reasons' => 3,
            'Property taking-out from the office' => 500,
            'Contact with other business companies for personal goals' => 300,
            'Office exploitation violation (forget to close the office door, entrance door, etc)' => 100,
            'Delay for a conference with a Head Office' => 50,
            'Not following the management orders' => 10,
            'Absence without any reason or without management warning' => 10,
            'Finishing the working day earlier' => 10,
            'Business ethics violation (public abuse, insult, raising voice, colleagues ignore, talking about management)' => 15,
            'Absence without any reasonable excuse or without management warning' => 10,
            'Inventory damage' => 100,
            'Hiding the working process information (problems, mistakes, information)' => 50,
            'Creating climate violating colleagues working process' => 50,
            'Not keeping the kitchen in order' => 20,
            'Not following management/head orders, regulations' => 50,
            'Working process violation (internet cut, legal issues, telephone, rent issues, no office supplies, etc)' => 50
        ];

        foreach ($penaltyData as $name => $sum) {
            $object = new PenaltyType();
            $object->name = $name;
            $object->sum = $sum;
            $object->active = 1;
            $object->type = PenaltyType::PENALTY_TYPE_SUM;
            $object->currency_id = 1;   //USD
            $object->created_at = time();
            $object->updated_at = time();
            $object->created_by = 1;    //superadmin
            $object->updated_by = 1;    //superadmin
            $object->save();
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
    }
}
