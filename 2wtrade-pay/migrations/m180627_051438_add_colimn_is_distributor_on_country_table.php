<?php
use app\components\CustomMigration as Migration;
use app\models\Country;

/**
 * Class m180627_051438_add_colimn_is_distributor_on_country_table
 */
class m180627_051438_add_colimn_is_distributor_on_country_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Country::tableName(), 'is_distributor', $this->boolean()->defaultValue(0));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Country::tableName(), 'is_distributor');
    }
}
