<?php
use app\components\PermissionMigration as Migration;
use app\models\Notification;
use yii\db\Query;

/**
 * Class m181108_113125_sql_error_notifycation
 */
class m181108_113125_sql_error_notifycation extends Migration
{

    protected $permissions = [
        'logger.sqlerror.index',
        'logger.sqlerror.view'
    ];

    protected $roles = [
        'technical.director' => [
            'logger.sqlerror.index',
            'logger.sqlerror.view'
        ],
        'support.manager' => [
            'logger.sqlerror.index',
            'logger.sqlerror.view'
        ],
    ];

    protected $notifications = [
        'sql.error.exception' => '{message}',
    ];

    protected $notification_roles = ['technical.director', 'support.manager'];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        foreach ($this->notifications as $trigger => $description) {

            $notificationId = (new Query())->select('id')
                ->from('notification')
                ->where(['trigger' => $trigger])
                ->scalar();

            $data = [
                'description' => $description,
                'trigger' => $trigger,
                'type' => Notification::TYPE_DANGER,
                'group' => Notification::GROUP_SYSTEM,
                'active' => Notification::ACTIVE,
            ];

            if (!$notificationId) {
                $this->insert('notification', $data);
            } else {
                $this->update('notification', $data, ['id' => $notificationId]);
            }
        }

        foreach ($this->notification_roles as $role) {
            foreach ($this->notifications as $trigger => $description) {
                $this->insert('notification_role', [
                    'role' => $role,
                    'trigger' => $trigger,
                    'created_at' => time(),
                    'updated_at' => time(),
                ]);
            }
        }

        parent::safeUp();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach ($this->notifications as $trigger => $description) {
            $this->delete('user_notification_setting', ['trigger' => $trigger]);
            $this->delete('notification', ['trigger' => $trigger]);
        }

        parent::safeDown();
    }
}