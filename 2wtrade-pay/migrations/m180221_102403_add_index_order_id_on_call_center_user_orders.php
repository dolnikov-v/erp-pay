<?php
use app\components\CustomMigration as Migration;

/**
 * Class m180221_102403_add_index_order_id_on_call_center_user_orders
 */
class m180221_102403_add_index_order_id_on_call_center_user_orders extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createIndex(
            $this->getIdxName("call_center_user_order", "order_id"),
            "call_center_user_order",
            "order_id"
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropIndex(
            $this->getIdxName("call_center_user_order", "order_id"),
            "call_center_user_order"
        );
    }
}
