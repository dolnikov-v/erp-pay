<?php
use app\components\PermissionMigration as Migration;

/**
 * Class m171120_054340_call_center_check_limits
 */
class m171120_054340_call_center_check_limits extends Migration
{
    protected $roles = [
        'controller.analyst' => [
            'checklist.call.index',
            'limit_call_center_persons_by_direction',
            'limit_call_center_persons_by_office',
        ],
        'country.curator' => [
            'checklist.call.index',
        ],
    ];
}
