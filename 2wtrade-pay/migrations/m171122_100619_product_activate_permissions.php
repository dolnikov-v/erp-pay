<?php
use app\components\CustomMigration as Migration;

/**
 * Class m171122_100619_product_activate_permissions
 */
class m171122_100619_product_activate_permissions extends \app\components\PermissionMigration
{
    /**
     * @var array
     */
    protected $permissions = [
        'catalog.product.activate',
        'catalog.product.deactivate'
    ];

    /**
     * @var array
     */
    protected $roles = [
        'project.manager' => [
            'catalog.product.activate',
            'catalog.product.deactivate'
        ],
        'admin' => [
            'catalog.product.activate',
            'catalog.product.deactivate'
        ],
    ];
}
