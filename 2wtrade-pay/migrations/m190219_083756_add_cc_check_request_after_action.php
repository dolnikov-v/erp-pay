<?php

use app\components\CustomMigration as Migration;

/**
 * Class m190219_083756_add_cc_check_request_after_action
 */
class m190219_083756_add_cc_check_request_after_action extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('call_center_check_request', 'after_action_id', $this->smallInteger()
            ->after('need_correction'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('call_center_check_request', 'after_action_id');
    }
}
