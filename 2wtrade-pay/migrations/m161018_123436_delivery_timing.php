<?php
use app\components\CustomMigration as Migration;

/**
 * Class m161018_123436_delivery_timing
 */
class m161018_123436_delivery_timing extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('delivery_timing', [
            'id' => $this->primaryKey(),
            'delivery_id' => $this->integer()->notNull(),
            'offset_from' => $this->integer()->notNull(),
            'offset_to' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $this->tableOptions);

        $this->addForeignKey(null, 'delivery_timing', 'delivery_id', 'delivery', 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('delivery_timing');
    }
}
