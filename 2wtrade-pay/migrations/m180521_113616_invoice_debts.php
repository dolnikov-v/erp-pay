<?php
use app\components\PermissionMigration as Migration;
use app\modules\report\models\InvoiceDebt;
use app\modules\report\models\Invoice;
use app\modules\report\models\InvoiceOrder;

/**
 * Class m180521_113616_invoice_debts
 */
class m180521_113616_invoice_debts extends Migration
{
    protected $permissions = ['report.debtsinvoice.index'];

    protected $roles = [
        'admin'=> ['report.debtsinvoice.index'],
        'auditor'=> ['report.debtsinvoice.index'],
        'controller.analyst'=> ['report.debtsinvoice.index'],
        'country.curator'=> ['report.debtsinvoice.index'],
        'finance.director'=> ['report.debtsinvoice.index'],
        'finance.manager'=> ['report.debtsinvoice.index'],
        'project.manager'=> ['report.debtsinvoice.index'],
        'quality.manager'=> ['report.debtsinvoice.index'],
        'sales.director'=> ['report.debtsinvoice.index'],
        'support.manager'=> ['report.debtsinvoice.index'],
        'technical.director'=> ['report.debtsinvoice.index'],
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(InvoiceDebt::tableName(), [
            'id' => $this->primaryKey(),
            'invoice_id' => $this->integer()->notNull(),
            'debt_month' => $this->date(),
            'period_from' => $this->integer(),
            'period_to' => $this->integer(),
            'buyout_local' => $this->double(),
            'buyout_usd' => $this->double(),
            'service_local' => $this->double(),
            'service_usd' => $this->double(),
            'total_local' => $this->double(),
            'total_usd' => $this->double(),
            'additional_charges_local' => $this->double(),
            'additional_charges_usd' => $this->double(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $this->tableOptions);

        $this->addForeignKey(null, InvoiceDebt::tableName(), 'invoice_id', Invoice::tableName(), 'id', self::CASCADE, self::RESTRICT);

        $this->addColumn(InvoiceOrder::tableName(), 'invoice_debt_id', $this->integer());

        parent::safeUp();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(InvoiceOrder::tableName(), 'invoice_debt_id');
        $this->dropTable(InvoiceDebt::tableName());

        parent::safeDown();
    }
}
