<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryApiClass;

/**
 * Class m170302_120822_add_delivery_api_class_ups
 */
class m170302_120822_add_delivery_api_class_ups extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $apiClass = new DeliveryApiClass();
        $apiClass->name = 'UPS';
        $apiClass->class_path = '/ups-api/src/upsApi.php';
        $apiClass->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        DeliveryApiClass::find()
            ->where([
                'name' => 'UPS',
            ])
            ->one()
            ->delete();
    }
}
