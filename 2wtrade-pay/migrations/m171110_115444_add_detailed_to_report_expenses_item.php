<?php

use app\components\CustomMigration as Migration;

/**
 * Handles adding detailed to table `report_expenses_item`.
 */
class m171110_115444_add_detailed_to_report_expenses_item extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('report_expenses_item', 'detailed', $this->integer()->defaultValue(0));
        $this->addColumn('report_expenses_item', 'is_expense', $this->integer()->defaultValue(1));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('report_expenses_item', 'detailed');
        $this->dropColumn('report_expenses_item', 'is_expense');
    }
}
