<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160520_103605_notification_user_setting
 */
class m160520_103605_notification_user_setting extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('notification_user_setting', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'trigger' => $this->string(255)->notNull(),
            'active' => $this->smallInteger()->defaultValue(1)->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $this->tableOptions);
        $this->addForeignKey(
            null, 'notification_user_setting', 'user_id', 'user', 'id', 'CASCADE'
        );
        $this->addForeignKey(
            null, 'notification_user_setting', 'trigger', 'notification', 'trigger', 'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('notification_user_setting');
    }
}
