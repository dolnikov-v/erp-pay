<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryApiClass;

/**
 * Class m170928_041832_add_vsp_api
 */
class m170928_041832_add_vsp_api extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $class = new DeliveryApiClass();
        $class->name = 'VSPApi';
        $class->class_path = '/vsp-api/src/VSPApi.php';
        $class->active = 1;
        $class->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        DeliveryApiClass::find()
            ->where([
                'name' => 'VSPApi',
            ])
            ->one()
            ->delete();
    }
}
