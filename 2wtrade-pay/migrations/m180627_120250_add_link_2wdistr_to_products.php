<?php

use app\models\Product;
use app\models\Source;
use app\models\SourceProduct;
use app\modules\api\components\filters\auth\HttpBearerAuth;
use yii\db\Migration;
use yii\helpers\ArrayHelper;

/**
 * Handles adding link_2wdistr to table `products`.
 */
class m180627_120250_add_link_2wdistr_to_products extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $products = Product::find()->collection();
        $source = Source::findOne(['name' => HttpBearerAuth::ACCESS_TYPE_2WDISTR]);

        if (!empty($products) && $source) {
            foreach ($products as $id => $name) {
                $source_product = new SourceProduct();
                $source_product->source_id = $source->id;
                $source_product->product_id = $id;
                $source_product->save();
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $source = Source::findOne(['name' => HttpBearerAuth::ACCESS_TYPE_2WDISTR]);

        if ($source) {
            SourceProduct::deleteAll(['source_id' => $source->id]);
        }
    }
}
