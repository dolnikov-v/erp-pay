<?php
use app\components\PermissionMigration as Migration;
use yii\db\Query;

/**
 * Class m180124_021405_check_list_call
 */
class m180124_021405_check_list_call extends Migration
{
    const TEXT = [
        'Чек-лист Супервайзера КЦ' => 'Check-list of Sales Leader',
    ];

    protected $roles = [
        'callcenter.leader' => ['checklist.call.set']
    ];

    protected $removeRoles = [
        'project.manager' => ['checklist.call.set'],
        'callcenter.teamlead' => ['checklist.call.set'],
        'admin' => ['checklist.call.set'],
    ];


    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $query = new Query();

        foreach ($this::TEXT as $ruText => $enText) {

            $is = $query->select('id')
                ->from('i18n_source_message')
                ->where(['category' => 'common', 'message' => $ruText])->one();

            if (!$is) {
                $this->insert('i18n_source_message', ['category' => 'common', 'message' => $ruText]);
                $id = Yii::$app->db->getLastInsertID();
                $this->insert('i18n_message', ['id' => $id, 'language' => 'en-US', 'translation' => $enText]);
            }
        }


        foreach ($this->removeRoles as $role => $permissions) {
            foreach ($permissions as $permission) {
                $this->delete($this->authManager->itemChildTable, [
                    'parent' => $role,
                    'child' => $permission
                ]);
            }
        }

        parent::safeUp();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach ($this::TEXT as $ruText => $enText) {
            $this->delete('i18n_source_message', ['category' => 'common', 'message' => $ruText]);
        }

        foreach ($this->roles as $role => $permissions) {
            foreach ($permissions as $permission) {
                $query = new Query();
                $exists = $query->from($this->authManager->itemChildTable)->where([
                    'parent' => $role,
                    'child' => $permission
                ])->exists();
                if (!$exists) {
                    $this->insert($this->authManager->itemChildTable, [
                        'parent' => $role,
                        'child' => $permission
                    ]);
                }
            }
        }

        parent::safeDown();
    }
}
