<?php
use app\components\CustomMigration as Migration;
use yii\db\Query;

/**
 * Class m171025_032953_unbuyout_reasons
 */
class m171025_032953_unbuyout_reasons extends Migration
{

    const RULES = [
        'catalog.unbuyoutreason.index',
        'catalog.unbuyoutreason.edit',
        'catalog.unbuyoutreason.delete',
        'catalog.unbuyoutreason.activate',
        'catalog.unbuyoutreason.deactivate',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $query = new Query();

        foreach (self::RULES as $rule) {

            $is_rule = $query->select('*')->from($this->authManager->itemTable)
                ->where(['name' => $rule, 'type' => 2])
                ->one();

            if (!$is_rule) {
                $this->insert($this->authManager->itemTable, [
                    'name' => $rule,
                    'description' => $rule,
                    'type' => 2,
                    'created_at' => time(),
                    'updated_at' => time()]);
            }
        }

        $data = [
            "Delivery: doesn’t pick up the phone" => "Невозможно дозвониться",
            "Delivery: not at home" => "Клиент отсутствует дома",
            "Delivery: bad products or package" => "Отказ по причине проблем с продуктом",
            "Delivery: late delivery" => "Слишком долго",
            "Delivery: already delivered" => "Уже доставлен",
            "Delivery: lack of money" => "Отказ, нет денег",
            "Delivery: no cash" => "Отказ, нет денег",
            "Delivery: bad courier communication" => "Отказ из-за поведения или общения курьера с клиентом",
            "Delivery: wrong address / can’t find an address" => "Адрес не найден",
            "Delivery: wrong goods quantity" => "Неправильное количество товаров",
            "Before delivery: lack of money" => "Отказ, нет денег",
            "Before delivery: no cash" => "Отказ, нет денег",
            "Before delivery: price" => "Не согласен с ценой",
            "Before delivery: didn’t order anything" => "Ничего не заказывал",
            "Before delivery: wrong product" => "Заказывал другой продукт",
            "Before delivery: didn’t pick up the phone" => "Невозможно дозвониться",
            "Before delivery: wrong phone number" => "Невозможно дозвониться",
            "Before delivery: already delivered" => "Уже доставлен",
            "Before delivery: order double" => "Двойной заказ",
            "Before delivery: out of town" => "Покупатель вне города",
            "Before delivery: changed his mind" => "Изменил свое мнение",
            "Before delivery: joke" => "Заказал вшутку",
            "Before delivery: wrong goods quantity" => "Неправильное количество товаров",
            "Before delivery: late order processing" => "Слишком долго",
            "Before delivery: inconvenient" => "Неудобно",
            "Goods are out of stocks" => "Товар закончился на складе",
            "Out of coverage" => "Вне зоны покрытия",
            "Other" => "Другое",
            "Cancelled through client call center" => "Отменено в исходном КЦ",
            "undefined" => "Другое",
            "no_money" => "Отказ, нет денег",
            "refuse" => "Изменил свое мнение",
            "invalid_address" => "Адрес не найден",
            "already_baught" => "Уже доставлен",
            "already_bought" => "Уже доставлен",
            "courier_not_come" => "Отказ из-за поведения или общения курьера с клиентом",
            "too_expensive" => "Не согласен с ценой",
            "doctor_recomendations" => "Отказ по причине проблем с продуктом",
            "unknown_product" => "Отказ по причине проблем с продуктом",
            "negative_feedback" => "Отказ по причине проблем с продуктом",
            "uncalled" => "Невозможно дозвониться",
            "delivery_issues" => "Адрес не найден",
    
        ];

        $this->createTable('unbuyout_reason', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'active' => $this->integer(1)->defaultValue(1),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $this->tableOptions);

        $this->createTable('unbuyout_reason_mapping', [
            'id' => $this->primaryKey(),
            'reason_id' => $this->integer()->notNull(),
            'foreign_reason' => $this->string(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $this->tableOptions);

        $this->addForeignKey(null, 'unbuyout_reason_mapping', 'reason_id', 'unbuyout_reason', 'id', self::CASCADE, self::NO_ACTION);
        $this->createIndex(null, 'unbuyout_reason_mapping', 'foreign_reason');

        foreach ($data as $foreign => $our) {
            $is_our = $query->select('*')->from('unbuyout_reason')
                ->where(['name' => $our])
                ->one();
            if (!$is_our) {
                $this->insert('unbuyout_reason', [
                    'name' => $our,
                    'active' => 1,
                    'created_at' => time(),
                    'updated_at' => time()]);
                $id = Yii::$app->db->getLastInsertID();
            } else {
                $id = $is_our['id'];
            }
            $is_foreign = $query->select('*')->from('unbuyout_reason_mapping')
                ->where([
                    'reason_id' => $id,
                    'foreign_reason' => $foreign
                ])
                ->one();

            if (!$is_foreign) {
                $this->insert('unbuyout_reason_mapping', [
                    'reason_id' => $id,
                    'foreign_reason' => $foreign,
                    'created_at' => time(),
                    'updated_at' => time()]);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach (self::RULES as $rule) {

            $this->delete($this->authManager->itemTable, [
                'name' => $rule,
                'type' => 2
            ]);

        }

        $this->dropTable('unbuyout_reason_mapping');
        $this->dropTable('unbuyout_reason');
    }
}
