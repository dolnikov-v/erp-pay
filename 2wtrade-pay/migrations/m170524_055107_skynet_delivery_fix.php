<?php
use app\components\CustomMigration as Migration;
use app\models\Country;
use app\modules\delivery\models\Delivery;

/**
 * Class m170524_055107_skynet_delivery_fix
 */
class m170524_055107_skynet_delivery_fix extends Migration
{

    /**
     * @return integer $countryId
     */
    private function getCountryId()
    {
        $countryId = Country::find()
            ->select('id')
            ->where(['slug' => 'ae'])
            ->scalar();
        return $countryId;
    }

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $skyNetDeliveriesAE = Delivery::find()
            ->where([
                'country_id' => $this->getCountryId(),
                'name' => 'Sky Net',
            ])
            ->select('id')
            ->asArray()
            ->all();

        if (count($skyNetDeliveriesAE)>1) {
            $i = 1;
            foreach ($skyNetDeliveriesAE as $delivery) {
                if ($i==1) {
                    $i++;
                    continue;
                }
                Delivery::find()
                    ->where([
                        'id' => $delivery['id'],
                    ])
                    ->one()
                    ->delete();
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

    }
}
