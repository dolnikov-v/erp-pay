<?php

use app\modules\order\models\OrderFinanceFact;
use yii\db\Migration;

/**
 * Handles adding fields_rate_currency to table `order_finance_fact`.
 */
class m180702_053839_add_fields_rate_currency_to_order_finance_fact extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn(OrderFinanceFact::tableName(), 'price_cod_currency_rate', $this->double()->after('price_cod_currency_id'));
        $this->addColumn(OrderFinanceFact::tableName(), 'price_delivery_currency_rate', $this->double()->after('price_delivery_currency_id'));
        $this->addColumn(OrderFinanceFact::tableName(), 'price_redelivery_currency_rate', $this->double()->after('price_redelivery_currency_id'));
        $this->addColumn(OrderFinanceFact::tableName(), 'price_delivery_back_currency_rate', $this->double()->after('price_delivery_back_currency_id'));
        $this->addColumn(OrderFinanceFact::tableName(), 'price_delivery_return_currency_rate', $this->double()->after('price_delivery_return_currency_id'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn(OrderFinanceFact::tableName(), 'price_cod_currency_rate');
        $this->dropColumn(OrderFinanceFact::tableName(), 'price_delivery_currency_rate');
        $this->dropColumn(OrderFinanceFact::tableName(), 'price_redelivery_currency_rate');
        $this->dropColumn(OrderFinanceFact::tableName(), 'price_delivery_back_currency_rate');
        $this->dropColumn(OrderFinanceFact::tableName(), 'price_delivery_return_currency_rate');
    }
}
