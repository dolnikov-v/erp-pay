<?php

use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryApiClass;

/**
 * Handles adding amazon_queue_url to table `delivery_api_class`.
 */
class m170803_085125_add_amazon_queue_url_to_delivery_api_class extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(DeliveryApiClass::tableName(), 'amazon_queue_url', $this->string(255)->after('use_amazon_queue'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(DeliveryApiClass::tableName(), 'amazon_queue_url');
    }
}
