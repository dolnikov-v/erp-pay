<?php
use app\components\CustomMigration as Migration;
use app\modules\salary\models\BonusType;
use app\modules\salary\models\Bonus;
use app\modules\salary\models\Person;
use app\models\User;

use yii\db\Query;

/**
 * Class m170714_032821_call_center_bonus
 */
class m170714_032821_call_center_bonus extends Migration
{
    const ROLES = [
        'finance.director',
        'callcenter.hr',
        'callcenter.manager',
        'project.manager'
    ];

    const RULES = [
        'callcenter.bonus.index',
        'callcenter.bonus.edit',
        'callcenter.bonus.delete',
        'callcenter.bonustype.index',
        'callcenter.bonustype.activate',
        'callcenter.bonustype.deactivate',
        'callcenter.bonustype.edit',
        'callcenter.bonustype.delete',
        'callcenter.penalty.index',
        'callcenter.penalty.edit',
        'callcenter.penalty.delete',
        'callcenter.penaltytype.index',
        'callcenter.penaltytype.activate',
        'callcenter.penaltytype.deactivate',
        'callcenter.penaltytype.edit',
        'callcenter.penaltytype.delete'
    ];


    const REMOVE_ROLES = [
        'country.curator',
    ];

    const REMOVE_RULES = [
        'callcenter.penaltytype.index',
        'callcenter.penaltytype.activate',
        'callcenter.penaltytype.deactivate',
        'callcenter.penaltytype.edit',
        'callcenter.penaltytype.delete'
    ];


    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        $query = new Query();

        foreach (self::RULES as $rule) {

            $is_rule = $query->select('*')->from($this->authManager->itemTable)
                ->where(['name' => $rule, 'type' => 2])
                ->one();

            if (!$is_rule) {
                $this->insert($this->authManager->itemTable, [
                    'name' => $rule,
                    'description' => $rule,
                    'type' => 2,
                    'created_at' => time(),
                    'updated_at' => time()]);
            }

            foreach (self::ROLES as $role) {
                $is_can = $query->select('*')->from($this->authManager->itemChildTable)->where([
                    'parent' => $role,
                    'child' => $rule
                ])->one();

                if (!$is_can) {
                    $this->insert($this->authManager->itemChildTable, [
                        'parent' => $role,
                        'child' => $rule
                    ]);
                }
            }
        }

        foreach (self::REMOVE_RULES as $rule) {
            foreach (self::REMOVE_ROLES as $role) {
                $this->delete($this->authManager->itemChildTable, [
                    'parent' => $role,
                    'child' => $rule
                ]);
            }
        }


        $this->createTable(BonusType::tableName(), [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'type' => $this->string(),
            'sum' => $this->float(),
            'percent' => $this->integer(),
            'active' => $this->integer(1)->defaultValue(1),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);


        $this->createTable(Bonus::tableName(), [
            'id' => $this->primaryKey(),
            'person_id' => $this->integer()->notNull(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'bonus_type_id' => $this->integer(),
            'bonus_percent' => $this->integer(),
            'bonus_sum_usd' => $this->float(),
            'bonus_sum_local' => $this->float(),
            'comment' => $this->string(255),
            'date' => $this->date(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addForeignKey(null, Bonus::tableName(), 'person_id', Person::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, Bonus::tableName(), 'bonus_type_id', BonusType::tableName(), 'id', self::SET_NULL, self::NO_ACTION);
        $this->addForeignKey(null, Bonus::tableName(), 'created_by', User::tableName(), 'id', self::SET_NULL, self::NO_ACTION);
        $this->addForeignKey(null, Bonus::tableName(), 'updated_by', User::tableName(), 'id', self::SET_NULL, self::NO_ACTION);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach (self::RULES as $rule) {
            foreach (self::ROLES as $role) {
                $this->delete($this->authManager->itemChildTable, [
                    'parent' => $role,
                    'child' => $rule
                ]);
            }
        }


        $tableSchema = Yii::$app->db->schema->getTableSchema('call_center_bonus');
        if ($tableSchema !== null) {
            $this->dropTable('call_center_bonus');
        }


        $tableSchema = Yii::$app->db->schema->getTableSchema('call_center_bonus_type');
        if ($tableSchema !== null) {
            $this->dropTable('call_center_bonus_type');
        }

    }
}
