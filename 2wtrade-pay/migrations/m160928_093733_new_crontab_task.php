<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m160928_093733_new_crontab_task
 */
class m160928_093733_new_crontab_task extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = new CrontabTask();
        $crontabTask->name = 'delivery_send_logistic_lists';
        $crontabTask->description = 'Отправка сгенерированных листов в службу доставки.';
        $crontabTask->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = CrontabTask::find()
            ->byName('delivery_send_logistic_lists')
            ->one();

        if ($crontabTask) {
            $crontabTask->delete();
        }
    }
}
