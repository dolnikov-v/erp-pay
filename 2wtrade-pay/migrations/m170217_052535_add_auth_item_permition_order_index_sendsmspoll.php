<?php
use app\components\CustomMigration as Migration;
use app\models\AuthItem;

/**
 * Class m170217_052535_add_auth_item_permition_order_index_sendsmspoll
 */
class m170217_052535_add_auth_item_permition_order_index_sendsmspoll extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert(AuthItem::tableName(), [
            'name' => 'order.index.sendsmspoll',
            'type' => 2,
            'description' => 'order.index.sendsmspoll',
            'created_at' => time(),
            'updated_at' => time()
        ]);

        $this->insert('i18n_source_message', ['category' => 'common', 'message' => 'Выберите СМС опрос, который вы хотите отправить']);
        $this->insert('i18n_source_message', ['category' => 'common', 'message' => 'Отправить СМС опрос']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete(AuthItem::tableName(), [
            'name' => 'order.index.sendsmspoll',
            'type' => 2
        ]);
    }
}
