<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170829_121000_callcenter_controller
 */
class m170829_121000_callcenter_controller extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}', [
            'name'=> 'callcenter.controller',
            'type' => '1',
            'description' => 'Контролер КЦ',
            'created_at' => time(),
            'updated_at' => time()
        ]);

        $this->insert($this->authManager->itemChildTable, [
            'parent' => 'callcenter.controller',
            'child' => 'home.index.index'
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete('{{%auth_item}}', ['name' => 'callcenter.controller']);
    }
}
