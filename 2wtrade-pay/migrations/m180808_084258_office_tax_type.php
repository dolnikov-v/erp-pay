<?php
use app\components\CustomMigration as Migration;
use app\modules\salary\models\OfficeTax;
use app\modules\salary\models\Person;

/**
 * Class m180808_084258_office_tax_type
 */
class m180808_084258_office_tax_type extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(OfficeTax::tableName(), 'tax_type', $this->string()->after('office_id')->defaultValue(OfficeTax::TAX_TYPE_INCOME));
        $this->addColumn(OfficeTax::tableName(), 'unpaid_amount', $this->float()->after('amount'));

        $this->addColumn(Person::tableName(), 'insurance_coefficient', $this->float()->defaultValue(1));
        $this->addColumn(Person::tableName(), 'pension_contribution', $this->float()->defaultValue(0));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(OfficeTax::tableName(), 'tax_type');
        $this->dropColumn(OfficeTax::tableName(), 'unpaid_amount');

        $this->dropColumn(Person::tableName(), 'insurance_coefficient');
        $this->dropColumn(Person::tableName(), 'pension_contribution');
    }
}
