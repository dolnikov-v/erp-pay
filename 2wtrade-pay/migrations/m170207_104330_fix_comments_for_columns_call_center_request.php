<?php
use app\components\CustomMigration as Migration;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\delivery\models\DeliveryRequest;
/**
 * Class m170207_104330_fix_comments_for_columns_call_center_request
 */
class m170207_104330_fix_comments_for_columns_call_center_request extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addCommentOnColumn(CallCenterRequest::tableName(), 'cc_send_response', 'Last call center response when sending lead');
        $this->addCommentOnColumn(CallCenterRequest::tableName(), 'cc_send_response_at', 'Time call center response when sending lead');
        $this->addCommentOnColumn(CallCenterRequest::tableName(), 'cc_update_response', 'Last call center response when update status');
        $this->addCommentOnColumn(CallCenterRequest::tableName(), 'cc_update_response_at', 'Time call center response when update status');
        $this->addCommentOnColumn(DeliveryRequest::tableName(), 'cs_send_response', 'Last courier service response when sending order');
        $this->addCommentOnColumn(DeliveryRequest::tableName(), 'cs_send_response_at', 'Time courier service response when sending order');

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

    }
}
