<?php
use app\components\CustomMigration as Migration;
use \app\modules\order\models\OrderExport;

/**
 * Class m180830_081421_add_type_zip_from_order_export
 */
class m180830_081421_add_type_zip_from_order_export extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn(OrderExport::tableName(), 'type', $this->enum(['csv', 'excel', 'zip']));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn(OrderExport::tableName(), 'type', $this->enum(['csv', 'excel']));
    }
}
