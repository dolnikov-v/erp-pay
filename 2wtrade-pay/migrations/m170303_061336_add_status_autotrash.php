<?php
use app\components\CustomMigration as Migration;
use app\modules\order\models\OrderStatus;

/**
 * Class m170303_061336_add_status_autotrash
 */
class m170303_061336_add_status_autotrash extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert(OrderStatus::tableName(), ['sort' => 3, 'name' => 'Автотреш заказа', 'comment' => null, 'group' => 'source', 'type' => 'final']);
        $this->insert('i18n_source_message', ['category' => 'common', 'message' => 'Автотреш заказа']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete(OrderStatus::tableName(), ['name' => 'Автотреш заказа']);
    }
}
