<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170804_050630_add_permission_report_expenses
 */
class m170804_110230_add_permission_report_expenses extends Migration
{
    protected $roles = [
        'finance.director',
        'accountant',
    ];

    protected $permissions = [
        'finance.reportexpenses.index',
        'finance.reportexpensescountry.index',
        'finance.reportexpensescountry.edit',
        'finance.reportexpensescountry.delete',
        'finance.reportexpensesitem.index',
        'finance.reportexpensesitem.edit',
        'finance.reportexpensesitem.delete',
        'finance.reportexpensescountryitem.index',
        'finance.reportexpensescountryitem.edit',
        'finance.reportexpensescountryitem.delete',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $authRole = $this->authManager->createRole('accountant');
        $authRole->description = 'Бухгалтер';
        $this->authManager->add($authRole);

        $authPermission = $this->authManager->getPermission('home.index.index');
        if ($this->authManager->canAddChild($authRole, $authPermission)) {
            $this->authManager->addChild($authRole, $authPermission);
        }

        foreach ($this->permissions as $permission) {
            $authPermission = $this->authManager->createPermission($permission);
            $authPermission->description = $permission;
            $this->authManager->add($authPermission);

            foreach ($this->roles as $role) {
                $authRole = $this->authManager->getRole($role);
                if ($this->authManager->canAddChild($authRole, $authPermission)) {
                    $this->authManager->addChild($authRole, $authPermission);
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach ($this->permissions as $permission) {
            $authPermission = $this->authManager->getPermission($permission);
            foreach ($this->roles as $role) {
                $this->authManager->removeChild($this->authManager->getRole($role), $authPermission);
            }
            $this->authManager->remove($authPermission);
        }

        $authRole = $this->authManager->getRole('accountant');
        $this->authManager->removeChild($authRole, $this->authManager->getPermission('home.index.index'));
        $this->authManager->remove($authRole);
    }
}
