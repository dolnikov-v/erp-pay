<?php

use app\components\CustomMigration as Migration;
use app\models\Certificate;
use app\models\CertificateFile;
use app\models\Product;
use app\models\Country;

/**
 * Handles the creation for table `product_certificate`.
 */
class m170120_084221_create_product_certificate extends Migration
{
    const CASCADE = 'CASCADE';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

        $this->createTable(Certificate::tableName(), [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'is_active' => $this->boolean()->defaultValue(1),
            'is_partner' => $this->boolean()->defaultValue(0),
            'description' => $this->text(),
            'product_id' => $this->integer()->notNull(),
            'country_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull()
        ], $tableOptions);


        $this->addForeignKey(null, Certificate::tableName(), 'product_id', Product::tableName(), 'id', self::CASCADE, self::CASCADE);
        $this->addForeignKey(null, Certificate::tableName(), 'country_id', Country::tableName(), 'id', self::CASCADE, self::CASCADE);

        $this->createTable(CertificateFile::tableName(), [
            'id' => $this->primaryKey(),
            'product_certificate_id' => $this->integer()->notNull(),
            'origfilename' => $this->string(255)->notNull(),
            'unicfilename' => $this->string(255)->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull()
        ], $tableOptions);

        $this->addForeignKey(null, CertificateFile::tableName(), 'product_certificate_id', Certificate::tableName(), 'id', self::CASCADE, self::CASCADE);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable(Certificate::tableName());
        $this->dropTable(CertificateFile::tableName());
    }
}
