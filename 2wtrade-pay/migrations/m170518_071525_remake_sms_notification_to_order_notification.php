<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170518_071525_remake_sms_notification_to_order_notification
 */
class m170518_071525_remake_sms_notification_to_order_notification extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropForeignKey(null, 'sms_workflow_status', 'sms_workflow_id');
        $this->dropForeignKey(null, 'sms_workflow_status', 'sms_notification_id');
        $this->dropForeignKey(null, 'country_sms_workflow', 'country_id');
        $this->dropForeignKey(null, 'country_sms_workflow', 'sms_workflow_id');
        $this->dropForeignKey(null, 'order_sms_notification_request', 'order_id');
        $this->dropForeignKey(null, 'order_sms_notification_request', 'order_status_id');
        $this->dropForeignKey(null, 'order_sms_notification_request', 'sms_notification_id');
        $this->dropForeignKey(null, 'order_sms_notification_request_log', 'order_sms_notification_request_id');
        $this->dropForeignKey(null, 'order_sms_notification_request_log', 'user_id');

        $this->dropTable('{{%country_sms_workflow}}');
        $this->dropTable('{{%sms_workflow_status}}');
        $this->dropTable('{{%sms_workflow}}');

        $this->renameTable('{{%sms_notification}}', '{{%order_notification}}');
        $this->renameTable('{{%order_sms_notification_request}}', '{{%order_notification_request}}');
        $this->renameTable('{{%order_sms_notification_request_log}}', '{{%order_notification_request_log}}');

        $this->addColumn('{{%order_notification}}', 'sms_active', $this->smallInteger()->defaultValue(0));
        $this->addColumn('{{%order_notification}}', 'sms_text', $this->string(500));
        $this->addColumn('{{%order_notification}}', 'email_active', $this->smallInteger()->defaultValue(0));
        $this->addColumn('{{%order_notification}}', 'email_text', $this->string(500));
        $this->addColumn('{{%order_notification}}', 'country_id', $this->integer()->notNull());
        $this->addColumn('{{%order_notification}}', 'order_statuses', $this->string(256)->notNull());

        $this->renameColumn('{{%order_notification_request}}', 'sms_notification_id', 'order_notification_id');
        $this->renameColumn('{{%order_notification_request}}', 'status', 'sms_status');
        $this->renameColumn('{{%order_notification_request}}', 'foreign_id', 'sms_foreign_id');
        $this->renameColumn('{{%order_notification_request}}', 'foreign_status', 'sms_foreign_status');
        $this->renameColumn('{{%order_notification_request}}', 'date_created', 'sms_date_created');
        $this->renameColumn('{{%order_notification_request}}', 'date_updated', 'sms_date_updated');
        $this->renameColumn('{{%order_notification_request}}', 'date_sent', 'sms_date_sent');
        $this->renameColumn('{{%order_notification_request}}', 'price', 'sms_price');
        $this->renameColumn('{{%order_notification_request}}', 'uri', 'sms_uri');
        $this->renameColumn('{{%order_notification_request}}', 'api_error', 'sms_api_error');
        $this->renameColumn('{{%order_notification_request}}', 'api_code', 'sms_api_code');
        $this->addColumn('{{%order_notification_request}}', 'email_to', $this->string(100));
        $this->addColumn('{{%order_notification_request}}', 'email_status', $this->smallInteger());

        $this->renameColumn('{{%order_notification_request_log}}', 'order_sms_notification_request_id', 'order_notification_request_id');

        $this->addForeignKey(null, 'order_notification_request', 'order_id', 'order', 'id', self::CASCADE, self::CASCADE);
        $this->addForeignKey(null, 'order_notification_request', 'order_status_id', 'order_status', 'id', self::CASCADE, self::NO_ACTION);

        $this->addForeignKey(null, 'order_notification_request', 'order_notification_id', 'order_notification', 'id', self::CASCADE, self::NO_ACTION);
        $this->addForeignKey(null, 'order_notification_request_log', 'order_notification_request_id', 'order_notification_request', 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, 'order_notification_request_log', 'user_id', 'user', 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey(null, 'order_notification_request', 'order_id');
        $this->dropForeignKey(null, 'order_notification_request', 'order_status_id');
        $this->dropForeignKey(null, 'order_notification_request', 'order_notification_id');
        $this->dropForeignKey(null, 'order_notification_request_log', 'order_notification_request_id');
        $this->dropForeignKey(null, 'order_notification_request_log', 'user_id');

        $this->dropColumn('{{%order_notification}}', 'sms_active');
        $this->dropColumn('{{%order_notification}}', 'sms_text');
        $this->dropColumn('{{%order_notification}}', 'email_active');
        $this->dropColumn('{{%order_notification}}', 'email_text');
        $this->dropColumn('{{%order_notification}}', 'country_id');
        $this->dropColumn('{{%order_notification}}', 'order_statuses');

        $this->dropColumn('{{%order_notification_request}}', 'email_to');
        $this->dropColumn('{{%order_notification_request}}', 'email_status');

        $this->renameTable('{{%order_notification}}', '{{%sms_notification}}');
        $this->renameTable('{{%order_notification_request}}', '{{%order_sms_notification_request}}');
        $this->renameTable('{{%order_notification_request_log}}', '{{%order_sms_notification_request_log}}');

        $this->renameColumn('{{%order_sms_notification_request}}', 'order_notification_id', 'sms_notification_id');
        $this->renameColumn('{{%order_sms_notification_request}}', 'sms_status', 'status');
        $this->renameColumn('{{%order_sms_notification_request}}', 'sms_foreign_id', 'foreign_id');
        $this->renameColumn('{{%order_sms_notification_request}}', 'sms_foreign_status', 'foreign_status');
        $this->renameColumn('{{%order_sms_notification_request}}', 'sms_date_created', 'date_created');
        $this->renameColumn('{{%order_sms_notification_request}}', 'sms_date_updated', 'date_updated');
        $this->renameColumn('{{%order_sms_notification_request}}', 'sms_date_sent', 'date_sent');
        $this->renameColumn('{{%order_sms_notification_request}}', 'sms_price', 'price');
        $this->renameColumn('{{%order_sms_notification_request}}', 'sms_uri', 'uri');
        $this->renameColumn('{{%order_sms_notification_request}}', 'sms_api_error', 'api_error');
        $this->renameColumn('{{%order_sms_notification_request}}', 'sms_api_code', 'api_code');

        $this->renameColumn('{{%order_sms_notification_request_log}}', 'order_notification_request_id', 'order_sms_notification_request_id');

        $this->createTable('{{%sms_workflow}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'comment' => $this->text(),
            'active' => $this->smallInteger()->defaultValue(0),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ], $this->tableOptions);

        $this->createTable('{{%sms_workflow_status}}', [
            'id' => $this->primaryKey(),
            'sms_workflow_id' => $this->integer()->notNull(),
            'sms_notification_id' => $this->integer()->notNull(),
            'status_ids' => $this->string(100),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ], $this->tableOptions);

        $this->createTable('{{%country_sms_workflow}}', [
            'id' => $this->primaryKey(),
            'country_id' => $this->integer()->notNull(),
            'sms_workflow_id' => $this->integer()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ], $this->tableOptions);

        $this->addForeignKey(null, 'sms_workflow_status', 'sms_workflow_id', 'sms_workflow', 'id', self::CASCADE, self::CASCADE);
        $this->addForeignKey(null, 'sms_workflow_status', 'sms_notification_id', 'sms_notification', 'id', self::CASCADE, self::CASCADE);
        $this->addForeignKey(null, 'country_sms_workflow', 'country_id', 'country', 'id', self::CASCADE, self::CASCADE);
        $this->addForeignKey(null, 'country_sms_workflow', 'sms_workflow_id', 'sms_workflow', 'id', self::CASCADE, self::CASCADE);
        $this->addForeignKey(null, 'order_sms_notification_request', 'order_id', 'order', 'id', self::CASCADE, self::CASCADE);
        $this->addForeignKey(null, 'order_sms_notification_request', 'order_status_id', 'order_status', 'id', self::CASCADE, self::NO_ACTION);
        $this->addForeignKey(null, 'order_sms_notification_request', 'sms_notification_id', 'sms_notification', 'id', self::CASCADE, self::NO_ACTION);
        $this->addForeignKey(null, 'order_sms_notification_request_log', 'order_sms_notification_request_id', 'order_sms_notification_request', 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, 'order_sms_notification_request_log', 'user_id', 'user', 'id', self::CASCADE, self::RESTRICT);
    }
}
