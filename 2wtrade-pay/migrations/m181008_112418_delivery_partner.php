<?php

use app\components\CustomMigration as Migration;

/**
 * Class m181008_112418_delivery_partner
 */
class m181008_112418_delivery_partner extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('delivery_partner', [
            'id' => $this->primaryKey(),
            'delivery_id' => $this->integer(),
            'name' => $this->string(100),
            'created_at' => $this->integer()
        ]);

        $this->addForeignKey(null, 'delivery_partner', 'delivery_id', 'delivery', 'id', self::CASCADE, self::CASCADE);

        $this->addColumn('delivery_request', 'delivery_partner_id', $this->integer()->after('partner_name'));

        $this->addForeignKey($this->getFkName('delivery_request', 'delivery_partner_id'), 'delivery_request', 'delivery_partner_id', 'delivery_partner', 'id', self::SET_NULL, self::NO_ACTION);

        $query = new \yii\db\Query();
        $query->from('delivery_request');
        $query->where(['is not', 'partner_name', null]);
        $query->andWhere(['<>', 'partner_name', '']);
        $query->select(['delivery_id', 'partner_name']);
        $query->groupBy('partner_name');

        foreach ($query->batch(1000) as $rows) {
            $insert = [];
            foreach ($rows as $row) {
                $insert[] = [
                    'delivery_id' => $row['delivery_id'],
                    'name' => $row['partner_name'],
                    'created_at' => time()
                ];
            }
            if ($insert) {
                $this->batchInsert('delivery_partner', ['delivery_id', 'name', 'created_at'], $insert);
            }
        }

        $queryPartner = new \yii\db\Query();
        $queryPartner->from('delivery_partner');
        $queryPartner->select(['id', 'delivery_id', 'name']);
        foreach ($queryPartner->all() as $partnerRow) {
            $this->update('delivery_request', [
                'delivery_partner_id' => $partnerRow['id']
            ], [
                'delivery_id' => $partnerRow['delivery_id'],
                'partner_name' => $partnerRow['name']
            ]);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey($this->getFkName('delivery_request', 'delivery_partner_id'), 'delivery_request');
        $this->dropColumn('delivery_request', 'delivery_partner_id');
        $this->dropTable('delivery_partner');
    }
}
