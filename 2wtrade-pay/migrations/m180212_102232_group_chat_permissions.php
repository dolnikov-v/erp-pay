<?php
use app\components\PermissionMigration;

/**
 * Class m180212_102232_group_chat_permissions
 */
class m180212_102232_group_chat_permissions extends PermissionMigration
{
    protected $permissions = [
        'notification.chatgroup.index',
        'notification.chatgroup.edit',
        'notification.chatgroup.delete',
    ];

    protected $roles = [
        'admin' => [
            'notification.chatgroup.index',
            'notification.chatgroup.edit',
            'notification.chatgroup.delete',
        ]
    ];
}
