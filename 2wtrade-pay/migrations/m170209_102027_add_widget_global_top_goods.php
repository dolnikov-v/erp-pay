<?php
use app\components\CustomMigration as Migration;
use app\modules\widget\models\WidgetType;
use app\modules\widget\models\WidgetRole;
/**
 * Class m170209_102027_add_widget_global_top_goods
 */
class m170209_102027_add_widget_global_top_goods extends Migration
{
    /**
     * cоздание записи о виджете, добавление роли пользователя к виджету
     */
    public function safeUp()
    {
        $this->insert(WidgetType::tableName(), [
            'code' => 'global_top_goods',
            'name' => 'Глобальный топ товаров',
            'status' => WidgetType::STATUS_ACTIVE
        ]);

        $type = WidgetType::find()
            ->where(['code' => 'global_top_goods'])
            ->one();

        $this->insert(WidgetRole::tableName(), [
            'role' => 'business_analyst',
            'type_id' => $type->id
        ]);

        $this->insert(WidgetRole::tableName(), [
            'role' => 'country.curator',
            'type_id' => $type->id
        ]);
    }


    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $type = WidgetType::find()
            ->where(['code' => 'global_top_goods'])
            ->one();

        $this->delete(WidgetRole::tableName(), [
            'role' => 'business_analyst',
            'type_id' => $type->id,
        ]);

        $this->delete(WidgetRole::tableName(), [
            'role' => 'country.curator',
            'type_id' => $type->id,
        ]);

        $this->delete(WidgetType::tableName(), ['code' => 'global_top_goods']);
    }
}
