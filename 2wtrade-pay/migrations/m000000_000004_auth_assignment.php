<?php
use app\components\CustomMigration as Migration;

class m000000_000004_auth_assignment extends Migration
{
    public function up()
    {
        $this->createTable($this->authManager->assignmentTable, [
            'item_name' => $this->string(64)->notNull(),
            'user_id' => $this->string(64)->notNull(),
            'created_at' => $this->integer(),
            $this->includePrimaryKey(['item_name', 'user_id']),
        ], $this->tableOptions);

        $this->addForeignKey(null, $this->authManager->assignmentTable, 'item_name', $this->authManager->itemTable, 'name', self::CASCADE, self::CASCADE);
    }

    public function down()
    {
        $this->dropTable($this->authManager->assignmentTable);
    }
}
