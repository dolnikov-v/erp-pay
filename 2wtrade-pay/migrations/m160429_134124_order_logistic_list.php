<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160429_134124_order_logistic_list
 */
class m160429_134124_order_logistic_list extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('order_logistic_list', [
            'id' => $this->primaryKey(),
            'country_id' => $this->integer()->notNull(),
            'number' => $this->integer()->notNull(),
            'ids' => $this->text()->notNull(),
            'ticket' => $this->string(100)->defaultValue(null),
            'list' => $this->string(100)->defaultValue(null),
            'invoice' => $this->string(100)->defaultValue(null),
            'user_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(null, 'order_logistic_list', 'country_id', 'country', 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, 'order_logistic_list', 'user_id', 'user', 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('order_logistic_list');
    }
}
