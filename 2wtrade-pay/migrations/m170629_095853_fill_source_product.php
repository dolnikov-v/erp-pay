<?php
use app\components\CustomMigration as Migration;
use app\models\Product;
use app\models\Source;
use app\models\SourceProduct;

/**
 * Class m170629_095853_fill_source_product
 */
class m170629_095853_fill_source_product extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $products = Product::find()->all();
        /**
         * @var $sourcesOld Source[]
         */
        $sourcesOld = Source::find()->where(['IN', 'name', ['adcombo', 'jeempo', '2wstore']])->all();
        $diavitaSourceId= Source::find()->select('id')->where(['IN', 'name', ['diavita']])->scalar();
        foreach ($sourcesOld as $source) {
            foreach ($products as $product) {
                if ($product->id < 86) {
                    $sourceProduct = new SourceProduct();
                    $sourceProduct->source_id = $source->id;
                    $sourceProduct->product_id = $product->id;
                    $sourceProduct->save();
                }
            }
        }

        foreach ($products as $product) {
            if (($product->id > 86) && ($product->id < 91)) {
                $sourceProduct = new SourceProduct();
                $sourceProduct->source_id = $diavitaSourceId;
                $sourceProduct->product_id = $product->id;
                $sourceProduct->save();
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->truncateTable(SourceProduct::tableName());
    }
}
