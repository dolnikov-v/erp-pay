<?php

use app\components\CustomMigration as Migration;

/**
 * Handles the creation for table `qr_list_table`.
 */
class m161130_070157_create_qr_list_table extends Migration
{
    /**
     * @inheritdoc
     */

    public function safeUp()
    {
        $this->createTable('qr_list', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(4),
            'file' => $this->string(100)->notNull(),
            'created_at' => $this->integer(11)->notNull(),
        ], $this->tableOptions);

        $this->addForeignKey('fk_user_qr_list_id', 'qr_list', 'user_id', 'user', 'id',  self::NO_ACTION, self::NO_ACTION);
    }
    
    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_user_qr_list_id', 'qr_list');
        $this->dropTable('qr_list');
    }
}
