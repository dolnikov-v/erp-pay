<?php

use app\modules\administration\models\CrontabTask;
use yii\db\Migration;

/**
 * Handles adding cron_for_trying to table `save_orders`.
 */
class m170830_115627_add_cron_for_trying_to_save_orders extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_DELIVERY_SAVE_TRANSFERRED_ORDERS]);
        if (!$crontabTask) {
            $crontabTask = new CrontabTask();
            $crontabTask->name = CrontabTask::TASK_DELIVERY_SAVE_TRANSFERRED_ORDERS;
            $crontabTask->description = "Попытка сохранения заказов, которые не удалось сохранить при отправке в КС";
            $crontabTask->save();
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_DELIVERY_SAVE_TRANSFERRED_ORDERS]);
        if ($crontabTask instanceof CrontabTask) {
            $crontabTask->delete();
        }
    }
}
