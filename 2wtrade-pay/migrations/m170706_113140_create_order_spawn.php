<?php

use app\models\OrderCreationDate;
use app\modules\order\models\Order;
use app\components\CustomMigration as Migration;

/**
 * Handles the creation for table `order_spawn`.
 */
class m170706_113140_create_order_spawn extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable(OrderCreationDate::tableName(), [
            'order_id' => $this->primaryKey(),
            'foreign_create_at' => $this->integer()->notNull()
        ]);
        $this->addForeignKey(null, OrderCreationDate::tableName(), 'order_id', Order::tableName(), 'id', Migration::CASCADE, Migration::CASCADE);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('order_spawn');
    }
}
