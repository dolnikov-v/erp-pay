<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160718_064734_update_enum_to_call_center_request
 */
class m160718_064734_update_enum_to_call_center_request extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('call_center_request', 'status', 'ENUM ("pending", "in_progress", "done", "error") DEFAULT "pending"');
        $this->addColumn('call_center_request', 'api_error', $this->string()->after('status'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('call_center_request', 'api_error'); 
        $this->alterColumn('call_center_request', 'status', 'ENUM ("in_progress", "done") DEFAULT "in_progress"');
    }
}
