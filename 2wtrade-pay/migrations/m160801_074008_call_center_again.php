<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160801_074008_call_center_again
 */
class m160801_074008_call_center_again extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('order', 'call_center_again', $this->smallInteger()->defaultValue(0)->after('adcombo_confirmed'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('order', 'call_center_again');
    }
}
