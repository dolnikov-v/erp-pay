<?php
use app\components\CustomMigration as Migration;
use app\modules\salary\models\Office;

/**
 * Class m170712_101901_min_salary
 */
class m170712_101901_min_salary extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Office::tableName(), 'min_salary_local', $this->float());
        $this->addColumn(Office::tableName(), 'min_salary_usd', $this->float());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Office::tableName(), 'min_salary_local');
        $this->dropColumn(Office::tableName(), 'min_salary_usd');
    }
}
