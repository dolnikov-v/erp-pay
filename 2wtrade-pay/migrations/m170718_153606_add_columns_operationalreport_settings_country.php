<?php

use yii\db\Migration;

/**
 * Class m170718_153606_add_columns_operationalreport_settings_country
 */
class m170718_153606_add_columns_operationalreport_settings_country extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->truncateTable('report_operational_settings_countries');
        $this->dropColumn('report_operational_settings_countries', 'status');
        $this->addColumn('report_operational_settings_countries', 'status', 'TINYINT(1) DEFAULT 1 AFTER country_name');
        $this->addColumn('report_operational_settings_countries', 'sms_status', 'TINYINT(1) DEFAULT 1 AFTER status');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('report_operational_settings_countries', 'sms_status');
    }
}
