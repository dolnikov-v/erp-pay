<?php

use app\components\CustomMigration as Migration;
use app\modules\deliveryreport\models\DeliveryReport;

/**
 * Handles adding column to table `delivery_report`.
 */
class m170407_083000_add_column_to_delivery_report_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn(DeliveryReport::tableName(), 'sum_total', $this->integer()->defaultValue(NULL)->after('payment_id'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn(DeliveryReport::tableName(), 'sum_total');
    }
}
