<?php
use app\components\CustomMigration as Migration;
use yii\db\Query;

/**
 * Class m181205_055607_cron_actualize_opers_directions
 */
class m181205_055607_cron_actualize_opers_directions extends Migration
{
    protected $crons = [
        'actualize_opers_directions' => 'Актуализация направлений у операторов',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        foreach ($this->crons as $name => $description) {
            $cronTaskId = (new Query())->select('id')
                ->from('crontab_task')
                ->where(['name' => $name])
                ->scalar();

            $data = [
                'name' => $name,
                'description' => $description,
                'updated_at' => time(),
            ];

            if (!$cronTaskId) {
                $data['created_at'] = time();
                $this->insert('crontab_task', $data);
            } else {
                $this->update('crontab_task', $data, ['id' => $cronTaskId]);
            }
        }

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach ($this->crons as $name => $description) {
            $this->delete('crontab_task', ['name' => $name]);
        }
    }
}
