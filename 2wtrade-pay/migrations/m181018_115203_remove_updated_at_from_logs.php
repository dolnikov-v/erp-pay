<?php
use app\components\CustomMigration as Migration;

/**
 * Class m181018_115203_remove_updated_at_from_logs
 */
class m181018_115203_remove_updated_at_from_logs extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropColumn('call_center_request_log', 'updated_at');
        $this->dropColumn('delivery_request_log', 'updated_at');
        $this->dropColumn('order_log', 'updated_at');
        $this->dropColumn('order_log_product', 'updated_at');
        $this->dropColumn('order_log_status', 'updated_at');
        $this->dropColumn('order_notification_request_log', 'updated_at');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->addColumn('call_center_request_log', 'updated_at', $this->integer()->notNull());
        $this->addColumn('delivery_request_log', 'updated_at', $this->integer()->notNull());
        $this->addColumn('order_log', 'updated_at', $this->integer());
        $this->addColumn('order_log_product', 'updated_at', $this->integer()->notNull());
        $this->addColumn('order_log_status', 'updated_at', $this->integer());
        $this->addColumn('order_notification_request_log', 'updated_at', $this->integer()->notNull());
    }
}
