<?php
use app\components\CustomMigration as Migration;
use app\models\Country;
use app\models\Timezone;
use app\models\Currency;

/**
 * Class m170623_083114_add_country_guatemala
 */
class m170623_083114_add_country_guatemala extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $params = [
            'num_code' => 320,
            'char_code' => 'GTQ'
        ];
        $currency = Currency::findOne($params) ?: new Currency($params);
        if ($currency->isNewRecord) {
            $currency->name = 'Гватемальский кетсаль';
            if (!$currency->save()) {
                return false;
            }
        }

        $params = [
            'name' => 'Гватемала',
            'char_code' => 'GT'
        ];
        if (!Country::find()->where($params)->exists()) {
            $timezone = Timezone::findOne(['timezone_id' => 'America/Guatemala']);
            $country = new Country($params);
            $country->slug = 'gt';
            $country->timezone_id = $timezone->id;
            $country->currency_id = $currency->id;
            if (!$country->save()) {
                return false;
            }
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $country = Country::findOne([
            'name' => 'Гватемала',
            'char_code' => 'GT'
        ]);
        $country && $country->delete();
    }
}
