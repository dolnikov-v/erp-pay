<?php
use \app\components\PermissionMigration;
use app\modules\salary\models\MonthlyStatementData;

/**
 * Class m180411_043541_salary_person_view
 */
class m180411_043541_salary_person_view extends PermissionMigration
{
    protected $permissions = [
        'salary.person.details',
        'media.salaryperson.sheetview',
        'media.salaryperson.sheetdownload',
    ];

    protected $roles = [
        'accountant' => [
            'salary.person.details',
            'media.salaryperson.sheetview',
            'media.salaryperson.sheetdownload',
        ],
        'admin' => [
            'salary.person.details',
            'media.salaryperson.sheetview',
            'media.salaryperson.sheetdownload',
        ],
        'auditor' => [
            'salary.person.details',
            'media.salaryperson.sheetview',
            'media.salaryperson.sheetdownload',
        ],
        'callcenter.hr' => [
            'salary.person.details',
            'media.salaryperson.sheetview',
            'media.salaryperson.sheetdownload',
        ],
        'callcenter.leader' => [
            'salary.person.details',
            'media.salaryperson.sheetview',
            'media.salaryperson.sheetdownload',
        ],
        'callcenter.manager' => [
            'salary.person.details',
            'media.salaryperson.sheetview',
            'media.salaryperson.sheetdownload',
        ],
        'callcenter.teamlead' => [
            'salary.person.details',
            'media.salaryperson.sheetview',
            'media.salaryperson.sheetdownload',
        ],
        'controller.analyst' => [
            'salary.person.details',
            'media.salaryperson.sheetview',
            'media.salaryperson.sheetdownload',
        ],
        'country.curator' => [
            'salary.person.details',
            'media.salaryperson.sheetview',
            'media.salaryperson.sheetdownload',
        ],
        'finance.director' => [
            'salary.person.details',
            'media.salaryperson.sheetview',
            'media.salaryperson.sheetdownload',
        ],
        'project.manager' => [
            'salary.person.details',
            'media.salaryperson.sheetview',
            'media.salaryperson.sheetdownload',
        ],
        'quality.manager' => [
            'salary.person.details',
            'media.salaryperson.sheetview',
            'media.salaryperson.sheetdownload',
        ],
        'salaryproject.clerk' => [
            'salary.person.details',
            'media.salaryperson.sheetview',
            'media.salaryperson.sheetdownload',
        ],
        'sales.director' => [
            'salary.person.details',
            'media.salaryperson.sheetview',
            'media.salaryperson.sheetdownload',
        ],
        'storage.manager' => [
            'salary.person.details',
            'media.salaryperson.sheetview',
            'media.salaryperson.sheetdownload',
        ],
        'support.manager' => [
            'salary.person.details',
            'media.salaryperson.sheetview',
            'media.salaryperson.sheetdownload',
        ],
        'technical.director' => [
            'salary.person.details',
            'media.salaryperson.sheetview',
            'media.salaryperson.sheetdownload',
        ],
    ];


    private static $translations = [
        "Открыть карточку" => "Open details",
        "Карточка сотрудника {name}" => "Employee's details {name}",
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        foreach (self::$translations as $ruText => $enText) {
            $this->insert('i18n_source_message', ['category' => 'common', 'message' => $ruText]);
            $id = Yii::$app->db->getLastInsertID();
            $this->insert('i18n_message', ['id' => $id, 'language' => 'en-US', 'translation' => $enText]);
        }

        $this->createIndex($this->getIdxName(MonthlyStatementData::tableName(), ['salary_sheet_file']), MonthlyStatementData::tableName(), 'salary_sheet_file');

        parent::safeUp();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach (self::$translations as $ruText => $enText) {
            $this->delete('i18n_source_message', ['category' => 'common', 'message' => $ruText]);
        }

        $this->dropIndex($this->getIdxName(MonthlyStatementData::tableName(), ['salary_sheet_file']), MonthlyStatementData::tableName());

        parent::safeDown();
    }
}
