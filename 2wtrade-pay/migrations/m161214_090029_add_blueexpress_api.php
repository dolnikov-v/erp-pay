<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryApiClass;

/**
 * Class m161214_090029_add_blueexpress_api
 */
class m161214_090029_add_blueexpress_api extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $class = DeliveryApiClass::find()->where([
            'name' => 'BlueExpressApi',
            'class_path' => '/blue-express-api/src/BlueExpressApi.php'
        ])->one();
        $class->name = 'BlueExpressApi';
        $class->class_path = '/blueexpress-api/src/BlueExpressApi.php';
        $class->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        DeliveryApiClass::find()
            ->where([
                'name' => 'BlueExpressApi',
                'class_path' => '/blueexpress-api/src/BlueExpressApi.php'
            ])
            ->one()
            ->delete();
    }
}
