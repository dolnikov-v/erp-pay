<?php
use app\components\CustomMigration as Migration;

/**
 * Class m180629_115740_add_landing_2wtradecom
 */
class m180629_115740_add_landing_2wtradecom extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $now = time();
        yii::$app->db->createCommand("insert into landing (url, created_at, updated_at) values ('http://2wtrade.com', {$now}, {$now})")->execute();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        yii::$app->db->createCommand("delete from landing where url ='http://2wtrade.com'")->execute();
    }
}
