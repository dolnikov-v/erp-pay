<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160714_150537_fix_logs
 */
class m160714_150537_fix_logs extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        Yii::$app->db->createCommand()->checkIntegrity(false)->execute();
        $this->truncateTable('call_center_request_log');
        $this->truncateTable('delivery_request_log');
        Yii::$app->db->createCommand()->checkIntegrity(true)->execute();

        $this->dropForeignKey($this->getFkName('call_center_request_log', 'order_id'), 'call_center_request_log');
        $this->dropForeignKey($this->getFkName('delivery_request_log', 'order_id'), 'delivery_request_log');
        $this->dropColumn('call_center_request_log', 'order_id');
        $this->dropColumn('delivery_request_log', 'order_id');

        $this->addColumn('call_center_request_log', 'call_center_request_id', $this->integer()->notNull()->after('id'));
        $this->addColumn('delivery_request_log', 'delivery_request_id', $this->integer()->notNull()->after('id'));
        $this->addForeignKey(null, 'call_center_request_log', 'call_center_request_id', 'call_center_request', 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, 'delivery_request_log', 'delivery_request_id', 'delivery_request', 'id', self::CASCADE, self::RESTRICT);
        $this->alterColumn('call_center_request_log', 'group_id', $this->string()->notNull());
        $this->alterColumn('delivery_request_log', 'group_id', $this->string()->notNull());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('call_center_request_log', 'group_id', $this->integer()->notNull());
        $this->alterColumn('delivery_request_log', 'group_id', $this->integer()->notNull());
        $this->dropForeignKey($this->getFkName('delivery_request_log', 'delivery_request_id'), 'delivery_request_log');
        $this->dropForeignKey($this->getFkName('call_center_request_log', 'call_center_request_id'), 'call_center_request_log');
        $this->dropColumn('delivery_request_log', 'delivery_request_id');
        $this->dropColumn('call_center_request_log', 'call_center_request_id');

        $this->addColumn('delivery_request_log', 'order_id', $this->integer()->notNull()->after('id'));
        $this->addColumn('call_center_request_log', 'order_id', $this->integer()->notNull()->after('id'));
        $this->addForeignKey(null, 'delivery_request_log', 'order_id', 'order', 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, 'call_center_request_log', 'order_id', 'order', 'id', self::CASCADE, self::RESTRICT);
    }
}
