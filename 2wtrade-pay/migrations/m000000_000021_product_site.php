<?php

use app\components\CustomMigration as Migration;
use app\models\Product;
use app\models\ProductLanding;

class m000000_000021_product_site extends Migration
{
    public function up()
    {
        $this->createTable('product_site', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer()->notNull(),
            'url' => $this->string(200)->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $this->tableOptions);

        $this->addForeignKey(null, 'product_site', 'product_id', 'product', 'id', self::NO_ACTION, self::NO_ACTION);
    }

    public function down()
    {
        $this->dropTable('product_site');
    }
}
