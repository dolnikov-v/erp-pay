<?php
use app\components\CustomMigration as Migration;
use app\models\Currency;

/**
 * Class m160422_071618_currencies
 */
class m160422_071618_currencies extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        foreach ($this->currenciesFixture() as $fixture) {
            $currency = new Currency($fixture);
            $currency->save();
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

    }

    /**
     * @return array
     */
    private function currenciesFixture()
    {
        return [
            [
                'name' => 'Доллар США',
                'num_code' => 840,
                'char_code' => 'USD',
            ],
            [
                'name' => 'Индийская рупия',
                'num_code' => 356,
                'char_code' => 'INR',
            ],
            [
                'name' => 'Тайский бат',
                'num_code' => 764,
                'char_code' => 'THB',
            ],
            [
                'name' => 'Мексиканский песо',
                'num_code' => 484,
                'char_code' => 'MXN',
            ],
            [
                'name' => 'Чилийское песо',
                'num_code' => 152,
                'char_code' => 'CLP',
            ],
            [
                'name' => 'Колумбийский песо',
                'num_code' => 170,
                'char_code' => 'COP',
            ],
            [
                'name' => 'Пакистанская рупия',
                'num_code' => 586,
                'char_code' => 'PKR',
            ],
            [
                'name' => 'Евро',
                'num_code' => 978,
                'char_code' => 'EUR',
            ],
            [
                'name' => 'Камбоджийский риель',
                'num_code' => 116,
                'char_code' => 'KHR',
            ],
            [
                'name' => 'Индонезийская рупия',
                'num_code' => 360,
                'char_code' => 'IDR',
            ],
            [
                'name' => 'Японская йена',
                'num_code' => 392,
                'char_code' => 'JPY',
            ],
            [
                'name' => 'Турецкая лира',
                'num_code' => 949,
                'char_code' => 'TRY',
            ],
            [
                'name' => 'Перуанский соль',
                'num_code' => 604,
                'char_code' => 'PEN',
            ],
        ];
    }
}
