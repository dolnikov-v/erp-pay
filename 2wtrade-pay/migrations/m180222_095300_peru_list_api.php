<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryApiClass;

/**
 * Class m180222_095300_peru_list_api
 */
class m180222_095300_peru_list_api extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $class = new DeliveryApiClass();
        $class->name = 'Peru-List';
        $class->class_path = '/peru-list-api/src/peruListApi.php';
        $class->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        DeliveryApiClass::find()
            ->where([
                'name' => 'Peru-List',
            ])
            ->one()
            ->delete();
    }
}
