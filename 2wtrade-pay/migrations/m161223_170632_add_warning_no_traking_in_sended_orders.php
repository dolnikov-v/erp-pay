<?php

use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;
use app\models\Notification;

/**
 * Add new warning about orders sended to delivery without tracking
 */
class m161223_170632_add_warning_no_traking_in_sended_orders extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
		$crontabTask = new CrontabTask();
        $crontabTask->name = 'report_no_track_sent_orders';
        $crontabTask->description = 'Ежедневные уведомления о заказах отправленных в курьерку без трекера (за 31 день)';
        $crontabTask->save();

        $model = new Notification([
            'description' => 'Найдено заказов отправленных в курьерку без трекера: {count}',
            'trigger' => Notification::TRIGGER_REPORT_NO_TRACK_SENT_ORDERS,
            'type' => Notification::TYPE_WARNING,
            'group' => Notification::GROUP_REPORT,
            'active' => Notification::ACTIVE,
        ]);

        $model->save(false);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
		$crontabTask = CrontabTask::find()
            ->byName('report_no_track_sent_orders')
            ->one();

        if ($crontabTask instanceof CrontabTask) {
            $crontabTask->delete();
        }

        $model = Notification::findOne(['trigger' => Notification::TRIGGER_REPORT_NO_TRACK_SENT_ORDERS]);

        if ($model instanceof Notification) {
            $model->delete();
        }
    }
}
