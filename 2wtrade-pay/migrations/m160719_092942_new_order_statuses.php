<?php
use app\components\CustomMigration as Migration;
use app\modules\order\models\OrderStatus;

/**
 * Class m160719_092942_new_order_statuses
 */
class m160719_092942_new_order_statuses extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        foreach ($this->getFixture() as $data) {
            $status = new OrderStatus($data);
            $status->save();
        }

        /** @var OrderStatus $status */
        $status = OrderStatus::findOne(OrderStatus::STATUS_DELIVERY_REJECTED);
        $status->type = OrderStatus::TYPE_MIDDLE;
        $status->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        /** @var OrderStatus $status */
        $status = OrderStatus::findOne(OrderStatus::STATUS_DELIVERY_REJECTED);
        $status->type = OrderStatus::TYPE_FINAL;
        $status->save();

        foreach ($this->getFixture() as $data) {
            /** @var OrderStatus $status */
            $status = OrderStatus::findOne($data['id']);

            if ($status) {
                $status->delete();
            }
        }
    }

    /**
     * @return array
     */
    private function getFixture()
    {
        return [
            [
                'id' => 31,
                'sort' => 1,
                'name' => 'Курьерка (отправлен на доставку)',
                'group' => OrderStatus::GROUP_DELIVERY,
                'type' => OrderStatus::TYPE_MIDDLE,
            ],
        ];
    }
}
