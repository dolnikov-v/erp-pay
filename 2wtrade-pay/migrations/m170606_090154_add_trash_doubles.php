<?php
use app\components\CustomMigration as Migration;
use app\models\Country;
use yii\db\Query;

/**
 * Class m170606_090154_add_trash_doubles
 */
class m170606_090154_add_trash_doubles extends Migration
{

    const ROLE = 'country.curator';

    const ROLE_NAME = 'Куратор по стране';

    const RULES = [
        'catalog.autotrash.index',
        'catalog.autotrash.edit',
        'catalog.autotrash.delete',
        'catalog.autotrash.autotrash-switch',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Country::tableName(), 'trash_doubles_enabled', $this->smallInteger()->defaultValue(null));

        $query = new Query();

        $is_role = $query->select('*')->from($this->authManager->itemTable)
            ->where(['name' => self::ROLE, 'type' => 1])
            ->one();

        if (!$is_role) {
            $this->insert($this->authManager->itemTable, [
                'name' => self::ROLE,
                'type' => 1,
                'description' => self::ROLE_NAME,
                'created_at' => time(),
                'updated_at' => time()]);
        }

        foreach (self::RULES as $rule) {

            $is_rule = $query->select('*')->from($this->authManager->itemTable)
                ->where(['name' => $rule, 'type' => 2])
                ->one();

            if (!$is_rule) {
                $this->insert($this->authManager->itemTable, [
                    'name' => $rule,
                    'description' => $rule,
                    'type' => 2,
                    'created_at' => time(),
                    'updated_at' => time()]);
            }

            $is_can = $query->select('*')->from($this->authManager->itemChildTable)->where([
                'parent' => self::ROLE,
                'child' => $rule
            ])->one();

            if (!$is_can) {
                $this->insert($this->authManager->itemChildTable, [
                    'parent' => self::ROLE,
                    'child' => $rule
                ]);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Country::tableName(), 'trash_doubles_enabled');

        foreach (self::RULES as $rule) {
            $this->delete($this->authManager->itemChildTable, [
                'parent' => self::ROLE,
                'child' => $rule
            ]);
        }
    }
}
