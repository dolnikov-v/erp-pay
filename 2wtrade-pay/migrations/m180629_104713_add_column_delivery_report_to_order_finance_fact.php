<?php

use app\modules\deliveryreport\models\DeliveryReport;
use app\modules\deliveryreport\models\DeliveryReportRecord;
use app\modules\order\models\OrderFinanceFact;
use app\components\CustomMigration as Migration;

/**
 * Handles adding column_delivery_report to table `order_finance_fact`.
 */
class m180629_104713_add_column_delivery_report_to_order_finance_fact extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn(OrderFinanceFact::tableName(), 'delivery_report_id', $this->integer());
        $this->addColumn(OrderFinanceFact::tableName(), 'delivery_report_record_id', $this->integer());

        $this->addForeignKey(
            null,
            OrderFinanceFact::tableName(),
            'delivery_report_id',
            DeliveryReport::tableName(),
            'id',
            self::CASCADE,
            self::RESTRICT);

        $this->addForeignKey(
            null,
            OrderFinanceFact::tableName(),
            'delivery_report_record_id',
            DeliveryReportRecord::tableName(),
            'id',
            self::CASCADE,
            self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn(OrderFinanceFact::tableName(), 'delivery_report_id');
        $this->dropColumn(OrderFinanceFact::tableName(), 'delivery_report_report_id');
    }
}
