<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170414_092102_fill_order_column_customer_address_additional
 */
class m170414_092102_fill_order_column_customer_address_additional extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        $field = '`customer_address_add`';
        $search = '"customer_address_add":"';
        $searchLength = strlen($search);

        $startPosition = 'LOCATE(\':search\', :field) + :searchLength';
        $endPosition = 'LOCATE(\'"\', :field, :startPosition)';
        $substring = 'SUBSTRING(:field, :startPosition, :endPosition - (:startPosition))';
        $substring = str_replace(
            [':endPosition', ':startPosition', ':searchLength', ':search', ':field'],
            [$endPosition, $startPosition, $searchLength, $search, $field],
            $substring
        );

        $pattern = '\'"customer_address_add":"[^".]+"\'';

        $this->db
            ->createCommand('UPDATE `order` SET `customer_address_additional`=' . $substring . ' WHERE ' . $field . ' REGEXP ' . $pattern)
            ->execute();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

    }
}
