<?php
use app\components\CustomMigration as Migration;
use yii\db;
use yii\db\Schema;

/**
 * Class m160729_042406_report_debts
 */
class m160729_042406_report_debts extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('report_debts', [
            'id' => $this->primaryKey(),
            'month_year' => $this->date()->notNull(),
            'day' => $this->integer(11)->notNull(), //$this->integer(11)->defaultExpression('UNIX_TIMESTAMP()'),
            'debts' => $this->decimal(11, 2)->defaultValue(null),
            'potential_revenue' => $this->decimal(11, 2)->defaultValue(null),
            'revenue' => $this->decimal(11, 2)->defaultValue(null),
            'refund' => $this->decimal(11, 2)->defaultValue(null),
            'delivery_id' => $this->integer(11),
            'country_id' => $this->integer(11)
        ]);

        $this->createIndex(null, 'report_debts', 'month_year');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable("report_debts");
    }
}
