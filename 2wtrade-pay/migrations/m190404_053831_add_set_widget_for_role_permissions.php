<?php
use app\components\PermissionMigration as Migration;

/**
 * Class m190404_053831_add_set_widget_for_role_permissions
 */
class m190404_053831_add_set_widget_for_role_permissions extends Migration
{
    protected $permissions = ['access.role.setwidgetforrole'];

    protected $roles = [
        'technical.director' => ['access.role.setwidgetforrole'],
        'admin' => ['access.role.setwidgetforrole'],
        'support.manager' => ['access.role.setwidgetforrole'],
    ];
}
