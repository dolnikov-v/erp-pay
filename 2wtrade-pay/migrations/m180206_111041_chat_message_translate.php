<?php
use app\components\CustomMigration as Migration;
use yii\db\Query;

/**
 * Class m180206_111041_chat_message_translate
 */
class m180206_111041_chat_message_translate extends Migration
{

    const TEXT = [
        'новых лидов сегодня по направлениям:' => 'new leads today in the directions:',
        'В {officeName} ({workTime}), новых лидов сегодня по направлениям: {text}.' => 'In {officeName} ({workTime}), new leads today in the directions: {text}.',
        'не найдено!' => 'not found!',
        '{all}, из них апрув {approve} ({approvePercent}%)' => '{all}, of them is approved {approve} ({approvePercent}%)'
    ];
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $query = new Query();

        foreach ($this::TEXT as $ruText => $enText) {

            $is = $query->select('id')
                ->from('i18n_source_message')
                ->where(['category' => 'common', 'message' => $ruText])->one();

            if (!$is) {
                $this->insert('i18n_source_message', ['category' => 'common', 'message' => $ruText]);
                $id = Yii::$app->db->getLastInsertID();
                $this->insert('i18n_message', ['id' => $id, 'language' => 'en-US', 'translation' => $enText]);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach ($this::TEXT as $ruText => $enText) {
            $this->delete('i18n_source_message', ['category' => 'common', 'message' => $ruText]);
        }
    }
}
