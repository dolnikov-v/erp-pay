<?php

use app\components\CustomMigration as Migration;

/**
 * Handles adding permission to table `country_curator`.
 */
class m170214_111032_add_permission_to_country_curator extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}',array(
            'name'=>'report.delivery.delaybycountry.index',
            'type' => '2',
            'description' => 'report.delivery.delaybycountry.index'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'country.curator',
            'child' => 'report.delivery.delaybycountry.index'
        ));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->authManager->itemChildTable, ['child' => 'report.delivery.delaybycountry.index', 'parent' => 'country.curator']);
        $this->delete('{{%auth_item}}', ['name' => 'report.delivery.delaybycountry.index']);
    }
}
