<?php
use app\components\CustomMigration as Migration;

/**
 * Class m180926_103845_invoice_for_order_packaging
 */
class m180926_103845_invoice_for_order_packaging extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('order_packaging' , 'invoice', $this->string(100));
        $this->addColumn('delivery' , 'packager_send_invoice', $this->boolean()->defaultValue(false));
        $this->addColumn('delivery' , 'packager_send_label', $this->boolean()->defaultValue(true));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('delivery', 'packager_send_label');
        $this->dropColumn('delivery', 'packager_send_invoice');
        $this->dropColumn('order_packaging', 'invoice');
    }
}
