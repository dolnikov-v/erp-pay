<?php
use app\components\PermissionMigration as Migration;

/**
 * Class m180921_113226_add_permision_report_charges_test
 */
class m180921_113226_add_permision_report_charges_test extends Migration
{
    protected $permissions = ['report.chargestest.index'];

    protected $roles = [
        'project.manager' => [
            'report.chargestest.index'
        ],
        'admin' => [
            'report.chargestest.index'
        ],
        'sales.director' => [
            'report.chargestest.index'
        ],
        'support.manager' => [
            'report.chargestest.index'
        ],
        'technical.director' => [
            'report.chargestest.index'
        ],
    ];
}
