<?php
use app\components\PermissionMigration as Migration;

/**
 * Class m180122_033317_delivery_contract_system_filename
 */
class m180122_033317_delivery_contract_system_filename extends Migration
{
    protected $permissions = [
        'delivery.control.contracts',
        'delivery.control.contractedit',
        'delivery.control.contractdelete',
        'delivery.control.contractfiledelete',
        'media.deliverycontract.file',
    ];

    protected $roles = [
        'finance.manager' => [
            'delivery.control.contracts',
            'delivery.control.contractedit',
            'delivery.control.contractdelete',
            'delivery.control.contractfiledelete',
            'media.deliverycontract.file',
        ],
        'finance.director' => [
            'delivery.control.contracts',
            'delivery.control.contractedit',
            'delivery.control.contractdelete',
            'delivery.control.contractfiledelete',
            'media.deliverycontract.file',
        ]
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('delivery_contract_file', 'system_filename', $this->string(100)->after('file_name'));
        $this->update('delivery_contract_file', ['system_filename' => new \yii\db\Expression('`file_name`')]);
        parent::safeUp();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('delivery_contract_file', 'system_filename');
        parent::safeDown();
    }
}
