<?php

use app\components\CustomMigration as Migration;

/**
 * Handles the creation for table `ticket_tables`.
 */
class m160919_055504_create_ticket_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('ticket_group', [
            'prefix' => $this->string(255)->notNull(),
            'name' => $this->string(255)->notNull(),
            'created_at' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)->notNull(),
            $this->includePrimaryKey('prefix')
        ], $this->tableOptions);

        $this->createTable('ticket_role', [
            'id' => $this->primaryKey(),
            'role_name' => $this->string(255)->notNull(),
            'ticket_prefix' => $this->string(255)->notNull(),
            'type' => $this->integer(3)->defaultValue(1)->notNull(),
            'created_at' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)->notNull()
        ], $this->tableOptions);

        $this->addForeignKey(null, 'ticket_role', 'ticket_prefix', 'ticket_group', 'prefix',
            self::CASCADE,
            self::CASCADE);
        $this->addForeignKey(null, 'ticket_role', 'role_name', $this->authManager->itemTable,
            'name', self::CASCADE,
            self::CASCADE);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('ticket_role');
        $this->dropTable('ticket_group');
        return true;
    }
}
