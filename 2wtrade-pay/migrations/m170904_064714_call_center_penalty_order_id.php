<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170904_064714_call_center_penalty_order_id
 */
class m170904_064714_call_center_penalty_order_id extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('call_center_penalty', 'order_id', $this->integer()->after('person_id'));
        $this->addForeignKey('fk_call_center_penalty_order_id', 'call_center_penalty', 'order_id', 'order', 'id', self::SET_NULL, self::CASCADE);
        $this->addColumn('call_center_penalty_type', 'trigger', $this->string(255)->after('id'));
        $this->createIndex('idx_call_center_penalty_type_trigger', 'call_center_penalty_type', 'trigger', true);

        if (!$this->authManager->getPermission('callcenter.penaltytype.changetrigger')) {
            $authPermission = $this->authManager->createPermission('callcenter.penaltytype.changetrigger');
            $authPermission->description = 'callcenter.penaltytype.changetrigger';
            $this->authManager->add($authPermission);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_call_center_penalty_order_id', 'call_center_penalty');
        $this->dropColumn('call_center_penalty', 'order_id');
        $this->dropIndex('idx_call_center_penalty_type_trigger', 'call_center_penalty_type');
        $this->dropColumn('call_center_penalty_type', 'trigger');

        if ($permission = $this->authManager->getPermission('callcenter.penaltytype.changetrigger')) {
            $this->authManager->remove($permission);
        }
    }
}
