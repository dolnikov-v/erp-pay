<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;
use app\models\Notification;

/**
 * Class m161017_050550_add_report_adcombo
 */
class m161017_050550_add_report_adcombo extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('report_adcombo', [
            'id' => $this->primaryKey(),
            'type' => $this->integer(2)->notNull()->defaultValue(1),
            'country_id' => $this->integer(11)->notNull(),
            'from' => $this->integer(11)->notNull(),
            'to' => $this->integer(11)->notNull(),
            'hold_count' => $this->integer(11)->notNull()->defaultValue(0),
            'our_hold_count' => $this->integer(11)->notNull()->defaultValue(0),
            'confirmed_count' => $this->integer(11)->notNull()->defaultValue(0),
            'our_confirmed_count' => $this->integer(11)->notNull()->defaultValue(0),
            'trash_count' => $this->integer(11)->notNull()->defaultValue(0),
            'our_trash_count' => $this->integer(11)->notNull()->defaultValue(0),
            'cancelled_count' => $this->integer(11)->notNull()->defaultValue(0),
            'our_cancelled_count' => $this->integer(11)->notNull()->defaultValue(0),
            'difference_count' => $this->integer(11)->notNull()->defaultValue(0),
            'difference' => $this->text(),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11)
        ], $this->tableOptions);

        $this->addForeignKey(null, 'report_adcombo', 'country_id', 'country', 'id', self::NO_ACTION, self::NO_ACTION);

        $crontabTask = new CrontabTask();
        $crontabTask->name = 'report_compare_with_adcombo';
        $crontabTask->description = 'Сверка с AdCombo';
        $crontabTask->save();

        $notification = new Notification();
        $notification->description = "AdCombo '{dateFrom} - {dateTo}': Всего {totalCount} | {ourTotalCount}; Hold {holdCount} | {ourHoldCount}; Confirmed {confirmedCount} | {ourConfirmedCount}; Trash {trashCount} | {ourTrashCount}; Cancelled {cancelledCount} | {ourCancelledCount};";
        $notification->trigger = "report.adcombo.daily-info";
        $notification->type = Notification::TYPE_INFO;
        $notification->group = Notification::GROUP_REPORT;
        $notification->active = 1;
        $notification->save();
        unset($notification);

        $notification1 = new Notification();
        $notification1->description = "AdCombo '{dateFrom} - {dateTo}' имеются различия: Не совпадает статус {statusCount}; Не найдено заказов в AdCombo {ourCount}; Не найдено заказов у нас {adcomboCount};";
        $notification1->trigger = "report.adcombo.daily-danger";
        $notification1->type = Notification::TYPE_DANGER;
        $notification1->group = Notification::GROUP_REPORT;
        $notification1->active = 1;
        $notification1->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = CrontabTask::find()->where(['name' => 'report_compare_with_adcombo'])->one();
        if ($crontabTask) {
            $crontabTask->delete();
        }
        $notification = Notification::find()->where(['trigger' => "report.adcombo.daily-info"])->one();
        if ($notification) {
            $notification->delete();
        }
        $notification = Notification::find()->where(['trigger' => "report.adcombo.daily-danger"])->one();
        if ($notification) {
            $notification->delete();
        }
        $this->dropTable('report_adcombo');
    }
}
