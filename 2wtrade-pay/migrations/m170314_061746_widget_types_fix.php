<?php

use app\components\CustomMigration as Migration;
use app\modules\widget\models\WidgetUser;
use app\modules\administration\models\CrontabTask;
use app\models\Country;
use app\modules\widget\models\WidgetType;


/**
 * Class m170314_061746_widget_types_fix
 */
class m170314_061746_widget_types_fix extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        $code = "top_countries";
        $widget = WidgetType::find()->where(['code' => $code])->one();
        if ($widget) {
            $widget->by_country = WidgetType::BY_ALL_COUNTRIES;
            $widget->save();
        }

        $code = "top20products";
        $widget = WidgetType::find()->where(['code' => $code])->one();
        if ($widget) {
            $widget->by_country = WidgetType::BY_COUNTRY;
            $widget->save();
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

    }
}
