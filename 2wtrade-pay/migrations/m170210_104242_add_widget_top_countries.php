<?php
use app\components\CustomMigration as Migration;
use app\modules\widget\models\WidgetType;
use app\modules\widget\models\WidgetRole;

/**
 * Class m170210_104242_add_widget_top_countries
 */
class m170210_104242_add_widget_top_countries extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $rule = 'widget.index.load';
        $roles = [
            'business_analyst',
            'country.curator',
            'callcenter.manager',
        ];

        $this->insert(WidgetType::tableName(), [
            'code' => 'top_countries',
            'name' => 'Топ стран',
            'status' => WidgetType::STATUS_ACTIVE
        ]);

        $type = WidgetType::find()
            ->where(['code' => 'top_countries'])
            ->one();

        $this->insert($this->authManager->itemTable, [
            'name' => $rule,
            'type' => 2,
            'description' => $rule,
        ]);

        foreach ($roles as $role) {

            $this->insert(WidgetRole::tableName(), [
                'role' => $role,
                'type_id' => $type->id
            ]);

            $this->insert($this->authManager->itemChildTable, [
                'parent' => $role,
                'child' => $rule
            ]);
        }
    }


    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $rule = 'widget.index.load';
        $roles = [
            'business_analyst',
            'country.curator',
            'callcenter.manager',
        ];

        $type = WidgetType::find()
            ->where(['code' => 'top_countries'])
            ->one();

        foreach ($roles as $role) {

            $this->delete(WidgetRole::tableName(), [
                'role' => $role,
                'type_id' => $type->id
            ]);

            $this->delete($this->authManager->itemChildTable, [
                'parent' => $role,
                'child' => 'widget.index.load'
            ]);
        }

        $this->delete($this->authManager->itemTable, [
            'name' => $rule,
            'type' => 2
        ]);

        $this->delete(WidgetType::tableName(), ['code' => 'top_countries']);
    }
}
