<?php

use app\components\CustomMigration as Migration;

/**
 * Handles adding column packager to table `delivery_table`.
 */
class m170607_050004_add_column_packager_to_delivery_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('delivery', 'packager_id', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('delivery', 'packager_id');
    }
}
