<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160512_143724_notification_role
 */
class m160512_143724_notification_role extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('notification_role', [
            'id' => $this->primaryKey(),
            'role' => $this->string(32)->notNull(),
            'trigger' => $this->string(255)->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $this->tableOptions);
        $this->addForeignKey(
            null, 'notification_role', 'role', 'auth_item', 'name', 'CASCADE'
        );
        $this->addForeignKey(
            null, 'notification_role', 'trigger', 'notification', 'trigger', 'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('notification_role');
    }
}
