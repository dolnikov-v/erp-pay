<?php
use app\components\PermissionMigration as Migration;

/**
 * Class m180402_040352_chat_transport
 */
class m180402_040352_chat_transport extends Migration
{
    protected $permissions = [
        'notification.chat.setcountry',
    ];

    protected $roles = [
        'admin' => [
            'notification.chat.setcountry',
        ],
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('chat_transport', [
            'id' => $this->primaryKey(),
            'chat_id' => $this->integer()->notNull(),
            'chat_number' => $this->string(256)->notNull(),
            'messenger' => $this->string(32)->notNull(),
        ],
            $this->tableOptions);

        $this->addForeignKey(
            null,
            'chat_transport',
            'chat_id',
            'chat',
            'id',
            self::CASCADE
        );

        $this->createIndex(null, 'chat_transport', 'chat_number');

        $this->dropColumn('chat', 'messenger');
        $this->dropColumn('chat', 'chat_id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('chat_transport');
    }
}
