<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryApiClass;

/**
 * Class m161020_080145_add_row_delivery_class_stallion_api
 */
class m161021_094827_add_row_delivery_class_eaglexpress_api extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $class = new DeliveryApiClass();
        $class->name = 'eaglExpressApi';
        $class->class_path = '/eaglexpress-api/src/eaglExpressApi.php';
        $class->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        DeliveryApiClass::find()
            ->where([
                'name' => 'eaglExpressApi',
                'class_path' => '/eaglexpress-api/src/eaglExpressApi.php'
            ])
            ->one()
            ->delete();
    }
}
