<?php
use app\components\CustomMigration as Migration;
use app\models\Notification;
use app\modules\administration\models\CrontabTask;

/**
 * Class m171229_083545_add_hold_leads_and_approve_shortage_notification
 */
class m171229_083545_add_hold_leads_and_approve_shortage_notification extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $notification = new Notification([
            'description' => 'За вчерашний день в оффисе {officeName} процент зависших заказов составил {hold}% из {all} заказов',
            'trigger' => Notification::TRIGGER_CALL_CENTER_HOLD_LEADS,
            'type' => Notification::TYPE_DANGER,
            'group' => Notification::GROUP_SYSTEM,
            'active' => Notification::ACTIVE,
        ]);
        $notification->save(false);

        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_CALL_CENTER_HOLD_LEADS]);
        if (!$crontabTask) {
            $crontabTask = new CrontabTask();
            $crontabTask->name = CrontabTask::TASK_CALL_CENTER_HOLD_LEADS;
            $crontabTask->description = "Уведомление о том, что за последние сутки % холдов превысил 50";
            $crontabTask->save();
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_CALL_CENTER_HOLD_LEADS]);
        $crontabTask->delete();
        $notification = Notification::findOne(['trigger' => Notification::TRIGGER_CALL_CENTER_HOLD_LEADS]);
        $notification->delete();
    }
}
