<?php

use yii\db\Migration;

/**
 * Handles adding function_convert_currency_order to table `currency_by_country`.
 */
class m180807_030726_add_function_convert_currency_order_to_currency_by_country extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $sql = <<<SQL
        CREATE FUNCTION convertToCurrencyByCountry(sum FLOAT, currency_from INTEGER,  currency_to INTEGER) RETURNS FLOAT NOT DETERMINISTIC
        BEGIN 
		  DECLARE converted_sum FLOAT DEFAULT 0;  		
		  	 	
          SET converted_sum = 
			 CASE WHEN currency_to IS NOT NULL
			      THEN sum / COALESCE ((select rate from currency_rate where currency_id = currency_from),0) * COALESCE ((select rate from currency_rate where currency_id = currency_to), 0)
			      ELSE sum
			      END;
		  
        RETURN converted_sum;
        END; 
SQL;

        yii::$app->db->createCommand($sql)->execute();
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $sql = 'DROP FUNCTION IF EXISTS convertToCurrencyByCountry;';
        yii::$app->db->createCommand($sql)->execute();
    }
}
