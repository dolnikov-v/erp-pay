<?php
use app\components\CustomMigration as Migration;
use app\modules\finance\models\ReportExpensesItem;
use yii\db\Query;

/**
 * Class m180828_030227_add_expense_adcombo_penalty
 */
class m180828_030227_add_expense_adcombo_penalty extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $query = new Query();

        $is = $query->select('id')
            ->from('report_expenses_item')
            ->where(['code' => ReportExpensesItem::ADCOMBO_PENALTY_ITEM_CODE])->one();

        if (!$is) {
            $this->insert('report_expenses_item',
                [
                    'code' => ReportExpensesItem::ADCOMBO_PENALTY_ITEM_CODE,
                    'name' => 'Adcombo: Штраф',
                    'type' => ReportExpensesItem::TYPE_REGULAR,
                    'created_at' => time(),
                    'updated_at' => time(),
                    'category_id' => null,
                    'detailed' => 1,
                    'active' => 1,
                    'is_expense' => 0,
                ]);
        }
    }
}
