<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m180227_084134_task_monitoringdeliverydate
 */
class m180227_084134_task_monitoringdeliverydate extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = new CrontabTask();
        $crontabTask->name = 'monitoring_delivery_date';
        $crontabTask->description = "Мониторинг просроченных сроков доставки и создание задач на исправление ситуации в Jira";
        $crontabTask->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        CrontabTask::findOne(['name' => 'monitoring_delivery_date'])->delete();
    }
}
