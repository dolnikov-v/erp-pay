<?php

use app\components\CustomMigration as Migration;

/**
 * Handles adding month to table `report_expenses_country_item`.
 */
class m170811_182547_add_month_to_report_expenses_country_item extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('report_expenses_country_item', 'year', $this->integer()->after('country_id'));
        $this->addColumn('report_expenses_country_item', 'month', $this->integer()->after('country_id'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('report_expenses_country_item', 'year');
        $this->dropColumn('report_expenses_country_item', 'month');
    }
}
