<?php
use app\components\CustomMigration as Migration;
use app\models\Country;
use app\models\Config;

/**
 * Class m170922_055143_auto_trash_country
 */
class m170922_055143_auto_trash_country extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('auto_trash', 'country_id', $this->integer()->after('is_active'));
        $this->addForeignKey('fk_auto_trash_country_id', 'auto_trash', 'country_id', 'country', 'id', self::CASCADE, self::RESTRICT);

        $this->addColumn(Country::tableName(), 'auto_trash', $this->boolean()->defaultValue(0));
        $this->update(Country::tableName(), ['auto_trash' => 1]);

        $this->delete(Config::tableName(), ['name' => 'STATE_AUTOTRASH_ORDERS']);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_auto_trash_country_id', 'auto_trash');
        $this->dropColumn('auto_trash', 'country_id');
        $this->dropTable('auto_trash_country');
        $this->insert(Config::tableName(), ['name' => 'STATE_AUTOTRASH_ORDERS', 'description' => 'Вкл/откл автотреша заказов', 'value' => 0]);
    }
}
