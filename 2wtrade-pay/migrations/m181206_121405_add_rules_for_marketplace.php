<?php

/**
 * Class m181206_121405_add_rules_for_marketplace
 */
class m181206_121405_add_rules_for_marketplace extends \app\components\PermissionMigration
{
    protected $roles = [
        'market.manager' => [
            'marketplace.index.index',
            'marketplace.index.switchavailability',
            'marketplace.view.index',
            'marketplace.view.configuration',
            'marketplace.view.products',
            'marketplace.view.editproduct',
            'marketplace.view.removeproduct',
        ],
        'technical.director' => [
            'marketplace.index.index',
            'marketplace.index.switchavailability',
            'marketplace.view.index',
            'marketplace.view.configuration',
            'marketplace.view.products',
            'marketplace.view.editproduct',
            'marketplace.view.removeproduct',
        ]
    ];
}
