<?php
use app\components\CustomMigration as Migration;

use app\modules\checklist\models\CheckListItem;
use app\modules\checklist\models\CheckList;
use app\modules\salary\models\Office;
use app\modules\salary\models\Person;
use app\modules\salary\models\Designation;
use app\models\User;
use yii\db\Query;

/**
 * Class m171110_080653_check_list_call_center
 */
class m171110_080653_check_list_call_center extends Migration
{

    const RULES = [
        'checklist.call.index',
    ];

    const ROLES = [
        'callcenter.leader',
        'callcenter.teamlead',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(User::tableName(), 'person_id', $this->integer());
        $this->addForeignKey(null, User::tableName(), 'person_id', Person::tableName(), 'id', self::SET_NULL, self::NO_ACTION);

        $this->addColumn(CheckListItem::tableName(), 'type', $this->string()->defaultValue('pay')->after('trigger'));

        $this->addColumn(Designation::tableName(), 'supervisor', $this->integer()->defaultValue(0));

        $this->update(Designation::tableName(), ['supervisor' => 1], ['name' => ['Административный директор', 'Супервайзер']]);

        $data = [
            1 => 'Скорость ответа клиенту с момента поступления заявки в систему В СРЕДНЕМ не более 3х минут',
            2 => 'Каждый оператор в смену сделал не менее 150 звонков',
            3 => '% аппрува от сегодняшних лидов не менее 40% на конец дня',
            4 => 'проверены все адреса в апрувах м них нет ошибок операторов в адресах и нет ложных апрувов по итогам дня',
            5 => 'прослушаны все звонки в апрувах и в них нет ложных по итогам дня',
            6 => 'прослушаны все звонки в отказах и в них нет ложных отказов по итогам дня',
            7 => 'На момент окончания рабочей смены в очереди первого звонка не находится более 20-ти лидов',
            8 => 'На момент окончания рабочей смены в очередях первых 4-х звонков находится не более 50 заказов',
            9 => 'На момент окончания рабочего дня средний чек не менее 65$',
            10 => 'Минимальная продолжительность звонка не менее 15 сек',
        ];

        $arrayForInsert = [];
        foreach ($data as $key => $name) {
            $arrayForInsert[] = [
                'position' => $key,
                'name' => $name,
                'days' => 1,
                'active' => 1,
                'type' => CheckListItem::TYPE_CALL,
                'created_at' => time(),
                'updated_at' => time(),
            ];
        }

        Yii::$app->db->createCommand()->batchInsert(
            CheckListItem::tableName(),
            [
                'position',
                'name',
                'days',
                'active',
                'type',
                'created_at',
                'updated_at',
            ],
            $arrayForInsert
        )->execute();


        $this->addColumn(CheckList::tableName(), 'office_id', $this->integer()->after('team_id'));
        $this->addColumn(CheckList::tableName(), 'teamlead_id', $this->integer()->after('office_id'));

        $this->addForeignKey(null, CheckList::tableName(), 'office_id', Office::tableName(), 'id', self::CASCADE, self::NO_ACTION);
        $this->addForeignKey(null, CheckList::tableName(), 'teamlead_id', Person::tableName(), 'id', self::CASCADE, self::NO_ACTION);

        $query = new Query();

        foreach (self::RULES as $rule) {

            $is_rule = $query->select('*')->from($this->authManager->itemTable)
                ->where(['name' => $rule, 'type' => 2])
                ->one();

            if (!$is_rule) {
                $this->insert($this->authManager->itemTable, [
                    'name' => $rule,
                    'description' => $rule,
                    'type' => 2,
                    'created_at' => time(),
                    'updated_at' => time()]);
            }

            foreach (self::ROLES as $role) {

                $is_role = $query->select('*')->from($this->authManager->itemTable)
                    ->where(['name' => $role, 'type' => 1])
                    ->one();

                if (!$is_role) {
                    $this->insert($this->authManager->itemTable, [
                        'name' => $role,
                        'type' => 1,
                        'description' => $role,
                        'created_at' => time(),
                        'updated_at' => time()]);
                }

                $is_can = $query->select('*')->from($this->authManager->itemChildTable)->where([
                    'parent' => $role,
                    'child' => $rule
                ])->one();

                if (!$is_can) {
                    $this->insert($this->authManager->itemChildTable, [
                        'parent' => $role,
                        'child' => $rule
                    ]);
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_user_person_id', User::tableName());
        $this->dropColumn(User::tableName(), 'person_id');

        $this->delete(CheckListItem::tableName(), ['type' => CheckListItem::TYPE_CALL]);
        $this->dropColumn(CheckListItem::tableName(), 'type');

        $this->dropForeignKey('fk_check_list_teamlead_id', CheckList::tableName());
        $this->dropForeignKey('fk_check_list_office_id', CheckList::tableName());

        $this->dropColumn(CheckList::tableName(), 'office_id');
        $this->dropColumn(CheckList::tableName(), 'teamlead_id');

        $this->dropColumn(Designation::tableName(), 'supervisor');

        foreach (self::RULES as $rule) {
            foreach (self::ROLES as $role) {
                $this->delete($this->authManager->itemChildTable, [
                    'parent' => $role,
                    'child' => $rule
                ]);
            }
        }
    }
}
