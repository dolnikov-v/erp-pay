<?php
use app\components\CustomMigration as Migration;
use app\modules\checklist\models\CheckList;
use app\modules\reportoperational\models\Team;

/**
 * Class m171025_101026_check_list_team
 */
class m171025_101026_check_list_team extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(CheckList::tableName(), 'team_id', $this->integer()->after('status'));
        $this->addForeignKey('fk_check_list_team_id', CheckList::tableName(), 'team_id', Team::tableName(), 'id', self::SET_NULL, self::NO_ACTION);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_check_list_team_id', CheckList::tableName());
        $this->dropColumn(CheckList::tableName(), 'team_id');
    }
}
