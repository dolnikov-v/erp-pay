<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;
use app\modules\checklist\models\CheckListItem;

/**
 * Class m171026_044340_check_list_auto
 */
class m171026_044340_check_list_auto extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(CheckListItem::tableName(), 'auto', $this->integer(1)->after('active'));
        $this->addColumn(CheckListItem::tableName(), 'trigger', $this->string()->after('auto'));

        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_AUTO_CHECK_LIST]);
        if (!$crontabTask) {
            $crontabTask = new CrontabTask();
            $crontabTask->name = CrontabTask::TASK_AUTO_CHECK_LIST;
            $crontabTask->description = "Автоматическая проверка пунктов чек листа";
            $crontabTask->save();
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(CheckListItem::tableName(), 'auto');
        $this->dropColumn(CheckListItem::tableName(), 'trigger');

        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_AUTO_CHECK_LIST]);
        if ($crontabTask instanceof CrontabTask) {
            $crontabTask->delete();
        }
    }
}
