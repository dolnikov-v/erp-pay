<?php
use app\components\CustomMigration as Migration;
use app\modules\salary\models\Office;
use yii\db\Query;

/**
 * Class m170911_083757_call_center_office_type
 */
class m170911_083757_call_center_office_type extends Migration
{

    protected $textData = [
        'Офис успешно добавлен.' => 'The office was successfully added.',
        'Офис успешно сохранен.' => 'The office was successfully saved.',
        'Офис успешно активирован.' => 'The office was successfully activated.',
        'Офис успешно деактивирован.' => 'The office was successfully deactivated.',
        'Офис не найден.' => 'Office not found.',
        'Добавить офис' => 'Add office',
        'Сохранить офис' => 'Save office',
        'Офисы' => 'Offices',
        'Таблица с офисами' => 'Table with offices',
        'Зарплата' => 'Salary',
        'Расчет ЗП' => 'Salary report',
        'Выберите Офис или Направление' => 'Choose office or direction',
        'Добавление сотрудника офиса' => 'Adding an employee',
        'Редактирование сотрудника офиса' => 'Editing an employee',
        'Сотрудник офиса' => 'Employee',
        'Необходимо отфильтровать список сотрудников по Офису' => 'It is necessary to filter the list of employees by Office',
    ];

    protected $renameRules = [
        'callcenter.bonus.delete',
        'callcenter.bonus.edit',
        'callcenter.bonus.gettypes',
        'callcenter.bonus.index',
        'callcenter.bonustype.activate',
        'callcenter.bonustype.deactivate',
        'callcenter.bonustype.delete',
        'callcenter.bonustype.edit',
        'callcenter.bonustype.index',
        'callcenter.designation.activate',
        'callcenter.designation.deactivate',
        'callcenter.designation.edit',
        'callcenter.designation.index',
        'callcenter.holiday.delete',
        'callcenter.holiday.edit',
        'callcenter.holiday.index',
        'callcenter.monthlystatement.activate',
        'callcenter.monthlystatement.deactivate',
        'callcenter.monthlystatement.delete',
        'callcenter.monthlystatement.deletepersons',
        'callcenter.monthlystatement.edit',
        'callcenter.monthlystatement.index',
        'callcenter.monthlystatementdata.delete',
        'callcenter.monthlystatementdata.edit',
        'callcenter.office.activate',
        'callcenter.office.deactivate',
        'callcenter.office.edit',
        'callcenter.office.index',
        'callcenter.penalty.delete',
        'callcenter.penalty.edit',
        'callcenter.penalty.index',
        'callcenter.penaltytype.activate',
        'callcenter.penaltytype.changetrigger',
        'callcenter.penaltytype.deactivate',
        'callcenter.penaltytype.delete',
        'callcenter.penaltytype.edit',
        'callcenter.penaltytype.index',
        'callcenter.person.activate',
        'callcenter.person.activatepersons',
        'callcenter.person.approve',
        'callcenter.person.approvepersons',
        'callcenter.person.changeteamleader',
        'callcenter.person.deactivate',
        'callcenter.person.deactivatepersons',
        'callcenter.person.delete',
        'callcenter.person.deletephoto',
        'callcenter.person.edit',
        'callcenter.person.export',
        'callcenter.person.getparents',
        'callcenter.person.getpersons',
        'callcenter.person.index',
        'callcenter.person.link',
        'callcenter.person.notapprove',
        'callcenter.person.notapprovepersons',
        'callcenter.person.setbonus',
        'callcenter.person.setpenalty',
        'callcenter.person.setrole',
        'callcenter.person.setwork',
        'callcenter.person.unlink',
        'callcenter.personimport.delete',
        'callcenter.personimport.import',
        'callcenter.personimport.index',
        'callcenter.personimport.process',
        'callcenter.personimport.recordedit',
        'callcenter.personimport.recordexclude',
        'callcenter.personimport.recordgroupapprovedhead',
        'callcenter.personimport.recordgroupexclude',
        'callcenter.personimport.recordgroupinclude',
        'callcenter.personimport.recordinclude',
        'callcenter.workingshift.delete',
        'callcenter.workingshift.edit',
        'callcenter.worktimeplan.delete',
        'callcenter.worktimeplan.deletelist',
        'callcenter.worktimeplan.edit',
        'callcenter.worktimeplan.generate',
        'callcenter.worktimeplan.index',
    ];


    const ROLES = [
        'storage.manager',
    ];

    const RULES = [
        'report.salary.index',
        'report.salary.getcallcenter',
        'report.salary.getteamlead',
        'salary.person.index',
        'limit_salary_persons_by_storage',
    ];


    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $query = new Query();

        foreach ($this->renameRules as $oldRule) {

            $newRule = str_replace('callcenter.', 'salary.', $oldRule);

            $this->update('auth_item',
                [
                    'name' => $newRule,
                    'description' => $newRule,
                ],
                ['name' => $oldRule]
            );
        }

        foreach (self::RULES as $rule) {

            $is_rule = $query->select('*')->from($this->authManager->itemTable)
                ->where(['name' => $rule, 'type' => 2])
                ->one();

            if (!$is_rule) {
                $this->insert($this->authManager->itemTable, [
                    'name' => $rule,
                    'description' => $rule,
                    'type' => 2,
                    'created_at' => time(),
                    'updated_at' => time()]);
            }

            foreach (self::ROLES as $role) {
                $is_can = $query->select('*')->from($this->authManager->itemChildTable)->where([
                    'parent' => $role,
                    'child' => $rule
                ])->one();

                if (!$is_can) {
                    $this->insert($this->authManager->itemChildTable, [
                        'parent' => $role,
                        'child' => $rule
                    ]);
                }
            }
        }

        $this->renameTable('call_center_office', 'salary_office');
        $this->renameTable('call_center_bonus', 'salary_bonus');
        $this->renameTable('call_center_bonus_type', 'salary_bonus_type');
        $this->renameTable('call_center_designation', 'salary_designation');
        $this->renameTable('call_center_holiday', 'salary_holiday');
        $this->renameTable('call_center_monthly_statement', 'salary_monthly_statement');
        $this->renameTable('call_center_monthly_statement_data', 'salary_monthly_statement_data');
        $this->renameTable('call_center_penalty', 'salary_penalty');
        $this->renameTable('call_center_penalty_type', 'salary_penalty_type');
        $this->renameTable('call_center_person', 'salary_person');
        $this->renameTable('call_center_person_import', 'salary_person_import');
        $this->renameTable('call_center_person_import_data', 'salary_person_import_data');
        $this->renameTable('call_center_working_shift', 'salary_working_shift');
        $this->renameTable('call_center_work_time_plan', 'salary_work_time_plan');

        $this->addColumn('salary_office', 'type', $this->string()->after('active')->defaultValue('call_center'));

        foreach ($this->textData as $ruText => $enText) {

            $is = $query->select('id')
                ->from('i18n_source_message')
                ->where(['category' => 'common', 'message' => $ruText])->one();

            if (!$is) {
                $this->insert('i18n_source_message', ['category' => 'common', 'message' => $ruText]);
                $id = Yii::$app->db->getLastInsertID();
                $this->insert('i18n_message', ['id' => $id, 'language' => 'en-US', 'translation' => $enText]);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

        foreach (self::RULES as $rule) {
            foreach (self::ROLES as $role) {
                $this->delete($this->authManager->itemChildTable, [
                    'parent' => $role,
                    'child' => $rule
                ]);
            }
        }

        foreach ($this->renameRules as $oldRule => $newRule) {

            $newRule = str_replace('callcenter.', 'salary.', $oldRule);

            $this->update('auth_item',
                [
                    'name' => $oldRule,
                    'description' => $oldRule
                ],
                ['name' => $newRule]
            );
        }

        $this->dropColumn(Office::tableName(), 'type');

        $this->renameTable('salary_office', 'call_center_office');
        $this->renameTable('salary_bonus', 'call_center_bonus');
        $this->renameTable('salary_bonus_type', 'call_center_bonus_type');
        $this->renameTable('salary_designation', 'call_center_designation');
        $this->renameTable('salary_holiday', 'call_center_holiday');
        $this->renameTable('salary_monthly_statement', 'call_center_monthly_statement');
        $this->renameTable('salary_monthly_statement_data', 'call_center_monthly_statement_data');
        $this->renameTable('salary_penalty', 'call_center_penalty');
        $this->renameTable('salary_penalty_type', 'call_center_penalty_type');
        $this->renameTable('salary_person', 'call_center_person');
        $this->renameTable('salary_person_import', 'call_center_person_import');
        $this->renameTable('salary_person_import_data', 'call_center_person_import_data');
        $this->renameTable('salary_working_shift', 'call_center_working_shift');
        $this->renameTable('salary_work_time_plan', 'call_center_work_time_plan');
    }
}
