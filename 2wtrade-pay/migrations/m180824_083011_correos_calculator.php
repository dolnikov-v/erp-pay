<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryChargesCalculator;

/**
 * Class m180824_083011_correos_calculator
 */
class m180824_083011_correos_calculator extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert(DeliveryChargesCalculator::tableName(), [
            'name' => 'CorreosDeMexico',
            'class_path' => 'app\modules\delivery\components\charges\correosdemexico\CorreosDeMexico',
            'active' => 1,
            'created_at' => time(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete(DeliveryChargesCalculator::tableName(), ['name' => 'CorreosDeMexico']);
    }
}
