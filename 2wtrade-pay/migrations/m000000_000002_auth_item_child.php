<?php
use app\components\CustomMigration as Migration;

class m000000_000002_auth_item_child extends Migration
{
    public function up()
    {
        $this->createTable($this->authManager->itemChildTable, [
            'parent' => $this->string(64)->notNull(),
            'child' => $this->string(64)->notNull(),
            $this->includePrimaryKey(['parent', 'child']),
        ], $this->tableOptions);

        $this->addForeignKey(null, $this->authManager->itemChildTable, 'parent', $this->authManager->itemTable, 'name', self::CASCADE, self::CASCADE);
        $this->addForeignKey(null, $this->authManager->itemChildTable, 'child', $this->authManager->itemTable, 'name', self::CASCADE, self::CASCADE);
    }

    public function down()
    {
        $this->dropTable($this->authManager->itemChildTable);
    }
}
