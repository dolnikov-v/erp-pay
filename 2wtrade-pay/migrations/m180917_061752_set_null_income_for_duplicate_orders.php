<?php
use app\components\CustomMigration as Migration;

/**
 * Class m180917_061752_set_null_income_for_duplicate_orders
 */
class m180917_061752_set_null_income_for_duplicate_orders extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->update('order', ['income' => null], ['or', ['source' => ['wtrade_post_sale', 'wtrade_trash']], ['source' => null]]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->execute("UPDATE `order` INNER JOIN `order` o2 ON o2.duplicate_order_id = `order`.id SET `order`.income = o2.income;");
    }
}
