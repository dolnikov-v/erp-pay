<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160728_075025_delivery_request_date_send
 */
class m160728_075025_delivery_request_date_send extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('delivery_request', 'cron_launched_at', $this->integer()->defaultValue(null)->after('updated_at'));
        $this->addColumn('delivery_request', 'sent_at', $this->integer()->defaultValue(null)->after('cron_launched_at'));

        $this->addColumn('call_center_request', 'cron_launched_at', $this->integer()->defaultValue(null)->after('updated_at'));
        $this->addColumn('call_center_request', 'sent_at', $this->integer()->defaultValue(null)->after('cron_launched_at'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('call_center_request', 'sent_at');
        $this->dropColumn('call_center_request', 'cron_launched_at');
        $this->dropColumn('delivery_request', 'sent_at');
        $this->dropColumn('delivery_request', 'cron_launched_at');
    }
}
