<?php
use app\components\CustomMigration as Migration;
use yii\db\Query;

/**
 * Class m190329_100426_cron_marketplace_create_orders
 */
class m190329_100426_cron_marketplace_create_orders extends Migration
{
    const name = 'marketplace_create_orders';
    const description = 'Сохранение заказов за промежуток времени';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $cronTaskId = (new Query())->select('id')
            ->from('crontab_task')
            ->where(['name' => self::name])
            ->scalar();

        $data = [
            'name' => self::name,
            'description' => self::description,
            'updated_at' => time(),
        ];

        if (!$cronTaskId) {
            $this->insert('crontab_task', $data);
        } else {
            $data['created_at'] = time();
            $this->update('crontab_task', $data, ['id' => $cronTaskId]);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete('crontab_task', ['name' => self::name]);
    }
}
