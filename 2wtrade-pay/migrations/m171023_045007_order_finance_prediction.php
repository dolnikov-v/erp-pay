<?php
use app\components\CustomMigration as Migration;

/**
 * Class m171023_045007_order_finance_prediction
 */
class m171023_045007_order_finance_prediction extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('order_finance_prediction', [
            'order_id' => $this->integer(),
            'price_cod' => $this->double(),
            'price_storage' => $this->double(),
            'price_fulfilment' => $this->double(),
            'price_packing' => $this->double(),
            'price_package' => $this->double(),
            'price_delivery' => $this->double(),
            'price_redelivery' => $this->double(),
            'price_delivery_back' => $this->double(),
            'price_delivery_return' => $this->double(),
            'price_address_correction' => $this->double(),
            'price_cod_service' => $this->double(),
            'price_vat' => $this->double(),
            'additional_prices' => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $this->tableOptions);

        $this->addPrimaryKey(null, 'order_finance_prediction', 'order_id');
        $this->addForeignKey(null, 'order_finance_prediction', 'order_id', 'order', 'id', self::CASCADE, self::CASCADE);
        $this->createIndex(null, 'order_finance_prediction', 'created_at');
        $this->createIndex(null, 'order_finance_prediction', 'updated_at');
        $this->createIndex($this->getIdxName('order_finance', 'payment'), 'order_finance', 'payment');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropIndex($this->getIdxName('order_finance', 'payment'), 'order_finance');
        $this->dropTable('order_finance_prediction');
    }
}
