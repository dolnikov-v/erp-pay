<?php
use app\components\CustomMigration as Migration;
use app\models\AuthItem;

/**
 * Class m170320_034832_add_permission_for_cc_operators_online
 */
class m170320_034832_add_permission_for_cc_operators_online extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert(AuthItem::tableName(),array(
            'name'=>'report.callcenteroperatorsonline.index',
            'type' => '2',
            'description' => 'report.callcenteroperatorsonline.index',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'callcenter.manager',
            'child' => 'report.callcenteroperatorsonline.index'
        ));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->authManager->itemChildTable, ['child' => 'report.callcenteroperatorsonline.index', 'parent' => 'callcenter.manager']);
        $this->delete(AuthItem::tableName(), ['name' => 'report.callcenteroperatorsonline.index']);
    }
}
