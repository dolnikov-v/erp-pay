<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170523_050706_order_notification_request_add_sms_status_inactive
 */
class m170523_050706_order_notification_request_add_sms_status_inactive extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('{{%order_notification_request}}', 'sms_status', "ENUM ('inactive', 'pending', 'in_progress', 'done', 'error') default 'pending' not null");
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('{{%order_notification_request}}', 'sms_status', "ENUM ('pending', 'in_progress', 'done', 'error') default 'pending' not null");
    }
}
