<?php
use app\components\CustomMigration as Migration;
use app\models\Notification;

/**
 * Class m160915_060458_add_notification_order_not_delivered
 */
class m160915_060458_add_notification_order_not_delivered extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $model = new Notification([
            'description' => 'У заказа #{order_id} нарушен срок доставки.',
            'trigger' => 'order.not.delivered',
            'type' => Notification::TYPE_DANGER,
            'group' => Notification::GROUP_COMMON,
            'active' => Notification::ACTIVE,
        ]);

        $model->save(false);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $model = Notification::findOne(['trigger' => 'order.not.delivered']);

        if ($model instanceof Notification) {
            $model->delete();
        }
    }
}
