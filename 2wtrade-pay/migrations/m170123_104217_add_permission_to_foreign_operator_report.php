<?php

use app\components\CustomMigration as Migration;

/**
 * Handles adding permission  `report.foreignoperator.index`.
 */
class m170123_104217_add_permission_to_foreign_operator_report extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}',array(
            'name'=>'report.foreignoperator.index',
            'type' => '2',
            'description' => 'report.foreignoperator.index',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'country.curator',
            'child' => 'report.foreignoperator.index'
        ));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->authManager->itemChildTable, ['child' => 'report.foreignoperator.index', 'parent' => 'country.curator']);
        $this->delete('{{%auth_item}}', ['name' => 'report.foreignoperator.index']);
    }
}
