<?php

use app\components\CustomMigration as Migration;
use app\modules\callcenter\models\CallCenterRequest;


class m170316_080003_fix_calcenterrequest_lastforeignoperator extends Migration
{
    
    public function up()
    {
        $offset = 0;
        $limit = 1000;
        $repeat = true;

        echo "-=-=-= START -=-=-=-";
        while ($repeat) {
            $requests = CallCenterRequest::find()
                ->where(["IS NOT", CallCenterRequest::tableName() . '.cc_update_response', NULL])
                ->offset($offset)
                ->limit($limit)
                ->all();

            foreach ($requests as $request) {
                $data = $request->cc_update_response;
                $responseArray = json_decode($data, true);
                $historyArray = $responseArray['history'];
                if (!empty($historyArray)) {
                    $index = count($historyArray) - 1;
                    $operatorId = $historyArray[$index]['oper'];
                    if ($operatorId != $request->last_foreign_operator) {
                        $request->last_foreign_operator = $operatorId;
                        if ($request->update(true, ['last_foreign_operator'])) {
                            echo "Fixed id: " .$request->id ."\r\n";
                        }
                        else {
                            echo "Can't fix id: " .$request->id ."\r\n";
                        }
                    }
                }
                else {
                    $request->last_foreign_operator = -1;
                    if ($request->update(true, ['last_foreign_operator'])) {
                        echo "Fixed id: " .$request->id ."\r\n";
                    }
                    else {
                        echo "Can't fix id: " .$request->id ."\r\n";
                    }
                }
            }

            if (empty($requests)) {
                $repeat = false;
            }

            $offset += $limit;
        }
        echo "-=-=-= FINISH -=-=-=-";
    }


    public function down()
    {

    }
}
