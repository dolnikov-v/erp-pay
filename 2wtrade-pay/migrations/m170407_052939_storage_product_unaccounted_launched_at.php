<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170407_052939_storage_product_unaccounted_launched_at
 */
class m170407_052939_storage_product_unaccounted_launched_at extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('storage_product_unaccounted', 'cron_launched_at', $this->integer(11)->defaultValue(0));

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('storage_product_unaccounted', 'cron_launched_at');
    }
}
