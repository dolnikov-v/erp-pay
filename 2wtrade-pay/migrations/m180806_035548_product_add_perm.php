<?php
use app\components\PermissionMigration as Migration;

/**
 * Class m180806_035548_product_add_perm
 */
class m180806_035548_product_add_perm extends Migration
{
    protected $permissions = ['catalog.product.add'];

    protected $roles = [
        'support.manager' => ['catalog.product.add'],
        'technical.director' => ['catalog.product.add'],
        'security.manager' => ['catalog.product.add'],
    ];
}
