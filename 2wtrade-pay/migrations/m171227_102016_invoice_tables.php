<?php
use app\components\CustomMigration as Migration;

/**
 * Class m171227_102016_invoice_tables
 */
class m171227_102016_invoice_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('invoice', [
            'id' => $this->primaryKey(),
            'delivery_id' => $this->integer(),
            'name' => $this->string(255),
            'period_from' => $this->integer(),
            'period_to' => $this->integer(),
            'status' => $this->string(255),
            'user_id' => $this->integer(),
            'invoice_filename' => $this->string(255),
            'orders_filename' => $this->string(255),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $this->tableOptions);

        $this->addForeignKey(null, 'invoice', 'delivery_id', 'delivery', 'id', self::CASCADE, self::CASCADE);
        $this->addForeignKey(null, 'invoice', 'user_id', 'user', 'id', self::SET_NULL, self::CASCADE);


        $this->createTable('invoice_order', [
            'invoice_id' => $this->integer(),
            'order_id' => $this->integer(),
        ]);

        $this->addPrimaryKey($this->getPkName('invoice_order', [
            'invoice_id',
            'order_id'
        ]), 'invoice_order', ['invoice_id', 'order_id']);

        $this->addForeignKey($this->getFkName('invoice_order', 'invoice_id'), 'invoice_order', 'invoice_id', 'invoice', 'id', self::CASCADE, self::CASCADE);
        $this->addForeignKey($this->getFkName('invoice_order', 'order_id'), 'invoice_order', 'order_id', 'order', 'id', self::CASCADE, self::CASCADE);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('invoice_order');
        $this->dropTable('invoice');
    }
}
