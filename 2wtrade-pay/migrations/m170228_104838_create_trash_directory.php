<?php

use app\components\CustomMigration as Migration;
use app\modules\catalog\models\Autotrash;

/**
 * NPAY - 724
 */
class m170228_104838_create_trash_directory extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable(Autotrash::tableName(), [
            'id' => $this->primaryKey(),
            'template' => $this->string(255)->notNull(),
            'field' => "ENUM(
                'customer_full_name',
                'customer_ip',
                'customer_phone',
                'customer_address'
             ) NOT NULL DEFAULT 'customer_full_name'",
            'filter' => "ENUM('max_length', 'min_length', 'regexp') NOT NULL DEFAULT 'regexp'",
            'is_active' => $this->boolean()->defaultValue(true),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),

        ]);

        $this->insert('i18n_source_message', ['category' => 'common', 'message' => 'Автотреш']);
        $this->insert('i18n_source_message', ['category' => 'common', 'message' => 'Шаблон']);
        $this->insert('i18n_source_message', ['category' => 'common', 'message' => 'Добавить фильтр']);
        $this->insert('i18n_source_message', ['category' => 'common', 'message' => 'Фильтр не найден']);
        $this->insert('i18n_source_message', ['category' => 'common', 'message' => 'Фильтр успешно добавлен.']);
        $this->insert('i18n_source_message', ['category' => 'common', 'message' => 'Фильтр успешно отредактирован.']);
        $this->insert('i18n_source_message', ['category' => 'common', 'message' => 'Максимальная длина']);
        $this->insert('i18n_source_message', ['category' => 'common', 'message' => 'Минимальная длина']);
        $this->insert('i18n_source_message', ['category' => 'common', 'message' => 'Регулярное выражение']);
        $this->insert('i18n_source_message', ['category' => 'common', 'message' => 'Добавление фильтра']);
        $this->insert('i18n_source_message', ['category' => 'common', 'message' => 'Редактирование фильтра']);
        $this->insert('i18n_source_message', ['category' => 'common', 'message' => 'Сохранить фильтр']);
        $this->insert('i18n_source_message', ['category' => 'common', 'message' => 'Добавить фильтр']);
        $this->insert('i18n_source_message', ['category' => 'common', 'message' => 'Таблица с фильтрами']);
        $this->insert('i18n_source_message', ['category' => 'common', 'message' => 'Фильтр не найден.']);
        $this->insert('i18n_source_message', ['category' => 'common', 'message' => 'Фильтр удален.']);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable(Autotrash::tableName());
    }
}
