<?php
use app\components\PermissionMigration as Migration;

/**
 * Class m181004_053129_delivery_payment
 */
class m181004_053129_delivery_payment extends Migration
{

    protected $permissions = ['report.deliverypayment.index'];

    protected $roles = [
        'project.manager' => ['report.deliverypayment.index'],
        'finance.director' => ['report.deliverypayment.index'],
        'finance.manager' => ['report.deliverypayment.index'],
        'support.manager' => ['report.deliverypayment.index'],
        'technical.director' => ['report.deliverypayment.index'],
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('delivery_payment', [
            'id' => $this->primaryKey(),
            'delivery_id' => $this->integer(),
            'paid_at' => $this->integer(),
            'receiving_bank' => $this->string(),
            'payer' => $this->string(),
            'bank_recipient' => $this->string(),
            'remittance_information' => $this->string(),
            'income' => $this->double(),
            'currency' => $this->string(),
            'currency_rate' => $this->double(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addForeignKey(null, 'delivery_payment', 'delivery_id', 'delivery', 'id', self::CASCADE, self::CASCADE);

        parent::safeUp();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('delivery_payment');

        parent::safeDown();
    }
}
