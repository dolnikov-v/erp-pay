<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryApiClass;

/**
 * Class m161130_040256_add_api_class_dtdc
 */
class m161130_040256_add_api_class_dtdc extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $class = new DeliveryApiClass();
        $class->name = 'DTDCApi';
        $class->class_path = '/dtdc-api/src/DTDCApi.php';
        $class->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        DeliveryApiClass::find()
            ->where([
                    'name' => 'DTDCApi',
                    'class_path' => '/dtdc-api/src/DTDCApi.php'
                ])
            ->one()
            ->delete();
    }
}
