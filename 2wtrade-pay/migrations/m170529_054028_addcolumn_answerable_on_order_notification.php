<?php
use app\components\CustomMigration as Migration;
use app\modules\order\models\OrderNotification;
use app\modules\order\models\OrderNotificationRequest;

/**
 * Class m170529_054028_addcolumn_answerable_on_order_notification
 */
class m170529_054028_addcolumn_answerable_on_order_notification extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(OrderNotification::tableName(), 'answerable', 'integer(1) default 0');
        $this->addColumn(OrderNotificationRequest::tableName(), 'sms_notification_text', 'text');
        $this->addColumn(OrderNotificationRequest::tableName(), 'email_notification_text', 'text');
        $this->addColumn(OrderNotificationRequest::tableName(), 'answer_text', 'text');
        $this->addColumn(OrderNotificationRequest::tableName(), 'answered_at', 'integer(11)');
        $this->addColumn(OrderNotificationRequest::tableName(), 'status', 'varchar(20) default "new"');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(OrderNotification::tableName(), 'answerable');

        $this->dropColumn(OrderNotificationRequest::tableName(), 'sms_notification_text');
        $this->dropColumn(OrderNotificationRequest::tableName(), 'email_notification_text');
        $this->dropColumn(OrderNotificationRequest::tableName(), 'answer_text');
        $this->dropColumn(OrderNotificationRequest::tableName(), 'answered_at');
        $this->dropColumn(OrderNotificationRequest::tableName(), 'status');
    }
}
