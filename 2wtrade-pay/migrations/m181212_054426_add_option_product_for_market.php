<?php
use app\components\CustomMigration as Migration;

/**
 * Class m181212_054426_add_option_product_for_market
 */
class m181212_054426_add_option_product_for_market extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('product', 'shell_on_market', $this->boolean()->defaultValue(true));

//        отключаем товары для взрослых
        $this->execute('UPDATE product p INNER JOIN product_link_category plc ON plc.product_id = p.id
            INNER JOIN product_category pc ON pc.id = plc.category_id SET p.shell_on_market = false WHERE pc.id = :id', [':id' => 2]);

        $this->update('product', ['shell_on_market' => false], ['name' => 'Chocolate Slim']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('product', 'shell_on_market');
    }
}
