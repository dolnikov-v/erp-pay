<?php
use app\components\CustomMigration as Migration;
use app\modules\report\models\ReportAdcombo;

/**
 * Class m161020_045245_change_report_adcombo_column_type
 */
class m161020_045245_change_report_adcombo_column_type extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn(ReportAdcombo::tableName(), 'type',
            "ENUM ('hourly', 'daily', 'weekly', 'monthly') NOT NULL DEFAULT 'daily'");
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn(ReportAdcombo::tableName(), 'type', $this->integer(2)->notNull()->defaultValue(1));
    }
}
