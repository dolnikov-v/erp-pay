<?php

use app\components\CustomMigration;
use app\modules\order\models\Order;
use app\modules\delivery\models\DeliveryRequest;

/**
 * Class m170111_082501_add_delivery_partner_name_to_del_request
 */
class m170111_082501_add_delivery_partner_name_to_del_request extends CustomMigration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropColumn(Order::tableName(), 'delivery_partner_name');
        $this->addColumn(DeliveryRequest::tableName(), 'partner_name', $this->string(100));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(DeliveryRequest::tableName(), 'partner_name');
        $this->addColumn(Order::tableName(), 'delivery_partner_name', $this->string(100));
    }
}
