<?php

use app\components\CustomMigration as Migration;

/**
 * Class m190208_092720_chenge_description_in_notification_dz
 */
class m190208_092720_change_description_in_notification_dz extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->update('notification', [
            'description' => "Дебиторская задолженность.\n{text}",
        ], [
            'trigger' => 'debts.notification.three.month',
        ]);
        $this->alterColumn('skype_user', 'skype_id', $this->string()->notNull());
        $this->alterColumn('skype_user', 'conversation_id', $this->string()->notNull());
        $this->alterColumn('skype_user', 'skype_name', $this->string()->notNull());
        $this->alterColumn('skype_user', 'bot_id', $this->string()->notNull());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('skype_user', 'skype_id', $this->string(50));
        $this->alterColumn('skype_user', 'conversation_id', $this->string(50));
        $this->alterColumn('skype_user', 'skype_name', $this->string(50));
        $this->alterColumn('skype_user', 'bot_id', $this->string(50));
        $this->update('notification', [
            'description' => "Дебеторская задолжность.\n{text}",
        ], [
            'trigger' => 'debts.notification.three.month',
        ]);
    }
}
