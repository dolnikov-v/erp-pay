<?php
use app\components\PermissionMigration as Migration;

/**
 * Class m180425_064739_add_permission_for_finance_report_list
 */
class m180425_064739_add_permission_for_finance_report_list extends Migration
{
    protected $permissions = [
        'report.invoice.getfinancereports',
    ];

    protected $roles = [
        'finance.director' => [
            'report.invoice.getfinancereports',
        ],
        'finance.anager' => [
            'report.invoice.getfinancereports',
        ],
        'country.curator' => [
            'report.invoice.getfinancereports',
        ],
    ];

}
