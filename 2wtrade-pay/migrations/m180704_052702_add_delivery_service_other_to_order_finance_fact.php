<?php

use app\modules\order\models\OrderFinanceFact;
use yii\db\Migration;

/**
 * Handles adding delivery_service_other to table `order_finance_fact`.
 */
class m180704_052702_add_delivery_service_other_to_order_finance_fact extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn(OrderFinanceFact::tableName(), 'price_storage_currency_rate', $this->double()->after('price_storage_currency_id'));
        $this->addColumn(OrderFinanceFact::tableName(), 'price_fulfilment_currency_rate', $this->double()->after('price_fulfilment_currency_id'));
        $this->addColumn(OrderFinanceFact::tableName(), 'price_packing_currency_rate', $this->double()->after('price_packing_currency_id'));
        $this->addColumn(OrderFinanceFact::tableName(), 'price_package_currency_rate', $this->double()->after('price_package_currency_id'));
        $this->addColumn(OrderFinanceFact::tableName(), 'price_address_correction_currency_rate', $this->double()->after('price_address_correction_currency_id'));
        $this->addColumn(OrderFinanceFact::tableName(), 'price_cod_service_currency_rate', $this->double()->after('price_cod_service_currency_id'));
        $this->addColumn(OrderFinanceFact::tableName(), 'price_vat_currency_rate', $this->double()->after('price_vat_currency_id'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn(OrderFinanceFact::tableName(), 'price_storage_currency_rate');
        $this->dropColumn(OrderFinanceFact::tableName(), 'price_fulfilment_currency_rate');
        $this->dropColumn(OrderFinanceFact::tableName(), 'price_packing_currency_rate');
        $this->dropColumn(OrderFinanceFact::tableName(), 'price_package_currency_rate');
        $this->dropColumn(OrderFinanceFact::tableName(), 'price_address_correction_currency_rate');
        $this->dropColumn(OrderFinanceFact::tableName(), 'price_cod_service_currency_rate');
        $this->dropColumn(OrderFinanceFact::tableName(), 'price_vat_currency_rate');
    }
}
