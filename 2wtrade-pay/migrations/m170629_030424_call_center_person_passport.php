<?php
use app\components\CustomMigration as Migration;
use app\modules\salary\models\Person;
use app\models\Country;

/**
 * Class m170629_030424_call_center_person_passport
 */
class m170629_030424_call_center_person_passport extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Person::tableName(), 'passport_id_number', $this->string());
        $this->addColumn(Person::tableName(), 'corporate_email', $this->string());
        $this->dropForeignKey('fk_call_center_person_country_id', Person::tableName());
        $this->dropColumn(Person::tableName(), 'country_id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Person::tableName(), 'passport_id_number');
        $this->dropColumn(Person::tableName(), 'corporate_email');
        $this->addColumn(Person::tableName(), 'country_id', $this->integer());
        $this->addForeignKey(null, Person::tableName(), 'country_id', Country::tableName(), 'id', self::CASCADE, self::RESTRICT);
    }
}
