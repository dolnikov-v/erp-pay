<?php

use app\modules\delivery\models\DeliveryApiClass;
use yii\db\Migration;

/**
 * Handles adding ips to table `delivery_api_class`.
 */
class m170929_060721_add_ips_to_delivery_api_class extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $ips = new DeliveryApiClass();
        $ips->name = 'IPS';
        $ips->class_path = '/ips-api/src/ipsApi.php';
        $ips->save();
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        DeliveryApiClass::findOne(['name' => "IPS"])->delete();
    }
}
