<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryApiClass;
use app\modules\delivery\models\Delivery;
use app\models\Country;

/**
 * Class m170427_123111_add_delivery_traflead
 */
class m170427_123111_add_delivery_traflead extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        if (DeliveryApiClass::find()->where(
            ['name' => 'TrafLeadApi', 'class_path' => '/traflead-api/src/trafleadApi.php']
        )->exists()
        ) {
            $apiClass = DeliveryApiClass::findOne(
                ['name' => 'TrafLeadApi', 'class_path' => '/traflead-api/src/trafleadApi.php']
            );
        } else {
            $apiClass = new DeliveryApiClass();
        }
        $apiClass->name = 'TrafLeadApi';
        $apiClass->class_path = '/traflead-api/src/trafleadApi.php';
        $apiClass->active = '1';
        $apiClass->created_at = time();
        $apiClass->updated_at = time();
        $apiClass->save();

        if (Country::find()->where(
            [
                'name' => 'Индонезия 1',
                'char_code' => 'ID'
            ]
        )->exists()
        ) {
            $country = Country::find()->where(
                [
                    'name' => 'Индонезия 1',
                    'char_code' => 'ID'
                ]
            )->select('id')->one();
        } else {
            //нет страны
            return false;
        }

        if (Delivery::find()->where(
            [
                'name' => 'TrafLead',
                'char_code' => 'FGE'
            ]
        )->exists()
        ) {
            $delivery = Delivery::findOne(
                [
                    'name' => 'TrafLead',
                    'char_code' => 'TLD'
                ]
            );
        } else {
            $delivery = new Delivery();
        }
        $delivery->country_id = $country->id;
        $delivery->name = 'TrafLead';
        $delivery->char_code = 'TLD';
        $delivery->api_class_id = $apiClass->id;
        $delivery->active = '1';
        $delivery->auto_sending = '0';
        $delivery->save(false);
        return true;
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        if (Country::find()->where(
            [
                'name' => 'Индонезия 1',
                'char_code' => 'ID'
            ]
        )->exists()
        ) {
            $country = Country::find()->where(
                [
                    'name' => 'Индонезия 1',
                    'char_code' => 'ID'
                ]
            )->select('id')->one();
        } else {
            //нет страны
            return false;
        }

        DeliveryApiClass::find()
            ->where([
                'name' => 'TrafLeadApi',
                'class_path' => '/traflead-api/src/trafleadApi.php'
            ])
            ->one()
            ->delete();

        Delivery::findOne(
            [
                'name' => 'TrafLead',
                'country_id' => $country->id
            ]
        )->delete();
        return true;
    }
}
