<?php

use app\components\CustomMigration as Migration;
use app\models\Country;
use app\models\ProductLanding;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;

class m000000_000023_order extends Migration
{
    public function up()
    {
        $this->createTable(Order::tableName(), [
            'id' => $this->primaryKey(),

            'foreign_id' => $this->integer()->notNull(),

            'country_id' => $this->integer()->notNull(),
            'site_id' => $this->integer()->defaultValue(null),

            'status_id' => $this->integer()->defaultValue(null),

            'customer_ip' => $this->string(15)->defaultValue(null),
            'customer_code' => $this->string(50)->defaultValue(null),
            'customer_full_name' => $this->string(200)->notNull(),
            'customer_phone' => $this->string(15)->defaultValue(null),
            'customer_phone_add' => $this->string(15)->defaultValue(null),
            'customer_city' => $this->string(11)->defaultValue(null),
            'customer_address' => $this->string(500)->defaultValue(null),
            'customer_address_add' => $this->string(500)->defaultValue(null),
            'customer_zip' => $this->string(11)->defaultValue(null),

            'longitude' => $this->decimal(11, 8)->defaultValue(null),
            'latitude' => $this->decimal(11, 8)->defaultValue(null),

            'comment' => $this->string(200),

            'price' => $this->double()->defaultValue(0),
            'total' => $this->double()->defaultValue(0),
            'income' => $this->double()->defaultValue(null),

            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $this->tableOptions);

        $this->addForeignKey(null, Order::tableName(), 'country_id', Country::tableName(), 'id', self::NO_ACTION, self::NO_ACTION);
        $this->addForeignKey(null, Order::tableName(), 'site_id', 'product_site', 'id', self::NO_ACTION, self::NO_ACTION);
        $this->addForeignKey(null, Order::tableName(), 'status_id', OrderStatus::tableName(), 'id', self::NO_ACTION, self::NO_ACTION);
    }

    public function down()
    {
        $this->dropTable(Order::tableName());
    }
}
