<?php

use yii\db\Migration;

/**
 * Handles adding cities to table `delivery`.
 */
class m180827_094154_add_cities_to_delivery extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('delivery_zipcodes', 'cities', $this->text());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('delivery_zipcodes', 'cities');
    }
}