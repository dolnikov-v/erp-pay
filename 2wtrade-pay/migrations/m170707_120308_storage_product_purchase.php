<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170707_120308_storage_product_purchase
 */
class m170707_120308_storage_product_purchase extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('storage_product_purchase', [
            'id' => $this->primaryKey(),
            'country_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
            'plan_lead_day' => $this->decimal(15,2),
            'limit_days' => $this->integer()->defaultValue(14),
            'last_purchase_count' => $this->decimal(15,2),
            'last_purchase_date' => $this->integer(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
        $this->createIndex(null, 'storage_product_purchase', 'country_id');
        $this->createIndex(null, 'storage_product_purchase', 'product_id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('storage_product_purchase');
    }
}
