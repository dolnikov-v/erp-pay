<?php
use app\components\CustomMigration as Migration;
use app\models\Landing;

/**
 * Class m160421_105149_fix_landing_urls
 */
class m160421_105149_fix_landing_urls extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $landings = Landing::find()->all();

        foreach ($landings as $landing) {
            $landing->url = Landing::prepareUrl($landing->url);
            $landing->save(true, ['url']);
        }

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

    }
}
