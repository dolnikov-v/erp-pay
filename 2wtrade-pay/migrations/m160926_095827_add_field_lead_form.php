<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160926_095827_add_field_lead_form
 */
class m160926_095827_add_field_lead_form extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('order', 'adcombo_form', $this->enum(['short', 'long'])->after('delivery')->defaultValue(null));
        $this->createIndex(null, 'order', 'adcombo_form');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('order', 'adcombo_form');
    }
}
