<?php
use app\components\CustomMigration as Migration;
use app\models\AuthItem;
use yii\db\Query;


/**
 * Class m171019_100645_add_can_order_contacts
 */
class m171019_100645_add_can_order_contacts extends Migration
{
    public $newPermissions = [
        'order.customer_address',
        'order.customer_zip',
        'order.customer_city',
        'order.customer_province',
        'order.customer_city_code',
        'order.customer_street_house',
        'order.customer_street',
        'order.customer_house_number',
        'order.customer_phone',
        'order.customer_mobile',
        'order.customer_email',
        'order.customer_subdistrict',
        'order.customer_address_additional',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        foreach ($this->newPermissions as $oneItem) {
            $this->insert(AuthItem::tableName(), [
                'name' => $oneItem,
                'type' => 2,
                'description' => $oneItem,
                'created_at' => time(),
                'updated_at' => time()
            ]);
        }

        $roles = (new Query())->select(['parent'])->from('auth_item_child')->where(['child' => 'order.index.edit'])->all();
        if (is_array($roles)) {
            foreach ($roles as $role) {
                foreach ($this->newPermissions as $oneItem) {
                    $this->insert('auth_item_child', ['parent' => $role['parent'], 'child' => $oneItem]);
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        AuthItem::deleteAll(['name' => $this->newPermissions]);
        $this->delete('{{%auth_item_child}}', ['name' => $this->newPermissions]);
    }
}
