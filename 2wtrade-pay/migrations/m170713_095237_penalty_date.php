<?php
use app\components\CustomMigration as Migration;
use app\modules\salary\models\Penalty;

/**
 * Class m170713_095237_penalty_date
 */
class m170713_095237_penalty_date extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Penalty::tableName(), 'date', $this->date());
        $this->dropColumn(Penalty::tableName(), 'penalty_at');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Penalty::tableName(), 'date');
        $this->addColumn(Penalty::tableName(), 'penalty_at', $this->integer());
    }
}
