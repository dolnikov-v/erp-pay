<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160801_115655_delivery_request_dates
 */
class m160801_115655_delivery_request_dates extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('delivery_request', 'returned_at', $this->integer()->defaultValue(null));
        $this->addColumn('delivery_request', 'approved_at', $this->integer()->defaultValue(null));
        $this->addColumn('delivery_request', 'accepted_at', $this->integer()->defaultValue(null));
        $this->addColumn('delivery_request', 'paid_at', $this->integer()->defaultValue(null));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('delivery_request', 'paid_at');
        $this->dropColumn('delivery_request', 'accepted_at');
        $this->dropColumn('delivery_request', 'approved_at');
        $this->dropColumn('delivery_request', 'returned_at');
    }
}
