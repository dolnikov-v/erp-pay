<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170803_045748_add_issue_collector_roles
 */
class m170803_045748_add_issue_collector_roles extends Migration
{
    private $permissions = [
        'order.issuecollector.index',
        'order.issuecollector.edit',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        $rolePM = $auth->getRole('project.manager');
        foreach ($this->permissions as $permission) {
            $perm = $auth->createPermission($permission);
            $perm->description = $permission;
            $auth->add($perm);
            $auth->addChild($rolePM, $perm);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;
        foreach ($this->permissions as $permission) {
            $perm = $auth->getPermission($permission);
            $auth->remove($perm);
        }
    }
}
