<?php

use app\components\CustomMigration as Migration;
use app\models\Country;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryApiClass;

/**
 * Class m170519_115816_add_delivery_ping_indo2
 *
 * @property null|array|\app\modules\delivery\models\DeliveryApiClass $apiClassId
 * @property \app\models\Country $countryId
 */
class m170519_115816_add_delivery_ping_indo2 extends Migration
{
    /**
     * @return integer $countryId
     */
    private function getCountryId()
    {
        $countryId = Country::find()
            ->select('id')
            ->where(['slug' => 'id2'])
            ->scalar();
        return $countryId;
    }

    /**
     * @return integer $apiClassId
     */
    private function getApiClassId()
    {
        $apiClassId = DeliveryApiClass::find()
            ->select('id')
            ->where(['name' => 'pingdelivery'])
            ->scalar();

        return $apiClassId;
    }

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $delivery = new Delivery();
        $delivery->country_id = $this->getCountryId();
        $delivery->name = 'Ping Delivery';
        $delivery->char_code = 'PD';
        $delivery->auto_sending = 0;
        $delivery->api_class_id = $this->getApiClassId();
        $delivery->brokerage_active = 0;
        $delivery->active = 0;
        $delivery->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        Delivery::find()
            ->where([
                'country_id' => $this->getCountryId(),
                'name' => 'Ping Delivery',
            ])
            ->one()
            ->delete();
    }
}
