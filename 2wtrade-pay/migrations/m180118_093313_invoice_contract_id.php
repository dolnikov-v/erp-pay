<?php
use app\components\PermissionMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m180118_093313_invoice_contract_id
 */
class m180118_093313_invoice_contract_id extends Migration
{
    protected $permissions = [
        'report.invoice.reloadinvoicelist',
        'report.invoice.closeinvoice',
    ];

    protected $roles = [
        'finance.manager' => [
            'report.invoice.index',
            'report.invoice.createinvoice',
            'report.invoice.regenerateinvoice',
            'report.invoice.deleteinvoice',
            'report.invoice.downloadinvoice',
            'report.invoice.currencyrate',
            'report.invoice.reloadinvoicelist',
            'report.invoice.closeinvoice',
            'report.invoice.index.invoicelist',
        ],
        'finance.director' => [
            'report.invoice.index',
            'report.invoice.createinvoice',
            'report.invoice.regenerateinvoice',
            'report.invoice.deleteinvoice',
            'report.invoice.downloadinvoice',
            'report.invoice.currencyrate',
            'report.invoice.reloadinvoicelist',
            'report.invoice.closeinvoice',
            'report.invoice.index.invoicelist',
        ],
        'admin' => [
            'report.invoice.index',
            'report.invoice.createinvoice',
            'report.invoice.regenerateinvoice',
            'report.invoice.deleteinvoice',
            'report.invoice.downloadinvoice',
            'report.invoice.currencyrate',
            'report.invoice.reloadinvoicelist',
            'report.invoice.closeinvoice',
            'report.invoice.index.invoicelist',
        ],
        'project.manager' => [
            'report.invoice.index',
            'report.invoice.createinvoice',
            'report.invoice.regenerateinvoice',
            'report.invoice.deleteinvoice',
            'report.invoice.downloadinvoice',
            'report.invoice.currencyrate',
            'report.invoice.reloadinvoicelist',
            'report.invoice.closeinvoice',
            'report.invoice.index.invoicelist',
        ],
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('invoice', 'contract_id', $this->integer()->after('delivery_id'));
        $this->addForeignKey($this->getFkName('invoice', 'contract_id'), 'invoice', 'contract_id', 'delivery_contract', 'id', self::SET_NULL, self::CASCADE);

        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_REPORT_GENERATE_INVOICES]);
        if (!$crontabTask) {
            $crontabTask = new CrontabTask();
            $crontabTask->name = CrontabTask::TASK_REPORT_GENERATE_INVOICES;
            $crontabTask->description = "Генерация инвойсов";
            $crontabTask->active = 1;
            $crontabTask->save();
        }

        $this->addColumn('delivery', 'bank_name', $this->string(255));
        $this->addColumn('delivery', 'bank_account', $this->string(255));
        $this->addColumn('delivery', 'address', $this->string(500));

        parent::safeUp();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey($this->getFkName('invoice', 'contract_id'), 'invoice');
        $this->dropColumn('invoice', 'contract_id');

        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_REPORT_GENERATE_INVOICES]);
        if ($crontabTask instanceof CrontabTask) {
            $crontabTask->delete();
        }

        $this->dropColumn('delivery', 'bank_name');
        $this->dropColumn('delivery', 'bank_account');
        $this->dropColumn('delivery', 'address');

        parent::safeDown();
    }
}
