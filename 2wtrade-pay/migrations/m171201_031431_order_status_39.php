<?php
use app\components\CustomMigration as Migration;
use app\modules\order\models\OrderStatus;
use yii\db\Query;

/**
 * Class m171201_031431_order_status_39
 */
class m171201_031431_order_status_39 extends Migration
{
    const TEXT = [
        'Колл-центр (на проверке)' => 'Call center (checking)',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $query = new Query();

        $this->insert(OrderStatus::tableName(), ['sort' => 39, 'name' => 'Колл-центр (на проверке)', 'comment' => null, 'group' => 'call_center', 'type' => 'middle']);

        foreach ($this::TEXT as $ruText => $enText) {

            $is = $query->select('id')
                ->from('i18n_source_message')
                ->where(['category' => 'common', 'message' => $ruText])->one();

            if (!$is) {
                $this->insert('i18n_source_message', ['category' => 'common', 'message' => $ruText]);
                $id = Yii::$app->db->getLastInsertID();
                $this->insert('i18n_message', ['id' => $id, 'language' => 'en-US', 'translation' => $enText]);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete(OrderStatus::tableName(), ['name' => 'Колл-центр (на проверке)']);

        foreach ($this::TEXT as $ruText => $enText) {
            $this->delete('i18n_source_message', ['category' => 'common', 'message' => $ruText]);
        }
    }
}
