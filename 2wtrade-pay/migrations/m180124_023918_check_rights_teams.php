<?php
use app\components\CustomMigration as Migration;
use app\modules\reportoperational\models\Team;
use app\modules\reportoperational\models\TeamCountryUser;

/**
 * Class m180124_023918_check_rights_teams
 */
class m180124_023918_check_rights_teams extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $roleCurator = 'country.curator';
        $authRole = Yii::$app->authManager->getRole($roleCurator);

        $data = Team::find()->all();
        if (is_array($data)) {
            foreach ($data as $item) {
                $isFounded = false;
                $roles = Yii::$app->authManager->getRolesByUser($item->leader_id);
                if (is_array($roles)) {
                    foreach ($roles as $role) {
                        if (strpos($role->name, $roleCurator) !== false) {
                            $isFounded = true;
                        }
                    }
                }
                if (!$isFounded) {
                    Yii::$app->authManager->assign($authRole, $item->leader_id);
                }
            }
        }

        $roleController = 'controller.analyst';
        $authRoleController = Yii::$app->authManager->getRole($roleController);

        $data = TeamCountryUser::find()->groupBy(['user_id'])->all();
        if (is_array($data)) {
            foreach ($data as $item) {
                $isFounded = false;
                $roles = Yii::$app->authManager->getRolesByUser($item->user_id);
                if (is_array($roles)) {
                    foreach ($roles as $role) {
                        if (strpos($role->name, $roleController) !== false) {
                            $isFounded = true;
                        }
                    }
                }
                if (!$isFounded) {
                    Yii::$app->authManager->assign($authRoleController, $item->user_id);
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

    }
}
