<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170910_145544_call_center_process_table
 */
class m170910_145544_call_center_process_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('cron_process_list', [
            'request_id' => $this->integer()->notNull(), // заявка
            'cron_launched_at' => $this->integer()->notNull(), // старт крона
            'type_cron' => $this->string(300)->notNull(), // тип крона
        ]);

        $this->createIndex(null, 'cron_process_list', 'request_id');
        $this->createIndex(null, 'cron_process_list', 'type_cron');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('cron_process_list');
    }
}
