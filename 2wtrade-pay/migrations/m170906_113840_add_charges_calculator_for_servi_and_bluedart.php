<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryChargesCalculator;

/**
 * Class m170906_113840_add_charges_calculator_for_servi_and_bluedart
 */
class m170906_113840_add_charges_calculator_for_servi_and_bluedart extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert(DeliveryChargesCalculator::tableName(), [
            'name' => 'Bluedart',
            'class_path' => 'app\modules\delivery\components\charges\bluedart\Bluedart',
            'active' => 1,
            'created_at' => time(),
        ]);
        $this->insert(DeliveryChargesCalculator::tableName(), [
            'name' => 'ServiEntrego',
            'class_path' => 'app\modules\delivery\components\charges\servientrego\ServiEntrego',
            'active' => 1,
            'created_at' => time(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete(DeliveryChargesCalculator::tableName(), ['name' => 'Bluedart']);
        $this->delete(DeliveryChargesCalculator::tableName(), ['name' => 'ServiEntrego']);
    }
}
