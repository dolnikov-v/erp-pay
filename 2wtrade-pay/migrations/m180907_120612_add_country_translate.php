<?php
use app\components\CustomMigration as Migration;
use yii\db\Query;

/**
 * Class m180907_120612_add_country_translate
 */
class m180907_120612_add_country_translate extends Migration
{
    private $country = [
        ['name' => 'Colombia (distributor)', 'name_en' => 'Colombia (distributor)', 'name_es' => 'Colombia (distribuidor)'],
        ['name' => 'Chile (distributor)', 'name_en' => 'Chile (distributor)', 'name_es' => 'Chile (distribuidor)'],
        ['name' => 'Oman', 'name_en' => 'Oman', 'name_es' => 'Omán'],
        ['name' => 'Аргентина', 'name_en' => 'Argentina', 'name_es' => 'Argentina'],
        ['name' => 'Бангладеш', 'name_en' => 'Bangladesh', 'name_es' => 'Bangladesh'],
        ['name' => 'Бахрейн', 'name_en' => 'Bahrain', 'name_es' => 'Bahrein'],
        ['name' => 'Бельгия', 'name_en' => 'Belgium', 'name_es' => 'Bélgica'],
        ['name' => 'Болгария', 'name_en' => 'Bulgaria', 'name_es' => 'Bulgaria'],
        ['name' => 'Боливия', 'name_en' => 'Bolivia', 'name_es' => 'Bolivia'],
        ['name' => 'Босния', 'name_en' => 'Bosnia & Herzegovina', 'name_es' => 'Bosnia'],
        ['name' => 'Великобритания', 'name_en' => 'United Kingdom', 'name_es' => 'Reino Unido'],
        ['name' => 'Виетдина', 'name_en' => 'Vietdin', 'name_es' => 'Vietdin'],
        ['name' => 'Вьетнам', 'name_en' => 'Vietnam', 'name_es' => 'Vietnam'],
        ['name' => 'Вьетнам 2', 'name_en' => 'Vietnam 2', 'name_es' => 'Vietnam 2'],
        ['name' => 'Гана', 'name_en' => 'Ghana', 'name_es' => 'Ghana'],
        ['name' => 'Гватемала', 'name_en' => 'Guatemala', 'name_es' => 'Guatemala'],
        ['name' => 'Гонконг', 'name_en' => 'Hong Kong', 'name_es' => 'Hong Kong'],
        ['name' => 'Египет', 'name_en' => 'Egypt', 'name_es' => 'Egipto'],
        ['name' => 'Индия', 'name_en' => 'India', 'name_es' => 'India'],
        ['name' => 'Индонезия 1', 'name_en' => 'Indonesia 1', 'name_es' => 'Indonesia 1'],
        ['name' => 'Индонезия 2', 'name_en' => 'Indonesia 2', 'name_es' => 'Indonesia 2'],
        ['name' => 'Иордания', 'name_en' => 'Jordan', 'name_es' => 'Jordania'],
        ['name' => 'Иран', 'name_en' => 'Iran', 'name_es' => 'Irán'],
        ['name' => 'Испания', 'name_en' => 'Spain', 'name_es' => 'España'],
        ['name' => 'Камбоджа', 'name_en' => 'Cambodia', 'name_es' => 'Camboya'],
        ['name' => 'Катар', 'name_en' => 'Qatar', 'name_es' => 'Qatar'],
        ['name' => 'Китай', 'name_en' => 'China', 'name_es' => 'China'],
        ['name' => 'Китай 2', 'name_en' => 'China 2', 'name_es' => 'China 2'],
        ['name' => 'Колумбия', 'name_en' => 'Colombia', 'name_es' => 'Colombia'],
        ['name' => 'Кувейт', 'name_en' => 'Kuwait', 'name_es' => 'Kuwait'],
        ['name' => 'Лаос', 'name_en' => 'Laos', 'name_es' => 'Laos'],
        ['name' => 'Малайзия 1', 'name_en' => 'Malaysia 1', 'name_es' => 'Malasia 1'],
        ['name' => 'Малайзия 2', 'name_en' => 'Malaysia 2', 'name_es' => 'Malasia 2'],
        ['name' => 'Мексика', 'name_en' => 'Mexico', 'name_es' => 'México'],
        ['name' => 'Нигерия', 'name_en' => 'Nigeria', 'name_es' => 'Nigeria'],
        ['name' => 'Нидерланды', 'name_en' => 'Netherlands', 'name_es' => 'Holanda'],
        ['name' => 'Нидерланды 2', 'name_en' => 'Netherlands 2', 'name_es' => 'Holanda 2'],
        ['name' => 'Никарагуа', 'name_en' => 'Nicaragua', 'name_es' => 'Nicaragua'],
        ['name' => 'ОАЭ', 'name_en' => 'United Arab Emirates', 'name_es' => 'Emiratos Árabes Unidos'],
        ['name' => 'Пакистан', 'name_en' => 'Pakistan', 'name_es' => 'Pakistán'],
        ['name' => 'Перу', 'name_en' => 'Peru', 'name_es' => 'Perú'],
        ['name' => 'Россия', 'name_en' => 'Russia', 'name_es' => 'Federación Rusa'],
        ['name' => 'Румыния', 'name_en' => 'Romania', 'name_es' => 'Rumania'],
        ['name' => 'Саудовская Аравия', 'name_en' => 'Saudi Arabia', 'name_es' => 'Arabia Saudita'],
        ['name' => 'Сербия', 'name_en' => 'Serbia', 'name_es' => 'Serbia'],
        ['name' => 'Сингапур', 'name_en' => 'Singapore', 'name_es' => 'Singapur'],
        ['name' => 'США', 'name_en' => 'United States', 'name_es' => 'Estados Unidos'],
        ['name' => 'Тайвань', 'name_en' => 'Taiwan', 'name_es' => 'Taiwan'],
        ['name' => 'Тайланд 1', 'name_en' => 'Thailand 1', 'name_es' => 'Tailandia 1'],
        ['name' => 'Тайланд 2', 'name_en' => 'Thailand 2', 'name_es' => 'Tailandia 2'],
        ['name' => 'Тунис', 'name_en' => 'Tunisia', 'name_es' => 'Túnez'],
        ['name' => 'Уганда', 'name_en' => 'Uganda', 'name_es' => 'Uganda'],
        ['name' => 'Филиппины', 'name_en' => 'Philippines', 'name_es' => 'Filipinas'],
        ['name' => 'Филиппины 2', 'name_en' => 'Philippines 2', 'name_es' => 'Filipinas 2'],
        ['name' => 'Чили', 'name_en' => 'Chile', 'name_es' => 'Chile'],
        ['name' => 'Шри-Ланка', 'name_en' => 'Sri Lanka', 'name_es' => 'Sri Lanka'],
        ['name' => 'ЮАР', 'name_en' => 'South Africa', 'name_es' => 'Sudáfrica'],
        ['name' => 'Южная Корея', 'name_en' => 'South Korea', 'name_es' => 'Corea del Sur'],
        ['name' => 'Япония', 'name_en' => 'Japan', 'name_es' => 'Japón']
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        $this->execute('DELETE t1 FROM i18n_source_message t1 INNER JOIN i18n_source_message t2 WHERE t1.id > t2.id AND t1.category = t2.category and t1.message = t2.message');

        foreach ($this->country as $country) {

            $idSource = (new Query())->select('id')
                ->from('i18n_source_message')
                ->where(['category' => 'common'])
                ->andWhere(['message' => $country['name']])
                ->scalar();

            if (!$idSource) {
                $this->insert('i18n_source_message', ['category' => 'common', 'message' => $country['name']]);
                $idSource = Yii::$app->db->getLastInsertID();
            }

            if ($idSource) {

                $is = (new Query())->select('id')
                    ->from('i18n_message')
                    ->where(['id' => $idSource])
                    ->andWhere(['language' => 'en-US'])
                    ->exists();

                if (!$is) {
                    $this->insert('i18n_message', [
                        'id' => $idSource,
                        'language' => 'en-US',
                        'translation' => $country['name_en']
                    ]);
                } else {
                    $this->update('i18n_message', [
                        'translation' => $country['name_en']
                    ], ['id' => $idSource, 'language' => 'en-US']);
                }


                $is = (new Query())->select('id')
                    ->from('i18n_message')
                    ->where(['id' => $idSource])
                    ->andWhere(['language' => 'es-ES'])
                    ->exists();

                if (!$is) {
                    $this->insert('i18n_message', [
                        'id' => $idSource,
                        'language' => 'es-ES',
                        'translation' => $country['name_es']
                    ]);
                } else {
                    $this->update('i18n_message', [
                        'translation' => $country['name_es']
                    ], ['id' => $idSource, 'language' => 'es-ES']);
                }
            }
        }
    }
}