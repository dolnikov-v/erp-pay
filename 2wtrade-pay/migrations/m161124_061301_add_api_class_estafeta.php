<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryApiClass;

/**
 * Class m161117_102718_add_api_class_nex
 */
class m161124_061301_add_api_class_estafeta extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $class = new DeliveryApiClass();
        $class->name = 'EstafetaApi';
        $class->class_path = '/estafeta-api/src/EstafetaApi.php';
        $class->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        DeliveryApiClass::find()
            ->where([
                'name' => 'EstafetaApi',
                'class_path' => '/estafeta-api/src/EstafetaApi.php'
            ])
            ->one()
            ->delete();
    }
}
