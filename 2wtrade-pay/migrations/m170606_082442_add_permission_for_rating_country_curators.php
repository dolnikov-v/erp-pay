<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170606_082442_add_permission_for_rating_country_curators
 */
class m170606_082442_add_permission_for_rating_country_curators extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}', array(
            'name' => 'rating.countrycurators.index',
            'type' => '2',
            'description' => 'rating.countrycurators.index'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'country.curator',
            'child' => 'rating.countrycurators.index'
        ));
    }



    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->authManager->itemChildTable, ['child' => 'rating.countrycurators.index', 'parent' => 'country.curator']);
        $this->delete('{{%auth_item}}', ['name' => 'rating.countrycurators.index']);
    }

}
