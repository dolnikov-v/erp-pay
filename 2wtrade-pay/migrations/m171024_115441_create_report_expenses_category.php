<?php
use app\components\CustomMigration as Migration;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * Class m171013_115441_create_report_expenses_category
 */
class m171024_115441_create_report_expenses_category extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $categoryList = [
            'call_center' => 'Колл-центр',
            'product' => 'Товар',
            'branch' => 'Филиал',
            'story' => 'Склад',
        ];

        $this->createTable('report_expenses_category', [
            'id' => $this->primaryKey(),
            'code' => $this->string(256),
            'name' => $this->string(256),
            'active' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        foreach ($categoryList as $code => $name) {
            $this->insert('report_expenses_category', [
                'code' => $code,
                'name' => $name,
                'active' => 1,
                'created_at' => time(),
                'updated_at' => time()
            ]);
        }

        $this->createTable('report_expenses_country_category', [
            'id' => $this->primaryKey(),
            'active' => $this->integer(),
            'name' => $this->string(256),
            'category_id' => $this->integer(),
            'country_id' => $this->integer(),
            'office_id' => $this->integer(),
            'delivery_id' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $countries = (new Query())->from('report_expenses_country')->indexBy('id')->all();
        $categories = (new Query())->from('report_expenses_category')->indexBy('code')->all();

        // добавляем все категории для всех страны
        foreach ($countries as $country) {
            foreach ($categories as $category) {
                $this->insert('report_expenses_country_category', [
                    'name' => $category['name'],
                    'active' => 1,
                    'category_id' => $category['id'],
                    'country_id' => $country['id'],
                    'created_at' => time(),
                    'updated_at' => time()
                ]);
            }
        }

        $expensesItems = (new Query())->from('report_expenses_item')->indexBy('id')->all();
        $expensesCountryItems = (new Query())->from('report_expenses_country_item')->indexBy('id')->all();
        $itemCategories = [];
        foreach ($expensesItems as $expensesItem) {
            $category = $expensesItem['category'] ?: 'root';
            $itemCategories[$category][] = $expensesItem['id'];
        }

        $this->alterColumn('report_expenses_item', 'category', $this->integer());
        $this->renameColumn('report_expenses_item', 'category', 'category_id');

        // Записываем группы в основные расходы
        foreach ($itemCategories as $categoryName => $list) {
            $categoryId = isset($categories[$categoryName]) ? $categories[$categoryName]['id'] : null;
            $this->update('report_expenses_item', ['category_id' => $categoryId], ['id' => $list]);
        }

        $this->addColumn('report_expenses_country_item', 'name', $this->string(256));
        $this->addColumn('report_expenses_country_item', 'category_id', $this->integer());

        // Выбераем все основные расходы уже с группами
        $expensesItems = (new Query())->from('report_expenses_item')->indexBy('id')->all();
        $expensesCountryCategories = (new Query())->from('report_expenses_country_category')->indexBy('id')->all();
        $expensesCountryCategoryGroups = ArrayHelper::index($expensesCountryCategories, 'country_id', ['category_id']);
        foreach ($expensesCountryItems as $expensesCountryItem) {
            $parentId = $expensesCountryItem['parent_id'];
            $countryId = $expensesCountryItem['country_id'];
            if ($parentId && isset($expensesItems[$parentId])) {
                $expensesItem = isset($expensesItems[$parentId]) ? $expensesItems[$parentId] : null;
                if ($categoryId = $expensesItem['category_id']) {
                    $category = (isset($expensesCountryCategoryGroups[$categoryId][$countryId])) ? $expensesCountryCategoryGroups[$categoryId][$countryId] : null;
                    if ($category) {
                        $this->update('report_expenses_country_item',
                            ['category_id' => $category['category_id']],
                            ['id' => $expensesCountryItem['id']]
                        );
                    }
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('report_expenses_category');
        $this->dropTable('report_expenses_country_category');
        $this->dropColumn('report_expenses_country_item', 'category_id');
        $this->dropColumn('report_expenses_country_item', 'name');

        $this->alterColumn('report_expenses_item', 'category_id', $this->string(256));
        $this->renameColumn('report_expenses_item', 'category_id', 'category');
    }
}
