<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170913_105454_add_permission_restriction_order_by_source
 */
class m170913_105454_add_permission_restriction_order_by_source extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}',
        [
            'name'=>'access.user.setsource',
            'type' => '2',
            'description' => 'access.user.setsource',
            'created_at' => time(),
            'updated_at' => time()
        ]);

        $roles = Yii::$app->authManager->getRoles();
        foreach ($roles as $role) {
            $this->insert($this->authManager->itemChildTable,
            [
                'parent' => $role->name,
                'child' => 'access.user.setsource'
            ]);
        }

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->authManager->itemTable, ['name' => 'access.user.setsource']);
    }
}