<?php
use app\components\PermissionMigration as Migration;

/**
 * Class m180129_064128_person_edit_ajax
 */
class m180129_064128_person_edit_ajax extends Migration
{
    protected $permissions = ['salary.person.getshifts'];

    protected $roles = [
        'project.manager' => [
            'salary.person.getshifts'
        ],
        'admin' => [
            'salary.person.getshifts'
        ],
        'callcenter.hr' => [
            'salary.person.getshifts'
        ],
        'callcenter.manager' => [
            'salary.person.getshifts'
        ],
        'salaryproject.clerk' => [
            'salary.person.getshifts'
        ],
    ];
}
