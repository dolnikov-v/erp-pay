<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170207_122600_collation_fix
 */
class m170207_122600_collation_fix extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->execute("ALTER TABLE `marketing_list` CHANGE COLUMN `name` `name` VARCHAR(64) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci';");
        $this->execute("ALTER TABLE `storage_part_moves` CHANGE COLUMN `type` `type` ENUM('move','remove') NULL DEFAULT 'move' COLLATE 'utf8_unicode_ci';");
        $this->execute("ALTER TABLE `storage_part_moves` CHANGE COLUMN `comment` `comment` TEXT NULL COLLATE 'utf8_unicode_ci';");
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

    }
}
