<?php
use app\components\CustomMigration as Migration;
use app\modules\salary\models\Office;
use app\modules\salary\models\WorkingShift;
use app\modules\salary\models\Person;


/**
 * Class m170725_113100_call_center_working_shift_data
 */
class m170725_113100_call_center_working_shift_data extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $callCenterOffices = Office::find()->all();
        foreach ($callCenterOffices as $office) {
            $callCenterWorkingShift = WorkingShift::find()->byOfficeId($office->id)->one();
            if (!$callCenterWorkingShift) {
                $callCenterWorkingShift = new WorkingShift();
                $callCenterWorkingShift->office_id = $office->id;
                $callCenterWorkingShift->name = 'Рабочие дни КЗоТ';

                if ($office->working_hours_per_week == 40) {
                    $callCenterWorkingShift->working_mon = '10:00-19:00';
                    $callCenterWorkingShift->working_tue = '10:00-19:00';
                    $callCenterWorkingShift->working_wed = '10:00-19:00';
                    $callCenterWorkingShift->working_thu = '10:00-19:00';
                    $callCenterWorkingShift->working_fri = '10:00-19:00';
                    $callCenterWorkingShift->working_sat = '';
                }
                if ($office->working_hours_per_week == 44) {
                    $callCenterWorkingShift->working_mon = '10:00-19:00';
                    $callCenterWorkingShift->working_tue = '10:00-19:00';
                    $callCenterWorkingShift->working_wed = '10:00-19:00';
                    $callCenterWorkingShift->working_thu = '10:00-19:00';
                    $callCenterWorkingShift->working_fri = '10:00-19:00';
                    $callCenterWorkingShift->working_sat = '10:00-15:00';
                }
                if ($office->working_hours_per_week == 48) {
                    $callCenterWorkingShift->working_mon = '10:00-19:00';
                    $callCenterWorkingShift->working_tue = '10:00-19:00';
                    $callCenterWorkingShift->working_wed = '10:00-19:00';
                    $callCenterWorkingShift->working_thu = '10:00-19:00';
                    $callCenterWorkingShift->working_fri = '10:00-19:00';
                    $callCenterWorkingShift->working_sat = '10:00-19:00';
                }
                $callCenterWorkingShift->lunch_time = '14:00-15:00';
                $callCenterWorkingShift->working_sun = '';
                $callCenterWorkingShift->save();
            }

            if ($callCenterWorkingShift->id) {
                Person::updateAll(
                    ['working_shift_id' => $callCenterWorkingShift->id],
                    ['office_id' => $office->id]
                );
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        Person::updateAll(
            ['working_shift_id' => null]
        );
    }
}
