<?php
use app\components\PermissionMigration;


/**
 * Class m180409_043404_accessusersetdelivery
 */
class m180409_043404_accessusersetdelivery extends PermissionMigration
{
    protected $permissions = [
        'access.user.setdelivery'
    ];

    protected $roles = [
        'technical.director' => [
            'access.user.setdelivery'
        ],
        'security.manager' => [
            'access.user.setdelivery'
        ],
    ];
}
