<?php

use app\components\CustomMigration as Migration;
use app\modules\order\models\OrderStatus;
use app\modules\delivery\models\Delivery;
use app\modules\administration\models\CrontabTask;

/**
 * Handles adding waiting to table `delivery_status`.
 */
class m170302_121904_add_waiting_to_delivery_status extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert(OrderStatus::tableName(),
            ['name' => 'Логистика (отложено)', 'group' => 'logistic', 'type' => 'middle']);
        $this->addColumn(Delivery::tableName(), 'max_delivery_period', $this->integer());
        $this->insert(CrontabTask::tableName(), [
            'name' => 'delivery_checking_delivery_period',
            'description' => 'Перевод заказов из статуса Логистика (отложено)'
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete(OrderStatus::tableName(),
            ['name' => 'Логистика (отложено)', 'group' => 'logistic', 'type' => 'middle']);
        $this->dropColumn(Delivery::tableName(), 'max_delivery_period');
        $this->delete(CrontabTask::tableName(), [
            'name' => 'delivery_checking_delivery_period',
            'description' => 'Перевод заказов из статуса Логистика (отложено)'
        ]);
    }
}
