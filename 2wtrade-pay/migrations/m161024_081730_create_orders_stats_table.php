<?php

use app\components\CustomMigration as Migration;

/**
 * Handles the creation for table `orders_stats_table`.
 */
class m161024_081730_create_orders_stats_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('order_stats', [
            'id' => $this->primaryKey(),
            'metric' => $this->string(255),
            'value' => $this->integer(11),
            'created_at' => $this->integer(11),
        ]);

        $this->createIndex(null, "order_stats", "metric");
        $this->createIndex(null, "order_stats", "created_at");
    }


    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('order_stats');
    }
}
