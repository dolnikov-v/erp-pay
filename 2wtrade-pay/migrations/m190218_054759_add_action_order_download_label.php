<?php

use app\components\PermissionMigration as Migration;

/**
 * Class m190218_054759_add_action_order_download_label
 */
class m190218_054759_add_action_order_download_label extends Migration
{
    protected $permissions = ['order.index.downloadlabel'];

    protected $roles = [
        'admin' => ['order.index.downloadlabel'],
        'auditor' => ['order.index.downloadlabel'],
        'controller.analyst' => ['order.index.downloadlabel'],
        'country.curator' => ['order.index.downloadlabel'],
        'delivery.manager' => ['order.index.downloadlabel'],
        'development.director' => ['order.index.downloadlabel'],
        'implant' => ['order.index.downloadlabel'],
        'junior.logist' => ['order.index.downloadlabel'],
        'logist' => ['order.index.downloadlabel'],
        'operational.director' => ['order.index.downloadlabel'],
        'partner' => ['order.index.downloadlabel'],
        'partner operator' => ['order.index.downloadlabel'],
        'project.manager' => ['order.index.downloadlabel'],
        'support.manager' => ['order.index.downloadlabel'],
        'technical.director' => ['order.index.downloadlabel'],
    ];
}
