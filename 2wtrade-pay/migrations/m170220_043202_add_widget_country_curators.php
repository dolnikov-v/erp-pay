<?php
use app\components\CustomMigration as Migration;
use app\modules\widget\models\WidgetType;
use app\modules\widget\models\WidgetRole;

/**
 * Class m170220_043202_add_widget_country_curators
 */
class m170220_043202_add_widget_country_curators extends Migration
{
    public function safeUp()
    {
        $this->insert(WidgetType::tableName(), [
            'code' => 'top_country_curators',
            'name' => 'Топ территориальных менеджеров',
            'status' => WidgetType::STATUS_ACTIVE
        ]);

        $type = WidgetType::find()
            ->where(['code' => 'top_country_curators'])
            ->one();

        $this->insert(WidgetRole::tableName(), [
            'role' => 'business_analyst',
            'type_id' => $type->id
        ]);

        $this->insert('i18n_source_message', ['category' => 'common', 'message' => 'Топ территориальных менеджеров']);
    }


    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $type = WidgetType::find()
            ->where(['code' => 'top_country_curators'])
            ->one();

        $this->delete(WidgetRole::tableName(), [
            'role' => 'business_analyst',
            'type_id' => $type->id,
        ]);

        $this->delete(WidgetType::tableName(), ['code' => 'top_country_curators']);
    }
}
