<?php
use app\components\CustomMigration as Migration;
use app\models\Notification;
use app\modules\administration\models\CrontabTask;
use app\modules\salary\models\Office;

/**
 * Class m170918_042122_add_notification_for_adcombo_and_cc_approve
 */
class m170918_042122_add_notification_for_adcombo_and_cc_approve extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Office::tableName(), 'work_time', $this->string(255));
        $notification = Notification::findOne(['trigger' => Notification::TRIGGER_CALL_CENTER_SMALL_APPROVE]);

        if (!($notification instanceof Notification)) {
            $notification = new Notification([
                'description' => 'Низкий апрув. За последние {minutes} минут в странах {countries} апрувлено всего {count} заказов',
                'trigger' => Notification::TRIGGER_CALL_CENTER_SMALL_APPROVE,
                'type' => Notification::TYPE_DANGER,
                'group' => Notification::GROUP_SYSTEM,
                'active' => Notification::ACTIVE,
            ]);
            $notification->save(false);
        }

        $notification = Notification::findOne(['trigger' => Notification::TRIGGER_ADCOMBO_DONT_GET_STATUSES]);

        if (!($notification instanceof Notification)) {
            $notification = new Notification([
                'description' => 'В странах {countries} - {count} заказов были апрувлены более {minutes} минут назад, и адкомбо не получило по ним статус',
                'trigger' => Notification::TRIGGER_ADCOMBO_DONT_GET_STATUSES,
                'type' => Notification::TYPE_DANGER,
                'group' => Notification::GROUP_SYSTEM,
                'active' => Notification::ACTIVE,
            ]);
            $notification->save(false);
        }

        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_CALL_CENTER_NOT_APPROVE_NOTIFICATION]);
        if (!$crontabTask) {
            $crontabTask = new CrontabTask();
            $crontabTask->name = CrontabTask::TASK_CALL_CENTER_NOT_APPROVE_NOTIFICATION;
            $crontabTask->description = "Уведомление о долгом отсутствии апрувнутых заказов";
            $crontabTask->save();
        }

        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_ADCOMBO_NOT_GET_STATUSES]);
        if (!$crontabTask) {
            $crontabTask = new CrontabTask();
            $crontabTask->name = CrontabTask::TASK_ADCOMBO_NOT_GET_STATUSES;
            $crontabTask->description = "Уведомлении о неполучении статусов адкомбо";
            $crontabTask->save();
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Office::tableName(), 'work_time');

        $notification = Notification::findOne(['trigger' => Notification::TRIGGER_CALL_CENTER_SMALL_APPROVE]);

        if ($notification instanceof Notification) {
            $notification->delete();
        }

        $notification = Notification::findOne(['trigger' => Notification::TRIGGER_ADCOMBO_DONT_GET_STATUSES]);

        if ($notification instanceof Notification) {
            $notification->delete();
        }

        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_CALL_CENTER_NOT_APPROVE_NOTIFICATION]);
        if ($crontabTask instanceof CrontabTask) {
            $crontabTask->delete();
        }

        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_ADCOMBO_NOT_GET_STATUSES]);
        if ($crontabTask instanceof CrontabTask) {
            $crontabTask->delete();
        }
    }
}
