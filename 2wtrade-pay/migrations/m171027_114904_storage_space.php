<?php
use app\components\CustomMigration as Migration;

/**
 * Class m171027_114904_storage_space
 */
class m171027_114904_storage_space extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('storage', 'space', $this->decimal(11,2));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('storage', 'space');
    }
}
