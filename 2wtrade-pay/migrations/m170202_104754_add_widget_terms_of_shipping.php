<?php
use app\components\CustomMigration as Migration;
use app\modules\widget\models\WidgetType;
use app\modules\widget\models\WidgetRole;
/**
 * Class m170202_104754_add_widget_terms_of_shipping
 */
class m170202_104754_add_widget_terms_of_shipping extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert(WidgetType::tableName(), [
            'code' => 'terms_of_shipping',
            'name' => 'Сроки отправки',
            'status' => WidgetType::STATUS_ACTIVE
        ]);


        $type = WidgetType::find()
            ->where(['code' => 'terms_of_shipping'])
            ->one();

        $this->insert(WidgetRole::tableName(), [
            'role' => 'business_analyst',
            'type_id' => $type->id
        ]);

        $this->insert(WidgetRole::tableName(), [
            'role' => 'country.curator',
            'type_id' => $type->id
        ]);

        //Добавление новых фраз в интернализацию для виджета \modules\widget\widgets\termsofshipping
        $this->insert('i18n_source_message', ['category' => 'common', 'message' => 'Сроки отправки']);
        $this->insert('i18n_source_message', ['category' => 'common', 'message' => 'Среднее время отправки']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $type = WidgetType::find()
            ->where(['code' => 'terms_of_shipping'])
            ->one();

        $this->delete(WidgetRole::tableName(), [
            'role' => 'business_analyst',
            'type_id' => $type->id,
        ]);

        $this->delete(WidgetRole::tableName(), [
            'role' => 'country.curator',
            'type_id' => $type->id,
        ]);

        $this->delete(WidgetType::tableName(), ['code' => 'terms_of_shipping']);
    }
}
