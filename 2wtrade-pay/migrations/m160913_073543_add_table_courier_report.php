<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160913_073543_add_table_courier_report
 */
class m160913_073543_add_table_courier_report extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%delivery_report}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'user_id' => $this->integer(11)->notNull(),
            'country_id' => $this->integer(11)->notNull(),
            'delivery_id' => $this->integer(11)->notNull(),
            'payment_id' => $this->string(255),
            'file' => $this->text(),
            'status' => "ENUM('queue', 'parsing', 'checking', 'error', 'complete', 'in_work', 'queue_checking') NOT NULL DEFAULT 'queue'",
            'status_pair' => $this->text(),
            'type' => "ENUM('financial', 'transitional') NOT NULL DEFAULT 'transitional'",
            'error_message' => $this->text(),
            'priority' => $this->integer(3)->defaultValue(1),
            'created_at' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)->notNull()
        ], $this->tableOptions);

        $this->addForeignKey(null, '{{%delivery_report}}', 'user_id', '{{%user}}', 'id');
        $this->addForeignKey(null, '{{%delivery_report}}', 'country_id', '{{%country}}', 'id');
        $this->addForeignKey(null, '{{%delivery_report}}', 'delivery_id', '{{%delivery}}', 'id');

        $this->createTable('{{%delivery_report_record}}', [
            'id' => $this->primaryKey(),
            'delivery_report_id' => $this->integer(11)->notNull(),
            'order_id' => $this->string(100),
            'tracking' => $this->string(100),
            'status' => $this->string(255),
            'customer_full_name' => $this->string(200),
            'customer_phone' => $this->string(100),
            'customer_address' => $this->string(500),
            'customer_zip' => $this->string(100),
            'customer_city' => $this->string(100),
            'customer_region' => $this->string(100),
            'customer_district' => $this->string(100),
            'comment' => $this->string(2000),
            'products' => $this->string(200),
            'amount' => $this->string(100),
            'price' => $this->string(100),
            'date_created' => $this->string(50),
            'date_approve' => $this->string(50),
            'date_return' => $this->string(50),
            'date_payment' => $this->string(50),
            'price_storage' => $this->string(50),
            'price_packing' => $this->string(50),
            'price_package' => $this->string(50),
            'price_address_correction' => $this->string(50),
            'price_delivery' => $this->string(50),
            'price_redelivery' => $this->string(50),
            'price_delivery_back' => $this->string(50),
            'price_delivery_return' => $this->string(50),
            'price_cod_service' => $this->string(50),
            'price_cod' => $this->string(50),
            'error_log' => $this->text(),
            'record_status' => "ENUM('active', 'approve', 'deleted', 'status_updated', 'export') NOT NULL DEFAULT 'active'",
            'created_at' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)->notNull()
        ], $this->tableOptions);

        $this->addForeignKey(null, '{{%delivery_report_record}}', 'delivery_report_id', '{{%delivery_report}}', 'id');

        $this->createTable('{{%order_finance}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(11)->notNull(),
            'payment' => $this->string(255),
            'price_cod' => $this->double(2),
            'price_storage' => $this->double(2),
            'price_packing' => $this->double(2),
            'price_package' => $this->double(2),
            'price_address_correction' => $this->double(2),
            'price_delivery' => $this->double(2),
            'price_redelivery' => $this->double(2),
            'price_delivery_back' => $this->double(2),
            'price_delivery_return' => $this->double(2),
            'price_cod_service' => $this->double(2),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11),
        ], $this->tableOptions);

        $this->addForeignKey(null, '{{%order_finance}}', 'order_id', '{{%order}}', 'id');

        $this->addColumn('{{%order}}', 'report_status_id', $this->integer(11));

        $this->addForeignKey(null, '{{%order}}', 'report_status_id', '{{%order_status}}', 'id', self::NO_ACTION,
            self::NO_ACTION);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%delivery_report_record}}');
        $this->dropTable('{{%delivery_report}}');
        $this->dropTable('{{%order_finance}}');
        $this->dropColumn('{{%order}}', 'report_status_id');
        return true;
    }
}
