<?php

use app\components\CustomMigration;
use app\modules\order\models\Order;

/**
 * Handles adding export_history to table `order`.
 */
class m170111_082057_add_export_history_to_order extends CustomMigration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Order::tableName(), 'export_history', $this->text());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Order::tableName(), 'export_history');
    }
}
