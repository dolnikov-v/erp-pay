<?php

use yii\db\Migration;
use yii\db\Query;

/**
 * Handles adding translate to table `finance_report_filter_invoice`.
 */
class m180419_033830_add_translate_to_finance_report_filter_invoice extends Migration
{
    const TEXT = [
        'Ошибка определения ид финансового отчёта (financeReport ) из параметров' => 'Error determining the id financial report (financeReport) from the parameters',
        'Финансовый отчет' => 'Financial report'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $query = new Query();

        foreach ($this::TEXT as $ruText => $enText) {

            $is = $query->select('id')
                ->from('i18n_source_message')
                ->where(['category' => 'common', 'message' => $ruText])->one();

            if (!$is) {
                $this->insert('i18n_source_message', ['category' => 'common', 'message' => $ruText]);
                $id = Yii::$app->db->getLastInsertID();
                $this->insert('i18n_message', ['id' => $id, 'language' => 'en-US', 'translation' => $enText]);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach ($this::TEXT as $ruText => $enText) {
            $this->delete('i18n_source_message', ['category' => 'common', 'message' => $ruText]);
        }

    }
}
