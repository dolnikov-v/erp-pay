<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m161014_110109_product_on_storage_task
 */
class m161014_110109_product_on_storage_task extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = new CrontabTask();
        $crontabTask->name = 'product_on_storage';
        $crontabTask->description = 'Отправка оповещения об остатках продуктов на складах.';
        $crontabTask->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = CrontabTask::find()
            ->byName('product_on_storage')
            ->one();

        if ($crontabTask) {
            $crontabTask->delete();
        }
    }
}
