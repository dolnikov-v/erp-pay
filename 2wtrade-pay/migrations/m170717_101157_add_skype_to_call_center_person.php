<?php
use app\components\CustomMigration as Migration;

/**
 * Handles adding skype to table `call_center_person`.
 */
class m170717_101157_add_skype_to_call_center_person extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('call_center_person', 'skype', $this->string(255)->after('corporate_email'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('call_center_person', 'skype');
    }
}
