<?php
use app\components\CustomMigration as Migration;

/**
 * Class m161021_091103_add_adcombo_request
 */
class m161021_091103_add_adcombo_request extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('adcombo_request', [
            'id' => $this->primaryKey(),
            'foreign_id' => $this->integer(11)->notNull(),
            'country_code' => $this->string(50)->notNull(),
            'name' => $this->string(255),
            'phone' => $this->string(50),
            'email' => $this->string(255),
            'state' => $this->string(255),
            'created_at' => $this->integer(11),
        ], $this->tableOptions);

        $this->createTable('adcombo_request_log', [
            'id' => $this->primaryKey(),
            'adcombo_request_id' => $this->integer(11)->notNull(),
            'field' => $this->string(255),
            'old' => $this->string(255),
            'new' => $this->string(255),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11)
        ], $this->tableOptions);

        $this->addForeignKey(null, 'adcombo_request_log', 'adcombo_request_id', 'adcombo_request', 'id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('adcombo_request_log');
        $this->dropTable('adcombo_request');
    }
}
