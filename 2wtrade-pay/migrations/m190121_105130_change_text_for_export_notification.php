<?php
use app\components\CustomMigration as Migration;
use app\models\Notification;

/**
 * Class m190121_105130_change_text_for_export_notification
 */
class m190121_105130_change_text_for_export_notification extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $description = 'Закончена генерация заказанного файла экспорта <a href="{link}">{name}</a>';
        $trigger = 'generate.export.file';

        if ((new \yii\db\Query())->from('notification')->where(['trigger' => $trigger])->exists($this->db)) {
            $this->update('notification', ['description' => $description], ['trigger' => $trigger]);
        } else {
            $this->insert('notification', [
                'description' => $description,
                'trigger' => $trigger,
                'type' => Notification::TYPE_SUCCESS,
                'group' => Notification::GROUP_SYSTEM,
                'active' => Notification::ACTIVE,
            ]);
        }

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $description = 'Закончена генерация заказанного файла экспорта {file}';
        $trigger = 'generate.export.file';

        if((new \yii\db\Query())->from('notification')->where(['trigger' => $trigger])->exists($this->db)) {
            $this->update('notification', ['description' => $description], ['trigger' => $trigger]);
        } else {
            $this->insert('notification', [
                'description' => $description,
                'trigger' => $trigger,
                'type' => Notification::TYPE_SUCCESS,
                'group' => Notification::GROUP_SYSTEM,
                'active' => Notification::ACTIVE,
            ]);
        }

    }
}
