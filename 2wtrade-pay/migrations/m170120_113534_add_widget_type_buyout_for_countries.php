<?php
use app\components\CustomMigration as Migration;
use app\modules\widget\models\WidgetType;
use app\modules\widget\models\WidgetRole;

/**
 * Class m170120_113534_add_widget_type_buyout_for_countries
 */
class m170120_113534_add_widget_type_buyout_for_countries extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        $this->insert(WidgetType::tableName(), [
            'code' => 'buy_out_for_countries',
            'name' => 'Рейтинг стран',
            'status' => WidgetType::STATUS_ACTIVE,
        ]);

        $type = WidgetType::find()
            ->where(['code' => 'buy_out_for_countries'])
            ->one();

        $this->insert(WidgetRole::tableName(), [
            'role' => 'business_analyst',
            'type_id' => $type->id,
        ]);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $type = WidgetType::find()
            ->where(['code' => 'buy_out_for_countries'])
            ->one();

        $this->delete(WidgetRole::tableName(), [
            'role' => 'business_analyst',
            'type_id' => $type->id,
        ]);

        $this->delete(WidgetType::tableName(), ['code' => 'buy_out_for_countries']);
    }
}
