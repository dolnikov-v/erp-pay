<?php
use app\components\CustomMigration as Migration;
use app\models\User;

/**
 * Class m161209_112323_add_qr_roles
 */
class m161209_112323_add_qr_roles extends Migration
{
    /**
     * @inheritdoc
     */
    public function Up(){
        $auth = $this->authManager;
        $role = $auth->createRole('factory');
        $role->description = 'Сотрудик фабрики';
        $auth->add($role);
        $users = User::findAll([
            'username' => [
                'maithailand',
                'cv_an_nur',
                'dibar',
                'white_pearl_cosmetics',
                'sarkar_herbal',
                'dynamic_lifecate',
                'perennial',
                'xinjang_wulian',
                'slk_systema',
                'hendel',
                'gentech'
            ]
        ]);
        foreach ($users as $user) {
            $auth->assign($role,$user->id);
        }

        $createQrCodes = $auth->createPermission('qr.create.index');
        $createQrCodes->description = 'QR codes creating';
        $auth->add($createQrCodes);
        $initiateQrCodes = $auth->createPermission('qr.initiate.index');
        $initiateQrCodes->description = 'QR codes initiating';
        $auth->add($initiateQrCodes);
        $auth->addChild($role,$createQrCodes);
        $auth->addChild($role,$initiateQrCodes);
    }



    /**
     * @inheritdoc
     */
    public function Down()
    {
        $auth = $this->authManager;
        $role = $auth->getRole('factory');
        $users = User::findAll([
            'usermame' => [
                'maithailand',
                'cv_an_nur',
                'dibar',
                'white_pearl_cosmetics',
                'sarkar_herbal',
                'dynamic_lifecate',
                'perennial',
                'xinjang_wulian',
                'slk_systema',
                'hendel',
                'gentech'
            ]
        ]);
        foreach ($users as $user) {
            $auth->revoke($role,$user->id);
        }
        $permissions = $auth->getChildren('factory');
        foreach ($permissions as $permission) {
            $auth->remove($permission);
        }
        $auth->remove($role);
    }
}
