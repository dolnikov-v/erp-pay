<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160930_051321_report_debts_add_column_updated
 */
class m160930_051321_report_debts_add_column_updated extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('report_debts', 'updated_at', $this->integer()->defaultValue(null));
        $this->addColumn('report_order', 'updated_at', $this->integer()->defaultValue(null));
        $this->addColumn('report_order', 'created_at', $this->integer()->defaultValue(null));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('report_debts', 'updated_at');
        $this->dropColumn('report_order', 'updated_at');
        $this->dropColumn('report_order', 'created_at');
    }
}
