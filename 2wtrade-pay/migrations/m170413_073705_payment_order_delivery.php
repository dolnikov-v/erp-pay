<?php
use app\components\CustomMigration as Migration;
use app\modules\deliveryreport\models\PaymentOrder;
use app\modules\deliveryreport\models\DeliveryReport;

/**
 * Class m170413_073705_payment_order_delivery
 */
class m170413_073705_payment_order_delivery extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(PaymentOrder::tableName(), 'paid_at', $this->integer()->after('sum'));
        $this->alterColumn(PaymentOrder::tableName(), 'sum', $this->double()->defaultValue(0));
        $this->alterColumn(DeliveryReport::tableName(), 'sum_total', $this->double()->defaultValue(0));

        $auth = $this->authManager;
        $permission = $auth->createPermission('deliveryreport.report.paymentlist');
        $permission->description = 'deliveryreport.report.paymentlist';
        $auth->add($permission);

        $permission = $auth->createPermission('deliveryreport.report.ignorebalance');
        $permission->description = 'deliveryreport.report.ignorebalance';
        $auth->add($permission);

        $permission = $auth->createPermission('deliveryreport.report.addpayment');
        $permission->description = 'deliveryreport.report.addpayment';
        $auth->add($permission);

        $permission = $auth->createPermission('deliveryreport.report.removepayment');
        $permission->description = 'deliveryreport.report.removepayment';
        $auth->add($permission);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(PaymentOrder::tableName(), 'paid_at');
        $this->alterColumn(PaymentOrder::tableName(), 'sum', $this->integer()->notNull());
        $this->alterColumn(DeliveryReport::tableName(), 'sum_total', $this->integer()->notNull());

        $auth = $this->authManager;

        $permission = $auth->getPermission('deliveryreport.report.paymentlist');
        $auth->remove($permission);

        $permission = $auth->getPermission('deliveryreport.report.ignorebalance');
        $auth->remove($permission);

        $permission = $auth->getPermission('deliveryreport.report.addpayment');
        $auth->remove($permission);

        $permission = $auth->getPermission('deliveryreport.report.removepayment');
        $auth->remove($permission);
    }
}
