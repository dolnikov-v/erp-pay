<?php

use app\components\CustomMigration as Migration;

class m171016_064743_sms_sent_error_log extends Migration
{
    public function safeUp()
    {
        $this->createTable('sms_sent_error_log', [
            'id' => $this->primaryKey(),
            'to' => $this->string(),
            'from' => $this->string(),
            'status' => $this->string(),
            'error' => $this->string(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $this->tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('sms_sent_error_log');
    }
}
