<?php
use app\components\PermissionMigration as Migration;

/**
 * Class m180925_055948_delivery_control_tabs
 */
class m180925_055948_delivery_control_tabs extends Migration
{
    protected $permissions = [
        'delivery.control.view',
        'delivery.control.editautolist',
        'delivery.control.edittimings',
        'delivery.control.editpaymentinvoice',
        'delivery.control.editbrokerage',
        'delivery.control.editproducts',
        'delivery.control.editstorage'
    ];

    protected $roles = [
        'admin' => [
            'delivery.control.view',
            'delivery.control.editautolist',
            'delivery.control.edittimings',
            'delivery.control.editpaymentinvoice',
            'delivery.control.editbrokerage',
            'delivery.control.editproducts',
            'delivery.control.editstorage'
        ],
        'country.curator' => [
            'delivery.control.view',
            'delivery.control.editautolist',
            'delivery.control.edittimings',
            'delivery.control.editpaymentinvoice',
            'delivery.control.editbrokerage',
            'delivery.control.editproducts',
            'delivery.control.editstorage'
        ],
        'finance.manager' => [
            'delivery.control.view',
            'delivery.control.editautolist',
            'delivery.control.edittimings',
            'delivery.control.editpaymentinvoice',
            'delivery.control.editbrokerage',
            'delivery.control.editproducts',
            'delivery.control.editstorage'
        ],
        'partners.manager' => [
            'delivery.control.view',
            'delivery.control.editautolist',
            'delivery.control.edittimings',
            'delivery.control.editpaymentinvoice',
            'delivery.control.editbrokerage',
            'delivery.control.editproducts',
            'delivery.control.editstorage'
        ],
        'project.manager' => [
            'delivery.control.view',
            'delivery.control.editautolist',
            'delivery.control.edittimings',
            'delivery.control.editpaymentinvoice',
            'delivery.control.editbrokerage',
            'delivery.control.editproducts',
            'delivery.control.editstorage'
        ],
        'support.manager' => [
            'delivery.control.view',
            'delivery.control.editautolist',
            'delivery.control.edittimings',
            'delivery.control.editpaymentinvoice',
            'delivery.control.editbrokerage',
            'delivery.control.editproducts',
            'delivery.control.editstorage'
        ],
        'technical.director' => [
            'delivery.control.view',
            'delivery.control.editautolist',
            'delivery.control.edittimings',
            'delivery.control.editpaymentinvoice',
            'delivery.control.editbrokerage',
            'delivery.control.editproducts',
            'delivery.control.editstorage'
        ]
    ];
}
