<?php
use app\components\CustomMigration as Migration;
use app\modules\widget\models\WidgetType;
use app\modules\widget\models\WidgetRole;
use app\models\AuthItem;

/**
 * Class m170130_061416_add_buyout_delivery_widget
 */
class m170130_061416_add_buyout_delivery_widget extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert(WidgetType::tableName(), [
            'code' => 'buyout_delivery',
            'name' => 'Выкупы по курьеркам',
            'status' => WidgetType::STATUS_ACTIVE
        ]);


        $type = WidgetType::find()
            ->where(['code' => 'buyout_delivery'])
            ->one();

        $this->insert(WidgetRole::tableName(), [
            'role' => 'business_analyst',
            'type_id' => $type->id
        ]);

        $this->insert(WidgetRole::tableName(), [
            'role' => 'country.curator',
            'type_id' => $type->id
        ]);

        //Добавление новых фраз в интернализацию для виджета \modules\widget\widgets\buyoutdelivery
        $this->insert('i18n_source_message', ['category' => 'common', 'message' => 'Выкупы по курьеркам']);
        $this->insert('i18n_source_message', ['category' => 'common', 'message' => 'Среднее количество выкупов']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $type = WidgetType::find()
            ->where(['code' => 'buyout_delivery'])
            ->one();

        $this->delete(WidgetRole::tableName(), [
            'role' => 'business_analyst',
            'type_id' => $type->id,
        ]);

        $this->delete(WidgetRole::tableName(), [
            'role' => 'country.curator',
            'type_id' => $type->id,
        ]);

        $this->delete(WidgetType::tableName(), ['code' => 'buyout_delivery']);
    }
}
