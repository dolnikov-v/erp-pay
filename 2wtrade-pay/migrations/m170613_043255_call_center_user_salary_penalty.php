<?php
use app\components\CustomMigration as Migration;
use app\modules\salary\models\PenaltyType;
//use app\modules\callcenter\models\CallCenterUserPenalty;
use app\models\Currency;
use app\models\User;
use yii\db\Query;
use app\modules\callcenter\models\CallCenterUser;
use app\modules\callcenter\models\CallCenter;
use app\models\Country;

/**
 * Class m170613_043255_call_center_user_salary_penalty
 */
class m170613_043255_call_center_user_salary_penalty extends Migration
{
    const ROLES = [
        'country.curator',
        'callcenter.manager',
        'callcenter.hr',
    ];

    const RULES = [
        'callcenter.penaltytype.index',
        'callcenter.penaltytype.edit',
        'callcenter.penaltytype.delete',
        'callcenter.penaltytype.activate',
        'callcenter.penaltytype.deactivate',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
/*
        $CallCenterUserPenaltySchema = Yii::$app->db->schema->getTableSchema(CallCenterUserPenalty::tableName());

        if ($CallCenterUserPenaltySchema !== null) {
            $this->dropTable(CallCenterUserPenalty::tableName());
        }
*/

        $CallCenterPenaltyTypeSchema = Yii::$app->db->schema->getTableSchema(PenaltyType::tableName());

        if ($CallCenterPenaltyTypeSchema !== null) {
            $this->dropTable(PenaltyType::tableName());
        }



        $this->createTable(PenaltyType::tableName(), [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'type' => $this->string(),
            'sum' => $this->float(),
            'percent' => $this->integer(),
            'currency_id' => $this->integer(),
            'active' => $this->integer(1)->defaultValue(1),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);


        $this->addForeignKey(null, PenaltyType::tableName(), 'currency_id', Currency::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, PenaltyType::tableName(), 'created_by', User::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, PenaltyType::tableName(), 'updated_by', User::tableName(), 'id', self::CASCADE, self::RESTRICT);
/*
        $this->createTable(CallCenterUserPenalty::tableName(), [
            'id' => $this->primaryKey(),
            'country_id' => $this->integer()->notNull(),
            'call_center_id' => $this->integer()->notNull(),
            'call_center_user_id' => $this->integer()->notNull(),
            'created_by' => $this->integer()->notNull(),
            'updated_by' => $this->integer()->notNull(),
            'penalty_type_id' => $this->integer()->notNull(),
            'penalty_percent' => $this->integer(),
            'penalty_sum' => $this->float(),
            'comment' => $this->string(255),
            'penalty_at' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addForeignKey(null, CallCenterUserPenalty::tableName(), 'country_id', Country::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, CallCenterUserPenalty::tableName(), 'call_center_id', CallCenter::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, CallCenterUserPenalty::tableName(), 'call_center_user_id', CallCenterUser::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, CallCenterUserPenalty::tableName(), 'penalty_type_id', 'call_center_penalty_type', 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, CallCenterUserPenalty::tableName(), 'created_by', User::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, CallCenterUserPenalty::tableName(), 'updated_by', User::tableName(), 'id', self::CASCADE, self::RESTRICT);
*/
        $query = new Query();

        foreach (self::RULES as $rule) {

            $is_rule = $query->select('*')->from($this->authManager->itemTable)
                ->where(['name' => $rule, 'type' => 2])
                ->one();

            if (!$is_rule) {
                $this->insert($this->authManager->itemTable, [
                    'name' => $rule,
                    'description' => $rule,
                    'type' => 2,
                    'created_at' => time(),
                    'updated_at' => time()]);
            }

            foreach (self::ROLES as $role) {
                $is_can = $query->select('*')->from($this->authManager->itemChildTable)->where([
                    'parent' => $role,
                    'child' => $rule
                ])->one();

                if (!$is_can) {
                    $this->insert($this->authManager->itemChildTable, [
                        'parent' => $role,
                        'child' => $rule
                    ]);
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        //$this->dropTable(CallCenterUserPenalty::tableName());
        $this->dropTable(PenaltyType::tableName());

        foreach (self::RULES as $rule) {
            foreach (self::ROLES as $role) {
                $this->delete($this->authManager->itemChildTable, [
                    'parent' => $role,
                    'child' => $rule
                ]);
            }
        }
    }
} 