<?php
use app\components\CustomMigration as Migration;
use \yii\db\Query;

/**
 * Class m170209_062753_marketing_lists_access
 */
class m170209_062753_marketing_lists_access extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        $query = new Query();

        $rules = [
            'order.marketinglist.index',
            'order.index.generatemarketinglist',
            'order.marketinglist.deleteorder',
            'order.marketinglist.deletelist',
            'order.marketinglist.details',
            'order.marketinglist.sendorderstodelivery',
        ];

        $roles = [
            'callcenter.manager',
            'project.manager'
        ];

        foreach ($rules as $rule) {
            $is_report = $query->select('*')->from($this->authManager->itemTable)
                ->where([
                    'name' => $rule,
                    'type' => 2])
                ->one();

            if (!$is_report) {
                $this->insert($this->authManager->itemTable, [
                    'name' => $rule,
                    'type' => 2,
                    'description' => $rule,
                    'created_at' => time(),
                    'updated_at' => time()]);
            }

            foreach ($roles as $role) {

                $is_role = $query->select('*')->from($this->authManager->itemTable)
                    ->where(['name' => $role, 'type' => 1])
                    ->one();

                if (!$is_role) {
                    $this->insert($this->authManager->itemTable, [
                        'name' => $role,
                        'type' => 1,
                        'description' => $role,
                        'created_at' => time(),
                        'updated_at' => time()]);
                }

                $is_br = $query->select('*')->from($this->authManager->itemChildTable)->where([
                    'parent' => $role,
                    'child' => $rule
                ])->one();

                if (!$is_br) {
                    $this->insert($this->authManager->itemChildTable, [
                        'parent' => $role,
                        'child' => $rule
                    ]);
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

        $rules = [
            'order.marketinglist.index',
            'order.index.generatemarketinglist',
            'order.marketinglist.deleteorder',
            'order.marketinglist.deletelist',
            'order.marketinglist.details',
            'order.marketinglist.sendorderstodelivery',
        ];

        $roles = [
            'callcenter.manager',
            'project.manager'
        ];

        foreach ($rules as $rule) {

            foreach ($roles as $role) {
                $this->delete($this->authManager->itemChildTable, [
                    'parent' => $role,
                    'child' => $rule
                ]);
            }

            $this->delete($this->authManager->itemTable, [
                'name' => $rule,
                'type' => 2]);
        }
    }
}
