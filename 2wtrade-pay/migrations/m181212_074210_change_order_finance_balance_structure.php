<?php

use app\components\CustomMigration as Migration;

/**
 * Class m181212_074210_change_order_finance_balance_structure
 */
class m181212_074210_change_order_finance_balance_structure extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->execute('DROP TRIGGER IF EXISTS `update_finance_balance`');

        $this->dropTable('order_finance_balance');
        $this->createTable('order_finance_balance', [
            'delivery_request_id' => $this->integer()->notNull(),
            'order_id' => $this->integer()->notNull(),
            'price_cod' => $this->double(),
            'price_storage' => $this->double(),
            'price_fulfilment' => $this->double(),
            'price_packing' => $this->double(),
            'price_package' => $this->double(),
            'price_delivery' => $this->double(),
            'price_redelivery' => $this->double(),
            'price_delivery_back' => $this->double(),
            'price_delivery_return' => $this->double(),
            'price_address_correction' => $this->double(),
            'price_cod_service' => $this->double(),
            'price_vat' => $this->double(),
            'price_cod_currency_id' => $this->integer(),
            'price_storage_currency_id' => $this->integer(),
            'price_fulfilment_currency_id' => $this->integer(),
            'price_packing_currency_id' => $this->integer(),
            'price_package_currency_id' => $this->integer(),
            'price_delivery_currency_id' => $this->integer(),
            'price_redelivery_currency_id' => $this->integer(),
            'price_delivery_back_currency_id' => $this->integer(),
            'price_delivery_return_currency_id' => $this->integer(),
            'price_address_correction_currency_id' => $this->integer(),
            'price_cod_service_currency_id' => $this->integer(),
            'price_vat_currency_id' => $this->integer(),
            'additional_prices' => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addPrimaryKey($this->getPkName('order_finance_balance', 'delivery_request_id'), 'order_finance_balance', 'delivery_request_id');
        $this->addForeignKey($this->getFkName('order_finance_balance', 'delivery_request_id'), 'order_finance_balance', 'delivery_request_id', 'delivery_request', 'id', self::CASCADE, self::CASCADE);
        $this->addForeignKey($this->getFkName('order_finance_balance', 'order_id'), 'order_finance_balance', 'order_id', 'order', 'id', self::CASCADE, self::CASCADE);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('order_finance_balance');
        $this->createTable('order_finance_balance', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->notNull(),
            'delivery_id' => $this->integer()->notNull(),
            'price_cod' => $this->float()->defaultValue(0),
            'prediction_price_cod' => $this->float(2)->defaultValue(0),
            'price_cod_currency_id' => $this->integer()->defaultValue(null),
            'price_storage' => $this->float()->defaultValue(0),
            'prediction_price_storage' => $this->float(2)->defaultValue(0),
            'price_storage_currency_id' => $this->integer()->defaultValue(null),
            'price_fulfilment' => $this->float()->defaultValue(0),
            'prediction_price_fulfilment' => $this->float(2)->defaultValue(0),
            'price_fulfilment_currency_id' => $this->integer()->defaultValue(null),
            'price_packing' => $this->float()->defaultValue(0),
            'prediction_price_packing' => $this->float(2)->defaultValue(0),
            'price_packing_currency_id' => $this->integer()->defaultValue(null),
            'price_package' => $this->float()->defaultValue(0),
            'prediction_price_package' => $this->float(2)->defaultValue(0),
            'price_package_currency_id' => $this->integer()->defaultValue(null),
            'price_delivery' => $this->float()->defaultValue(0),
            'prediction_price_delivery' => $this->float(2)->defaultValue(0),
            'price_delivery_currency_id' => $this->integer()->defaultValue(null),
            'price_redelivery' => $this->float()->defaultValue(0),
            'prediction_price_redelivery' => $this->float(2)->defaultValue(0),
            'price_redelivery_currency_id' => $this->integer()->defaultValue(null),
            'price_delivery_back' => $this->float()->defaultValue(0),
            'prediction_price_delivery_back' => $this->float(2)->defaultValue(0),
            'price_delivery_back_currency_id' => $this->integer()->defaultValue(null),
            'price_delivery_return' => $this->float()->defaultValue(0),
            'prediction_price_delivery_return' => $this->float(2)->defaultValue(0),
            'price_delivery_return_currency_id' => $this->integer()->defaultValue(null),
            'price_address_correction' => $this->float()->defaultValue(0),
            'prediction_price_address_correction' => $this->float(2)->defaultValue(0),
            'price_address_correction_currency_id' => $this->integer()->defaultValue(null),
            'price_cod_service' => $this->float()->defaultValue(0),
            'prediction_price_cod_service' => $this->float(2)->defaultValue(0),
            'price_cod_service_currency_id' => $this->integer()->defaultValue(null),
            'price_vat' => $this->float()->defaultValue(0),
            'prediction_price_vat' => $this->float(2)->defaultValue(0),
            'price_vat_currency_id' => $this->integer()->defaultValue(null),
            'last_error' => $this->string()->defaultValue(null),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);

        $this->addForeignKey(null, 'order_finance_balance', 'order_id', 'order', 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, 'order_finance_balance', 'delivery_id', 'delivery', 'id', self::CASCADE, self::RESTRICT);

        $sql = <<<SQL
              CREATE TRIGGER update_finance_balance AFTER UPDATE ON order_finance_prediction FOR EACH ROW
              BEGIN
                DECLARE delivery_id INTEGER;
                
                SET delivery_id = 0;
                 SELECT delivery_id INTO delivery_id FROM delivery_request where order_id = NEW.order_id;
                 
                update order_finance_balance set 
                prediction_price_cod = NEW.price_cod,
                price_cod_currency_id = NEW.price_cod_currency_id,
                prediction_price_storage = NEW.price_storage,
                price_storage_currency_id = NEW.price_storage_currency_id,
                prediction_price_fulfilment = NEW.price_fulfilment,
                price_fulfilment_currency_id = NEW.price_fulfilment_currency_id,
                prediction_price_packing = NEW.price_packing,
                price_packing_currency_id = NEW.price_packing_currency_id,
                prediction_price_package = NEW.price_package,
                price_package_currency_id = NEW.price_package_currency_id,
                prediction_price_delivery = NEW.price_delivery,
                price_delivery_currency_id = NEW.price_delivery_currency_id,
                prediction_price_redelivery = NEW.price_redelivery,
                price_redelivery_currency_id = NEW.price_redelivery_currency_id,
                prediction_price_delivery_back = NEW.price_delivery_back,
                price_delivery_back_currency_id = NEW.price_delivery_back_currency_id,
                prediction_price_delivery_return = NEW.price_delivery_return,
                price_delivery_return_currency_id = NEW.price_delivery_return_currency_id,
                prediction_price_address_correction = NEW.price_address_correction,
                price_address_correction_currency_id = NEW.price_address_correction_currency_id,
                prediction_price_cod_service = NEW.price_cod_service,
                price_cod_service_currency_id = NEW.price_cod_service_currency_id,
                prediction_price_vat = NEW.price_vat,
                price_vat_currency_id = NEW.price_vat_currency_id
                where order_id = NEW.order_id and delivery_id = delivery_id;
                        
              END;
SQL;
        $this->execute($sql);

    }
}
