<?php
use app\components\CustomMigration as Migration;

/**
 * Class m171025_100101_charges_calculators_for_chili
 */
class m171025_100101_charges_calculators_for_chili extends Migration
{
    protected static $calculators = [
        'Starken' => 'app\modules\delivery\components\charges\starken\Starken',
        'DifferentDeliveryChargeByStatus' => 'app\modules\delivery\components\charges\DifferentDeliveryChargeByStatus',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        foreach (self::$calculators as $name => $route) {
            $this->insert('delivery_charges_calculator', [
                'name' => $name,
                'class_path' => $route,
                'active' => 1,
                'created_at' => time(),
            ]);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach (self::$calculators as $name => $route) {
            $this->delete('delivery_charges_calculator', ['name' => $name]);
        }
    }
}
