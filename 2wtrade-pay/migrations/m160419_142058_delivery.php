<?php
use app\components\CustomMigration as Migration;

class m160419_142058_delivery extends Migration
{
    public function safeUp()
    {
        $this->createTable('delivery', [
            'id' => $this->primaryKey(),
            'country_id' => $this->integer()->notNull(),
            'name' => $this->string(100)->notNull(),
            'comment' => $this->text()->defaultValue(null),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(null, 'delivery', 'country_id', 'country', 'id', self::CASCADE, self::RESTRICT);
    }

    public function safeDown()
    {
        $this->dropTable('delivery');
    }
}
