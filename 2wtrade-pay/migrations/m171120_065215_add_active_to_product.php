<?php

use app\models\Product;
use yii\db\Migration;

/**
 * Handles adding active to table `product`.
 */
class m171120_065215_add_active_to_product extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
	    $this->addColumn( Product::tableName(), 'active', $this->boolean()->defaultValue(1));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
	    $this->dropColumn( Product::tableName(), 'active');
    }
}
