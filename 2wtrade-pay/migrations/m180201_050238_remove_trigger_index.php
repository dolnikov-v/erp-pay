<?php
use app\components\CustomMigration as Migration;

/**
 * Class m180201_050238_remove_trigger_index
 */
class m180201_050238_remove_trigger_index extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropIndex('idx_call_center_penalty_type_trigger', 'salary_penalty_type');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->createIndex('idx_call_center_penalty_type_trigger', 'salary_penalty_type', 'trigger', true);
    }
}
