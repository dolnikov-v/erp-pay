<?php
use app\components\CustomMigration as Migration;

class m170911_095417_add_user_source_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('user_source', [
            'user_id' => $this->integer()->notNull(),
            'source' => $this->string(64)->notNull(),
            'active' => $this->boolean()->defaultValue(1), // по умолчанию включено всё
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->addPrimaryKey(null, 'user_source', ['user_id', 'source']);
    }

    public function safeDown()
    {
        $this->dropTable('user_source');
    }
}
