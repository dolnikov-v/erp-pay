<?php
use app\components\CustomMigration as Migration;

/**
 * Class m181118_060827_balance_as_sum_total_and_additional
 */
class m181118_060827_balance_as_sum_total_and_additional extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('delivery_report', 'old_sum_total', $this->double());

        $this->update('delivery_report', [
            'old_sum_total' => new \yii\db\Expression('sum_total'),
            'sum_total' => new \yii\db\Expression('(total_costs + additional_costs)')
        ], new \yii\db\Expression('sum_total > (total_costs + additional_costs)'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->update('delivery_report', ['sum_total' => new \yii\db\Expression('old_sum_total')], ['>' , 'old_sum_total', 0]);

        $this->dropColumn('delivery_report', 'old_sum_total');
    }
}
