<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170524_052544_add_permission_for_group_operation_cc
 */
class m170524_052544_add_permission_for_group_operation_cc extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
            $this->insert('{{%auth_item}}', array(
                'name' => 'callcenter.user.setrole',
                'type' => '2',
                'description' => 'callcenter.user.setrole'
            ));

            $this->insert($this->authManager->itemChildTable, array(
                'parent' => 'country.curator',
                'child' => 'callcenter.user.setrole'
            ));

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
            $this->delete($this->authManager->itemChildTable, ['child' => 'callcenter.user.setrole', 'parent' => 'country.curator']);
            $this->delete('{{%auth_item}}', ['name' => 'callcenter.user.setrole']);
    }
}
