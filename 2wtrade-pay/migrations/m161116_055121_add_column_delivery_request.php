<?php
use app\components\CustomMigration as Migration;

/**
 * Class m161116_055121_add_column_delivery_request
 */
class m161116_055121_add_column_delivery_request extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('delivery_request', 'response_info', 'VARCHAR(255) DEFAULT NULL AFTER foreign_info');

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('delivery_request', 'response_info');

    }
}
