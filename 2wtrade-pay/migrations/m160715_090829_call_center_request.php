<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160715_090829_call_center_request
 */
class m160715_090829_call_center_request extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropForeignKey($this->getFkName('delivery_request', 'order_id'), 'delivery_request');
        $this->createIndex(null, 'delivery_request', 'order_id', true);
        $this->addForeignKey(null, 'delivery_request', 'order_id', 'order', 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->createIndex($this->getFkName('delivery_request', 'order_id'), 'delivery_request', 'order_id', false);
        $this->dropIndex($this->getUniName('delivery_request', 'order_id'), 'delivery_request');
    }
}
