<?php
use app\components\CustomMigration as Migration;
use app\modules\order\models\OrderStatus;
use app\modules\order\models\OrderWorkflowStatus;

class m160418_144243_order_workflow_status extends Migration
{
    public function safeUp()
    {
        $this->createTable('order_workflow_status', [
            'id' => $this->primaryKey(),
            'workflow_id' => $this->integer()->notNull(),
            'status_id' => $this->integer()->notNull(),
            'parents' => $this->string(100)->defaultValue(null),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(null, 'order_workflow_status', 'workflow_id', 'order_workflow', 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, 'order_workflow_status', 'status_id', 'order_status', 'id', self::CASCADE, self::RESTRICT);

        foreach ($this->getFixture() as $data) {
            $model = new OrderWorkflowStatus($data);
            $model->save();
        }
    }

    public function safeDown()
    {
        $this->dropTable('order_workflow_status');
    }

    /**
     * @return array
     */
    private function getFixture()
    {
        return [
            ['workflow_id' => 1, 'status_id' => OrderStatus::STATUS_SOURCE_SHORT_FORM, 'parents' => [3]],
            ['workflow_id' => 1, 'status_id' => OrderStatus::STATUS_CC_INPUT_QUEUE, 'parents' => [6]],
            ['workflow_id' => 1, 'status_id' => OrderStatus::STATUS_CC_POST_CALL, 'parents' => [4, 5, 6, 7, 25, 26]],
            ['workflow_id' => 1, 'status_id' => OrderStatus::STATUS_CC_RECALL, 'parents' => [5, 6, 7, 25, 26]],
            ['workflow_id' => 1, 'status_id' => OrderStatus::STATUS_CC_APPROVED, 'parents' => [12]],
            ['workflow_id' => 1, 'status_id' => OrderStatus::STATUS_DELIVERY_ACCEPTED, 'parents' => [19, 22]],
            ['workflow_id' => 1, 'status_id' => OrderStatus::STATUS_DELIVERY_LEFT_THEIR_WAREHOUSE, 'parents' => [15, 16, 21, 29]],
            ['workflow_id' => 1, 'status_id' => OrderStatus::STATUS_DELIVERY_DENIAL, 'parents' => [17]],
            ['workflow_id' => 1, 'status_id' => OrderStatus::STATUS_DELIVERY_REDELIVERY, 'parents' => [15, 16]],
            ['workflow_id' => 1, 'status_id' => OrderStatus::STATUS_DELIVERY_BUYOUT, 'parents' => [18]],

            ['workflow_id' => 2, 'status_id' => OrderStatus::STATUS_SOURCE_LONG_FORM, 'parents' => [2, 12]],
            ['workflow_id' => 2, 'status_id' => OrderStatus::STATUS_SOURCE_SHORT_FORM, 'parents' => [3]],
            ['workflow_id' => 2, 'status_id' => OrderStatus::STATUS_CC_POST_CALL, 'parents' => [4, 6]],
            ['workflow_id' => 2, 'status_id' => OrderStatus::STATUS_CC_RECALL, 'parents' => [5, 6, 7, 25, 26]],
            ['workflow_id' => 2, 'status_id' => OrderStatus::STATUS_CC_APPROVED, 'parents' => [12]],
            ['workflow_id' => 2, 'status_id' => OrderStatus::STATUS_DELIVERY_ACCEPTED, 'parents' => [19, 22]],
            ['workflow_id' => 2, 'status_id' => OrderStatus::STATUS_DELIVERY_LEFT_THEIR_WAREHOUSE, 'parents' => [15, 16, 21, 29]],
            ['workflow_id' => 2, 'status_id' => OrderStatus::STATUS_DELIVERY_DENIAL, 'parents' => [17]],
            ['workflow_id' => 2, 'status_id' => OrderStatus::STATUS_DELIVERY_REDELIVERY, 'parents' => [15, 16]],
            ['workflow_id' => 2, 'status_id' => OrderStatus::STATUS_DELIVERY_BUYOUT, 'parents' => [18]],

            ['workflow_id' => 3, 'status_id' => OrderStatus::STATUS_SOURCE_SHORT_FORM, 'parents' => [3]],
            ['workflow_id' => 3, 'status_id' => OrderStatus::STATUS_CC_INPUT_QUEUE, 'parents' => [6]],
            ['workflow_id' => 3, 'status_id' => OrderStatus::STATUS_CC_POST_CALL, 'parents' => [4, 5, 6, 7, 25, 26]],
            ['workflow_id' => 3, 'status_id' => OrderStatus::STATUS_CC_RECALL, 'parents' => [5, 6, 7, 25, 26]],
            ['workflow_id' => 3, 'status_id' => OrderStatus::STATUS_CC_APPROVED, 'parents' => [8]],
            ['workflow_id' => 3, 'status_id' => OrderStatus::STATUS_LOG_ACCEPTED, 'parents' => [9]],
            ['workflow_id' => 3, 'status_id' => OrderStatus::STATUS_LOG_GENERATED, 'parents' => [10]],
            ['workflow_id' => 3, 'status_id' => OrderStatus::STATUS_LOG_SET, 'parents' => [11]],
            ['workflow_id' => 3, 'status_id' => OrderStatus::STATUS_LOG_PASTED, 'parents' => [12]],
            ['workflow_id' => 3, 'status_id' => OrderStatus::STATUS_DELIVERY_ACCEPTED, 'parents' => [19, 20]],
            ['workflow_id' => 3, 'status_id' => OrderStatus::STATUS_DELIVERY_LEFT_OUR_WAREHOUSE, 'parents' => [15, 16, 21, 29]],
            ['workflow_id' => 3, 'status_id' => OrderStatus::STATUS_DELIVERY_REDELIVERY, 'parents' => [16, 29]],
            ['workflow_id' => 3, 'status_id' => OrderStatus::STATUS_DELIVERY_DENIAL, 'parents' => [27]],
            ['workflow_id' => 3, 'status_id' => OrderStatus::STATUS_DELIVERY_REFUND, 'parents' => [17]],
            ['workflow_id' => 3, 'status_id' => OrderStatus::STATUS_DELIVERY_BUYOUT, 'parents' => [18]],

            ['workflow_id' => 4, 'status_id' => OrderStatus::STATUS_SOURCE_LONG_FORM, 'parents' => [3, 8]],
            ['workflow_id' => 4, 'status_id' => OrderStatus::STATUS_CC_POST_CALL, 'parents' => [4, 5, 6, 7, 25, 26]],
            ['workflow_id' => 4, 'status_id' => OrderStatus::STATUS_CC_RECALL, 'parents' => [5, 6, 7, 25, 26]],
            ['workflow_id' => 4, 'status_id' => OrderStatus::STATUS_CC_APPROVED, 'parents' => [8]],
            ['workflow_id' => 4, 'status_id' => OrderStatus::STATUS_LOG_ACCEPTED, 'parents' => [9]],
            ['workflow_id' => 4, 'status_id' => OrderStatus::STATUS_LOG_GENERATED, 'parents' => [10]],
            ['workflow_id' => 4, 'status_id' => OrderStatus::STATUS_LOG_SET, 'parents' => [11]],
            ['workflow_id' => 4, 'status_id' => OrderStatus::STATUS_LOG_PASTED, 'parents' => [12]],
            ['workflow_id' => 4, 'status_id' => OrderStatus::STATUS_DELIVERY_ACCEPTED, 'parents' => [19, 20]],
            ['workflow_id' => 4, 'status_id' => OrderStatus::STATUS_DELIVERY_LEFT_OUR_WAREHOUSE, 'parents' => [15, 16, 21, 29]],
            ['workflow_id' => 4, 'status_id' => OrderStatus::STATUS_DELIVERY_REDELIVERY, 'parents' => [16, 29]],
            ['workflow_id' => 4, 'status_id' => OrderStatus::STATUS_DELIVERY_DENIAL, 'parents' => [27]],
            ['workflow_id' => 4, 'status_id' => OrderStatus::STATUS_DELIVERY_REFUND, 'parents' => [17]],
            ['workflow_id' => 4, 'status_id' => OrderStatus::STATUS_DELIVERY_BUYOUT, 'parents' => [18]],
        ];
    }
}
