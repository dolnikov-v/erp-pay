<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160930_125405_add_order_product_change
 */
class m160930_125405_add_order_product_change extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('order_log_product', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
            'action' => $this->enum(['create', 'update', 'delete']),
            'price' => $this->integer()->notNull(),
            'quantity' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(null, 'order_log_product', 'order_id', 'order', 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, 'order_log_product', 'product_id', 'product', 'id', self::CASCADE, self::RESTRICT);
        $this->createIndex(null, 'order_log_product', 'action');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('order_log_product');
    }
}
