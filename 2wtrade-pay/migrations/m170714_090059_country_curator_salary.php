<?php
use app\components\CustomMigration as Migration;
use yii\db\Query;

/**
 * Class m170714_090059_country_curator_salary
 */
class m170714_090059_country_curator_salary extends Migration
{
    const ROLES = [
        'country.curator',
        'finance.director',
        'callcenter.hr',
        'callcenter.manager',
        'project.manager'
    ];

    const RULES = [
        'report.timesheet.index',
        'report.salary.index',
        'report.salary.getcallcenter',
        'report.salary.getteamlead',
        'limit_call_center_persons_by_direction',
        'limit_call_center_persons_by_office',
    ];


    const REMOVE_ROLES = [
        'country.curator',
    ];

    const REMOVE_RULES = [
        'callcenter.penalty.index',
        'callcenter.penalty.edit',
        'callcenter.penalty.delete',
        'callcenter.person.delete',
        'callcenter.person.edit',
        'callcenter.person.activate',
        'callcenter.person.deactivate',
        'limit_call_center_persons_by_office',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $query = new Query();

        foreach (self::RULES as $rule) {

            $is_rule = $query->select('*')->from($this->authManager->itemTable)
                ->where(['name' => $rule, 'type' => 2])
                ->one();

            if (!$is_rule) {
                $this->insert($this->authManager->itemTable, [
                    'name' => $rule,
                    'description' => $rule,
                    'type' => 2,
                    'created_at' => time(),
                    'updated_at' => time()]);
            }

            foreach (self::ROLES as $role) {
                $is_can = $query->select('*')->from($this->authManager->itemChildTable)->where([
                    'parent' => $role,
                    'child' => $rule
                ])->one();

                if (!$is_can) {
                    $this->insert($this->authManager->itemChildTable, [
                        'parent' => $role,
                        'child' => $rule
                    ]);
                }
            }
        }

        foreach (self::REMOVE_RULES as $rule) {
            foreach (self::REMOVE_ROLES as $role) {
                $this->delete($this->authManager->itemChildTable, [
                    'parent' => $role,
                    'child' => $rule
                ]);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach (self::RULES as $rule) {
            foreach (self::ROLES as $role) {
                $this->delete($this->authManager->itemChildTable, [
                    'parent' => $role,
                    'child' => $rule
                ]);
            }
        }
    }
}
