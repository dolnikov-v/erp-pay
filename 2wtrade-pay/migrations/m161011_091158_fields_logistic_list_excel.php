<?php
use app\components\CustomMigration as Migration;

/**
 * Class m161011_091158_fields_logistic_list_excel
 */
class m161011_091158_fields_logistic_list_excel extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('order_logistic_list_excel', 'columns_hash', $this->string(32)->defaultValue(null)->after('delivery_file_name'));
        $this->addColumn('order_logistic_list_excel', 'format', $this->string(20)->defaultValue(null)->after('columns_hash'));
        //индекс для поля orders_hash называется так же как должен называться для columns_hash. называем правильно
        $this->dropIndex($this->getIdxName('order_logistic_list_excel', 'columns_hash'), 'order_logistic_list_excel');
        $this->createIndex(null, 'order_logistic_list_excel', 'orders_hash');

        $this->createIndex(null, 'order_logistic_list_excel', 'columns_hash');
        $this->createIndex(null, 'order_logistic_list_excel', 'format');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('order_logistic_list_excel', 'format');
        $this->dropColumn('order_logistic_list_excel', 'columns_hash');

        $this->dropIndex($this->getIdxName('order_logistic_list_excel', 'orders_hash'), 'order_logistic_list_excel');
        $this->createIndex('idx_order_logistic_list_excel_columns_hash', 'order_logistic_list_excel', 'orders_hash');
    }
}
