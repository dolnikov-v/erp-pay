<?php
use app\components\CustomMigration as Migration;
use app\models\Notification;

/**
 * Class m170613_083548_change_description_notification_lead_not_added
 */
class m170613_083548_change_description_notification_lead_not_added extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->update(Notification::tableName(),
            ['description'=>'Не удалось добавить нового лида от Adcombo. Ошибка: {exception}'],
            ['trigger' => 'lead.not.added']
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->update(Notification::tableName(),
            ['description'=>'Не удалось добавить нового лида от Adcombo. Ошибка: {exeption}'],
            ['trigger' => 'lead.not.added']
        );
    }

}
