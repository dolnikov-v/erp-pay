<?php
use app\components\PermissionMigration as Migration;

/**
 * Class m180905_041152_add_shelf_life_from_storage
 */
class m180905_041152_add_shelf_life_from_storage extends Migration
{
    protected $permissions = [
        'logger.browser.index',
        'administration.crontab.logs.index',
        'administration.crontab.logs.answers',
        'api.logs.lead',
        'api.logs.lead.view',
        'api.logs.callcenter',
        'api.logs.callcenter.view',
        'api.logs.delivery',
        'api.logs.delivery.view',
    ];

    protected $roles = [
        'support.manager' => [
            'storage.storageproduct.getselectoptionsshelflife'
            ]
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('storage_document', 'shelf_life', $this->date());
        $this->addColumn('storage_product', 'shelf_life', $this->date());
        $this->addColumn('storage_part', 'shelf_life', $this->date());
        parent::safeUp();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('storage_part', 'shelf_life');
        $this->dropColumn('storage_product', 'shelf_life');
        $this->dropColumn('storage_document', 'shelf_life');
        parent::safeDown();
    }
}
