<?php
use app\components\CustomMigration as Migration;
use app\modules\salary\models\Office;
use app\modules\salary\models\Holiday;
use app\modules\administration\models\CrontabTask;

/**
 * Class m171011_100919_greeting_card
 */
class m171011_100919_greeting_card extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_MAKE_GREETING_CARDS]);
        if (!$crontabTask) {
            $crontabTask = new CrontabTask();
            $crontabTask->name = CrontabTask::TASK_MAKE_GREETING_CARDS;
            $crontabTask->description = "Создание поздравлений с праздниками сотрудникам";
            $crontabTask->save();
        }

        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_SEND_GREETING_CARDS]);
        if (!$crontabTask) {
            $crontabTask = new CrontabTask();
            $crontabTask->name = CrontabTask::TASK_SEND_GREETING_CARDS;
            $crontabTask->description = "Отправка поздравлений с праздниками сотрудникам";
            $crontabTask->save();
        }

        $this->addColumn(Office::tableName(), 'birthday_greeting_card_template_subject', $this->string());
        $this->addColumn(Office::tableName(), 'birthday_greeting_card_template_message', $this->text());
        $this->addColumn(Holiday::tableName(), 'greeting_card_template_subject', $this->text()->after('name'));
        $this->addColumn(Holiday::tableName(), 'greeting_card_template_message', $this->text()->after('greeting_card_template_subject'));

        $this->createTable('salary_person_greeting_card', [
            'id' => $this->primaryKey(),
            'person_id' => $this->integer(),
            'email_to' => $this->string(),
            'subject' => $this->string(),
            'message' => $this->text(),
            'status' => $this->string(),
            'send_at' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $this->tableOptions);

        $this->addForeignKey(null, 'salary_person_greeting_card', 'person_id', 'salary_person', 'id', self::SET_NULL, self::NO_ACTION);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_MAKE_GREETING_CARDS]);
        if ($crontabTask instanceof CrontabTask) {
            $crontabTask->delete();
        }

        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_SEND_GREETING_CARDS]);
        if ($crontabTask instanceof CrontabTask) {
            $crontabTask->delete();
        }

        $this->dropColumn(Office::tableName(), 'birthday_greeting_card_template_subject');
        $this->dropColumn(Office::tableName(), 'birthday_greeting_card_template_message');
        $this->dropColumn(Holiday::tableName(), 'greeting_card_template_subject');
        $this->dropColumn(Holiday::tableName(), 'greeting_card_template_message');

        $this->dropTable('salary_person_greeting');
    }
}
