<?php
use app\components\CustomMigration as Migration;
use yii\db\Query;

/**
 * Class m181019_063934_operational_director
 */
class m181019_063934_operational_director extends Migration
{

    protected $user = 'antipova.a';
    protected $fromRole = 'project.manager';
    protected $toRole = 'operational.director';
        protected $toDescription = 'Операционный директор';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $query = new Query();

        if (!$query->from('auth_item')
            ->where([
                'name' => $this->toRole,
                'type' => 1
            ])
            ->exists()) {
            $this->insert('auth_item', [
                'name' => $this->toRole,
                'type' => 1,
                'description' => $this->toDescription,
                'created_at' => time(),
                'updated_at' => time()
            ]);
        }

        $insert = "INSERT INTO auth_item_child (parent, child) SELECT '" . $this->toRole . "', old.child FROM auth_item_child as old WHERE parent = '" . $this->fromRole . "'";
        Yii::$app->db->createCommand($insert)->execute();


        $userId = (new Query())->select('id')
            ->from('user')
            ->where(['username' => $this->user])
            ->scalar();

        if ($userId) {
            $this->delete('auth_assignment', [
                'item_name' => $this->fromRole,
                'user_id' => $userId,
            ]);

            $this->insert('auth_assignment', [
                'item_name' => $this->toRole,
                'user_id' => $userId,
            ]);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

        $userId = (new Query())->select('id')
            ->from('user')
            ->where(['username' => $this->user])
            ->scalar();

        if ($userId) {
            $this->delete('auth_assignment', [
                'item_name' => $this->toRole,
                'user_id' => $userId,
            ]);

            $this->insert('auth_assignment', [
                'item_name' => $this->fromRole,
                'user_id' => $userId,
            ]);
        }

        $this->delete('auth_item', ['name' => $this->toRole, 'type' => 1]);
    }
}
