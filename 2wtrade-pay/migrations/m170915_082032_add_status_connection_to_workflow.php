<?php

use app\components\CustomMigration;
use app\modules\order\models\OrderStatus;
use app\modules\order\models\OrderWorkflowStatus;

/**
 * Handles adding status_connection to table `workflow`.
 */
class m170915_082032_add_status_connection_to_workflow extends CustomMigration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        /**
         * @var OrderWorkflowStatus[] $statuses
         */
        $statuses = OrderWorkflowStatus::find()->where(['status_id' => [OrderStatus::STATUS_CC_APPROVED, OrderStatus::STATUS_DELIVERY_PENDING, OrderStatus::STATUS_LOG_DEFERRED]])->all();

        foreach ($statuses as $status) {
            $parents = $status->getParents();

            if(!in_array(OrderStatus::STATUS_CC_REJECTED, $parents)) {
                $parents[] = OrderStatus::STATUS_CC_REJECTED;
                sort($parents);
                $status->parents = $parents;
                $status->save();
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        /**
         * @var OrderWorkflowStatus[] $statuses
         */
        $statuses = OrderWorkflowStatus::find()->where(['status_id' => [OrderStatus::STATUS_CC_APPROVED, OrderStatus::STATUS_DELIVERY_PENDING, OrderStatus::STATUS_LOG_DEFERRED]])->all();

        foreach ($statuses as $status) {
            $parents = explode(',', $status->parents);

            if(in_array(OrderStatus::STATUS_CC_REJECTED, $parents)) {
                $index = array_search(OrderStatus::STATUS_CC_REJECTED, $parents);
                unset($parents[$index]);
                $status->parents = implode(',', $parents);
                $status->save();
            }
        }
    }
}
