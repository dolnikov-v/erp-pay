<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m170428_035915_trash_check_lead
 */
class m170428_035915_trash_check_lead extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = CrontabTask::find()
            ->byName('call_center_send_analysis_trash')
            ->one();

        if ($crontabTask) {
            $crontabTask->delete();
        }

        $crontabTask = CrontabTask::find()
            ->byName('call_center_get_analysis_trash')
            ->one();

        if ($crontabTask) {
            $crontabTask->delete();
        }

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

        $crontabTask = new CrontabTask();
        $crontabTask->name = 'call_center_send_analysis_trash';
        $crontabTask->description = 'Отправка трэшей на анализиз в колл-центр';
        $crontabTask->save();

        $crontabTask = new CrontabTask();
        $crontabTask->name = 'call_center_get_analysis_trash';
        $crontabTask->description = 'Получение результатов анализиза трэшей из колл-центра';
        $crontabTask->save();


    }
}
