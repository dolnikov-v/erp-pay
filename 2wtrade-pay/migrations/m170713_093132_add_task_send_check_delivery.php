<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m170713_093132_add_task_send_check_delivery
 */
class m170713_093132_add_task_send_check_delivery extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_CC_SEND_CHECK_DELIVERY]);
        if (!$crontabTask) {
            $crontabTask = new CrontabTask();
            $crontabTask->name = CrontabTask::TASK_CC_SEND_CHECK_DELIVERY;
            $crontabTask->description = "Отправка 10 заказов(выкуп) в check_delivery";
            $crontabTask->save();
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_CC_SEND_CHECK_DELIVERY]);
        if ($crontabTask instanceof CrontabTask) {
            $crontabTask->delete();
        }
    }
}
