<?php

use app\components\CustomMigration as Migration;
use app\modules\deliveryreport\models\DeliveryReportRecord;

/**
 * Handles adding delivery_time to table `delivery_report`.
 */
class m170118_102237_add_delivery_time_to_delivery_report extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(DeliveryReportRecord::tableName(), 'delivery_time_from', $this->string(50));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(DeliveryReportRecord::tableName(), 'delivery_time_from');
    }
}
