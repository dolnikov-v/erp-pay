<?php

/**
 * Class m181122_060925_add_currency_rate_page_rules
 */
class m181122_060925_add_currency_rate_page_rules extends \app\components\PermissionMigration
{
    protected $roles = [
        'finance.manager' => [
            'catalog.currencyrate.index'
        ],
        'technical.director' => [
            'catalog.currencyrate.index'
        ],
        'admin' => [
            'catalog.currencyrate.index'
        ],
        'auditor' => [
            'catalog.currencyrate.index'
        ],
        'controller.analyst' => [
            'catalog.currencyrate.index'
        ],
        'country.curator' => [
            'catalog.currencyrate.index'
        ],
        'Demand & Supply Planner' => [
            'catalog.currencyrate.index'
        ],
        'finance.director' => [
            'catalog.currencyrate.index'
        ],
        'operational.director' => [
            'catalog.currencyrate.index'
        ],
        'project.manager' => [
            'catalog.currencyrate.index'
        ],
        'quality.manager' => [
            'catalog.currencyrate.index'
        ],
        'sales.director' => [
            'catalog.currencyrate.index'
        ],
        'support.manager' => [
            'catalog.currencyrate.index'
        ],
    ];
}
