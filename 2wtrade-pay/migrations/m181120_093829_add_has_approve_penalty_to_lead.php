<?php
use app\components\CustomMigration as Migration;
use yii\db\Query;

/**
 * Class m181120_093829_add_has_approve_penalty_to_lead
 */
class m181120_093829_add_has_approve_penalty_to_lead extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('lead', 'has_approve_penalty', $this->boolean()->defaultValue(false));

        $cronTaskId = (new Query())->select('id')
            ->from('crontab_task')
            ->where(['name' => \app\modules\administration\models\CrontabTask::TASK_ADCOMBO_PARSE_APPROVE_PENALTIES])
            ->scalar();

        $data = [
            'name' => \app\modules\administration\models\CrontabTask::TASK_ADCOMBO_PARSE_APPROVE_PENALTIES,
            'description' => 'Получение информации из адкомбо о штрафах по доапруву',
            'updated_at' => time(),
        ];

        if (!$cronTaskId) {
            $this->insert('crontab_task', $data);
        } else {
            $data['created_at'] = time();
            $this->update('crontab_task', $data, ['id' => $cronTaskId]);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('lead', 'has_approve_penalty');
        $this->delete('crontab_task', ['name' => \app\modules\administration\models\CrontabTask::TASK_ADCOMBO_PARSE_APPROVE_PENALTIES]);
    }
}
