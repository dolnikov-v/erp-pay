<?php
use app\components\CustomMigration as Migration;
use app\models\Country;
use app\models\User;

/**
 * Class m170605_034814_add_cccountry_curator
 */
class m170605_034814_add_cccountry_curator extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Country::tableName(), 'curator_user_id', $this->integer()->defaultValue(null));
        $this->addForeignKey(null, Country::tableName(), 'curator_user_id', User::tableName(), 'id', self::SET_NULL, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Country::tableName(), 'curator_user_id');
    }
}
