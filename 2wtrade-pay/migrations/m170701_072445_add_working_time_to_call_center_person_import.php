<?php

use app\components\CustomMigration;

/**
 * Handles adding working_time to table `call_center_person_import`.
 */
class m170701_072445_add_working_time_to_call_center_person_import extends CustomMigration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('call_center_person_import', 'shift_time', $this->string(255));
        $this->addColumn('call_center_person_import', 'lunch_time', $this->string(255));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('call_center_person_import', 'shift_time');
        $this->dropColumn('call_center_person_import', 'lunch_time');
    }
}
