<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160905_081643_add_column_fileanswer
 */
class m160905_081643_add_column_fileanswer extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('crontab_task_log_answer', 'file_answer', $this->string()->defaultValue(null)->after('answer'));
        $this->addColumn('crontab_task_log_subanswer', 'file_answer', $this->string()->defaultValue(null)->after('answer'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('crontab_task_log_answer', 'file_answer');
        $this->dropColumn('crontab_task_log_subanswer', 'file_answer');
    }
}
