<?php
use app\components\CustomMigration as Migration;
use app\models\Notification;
use app\modules\administration\models\CrontabTask;

/**
 * Class m170619_103120_add_notification_for_rapid_autotrash
 */
class m170619_103120_add_notification_for_rapid_autotrash extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $notification = Notification::findOne(['trigger' => Notification::TRIGGER_RAPID_INCREASE_ORDERS_IN_AUTOTRASH]);

        if (!($notification instanceof Notification)) {
            $notification = new Notification([
                'description' => "Количество заказов в автотреше по всем странам увеличилось на {count} (норма - {normal}).",
                'trigger' => Notification::TRIGGER_RAPID_INCREASE_ORDERS_IN_AUTOTRASH,
                'type' => Notification::TYPE_WARNING,
                'group' => Notification::GROUP_REPORT,
                'active' => Notification::ACTIVE,
            ]);
            $notification->save(false);
        }

        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_REPORT_RAPID_INCREASE_ORDERS_IN_AUTOTRASH]);

        if (!($crontabTask instanceof CrontabTask)) {
            $crontabTask = new CrontabTask();
            $crontabTask->name = CrontabTask::TASK_REPORT_RAPID_INCREASE_ORDERS_IN_AUTOTRASH;
            $crontabTask->description = "Резкое увеличение заказов в автотреше";
            $crontabTask->save();
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $notification = Notification::findOne(['trigger' => Notification::TRIGGER_RAPID_INCREASE_ORDERS_IN_AUTOTRASH]);

        if ($notification instanceof Notification) {
            $notification->delete();
        }

        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_REPORT_RAPID_INCREASE_ORDERS_IN_AUTOTRASH]);

        if ($crontabTask instanceof CrontabTask) {
            $crontabTask->delete();
        }
    }
}
