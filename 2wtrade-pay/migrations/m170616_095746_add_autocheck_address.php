<?php
use app\components\CustomMigration as Migration;
use app\modules\callcenter\models\CallCenterRequest;

/**
 * Class m170616_095746_add_autocheck_address
 */
class m170616_095746_add_autocheck_address extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(CallCenterRequest::tableName(), 'autocheck_address', $this->string(50)->after('comment'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(CallCenterRequest::tableName(), 'autocheck_address');
    }
}
