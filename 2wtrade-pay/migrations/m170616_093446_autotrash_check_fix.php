<?php
use app\components\CustomMigration as Migration;
use yii\db\Query;

/**
 * Class m170616_093446_autotrash_check_fix
 */
class m170616_093446_autotrash_check_fix extends Migration
{
    const ROLE = 'country.curator';

    const RULE = 'catalog.autotrash.autotrashswitch';
    const RULE_BAD = 'catalog.autotrash.autotrash-switch';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $query = new Query();

        $is_rule = $query->select('*')->from($this->authManager->itemTable)
            ->where(['name' => self::RULE, 'type' => 2])
            ->one();

        if (!$is_rule) {
            $this->insert($this->authManager->itemTable, [
                'name' => self::RULE,
                'description' => self::RULE,
                'type' => 2,
                'created_at' => time(),
                'updated_at' => time()]);
        }

        $is_can = $query->select('*')->from($this->authManager->itemChildTable)->where([
            'parent' => self::ROLE,
            'child' => self::RULE
        ])->one();

        if (!$is_can) {
            $this->insert($this->authManager->itemChildTable, [
                'parent' => self::ROLE,
                'child' => self::RULE
            ]);
        }

        $this->delete($this->authManager->itemChildTable, [
            'child' => self::RULE_BAD
        ]);

        $this->delete($this->authManager->itemTable, [
            'name' => self::RULE_BAD
        ]);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->authManager->itemChildTable, [
            'parent' => self::ROLE,
            'child' => self::RULE
        ]);

    }
}
