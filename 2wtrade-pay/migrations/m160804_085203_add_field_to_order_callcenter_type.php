<?php

use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Handles adding field to table `order_callcenter_type`.
 */
class m160804_085203_add_field_to_order_callcenter_type extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('order', 'call_center_type', "ENUM('order', 'new_return') NOT NULL DEFAULT 'order' AFTER `call_center_again`");
        $this->addColumn('delivery_request', 'api_error_type',"ENUM('system', 'courier') NOT NULL DEFAULT 'system' AFTER `api_error`");
        $this->createIndex(null, 'order', 'call_center_type');
        $this->createIndex(null, 'delivery_request', 'api_error_type');

        $crontabTask = new CrontabTask();
        $crontabTask->name = 'call_center_resend_requests';
        $crontabTask->description = 'Повторная отправка заказов по заявкам.';
        $crontabTask->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $cronTaskSend = CrontabTask::find()
            ->byName('call_center_resend_requests')
            ->one();

        if ($cronTaskSend) {
            $cronTaskSend->delete();
        }

        $this->dropIndex($this->getIdxName('delivery_request', 'api_error_type'), 'delivery_request');
        $this->dropIndex($this->getIdxName('order', 'call_center_type'), 'order');
        $this->dropColumn('delivery_request', 'api_error_type');
        $this->dropColumn('order', 'call_center_type');
    }
}
