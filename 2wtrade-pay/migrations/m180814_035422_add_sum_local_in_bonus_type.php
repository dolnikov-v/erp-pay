<?php
use app\components\CustomMigration as Migration;
use app\modules\salary\models\BonusType;

/**
 * Class m180814_035422_add_sum_local_in_bonus_type
 */
class m180814_035422_add_sum_local_in_bonus_type extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(BonusType::tableName(), 'sum_local', $this->float()->after('sum'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(BonusType::tableName(), 'sum_local');
    }
}
