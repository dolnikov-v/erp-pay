<?php
use app\components\CustomMigration as Migration;

/**
 * Class m190403_051134_add_notification_adcombo_parser
 */
class m190403_051134_add_notification_adcombo_parser extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('notification', [
            'description' => 'Ошибка парсера данных из AdCombo: {error}',
            'trigger' => 'adcombo.parser.error',
            'type' => 'danger',
            'group' => 'system',
            'active' => 1
        ]);

        $roles = [
            'technical.director',
            'support.manager',
        ];

        foreach ($roles as $role) {
            $this->insert('notification_role', [
                'role' => $role,
                'trigger' => 'adcombo.parser.error',
                'created_at' => time(),
                'updated_at' => time(),
            ]);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete('user_notification_setting', ['trigger' => 'adcombo.parser.error']);
        $this->delete('notification', ['trigger' => 'adcombo.parser.error']);
    }
}
