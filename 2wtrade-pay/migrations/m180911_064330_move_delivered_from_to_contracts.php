<?php
use app\components\CustomMigration as Migration;

/**
 * Class m180911_064330_move_delivered_from_to_contracts
 */
class m180911_064330_move_delivered_from_to_contracts extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        $this->createTable('delivery_contract_time', [
            'id' => $this->primaryKey(),
            'contract_id' => $this->integer()->notNull(),
            'delivered_from' => $this->integer()->defaultValue(null),
            'delivered_to' => $this->float()->defaultValue(null),
            'field_name' => $this->string(),
            'field_value' => $this->string()
        ]);

        $this->addForeignKey(null, 'delivery_contract_time', 'contract_id', 'delivery_contract', 'id', self::CASCADE, self::RESTRICT);

        $sql = 'DROP FUNCTION IF EXISTS is_bad_delivery_request_sent_at;';
        $this->db->createCommand($sql)->execute();

        $sqlFunction = <<<SQL
CREATE FUNCTION `is_bad_delivery_request_sent_at`(
  `v_delivery_request_id` INTEGER,
  `v_zip` VARCHAR(50),
  `v_city` VARCHAR(50),
  `v_province` VARCHAR(50)
)
RETURNS tinyint(4)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT ''        
BEGIN
    DECLARE v_contract_id INTEGER DEFAULT NULL;
    DECLARE v_delivered_to INTEGER DEFAULT NULL;
    DECLARE v_sent_at INTEGER DEFAULT NULL;
    DECLARE is_bad_time BOOL DEFAULT FALSE;
      SELECT 
     `delivery_contract`.`id`, 
    `delivery_contract`.`delivered_to`,
    `delivery_request`.sent_at
    INTO 
    v_contract_id, 
    v_delivered_to,
      v_sent_at
      FROM `delivery_request`
      LEFT JOIN `delivery` ON `delivery_request`.delivery_id = `delivery`.id 
      LEFT JOIN `delivery_contract` ON `delivery_contract`.delivery_id = `delivery`.id
      WHERE 
      `delivery_request`.id = v_delivery_request_id 
         AND CASE 
         WHEN `delivery_contract`.date_from IS NOT NULL AND `delivery_contract`.date_to IS NOT NULL
         THEN DATE_FORMAT(FROM_UNIXTIME(`delivery_request`.sent_at), '%Y-%m-%d') >= `delivery_contract`.date_from AND DATE_FORMAT(FROM_UNIXTIME(`delivery_request`.sent_at), '%Y-%m-%d') <= `delivery_contract`.date_to
         WHEN `delivery_contract`.date_from IS NOT NULL AND `delivery_contract`.date_to IS NULL
         THEN DATE_FORMAT(FROM_UNIXTIME(`delivery_request`.sent_at), '%Y-%m-%d') >= `delivery_contract`.date_from
         WHEN `delivery_contract`.date_from IS NULL AND `delivery_contract`.date_to IS NOT NULL
         THEN DATE_FORMAT(FROM_UNIXTIME(`delivery_request`.sent_at), '%Y-%m-%d') <= `delivery_contract`.date_to 
         ELSE 1 END              
      LIMIT 1;
      
      IF (ISNULL(v_contract_id)) THEN
      RETURN is_bad_time;
    END IF;  
            
      SELECT 
    IFNULL(MAX(`delivery_contract_time`.delivered_to), v_delivered_to) 
      INTO v_delivered_to
      FROM 
      `delivery_contract_time`
      WHERE `delivery_contract_time`.contract_id = v_contract_id
      AND 
      ( 
        (`delivery_contract_time`.field_name = 'customer_zip' and `delivery_contract_time`.field_value = v_zip COLLATE utf8_unicode_ci) OR 
        (`delivery_contract_time`.field_name = 'customer_city' and `delivery_contract_time`.field_value = v_city COLLATE utf8_unicode_ci) OR
        (`delivery_contract_time`.field_name = 'customer_province' and `delivery_contract_time`.field_value = v_province COLLATE utf8_unicode_ci)
      );         
        
     IF (v_sent_at + 86400 * v_delivered_to < CURRENT_TIMESTAMP()) THEN
      SET is_bad_time = TRUE;
    END IF;  
    RETURN is_bad_time;
  END
SQL;

        $this->execute($sqlFunction);

        $this->addColumn('delivery_contract', 'delivered_to', $this->integer()->defaultValue(null));
        $this->addColumn('delivery_contract', 'delivered_from', $this->integer()->defaultValue(null));
        $this->addColumn('delivery_contract', 'paid_from', $this->integer()->defaultValue(null));
        $this->addColumn('delivery_contract', 'paid_to', $this->integer()->defaultValue(null));

        $sqlCopy = <<<SQL
      UPDATE delivery_contract left join delivery on delivery_contract.delivery_id = delivery.id 
      SET 
      delivery_contract.delivered_to = delivery.delivered_to, 
      delivery_contract.delivered_from = delivery.delivered_from,
      delivery_contract.paid_from = delivery.paid_from,
      delivery_contract.paid_to = delivery.paid_to;
SQL;
        $this->execute($sqlCopy);

        $this->dropColumn('delivery', 'delivered_from');
        $this->dropColumn('delivery', 'delivered_to');
        $this->dropColumn('delivery', 'paid_from');
        $this->dropColumn('delivery', 'paid_to');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('delivery_contract_time');

        $sqlDrop = 'DROP FUNCTION IF EXISTS is_bad_delivery_request_sent_at;';
        $this->db->createCommand($sqlDrop)->execute();

        $this->addColumn('delivery', 'delivered_from', $this->integer(11)->defaultValue(null)->after('workflow_id'));
        $this->addColumn('delivery', 'delivered_to', $this->integer(11)->defaultValue(null)->after('delivered_from'));
        $this->addColumn('delivery', 'paid_from', $this->integer(11)->defaultValue(null));
        $this->addColumn('delivery', 'paid_to', $this->integer(11)->defaultValue(null));

        $sql = <<<SQL
      UPDATE delivery left join delivery_contract on delivery_contract.delivery_id = delivery.id 
      SET 
      delivery.delivered_to = delivery_contract.delivered_to, 
      delivery.delivered_from = delivery_contract.delivered_from,
      delivery.paid_to = delivery_contract.paid_to, 
      delivery.paid_from = delivery_contract.paid_from;
SQL;
        $this->execute($sql);

        $this->dropColumn('delivery_contract', 'delivered_to');
        $this->dropColumn('delivery_contract', 'delivered_from');
        $this->dropColumn('delivery_contract', 'paid_from');
        $this->dropColumn('delivery_contract', 'paid_to');
    }
}
