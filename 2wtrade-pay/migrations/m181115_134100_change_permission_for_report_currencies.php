<?php
use app\components\PermissionMigration as Migration;

/**
 * Class m181115_134100_change_permission_for_report_currencies
 */
class m181115_134100_change_permission_for_report_currencies extends Migration
{
    protected $oldPermissions = ['delivery.control.setcurrencies'];
    protected $newPermissions = ['deliveryreport.report.setcurrencies'];
    protected $permissions = [];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $query = new \yii\db\Query();
        $permissions = $query->from($this->authManager->itemChildTable)->where([
            'child' => 'deliveryreport.report.view'
        ])->all();
        foreach ($permissions as $permission) {
            $this->roles[$permission['parent']] = $this->permissions;
        }

        $this->permissions = $this->oldPermissions;
        parent::safeDown();
        $this->permissions = $this->newPermissions;
        parent::safeUp();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $query = new \yii\db\Query();
        $permissions = $query->from($this->authManager->itemChildTable)->where([
            'child' => 'deliveryreport.report.view'
        ])->all();
        foreach ($permissions as $permission) {
            $this->roles[$permission['parent']] = $this->permissions;
        }
        $this->permissions = $this->newPermissions;
        parent::safeDown();
        $this->permissions = $this->oldPermissions;
        parent::safeUp();
    }
}