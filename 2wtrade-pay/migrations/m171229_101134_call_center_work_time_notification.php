<?php
use app\components\CustomMigration as Migration;
use app\models\Notification;
use app\modules\administration\models\CrontabTask;

/**
 * Class m171229_101134_call_center_work_time_notification
 */
class m171229_101134_call_center_work_time_notification extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $notification = new Notification([
            'description' => 'За последний час в офисе {officeName} не было апрувов по новым лидам',
            'trigger' => Notification::TRIGGER_CALL_CENTER_NO_APPROVE_LAST_HOUR,
            'type' => Notification::TYPE_DANGER,
            'group' => Notification::GROUP_SYSTEM,
            'active' => Notification::ACTIVE,
        ]);
        $notification->save(false);

        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_CALL_CENTER_NO_APPROVE_LAST_HOUR]);
        if (!$crontabTask) {
            $crontabTask = new CrontabTask();
            $crontabTask->name = CrontabTask::TASK_CALL_CENTER_NO_APPROVE_LAST_HOUR;
            $crontabTask->description = "Уведомление о том, что за последний час в офисе не было апрувов по новым лидам";
            $crontabTask->save();
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_CALL_CENTER_NO_APPROVE_LAST_HOUR]);
        $crontabTask->delete();
        $notification = Notification::findOne(['trigger' => Notification::TRIGGER_CALL_CENTER_NO_APPROVE_LAST_HOUR]);
        $notification->delete();
    }
}
