<?php
use app\components\PermissionMigration as Migration;
use app\modules\checklist\models\CheckListItem;
use yii\db\Query;

/**
 * Class m180118_110230_check_list_category
 */
class m180118_110230_check_list_category extends Migration
{
    protected $permissions = [
        'checklist.admincategory.index',
        'checklist.admincategory.delete',
        'checklist.admincategory.edit',
        'checklist.tm.index',
        'checklist.tm.set',
    ];

    protected $roles = [
        'auditor' => [
            'checklist.admincategory.index',
            'checklist.admincategory.delete',
            'checklist.admincategory.edit'
        ],
        'deliveryreport.clerk' => [
            'checklist.admincategory.index',
            'checklist.admincategory.delete',
            'checklist.admincategory.edit'
        ],
        'project.manager' => [
            'checklist.admincategory.index',
            'checklist.admincategory.delete',
            'checklist.admincategory.edit',
            'checklist.tm.index',
        ],
        'admin' => [
            'checklist.tm.index',
        ],
        'country.curator' => [
            'checklist.tm.index',
            'checklist.tm.set',
        ],
        'finance.director' => [
            'checklist.tm.index',
        ],
    ];

    const TEXT = [
        'Чек-лист по финансам' => 'Finance checklist',
        'Чек-лист по кадрам' => 'Personnel checklist',
        'Чек-лист по безопасности' => 'Security checklist',
        'Чек-листы подотчетных сотрудников' => 'Checklists of accountable employees',
    ];


    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('check_list_category', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'type' => $this->string(),
            'position' => $this->integer(),
            'active' => $this->integer(1),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $this->tableOptions);

        $this->addColumn('check_list_item', 'category_id', $this->integer()->after('explainable'));
        $this->addForeignKey('fk_check_list_item_category_id', 'check_list_item', 'category_id', 'check_list_category', 'id', self::SET_NULL, self::NO_ACTION);

        $dataCategory = [
            [
                'id' => 1,
                'name' => 'Чек-лист по финансам',
                'type' => CheckListItem::TYPE_TM,
                'position' => 1,
                'active' => 1,
                'created_at' => time(),
                'updated_at' => time()
            ],
            [
                'id' => 2,
                'name' => 'Чек-лист по кадрам',
                'type' => CheckListItem::TYPE_TM,
                'position' => 2,
                'active' => 1,
                'created_at' => time(),
                'updated_at' => time()
            ],
            [
                'id' => 3,
                'name' => 'Чек-лист по безопасности',
                'type' => CheckListItem::TYPE_TM,
                'position' => 3,
                'active' => 1,
                'created_at' => time(),
                'updated_at' => time()
            ],
            [
                'id' => 4,
                'name' => 'Чек-листы подотчетных сотрудников',
                'type' => CheckListItem::TYPE_TM,
                'position' => 4,
                'active' => 1,
                'created_at' => time(),
                'updated_at' => time()
            ],
        ];


        Yii::$app->db->createCommand()->batchInsert(
            'check_list_category',
            [
                'id',
                'name',
                'type',
                'position',
                'active',
                'created_at',
                'updated_at'
            ],
            $dataCategory
        )->execute();



        $dataItem = [
            [
                'category_id' => 1,
                'name' => 'Долг каждой КС перед 2wtrade по COD',
                'description' => 'Долг не более 10 000 $ и не более 1 месяца',
                'type' => CheckListItem::TYPE_TM,
                'position' => 1,
                'active' => 1,
                'days' => 1,
                'created_at' => time(),
                'updated_at' => time()
            ],
            [
                'category_id' => 1,
                'name' => 'Прибыль компании по итогам дня',
                'description' => 'Не менее 30% (на основе текущего и статусов заказов)',
                'type' => CheckListItem::TYPE_TM,
                'position' => 2,
                'active' => 1,
                'days' => 1,
                'created_at' => time(),
                'updated_at' => time()
            ],
            [
                'category_id' => 1,
                'name' => 'По заказам получившим статус "выкуп" были фактически получены деньги от КС и выставлен статус "Деньги получены"',
                'description' => 'Условие выполняется для заказов отправленных 45 дней назад',
                'type' => CheckListItem::TYPE_TM,
                'position' => 3,
                'active' => 1,
                'days' => 1,
                'created_at' => time(),
                'updated_at' => time()
            ],
            [
                'category_id' => 1,
                'name' => 'В бранч-офисах и КЦ аренда и интернет оплачены',
                'description' => 'Крайний срок оплаты 5-е число каждого месяца',
                'type' => CheckListItem::TYPE_TM,
                'position' => 4,
                'active' => 1,
                'days' => 1,
                'created_at' => time(),
                'updated_at' => time()
            ],
            [
                'category_id' => 2,
                'name' => 'Все данные по активным сотрудникам в ЗП проекте введены корректно',
                'description' => 'Данные присутствуют в системе, по отсутствующим сотрудникам ЗП начислена не будет',
                'type' => CheckListItem::TYPE_TM,
                'position' => 5,
                'active' => 1,
                'days' => 1,
                'created_at' => time(),
                'updated_at' => time()
            ],
            [
                'category_id' => 2,
                'name' => 'Все уволенные сотрудники КЦ и бранч-офисов имеют соответствующую отметку в NPay (ЗП проект) и их доступ в системы call и pay заблокирован',
                'description' => 'Всем уволенным сотрудникам заблокирован доступ.',
                'type' => CheckListItem::TYPE_TM,
                'position' => 6,
                'active' => 1,
                'days' => 1,
                'created_at' => time(),
                'updated_at' => time()
            ],
            [
                'category_id' => 2,
                'name' => 'ЗП полностью рассчитана и передана на оплату в срок',
                'description' => 'Срок до 3-го числа каждого месяца',
                'type' => CheckListItem::TYPE_TM,
                'position' => 7,
                'active' => 1,
                'days' => 1,
                'created_at' => time(),
                'updated_at' => time()
            ],
            [
                'category_id' => 3,
                'name' => 'Проанализирован отчет Crocotime всех сотрудников, по спорным моментам просмотрены камеры.',
                'description' => 'По фактам нарушения стандартов компании сотрудникам КЦ выписаны штрафы',
                'type' => CheckListItem::TYPE_TM,
                'position' => 8,
                'active' => 1,
                'days' => 1,
                'created_at' => time(),
                'updated_at' => time()
            ],
            [
                'category_id' => 3,
                'name' => 'Колл-центр оборудован микротиком, видеокамерами и 2 независимымыми линиями интернета',
                'description' => 'Условие выполнено, КЦ соответствует стандартам компании',
                'type' => CheckListItem::TYPE_TM,
                'position' => 9,
                'active' => 1,
                'days' => 1,
                'created_at' => time(),
                'updated_at' => time()
            ],

            [
                'category_id' => 4,
                'name' => 'Выполнен чеклист супервайзера по каждой стране',
                'description' => 'На 100 %',
                'type' => CheckListItem::TYPE_TM,
                'position' => 10,
                'active' => 1,
                'days' => 1,
                'created_at' => time(),
                'updated_at' => time()
            ],
            [
                'category_id' => 4,
                'name' => 'Выполнен чеклист аудитора по каждой стране',
                'description' => 'На 100 %',
                'type' => CheckListItem::TYPE_TM,
                'position' => 11,
                'active' => 1,
                'days' => 1,
                'created_at' => time(),
                'updated_at' => time()
            ],
            [
                'category_id' => 4,
                'name' => 'Выполнен чеклист контролера продаж по каждой стране',
                'description' => 'На 100 %',
                'type' => CheckListItem::TYPE_TM,
                'position' => 12,
                'active' => 1,
                'days' => 1,
                'created_at' => time(),
                'updated_at' => time()
            ],
            [
                'category_id' => 4,
                'name' => 'Часть новых заказов поступают из внутренних источников траффика (сайт 2wstore и входящая линия КЦ, дистрибьюторы)',
                'description' => 'Из новых источников поступают 10 % заказов',
                'type' => CheckListItem::TYPE_TM,
                'position' => 13,
                'active' => 1,
                'days' => 1,
                'created_at' => time(),
                'updated_at' => time()
            ],

        ];


        Yii::$app->db->createCommand()->batchInsert(
            'check_list_item',
            [
                'category_id',
                'name',
                'description',
                'type',
                'position',
                'active',
                'days',
                'created_at',
                'updated_at'
            ],
            $dataItem
        )->execute();


        $query = new Query();

        foreach ($this::TEXT as $ruText => $enText) {

            $is = $query->select('id')
                ->from('i18n_source_message')
                ->where(['category' => 'common', 'message' => $ruText])->one();

            if (!$is) {
                $this->insert('i18n_source_message', ['category' => 'common', 'message' => $ruText]);
                $id = Yii::$app->db->getLastInsertID();
                $this->insert('i18n_message', ['id' => $id, 'language' => 'en-US', 'translation' => $enText]);
            }
        }

        parent::safeUp();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

        $this->dropForeignKey('fk_check_list_item_category_id', 'check_list_item');
        $this->dropColumn('check_list_item', 'category_id');
        $this->dropTable('check_list_category');

        foreach ($this::TEXT as $ruText => $enText) {
            $this->delete('i18n_source_message', ['category' => 'common', 'message' => $ruText]);
        }

        Yii::$app->db->createCommand()->delete(
            'check_list_item',
            ['type' => CheckListItem::TYPE_TM]
        )->execute();

        parent::safeDown();
    }
}
