<?php
use app\components\CustomMigration as Migration;
use app\modules\finance\models\ReportExpensesCountryItem;
use app\modules\finance\models\ReportExpensesCountryItemProduct;

/**
 * Class m180322_110939_report_expenses_country_item_lead_limit
 */
class m180322_110939_report_expenses_country_item_lead_limit extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(ReportExpensesCountryItem::tableName(), 'lead_limit', $this->integer());

        $this->dropForeignKey($this->getFkName('report_expenses_country_item_product', 'item_id'), 'report_expenses_country_item_product', 'item_id');
        $this->addForeignKey($this->getFkName('report_expenses_country_item_product', 'item_id'), 'report_expenses_country_item_product', 'item_id', 'report_expenses_country_item', 'id', self::CASCADE, self::CASCADE);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(ReportExpensesCountryItem::tableName(), 'lead_limit');
    }
}
