<?php
use app\components\CustomMigration as Migration;
use app\modules\catalog\models\Autotrash;

/**
 * Class m170619_094233_modify_column_filter_on_auto_trash
 */
class m170619_094233_modify_column_filter_on_auto_trash extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn(Autotrash::tableName(), 'filter', $this->string(25)->notNull());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn(Autotrash::tableName(), 'filter', $this->enum(['max_length', 'min_length', 'regexp'])->defaultValue('regexp'));
    }
}
