<?php
use app\components\PermissionMigration as Migration;
use app\modules\salary\models\MonthlyStatement;
use app\modules\salary\models\MonthlyStatementData;

/**
 * Class m180130_081943_monthly_statement_close
 */
class m180130_081943_monthly_statement_close extends Migration
{

    protected $permissions = ['report.salary.close'];

    protected $roles = [
        'project.manager' => [
            'report.salary.close'
        ],
        'country.curator' => [
            'report.salary.close'
        ],
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(MonthlyStatement::tableName(), 'closed_at', $this->integer());
        $this->addColumn(MonthlyStatement::tableName(), 'closed_by', $this->integer());
        $this->addColumn(MonthlyStatementData::tableName(), 'count_normal_hours', $this->decimal(5, 2));
        $this->addColumn(MonthlyStatementData::tableName(), 'count_time', $this->decimal(5, 2));
        $this->addColumn(MonthlyStatementData::tableName(), 'count_calls', $this->integer());
        $this->addColumn(MonthlyStatementData::tableName(), 'count_extra_hours', $this->decimal(5, 2));
        $this->addColumn(MonthlyStatementData::tableName(), 'holiday_worked_time', $this->decimal(5, 2));
        $this->addColumn(MonthlyStatementData::tableName(), 'salary_hand_local_extra', $this->double());
        $this->addColumn(MonthlyStatementData::tableName(), 'salary_hand_usd_extra', $this->double());
        $this->addColumn(MonthlyStatementData::tableName(), 'salary_hand_local_holiday', $this->double());
        $this->addColumn(MonthlyStatementData::tableName(), 'salary_hand_usd_holiday', $this->double());
        $this->addColumn(MonthlyStatementData::tableName(), 'penalty_sum_local', $this->double());
        $this->addColumn(MonthlyStatementData::tableName(), 'penalty_sum_usd', $this->double());
        $this->addColumn(MonthlyStatementData::tableName(), 'bonus_sum_local', $this->double());
        $this->addColumn(MonthlyStatementData::tableName(), 'bonus_sum_usd', $this->double());
        $this->addColumn(MonthlyStatementData::tableName(), 'salary_hand_local', $this->double());
        $this->addColumn(MonthlyStatementData::tableName(), 'salary_tax_local', $this->double());
        $this->addColumn(MonthlyStatementData::tableName(), 'salary_hand_usd', $this->double());
        $this->addColumn(MonthlyStatementData::tableName(), 'salary_tax_usd', $this->double());

        parent::safeUp();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(MonthlyStatement::tableName(), 'closed_at');
        $this->dropColumn(MonthlyStatement::tableName(), 'closed_by');
        $this->dropColumn(MonthlyStatementData::tableName(), 'count_normal_hours');
        $this->dropColumn(MonthlyStatementData::tableName(), 'count_time');
        $this->dropColumn(MonthlyStatementData::tableName(), 'count_calls');
        $this->dropColumn(MonthlyStatementData::tableName(), 'count_extra_hours');
        $this->dropColumn(MonthlyStatementData::tableName(), 'holiday_worked_time');
        $this->dropColumn(MonthlyStatementData::tableName(), 'salary_hand_local_extra');
        $this->dropColumn(MonthlyStatementData::tableName(), 'salary_hand_usd_extra');
        $this->dropColumn(MonthlyStatementData::tableName(), 'salary_hand_local_holiday');
        $this->dropColumn(MonthlyStatementData::tableName(), 'salary_hand_usd_holiday');
        $this->dropColumn(MonthlyStatementData::tableName(), 'penalty_sum_local');
        $this->dropColumn(MonthlyStatementData::tableName(), 'penalty_sum_usd');
        $this->dropColumn(MonthlyStatementData::tableName(), 'bonus_sum_local');
        $this->dropColumn(MonthlyStatementData::tableName(), 'bonus_sum_usd');
        $this->dropColumn(MonthlyStatementData::tableName(), 'salary_hand_local');
        $this->dropColumn(MonthlyStatementData::tableName(), 'salary_tax_local');
        $this->dropColumn(MonthlyStatementData::tableName(), 'salary_hand_usd');
        $this->dropColumn(MonthlyStatementData::tableName(), 'salary_tax_usd');

        parent::safeDown();
    }
}
