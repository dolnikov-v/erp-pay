<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160816_051557_notification_catalog_change_type_column_description_to_text
 */
class m160816_051557_notification_catalog_change_type_column_description_to_text extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('notification','description',$this->text()->notNull());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('notification','description',$this->string()->notNull());
    }
}
