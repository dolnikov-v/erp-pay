<?php
use app\components\CustomMigration as Migration;
use app\models\Product;
use app\modules\order\models\Order;
use app\modules\order\models\MarketingList;
use app\modules\order\models\MarketingListOrder;
use app\models\Country;
use app\models\User;
use \yii\db\Query;

/**
 * Class m170201_085754_marketing_lists
 */
class m170201_085754_marketing_lists extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        $query = new Query();

        $rules = [
            'order.marketinglist.index',
            'order.index.generatemarketinglist',
            'order.marketinglist.deleteorder',
            'order.marketinglist.deletelist',
            'order.marketinglist.details',
            'order.marketinglist.sendorderstodelivery',
        ];

        $roles = [
            'business_analyst',
            'country.curator'
        ];

        foreach ($rules as $rule) {
            $is_report = $query->select('*')->from($this->authManager->itemTable)
                ->where([
                    'name' => $rule,
                    'type' => 2])
                ->one();

            if (!$is_report) {
                $this->insert($this->authManager->itemTable, [
                    'name' => $rule,
                    'type' => 2,
                    'description' => $rule,
                    'created_at' => time(),
                    'updated_at' => time()]);
            }

            foreach ($roles as $role) {

                $is_role = $query->select('*')->from($this->authManager->itemTable)
                    ->where(['name' => $role, 'type' => 1])
                    ->one();

                if (!$is_role) {
                    echo "Error: No ".$role." exists";
                    die();
                }

                $is_br = $query->select('*')->from($this->authManager->itemChildTable)->where([
                    'parent' => $role,
                    'child' => $rule
                ])->one();

                if (!$is_br) {
                    $this->insert($this->authManager->itemChildTable, [
                        'parent' => $role,
                        'child' => $rule
                    ]);
                }
            }
        }


        $this->alterColumn(Order::tableName(), 'call_center_type', "ENUM('order', 'new_return', 'return_noprod', 'jeempo', 'return', 'repeat') NOT NULL DEFAULT 'order' AFTER `call_center_again`");

        $this->addColumn(Product::tableName(), 'use_days', $this->integer()->defaultValue(9999)->notNull());

        $this->createTable(MarketingList::tableName(), [
            'id' => $this->primaryKey(),
            'country_id' => $this->integer()->notNull(),
            'name' => $this->string(64),
            'user_id' => $this->integer()->notNull(),
            'sent_at' => $this->integer(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(null, MarketingList::tableName(), 'country_id', Country::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, MarketingList::tableName(), 'user_id', User::tableName(), 'id', self::CASCADE, self::RESTRICT);

        $this->createTable(MarketingListOrder::tableName(), [
            'id' => $this->primaryKey(),
            'marketing_list_id' => $this->integer()->notNull(),
            'old_order_id' => $this->integer()->notNull(),
            'new_order_id' => $this->integer(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(null, MarketingListOrder::tableName(), 'marketing_list_id', 'marketing_list', 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, MarketingListOrder::tableName(), 'old_order_id', Order::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, MarketingListOrder::tableName(), 'new_order_id', Order::tableName(), 'id', self::SET_NULL, self::RESTRICT);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

        $rules = [
            'order.marketinglist.index',
            'order.index.generatemarketinglist',
            'order.marketinglist.deleteorder',
            'order.marketinglist.deletelist',
            'order.marketinglist.details',
            'order.marketinglist.sendorderstodelivery',
        ];

        $roles = [
            'business_analyst',
            'country.curator'
        ];

        foreach ($rules as $rule) {

            foreach ($roles as $role) {
                $this->delete($this->authManager->itemChildTable, [
                    'parent' => $role,
                    'child' => $rule
                ]);
            }

            $this->delete($this->authManager->itemTable, [
                'name' => $rule,
                'type' => 2]);
        }

        $this->alterColumn(Order::tableName(), 'call_center_type', "ENUM('order', 'new_return', 'return_noprod', 'jeempo', 'return') NOT NULL DEFAULT 'order' AFTER `call_center_again`");

        $this->dropColumn(Product::tableName(), 'use_days');
        $this->dropTable(MarketingListOrder::tableName());
        $this->dropTable(MarketingList::tableName());
    }
}
