<?php
use app\components\CustomMigration as Migration;

/**
 * Class m171109_091952_add_asp_charges_calculator
 */
class m171109_091952_add_asp_charges_calculator extends Migration
{
    protected static $calculators = [
        'ASP Express' => 'app\modules\delivery\components\charges\asp\ASP',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        foreach (self::$calculators as $name => $route) {
            $this->insert('delivery_charges_calculator', [
                'name' => $name,
                'class_path' => $route,
                'active' => 1,
                'created_at' => time(),
            ]);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach (self::$calculators as $name => $route) {
            $this->delete('delivery_charges_calculator', ['name' => $name]);
        }
    }
}
