<?php
use app\components\CustomMigration as Migration;

class m170703_113107_operational_report_block_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('report_operational_settings_team', [
            'id' => $this->primaryKey(),
            'name' => $this->string(64)->defaultValue(null),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->createTable('report_operational_settings_countries', [
            'id' => $this->primaryKey(),
            'country_id' => $this->integer()->notNull(),
            'char_code' => $this->string(10)->notNull(),
            'country_slug' => $this->string(10)->notNull(),
            'country_name' => $this->string(32)->notNull(),
            'status' => $this->boolean()->defaultValue(0),
            'partner_buyout_percent' => $this->integer()->defaultValue(null),
            'team' => $this->integer()->defaultValue(null),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('report_operational_settings_countries');
        $this->dropTable('report_operational_settings_team');
    }

}
