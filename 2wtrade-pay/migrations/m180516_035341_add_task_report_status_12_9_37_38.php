<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m180516_035341_add_task_report_status_12_9_37_38
 */
class m180516_035341_add_task_report_status_12_9_37_38 extends Migration
{
    const RU_TEXT = 'Скопление заказов в колонке 12, 9, 37, 38';
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = new CrontabTask();
        $crontabTask->name = CrontabTask::TASK_REPORT_STATUS_IN_12_9_37_38;
        $crontabTask->description = self::RU_TEXT;
        $crontabTask->save();


        $this->insert('i18n_source_message', ['category' => 'common', 'message' => self::RU_TEXT]);
        $id = Yii::$app->db->getLastInsertID();
        $this->insert('i18n_message', ['id' => $id, 'language' => 'en-US', 'translation' => 'The accumulation of orders in column 12, 9, 37,38']);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = CrontabTask::find()
            ->byName(CrontabTask::TASK_REPORT_STATUS_IN_12_9_37_38)
            ->one();

        if ($crontabTask) {
            $crontabTask->delete();
        }


        $this->delete('i18n_source_message', ['category' => 'common', 'message' => self::RU_TEXT]);

    }
}
