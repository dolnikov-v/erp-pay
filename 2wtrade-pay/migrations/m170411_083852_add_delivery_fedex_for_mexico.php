<?php

use app\components\CustomMigration as Migration;
use app\models\Country;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryApiClass;

/**
 * Class m170411_083852_add_delivery_fedex_for_mexico
 */
class m170411_083852_add_delivery_fedex_for_mexico extends Migration
{
    /**
     * @return Country
     */
    private function getCountry()
    {
        $country = Country::find()
            ->select('id')
            ->where(['char_code' => 'MX'])
            ->one();

        return $country;
    }

    /**
     * @return array|null|DeliveryApiClass
     */
    private function getApiClass()
    {
        $apiClass = DeliveryApiClass::find()
            ->select('id')
            ->where(['name' => 'FedExApi'])
            ->one();

        return $apiClass;
    }

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $country = $this->getCountry();
        $apiClass = $this->getApiClass();

        $delivery = new Delivery();
        $delivery->country_id = $country->id;
        $delivery->name = 'FedEx';
        $delivery->char_code = 'FED';
        $delivery->auto_sending = 0;
        $delivery->api_class_id = $apiClass->id;
        $delivery->brokerage_active = 0;
        $delivery->active = 1;

        $delivery->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $country = $this->getCountry();
        $apiClass = $this->getApiClass();

        Delivery::find()
            ->where([
                'country_id' => $country->id,
                'name' => 'FedEx',
                'char_code' => 'FED',
                'api_class_id' =>  $apiClass->id,
            ])
            ->one()
            ->delete();
    }
}
