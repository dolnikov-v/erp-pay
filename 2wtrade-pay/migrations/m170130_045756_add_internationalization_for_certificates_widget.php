<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170130_045756_add_internationalization_for_certificates_widget
 */
class m170130_045756_add_internationalization_for_certificates_widget extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        //Добавление новых фраз в интернализацию для виджета \modules\widget\widgets\certificates
        $list = [
            'Статус сертификации',
            'Не требует сертификации',
            'Есть сертификаты (наши)',
            'Есть сертификаты (партнёрские)',
            'Нет сертификатов'
        ];

        foreach ($list as $phrase) {
            $this->insert('i18n_source_message', ['category' => 'common', 'message' => $phrase]);
        }
    }


    /**
     * @inheritdoc
     */
    public function safeDown()
    {

    }
}
