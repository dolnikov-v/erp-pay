<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160518_122054_add_active_notification_user
 */
class m160518_122054_add_active_notification_user extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('notification_user', 'active', $this->boolean()->defaultValue(1)->after('trigger'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('notification_user', 'active');
    }
}
