<?php

use app\components\CustomMigration as Migration;
use app\modules\storage\models\StoragePart;
/**
 * Handles adding expected_columns to table `storage_part`.
 */
class m170406_052822_add_expected_columns_to_storage_part extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(StoragePart::tableName(), 'expected', $this->boolean()->defaultValue(null));
        $this->addColumn(StoragePart::tableName(), 'expected_at', $this->integer(11)->defaultValue(null));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(StoragePart::tableName(), 'expected');
        $this->dropColumn(StoragePart::tableName(), 'expected_at');
    }
}
