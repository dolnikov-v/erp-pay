<?php

use yii\db\Migration;
use app\models\Notification;

/**
 * Handles adding notification to table `catalog`.
 */
class m160817_040441_add_notification_to_catalog extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        foreach ($this->getNotifications() as $data) {
            $model = new Notification($data);
            $model->save();
        }
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        foreach ($this->getNotifications() as $data) {
            $model = Notification::findOne(['trigger' => $data['trigger']]);
            if($model instanceof Notification){
                $model->delete();
            }
        }
    }
    /**
     * @return array
     */
    private function getNotifications()
    {
        return [
            [
                'description' => 'Заказ #{order_id} ({country}:{delivery}) не отправлен в КС. ERROR: {error}',
                'trigger' => 'order.not.sent.to.courier',
                'type' => Notification::TYPE_WARNING,
                'group' => Notification::GROUP_COMMON,
                'active' => Notification::ACTIVE,
            ],
            [
                'description' => 'Заказ #{order_id} ({country}) не отправлен в КЦ. ERROR: {error}',
                'trigger' => 'callcenter.response.error',
                'type' => Notification::TYPE_WARNING,
                'group' => Notification::GROUP_COMMON,
                'active' => Notification::ACTIVE,
            ],
            [
                'description' => 'Exception: Заказ #{order_id} ({country}:{delivery}) не отправлен в КС. ERROR: {error}',
                'trigger' => 'exception.sent.to.courier',
                'type' => Notification::TYPE_DANGER,
                'group' => Notification::GROUP_SYSTEM,
                'active' => Notification::ACTIVE,
            ],
            [
                'description' => 'Заказ #{order_id} ({country}:{delivery}) не получил статус из КС. ERROR: {error}',
                'trigger' => 'dont.get.order.info.from.courier',
                'type' => Notification::TYPE_WARNING,
                'group' => Notification::GROUP_COMMON,
                'active' => Notification::ACTIVE,
            ],
            [
                'description' => 'Exception: Заказ #{order_id} ({country}:{delivery}) не получил статус из КС. ERROR: {error}',
                'trigger' => 'exception.get.order.info.from.courier',
                'type' => Notification::TYPE_DANGER,
                'group' => Notification::GROUP_SYSTEM,
                'active' => Notification::ACTIVE,
            ]
        ];
    }
}
