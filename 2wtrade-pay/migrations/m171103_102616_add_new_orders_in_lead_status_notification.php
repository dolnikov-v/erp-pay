<?php
use app\components\CustomMigration as Migration;
use app\models\Notification;
use app\modules\administration\models\CrontabTask;

/**
 * Class m171103_102616_add_new_orders_in_lead_status_notification
 */
class m171103_102616_add_new_orders_in_lead_status_notification extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $notification = new Notification([
            'description' => 'Лидов по странам: {text}',
            'trigger' => Notification::TRIGGER_REPORT_ORDERS_IN_LEAD_STATUS_NEW,
            'type' => Notification::TYPE_INFO,
            'group' => Notification::GROUP_REPORT,
            'active' => Notification::ACTIVE,
        ]);
        $notification->save();

        $crontabTask = new CrontabTask();
        $crontabTask->name = 'orders_in_lead_status_new';
        $crontabTask->description = 'Уведомление о лидах с тенденциями по странам';
        $crontabTask->save();
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $notification = Notification::findOne(['trigger' => Notification::TRIGGER_REPORT_ORDERS_IN_LEAD_STATUS_NEW]);

        if ($notification instanceof Notification) {
            $notification->delete();
        }

        $crontabTask = CrontabTask::findOne(['name' => 'orders_in_lead_status_new']);

        if ($crontabTask instanceof CrontabTask) {
            $crontabTask->delete();
        }
    }
}
