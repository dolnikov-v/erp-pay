<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m160801_135743_crontab_task
 */
class m160801_135743_crontab_task extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = new CrontabTask();
        $crontabTask->name = 'call_center_get_new_orders';
        $crontabTask->description = 'Получение новых заказов из колл-центра.';
        $crontabTask->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = CrontabTask::find()
            ->byName('call_center_get_new_orders')
            ->one();

        if ($crontabTask) {
            $crontabTask->delete();
        }
    }
}
