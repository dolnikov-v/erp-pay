<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170713_111120_alter_user_in_order_check_history
 */
class m170713_111120_alter_user_in_order_check_history extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('order_check_history', 'user_id', $this->integer()->defaultValue(null));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('order_check_history', 'user_id', $this->integer()->notNull());
    }
}
