<?php
use app\components\CustomMigration as Migration;
use app\modules\report\models\ReportAdcombo;
use app\models\Notification;

/**
 * Class m161019_082058_add_cc_columns_report_adcombo
 */
class m161019_082058_add_cc_columns_report_adcombo extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(ReportAdcombo::tableName(), 'cc_count', $this->integer(11)->notNull()->defaultValue(0));
        $this->addColumn(ReportAdcombo::tableName(), 'adcombo_total_count', $this->integer(11)->notNull()->defaultValue(0));
        $this->addColumn(ReportAdcombo::tableName(), 'our_total_count', $this->integer(11)->notNull()->defaultValue(0));
        $notification = Notification::find()->where(['trigger' => 'report.adcombo.daily-info'])->one();
        if(!$notification) {
            $notification = new Notification();
            $notification->trigger = "report.adcombo.daily-info";
            $notification->type = Notification::TYPE_INFO;
            $notification->group = Notification::GROUP_REPORT;
        }

        $notification->description = "AdCombo '{dateFrom} - {dateTo}': Всего {totalCount}|{ourTotalCount}; КЦ {ccCount}; Hold {holdCount}|{ourHoldCount}; Confirmed {confirmedCount}|{ourConfirmedCount}; Trash {trashCount}|{ourTrashCount}; Cancelled {cancelledCount}|{ourCancelledCount};";
        $notification->save();

        $notification = Notification::find()->where(['trigger' => 'report.adcombo.daily-danger'])->one();
        if(!$notification) {
            $notification = new Notification();
            $notification->trigger = "report.adcombo.daily-danger";
            $notification->type = Notification::TYPE_DANGER;
            $notification->group = Notification::GROUP_REPORT;
        }
        $notification->description = "AdCombo '{dateFrom} - {dateTo}' имеются различия: Не совпадает статус {statusCount}; Не найдено заказов в AdCombo {ourCount}; Не найдено заказов у нас {adcomboCount}; Не хватает записей КЦ - {ccCount};";
        $notification->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(ReportAdcombo::tableName(), 'cc_count');
        $notification = Notification::find()->where(['trigger' => 'report.adcombo.daily-info'])->one();
        if(!$notification) {
            $notification = new Notification();
            $notification->trigger = "report.adcombo.daily-info";
            $notification->type = Notification::TYPE_INFO;
            $notification->group = Notification::GROUP_REPORT;
        }
        $notification->description = "AdCombo '{dateFrom} - {dateTo}': Всего {totalCount} | {ourTotalCount}; Hold {holdCount} | {ourHoldCount}; Confirmed {confirmedCount} | {ourConfirmedCount}; Trash {trashCount} | {ourTrashCount}; Cancelled {cancelledCount} | {ourCancelledCount};";
        $notification->save();

        $notification = Notification::find()->where(['trigger' => 'report.adcombo.daily-danger'])->one();
        if(!$notification) {
            $notification = new Notification();
            $notification->trigger = "report.adcombo.daily-danger";
            $notification->type = Notification::TYPE_DANGER;
            $notification->group = Notification::GROUP_REPORT;
        }
        $notification->description = "AdCombo '{dateFrom} - {dateTo}' имеются различия: Не совпадает статус {statusCount}; Не найдено заказов в AdCombo {ourCount}; Не найдено заказов у нас {adcomboCount};";
        $notification->save();
    }
}
