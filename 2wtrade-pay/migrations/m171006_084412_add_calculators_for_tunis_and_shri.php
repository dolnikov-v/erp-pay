<?php
use app\components\CustomMigration as Migration;

/**
 * Class m171006_084412_add_calculators_for_tunis_and_shri
 */
class m171006_084412_add_calculators_for_tunis_and_shri extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('delivery_charges_calculator', [
            'name' => 'DeliveryChargeByZip',
            'class_path' => 'app\modules\delivery\components\charges\DeliveryChargeByZip',
            'active' => 1,
            'created_at' => time(),
        ]);
        $this->insert('delivery_charges_calculator', [
            'name' => 'JetExpress',
            'class_path' => 'app\modules\delivery\components\charges\jetexpress\JetExpress',
            'active' => 1,
            'created_at' => time(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete('delivery_charges_calculator', ['name' => 'DeliveryChargeByZip']);
        $this->delete('delivery_charges_calculator', ['name' => 'JetExpress']);
    }
}
