<?php
use app\components\CustomMigration as Migration;

/**
 * Class m190124_052915_long_url_in_user_action_log
 */
class m190124_052915_long_url_in_user_action_log extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('user_action_log', 'url', $this->string(2083)->notNull());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('user_action_log', 'url', $this->string(1024)->notNull());
    }
}
