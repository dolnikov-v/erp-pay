<?php
use app\components\CustomMigration as Migration;

/**
 * Class m180220_094021_charging_calculator_to_contract
 */
class m180220_094021_charging_calculator_to_contract extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('delivery_contract', 'charges_calculator_id', $this->integer()->after('id'));
        $this->addForeignKey($this->getFkName('delivery_contract', 'charges_calculator_id'), 'delivery_contract', 'charges_calculator_id', 'delivery_charges_calculator', 'id', self::SET_NULL, self::CASCADE);
        $query = new \yii\db\Query();
        $query->from('delivery');
        $query->select(['id', 'charges_calculator_id']);
        $query->where(['is not', 'delivery.charges_calculator_id', null]);
        $data = $query->all();

        foreach ($data as $deliveryInfo) {
            $this->update('delivery_contract', ['charges_calculator_id' => $deliveryInfo['charges_calculator_id']], ['delivery_id' => $deliveryInfo['id']]);
        }

        $this->dropForeignKey($this->getFkName('delivery', 'charges_calculator_id'), 'delivery');
        $this->dropColumn('delivery', 'charges_calculator_id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->addColumn('delivery', 'charges_calculator_id', $this->integer()->after('api_class_id'));
        $this->addForeignKey($this->getFkName('delivery', 'charges_calculator_id'), 'delivery', 'charges_calculator_id', 'delivery_charges_calculator', 'id', self::SET_NULL, self::CASCADE);
        $query = new \yii\db\Query();
        $query->from('delivery_contract');
        $query->select(['delivery_id', 'charges_calculator_id']);
        $query->where(['is not', 'delivery_contract.charges_calculator_id', null]);
        $query->groupBy(['delivery_contract.delivery_id']);
        $data = $query->all();
        foreach ($data as $deliveryInfo) {
            $this->update('delivery', ['charges_calculator_id' => $deliveryInfo['charges_calculator_id']], ['id' => $deliveryInfo['delivery_id']]);
        }

        $this->dropForeignKey($this->getFkName('delivery_contract', 'charges_calculator_id'), 'delivery_contract');
        $this->dropColumn('delivery_contract', 'charges_calculator_id');
    }
}
