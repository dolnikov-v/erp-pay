<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160928_080651_add_column_orders_hash
 */
class m160928_080651_add_column_orders_hash extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('order_logistic_list', 'orders_hash', $this->string(32)->defaultValue(null)->after('ids'));
        $this->createIndex(null, 'order_logistic_list', 'orders_hash');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropIndex($this->getIdxName('order_logistic_list', 'orders_hash'), 'order_logistic_list');
        $this->dropColumn('order_logistic_list', 'orders_hash');
    }
}
