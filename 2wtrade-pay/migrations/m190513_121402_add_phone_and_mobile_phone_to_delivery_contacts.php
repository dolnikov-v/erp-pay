<?php
use app\components\CustomMigration as Migration;

/**
 * Class m190513_121402_add_phone_and_mobile_phone_to_delivery_contacts
 */
class m190513_121402_add_phone_and_mobile_phone_to_delivery_contacts extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('delivery_contacts', 'phone', $this->string()->after('email'));
        $this->addColumn('delivery_contacts', 'mobile_phone', $this->string()->after('phone'));

        $this->insert('i18n_source_message', ['category' => 'common', 'message' => 'Мобильный телефон']);
        $id = Yii::$app->db->getLastInsertID();
        $this->insert('i18n_message', ['id' => $id, 'language' => 'en-US', 'translation' => 'Mobile phone']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('delivery_contacts', 'phone');
        $this->dropColumn('delivery_contacts', 'mobile_phone');

        $this->delete('i18n_source_message', ['category' => 'common', 'message' => 'Мобильный телефон']);
    }
}
