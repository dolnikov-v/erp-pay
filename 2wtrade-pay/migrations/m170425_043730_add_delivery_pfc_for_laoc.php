<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryApiClass;
use app\models\Country;

/**
 * Class m170425_043730_add_delivery_pfc_for_laoc
 */
class m170425_043730_add_delivery_pfc_for_laoc extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $apiClass = DeliveryApiClass::find()
            ->where(['name' => 'PfcApi'])
            ->one();

        $country = Country::find()
            ->where([
                'name' => 'Лаос',
                'char_code' => 'LA'
            ])
            ->one();

        if ($apiClass && $country) {

            $result = Delivery::find()
                ->where([
                    'name' => 'PFC',
                    'char_code' => 'PFC',
                    'country_id' => $country->id,
                    'api_class_id' => $apiClass->id,
                ])
                ->exists();

            if (!$result) {
                $delivery = new Delivery;
                $delivery->country_id = $country->id;
                $delivery->name = 'PFC';
                $delivery->char_code = 'PFC';
                $delivery->api_class_id = $apiClass->id;
                $delivery->active = '1';
                $delivery->auto_sending = '0';
                $result = $delivery->save();
            }
            return $result;
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $apiClass = DeliveryApiClass::find()
            ->where(['name' => 'PfcApi'])
            ->one();

        $country = Country::find()
            ->where([
                'name' => 'Лаос',
                'char_code' => 'LA'
            ])
            ->one();

        if ($apiClass && $country) {

            $delivery = Delivery::find()
                ->where([
                    'name' => 'PFC',
                    'char_code' => 'PFC',
                    'country_id' => $country->id,
                    'api_class_id' => $apiClass->id,
                ])
                ->one();

            if ($delivery) {
                $delivery->delete();
            }
        }
    }
}
