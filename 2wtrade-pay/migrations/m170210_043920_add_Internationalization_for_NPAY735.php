<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170210_043920_add_Internationalization_for_NPAY735
 */
class m170210_043920_add_Internationalization_for_NPAY735 extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $list = [
            'Отсутствуют данные для формирования накладных.',
            'Неизвестный тип InvoiceMerger',
            'Невозможно открыть',
            'Не удалось сохранить архив.',
            'Не удалось создать архив.',
            'Невозможно скопировать файл накладной.',
            'В данном листе накладных не найдено.',
            'Все накладные в одном документе'
        ];

        foreach ($list as $phrase) {
            $this->insert('i18n_source_message', ['category' => 'common', 'message' => $phrase]);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

    }
}
