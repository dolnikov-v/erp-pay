<?php
use app\components\CustomMigration as Migration;
use app\models\Notification;

/**
 * Class m170621_114421_fix_trigger_lead_not_added
 */
class m170621_114421_fix_trigger_lead_not_added extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->update(Notification::tableName(),
            ['description'=>'Не удалось добавить нового лида от {source}. Ошибка: {exception}'],
            ['trigger' => 'lead.not.added']
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->update(Notification::tableName(),
            ['description'=>'Не удалось добавить нового лида от Adcombo. Ошибка: {exeption}'],
            ['trigger' => 'lead.not.added']
        );
    }
}
