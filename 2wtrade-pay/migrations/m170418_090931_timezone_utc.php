<?php
use app\components\CustomMigration as Migration;
use app\models\Timezone;

/**
 * Class m170418_090931_timezone_utc
 */
class m170418_090931_timezone_utc extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tz = new Timezone();
        $tz->time_offset = 0;
        $tz->name = '(GMT 00:00) UTC';
        $tz->timezone_id = 'UTC';
        $tz->save(false);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        Timezone::deleteAll(['timezone_id' => 'UTC']);

    }
}
