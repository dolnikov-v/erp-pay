<?php
use app\components\CustomMigration as Migration;
use app\modules\salary\models\MonthlyStatement;
use app\modules\salary\models\MonthlyStatementData;
use app\modules\administration\models\CrontabTask;

/**
 * Class m180330_120616_send_salary_sheet
 */
class m180330_120616_send_salary_sheet extends Migration
{
    private static $translations = [
        "Средний чек апрувленный" => "Average check approve",
        "Средний чек выкупной" => "Average check buyout",
        "Оператор продаж" => "Sales operator",
        "Колл-центр Индонезия A" => "Call Center Indonesia A",
        "Колл-центр Индонезия B" => "Call Center Indonesia B",
        "Индонезия 1" => "Indonesia 1",
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        foreach (self::$translations as $ruText => $enText) {
            $this->insert('i18n_source_message', ['category' => 'common', 'message' => $ruText]);
            $id = Yii::$app->db->getLastInsertID();
            $this->insert('i18n_message', ['id' => $id, 'language' => 'en-US', 'translation' => $enText]);
        }

        $this->addColumn(MonthlyStatement::tableName(), 'sent_at', $this->integer());
        $this->addColumn(MonthlyStatementData::tableName(), 'sent_at', $this->integer());
        $this->addColumn(MonthlyStatementData::tableName(), 'salary_sheet_file', $this->string());

        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_SEND_SALARY_SHEET]);
        if (!$crontabTask) {
            $crontabTask = new CrontabTask();
            $crontabTask->name = CrontabTask::TASK_SEND_SALARY_SHEET;
            $crontabTask->description = "Отправить расчетки сотрудникам ЗП проекта";
            $crontabTask->save();
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach (self::$translations as $ruText => $enText) {
            $this->delete('i18n_source_message', ['category' => 'common', 'message' => $ruText]);
        }

        $this->dropColumn(MonthlyStatement::tableName(), 'sent_at');
        $this->dropColumn(MonthlyStatementData::tableName(), 'sent_at');
        $this->dropColumn(MonthlyStatementData::tableName(), 'salary_sheet_file');

        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_SEND_SALARY_SHEET]);
        if ($crontabTask) {
            $crontabTask->delete();
        }
    }
}
