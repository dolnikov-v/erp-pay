<?php
use app\components\PermissionMigration;

/**
 * Class m171213_070359_permission_export_prohibited_columns
 */
class m171213_070359_permission_export_prohibited_columns extends PermissionMigration
{
    protected $permissions = [
        'order.index.index.exportprohibitedcolumns',
    ];

    protected $roles = [
        'deliveryreport.clerk' => [
            'order.index.index.exportprohibitedcolumns'
        ]
    ];
}
