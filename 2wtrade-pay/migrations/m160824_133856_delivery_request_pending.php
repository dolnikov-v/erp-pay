<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160824_133856_delivery_request_pending
 */
class m160824_133856_delivery_request_pending extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('order', 'pending_delivery_id', $this->integer()->defaultValue(null)->after('adcombo_confirmed'));
        $this->addForeignKey(null, 'order', 'pending_delivery_id', 'delivery', 'id', self::SET_NULL, self::SET_NULL);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey($this->getFkName('order', 'pending_delivery_id'), 'order');
        $this->dropColumn('order', 'pending_delivery_id');
    }
}
