<?php

use app\components\CustomMigration as Migration;
use app\models\Notification;

/**
 * Handles adding cc_not_available to table `notification`.
 */
class m170906_014525_add_cc_not_available_to_notification extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->delete(Notification::tableName(), ['trigger' => 'zabbix.test']);
        $model = new Notification([
            'description' => 'Call-center name: {name} not available. Error code: {code}',
            'trigger' => 'callcenter.not.available',
            'type' => Notification::TYPE_DANGER,
            'group' => Notification::GROUP_SYSTEM,
            'active' => Notification::ACTIVE,
        ]);
        $model->save(false);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete(Notification::tableName(), ['trigger' => 'callcenter.not.available']);
        $model = new Notification([
            'description' => 'Message: {message}. Amount: {amount} in status {status_id} more than {time_norm}' . PHP_EOL,
            'trigger' => 'zabbix.test',
            'type' => Notification::TYPE_DANGER,
            'group' => Notification::GROUP_SYSTEM,
            'active' => Notification::ACTIVE,
        ]);
        $model->save(false);
    }
}
