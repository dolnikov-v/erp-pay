<?php
use app\components\CustomMigration as Migration;
use app\modules\bar\models\Bar;

/**
 * Class m170417_082451_alter_column_bar_codes
 */
class m170417_082451_alter_column_bar_codes extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn(Bar::tableName(), 'status', $this->enum(["created", "initiated", "packed", "bought", "returned", "defect"])->defaultValue('created'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn(Bar::tableName(), 'status', $this->integer(2));
    }
}
