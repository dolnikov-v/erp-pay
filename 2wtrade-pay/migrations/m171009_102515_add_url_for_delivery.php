<?php
use app\components\CustomMigration as Migration;

/**
 * Class m171009_102515_add_url_for_delivery
 */
class m171009_102515_add_url_for_delivery extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('delivery', 'url', $this->string(255)->after('char_code'));
        $this->createIndex($this->getIdxName('delivery', 'active'), 'delivery', 'active');
        $this->createIndex($this->getIdxName('delivery', 'auto_sending'), 'delivery', 'auto_sending');
        $this->createIndex($this->getIdxName('delivery', 'can_tracking'), 'delivery', 'can_tracking');
        $this->createIndex($this->getIdxName('delivery', 'our_api'), 'delivery', 'our_api');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('delivery', 'url');
        $this->dropIndex($this->getIdxName('delivery', 'active'), 'delivery');
        $this->dropIndex($this->getIdxName('delivery', 'auto_sending'), 'delivery');
        $this->dropIndex($this->getIdxName('delivery', 'can_tracking'), 'delivery');
        $this->dropIndex($this->getIdxName('delivery', 'our_api'), 'delivery');
    }
}
