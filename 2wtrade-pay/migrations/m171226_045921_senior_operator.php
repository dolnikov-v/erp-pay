<?php
use app\components\CustomMigration as Migration;
use app\modules\salary\models\Designation;

/**
 * Class m171226_045921_senior_operator
 */
class m171226_045921_senior_operator extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Designation::tableName(), 'senior_operator', $this->integer(1)->defaultValue(0));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Designation::tableName(), 'senior_operator');
    }
}
