<?php
use app\components\CustomMigration as Migration;
use app\models\AuthItem;
use yii\db\Query;

/**
 * Class m170919_030748_add_moderator_cc_role
 */
class m170919_030748_add_moderator_cc_role extends Migration
{
    public $moderatorCcPermissions = [
        'home.index.index',
        'order.index.index',
        'order.index.view',
        'order.index.edit',
        'order.index.edit.customer',
        'order.index.edit.products',
    ];

    public $newPermissions = [
        'order.index.edit.main',
        'order.index.edit.customer',
        'order.index.edit.products',
        'order.index.edit.cc',
        'order.index.edit.delivery',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $roles = (new Query())->select(['parent'])->from('auth_item_child')->where(['child' => 'order.index.edit'])->all();
        foreach ($this->newPermissions as $newPermission) {
            $moderatorRole = new AuthItem();
            $moderatorRole->name = $newPermission;
            $moderatorRole->type = 2;
            $moderatorRole->description = $newPermission;
            $moderatorRole->save();

            foreach ($roles as $role) {
                $this->insert('auth_item_child', ['parent' => $role['parent'], 'child' => $newPermission]);
            }
        }

        $moderatorRole = new AuthItem();
        $moderatorRole->name = 'moderator_cc';
        $moderatorRole->type = 1;
        $moderatorRole->description = 'Модератор Колл Центра';
        $moderatorRole->save();

        foreach ($this->moderatorCcPermissions as $moderatorCcPermission) {
            $this->insert('auth_item_child', ['parent' => 'moderator_cc', 'child' => $moderatorCcPermission]);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete('auth_item', ['name' => 'moderator_cc']);
        foreach ($this->newPermissions as $permission) {
            $this->delete('auth_item', ['name' => $permission]);
        }
    }
}
