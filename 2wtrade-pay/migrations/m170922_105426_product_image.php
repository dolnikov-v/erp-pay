<?php
use app\components\CustomMigration as Migration;
use app\models\Product;

/**
 * Class m170922_105426_product_image
 */
class m170922_105426_product_image extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn(Product::tableName(), 'image', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn(Product::tableName(), 'image', 'MEDIUMTEXT after description');
    }
}
