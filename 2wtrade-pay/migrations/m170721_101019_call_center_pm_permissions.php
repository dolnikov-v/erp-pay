<?php
use app\components\CustomMigration as Migration;
use yii\db\Query;

/**
 * Class m170721_101019_call_center_pm_permissions
 */
class m170721_101019_call_center_pm_permissions extends Migration
{
    const ROLES = [
        'project.manager'
    ];

    const RULES = [
        'callcenter.workingshift.delete',
        'callcenter.workingshift.edit',
        'callcenter.office.index',
        'callcenter.office.edit',
        'callcenter.office.activate',
        'callcenter.office.deactivate',
        'callcenter.person.activate',
        'callcenter.person.activatepersons',
        'callcenter.person.changeteamleader',
        'callcenter.person.deactivate',
        'callcenter.person.deactivatepersons',
        'callcenter.person.delete',
        'callcenter.person.deletephoto',
        'callcenter.person.edit',
        'callcenter.person.getparents',
        'callcenter.person.getpersons',
        'callcenter.person.index',
        'callcenter.person.link',
        'callcenter.person.setrole',
        'callcenter.person.setwork',
        'callcenter.person.unlink',
        'callcenter.personimport.import',
        'callcenter.personimport.index',
        'callcenter.personimport.process',
        'callcenter.personimport.recordedit',
        'callcenter.personimport.recordexclude',
        'callcenter.personimport.recordgroupapprovedhead',
        'callcenter.personimport.recordgroupexclude',
        'callcenter.personimport.recordgroupinclude',
        'callcenter.personimport.recordinclude',
        'callcenter.user.activate',
        'callcenter.user.activateusers',
        'callcenter.user.changeteamleader',
        'callcenter.user.deactivate',
        'callcenter.user.deactivateusers',
        'callcenter.user.deletephoto',
        'callcenter.user.edit',
        'callcenter.user.getparents',
        'callcenter.user.getusers',
        'callcenter.user.index',
        'callcenter.user.setrole',
        'callcenter.user.view',
    ];


    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $query = new Query();

        foreach (self::RULES as $rule) {

            $is_rule = $query->select('*')->from($this->authManager->itemTable)
                ->where(['name' => $rule, 'type' => 2])
                ->one();

            if (!$is_rule) {
                $this->insert($this->authManager->itemTable, [
                    'name' => $rule,
                    'description' => $rule,
                    'type' => 2,
                    'created_at' => time(),
                    'updated_at' => time()]);
            }

            foreach (self::ROLES as $role) {
                $is_can = $query->select('*')->from($this->authManager->itemChildTable)->where([
                    'parent' => $role,
                    'child' => $rule
                ])->one();

                if (!$is_can) {
                    $this->insert($this->authManager->itemChildTable, [
                        'parent' => $role,
                        'child' => $rule
                    ]);
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach (self::RULES as $rule) {
            foreach (self::ROLES as $role) {
                $this->delete($this->authManager->itemChildTable, [
                    'parent' => $role,
                    'child' => $rule
                ]);
            }
        }
    }
}
