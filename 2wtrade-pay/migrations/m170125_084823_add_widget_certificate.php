<?php
use app\components\CustomMigration as Migration;
use app\modules\widget\models\WidgetType;
use app\modules\widget\models\WidgetRole;

/**
 * Class m170125_084823_add_widget_certificate
 */
class m170125_084823_add_widget_certificate extends Migration
{
    /**
     * cоздание записи о виджете, добавление роли пользователя к виджету
     */
    public function safeUp()
    {
        $this->insert(WidgetType::tableName(), [
            'code' => 'products_certificates',
            'name' => 'Сертификаты товаров',
            'status' => WidgetType::STATUS_ACTIVE
        ]);

        $type = WidgetType::find()
            ->where(['code' => 'products_certificates'])
            ->one();

        $this->insert(WidgetRole::tableName(), [
            'role' => 'business_analyst',
            'type_id' => $type->id
        ]);
    }

    
    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $type = WidgetType::find()
            ->where(['code' => 'products_certificates'])
            ->one();

        $this->delete(WidgetRole::tableName(), [
            'role' => 'business_analyst',
            'type_id' => $type->id,
        ]);

        $this->delete(WidgetType::tableName(), ['code' => 'products_certificates']);
    }
}
