<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170607_050001_create_packager_tables
 */
class m170607_050001_create_packager_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('packager', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'email' => $this->string(255),
            'country_id' => $this->integer()->notNull(),
            'active' => $this->smallInteger()->defaultValue(0),
            'auto_sending' => $this->smallInteger()->defaultValue(0),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->createTable('order_packaging', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->notNull(),
            'packager_id' => $this->integer()->notNull(),
            'delivery_id' => $this->integer()->notNull(),
            'status' => $this->smallInteger(),
            'method' => $this->smallInteger(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('packager');
        $this->dropTable('order_packaging');
    }
}
