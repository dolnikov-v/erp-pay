<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170622_101321_nondelivery_products
 */
class m170622_101321_nondelivery_products extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('delivery_not_products', [
            'id' => $this->primaryKey(),
            'delivery_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $this->tableOptions);

        $this->addForeignKey(null, 'delivery_not_products', 'delivery_id', 'delivery', 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, 'delivery_not_products', 'product_id', 'product', 'id', self::CASCADE, self::RESTRICT);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('delivery_not_products');
    }
}
