<?php
use app\components\CustomMigration as Migration;
use app\models\Country;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryApiClass;

/**
 * Class m170524_035200_add_tcc_delivery_co
 *
 * @property int $apiClassId
 * @property int $countryId
 */
class m170524_035200_add_tcc_delivery_co extends Migration
{
    /**
     * @return integer $countryId
     */
    private function getCountryId()
    {
        $countryId = Country::find()
            ->select('id')
            ->where(['slug' => 'co'])
            ->scalar();
        return $countryId;
    }

    /**
     * @return integer $apiClassId
     */
    private function getApiClassId()
    {
        $apiClassId = DeliveryApiClass::find()
            ->select('id')
            ->where(['name' => 'TCC'])
            ->scalar();

        return $apiClassId;
    }
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $delivery = new Delivery();
        $delivery->country_id = $this->getCountryId();
        $delivery->name = 'TCC';
        $delivery->char_code = 'TCC';
        $delivery->auto_sending = 0;
        $delivery->api_class_id = $this->getApiClassId();
        $delivery->brokerage_active = 0;
        $delivery->active = 0;
        $delivery->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        Delivery::find()
            ->where([
                'country_id' => $this->getCountryId(),
                'name' => 'TCC',
            ])
            ->one()
            ->delete();
    }
}
