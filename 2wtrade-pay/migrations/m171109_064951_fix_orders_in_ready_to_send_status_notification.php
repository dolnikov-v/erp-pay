<?php
use app\components\CustomMigration as Migration;
use app\models\Notification;
use app\modules\administration\models\CrontabTask;

/**
 * Class m171109_064951_fix_orders_in_ready_to_send_status_notification
 */
class m171109_064951_fix_orders_in_ready_to_send_status_notification extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $notification = Notification::findOne(['trigger' => 'report.orders.in.ready.to.send.status']);
        if ($notification instanceof Notification) {
            $notification->delete();
        }
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $notification = new Notification([
            'description' => 'Заказов готовых к отправке по странам: {text}',
            'trigger' => 'report.orders.in.ready.to.send.status',
            'type' => Notification::TYPE_INFO,
            'group' => Notification::GROUP_REPORT,
            'active' => Notification::ACTIVE,
        ]);
        $notification->save();
    }
}
