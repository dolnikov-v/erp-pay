<?php

use app\components\CustomMigration as Migration;

/**
 * Handles adding costs_columns to table `delivery_report`.
 */
class m171201_140720_add_costs_columns_to_delivery_report extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('delivery_report_record', 'price_fulfilment', $this->string(50)->after('price_storage'));
        $this->addColumn('delivery_report_record', 'price_vat', $this->string(50)->after('price_cod_service'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('delivery_report_record', 'price_fulfilment');
        $this->dropColumn('delivery_report_record', 'price_vat');
    }
}
