<?php
use app\components\CustomMigration as Migration;

/**
 * Class m171228_063014_order_finance_prediction_rename
 */
class m171228_063014_order_finance_prediction_rename extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->renameColumn('order_finance_prediction', 'currency_rate_id', 'currency_id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->renameColumn('order_finance_prediction', 'currency_id', 'currency_rate_id');
    }
}
