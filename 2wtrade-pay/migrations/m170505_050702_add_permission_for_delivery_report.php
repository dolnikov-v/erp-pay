<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170505_050702_add_permission_for_delivery_report
 */
class m170505_050702_add_permission_for_delivery_report extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}', array(
            'name' => 'deliveryreport.report.createduplicateorder',
            'type' => '2',
            'description' => 'deliveryreport.report.createduplicateorder',
            'created_at' => time(),
            'updated_at' => time()
        ));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete('{{%auth_item}}', ['name' => 'deliveryreport.report.createduplicateorder']);
    }
}
