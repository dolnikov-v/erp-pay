<?php
use app\components\CustomMigration as Migration;
use yii\db\Query;

/**
 * Class m170726_112519_call_center_salary_project_clerk
 */
class m170726_112519_call_center_salary_project_clerk extends Migration
{
    const ROLE = 'salaryproject.clerk';

    const RULES = [
        'callcenter.workingshift.delete',
        'callcenter.workingshift.edit',
        'callcenter.office.index',
        'callcenter.office.edit',
        'callcenter.office.activate',
        'callcenter.office.deactivate',
        'callcenter.person.activate',
        'callcenter.person.activatepersons',
        'callcenter.person.changeteamleader',
        'callcenter.person.deactivate',
        'callcenter.person.deactivatepersons',
        'callcenter.person.delete',
        'callcenter.person.deletephoto',
        'callcenter.person.edit',
        'callcenter.person.getparents',
        'callcenter.person.getpersons',
        'callcenter.person.index',
        'callcenter.person.link',
        'callcenter.person.setrole',
        'callcenter.person.setwork',
        'callcenter.person.unlink',
        'callcenter.personimport.import',
        'callcenter.personimport.index',
        'callcenter.personimport.process',
        'callcenter.personimport.recordedit',
        'callcenter.personimport.recordexclude',
        'callcenter.personimport.recordgroupapprovedhead',
        'callcenter.personimport.recordgroupexclude',
        'callcenter.personimport.recordgroupinclude',
        'callcenter.personimport.recordinclude',
        'callcenter.user.activate',
        'callcenter.user.activateusers',
        'callcenter.user.changeteamleader',
        'callcenter.user.deactivate',
        'callcenter.user.deactivateusers',
        'callcenter.user.deletephoto',
        'callcenter.user.edit',
        'callcenter.user.getparents',
        'callcenter.user.getusers',
        'callcenter.user.index',
        'callcenter.user.setrole',
        'callcenter.user.view',
        'callcenter.holiday.edit',
        'callcenter.holiday.delete',
        'callcenter.holiday.index'
    ];


    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $query = new Query();

        $is_role = $query->select('*')->from($this->authManager->itemTable)
            ->where(['name' => self::ROLE, 'type' => 1])
            ->one();

        if (!$is_role) {
            $this->insert($this->authManager->itemTable, [
                'name' => self::ROLE,
                'type' => 1,
                'description' => self::ROLE,
                'created_at' => time(),
                'updated_at' => time()]);
        }

        foreach (self::RULES as $rule) {

            $is_rule = $query->select('*')->from($this->authManager->itemTable)
                ->where(['name' => $rule, 'type' => 2])
                ->one();

            if (!$is_rule) {
                $this->insert($this->authManager->itemTable, [
                    'name' => $rule,
                    'description' => $rule,
                    'type' => 2,
                    'created_at' => time(),
                    'updated_at' => time()]);
            }


            $is_can = $query->select('*')->from($this->authManager->itemChildTable)->where([
                'parent' => self::ROLE,
                'child' => $rule
            ])->one();

            if (!$is_can) {
                $this->insert($this->authManager->itemChildTable, [
                    'parent' => self::ROLE,
                    'child' => $rule
                ]);
            }

        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach (self::RULES as $rule) {
            $this->delete($this->authManager->itemChildTable, [
                'parent' => self::ROLE,
                'child' => $rule
            ]);
        }

        $this->delete($this->authManager->itemTable, [
            'name' => self::ROLE,
            'type' => 1
        ]);
    }
}
