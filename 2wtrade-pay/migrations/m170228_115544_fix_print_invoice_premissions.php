<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170228_115544_fix_print_invoice_premissions
 */
class m170228_115544_fix_print_invoice_premissions extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        $permission = $auth->getPermission('order.index.print-invoice');
        $auth->remove($permission);
        $permission = $auth->createPermission('order.index.printinvoice');
        $permission->description = 'Print one invoice from list';
        $permission->type = 2;
        $auth->add($permission);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;
        $permission = $auth->getPermission('order.index.printinvoice');
        $auth->remove($permission);
    }
}
