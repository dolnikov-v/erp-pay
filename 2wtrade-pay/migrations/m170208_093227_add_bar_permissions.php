<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170208_093227_add_bar_permissions
 */
class m170208_093227_add_bar_permissions extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $auth = $this->authManager;
        $role = $auth->getRole('logist');

        $packOrder = $auth->createPermission('bar.pack.index');
        $packOrder->description = 'Pack order';
        $auth->add($packOrder);
        $returnOrder = $auth->createPermission('bar.return.index');
        $returnOrder->description = 'Return order';
        $auth->add($returnOrder);
        $auth->addChild($role,$packOrder);
        $auth->addChild($role,$returnOrder);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $auth = $this->authManager;

        $packOrder = $auth->getPermission('bar.pack.index');
        $returnOrder = $auth->getPermission('bar.return.index');

        $auth->remove($packOrder);
        $auth->remove($returnOrder);
    }
}
