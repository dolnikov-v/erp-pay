<?php
use app\components\CustomMigration as Migration;
use app\modules\order\models\MarketingList;
use app\modules\order\models\MarketingListOrder;
use app\modules\order\models\Order;
use app\modules\callcenter\models\CallCenterCheckRequest;
use app\modules\order\models\OrderStatus;
use app\models\Country;
use app\models\User;

/**
 * Class m170207_110010_tables_type_collation_fix
 */
class m170207_110010_tables_type_collation_fix extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->execute("ALTER TABLE `storage_part_moves` COLLATE='utf8_unicode_ci', ENGINE=InnoDB;");
        $this->execute("ALTER TABLE `marketing_list_order` COLLATE='utf8_unicode_ci', ENGINE=InnoDB;");
        $this->execute("ALTER TABLE `marketing_list` COLLATE='utf8_unicode_ci', ENGINE=InnoDB;");
        $this->execute("ALTER TABLE `order_check_history` COLLATE='utf8_unicode_ci', ENGINE=InnoDB;");
        $this->execute("DROP TABLE IF EXISTS `tmp`");

        $this->addForeignKey(null, MarketingList::tableName(), 'country_id', Country::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, MarketingList::tableName(), 'user_id', User::tableName(), 'id', self::CASCADE, self::RESTRICT);

        $this->addForeignKey(null, MarketingListOrder::tableName(), 'marketing_list_id', 'marketing_list', 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, MarketingListOrder::tableName(), 'old_order_id', Order::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, MarketingListOrder::tableName(), 'new_order_id', Order::tableName(), 'id', self::SET_NULL, self::RESTRICT);

        $this->addForeignKey(null, 'storage_part_moves', 'product_id', 'product', 'id', Migration::CASCADE);
        $this->addForeignKey(null, 'storage_part_moves', 'storage_part_from', 'storage_part', 'id', Migration::CASCADE);
        $this->addForeignKey(null, 'storage_part_moves', 'storage_part_to', 'storage_part', 'id', Migration::CASCADE);
        $this->addForeignKey(null, 'storage_part_moves', 'user_from', 'user', 'id', Migration::CASCADE);
        $this->addForeignKey(null, 'storage_part_moves', 'user_to', 'user', 'id', Migration::CASCADE);
        $this->addForeignKey(null, 'storage_part_moves', 'storage_to', 'storage', 'id', Migration::CASCADE);

        $this->addForeignKey(null, CallCenterCheckRequest::tableName(), 'order_id', Order::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, CallCenterCheckRequest::tableName(), 'user_id', User::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, CallCenterCheckRequest::tableName(), 'status_id', OrderStatus::tableName(), 'id', self::CASCADE, self::RESTRICT);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

    }
}
