<?php
use app\components\CustomMigration as Migration;
use app\modules\callcenter\models\CallCenterWorkTime;
use app\modules\administration\models\CrontabTask;

/**
 * Class m170705_102028_call_center_number_of_calls
 */
class m170705_102028_call_center_number_of_calls extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(CallCenterWorkTime::tableName(), 'number_of_calls', $this->integer()->after('time'));

        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_CC_GET_OPERS_NUMBER_OF_CALLS]);
        if (!$crontabTask) {
            $crontabTask = new CrontabTask();
            $crontabTask->name = CrontabTask::TASK_CC_GET_OPERS_NUMBER_OF_CALLS;
            $crontabTask->description = "Получение кол-ва звонков из КЦ по пользователям";
            $crontabTask->save();
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(CallCenterWorkTime::tableName(), 'number_of_calls');

        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_CC_GET_OPERS_NUMBER_OF_CALLS]);
        if ($crontabTask instanceof CrontabTask) {
            $crontabTask->delete();
        }
    }
}
