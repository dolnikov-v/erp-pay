<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160426_115404_crontab_subanswer
 */
class m160426_115404_crontab_subanswer extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('crontab_task_log_subanswer', [
            'id' => $this->primaryKey(),
            'answer_id' => $this->integer()->notNull(),
            'data' => $this->text(),
            'answer' => $this->text(),
            'status' => "ENUM('fail', 'success') DEFAULT 'fail'",
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(null, 'crontab_task_log_subanswer', 'answer_id', 'crontab_task_log_answer', 'id', self::CASCADE, self::RESTRICT);
        $this->createIndex(null, 'crontab_task_log_subanswer', 'status');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('crontab_task_log_subanswer');
    }
}
