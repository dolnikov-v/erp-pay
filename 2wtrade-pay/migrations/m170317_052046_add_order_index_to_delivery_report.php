<?php

use app\components\CustomMigration as Migration;
use app\modules\deliveryreport\models\DeliveryReportRecord;

/**
 * Handles adding order index to table `delivery_report`.
 */
class m170317_052046_add_order_index_to_delivery_report extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createIndex('fk_delivery_report_record_order_id', DeliveryReportRecord::tableName(), 'order_id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropIndex('fk_delivery_report_record_order_id', DeliveryReportRecord::tableName());
    }
}
