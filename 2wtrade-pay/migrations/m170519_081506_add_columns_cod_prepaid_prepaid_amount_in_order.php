<?php
use app\components\CustomMigration as Migration;
use app\modules\order\models\Order;

/**
 * Class m170519_081506_add_columns_cod_prepaid_prepaid_amount_in_order
 */
class m170519_081506_add_columns_cod_prepaid_prepaid_amount_in_order extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Order::tableName(), 'payment_type', $this->string(20)->defaultValue(null) . ' AFTER `comment`');
        $this->addColumn(Order::tableName(), 'payment_amount', $this->double()->defaultValue(0) . ' AFTER `payment_type`');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Order::tableName(), 'payment_type');
        $this->dropColumn(Order::tableName(), 'payment_amount');
    }
}
