<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m170712_053935_add_task_storage_product_purchase
 */
class m170712_053935_add_task_storage_product_purchase extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_STORAGE_PRODUCT_PURCHASE]);
        if (!$crontabTask) {
            $crontabTask = new CrontabTask();
            $crontabTask->name = CrontabTask::TASK_STORAGE_PRODUCT_PURCHASE;
            $crontabTask->description = "Отправка email о min складских остатках";
            $crontabTask->save();
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_STORAGE_PRODUCT_PURCHASE]);
        if ($crontabTask instanceof CrontabTask) {
            $crontabTask->delete();
        }
    }
}
