<?php
use app\components\CustomMigration as Migration;
use app\models\CurrencyRateHistory;


/**
 * Class m180807_053446_currency_rate_history
 */
class m180807_053446_currency_rate_history extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createIndex($this->getIdxName(CurrencyRateHistory::tableName(), ['date', 'currency_id']), CurrencyRateHistory::tableName(), ['date', 'currency_id'], true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropIndex($this->getIdxName(CurrencyRateHistory::tableName(), ['date', 'currency_id']), CurrencyRateHistory::tableName());
    }
}
