<?php

use app\components\CustomMigration as Migration;

class m000000_000026_log_api extends Migration
{
    public function up()
    {
        $this->createTable('{{%log_api%}}', [
            'id' => $this->primaryKey(),
            'status' => $this->smallInteger()->defaultValue(0),
            'message' => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $this->tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%log_api%}}');
    }
}
