<?php
use app\components\CustomMigration as Migration;
use app\modules\order\models\Order;

/**
 * Class m161026_041450_add_order_column_customer_city_code
 */
class m161026_041450_add_order_column_customer_city_code extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp(){
        $this->addColumn(Order::tableName(),'customer_city_code', $this->string(16)->defaultValue(NULL)->after('customer_city'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown(){
        $this->dropColumn(Order::tableName(), 'customer_city_code');
    }
}
