<?php

use app\components\CustomMigration as Migration;
use app\models\Country;
use app\models\Source;
use app\models\SourceCountry;

/**
 * Handles the creation for table `source_country_table`.
 */
class m170627_094405_create_source_country_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(SourceCountry::tableName(), [
            'id' => $this->primaryKey(),
            'source_id' => $this->integer()->notNull(),
            'country_id' => $this->integer()->notNull(),
            'active' => $this->boolean()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $this->tableOptions);
        $this->createIndex(null, SourceCountry::tableName(), ['source_id', 'country_id'], $unique = true);
        $this->addForeignKey(null, SourceCountry::tableName(), 'source_id', Source::tableName(), 'id', Migration::CASCADE, Migration::CASCADE);
        $this->addForeignKey(null, SourceCountry::tableName(), 'country_id', Country::tableName(), 'id', Migration::CASCADE, Migration::CASCADE);

        /*$adcomboId = SourceCountry::find()->select('id')->where(['name' => 'adcombo'])->scalar();
        $jeempoId = SourceCountry::find()->select('id')->where(['name' => 'jeempo'])->scalar();

        $countries = Country::find()->all();
        foreach ($countries as $country) {
            $sourceCountry = new SourceCountry();
            $sourceCountry->country_id = $country->id;
            $sourceCountry->country_id = $country->id;
        }*/
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable(SourceCountry::tableName());
    }
}
