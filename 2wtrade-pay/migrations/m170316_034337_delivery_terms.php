<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryZipcodes;
use \yii\db\Query;

/**
 * Class m170316_034337_delivery_terms
 */
class m170316_034337_delivery_terms extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(DeliveryZipcodes::tableName(), 'max_term_days', $this->integer()->after('price')->defaultValue(2));

        $query = new Query();

        $rules = [
            'report.deliveryterms.index',
        ];

        $roles = [
            'business_analyst',
            'country.curator'
        ];

        foreach ($rules as $rule) {
            $is_report = $query->select('*')->from($this->authManager->itemTable)
                ->where([
                    'name' => $rule,
                    'type' => 2])
                ->one();

            if (!$is_report) {
                $this->insert($this->authManager->itemTable, [
                    'name' => $rule,
                    'type' => 2,
                    'description' => $rule,
                    'created_at' => time(),
                    'updated_at' => time()]);
            }

            foreach ($roles as $role) {

                $is_role = $query->select('*')->from($this->authManager->itemTable)
                    ->where(['name' => $role, 'type' => 1])
                    ->one();

                if (!$is_role) {
                    $this->insert($this->authManager->itemTable, [
                        'name' => $role,
                        'type' => 1,
                        'description' => $role,
                        'created_at' => time(),
                        'updated_at' => time()]);
                }

                $is_br = $query->select('*')->from($this->authManager->itemChildTable)->where([
                    'parent' => $role,
                    'child' => $rule
                ])->one();

                if (!$is_br) {
                    $this->insert($this->authManager->itemChildTable, [
                        'parent' => $role,
                        'child' => $rule
                    ]);
                }
            }
        }

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(DeliveryZipcodes::tableName(), 'max_term_days');

        $rules = [
            'report.deliveryterms.index',
        ];

        $roles = [
            'business_analyst',
            'country.curator'
        ];

        foreach ($rules as $rule) {

            foreach ($roles as $role) {
                $this->delete($this->authManager->itemChildTable, [
                    'parent' => $role,
                    'child' => $rule
                ]);
            }

            $this->delete($this->authManager->itemTable, [
                'name' => $rule,
                'type' => 2]);
        }

    }
}
