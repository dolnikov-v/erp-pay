<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryApiClass;
use app\modules\delivery\models\Delivery;
use app\models\Country;

/**
 * Class m170119_045116_add_zeptoApi
 */
class m170119_045116_add_zeptoApi extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        if (DeliveryApiClass::find()->where(
            ['name' => 'zeptoApi', 'class_path' => '/zepto-api/src/zeptoApi.php']
        )->exists()
        ) {
            $apiClass = DeliveryApiClass::findOne(
                ['name' => 'zeptoApi', 'class_path' => '/zepto-api/src/zeptoApi.php']
            );
        } else {
            $apiClass = new DeliveryApiClass();
        }
        $apiClass->name = 'zeptoApi';
        $apiClass->class_path = '/zepto-api/src/zeptoApi.php';
        $apiClass->active = '1';
        $apiClass->created_at = time();
        $apiClass->updated_at = time();
        $apiClass->save();

        if (Country::find()->where(
            [
                'name' => 'Малайзия 1',
                'char_code' => 'MY'
            ]
        )->exists()
        ) {
            $country = Country::find()->where(
                [
                    'name' => 'Малайзия 1',
                    'char_code' => 'MY'
                ]
            )->select('id')->one();
        } else {
            //нет страны
            return false;
        }

        if (Delivery::find()->where(
            [
                'name' => 'Zepto',
                'char_code' => 'ZEP'
            ]
        )->exists()
        ) {
            $delivery = Delivery::findOne(
                [
                    'name' => 'Zepto',
                    'char_code' => 'ZEP'
                ]
            );
        } else {
            $delivery = new Delivery();
        }
        $delivery->country_id = $country->id;
        $delivery->name = 'Zepto';
        $delivery->char_code = 'ZEP';
        $delivery->api_class_id = $apiClass->id;
        $delivery->active = '1';
        $delivery->auto_sending = '0';
        $delivery->save(false);
        return true;
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        if (Country::find()->where(
            [
                'name' => 'Малайзия 1',
                'char_code' => 'MY'
            ]
        )->exists()
        ) {
            $country = Country::find()->where(
                [
                    'name' => 'Малайзия 1',
                    'char_code' => 'MY'
                ]
            )->select('id')->one();
        } else {
            //нет страны
            return false;
        }

        DeliveryApiClass::find()
            ->where([
                'name' => 'zeptoApi',
                'class_path' => '/zepto-api/src/zeptoApi.php'
            ])
            ->one()
            ->delete();

        Delivery::findOne(
            [
                'name' => 'Zepto',
                'country_id' => $country->id
            ]
        )->delete();
        return true;
    }
}