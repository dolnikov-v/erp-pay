<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryApiClass;
use app\modules\delivery\models\Delivery;
use app\models\Country;

/**
 * Class m170417_100949_add_delivery_fongExpress_for_Hana
 */
class m170417_100949_add_delivery_fongExpress_for_Hana extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        if (DeliveryApiClass::find()->where(
            ['name' => 'FongExpressApi', 'class_path' => '/fongexpress-api/src/fongExpressApi.php']
        )->exists()
        ) {
            $apiClass = DeliveryApiClass::findOne(
                ['name' => 'FongExpressApi', 'class_path' => '/fongexpress-api/src/fongExpressApi.php']
            );
        } else {
            $apiClass = new DeliveryApiClass();
        }
        $apiClass->name = 'FongExpressApi';
        $apiClass->class_path = '/fongexpress-api/src/fongExpressApi.php';
        $apiClass->active = '1';
        $apiClass->created_at = time();
        $apiClass->updated_at = time();
        $apiClass->save();

        if (Country::find()->where(
            [
                'name' => 'Гана',
                'char_code' => 'GH'
            ]
        )->exists()
        ) {
            $country = Country::find()->where(
                [
                    'name' => 'Гана',
                    'char_code' => 'GH'
                ]
            )->select('id')->one();
        } else {
            //нет страны
            return false;
        }

        if (Delivery::find()->where(
            [
                'name' => 'FongExpress',
                'char_code' => 'FGE'
            ]
        )->exists()
        ) {
            $delivery = Delivery::findOne(
                [
                    'name' => 'FongExpress',
                    'char_code' => 'FGE'
                ]
            );
        } else {
            $delivery = new Delivery();
        }
        $delivery->country_id = $country->id;
        $delivery->name = 'FongExpress';
        $delivery->char_code = 'FGE';
        $delivery->api_class_id = $apiClass->id;
        $delivery->active = '1';
        $delivery->auto_sending = '0';
        $delivery->save(false);
        return true;
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        if (Country::find()->where(
            [
                'name' => 'Гана',
                'char_code' => 'GH'
            ]
        )->exists()
        ) {
            $country = Country::find()->where(
                [
                    'name' => 'Гана',
                    'char_code' => 'GH'
                ]
            )->select('id')->one();
        } else {
            //нет страны
            return false;
        }

        DeliveryApiClass::find()
            ->where([
                'name' => 'FongExpressApi',
                'class_path' => '/fongexpress-api/src/fongExpressApi.php'
            ])
            ->one()
            ->delete();

        Delivery::findOne(
            [
                'name' => 'FongExpress',
                'country_id' => $country->id
            ]
        )->delete();
        return true;
    }
}
