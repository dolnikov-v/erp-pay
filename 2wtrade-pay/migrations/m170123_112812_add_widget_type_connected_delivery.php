<?php
use app\components\CustomMigration as Migration;
use app\modules\widget\models\WidgetType;
use app\modules\widget\models\WidgetRole;

/**
 * Class m170123_112812_add_widget_type_connected_delivery
 */
class m170123_112812_add_widget_type_connected_delivery extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert(WidgetType::tableName(), [
            'code' => 'connected_delivery',
            'name' => 'Подключено курьерок',
            'status' => WidgetType::STATUS_ACTIVE,
        ]);

        $type = WidgetType::find()
            ->where(['code' => 'connected_delivery'])
            ->one();

        $this->insert(WidgetRole::tableName(), [
            'role' => 'business_analyst',
            'type_id' => $type->id,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $type = WidgetType::find()
            ->where(['code' => 'connected_delivery'])
            ->one();

        $this->delete(WidgetRole::tableName(), [
            'role' => 'business_analyst',
            'type_id' => $type->id,
        ]);

        $this->delete(WidgetType::tableName(), ['code' => 'connected_delivery']);
    }
}
