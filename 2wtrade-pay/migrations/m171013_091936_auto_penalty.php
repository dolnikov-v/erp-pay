<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;
use app\models\Notification;


/**
 * Class m171013_091936_auto_penalty
 */
class m171013_091936_auto_penalty extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_MAKE_AUTO_PENALTY]);
        if (!$crontabTask) {
            $crontabTask = new CrontabTask();
            $crontabTask->name = CrontabTask::TASK_MAKE_AUTO_PENALTY;
            $crontabTask->description = "Создание автоматических штрафов сотрудникам";
            $crontabTask->save();
        }

        $notification = Notification::findOne(['trigger' => Notification::TRIGGER_MAKE_AUTO_PENALTY]);
        if (!($notification instanceof Notification)) {
            $notification = new Notification([
                'description' => 'A penalty was given to an employee {person} from the office {office} for {penalty} {date}',
                'trigger' => Notification::TRIGGER_MAKE_AUTO_PENALTY,
                'type' => Notification::TYPE_INFO,
                'group' => Notification::GROUP_SYSTEM,
                'active' => Notification::ACTIVE,
            ]);
            $notification->save(false);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_MAKE_AUTO_PENALTY]);
        if ($crontabTask instanceof CrontabTask) {
            $crontabTask->delete();
        }

        $notification = Notification::findOne(['trigger' => Notification::TRIGGER_MAKE_AUTO_PENALTY]);
        if ($notification instanceof Notification) {
            $notification->delete();
        }
    }
}
