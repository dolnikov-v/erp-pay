<?php

use app\components\PermissionMigration as Migration;

/**
 * Class m190116_083653_information_queue
 */
class m190116_083653_information_queue extends Migration
{
    protected $permissions = ['order.index.sendinformation'];

    protected $roles = [
        'country.curator' => [
            'order.index.sendinformation'
        ],
        'operational.director' => [
            'order.index.sendinformation'
        ],
        'project.manager' => [
            'order.index.sendinformation'
        ],
        'support.manager' => [
            'order.index.sendinformation'
        ],
        'technical.director' => [
            'order.index.sendinformation'
        ],
    ];


    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        $this->alterColumn('call_center_check_request', 'comment', $this->text());
        $this->addColumn('call_center_check_request', 'request_comment', $this->string(1000)->after('comment'));

        parent::safeUp();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('call_center_check_request', 'comment', $this->string(1000));
        $this->dropColumn('call_center_check_request', 'request_comment');

        parent::safeDown();
    }
}
