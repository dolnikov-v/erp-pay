<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170404_063107_add_permission_for_report_standart_sales_index
 */
class m170404_063107_add_permission_for_report_standart_sales_index extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}',array(
            'name'=>'report.standartsales.index',
            'type' => '2',
            'description' => 'report.standartsales.index'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'country.curator',
            'child' => 'report.standartsales.index'
        ));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->authManager->itemChildTable, ['child' => 'report.standartsales.index', 'parent' => 'country.curator']);
        $this->delete('{{%auth_item}}', ['name' => 'report.standartsales.index']);
    }
}
