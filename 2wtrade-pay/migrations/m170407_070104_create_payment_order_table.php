<?php

use app\modules\deliveryreport\models\DeliveryReport;
use app\modules\deliveryreport\models\PaymentOrder;
use app\components\CustomMigration as Migration;

/**
 * Handles the creation for table `payment_order_delivery`.
 */
class m170407_070104_create_payment_order_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable(PaymentOrder::tableName(), [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'sum' => $this->integer()->notNull(),
            'delivery_report_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull()
        ]);
        $this->addForeignKey(null, PaymentOrder::tableName(), 'delivery_report_id', DeliveryReport::tableName(), 'id', self::CASCADE, self::CASCADE);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable(PaymentOrder::tableName());
    }
}
