<?php
use app\components\CustomMigration as Migration;

/**
 * Class m181122_025606_office_expense_add
 */
class m181122_025606_office_expense_add extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('salary_office', 'expense_legal_service', $this->double()->after('expense_it_support'));
        $this->addColumn('salary_office', 'expense_bank_fees', $this->double()->after('expense_legal_service'));
        $this->addColumn('salary_office', 'expense_representation', $this->double()->after('expense_bank_fees'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('salary_office', 'expense_legal_service');
        $this->dropColumn('salary_office', 'expense_bank_fees');
        $this->dropColumn('salary_office', 'expense_representation');
    }
}
