<?php
use yii\db\Migration;

/**
 * Class m170929_105247_add_theme_to_user
 */
class m170929_105247_add_theme_to_user extends Migration
{

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('user', 'theme', $this->string(64)->defaultValue('basic'));
        $this->addColumn('user', 'skin', $this->string(64)->defaultValue('basic'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('user', 'theme');
        $this->dropColumn('user', 'skin');
    }
}
