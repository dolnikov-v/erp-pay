<?php
use app\components\CustomMigration as Migration;

/**
 * Class m180122_071253_invoice_payment
 */
class m180122_071253_invoice_payment extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('invoice_payment', [
            'invoice_id' => $this->integer(),
            'payment_id' => $this->integer(),
        ], $this->tableOptions);

        $this->addPrimaryKey($this->getPkName('invoice_payment', [
            'invoice_id',
            'payment_id'
        ]), 'invoice_payment', ['invoice_id', 'payment_id']);

        $this->addForeignKey($this->getFkName('invoice_payment', 'invoice_id'), 'invoice_payment', 'invoice_id', 'invoice', 'id', self::CASCADE, self::CASCADE);
        $this->addForeignKey($this->getFkName('invoice_payment', 'payment_id'), 'invoice_payment', 'payment_id', 'payment_order_delivery', 'id', self::CASCADE, self::CASCADE);

        $this->alterColumn('payment_order_delivery', 'delivery_report_id', $this->integer()->defaultValue(null));

        $this->addColumn('payment_order_delivery', 'currency_id', $this->integer()->defaultValue(null)->after('sum'));
        $this->addForeignKey($this->getFkName('payment_order_delivery', 'currency_id'), 'payment_order_delivery', 'currency_id', 'currency', 'id', self::SET_NULL, self::CASCADE);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey($this->getFkName('payment_order_delivery', 'currency_id'), 'payment_order_delivery');
        $this->dropColumn('payment_order_delivery', 'currency_id');

        $this->alterColumn('payment_order_delivery', 'delivery_report_id', $this->integer());
        $this->dropTable('invoice_payment');
    }
}
