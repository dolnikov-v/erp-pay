<?php
use app\components\PermissionMigration as Migration;
use app\modules\salary\models\Office;

/**
 * Class m171226_084013_staffing_access
 */
class m171226_084013_staffing_access extends Migration
{
    protected $permissions = [
        'salary.staffing.index',
        'view_staffing_salary',
        'view_staffing_salary_moscow',
        'office_edit_hide_staffing_salary',
    ];

    protected $roles = [
        'country.curator' => [
            'salary.staffing.index',
            'view_staffing_salary'
        ],
        //Директор по Фин-юр вопросам и операционной деятельности
        'finance.director' => [
            'salary.staffing.index',
            'view_staffing_salary'
        ],
        //Генеральный директор
        'general.director' => [
            'salary.staffing.index',
            'salary.staffing.delete',
            'salary.staffing.edit',
            'view_staffing_salary_moscow'
        ],
        //Директор по развитию
        'development.director' => [
            'salary.staffing.index',
            'view_staffing_salary'
        ],
        //Исполнительный директор
        'executive.director' => [
            'salary.staffing.index',
            'view_staffing_salary'
        ],
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Office::tableName(), 'hide_staffing_salary', $this->integer(1)->defaultValue(0));

        parent::safeUp();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Office::tableName(), 'hide_staffing_salary');

        parent::safeDown();
    }
}
