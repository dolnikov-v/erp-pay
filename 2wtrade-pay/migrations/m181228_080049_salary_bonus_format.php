<?php
use app\components\CustomMigration as Migration;

/**
 * Class m181228_080049_salary_bonus_format
 */
class m181228_080049_salary_bonus_format extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('salary_bonus', 'bonus_sum_usd', $this->decimal(15, 2));
        $this->alterColumn('salary_bonus', 'bonus_sum_local', $this->decimal(15, 2));

        $this->alterColumn('salary_penalty', 'penalty_sum_usd', $this->decimal(15, 2));
        $this->alterColumn('salary_penalty', 'penalty_sum_local', $this->decimal(15, 2));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('salary_bonus', 'bonus_sum_usd', $this->float());
        $this->alterColumn('salary_bonus', 'bonus_sum_local', $this->float());

        $this->alterColumn('salary_penalty', 'penalty_sum_usd', $this->float());
        $this->alterColumn('salary_penalty', 'penalty_sum_local', $this->float());
    }
}
