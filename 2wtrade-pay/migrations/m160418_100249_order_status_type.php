<?php
use app\components\CustomMigration as Migration;
use app\modules\order\models\OrderStatus;

class m160418_100249_order_status_type extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('order_status', 'type', "ENUM('starting', 'middle', 'final') DEFAULT NULL");
        $this->createIndex('idx_order_status_type', 'order_status', 'type');

        /** @var OrderStatus[] $statuses */
        $statuses = OrderStatus::find()->all();

        $fixture = $this->getFixture();

        foreach ($statuses as $status) {
            $status->type = $fixture[$status->id];
            $status->save(false, ['type']);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('order_status', 'type');
    }

    /**
     * @return array
     */
    private function getFixture()
    {
        return [
            OrderStatus::STATUS_SOURCE_LONG_FORM => OrderStatus::TYPE_STARTING,
            OrderStatus::STATUS_SOURCE_SHORT_FORM => OrderStatus::TYPE_STARTING,
            OrderStatus::STATUS_CC_POST_CALL => OrderStatus::TYPE_MIDDLE,
            OrderStatus::STATUS_CC_RECALL => OrderStatus::TYPE_MIDDLE,
            OrderStatus::STATUS_CC_FAIL_CALL => OrderStatus::TYPE_FINAL,
            OrderStatus::STATUS_CC_APPROVED => OrderStatus::TYPE_MIDDLE,
            OrderStatus::STATUS_CC_REJECTED => OrderStatus::TYPE_FINAL,
            OrderStatus::STATUS_LOG_ACCEPTED => OrderStatus::TYPE_MIDDLE,
            OrderStatus::STATUS_LOG_GENERATED => OrderStatus::TYPE_MIDDLE,
            OrderStatus::STATUS_LOG_SET => OrderStatus::TYPE_MIDDLE,
            OrderStatus::STATUS_LOG_PASTED => OrderStatus::TYPE_MIDDLE,
            OrderStatus::STATUS_DELIVERY_ACCEPTED => OrderStatus::TYPE_MIDDLE,
            OrderStatus::STATUS_DELIVERY_LEFT_WAREHOUSE => OrderStatus::TYPE_MIDDLE,
            OrderStatus::STATUS_DELIVERY => OrderStatus::TYPE_MIDDLE,
            OrderStatus::STATUS_DELIVERY_BUYOUT => OrderStatus::TYPE_MIDDLE,
            OrderStatus::STATUS_DELIVERY_DENIAL => OrderStatus::TYPE_MIDDLE,
            OrderStatus::STATUS_DELIVERY_RETURNED => OrderStatus::TYPE_FINAL,
            OrderStatus::STATUS_FINANCE_MONEY_RECEIVED => OrderStatus::TYPE_MIDDLE,
            OrderStatus::STATUS_DELIVERY_REJECTED => OrderStatus::TYPE_FINAL,
            OrderStatus::STATUS_DELIVERY_LEFT_OUR_WAREHOUSE => OrderStatus::TYPE_MIDDLE,
            OrderStatus::STATUS_DELIVERY_REDELIVERY => OrderStatus::TYPE_MIDDLE,
            OrderStatus::STATUS_DELIVERY_LEFT_THEIR_WAREHOUSE => OrderStatus::TYPE_MIDDLE,
            OrderStatus::STATUS_LOGISTICS_REJECTED => OrderStatus::TYPE_MIDDLE,
            OrderStatus::STATUS_DELIVERY_DELAYED => OrderStatus::TYPE_MIDDLE,
            OrderStatus::STATUS_CC_TRASH => OrderStatus::TYPE_FINAL,
            OrderStatus::STATUS_CC_DOUBLE => OrderStatus::TYPE_FINAL,
            OrderStatus::STATUS_DELIVERY_REFUND => OrderStatus::TYPE_MIDDLE,
            OrderStatus::STATUS_LOGISTICS_ACCEPTED => OrderStatus::TYPE_MIDDLE,
            OrderStatus::STATUS_DELIVERY_LOST => OrderStatus::TYPE_FINAL,
            OrderStatus::STATUS_CC_INPUT_QUEUE => OrderStatus::TYPE_MIDDLE,
            OrderStatus::STATUS_CC_INPUT_QUEUE => OrderStatus::TYPE_STARTING,
        ];
    }
}
