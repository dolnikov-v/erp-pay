<?php
use app\components\CustomMigration as Migration;
use app\models\Notification;
use app\modules\administration\models\CrontabTask;

/**
 * Class m171105_091912_add_orders_in_ready_to_send_status_notification
 */
class m171105_091912_add_orders_in_ready_to_send_status_notification extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $notification = new Notification([
            'description' => 'Заказов готовых к отправке по странам: {text}',
            'trigger' => 'report.orders.in.ready.to.send.status',
            'type' => Notification::TYPE_INFO,
            'group' => Notification::GROUP_REPORT,
            'active' => Notification::ACTIVE,
        ]);
        $notification->save();

        $crontabTask = new CrontabTask();
        $crontabTask->name = 'orders_in_ready_to_send_status';
        $crontabTask->description = 'Уведомление о заказах в статусах готовых к отправке с тенденциями по странам';
        $crontabTask->save();
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $notification = Notification::findOne(['trigger' => 'report.orders.in.ready.to.send.status']);

        if ($notification instanceof Notification) {
            $notification->delete();
        }

        $crontabTask = CrontabTask::findOne(['name' => 'orders_in_ready_to_send_status']);

        if ($crontabTask instanceof CrontabTask) {
            $crontabTask->delete();
        }
    }
}
