<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160420_094829_storage_call_center
 */
class m160420_094829_storage_call_center extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('storage', 'call_center_id', $this->integer()->defaultValue(null) . ' AFTER `country_id`');
        $this->addForeignKey(null, 'storage', 'call_center_id', 'call_center', 'id', self::SET_NULL, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey($this->getFkName('storage', 'call_center_id'), 'storage');
        $this->dropColumn('storage', 'call_center_id');
    }
}
