<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m170821_034547_express_lists
 */
class m170821_034547_express_lists extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_DELIVERY_GENERATE_EXPRESS_LISTS]);
        if (!$crontabTask) {
            $crontabTask = new CrontabTask();
            $crontabTask->name = CrontabTask::TASK_DELIVERY_GENERATE_EXPRESS_LISTS;
            $crontabTask->description = "Генерация и отправка экспресс листов";
            $crontabTask->save();
        }

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_DELIVERY_GENERATE_EXPRESS_LISTS]);
        if ($crontabTask instanceof CrontabTask) {
            $crontabTask->delete();
        }
    }
}
