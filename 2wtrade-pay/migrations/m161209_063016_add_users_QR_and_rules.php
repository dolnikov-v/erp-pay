<?php
use app\components\CustomMigration as Migration;
use app\models\User;

/**
 * Class m161209_063016_add_users_QR_and_rules
 */
class m161209_063016_add_users_QR_and_rules extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $user = new User();
        $user->username = 'maithailand';
        $user->setPassword('maithailand');
        $user->generateAuthKey();
        $user->status = User::STATUS_DEFAULT;
        $user->save(false);

        $user = new User();
        $user->username = 'cv_an_nur';
        $user->setPassword('cv_an_nur');
        $user->generateAuthKey();
        $user->status = User::STATUS_DEFAULT;
        $user->save(false);

        $user = new User();
        $user->username = 'dibar';
        $user->setPassword('dibar');
        $user->generateAuthKey();
        $user->status = User::STATUS_DEFAULT;
        $user->save(false);

        $user = new User();
        $user->username = 'white_pearl_cosmetics';
        $user->setPassword('white_pearl_cosmetics');
        $user->generateAuthKey();
        $user->status = User::STATUS_DEFAULT;
        $user->save(false);

        $user = new User();
        $user->username = 'sarkar_herbal';
        $user->setPassword('sarkar_herbal');
        $user->generateAuthKey();
        $user->status = User::STATUS_DEFAULT;
        $user->save(false);

        $user = new User();
        $user->username = 'dynamic_lifecate';
        $user->setPassword('dynamic_lifecate');
        $user->generateAuthKey();
        $user->status = User::STATUS_DEFAULT;
        $user->save(false);

        $user = new User();
        $user->username = 'perennial';
        $user->setPassword('perennial');
        $user->generateAuthKey();
        $user->status = User::STATUS_DEFAULT;
        $user->save(false);

        $user = new User();
        $user->username = 'xinjang_wulian';
        $user->setPassword('xinjang_wulian');
        $user->generateAuthKey();
        $user->status = User::STATUS_DEFAULT;
        $user->save(false);

        $user = new User();
        $user->username = 'slk_systema';
        $user->setPassword('slk_systema');
        $user->generateAuthKey();
        $user->status = User::STATUS_DEFAULT;
        $user->save(false);

        $user = new User();
        $user->username = 'hendel';
        $user->setPassword('hendel');
        $user->generateAuthKey();
        $user->status = User::STATUS_DEFAULT;
        $user->save(false);

        $user = new User();
        $user->username = 'gentech';
        $user->setPassword('gentech');
        $user->generateAuthKey();
        $user->status = User::STATUS_DEFAULT;
        $user->save(false);
    }

    /**
     * @inheritdoc
     */
    public function safeDown(){
        User::deleteAll(['username' => [
            'maithailand',
            'cv_an_nur',
            'dibar',
            'white_pearl_cosmetics',
            'sarkar_herbal',
            'dynamic_lifecate',
            'perennial',
            'xinjang_wulian',
            'slk_systema',
            'hendel',
            'gentech'
        ]]);
    }
}
