<?php
use app\components\CustomMigration as Migration;

/**
 * Class m161214_114347_rename_qrlist_bar
 */
class m161214_114347_rename_qrlist_bar extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropTable('qr_list');

        $this->createTable('bar_list', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(4),
            'file' => $this->string(100)->notNull(),
            'created_at' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)->notNull(),
        ], $this->tableOptions);
        
        $this->addForeignKey(null, 'bar_list', 'user_id', 'user', 'id',  self::NO_ACTION, self::NO_ACTION);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('bar_list');

        $this->createTable('qr_list', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(4),
            'file' => $this->string(100)->notNull(),
            'created_at' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)->notNull(),
        ], $this->tableOptions);

        $this->addForeignKey(null, 'qr_list', 'user_id', 'user', 'id',  self::NO_ACTION, self::NO_ACTION);
    }
}
