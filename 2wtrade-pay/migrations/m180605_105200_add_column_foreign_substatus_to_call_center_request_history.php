<?php

use app\modules\callcenter\models\CallCenterRequestHistory;
use yii\db\Migration;

/**
 * Handles adding column_foreign_substatus to table `call_center_request_history`.
 */
class m180605_105200_add_column_foreign_substatus_to_call_center_request_history extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn(CallCenterRequestHistory::tableName(), 'foreign_substatus', $this->integer()->after('foreign_status')->defaultValue(null));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn(CallCenterRequestHistory::tableName(), 'foreign_substatus');
    }
}
