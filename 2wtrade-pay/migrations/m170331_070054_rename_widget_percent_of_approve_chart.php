<?php

use app\components\CustomMigration as Migration;
use app\modules\widget\models\WidgetType;
use app\modules\widget\models\WidgetRole;

/**
 * Class m170331_070054_rename_widget_percent_of_approve_chart
 */
class m170331_070054_rename_widget_percent_of_approve_chart extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->update(WidgetType::tableName(), ['name' => 'Процент апрува'], ['code' => 'percent_of_approves_chart']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->update(WidgetType::tableName(), ['name' => 'Процент по апрувам'], ['code' => 'percent_of_approves_chart']);
    }
}

