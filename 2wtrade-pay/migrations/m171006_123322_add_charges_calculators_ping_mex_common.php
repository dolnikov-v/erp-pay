<?php
use app\components\CustomMigration as Migration;

/**
 * Class m171006_123322_add_charges_calculators_ping_mex_common
 */
class m171006_123322_add_charges_calculators_ping_mex_common extends Migration
{
    protected static $calculators = [
        'Amerisa' => 'app\modules\delivery\components\charges\amerisa\Amerisa',
        'Merq' => 'app\modules\delivery\components\charges\merq\Merq',
        'OnlyDeliveryCharge' => 'app\modules\delivery\components\charges\OnlyDeliveryCharge',
        'PingDelivery' => 'app\modules\delivery\components\charges\pingdelivery\PingDelivery',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        foreach (self::$calculators as $name => $route) {
            $this->insert('delivery_charges_calculator', [
                'name' => $name,
                'class_path' => $route,
                'active' => 1,
                'created_at' => time(),
            ]);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach (self::$calculators as $name => $route) {
            $this->delete('delivery_charges_calculator', ['name' => $name]);
        }
    }
}
