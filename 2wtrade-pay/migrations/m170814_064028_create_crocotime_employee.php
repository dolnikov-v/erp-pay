<?php

use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;
use app\modules\salary\models\Office;
use app\modules\salary\models\Person;

/**
 * Handles the creation for table `crocotime_employee`.
 */
class m170814_064028_create_crocotime_employee extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        /*$this->createTable('crocotime_employee', [
            'id' => $this->primaryKey(),
            'employee_id' => $this->integer(6),
            'display_name' => $this->string(128),
            'first_name' => $this->string(64),
            'second_name' => $this->string(64),
            'email' => $this->string(128),
            'time_zone' => $this->string(64),
            'parent_group_id' => $this->integer(3),
        ]);*/

        $this->addColumn(Person::tableName(), 'crocotime_employee_id', $this->integer(6));
        //$this->addForeignKey(null, Person::tableName(), 'crocotime_employee_id', 'crocotime_employee', 'employee_id');
        $this->createIndex(null, Person::tableName(), 'crocotime_employee_id', true);
        $this->addColumn(Office::tableName(), 'crocotime_parent_group_id', $this->integer(3));
        //$this->addForeignKey(null, Office::tableName(), 'crocotime_parent_group_id', 'crocotime_employee', 'parent_group_id', self::SET_NULL, self::NO_ACTION);

        $crontabTask = new CrontabTask();
        $crontabTask->name = 'sync_employees';
        $crontabTask->active = 1;
        $crontabTask->description = 'Синхронизирует список сотрудников в crocotime и в пее.';
        $crontabTask->save();

        $crontabTask = new CrontabTask();
        $crontabTask->name = 'sync_offices';
        $crontabTask->active = 1;
        $crontabTask->description = 'Синхронизирует отделы в crocotime и CallCenterOffice в пее.';
        $crontabTask->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        //$this->dropTable('crocotime_employee');
        $this->dropColumn(Person::tableName(), 'crocotime_employee_id');
        $this->dropColumn(Office::tableName(), 'crocotime_parent_group_id');
        $this->delete(CrontabTask::tableName(), ['name' => ['sync_employees', 'sync_offices']]);
    }
}
