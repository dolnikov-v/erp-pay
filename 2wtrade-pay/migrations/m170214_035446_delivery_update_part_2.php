<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170214_035446_delivery_update_part_2
 */
class m170214_035446_delivery_update_part_2 extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropColumn('delivery', 'zipcodes');
        $this->addColumn('delivery', 'delivery_intervals', $this->text());

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->addColumn('delivery', 'zipcodes', $this->text());
        $this->dropColumn('delivery', 'delivery_intervals');
    }
}
