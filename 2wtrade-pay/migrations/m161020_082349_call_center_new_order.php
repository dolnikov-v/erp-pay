<?php
use app\components\CustomMigration as Migration;

/**
 * Class m161020_082349_call_center_new_order
 */
class m161020_082349_call_center_new_order extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('call_center_request', 'foreign_waiting', $this->smallInteger()->defaultValue(0)->after('foreign_status'));
        $this->createIndex(null, 'call_center_request', 'foreign_waiting');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropIndex($this->getIdxName('call_center_request', 'foreign_waiting'), 'call_center_request');
        $this->dropColumn('call_center_request', 'foreign_waiting');
    }
}
