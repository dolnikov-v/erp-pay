<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170720_101742_add_permission_validate_finance_report_export_to_excel
 */
class m170720_101742_add_permission_validate_finance_report_export_to_excel extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}',
        [
            'name'=>'report.validatefinance.exporttoexcel',
            'type' => '2',
            'description' => 'report.validatefinance.exporttoexcel',
            'created_at' => time(),
            'updated_at' => time()
        ]);

        $roles = Yii::$app->authManager->getRoles();
        foreach ($roles as $role) {
            $this->insert($this->authManager->itemChildTable,
            [
                'parent' => $role->name,
                'child' => 'report.validatefinance.exporttoexcel'
            ]);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->authManager->itemTable, ['name' => 'report.validatefinance.exporttoexcel']);
    }
}