<?php
use app\components\CustomMigration as Migration;
use app\modules\order\models\Order;

/**
 * Class m161107_071035_lead_source
 */
class m161107_071035_lead_source extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('order', 'source', $this->enum(['adcombo', 'jeempo'])->defaultValue(null)->after('id'));

        $this->dropIndex($this->getIdxName('order', 'foreign_id'), 'order');
        $this->createIndex(null, 'order', ['source', 'foreign_id'], true);

        Order::updateAll(['source' => 'adcombo'], 'foreign_id IS NOT NULL');

        $this->renameColumn('order', 'adcombo_form', 'source_form');
        $this->renameColumn('order', 'adcombo_confirmed', 'source_confirmed');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->renameColumn('order', 'source_confirmed', 'adcombo_confirmed');
        $this->renameColumn('order', 'source_form', 'adcombo_form');

        $this->dropIndex($this->getUniName('order', ['source', 'foreign_id']), 'order');
        $this->createIndex(null, 'order', 'foreign_id');

        $this->dropColumn('order', 'source');
    }
}
