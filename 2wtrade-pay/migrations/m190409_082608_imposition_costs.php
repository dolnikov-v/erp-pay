<?php

use app\components\PermissionMigration as Migration;

/**
 * Class m190409_082608_imposition_costs
 */
class m190409_082608_imposition_costs extends Migration
{
    const PRECISION = 10;

    protected $permissions = [
        'deliveryreport.report.changeadditionalcost',
        'deliveryreport.report.deleteadditionalcost',
    ];

    protected $roleList = [
        'admin',
        'country.curator',
        'deliveryreport.clerk',
        'finance.manager',
        'finance.anager',
        'finance.director',
        'foreign.deliveryreport.clerk',
        'technical.director',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('delivery_report_costs', [
            'id' => $this->primaryKey(),
            'delivery_report_id' => $this->integer()->notNull(),
            'sum' => $this->double()->defaultValue(0),
            'balance' => $this->double()->defaultValue(0),
            'from' => $this->integer()->notNull(),
            'to' => $this->integer()->notNull(),
            'description' => $this->string(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);

        $this->addForeignKey(null, 'delivery_report_costs', 'delivery_report_id', 'delivery_report', 'id', self::CASCADE, self::CASCADE);

        $this->addCommentOnTable('delivery_report_costs', 'Дополнительные издержки');
        $this->addCommentOnColumn('delivery_report_costs', 'delivery_report_id', 'Отчет');
        $this->addCommentOnColumn('delivery_report_costs', 'sum', 'Сумма издержки');
        $this->addCommentOnColumn('delivery_report_costs', 'balance', 'Остаток от sum для списания');
        $this->addCommentOnColumn('delivery_report_costs', 'from', 'Начало периода издержки');
        $this->addCommentOnColumn('delivery_report_costs', 'to', 'Конец периода издержки');
        $this->addCommentOnColumn('delivery_report_costs', 'description', 'Краткое описание издержки');

        $this->db->createCommand("
            INSERT INTO `delivery_report_costs` 
                (`delivery_report_id`, `sum`, `balance`, `from`, `to`, `description`, `created_at`, `updated_at`)
                SELECT `id`, `additional_costs`, 
                    ROUND(IF(`sum_total` > `additional_costs`, `additional_costs`, `sum_total`), :precision) AS balance, 
                    IF(IFNULL(`period_from`, 0) = 0, UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 1 DAY)), `period_from`),
                    IF(IFNULL(`period_to`, 0) = 0, UNIX_TIMESTAMP(), `period_from`),
                    'Доп. издержки', UNIX_TIMESTAMP(), UNIX_TIMESTAMP()
                FROM `delivery_report`
                WHERE `additional_costs` is not null and `additional_costs` != 0 and `sum_total` > 0
            ", [':precision' => static::PRECISION]
        )->execute();

        $this->db->createCommand("
            UPDATE `delivery_report`
            SET `sum_total` = IF(`sum_total` > `additional_costs`, ROUND(`sum_total` - `additional_costs`, :precision), 0)
            WHERE `additional_costs` is not null and `additional_costs` != 0 and `sum_total` > 0
            ", [':precision' => static::PRECISION]
        )->execute();

        $this->dropColumn('delivery_report', 'additional_costs');

        parent::safeUp();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->addColumn('delivery_report', 'additional_costs', $this->double()->after('total_costs')->defaultValue(0));

        $this->db->createCommand("
            UPDATE `delivery_report`
            INNER JOIN `delivery_report_costs` on `delivery_report_costs`.`delivery_report_id` = `delivery_report`.`id`
            SET `delivery_report`.`sum_total` = ROUND(`delivery_report`.`sum_total` + `delivery_report_costs`.`balance`, :precision),
                `delivery_report`.`additional_costs` = `delivery_report_costs`.`sum`
            WHERE `delivery_report_costs`.`sum` > 0 OR `delivery_report_costs`.`balance` > 0
          ", [':precision' => static::PRECISION]
        )->execute();

        $this->dropTable('delivery_report_costs');

        parent::safeDown();
    }
}
