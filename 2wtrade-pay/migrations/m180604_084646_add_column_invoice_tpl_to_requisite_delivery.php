<?php

use app\modules\catalog\models\RequisiteDelivery;
use yii\db\Migration;

/**
 * Handles adding column_invoice_tpl to table `requisite_delivery`.
 */
class m180604_084646_add_column_invoice_tpl_to_requisite_delivery extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn(RequisiteDelivery::tableName(), 'invoice_tpl', $this->integer()->after('swift_code')->defaultValue(null));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn(RequisiteDelivery::tableName(), 'invoice_tpl');
    }
}
