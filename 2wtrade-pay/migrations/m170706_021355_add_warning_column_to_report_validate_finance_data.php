<?php

use yii\db\Migration;
use app\modules\report\models\ReportValidateFinanceData;

/**
 * Handles adding warning_column to table `report_validate_finance_data`.
 */
class m170706_021355_add_warning_column_to_report_validate_finance_data extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn(ReportValidateFinanceData::tableName(), 'warning', $this->string(500)->after('error'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn(ReportValidateFinanceData::tableName(), 'warning');
    }
}
