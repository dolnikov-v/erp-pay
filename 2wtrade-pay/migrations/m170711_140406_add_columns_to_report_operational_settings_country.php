<?php

use yii\db\Migration;

/**
 * Class m170711_140406_add_columns_to_report_operational_settings_country
 */
class m170711_140406_add_columns_to_report_operational_settings_country extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('report_operational_settings_countries', 'leader_id', 'INT(11) AFTER team');
        $this->addColumn('report_operational_settings_countries', 'leader_name', 'VARCHAR(255) AFTER leader_id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('report_operational_settings_countries', 'leader_name');
        $this->dropColumn('report_operational_settings_countries', 'leader_id');
    }
}
