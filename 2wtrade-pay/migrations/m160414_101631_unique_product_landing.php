<?php

use app\components\CustomMigration as Migration;

class m160414_101631_unique_product_landing extends Migration
{
    public function safeUp()
    {
        $this->truncateTable('product_landing');
        $this->createIndex('unique_product_id_landing_id', 'product_landing', ['product_id', 'landing_id'], true);
    }

    public function safeDown()
    {
        Yii::$app->db->createCommand()->checkIntegrity(false)->execute();

        $this->dropIndex('unique_product_id_landing_id', 'product_landing');

        Yii::$app->db->createCommand()->checkIntegrity(true)->execute();
    }
}
