<?php
use app\components\CustomMigration as Migration;
use app\modules\checklist\models\CheckList;
use app\modules\checklist\models\CheckListItem;

/**
 * Class m171031_093038_check_list_six
 */
class m171031_093038_check_list_six extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(CheckListItem::tableName(), 'explainable', $this->integer(1)->after('trigger'));
        $this->addColumn(CheckList::tableName(), 'data_info', $this->text()->after('team_id'));
        $this->addColumn(CheckList::tableName(), 'comment', $this->text()->after('team_id'));

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(CheckListItem::tableName(), 'explainable');
        $this->dropColumn(CheckList::tableName(), 'data_info');
        $this->dropColumn(CheckList::tableName(), 'comment');
    }
}
