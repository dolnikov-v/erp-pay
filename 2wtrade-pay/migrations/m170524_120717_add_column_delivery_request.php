<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryRequest;

/**
 * Class m170524_120717_add_column_delivery_request
 */
class m170524_120717_add_column_delivery_request extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(DeliveryRequest::tableName(), 'unshipping_reason', $this->string(32)->defaultValue(null));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(DeliveryRequest::tableName(),'unshipping_reason');
    }
}
