<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;
use app\models\AccountingCost;
use \yii\db\Query;

/**
 * Class m170131_052305_add_cron_import_1c_costs
 */
class m170131_052305_add_cron_import_1c_costs extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $query = new Query();

        $roles = [
            'business_analyst',
            'finance.manager'
        ];

        foreach ($roles as $role) {
            $is_role = $query->select('*')->from($this->authManager->itemTable)
                ->where([
                    'name' => $role,
                    'type' => 1])
                ->one();

            if (!$is_role) {
                echo "Error: No " . $role . " exists";
                die();
            }
        }

        $crontabTask = CrontabTask::findOne(['name' => 'import_1c_costs']);

        if (!($crontabTask instanceof CrontabTask)) {
            $crontabTask = new CrontabTask();
            $crontabTask->name = 'import_1c_costs';
            $crontabTask->description = 'Подтягивание расходов из 1С';
            $crontabTask->save();
        }

        $this->createTable(AccountingCost::tableName(), [
            'id' => $this->primaryKey(),
            'date_article' => $this->date()->notNull(),
            'code' => $this->string(16),
            'name' => $this->string(64),
            'country' => $this->string(2),
            'subdivision' => $this->string(64),
            'turnover' => $this->double(),
            'incoming' => $this->double(),
            'outcoming' => $this->double(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $this->tableOptions);

        $report = "report.accountingcosts.index";

        $is_report = $query->select('*')->from($this->authManager->itemTable)
            ->where([
                'name' => $report,
                'type' => 2])
            ->one();

        if (!$is_report) {
            $this->insert($this->authManager->itemTable, [
                'name' => $report,
                'type' => 2,
                'description' => $report,
                'created_at' => time(),
                'updated_at' => time()]);
        }

        foreach ($roles as $role) {
            $is_br = $query->select('*')->from($this->authManager->itemChildTable)->where([
                'parent' => $role,
                'child' => $report
            ])->one();

            if (!$is_br) {
                $this->insert($this->authManager->itemChildTable, [
                    'parent' => $role,
                    'child' => $report
                ]);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = CrontabTask::findOne(['name' => 'import_1c_costs']);

        if ($crontabTask instanceof CrontabTask) {
            $crontabTask->delete();
        }

        $this->dropTable(AccountingCost::tableName());

        $report = "report.accountingcosts.index";

        $this->delete($this->authManager->itemTable, [
            'name' => $report,
        ]);

        $roles = [
            'business_analyst',
            'finance.manager'
        ];

        foreach ($roles as $role) {
            $this->delete($this->authManager->itemChildTable, [
                'parent' => $role,
                'child' => $report
            ]);
        }
    }
}
