<?php
use app\models\Timezone;
use yii\db\Migration;

class m160411_133425_fix_timezones extends Migration
{
    public function safeUp()
    {
        /** @var Timezone $timezone */
        $timezone = Timezone::find()->byTimezoneId('America/Mexico_City')->one();

        if ($timezone) {
            $timezone->time_offset = -18000;
            $timezone->name = '(GMT -05:00) Америка/Центральное время – Мехико';
            $timezone->save();
        }

        $timezone = Timezone::find()->byTimezoneId('Asia/Amman')->one();

        if ($timezone) {
            $timezone->time_offset = 10800;
            $timezone->name = '(GMT +03:00) Азия/Амман';
            $timezone->save();
        }
    }

    public function safeDown()
    {
        /** @var Timezone $timezone */
        $timezone = Timezone::find()->byTimezoneId('America/Mexico_City')->one();

        if ($timezone) {
            $timezone->time_offset = -21600;
            $timezone->name = '(GMT -06:00) Америка/Центральное время – Мехико';
            $timezone->save();
        }

        $timezone = Timezone::find()->byTimezoneId('Asia/Amman')->one();

        if ($timezone) {
            $timezone->time_offset = 7200;
            $timezone->name = '(GMT +02:00) Азия/Амман';
            $timezone->save();
        }
    }
}
