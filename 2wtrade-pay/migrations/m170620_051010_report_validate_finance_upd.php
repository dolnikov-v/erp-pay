<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170620_051010_report_validate_finance_upd
 */
class m170620_051010_report_validate_finance_upd extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableSchema = Yii::$app->db->schema->getTableSchema('report_validate_finance');

        if ($tableSchema !== null) {
            $this->dropTable('report_validate_finance');
        }

        $this->createTable('report_validate_finance', [
            'id' => $this->primaryKey(),
            'file_name' => $this->string(200)->notNull(),
            'row_begin' => $this->integer(),
            'cell_order_id' => $this->integer(),
            'cell_tracking' => $this->integer(),
            'cell_delivery_cost_percent' => $this->integer(),
            'cell_fulfillment_cost' => $this->integer(),
            'cell_nds' => $this->integer(),
            'cell_fulfillment_nds' => $this->integer(),
            'cell_delivery_price' => $this->integer(),
            'cell_total_price' => $this->integer(),
            'cell_status' => $this->integer(),
            'status_map' => $this->text(),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11),
        ]);

        $this->createTable('report_validate_finance_data', [
            'id' => $this->primaryKey(),
            'file_id' => $this->integer(),
            'order_id' => $this->string(50),
            'order_id_file' => $this->string(50),
            'tracking' => $this->string(50),
            'tracking_file' => $this->string(50),
            'delivery_cost_percent' => $this->string(50),
            'delivery_cost_percent_file' => $this->string(50),
            'fulfillment_cost' => $this->string(50),
            'fulfillment_cost_file' => $this->string(50),
            'nds' => $this->string(50),
            'nds_file' => $this->string(50),
            'fulfillment_nds' => $this->string(50),
            'fulfillment_nds_file' => $this->string(50),
            'delivery_price' => $this->string(50),
            'delivery_price_file' => $this->string(50),
            'total_price' => $this->string(50),
            'total_price_file' => $this->string(50),
            'status' => $this->string(50),
            'status_file' => $this->string(50),
            'error' => $this->string(500)->defaultValue(null),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11),
        ]);

        $this->addForeignKey(null, 'report_validate_finance_data', 'file_id', 'report_validate_finance', 'id', self::CASCADE, self::RESTRICT);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $tableSchema = Yii::$app->db->schema->getTableSchema('report_validate_finance_data');
        if ($tableSchema !== null) {
            $this->dropTable('report_validate_finance_data');
        }

        $tableSchema = Yii::$app->db->schema->getTableSchema('report_validate_finance');
        if ($tableSchema !== null) {
            $this->dropTable('report_validate_finance');
        }
    }
}
