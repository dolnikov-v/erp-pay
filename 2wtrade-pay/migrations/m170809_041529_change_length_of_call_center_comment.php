<?php
use app\components\CustomMigration as Migration;
use app\modules\callcenter\models\CallCenterRequest;

/**
 * Class m170809_041529_change_length_of_call_center_comment
 */
class m170809_041529_change_length_of_call_center_comment extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn(CallCenterRequest::tableName(), 'comment', $this->string(1000));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn(CallCenterRequest::tableName(), 'comment', $this->string(100));
    }
}
