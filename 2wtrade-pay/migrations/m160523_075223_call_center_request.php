<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160523_075223_call_center_request
 */
class m160523_075223_call_center_request extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('call_center_request', 'comment', $this->string(100)->defaultValue(null)->after('foreign_status'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('call_center_request', 'comment');
    }
}
