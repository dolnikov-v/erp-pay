<?php
use app\components\CustomMigration as Migration;
use app\modules\salary\models\Designation;

/**
 * Class m171213_114606_salary_designation
 */
class m171213_114606_salary_designation extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Designation::tableName(), 'comment', $this->string());
        $this->addColumn(Designation::tableName(), 'salary_usd', $this->double());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Designation::tableName(), 'comment');
        $this->dropColumn(Designation::tableName(), 'salary_usd');
    }
}
