<?php
use app\components\CustomMigration as Migration;
use app\modules\widget\models\WidgetType;
use app\modules\widget\models\WidgetRole;

/**
 * Class m170411_055435_add_widget_buyout
 */
class m170411_055435_add_widget_buyout extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $code = 'buyout';

        $roles = [
            'business_analyst',
            'country.curator'
        ];

        $this->insert(WidgetType::tableName(), [
            'code' => $code,
            'name' => 'Выкупы',
            'status' => WidgetType::STATUS_ACTIVE,
            'by_country' => WidgetType::BY_ALL_COUNTRIES,
        ]);

        $type = WidgetType::find()
            ->where(['code' => $code])
            ->one();

        foreach ($roles as $role) {
            $this->insert(WidgetRole::tableName(), [
                'role' => $role,
                'type_id' => $type->id
            ]);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $code = 'buyout';
        $roles = [
            'business_analyst',
            'country.curator',
        ];

        $type = WidgetType::find()
            ->where(['code' => $code])
            ->one();

        foreach ($roles as $role) {
            $this->delete(WidgetRole::tableName(), [
                'role' => $role,
                'type_id' => $type->id
            ]);
        }

        $this->delete(WidgetType::tableName(), ['code' => $code]);
    }
}
