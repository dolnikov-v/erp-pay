<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m160915_062417_not_delivered_orders
 */
class m160915_062417_not_delivered_orders extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = new CrontabTask();
        $crontabTask->name = 'not_delivered_orders';
        $crontabTask->description = 'Нарушение сроков доставки заказов.';
        $crontabTask->save(false);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = CrontabTask::find()
            ->byName('not_delivered_orders')
            ->one();

        if ($crontabTask) {
            $crontabTask->delete();
        }
    }
}
