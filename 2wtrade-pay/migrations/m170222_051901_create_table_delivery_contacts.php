<?php

use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryContacts;
use app\modules\delivery\models\Delivery;

/**
 * Handles the creation for table `table_delivery_contacts`.
 */
class m170222_051901_create_table_delivery_contacts extends Migration
{
    const CASCADE = 'CASCADE';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

        $this->createTable(DeliveryContacts::tableName(), [
            'id' => $this->primaryKey(),
            'delivery_id' => $this->integer()->notNull(),
            'job_title' => $this->string(255)->notNull(),
            'full_name' => $this->string(255)->notNull(),
            'email' => $this->string(255)->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull()
        ], $tableOptions);

        $this->addForeignKey(null, DeliveryContacts::tableName(), 'delivery_id', Delivery::tableName(), 'id', self::CASCADE, self::CASCADE);

        $list = [
            'Должность',
            'Электронная почта',
            'Фамилия, имя, отчество',
            'Ответственные лица'
        ];

        foreach ($list as $phrase) {
            $this->insert('i18n_source_message', ['category' => 'common', 'message' => $phrase]);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable(DeliveryContacts::tableName());
    }
}
