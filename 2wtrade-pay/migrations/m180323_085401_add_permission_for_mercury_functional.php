<?php

use app\components\CustomMigration as Migration;
use app\models\AuthItem;

/**
 * Class m180323_085401_add_permission_for_mercury_functional
 */
class m180323_085401_add_permission_for_mercury_functional extends Migration
{
    public $translations = [
        'Справочники / Сторонние источники' => 'Directories / Third-party sources',
        'Справочники / Роли сторонних источников' => 'Directories / Third-party sources',
        'Справочники / Сторонние источники / Создание (Редактирование)' => 'Directories / Third-Party Sources / Creation (Editing)',
        'Справочники / Сторонние источники / Удаление' => 'Directories / Third-party sources / Removal',
        'Справочники / Роли сторонних источников / Создание (Редактирование)' => 'Directories / Third-party Roles / Creation (Editing)',
    ];

    public $permissions = [
        'catalog.externalsource.index' => 'Справочники / Сторонние источники',
        'catalog.externalsourceroles.index' => 'Справочники / Роли сторонних источников',
        'catalog.externalsource.edit' => 'Справочники / Сторонние источники / Создание (Редактирование)',
        'catalog.externalsource.delete' => 'Справочники / Сторонние источники / Удаление',
        'catalog.externalsourcerole.update' => 'Справочники / Роли сторонних источников / Создание (Редактирование)',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        foreach ($this->permissions as $name => $description) {
            $this->insert(AuthItem::tableName(), array(
                'name' => $name,
                'type' => '2',
                'description' => $description,
                'created_at' => time(),
                'updated_at' => time()
            ));
        }

        foreach($this->translations as $ruText => $enText) {
            $this->insert('i18n_source_message', ['category' => 'common', 'message' => $ruText]);
            $id = Yii::$app->db->getLastInsertID();
            $this->insert('i18n_message', ['id' => $id, 'language' => 'en-US', 'translation' => $enText]);
        }

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete(AuthItem::tableName(), ['name' => 'report.callcenteroperatorsonline.index']);

        foreach ($this->translations as $ruText => $enText) {
            $this->delete('i18n_source_message', ['category' => 'common', 'message' => $ruText]);
        }

    }
}
