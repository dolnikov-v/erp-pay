<?php
use app\components\CustomMigration as Migration;

/**
 * Class m180926_045039_remove_response_columns_from_request_tables
 */
class m180926_045039_remove_response_columns_from_request_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->execute("ALTER TABLE `delivery_request` DROP COLUMN `cs_send_response`, DROP COLUMN `last_update_info`");
        $this->execute("ALTER TABLE `call_center_request` DROP COLUMN `cc_send_response`, DROP COLUMN `cc_update_response`");
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->execute("ALTER TABLE `delivery_request` ADD COLUMN `cs_send_response` TEXT AFTER `partner_name`, `last_update_info` VARCHAR(2000) AFTER `foreign_info`");
        $this->execute("ALTER TABLE `call_center_request` ADD COLUMN `cc_send_response` TEXT AFTER `last_foreign_operator`, ADD COLUMN `cc_update_response` TEXT AFTER `cs_send_response_at`");
    }
}
