<?php

use yii\db\Migration;
use app\modules\storage\models\Storage;

/**
 * Handles adding columns to table `storage_table`.
 */
class m170124_081632_add_columns_to_storage_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn(Storage::tableName(),'sender_name', $this->string(100)->after('name'));
        $this->addColumn(Storage::tableName(),'sender_zip', $this->string(8)->after('sender_name'));
        $this->addColumn(Storage::tableName(),'sender_city', $this->string(64)->after('sender_zip'));
        $this->addColumn(Storage::tableName(),'sender_phone', $this->string(16)->after('sender_city'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn(Storage::tableName(),'sender_name');
        $this->dropColumn(Storage::tableName(),'sender_zip');
        $this->dropColumn(Storage::tableName(),'sender_city');
        $this->dropColumn(Storage::tableName(),'sender_phone');

    }
}
