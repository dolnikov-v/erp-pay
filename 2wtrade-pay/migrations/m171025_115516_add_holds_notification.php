<?php
use app\components\CustomMigration as Migration;
use app\models\Notification;
use app\modules\administration\models\CrontabTask;

/**
 * Class m171025_115516_add_holds_notification
 */
class m171025_115516_add_holds_notification extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $notification = new Notification([
            'description' => 'Холды: {text}',
            'trigger' => Notification::TRIGGER_REPORT_HOLDS_NOTIFICATION,
            'type' => Notification::TYPE_INFO,
            'group' => Notification::GROUP_REPORT,
            'active' => Notification::ACTIVE,
        ]);
        $notification->save();

        $crontabTask = new CrontabTask();
        $crontabTask->name = 'send_holds_notifications';
        $crontabTask->description = 'Уведомление о холдах (1, 2, 3, 4, 5 колонка)';
        $crontabTask->save();
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $notification = Notification::findOne(['trigger' => Notification::TRIGGER_REPORT_HOLDS_NOTIFICATION]);

        if ($notification instanceof Notification) {
            $notification->delete();
        }

        $crontabTask = CrontabTask::findOne(['name' => 'send_holds_notifications']);

        if ($crontabTask instanceof CrontabTask) {
            $crontabTask->delete();
        }
    }
}
