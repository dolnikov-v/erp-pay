<?php
use app\components\CustomMigration as Migration;

/**
 * Class m181016_075139_add_currencies_in_deliveryreport
 */
class m181016_075139_add_currencies_in_deliveryreport extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('delivery_report_currency', [
            'delivery_report_id' => $this->primaryKey(),
            'price_cod_currency_id' => $this->integer()->defaultValue(null),
            'price_storage_currency_id' => $this->integer()->defaultValue(null),
            'price_fulfilment_currency_id' => $this->integer()->defaultValue(null),
            'price_packing_currency_id' => $this->integer()->defaultValue(null),
            'price_package_currency_id' => $this->integer()->defaultValue(null),
            'price_delivery_currency_id' => $this->integer()->defaultValue(null),
            'price_redelivery_currency_id' => $this->integer()->defaultValue(null),
            'price_delivery_return_currency_id' => $this->integer()->defaultValue(null),
            'price_delivery_back_currency_id' => $this->integer()->defaultValue(null),
            'price_cod_service_currency_id' => $this->integer()->defaultValue(null),
            'price_vat_currency_id' => $this->integer()->defaultValue(null),
            'price_address_correction_currency_id' => $this->integer()->defaultValue(null),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addForeignKey(null, 'delivery_report_currency', 'delivery_report_id', 'delivery_report', 'id', self::CASCADE, self::CASCADE);
        $this->addForeignKey(null, 'delivery_report_currency', 'price_cod_currency_id', 'currency', 'id', self::CASCADE, self::CASCADE);
        $this->addForeignKey(null, 'delivery_report_currency', 'price_storage_currency_id', 'currency', 'id', self::CASCADE, self::CASCADE);
        $this->addForeignKey(null, 'delivery_report_currency', 'price_fulfilment_currency_id', 'currency', 'id', self::CASCADE, self::CASCADE);
        $this->addForeignKey(null, 'delivery_report_currency', 'price_packing_currency_id', 'currency', 'id', self::CASCADE, self::CASCADE);
        $this->addForeignKey(null, 'delivery_report_currency', 'price_delivery_currency_id', 'currency', 'id', self::CASCADE, self::CASCADE);
        $this->addForeignKey(null, 'delivery_report_currency', 'price_redelivery_currency_id', 'currency', 'id', self::CASCADE, self::CASCADE);
        $this->addForeignKey(null, 'delivery_report_currency', 'price_delivery_return_currency_id', 'currency', 'id', self::CASCADE, self::CASCADE);
        $this->addForeignKey(null, 'delivery_report_currency', 'price_delivery_back_currency_id', 'currency', 'id', self::CASCADE, self::CASCADE);
        $this->addForeignKey(null, 'delivery_report_currency', 'price_cod_service_currency_id', 'currency', 'id', self::CASCADE, self::CASCADE);
        $this->addForeignKey(null, 'delivery_report_currency', 'price_vat_currency_id', 'currency', 'id', self::CASCADE, self::CASCADE);
        $this->addForeignKey(null, 'delivery_report_currency', 'price_address_correction_currency_id', 'currency', 'id', self::CASCADE, self::CASCADE);

        $this->createTable('delivery_report_currency_rate', [
            'delivery_report_id' => $this->integer()->notNull(),
            'currency_id_from' => $this->integer()->notNull(),
            'currency_id_to' => $this->integer()->notNull(),
            'rate' => $this->decimal(10, 4)->defaultValue(null),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addPrimaryKey('pk_delivery_report_currency_rate', 'delivery_report_currency_rate', ['delivery_report_id', 'currency_id_from', 'currency_id_to']);
        $this->createIndex(null, 'delivery_report_currency_rate', 'delivery_report_id');
        $this->createIndex(null, 'delivery_report_currency_rate', 'currency_id_from');
        $this->createIndex(null, 'delivery_report_currency_rate', 'currency_id_to');
        $this->addForeignKey(null, 'delivery_report_currency_rate', 'delivery_report_id', 'delivery_report', 'id', self::CASCADE, self::CASCADE);
        $this->addForeignKey(null, 'delivery_report_currency_rate', 'currency_id_from', 'currency', 'id', self::CASCADE, self::CASCADE);
        $this->addForeignKey(null, 'delivery_report_currency_rate', 'currency_id_to', 'currency', 'id', self::CASCADE, self::CASCADE);

        $this->createTable('payment_currency_rate', [
            'payment_id' => $this->integer()->notNull(),
            'currency_id_from' => $this->integer()->notNull(),
            'currency_id_to' => $this->integer()->notNull(),
            'rate' => $this->decimal(10, 4)->defaultValue(null),
            'created_at' => $this->integer(),
        ]);

        $this->addPrimaryKey('pk_payment_currency_rate', 'payment_currency_rate', ['payment_id', 'currency_id_from', 'currency_id_to']);
        $this->createIndex(null, 'payment_currency_rate', 'payment_id');
        $this->createIndex(null, 'payment_currency_rate', 'currency_id_from');
        $this->createIndex(null, 'payment_currency_rate', 'currency_id_to');
        $this->addForeignKey(null, 'payment_currency_rate', 'payment_id', 'payment_order_delivery', 'id', self::CASCADE, self::CASCADE);
        $this->addForeignKey(null, 'payment_currency_rate', 'currency_id_from', 'currency', 'id', self::CASCADE, self::CASCADE);
        $this->addForeignKey(null, 'payment_currency_rate', 'currency_id_to', 'currency', 'id', self::CASCADE, self::CASCADE);

        $this->getDb()->createCommand('
            INSERT INTO payment_currency_rate (payment_id, currency_id_from, currency_id_to, rate, created_at) 
                SELECT pod.id as payment_id, pod.currency_id as currency_id_from, c.currency_id as currency_id_to, pod.rate, unix_timestamp() as created_at
                FROM payment_order_delivery pod
                INNER JOIN payment_delivery_report pdr on pdr.payment_id = pod.id
                INNER JOIN delivery_report dr on dr.id = pdr.delivery_report_id
                INNER JOIN country c on c.id = dr.country_id
                WHERE pod.currency_id is not null and pod.rate is not null
                GROUP BY pod.id, pod.currency_id, c.currency_id, pod.rate
        ')->execute();

        $this->getDb()->createCommand('
            UPDATE payment_order_delivery pod
                INNER JOIN payment_delivery_report pdr on pdr.payment_id = pod.id
                INNER JOIN delivery_report dr on dr.id = pdr.delivery_report_id
                INNER JOIN country on country.id = dr.country_id
            SET pod.currency_id = country.currency_id
        ')->execute();

        $this->dropColumn('payment_order_delivery', 'rate');

        $this->addColumn('payment_order_delivery', 'balance', $this->double()->defaultValue(0));
        $this->getDb()->createCommand('
            UPDATE `payment_order_delivery` SET balance = `sum`
        ')->execute();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('payment_order_delivery', 'balance');
        $this->addColumn('payment_order_delivery', 'rate', $this->decimal(10, 4)->defaultValue(null));
        $this->getDb()->createCommand('
            UPDATE payment_order_delivery pod
                INNER JOIN payment_currency_rate pcr on pcr.payment_id = pod.id and pcr.currency_id_from = pod.currency_id
                INNER JOIN payment_delivery_report pdr on pdr.payment_id = pod.id
                INNER JOIN delivery_report dr on dr.id = pdr.delivery_report_id
                INNER JOIN country c on c.id = dr.country_id and c.currency_id = pcr.currency_id_to
                SET pod.currency_id = pcr.currency_id_from, pod.rate = pcr.rate;
        ')->execute();
        $this->dropTable('payment_currency_rate');
        $this->dropTable('delivery_report_currency_rate');
        $this->dropTable('delivery_report_currency');
    }
}