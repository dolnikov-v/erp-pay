<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160714_083219_rename_table_api_lead_log
 */
class m160714_083219_rename_table_api_lead_log extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->renameTable('api_lead_log', 'api_log');
        $this->addColumn('api_log', 'type', 'ENUM ("lead", "callcenter") DEFAULT "lead" AFTER id');
        $this->createIndex(null, 'api_log', 'type');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropIndex($this->getIdxName('api_log', 'type'), 'api_log');
        $this->dropColumn('api_log', 'type');
        $this->renameTable('api_log', 'api_lead_log');
    }
}
