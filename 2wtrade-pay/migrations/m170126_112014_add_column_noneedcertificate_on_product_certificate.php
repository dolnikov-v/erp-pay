<?php
use app\components\CustomMigration as Migration;
use app\models\Certificate;

/**
 * Class m170126_112014_add_column_noneedcertificate_on_product_certificate
 */
class m170126_112014_add_column_noneedcertificate_on_product_certificate extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Certificate::tableName(), 'certificate_not_needed', 'bool AFTER is_partner');
        $this->insert('i18n_source_message', ['category' => 'common', 'message' => 'Сертификат не нужен']);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('certificate_not_needed');
    }
}
