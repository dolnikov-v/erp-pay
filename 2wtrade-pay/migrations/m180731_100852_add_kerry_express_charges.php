<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryChargesCalculator;

/**
 * Class m180731_100852_add_kerry_express_charges
 */
class m180731_100852_add_kerry_express_charges extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $model = new DeliveryChargesCalculator();
        $model->name = 'KerryExpress';
        $model->active = 1;
        $model->class_path = 'app\modules\delivery\components\charges\kerryexpress\KerryExpress';
        $model->save();

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $model = DeliveryChargesCalculator::find()->where(['name' => 'KerryExpress'])->one();
        if ($model) {
            $model->delete();
        }
    }
}
