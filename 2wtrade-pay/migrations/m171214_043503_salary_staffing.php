<?php
use app\components\PermissionMigration as Migration;
use app\modules\salary\models\Office;
use app\modules\salary\models\Designation;


/**
 * Class m171214_043503_salary_staffing
 */
class m171214_043503_salary_staffing extends Migration
{

    protected $permissions = [
        'salary.staffing.index',
        'salary.staffing.edit',
        'salary.staffing.delete',
    ];

    protected $roles = [
        'project.manager' => [
            'salary.staffing.index',
            'salary.staffing.edit',
            'salary.staffing.delete',
        ],
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        $this->addColumn(Office::tableName(), 'jobs_number', $this->integer());

        $this->createTable('salary_staffing', [
            'id' => $this->primaryKey(),
            'office_id' => $this->integer()->notNull(),
            'designation_id' => $this->integer()->notNull(),
            'max_persons' => $this->integer(),
            'min_jobs' => $this->integer(),
            'salary_usd' => $this->double(),
            'salary_local' => $this->double(),
            'comment' => $this->string(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $this->tableOptions);

        $this->addForeignKey(null, 'salary_staffing', 'office_id', Office::tableName(), 'id', self::CASCADE, self::NO_ACTION);
        $this->addForeignKey(null, 'salary_staffing', 'designation_id', Designation::tableName(), 'id', self::CASCADE, self::NO_ACTION);

        parent::safeUp();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Office::tableName(), 'jobs_number');

        $this->dropTable('salary_staffing');

        parent::safeDown();
    }
}
