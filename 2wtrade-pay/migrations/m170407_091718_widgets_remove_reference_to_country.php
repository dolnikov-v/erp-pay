<?php
use app\components\CustomMigration as Migration;
use app\modules\widget\models\WidgetType;
/**
 * Class m170407_091718_widgets_remove_reference_to_country
 */
class m170407_091718_widgets_remove_reference_to_country extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $models = WidgetType::find()
            ->where([
                'code' => [
                    'leads_chart',
                    'average_bill_of_approves_chart',
                    'percent_of_approves_chart',
                ]
            ])
            ->all();

        foreach ($models as $model) {
            /** @var WidgetType $model */
            $model->by_country = WidgetType::BY_ALL_COUNTRIES;
            $model->save();
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

    }
}
