<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryApiClass;

/**
 * Class m161117_102718_add_api_class_nex
 */
class m161117_102718_add_api_class_nex extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $class = new DeliveryApiClass();
        $class->name = 'nexApi';
        $class->class_path = '/nex-api/src/nexApi.php';
        $class->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        DeliveryApiClass::find()
            ->where([
                'name' => 'nexApi',
                'class_path' => '/nex-api/src/nexApi.php'
            ])
            ->one()
            ->delete();
    }
}
