<?php

use app\components\CustomMigration;
use app\modules\administration\models\CrontabTask;
use app\models\Notification;

/**
 * Class m161101_035620_create_report_cc_table
 */
class m161101_035620_create_report_cc_table extends CustomMigration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('report_call_center', [
            'id' => $this->primaryKey(),
            'country_id' => $this->integer(11)->notNull(),
            'type' => "ENUM ('hourly', 'daily', 'weekly', 'monthly') NOT NULL DEFAULT 'daily'",
            'from' => $this->integer(11)->notNull(),
            'to' => $this->integer(11)->notNull(),
            'cc_post_call_count' => $this->integer(11)->defaultValue(0),
            'cc_approved_count' => $this->integer(11)->defaultValue(0),
            'cc_fail_call_count' => $this->integer(11)->defaultValue(0),
            'cc_recall_count' => $this->integer(11)->defaultValue(0),
            'cc_rejected_count' => $this->integer(11)->defaultValue(0),
            'cc_double_count' => $this->integer(11)->defaultValue(0),
            'cc_trash_count' => $this->integer(11)->defaultValue(0),
            'our_post_call_count' => $this->integer(11)->defaultValue(0),
            'our_approved_count' => $this->integer(11)->defaultValue(0),
            'our_fail_call_count' => $this->integer(11)->defaultValue(0),
            'our_recall_count' => $this->integer(11)->defaultValue(0),
            'our_rejected_count' => $this->integer(11)->defaultValue(0),
            'our_double_count' => $this->integer(11)->defaultValue(0),
            'our_trash_count' => $this->integer(11)->defaultValue(0),
            'difference_count' => $this->integer(11)->defaultValue(0),
            'difference' => $this->text(),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11),
        ], $this->tableOptions);

        $this->addForeignKey(null, 'report_call_center', 'country_id', 'country', 'id', self::NO_ACTION, self::NO_ACTION);

        $crontabTask = new CrontabTask();
        $crontabTask->name = 'report_compare_with_call_center';
        $crontabTask->description = 'Сверка с КЦ';
        $crontabTask->save();

        $notification = new Notification([
            'description' => 'Сверка с КЦ {from} - {to}: Всего {ccCount}|{ourCount}, Обзвон {ccP}|{ourP}, Одобрено {ccApprove}|{ourApprove}, Недозвон {ccFail}|{ourFail}, Перезвон {ccRecall}|{ourRecall}, Отклонен {ccCancel}|{ourCancel}, Дубль {ccDouble}|{ourDouble}, Треш {ccT}|{ourT}',
            'trigger' => 'report.call-center-compare.info',
            'type' => Notification::TYPE_INFO,
            'group' => Notification::GROUP_REPORT,
            'active' => Notification::ACTIVE
        ]);

        $notification->save(false);

        $notification = new Notification([
            'description' => 'Сверка с КЦ {from} - {to}: Всего в КЦ {ccCount}, Всего у нас {ourCount}, Имеются различия {differenceCount}',
            'trigger' => 'report.call-center-compare.danger',
            'type' => Notification::TYPE_DANGER,
            'group' => Notification::GROUP_REPORT,
            'active' => Notification::ACTIVE
        ]);

        $notification->save(false);

        $this->createTable('call_center_full_request', [
            'id' => $this->primaryKey(),
            'call_center_id' => $this->integer(11)->notNull(),
            'order_id' => $this->integer(11),
            'foreign_id' => $this->integer(11)->notNull(),
            'status' => $this->integer(11),
            'sub_status' => $this->integer(11),
            'name' => $this->string(255),
            'phone' => $this->string(100),
            'address' => $this->text(),
            'address_components' => $this->text(),
            'products' => $this->text(),
            'shipping' => $this->text(),
            'history' => $this->text(),
            'delivery' => $this->double(),
            'comment' => $this->text(),
            'last_update' => $this->integer(11),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11)
        ], $this->tableOptions);

        $this->addForeignKey(null, 'call_center_full_request', 'call_center_id', 'call_center', 'id', self::NO_ACTION,
            self::NO_ACTION);

        $this->addForeignKey(null, 'call_center_full_request', 'order_id', 'order', 'id', self::NO_ACTION,
            self::NO_ACTION);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = CrontabTask::find()->byName('report_compare_with_call_center')->one();

        if ($crontabTask) {
            $crontabTask->delete();
        }

        $notifications = Notification::find()->where([
            'trigger' => [
                'report.call-center-compare.info',
                'report.call-center-compare.danger'
            ]
        ])->all();

        foreach ($notifications as $notification) {
            $notification->delete();
        }

        $this->dropTable('report_call_center');
        $this->dropTable('call_center_full_request');
    }
}
