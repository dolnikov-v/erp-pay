<?php
use app\components\CustomMigration as Migration;
use yii\db\Query;

/**
 * Class m170928_122201_add_permissions_for_order
 */
class m170928_122201_add_permissions_for_order extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}', [
            'name' => 'order.index.group.ignoreworkflow',
            'type' => '2',
            'description' => 'order.index.group.ignoreworkflow',
            'created_at' => time(),
            'updated_at' => time()
        ]);
        $this->insert('{{%auth_item}}', [
            'name' => 'order.index.group.changedeliveryid',
            'type' => '2',
            'description' => 'order.index.group.changedeliveryid',
            'created_at' => time(),
            'updated_at' => time()
        ]);

        $role = 'deliveryreport.clerk';
        $query = new Query();

        $is_role = $query->select('*')->from($this->authManager->itemTable)
            ->where(['name' => $role, 'type' => 1])
            ->one();

        if (!$is_role) {
            $this->insert($this->authManager->itemTable, [
                'name' => $role,
                'type' => 1,
                'description' => $role,
                'created_at' => time(),
                'updated_at' => time()]);
        }

        $this->insert('{{%auth_item_child}}', [
            'parent' => $role,
            'child' => 'order.index.group.changedeliveryid',
        ]);
        $this->insert('{{%auth_item_child}}', [
            'parent' => $role,
            'child' => 'order.index.group.ignoreworkflow',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete('{{%auth_item}}', ['name' => 'order.index.group.ignoreworkflow']);
        $this->delete('{{%auth_item}}', ['name' => 'order.index.group.changedeliveryid']);
    }
}
