<?php
use app\components\CustomMigration as Migration;
use app\models\User;
use yii\db\Query;

/**
 * Class m171019_065125_check_list
 */
class m171019_065125_check_list extends Migration
{

    const RULES = [
        'checklist.check.index',
    ];

    const ROLES = [
        'controller.analyst',
        'finance.director',
        'country.curator',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(User::tableName(), 'parent_id', $this->integer());
        $this->addForeignKey('fk_user_parent_id', User::tableName(), 'parent_id', User::tableName(), 'id', self::SET_NULL, self::NO_ACTION);

        $this->createTable('check_list_item', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'description' => $this->text(),
            'position' => $this->integer(),
            'days' => $this->integer()->defaultValue(1),
            'active' => $this->integer(1)->defaultValue(0),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $this->tableOptions);

        $this->createTable('check_list', [
            'id' => $this->primaryKey(),
            'check_list_item_id' => $this->integer(),
            'country_id' => $this->integer(),
            'user_id' => $this->integer(),
            'date' => $this->date(),
            'status' => $this->string(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $this->tableOptions);

        $this->addForeignKey(null, 'check_list', 'check_list_item_id', 'check_list_item', 'id', self::CASCADE, self::NO_ACTION);
        $this->addForeignKey(null, 'check_list', 'country_id', 'country', 'id', self::CASCADE, self::NO_ACTION);
        $this->addForeignKey(null, 'check_list', 'user_id', 'user', 'id', self::SET_NULL, self::NO_ACTION);

        $data = [
            1 => 'Отсутствуют заказы, полученные из Adcombo и не переданные на прозвон в КЦ',
            2 => 'Все лиды (в т.ч. сегодняшние) прозвонены и средний чек аппрувов выше 65 USD. Аппрув от свежих лидов не менее XX %',
            3 => 'Все аппрувы отправлены в КС (Отсутствуют заказы в очереди на отправку, а также нет заказов с неисправленной ошибкой отправки в КС)',
            4 => 'КС имеет достаточное количество товаров, чтобы дотянуть до следующей поставки товаров при текущем траффике.',
            5 => 'Получены обновления статусов от каждой курьерки, у которой есть заказы в работе.',
            6 => 'Выкуп за последние 7 дней больше аналогичного показателя 24 часа назад, если меньше то дано объяснение.',
            7 => 'Долг каждой курьерки по COD не более месяца и не более 10 000 USD.',
            8 => 'Все возвраты за последние 3 дня прозвонены КЦ.',
            9 => 'Есть сертификаты на все товары продаваемые в стране.',
            10 => 'С момента последней инвентаризации склада каждой КС прошло не более 7 дней.',
        ];

        $arrayForInsert = [];
        foreach ($data as $k => $n) {
            $arrayForInsert[] = [
                'position' => $k,
                'name' => $n,
                'active' => 1,
                'created_at' => time(),
                'updated_at' => time(),
            ];
        }

        Yii::$app->db->createCommand()->batchInsert(
            'check_list_item',
            [
                'position',
                'name',
                'active',
                'created_at',
                'updated_at',
            ],
            $arrayForInsert
        )->execute();

        $query = new Query();

        foreach (self::RULES as $rule) {

            $is_rule = $query->select('*')->from($this->authManager->itemTable)
                ->where(['name' => $rule, 'type' => 2])
                ->one();

            if (!$is_rule) {
                $this->insert($this->authManager->itemTable, [
                    'name' => $rule,
                    'description' => $rule,
                    'type' => 2,
                    'created_at' => time(),
                    'updated_at' => time()]);
            }

            foreach (self::ROLES as $role) {

                $is_role = $query->select('*')->from($this->authManager->itemTable)
                    ->where(['name' => $role, 'type' => 1])
                    ->one();

                if (!$is_role) {
                    $this->insert($this->authManager->itemTable, [
                        'name' => $role,
                        'type' => 1,
                        'description' => $role,
                        'created_at' => time(),
                        'updated_at' => time()]);
                }

                $is_can = $query->select('*')->from($this->authManager->itemChildTable)->where([
                    'parent' => $role,
                    'child' => $rule
                ])->one();

                if (!$is_can) {
                    $this->insert($this->authManager->itemChildTable, [
                        'parent' => $role,
                        'child' => $rule
                    ]);
                }
            }
        }

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('check_list');
        $this->dropTable('check_list_item');

        $this->dropForeignKey('fk_user_parent_id', User::tableName());
        $this->dropColumn(User::tableName(), 'parent_id');

        foreach (self::RULES as $rule) {
            foreach (self::ROLES as $role) {
                $this->delete($this->authManager->itemChildTable, [
                    'parent' => $role,
                    'child' => $rule
                ]);
            }
        }
    }
}
