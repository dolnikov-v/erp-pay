<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryApiClass;

/**
 * Class m160714_103021_fix_delivery_request
 */
class m160714_103021_fix_delivery_request extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropIndex($this->getIdxName('delivery_request', 'foreign_id'), 'delivery_request');

        $this->dropColumn('delivery_request', 'foreign_id');
        $this->dropColumn('delivery_request', 'foreign_status');

        $this->addColumn('delivery_request', 'foreign_info', $this->text()->defaultValue(null)->after('tracking'));
        $this->addColumn('delivery_request', 'api_error', $this->string(255)->defaultValue(null)->after('comment'));

        foreach ($this->getFixture() as $data) {
            $apiClass = new DeliveryApiClass($data);
            $apiClass->save();
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach ($this->getFixture() as $data) {
            $apiClass = DeliveryApiClass::find()->where([
                'name' => $data['name'],
            ])->one();

            if ($apiClass) {
                $apiClass->delete();
            }
        }

        $this->dropColumn('delivery_request', 'foreign_info');
        $this->dropColumn('delivery_request', 'api_error');

        $this->addColumn('delivery_request', 'foreign_status', $this->string(30)->defaultValue(null)->after('status'));
        $this->addColumn('delivery_request', 'foreign_id', $this->integer()->defaultValue(null)->after('id'));

        $this->createIndex(null, 'delivery_request', 'foreign_id');
    }

    /**
     * @return array
     */
    private function getFixture()
    {
        return [
            [
                'name' => 'BizApi',
                'class_path' => '/biz-api/src/BizApi.php',
                'active' => 1,
            ],
        ];
    }
}
