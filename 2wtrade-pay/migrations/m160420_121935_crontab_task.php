<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m160420_121935_crontab_task
 */
class m160420_121935_crontab_task extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('crontab_task', [
            'id' => $this->primaryKey(),
            'name' => $this->string(256)->notNull(),
            'description' => $this->string(256)->notNull(),
            'active' => $this->smallInteger()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $crontabTask = new CrontabTask();
        $crontabTask->name = 'clean_user_action_log';
        $crontabTask->description = 'Очистка истории посещений пользователей.';
        $crontabTask->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('crontab_task');
    }
}
