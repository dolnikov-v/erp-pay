<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\Delivery;
/**
 * Class m180719_034704_add_column_delivery_group
 */
class m180719_034704_add_column_delivery_group extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Delivery::tableName(), 'group_name', $this->string()->after('name'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Delivery::tableName(), 'group_name');
    }
}
