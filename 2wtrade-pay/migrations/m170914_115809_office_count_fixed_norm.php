<?php
use app\components\CustomMigration as Migration;
use app\modules\salary\models\Office;

/**
 * Class m170914_115809_office_count_fixed_norm
 */
class m170914_115809_office_count_fixed_norm extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Office::tableName(), 'count_fixed_norm', $this->integer(1)->defaultValue(0));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Office::tableName(), 'count_fixed_norm');
    }
}
