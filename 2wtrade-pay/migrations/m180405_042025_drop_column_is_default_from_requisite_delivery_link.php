<?php
use app\modules\catalog\models\RequisiteDeliveryLink;

use yii\db\Migration;

/**
 * Handles dropping column_is_default from table `requisite_delivery_link`.
 */
class m180405_042025_drop_column_is_default_from_requisite_delivery_link extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropColumn(RequisiteDeliveryLink::tableName(), 'is_default');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->addColumn(RequisiteDeliveryLink::tableName(), 'is_default', $this->integer());
    }
}
