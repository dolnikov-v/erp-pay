<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryRequest;
use yii\db\Query;

/**
 * Class m170420_054455_second_delivery_attempt
 */
class m170420_054455_second_delivery_attempt extends Migration
{
    const ROLES = [
        'business_analyst',
        'callcenter.manager',
        'country.curator',
        'project.manager',
    ];

    const RULES = [
        'order.index.seconddeliveryattempt'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(DeliveryRequest::tableName(), 'second_sent_at', $this->integer()->after('sent_at'));


        $query = new Query();

        foreach (self::ROLES as $role) {
            $is_role = $query->select('*')->from($this->authManager->itemTable)
                ->where(['name' => $role, 'type' => 1])
                ->one();

            if (!$is_role) {
                $this->insert($this->authManager->itemTable, [
                    'name' => $role,
                    'type' => 1,
                    'description' => $role,
                    'created_at' => time(),
                    'updated_at' => time()]);
            }

            foreach (self::RULES as $rule) {

                $is_rule = $query->select('*')->from($this->authManager->itemTable)
                    ->where(['name' => $rule, 'type' => 2])
                    ->one();

                if (!$is_rule) {
                    $this->insert($this->authManager->itemTable, [
                        'name' => $rule,
                        'description' => $rule,
                        'type' => 2,
                        'created_at' => time(),
                        'updated_at' => time()]);
                }

                $is_can = $query->select('*')->from($this->authManager->itemChildTable)->where([
                    'parent' => $role,
                    'child' => $rule
                ])->one();

                if (!$is_can) {
                    $this->insert($this->authManager->itemChildTable, [
                        'parent' => $role,
                        'child' => $rule
                    ]);
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(DeliveryRequest::tableName(), 'second_sent_at');

        foreach (self::ROLES as $role) {
            foreach (self::RULES as $rule) {
                $this->delete($this->authManager->itemChildTable, [
                    'parent' => $role,
                    'child' => $rule
                ]);
            }
        }
    }
}
