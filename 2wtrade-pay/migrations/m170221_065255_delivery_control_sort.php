<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\Delivery;

/**
 * Class m170221_065255_delivery_control_sort
 */
class m170221_065255_delivery_control_sort extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Delivery::tableName(), 'sort_order', $this->integer());
        $this->execute("update " . Delivery::tableName(). " set sort_order = id ");
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Delivery::tableName(), 'sort_order');
    }
}
