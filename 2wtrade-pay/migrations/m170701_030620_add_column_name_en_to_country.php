<?php

use app\components\CustomMigration;
use app\models\Country;

/**
 * Handles adding column name_en to table `country`.
 */
class m170701_030620_add_column_name_en_to_country extends CustomMigration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('country', 'name_en', $this->string(255)->after('name'));

        $counties = Country::find()->all();
        foreach ($counties as $country) {
            $country->save();
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('country', 'name_en');
    }
}
