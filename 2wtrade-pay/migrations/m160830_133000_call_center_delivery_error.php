<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160830_133000_call_center_delivery_error
 */
class m160830_133000_call_center_delivery_error extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('call_center_request', 'delivery_error', $this->string(255)->defaultValue(null)->after('api_error'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('call_center_request', 'delivery_error');
    }
}
