<?php
use app\components\CustomMigration as Migration;

/**
 * Class m181009_043620_add_delivered_from_to_delivery
 */
class m181009_043620_add_delivered_from_to_delivery extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('delivery', 'delivered_from', $this->integer(11)->defaultValue(null)->after('workflow_id'));
        $this->addColumn('delivery', 'delivered_to', $this->integer(11)->defaultValue(null)->after('delivered_from'));

        $sql = <<<SQL
      UPDATE delivery left join delivery_contract on delivery_contract.delivery_id = delivery.id 
      SET 
      delivery.delivered_to = delivery_contract.delivered_to, 
      delivery.delivered_from = delivery_contract.delivered_from
      ;
SQL;
        $this->execute($sql);


        $sql = 'DROP FUNCTION IF EXISTS is_bad_delivery_request_sent_at;';
        $this->db->createCommand($sql)->execute();

        $sqlFunction = <<<SQL
CREATE FUNCTION `is_bad_delivery_request_sent_at`(
  `v_delivery_request_id` INTEGER,
  `v_zip` VARCHAR(50),
  `v_city` VARCHAR(50),
  `v_province` VARCHAR(50)
)
RETURNS tinyint(4)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT ''        
BEGIN
    DECLARE v_contract_id INTEGER DEFAULT NULL;
    DECLARE v_delivered_to INTEGER DEFAULT NULL;
    DECLARE v_sent_at INTEGER DEFAULT NULL;
    DECLARE is_bad_time BOOL DEFAULT FALSE;
      SELECT 
     `delivery_contract`.`id`, 
    IFNULL(`delivery_contract`.`delivered_to`,`delivery`.`delivered_to`),
    `delivery_request`.sent_at
    INTO 
    v_contract_id, 
    v_delivered_to,
      v_sent_at
      FROM `delivery_request`
      LEFT JOIN `delivery` ON `delivery_request`.delivery_id = `delivery`.id 
      LEFT JOIN `delivery_contract` ON `delivery_contract`.delivery_id = `delivery`.id
      WHERE 
      `delivery_request`.id = v_delivery_request_id 
         AND CASE 
         WHEN `delivery_contract`.date_from IS NOT NULL AND `delivery_contract`.date_to IS NOT NULL
         THEN DATE_FORMAT(FROM_UNIXTIME(`delivery_request`.sent_at), '%Y-%m-%d') >= `delivery_contract`.date_from AND DATE_FORMAT(FROM_UNIXTIME(`delivery_request`.sent_at), '%Y-%m-%d') <= `delivery_contract`.date_to
         WHEN `delivery_contract`.date_from IS NOT NULL AND `delivery_contract`.date_to IS NULL
         THEN DATE_FORMAT(FROM_UNIXTIME(`delivery_request`.sent_at), '%Y-%m-%d') >= `delivery_contract`.date_from
         WHEN `delivery_contract`.date_from IS NULL AND `delivery_contract`.date_to IS NOT NULL
         THEN DATE_FORMAT(FROM_UNIXTIME(`delivery_request`.sent_at), '%Y-%m-%d') <= `delivery_contract`.date_to 
         ELSE 1 END              
      LIMIT 1;
      
      IF (ISNULL(v_contract_id)) THEN
      RETURN is_bad_time;
    END IF;  
            
      SELECT 
    IFNULL(MAX(`delivery_contract_time`.delivered_to), v_delivered_to) 
      INTO v_delivered_to
      FROM 
      `delivery_contract_time`
      WHERE `delivery_contract_time`.contract_id = v_contract_id
      AND 
      ( 
        (`delivery_contract_time`.field_name = 'customer_zip' and `delivery_contract_time`.field_value = IFNULL(v_zip, '') COLLATE utf8_unicode_ci) OR 
        (`delivery_contract_time`.field_name = 'customer_city' and `delivery_contract_time`.field_value = IFNULL(v_city, '') COLLATE utf8_unicode_ci) OR
        (`delivery_contract_time`.field_name = 'customer_province' and `delivery_contract_time`.field_value = IFNULL(v_province, '') COLLATE utf8_unicode_ci)
      );         
        
     IF (v_sent_at + 86400 * v_delivered_to < CURRENT_TIMESTAMP()) THEN
      SET is_bad_time = TRUE;
    END IF;  
    RETURN is_bad_time;
  END
SQL;

        $this->execute($sqlFunction);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('delivery', 'delivered_from');
        $this->dropColumn('delivery', 'delivered_to');

        $sql = 'DROP FUNCTION IF EXISTS is_bad_delivery_request_sent_at;';
        $this->db->createCommand($sql)->execute();

        $sqlFunction = <<<SQL
CREATE FUNCTION `is_bad_delivery_request_sent_at`(
  `v_delivery_request_id` INTEGER,
  `v_zip` VARCHAR(50),
  `v_city` VARCHAR(50),
  `v_province` VARCHAR(50)
)
RETURNS tinyint(4)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT ''        
BEGIN
    DECLARE v_contract_id INTEGER DEFAULT NULL;
    DECLARE v_delivered_to INTEGER DEFAULT NULL;
    DECLARE v_sent_at INTEGER DEFAULT NULL;
    DECLARE is_bad_time BOOL DEFAULT FALSE;
      SELECT 
     `delivery_contract`.`id`, 
    `delivery_contract`.`delivered_to`,
    `delivery_request`.sent_at
    INTO 
    v_contract_id, 
    v_delivered_to,
      v_sent_at
      FROM `delivery_request`
      LEFT JOIN `delivery` ON `delivery_request`.delivery_id = `delivery`.id 
      LEFT JOIN `delivery_contract` ON `delivery_contract`.delivery_id = `delivery`.id
      WHERE 
      `delivery_request`.id = v_delivery_request_id 
         AND CASE 
         WHEN `delivery_contract`.date_from IS NOT NULL AND `delivery_contract`.date_to IS NOT NULL
         THEN DATE_FORMAT(FROM_UNIXTIME(`delivery_request`.sent_at), '%Y-%m-%d') >= `delivery_contract`.date_from AND DATE_FORMAT(FROM_UNIXTIME(`delivery_request`.sent_at), '%Y-%m-%d') <= `delivery_contract`.date_to
         WHEN `delivery_contract`.date_from IS NOT NULL AND `delivery_contract`.date_to IS NULL
         THEN DATE_FORMAT(FROM_UNIXTIME(`delivery_request`.sent_at), '%Y-%m-%d') >= `delivery_contract`.date_from
         WHEN `delivery_contract`.date_from IS NULL AND `delivery_contract`.date_to IS NOT NULL
         THEN DATE_FORMAT(FROM_UNIXTIME(`delivery_request`.sent_at), '%Y-%m-%d') <= `delivery_contract`.date_to 
         ELSE 1 END              
      LIMIT 1;
      
      IF (ISNULL(v_contract_id)) THEN
      RETURN is_bad_time;
    END IF;  
            
      SELECT 
    IFNULL(MAX(`delivery_contract_time`.delivered_to), v_delivered_to) 
      INTO v_delivered_to
      FROM 
      `delivery_contract_time`
      WHERE `delivery_contract_time`.contract_id = v_contract_id
      AND 
      ( 
        (`delivery_contract_time`.field_name = 'customer_zip' and `delivery_contract_time`.field_value = v_zip COLLATE utf8_unicode_ci) OR 
        (`delivery_contract_time`.field_name = 'customer_city' and `delivery_contract_time`.field_value = v_city COLLATE utf8_unicode_ci) OR
        (`delivery_contract_time`.field_name = 'customer_province' and `delivery_contract_time`.field_value = v_province COLLATE utf8_unicode_ci)
      );         
        
     IF (v_sent_at + 86400 * v_delivered_to < CURRENT_TIMESTAMP()) THEN
      SET is_bad_time = TRUE;
    END IF;  
    RETURN is_bad_time;
  END
SQL;

        $this->execute($sqlFunction);
    }
}
