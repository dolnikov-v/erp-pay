<?php
use app\components\CustomMigration as Migration;
use app\modules\finance\models\ReportExpensesCountryItem;

/**
 * Class m180503_043618_report_expenses_country_item_dates
 */
class m180503_043618_report_expenses_country_item_dates extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(ReportExpensesCountryItem::tableName(), 'date_from', $this->date()->after('country_id'));
        $this->addColumn(ReportExpensesCountryItem::tableName(), 'date_to', $this->date()->after('date_from'));
        $this->dropColumn(ReportExpensesCountryItem::tableName(), 'month');
        $this->dropColumn(ReportExpensesCountryItem::tableName(), 'year');

        $this->update(ReportExpensesCountryItem::tableName(), [
            'date_from' => date('Y-m-01'),
            'date_to' => date('Y-m-t'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(ReportExpensesCountryItem::tableName(), 'date_from');
        $this->dropColumn(ReportExpensesCountryItem::tableName(), 'date_to');
        $this->addColumn(ReportExpensesCountryItem::tableName(), 'month', $this->date()->after('country_id'));
        $this->addColumn(ReportExpensesCountryItem::tableName(), 'year', $this->date()->after('month'));
    }
}
