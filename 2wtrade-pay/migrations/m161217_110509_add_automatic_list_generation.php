<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\Delivery;
use app\modules\administration\models\CrontabTask;
use app\modules\order\models\OrderLogisticListExcel;

/**
 * Class m161217_110509_add_automatic_list_generation
 */
class m161217_110509_add_automatic_list_generation extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Delivery::tableName(), 'auto_list_generation', $this->smallInteger(6)->defaultValue(0));
        $this->addColumn(Delivery::tableName(), 'list_send_to_emails', $this->text());
        $this->addColumn(OrderLogisticListExcel::tableName(), 'waiting_for_send', $this->smallInteger(6)->defaultValue(0));

        $cron = new CrontabTask();
        $cron->name = 'delivery_generate_lists';
        $cron->description = 'Автоматическая генерация и отправка листов';
        $cron->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Delivery::tableName(), 'auto_list_generation');
        $this->dropColumn(Delivery::tableName(), 'list_send_to_emails');
        $this->dropColumn(OrderLogisticListExcel::tableName(), 'waiting_for_send');

        $crontabTask = CrontabTask::find()
            ->byName('delivery_generate_lists')
            ->one();

        if ($crontabTask) {
            $crontabTask->delete();
        }
    }
}
