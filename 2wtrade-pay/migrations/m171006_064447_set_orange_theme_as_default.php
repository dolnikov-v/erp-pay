<?php
use yii\db\Migration;

/**
 * Class m171006_064447_set_orange_theme_as_default
 */
class m171006_064447_set_orange_theme_as_default extends Migration
{

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->update('user', ['theme' => 'basic']);
        $this->update('user', ['skin' => 'orange']);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->update('user', ['theme' => 'basic']);
        $this->update('user', ['skin' => 'basic']);
    }
}
