<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m170130_095959_add_cron_get_orders_info_for_lists
 */
class m170130_095959_add_cron_get_orders_info_for_lists extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = new CrontabTask();
        $crontabTask->name = 'delivery_get_orders_info_for_lists';
        $crontabTask->description = 'Получение статусов из службы доставки для заказов из листов';
        $crontabTask->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = CrontabTask::find()
            ->byName('delivery_get_orders_info_for_lists')
            ->one();

        if ($crontabTask) {
            $crontabTask->delete();
        }
    }
}
