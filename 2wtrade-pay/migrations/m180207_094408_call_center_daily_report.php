<?php
use app\components\CustomMigration as Migration;
use app\models\Notification;
use app\modules\administration\models\CrontabTask;
use yii\db\Query;

/**
 * Class m180207_094408_call_center_daily_report
 */
class m180207_094408_call_center_daily_report extends Migration
{

    const TEXT = [
        'Отчет работы' => 'Work report',
        'Данные за период' => 'Data for the period',
        'Сегодня режим работы КЦ' => 'Working hours of the Call Center today',
        'Операторов онлайн' => 'Operators online',
        'Новых лидов сегодня по направлениям:' => 'New leads today in the directions:',
        'Общее кол-во товаров: {products};' => 'Total number of products: {products};',
        'Всего по направлениям в холде: {totalHold};' => 'Total for directions in the hold: {totalHold};',
        '{all} (норма {allNorma}) из них апрув {approve} ({approvePercent}%) при норме {approvePercentNorma}%, холд {hold} ({holdPercent}%), треш {trash} ({trashPercent}%)' =>
            '{all} (norm {allNorma}) from them approve {approve} ({approvePercent}%) at the norm {approvePercentNorma}%, hold {hold} ({holdPercent}%), trash {trash} ({trashPercent}%)',
    ];


    public function safeUp()
    {
        $notification = Notification::findOne(['trigger' => Notification::TRIGGER_CALL_CENTER_DAILY_REPORT]);
        if (!$notification instanceof Notification) {
            $notification = new Notification();
        }

        $notification->description = 'Отчет работы {officeName}: {text}';
        $notification->trigger = Notification::TRIGGER_CALL_CENTER_DAILY_REPORT;
        $notification->type = Notification::TYPE_INFO;
        $notification->group = Notification::GROUP_SYSTEM;
        $notification->active = Notification::ACTIVE;
        $notification->save(false);

        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_CALL_CENTER_DAILY_REPORT]);
        if (!$crontabTask) {
            $crontabTask = new CrontabTask();
            $crontabTask->name = CrontabTask::TASK_CALL_CENTER_DAILY_REPORT;
            $crontabTask->description = "Уведомление отчет работы КЦ";
            $crontabTask->save();
        }

        $query = new Query();

        foreach ($this::TEXT as $ruText => $enText) {

            $is = $query->select('id')
                ->from('i18n_source_message')
                ->where(['category' => 'common', 'message' => $ruText])->one();

            if (!$is) {
                $this->insert('i18n_source_message', ['category' => 'common', 'message' => $ruText]);
                $id = Yii::$app->db->getLastInsertID();
                $this->insert('i18n_message', ['id' => $id, 'language' => 'en-US', 'translation' => $enText]);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $notification = Notification::findOne(['trigger' => Notification::TRIGGER_CALL_CENTER_DAILY_REPORT]);
        $notification->delete();
        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_CALL_CENTER_DAILY_REPORT]);
        $crontabTask->delete();

        foreach ($this::TEXT as $ruText => $enText) {
            $this->delete('i18n_source_message', ['category' => 'common', 'message' => $ruText]);
        }
    }
}
