<?php

use app\components\PermissionMigration as Migration;

/**
 * Class m190322_083108_order_notification_log
 */
class m190322_083108_order_notification_log extends Migration
{
    protected $permissions = ['order.notification.log'];

    protected $roleList = [
        'admin',
        'business_analyst',
        'it_controller',
        'operational.director',
        'project.manager',
        'technical.director',
    ];
}
