<?php
use app\components\CustomMigration as Migration;
use app\modules\salary\models\Office;
use app\models\Country;
use app\modules\callcenter\models\CallCenter;

/**
 * Class m170614_104256_call_center_office
 */
class m170614_104256_call_center_office extends Migration
{
    const ROLES = [
        'country.curator',
        'callcenter.manager',
        'callcenter.hr',
    ];

    const RULES = [
        'callcenter.office.index',
        'callcenter.office.edit',
        'callcenter.office.delete',
        'callcenter.office.activate',
        'callcenter.office.deactivate',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $CallCenterOfficeSchema = Yii::$app->db->schema->getTableSchema(Office::tableName());

        if ($CallCenterOfficeSchema !== null) {
            $this->dropTable(Office::tableName());
        }

        $this->createTable(Office::tableName(), [
            'id' => $this->primaryKey(),
            'country_id' => $this->integer()->notNull(),
            'name' => $this->string(),
            'active' => $this->integer(1)->defaultValue(1),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addForeignKey(null, Office::tableName(), 'country_id', Country::tableName(), 'id', self::CASCADE, self::RESTRICT);

        $this->addColumn(CallCenter::tableName(), 'office_id', $this->integer());
        $this->addForeignKey(null, CallCenter::tableName(), 'office_id', Office::tableName(), 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $CallCenterOfficeSchema = Yii::$app->db->schema->getTableSchema(Office::tableName());

        if ($CallCenterOfficeSchema !== null) {
            $this->dropTable(Office::tableName());
        }

        $this->dropColumn(CallCenter::tableName(), 'office_id');
    }
}
