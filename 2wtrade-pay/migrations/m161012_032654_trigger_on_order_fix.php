<?php
use app\components\CustomMigration as Migration;
use app\modules\order\models\OrderStatus;

/**
 * Class m161012_032654_trigger_on_order_fix
 */
class m161012_032654_trigger_on_order_fix extends Migration
{
    /**
     * @var array - статусы заказа для списания на складе
     */
    private $statusesMinus = [
        OrderStatus::STATUS_DELIVERY_ACCEPTED,
        OrderStatus::STATUS_DELIVERY_LEFT_WAREHOUSE,
        OrderStatus::STATUS_DELIVERY,
        OrderStatus::STATUS_DELIVERY_BUYOUT,
        OrderStatus::STATUS_FINANCE_MONEY_RECEIVED,
        OrderStatus::STATUS_DELIVERY_LEFT_OUR_WAREHOUSE,
        OrderStatus::STATUS_DELIVERY_REDELIVERY,
        OrderStatus::STATUS_DELIVERY_LEFT_THEIR_WAREHOUSE,
        OrderStatus::STATUS_DELIVERY_DELAYED,
        OrderStatus::STATUS_DELIVERY_LOST,
        OrderStatus::STATUS_DELIVERY_PENDING,
    ];

    /**
     * @var array - статусы заказа для поступления на складе
     */
    private $statusesPlus = [
        OrderStatus::STATUS_DELIVERY_DENIAL,
        OrderStatus::STATUS_DELIVERY_RETURNED,
        OrderStatus::STATUS_DELIVERY_REJECTED,
        OrderStatus::STATUS_LOGISTICS_REJECTED,
        OrderStatus::STATUS_DELIVERY_REFUND,
        OrderStatus::STATUS_LOGISTICS_ACCEPTED,
    ];
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->execute('DROP TRIGGER IF EXISTS `order_change_status_id`');

        $createTriggerSql = "
CREATE TRIGGER `order_change_status_id` AFTER UPDATE ON `order`
    
    FOR EACH ROW BEGIN
        DECLARE v_order_product_id INT;
        DECLARE v_product_id INT;
        DECLARE v_quantity INT;
        DECLARE v_storage_part_id INT;
        DECLARE cursor_done integer default 0;
        /* курсор на выборку товаров заказа */
        DECLARE products_cur CURSOR FOR SELECT id, product_id, quantity, storage_part_id
                                    FROM order_product
                                    WHERE order_id = NEW.id;
        DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET cursor_done=1;
        IF NEW.status_id != OLD.status_id THEN /* делаем только при изменении статуса */
            /* только для необходимых order.status_id делаем действия */
            IF NEW.status_id IN (" . implode(",", $this->statusesMinus) . "," . implode(",", $this->statusesPlus) . ") THEN
            
                SET @is_have = NULL;
                IF NEW.status_id IN (" . implode(",", $this->statusesMinus) . ") THEN /* списание со склада */
                    SELECT 1 into @is_have
                    FROM storage_product_unaccounted
                    WHERE order_id=NEW.id
                    LIMIT 1;
                    IF @is_have is NULL THEN
                        SELECT 1 into @is_have
                        FROM storage_part_history
                        WHERE order_id=NEW.id and type='minus'
                        LIMIT 1;
                    END IF;
                END IF;
                IF NEW.status_id IN (" . implode(",", $this->statusesPlus) . ") THEN /* поступление на склад */
                    IF v_storage_part_id != NULL THEN
                        SELECT 1 into @is_have
                        FROM storage_part_history
                        WHERE order_id=NEW.id and type='plus'
                        LIMIT 1;
                    END IF;
                END IF;
                
                IF @is_have is NULL THEN /*для такого order ещё не вносилась информация*/
                
                    SELECT delivery_id into @v_delivery_id
                    FROM delivery_request
                    WHERE order_id=NEW.id
                    LIMIT 1;
                    
                    OPEN products_cur;
                    SET cursor_done = 0;
                    FETCH products_cur INTO v_order_product_id, v_product_id, v_quantity, v_storage_part_id;
                    WHILE cursor_done = 0 DO 
                        
                        IF NEW.status_id IN (" . implode(",", $this->statusesMinus) . ") THEN /* списание со склада */
                            SET @id_storage_part_minus = NULL;
                        
                            SELECT part.id into @id_storage_part_minus 
                            FROM storage s,
                                 storage_part part
                            WHERE s.country_id = NEW.country_id
                                  and s.delivery_id = @v_delivery_id
                                  and s.id = part.storage_id
                                  and part.product_id = v_product_id 
                                  and part.active = 1
                                  and part.balance >= v_quantity
                            ORDER BY s.created_at, part.created_at
                            LIMIT 1;
                            
                            IF @id_storage_part_minus is not NULL THEN
                                INSERT INTO storage_part_history SET storage_part_id=@id_storage_part_minus, type='minus', amount=v_quantity, order_id=NEW.id, created_at=UNIX_TIMESTAMP();
                                UPDATE storage_part SET active=if(balance>v_quantity, 1, 0), balance=balance-v_quantity WHERE id=@id_storage_part_minus;
                                UPDATE order_product SET storage_part_id=@id_storage_part_minus WHERE id=v_order_product_id;
                            ELSE
                                SET @id_storage = NULL;
                                
                                SELECT s.id into @id_storage 
                                FROM storage s
                                WHERE s.country_id = NEW.country_id
                                ORDER BY s.created_at
                                LIMIT 1;
                                
                                INSERT INTO storage_product_unaccounted SET storage_id=@id_storage, product_id=v_product_id, amount=v_quantity, order_id=NEW.id, country_id=NEW.country_id;
                            END IF;
                        END IF;
                        
                        IF NEW.status_id IN (" . implode(",", $this->statusesPlus) . ") THEN /* поступление на склад */
                            IF v_storage_part_id != NULL THEN
                                INSERT INTO storage_part_history SET storage_part_id=v_storage_part_id, type='plus', amount=v_quantity, order_id=NEW.id, created_at=UNIX_TIMESTAMP();
                                UPDATE storage_part SET active=1, balance=balance+v_quantity WHERE id=v_storage_part_id;
                                UPDATE order_product SET storage_part_id=null WHERE id=v_order_product_id;
                            END IF;
                        END IF;
                        
                        SET cursor_done = 0;
                        FETCH products_cur INTO v_order_product_id, v_product_id, v_quantity, v_storage_part_id;
                    
                    END WHILE;
                    CLOSE products_cur;
                
                END IF;    
     
            END IF;
        END IF;
    
END;";

        $this->execute($createTriggerSql);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->execute('DROP TRIGGER IF EXISTS `order_change_status_id`');

        $createTriggerSql = "
CREATE TRIGGER `order_change_status_id` AFTER UPDATE ON `order`
    
    FOR EACH ROW BEGIN
        DECLARE v_order_product_id INT;
        DECLARE v_product_id INT;
        DECLARE v_quantity INT;
        DECLARE v_storage_part_id INT;
        DECLARE cursor_done integer default 0;
        /* курсор на выборку товаров заказа */
        DECLARE products_cur CURSOR FOR SELECT id, product_id, quantity, storage_part_id
                                    FROM order_product
                                    WHERE order_id = NEW.id;
        DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET cursor_done=1;
        IF NEW.status_id != OLD.status_id THEN /* делаем только при изменении статуса */
            /* только для необходимых order.status_id делаем действия */
            IF NEW.status_id IN (" . implode(",", $this->statusesMinus) . "," . implode(",", $this->statusesPlus) . ") THEN
                SELECT delivery_id into @v_delivery_id
                FROM delivery_request
                WHERE order_id=NEW.id 
                LIMIT 1;
                
                OPEN products_cur;
                SET cursor_done = 0;
                FETCH products_cur INTO v_order_product_id, v_product_id, v_quantity, v_storage_part_id;
                WHILE cursor_done = 0 DO 
                    
                    IF NEW.status_id IN (" . implode(",", $this->statusesMinus) . ") THEN /* списание со склада */
                        SET @id_storage_part_minus = NULL;
                    
                        SELECT part.id into @id_storage_part_minus 
                        FROM storage s,
                             storage_part part
                        WHERE s.country_id = NEW.country_id
                              and s.delivery_id = @v_delivery_id
                              and s.id = part.storage_id
                              and part.product_id = v_product_id 
                              and part.active = 1
                              and part.balance >= v_quantity
                        ORDER BY s.created_at, part.created_at
                        LIMIT 1;
                        
                        IF @id_storage_part_minus is not NULL THEN
                            INSERT INTO storage_part_history SET storage_part_id=@id_storage_part_minus, type='minus', amount=v_quantity, order_id=NEW.id, created_at=UNIX_TIMESTAMP();
                            UPDATE storage_part SET active=if(balance>v_quantity, 1, 0), balance=balance-v_quantity WHERE id=@id_storage_part_minus;
                            UPDATE order_product SET storage_part_id=@id_storage_part_minus WHERE id=v_order_product_id;
                        ELSE
                            SET @id_storage = NULL;
                            
                            SELECT s.id into @id_storage 
                            FROM storage s
                            WHERE s.country_id = NEW.country_id
                            ORDER BY s.created_at
                            LIMIT 1;
                            
                            INSERT INTO storage_product_unaccounted SET storage_id=@id_storage, product_id=v_product_id, amount=v_quantity, order_id=NEW.id, country_id=NEW.country_id;
                        END IF;
                    END IF;
                    
                    IF NEW.status_id IN (" . implode(",", $this->statusesPlus) . ") THEN /* поступление на склад */
                        IF v_storage_part_id != NULL THEN
                            INSERT INTO storage_part_history SET storage_part_id=v_storage_part_id, type='plus', amount=v_quantity, order_id=NEW.id, created_at=UNIX_TIMESTAMP();
                            UPDATE storage_part SET active=1, balance=balance+v_quantity WHERE id=v_storage_part_id;
                            UPDATE order_product SET storage_part_id=null WHERE id=v_order_product_id;
                        END IF;
                    END IF;
                    
                    SET cursor_done = 0;
                    FETCH products_cur INTO v_order_product_id, v_product_id, v_quantity, v_storage_part_id;
                
                END WHILE;
                CLOSE products_cur;
     
            END IF;
        END IF;
    
END;";

        $this->execute($createTriggerSql);
    }
}
