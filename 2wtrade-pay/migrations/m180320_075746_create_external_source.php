<?php

use app\modules\catalog\models\ExternalSource;
use app\components\CustomMigration as Migration;

/**
 * Handles the creation for table `external_source`.
 */
class m180320_075746_create_external_source extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable(ExternalSource::tableName(), [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'url' => $this->string(),
            'description' => $this->string(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ], $this->tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable(ExternalSource::tableName());
    }
}