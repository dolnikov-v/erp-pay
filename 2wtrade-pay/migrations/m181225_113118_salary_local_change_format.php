<?php

use app\components\PermissionMigration as Migration;

/**
 * Class m181225_113118_salary_local_change_format
 */
class m181225_113118_salary_local_change_format extends Migration
{
    protected $permissions = ['salary.monthlystatement.deleteclosed'];

    protected $roles = [
        'salaryproject.clerk' => ['salary.monthlystatement.deleteclosed']
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('salary_person', 'salary_local', $this->decimal(15, 2));
        $this->alterColumn('salary_person', 'salary_usd', $this->decimal(15, 2));

        $this->alterColumn('salary_monthly_statement_data', 'salary_local', $this->decimal(15, 2));
        $this->alterColumn('salary_monthly_statement_data', 'salary_usd', $this->decimal(15, 2));

        parent::safeUp();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('salary_person', 'salary_local', $this->float());
        $this->alterColumn('salary_person', 'salary_usd', $this->float());

        $this->alterColumn('salary_monthly_statement_data', 'salary_local', $this->float());
        $this->alterColumn('salary_monthly_statement_data', 'salary_usd', $this->float());

        parent::safeDown();
    }
}
