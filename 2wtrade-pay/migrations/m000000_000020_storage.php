<?php
use app\components\CustomMigration;

class m000000_000020_storage extends CustomMigration
{
    public function safeUp()
    {
        $this->createTable('storage', [
            'id' => $this->primaryKey(),
            'country_id' => $this->integer()->notNull(),
            'name' => $this->string(100)->notNull(),
            'address' => $this->text()->defaultValue(null),
            'comment' => $this->text()->defaultValue(null),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(null, 'storage', 'country_id', 'country', 'id', 'CASCADE', 'RESTRICT');

        $this->createIndex('index_country_id', 'storage', 'country_id');

        $this->createTable('storage_product', [
            'id' => $this->primaryKey(),
            'storage_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
            'active' => $this->smallInteger()->notNull()->defaultValue(1),
            'balance' => $this->integer()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(null, 'storage_product', 'storage_id', 'storage', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey(null, 'storage_product', 'product_id', 'product', 'id', 'CASCADE', 'RESTRICT');

        $this->createTable('storage_product_history', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'storage_product_id' => $this->integer()->notNull(),
            'type' => "ENUM('plus', 'minus') NOT NULL DEFAULT 'plus'",
            'amount' => $this->integer()->notNull(),
            'comment' => $this->string(100)->defaultValue(null),
            'created_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(null, 'storage_product_history', 'user_id', 'user', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey(null, 'storage_product_history', 'storage_product_id', 'storage_product', 'id', 'CASCADE', 'RESTRICT');

        $this->createIndex('index_type', 'storage_product_history', 'type');
    }

    public function safeDown()
    {
        $this->dropTable('storage_product_history');
        $this->dropTable('storage_product');
        $this->dropTable('storage');
    }
}
