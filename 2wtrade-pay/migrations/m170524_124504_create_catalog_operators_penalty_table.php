<?php

use app\components\CustomMigration as Migration;

/**
 * Class m170524_124504_create_catalog_operators_penalty_table
 */
class m170524_124504_create_catalog_operators_penalty_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('catalog_operators_penalty', [
            'id' => $this->primaryKey(),
            'country_id' => $this->integer()->notNull(),
            'call_center_id' => $this->integer()->notNull(),
            'name' => $this->string(64),
            'penalty_value' => $this->float(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('catalog_operators_penalty');
    }
}
