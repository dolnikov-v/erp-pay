<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170524_093618_api_log_duplicate
 */
class m170524_093618_api_log_duplicate extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('api_log', 'status', $this->enum(['fail', 'success', 'duplicate']));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('api_log', 'status', $this->enum(['fail', 'success']));
    }
}
