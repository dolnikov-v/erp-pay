<?php
use app\components\CustomMigration as Migration;
use app\modules\order\models\OrderWorkflow;

class m160418_142727_order_workflow extends Migration
{
    public function safeUp()
    {
        $this->createTable('order_workflow', [
            'id' => $this->primaryKey(),
            'name' => $this->string(100)->notNull(),
            'comment' => $this->text()->defaultValue(null),
            'active' => $this->smallInteger()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        foreach ($this->getFixture() as $data) {
            $model = new OrderWorkflow($data);
            $model->save(false);
        }
    }

    public function safeDown()
    {
        $this->dropTable('order_workflow');
    }

    /**
     * @return array
     */
    private function getFixture()
    {
        return [
            ['name' => 'Fulfillment – Short Form', 'active' => 1],
            ['name' => 'Fulfillment – Long Form', 'active' => 1],
            ['name' => 'Warehouse and Courier Separately - Short Form', 'active' => 1],
            ['name' => 'Warehouse and Courier Separately - Long Form', 'active' => 1],
        ];
    }
}
