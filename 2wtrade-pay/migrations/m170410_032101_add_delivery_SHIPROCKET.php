<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryApiClass;
use app\modules\delivery\models\Delivery;
use app\models\Country;


/**
 * Class m170410_032101_add_delivery_SHIPROCKET
 */
class m170410_032101_add_delivery_SHIPROCKET extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        if (DeliveryApiClass::find()->where(
            ['name' => 'ShiprocketApi', 'class_path' => '/shiprocket-api/src/ShiprocketApi.php']
        )->exists()
        ) {
            $apiClass = DeliveryApiClass::findOne(
                ['name' => 'ShiprocketApi', 'class_path' => '/shiprocket-api/src/ShiprocketApi.php']
            );
        } else {
            $apiClass = new DeliveryApiClass();
        }
        $apiClass->name = 'ShiprocketApi';
        $apiClass->class_path = '/shiprocket-api/src/ShiprocketApi.php';
        $apiClass->active = '1';
        $apiClass->created_at = time();
        $apiClass->updated_at = time();
        $apiClass->save();

        if (Country::find()->where(
            [
                'name' => 'Индия',
                'char_code' => 'IN'
            ]
        )->exists()
        ) {
            $country = Country::find()->where(
                [
                    'name' => 'Индия',
                    'char_code' => 'IN'
                ]
            )->select('id')->one();
        } else {
            //нет страны
            return false;
        }

        if (Delivery::find()->where(
            [
                'name' => 'ShipRocket',
                'char_code' => 'SHR'
            ]
        )->exists()
        ) {
            $delivery = Delivery::findOne(
                [
                    'name' => 'ShipRocket',
                    'char_code' => 'SHR'
                ]
            );
        } else {
            $delivery = new Delivery();
        }
        $delivery->country_id = $country->id;
        $delivery->name = 'ShipRocket';
        $delivery->char_code = 'SHR';
        $delivery->api_class_id = $apiClass->id;
        $delivery->active = '1';
        $delivery->auto_sending = '0';
        $delivery->save(false);
        return true;
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        if (Country::find()->where(
            [
                'name' => 'Индия',
                'char_code' => 'IN'
            ]
        )->exists()
        ) {
            $country = Country::find()->where(
                [
                    'name' => 'Индия',
                    'char_code' => 'IN'
                ]
            )->select('id')->one();
        } else {
            //нет страны
            return false;
        }

        DeliveryApiClass::find()
            ->where([
                'name' => 'ShoprocketApi',
                'class_path' => '/shiprocket-api/src/ShiprocketApi.php'
            ])
            ->one()
            ->delete();

        Delivery::findOne(
            [
                'name' => 'ShipRocket',
                'country_id' => $country->id
            ]
        )->delete();
        return true;
    }
}
