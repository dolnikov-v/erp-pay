<?php
use app\components\PermissionMigration as Migration;

/**
 * Class m171215_032457_check_list_auditor
 */
class m171215_032457_check_list_auditor extends Migration
{
    protected $permissions = [
        'checklist.auditor.index',
        'checklist.auditor.set',
    ];

    protected $roles = [
        'callcenter.controller' => [
            'checklist.auditor.index',
            'checklist.auditor.set',
        ],
        'country.curator' => [
            'checklist.auditor.index',
        ],
        'project.manager' => [
            'checklist.auditor.index',
        ],
    ];

}
