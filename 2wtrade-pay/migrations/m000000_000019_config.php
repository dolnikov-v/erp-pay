<?php
use app\models\Config;
use yii\db\Migration;

class m000000_000019_config extends Migration
{
    public function safeUp()
    {
        $this->createTable(Config::tableName(), [
            'id' => $this->primaryKey(),
            'name' => $this->string(100)->notNull(),
            'description' => $this->text(),
            'value' => $this->text(),
        ]);

        foreach ($this->configFixture() as $fixture) {
            $config = new Config($fixture);
            $config->save(false);
        }
    }

    public function safeDown()
    {
        $this->dropTable(Config::tableName());
    }

    /**
     * @return array
     */
    private function configFixture()
    {
        return [
            [
                'name' => Config::ORDER_STATUSES_LINK_SCHEMA,
                'description' => 'Ссылка на схему статусов',
                'value' => '',
            ],
        ];
    }
}
