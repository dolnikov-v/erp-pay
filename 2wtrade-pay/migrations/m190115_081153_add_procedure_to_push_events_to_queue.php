<?php

use app\components\CustomMigration as Migration;

/**
 * Class m190115_081153_add_procedure_to_push_events_to_queue
 */
class m190115_081153_add_procedure_to_push_events_to_queue extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('db_config', [
            'key' => $this->string(255),
            'value' => $this->string(255)
        ]);
        $this->addPrimaryKey($this->getPkName('db_config', 'key'), 'db_config', 'key');

        $this->execute('DROP FUNCTION IF EXISTS `push_event_to_queue`');

        $sql = <<<SQL
CREATE FUNCTION `push_event_to_queue` (`event_id` VARCHAR (255) , `type` VARCHAR(50), `primary_key` VARCHAR(255), `values` TEXT)
  RETURNS INT
LANGUAGE SQL
DETERMINISTIC
  BEGIN
    DECLARE `result` INTEGER DEFAULT 1;
    DECLARE `buffer` INTEGER; # записываем сюда всякий мусор
    DECLARE `counter` INT DEFAULT 3; # У делаем 3 попытки отправить сообщение на редис
    DECLARE `redis_host`, `redis_password`, `redis_db_events_channel` VARCHAR(255);
    DECLARE `redis_port` INT;
    DECLARE `uuid` VARCHAR(160);

    IF `event_id` IS NULL OR `type` IS NULL OR `primary_key` IS NULL THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'NULL is not allowed.';
    END IF;

    SET `uuid`=SHA(CONCAT(UUID(), `event_id`, `type`, `primary_key`, IFNULL(`values`, '')));

    SELECT `value` INTO `redis_host` FROM `db_config` WHERE `key` = 'redis_host';
    SELECT `value` INTO `redis_port` FROM `db_config` WHERE `key` = 'redis_port';
    SELECT `value` INTO `redis_password` FROM `db_config` WHERE `key` = 'redis_password';
    SELECT `value` INTO `redis_db_events_channel` FROM `db_config` WHERE `key` = 'redis_db_events_channel';

    IF `redis_password` IS NOT NULL AND `redis_host` IS NOT NULL AND `redis_port` IS NOT NULL AND `redis_db_events_channel` IS NOT NULL THEN
      SELECT redis_server_set(`redis_host`, `redis_port`, `redis_password`) INTO `buffer`;
      WHILE `result` AND `counter` > 0 DO
        SET `result` = redis_exec('hset', CONCAT(`redis_db_events_channel`, '.messages'), `uuid`, json_object('event_id', `event_id`, 'type', `type`, 'primary_key', `primary_key`, 'values', `values`));
        SET `counter` = `counter` - 1;
      END WHILE;
      IF `result` THEN
        SELECT redis_exec('quit') INTO `buffer`;
      END IF;
      IF NOT `result` AND redis_exec('lpush', CONCAT(`redis_db_events_channel`, '.waiting'), `uuid`) THEN
        SELECT redis_exec('hdel', CONCAT(`redis_db_events_channel`, '.messages'), `uuid`) INTO `result`;
        SET `result` = 1;
      END IF;
    END IF;
    IF `result` THEN
      INSERT INTO `db_events` (`id`, `event_id`, `type`, `primary_key`, `values`) VALUES (`uuid`, `event_id`, `type`, `primary_key`, `values`);
      RETURN 2;
    ELSE
      RETURN 1;
    END IF;
  END;
SQL;
        $this->execute($sql);

        $this->execute('DROP TRIGGER IF EXISTS `order_finance_prediction_insert_event_log`');
        $createTriggerSql = <<<SQL
CREATE TRIGGER `order_finance_prediction_insert_event_log` AFTER INSERT ON `order_finance_prediction`
    FOR EACH ROW BEGIN
      DECLARE result INT;
      SELECT push_event_to_queue('order_finance_prediction', 'insert', NEW.order_id, NULL) INTO result;
    END;

SQL;
        $this->execute($createTriggerSql);

        $this->execute('DROP TRIGGER IF EXISTS `order_finance_prediction_update_event_log`');
        $createTriggerSql = <<<SQL
CREATE TRIGGER `order_finance_prediction_update_event_log` AFTER UPDATE ON `order_finance_prediction`
    FOR EACH ROW BEGIN
      DECLARE result INT;
      SELECT push_event_to_queue('order_finance_prediction', 'update', NEW.order_id, NULL) INTO result;
    END;
SQL;
        $this->execute($createTriggerSql);

        $this->execute('DROP TRIGGER IF EXISTS `order_finance_prediction_delete_event_log`');
        $createTriggerSql = <<<SQL
CREATE TRIGGER `order_finance_prediction_delete_event_log` AFTER DELETE ON `order_finance_prediction`
    FOR EACH ROW BEGIN
      DECLARE result INT;
      SELECT push_event_to_queue('order_finance_prediction', 'delete', OLD.order_id, NULL) INTO result;
    END;
SQL;
        $this->execute($createTriggerSql);


        $this->execute('DROP TRIGGER IF EXISTS `order_finance_fact_insert_event_log`');
        $createTriggerSql = <<<SQL
CREATE TRIGGER `order_finance_fact_insert_event_log` AFTER INSERT ON `order_finance_fact`
    FOR EACH ROW BEGIN
      DECLARE result INT;
      SELECT push_event_to_queue('order_finance_fact', 'insert', NEW.id, NULL) INTO result;
    END;
SQL;
        $this->execute($createTriggerSql);

        $this->execute('DROP TRIGGER IF EXISTS `order_finance_fact_update_event_log`');
        $createTriggerSql = <<<SQL
CREATE TRIGGER `order_finance_fact_update_event_log` AFTER UPDATE ON `order_finance_fact`
    FOR EACH ROW BEGIN
      DECLARE result INT;
      SELECT push_event_to_queue('order_finance_fact', 'update', NEW.id, NULL) INTO result;
    END;
SQL;
        $this->execute($createTriggerSql);

        $this->execute('DROP TRIGGER IF EXISTS `order_finance_fact_delete_event_log`');
        $createTriggerSql = <<<SQL
CREATE TRIGGER `order_finance_fact_delete_event_log` AFTER DELETE ON `order_finance_fact`
    FOR EACH ROW BEGIN
      DECLARE result INT;
      SELECT push_event_to_queue('order_finance_fact', 'delete', OLD.id, NULL) INTO result;
    END;
SQL;
        $this->execute($createTriggerSql);

        $this->execute('DROP TRIGGER IF EXISTS `order_status_id_was_changed_event_log`');
        $createTriggerSql = <<<SQL
CREATE TRIGGER `order_status_id_was_changed_event_log` AFTER UPDATE ON `order`
    FOR EACH ROW BEGIN
        DECLARE result INT;
        IF OLD.status_id != NEW.status_id THEN
          SELECT push_event_to_queue('order_status_id_was_changed', 'update', NEW.id, JSON_OBJECT('old', OLD.status_id, 'new', NEW.status_id)) INTO result;
        END IF; 
    END;
SQL;
        $this->execute($createTriggerSql);

        $createTriggerSql = <<<SQL
CREATE TRIGGER `db_config_insert_redis_settings` AFTER INSERT ON `db_config`
    FOR EACH ROW BEGIN
        DECLARE result INT;
        IF NEW.`key` = 'redis_host' OR NEW.`key` = 'redis_password' OR NEW.`key` = 'redis_port' THEN
          SELECT redis_exec('quit') INTO `result`;      
        END IF;
    END;
SQL;
        $this->execute($createTriggerSql);

        $createTriggerSql = <<<SQL
CREATE TRIGGER `db_config_update_redis_settings` AFTER UPDATE ON `db_config`
    FOR EACH ROW BEGIN
        DECLARE result INT;
        IF NEW.`key` = 'redis_host' OR NEW.`key` = 'redis_password' OR NEW.`key` = 'redis_port' OR OLD.`key` = 'redis_host' OR OLD.`key` = 'redis_password' OR OLD.`key` = 'redis_port' THEN
          SELECT redis_exec('quit') INTO `result`;      
        END IF;
    END;
SQL;
        $this->execute($createTriggerSql);

        $createTriggerSql = <<<SQL
CREATE TRIGGER `db_config_delete_redis_settings` AFTER DELETE ON `db_config`
    FOR EACH ROW BEGIN
        DECLARE result INT;
        IF OLD.`key` = 'redis_host' OR OLD.`key` = 'redis_password' OR OLD.`key` = 'redis_port' THEN
          SELECT redis_exec('quit') INTO `result`;      
        END IF;
    END;
SQL;
        $this->execute($createTriggerSql);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->execute('DROP TRIGGER IF EXISTS `order_finance_prediction_insert_event_log`');
        $createTriggerSql = <<<SQL
CREATE TRIGGER `order_finance_prediction_insert_event_log` AFTER INSERT ON `order_finance_prediction`
    FOR EACH ROW BEGIN
        INSERT INTO `db_events` SET `event_id`='order_finance_prediction', `type`='insert', `primary_key`=NEW.order_id;
    END;
SQL;
        $this->execute($createTriggerSql);

        $this->execute('DROP TRIGGER IF EXISTS `order_finance_prediction_update_event_log`');
        $createTriggerSql = <<<SQL
CREATE TRIGGER `order_finance_prediction_update_event_log` AFTER UPDATE ON `order_finance_prediction`
    FOR EACH ROW BEGIN
        INSERT INTO `db_events` SET `event_id`='order_finance_prediction', `type`='update', `primary_key`=NEW.order_id;
    END;
SQL;
        $this->execute($createTriggerSql);

        $this->execute('DROP TRIGGER IF EXISTS `order_finance_prediction_delete_event_log`');
        $createTriggerSql = <<<SQL
CREATE TRIGGER `order_finance_prediction_delete_event_log` AFTER DELETE ON `order_finance_prediction`
    FOR EACH ROW BEGIN
        INSERT INTO `db_events` SET `event_id`='order_finance_prediction', `type`='delete', `primary_key`=OLD.order_id;
    END;
SQL;
        $this->execute($createTriggerSql);


        $this->execute('DROP TRIGGER IF EXISTS `order_finance_fact_insert_event_log`');
        $createTriggerSql = <<<SQL
CREATE TRIGGER `order_finance_fact_insert_event_log` AFTER INSERT ON `order_finance_fact`
    FOR EACH ROW BEGIN
        INSERT INTO `db_events` SET `event_id`='order_finance_fact', `type`='insert', `primary_key`=NEW.id;
    END;
SQL;
        $this->execute($createTriggerSql);

        $this->execute('DROP TRIGGER IF EXISTS `order_finance_fact_update_event_log`');
        $createTriggerSql = <<<SQL
CREATE TRIGGER `order_finance_fact_update_event_log` AFTER UPDATE ON `order_finance_fact`
    FOR EACH ROW BEGIN
        INSERT INTO `db_events` SET `event_id`='order_finance_fact', `type`='update', `primary_key`=NEW.id;
    END;
SQL;
        $this->execute($createTriggerSql);

        $this->execute('DROP TRIGGER IF EXISTS `order_finance_fact_delete_event_log`');
        $createTriggerSql = <<<SQL
CREATE TRIGGER `order_finance_fact_delete_event_log` AFTER DELETE ON `order_finance_fact`
    FOR EACH ROW BEGIN
        INSERT INTO `db_events` SET `event_id`='order_finance_fact', `type`='delete', `primary_key`=OLD.id;
    END;
SQL;
        $this->execute($createTriggerSql);

        $this->execute('DROP TRIGGER IF EXISTS `order_status_id_was_changed_event_log`');
        $createTriggerSql = <<<SQL
CREATE TRIGGER `order_status_id_was_changed_event_log` AFTER UPDATE ON `order`
    FOR EACH ROW BEGIN
        IF OLD.status_id != NEW.status_id THEN
          INSERT INTO `db_events` SET `event_id`='order_status_id_was_changed', `type`='update', `primary_key`=NEW.id, `values`=JSON_OBJECT('old', OLD.status_id, 'new', NEW.status_id);
        END IF; 
    END;
SQL;
        $this->execute($createTriggerSql);

        $this->execute('DROP TRIGGER IF EXISTS `db_config_delete_redis_settings`');
        $this->execute('DROP TRIGGER IF EXISTS `db_config_update_redis_settings`');
        $this->execute('DROP TRIGGER IF EXISTS `db_config_insert_redis_settings`');

        $this->execute('DROP FUNCTION IF EXISTS `push_event_to_queue`');
        $this->dropTable('db_config');
    }
}
