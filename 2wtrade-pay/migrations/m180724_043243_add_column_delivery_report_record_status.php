<?php
use app\components\CustomMigration as Migration;
use app\modules\deliveryreport\models\DeliveryReportRecord;

/**
 * Class m180724_043243_add_column_delivery_report_record_status
 */
class m180724_043243_add_column_delivery_report_record_status extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(DeliveryReportRecord::tableName(), 'status_id', $this->integer()->after('status'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(DeliveryReportRecord::tableName(), 'status_id');
    }
}
