<?php
use app\components\CustomMigration as Migration;

use app\models\Partner;
use app\models\Timezone;

/**
 * Class m180724_075839_add_column_partner_time_zone
 */
class m180724_075839_add_column_partner_time_zone extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Partner::tableName(), 'timezone_id', $this->integer()->after('active'));
        $this->addForeignKey($this->getFkName(Partner::tableName(), 'timezone_id'), Partner::tableName(), 'timezone_id', Timezone::tableName(), 'id', Migration::SET_NULL, Migration::NO_ACTION);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey($this->getFkName(Partner::tableName(), 'timezone_id'), Partner::tableName());
        $this->dropColumn(Partner::tableName(), 'timezone_id');
    }
}
