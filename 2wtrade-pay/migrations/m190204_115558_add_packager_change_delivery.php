<?php

use app\components\PermissionMigration as Migration;

/**
 * Class m190204_115558_add_packager_change_delivery
 */
class m190204_115558_add_packager_change_delivery extends Migration
{
    protected $permissions = ['packager.request.changedelivery'];

    protected $roles = [
        'admin' => ['packager.request.changedelivery'],
        'controller.analyst' => ['packager.request.changedelivery'],
        'country.curator' => ['packager.request.changedelivery'],
        'deliveryreport.clerk' => ['packager.request.changedelivery'],
        'finance.director' => ['packager.request.changedelivery'],
        'operational.director' => ['packager.request.changedelivery'],
        'partner' => ['packager.request.changedelivery'],
        'project.manager' => ['packager.request.changedelivery'],
        'support.manager' => ['packager.request.changedelivery'],
        'technical.director' => ['packager.request.changedelivery'],
    ];
}
