<?php
use app\components\CustomMigration as Migration;
use app\models\Product;

/**
 * Class m170627_044036_add_columns_product_table
 */
class m170627_044036_add_columns_product_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Product::tableName(), 'sku', $this->string(128)->defaultValue(null) . ' AFTER `name`');
        $this->addColumn(Product::tableName(), 'source', $this->string(128)->defaultValue(null) . ' AFTER `sku`');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Product::tableName(), 'sku');
        $this->dropColumn(Product::tableName(), 'source');
    }
}
