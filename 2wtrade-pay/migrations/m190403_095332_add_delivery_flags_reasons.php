<?php
use app\components\PermissionMigration as Migration;

/**
 * Class m190403_095332_add_delivery_flags_reasons
 */
class m190403_095332_add_delivery_flags_reasons extends Migration
{

    protected $permissions = ['delivery.control.setcities'];

    protected $roleList = [
        'admin',
        'controller.analyst',
        'country.curator',
        'partners.manager',
        'support.manager',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('delivery', 'flags', $this->integer()->defaultValue(0));

        $this->createTable('delivery_auto_change_reason', [
            'id' => $this->primaryKey(),
            'from_delivery_id' => $this->integer(),
            'to_delivery_id' => $this->integer(),
            'reason' => $this->string(),
        ]);

        $this->addForeignKey(null, 'delivery_auto_change_reason', 'from_delivery_id', 'delivery', 'id', self::CASCADE, self::CASCADE);
        $this->addForeignKey(null, 'delivery_auto_change_reason', 'to_delivery_id', 'delivery', 'id', self::CASCADE, self::CASCADE);

        $this->insert('notification', [
            'description' => 'Ошибка автоматической смены Службы Доставки на {delivery} при отклонении заказа {order}: {error}',
            'trigger' => 'auto.change.delivery.error',
            'type' => 'warning',
            'group' => 'system',
            'active' => 1
        ]);

        $roles = [
            'support.manager',
        ];

        foreach ($roles as $role) {
            $this->insert('notification_role', [
                'role' => $role,
                'trigger' => 'auto.change.delivery.error',
                'created_at' => time(),
                'updated_at' => time(),
            ]);
        }

        parent::safeUp();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('delivery', 'flags');
        $this->dropTable('delivery_auto_change_reason');

        $this->delete('user_notification_setting', ['trigger' => 'auto.change.delivery.error']);
        $this->delete('notification', ['trigger' => 'auto.change.delivery.error']);

        parent::safeDown();
    }
}
