<?php
use app\components\CustomMigration as Migration;
use yii\db\Query;

/**
 * Class m180904_094722_dismissal_bonus_columbia
 */
class m180904_094722_dismissal_bonus_columbia extends Migration
{

    const CHAR_CODE = 'CO';

    protected $bonuses = [
        'Выходное пособие Колумбия, Severance' => [
            'type' => '13_salary',
            'sum' => null,
            'sum_local' => 88211,
            'percent' => 100,
        ],
        'Выходное пособие Колумбия, Interest' => [
            'type' => '13_salary',
            'sum' => null,
            'sum_local' => 88211,
            'percent' => 12,
        ],
        'Выходное пособие Колумбия, Service bonus' => [
            'type' => '13_salary',
            'sum' => null,
            'sum_local' => 88211,
            'percent' => 100,
        ],
        'Выходное пособие Колумбия, Holidays' => [
            'type' => '13_salary',
            'sum' => null,
            'sum_local' => 0,
            'percent' => 50,
        ],
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $offices = (new Query())->select('salary_office.id')
            ->from('salary_office')
            ->leftJoin('country', 'country.id = salary_office.country_id')
            ->where(['country.char_code' => self::CHAR_CODE])
            ->column();

        foreach ($offices as $officeId) {
            foreach ($this->bonuses as $bonusName => $bonusData) {

                $bonusTypeId = (new Query())->select('id')
                    ->from('salary_bonus_type')
                    ->where(['office_id' => $officeId])
                    ->andWhere(['name' => $bonusName])
                    ->scalar();

                $data = [
                    'office_id' => $officeId,
                    'name' => $bonusName,
                    'type' => $bonusData['type'],
                    'sum' => $bonusData['sum'],
                    'sum_local' => $bonusData['sum_local'],
                    'percent' => $bonusData['percent'],
                    'active' => 1,
                    'is_dismissal_pay' => 1,
                    'created_at' => time(),
                    'updated_at' => time(),
                ];

                if (!$bonusTypeId) {
                    $this->insert('salary_bonus_type', $data);
                }
                else {
                    $this->update('salary_bonus_type', $data, ['id' => $bonusTypeId]);
                }
            }
        }
    }
}
