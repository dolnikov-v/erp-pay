<?php
use app\components\CustomMigration as Migration;
use app\models\Notification;

/**
 * Class m170904_091929_add_zabbix_notification
 */
class m170904_091929_add_zabbix_notification extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $model = new Notification([
            'description' => 'Message: {message}. Amount: {amount} in status {status_id} more than {time_norm}' . PHP_EOL,
            'trigger' => 'zabbix.test',
            'type' => Notification::TYPE_DANGER,
            'group' => Notification::GROUP_SYSTEM,
            'active' => Notification::ACTIVE,
        ]);

        $model->save(false);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete(Notification::tableName(), ['trigger' => 'zabbix.test']);
    }
}
