<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170406_041542_fix_report_standart_sales_permission
 */
class m170406_041542_fix_report_standart_sales_permission extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->delete($this->authManager->itemChildTable, ['child' => 'report.standartsales.index', 'parent' => 'country.curator']);
        $this->delete('{{%auth_item}}', ['name' => 'report.standartsales.index']);

        $this->insert('{{%auth_item}}',array(
            'name'=>'report.reportstandartsales.index',
            'type' => '2',
            'description' => 'report.reportstandartsales.index'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'country.curator',
            'child' => 'report.reportstandartsales.index'
        ));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

    }
}
