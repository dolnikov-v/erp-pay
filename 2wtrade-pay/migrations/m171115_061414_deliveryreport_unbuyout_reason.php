<?php
use app\components\CustomMigration as Migration;

use app\modules\deliveryreport\models\DeliveryReport;

/**
 * Class m171115_061414_deliveryreport_unbuyout_reason
 */
class m171115_061414_deliveryreport_unbuyout_reason extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(DeliveryReport::tableName(), 'unbuyout_reason_pair', $this->text()->after('status_pair'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(DeliveryReport::tableName(), 'unbuyout_reason_pair');
    }
}
