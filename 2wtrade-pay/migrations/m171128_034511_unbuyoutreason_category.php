<?php
use app\components\CustomMigration as Migration;
use app\modules\catalog\models\UnBuyoutReason;

/**
 * Class m171128_034511_unbuyoutreason_category
 */
class m171128_034511_unbuyoutreason_category extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(UnBuyoutReason::tableName(), 'category', $this->integer()->after('name'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(UnBuyoutReason::tableName(), 'category');
    }
}
