<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryApiClass;

/**
 * Class m171003_093410_add_detrack_api_class
 */
class m171003_093410_add_detrack_api_class extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $ips = new DeliveryApiClass();
        $ips->name = 'Detrack';
        $ips->class_path = '/detrack-api/src/DetrackApi.php';
        $ips->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        DeliveryApiClass::findOne(['name' => "Detrack"])->delete();
    }
}
