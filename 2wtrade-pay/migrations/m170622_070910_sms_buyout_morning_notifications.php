<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;
use app\models\Notification;

/**
 * Class m170622_070910_sms_buyout_morning_notifications
 */
class m170622_070910_sms_buyout_morning_notifications extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
		$crontabTask = new CrontabTask();
        $crontabTask->name = 'sms_buyouts_notification';
        $crontabTask->description = 'Утреннее SMS уведомление о выкупах по циклу 15 дней';
        $crontabTask->save();

        $model = new Notification([
            'description' => 'Доброе утро! По состоянию на {date} средний выкуп 2wtrade составляет {vikup_percent}',
            'trigger' => Notification::TRIGGER_SMS_BUYOUTS_NOTIFICATION,
            'type' => Notification::TYPE_INFO,
            'group' => Notification::GROUP_REPORT,
            'active' => Notification::ACTIVE,
        ]);

        $model->save(false);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $model = Notification::findOne(['trigger' => Notification::TRIGGER_SMS_BUYOUTS_NOTIFICATION]);

        if ($model instanceof Notification) {
            $model->delete();
        }

		$crontabTask = CrontabTask::find()
            ->byName('sms_buyouts_notification')
            ->one();

        if ($crontabTask instanceof CrontabTask) {
            $crontabTask->delete();
        }
    }
}
