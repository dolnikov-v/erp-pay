<?php
use app\components\CustomMigration as Migration;

/**
 * Class m161214_114841_rename_qrcodes_bar
 */
class m161214_114841_rename_qrcodes_bar extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropTable('qr_codes');

        $this->createTable('bar_codes', [
            'id' => $this->primaryKey(),
            'bar_code' => $this->string(500)->notNull(),
            'created_at' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)->notNull(),
            'user_id' => $this->integer(4),
            'product_id' => $this->integer(4),
            'order_id' => $this->integer(16),
            'status' => $this->integer(2)->defaultValue(0),
        ], $this->tableOptions);

        $this->addForeignKey(null, 'bar_codes', 'user_id', 'user', 'id',  self::NO_ACTION, self::NO_ACTION);
        $this->addForeignKey(null, 'bar_codes', 'product_id', 'product', 'id',  self::NO_ACTION, self::NO_ACTION);
        $this->addForeignKey(null, 'bar_codes', 'order_id', 'order', 'id',  self::NO_ACTION, self::NO_ACTION);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('bar_codes');

        $this->createTable('qr_codes', [
            'id' => $this->primaryKey(),
            'qr_code' => $this->string(500)->notNull(),
            'created_at' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)->notNull(),
            'user_id' => $this->integer(4),
            'product_id' => $this->integer(4),
            'order_id' => $this->integer(16),
            'status' => $this->integer(2)->defaultValue(0),
        ], $this->tableOptions);

        $this->addForeignKey(null, 'qr_codes', 'user_id', 'user', 'id',  self::NO_ACTION, self::NO_ACTION);
        $this->addForeignKey(null, 'qr_codes', 'product_id', 'product', 'id',  self::NO_ACTION, self::NO_ACTION);
        $this->addForeignKey(null, 'qr_codes', 'order_id', 'order', 'id',  self::NO_ACTION, self::NO_ACTION);
    }
}
