<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\Delivery;

/**
 * Class m170315_103017_add_generating_invoice_flag
 */
class m170315_103017_add_generating_invoice_flag extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Delivery::tableName(), 'auto_generating_invoice', $this->integer(1)->after('auto_list_generation')->defaultValue(0));
        Delivery::updateAll(['auto_generating_invoice' => 1], ['auto_list_generation' => 1]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Delivery::tableName(), 'auto_generating_invoice');
    }
}
