<?php

use app\components\CustomMigration as Migration;
use app\modules\i18n\models\Message;
use app\modules\i18n\models\SourceMessage;

class m000000_000007_message extends Migration
{
    public function safeUp()
    {
        $this->createTable(Message::tableName(), [
            'id' => $this->integer(),
            'language' => $this->string(16),
            'translation' => $this->text(),
        ], $this->tableOptions);

        $this->addPrimaryKey(null, Message::tableName(), ['id', 'language']);
        $this->addForeignKey(null, Message::tableName(), 'id', SourceMessage::tableName(), 'id', self::CASCADE, self::RESTRICT);
    }

    public function safeDown()
    {
        $this->dropTable(Message::tableName());
    }
}
