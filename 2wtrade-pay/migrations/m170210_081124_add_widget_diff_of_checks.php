<?php
use app\components\CustomMigration as Migration;
use app\modules\widget\models\WidgetType;
use app\modules\widget\models\WidgetRole;

/**
 * Class m170210_081124_add_widget_diff_of_checks
 */
class m170210_081124_add_widget_diff_of_checks extends Migration
{
    public function safeUp()
    {
        $this->insert(WidgetType::tableName(), [
            'code' => 'diff_of_checks',
            'name' => 'Расхождение чеков',
            'status' => WidgetType::STATUS_ACTIVE
        ]);

        $type = WidgetType::find()
            ->where(['code' => 'diff_of_checks'])
            ->one();

        $this->insert(WidgetRole::tableName(), [
            'role' => 'business_analyst',
            'type_id' => $type->id
        ]);

        $this->insert(WidgetRole::tableName(), [
            'role' => 'callcenter.manager',
            'type_id' => $type->id
        ]);

        $this->insert('i18n_source_message', ['category' => 'common', 'message' => 'Средний чек по КЦ']);
        $this->insert('i18n_source_message', ['category' => 'common', 'message' => 'Средний чек по выкупу']);
        $this->insert('i18n_source_message', ['category' => 'common', 'message' => 'Расхождение']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $type = WidgetType::find()
            ->where(['code' => 'diff_of_checks'])
            ->one();

        $this->delete(WidgetRole::tableName(), [
            'role' => 'business_analyst',
            'type_id' => $type->id,
        ]);

        $this->delete(WidgetRole::tableName(), [
            'role' => 'callcenter.manager',
            'type_id' => $type->id,
        ]);

        $this->delete(WidgetType::tableName(), ['code' => 'diff_of_checks']);
    }
}
