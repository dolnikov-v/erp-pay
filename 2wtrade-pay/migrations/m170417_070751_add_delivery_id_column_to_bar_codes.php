<?php

use app\components\CustomMigration as Migration;
use app\modules\bar\models\Bar;
use app\modules\delivery\models\Delivery;

/**
 * Handles adding delivery_id_column to table `bar_codes`.
 */
class m170417_070751_add_delivery_id_column_to_bar_codes extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn(Bar::tableName(), 'delivery_id', $this->integer(4)->after('user_id'));
        $this->addForeignKey(null, Bar::tableName(), 'delivery_id', Delivery::tableName(), 'id',  self::NO_ACTION, self::NO_ACTION);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn(Bar::tableName(), 'delivery_id');
    }
}
