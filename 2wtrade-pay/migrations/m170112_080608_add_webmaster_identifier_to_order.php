<?php

use app\components\CustomMigration;
use app\modules\order\models\Order;

/**
 * Handles adding webmaster_identifier to table `order`.
 */
class m170112_080608_add_webmaster_identifier_to_order extends CustomMigration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Order::tableName(), 'webmaster_identifier', $this->string(64));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Order::tableName(), 'webmaster_identifier');
    }
}
