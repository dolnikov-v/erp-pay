<?php
use app\components\CustomMigration as Migration;
use yii\db\Query;

/**
 * Class m170717_104312_call_center_person_operations
 */
class m170717_104312_call_center_person_operations extends Migration
{
    const ROLES = [
        'finance.director',
        'callcenter.hr',
        'callcenter.manager',
        'project.manager'
    ];

    const RULES = [
        'callcenter.person.changeteamleader',
        'callcenter.person.setrole',
        'callcenter.person.setwork',
        'callcenter.person.activatepersons',
        'callcenter.person.deactivatepersons',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $query = new Query();

        foreach (self::RULES as $rule) {

            $is_rule = $query->select('*')->from($this->authManager->itemTable)
                ->where(['name' => $rule, 'type' => 2])
                ->one();

            if (!$is_rule) {
                $this->insert($this->authManager->itemTable, [
                    'name' => $rule,
                    'description' => $rule,
                    'type' => 2,
                    'created_at' => time(),
                    'updated_at' => time()]);
            }

            foreach (self::ROLES as $role) {
                $is_can = $query->select('*')->from($this->authManager->itemChildTable)->where([
                    'parent' => $role,
                    'child' => $rule
                ])->one();

                if (!$is_can) {
                    $this->insert($this->authManager->itemChildTable, [
                        'parent' => $role,
                        'child' => $rule
                    ]);
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach (self::RULES as $rule) {
            foreach (self::ROLES as $role) {
                $this->delete($this->authManager->itemChildTable, [
                    'parent' => $role,
                    'child' => $rule
                ]);
            }
        }
    }
}
