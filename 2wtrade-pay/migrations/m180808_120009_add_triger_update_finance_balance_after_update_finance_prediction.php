<?php

use app\components\CustomMigration as Migration;

/**
 * Class m180808_120009_add_triger_update_finance_balance_after_update_finance_prediction
 */
class m180808_120009_add_triger_update_finance_balance_after_update_finance_prediction extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $sql = <<<SQL
              CREATE TRIGGER update_finance_balance AFTER UPDATE ON order_finance_prediction FOR EACH ROW
              BEGIN
                DECLARE delivery_id INTEGER;
                
                SET delivery_id = 0;
                 SELECT delivery_id INTO delivery_id FROM delivery_request where order_id = NEW.order_id;
                 
                update order_finance_balance set 
                prediction_price_cod = NEW.price_cod,
                price_cod_currency_id = NEW.price_cod_currency_id,
                prediction_price_storage = NEW.price_storage,
                price_storage_currency_id = NEW.price_storage_currency_id,
                prediction_price_fulfilment = NEW.price_fulfilment,
                price_fulfilment_currency_id = NEW.price_fulfilment_currency_id,
                prediction_price_packing = NEW.price_packing,
                price_packing_currency_id = NEW.price_packing_currency_id,
                prediction_price_package = NEW.price_package,
                price_package_currency_id = NEW.price_package_currency_id,
                prediction_price_delivery = NEW.price_delivery,
                price_delivery_currency_id = NEW.price_delivery_currency_id,
                prediction_price_redelivery = NEW.price_redelivery,
                price_redelivery_currency_id = NEW.price_redelivery_currency_id,
                prediction_price_delivery_back = NEW.price_delivery_back,
                price_delivery_back_currency_id = NEW.price_delivery_back_currency_id,
                prediction_price_delivery_return = NEW.price_delivery_return,
                price_delivery_return_currency_id = NEW.price_delivery_return_currency_id,
                prediction_price_address_correction = NEW.price_address_correction,
                price_address_correction_currency_id = NEW.price_address_correction_currency_id,
                prediction_price_cod_service = NEW.price_cod_service,
                price_cod_service_currency_id = NEW.price_cod_service_currency_id,
                prediction_price_vat = NEW.price_vat,
                price_vat_currency_id = NEW.price_vat_currency_id
                where order_id = NEW.order_id and delivery_id = delivery_id;
                        
              END;
SQL;
        $this->execute($sql);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->execute("DROP TRIGGER update_finance_balance");
    }
}
