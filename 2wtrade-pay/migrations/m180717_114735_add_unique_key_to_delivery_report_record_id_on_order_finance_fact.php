<?php

use app\modules\order\models\OrderFinanceFact;
use yii\db\Migration;

/**
 * Handles adding unique_key to table `delivery_report_record_id_on_order_finance_fact`.
 */
class m180717_114735_add_unique_key_to_delivery_report_record_id_on_order_finance_fact extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createIndex('unique_index_order_finance_fact_delivery_report_record_id', OrderFinanceFact::tableName(), ['delivery_report_record_id'], true);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        Yii::$app->db->createCommand()->checkIntegrity(false)->execute();

        $this->dropIndex('unique_index_order_finance_fact_delivery_report_record_id', OrderFinanceFact::tableName());

        Yii::$app->db->createCommand()->checkIntegrity(true)->execute();
    }
}
