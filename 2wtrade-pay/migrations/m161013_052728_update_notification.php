<?php
use app\components\CustomMigration as Migration;
use app\models\Notification;

/**
 * Class m161013_052728_update_notification
 */
class m161013_052728_update_notification extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $model = Notification::findOne(['trigger' => 'report.order.daily']);
        if ($model != null) {
            $model->description = 'Ежедневная сверка заказов c {dateFrom} по {dateTo}: Создано {created}; Отправлено в колл-центр {call_sent}; Одобрено колл-центром {call_approved}; Принято курьерской службой {delivery_sent}; Отклонено курьерской службой {delivery_error}.';
            $model->save(false);
        }

        $model = Notification::findOne(['trigger' => 'report.order.two.days.error']);
        if ($model != null) {
            $model->description = 'Заказы с ошибкой от 1 до 2-х дней: созданные с {dateFrom} по {dateTo}, Отклонено курьерской службой {delivery_error}.';
            $model->save(false);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $model = Notification::findOne(['trigger' => 'report.order.daily']);
        if ($model != null) {
            $model->description = 'Ежедневная сверка заказов c {dateFrom} по {dateTo}, страна {country}: Создано {created}; Отправлено в колл-центр {call_sent}; Одобрено колл-центром {call_approved}; Принято курьерской службой {delivery_sent}; Отклонено курьерской службой {delivery_error}';
            $model->save(false);
        }

        $model = Notification::findOne(['trigger' => 'report.order.two.days.error']);
        if ($model != null) {
            $model->description = 'Заказы с ошибкой от 1 до 2-х дней: созданные с {dateFrom} по {dateTo}, страна {country}, Отклонено курьерской службой {delivery_error}';
            $model->save(false);
        }
    }
}
