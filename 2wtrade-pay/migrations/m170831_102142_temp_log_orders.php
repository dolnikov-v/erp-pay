<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170831_102142_temp_log_orders
 */
class m170831_102142_temp_log_orders extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('temp_log_orders', [
            'order_id' => $this->integer()->notNull(), // заказ
            'begin' => $this->integer()->notNull(), // старт крона
            'end' => $this->integer()->notNull(), // конец крона
            'block_file' => $this->string(300), // файл блокировки
            'string_params_cron' => $this->string(300), // строка с параметрами запуска крона
        ]);

        $this->addForeignKey(null, 'temp_log_orders', 'order_id', 'order', 'id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('temp_log_orders');
    }
}
