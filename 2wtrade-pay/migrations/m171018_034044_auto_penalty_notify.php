<?php
use app\components\CustomMigration as Migration;
use app\models\Notification;
use app\models\AuthAssignment;
use app\models\User;
use app\models\UserNotificationSetting;

/**
 * Class m171018_034044_auto_penalty_notify
 */
class m171018_034044_auto_penalty_notify extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        // включить SMS уведомления группе пользователей
        $roles = ['controller.analyst', 'country.curator', 'callcenter.hr', 'callcenter.leader', 'finance.director'];
        $triggers = [Notification::TRIGGER_MAKE_AUTO_PENALTY];

        $usersByTriggerRole = AuthAssignment::find()
            ->distinct()
            ->select([AuthAssignment::tableName().'.user_id'])
            ->leftJoin(User::tableName(), User::tableName().'.id='.AuthAssignment::tableName().'.user_id')
            ->where(['IN', 'item_name', $roles])
            ->andWhere(['NOT IN', User::tableName().'.status', [User::STATUS_BLOCKED, User::STATUS_DELETED]])
            ->asArray()
            ->all();


        foreach ($triggers as $trigger) {
            foreach ($usersByTriggerRole as $userId) {

                $link = UserNotificationSetting::find()
                    ->where(['user_id' => $userId['user_id'], 'trigger' => $trigger])
                    ->one();

                if ($link instanceof UserNotificationSetting) {
                    $link->sms_active = 1;
                    $link->save();
                } else {
                    $record = new UserNotificationSetting();
                    $record->user_id = $userId['user_id'];
                    $record->trigger = $trigger;
                    $record->active = Notification::ACTIVE;
                    $record->sms_active = 1;
                    $record->save();
                }
            }
        }

        $notification = Notification::findOne(['trigger' => Notification::TRIGGER_MAKE_AUTO_PENALTY]);
        if (!($notification instanceof Notification)) {
            $notification = new Notification([
                'description' => 'A penalty was given to an employee {person} from the office {office} for {penalty} {date}',
                'trigger' => Notification::TRIGGER_MAKE_AUTO_PENALTY,
                'type' => Notification::TYPE_INFO,
                'group' => Notification::GROUP_SYSTEM,
                'active' => Notification::ACTIVE,
            ]);
            $notification->save(false);
        } else {
            $notification->description = 'A penalty was given to an employee {person} from the office {office} for {penalty} {date}';
            $notification->save(false);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

    }
}
