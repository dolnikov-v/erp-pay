<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m170922_104539_add_cron_jira_monitor_order_statuses
 */
class m170922_104539_add_cron_jira_monitor_order_statuses extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = new CrontabTask();
        $crontabTask->name = 'monitor_order_statuses';
        $crontabTask->description = "Мониторинг статусов заказов на скопление больше определенного времени и создание задач на исправление ситуации в Jira";
        $crontabTask->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        CrontabTask::findOne(['name' => 'monitor_order_statuses'])->delete();
    }
}
