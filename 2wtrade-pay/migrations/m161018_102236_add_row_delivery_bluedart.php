<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\Delivery;
/**
 * Class m161018_102236_add_row_delivery_bluedart
 */
class m161018_102236_add_row_delivery_bluedart extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $class = new Delivery();
        $class->country_id = '1';
        $class->name = 'Bluedart';
        $class->char_code = 'BD';
        $class->api_class_id = \app\modules\delivery\models\DeliveryApiClass::findOne(['name' => 'BluedartApi'])->id;
        $class->active = '1';
        $class->auto_sending = '1';
        $class->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        Delivery::findOne(['name' => 'Bluedart'])
        ->delete();
    }
}
