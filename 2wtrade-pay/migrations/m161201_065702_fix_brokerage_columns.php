<?php
use app\components\CustomMigration as Migration;

/**
 * Class m161109_165756_alter_table_order_column_adress_add
 */
class m161201_065702_fix_brokerage_columns extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropColumn('delivery', 'brokerage_enabled');
        $this->addColumn('country', 'brokerage_enabled', $this->boolean()->defaultValue(false)->after('active'));

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->addColumn('delivery', 'brokerage_enabled', $this->boolean()->defaultValue(false)->after('brokerage_stats_cutoff'));
        $this->dropColumn('country', 'brokerage_enabled');
    }
}
