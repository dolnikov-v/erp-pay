<?php

use app\components\CustomMigration as Migration;

/**
 * Class m170607_083414_add_packager_permissions_to_country_curator
 */
class m170607_083414_add_packager_permissions_to_country_curator extends Migration
{
    protected $permissions = [
        'packager.control.index',
        'packager.control.edit',
        'packager.control.delete',
        'packager.control.activate',
        'packager.control.deactivate',
        'packager.control.activateautosend',
        'packager.control.deactivateautosend',
        'packager.request.index',
        'packager.request.downloadlabel',
        'packager.request.send',
    ];

    /**
     * @inheritdoc
     */
    public function up()
    {
        foreach ($this->permissions as $permission) {
            $this->insert($this->authManager->itemChildTable, [
                'parent' => 'country.curator',
                'child' => $permission
            ]);
        }
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        foreach ($this->permissions as $permission) {
            $this->delete($this->authManager->itemChildTable, [
                'parent' => 'country.curator',
                'child' => $permission,
            ]);
        }
    }
}
