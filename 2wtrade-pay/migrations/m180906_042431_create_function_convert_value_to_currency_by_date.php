<?php

use yii\db\Migration;

/**
 * Handles the creation for table `function_convert_value_to_currency_by_date`.
 */
class m180906_042431_create_function_convert_value_to_currency_by_date extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $currencyRateForDateSql = <<<SQL
CREATE FUNCTION get_currency_rate_for_date(v_currency_id INTEGER, v_date DATE)
  RETURNS DECIMAL(10, 4) NOT DETERMINISTIC
  BEGIN
    DECLARE v_rate FLOAT DEFAULT NULL;

    IF v_date IS NULL OR v_date >= CURRENT_DATE()
    THEN
      SELECT `rate`
      INTO v_rate
      FROM `currency_rate`
      WHERE currency_id = v_currency_id
      LIMIT 1;
    ELSE
      SELECT `rate`
      INTO v_rate
      FROM `currency_rate_history`
      WHERE `currency_id` = v_currency_id AND ABS(`date` - v_date) = (SELECT MIN(ABS(crh_temp.`date` - v_date))
                                                                      FROM `currency_rate_history` crh_temp
                                                                      WHERE crh_temp.currency_id =
                                                                            currency_rate_history.currency_id)
      ORDER BY id
      LIMIT 1;
    END IF;
    RETURN v_rate;
  END;
SQL;

        $convertFunctionSql = <<<SQL
CREATE FUNCTION convert_value_to_currency_by_date(value FLOAT, from_currency_id INTEGER, to_currency_id INTEGER,
                                                  date  DATE)
  RETURNS FLOAT NOT DETERMINISTIC
  BEGIN
    DECLARE converted_value FLOAT DEFAULT NULL;
    DECLARE from_currency_rate FLOAT DEFAULT NULL;

    IF from_currency_id = to_currency_id
    THEN
      RETURN `value`;
    END IF;
    
    IF from_currency_id IS NULL OR to_currency_id IS NULL
    THEN
      RETURN NULL;
    END IF;  

    SELECT COALESCE(get_currency_rate_for_date(from_currency_id, date), 0)
    INTO from_currency_rate;

    SET converted_value =
    CASE WHEN from_currency_rate != 0
      THEN `value` / from_currency_rate * COALESCE(get_currency_rate_for_date(to_currency_id, date), 0)
    ELSE 0
    END;

    RETURN converted_value;
  END;
SQL;

        $this->db->createCommand($currencyRateForDateSql)->execute();
        $this->db->createCommand($convertFunctionSql)->execute();

        $this->db->createCommand('DROP FUNCTION IF EXISTS convertToCurrencyByCountry;')->execute();
        $this->db->createCommand('CREATE FUNCTION convertToCurrencyByCountry(sum FLOAT, currency_from INT, currency_to INT)
  RETURNS FLOAT
  BEGIN
    DECLARE converted_sum FLOAT DEFAULT 0;

    IF sum IS NULL OR sum = 0
    THEN
      RETURN 0;
    END IF;

    IF currency_from IS NULL OR currency_to IS NULL
      THEN
      RETURN NULL;
    END IF;

    SET converted_sum = CASE WHEN currency_from = currency_to
      THEN sum
        ELSE
          sum / COALESCE((SELECT rate
                          FROM currency_rate
                          WHERE currency_id = currency_from), 0) * COALESCE((SELECT rate
                                                                             FROM currency_rate
                                                                             WHERE currency_id =
                                                                                   currency_to),
                                                                            0)
        END;

    RETURN converted_sum;
  END;')->execute();
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $sql = 'DROP FUNCTION IF EXISTS convert_value_to_currency_by_date;';
        $this->db->createCommand($sql)->execute();
        $sql = 'DROP FUNCTION IF EXISTS get_currency_rate_for_date;';
        $this->db->createCommand($sql)->execute();

        $this->db->createCommand('DROP FUNCTION IF EXISTS convertToCurrencyByCountry;')->execute();
        $this->db->createCommand('CREATE FUNCTION convertToCurrencyByCountry(sum FLOAT, currency_from INT, currency_to INT)
  RETURNS FLOAT
  BEGIN
		  DECLARE converted_sum FLOAT DEFAULT 0;

          SET converted_sum = CASE WHEN  currency_from = currency_to THEN sum ELSE
			 CASE WHEN currency_to IS NOT NULL
			      THEN sum / COALESCE ((SELECT rate FROM currency_rate WHERE currency_id = currency_from),0) * COALESCE ((SELECT rate FROM currency_rate WHERE currency_id = currency_to), 0)
			      ELSE sum
			      END
			 END;

        RETURN converted_sum;
        END;')->execute();
    }
}
