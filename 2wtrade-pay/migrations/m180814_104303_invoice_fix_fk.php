<?php

use app\components\CustomMigration as Migration;
use app\models\Country;
use app\modules\catalog\models\RequisiteDelivery;
use app\modules\catalog\models\RequisiteDeliveryLink;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryContract;
use app\modules\report\models\Invoice;

/**
 * Class m180814_104303_invoice_fix_fk
 */
class m180814_104303_invoice_fix_fk extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        $this->dropForeignKey(
            $this->getFkName(Invoice::tableName(), 'requisite_delivery_id'),
            Invoice::tableName()
        );
        $this->addForeignKey(null, Invoice::tableName(), 'requisite_delivery_id', RequisiteDelivery::tableName(), 'id', self::SET_NULL, self::NO_ACTION);

        $this->dropForeignKey(
            $this->getFkName(RequisiteDeliveryLink::tableName(), 'requisite_delivery_id'),
            RequisiteDeliveryLink::tableName()
        );
        $this->alterColumn(RequisiteDeliveryLink::tableName(), 'requisite_delivery_id', $this->integer()->defaultValue(null));
        $this->addForeignKey(null, RequisiteDeliveryLink::tableName(), 'requisite_delivery_id', RequisiteDelivery::tableName(), 'id', self::SET_NULL, self::NO_ACTION);

        $this->dropForeignKey(
            $this->getFkName(RequisiteDeliveryLink::tableName(), 'delivery_id'),
            RequisiteDeliveryLink::tableName()
        );
        $this->alterColumn(RequisiteDeliveryLink::tableName(), 'delivery_id', $this->integer()->defaultValue(null));
        $this->addForeignKey($this->getFkName(RequisiteDeliveryLink::tableName(), 'delivery_id'), RequisiteDeliveryLink::tableName(), 'delivery_id', Delivery::tableName(), 'id', self::SET_NULL, self::NO_ACTION);

        $this->dropForeignKey(
            $this->getFkName(RequisiteDeliveryLink::tableName(), 'delivery_contract_id'),
            RequisiteDeliveryLink::tableName()
        );
        $this->alterColumn(RequisiteDeliveryLink::tableName(), 'delivery_contract_id', $this->integer()->defaultValue(null));
        $this->addForeignKey(null, RequisiteDeliveryLink::tableName(), 'delivery_contract_id', DeliveryContract::tableName(), 'id', self::SET_NULL, self::NO_ACTION);

        $this->dropForeignKey(
            $this->getFkName(RequisiteDeliveryLink::tableName(), 'country_id'),
            RequisiteDeliveryLink::tableName()
        );
        $this->alterColumn(RequisiteDeliveryLink::tableName(), 'country_id', $this->integer()->defaultValue(null));
        $this->addForeignKey(null, RequisiteDeliveryLink::tableName(), 'country_id', Country::tableName(), 'id', self::SET_NULL, self::NO_ACTION);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

        $this->dropForeignKey(
            $this->getFkName(Invoice::tableName(), 'requisite_delivery_id'),
            Invoice::tableName()
        );
        $this->addForeignKey(null, Invoice::tableName(), 'requisite_delivery_id', RequisiteDelivery::tableName(), 'id', self::CASCADE, self::RESTRICT);

        $this->dropForeignKey(
            $this->getFkName(RequisiteDeliveryLink::tableName(), 'requisite_delivery_id'),
            RequisiteDeliveryLink::tableName()
        );
        $this->addForeignKey(null, RequisiteDeliveryLink::tableName(), 'requisite_delivery_id', RequisiteDelivery::tableName(), 'id', self::CASCADE, self::RESTRICT);

        $this->dropForeignKey(
            $this->getFkName(RequisiteDeliveryLink::tableName(), 'delivery_id'),
            RequisiteDeliveryLink::tableName()
        );
        $this->addForeignKey($this->getFkName(RequisiteDeliveryLink::tableName(), 'delivery_id'), RequisiteDeliveryLink::tableName(), 'delivery_id', Delivery::tableName(), 'id', self::CASCADE, self::RESTRICT);


        $this->dropForeignKey(
            $this->getFkName(RequisiteDeliveryLink::tableName(), 'delivery_contract_id'),
            RequisiteDeliveryLink::tableName()
        );
        $this->addForeignKey(null, RequisiteDeliveryLink::tableName(), 'delivery_contract_id', DeliveryContract::tableName(), 'id', self::CASCADE, self::RESTRICT);

        $this->dropForeignKey(
            $this->getFkName(RequisiteDeliveryLink::tableName(), 'country_id'),
            RequisiteDeliveryLink::tableName()
        );
        $this->addForeignKey(null, RequisiteDeliveryLink::tableName(), 'country_id', Country::tableName(), 'id', self::CASCADE, self::RESTRICT);

    }
}
