<?php
use app\components\CustomMigration as Migration;
use app\modules\widget\models\WidgetRole;
use app\modules\widget\models\WidgetType;
use app\models\AuthItem;

/**
 * Class m170131_063519_add_delivery_debts_widget
 */
class m170131_063519_add_delivery_debts_widget extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert(WidgetType::tableName(), [
            'code' => 'delivery_debts',
            'name' => 'Дебиторская задолжность',
            'status' => WidgetType::STATUS_ACTIVE,
        ]);

        $type = WidgetType::find()
            ->where(['code' => 'delivery_debts'])
            ->one();

        $role = AuthItem::find()->where(['name' => 'business_analyst'])->one();
        if (is_null($role)) {
            $this->insert(AuthItem::tableName(), [
                'name' => 'business_analyst',
                'type' => 1,
                'description' => 'Бизнес-аналитик',
            ]);
        }
        $this->insert(WidgetRole::tableName(), [
            'role' => 'business_analyst',
            'type_id' => $type->id,
        ]);

        $role = AuthItem::find()->where(['name' => 'country.curator'])->one();
        if (is_null($role)) {
            $this->insert(AuthItem::tableName(), [
                'name' => 'country.curator',
                'type' => 1,
                'description' => 'Куратор по стране',
            ]);
        }
        $this->insert(WidgetRole::tableName(), [
            'role' => 'country.curator',
            'type_id' => $type->id,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $type = WidgetType::find()
            ->where(['code' => 'delivery_debts'])
            ->one();

        $this->delete(WidgetRole::tableName(), [
            'role' => 'business_analyst',
            'type_id' => $type->id,
        ]);


        $this->delete(WidgetRole::tableName(), [
            'role' => 'country.curator',
            'type_id' => $type->id,
        ]);

        $this->delete(WidgetType::tableName(), ['code' => 'delivery_debts']);
    }
}
