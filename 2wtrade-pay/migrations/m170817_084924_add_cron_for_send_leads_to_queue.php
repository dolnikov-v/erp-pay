<?php

use app\components\CustomMigration;
use app\modules\administration\models\CrontabTask;

/**
 * Handles adding cron_for_send_leads to table `queue`.
 */
class m170817_084924_add_cron_for_send_leads_to_queue extends CustomMigration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_CALL_CENTER_SEND_REQUESTS_TO_QUEUE]);
        if (!$crontabTask) {
            $crontabTask = new CrontabTask();
            $crontabTask->name = CrontabTask::TASK_CALL_CENTER_SEND_REQUESTS_TO_QUEUE;
            $crontabTask->description = "Отправка лидов в очередь";
            $crontabTask->save();
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_CALL_CENTER_SEND_REQUESTS_TO_QUEUE]);
        if ($crontabTask instanceof CrontabTask) {
            $crontabTask->delete();
        }
    }
}
