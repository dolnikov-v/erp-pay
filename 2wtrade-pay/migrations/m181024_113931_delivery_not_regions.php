<?php
use app\components\CustomMigration as Migration;

/**
 * Class m181024_113931_delivery_not_regions
 */
class m181024_113931_delivery_not_regions extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('delivery_not_regions', [
            'id' => $this->primaryKey(),
            'delivery_id' => $this->integer(),
            'type' => $this->string(10),
            'region' => $this->string(100),
        ]);

        $this->addForeignKey(null, 'delivery_not_regions', 'delivery_id', 'delivery', 'id', self::CASCADE, self::CASCADE);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('delivery_not_regions');
    }
}
