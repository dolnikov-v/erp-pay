<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m160815_092608_fix_report_insert_debts
 */
class m160815_092608_fix_report_insert_debts extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        /** @var CrontabTask $task */
        $task = CrontabTask::find()
            ->byName('reports_insert_debts')
            ->one();

        if ($task) {
            $task->name = 'report_insert_debts';
            $task->description = 'Заполнение отчетов по дебиторским задолженностям.';
            $task->save();
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        /** @var CrontabTask $task */
        $task = CrontabTask::find()
            ->byName('report_insert_debts')
            ->one();

        if ($task) {
            $task->name = 'reports_insert_debts';
            $task->description = 'Заполнение таблицы отчетов report_debts';
            $task->save();
        }
    }
}
