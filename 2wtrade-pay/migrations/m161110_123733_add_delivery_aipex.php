<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryApiClass;
use app\models\Country;

/**
 * Class m161110_123733_add_delivery_aipex
 */
class m161110_123733_add_delivery_aipex extends Migration
{
    /**
     * @inheritdoc
     *
     */
    public function safeUp()
    {
        //добавление DeliveryApiClass
        if (DeliveryApiClass::find()->where(
            ['name' => 'AipexApi', 'class_path' => '/aipex-api/src/AipexApi.php']
        )->exists()
        ) {
            $apiClass = DeliveryApiClass::findOne(
                ['name' => 'AipexApi', 'class_path' => '/aipex-api/src/AipexApi.php']
            );
        } else {
            $apiClass = new DeliveryApiClass();
        }
        $apiClass->name = 'AipexApi';
        $apiClass->class_path = '/aipex-api/src/AipexApi.php';
        $apiClass->active = '1';
        $apiClass->created_at = time();
        $apiClass->updated_at = time();
        $apiClass->save();

        if (Country::find()->where(
            [
                'name' => 'Индия',
                'char_code' => 'IN'
            ]
        )->exists()
        ) {
            $country = Country::find()->where(
                [
                    'name' => 'Индия',
                    'char_code' => 'IN'
                ]
            )->select('id')->one();
        } else {
            //нет страны
            return false;
        }


        if (Delivery::find()->where(
            [
                'name' => 'Aipex',
                'char_code' => 'AIP'
            ]
        )->exists()
        ) {
            $delivery = Delivery::findOne(
                [
                    'name' => 'Aipex',
                    'char_code' => 'AIP'
                ]
            );
        } else {
            $delivery = new Delivery();
        }
        $delivery->country_id = $country->id;
        $delivery->name = 'Aipex';
        $delivery->char_code = 'AIP';
        $delivery->api_class_id = $apiClass->id;
        $delivery->active = '1';
        $delivery->auto_sending = '0';
        $delivery->save(false);
        return true;
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        if (Country::find()->where(
            [
                'name' => 'Индия',
                'char_code' => 'IN'
            ]
        )->exists()
        ) {
            $country = Country::find()->where(
                [
                    'name' => 'Индия',
                    'char_code' => 'IN'
                ]
            )->select('id')->one();
        } else {
            //нет страны
            return false;
        }

        Delivery::findOne(
            [
                'name' => 'Aipex',
                'country_id' => $country->id
            ]
        )->delete();

        DeliveryApiClass::findOne([
                'name' => 'AipexApi',
                'class_path' => '/aipex-api/src/AipexApi.php'
            ])->delete();
        return true;
    }
}
