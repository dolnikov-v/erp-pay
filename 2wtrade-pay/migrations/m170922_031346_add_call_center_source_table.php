<?php
use app\components\CustomMigration as Migration;
use app\models\Source;
use app\modules\callcenter\models\CallCenter;

/**
 * Class m170922_031346_add_call_center_source_table
 */
class m170922_031346_add_call_center_source_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('call_center_source', [
            'call_center_id' => $this->integer()->notNull(),
            'source' => $this->string(64)->notNull(),
            'active' => $this->boolean()->defaultValue(1), // по умолчанию включено всё
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->addPrimaryKey(null, 'call_center_source', ['call_center_id', 'source']);

        $this->truncateTable('call_center_source');

        $data = CallCenter::find()->all();
        $arrayForInsert = [];
        if (is_array($data)) {
            foreach ($data as $item) {
                $sources = [
                    Source::SOURCE_ADCOMBO => 'adcombo',
                    Source::SOURCE_JEEMPO => 'jeempo',
                    Source::SOURCE_2WSTORE => '2wstore',
                    Source::SOURCE_DIAVITA => 'diavita',
                    Source::SOURCE_WTRADE_TRASH => 'wtrade_trash',
                    Source::SOURCE_2WCALL => '2wcall',
                    Source::SOURCE_2WDISTR => '2wDistr',
                    Source::SOURCE_WTRADE_POST_SALE => 'wtrade_post_sale',
                    Source::SOURCE_TEST_1 => 'test_1',
                    Source::SOURCE_MY_EPC => 'myEPC',
                    Source::SOURCE_TEST_3 => 'test_3',
                    Source::SOURCE_TEST_4 => 'test_4',
                    Source::SOURCE_TEST_5 => 'test_5',
                    Source::SOURCE_MERCADOLIBRE => 'mercadolibre',
                    Source::SOURCE_CALL_CENTER => 'call_center',
                ];
                if (is_array($sources)) {
                    foreach ($sources as $sourceItem)
                        $arrayForInsert[] = [
                            'call_center_id' => $item->id,
                            'source' => $sourceItem,
                            'active' => 1,
                        ];
                }
            }
        }

        Yii::$app->db->createCommand()->batchInsert(
            'call_center_source',
            [
                'call_center_id',
                'source',
                'active',
            ],
            $arrayForInsert
        )->execute();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->truncateTable('call_center_source');
        $this->dropTable('call_center_source');
    }
}
