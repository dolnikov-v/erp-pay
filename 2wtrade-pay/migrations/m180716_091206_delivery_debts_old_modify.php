<?php
use app\components\CustomMigration as Migration;
use app\modules\report\models\ReportDeliveryDebtsOld;

/**
 * Class m180716_091206_delivery_debts_old_modify
 */
class m180716_091206_delivery_debts_old_modify extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropColumn('report_delivery_debts_old', 'id');
        $this->dropColumn('report_delivery_debts_old', 'time_from');
        $this->dropColumn('report_delivery_debts_old', 'time_to');
        $this->dropColumn('report_delivery_debts_old', 'sum_total');
        $this->dropColumn('report_delivery_debts_old', 'sum_total_bucks');

        $this->addColumn('report_delivery_debts_old', 'month', $this->date());
        $this->addColumn('report_delivery_debts_old', 'data', $this->text());

        $this->delete('report_delivery_debts_old');
        $this->dropForeignKey($this->getFkName('report_delivery_debts_old', 'delivery_id'), 'report_delivery_debts_old');
        $this->dropIndex($this->getFkName('report_delivery_debts_old', 'delivery_id'), 'report_delivery_debts_old');
        $this->addPrimaryKey($this->getPkName('report_delivery_debts_old', [
            'delivery_id',
            'month'
        ]), 'report_delivery_debts_old', ['delivery_id', 'month']);
        $this->addForeignKey($this->getFkName('report_delivery_debts_old', 'delivery_id'), 'report_delivery_debts_old', 'delivery_id', 'delivery', 'id', self::CASCADE, self::CASCADE);

        $this->dropForeignKey($this->getFkName(ReportDeliveryDebtsOld::tableName(), 'delivery_id'), ReportDeliveryDebtsOld::tableName());

        $this->dropPrimaryKey($this->getPkName(ReportDeliveryDebtsOld::tableName(), [
            'delivery_id',
            'month'
        ]), ReportDeliveryDebtsOld::tableName());

        $this->addColumn(ReportDeliveryDebtsOld::tableName(), 'id', $this->primaryKey());
        $this->addColumn(ReportDeliveryDebtsOld::tableName(), 'created_at', $this->integer());

        $this->update(ReportDeliveryDebtsOld::tableName(), [
            'created_at' => time(),
        ]);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

        $this->dropForeignKey($this->getFkName('report_delivery_debts_old', 'delivery_id'), 'report_delivery_debts_old');
        $this->dropPrimaryKey($this->getPkName('report_delivery_debts_old', [
            'delivery_id',
            'month'
        ]), 'report_delivery_debts_old');
        $this->dropColumn('report_delivery_debts_old', 'month');
        $this->dropColumn('report_delivery_debts_old', 'data');

        $this->addColumn('report_delivery_debts_old', 'id', $this->primaryKey());
        $this->alterColumn('report_delivery_debts_old', 'delivery_id', $this->integer()->after('id'));
        $this->addForeignKey($this->getFkName('report_delivery_debts_old', 'delivery_id'), 'report_delivery_debts_old', 'delivery_id', 'delivery', 'id', self::CASCADE, self::CASCADE);
        $this->addColumn('report_delivery_debts_old', 'time_from', $this->integer());
        $this->addColumn('report_delivery_debts_old', 'time_to', $this->integer());
        $this->addColumn('report_delivery_debts_old', 'sum_total', $this->double());
        $this->addColumn('report_delivery_debts_old', 'sum_total_bucks', $this->double());


        $this->dropColumn(ReportDeliveryDebtsOld::tableName(), 'id');
        $this->addPrimaryKey($this->getPkName(ReportDeliveryDebtsOld::tableName(), [
            'delivery_id',
            'month'
        ]), ReportDeliveryDebtsOld::tableName(), ['delivery_id', 'month']);
        $this->dropColumn(ReportDeliveryDebtsOld::tableName(), 'created_at');


    }
}
