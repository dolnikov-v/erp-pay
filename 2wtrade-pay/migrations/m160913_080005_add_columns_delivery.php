<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160913_080005_add_columns_delivery
 */
class m160913_080005_add_columns_delivery extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('delivery', 'delivered_from', $this->integer(11)->defaultValue(null)->after('workflow_id'));
        $this->addColumn('delivery', 'delivered_to', $this->integer(11)->defaultValue(null)->after('delivered_from'));
        $this->addColumn('delivery', 'paid_from', $this->integer(11)->defaultValue(null)->after('delivered_to'));
        $this->addColumn('delivery', 'paid_to', $this->integer(11)->defaultValue(null)->after('paid_from'));
        $this->addColumn('delivery', 'is_keep', $this->integer(1)->defaultValue(0)->after('paid_to'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('delivery', 'delivered_from');
        $this->dropColumn('delivery', 'delivered_to');
        $this->dropColumn('delivery', 'paid_from');
        $this->dropColumn('delivery', 'paid_to');
        $this->dropColumn('delivery', 'is_keep');
    }
}
