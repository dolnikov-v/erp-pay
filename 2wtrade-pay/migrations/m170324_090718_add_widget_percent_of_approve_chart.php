<?php

use app\components\CustomMigration as Migration;
use app\modules\widget\models\WidgetType;
use app\modules\widget\models\WidgetRole;

/**
 * Class m170324_090718_add_widget_percent_of_approve_chart
 */
class m170324_090718_add_widget_percent_of_approve_chart extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $code = 'percent_of_approves_chart';

        $roles = [
            'business_analyst',
            'country.curator',
            'callcenter.manager',
        ];

        $this->insert(WidgetType::tableName(), [
            'code' => $code,
            'name' => 'Процент по апрувам',
            'status' => WidgetType::STATUS_ACTIVE,
            'by_country' => WidgetType::BY_COUNTRY,
        ]);

        $type = WidgetType::find()
            ->where(['code' => $code])
            ->one();

        foreach ($roles as $role) {
            $this->insert(WidgetRole::tableName(), [
                'role' => $role,
                'type_id' => $type->id
            ]);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $code = 'percent_of_approves_chart';
        $roles = [
            'business_analyst',
            'country.curator',
            'callcenter.manager',
        ];

        $type = WidgetType::find()
            ->where(['code' => $code])
            ->one();

        foreach ($roles as $role) {
            $this->delete(WidgetRole::tableName(), [
                'role' => $role,
                'type_id' => $type->id
            ]);
        }

        $this->delete(WidgetType::tableName(), ['code' => $code]);
    }
}
