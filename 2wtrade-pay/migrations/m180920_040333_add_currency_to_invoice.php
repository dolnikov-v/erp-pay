<?php
use app\components\CustomMigration as Migration;

/**
 * Class m180920_040333_add_currency_to_invoice
 */
class m180920_040333_add_currency_to_invoice extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('invoice', 'currency_id', $this->integer()->after('contract_id'));
        $this->addColumn('invoice_currency', 'invoice_currency_id', $this->integer()->after('currency_id'));

        $usdCurrencyId = (new \yii\db\Query())->from('currency')->where(['char_code' => 'USD'])->select('id')->scalar();
        $this->update('invoice', ['currency_id' => $usdCurrencyId]);
        $this->update('invoice_currency', ['invoice_currency_id' => $usdCurrencyId]);

        $this->alterColumn('invoice', 'currency_id', $this->integer()->notNull());
        $this->alterColumn('invoice_currency', 'invoice_currency_id', $this->integer()->notNull());
        $this->alterColumn('invoice_currency', 'rate', $this->decimal(12, 7));

        $this->addForeignKey($this->getFkName('invoice', 'currency_id'), 'invoice', 'currency_id', 'currency', 'id', self::CASCADE, self::CASCADE);
        $this->addForeignKey($this->getFkName('invoice_currency', 'invoice_currency_id'), 'invoice_currency', 'invoice_currency_id', 'currency', 'id', self::CASCADE, self::CASCADE);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey($this->getFkName('invoice', 'currency_id'), 'invoice');
        $this->dropForeignKey($this->getFkName('invoice_currency', 'invoice_currency_id'), 'invoice_currency');
        $this->dropColumn('invoice', 'currency_id');
        $this->dropColumn('invoice_currency', 'invoice_currency_id');
        $this->alterColumn('invoice_currency', 'rate', $this->decimal());
    }
}
