<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryApiClass;

/**
 * Class m160927_065450_add_api_classes_ontime360_blueex
 */
class m160927_065450_add_api_classes_ontime360_blueex extends Migration
{
    public function safeUp()
    {
        $class = new DeliveryApiClass();
        $class->name = 'Ontime360Api';
        $class->class_path = '/ontime360-api/src/ontime360Api.php';
        $class->save();

        $class = new DeliveryApiClass();
        $class->name = 'BlueexApi';
        $class->class_path = '/blueEx-api/src/blueExApi.php';
        $class->save();


    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        DeliveryApiClass::find()
            ->where([
                        'name' => 'Ontime360Api',
                        'class_path' => '/ontime360-api/src/ontime360Api.php'
                    ])
            ->one()
            ->delete();

        DeliveryApiClass::find()
            ->where([
                        'name' => 'BlueexApi',
                        'class_path' => '/blueEx-api/src/blueExApi.php'
                    ])
            ->one()
            ->delete();

    }
}
