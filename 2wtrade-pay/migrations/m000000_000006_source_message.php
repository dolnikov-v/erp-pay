<?php
use app\components\CustomMigration as Migration;
use app\modules\i18n\models\SourceMessage;

class m000000_000006_source_message extends Migration
{
    public function safeUp()
    {
        $this->createTable(SourceMessage::tableName(), [
            'id' => $this->primaryKey(),
            'category' => $this->string(32),
            'message' => $this->text()
        ], $this->tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable(SourceMessage::tableName());
    }
}
