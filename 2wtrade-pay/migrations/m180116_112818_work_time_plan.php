<?php
use app\components\PermissionMigration as Migration;

use app\modules\salary\models\WorkTimePlan;

/**
 * Class m180116_112818_work_time_plan
 */
class m180116_112818_work_time_plan extends Migration
{

    protected $permissions = [
        'salary.worktimeplan.gettimes',
        'salary.worktimeplan.getshifts',
    ];

    protected $roles = [
        'admin' => [
            'salary.worktimeplan.gettimes',
            'salary.worktimeplan.getshifts'
        ],
        'callcenter.hr' => [
            'salary.worktimeplan.gettimes',
            'salary.worktimeplan.getshifts'
        ],
        'controller.analyst' => [
            'salary.worktimeplan.gettimes',
            'salary.worktimeplan.getshifts'
        ],
        'finance.director' => [
            'salary.worktimeplan.gettimes',
            'salary.worktimeplan.getshifts'
        ],
        'project.manager' => [
            'salary.worktimeplan.gettimes',
            'salary.worktimeplan.getshifts'
        ],
        'salaryproject.clerk' => [
            'salary.worktimeplan.gettimes',
            'salary.worktimeplan.getshifts'
        ]
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(WorkTimePlan::tableName(), 'working_shift_id', $this->integer()->after('person_id'));
        parent::safeUp();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(WorkTimePlan::tableName(), 'working_shift_id');
        parent::safeDown();
    }
}
