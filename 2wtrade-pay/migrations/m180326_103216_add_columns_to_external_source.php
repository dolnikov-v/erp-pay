<?php

use app\modules\catalog\models\ExternalSource;
use yii\db\Migration;

/**
 * Handles adding columns to table `external_source`.
 */
class m180326_103216_add_columns_to_external_source extends Migration
{
    private static $translations = [
        "Url исходящей очереди Амазон" => "Url outbound queue Amazon",
        "Url входящей очереди Amazon" => "Url of the incoming Amazon queue",
    ];

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn(ExternalSource::tableName(), 'outbound_queue_amazon_url', $this->string());
        $this->addColumn(ExternalSource::tableName(), 'incoming_queue_amazon_url', $this->string());

        foreach(self::$translations as $ruText => $enText) {
            $this->insert('i18n_source_message', ['category' => 'common', 'message' => $ruText]);
            $id = Yii::$app->db->getLastInsertID();
            $this->insert('i18n_message', ['id' => $id, 'language' => 'en-US', 'translation' => $enText]);
        }

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn(ExternalSource::tableName(), 'outbound_queue_amazon_url');
        $this->dropColumn(ExternalSource::tableName(), 'incoming_queue_amazon_url');

        foreach (self::$translations as $ruText => $enText) {
            $this->delete('i18n_source_message', ['category' => 'common', 'message' => $ruText]);
        }
    }
}