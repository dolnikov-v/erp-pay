<?php

use yii\db\Migration;
use app\models\User;

/**
 * Handles adding language to table `user`.
 */
class m170303_063317_add_language_to_user extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn(User::tableName(), 'language', $this->string(8));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn(User::tableName(), 'language');
    }
}
