<?php
use app\components\PermissionMigration as Migration;

/**
 * Class m181016_083845_report_debts_getpayments
 */
class m181016_083845_report_debts_getpayments extends Migration
{
    protected $permissions = ['report.debts.getpayments'];

    protected $roles = [
        'admin' => ['report.debts.getpayments'],
        'auditor' => ['report.debts.getpayments'],
        'controller.analyst' => ['report.debts.getpayments'],
        'country.curator' => ['report.debts.getpayments'],
        'finance.director' => ['report.debts.getpayments'],
        'finance.manager' => ['report.debts.getpayments'],
        'project.manager' => ['report.debts.getpayments'],
        'quality.manager' => ['report.debts.getpayments'],
        'sales.director' => ['report.debts.getpayments'],
        'support.manager' => ['report.debts.getpayments'],
        'technical.director' => ['report.debts.getpayments'],
    ];
}
