<?php
use app\components\PermissionMigration as Migration;

/**
 * Class m171229_055608_office_pages
 */
class m171229_055608_office_pages extends Migration
{

    protected $permissions = [
        'salary.office.editgreeting',
        'salary.office.editexpenses',
        'salary.office.edittax',
        'salary.office.editdirection',
        'salary.office.editworking',
        'salary.office.editstaffing',
    ];

    protected $roles = [
        'admin' => [
            'salary.office.editgreeting',
            'salary.office.editexpenses',
            'salary.office.edittax',
            'salary.office.editdirection',
            'salary.office.editworking',
        ],
        'callcenter.hr' => [
            'salary.office.editgreeting',
            'salary.office.editexpenses',
            'salary.office.edittax',
            'salary.office.editdirection',
            'salary.office.editworking',
        ],
        'controller.analyst' => [
            'salary.office.editgreeting',
            'salary.office.editexpenses',
            'salary.office.edittax',
            'salary.office.editdirection',
            'salary.office.editworking',
        ],
        'country.curator' => [
            'salary.office.editgreeting',
            'salary.office.editexpenses',
            'salary.office.edittax',
            'salary.office.editdirection',
            'salary.office.editworking',
        ],
        'project.manager' => [
            'salary.office.editstaffing',
            'view_staffing_salary',
        ],
        'salaryproject.clerk' => [
            'salary.office.editgreeting',
            'salary.office.editexpenses',
            'salary.office.edittax',
            'salary.office.editdirection',
            'salary.office.editworking',
        ],
    ];
}
