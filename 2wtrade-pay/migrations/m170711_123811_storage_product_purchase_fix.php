<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170711_123811_storage_product_purchase_fix
 */
class m170711_123811_storage_product_purchase_fix extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('storage_product_purchase', 'limit_count', $this->decimal(15,2)->after('limit_days'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('storage_product_purchase', 'limit_count');
    }
}
