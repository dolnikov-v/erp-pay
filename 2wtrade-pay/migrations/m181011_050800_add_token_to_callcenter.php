<?php
use app\components\CustomMigration as Migration;

/**
 * Class m181011_050800_add_token_to_callcenter
 */
class m181011_050800_add_token_to_callcenter extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('call_center', 'token', $this->string(255)->after('api_pid'));
        $this->update('call_center', ['token' => sha1('2wtrade')], ['is_amazon_query' => 1]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('call_center', 'token');
    }
}
