<?php
use app\components\CustomMigration as Migration;
use app\modules\salary\models\WorkingShift;
use app\modules\salary\models\Office;
use app\modules\salary\models\Person;
use yii\db\Query;

/**
 * Class m170711_062245_call_center_working_shift
 */
class m170711_062245_call_center_working_shift extends Migration
{

    const ROLES = [
        'country.curator',
        'callcenter.manager',
        'callcenter.hr',
    ];

    const RULES = [
        'callcenter.workingshift.edit',
        'callcenter.workingshift.delete',
    ];


    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        $query = new Query();

        foreach (self::RULES as $rule) {

            $is_rule = $query->select('*')->from($this->authManager->itemTable)
                ->where(['name' => $rule, 'type' => 2])
                ->one();

            if (!$is_rule) {
                $this->insert($this->authManager->itemTable, [
                    'name' => $rule,
                    'description' => $rule,
                    'type' => 2,
                    'created_at' => time(),
                    'updated_at' => time()]);
            }

            foreach (self::ROLES as $role) {
                $is_can = $query->select('*')->from($this->authManager->itemChildTable)->where([
                    'parent' => $role,
                    'child' => $rule
                ])->one();

                if (!$is_can) {
                    $this->insert($this->authManager->itemChildTable, [
                        'parent' => $role,
                        'child' => $rule
                    ]);
                }
            }
        }


        $this->createTable(WorkingShift::tableName(), [
            'id' => $this->primaryKey(),
            'office_id' => $this->integer(),
            'name' => $this->string(),
            'lunch_time' => $this->string(),
            'working_mon' => $this->string(),
            'working_tue' => $this->string(),
            'working_wed' => $this->string(),
            'working_thu' => $this->string(),
            'working_fri' => $this->string(),
            'working_sat' => $this->string(),
            'working_sun' => $this->string(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addForeignKey('fk_call_center_working_shift_office_id', WorkingShift::tableName(), 'office_id', Office::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addColumn(Person::tableName(), 'working_shift_id', $this->integer());
        $this->addForeignKey('fk_call_center_person_working_shift_id', Person::tableName(), 'working_shift_id', WorkingShift::tableName(), 'id', self::SET_NULL, self::NO_ACTION);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach (self::RULES as $rule) {
            foreach (self::ROLES as $role) {
                $this->delete($this->authManager->itemChildTable, [
                    'parent' => $role,
                    'child' => $rule
                ]);
            }
        }

        $this->dropForeignKey('fk_call_center_person_working_shift_id', Person::tableName());
        $this->dropColumn(Person::tableName(), 'working_shift_id');
        $this->dropTable(WorkingShift::tableName());
    }
}
