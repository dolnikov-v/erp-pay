<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170717_105007_add_fields_to_personal_import
 */
class m170717_105007_add_fields_to_personal_import extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('call_center_person_import', 'cell_email', $this->string(255));
        $this->addColumn('call_center_person_import', 'cell_skype', $this->string(255));

        $this->addColumn('call_center_person_import_data', 'person_email', $this->string(255));
        $this->addColumn('call_center_person_import_data', 'person_skype', $this->string(255));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('call_center_person_import', 'cell_email');
        $this->dropColumn('call_center_person_import', 'cell_skype');

        $this->dropColumn('call_center_person_import_data', 'person_email');
        $this->dropColumn('call_center_person_import_data', 'person_skype');
    }
}
