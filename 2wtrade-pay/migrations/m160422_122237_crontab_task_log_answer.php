<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160422_122237_crontab_task_log_answer
 */
class m160422_122237_crontab_task_log_answer extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('crontab_task_log_answer', [
            'id' => $this->primaryKey(),
            'log_id' => $this->integer()->notNull(),
            'answer' => $this->text(),
            'status' => "ENUM('fail', 'success') DEFAULT 'fail'",
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(null, 'crontab_task_log_answer', 'log_id', 'crontab_task_log', 'id', self::CASCADE, self::RESTRICT);
        $this->createIndex(null, 'crontab_task_log_answer', 'status');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('crontab_task_log_answer');
    }
}
