<?php
use app\components\CustomMigration as Migration;
use app\models\UserActionLog;
use app\models\UserActionLogQuery;

class m000000_000010_user_action_log_query extends Migration
{
    public function safeUp()
    {
        $this->createTable(UserActionLogQuery::tableName(), [
            'id' => $this->primaryKey(),
            'action_log_id' => $this->integer()->notNull(),
            'query' => $this->string(1024)->notNull(),
            'created_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(null, UserActionLogQuery::tableName(), 'action_log_id', UserActionLog::tableName(), 'id', 'CASCADE', 'RESTRICT');
    }

    public function safeDown()
    {
        $this->dropTable(UserActionLogQuery::tableName());
    }
}
