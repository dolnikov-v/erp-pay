<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170801_073254_add_permission_change_status_by_limited_user
 */
class m170801_073254_add_permission_change_status_by_limited_user extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}',
        [
            'name'=>'order.index.changestatusbylimiteduser',
            'type' => '2',
            'description' => 'order.index.changestatusbylimiteduser',
            'created_at' => time(),
            'updated_at' => time()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->authManager->itemTable, ['name' => 'order.index.changestatusbylimiteduser']);
    }
}