<?php
use app\components\DeliveryChargesCalculatorMigration;

/**
 * Class m180305_063812_mll_calculator
 */
class m180305_063812_mls_calculator extends DeliveryChargesCalculatorMigration
{

    /**
     * @return array
     */
    protected static function getCalculators()
    {
        return [
            'MLS' => 'app\modules\delivery\components\charges\mls\MLS',
        ];
    }
}
