<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160923_055418_report_debts_add_foreign_keys
 */
class m160923_055418_report_debts_add_foreign_keys extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addForeignKey(null, 'report_debts', 'country_id', 'country', 'id');
        $this->addForeignKey(null, 'report_debts', 'delivery_id', 'delivery', 'id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey($this->getFkName('report_debts', 'country_id'), 'report_debts');
        $this->dropForeignKey($this->getFkName('report_debts', 'delivery_id'), 'report_debts');
    }
}
