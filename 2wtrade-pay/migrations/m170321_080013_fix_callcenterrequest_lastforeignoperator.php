<?php
use app\components\CustomMigration as Migration;
use app\modules\callcenter\models\CallCenterRequest;

/**
 * Class m1700321_080013_fix_callcenterrequest_lastforeignoperator
 */
class m170321_080013_fix_callcenterrequest_lastforeignoperator extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->update(CallCenterRequest::tableName(), ['last_foreign_operator' => NULL], ['last_foreign_operator' => -1]);
        $this->update(CallCenterRequest::tableName(), ['last_foreign_operator' => NULL], ['last_foreign_operator' => 0]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

    }
}
