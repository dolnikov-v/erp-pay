<?php
use app\components\PermissionMigration as Migration;

/**
 * Class m180109_080521_office_type_can
 */
class m180109_080521_office_type_can extends Migration
{
    protected $permissions = [
        'office_view_type_call_center',
        'office_view_type_storage',
        'office_view_type_branch',
        'office_view_type_office',
    ];

    protected $roles = [
        'country.curator' => [
            'office_view_type_call_center',
            'office_view_type_storage',
            'office_view_type_branch',
            'office_view_type_office',
            'limit_call_center_persons_by_direction',
        ],
        'project.manager' => [
            'office_view_type_call_center',
            'office_view_type_storage',
            'office_view_type_branch',
            'office_view_type_office',
        ],
        'admin' => [
            'office_view_type_call_center',
            'office_view_type_storage',
            'office_view_type_branch',
            'office_view_type_office',
        ],
        'finance.director' => [
            'office_view_type_call_center',
            'office_view_type_storage',
            'office_view_type_branch',
            'office_view_type_office',
        ],
        'general.director' => [
            'office_view_type_call_center',
            'office_view_type_storage',
            'office_view_type_branch',
            'office_view_type_office',
        ],
        'development.director' => [
            'office_view_type_call_center',
            'office_view_type_storage',
            'office_view_type_branch',
            'office_view_type_office',
        ],
        'executive.director' => [
            'office_view_type_call_center',
            'office_view_type_storage',
            'office_view_type_branch',
            'office_view_type_office',
        ],
        'salaryproject.clerk' => [
            'office_view_type_call_center',
            'office_view_type_storage',
            'office_view_type_branch',
            'office_view_type_office',
        ],
        'storage.manager' => [
            'office_view_type_storage',
            'office_view_type_branch',
        ],
        'controller.analyst' => [
            'office_view_type_call_center',
            'office_view_type_storage',
            'office_view_type_branch',
        ],
        'callcenter.hr' => [
            'office_view_type_call_center',
        ],
        'callcenter.leader' => [
            'office_view_type_call_center',
        ],
        'callcenter.manager' => [
            'office_view_type_call_center',
        ],
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->delete($this->authManager->itemChildTable, [
            'child' => 'limit_call_center_persons_by_direction'
        ]);
        parent::safeUp();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        parent::safeDown();
    }
}
