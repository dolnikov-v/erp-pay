<?php
use app\components\CustomMigration as Migration;

/**
 * Class m161108_065343_add_enum_in_list
 */
class m161108_065343_add_enum_in_list extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('delivery_request', 'status', $this->enum(['pending', 'in_progress', 'done', 'error', 'done_error', 'in_list'])->defaultValue('pending'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('delivery_request', 'status', $this->enum(['pending', 'in_progress', 'done', 'error', 'done_error'])->defaultValue('pending'));
    }
}
