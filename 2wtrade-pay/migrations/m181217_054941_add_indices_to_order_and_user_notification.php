<?php
use app\components\CustomMigration as Migration;

/**
 * Class m181217_054941_add_indices_to_order_and_user_notification
 */
class m181217_054941_add_indices_to_order_and_user_notification extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createIndex($this->getIdxName('user_notification', ['user_id', 'trigger', 'read']), 'user_notification', ['user_id', 'trigger', 'read']);
        $this->createIndex($this->getIdxName('order', ['country_id', 'status_id']), 'order', ['country_id', 'status_id']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropIndex($this->getIdxName('order', ['country_id', 'status_id']), 'order');
        $this->dropIndex($this->getIdxName('user_notification', ['user_id', 'trigger', 'read']), 'user_notification');
    }
}
