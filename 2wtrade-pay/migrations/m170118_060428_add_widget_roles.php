<?php

use app\components\CustomMigration as Migration;
use app\modules\widget\models\WidgetType;
use yii\helpers\ArrayHelper;

/**
 * Class m170118_060428_add_widget_roles
 */
class m170118_060428_add_widget_roles extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $types = WidgetType::find()
            ->where(['code' => [
                'payback_advertising',
                'call_center_rating',
                'buy_out_goods'
            ]])
            ->indexBy('code')
            ->asArray()
            ->all();

        if ($type = ArrayHelper::getValue($types, 'buy_out_goods')) {

            $this->insert('{{%widget_role}}', [
                'role' => 'country.curator',
                'type_id' => $type['id'],
            ]);

            $this->insert('{{%widget_role}}', [
                'role' => 'business_analyst',
                'type_id' => $type['id'],
            ]);

        }

        if ($type = ArrayHelper::getValue($types, 'call_center_rating')) {

            $this->insert('{{%widget_role}}', [
                'role' => 'callcenter.manager',
                'type_id' => $type['id'],
            ]);

            $this->insert('{{%widget_role}}', [
                'role'=> 'business_analyst',
                'type_id' => $type['id'],
            ]);

        }

        if ($type = ArrayHelper::getValue($types, 'payback_advertising')) {

            $this->insert('{{%widget_role}}', [
                'role'=> 'business_analyst',
                'type_id' => $type['id'],
            ]);

        }

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

        $types = WidgetType::find()
            ->where(['code' => [
                'payback_advertising',
                'call_center_rating',
                'buy_out_goods'
            ]])
            ->indexBy('code')
            ->asArray()
            ->all();

        if ($type = ArrayHelper::getValue($types, 'buy_out_goods')) {

            $this->delete('{{%widget_role}}', [
                'role' => 'country.curator',
                'type_id' => $type['id'],
            ]);

            $this->delete('{{%widget_role}}', [
                'role' => 'business_analyst',
                'type_id' => $type['id'],
            ]);

        }

        if ($type = ArrayHelper::getValue($types, 'call_center_rating')) {

            $this->delete('{{%widget_role}}', [
                'role' => 'callcenter.manager',
                'type_id' => $type['id'],
            ]);

            $this->delete('{{%widget_role}}', [
                'role'=> 'business_analyst',
                'type_id' => $type['id'],
            ]);

        }

        if ($type = ArrayHelper::getValue($types, 'payback_advertising')) {

            $this->delete('{{%widget_role}}', [
                'role'=> 'business_analyst',
                'type_id' => $type['id'],
            ]);

        }
    }
}
