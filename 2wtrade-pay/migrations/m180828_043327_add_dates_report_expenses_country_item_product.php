<?php
use app\components\CustomMigration as Migration;
use app\modules\finance\models\ReportExpensesCountryItemProduct;

/**
 * Class m180828_043327_add_dates_report_expenses_country_item_product
 */
class m180828_043327_add_dates_report_expenses_country_item_product extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('report_expenses_country_item_product', 'date', $this->date());
        $this->addColumn('report_expenses_country_item_product', 'date_from', $this->date());
        $this->addColumn('report_expenses_country_item_product', 'date_to', $this->date());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('report_expenses_country_item_product', 'date');
        $this->dropColumn('report_expenses_country_item_product', 'date_from');
        $this->dropColumn('report_expenses_country_item_product', 'date_to');
    }
}
