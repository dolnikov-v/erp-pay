<?php
use app\components\CustomMigration as Migration;
use app\modules\order\models\OrderStatus;

/**
 * Class m170704_051434_status_trash_by_double
 */
class m170704_051434_status_trash_by_double extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert(OrderStatus::tableName(), ['sort' => 35, 'name' => 'Автотреш заказа по дублям', 'comment' => null, 'group' => 'source', 'type' => 'final']);
        $this->insert('i18n_source_message', ['category' => 'common', 'message' => 'Автотреш заказа по дублям']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete(OrderStatus::tableName(), ['name' => 'Автотреш заказа по дублям']);
    }
}
