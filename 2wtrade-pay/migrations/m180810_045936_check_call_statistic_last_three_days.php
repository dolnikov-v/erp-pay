<?php
use app\components\CustomMigration as Migration;
use app\models\Notification;
use app\models\NotificationRole;

/**
 * Class m180810_045936_check_call_statistic_last_three_days
 */
class m180810_045936_check_call_statistic_last_three_days extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $notification = new Notification();
        $notification->description = 'По стране {name} отсутствуют данные телефонии более 3-х дней';
        $notification->trigger = Notification::TRIGGER_CALL_CENTER_NO_STATISTIC_LAST_THREE_DAYS;
        $notification->type = Notification::TYPE_WARNING;
        $notification->group = Notification::GROUP_COMMON;
        $notification->active = Notification::ACTIVE;
        $notification->save(false);

        $notificationRole = new NotificationRole([
            'role' => 'admin',
            'trigger' => Notification::TRIGGER_CALL_CENTER_NO_STATISTIC_LAST_THREE_DAYS,
        ]);
        $notificationRole->save(false);
        $notificationRole = new NotificationRole([
            'role' => 'project.manager',
            'trigger' => Notification::TRIGGER_CALL_CENTER_NO_STATISTIC_LAST_THREE_DAYS,
        ]);
        $notificationRole->save(false);
        $notificationRole = new NotificationRole([
            'role' => 'technical.director',
            'trigger' => Notification::TRIGGER_CALL_CENTER_NO_STATISTIC_LAST_THREE_DAYS,
        ]);
        $notificationRole->save(false);
        $notificationRole = new NotificationRole([
            'role' => 'support.manager',
            'trigger' => Notification::TRIGGER_CALL_CENTER_NO_STATISTIC_LAST_THREE_DAYS,
        ]);
        $notificationRole->save(false);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete(NotificationRole::tableName(), ['trigger' => Notification::TRIGGER_CALL_CENTER_NO_STATISTIC_LAST_THREE_DAYS,]);
        $notification = Notification::findOne(['trigger' => Notification::TRIGGER_CALL_CENTER_NO_STATISTIC_LAST_THREE_DAYS]);
        $notification->delete();
    }
}
