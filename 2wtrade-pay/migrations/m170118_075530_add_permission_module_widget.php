<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170118_075530_add_permission_module_widget
 */
class m170118_075530_add_permission_module_widget extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}', [
            'name'=> 'widget.index.add',
            'type' => '2',
            'description' => 'widget.index.add',
            'created_at' => time(),
            'updated_at' => time()
        ]);

        $this->insert('{{%auth_item}}', [
            'name'=> 'widget.index.delete',
            'type' => '2',
            'description' => 'widget.index.delete',
            'created_at' => time(),
            'updated_at' => time()
        ]);

        $this->insert('{{%auth_item}}', [
            'name'=> 'widget.index.changeplace',
            'type' => '2',
            'description' => 'widget.index.changeplace',
            'created_at' => time(),
            'updated_at' => time()
        ]);



        $this->insert($this->authManager->itemChildTable, [
            'parent' => 'business_analyst',
            'child' => 'widget.index.add'
        ]);

        $this->insert($this->authManager->itemChildTable, [
            'parent' => 'business_analyst',
            'child' => 'widget.index.delete'
        ]);

        $this->insert($this->authManager->itemChildTable, [
            'parent' => 'business_analyst',
            'child' => 'widget.index.changeplace'
        ]);



        $this->insert($this->authManager->itemChildTable, [
            'parent' => 'callcenter.manager',
            'child' => 'widget.index.add'
        ]);

        $this->insert($this->authManager->itemChildTable, [
            'parent' => 'callcenter.manager',
            'child' => 'widget.index.delete'
        ]);

        $this->insert($this->authManager->itemChildTable, [
            'parent' => 'callcenter.manager',
            'child' => 'widget.index.changeplace'
        ]);



        $this->insert($this->authManager->itemChildTable, [
            'parent' => 'country.curator',
            'child' => 'widget.index.add'
        ]);

        $this->insert($this->authManager->itemChildTable, [
            'parent' => 'country.curator',
            'child' => 'widget.index.delete'
        ]);

        $this->insert($this->authManager->itemChildTable, [
            'parent' => 'country.curator',
            'child' => 'widget.index.changeplace'
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->authManager->itemChildTable, [
            'parent' => 'business_analyst',
            'child' => 'widget.index.add'
        ]);

        $this->delete($this->authManager->itemChildTable, [
            'parent' => 'business_analyst',
            'child' => 'widget.index.delete'
        ]);

        $this->delete($this->authManager->itemChildTable, [
            'parent' => 'business_analyst',
            'child' => 'widget.index.changeplace'
        ]);



        $this->delete($this->authManager->itemChildTable, [
            'parent' => 'callcenter.manager',
            'child' => 'widget.index.add'
        ]);

        $this->delete($this->authManager->itemChildTable, [
            'parent' => 'callcenter.manager',
            'child' => 'widget.index.delete'
        ]);

        $this->delete($this->authManager->itemChildTable, [
            'parent' => 'callcenter.manager',
            'child' => 'widget.index.changeplace'
        ]);



        $this->delete($this->authManager->itemChildTable, [
            'parent' => 'country.curator',
            'child' => 'widget.index.add'
        ]);

        $this->delete($this->authManager->itemChildTable, [
            'parent' => 'country.curator',
            'child' => 'widget.index.delete'
        ]);

        $this->delete($this->authManager->itemChildTable, [
            'parent' => 'country.curator',
            'child' => 'widget.index.changeplace'
        ]);


        $this->delete('{{%auth_item}}', ['name' => 'widget.index.add']);
        $this->delete('{{%auth_item}}', ['name' => 'widget.index.delete']);
        $this->delete('{{%auth_item}}', ['name' => 'widget.index.changeplace']);
    }
}
