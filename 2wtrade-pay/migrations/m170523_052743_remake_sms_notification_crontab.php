<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;
/**
 * Class m170523_052743_remake_sms_notification_crontab
 */
class m170523_052743_remake_sms_notification_crontab extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = CrontabTask::find()
            ->where(['name' => 'sms_notifier_send_sms_for_order'])
            ->one();

        $crontabTask->name = 'order_notifier_send_notifications';
        $crontabTask->description = 'Рассылка уведомлений клиентам';
        $crontabTask->save();

        $crontabTask = CrontabTask::find()
            ->where(['name' => 'sms_notifier_check_sms_status_for_order'])
            ->one();

        $crontabTask->name = 'order_notifier_check_notifications';
        $crontabTask->description = 'Обновление статусов заявок уведомлений';
        $crontabTask->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = CrontabTask::find()
            ->where(['name' => 'order_notifier_send_notifications'])
            ->one();

        $crontabTask->name = 'sms_notifier_send_sms_for_order';
        $crontabTask->description = 'Рассылка СМС уведомлений клиентам';
        $crontabTask->save();

        $crontabTask = CrontabTask::find()
            ->where(['name' => 'order_notifier_check_notifications'])
            ->one();

        $crontabTask->name = 'sms_notifier_check_sms_status_for_order';
        $crontabTask->description = 'Обновление статусов заявок смс уведомлений';
        $crontabTask->save();
    }
}
