<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170109_150045_zipcodes
 */
class m170109_150045_zipcodes extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('delivery', 'zipcodes', $this->string(150000));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('delivery', 'zipcodes', $this->string(10240));
    }
}
