<?php
use app\components\DeliveryChargesCalculatorMigration as Migration;

/**
 * Class m171129_101326_add_charges_calculators
 */
class m171129_101326_add_charges_calculators extends Migration
{
    /**
     * @return array
     */
    protected static function getCalculators()
    {
        return [
            'SkynetAE' => 'app\modules\delivery\components\charges\skynet\SkynetAE',
            'YST' => 'app\modules\delivery\components\charges\yst\YST',
            'PDHS' => 'app\modules\delivery\components\charges\pdhs\PDHS',
            'Leopard' => 'app\modules\delivery\components\charges\leopard\Leopard',
        ];
    }
}
