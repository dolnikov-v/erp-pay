<?php

use app\models\Product;
use app\models\Source;
use app\models\SourceProduct;
use app\components\CustomMigration as Migration;

/**
 * Handles the creation for table `source_product`.
 */
class m170629_094141_create_source_product extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable(SourceProduct::tableName(), [
            'id' => $this->primaryKey(),
            'source_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
            'active' => $this->boolean()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->createIndex(null, SourceProduct::tableName(), ['source_id', 'product_id'], $unique = true);
        $this->addForeignKey(null, SourceProduct::tableName(), 'source_id', Source::tableName(), 'id', Migration::CASCADE, Migration::CASCADE);
        $this->addForeignKey(null, SourceProduct::tableName(), 'product_id', Product::tableName(), 'id', Migration::CASCADE, Migration::CASCADE);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable(SourceProduct::tableName());
    }
}
