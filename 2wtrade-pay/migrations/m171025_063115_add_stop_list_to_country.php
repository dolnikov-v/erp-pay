<?php
use app\components\CustomMigration as Migration;

/**
 * Class m171025_063115_add_stop_list_to_country
 */
class m171025_063115_add_stop_list_to_country extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('country', 'is_stop_list', $this->boolean()->defaultValue(0));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('country', 'is_stop_list');
    }
}
