<?php
use app\components\CustomMigration as Migration;
use app\modules\widget\models\WidgetType;
use app\modules\widget\models\WidgetRole;
/**
 * Class m170124_050822_add_widget_type_top_deliveries
 */
class m170124_050822_add_widget_type_top_deliveries extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert(WidgetType::tableName(), [
            'code' => 'top_deliveries',
            'name' => 'Топ курьерок',
            'status' => WidgetType::STATUS_ACTIVE,
        ]);

        $type = WidgetType::find()
            ->where(['code' => 'top_deliveries'])
            ->one();

        $this->insert(WidgetRole::tableName(), [
            'role' => 'business_analyst',
            'type_id' => $type->id,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $type = WidgetType::find()
            ->where(['code' => 'top_deliveries'])
            ->one();

        $this->delete(WidgetRole::tableName(), [
            'role' => 'business_analyst',
            'type_id' => $type->id,
        ]);

        $this->delete(WidgetType::tableName(), ['code' => 'top_deliveries']);
    }
}
