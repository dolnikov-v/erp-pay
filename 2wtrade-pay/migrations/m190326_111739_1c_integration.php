<?php
use app\components\CustomMigration as Migration;

/**
 * Class m190326_111739_add_cron_task_for_1c_integration
 */
class m190326_111739_1c_integration extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('country', 'sink1c', $this->boolean()->defaultValue(false));
        $this->createIndex(null,'country', 'sink1c');
        $this->update('country', ['sink1c' => true], ['id' => 67]);
        $this->insert('crontab_task', ['name' => 'send_to_1c_changes', 'description' => 'Отправка изменений в 1С']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete('crontab_task', ['name' => 'send_to_1c_changes']);
        $this->dropColumn('country', 'sink1c');
    }
}
