<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryApiClass;
/**
 * Class m160924_063035_add_row_delivery_class_skynet_api
 */
class m160924_063035_add_row_delivery_class_skynet_api extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $class = new DeliveryApiClass();
        $class->name = 'SkyNetApi';
        $class->class_path = '/skynet-api/src/skyNetApi.php';
        $class->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        DeliveryApiClass::find()
            ->where([
                        'name' => 'SkyNetApi',
                        'class_path' => '/skynet-api/src/skyNetApi.php'
                    ])
            ->one()
            ->delete();
    }
}
