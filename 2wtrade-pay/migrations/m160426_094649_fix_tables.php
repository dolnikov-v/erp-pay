<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160426_094649_fix_tables
 */
class m160426_094649_fix_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->renameColumn('order', 'customer_phone_add', 'customer_mobile');
        $this->renameColumn('crontab_task_log_answer', 'input_data', 'data');
        $this->renameColumn('crontab_task_log_answer', 'output_data', 'answer');

        $this->dropForeignKey($this->getFkName('storage_product', 'landing_id'), 'storage_product');
        $this->dropColumn('storage_product', 'landing_id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->addColumn('storage_product', 'landing_id', $this->integer()->defaultValue(null) . ' AFTER `product_id`');
        $this->addForeignKey(null, 'storage_product', 'landing_id', 'landing', 'id', self::SET_NULL, self::RESTRICT);

        $this->renameColumn('crontab_task_log_answer', 'answer', 'output_data');
        $this->renameColumn('crontab_task_log_answer', 'data', 'input_data');
        $this->renameColumn('order', 'customer_mobile', 'customer_phone_add');
    }
}
