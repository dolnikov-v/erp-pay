<?php
use app\components\CustomMigration as Migration;

/**
 * Class m180306_085131_delivery_report_record_error
 */
class m180306_085131_delivery_report_record_error extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('delivery_report_record_error', [
            'delivery_report_record_id' => $this->integer(),
            'error_id' => $this->integer()->notNull(),
            'values' => $this->text()
        ], $this->tableOptions);

        $this->addPrimaryKey('pk_delivery_report_record_error_record_id_error_id', 'delivery_report_record_error', [
            'delivery_report_record_id',
            'error_id'
        ]);
        $this->addForeignKey($this->getFkName('delivery_report_record_error', 'delivery_report_record_id'), 'delivery_report_record_error', 'delivery_report_record_id', 'delivery_report_record', 'id', self::CASCADE, self::CASCADE);
        $this->delete('delivery_report_record_error');
        $query = new \yii\db\Query();
        $query->from('delivery_report_record');
        $query->select(['id', 'error_log']);
        foreach ($query->batch(100) as $records) {
            $inserts = [];
            foreach ($records as $record) {
                $errors = json_decode($record['error_log'], true);
                if ($errors) {
                    foreach ($errors as $error) {
                        $inserts[] = [
                            'delivery_report_record_id' => $record['id'],
                            'error_id' => $error['type'],
                            'values' => json_encode($error['values'], JSON_UNESCAPED_UNICODE)
                        ];
                    }
                }
            }
            if ($inserts) {
                $this->batchInsert('delivery_report_record_error', [
                    'delivery_report_record_id',
                    'error_id',
                    'values'
                ], $inserts);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('delivery_report_record_error');
    }
}
