<?php
use app\components\PermissionMigration as Migration;
use app\modules\salary\models\Office;

/**
 * Class m180109_094018_office_technical_equipment
 */
class m180109_094018_office_technical_equipment extends Migration
{

    protected $permissions = [
    ];

    protected $roles = [
        'admin' => [
            'salary.office.editsystem'
        ],
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Office::tableName(), 'technical_equipment', $this->integer(1)->defaultValue(0));

        parent::safeUp();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Office::tableName(), 'technical_equipment');

        parent::safeDown();
    }
}
