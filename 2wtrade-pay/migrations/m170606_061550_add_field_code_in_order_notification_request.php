<?php
use app\components\CustomMigration as Migration;
use app\modules\order\models\OrderNotificationRequest;

/**
 * Class m170606_061550_add_field_code_in_order_notification_request
 */
class m170606_061550_add_field_code_in_order_notification_request extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(OrderNotificationRequest::tableName(), 'code', $this->string(4));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(OrderNotificationRequest::tableName(), 'code');
    }
}
