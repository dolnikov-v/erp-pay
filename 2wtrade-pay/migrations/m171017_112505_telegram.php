<?php
use app\components\CustomMigration as Migration;
use app\models\UserNotificationSetting;
use app\models\User;
use app\modules\administration\models\CrontabTask;
use app\models\UserNotificationQueue;

/**
 * Class m171017_112505_telegram
 */
class m171017_112505_telegram extends Migration
{

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(UserNotificationSetting::tableName(), 'telegram_active', $this->smallInteger()->defaultValue(0)->after('sms_active'));

        $this->addColumn(User::tableName(), 'telegram_chat_id', $this->string());

        $this->alterColumn(UserNotificationQueue::tableName(), 'type_sending', $this->string());

        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_GET_TELEGRAM_CHATS]);
        if (!$crontabTask) {
            $crontabTask = new CrontabTask();
            $crontabTask->name = CrontabTask::TASK_GET_TELEGRAM_CHATS;
            $crontabTask->description = "Telegram получить список чатов (пользователей) бота";
            $crontabTask->save();
        }

        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_SEND_TELEGRAM_NOTIFICATIONS]);
        if (!$crontabTask) {
            $crontabTask = new CrontabTask();
            $crontabTask->name = CrontabTask::TASK_SEND_TELEGRAM_NOTIFICATIONS;
            $crontabTask->description = "Telegram отправка нотификаций пользователям";
            $crontabTask->save();
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(UserNotificationSetting::tableName(), 'telegram_active');

        $this->dropColumn(User::tableName(), 'telegram_chat_id');

        $this->alterColumn(UserNotificationQueue::tableName(), 'type_sending', $this->enum(['email', 'skype', 'sms']));

        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_GET_TELEGRAM_CHATS]);
        if ($crontabTask instanceof CrontabTask) {
            $crontabTask->delete();
        }

        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_SEND_TELEGRAM_NOTIFICATIONS]);
        if ($crontabTask instanceof CrontabTask) {
            $crontabTask->delete();
        }
    }
}
