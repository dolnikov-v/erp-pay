<?php
use app\components\CustomMigration as Migration;
use app\modules\widget\models\WidgetType;
use app\modules\widget\models\WidgetRole;

/**
 * Class m170306_060238_add_widget_old_orders_country_curators
 */
class m170306_060238_add_widget_old_orders_country_curators extends Migration
{
    public function safeUp()
    {
        $this->insert(WidgetType::tableName(), [
            'code' => 'old_orders_country_curators',
            'name' => 'Старые заказы менеджеров',
            'status' => WidgetType::STATUS_ACTIVE
        ]);

        $type = WidgetType::find()
            ->where(['code' => 'old_orders_country_curators'])
            ->one();

        $this->insert(WidgetRole::tableName(), [
            'role' => 'business_analyst',
            'type_id' => $type->id
        ]);

        $this->insert('i18n_source_message', ['category' => 'common', 'message' => 'Срок старых заказов (дней)']);
    }


    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $type = WidgetType::find()
            ->where(['code' => 'old_orders_country_curators'])
            ->one();

        $this->delete(WidgetRole::tableName(), [
            'role' => 'business_analyst',
            'type_id' => $type->id,
        ]);

        $this->delete(WidgetType::tableName(), ['code' => 'old_orders_country_curators']);
    }
}
