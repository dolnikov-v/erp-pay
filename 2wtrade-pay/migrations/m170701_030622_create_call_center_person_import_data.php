<?php

use app\components\CustomMigration;

/**
 * Handles the creation for table `call_center_person_import_data`.
 */
class m170701_030622_create_call_center_person_import_data extends CustomMigration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('call_center_person_import_data', [
            'id' => $this->primaryKey(),
            'import_id' => $this->integer()->notNull(),
            'row_number' => $this->integer()->notNull(),
            'user_login' => $this->string(100),
            'person_full_name' => $this->string(100),
            'person_position' => $this->string(100),
            'country' => $this->string(100),
            'person_salary' => $this->string(100),
            'person_salary_usd' => $this->string(100),
            'status' => $this->smallInteger()->defaultValue(0),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11),
        ]);

        $this->addForeignKey(null, 'call_center_person_import_data', 'import_id', 'call_center_person_import', 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $tableSchema = Yii::$app->db->schema->getTableSchema('call_center_person_import_data');
        if ($tableSchema !== null) {
            $this->dropTable('call_center_person_import_data');
        }
    }
}
