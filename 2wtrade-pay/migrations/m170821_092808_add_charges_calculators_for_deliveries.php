<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryChargesCalculator;

/**
 * Class m170821_092808_add_charges_calculators_for_deliveries
 */
class m170821_092808_add_charges_calculators_for_deliveries extends Migration
{
    protected static $chargesCalculators = [
        'ABL' => 'abl\ABL',
        'ACommerceID' => 'acommerce\AcommerceID',
        'ACommerceTH' => 'acommerce\AcommerceTH',
        'BlueEx' => 'blueex\BlueEx',
        'CorreosDePeru' => 'correosdeperu\CorreosDePeru',
        'CourierMate' => 'couriermate\CourierMate',
        'DTDC' => 'dtdc\DTDC',
        'Estafeta' => 'estafeta\Estafeta',
        'Etobee' => 'etobee\Etobee',
        'Fedex' => 'fedex\Fedex',
        'FullPhilippines' => 'fullphilippines\FullPhilippines',
        'GAL/LINEX' => 'gal\GAL',
        'GlobalExpress' => 'globalexpress\GlobalExpress',
        'NEX' => 'nex\Nex',
        'PCP' => 'pcp\PCP',
        'Skynet' => 'skynet\Skynet'
    ];

    protected static $permissions = [
        'delivery.control.editchargesvalues',
        'delivery.chargescalculator.index',
        'delivery.chargescalculator.view',
        'delivery.chargescalculator.activate',
        'delivery.chargescalculator.deactivate',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        foreach (self::$chargesCalculators as $name => $route) {
            $this->insert(DeliveryChargesCalculator::tableName(), [
                'name' => $name,
                'class_path' => 'app\modules\delivery\components\charges\\' . $route,
                'active' => 1,
                'created_at' => time(),
            ]);
        }

        foreach (self::$permissions as $permission) {
            $authPermission = $this->authManager->createPermission($permission);
            $authPermission->description = $permission;
            $this->authManager->add($authPermission);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach (self::$chargesCalculators as $name => $route) {
            $this->delete(DeliveryChargesCalculator::tableName(), ['name' => $name]);
        }

        foreach (self::$permissions as $permission) {
            $authPermission = $this->authManager->getPermission($permission);
            $this->authManager->remove($authPermission);
        }
    }
}
