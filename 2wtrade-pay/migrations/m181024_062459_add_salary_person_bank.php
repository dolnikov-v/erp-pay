<?php
use app\components\CustomMigration as Migration;
use yii\db\Query;

/**
 * Class m181024_062459_add_salary_person_bank
 */
class m181024_062459_add_salary_person_bank extends Migration
{
    private $translate = [
        ['name' => 'Название банка', 'name_en' => 'Bank name', 'name_es' => 'Nombre del banco'],
        ['name' => 'Адрес банка', 'name_en' => 'Bank address', 'name_es' => 'Dirección del banco'],
        ['name' => 'СВИФТ–код банка', 'name_en' => 'Bank swift code', 'name_es' => 'Código swift del banco'],
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('salary_person', 'bank_name', $this->string()->after('account_number'));
        $this->addColumn('salary_person', 'bank_address', $this->string()->after('bank_name'));
        $this->addColumn('salary_person', 'bank_swift_code', $this->string()->after('bank_address'));

        foreach ($this->translate as $item) {
            $idSource = (new Query())->select('id')
                ->from('i18n_source_message')
                ->where(['category' => 'common'])
                ->andWhere(['message' => $item['name']])
                ->scalar();

            if (!$idSource) {
                $this->insert('i18n_source_message', ['category' => 'common', 'message' => $item['name']]);
                $idSource = Yii::$app->db->getLastInsertID();
            }

            if ($idSource) {
                $is = (new Query())->select('id')
                    ->from('i18n_message')
                    ->where(['id' => $idSource])
                    ->andWhere(['language' => 'en-US'])
                    ->exists();

                if (!$is) {
                    $this->insert('i18n_message', [
                        'id' => $idSource,
                        'language' => 'en-US',
                        'translation' => $item['name_en']
                    ]);
                } else {
                    $this->update('i18n_message', [
                        'translation' => $item['name_en']
                    ], ['id' => $idSource, 'language' => 'en-US']);
                }

                $is = (new Query())->select('id')
                    ->from('i18n_message')
                    ->where(['id' => $idSource])
                    ->andWhere(['language' => 'es-ES'])
                    ->exists();

                if (!$is) {
                    $this->insert('i18n_message', [
                        'id' => $idSource,
                        'language' => 'es-ES',
                        'translation' => $item['name_es']
                    ]);
                } else {
                    $this->update('i18n_message', [
                        'translation' => $item['name_es']
                    ], ['id' => $idSource, 'language' => 'es-ES']);
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('salary_person', 'bank_name');
        $this->dropColumn('salary_person', 'bank_address');
        $this->dropColumn('salary_person', 'bank_swift_code');

        foreach ($this->translate as $item) {
            $this->delete('i18n_source_message', ['category' => 'common', 'message' => $item['name']]);
        }
    }
}
