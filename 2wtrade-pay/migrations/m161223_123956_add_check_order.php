<?php
use app\components\CustomMigration as Migration;
use app\modules\order\models\Order;
use app\modules\callcenter\models\CallCenterCheckRequest;
use app\modules\order\models\OrderStatus;
use app\models\User;
use app\modules\administration\models\CrontabTask;

/**
 * Class m161223_123956_add_check_order
 */
class m161223_123956_add_check_order extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Order::tableName(), 'date_check', $this->integer(11)->notNull());
        $this->addColumn(Order::tableName(), 'check', "ENUM('pending', 'done') DEFAULT NULL");


        $this->createTable(CallCenterCheckRequest::tableName(), [
                'id' => $this->primaryKey(),
                'order_id' => $this->integer()->notNull(),
                'user_id' => $this->integer()->notNull(),
                'status_id' => $this->integer()->notNull(),
                'created_at' => $this->integer()->notNull(),
            ]);

        $this->addForeignKey(null, CallCenterCheckRequest::tableName(), 'order_id', Order::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, CallCenterCheckRequest::tableName(), 'user_id', User::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, CallCenterCheckRequest::tableName(), 'status_id', OrderStatus::tableName(), 'id', self::CASCADE, self::RESTRICT);


        //добавляем права для отправки заказов в кц для проверки что у заказа действительный статус
        $auth = Yii::$app->authManager;
        $sendCheckOrder = $auth->createPermission("send_check_order");
        $sendCheckOrder->description = "отправка заказов на проверку";
        $auth->add($sendCheckOrder);


        //добавляем crontab send_check_order
        $crontabTask = new CrontabTask();
        $crontabTask->name = 'send_check_order';
        $crontabTask->description = 'Отправка заказов в КЦ для проверки.';
        $crontabTask->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable(CallCenterCheckRequest::tableName());

        $this->dropColumn(Order::tableName(), 'date_check');
        $this->dropColumn(Order::tableName(), 'check');

        $auth = Yii::$app->authManager;
        $sendCheckOrder = $auth->getPermission("send_check_order");
        $auth->remove($sendCheckOrder);

        $crontabTask = CrontabTask::find()
            ->byName('send_check_order')
            ->one();

        if ($crontabTask) {
            $crontabTask->delete();
        }
    }
}
