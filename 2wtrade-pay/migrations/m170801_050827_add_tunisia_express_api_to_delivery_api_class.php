<?php

use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryApiClass;

/**
 * Handles adding tunisia_express_api to table `delivery_api_class`.
 */
class m170801_050827_add_tunisia_express_api_to_delivery_api_class extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert(DeliveryApiClass::tableName(), [
            'name' => 'Tunisia-express',
            'class_path' => '/tunisia-express-api/src/TunisiaExpressApi.php'
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete(DeliveryApiClass::tableName(),[
            'name' => 'Tunisia-express',
            'class_path' => '/tunisia-express-api/src/TunisiaExpressApi.php'
        ]);
    }
}
