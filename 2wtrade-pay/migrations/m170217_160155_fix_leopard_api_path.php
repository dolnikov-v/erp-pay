<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryApiClass;

/**
 * Class m170217_160155_fix_leopard_api_path
 */
class m170217_160155_fix_leopard_api_path extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $apiClass = DeliveryApiClass::findOne(
            ['name' => 'leopardApi', 'class_path' => '/leopard-api/src/leopardApi.php']
        );
        if(!is_null($apiClass)) {
            $apiClass->class_path = '/leopard-api/src/LeopardApi.php';
            $apiClass->save();
        }
        return true;
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $apiClass = DeliveryApiClass::findOne(
            ['name' => 'leopardApi', 'class_path' => '/leopard-api/src/LeopardApi.php']
        );
        if(!is_null($apiClass)) {
            $apiClass->class_path = '/leopard-api/src/leopardApi.php';
            $apiClass->save();
        }
        return true;
    }
}
