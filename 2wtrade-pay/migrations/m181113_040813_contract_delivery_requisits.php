<?php
use app\components\CustomMigration as Migration;
use yii\db\Query;

/**
 * Class m181113_040813_contract_delivery_requisits
 */
class m181113_040813_contract_delivery_requisits extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        // продублировать из delivery_contract.requisite_delivery_id в таблицу requisite_delivery_link
        $contracts = (new Query())
            ->select([
                'id' => 'delivery_contract.id',
                'country_id' => 'delivery.country_id',
                'delivery_id' => 'delivery_contract.delivery_id',
                'requisite_delivery_id' => 'delivery_contract.requisite_delivery_id',
            ])
            ->from('delivery_contract')
            ->leftJoin('delivery', 'delivery_contract.delivery_id = delivery.id')
            ->where(['is not', 'requisite_delivery_id', null])
            ->all();

        $insertArray = [];
        foreach ($contracts as $contract) {

            $is = (new Query())->select('id')
                ->from('requisite_delivery_link')
                ->where([
                    'requisite_delivery_id' => $contract['requisite_delivery_id'],
                    'country_id' => $contract['country_id'],
                    'delivery_id' => $contract['delivery_id'],
                    'delivery_contract_id' => $contract['id'],
                ])
                ->exists();

            if (!$is) {
                $insertArray[] = [
                    'requisite_delivery_id' => $contract['requisite_delivery_id'],
                    'country_id' => $contract['country_id'],
                    'delivery_id' => $contract['delivery_id'],
                    'delivery_contract_id' => $contract['id'],
                    'created_at' => time(),
                    'updated_at' => time(),
                ];
            }
        }

        if ($insertArray) {
            $this->batchInsert('requisite_delivery_link', [
                'requisite_delivery_id',
                'country_id',
                'delivery_id',
                'delivery_contract_id',
                'created_at',
                'updated_at'
            ], $insertArray);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        // не нужно
    }
}
