<?php
use app\components\CustomMigration as Migration;
use app\modules\widget\models\WidgetType;

/**
 * Class m170215_083621_fix_name_widgets_TOP20
 */
class m170215_083621_fix_name_widgets_TOP20 extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->update(
            WidgetType::tableName(),
            [WidgetType::tableName() . '.name' => 'Топ 20 городов'],
            [WidgetType::tableName() . '.code' => 'top20cities']
        );

        $this->update(
            WidgetType::tableName(),
            [WidgetType::tableName() . '.name' => 'Топ 20 товаров в городах'],
            [WidgetType::tableName() . '.code' => 'top20products']
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

    }
}
