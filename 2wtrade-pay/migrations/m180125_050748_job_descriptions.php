<?php
use app\components\PermissionMigration as Migration;

use app\modules\salary\models\Designation;

/**
 * Class m180125_050748_job_descriptions
 */
class m180125_050748_job_descriptions extends Migration
{

    protected $permissions = [
        'media.salarydesignation.jobdescription',
        'salary.staffing.getjobdescription',
        'salary.designation.deletejobdescriptionfile',
    ];

    protected $roles = [
        'admin' => [
            'salary.designation.deletejobdescriptionfile',
            'media.salarydesignation.jobdescription',
            'salary.staffing.getjobdescription',
        ],
        'project.manager' => [
            'salary.designation.deletejobdescriptionfile',
            'media.salarydesignation.jobdescription',
            'salary.staffing.getjobdescription',
        ],
        'salaryproject.clerk' => [
            'salary.designation.deletejobdescriptionfile',
            'media.salarydesignation.jobdescription',
            'salary.staffing.getjobdescription',
        ],
        'finance.director' => [
            'salary.designation.deletejobdescriptionfile',
            'media.salarydesignation.jobdescription',
            'salary.staffing.getjobdescription',
        ],
        'auditor' => [
            'media.salarydesignation.jobdescription',
            'salary.staffing.getjobdescription',
        ],
        'callcenter.hr' => [
            'media.salarydesignation.jobdescription',
            'salary.staffing.getjobdescription',
        ],
        'callcenter.leader' => [
            'media.salarydesignation.jobdescription',
            'salary.staffing.getjobdescription',
        ],
        'controller.analyst' => [
            'media.salarydesignation.jobdescription',
            'salary.staffing.getjobdescription',
        ],
        'country.curator' => [
            'media.salarydesignation.jobdescription',
            'salary.staffing.getjobdescription',
        ],
        'development.director' => [
            'media.salarydesignation.jobdescription',
            'salary.staffing.getjobdescription',
        ],
        'executive.director' => [
            'media.salarydesignation.jobdescription',
            'salary.staffing.getjobdescription',
        ],
        'general.director' => [
            'media.salarydesignation.jobdescription',
            'salary.staffing.getjobdescription',
        ],
    ];


    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Designation::tableName(), 'job_description_text', 'MEDIUMTEXT');
        $this->addColumn(Designation::tableName(), 'job_description_file', $this->string());

        parent::safeUp();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Designation::tableName(), 'job_description_text');
        $this->dropColumn(Designation::tableName(), 'job_description_file');

        parent::safeDown();
    }
}
