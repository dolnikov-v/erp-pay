<?php

use app\components\CustomMigration as Migration;

/**
 * Handles the creation for table `product_link_category`.
 * Has foreign keys to the tables:
 *
 * - `product`
 * - `product_category`
 */
class m171101_071058_create_product_link_category extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('product_link_category', [
            'product_id' => $this->integer()->notNull(),
            'category_id' => $this->integer()->notNull(),
        ]);

        $this->addPrimaryKey(null, 'product_link_category', ['product_id', 'category_id']);

        // creates index for column `product_id`
        $this->createIndex(
            null,
            'product_link_category',
            'product_id'
        );

        // add foreign key for table `product`
        $this->addForeignKey(
            null,
            'product_link_category',
            'product_id',
            'product',
            'id',
            self::CASCADE
        );

        // creates index for column `category_id`
        $this->createIndex(
            null,
            'product_link_category',
            'category_id'
        );

        // add foreign key for table `product_category`
        $this->addForeignKey(
            null,
            'product_link_category',
            'category_id',
            'product_category',
            'id',
            self::CASCADE
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('product_link_category');
    }
}
