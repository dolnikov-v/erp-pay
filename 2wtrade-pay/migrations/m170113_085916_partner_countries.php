<?php
use app\components\CustomMigration as Migration;
use app\models\Country;
use app\models\Partner;

/**
 * Class m170113_085916_partner_countries
 */
class m170113_085916_partner_countries extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('partner_country', [
            'partner_id' => $this->integer()->notNull(),
            'country_id' => $this->integer()->notNull(),
            'active' => $this->boolean()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $this->tableOptions);

        $this->addPrimaryKey(null, 'partner_country', ['partner_id', 'country_id']);
        $this->addForeignKey(null, 'partner_country', 'partner_id', Partner::tableName(), 'id', Migration::CASCADE, Migration::CASCADE);
        $this->addForeignKey(null, 'partner_country', 'country_id', Country::tableName(), 'id', Migration::CASCADE, Migration::CASCADE);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('partner_country');
    }
}
