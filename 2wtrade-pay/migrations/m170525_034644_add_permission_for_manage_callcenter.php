<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170525_034644_add_permission_for_manage_callcenter
 */
class m170525_034644_add_permission_for_manage_callcenter extends Migration
{
    public $permissions = [
        'callcenter.user.changeteamleader',
        'callcenter.user.deactivateusers',
        'callcenter.user.activateusers',
        'callcenter.user.setrole'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        foreach ($this->permissions as $v) {
            $this->insert($this->authManager->itemChildTable, array(
                'parent' => 'callcenter.manager',
                'child' => $v
            ));
        }

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach ($this->permissions as $v) {
            $this->delete($this->authManager->itemChildTable, ['child' => $v, 'parent' => 'callcenter.manager']);
        }

    }
}
