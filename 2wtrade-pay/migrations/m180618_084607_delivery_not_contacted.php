<?php
use app\components\CustomMigration as Migration;

/**
 * Class m180618_084607_delivery_not_contacted
 */
class m180618_084607_delivery_not_contacted extends Migration
{
    private $translate = [
        "КС не связалась с клиентом" => "Delivery didn't contacted customer",
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        foreach ($this->translate as $ruText => $enText) {
            $this->insert('i18n_source_message', ['category' => 'common', 'message' => $ruText]);
            $id = Yii::$app->db->getLastInsertID();
            $this->insert('i18n_message', ['id' => $id, 'language' => 'en-US', 'translation' => $enText]);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach ($this->translate as $ruText => $enText) {
            $this->delete('i18n_source_message', ['category' => 'common', 'message' => $ruText]);
        }
    }
}
