<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m170801_095140_add_cron_delivery_calculate_charges
 */
class m170801_095140_add_cron_delivery_calculate_charges extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_DELIVERY_CALCULATE_CHARGES]);
        if (!$crontabTask) {
            $crontabTask = new CrontabTask();
            $crontabTask->name = CrontabTask::TASK_DELIVERY_CALCULATE_CHARGES;
            $crontabTask->description = "Пересчет расходов на доставку для каждого заказа";
            $crontabTask->save();
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = CrontabTask::findOne(['name' => CrontabTask::TASK_DELIVERY_CALCULATE_CHARGES]);
        if ($crontabTask instanceof CrontabTask) {
            $crontabTask->delete();
        }
    }
}
