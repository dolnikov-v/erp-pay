<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m170124_060126_add_report_courier_debts
 */
class m170124_060126_add_report_delivery_debts extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('report_delivery_debts', [
            'id' => $this->primaryKey(),
            'delivery_id' => $this->integer()->notNull(),
            'time_from' => $this->integer(),
            'time_to' => $this->integer(),
            'sum_total' => $this->double(),
            'sum_total_bucks' => $this->double(),
        ], $this->tableOptions);

        $this->addForeignKey(null, 'report_delivery_debts', 'delivery_id', 'delivery', 'id', self::CASCADE,
            self::CASCADE);

        $crontabTask = new CrontabTask();
        $crontabTask->name = 'report_calculate_delivery_debts';
        $crontabTask->description = 'Подсчет дебеторской задолжности по месяцам';
        $crontabTask->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('report_delivery_debts');

        $crontabTask = CrontabTask::find()
            ->byName('report_calculate_delivery_debts')
            ->one();

        if ($crontabTask) {
            $crontabTask->delete();
        }
    }
}
