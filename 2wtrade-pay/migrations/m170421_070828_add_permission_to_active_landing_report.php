<?php

use app\components\CustomMigration as Migration;

/**
 * Handles add permission to report "Active landing"
 */
class m170421_070828_add_permission_to_active_landing_report extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}',array(
            'name'=> 'report.activelanding.index',
            'type' => '2',
            'description' => 'report.activelanding.index',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'country.curator',
            'child' => 'report.activelanding.index'
        ));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->authManager->itemChildTable, ['child' => 'report.activelanding.index', 'parent' => 'country.curator']);
        $this->delete('{{%auth_item}}', ['name' => 'report.activelanding.index']);
    }
}
