<?php
use app\components\CustomMigration as Migration;

/**
 * Class m161206_101945_AddRuleChangeDelivery
 */
class m161209_131002_add_premission_old_orders extends Migration
{
    public function up()
    {
        $auth = Yii::$app->authManager;
        $oldorders = $auth->createPermission("old_orders_access");
        $oldorders->description = "доступ к староватым заказам и листам";
        $auth->add($oldorders);
    }

    public function down()
    {
        $auth = Yii::$app->authManager;
        $oldorders = $auth->getPermission("old_orders_access");
        $auth->remove($oldorders);
    }
}
