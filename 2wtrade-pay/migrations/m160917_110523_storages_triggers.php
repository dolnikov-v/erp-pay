<?php
use app\components\CustomMigration as Migration;
use app\modules\order\models\OrderStatus;

/**
 * Class m160917_110523_storages_triggers
 */
class m160917_110523_storages_triggers extends Migration
{
    /**
     * @var array - статусы заказа для списания на складе
     */
    private $statusesMinus = [
        OrderStatus::STATUS_DELIVERY_ACCEPTED,
        OrderStatus::STATUS_DELIVERY_LEFT_WAREHOUSE,
        OrderStatus::STATUS_DELIVERY,
        OrderStatus::STATUS_DELIVERY_BUYOUT,
        OrderStatus::STATUS_FINANCE_MONEY_RECEIVED,
        OrderStatus::STATUS_DELIVERY_LEFT_OUR_WAREHOUSE,
        OrderStatus::STATUS_DELIVERY_REDELIVERY,
        OrderStatus::STATUS_DELIVERY_LEFT_THEIR_WAREHOUSE,
        OrderStatus::STATUS_DELIVERY_DELAYED,
        OrderStatus::STATUS_DELIVERY_LOST,
        OrderStatus::STATUS_DELIVERY_PENDING,
    ];

    /**
     * @var array - статусы заказа для поступления на складе
     */
    private $statusesPlus = [
        OrderStatus::STATUS_DELIVERY_DENIAL,
        OrderStatus::STATUS_DELIVERY_RETURNED,
        OrderStatus::STATUS_DELIVERY_REJECTED,
        OrderStatus::STATUS_LOGISTICS_REJECTED,
        OrderStatus::STATUS_DELIVERY_REFUND,
        OrderStatus::STATUS_LOGISTICS_ACCEPTED,
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $createTriggerSql = "
CREATE TRIGGER `order_change_status_id` AFTER UPDATE ON `order`
    
    FOR EACH ROW BEGIN
        DECLARE v_order_product_id INT;
        DECLARE v_product_id INT;
        DECLARE v_quantity INT;
        DECLARE v_storage_part_id INT;
        /* курсор на выборку товаров заказа */
        DECLARE products_cur CURSOR FOR SELECT id, product_id, quantity, storage_part_id
                                    FROM order_product
                                    WHERE order_id = NEW.id;
        /* только для необходимых order.status_id делаем действия */
        IF NEW.status_id IN (" . implode(",", $this->statusesMinus) . "," . implode(",", $this->statusesPlus) . ") THEN
            SELECT delivery_id into @v_delivery_id
            FROM delivery_request
            WHERE order_id=NEW.id 
            LIMIT 1;
            
            OPEN products_cur;
            LOOP
                FETCH products_cur INTO v_order_product_id, v_product_id, v_quantity, v_storage_part_id;
                
                IF NEW.status_id IN (" . implode(",", $this->statusesMinus) . ") THEN /* списание со склада */
                    SET @id_storage_part_minus = NULL;
                
                    SELECT part.id into @id_storage_part_minus 
                    FROM storage s,
                         storage_part part
                    WHERE s.country_id = NEW.country_id
                          and s.delivery_id = @v_delivery_id
                          and s.id = part.storage_id
                          and part.product_id = v_product_id 
                          and part.active = 1
                          and part.balance >= v_quantity
                    ORDER BY s.created_at, part.created_at
                    LIMIT 1;
                    
                    IF @id_storage_part_minus is not NULL THEN
                        INSERT INTO storage_part_history SET storage_part_id=@id_storage_part_minus, type='minus', amount=v_quantity, order_id=NEW.id, created_at=UNIX_TIMESTAMP();
                        UPDATE storage_part SET active=if(balance>v_quantity, 1, 0), balance=balance-v_quantity WHERE id=@id_storage_part_minus;
                        UPDATE order_product SET storage_part_id=@id_storage_part_minus WHERE id=v_order_product_id;
                    ELSE
                        SET @id_storage = NULL;
                        
                        SELECT s.id into @id_storage 
                        FROM storage s,
                             storage_product_minus m
                        WHERE s.country_id = NEW.country_id
                              and s.id=m.storage_id
                              and m.product_id = v_product_id
                        ORDER BY s.created_at
                        LIMIT 1;
                        
                        INSERT INTO storage_product_unaccounted SET storage_id=@id_storage, product_id=v_product_id, amount=v_quantity, order_id=NEW.id, country_id=NEW.country_id;
                    END IF;
                END IF;
                
                IF NEW.status_id IN (" . implode(",", $this->statusesPlus) . ") THEN /* поступление на склад */
                    IF v_storage_part_id != NULL THEN
                        INSERT INTO storage_part_history SET storage_part_id=v_storage_part_id, type='plus', amount=v_quantity, order_id=NEW.id, created_at=UNIX_TIMESTAMP();
                        UPDATE storage_part SET active=1, balance=balance+v_quantity WHERE id=v_storage_part_id;
                        UPDATE order_product SET storage_part_id=null WHERE id=v_order_product_id;
                    END IF;
                END IF;
            
            END LOOP;
            CLOSE products_cur;
 
        END IF;
    
END;";

        $this->execute($createTriggerSql);

        $createTriggerSql = "
CREATE TRIGGER `order_product_change_quantity` AFTER UPDATE ON `order_product`
    
    FOR EACH ROW BEGIN
        IF NEW.quantity != OLD.quantity and NEW.storage_part_id is not NULL THEN
          UPDATE storage_part_history SET amount=NEW.quantity WHERE storage_part_id=NEW.storage_part_id;
        END IF;

END;";

        $this->execute($createTriggerSql);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->execute('DROP TRIGGER IF EXISTS `order_change_status_id`');
        $this->execute('DROP TRIGGER IF EXISTS `order_product_change_quantity`');
    }
}
