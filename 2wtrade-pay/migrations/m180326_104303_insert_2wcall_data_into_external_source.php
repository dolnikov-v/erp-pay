<?php
use app\components\CustomMigration as Migration;
use app\modules\catalog\models\ExternalSource;

/**
 * Class m180326_104303_insert_2wcall_data_into_external_source
 */
class m180326_104303_insert_2wcall_data_into_external_source extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert(ExternalSource::tableName(),[
            'name' => '2wcall',
            'url' => 'https://2wcall.com',
            'description' => 'Новый КЦ (CRM 2WCALL)',
            'incoming_queue_amazon_url' => 'https://sqs.eu-west-1.amazonaws.com/636470419474/ActionWithUserFromCRM',
            'outbound_queue_amazon_url' => 'https://sqs.eu-west-1.amazonaws.com/636470419474/ActionWithUserFromERP',
            'created_at' => time(),
            'updated_at' => time()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete(ExternalSource::tableName(), [
            'name' => '2wcall'
        ]);
    }
}