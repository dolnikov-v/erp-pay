<?php
use app\components\CustomMigration as Migration;
use app\models\Country;
use app\models\User;
use app\models\UserCountry;

class m000000_000018_user_country extends Migration
{
    public function safeUp()
    {
        $this->createTable(UserCountry::tableName(), [
            'user_id' => $this->integer()->notNull(),
            'country_id' => $this->integer()->notNull(),
            'active' => $this->boolean()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $this->tableOptions);

        $this->addPrimaryKey(null, UserCountry::tableName(), ['user_id', 'country_id']);
        $this->addForeignKey(null, UserCountry::tableName(), 'user_id', User::tableName(), 'id', Migration::CASCADE, Migration::CASCADE);
        $this->addForeignKey(null, UserCountry::tableName(), 'country_id', Country::tableName(), 'id', Migration::CASCADE, Migration::CASCADE);
    }

    public function safeDown()
    {
        $this->dropTable(UserCountry::tableName());
    }
}
