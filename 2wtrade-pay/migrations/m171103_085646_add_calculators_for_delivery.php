<?php
use app\components\CustomMigration as Migration;

/**
 * Class m171103_085646_add_calculators_for_delivery
 */
class m171103_085646_add_calculators_for_delivery extends Migration
{
    protected static $calculators = [
        'PingDeliveryMX' => 'app\modules\delivery\components\charges\pingdelivery\PingDeliveryMX',
        'AlKarsf' => 'app\modules\delivery\components\charges\alkarsf\AlKarsf',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        foreach (self::$calculators as $name => $route) {
            $this->insert('delivery_charges_calculator', [
                'name' => $name,
                'class_path' => $route,
                'active' => 1,
                'created_at' => time(),
            ]);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach (self::$calculators as $name => $route) {
            $this->delete('delivery_charges_calculator', ['name' => $name]);
        }
    }
}
