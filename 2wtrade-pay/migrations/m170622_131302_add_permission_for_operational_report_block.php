<?php

use app\components\CustomMigration as Migration;

/**
 * Class m170622_131302_add_permission_for_operational_report_block
 */
class m170622_131302_add_permission_for_operational_report_block extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        // Доступ к блоку операционных отчетов
        $this->insert('{{%auth_item}}',array(
            'name'=>'report.dailyoperationalreports.index',
            'type' => '2',
            'description' => 'report.dailyoperationalreports.index',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'finance.director',
            'child' => 'report.dailyoperationalreports.index'
        ));

        // Доступ к ежедневному операционному отчету по выкупам
        $this->insert('{{%auth_item}}',array(
            'name'=>'report.dailyoperationalbybuyouts.index',
            'type' => '2',
            'description' => 'report.dailyoperationalbybuyouts.index',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'finance.director',
            'child' => 'report.dailyoperationalbybuyouts.index'
        ));

        // Доступ к ежедневному операционному отчету по продуктам
        $this->insert('{{%auth_item}}',array(
            'name'=>'report.dailyoperationalbyproducts.index',
            'type' => '2',
            'description' => 'report.dailyoperationalbyproducts.index',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'finance.director',
            'child' => 'report.dailyoperationalbyproducts.index'
        ));

        // Доступ к ежедневному операционному отчету по выкупам
        $this->insert('{{%auth_item}}',array(
            'name'=>'report.dailyoperationalbyshippings.index',
            'type' => '2',
            'description' => 'report.dailyoperationalbyshippings.index',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'finance.director',
            'child' => 'report.dailyoperationalbyshippings.index'
        ));

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->authManager->itemChildTable, ['child' => 'report.dailyoperationalreports.index', 'parent' => 'finance.director']);
        $this->delete('{{%auth_item}}', ['name' => 'report.dailyoperationalreports.index']);

        $this->delete($this->authManager->itemChildTable, ['child' => 'report.dailyoperationalbybuyouts.index', 'parent' => 'finance.director']);
        $this->delete('{{%auth_item}}', ['name' => 'report.dailyoperationalbybuyouts.index']);

        $this->delete($this->authManager->itemChildTable, ['child' => 'report.dailyoperationalbyproducts.index', 'parent' => 'finance.director']);
        $this->delete('{{%auth_item}}', ['name' => 'report.dailyoperationalbyproducts.index']);

        $this->delete($this->authManager->itemChildTable, ['child' => 'report.dailyoperationalbyshippings.index', 'parent' => 'finance.director']);
        $this->delete('{{%auth_item}}', ['name' => 'report.dailyoperationalbyshippings.index']);
    }
}
