<?php

use yii\db\Migration;

/**
 * Handles adding column_vat_included to table `order_finance`.
 */
class m180830_102621_add_column_vat_included_to_order_finance extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('order_finance_prediction', 'vat_included', $this->double()->defaultValue(null));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('order_finance_prediction', 'vat_included');
    }
}
