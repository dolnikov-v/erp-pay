<?php
use app\components\CustomMigration as Migration;

/**
 * Class m161205_101354_add_default_status_qr_codes
 */
class m161205_101354_add_default_status_qr_codes extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('qr_codes', 'status', $this->integer(2)->defaultValue(0));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('qr_codes', 'status', $this->integer(2));
    }
}
