<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170314_115408_add_operations_for_delivery_report
 */
class m170314_115408_add_operations_for_delivery_report extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}',array(
            'name'=>'deliveryreport.report.rewritedata',
            'type' => '2',
            'description' => 'deliveryreport.report.rewritedata',
            'created_at' => time(),
            'updated_at' => time()
        ));
        $this->insert('{{%auth_item}}',array(
            'name'=>'deliveryreport.report.index',
            'type' => '2',
            'description' => 'deliveryreport.report.index',
            'created_at' => time(),
            'updated_at' => time()
        ));
        $this->insert('{{%auth_item}}',array(
            'name'=>'deliveryreport.report.view',
            'type' => '2',
            'description' => 'deliveryreport.report.view',
            'created_at' => time(),
            'updated_at' => time()
        ));
        $this->insert('{{%auth_item}}',array(
            'name'=>'deliveryreport.report.vieworder',
            'type' => '2',
            'description' => 'deliveryreport.report.vieworder',
            'created_at' => time(),
            'updated_at' => time()
        ));
        $this->insert('{{%auth_item}}',array(
            'name'=>'deliveryreport.report.create',
            'type' => '2',
            'description' => 'deliveryreport.report.create',
            'created_at' => time(),
            'updated_at' => time()
        ));
        $this->insert('{{%auth_item}}',array(
            'name'=>'deliveryreport.report.editorder',
            'type' => '2',
            'description' => 'deliveryreport.report.editorder',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert('{{%auth_item}}',array(
            'name'=>'deliveryreport.report.setpaymentid',
            'type' => '2',
            'description' => 'deliveryreport.report.setpaymentid',
            'created_at' => time(),
            'updated_at' => time()
        ));
        $this->insert('{{%auth_item}}',array(
            'name'=>'deliveryreport.report.setorderprices',
            'type' => '2',
            'description' => 'deliveryreport.report.setorderprices',
            'created_at' => time(),
            'updated_at' => time()
        ));
        $this->insert('{{%auth_item}}',array(
            'name'=>'deliveryreport.report.setapprove',
            'type' => '2',
            'description' => 'deliveryreport.report.setapprove',
            'created_at' => time(),
            'updated_at' => time()
        ));
        $this->insert('{{%auth_item}}',array(
            'name'=>'deliveryreport.report.unsetapprove',
            'type' => '2',
            'description' => 'deliveryreport.report.unsetapprove',
            'created_at' => time(),
            'updated_at' => time()
        ));
        $this->insert('{{%auth_item}}',array(
            'name'=>'deliveryreport.report.delete',
            'type' => '2',
            'description' => 'deliveryreport.report.delete',
            'created_at' => time(),
            'updated_at' => time()
        ));
        $this->insert('{{%auth_item}}',array(
            'name'=>'deliveryreport.report.deleterecord',
            'type' => '2',
            'description' => 'deliveryreport.report.deleterecord',
            'created_at' => time(),
            'updated_at' => time()
        ));
        $this->insert('{{%auth_item}}',array(
            'name'=>'deliveryreport.report.deleterecords',
            'type' => '2',
            'description' => 'deliveryreport.report.deleterecords',
            'created_at' => time(),
            'updated_at' => time()
        ));
        $this->insert('{{%auth_item}}',array(
            'name'=>'deliveryreport.report.updateorderdata',
            'type' => '2',
            'description' => 'deliveryreport.report.updateorderdata',
            'created_at' => time(),
            'updated_at' => time()
        ));
        $this->insert('{{%auth_item}}',array(
            'name'=>'deliveryreport.report.updateorderstatus',
            'type' => '2',
            'description' => 'deliveryreport.report.updateorderstatus',
            'created_at' => time(),
            'updated_at' => time()
        ));
        $this->insert('{{%auth_item}}',array(
            'name'=>'deliveryreport.report.updateordertracking',
            'type' => '2',
            'description' => 'deliveryreport.report.updateordertracking',
            'created_at' => time(),
            'updated_at' => time()
        ));
        $this->insert('{{%auth_item}}',array(
            'name'=>'deliveryreport.report.exportwithoutreportorders',
            'type' => '2',
            'description' => 'deliveryreport.report.exportwithoutreportorders',
            'created_at' => time(),
            'updated_at' => time()
        ));
        $this->insert('{{%auth_item}}',array(
            'name'=>'deliveryreport.report.withoutreportordercount',
            'type' => '2',
            'description' => 'deliveryreport.report.withoutreportordercount',
            'created_at' => time(),
            'updated_at' => time()
        ));
        $this->insert('{{%auth_item}}',array(
            'name'=>'deliveryreport.report.downloadfile',
            'type' => '2',
            'description' => 'deliveryreport.report.downloadfile',
            'created_at' => time(),
            'updated_at' => time()
        ));
        $this->insert('{{%auth_item}}',array(
            'name'=>'deliveryreport.report.downloadtmpfile',
            'type' => '2',
            'description' => 'deliveryreport.report.downloadtmpfile',
            'created_at' => time(),
            'updated_at' => time()
        ));
        $this->insert('{{%auth_item}}',array(
            'name'=>'deliveryreport.report.setreportcomplete',
            'type' => '2',
            'description' => 'deliveryreport.report.setreportcomplete',
            'created_at' => time(),
            'updated_at' => time()
        ));
        $this->insert('{{%auth_item}}',array(
            'name'=>'deliveryreport.report.sendtochecking',
            'type' => '2',
            'description' => 'deliveryreport.report.sendtochecking',
            'created_at' => time(),
            'updated_at' => time()
        ));
        $this->insert('{{%auth_item}}',array(
            'name'=>'deliveryreport.report.increasereportpriority',
            'type' => '2',
            'description' => 'deliveryreport.report.increasereportpriority',
            'created_at' => time(),
            'updated_at' => time()
        ));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete('{{%auth_item}}',array(
            'name'=>'deliveryreport.report.rewritedata'
        ));
        $this->delete('{{%auth_item}}',array(
            'name'=>'deliveryreport.report.index'
        ));
        $this->delete('{{%auth_item}}',array(
            'name'=>'deliveryreport.report.view'
        ));
        $this->delete('{{%auth_item}}',array(
            'name'=>'deliveryreport.report.vieworder'
        ));
        $this->delete('{{%auth_item}}',array(
            'name'=>'deliveryreport.report.create'
        ));
        $this->delete('{{%auth_item}}',array(
            'name'=>'deliveryreport.report.editorder'
        ));

        $this->delete('{{%auth_item}}',array(
            'name'=>'deliveryreport.report.setpaymentid'
        ));
        $this->delete('{{%auth_item}}',array(
            'name'=>'deliveryreport.report.setorderprices'
        ));
        $this->delete('{{%auth_item}}',array(
            'name'=>'deliveryreport.report.setapprove'
        ));
        $this->delete('{{%auth_item}}',array(
            'name'=>'deliveryreport.report.unsetapprove'
        ));
        $this->delete('{{%auth_item}}',array(
            'name'=>'deliveryreport.report.delete'
        ));
        $this->delete('{{%auth_item}}',array(
            'name'=>'deliveryreport.report.deleterecord'
        ));
        $this->delete('{{%auth_item}}',array(
            'name'=>'deliveryreport.report.deleterecords'
        ));
        $this->delete('{{%auth_item}}',array(
            'name'=>'deliveryreport.report.updateorderdata'
        ));
        $this->delete('{{%auth_item}}',array(
            'name'=>'deliveryreport.report.updateorderstatus'
        ));
        $this->delete('{{%auth_item}}',array(
            'name'=>'deliveryreport.report.updateordertracking'
        ));
        $this->delete('{{%auth_item}}',array(
            'name'=>'deliveryreport.report.exportwithoutreportorders'
        ));
        $this->delete('{{%auth_item}}',array(
            'name'=>'deliveryreport.report.withoutreportordercount'
        ));
        $this->delete('{{%auth_item}}',array(
            'name'=>'deliveryreport.report.downloadfile'
        ));
        $this->delete('{{%auth_item}}',array(
            'name'=>'deliveryreport.report.downloadtmpfile'
        ));
        $this->delete('{{%auth_item}}',array(
            'name'=>'deliveryreport.report.setreportcomplete'
        ));
        $this->delete('{{%auth_item}}',array(
            'name'=>'deliveryreport.report.sendtochecking'
        ));
        $this->delete('{{%auth_item}}',array(
            'name'=>'deliveryreport.report.increasereportpriority'
        ));
    }
}
