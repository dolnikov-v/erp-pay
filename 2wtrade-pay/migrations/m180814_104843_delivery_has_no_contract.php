<?php
use app\components\CustomMigration as Migration;
use app\models\Notification;
use app\models\NotificationRole;

/**
 * Class m180814_104843_delivery_has_no_contract
 */
class m180814_104843_delivery_has_no_contract extends Migration
{

    public $translate = [
        'Заказы не могут быть отправлены в КС. Заполните данные контракта.' => 'Orders can not be sent to the Delivery. Fill in the details of the contract',
        'Заказы не могут быть отправлены в КС {delivery}. Данные контракта не заполнены.' => 'Orders can not be sent to {delivery}. Fill in the details of the contract',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        $notification = Notification::findOne(['trigger' => Notification::TRIGGER_DELIVERY_HAS_NO_CONTRACT]);

        if (!($notification instanceof Notification)) {
            $notification = new Notification([
                'description' => 'Заказы не могут быть отправлены в КС {delivery}. Данные контракта не заполнены.',
                'trigger' => Notification::TRIGGER_DELIVERY_HAS_NO_CONTRACT,
                'type' => Notification::TYPE_WARNING,
                'group' => Notification::GROUP_REPORT,
                'active' => Notification::ACTIVE,
            ]);
            $notification->save(false);
        }

        foreach ($this->translate as $ruText => $enText) {
            $this->insert('i18n_source_message', ['category' => 'common', 'message' => $ruText]);
            $id = Yii::$app->db->getLastInsertID();
            $this->insert('i18n_message', ['id' => $id, 'language' => 'en-US', 'translation' => $enText]);
        }

        $notificationRole = NotificationRole::findOne([
            'role' => 'country.curator',
            'trigger' => Notification::TRIGGER_DELIVERY_HAS_NO_CONTRACT,
        ]);
        if (!$notificationRole) {
            $notificationRole = new NotificationRole([
                'role' => 'country.curator',
                'trigger' => Notification::TRIGGER_DELIVERY_HAS_NO_CONTRACT,
            ]);
        }
        $notificationRole->save(false);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        NotificationRole::deleteAll([
            'trigger' => Notification::TRIGGER_DELIVERY_HAS_NO_CONTRACT,
        ]);

        foreach ($this->translate as $ruText => $enText) {
            $this->delete('i18n_source_message', ['category' => 'common', 'message' => $ruText]);
        }
    }
}
