<?php
use app\components\CustomMigration as Migration;
use yii\db\Query;

/**
 * Class m170925_080431_product_catalog
 */
class m170925_080431_product_catalog extends Migration
{

    const RULES = [
        'catalog.language.index',
        'catalog.language.edit',
        'catalog.language.delete',
        'catalog.productprice.edit',
        'catalog.productprice.delete',
    ];

    const ROLES = [
        'admin',
        'project.manager',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('language', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'locale' => $this->string(),
            'active' => $this->boolean()->defaultValue(0),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $this->tableOptions);

        $this->addColumn('country', 'language_id', $this->integer()->after('currency_id'));
        $this->addForeignKey('fk_country_language_id', 'country', 'language_id', 'language', 'id', self::SET_NULL, self::NO_ACTION);

        $this->renameColumn('product_translation', 'language', 'name');
        $this->addColumn('product_translation', 'language_id', $this->integer()->after('product_id'));
        $this->addForeignKey('fk_product_translation_language_id', 'product_translation', 'language_id', 'language', 'id', self::CASCADE, self::CASCADE);

        $this->createTable('product_price', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer()->notNull(),
            'currency_id' => $this->integer()->notNull(),
            'price' => $this->double(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $this->tableOptions);

        $this->addForeignKey(null, 'product_price', 'product_id', 'product', 'id', self::CASCADE, self::CASCADE);
        $this->addForeignKey(null, 'product_price', 'currency_id', 'currency', 'id', self::CASCADE, self::CASCADE);

        $query = new Query();

        foreach (self::RULES as $rule) {

            $is_rule = $query->select('*')->from($this->authManager->itemTable)
                ->where(['name' => $rule, 'type' => 2])
                ->one();

            if (!$is_rule) {
                $this->insert($this->authManager->itemTable, [
                    'name' => $rule,
                    'description' => $rule,
                    'type' => 2,
                    'created_at' => time(),
                    'updated_at' => time()]);
            }

            foreach (self::ROLES as $role) {
                $is_can = $query->select('*')->from($this->authManager->itemChildTable)->where([
                    'parent' => $role,
                    'child' => $rule
                ])->one();

                if (!$is_can) {
                    $this->insert($this->authManager->itemChildTable, [
                        'parent' => $role,
                        'child' => $rule
                    ]);
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_country_language_id', 'country');
        $this->dropForeignKey('fk_product_translation_language_id', 'product_translation');
        $this->dropTable('language');
        $this->dropColumn('country', 'language_id');
        $this->dropColumn('product_translation', 'language_id');
        $this->renameColumn('product_translation', 'name', 'language');
        $this->dropTable('product_price');

        foreach (self::RULES as $rule) {
            foreach (self::ROLES as $role) {
                $this->delete($this->authManager->itemChildTable, [
                    'parent' => $role,
                    'child' => $rule
                ]);
            }
        }
    }
}
