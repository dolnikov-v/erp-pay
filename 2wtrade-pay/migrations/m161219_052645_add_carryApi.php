<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryApiClass;

/**
 * Class m161219_052645_add_carryApi
 */
class m161219_052645_add_carryApi extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $class = new DeliveryApiClass();
        $class->name = 'carryApi';
        $class->class_path = '/carry-api/src/carryApi.php';
        $class->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        DeliveryApiClass::find()
            ->where([
                'name' => 'carryApi',
                'class_path' => '/carry-api/src/carryApi.php'
            ])
            ->one()
            ->delete();
    }
}
