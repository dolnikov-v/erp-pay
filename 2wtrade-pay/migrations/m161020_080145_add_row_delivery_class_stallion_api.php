<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryApiClass;

/**
 * Class m161020_080145_add_row_delivery_class_stallion_api
 */
class m161020_080145_add_row_delivery_class_stallion_api extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $class = new DeliveryApiClass();
        $class->name = 'StallionApi';
        $class->class_path = '/stallion-api/src/stallionApi.php';
        $class->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        DeliveryApiClass::find()
            ->where([
                'name' => 'StallionApi',
                'class_path' => '/stallion-api/src/stallionApi.php'
            ])
            ->one()
            ->delete();
    }
}
