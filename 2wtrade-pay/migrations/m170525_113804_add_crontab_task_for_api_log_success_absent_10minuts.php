<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m170525_113804_add_crontab_task_for_api_log_success_absent_10minuts
 */
class m170525_113804_add_crontab_task_for_api_log_success_absent_10minuts extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = new CrontabTask();
        $crontabTask->name = 'lead_log_success_absent_10minuts';
        $crontabTask->description = 'Поиск в api_log по типу lead если за последние 10 мин не было логов success на создание лидов';
        $crontabTask->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = CrontabTask::find()
            ->byName('lead_log_success_absent_10minuts')
            ->one();

        if ($crontabTask) {
            $crontabTask->delete();
        }

    }
}
