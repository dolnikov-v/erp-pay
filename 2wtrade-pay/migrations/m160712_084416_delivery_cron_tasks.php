<?php
use app\components\CustomMigration as Migration;
use app\modules\administration\models\CrontabTask;

/**
 * Class m160712_084416_delivery_cron_tasks
 */
class m160712_084416_delivery_cron_tasks extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = new CrontabTask();
        $crontabTask->name = 'delivery_send_orders';
        $crontabTask->description = 'Отправка заказов в службу доставки.';
        $crontabTask->save();

        $crontabTask = new CrontabTask();
        $crontabTask->name = 'delivery_get_orders_info';
        $crontabTask->description = 'Получение информации о заказе из службы доставки.';
        $crontabTask->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = CrontabTask::find()
            ->byName('delivery_send_orders')
            ->one();

        if ($crontabTask) {
            $crontabTask->delete();
        }

        $crontabTask = CrontabTask::find()
            ->byName('delivery_get_orders_info')
            ->one();

        if ($crontabTask) {
            $crontabTask->delete();
        }
    }
}
