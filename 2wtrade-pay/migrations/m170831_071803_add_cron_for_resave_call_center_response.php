<?php
use app\components\CustomMigration as Migration;
use app\models\Notification;
use app\modules\administration\models\CrontabTask;

/**
 * Class m170831_071803_add_cron_for_resave_call_center_response
 */
class m170831_071803_add_cron_for_resave_call_center_response extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $crontabTask = CrontabTask::findOne(['name' => 'cc_try_to_save_sent_leads']);
        if (!$crontabTask) {
            $crontabTask = new CrontabTask();
            $crontabTask->name = 'cc_try_to_save_sent_leads';
            $crontabTask->description = "Попытка сохранения заказов, которые не удалось сохранить при отправке в КЦ";
            $crontabTask->save();
        }

        $model = new Notification([
            'description' => 'Отправка лидов в КЦ: Заказ #{order_id}, EXCEPTION: {error}',
            'trigger' => 'exception.sent.to.call.center',
            'type' => Notification::TYPE_DANGER,
            'group' => Notification::GROUP_SYSTEM,
            'active' => Notification::ACTIVE,
        ]);

        $model->save();

        $model = new Notification([
            'description' => 'Получение статусов из КЦ: Заказ #{order_id}, EXCEPTION: {error}',
            'trigger' => 'exception.get.statuses.from.call.center',
            'type' => Notification::TYPE_DANGER,
            'group' => Notification::GROUP_SYSTEM,
            'active' => Notification::ACTIVE,
        ]);

        $model->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $crontabTask = CrontabTask::findOne(['name' => 'cc_try_to_save_sent_leads']);
        if ($crontabTask instanceof CrontabTask) {
            $crontabTask->delete();
        }

        $model = Notification::findOne(['trigger' => 'exception.sent.to.call.center']);
        if ($model instanceof Notification) {
            $model->delete();
        }

        $model = Notification::findOne(['trigger' => 'exception.get.statuses.from.call.center']);
        if ($model instanceof Notification) {
            $model->delete();
        }
    }
}
