<?php
use app\components\CustomMigration as Migration;

/**
 * Class m170414_022412_add_new_source_2wstore
 */
class m170414_022412_add_new_source_2wstore extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('order', 'source', $this->string(50)->defaultValue(null));
        $this->alterColumn('partner', 'source', $this->string(50)->defaultValue(null));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('order', 'source', $this->enum(['adcombo', 'jeempo'])->defaultValue(null));
        $this->alterColumn('partner', 'source', $this->enum(['adcombo', 'jeempo'])->defaultValue(null));
    }
}
