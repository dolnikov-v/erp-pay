<?php

use app\modules\catalog\models\RequisiteDelivery;
use app\modules\report\models\Invoice;
use app\components\CustomMigration as Migration;

/**
 * Handles adding column_requisite_delivery_id to table `invoice`.
 */
class m180406_111818_add_column_requisite_delivery_id_to_invoice extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn(Invoice::tableName(), 'requisite_delivery_id', $this->integer());
        $this->addForeignKey(null, Invoice::tableName(), 'requisite_delivery_id', RequisiteDelivery::tableName(), 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn(Invoice::tableName(), 'requisite_delivery_id');
    }
}
