<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160926_100613_fix_logistic_list
 */
class m160926_100613_fix_logistic_list extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->renameColumn('order_logistic_list_excel', 'columns_hash', 'orders_hash');
        $this->renameColumn('order_logistic_list_excel', 'excel', 'system_file_name');
        $this->renameColumn('order_logistic_list_excel', 'excel_local', 'delivery_file_name');

        $this->addColumn('order_logistic_list_excel', 'sent_user_id', $this->integer()->defaultValue(null)->after('user_id'));
        $this->addColumn('order_logistic_list_excel', 'sent_at', $this->integer()->defaultValue(null)->after('updated_at'));

        $this->dropColumn('order_logistic_list_excel', 'format');
        $this->alterColumn('order_logistic_list_excel', 'user_id', $this->integer()->defaultValue(null));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey($this->getFkName('order_logistic_list_excel', 'user_id'), 'order_logistic_list_excel');
        $this->alterColumn('order_logistic_list_excel', 'user_id', $this->integer()->notNull());
        $this->addForeignKey(null, 'order_logistic_list_excel', 'user_id', 'user', 'id', self::CASCADE, self::RESTRICT);

        $this->addColumn('order_logistic_list_excel', 'format', $this->string(20)->notNull()->after('list_id'));

        $this->renameColumn('order_logistic_list_excel', 'delivery_file_name', 'excel_local');
        $this->renameColumn('order_logistic_list_excel', 'system_file_name', 'excel');
        $this->renameColumn('order_logistic_list_excel', 'orders_hash', 'columns_hash');

        $this->dropColumn('order_logistic_list_excel', 'sent_at');
        $this->dropColumn('order_logistic_list_excel', 'sent_user_id');
    }
}
