<?php
use app\components\CustomMigration as Migration;
use app\models\Notification;

/**
 * Class m180115_045151_add_sms_buyouts_notifications
 */
class m180115_045151_add_sms_buyouts_notifications extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $notification = new Notification([
            'description' => 'Общая информация по выкупам всех команд {text}',
            'trigger' => Notification::TRIGGER_ALL_TEAMS_BUYOUTS_NOTIFICATION,
            'type' => Notification::TYPE_INFO,
            'group' => Notification::GROUP_REPORT,
            'active' => Notification::ACTIVE,
        ]);
        $notification->save(false);

        $notification = new Notification([
            'description' => 'Информация по выкупам команды {text}',
            'trigger' => Notification::TRIGGER_TEAM_BUYOUTS_NOTIFICATION,
            'type' => Notification::TYPE_INFO,
            'group' => Notification::GROUP_REPORT,
            'active' => Notification::ACTIVE,
        ]);
        $notification->save(false);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $notification = Notification::findOne(['trigger' => Notification::TRIGGER_ALL_TEAMS_BUYOUTS_NOTIFICATION]);
        $notification->delete();

        $notification = Notification::findOne(['trigger' => Notification::TRIGGER_TEAM_BUYOUTS_NOTIFICATION]);
        $notification->delete();
    }
}
