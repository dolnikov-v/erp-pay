<?php
use app\components\CustomMigration as Migration;
use app\models\Notification;
use app\models\UserNotificationSetting;
use app\models\NotificationUser;
use app\models\NotificationRole;

/**
 * Class m180301_094754_make_auto_penalty
 */
class m180301_094754_make_auto_penalty extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $notification = Notification::findOne(['trigger' => Notification::TRIGGER_MAKE_AUTO_PENALTY]);
        if (!($notification instanceof Notification)) {
            $notification = new Notification([
                'description' => 'A penalty was given to an employee {person} from the office {office} for {penalty} {date}',
                'trigger' => Notification::TRIGGER_MAKE_AUTO_PENALTY,
                'type' => Notification::TYPE_INFO,
                'group' => Notification::GROUP_SYSTEM,
                'active' => Notification::ACTIVE,
            ]);
            $notification->save(false);
        } else {
            $notification->description = 'A penalty was given to an employee {person} from the office {office} for {penalty} {date}';
            $notification->save(false);
        }

        try {
            $record = new UserNotificationSetting();
            $record->user_id = 285; // biryukov.a
            $record->trigger = Notification::TRIGGER_MAKE_AUTO_PENALTY;
            $record->active = Notification::ACTIVE;
            $record->telegram_active = 1;
            $record->save();
        } catch (Exception $e) {

        }

        try {
            $record = new NotificationUser();
            $record->user_id = 285; // biryukov.a
            $record->trigger = Notification::TRIGGER_MAKE_AUTO_PENALTY;
            $record->active = Notification::ACTIVE;
            $record->save();
        } catch (Exception $e) {

        }

        try {
            $record = new NotificationUser();
            $record->user_id = 145; // yulia
            $record->trigger = Notification::TRIGGER_MAKE_AUTO_PENALTY;
            $record->active = Notification::ACTIVE;
            $record->save();
        } catch (Exception $e) {

        }

        try {
            $record = new NotificationUser();
            $record->user_id = 6; // smykov
            $record->trigger = Notification::TRIGGER_MAKE_AUTO_PENALTY;
            $record->active = Notification::ACTIVE;
            $record->save();
        } catch (Exception $e) {

        }

        NotificationUser::deleteAll(['trigger' => 'make.auto.penalty.social']);
        NotificationUser::deleteAll(['trigger' => 'make.auto.penalty.late']);

        UserNotificationSetting::updateAll(['trigger' => Notification::TRIGGER_MAKE_AUTO_PENALTY], ['trigger' => 'make.auto.penalty.late']);
        UserNotificationSetting::deleteAll(['trigger' => 'make.auto.penalty.social']);

        NotificationRole::updateAll(['trigger' => Notification::TRIGGER_MAKE_AUTO_PENALTY], ['trigger' => 'make.auto.penalty.late']);
        NotificationRole::deleteAll(['trigger' => 'make.auto.penalty.social']);

        Notification::deleteAll(['trigger' => 'make.auto.penalty.late']);
        Notification::deleteAll(['trigger' => 'make.auto.penalty.social']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

    }
}
