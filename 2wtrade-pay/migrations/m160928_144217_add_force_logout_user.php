<?php
use app\components\CustomMigration as Migration;

/**
 * Class m160928_144217_add_force_logout_user
 */
class m160928_144217_add_force_logout_user extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('user', 'force_logout', $this->smallInteger()->defaultValue(0)->after('status'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'force_logout');
    }
}
