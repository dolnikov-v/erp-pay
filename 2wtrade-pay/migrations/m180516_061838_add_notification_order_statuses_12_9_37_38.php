<?php

use app\components\CustomMigration as Migration;
use app\models\Notification;
use app\modules\administration\models\CrontabTask;


/**
 * Class m180516_061838_add_notification_order_statuses_12_9_37_38
 */
class m180516_061838_add_notification_order_statuses_12_9_37_38 extends Migration
{

    const RU_TEXT = '{date} заказов не передано на доставку более 12 ч / общее кол-во не отправленных заказов ({in12hour}/{total}): {info}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $notification = Notification::findOne(['trigger' => Notification::TRIGGER_REPORT_ORDERS_STATUS_IN_12_9_37_38]);

        if (!($notification instanceof Notification)) {
            $notification = new Notification([
                'description' => self::RU_TEXT,
                'trigger' => Notification::TRIGGER_REPORT_ORDERS_STATUS_IN_12_9_37_38,
                'type' => Notification::TYPE_WARNING,
                'group' => Notification::GROUP_REPORT,
                'active' => Notification::ACTIVE,
            ]);
            $notification->save(false);


            $this->insert('i18n_source_message', ['category' => 'common', 'message' => self::RU_TEXT]);
            $id = Yii::$app->db->getLastInsertID();
            $this->insert('i18n_message', ['id' => $id, 'language' => 'en-US', 'translation' => '{date} orders were not forwarded for delivery more than 12 hours / total number of not sent orders ({in12hour} / {total}): {info}']);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $notification = Notification::findOne(['trigger' => Notification::TRIGGER_REPORT_ORDERS_STATUS_IN_12_9_37_38]);

        if ($notification instanceof Notification) {
            $notification->delete();
        }

        $this->delete('i18n_source_message', ['category' => 'common', 'message' => self::RU_TEXT]);

    }
}
