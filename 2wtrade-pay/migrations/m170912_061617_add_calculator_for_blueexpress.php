<?php
use app\components\CustomMigration as Migration;
use app\modules\delivery\models\DeliveryChargesCalculator;

/**
 * Class m170912_061617_add_calculator_for_blueexpress
 */
class m170912_061617_add_calculator_for_blueexpress extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert(DeliveryChargesCalculator::tableName(), [
            'name' => 'Blue Express',
            'class_path' => 'app\modules\delivery\components\charges\blueexpress\BlueExpress',
            'active' => 1,
            'created_at' => time(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete(DeliveryChargesCalculator::tableName(), ['name' => 'Blue Express']);
    }
}
