<?php

use app\components\CustomMigration as Migration;
use app\modules\order\models\OrderFinanceFact;
use app\modules\order\models\OrderFinancePretension;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * Class m180801_050753_move_pretension_on_order_finance_fact_to_order_finance_pretension
 */
class m180801_050753_move_pretension_on_order_finance_fact_to_order_finance_pretension extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $pretensions = [];

        //на локальной бд все вперемешку. на мастере всё путём должно быть.
        $this->execute("SET foreign_key_checks = 0;");

        $query = OrderFinanceFact::find()
            ->where(['is not', 'pretension', null]);

        if ($query->exists()) {
            foreach ($query->batch(50) as $orderFinanceFacts) {
                /** @var OrderFinanceFact $fact */
                foreach ($orderFinanceFacts as $fact) {
                    $pretensions[$fact->id] = [
                        'type' => $fact->pretension,
                        'status' => OrderFinancePretension::PRETENSION_STATUS_IN_PROGRESS,
                        'comment' => null,
                        'order_id' => $fact->order_id,
                        'delivery_report_record_id' => $fact->delivery_report_record_id,
                        'delivery_report_id' => $fact->delivery_report_id,
                        'created_at' => time(),
                        'updated_at' => time()
                    ];
                }
            }

            yii::$app->db->createCommand()->batchInsert(
                OrderFinancePretension::tableName(),
                ['type', 'status', 'comment', 'order_id', 'delivery_report_record_id', 'delivery_report_id', 'created_at', 'updated_at'],
                $pretensions
            )->execute();
        }

        $this->execute("SET foreign_key_checks = 1;");

        //оставить только по одному факту на запись отчета
        //лишние факты по заказам грохну по id по выбрке этого запроса
        $query = OrderFinanceFact::find()
            ->select([
                'id' => new Expression('max(id)')
            ])
            ->groupBy(['order_id', 'delivery_report_record_id', 'delivery_report_id'])
            ->having(new Expression('count(*) >= 1'));

        //Факты перегенерим отдельной задачей
        //if($query->exists()){
        //     OrderFinanceFact::deleteAll(['not in', 'id', ArrayHelper::getColumn($query->asArray()->all(), 'id')]);
        //}


    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete(OrderFinancePretension::tableName(), []);
    }
}
