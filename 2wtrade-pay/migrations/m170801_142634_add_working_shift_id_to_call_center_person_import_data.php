<?php

use app\components\CustomMigration as Migration;

/**
 * Handles adding working_shift_id to table `call_center_person_import_data`.
 */
class m170801_142634_add_working_shift_id_to_call_center_person_import_data extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('call_center_person_import_data', 'working_shift_id', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('call_center_person_import_data', 'working_shift_id');
    }
}
