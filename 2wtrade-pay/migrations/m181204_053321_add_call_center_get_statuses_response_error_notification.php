<?php
use app\components\CustomMigration as Migration;
use app\models\Notification;
use yii\db\Query;

/**
 * Class m181204_053321_add_call_center_get_statuses_response_error_notification
 */
class m181204_053321_add_call_center_get_statuses_response_error_notification extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $notificationId = (new Query())->select('id')
            ->from('notification')
            ->where(['trigger' => 'callcenter.getstatuses.error'])
            ->scalar();

        $data = [
            'description' => 'Заказ #{order_id} ({country}) не получил статус из КЦ. ERROR: {error}',
            'trigger' => 'callcenter.getstatuses.error',
            'type' => Notification::TYPE_DANGER,
            'group' => Notification::GROUP_SYSTEM,
            'active' => Notification::ACTIVE,
            'created_at' => time(),
            'updated_at' => time(),
        ];

        if (!$notificationId) {
            $this->insert('notification', $data);
        } else {
            $this->update('notification', $data, ['id' => $notificationId]);
        }

        $roles = (new Query())->select('role')->from('notification_role')->where(['trigger' => 'callcenter.response.error'])->column();

        foreach ($roles as $role) {
            $this->insert('notification_role', [
                'role' => $role,
                'trigger' => 'callcenter.getstatuses.error',
                'created_at' => time(),
                'updated_at' => time(),
            ]);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete('user_notification_setting', ['trigger' => 'callcenter.getstatuses.error']);
        $this->delete('notification', ['trigger' => 'callcenter.getstatuses.error']);
    }
}
