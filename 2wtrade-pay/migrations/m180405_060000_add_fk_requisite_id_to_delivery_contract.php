<?php

use app\modules\catalog\models\RequisiteDelivery;
use app\modules\delivery\models\DeliveryContract;
use app\components\CustomMigration as Migration;

/**
 * Handles adding fk_requisite_id to table `delivery_contract`.
 */
class m180405_060000_add_fk_requisite_id_to_delivery_contract extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addForeignKey(null, DeliveryContract::tableName(), 'requisite_delivery_id', RequisiteDelivery::tableName(), 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(DeliveryContract::tableName(), 'requisite_delivery_id');
    }
}
