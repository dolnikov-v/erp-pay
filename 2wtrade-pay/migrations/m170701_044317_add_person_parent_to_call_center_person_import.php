<?php

use app\components\CustomMigration;

/**
 * Handles adding person_parent to table `call_center_person_import_data`.
 */
class m170701_044317_add_person_parent_to_call_center_person_import extends CustomMigration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('call_center_person_import_data', 'head_id', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('call_center_person_import_data', 'head_id');
    }
}
