<?php
use app\components\CustomMigration as Migration;
use app\models\Notification;

/**
 * Class m161002_071128_add_notification_product_unaccounted
 */
class m161002_071128_add_notification_product_unaccounted extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $model = new Notification([
            'description' => 'Склады: {count} товаров {product_name} не привязано к {object} {name}.',
            'trigger' => 'product.unaccounted',
            'type' => Notification::TYPE_WARNING,
            'group' => Notification::GROUP_REPORT,
            'active' => Notification::ACTIVE,
        ]);

        $model->save(false);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $model = Notification::findOne(['trigger' => 'product.unaccounted']);

        if ($model instanceof Notification) {
            $model->delete();
        }
    }
}
