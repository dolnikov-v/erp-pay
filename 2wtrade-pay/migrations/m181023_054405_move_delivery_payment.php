<?php
use app\components\CustomMigration as Migration;

/**
 * Class m181023_054405_move_delivery_payment
 */
class m181023_054405_move_delivery_payment extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        // 1 удаляем пустую таблицу
        $this->dropTable('delivery_payment');

        // 2 добавить поля в payment_order_delivery
        $this->addColumn('payment_order_delivery', 'delivery_id', $this->integer()->after('rate'));
        $this->addColumn('payment_order_delivery', 'receiving_bank', $this->string()->after('name'));
        $this->addColumn('payment_order_delivery', 'payer', $this->string()->after('receiving_bank'));
        $this->addColumn('payment_order_delivery', 'bank_recipient', $this->string()->after('payer'));

        // 3 заполнить delivery_id в payment_order_delivery
        $sql = <<<SQL
      UPDATE payment_order_delivery left join delivery_report on payment_order_delivery.delivery_report_id = delivery_report.id 
      SET payment_order_delivery.delivery_id = delivery_report.delivery_id;
SQL;
        $this->execute($sql);

        // 4 добавить ключ для нового поля
        $this->addForeignKey($this->getFkName('payment_order_delivery', 'delivery_id'), 'payment_order_delivery', 'delivery_id', 'delivery', 'id', self::CASCADE, self::CASCADE);

        // 5 создать таблицу для множественной связи
        $this->createTable('payment_delivery_report', [
            'payment_id' => $this->integer(),
            'delivery_report_id' => $this->integer()
        ]);

        $this->addPrimaryKey($this->getPkName('payment_delivery_report', [
            'payment_id',
            'delivery_report_id'
        ]), 'payment_delivery_report', ['payment_id', 'delivery_report_id']);

        $this->addForeignKey($this->getFkName('payment_delivery_report', 'payment_id'), 'payment_delivery_report', 'payment_id', 'payment_order_delivery', 'id', self::CASCADE, self::CASCADE);
        $this->addForeignKey($this->getFkName('payment_delivery_report', 'delivery_report_id'), 'payment_delivery_report', 'delivery_report_id', 'delivery_report', 'id', self::CASCADE, self::CASCADE);

        // 6 заполнить данными множественную связь
        $sql = <<<SQL
      INSERT INTO payment_delivery_report (payment_id, delivery_report_id) SELECT id, delivery_report_id FROM payment_order_delivery WHERE delivery_report_id is not NULL;
SQL;
        $this->execute($sql);

        // 7 удалить ключ еденичной связи
        $this->dropForeignKey($this->getFkName('payment_order_delivery', 'delivery_report_id'), 'payment_order_delivery');

        // 8 удалить поле еденичной связи
        $this->dropColumn('payment_order_delivery', 'delivery_report_id');

        // 9 переименовать таблицу
        // $this->renameTable('payment_order_delivery', 'payment_delivery');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        // 9 переименовать таблицу
        // $this->renameTable('payment_delivery', 'payment_order_delivery');

        // 8 вернуть поле еденичной связи
        $this->addColumn('payment_order_delivery', 'delivery_report_id', $this->integer());

        // 7 вернуть ключ еденичной связи
        $this->addForeignKey($this->getFkName('payment_order_delivery', 'delivery_report_id'), 'payment_order_delivery', 'delivery_report_id', 'delivery_report', 'id', self::CASCADE, self::CASCADE);

        // 6 заполнить данными еденичную связь
        $sql = <<<SQL
      UPDATE payment_order_delivery 
      LEFT JOIN payment_delivery_report on payment_order_delivery.id = payment_delivery_report.payment_id 
      SET payment_order_delivery.delivery_report_id = payment_delivery_report.delivery_report_id;
SQL;
        $this->execute($sql);

        // 5 удалить таблицу для множественной связи
        $this->dropTable('payment_delivery_report');

        // 4 удалить ключ для нового поля
        $this->dropForeignKey($this->getFkName('payment_order_delivery', 'delivery_id'), 'payment_order_delivery');

        // 2 удалить поля из payment_order_delivery
        $this->dropColumn('payment_order_delivery', 'delivery_id');
        $this->dropColumn('payment_order_delivery', 'receiving_bank');
        $this->dropColumn('payment_order_delivery', 'payer');
        $this->dropColumn('payment_order_delivery', 'bank_recipient');

        // 1 добавляем таблицу
        $this->createTable('delivery_payment', [
            'id' => $this->primaryKey(),
            'delivery_id' => $this->integer(),
            'paid_at' => $this->integer(),
            'receiving_bank' => $this->string(),
            'payer' => $this->string(),
            'bank_recipient' => $this->string(),
            'remittance_information' => $this->string(),
            'income' => $this->double(),
            'currency' => $this->string(),
            'currency_rate' => $this->double(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addForeignKey(null, 'delivery_payment', 'delivery_id', 'delivery', 'id', self::CASCADE, self::CASCADE);
    }
}
