<?php
namespace app\controllers;

use app\components\web\Controller;
use Yii;

/**
 * Class ErrorsController
 * @package app\controllers
 */
class ErrorsController extends Controller
{
    public $layout = 'error';
    
    /**
     * @return array
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }
}
