<?php

namespace app\controllers\console;


use yii\console\Controller;
use yii\console\ExitCode;
use yii\db\Connection;
use yii\db\Query;
use yii\di\Instance;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;

/**
 * Class DbController
 * @package app\controllers\console
 */
class DbController extends Controller
{
    const MAX_CONFIGURATION_KEY_LENGTH = 255;
    const MAX_CONFIGURATION_VALUE_LENGTH = 255;

    /**
     * Список ключ-значение, которые записываются в БД
     * @var array
     */
    public $configurationValues = [];

    /**
     * @var string|Connection
     */
    public $db = 'db';

    /**
     * @var string
     */
    public $configurationTable = '{{%db_config}}';

    /**
     * @var string
     */
    public $defaultAction = 'up-configuration';

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->db = Instance::ensure($this->db, Connection::className());
            return true;
        }
        return false;
    }

    /**
     * Обновление значений конфигурации для БД согласно заданным значениям
     * @throws \yii\db\Exception
     */
    public function actionUpConfiguration()
    {
        if ($this->db->schema->getTableSchema($this->configurationTable, true) === null) {
            $this->createConfigurationTable();
        }
        if (!is_array($this->configurationValues)) {
            $this->stderr("Database configuration is not correct!");
            return ExitCode::DATAERR;
        }
        $dbValues = ArrayHelper::map((new Query())->from($this->configurationTable)
            ->select(['key', 'value'])
            ->all($this->db), 'key', 'value');
        $insert = [];
        $delete = array_diff(array_keys($dbValues), array_keys($this->configurationValues));
        foreach ($this->configurationValues as $key => $value) {
            if (!in_array($key, array_keys($dbValues))) {
                $insert[] = [
                    'key' => $key,
                    'value' => $value
                ];
            } elseif($value != $dbValues[$key]) {
                $this->db->createCommand()->update($this->configurationTable, ['value' => $value], ['key' => $key])->execute();
            }
        }
        if (!empty($insert)) {
            $this->db->createCommand()->batchInsert($this->configurationTable, ['key', 'value'], $insert)->execute();
        }
        if (!empty($delete)) {
            $this->db->createCommand()->delete($this->configurationTable, ['key' => $delete])->execute();
        }
        $this->stdout("Configuration values for database was successful update.", Console::FG_GREEN);
        return ExitCode::OK;
    }

    /**
     * Удаление всех записей из таблицы с конфигурацией для БД
     * @throws \yii\db\Exception
     */
    public function actionClearConfiguration()
    {
        if ($this->confirm(
            "Are you sure you want to clear database configuration?\nAll data will be lost irreversibly!")) {
            if ($this->db->schema->getTableSchema($this->configurationTable, true) !== null) {
                $this->db->createCommand()->truncateTable($this->configurationTable)->execute();
            }
        } else {
            $this->stdout('Action was cancelled by user. Nothing has been performed.');
        }
    }

    /**
     * Удаление таблицы с конфигурацией для БД
     * @throws \yii\db\Exception
     */
    public function actionDropConfiguration()
    {
        if (YII_ENV_PROD) {
            $this->stdout("YII_ENV is set to 'prod'.\nDropping database configuration is not possible on production systems.\n");
            return ExitCode::OK;
        }
        if ($this->confirm(
            "Are you sure you want to drop database configuration?\nAll data will be lost irreversibly!")) {
            if ($this->db->schema->getTableSchema($this->configurationTable, true) !== null) {
                $this->removeConfigurationTable();
            }
        } else {
            $this->stdout('Action was cancelled by user. Nothing has been performed.');
        }
        return ExitCode::OK;
    }

    /**
     * @throws \yii\db\Exception
     */
    protected function createConfigurationTable()
    {
        $tableName = $this->db->schema->getRawTableName($this->configurationTable);
        $this->stdout("Creating db configuration table \"$tableName\"...", Console::FG_YELLOW);
        $this->db->createCommand()->createTable($this->configurationTable, [
            'key' => 'varchar(' . static::MAX_CONFIGURATION_KEY_LENGTH . ') NOT NULL PRIMARY KEY',
            'value' => 'varchar(' . static::MAX_CONFIGURATION_VALUE_LENGTH . ')',
        ])->execute();
        $this->stdout("Done.\n", Console::FG_GREEN);
    }

    /**
     * @throws \yii\db\Exception
     */
    protected function removeConfigurationTable()
    {
        $command = $this->db->createCommand();
        $command->dropTable($this->configurationTable)->execute();
    }
}