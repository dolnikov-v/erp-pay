<?php

namespace app\controllers\console;


use app\components\console\Controller;
use yii\helpers\Console;

/**
 * Управление созданием и конфигурированием списком сервисов в Amazon AWS
 *
 * Class AmazonController
 * @package app\commands
 */
class AmazonController extends Controller
{
    /**
     * Список очередей
     *
     * @var array
     */
    public $sqsQueueList = [];

    /**
     * @var string
     */
    public $defaultAction = 'create-queues';

    /**
     * Создание очередей в Amazon SQS
     */
    public function actionCreateQueues()
    {
        foreach ($this->sqsQueueList as $queueConfig) {
            if (!isset($queueConfig['QueueName'])) {
                continue;
            }
            if ($this->sqsClient->existsQueue($queueConfig['QueueName'])) {
                Console::output("Queue \"{$queueConfig['QueueName']}\" exists.");
                continue;
            }
            Console::output("Creating queue \"{$queueConfig['QueueName']}\"...");
            $result = $this->sqsClient->createQueue($queueConfig);
            Console::output("Queue \"{$queueConfig['QueueName']}\" was created with URL \"{$result->get('QueueUrl')}\"");
        }
    }
}