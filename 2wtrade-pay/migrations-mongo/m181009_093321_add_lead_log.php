<?php

class m181009_093321_add_lead_log extends \yii\mongodb\Migration
{
    /**
     * @return bool
     */
    public function up()
    {
        $this->createCollection('lead_log');
        $this->createIndex('lead_log', ['order_id' => 1, 'created_at' => 1]);
        return true;
    }

    /**
     * @return bool
     */
    public function down()
    {
        $this->dropCollection('lead_log');
        return true;
    }
}
