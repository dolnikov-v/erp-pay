<?php

/**
 * Class m181011_081451_create_call_center_check_request_log
 */
class m181011_081451_create_call_center_check_request_log extends \yii\mongodb\Migration
{
    /**
     * @return bool
     */
    public function up()
    {
        $this->createCollection('call_center_check_request_log');
        $this->createIndex('call_center_check_request_log', ['request_id' => 1, 'created_at' => -1]);
        $this->createCollection('call_center_check_request_action_log');
        $this->createIndex('call_center_check_request_action_log', ['request_id' => 1, 'type' => 1, 'created_at' => -1]);
        return true;
    }

    /**
     * @return bool
     */
    public function down()
    {
        $this->dropCollection('call_center_check_request_action_log');
        $this->dropCollection('call_center_check_request_log');
        return true;
    }
}