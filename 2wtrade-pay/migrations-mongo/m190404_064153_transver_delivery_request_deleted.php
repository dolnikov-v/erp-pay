<?php

use yii\db\Query as MySqlQuery;
use yii\mongodb\Migration;

/**
 * Class m190404_064153_transver_delivery_request_deleted
 */
class m190404_064153_transver_delivery_request_deleted extends Migration
{
    public function up()
    {
        try {
            $this->createCollection('delivery_request_deleted');
            $this->createIndex('delivery_request_deleted', ['delivery_id' => 1, 'created_at' => -1]);
        } catch (\yii\mongodb\Exception $e) {

        }

        $mysql = new MySqlQuery();
        $mysql->select(['delivery_request_deleted.*'])->from('delivery_request_deleted');

        foreach ($mysql->batch(1000) as $records) {
            $data = [];
            foreach ($records as $record) {
                $data[] = [
                    'delivery_id' => $record['delivery_id'],
                    'order_id' => $record['order_id'],
                    'status' => $record['status'],
                    'tracking' => $record['tracking'],
                    'foreign_info' => $record['foreign_info'],
                    'last_update_info_at' => $record['last_update_info_at'],
                    'response_info' => $record['response_info'],
                    'comment' => $record['comment'],
                    'api_error' => $record['api_error'],
                    'api_error_type' => $record['api_error_type'],
                    'created_at' => $record['updated_at'],
                    'cron_launched_at' => $record['cron_launched_at'],
                    'sent_at' => $record['sent_at'],
                    'second_sent_at' => $record['second_sent_at'],
                    'sent_clarification_at' => $record['sent_clarification_at'],
                    'returned_at' => $record['returned_at'],
                    'approved_at' => $record['approved_at'],
                    'accepted_at' => $record['accepted_at'],
                    'paid_at' => $record['paid_at'],
                    'done_at' => $record['done_at'],
                    'finance_tracking' => $record['finance_tracking'],
                    'partner_name' => $record['partner_name'],
                    'delivery_partner_id' => $record['delivery_partner_id'],
                    'cs_send_response_at' => $record['cs_send_response_at'],
                    'unshipping_reason' => $record['unshipping_reason']
                ];
            }
            if (!empty($data)) {
                $this->batchInsert('delivery_request_deleted', $data);
            }
        }
    }

    public function down()
    {
        try {
            $this->dropCollection('delivery_request_deleted');
        } catch (\yii\mongodb\Exception $e) {

        }
    }
}
