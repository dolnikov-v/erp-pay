<?php

class m181107_093647_sql_error_log extends \yii\mongodb\Migration
{
    public function up()
    {
        $this->createCollection('sql_error_log');
        $this->createIndex('sql_error_log', ['sql_hash' => 'hashed']);
        $this->createIndex('sql_error_log', ['created_at' => 1], ['expireAfterSeconds' => 1209600]);
    }

    public function down()
    {
        $this->dropCollection('sql_error_log');
    }
}
