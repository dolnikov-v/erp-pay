<?php

/**
 * Class m180828_085249_create_request_logs_collections
 */
class m180828_085249_create_request_logs_collections extends \yii\mongodb\Migration
{
    public function up()
    {
        $this->createCollection('call_center_request_send_response');
        $this->createCollection('call_center_request_update_response');
        $this->createIndex('call_center_request_send_response', ['call_center_request_id' => 1, 'created_at' => -1]);
        $this->createIndex('call_center_request_update_response', ['call_center_request_id' => 1, 'created_at' => -1]);

        $this->createCollection('delivery_request_send_response');
        $this->createCollection('delivery_request_update_response');
        $this->createIndex('delivery_request_send_response', ['delivery_request_id' => 1, 'created_at' => -1]);
        $this->createIndex('delivery_request_update_response', ['delivery_request_id' => 1, 'created_at' => -1]);

    }

    public function down()
    {
        $this->dropCollection('delivery_request_update_response');
        $this->dropCollection('delivery_request_send_response');
        $this->dropCollection('call_center_request_update_response');
        $this->dropCollection('call_center_request_send_response');
        return true;
    }
}
