<?php

class m181114_060923_add_market_place_product_collection extends \yii\mongodb\Migration
{
    public function up()
    {
        $this->createCollection('market_place_product');
        $this->createIndex('market_place_product', ['market_place_id' => 1, 'id' => 1]);
        $this->createIndex('market_place_product', ['market_place_id' => 1, 'foreignId' => 1]);
    }

    public function down()
    {
        $this->dropCollection('market_place_product');
    }
}
