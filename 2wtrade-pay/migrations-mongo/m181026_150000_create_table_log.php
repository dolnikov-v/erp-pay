<?php

/**
 * Class m181026_150000_create_table_log
 */
class m181026_150000_create_table_log extends \yii\mongodb\Migration
{
    /**
     * @return bool
     */
    public function up()
    {
        $this->createCollection('table_log');
        $this->createIndex('table_log', ['table' => 1, 'created_at' => -1]);
        $this->createIndex('table_log', ['type' => 1, 'created_at' => -1]);
        $this->createIndex('table_log', ['id' => 1, 'created_at' => -1]);
        return true;
    }

    /**
     * @return bool
     */
    public function down()
    {
        $this->dropCollection('table_log');
        return true;
    }
}