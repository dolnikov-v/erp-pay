<?php

use yii\db\Query as MySqlQuery;

class m181107_035309_transver_apilog_for_orders extends \yii\mongodb\Migration
{
    public function up()
    {
        $this->createCollection('lead_create_log');
        $this->createIndex('lead_create_log', ['order_id' => 1, 'created_at' => -1]);

        $mysql = new MySqlQuery();
        $mysql->select(['api_log.*', 'order_id' => 'order.id'])
            ->from('order')
            ->innerJoin('api_log', 'api_log.id = api_log_id');
        foreach ($mysql->batch(1000) as $records) {
            $data = [];
            foreach ($records as $record) {
                $data[] = [
                    'order_id' => (int)$record['order_id'],
                    'input_data' => $record['input_data'],
                    'output_data' => $record['output_data'],
                    'created_at' => (int)$record['updated_at']
                ];
            }
            if (!empty($data)) {
                $this->batchInsert('lead_create_log', $data);
            }
        }

//        TODO: удалить после поле api_log_id из order. Не реализованно здесь из-за невозможности отката данной операции
    }

    public function down()
    {
        $this->dropCollection('lead_create_log');
    }
}
