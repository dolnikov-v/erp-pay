<?php
/**
 * @var array $data
 * @var string $logoBase64
 * @var array $penalties
 * @var array $bonuses
 * @var array $times
 * @var array $dates
 * @var array $plan
 * @var boolean $calcSalaryUSD
 * @var boolean $calcSalaryLocal
 * @var string $dateFrom
 * @var string $dateTo
 * @var string $lang
 */

?>
    <table class="head">
        <tr>
            <td style="width: 35%;">
                <img src="<?= $logoBase64 ?>" style="width: 6.85cm; height: 1.87cm;">
            </td>
            <td style="width: 30%; padding-left: 30px;">
                <br/>
                Head office<br/>
                www.2wtrade.com<br/>
                mail: info@2wtrade.com
            </td>
            <td style="width: 35%;"><b>2WTRADE LLP</b><br/>
                Address : 71-75 SHELTON STREET, COVENT GARDEN, LONDON, ENGLAND, WC2H 9JQ
            </td>
        </tr>
    </table>
    <br>
    <table>
        <?php foreach ($data as $row): ?>
            <tr>
                <th><?= $row['label'] ?></th>
                <td><?= $row['value'] ?></td>
            </tr>
        <?php endforeach; ?>
    </table>

<?php if ($penalties): ?>
    <h3><?= Yii::t('common', 'Штрафы', [], $lang) ?></h3>
    <table class="head">
        <tr>
            <th><?= Yii::t('common', 'Тип штрафа', [], $lang) ?></th>
            <th style="text-align: center"><?= Yii::t('common', 'Дата штрафа', [], $lang) ?></th>
            <th><?= Yii::t('common', 'Комментарий', [], $lang) ?></th>
            <?php if ($calcSalaryUSD): ?>
                <th style="text-align: center"><?= Yii::t('common', 'Сумма штрафа в USD', [], $lang) ?></th>
            <?php endif; ?>
            <?php if ($calcSalaryLocal): ?>
                <th style="text-align: center"><?= Yii::t('common', 'Сумма штрафа в местной валюте', [], $lang) ?></th>
            <?php endif; ?>
        </tr>
        <?php foreach ($penalties as $penalty): ?>
            <tr>
                <td><?= (!empty($penalty->penalty_type_id) ? $penalty->penaltyType->name : '') ?></td>
                <td style="text-align: center"><?= Yii::$app->formatter->asDate($penalty->date) ?></td>
                <td><?= $penalty->comment ?></td>
                <?php if ($calcSalaryUSD): ?>
                    <td style="text-align: right"><?= round($penalty->penalty_sum_usd, 2) ?></td>
                <?php endif; ?>
                <?php if ($calcSalaryLocal): ?>
                    <td style="text-align: right"><?= round($penalty->penalty_sum_local, 2) ?></td>
                <?php endif; ?>
            </tr>
        <?php endforeach; ?>
    </table>
<?php endif; ?>

<?php if ($bonuses): ?>
    <h3><?= Yii::t('common', 'Бонусы', [], $lang) ?></h3>
    <table class="head">
        <tr>
            <th><?= Yii::t('common', 'Тип бонуса', [], $lang) ?></th>
            <th style="text-align: center"><?= Yii::t('common', 'Дата бонуса', [], $lang) ?></th>
            <th><?= Yii::t('common', 'Комментарий', [], $lang) ?></th>
            <th style="text-align: center"><?= Yii::t('common', 'Процент', [], $lang) ?></th>
            <th style="text-align: center"><?= Yii::t('common', 'Процент часа', [], $lang) ?></th>
            <th style="text-align: center"><?= Yii::t('common', 'Час', [], $lang) ?></th>
            <?php if ($calcSalaryUSD): ?>
                <th style="text-align: center"><?= Yii::t('common', 'Сумма штрафа в USD', [], $lang) ?></th>
            <?php endif; ?>
            <?php if ($calcSalaryLocal): ?>
                <th style="text-align: center"><?= Yii::t('common', 'Сумма штрафа в местной валюте', [], $lang) ?></th>
            <?php endif; ?>
        </tr>
        <?php foreach ($bonuses as $bonus): ?>
            <tr>
                <td><?= (!empty($bonus->bonus_type_id) ? $bonus->bonusType->name : '') ?></td>
                <td style="text-align: center"><?= Yii::$app->formatter->asDate($bonus->date) ?></td>
                <td><?= $bonus->comment ?></td>
                <td style="text-align: right"><?= ($bonus->bonus_percent ? round($bonus->bonus_percent, 2) : '') ?></td>
                <td style="text-align: right"><?= ($bonus->bonus_percent_hour ? round($bonus->bonus_percent_hour, 2) : '') ?></td>
                <td style="text-align: right"><?= ($bonus->bonus_hour ? round($bonus->bonus_hour, 2) : '') ?></td>
                <?php if ($calcSalaryUSD): ?>
                    <td style="text-align: right"><?= round($bonus->bonus_sum_usd, 2) ?></td>
                <?php endif; ?>
                <?php if ($calcSalaryLocal): ?>
                    <td style="text-align: right"><?= round($bonus->bonus_sum_local, 2) ?></td>
                <?php endif; ?>
            </tr>
        <?php endforeach; ?>
    </table>
<?php endif; ?>

<?php if ($times): ?>
    <h3><?= Yii::t('common', 'Табель', [], $lang) ?></h3>
    <?php
    $month = date('n', strtotime($dateFrom));
    $year = date('y', strtotime($dateFrom));
    $running_day = date('N', strtotime($dateFrom));
    $days_in_month = date('t', strtotime($dateFrom));
    $headings = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
    ?>
    <table class="calendar">
        <tr>
            <?php foreach ($headings as $heading): ?>
                <th class="h"><?= $heading ?></th>
            <?php endforeach; ?>
        </tr>
        <tr>
        <?php for ($x = 1; $x < $running_day; $x++): ?>
            <td class="wrap np"></td>
        <?php endfor; ?>
            <?php for ($day = 1; $day <= $days_in_month; $day++):
            $cur_date = date('Y-m-d', mktime(0, 0, 0, $month, $day, $year));
            ?>
            <td class="wrap">
                <table class="day">
                    <tr>
                        <th><?= $day ?></th>
                    </tr>
                    <tr>
                        <td>
                            <?= round(($times[$cur_date] ?? 0), 1) ?> / <?= round(($plan[$cur_date] ?? 0), 1) ?>
                        </td>
                    </tr>
                </table>
            </td>
            <?php if ($running_day == 7): ?>
        </tr>
        <?php if (($day + 1) <= $days_in_month): ?>
        <tr>
            <?php endif;
            $running_day = 1;
            ?>
            <?php else:
                $running_day++;
            endif;
            ?>
            <?php endfor; ?>
            <?php if ($running_day != 1): ?>
                <?php for ($x = $running_day; $x <= 7; $x++): ?>
                    <td class="wrap np"></td>
                <?php endfor; ?>
            <?php endif; ?>
        </tr>
    </table>
<?php endif; ?>