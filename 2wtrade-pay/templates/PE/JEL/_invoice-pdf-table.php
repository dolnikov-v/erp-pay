<?php
/** @var string $InvPrint */
/** @var string $P_name1 */
/** @var string $P_q1 */
/** @var string $P_pr1 */
/** @var string $T1 */
/** @var string $Tt1 */
/** @var string $P_name2 */
/** @var string $P_q2 */
/** @var string $P_pr2 */
/** @var string $T2 */
/** @var string $Tt2 */
/** @var string $P_name3 */
/** @var string $P_q3 */
/** @var string $P_pr3 */
/** @var string $T3 */
/** @var string $Tt3 */
/** @var string $CC */
/** @var string $Sum */
/** @var string $InvoiceDateOne */
/** @var string $Pid */
/** @var string $InvoiceDateTwo */
/** @var string $Buyer */
/** @var string $Address */
/** @var string $Phone */
/** @var string $P_qS */
/** @var string $TS */
/** @var string $Sum_word */
/** @var string $logoBase64 */
?>

<table cellspacing="0" cellpadding="0" style="border: 0.5mm solid #000000; margin: 0; width: 100%; font-size: 8pt;">
    <tbody>
    <tr>
        <td rowspan="2" style="padding-left: 7.3mm; padding-top: 1.5mm; vertical-align: top; width: 98mm;">
            <img src="<?= $logoBase64 ?>"/>
        </td>
        <td style="color: #17375d; font-size: 10pt; font-weight: bold; height: 10mm; padding-top: 3.6mm; vertical-align: top;">
            2WTRADE LLP
        </td>
    </tr>
    <tr>
        <td style="color: #555555; font-size: 7pt; height: 10.6mm; vertical-align: top;">
            71-75 SHELTON STREET, COVENT GARDEN, LONDON,<br/>
            ENGLAND, WC2H 9JQ<br/>
            Phone: +442037693385, INFO@2WTRADE.COM
        </td>
    </tr>
    </tbody>
</table>

<table cellspacing="0" cellpadding="0" style="border-left: 0.5mm solid #000000; border-right: 0.5mm solid #000000; margin: 0; width: 100%; font-size: 7pt;">
    <tbody>
    <tr>
        <td colspan="3" style="border-bottom: 0.2mm solid #000000; font-size: 10pt; font-weight: bold; height: 4.6mm; padding-left: 60.5mm;">INVOICE</td>
    </tr>
    <tr>
        <td style="border-right: 0.2mm solid #000000; height: 5.7mm; padding-left: 2mm; vertical-align: top; width: 100.7mm;">Invoice No.</td>
        <td colspan="2" style="font-size: 8pt; font-weight: bold; padding-left: 2mm; vertical-align: top;">COD AMOUNT TO BE COLLECTED:</td>
    </tr>
    <tr>
        <td style="border-bottom: 0.2mm solid #000000; border-right: 0.2mm solid #000000; font-size: 8pt; height: 4.5mm; padding-left: 2mm; vertical-align: top;">
            <?= $InvPrint ?>
        </td>
        <td style="border-bottom: 0.2mm solid #000000; border-right: 0.2mm solid #000000; border-top: 0.2mm solid #000000; font-size: 9pt; font-weight: bold; height: 4mm; text-align: center; vertical-align: top; width: 25.5mm;">
            <?= $CC ?> <?= $Sum ?>
        </td>
        <td style="border-bottom: 0.2mm solid #000000;"></td>
    </tr>
    <tr>
        <td style="border-right: 0.2mm solid #000000; height: 4mm; padding-left: 2mm; vertical-align: top;">Dated</td>
        <td style="border-right: 0.2mm solid #000000; text-align: center; vertical-align: top;">Buyer's Order No.</td>
        <td style="padding-left: 2mm; vertical-align: top;">Dated</td>
    </tr>
    <tr>
        <td style="border-right: 0.2mm solid #000000; font-weight: bold; height: 3.4mm;  padding-left: 2mm; vertical-align: top;"><?= $InvoiceDateOne ?></td>
        <td style="border-right: 0.2mm solid #000000; font-weight: bold; text-align: center; vertical-align: top;"><?= $Pid ?></td>
        <td style="font-weight: bold; padding-left: 2mm; vertical-align: top;"><?= $InvoiceDateTwo ?></td>
    </tr>
    </tbody>
</table>

<table cellspacing="0" cellpadding="0" style="border: 0.5mm solid #000000; margin: 0; width: 100%; font-size: 7pt;">
    <tbody>
    <tr>
        <td style="border-right: 0.5mm solid #000000; font-weight: bold; height: 45mm; padding-left: 2mm; vertical-align: top; width: 100.9mm;">
            BUYER:<br/>
            <span style="font-size: 9pt;"><?= $Buyer ?></span><br/>
            <br/>
            SHIPPING ADDRESS:<br/>
            <span style="font-size: 9pt;"><?= $Address ?></span><br/>
            <br/>
            MOBILE:<br/>
            <p style="font-size: 9px;"><?= $Phone ?></p>
        </td>
        <td style="padding-left: 2mm; padding-top: 5mm; vertical-align: top;">
            <span style="font-size: 8pt; font-weight: bold;">IF UNDELIVERED, RETURN TO:</span><br/>
            <br/>
            <span style="font-size: 8pt; font-weight: bold;">Imp y Com Peru 2wtrade SAC</span><br/>
            <br/>
            <!--Carmen Covarrubias 32 oficina 304,
            Comuna Ñuñoa, Region Metropolitana, Santiago-->
            Cooperativa Cajabamba Mz. A, Lote 20-A,<br/>
            distrito Los Olivos, provincia y departamento de Lima
        </td>
    </tr>
    </tbody>
</table>

<table cellspacing="0" cellpadding="0" style="border-left: 0.5mm solid #000000; border-right: 0.5mm solid #000000; border-bottom: 0.5mm solid #000000; margin: 0; width: 100%; font-size: 7pt;">
    <tr>
        <td style="border-bottom: 0.2mm solid #000000; border-right: 0.2mm solid #000000; font-size: 8pt; font-weight: bold; height: 6mm; padding-left: 2mm; width: 115mm;">Product</td>
        <td style="border-bottom: 0.2mm solid #000000; border-right: 0.2mm solid #000000; font-size: 8pt; font-weight: bold; text-align: center; width: 16mm;">Quantity</td>
        <td style="border-bottom: 0.2mm solid #000000; border-right: 0.2mm solid #000000; font-size: 8pt; font-weight: bold; text-align: center; width: 22mm;">Price</td>
        <td style="border-bottom: 0.2mm solid #000000; border-right: 0.2mm solid #000000; font-size: 8pt; font-weight: bold; text-align: center; width: 16mm;">Tax</td>
        <td style="border-bottom: 0.2mm solid #000000; font-size: 8pt; font-weight: bold; text-align: center;">Total</td>
    </tr>
    <tr>
        <td style="border-right: 0.2mm solid #000000; height: 14.5mm; padding-left: 2mm; vertical-align: top;">
            <span style="font-weight: bold;"><?= $P_name1 ?></span><br/>
            <span style="font-weight: bold;"><?= $P_name2 ?></span><br/>
            <span style="font-weight: bold;"><?= $P_name3 ?></span><br/>
            <span style="font-weight: bold;"><?= $P_name4 ?></span><br/>
            <span style="font-weight: bold;"><?= $P_name5 ?></span>
            FREE/GIFT
        </td>
        <td style="border-right: 0.2mm solid #000000; text-align: center; vertical-align: top;">
            <?= $P_q1 ?><br/>
            <?= $P_q2 ?><br/>
            <?= $P_q3 ?><br/>
            <?= $P_q4 ?><br/>
            <?= $P_q5 ?>
            0
        </td>
        <td style="border-right: 0.2mm solid #000000; text-align: center; vertical-align: top;">
            <?= $P_pr1 ?><br/>
            <?= $P_pr2 ?><br/>
            <?= $P_pr3 ?><br/>
            <?= $P_pr4 ?><br/>
            <?= $P_pr5 ?>
        </td>
        <td style="border-right: 0.2mm solid #000000; text-align: center; vertical-align: top;">
            <?= $T1 ?><br/>
            <?= $T2 ?><br/>
            <?= $T3 ?><br/>
            <?= $T4 ?><br/>
            <?= $T5 ?>
        </td>
        <td style="font-weight: bold; text-align: right; padding-right: 2mm; vertical-align: top;">
            <?= $Tt1 ?><br/>
            <?= $Tt2 ?><br/>
            <?= $Tt3 ?><br/>
            <?= $Tt4 ?><br/>
            <?= $Tt5 ?>
        </td>
    </tr>
    <tr>
        <td style="border-top: 0.2mm solid #000000; border-right: 0.2mm solid #000000; height: 4.5mm; text-align: right; padding-right: 2mm; vertical-align: top;">TOTAL</td>
        <td style="border-right: 0.2mm solid #000000; font-weight: bold; text-align: center;"><?= $P_qS ?></td>
        <td style="border-right: 0.2mm solid #000000;"></td>
        <td style="border-right: 0.2mm solid #000000;"></td>
        <td style="border-top: 0.2mm solid #000000; font-weight: bold; padding-right: 2mm; text-align: right; vertical-align: top;"><?= $TS ?></td>
    </tr>
    <tr>
        <td style="border-bottom: 0.5mm solid #000000; border-top: 0.5mm solid #000000; border-right: 0.2mm solid #000000; height: 4.5mm; text-align: right; padding-right: 2mm; vertical-align: top;">TOTAL PRICE WITH DELIVERY</td>
        <td style="border-bottom: 0.5mm solid #000000; border-top: 0.5mm solid #000000; border-right: 0.2mm solid #000000;"></td>
        <td style="border-bottom: 0.5mm solid #000000; border-top: 0.5mm solid #000000; border-right: 0.2mm solid #000000;"></td>
        <td style="border-bottom: 0.5mm solid #000000; border-top: 0.5mm solid #000000; border-right: 0.2mm solid #000000;"></td>
        <td style="border-bottom: 0.5mm solid #000000; border-top: 0.5mm solid #000000; font-size: 8pt; text-align: right; padding-right: 2mm; vertical-align: top;"><?= $Sum ?></td>
    </tr>
    <tr>
        <td style="border-bottom: 0.2mm solid #000000; border-right: 0.2mm solid #000000; height: 6mm; padding-left: 2mm;">Amount Chargeable (in words): <span style="font-weight: bold;"><?= $CC ?></span> <span style="font-weight: bold;"><?= $Sum_word ?></span></td>
        <td colspan="4" rowspan="2" style="text-align: right; padding-right: 2mm; vertical-align: middle;">
            <span style="font-weight: bold;">For 2WTRADE LLP</span><br/>
            Authorized Signatory
        </td>
    </tr>
    <tr>
        <td style="border-right: 0.2mm solid #000000; height: 6mm; padding-left: 2mm;"></td>
    </tr>
    <tr>
        <td colspan="5" style="border-top: 0.2mm solid #000000; font-size: 6pt; height: 4mm; padding-left: 2mm;">
            <span style="text-decoration: underline;">Declaration</span>: We declare that this invoice shows the actual price of the goods described and that all particulars are true and correct.
        </td>
    </tr>
</table>
