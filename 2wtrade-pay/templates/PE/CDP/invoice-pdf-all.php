<?php
/** @var array $data */
?>

<div style="padding-top: 5mm;">
    <?= $this->render('_invoice-pdf-table', $data) ?>
</div>
