<?php
/**
 * @var array $rows
 * @var float $total_amount
 * @var string $invoiceName
 * @var string $invoiceDate
 * @var string $deliveryBankName
 * @var string $deliveryBankAccountNumber
 * @var string $deliveryAddress
 * @var string $deliveryName
 * @var string $logoBase64
 * @var string $period
 * @var string $paymentDate
 * @var string $signatureBase64
 * @var \app\modules\catalog\models\RequisiteDelivery $requisiteDelivery
 * @var string $invoiceCurrencyCharCode
 * @var string $currencyRateStr
 */

$counter = 1;
?>
<p><a href="#" class="invoice-header">INVOICE</a></p>
<p style="margin-top: 0; margin-bottom: 3px;" class="information">
    Number <?= $invoiceName ?></p>
<p style="margin-top: 0; margin-bottom: 3px;" class="information">
    Date: <?= $invoiceDate ?></p>
<p style="margin-top: 0; margin-bottom: 3px;" class="information">
    Period: <?= $period ?></p>
<?php if ($paymentDate): ?>
    <p style="margin-top: 0; margin-bottom: 3px;" class="information">Payment
        due date: <?= $paymentDate ?></p>
<?php endif; ?>

<?php if (!empty($currencyRateStr)): ?>
<p style="margin-top: 0;" class="information">Currency exchange rate: <?=$currencyRateStr?></p>
<?php endif;?>

<p style="margin-top: 0; margin-bottom: 5px;" class="address-header">SENT TO: <?= $deliveryName ?></p>
<p style="margin-top: 0; margin-bottom: 3px;"><span class="address">Bank: <?= $deliveryBankName ?></span></p>
<p style="margin-top: 0; margin-bottom: 3px;"><span
            class="address">Account number: <?= $deliveryBankAccountNumber ?></span>
</p>
<p style="margin-top: 0; margin-bottom: 0pt;"><span
            class="address">Address: <?= $deliveryAddress ?></span></p>
<br>

<h4>Dear partners,<br/>
    To transfer, please use following Bank details:
</h4>


<?php if (!$requisiteDelivery): ?>

    <p style="margin-bottom: 5px;" class="address-header">SENT FROM:</p>
    <p style="margin-top: 0; margin-bottom: 3px;"><span class="address">Company Name: 2WTRADE LLP</span>
    </p>
    <p style="margin-top: 0; margin-bottom: 3px;"><span
                class="address">Address: 71-75 SHELTON STREET, COVENT GARDEN</span>
    </p>
    <p style="margin-top: 0; margin-bottom: 3px;"><span class="address">City, country: LONDON, ENGLAND, WC2H 9JQ</span>
    </p>
    <p style="margin-top: 0; margin-bottom: 3px;"><span class="address">Beneficiary’s bank: BANK ALPINUM AG</span>
    </p>
    <p style="margin-top: 0; margin-bottom: 3px;"><span
                class="address">Bank Address: VADUZ 17 STAEDTLE 9490, Liechtenstein</span>
    </p>
    <p style="margin-top: 0;margin-bottom: 3px;"><span class="address">Bank account: LI39 08801201622101333</span>
    </p>
    <p style="margin-top: 0; margin-bottom: 3px;"><span class="address">SWIFT Code: BALPLI22</span></p>
    <p style="margin-top: 0; margin-bottom: 3px;"><span class="address">Corresponded bank: INCORE BANK Zurich</p>
    <br>

<?php else: ?>

    <p style="margin-top: 0; margin-bottom: 5px;" class="address-header">SENT FROM: <?= $requisiteDelivery->name ?></p>
    <p style="margin-top: 0; margin-bottom: 3px;"><span
                class="address">Beneficiary Address: <?= $requisiteDelivery->address ?></p>
    <p style="margin-top: 0; margin-bottom: 3px;"><span class="address">City: <?= $requisiteDelivery->city ?></p>
    <p style="margin-top: 0; margin-bottom: 3px;"><span
                class="address">Beneficiary IBAN: <?= $requisiteDelivery->bank_account ?></p>
    <p style="margin-top: 0; margin-bottom: 3px;"><span
                class="address">Beneficiary's bank: <?= $requisiteDelivery->beneficiary_bank ?></p>
    <p style="margin-top: 0; margin-bottom: 3px;"><span
                class="address">Bank Address: <?= $requisiteDelivery->bank_address ?></p>
    <p style="margin-top: 0; margin-bottom: 3px;"><span class="address">SWIFT Code: <?= $requisiteDelivery->name ?></p>

<?php endif; ?>


<br/>


<table class="charges">
    <tr style="font-weight: bold;">
        <th class="charges" style="width: 5%;">No</th>
        <th class="charges" style="width: 25%;">Description</th>
        <th class="charges">Quantity</th>
        <th class="charges" style="width: 20%;">Unit price, <?=$invoiceCurrencyCharCode?></th>
        <th class="charges"><?=$invoiceCurrencyCharCode?> amount</th>
    </tr>
    <?php foreach ($rows as $row): ?>
        <tr>
            <td class="charges"><?= $counter++ ?></td>
            <td class="charges"><?= $row['desc'] ?></td>
            <td class="charges"><?= $row['qty'] ?></td>
            <td class="charges"><?= $row['unit'] ?></td>
            <td class="charges"><?= $row['amount'] ?></td>
        </tr>
    <?php endforeach; ?>
    <tr>
        <td class="charges"><?= $counter ?></td>
        <td colspan="3" class="charges">TOTAL</td>
        <td class="charges"><?= $total_amount ?></td>
    </tr>
</table>
