<?php

use app\modules\catalog\models\RequisiteDelivery;

/**
 * @var string $signatureBase64
 * @var string $invoiceDate
 * @var  \app\modules\delivery\models\DeliveryRequest $requisiteDelivery
 */

/** @var RequisiteDelivery $requisiteDelivery */
$invoice_tpl = $requisiteDelivery->invoice_tpl;

$stamp = is_null($invoice_tpl)
    ? $signatureBase64
    : "data:image/png;base64," . base64_encode(file_get_contents($requisiteDelivery->getStampByTemplate($requisiteDelivery->invoice_tpl)))
?>

<table class="signature">
    <tr>
        <td style="width: 15%; text-align: center; vertical-align: bottom; padding-bottom: 20px;">Signature:</td>
        <td style="width: 30%; text-align: center;"><img src="<?= $stamp ?>" style="width: 5.08cm; height: 5.02cm;">
        </td>
        <td style="text-align: center; width: 30%; vertical-align: bottom; padding-bottom: 20px;">
            Name: <?= $requisiteDelivery->getSigner() ?>
        </td>
        <td style="text-align: right; width: 25%; vertical-align: bottom; padding-bottom: 20px;">
            Date: <?= $invoiceDate ?></td>
    </tr>
</table>