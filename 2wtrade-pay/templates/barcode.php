<?php

use app\modules\order\models\OrderLogisticListBarcode;

/**
 * @var \app\modules\order\models\OrderLogisticListBarcode[] $barcodes
 */
?>

<?php foreach ($barcodes as $barcode): ?>
    <p><?= $barcode->order_id ?></p>
    <img width="300" height="300" src="data:image/png;base64,<?= base64_encode(file_get_contents(OrderLogisticListBarcode::getPathDir() . $barcode->barcode_filename)) ?>">
    <p><?= $barcode->barcode ?></p>
<?php endforeach; ?>