<?php
/**
 * @var string $logoBase64
 */

use app\modules\catalog\models\RequisiteDelivery;

/** @var RequisiteDelivery $requisiteDelivery */
$invoice_tpl = $requisiteDelivery->invoice_tpl ?? null;
?>

<?php if(is_null($invoice_tpl) || $invoice_tpl != RequisiteDelivery::INVOICE_TPL_NUM_ASAP_UNION_LLP): ?>

<table style="width: 100%; font-family: 'AvantGarde'; font-size: 9pt;">
    <tr>
        <td style="width: 35%;">
            <img src="<?= $logoBase64 ?>" style="width: 6.85cm; height: 1.87cm;">
        </td>
        <td style="width: 30%; padding-left: 30px;">
            <br/>
            Head office<br/>
            www.2wtrade.com<br/>
            mail: info@2wtrade.com
        </td>
        <td style="width: 35%;"><b>2WTRADE LLP</b><br/>
            Address : 71-75 SHELTON STREET, COVENT GARDEN, LONDON, ENGLAND, WC2H 9JQ
        </td>
    </tr>
</table>

<?php else:?>

    <table style="width: 100%; font-family: 'AvantGarde'; font-size: 9pt;">
        <tr>
            <td style="width: 35%;">
                <img src="data:image/png;base64,<?= base64_encode(file_get_contents($requisiteDelivery->getAsapUnionLogo())); ?>" style="width: 6.85cm; height: 1.87cm;">
            </td>
            <td style="width: 30%; padding-left: 30px;">
                www.asapunion.com<br/>
                mail: info@asapunion.com
            </td>
            <td style="width: 35%;"><b>ASAP UNION LLP</b><br/>
                Address : 71-75 SHELTON STREET, COVENT GARDEN, LONDON, ENGLAND, WC2H 9JQ
            </td>
        </tr>
    </table>

<?php endif;?>
