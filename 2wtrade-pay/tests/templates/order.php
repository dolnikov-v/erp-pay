<?php
/**
 * @var $faker \Faker\Generator
 * @var $index integer
 */
return [
    'id' => $faker->randomNumber(6),
    'status_id' => $faker->randomNumber(),
    'country_id' => $faker->randomNumber(1),
    'customer_full_name' => $faker->firstName.' '.$faker->lastName,
    'customer_phone' => $faker->phoneNumber,
];