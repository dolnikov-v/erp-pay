<?php
use app\tests\helpers\api\AuthHelper;
use app\tests\helpers\api\LeadHelper;
use app\tests\helpers\api\CallCenterHelper;
use yii\helpers\ArrayHelper;

class CallCenterCest
{
    public function _before(ApiTester $I)
    {
        $I->haveHttpHeader('Authorization', AuthHelper::getToken(AuthHelper::TYPE_TOKEN_CALL_CENTER));
    }

    /**
     * CallCenter - Получение номеров заказов по внешним ID
     * @group apiCallCenter
     */
    public function testIds(ApiTester $I)
    {
        $I->wantTo('get numbers via API');

        // get real data
        $number = 3;
        $data = CallCenterHelper::getRandomForeignKeys($number);

        // do test
        $I->sendGET(AuthHelper::getUrl('call-center/ids'), $data);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'success']);
        $response = json_decode($I->grabResponse(), true);
        $I->assertEquals(sizeof($response['data']), $number);
    }

    /**
     * CallCenter - Получение статус
     * @group apiCallCenter
     */
    public function testStatuses(ApiTester $I)
    {
        $I->wantTo('get statuses via API');

        // get real data
        $number = 3;
        $data = CallCenterHelper::getRandomForeignKeys($number);

        // do test
        $I->sendGET(AuthHelper::getUrl('call-center/statuses'), $data);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'success']);
        $response = json_decode($I->grabResponse(), true);
        $I->assertEquals(sizeof($response['data']), $number);
    }

    /**
     * CallCenter - Возможность редактирования
     * @group apiCallCenter
     */
    public function testCanUpdate(ApiTester $I)
    {
        $I->wantTo('can update order via API');

        // get real data
        $number = 3;
        $data = CallCenterHelper::getRandomOrders($number);
        $ids = ArrayHelper::getColumn($data, 'id');

        // do test
        $I->sendGET(AuthHelper::getUrl('call-center/can-update'), ['id' => $ids]);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'success']);
        $response = json_decode($I->grabResponse(), true);
        $I->assertEquals(sizeof($response['data']), $number);
    }

    /**
     * CallCenter - Изменение заказа
     * @group apiCallCenter
     */
    public function testUpdateOrder(ApiTester $I)
    {
        $I->wantTo('update order via API');

        // get real data
        $data = CallCenterHelper::getRandomOrders(1);

        // do test
        $I->sendPOST(AuthHelper::getUrl('call-center/update-order/' . $data[0]->id));
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'success']);
        $response = json_decode($I->grabResponse(), true);
        $I->assertEquals($response['data']['order_id'], $data[0]->id);
    }

    /**
     * CallCenter - Получение списка колл-центров
     * @group apiCallCenter
     */
    public function testList(ApiTester $I)
    {
        $I->wantTo('get call-center list via API');

        // get real data
        $data = LeadHelper::getRandomCountry();

        // do test
        $I->sendGET(AuthHelper::getUrl('call-center/list'), ['country' => $data]);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'success']);
    }

    /**
     * CallCenter - Отправка информации о заказе после прозвона
     * @group apiCallCenter
     */
    public function testOrder(ApiTester $I)
    {
        $I->wantTo('send order info via API');

        // get real data
        $data = CallCenterHelper::getRandomForeignOrders(1);

        // do test
        $I->sendPOST(AuthHelper::getUrl('call-center/order'), json_encode($data));
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'success']);
        $response = json_decode($I->grabResponse(), true);
        $I->assertEquals(sizeof($response['data']), sizeof($data));
    }
}