<?php
use app\tests\helpers\api\AuthHelper;
use app\tests\helpers\api\LeadHelper;
use app\tests\helpers\api\DeliveryHelper;

class DeliveryCest
{
    public function _before(ApiTester $I)
    {
        $I->haveHttpHeader('Authorization', AuthHelper::getToken(AuthHelper::TYPE_TOKEN_CALL_CENTER));
    }

    /**
     * Delivery - Получение списка служб доставок
     * @group apiDelivery
     */
    public function testList(ApiTester $I)
    {
        $I->wantTo('get list of deliveries by country via API');

        // get data
        $country = LeadHelper::getRandomCountry();

        // do test
        $I->sendGET(AuthHelper::getUrl('delivery/list'), ['country' => $country]);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'success']);
    }

    /**
     * Delivery - Проверка доступности доставки по zip-коду
     * @group apiDelivery
     */
    public function testCheck(ApiTester $I)
    {
        $I->wantTo('get check delivery by postal code via API');

        // get data
        $data = DeliveryHelper::getRandomPostalCode();

        // do test
        $I->sendGET(AuthHelper::getUrl('delivery/check'), $data);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'success']);
    }
}