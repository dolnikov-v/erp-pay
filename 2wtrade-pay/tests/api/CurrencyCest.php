<?php
use app\tests\helpers\api\AuthHelper;
use app\tests\helpers\api\CurrencyHelper;

class CurrencyCest
{
    public function _before(ApiTester $I)
    {
        $I->haveHttpHeader('Authorization', AuthHelper::getToken(AuthHelper::TYPE_TOKEN_MERCURY));
    }

    /**
     * CallCenter - Получение номеров заказов по внешним ID
     * @group apiCallCenter
     */
    public function testGetRate(ApiTester $I)
    {
        // get real data
        $code = CurrencyHelper::getRandomCurrencyCode();
        $yesterday = date("Y-m-d", time()-(60*60*24));
        $today = date("Y-m-d");

        $I->wantTo('get rate via API yesterday');

        // do test за вчерашний день
        $I->sendGET(AuthHelper::getUrl('currency/get-rate') . '?code=' . $code . '&date=' . $yesterday);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'success']);
        $response = json_decode($I->grabResponse(), true);
        $I->assertEquals($response['data']['code'], $code);
        $I->assertEquals($response['data']['date'], $yesterday);

        $I->wantTo('get rate via API today');

        // do test за сегодня
        $I->sendGET(AuthHelper::getUrl('currency/get-rate') . '?code=' . $code . '&date=' . $today);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'success']);
        $response = json_decode($I->grabResponse(), true);
        $I->assertEquals($response['data']['code'], $code);
        $I->assertEquals($response['data']['date'], $today);
    }
}