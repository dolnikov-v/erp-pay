<?php
use app\tests\helpers\api\AuthHelper;
use app\tests\helpers\api\ProductHelper;
use app\tests\helpers\api\DeliveryHelper;
use app\tests\helpers\api\LeadHelper;

class ProductCest
{
    public function _before(ApiTester $I)
    {
        $I->haveHttpHeader('Authorization', AuthHelper::getToken(AuthHelper::TYPE_TOKEN_ADCOMBO));
    }

    /*public function _after(ApiTester $I)
    {
        ProductHelper::deleteDataTestCreate(json_decode($I->grabResponse()));
    }*/

    /**
     * Product - Получение списка товаров
     * @group apiProduct
     */
    public function testProductsList(ApiTester $I)
    {
        $I->wantTo('get products via API');

        // get data
        $product = ProductHelper::getRandomProduct();

        // do test
        $I->sendGET(AuthHelper::getUrl('product/products'));
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'success']);
        $I->seeResponseContainsJson(['name' => $product->name]);
    }

    /**
     * Product - Получение списка товаров на складах
     * @group apiProduct
     */
    public function testStorageProducts(ApiTester $I)
    {
        $I->wantTo('get storage products via API');

        // get data
        $country = LeadHelper::getRandomCountry();

        // do test
        $I->sendGET(AuthHelper::getUrl('product/storage-products'), [
            'country' => $country
        ]);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'success']);
        $response = json_decode($I->grabResponse(), true);
        $I->assertEquals(is_array($response['data']), true);
    }

    /**
     * Product - Получение списка товаров в заказах, принятых в работу курьерской службо
     * @group apiProduct
     */
    public function testProductsOnDelivery(ApiTester $I)
    {
        $I->wantTo('get products on delivery via API');

        // get data
        $country = LeadHelper::getRandomCountry();

        // do test
        $I->sendGET(AuthHelper::getUrl('product/products-on-delivery'), [
            'country' => $country
        ]);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'success']);
        $response = json_decode($I->grabResponse(), true);
        $I->assertEquals(is_array($response['data']), true);
    }

    /**
     * Product - Получение списка товаров с указанием количества и суммы по стране, курьерке, дате и типу (передано в курьерку листами/по АПИ или выкуп)
     * @group apiProduct
     */
    public function testProductsReport(ApiTester $I)
    {
        $I->wantTo('get products report via API');

        // get data
        $types = ['ship', 'sale'];
        $data = DeliveryHelper::getRandomDelivery();
        $data['type'] = $types[rand(0, 1)];
        $data['date_from'] = date('Y-m-d', strtotime('-7days'));

        // do test
        $I->sendGET(AuthHelper::getUrl('product/products-report'), $data);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'success']);
        $response = json_decode($I->grabResponse(), true);
        $I->assertEquals(is_array($response['data']), true);
    }

    /**
     * Create - Создание товаров по имени
     * @group apiProduct
     * @param ApiTester $I
     */
    public function testCreate(ApiTester $I)
    {
        $I->wantTo('Create products via API');

        // get data

        $data = [];
        $data[] = [
            'name' =>'super92',
            'sku' => 123
        ];
        $data[] = [
            'name' =>'super93',
            'sku' => 1234
        ];
        // do test
        $I->sendPOST(AuthHelper::getUrl('product/create'), json_encode($data));
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'success']);
        ProductHelper::deleteDataTestCreate(json_decode($I->grabResponse()));
    }
}