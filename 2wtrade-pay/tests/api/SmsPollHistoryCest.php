<?php
use app\tests\helpers\api\AuthHelper;

class SmsPollHistoryCest
{
    public function _before(ApiTester $I)
    {
        $I->haveHttpHeader('Authorization', AuthHelper::getToken(AuthHelper::TYPE_TOKEN_2WSTORE));
    }

    /**
     * SmsPollHistory - Получение списка вопросов
     */
    public function testGetHistory(ApiTester $I)
    {
        $I->wantTo('get sms poll history via API');

        // do test
        $I->sendPOST(AuthHelper::getUrl('sms-poll-history/get-history'));
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'success']);
        $response = json_decode($I->grabResponse(), true);
        $I->assertEquals(is_array($response['questions']), true);
    }
}
