<?php
use app\tests\helpers\api\AuthHelper;
use app\tests\helpers\api\LeadHelper;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;

class LeadCest
{
    public function _before(ApiTester $I)
    {
        $I->haveHttpHeader('Authorization', AuthHelper::getToken());
    }

    // tests

    /**
     * Lead - Создание лида
     * @group apiLead
     */
    public function testCreate(ApiTester $I)
    {
        $I->wantTo('create a Lead via API');

        // generate fake data
        $leadData = LeadHelper::getTestLeads();

        // do test
        $I->sendPOST(AuthHelper::getUrl('lead/create'), $leadData[0]);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'success']);

        // delete test data
        // LeadHelper::deleteDataTests(json_decode($I->grabResponse(), true));
        print_r($I->grabResponse());
    }


    /**
     * Lead - Создание предоплаченного лида
     * @group apiLead
     */
    public function testPrepaidCreate(ApiTester $I)
    {
        $I->wantTo('create a Prepaid Lead via API');

        // generate fake data
        $leadData = LeadHelper::getTestLeads();
        $leadData[0]['country'] = 'MY';
        $leadData[0]['payment_type'] = 'Prepaid';

        // do test
        $I->sendPOST(AuthHelper::getUrl('lead/create'), $leadData[0]);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        //$I->seeResponseIsJson();
        //$I->seeResponseContainsJson(['status' => 'success']);

        print_r($I->grabResponse());
    }

    /**
     * Lead - Создание предоплаченного лида
     * @group apiLead
     */
    public function testPrepaidEdit(ApiTester $I)
    {
        $I->wantTo('edit a Prepaid Lead via API');

        // load lead data
        $leadData = LeadHelper::getTestLeads();
        $leadData[0]['country'] = 'MY';
        $leadData[0]['payment_type'] = 'Prepaid';

        $order = Order::find()
            ->where([
                'payment_type' => Order::ORDER_PAYMENT_TYPE_PREPAID,
                'status_id' => OrderStatus::getEditablePrepaidOrderList()
            ])
            ->one();

        $leadData[0]['order_id'] = $order->foreign_id;

        // do test
        $I->sendPOST(AuthHelper::getUrl('lead/edit'), $leadData[0]);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'success']);

        print_r($I->grabResponse());
    }

    /**
     * Lead - Получение статусов
     * @group apiLead
     */
    public function testStatuses(ApiTester $I)
    {
        $I->wantTo('get order statuses via API');

        // get real data
        $number = 3;
        $ids = LeadHelper::getRandomOrders($number);

        // do test
        $I->sendGET(AuthHelper::getUrl('lead/statuses'), $ids);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'success']);
        $response = json_decode($I->grabResponse(), true);
        $I->assertEquals(sizeof($response['data']), $number);
    }

    /**
     * Lead - Определить выкуплен лид или нет
     * @group apiLead
     */
    public function testBuyouts(ApiTester $I, $scenario)
    {
        $I->wantTo('get order buyouts via API');

        // get real data
        $number = 3;
        $ids = LeadHelper::getRandomOrders($number);

        // do test
        $I->sendGET(AuthHelper::getUrl('lead/buyouts'), $ids);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'success']);
        $I->seeResponseContains('buyout');
        $response = json_decode($I->grabResponse(), true);
        $I->assertEquals(sizeof($response['data']), $number);
    }


    /**
     * Lead - отмена предоплаченного лида
     * @group apiLead
     */
    public function testPrepaidCancel(ApiTester $I)
    {
        $I->wantTo('cancel a Prepaid Lead via API');

        // get real data
        $number = 3;
        $ids = LeadHelper::getRandomPrepaidOrders($number);

        // do test
        $I->sendGET(AuthHelper::getUrl('lead/cancel-orders'), $ids);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'success']);

        print_r($I->grabResponse());
    }
}
