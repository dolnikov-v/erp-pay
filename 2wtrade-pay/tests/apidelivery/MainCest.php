<?php
use app\tests\helpers\apidelivery\AuthHelper;
use app\tests\helpers\apidelivery\UserHelper;

class MainCest
{
    public $token;

    public function _before(ApideliveryTester $I)
    {
        $this->token = AuthHelper::makeToken();
        $I->haveHttpHeader('Authorization', 'Basic ' . base64_encode($this->token));
    }

    public function _after(ApideliveryTester $I)
    {
        AuthHelper::deleteToken($this->token);
    }

    // tests
    
    /**
     * /apidelivery/v1/main/products
     */
    public function testProducts(ApideliveryTester $I)
    {
        $I->wantTo('Get list of products via API');

        // do test
        $I->sendGET(AuthHelper::getUrl('main/products'));
        $I->seeResponseIsJson();
        $response = json_decode($I->grabResponse(), true);
        $I->assertArrayNotHasKey('messages', $response['data']);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseContainsJson(['status' => 'success']);
        $I->assertArrayHasKey('data', $response);
        $I->assertTrue(is_array($response['data']));
    }
}
