<?php
use app\tests\helpers\apidelivery\AuthHelper;
use app\models\api\ApiUser;
use app\models\api\ApiUserToken;


class AuthCest
{
    public $token;

    public function _before(ApideliveryTester $I)
    {
    }

    public function _after(ApideliveryTester $I)
    {
        AuthHelper::deleteToken($this->token);
    }

    // tests
    /**
     * Authorization
     * /apidelivery/v1/auth/login
     */
    public function testAuth(ApideliveryTester $I)
    {
        $I->wantTo('Auth as delivery service via API');

        $I->amHttpAuthenticated('test', 'test');

        // do test
        $I->sendPOST(AuthHelper::getUrl('auth/login'), [
            'username' => Yii::$app->params['2wtradeApiUsername'],
            'password' => Yii::$app->params['2wtradeApiPassword']
        ]);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'success']);
        $response = json_decode($I->grabResponse(), true);
        $I->assertArrayHasKey('token', $response['data']);
        $this->token = $response['data']['token'][0];
        $I->assertEquals(strlen(sha1('somestr')), strlen($this->token));
    }
}
