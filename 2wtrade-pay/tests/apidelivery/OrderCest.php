<?php
use app\tests\helpers\apidelivery\AuthHelper;
use app\tests\helpers\apidelivery\UserHelper;

class OrderCest
{
    public $token;

    public function _before(ApideliveryTester $I)
    {
        $this->token = AuthHelper::makeToken();
        $I->haveHttpHeader('Authorization', 'Basic ' . base64_encode($this->token));
    }

    public function _after(ApideliveryTester $I)
    {
        AuthHelper::deleteToken($this->token);
    }

    // tests

    /**
     * /apidelivery/v1/order/orders
     */
    public function testOrders(ApideliveryTester $I)
    {
        $I->wantTo('Get list of orders via API');

        // get random data
        $data = UserHelper::getRandomDeliveryCountry();

        $I->assertArrayHasKey('country', $data, 'no country+delivery found for ApiUser');

        // do test
        $I->sendGET(AuthHelper::getUrl('order/orders'), [
            'country' => $data['country'],
            'page' => 1,
            'perPage' => 1,
        ]);
        $I->seeResponseIsJson();
        $response = json_decode($I->grabResponse(), true);
        $I->assertArrayNotHasKey('messages', $response['data']);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseContainsJson(['status' => 'success']);
        $I->assertArrayHasKey('data', $response);
        $I->assertArrayHasKey('ordersCount', $response['data'][0]);
    }

    /**
     * /apidelivery/v1/order/update-status
     */
    public function testUpdateStatus(ApideliveryTester $I)
    {
        $I->wantTo('Change order`s status via API');

        // get random data
        $data = UserHelper::getRandomDeliveryCountry();
        $I->assertArrayHasKey('delivery', $data, 'no country+delivery found for ApiUser');

        $order = UserHelper::getRandomDeliveryOrder($data['delivery']);
        $I->assertInstanceOf(app\modules\order\models\Order::class, $order, 'no order for test');

        $request = [
            'orderId' => $order->id,
            'status' => $order->status_id
        ];
        $I->sendGET(AuthHelper::getUrl('order/update-status'), $request);
        $I->seeResponseIsJson();
        $response = json_decode($I->grabResponse(), true);

        // ничего не хотели менять получили ошибку, т.к. сейчас наше API не принимает повторно тот же статус
        $I->assertArrayHasKey('messages', $response['data']);
        $I->assertContains('Status of order cannot be changed to '.$order->status_id, $response['data']['messages'][0]);
    }
}
