<?php
namespace rating;
use app\modules\rating\models\query\CountryCurators;
use app\modules\rating\controllers\CountryCuratorsController;
use app\tests\helpers\rating\CountryCuratorsHelper;
use \NoGuy;
use yii\helpers\ArrayHelper;
use yii;

class CountryCuratorsCest
{
    /**
     * @var CountryCurators
     */
    public $modelCountryCurators;

    public function _before(NoGuy $I)
    {
        $this->modelCountryCurators = new CountryCurators();
    }

    public function _after(NoGuy $I)
    {
    }

    // tests
    public function getMapStatusesTest(NoGuy $I)
    {
        $I->wantTo('Test method CountryCurators->getMapStatuses()');
        $I->assertTrue(is_array($this->modelCountryCurators->getMapStatuses()), 'method getMapStatuses() return is array ?');
    }

    public function getUsernamesTest(NoGuy $I)
    {
        $I->wantTo('Test method CountryCurators->getUsernames()');

        $user_ids = ArrayHelper::getColumn(CountryCuratorsHelper::getUsers(), 'id');

        $I->assertTrue(is_array($this->modelCountryCurators->getUsernames($user_ids)), 'method getUsernames() return is array ?');
    }

    public function getDataByCountriesTest(NoGuy $I)
    {
        $I->wantTo('Test method CountryCurators->getDataByCountries()');

        $I->assertTrue(is_array($this->modelCountryCurators->getDataByCountries()), 'method getDataByCountries() return is array ?');
    }

    public function generateDataTest(NoGuy $I)
    {
        $I->wantTo('Test generateData() in CountryCuratorsController');

        $controller = new CountryCuratorsController('country-curators', yii::$app->getModule('rating'));

        $I->assertTrue(is_array($controller->generateData()), 'method generateData() return is array ?');
    }
}
