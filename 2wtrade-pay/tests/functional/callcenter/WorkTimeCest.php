<?php
namespace rating;
use app\modules\callcenter\models\CallCenter;
use app\modules\callcenter\models\CallCenterWorkTime;
use NoGuy;

class WorkTimeCest
{

    public function getNumberOfCallsLastDateTest(NoGuy $I)
    {
        $I->wantTo('Test method CallCenterWorkTime::getNumberOfCallsLastDate()');

        $callCenters = CallCenter::find()
            ->canGetOperatorWorkTimeByApi()
            ->active()
            ->all();

        foreach ($callCenters as $callCenter) {
            $date = CallCenterWorkTime::getNumberOfCallsLastDate($callCenter->id);
            $result = is_bool($date) || is_string($date);

            $I->assertTrue($result, "method getNumberOfCallsLastDate(CallCenterId = {$callCenter->id}) return is bool or is string ?");
        }
    }

    public function getTimeLastDateTest(NoGuy $I)
    {
        $I->wantTo('Test method CallCenterWorkTime::getTimeLastDate()');

        $callCenters = CallCenter::find()
            ->canGetOperatorWorkTimeByApi()
            ->active()
            ->all();

        foreach ($callCenters as $callCenter) {
            $date = CallCenterWorkTime::getTimeLastDate($callCenter->id);
            $result = is_bool($date) || is_string($date);

            $I->assertTrue($result, "method getTimeLastDate(CallCenterId = {$callCenter->id}) return is bool or is string ?");
        }
    }

}
