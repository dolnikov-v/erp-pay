<?php
namespace callcenter;

use app\modules\callcenter\models\CallCenterRequest;
use app\modules\callcenter\models\CallCenterRequestExtra;
use \NoGuy;

class ApiLeadCest
{

    public function testCallCenterRequestExtra(NoGuy $I)
    {
        $I->wantTo('Test CallCenterRequestExtra Save');

        $id = 3450972;

        $json = '{"success":true,"response":{"'.$id.'":{"id":"203289","name":"SoCheata (7788)","phone":"85516762136","mobile":"855167","address":"12157, Sangkat Orussei 4, N\/A, N\/A, N\/A, Khan 7 Makara , Phnom Penh, Phnom Penh","address_components":{"customer_address":"Sangkat Orussei 4","customer_street":"N\/A","customer_house_number":"N\/A","customer_address_add":"N\/A","customer_district":"Khan 7 Makara ","customer_city":"Phnom Penh","customer_province":"Phnom Penh","google_lat":"51.207812","google_lon":"4.396213500000001","zip":"12157"},"status":"4","comment":"Call Before Delivery \/","delivery":"0.00","history":[{"status_id":"4","sub_status_id":"401","oper":"106","oe_changed":"2017-10-11 12:08:26","oe_comment":"New=>approved.\r\nCall Before Delivery \/"}],"oper":"106","last_update":"2017-10-11 12:08:26","products":[{"product_id":"29","quantity":"1","price":"30"},{"product_id":"29","quantity":"1","price":"30"},{"product_id":"29","quantity":"1","price":"0"}],"shipping":{"type":"ABLworldwide","id":105,"time_from":"2017-10-19 08:00:00","time_to":"2017-10-19 10:00:00"},"express_delivery":true,"call_count":"3"}}}';
        $dataJson = json_decode($json, true);

        $data['call_count'] = $dataJson['response'][$id]['call_count'];

        $data['call_count'] = rand(0, 9);

        $request = CallCenterRequest::findOne($id);

        $compareError = '';
        if (isset($data['call_count'])) {
            $callsCount = (int)$data['call_count'];
            if ($request->extra instanceof CallCenterRequestExtra) {
                $request->extra->calls_count = $callsCount;
                if (!$request->extra->save()) {
                    $compareError = $request->extra->getFirstErrorAsString();
                }
            }
            else {
                $callCenterRequestExtra = new CallCenterRequestExtra();
                $callCenterRequestExtra->call_center_request_id = $request->id;
                $callCenterRequestExtra->calls_count = $callsCount;
                if (!$callCenterRequestExtra->save()) {
                    $compareError = $callCenterRequestExtra->getFirstErrorAsString();
                }
            }
        }

        var_dump($compareError);
        die();
    }
}
