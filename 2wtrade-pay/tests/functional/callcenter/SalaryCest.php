<?php
namespace rating;

use app\models\Country;
use app\modules\callcenter\models\CallCenter;
use app\modules\salary\models\Designation;
use app\modules\salary\models\Holiday;
use app\modules\salary\models\Person;
use app\modules\callcenter\models\CallCenterUser;
use app\modules\callcenter\models\CallCenterWorkTime;
use \NoGuy;

class SalaryCest
{

    const INDIA = 1;

    public function generateHoliday(NoGuy $I)
    {
        $I->wantTo('Fill table call_center_holiday');

        $dates = [
            '2017-01-01',
            '2017-01-08',
            '2017-01-14',
            '2017-04-09',
            '2017-04-16',
            '2017-04-22',
            '2017-05-06',
            '2017-06-22',
            '2017-09-30',
            '2017-11-04',
        ];

        foreach ($dates as $date) {

            $c = Holiday::findOne([
                'country_id' => self::INDIA,
                'date' => $date
            ]);

            if (!$c) {
                $c = new Holiday();
                $c->country_id = self::INDIA; // Индия
                $c->date = $date;
                $c->save();
            }
        }
    }


    public function generatePerson(NoGuy $I)
    {

        $country = Country::findOne(self::INDIA);

        $callCenter = CallCenter::find()->byCountryId(self::INDIA)->one();

        $usersQ = CallCenterUser::find()
            ->byCallCenterId($callCenter->id)
            ->andWhere(['is', 'person_id', null]);

        if (property_exists(CallCenterUser::class, 'user_role')) {
            $usersQ->andWhere(['user_role', CallCenterUser::USER_ROLE_OPER]);
        }

        $users = $usersQ->all();

        foreach ($users as $user) {
            $person = new Person();

            $person->name = 'Name Oper ' . $user->user_login;
            $person->active = 1;
            $person->birth_date = '1990-01-01';
            $person->start_date = '2017-01-01';
            $person->salary_scheme = Person::SALARY_SCHEME_PER_MONTH;
            $person->salary_local = 500;

            if ($person->save()) {
                $user->person_id = $person->id;
                if (!$user->save()) {
                    print_r($user->errors);
                }
            } else {
                print_r($person->errors);
            }

        }
    }


    public function generateWork(NoGuy $I)
    {
        $callCenter = CallCenter::find()->byCountryId(self::INDIA)->one();

        $users = CallCenterUser::find()
            ->byCallCenterId($callCenter->id)
            ->active()
            ->andWhere(['is not', 'person_id', null])
            ->all();
        // Start date
        $date = '2017-01-01';
        // End date
        $end_date = '2017-02-01';

        while (strtotime($date) <= strtotime($end_date)) {

            foreach ($users as $user) {


                if (!rand(0, 5)) continue;

                $work = CallCenterWorkTime::findOne([
                    'call_center_user_id' => $user->id,
                    'date' => $date
                ]);
                if (!$work) {
                    $work = new CallCenterWorkTime();
                }
                $work->call_center_user_id = $user->id;
                $work->date = $date;
                $work->time = rand(27800, 29800);

                if (!$work->save()) {
                    print_r($work->errors);
                }
            }

            $date = date("Y-m-d", strtotime("+1 day", strtotime($date)));
        }
    }
}
