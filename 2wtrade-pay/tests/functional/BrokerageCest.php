<?php
use app\modules\delivery\models\Delivery;
use yii\db\Expression;
use yii\test\FixtureTrait;
use \Codeception\Util\Stub;


use app\commands\crontab\CallCenterController;

class BrokerageCest
{

    public function _before(NoGuy $I)
    {

    }

    public function _after(NoGuy $I)
    {
    }

    // tests
    public function testFix(NoGuy $I)
    {
        /*
        $I->haveFixtures(['orders' => \tests\fixtures\OrderFixture::className()]);
        $orders = \app\modules\order\models\Order::find()->all();
        echo sizeof($orders);
        die();
        */
    }

    /**
     * test Delivery::brokerageScore()
     */
    public function testScore(NoGuy $I)
    {

        $I->amGoingTo('test brokerageScore() function');

        $deliveries = Delivery::find()
            ->active()
            ->brokerageActive()
            ->where([
                'country_id' => 1
            ])
            ->orderBy(new Expression('RAND()'))
            ->all();

        foreach ($deliveries as $delivery) {
            $score = $delivery->brokerageScore(null, true);
            echo $delivery->name . ' ' . $score . PHP_EOL;
            $I->assertTrue(is_numeric($score));
        }
    }
}
