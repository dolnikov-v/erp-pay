<?php
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\callcenter\models\CallCenterRequestExtra;
use app\modules\delivery\components\DeliveryHandler;
use app\modules\delivery\models\DeliveryRequest;
use \NoGuy;

class DeliveryCest
{

    public function testSaveUnBuyoutReason(NoGuy $I)
    {
        $I->wantTo('Test saveUnBuyoutReason');

        $randomDeliveryRequest = DeliveryRequest::find()->orderBy(['id' => SORT_DESC])->offset(rand(1, 10))->one();

        $response['unshipping_reason'] = 'some new reason param pam';

        try {
            $result = $randomDeliveryRequest->saveUnBuyoutReason($response['unshipping_reason'] ?? '');

        } catch (\Throwable $e) {
            print_r($e->getMessage());
        }

        echo $randomDeliveryRequest->id;
        $I->assertTrue($result, 'true result Request ID = ' . $randomDeliveryRequest->id);

    }
}
