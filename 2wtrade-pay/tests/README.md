# 2WTrade - Тестирование

КОНФИГУРАЦИЯ
-------------

* Разворачиваем Codeception
```
composer install
```
* Можно поставить codeception глобально
```
composer global require codeception/codeception
```

* Создаём /tests/config/console.php (для примера посмотрите /tests/config/console.php.example)

ДОКУМЕНТАЦИЯ
-------------

http://www.yiiframework.com/doc-2.0/guide-test-overview.html - Testing YII2

http://codeception.com/docs/modules/Yii2 - Фреймворк Codeception в Yii2 

https://www.youtube.com/watch?v=gRmEpUYaS20 - видео туториал Тестирование с PHPUnit и Codeception вообще и в Yii2                                                              

http://krivochenko.ru/blog/post/introduction-to-testing-yii2 - cтатья введение в тестирование в Yii2

ИСПОЛЬЗОВАНИЕ
-------------

* Запускать тесты 
```
php vendor/codeception/codeception/codecept run 
```
* При глобальной установке

```
codecept run
 ```

* Запустить API тесты 
```
codecept run api 
```

* Для запуска конкретного теста
```
codecept run tests/api/ProductCest.php:testStorageProducts
```

* Для запуска группы тестов (для этого тестам задали аннотацию @group page)
```
codecept run functional --group page
```

* Установка Selenium (для запуска реального браузера и визуальной работы теста в нем)
```
npm install selenium-standalone@latest -g
selenium-standalone install
selenium-standalone start
```

* Генератор классов фикстур
 ```
http://*my_localhost*/mx/gii/fixture
```


* Генерация данных Фикстур 
```
yii fixture/generate-all --count=5
```


* Генерация теста 
```
codecept generate:cest functional NameOfModel
```