<?php
namespace rating;
use \AcceptanceTester;
use Yii;

class CountryCuratorsCest
{
    public function _before(AcceptanceTester $I)
    {
        $I->amOnUrl(Yii::$app->params['url']);
    }

    public function _after(AcceptanceTester $I)
    {
    }

    protected function login(AcceptanceTester $I) {
        $I->amOnPage('/auth/login');
        $I->see('2Wtrade');
        $I->fillField('input[name="LoginForm[username]"]', Yii::$app->params['2wtradeLogin']);
        $I->fillField('input[name="LoginForm[password]"]', Yii::$app->params['2wtradePassword']);
        $I->click('button[type=submit]');
    }

    /**
     * @before login
     */
    public function viewIndexPageTest(AcceptanceTester $I)
    {
        $I->wantTo('I wan to see page /rating/country-curators/index');
        $I->amOnPage('/rating/country-curators/index');
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->see('Рейтинг 2WTRADE' || 'Rating 2WTRADE');
    }
}
