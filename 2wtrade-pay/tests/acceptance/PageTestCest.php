<?php


class PageTestCest
{
    public function _before(AcceptanceTester $I)
    {
        $I->amOnUrl(Yii::$app->params['url']);
    }

    public function _after(AcceptanceTester $I)
    {
    }

    protected function login(AcceptanceTester $I) {
        $I->amOnPage('/auth/login');
        $I->see('2Wtrade');
        $I->fillField('input[name="LoginForm[username]"]', Yii::$app->params['2wtradeLogin']);
        $I->fillField('input[name="LoginForm[password]"]', Yii::$app->params['2wtradePassword']);
        $I->click('button[type=submit]');
    }

    /**
     * @before login
     */
    public function tryToTest(AcceptanceTester $I, $scenario)
    {
        $I->wantTo('Open Orders list page');

        $I->amOnPage('/order/index/index');

        $pageTitle = $I->grabTextFrom('.page-title');
        if ($pageTitle == 'Orders list' || $pageTitle == 'Список заказов') {
            $I->see($pageTitle);
        }
        else {
            // не находимся на странице заказов выпадет ошибка
            $I->see('Orders list');
        }

        // before close
        if (method_exists($I, 'wait')) {
            $I->wait(3);
        }
    }
}
