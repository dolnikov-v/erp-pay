<?php

namespace tests\fixtures;

use yii\test\ActiveFixture;

class CountryFixture extends ActiveFixture
{
    public $modelClass = 'app\models\Country';
    public $dataFile = '@tests/fixtures/data/country.php';
}