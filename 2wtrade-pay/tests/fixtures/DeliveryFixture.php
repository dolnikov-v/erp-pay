<?php
namespace tests\fixtures;

use yii\test\ActiveFixture;

/**
 * Delivery fixture
 */
class DeliveryFixture extends ActiveFixture
{
    public $modelClass = 'app\modules\delivery\models\Delivery';
    public $dataFile = '@tests/fixtures/data/delivery.php';
}