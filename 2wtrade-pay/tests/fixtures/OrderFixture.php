<?php

namespace tests\fixtures;

use yii\test\ActiveFixture;

class OrderFixture extends ActiveFixture
{
    public $modelClass = 'app\modules\order\models\Order';
    public $dataFile = '@tests/fixtures/data/order.php';
}