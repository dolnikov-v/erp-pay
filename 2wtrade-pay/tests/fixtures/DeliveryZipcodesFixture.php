<?php

namespace tests\fixtures;

use yii\test\ActiveFixture;

class DeliveryZipcodesFixture extends ActiveFixture
{
    public $modelClass = 'app\modules\delivery\models\DeliveryZipcodes';
    public $dataFile = '@tests/fixtures/data/delivery_zipcodes.php';
}