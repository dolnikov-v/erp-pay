<?php

namespace tests\fixtures;

use yii\test\ActiveFixture;

class DeliveryRequestFixture extends ActiveFixture
{
    public $modelClass = 'app\modules\delivery\models\DeliveryRequest';
    public $dataFile = '@tests/fixtures/data/delivery_request.php';
}