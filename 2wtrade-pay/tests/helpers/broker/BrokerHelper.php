<?php
namespace app\tests\helpers\broker;

use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryNotProducts;
use app\modules\delivery\models\DeliveryPackage;
use app\modules\delivery\models\DeliveryZipcodes;
use app\models\Country;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\db\Query;

class BrokerHelper
{
    /**
     * @param int $count
     * @return [] Delivery
     */
    public static function getRandomActiveDelivery($count = 1)
    {
        $delivery = Delivery::find()
            ->where(['=', 'active', 1])
            ->orderBy(new Expression('rand()'))
            ->limit($count)
            ->all();

        return $delivery;
    }

    /**
     * @param int $count
     * @return [] Delivery
     */
    public static function getRandomBrokerDelivery($count = 1)
    {
        $delivery = Delivery::find()
            ->where(['=', 'active', 1])
            ->andWhere(['=', 'brokerage_active', 1])
            ->orderBy(new Expression('rand()'))
            ->limit($count)
            ->all();

        return $delivery;
    }

    /**
     * @param int $count
     * @return [] DeliveryZipcodes
     */
    public static function getRandomDeliveryZipcodes($count = 1)
    {
        $deliveryZipcodes = DeliveryZipcodes::find()
            ->leftJoin(Delivery::tableName(), [Delivery::tableName() . '.id=' . DeliveryZipcodes::tableName() . '.delivery_id'])
            ->orderBy(new Expression('rand()'))
            ->limit($count)
            ->all();

        return $deliveryZipcodes;
    }

    /**
     * @param int $count
     * @param bool $nonempty
     * @return [] DeliveryZipcodes
     */
    public static function getRandomBrokerDeliveryZipcodes($count = 1, $nonempty = true)
    {
        $cond = ($nonempty === true) ? '>' : '=';
        $deliveryZipcodes = DeliveryZipcodes::find()
            ->leftJoin(Delivery::tableName(), Delivery::tableName() . '.id=' . DeliveryZipcodes::tableName() . '.delivery_id')
            ->where(['=', Delivery::tableName() . '.active', 1])
            ->andWhere(['=', Delivery::tableName() . '.brokerage_active', 1])
            ->andWhere([$cond, DeliveryZipcodes::tableName() . '.zipcodes', ''])
            ->orderBy(new Expression('rand()'))
            ->limit($count)
            ->all();

        return $deliveryZipcodes;
    }

    /**
     * @return string
     */
    public static function getRandomPostalCode()
    {
        $zips = DeliveryZipcodes::find()
            ->leftJoin(Delivery::tableName(), DeliveryZipcodes::tableName() . '.delivery_id=' . Delivery::tableName() . '.id')
            ->where(
                [
                    'and',
                    ['is not', DeliveryZipcodes::tableName() . '.zipcodes', null],
                    ['!=', DeliveryZipcodes::tableName() . '.zipcodes', ''],
                    ['=', Delivery::tableName() . '.active', 1],
                    ['=', Delivery::tableName() . '.brokerage_active', 1]
                ]
            )
            ->orderBy(new Expression('rand()'))
            ->limit(1)
            ->one();

        $zipCodes = explode(',', $zips->zipcodes);
        $randKey = array_rand($zipCodes);

        return $zipCodes[$randKey];
    }

    /**
     * @return ActiveRecord
     */
    public static function getRandomDeliveryWithStop()
    {
        return DeliveryNotProducts::find()->with('delivery')->orderBy(new Expression('rand()'))->one();
    }

    /**
     * @return ActiveRecord
     */
    public static function getRandomDeliveryWithPackage()
    {
        return DeliveryPackage::find()->with('delivery')->orderBy(new Expression('rand()'))->one();
    }
}