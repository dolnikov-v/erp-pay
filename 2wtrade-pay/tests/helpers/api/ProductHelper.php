<?php
namespace app\tests\helpers\api;

use app\models\Product;
use yii\db\Expression;

class ProductHelper
{
    /**
     * @return Product
     */
    public static function getRandomProduct()
    {
        return Product::find()
            ->orderBy(new Expression('rand()'))
            ->limit(1)
            ->one();
    }

    /**
     * @param \stdClass $response
     */
    public static function deleteDataTestCreate($response)
    {
        $successProductIds = [];
        if (!isset($response->data->result)) {
            return;
        }
        foreach ($response->data->result as $product) {
            if ($product->status == "success") {
                $successProductIds[] = $product->id;
            }
        }
        if ($successProductIds) {
            Product::deleteAll(['IN', 'id', $successProductIds]);
        }
    }
}