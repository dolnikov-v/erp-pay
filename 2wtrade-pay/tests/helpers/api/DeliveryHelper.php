<?php
namespace app\tests\helpers\api;

use app\models\Country;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryZipcodes;
use app\modules\order\models\Order;
use yii\db\Expression;

class DeliveryHelper
{
    /**
     * @return array
     */
    public static function getRandomPostalCode()
    {
        $zips = DeliveryZipcodes::find()
            ->leftJoin(Delivery::tableName(), DeliveryZipcodes::tableName() . '.delivery_id=' . Delivery::tableName() . '.id')
            ->leftJoin(Country::tableName(), Delivery::tableName() . '.country_id=' . Country::tableName() . '.id')
            ->where(
                [
                    'and',
                    ['is not', DeliveryZipcodes::tableName() . '.zipcodes', null],
                    ['!=', DeliveryZipcodes::tableName() . '.zipcodes', ''],
                    ['=', Delivery::tableName() . '.active', 1],
                    ['=', Country::tableName() . '.active', 1],
                    ['=', Country::tableName() . '.brokerage_enabled', 1]
                ]
            )
            ->orderBy(new Expression('rand()'))
            ->limit(1)
            ->one();

        $zipCodes = explode(',', $zips->zipcodes);
        $randKey = array_rand($zipCodes);

        return [
            'country' => $zips->delivery->country->char_code,
            'postal_code' => $zipCodes[$randKey],
        ];
    }

    /**
     * @param int $count
     * @return array
     */
    public static function getRandomOrders($count = 1)
    {
        return Order::find()
            ->where(['<>', 'source_confirmed', 1])
            ->orderBy(
                [Order::tableName() . '.id' => SORT_DESC]
            )
            ->offset(rand(99, 999))
            ->limit($count)
            ->all();
    }

    /**
     * @param int $count
     * @return array
     */
    public static function getRandomForeignOrders($count = 1)
    {
        $orders = Order::find()
            ->where(['is not', 'foreign_id', null])
            ->orderBy(
                [Order::tableName() . '.id' => SORT_DESC]
            )
            ->offset(rand(99, 999))
            ->limit($count)
            ->all();

        $data = [];
        foreach ($orders as $order) {
            $data[$order->id] = [
                'id' => $order->foreign_id
            ];
        }
        return $data;
    }


    /**
     * @return array
     */
    public static function getRandomDelivery()
    {
        $delivery = Delivery::find()
            ->leftJoin(Country::tableName(), Delivery::tableName() . '.country_id=' . Country::tableName() . '.id')
            ->where(
                [
                    'and',
                    ['=', Delivery::tableName() . '.active', 1],
                    ['=', Country::tableName() . '.active', 1],
                    ['=', Country::tableName() . '.brokerage_enabled', 1]
                ]
            )
            ->orderBy(new Expression('rand()'))
            ->limit(1)
            ->one();

        return [
            'country' => $delivery->country->char_code,
            'delivery' => $delivery->id,
        ];
    }

}