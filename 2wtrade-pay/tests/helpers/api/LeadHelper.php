<?php
namespace app\tests\helpers\api;

use app\models\Country;
use app\models\Product;
use app\modules\callcenter\models\CallCenter;
use app\modules\order\models\Lead;
use app\modules\order\models\OrderProduct;
use app\modules\order\models\LeadProduct;
use app\modules\order\models\OrderStatus;
use Faker\Factory;
use Yii;
use yii\base\InvalidParamException;
use yii\db\Expression;
use app\modules\order\models\Order;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * Class LeadHelper
 * @package app\tests\helpers
 */
class LeadHelper
{
    /**
     * @param int $count
     * @return array
     */
    public static function getTestLeads($count = 1)
    {
        $faker = Factory::create();
        $arrayLeads = [];
        $packages = [null, 1, 3, 5];

        for ($i = 1; $i <= $count; $i++) {
            $arrayLeads[] = [
                'country' => self::getRandomCountry(),
                'order_id' => rand(888888888, 999999999),
                'revenue' => $faker->numberBetween(3, 5),
                'price' => $faker->numberBetween(10, 50),
                'total_price' => $faker->numberBetween(500, 7000),
                'site' => $faker->domainName,
                'goods_id' => self::getRandomGoods(),
                'num' => $faker->numberBetween(1, 5),
                'phone' => $faker->phoneNumber,
                'name' => $faker->name,
                'comment' => $faker->sentence(),
                'ip' => $faker->ipv4,
                'street' => $faker->streetAddress,
                'postal_code' => $faker->postcode,
                'address_detail' => $faker->address,
                'name_detail' => $faker->sentence(4),
                'city' => $faker->city,
                'district' => $faker->sentence(2),
                'delivery' => '2.0',
                'is_validated' => null,
                'address' => $faker->streetAddress,
                'prefecture' => $faker->word,
                'sub_district' => $faker->word,
                'with_address' => $faker->word,
                'package_id' => $packages[rand(0, 3)],
                'ts_spawn' => $faker->unixTime,
            ];
        }

        return $arrayLeads;
    }

    /**
     * @param int $count
     * @return array
     */
    public static function getRandomOrders($count = 1)
    {
        $orders = Order::find()
            ->select('foreign_id')
            ->where(['is not', 'foreign_id', null])
            ->orderBy(
                [Order::tableName() . '.id' => SORT_DESC]
            )
            ->offset(rand(99, 999))
            ->limit($count)
            ->all();

        if ($orders) {
            return ['ids' => implode(",", ArrayHelper::getColumn($orders, 'foreign_id'))];
        } else {
            throw new InvalidParamException(Yii::t('common', 'Не удалось найти лидов.'));
        }
    }

    /**
     * @param int $count
     * @return array
     */
    public static function getRandomPrepaidOrders($count = 1)
    {
        $orders = Order::find()
            ->select('foreign_id')
            ->where(['is not', 'foreign_id', null])
            ->andWhere([
                'payment_type' => Order::ORDER_PAYMENT_TYPE_PREPAID
            ])
            ->andWhere([
                'status_id' => OrderStatus::getEditablePrepaidOrderList()
            ])
            ->orderBy(
                [Order::tableName() . '.id' => SORT_DESC]
            )
            ->limit($count)
            ->all();

        if ($orders) {
            return ['ids' => implode(",", ArrayHelper::getColumn($orders, 'foreign_id'))];
        } else {
            throw new InvalidParamException(Yii::t('common', 'Не удалось найти лидов.'));
        }
    }

    /**
     * @param int $count
     * @return array
     */
    public static function getRandomDataOrders($count = 1)
    {
        $orders = Order::find()
            ->where(['is not', 'foreign_id', null])
            ->orderBy(
                [Order::tableName() . '.id' => SORT_DESC]
            )
            ->offset(rand(99, 999))
            ->limit($count)
            ->all();

        if ($orders) {
            return $orders;
        } else {
            throw new InvalidParamException(Yii::t('common', 'Не удалось найти лидов.'));
        }
    }

    /**
     * @param $data
     * @return array
     */
    public static function getTestLeadForCallCenter($data)
    {
        $faker = Factory::create('ru_RU');

        $callCenter = new CallCenter();
        $callCenter->id = self::getRandomCallCenter();
        $callCenter->url = Yii::$app->params['urlCallCenter'];
        $callCenter->api_pid = Yii::$app->params['apiPid'];
        $callCenter->api_key = Yii::$app->params['apiKey'];

        $lead = LeadHelper::getTestLeads();
        $lead[0]['order_id'] = $data->id;

        $newFields = [
            'order_date' => time(),
            'fio' => $data->customer_full_name,
            'country' => Yii::$app->params['countryCallCenter'],
            'currency' => Yii::$app->params['currencyCallCenter'],
            'products' => [
                [
                    'product_id' => self::getRandomGoods(),
                    'quantity' => $faker->randomNumber(),
                    'price' => $faker->randomNumber(),
                ],
                [
                    'product_id' => self::getRandomGoods(),
                    'quantity' => $faker->randomNumber(),
                    'price' => $faker->randomNumber(),
                ],
            ],
        ];
        $fields = array_merge($lead[0], $newFields);

        return $newArray[] = [
            'callCenter' => $callCenter,
            'fields' => $fields
        ];
    }

    /**
     * @return string
     * @throws InvalidParamException
     */
    public static function getRandomCountry()
    {
        $query = new Query();
        $country = $query
            ->select(Country::tableName() . '.char_code')
            ->from(Country::tableName())
            ->innerJoin(CallCenter::tableName(), Country::tableName() . '.id=' . CallCenter::tableName() . '.country_id')
            ->where([
                Country::tableName() . '.active' => 1,
                CallCenter::tableName() . '.active' => 1,
                Country::tableName() . '.brokerage_enabled' => 1
            ])
            ->orderBy(new Expression('rand()'))
            ->one();

        if ($country) {
            return $country['char_code'];
        } else {
            throw new InvalidParamException(Yii::t('common', 'Не удалось найти страну.'));
        }
    }

    /**
     * @return int
     * @throws InvalidParamException
     */
    protected static function getRandomCallCenter()
    {
        $query = CallCenter::find()
            ->select('id')
            ->orderBy(new Expression('rand()'))
            ->one();
        if ($query) {
            return $query->id;
        } else {
            throw new InvalidParamException(Yii::t('common', 'Не удалось найти КЦ.'));
        }
    }

    /**
     * @return int
     * @throws InvalidParamException
     */
    protected static function getRandomGoods()
    {
        $query = Product::find()
            ->select('id')
            ->orderBy(new Expression('rand()'))
            ->one();
        if ($query) {
            return $query->id;
        } else {
            throw new InvalidParamException(Yii::t('common', 'Не удалось найти продукт.'));
        }
    }

    /**
     * @param array $data
     */
    public static function deleteDataTests($data)
    {
        $listId = [];
        foreach ($data['data'] as $item) {
            $listId[] = $item;
        }
        if ($listId) {
            OrderProduct::deleteAll(['IN', 'order_id', $listId]);
            LeadProduct::deleteAll(['IN', 'order_id', $listId]);
            Lead::deleteAll(['IN', 'order_id', $listId]);
            Order::deleteAll(['IN', 'id', $listId]);
        }
    }
}
