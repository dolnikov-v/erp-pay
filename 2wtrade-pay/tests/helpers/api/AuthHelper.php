<?php
namespace app\tests\helpers\api;

use app\modules\api\components\filters\auth\HttpBearerAuth;
use Yii;

/**
 * Class AuthHelper
 * @package app\tests\helpers
 */
class AuthHelper
{
    const TYPE_TOKEN_ADCOMBO = 'adcombo';
    const TYPE_TOKEN_CALL_CENTER = 'callcenter';
    const TYPE_TOKEN_2WSTORE = '2wstore';
    const TYPE_TOKEN_MERCURY = 'mercury';

    /**
     * @return string
     */
    public static function getToken($type = self::TYPE_TOKEN_ADCOMBO)
    {
        $tokens = array_flip(HttpBearerAuth::$accessTokens);
        $token = $tokens[$type];
        return 'Bearer ' . base64_encode($token);
    }

    /**
     * @param string $method
     * @return string
     */
    public static function getUrl($method)
    {
        return Yii::$app->params['url'] . '/api/v1/' . $method;
    }
}