<?php
namespace app\tests\helpers\api;

use app\modules\callcenter\models\CallCenter;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\order\models\Order;
use Yii;
use yii\base\InvalidParamException;
use yii\helpers\ArrayHelper;

class CallCenterHelper
{
    /**
     * @param integer $count
     * @return array
     */
    public static function getRandomForeignKeys($count = 1)
    {
        $request = CallCenterRequest::find()
            ->joinWith('callCenter')
            ->orderBy(
                [CallCenterRequest::tableName() . '.id' => SORT_DESC]
            )
            ->offset(rand(99, 999))
            ->limit(1)
            ->one();

        if (empty($request)) {
            throw new InvalidParamException(Yii::t('common', 'Не удалось определить страну.'));
        }

        $country = $request->callCenter->country;

        $query = CallCenterRequest::find()
            ->select('foreign_id')
            ->joinWith('callCenter')
            ->where([CallCenter::tableName() . '.country_id' => $country->id])
            ->orderBy(
                [CallCenterRequest::tableName() . '.id' => SORT_DESC]
            )
            ->offset(rand(99, 999))
            ->limit($count)
            ->all();

        $ids = ArrayHelper::getColumn($query, 'foreign_id');

        return [
            'country' => $country->char_code,
            'id' => $ids,
        ];
    }

    /**
     * @param int $count
     * @return array
     */
    public static function getRandomOrders($count = 1)
    {
        return Order::find()
            ->where(['<>', 'source_confirmed', 1])
            ->orderBy(
                [Order::tableName() . '.id' => SORT_DESC]
            )
            ->offset(rand(99, 999))
            ->limit($count)
            ->all();
    }

    /**
     * @param int $count
     * @return array
     */
    public static function getRandomForeignOrders($count = 1)
    {
        $orders = Order::find()
            ->where(['is not', 'foreign_id', null])
            ->orderBy(
                [Order::tableName() . '.id' => SORT_DESC]
            )
            ->offset(rand(99, 999))
            ->limit($count)
            ->all();

        $data = [];
        foreach ($orders as $order) {
            $data[$order->id] = [
                'id' => $order->foreign_id
            ];
        }

        return $data;

    }
}