<?php
namespace app\tests\helpers\api;

use app\models\Currency;

class CurrencyHelper
{
    /**
     * @return string
     */
    public static function getRandomCurrencyCode()
    {
        $maxIdCurrency = Currency::find()->orderBy(['id' => SORT_DESC])->limit(1)->one();
        if ($maxIdCurrency === null) {
            $maxIdCurrency = 50;
        }
        $currency = Currency::find()
            ->offset(rand(1, $maxIdCurrency->id))
            ->limit(1)
            ->one();
        if ($currency !== null) {
            return $currency->char_code;
        }
        return '';
    }

}