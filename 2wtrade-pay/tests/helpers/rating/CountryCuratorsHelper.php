<?php
namespace app\tests\helpers\rating;

use app\models\Country;
use app\models\User;
use yii\db\Expression;

class CountryCuratorsHelper
{
    /**
     * @param array $where
     * @return User[]
     */
    public static function getUsers($where = [])
    {
        $users = User::find();

        if(is_array($where) && !empty($where)){
            $users->where($where);
        }

        return $users->all();
    }
}