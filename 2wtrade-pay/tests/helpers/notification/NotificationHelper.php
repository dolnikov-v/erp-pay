<?php
namespace app\tests\helpers\notification;

use app\models\Country;
use app\models\UserNotification;
use yii\db\Expression;

class NotificationHelper
{
    /**
     * @return Country
     */
    public static function getRandomCountry()
    {
        return Country::find()
            ->orderBy(new Expression('rand()'))
            ->limit(1)
            ->one();
    }

    /**
     * @return UserNotification
     */
    public static function getRandomTrigger()
    {
        return UserNotification::find()
            ->orderBy(new Expression('rand()'))
            ->limit(1)
            ->one();
    }
}