<?php
namespace app\tests\helpers\apidelivery;

use app\modules\api\components\filters\auth\HttpBearerAuth;
use Yii;
use app\models\api\ApiUser;
use app\models\api\ApiUserToken;

/**
 * Class AuthHelper
 * @package app\tests\helpers
 */
class AuthHelper
{

    /**
     * @return string
     */
    public static function makeToken()
    {
        $apiToken = new ApiUserToken();
        $apiToken->user_id = Yii::$app->params['2wtradeApiUserId'];
        $apiToken->type = ApiUserToken::TYPE_DELIVERY;
        $apiToken->auth_token = $apiToken->generateToken();
        $apiToken->lifetime = $apiToken->getLifetimeToken();
        if ($apiToken->save()) {
            return $apiToken->auth_token;
        }
        return false;
    }

    /**
     * @param string $token
     */
    public static function deleteToken($token) {
        if ($token) {
            ApiUserToken::deleteAll(['type' => ApiUserToken::TYPE_DELIVERY, 'auth_token' => $token]);
        }
    }

    /**
     * @param string $method
     * @return string
     */
    public static function getUrl($method)
    {
        return Yii::$app->params['url'] . '/apidelivery/v1/' . $method;
    }
}