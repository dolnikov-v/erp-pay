<?php
namespace app\tests\helpers\apidelivery;

use app\models\api\ApiUserDelivery;
use app\models\Country;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use Yii;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * Class UserHelper
 * @package app\tests\helpers
 */
class UserHelper
{
    /**
     * @return mixed
     */
    public static function getRandomDeliveryCountry()
    {

        $query = new Query();
        $country = $query
            ->select([
                'country' => Country::tableName() . '.char_code',
                'delivery' => Delivery::tableName() . '.id'
            ])
            ->from(Country::tableName())
            ->innerJoin(Delivery::tableName(), Delivery::tableName() . '.country_id=' . Country::tableName() . '.id')
            ->innerJoin(ApiUserDelivery::tableName(), ApiUserDelivery::tableName() . '.delivery_id=' . Delivery::tableName() . '.id')
            ->where([
                Country::tableName() . '.active' => 1,
                Delivery::tableName() . '.active' => 1,
                Delivery::tableName() . '.our_api' => 1,
                ApiUserDelivery::tableName() . '.user_id' => Yii::$app->params['2wtradeApiUserId']
            ])
            ->groupBy([
                Country::tableName() . '.id'
            ])
            ->orderBy(new Expression('rand()'))
            ->one();

        if ($country) {
            return $country;
        }

        return false;
    }

    /**
     * @param integer $deliveryId
     * @return Order
     */
    public static function getRandomDeliveryOrder($deliveryId)
    {
        return Order::find()
            ->joinWith('deliveryRequest')
            ->where([
                DeliveryRequest::tableName() . '.delivery_id' => $deliveryId
            ])
            ->andWhere([
                Order::tableName() . '.status_id' => OrderStatus::find()->select('id')->where(['group' => OrderStatus::GROUP_DELIVERY])
            ])
            ->orderBy([
                Order::tableName() . '.id' => SORT_DESC
            ])
            ->offset(rand(99, 999))
            ->limit(1)
            ->one();
    }
}