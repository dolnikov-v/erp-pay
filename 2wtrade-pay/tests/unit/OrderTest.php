<?php
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use Codeception\Specify;

class OrderTest extends \Codeception\Test\Unit
{
    use Specify;

    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var Order
     */
    private $order;

    protected function _before()
    {
        $this->order = new Order();
    }

    protected function _after()
    {
    }

    // tests
    public function testModelValidate()
    {
        $this->specify('fields are empty' ,function () {
            expect('order is not valid', $this->order->validate())->false();
            expect('status_id has error', $this->order->getErrors())->hasKey('status_id');
            expect('country_id has error', $this->order->getErrors())->hasKey('country_id');
            expect('customer_full_name has error', $this->order->getErrors())->hasKey('customer_full_name');
            expect('customer_phone has error', $this->order->getErrors())->hasKey('customer_phone');
        });

        $this->specify('fields are valid' ,function () {
            $this->order->status_id = OrderStatus::STATUS_SOURCE_LONG_FORM;
            $this->order->country_id = 1;
            $this->order->customer_full_name = 'John Doe';
            $this->order->customer_phone = '1234597890';

            expect('order is valid', $this->order->validate())->true();
            expect('status_id has no error', $this->order->getErrors())->hasntKey('status_id');
            expect('country_id has noerror', $this->order->getErrors())->hasntKey('country_id');
            expect('customer_full_name has no error', $this->order->getErrors())->hasntKey('customer_full_name');
            expect('customer_phone has no error', $this->order->getErrors())->hasntKey('customer_phone');
        });
    }

    /**
     * @dataProvider getStatusVariants
     */
    public function testStatus($status, $result) {
        $this->order->status_id = $status;
        $this->assertEquals($this->order->validate(['status_id']), $result);
    }

    public function getStatusVariants() {
        return [
            '' => ['', false],
            // TODO такой тест не должен проходить, но у нас почемуто возможен статус 0
            //'0' => [0, false],
            '1' => [OrderStatus::STATUS_SOURCE_LONG_FORM, true],
            '2' => [OrderStatus::STATUS_SOURCE_SHORT_FORM, true]
        ];
    }
}