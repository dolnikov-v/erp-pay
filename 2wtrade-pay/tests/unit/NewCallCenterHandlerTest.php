<?php

use Codeception\Specify;
use app\modules\callcenter\components\NewCallCenterHandler;
use app\modules\order\models\Order;

class Orderr extends \app\components\db\ActiveRecord {
    public $id;
}

class NewCallCenterHandlerTest extends \Codeception\Test\Unit
{
    use Specify;

    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     *  Тест NewCallCenterHandler::prepareAndLoadValuesToOrder
     */
    public function testPrepareAndLoadValuesToOrder()
    {

        $s = '{"id":2426845,"status":6,"sub_status":"608","foreign_id":9963414,"init_price":141000,"final_price":141000,"shipping_price":0,"extra_price":0,"shipping_id":false,"disable_broker":false,"order_comment":"number is in correct format ","country_char_code":"KH","source":"2wcall","customer_components":"{\"general\":{\"customer_full_name\":\"\\u17c1\\u17a0\\u1784\\u179b\\u17b8\",\"customer_phone\":\"0936666\",\"customer_mobile\":\"\",\"customer_email\":\"\"},\"address\":{\"customer_city\":\"\",\"customer_house_number\":\"\",\"customer_zip\":\"\",\"customer_district\":\"\",\"customer_province\":\"Banteay Meanchey\",\"customer_address_add\":\"\",\"customer_street\":\"\",\"customer_address\":\"Banteay Meanchey\"}}","updated_at":1542351870,"last_operator_id":1268,"delivery_from":null,"delivery_to":null,"comment":"number is in correct format ","history":[{"field":"id","old":"","new":"","user_id":null,"operator":null},{"field":"country_id","old":"","new":"44","user_id":null,"operator":null},{"field":"type_id","old":"","new":"1","user_id":null,"operator":null},{"field":"sub_status","old":"","new":"","user_id":null,"operator":null},{"field":"partner_id","old":"","new":"1","user_id":null,"operator":null},{"field":"foreign_id","old":"","new":"9963414","user_id":null,"operator":null},{"field":"customer_phone","old":"","new":"0936666","user_id":null,"operator":null},{"field":"customer_mobile","old":"","new":"","user_id":null,"operator":null},{"field":"customer_address","old":"","new":"","user_id":null,"operator":null},{"field":"customer_ip","old":"","new":"191.101.39.220","user_id":null,"operator":null},{"field":"customer_components","old":"","new":"{\"general\":{\"customer_full_name\":\"េហងលី\",\"customer_phone\":null,\"customer_mobile\":null,\"customer_email\":\"\"},\"address\":{\"customer_city\":null,\"customer_house_number\":null,\"customer_zip\":null,\"customer_district\":null,\"customer_province\":null,\"customer_ad","user_id":null,"operator":null},{"field":"init_price","old":"","new":"141000","user_id":null,"operator":null},{"field":"final_price","old":"","new":"141000","user_id":null,"operator":null},{"field":"shipping_price","old":"","new":"","user_id":null,"operator":null},{"field":"created_at","old":"","new":"","user_id":null,"operator":null},{"field":"updated_at","old":"","new":"","user_id":null,"operator":null},{"field":"form_id","old":"","new":"71","user_id":null,"operator":null},{"field":"recall_date","old":"","new":"","user_id":null,"operator":null},{"field":"customer_first_name","old":"","new":"","user_id":null,"operator":null},{"field":"customer_last_name","old":"","new":"","user_id":null,"operator":null},{"field":"customer_middle_name","old":"","new":"","user_id":null,"operator":null},{"field":"shipping_id","old":"","new":"","user_id":null,"operator":null},{"field":"customer_email","old":"","new":"","user_id":null,"operator":null},{"field":"ordered_at","old":"","new":"1542350884","user_id":null,"operator":null},{"field":"order_phones","old":"","new":"","user_id":null,"operator":null},{"field":"language_id","old":"","new":"2","user_id":null,"operator":null},{"field":"last_queue_id","old":"","new":"","user_id":null,"operator":null},{"field":"finished","old":"","new":"","user_id":null,"operator":null},{"field":"approve_components","old":"","new":"","user_id":null,"operator":null},{"field":"source_uri","old":"","new":"http:\/\/titangel.com","user_id":null,"operator":null},{"field":"phone_error","old":"","new":"","user_id":null,"operator":null},{"field":"blocked_to","old":"","new":"","user_id":null,"operator":null},{"field":"attempts","old":"","new":"","user_id":null,"operator":null},{"field":"queue_type","old":"","new":"","user_id":null,"operator":null},{"field":"order_comment","old":"","new":"","user_id":null,"operator":null},{"field":"customer_full_name","old":"","new":"េហងលី","user_id":null,"operator":null},{"field":"sent_to_sqs","old":"","new":"","user_id":null,"operator":null},{"field":"delivery_from","old":"","new":"","user_id":null,"operator":null},{"field":"delivery_to","old":"","new":"","user_id":null,"operator":null},{"field":"latitude","old":"","new":"","user_id":null,"operator":null},{"field":"longitude","old":"","new":"","user_id":null,"operator":null},{"field":"event","old":"","new":"","user_id":null,"operator":null},{"field":"google_zip","old":"","new":"","user_id":null,"operator":null},{"field":"price_shipping_id","old":"","new":"","user_id":null,"operator":null},{"field":"partner_created_at","old":"","new":"1542350884","user_id":null,"operator":null},{"field":"confirm_address","old":"","new":"","user_id":null,"operator":null},{"field":"queue_counter","old":"","new":"","user_id":null,"operator":null},{"field":"prevent_queue_id","old":"","new":"","user_id":null,"operator":null},{"field":"date_first_call","old":"","new":"","user_id":null,"operator":null},{"field":"status","old":"1","new":"6","user_id":1268,"operator":"THOP1PM-1268"},{"field":"sub_status","old":"","new":"608","user_id":1268,"operator":"THOP1PM-1268"},{"field":"customer_components","old":"{\"general\":{\"customer_full_name\":\"េហងលី\",\"customer_phone\":null,\"customer_mobile\":null,\"customer_email\":\"\"},\"address\":{\"customer_city\":null,\"customer_house_number\":null,\"customer_zip\":null,\"customer_district\":null,\"customer_province\":null,\"customer_address_add\":null,\"customer_street\":null,\"customer_address\":null}}","new":"{\"general\":{\"customer_full_name\":\"េហងលី\",\"customer_phone\":\"0936666\",\"customer_mobile\":\"\",\"customer_email\":\"\"},\"address\":{\"customer_city\":\"\",\"customer_house_number\":\"\",\"customer_zip\":\"\",\"customer_district\":\"\",\"customer_province\":\"Banteay Meanchey\",\"customer_address_add\":\"\",\"customer_street\":\"\",\"customer_address\":\"\"}}","user_id":1268,"operator":"THOP1PM-1268"},{"field":"last_queue_id","old":"279","new":"","user_id":1268,"operator":"THOP1PM-1268"},{"field":"attempts","old":"0","new":"1","user_id":1268,"operator":"THOP1PM-1268"},{"field":"prevent_queue_id","old":"","new":"279","user_id":1268,"operator":"THOP1PM-1268"}],"queue_type":"order","confirm_address":0,"hold_to":0,"call_data":[{"record_date":"2018-11-16 07:01:31","record_phone":"0936666","user_sip":1268}],"orderProducts":[{"product_id":2,"price":141000,"quantity":1,"cost":141000,"gift":false}]}';
        $response = json_decode($s,true);

        $order = new Order();

        try {
            NewCallCenterHandler::prepareAndLoadValuesToOrder($response,$order);
        } catch (\Exception $e) {
            print $e->getMessage();
            die();
        }

        $this->assertSame($order->customer_address, 'Banteay Meanchey', 'customer_address ok');
        $this->assertSame($order->customer_phone, '0936666', 'customer_phone ok');
        $this->assertSame($order->price_total, 141000, 'price_total ok');
    }


    /**
     *  Тест NewCallCenterHandler::getIsHold
     */
    public function getIsHold()
    {

        $response = [];
        $result = NewCallCenterHandler::getIsHold($response);
        $this->assertFalse($result, 'Is Hold is False');

        $response = [
            'confirm_address' => 1,
            'hold_to' => strtotime('+1 day')
        ];
        $result = NewCallCenterHandler::getIsHold($response);
        $this->assertTrue($result, 'Is Hold is True');
    }
}