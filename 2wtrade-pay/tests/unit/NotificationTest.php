<?php

use app\tests\helpers\notification\NotificationHelper;

class NotificationTest extends \Codeception\Test\Unit
{
    public $component;
    /** @var  @var \app\models\Country */
    public $country;
    /** @var  @var \app\models\UserNotification */
    public $trigger;
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
        $this->component = yii::$app->notification;
        $this->country = NotificationHelper::getRandomCountry();
        $this->trigger = NotificationHelper::getRandomTrigger();
    }

    protected function _after()
    {
    }

    // tests
    public function testGetListUsers()
    {
        $this->trigger->trigger = app\models\Notification::TRIGGER_REPORT_ORDERS_STATUS_IN_1_2_BY_COUNTRY;
        $this->country->id = 1;

        $listUsers = $this->component->getListUsers($this->trigger->trigger);


        $this->assertTrue(is_array($listUsers), 'Method getListUsers() return array?');
    }
}