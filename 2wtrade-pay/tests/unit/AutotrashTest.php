<?php
use app\tests\helpers\api\LeadHelper;
use app\modules\catalog\controllers\AutotrashController;
use app\models\User;

class AutotrashTest extends \Codeception\Test\Unit
{
    /**
     * @var  []
     */
    public $lead;

    public function _before()
    {
        $this->lead = LeadHelper::getRandomDataOrders(1);
    }

    public function _after()
    {
    }

    // tests
    public function testIsAutotrash()
    {
        $autotrash = AutotrashController::classifyOrder($this->lead[0]);
        $this->assertInternalType('boolean', $autotrash, 'autotrash is bool ?');
    }
}
