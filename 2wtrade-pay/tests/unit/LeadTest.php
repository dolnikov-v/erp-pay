<?php
use app\modules\order\models\Lead;
use Codeception\Specify;

class LeadTest extends \Codeception\Test\Unit
{
    use Specify;

    /**
     * @var \UnitTester
     */
    protected $tester;

    public function testDetectPackageByProducts()
    {
        $products = [
            [
                "product_id" => "7",
                "quantity" => 2,
                "price" => 1235
            ],
            [
                "product_id" => "7",
                "quantity" => 1,
                "price" => 0
            ],
            [
                "product_id" => "8",
                "quantity" => 1,
                "price" => 100
            ],
        ];

        $result = Lead::detectPackageByProducts($products);
        $this->assertEquals(3, $result, '3 package detected');

        $products = [
            [
                "product_id" => "7",
                "quantity" => 2,
                "price" => 1235
            ],
            [
                "product_id" => "7",
                "quantity" => 1,
                "price" => 0
            ],
        ];

        $result = Lead::detectPackageByProducts($products);
        $this->assertEquals(3, $result, '3 package detected');

        $products = [
            [
                "product_id" => "7",
                "quantity" => 1,
                "price" => 1235
            ],
        ];

        $result = Lead::detectPackageByProducts($products);
        $this->assertEquals(1, $result, '1 package detected');

        $products = [
            [
                "product_id" => "7",
                "quantity" => 3,
                "price" => 1235
            ],
            [
                "product_id" => "7",
                "quantity" => 2,
                "price" => 0
            ],
        ];

        $result = Lead::detectPackageByProducts($products);
        $this->assertEquals(5, $result, '5 package detected');

        $products = [
            [
                "product_id" => Lead::PRODUCT_GREEN_COFFEE,
                "quantity" => 2,
                "price" => 400
            ],
            [
                "product_id" => Lead::PRODUCT_GOJI_CREAM,
                "quantity" => 1,
                "price" => 0
            ],
        ];

        $result = Lead::detectPackageByProducts($products);
        $this->assertEquals(2, $result, '2 package detected');

        $products = [
            [
                "product_id" => Lead::PRODUCT_GREEN_COFFEE,
                "quantity" => 2,
                "price" => 400
            ],
            [
                "product_id" => Lead::PRODUCT_GREEN_COFFEE,
                "quantity" => 0,
                "price" => 200
            ],
            [
                "product_id" => Lead::PRODUCT_VARIKOSETTE,
                "quantity" => 1,
                "price" => 0
            ],
        ];

        $result = Lead::detectPackageByProducts($products);
        $this->assertEquals(6, $result, '6 package detected');

        $products = [
            [
                "product_id" => Lead::PRODUCT_GREEN_COFFEE,
                "quantity" => 2,
                "price" => 400
            ],
            [
                "product_id" => Lead::PRODUCT_GREEN_COFFEE,
                "quantity" => 1,
                "price" => 200
            ],
            [
                "product_id" => Lead::PRODUCT_INTOXIC,
                "quantity" => 1,
                "price" => 0
            ],
        ];

        $result = Lead::detectPackageByProducts($products);
        $this->assertEquals(4, $result, '4 package detected');

        $products = [
            [
                "product_id" => Lead::PRODUCT_GREEN_COFFEE,
                "quantity" => 2,
                "price" => 500
            ],
            [
                "product_id" => Lead::PRODUCT_NEOGELIO_MASK,
                "quantity" => 1,
                "price" => 0
            ],
        ];

        $result = Lead::detectPackageByProducts($products);
        $this->assertEquals(7, $result, '7 package detected');
    }

}