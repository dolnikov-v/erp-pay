<?php
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryZipcodes;
use app\tests\helpers\broker\BrokerHelper;

class BrokerTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var DeliveryZipcodes
     */
    public $modelDeliveryZipcodes;

    /**
     * @var Delivery
     */
    public $modelDelivery;

    protected function _before()
    {
        $deliveryZipcodes = BrokerHelper::getRandomBrokerDeliveryZipcodes();
        $this->modelDeliveryZipcodes = $deliveryZipcodes;

        $deliveries = BrokerHelper::getRandomBrokerDelivery();
        $this->modelDelivery = $deliveries;
    }

    protected function _after()
    {
    }

    // tests
    /**
     * Тест  Delivery метод getOrdersApprovedOneDay()
     */
    public function testGetApprovedOrders()
    {
        $countApprovedOrdersForDelivery = $this->modelDelivery[0]->getOrdersApprovedOneDay();
        $this->assertInternalType('integer', $countApprovedOrdersForDelivery, 'I have: ' . $countApprovedOrdersForDelivery . ' this integer?');
    }

    /**
     * Тест  DeliveryZipcodes метод getOrdersApprovedOneDay()
     */
    public function testGetApprovedOrdersByDeliveryZipcodes()
    {
        $countApprovedOrdersForDeliveryZipcodes = $this->modelDeliveryZipcodes[0]->getOrdersApprovedOneDay();
        $this->assertInternalType('integer', $countApprovedOrdersForDeliveryZipcodes, 'I have: ' . $countApprovedOrdersForDeliveryZipcodes . ' this integer?');
    }

    /**
     * Тест Delivery метод brokerageScore()
     */
    public function testBrokerageScoreForDelivery()
    {
        $scoreDelivery = $this->modelDelivery[0]->brokerageScore();
        $result = is_numeric($scoreDelivery) || is_null($scoreDelivery);

        $this->assertTrue($result, 'I have score by Delivery: ' . $result . ' this double or null?');
    }

    /**
     * Тест Delivery метод brokerageScore($deliveryZipcodes)
     */
    public function testBrokerageScoreForDeliveryZipcodes()
    {
        $scoreDeliveryZipcodes = $this->modelDelivery[0]->brokerageScore($this->modelDeliveryZipcodes[0]);
        $result = is_numeric($scoreDeliveryZipcodes) || is_null($scoreDeliveryZipcodes);

        $this->assertTrue($result, 'I have score by DeliveryZipcodes: ' . $result . ' this double or null?');
    }

    /**
     *  Тест Delivery метод verifyZipCode($zip)
     */
    public function testVerifyZipCode()
    {
        $zip = BrokerHelper::getRandomPostalCode();
        $verifyZipCode = $this->modelDelivery[0]->verifyZipCode($zip);

        $result = $verifyZipCode instanceof DeliveryZipcodes || $verifyZipCode === false;

        $this->assertTrue($result, 'VerifyZipCode by Delivery: this DeliveryZipCodes or false?');

    }


    /**
     *  Тест Delivery метод canDeliverProducts($productIds)
     */
    public function testCanDeliverProducts()
    {

        $data = BrokerHelper::getRandomDeliveryWithStop();

        $this->assertFalse($data->delivery->canDeliverProducts([$data->product_id]));
    }

    /**
     *  Тест Delivery метод canDeliverPackage($productIds)
     */
    public function testCanDeliverPackage()
    {

        $data = BrokerHelper::getRandomDeliveryWithPackage();
        $this->assertTrue($data->delivery->canDeliverPackage($data->package_id));
    }
}