<?php
namespace delivery;

use Codeception\Test\Unit;
use app\modules\delivery\models\Delivery;

class DeliveryModelGetEmailListTest extends Unit
{

    /**
     * @var \UnitTester
     */
    protected $tester;

    private $delivery;

    protected function _before()
    {
        $this->delivery = new Delivery();
    }

    protected function _after()
    {
        unset($this->delivery);
    }

    /**
     * @dataProvider emailProvider
     * @param string $emailString список адресов почты, собранных в строку через разделитель
     * @param array $expectedEmails массив провалидированных email, ожидающихся на выходе
     */
    public function testGetEmails(string $emailString, array $expectedEmails) : void
    {
        $this->delivery->list_send_to_emails = $emailString;
        $this->assertEquals(
            $this->delivery->getEmailList(),
            $expectedEmails
        );
    }
    
    public function emailProvider() : array
    {
        return [
            "Простой валидный email в единственном экземпляре" => [
                "emailString" => "test@test.ru",
                "expectedEmails" => [
                    "test@test.ru"
                ]
            ],
            "Простой невалидный email в единственном экземпляре" => [
                "emailString" => "test@test",
                "expectedEmails" => []
            ],
            "Email с двухуровневым доменом" => [
                "emailString" => "yuttanait.y@business-idea.co.th",
                "expectedEmails" => [
                    "yuttanait.y@business-idea.co.th"
                ]
            ],
            "Список email-ов" => [
                "emailString" => "test@test.ru,yuttanait.y@business-idea.co.th",
                "expectedEmails" => [
                    "test@test.ru",
                    "yuttanait.y@business-idea.co.th"
                ]
            ],
        ];
    }
    
}