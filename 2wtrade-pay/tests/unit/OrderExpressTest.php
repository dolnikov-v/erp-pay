<?php
use app\modules\order\models\OrderExpressDelivery;
use \app\tests\helpers\api\LeadHelper;

class OrderExpressTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     *  Тест OrderExpressDelivery
     */
    public function testCanSave()
    {
        $data = LeadHelper::getRandomDataOrders(1);
        $this->assertTrue(OrderExpressDelivery::saveIt($data[0]['id'], true));
        $this->assertEquals(1, OrderExpressDelivery::saveIt($data[0]['id'], false));
    }

}