<?php

namespace app\widgets;

use app\helpers\i18n\DateTimePicker as DateTimePickerHelper;
use Yii;

/**
 * Class DateRangePicker
 * @package app\widgets
 */
class DateRangePicker extends DateTimePicker
{
    /** @var string */
    public $locale;

    /** @var string */
    public $format;

    /** @var string */
    public $nameFrom;

    /** @var string */
    public $valueFrom;

    /** @var string */
    public $titleFrom;

    /** @var string */
    public $placeholderFrom;

    /** @var boolean */
    public $disabledFrom = false;

    /** @var string */
    public $nameTo;

    /** @var string */
    public $valueTo;

    /** @var string */
    public $titleTo;

    /** @var string */
    public $placeholderTo;

    /** @var boolean */
    public $disabledTo = false;

    /** @var string */
    public $minDate;

    /** @var string */
    public $maxDate;

    /**
     * @var array
     */
    public $ranges = [];

    /**
     * @var boolean
     */
    public $rangesDisabled = false;

    /**
     * @var bool
     */
    public $hideRanges = false;

    /**
     * @var string
     */
    public $formatFunc;

    /**
     * @return string
     * @throws \Exception
     */
    public function run()
    {
        if (empty($this->ranges)) {
            $this->ranges = $this->getDefaultRanges();
        }

        $attributes = $this->attributes ?? [];
        if ($this->minDate) {
            $attributes['data-mindate'] = $this->minDate;
        }

        if ($this->maxDate) {
            $attributes['data-maxdate'] = $this->maxDate;
        }

        $attributesFrom = $attributes;
        $attributesTo = $attributes;

        if ($this->titleFrom) {
            $attributesFrom['title'] = $this->titleFrom;
        }

        if ($this->placeholderFrom) {
            $attributesFrom['placeholder'] = $this->placeholderFrom;
        }

        if ($this->titleTo) {
            $attributesTo['title'] = $this->titleTo;
        }

        if ($this->placeholderTo) {
            $attributesTo['placeholder'] = $this->placeholderTo;
        }

        $format = $this->format ? $this->format : DateTimePickerHelper::getDateFormat();
        $formatFunc = $this->formatFunc ?: 'asDate';
        return $this->render('date-range-picker', [
            'locale' => $this->locale ? $this->locale : DateTimePickerHelper::getLocale(),
            'format' => $format,

            'inputFrom' => new InputText([
                'name' => $this->nameFrom,
                'value' => ($this->valueFrom) ? Yii::$app->formatter->$formatFunc($this->valueFrom) : '',
                'disabled' => $this->disabledFrom,
                'attributes' =>  $attributesFrom
            ]),

            'inputTo' => new InputText([
                'name' => $this->nameTo,
                'value' => ($this->valueTo) ? Yii::$app->formatter->$formatFunc($this->valueTo) : '',
                'disabled' => $this->disabledTo,
                'attributes' =>  $attributesTo
            ]),

            'rangeCollection' => $this->hideRanges ? false : new Select2([
                'items' => $this->ranges,
                'length' => -1,
                'disabled' => $this->rangesDisabled,
            ]),
        ]);
    }

    /**
     * @return array
     */
    public static function getFutureRanges()
    {
        return [
            'today' => Yii::t('common', 'Сегодня'),
            'tomorrow' => Yii::t('common', 'Завтра'),
            'next_week' => Yii::t('common', 'Следующая неделя'),
            'next_two_weeks' => Yii::t('common', 'Следующие две недели'),
            'next_month' => Yii::t('common', 'Следующий месяц'),
        ];
    }

    /**
     * @return array
     */
    private function getDefaultRanges()
    {
        return [
            'today' => Yii::t('common', 'Сегодня'),
            'yesterday' => Yii::t('common', 'Вчера'),
            'current_week' => Yii::t('common', 'Текущая неделя'),
            'before_week' => Yii::t('common', 'Прошлая неделя'),
            'current_month' => Yii::t('common', 'Текущий месяц'),
            'before_month' => Yii::t('common', 'Прошлый месяц'),
        ];
    }

    /**
     * @return array
     */
    public static function getMonthRanges()
    {
        return [
            'current_month' => Yii::t('common', 'Текущий месяц'),
            'before_month' => Yii::t('common', 'Прошлый месяц'),
            'next_month' => Yii::t('common', 'Следующий месяц'),
        ];
    }
}
