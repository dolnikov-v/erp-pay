<?php
namespace app\widgets;

use app\widgets\assets\CropperAsset;
use yii\helpers\Url;

/**
 * Class Cropper
 * @package app\widgets
 */
class Cropper extends Widget
{
    const RATIO_FREE = false;
    const RATIO_RECTANGLE = 1.3333;
    const RATIO_SQUARE = 1;

    /** @var  string */
    public $image;

    /** @var  string */
    public $cropData;

    /** @var  integer */
    public $linkId;

    /** @var  string */
    public $aspectRatio = self::RATIO_SQUARE;

    /** @var  boolean */
    public $circleImage = false;

    /** @var  string */
    private $fileName;

    /** @var  string */
    private $nameInput;

    /** @var  string */
    private $urlUpload;

    /** @var  string */
    private $urlCropSave;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        /** @var array $fileName */
        $fileName = explode('/', $this->image);
        $this->fileName = array_pop($fileName);
        $this->nameInput = 'mediaFile';
        $this->urlUpload = Url::to('/media/index/upload');
        $this->urlCropSave = Url::to('/media/index/save-crop-data');
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        CropperAsset::register($this->getView());

        return $this->render('cropper', [
            'nameInput' => $this->nameInput,
            'fileName' => $this->fileName,
            'image' => $this->image,
            'linkId' => $this->linkId,
            'cropData' => $this->cropData,
            'urlUpload' => $this->urlUpload,
            'urlCropSave' => $this->urlCropSave,
            'aspectRatio' => $this->aspectRatio,
            'circleImage' => $this->circleImage,
        ]);
    }

}
