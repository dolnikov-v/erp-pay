<?php
namespace app\widgets;

/**
 * Class Select
 * @package app\widgets
 */
class Select extends Widget
{
    /**
     * @var string Имя для select
     */
    public $name;

    /**
     * @var string Выбранное значение
     */
    public $value;

    /**
     * @var string
     */
    public $prompt;

    /**
     * @var array
     */
    public $items = [];

    /**
     * @var bool
     */
    public $multiple = false;

    /**
     * @var bool
     */
    public $disabled = false;

    /**
     * @var bool|string
     */
    public $placeholder = false;

    /**
     * @var bool|string
     */
    public $style = false;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('select', [
            'id' => $this->id,
            'name' => $this->name,
            'value' => $this->value,
            'prompt' => $this->prompt,
            'items' => $this->items,
            'multiple' => $this->multiple,
            'disabled' => $this->disabled,
            'placeholder' => $this->placeholder,
            'style' => $this->style,
            'attributes' => $this->prepareAttributes(),
        ]);
    }
}
