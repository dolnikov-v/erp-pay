<?php
namespace app\widgets;

/**
 * Class InputFile
 * @package app\widgets
 */
class InputFile extends Widget
{
    const SIZE_SMALL = 'input-sm';
    const SIZE_LARGE = 'input-lg';

    /**
     * @var string
     */
    public $type = 'file';

    /**
     * @var
     */
    public $name;

    /**
     * @var
     */
    public $count;

    /**
     * @var boolean
     */
    public $disabled = false;

    /**
     * @var boolean
     */
    public $focus = false;

    /**
     * @var string
     */
    public $hook = false;

    /**
     * @var bool
     */
    public $multiple = false;

    /**
     * @var string
     */
    public $size;

    /**
     * @var string
     */
    public $accept;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('input-file', [
            'name' => $this->name,
            'count' => $this->count,
            'disabled' => $this->disabled,
            'focus' => $this->focus,
            'id' => $this->id,
            'hook' => $this->hook,
            'multiple' => $this->multiple,
            'size' => $this->size,
            'accept' => $this->accept,
            'attributes' => $this->prepareAttributes(),
        ]);
    }
}
