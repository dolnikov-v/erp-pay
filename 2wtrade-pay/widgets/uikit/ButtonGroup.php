<?php
namespace app\widgets\uikit;

use app\widgets\Button;

/**
 * Class ButtonGroup
 * @package app\widgets\uikit
 */
class ButtonGroup extends Button
{
    /**
     * @var string
     */
    public $style = Button::STYLE_PRIMARY;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $value;

    /**
     * @var boolean
     */
    public $withIcon = true;

    /**
     * @var \app\widgets\Button[] Кнопки
     */
    public $buttons = [];

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('button-group', [
            'style' => $this->style,
            'size' => $this->size,
            'name' => $this->name,
            'value' => $this->value,
            'withIcon' => $this->withIcon,
            'buttons' => $this->buttons,
            'attributes' => $this->prepareAttributes(),
        ]);
    }
}
