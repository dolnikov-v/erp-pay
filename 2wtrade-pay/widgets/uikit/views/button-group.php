<?php
/** @var string $style */
/** @var string $size */
/** @var string $name */
/** @var string $value */
/** @var boolean $withIcon */
/** @var \app\widgets\Button[] $buttons */
/** @var string $attributes */
?>

<?php if ($buttons): ?>
    <div class="btn-group" data-toggle="buttons" role="group" <?= $attributes ?>>
        <?php foreach ($buttons as $button): ?>
            <label class="btn btn-outline <?= $style ?> <?= $size ?> <?php if ($button->value == $value): ?>active<?php endif; ?>">
                <input type="radio"
                       name="<?= $name ?>"
                       autocomplete="off"
                       value="<?= $button->value ?>" <?php if ($button->value == $value): ?>checked="checked"<?php endif; ?>/>

                <?php if ($withIcon): ?>
                    <i class="icon wb-check text-active" aria-hidden="true"></i>
                <?php endif; ?>

                <?= $button->label ?>
            </label>
        <?php endforeach; ?>
    </div>
<?php endif; ?>
