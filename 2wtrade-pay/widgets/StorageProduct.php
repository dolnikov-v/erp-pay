<?php
namespace app\widgets;

use Yii;
use app\modules\storage\models\StorageProduct as StorageProductModel;
use app\modules\storage\models\Storage;

/**
 * Class StorageProduct
 * @package app\widgets
 */
class StorageProduct extends Widget
{
    /**
     * @var string
     */
    public $nameStorage;

    /**
     * @var string
     */
    public $nameProduct;

    /**
     * @var string
     */
    public $nameShelfLife;

    /**
     * @var integer
     */
    public $storage = null;

    /**
     * @var integer
     */
    public $product = null;

    /**
     * @var array
     */
    public $storages = [];

    /**
     * @var array
     */
    public $products = [];

    /**
     * @var boolean
     */
    public $productsDisabled = false;

    /**
     * @var boolean
     */
    public $noUseBar;

    /**
     * @return string
     * @throws \Exception
     */
    public function run()
    {
        $this->storages = Storage::find()
            ->byCountryId(Yii::$app->user->country->id)
            ->noUseBar($this->noUseBar)
            ->orderBy('name')
            ->collection();

        if ($this->storage != null) {
            $productsAll = StorageProductModel::find()
                ->joinWith(['product'])
                ->byStorageId($this->storage)
                ->orderBy('name')
                ->all();

            $result = null;
            if (count($productsAll) > 0) {
                foreach ($productsAll as $productOne) {
                    $result[$productOne->product->id] = $productOne->product->name;
                }
                $this->products = $result;
            }
        }

        if (count($this->products) == 0) {
            $this->productsDisabled = true;
            $this->product = null;
        }

        return $this->render('storage-product', [
            'storageCollection' => new Select2([
                'items' => $this->storages,
                'prompt' => '—',
                'length' => false,
                'name' => $this->nameStorage,
                'value' => $this->storage,
            ]),

            'productCollection' => new Select2([
                'items' => $this->products,
                'prompt' => '—',
                'length' => false,
                'name' => $this->nameProduct,
                'disabled' => $this->productsDisabled,
                'value' => $this->product,
            ]),

            'productShelfLife' => new Select2([
                'items' => [],
                'prompt' => '—',
                'length' => false,
                'name' => $this->nameShelfLife,
                'disabled' => true,
                'value' => null,
                'defaultValue' => 1
            ]),
        ]);
    }
}