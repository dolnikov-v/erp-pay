<?php
namespace app\widgets;

/**
 * Class ListGroup
 * @package app\widgets
 */
class ListGroup extends Widget
{
    /**
     * @var ListGroupItem[]
     */
    public $items = [];
    /**
     * @var string
     */
    public $header = '';

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('list-group/list-group', [
            'header' => $this->header,
            'items' => $this->items,
        ]);
    }
}
