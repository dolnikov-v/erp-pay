<?php
namespace app\widgets;

/**
 * Class InputGroup
 * @package app\widgets
 */
class InputGroup extends Widget
{
    /**
     * @var string
     */
    public $label;

    /**
     * @var InputText
     */
    public $input;

    /**
     * @var array
     */
    public $left = [];

    /**
     * @var array
     */
    public $right = [];

    /**
     * @return string
     * @throws \InvalidArgumentException
     */
    public function run()
    {
        return $this->render('input-group', [
            'label' => $this->label,
            'input' => $this->input,
            'left' => $this->left,
            'right' => $this->right,
        ]);
    }
}
