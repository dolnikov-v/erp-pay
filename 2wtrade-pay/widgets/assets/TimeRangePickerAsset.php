<?php

namespace app\widgets\assets;


use yii\web\AssetBundle;

/**
 * Class TimeRangePickerAsset
 * @package app\widgets\assets
 */
class TimeRangePickerAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@app/web/widgets/time-range-picker';

    /**
     * @var array
     */
    public $css = [
        'time-range-picker.css',
    ];

    /**
     * @var array
     */
    public $js = [
        'time-range-picker.js',
    ];

    /**
     * @var array
     */
    public $depends = [
        'app\assets\vendor\MomentAsset',
        'app\widgets\assets\DatePickerAsset',
        'app\widgets\assets\DateTimePickerAsset',
    ];
}