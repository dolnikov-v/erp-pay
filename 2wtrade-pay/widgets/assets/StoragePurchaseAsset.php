<?php
namespace app\widgets\assets;

use yii\web\AssetBundle;

/**
 * Class StoragePurchaseAsset
 * @package app\widgets\assets
 */
class StoragePurchaseAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@app/web/widgets/storage-purchase';

    /**
     * @var array
     */
    public $css = [
        'storage-purchase.css',
    ];

    /**
     * @var array
     */
    public $js = [

    ];

    /**
     * @var array
     */
    public $depends = [
        'app\assets\SiteAsset',
    ];
}