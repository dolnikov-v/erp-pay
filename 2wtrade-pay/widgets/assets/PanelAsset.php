<?php

namespace app\widgets\assets;

use yii\web\AssetBundle;

/**
 * Class PanelAsset
 * @package app\widgets\assets
 */
class PanelAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@app/web/vendor/components/panel';

    /**
     * @var array
     */
    public $js = [
        'panel.js',
    ];

    public $css = [
        'panel.css',
    ];

    /**
     * @var array
     */
    public $depends = [
        'app\assets\SiteAsset',
    ];
}
