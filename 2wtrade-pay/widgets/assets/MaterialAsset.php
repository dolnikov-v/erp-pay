<?php
namespace app\widgets\assets;

use yii\web\AssetBundle;

/**
 * Class MaterialAsset
 * @package app\widgets\assets
 */
class MaterialAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@app/web/vendor/components/material';

    /**
     * @var array
     */
    public $js = [
        'material.min.js',
    ];

    /**
     * @var array
     */
    public $depends = [
        'app\assets\SiteAsset',
    ];
}
