<?php
namespace app\widgets\assets;

use yii\web\AssetBundle;

/**
 * Class StorageProductAsset
 * @package app\widgets\assets
 */
class StorageProductAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@app/web/widgets/storage-product';

    /**
     * @var array
     */
    public $css = [
        'storage-product.css',
    ];

    /**
     * @var array
     */
    public $js = [
        'storage-product.js',
    ];

    /**
     * @var array
     */
    public $depends = [
        'app\assets\SiteAsset',
    ];
}