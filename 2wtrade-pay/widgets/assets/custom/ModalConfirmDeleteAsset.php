<?php
namespace app\widgets\assets\custom;

use yii\web\AssetBundle;

/**
 * Class ModalConfirmDeleteAsset
 * @package app\widgets\assets\custom
 */
class ModalConfirmDeleteAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@app/web/widgets/custom/modal-confirm-delete';

    /**
     * @var array
     */
    public $js = [
        'modal-confirm-delete.js',
    ];

    /**
     * @var array
     */
    public $depends = [
        'app\assets\SiteAsset',
    ];
}
