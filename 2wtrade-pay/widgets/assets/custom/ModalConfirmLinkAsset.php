<?php
namespace app\widgets\assets\custom;

use yii\web\AssetBundle;

/**
 * Class ModalConfirmLinkAsset
 * @package app\widgets\assets\custom
 */
class ModalConfirmLinkAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@app/web/widgets/custom/modal-confirm-link';

    /**
     * @var array
     */
    public $js = [
        'modal-confirm-link.js',
    ];

    /**
     * @var array
     */
    public $depends = [
        'app\assets\SiteAsset',
    ];
}
