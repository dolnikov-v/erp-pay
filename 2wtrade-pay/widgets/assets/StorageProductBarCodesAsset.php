<?php
namespace app\widgets\assets;

use yii\web\AssetBundle;

/**
 * Class StorageProductAsset
 * @package app\widgets\assets
 */
class StorageProductBarCodesAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@app/web/widgets/storage-product-bar-codes';

    /**
     * @var array
     */
    public $css = [
        'storage-product-bar-codes.css',
    ];

    /**
     * @var array
     */
    public $js = [
        'storage-product-bar-codes.js',
    ];

    /**
     * @var array
     */
    public $depends = [
        'app\assets\SiteAsset',
    ];
}