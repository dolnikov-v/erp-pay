<?php
namespace app\widgets\assets;

use yii\web\AssetBundle;

/**
 * Class CountryDeliveryAsset
 * @package app\widgets\assets
 */
class CountryDeliveryAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@app/web/widgets/country-delivery';

    /**
     * @var array
     */
    public $css = [
        'country-delivery.css',
    ];

    /**
     * @var array
     */
    public $js = [
        'country-delivery.js',
    ];

    /**
     * @var array
     */
    public $depends = [
        'app\assets\SiteAsset',
    ];
}
