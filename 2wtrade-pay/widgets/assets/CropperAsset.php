<?php
namespace app\widgets\assets;

use app\helpers\i18n\DateTimePicker;
use Yii;
use yii\web\AssetBundle;

/**
 * Class JCropAsset
 * @package app\widgets\assets
 */
class CropperAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@app/web/widgets/profile/cropper';

    /**
     * @var array
     */
    public $css = [
        'cropper.css',
    ];

    /**
     * @var array
     */
    public $js = [
        'cropper.js',
    ];

    /**
     * @var array
     */
    public $depends = [
        'app\assets\vendor\CropperAsset'
    ];
}
