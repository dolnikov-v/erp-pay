<?php
namespace app\widgets\assets;

use yii\web\AssetBundle;

/**
 * Class CountryStorageAsset
 * @package app\widgets\assets
 */
class CountryStorageAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@app/web/widgets/country-storage';

    /**
     * @var array
     */
    public $css = [
        'country-storage.css',
    ];

    /**
     * @var array
     */
    public $js = [
        'country-storage.js',
    ];

    /**
     * @var array
     */
    public $depends = [
        'app\assets\SiteAsset',
    ];
}
