<?php
namespace app\widgets\assets;

use yii\web\AssetBundle;

/**
 * Class InputGroupFileAsset
 * @package app\widgets\assets
 */
class InputGroupFileAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@app/web/widgets/input-group-file';

    /**
     * @var array
     */
    public $js = [
        'input-group-file.js',
    ];

    /**
     * @var array
     */
    public $depends = [
        'app\assets\SiteAsset',
    ];
}
