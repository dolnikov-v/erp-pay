<?php
namespace app\widgets\assets;

use app\helpers\i18n\DateTimePicker;
use Yii;
use yii\web\AssetBundle;

/**
 * Class DatePickerAsset
 * @package app\widgets\assets
 */
class DatePickerAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@app/web/vendor/vendor/bootstrap-datepicker';

    /**
     * @var array
     */
    public $css = [
        'bootstrap-datepicker.min.css',
    ];

    /**
     * @var array
     */
    public $js = [
        'bootstrap-datepicker.min.js',
        'components/bootstrap-datepicker.min.js',
    ];

    /**
     * @var array
     */
    public $depends = [
        'app\assets\SiteAsset',
    ];

    /**
     * Init
     */
    public function init()
    {
        $locale = DateTimePicker::getLocale();

        $file = Yii::getAlias($this->sourcePath . '/locales/bootstrap-datepicker.' . $locale . '.min.js');

        if (file_exists($file)) {
            $this->js[] = 'locales/bootstrap-datepicker.' . $locale . '.min.js';
        }

        parent::init();
    }
}
