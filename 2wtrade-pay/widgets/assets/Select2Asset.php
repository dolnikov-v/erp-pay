<?php
namespace app\widgets\assets;

use app\helpers\i18n\DateTimePicker;
use Yii;
use yii\web\AssetBundle;

/**
 * Class Select2Asset
 * @package app\widgets\assets
 */
class Select2Asset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@app/web/vendor/vendor/select2';

    public $css = [
        'select2.min.css',
    ];

    /**
     * @var array
     */
    public $js = [
        'select2.full.min.js',
        'select2.custom.js',
        'components/select2.min.js',
    ];

    /**
     * @var array
     */
    public $depends = [
        'app\assets\SiteAsset',
    ];

    /**
     * @inheritdoc
     */
    public function init()
    {
        if ($locale = DateTimePicker::getLocale()) {
            $path = 'i18n/' . $locale . '.js';
            $file = Yii::getAlias($this->sourcePath . '/' . $path);

            if (file_exists($file)) {
                $this->js[] = $path;
            }
        }

        parent::init();
    }
}
