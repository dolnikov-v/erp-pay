<?php

namespace app\widgets\assets;


use yii\web\AssetBundle;

class ChangeableListAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@app/web/widgets/changeable-list';

    /**
     * @var array
     */
    public $css = [
        'changeable-list.css',
    ];

    /**
     * @var array
     */
    public $js = [
        'changeable-list.js',
    ];

    /**
     * @var array
     */
    public $depends = [
        'app\assets\SiteAsset',
    ];
}