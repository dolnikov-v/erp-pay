<?php
namespace app\widgets\assets;

use app\helpers\i18n\DateTimePicker;
use Yii;
use yii\web\AssetBundle;

/**
 * Class DateTimePickerAsset
 * @package app\widgets\assets
 */
class DateTimePickerAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@app/web/vendor/vendor/bootstrap-datetimepicker';

    /**
     * @var array
     */
    public $css = [
        'bootstrap-datetimepicker.min.css',
        'datetimepicker.css',
    ];

    /**
     * @var array
     */
    public $js = [
        'bootstrap-datetimepicker.min.js',
        'datetimepicker.js',
        'components/bootstrap-datetimepicker.js',
        'components/bootstrap-daterangepicker.js',
    ];

    /**
     * @var array
     */
    public $depends = [
        'app\assets\vendor\MomentAsset',
    ];
}
