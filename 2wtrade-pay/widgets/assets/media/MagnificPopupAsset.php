<?php
namespace app\widgets\assets\media;

use Yii;
use yii\web\AssetBundle;

/**
 * Class MagnificPopupAsset
 * @package app\widgets\assets\media
 */
class MagnificPopupAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@app/web/vendor/vendor/magnific-popup';

    /**
     * @var array
     */
    public $css = [
        'magnific-popup.min.css',
    ];

    /**
     * @var array
     */
    public $js = [
        'jquery.magnific-popup.min.js',
        'components/magnific-popup.min.js',
    ];

    /**
     * @var array
     */
    public $depends = [
        'app\assets\SiteAsset',
    ];
}
