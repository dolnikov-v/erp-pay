<?php
namespace app\widgets;

/**
 * Class ProgressBar
 * @package app\widgets
 */
class ProgressBar extends Widget
{
    const COLOR_DEFAULT = 'progress-bar-default';
    const COLOR_SUCCESS = 'progress-bar-success';
    const COLOR_DANGER = 'progress-bar-danger';

    const SIZE_XS = 'progress-xs';
    const SIZE_SM = 'progress-sm';
    const SIZE_MD = 'progress-md';
    const SIZE_LG = 'progress-lg';

    /**
     * @var string
     */
    public $color = self::COLOR_DEFAULT;

    /**
     * @var string
     */
    public $size = self::SIZE_MD;

    /**
     * @var boolean
     */
    public $active = true;

    /**
     * @var integer
     */
    public $start = 0;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('progress-bar', [
            'color' => $this->color,
            'size' => $this->size,
            'active' => $this->active,
            'start' => $this->start,
            'attributes' => $this->prepareAttributes(),
        ]);
    }
}
