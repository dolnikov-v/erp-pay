<?php
namespace app\widgets;

/**
 * Class Button
 * @package app\widgets
 */
class ButtonProgress extends Button
{
    const ANIMATION_EXPAND_LEFT = 'expand-left';
    const ANIMATION_EXPAND_RIGHT = 'expand-right';
    const ANIMATION_ZOOM_IN = 'zoom-in';
    const ANIMATION_ZOOM_OUT = 'zoom-out';
    const ANIMATION_SLIDE_LEFT = 'slide-left';
    const ANIMATION_SLIDE_RIGHT = 'slide-right';
    const ANIMATION_SLIDE_UP = 'slide-up';
    const ANIMATION_SLIDE_DOWN = 'slide-down';

    const DATA_PLUGIN = 'laddaProgress';

    /**
     * @var string
     */
    public $animation = self::ANIMATION_SLIDE_UP;

    /**
     * @var array
     */
    public $attributes = [];

    /**
     * @return string
     */
    public function run()
    {
        $this->attributes['data-style'] = $this->animation;
        $this->attributes['data-plugin'] = self::DATA_PLUGIN;

        return $this->render('button-progress', [
            'id' => $this->id,
            'label' => $this->label,
            'style' => $this->style,
            'size' => $this->size,
            'icon' => $this->icon,
            'attributes' => $this->prepareAttributes(),
        ]);
    }
}
