<?php
namespace app\widgets;

/**
 * Class Badge
 * @package app\widgets
 */
class Badge extends Widget
{
    const STYLE_DEFAULT = 'badge-default';
    const STYLE_PRIMARY = 'badge-primary';
    const STYLE_SUCCESS = 'badge-success';
    const STYLE_INFO = 'badge-info';
    const STYLE_WARNING = 'badge-warning';
    const STYLE_DANGER = 'badge-danger';
    const STYLE_DARK = 'badge-dark';

    /**
     * @var string
     */
    public $style = self::STYLE_DEFAULT;

    /**
     * @var string
     */
    public $size;

    /**
     * @var bool
     */
    public $round = false;


    /**
     * @var
     */
    public $label;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('badge', [
            'label' => $this->label,
            'style' => $this->style,
            'size' => $this->size,
            'round' => $this->round,
        ]);
    }
}
