<?php

use app\widgets\assets\ChangeableListAsset;

/**
 * @var \yii\web\View $this
 * @var string $label
 * @var string $template
 * @var string[] $items
 * @var string $id
 * @var string $class
 * @var string $callbackJsFuncAfterAddRow
 */

ChangeableListAsset::register($this);
?>

<div class="changeable-list <?= $class ?>" id="<?= $id ?>">
    <?php if ($label): ?>
        <div class="changeable-list-header row margin-bottom-10">
            <div class="col-lg-12">
                <?= $label ?>
            </div>
        </div>
    <?php endif; ?>

    <?php foreach ($items as $item): ?>
        <div class="changeable-list-element row">
            <?= $item ?>
            <button class="btn btn-danger btn-changeable-list delete-changeable-element" type="button"
                    data-class="<?= $class ?>">
                <i class="icon wb-minus"></i>
            </button>
        </div>
    <?php endforeach; ?>
    <div class="changeable-list-element changeable-list-template row">
        <?= $template ?>
        <button class="btn btn-success btn-changeable-list add-changeable-element" type="button"
                data-class="<?= $class ?>" data-callback-func="<?= $callbackJsFuncAfterAddRow ?>">
            <i class="icon wb-plus"></i>
        </button>
        <button class="btn btn-danger btn-changeable-list delete-changeable-element hidden" type="button"
                data-class="<?= $class ?>">
            <i class="icon wb-minus"></i>
        </button>
    </div>
</div>
