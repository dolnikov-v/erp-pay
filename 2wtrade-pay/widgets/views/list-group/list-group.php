<?php

/** @var string $header */
/** @var \app\widgets\ListGroupItem[] $items */
?>

<ul class="list-group list-group-bordered">
    <li class="list-group-item active"><?= $header ?></li>
    <?php foreach ($items as $item): ?>
        <?= $item->run() ?>
    <?php endforeach; ?>
</ul>
