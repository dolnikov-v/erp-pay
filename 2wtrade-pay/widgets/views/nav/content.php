<?php
/** @var array $tabs */
/** @var integer $activeTab */
?>

<div class="tab-content">
    <?php foreach ($tabs as $key => $tab): ?>
        <?php if (isset($tab['content'])):?>
            <div class="tab-pane <?php if ($activeTab == $key): ?>active<?php endif; ?>" id="tab<?= $key ?>"><?= $tab['content'] ?></div>
        <?php endif;?>
    <?php endforeach; ?>
</div>
