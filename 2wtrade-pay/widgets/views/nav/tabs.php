<?php
/** @var string $id */
/** @var string $typeNav */
/** @var string $typeTabs */
/** @var array $tabs */
/** @var integer $activeTab */
?>

<ul <?php if ($id): ?>id="<?= $id ?>"<?php endif; ?> class="nav <?= $typeNav ?> <?= $typeTabs ?>" data-plugin="nav-tabs">
    <?php foreach ($tabs as $key => $tab): ?>
        <?php if ($tab['visible'] ?? true): ?>
            <?php if (isset($tab['label'])): ?>
                <li class="<?php if ((!isset($tab['url']) && $activeTab == $key) || ($tab['active'] ?? false)): ?>active<?php endif; ?>">
                    <a <?php if(!isset($tab['url'])): ?>data-toggle="tab"<?php endif; ?> href="<?= $tab['url'] ?? "#tab$key" ?>">
                        <?php if (isset($tab['icon'])): ?>
                            <i class="icon <?= $tab['icon'] ?>"></i>
                        <?php endif; ?>
                        <?= $tab['label'] ?>
                    </a>
                </li>
            <?php endif; ?>
        <?php endif; ?>
    <?php endforeach; ?>
</ul>
