<?php
/** @var string $color */
/** @var string $size */
/** @var boolean $active */
/** @var integer $start */
/** @var string $attributes */
?>

<div class="progress <?= $size ?>" <?= $attributes ?>>
    <div class="progress-bar progress-bar-indicating <?= $color ?> <?php if ($active): ?>active<?php endif; ?>" role="progressbar" style="width: <?= $start ?>%;"></div>
</div>
