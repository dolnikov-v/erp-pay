<?php
/** @var string $id */
/** @var string $label */
/** @var string $style */
/** @var string $size */
/** @var string $icon */
/** @var string $attributes */
?>

<button <?php if ($id): ?>id="<?= $id ?>"<?php endif; ?>
        type="button"
        class="btn <?= $style ?> <?= $size ?> ladda-button" <?= $attributes ?>>
    <span class="ladda-label">
        <?php if ($icon): ?>
            <?= $icon ?>
        <?php endif; ?>

        <?= $label ?>
    </span>
    <span class="ladda-spinner"></span>
</button>
