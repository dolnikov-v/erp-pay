<?php
/** @var string $id */
/** @var string $name */
/** @var string $checked */
/** @var string $disabled */
/** @var string $attributes Атрибуты */
?>

<input <?php if ($id): ?>id="<?= $id ?>"<?php endif; ?>
       type="radio"
       <?php if ($name): ?>name="<?= $name ?>"<?php endif; ?>
       <?php if ($checked): ?>checked="checked"<?php endif; ?>
       <?php if ($disabled): ?>disabled="disabled"<?php endif; ?>
       <?= $attributes ?>/>
