<?php
/** @var string $label */
/** @var string $style */
/** @var string $size */
/** @var boolean $round */
/** @var boolean $outline */
/** @var string $title */
?>

<span class="label <?= $style ?> <?= $size ?> <?php if ($round): ?>label-round<?php endif; ?> <?php if ($outline): ?>label-outline<?php endif; ?>" <?php if ($title): ?>title="<?=$title?>" <?php endif; ?>>
    <?= $label ?>
</span>
