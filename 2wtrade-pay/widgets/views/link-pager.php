<?php
use yii\helpers\Url;

/**
 * @var int $pageSize текущее число строк в таблице
 * @var array $variants массив вариантов числа строк в таблице
 * @var string $paramName название параметра содержащегося в свойстве $pageSizeParam объекта класса yii\data\Pagination
 * @var string $widgetId Идентификатор виджета
 */
?>
<li class="btn-group dropup">
    <button type="button" class="btn btn-default dropdown-toggle btn-sm link-pager-selector" data-toggle="dropdown">
        <?= $pageSize ?>
        <span class="caret"></span>
    </button>
    <ul class="dropdown-menu drop-menu">
        <?php foreach ($variants as $i): ?>
            <li><a href="<?= Url::to(['', $paramName => $i, 'wid' => $widgetId]) ?>"><?= $i ?></a></li>
        <?php endforeach; ?>
    </ul>
</li>