<?php
/** @var array $links Тип кнопки */
/** @var boolean $arrow Cо стрелкой */
/** @var boolean $fixed */
?>

<?php if (!empty($links)): ?>
    <ol class="breadcrumb <?php if ($arrow): ?>breadcrumb-arrow<?php endif; ?> <?php if ($fixed): ?>breadcrumb-fixed<?php endif; ?>">
        <?php foreach ($links as $key => $link): ?>
            <li>
                <?php if (isset($link['url'])): ?>
                    <a href="<?= $link['url'] ?>">
                        <?php if (isset($link['icon']) && $link['icon']) : ?>
                            <i class="icon <?= $link['icon'] ?>"></i>
                        <?php endif; ?>

                        <?= $link['label'] ?>
                    </a>
                <?php else: ?>
                    <?= $link['label'] ?>
                <?php endif; ?>
            </li>
        <?php endforeach; ?>
    </ol>
<?php endif; ?>
