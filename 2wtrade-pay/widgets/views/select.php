<?php
use yii\helpers\ArrayHelper;

/** @var string $id */
/** @var string $name */
/** @var string|array $value */
/** @var string $prompt */
/** @var array $items */
/** @var boolean $multiple */
/** @var boolean $disabled */
/** @var string $placeholder */
/** @var string $style */
/** @var string $attributes */

if (is_array($value)) {
    $value = ArrayHelper::getColumn($value, 'item_name');
} else {
    $value = (array)$value;
}
?>

<select class="form-control <?= $style ?>"
        <?php if ($id): ?>id="<?= $id ?>"<?php endif; ?>
        <?php if ($name): ?>name="<?= $name ?>"<?php endif; ?>
        <?php if ($multiple): ?>multiple<?php endif; ?>
        <?php if ($disabled): ?>disabled<?php endif; ?>
        <?php if ($placeholder): ?>data-placeholder="<?= $placeholder ?>"<?php endif; ?>
        <?= $attributes ?>>

    <?php if ($prompt): ?>
        <option value="" <?= $value == '' ? 'selected' : '' ?>><?= $prompt ?></option>
    <?php endif; ?>

    <?php foreach ($items as $key => $item): ?>
        <option value="<?= $key ?>"
                <?php if (in_array($key, $value)): ?>selected="selected"<?php endif; ?>><?= $item ?></option>
    <?php endforeach; ?>
</select>
