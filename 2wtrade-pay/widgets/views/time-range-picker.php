<?php
use app\helpers\i18n\Moment;

/** @var string $offsetFrom */
/** @var string $offsetTo */
/** @var string $nameFrom */
/** @var string $nameTo */
/** @var boolean $disabled */
?>

<div class="container-time-range-picker">
    <div class="inputs-time-range-picker">
        <div class="input-group">
            <span class="input-group-addon">
                <i class="icon wb-time" aria-hidden="true"></i>
            </span>
            <input class="form-control input-timing" name="<?= $nameFrom ?>" type="text"
                   data-format="<?= Moment::getFullTimeFormat() ?>"
                   <?php if ($disabled): ?>disabled<?php endif; ?>
                   value="<?= $offsetFrom ?>"/>
        </div>
        <div class="input-group">
            <span class="input-group-addon">—</span>
            <input class="form-control input-timing" name="<?= $nameTo ?>" type="text"
                   data-format="<?= Moment::getFullTimeFormat() ?>"
                   <?php if ($disabled): ?>disabled<?php endif; ?>
                   value="<?= $offsetTo ?>"/>
        </div>
    </div>
</div>