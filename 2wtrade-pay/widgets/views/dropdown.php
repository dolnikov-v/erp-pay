<?php
/** @var string $type Тип кнопки */
/** @var string $label Текст кнопки */
/** @var string $style Основной класс */
/** @var string $styleDropdown Основной класс */
/** @var string $size Размер */
/** @var array $links Ссылки */
/** @var array $animate C анимацией */
/** @var array $right C права */
/** @var array $dropup Открытие вверх */
/** @var array $bullet */
?>

<div class="dropdown <?= $styleDropdown ?> <?php if ($right): ?>btn-group<?php endif; ?> <?php if ($dropup): ?>dropup<?php endif; ?>">
    <button type="<?= $type ?>"
            class="btn <?= $style ?> <?= $size ?> <?php if ($right): ?>pull-right<?php endif; ?> dropdown-toggle"
            data-toggle="dropdown">
        <?= $label ?>
        <span class="caret"></span>
    </button>
    <?php if (!empty($links)): ?>
        <ul class="dropdown-menu <?php if ($animate): ?>animate<?php endif; ?> <?php if ($right): ?>dropdown-menu-right<?php endif; ?> <?php if ($bullet): ?>bullet<?php endif; ?>">
            <?php foreach ($links as $link): ?>

                <?php if (is_array($link)): ?>

                    <?php if (isset($link['url'])): ?>
                        <?php $class = (isset($link['active'])) ? 'active' : '' ?>
                        <li class="<?= $class ?>">
                            <a href="<?= $link['url'] ?>">
                                <?php if (isset($link['icon'])): ?>
                                    <i class="icon <?= $link['icon'] ?>" aria-hidden="true"></i>
                                <?php endif; ?>
                                <?= $link['label'] ?>
                            </a>
                        </li>
                    <?php else: ?>
                        <li class="dropdown-header"><?= $link['label'] ?></li>
                    <?php endif; ?>

                <?php else: ?>
                    <li class="divider"></li>
                <?php endif; ?>

            <?php endforeach; ?>
        </ul>
    <?php endif; ?>
</div>
