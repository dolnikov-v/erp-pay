<?php

use app\widgets\assets\custom\ModalConfirmLinkAsset;
use app\widgets\custom\ModalConfirmLink;
use app\widgets\Modal;
use app\widgets\ButtonLink;

/** @var string $id ID */
/** @var string $icon Иконка */
/** @var string $label Текст кнопки */
/** @var string $url Сылка */
/** @var string $style Основной класс */
/** @var string $size Размер */
/** @var boolean $isBlock btn-block */
/** @var string $attributes Атрибуты */
/** @var string $title Заголовок */
/** @var string $confirm Текст */
/** @var string $modalColor */
/** @var string $modalButtonStyle */

ModalConfirmLinkAsset::register($this);
?>
    <a <?php if ($id): ?>id="<?= $id ?>"<?php endif; ?>
       href="#"
       data-href="<?= $url ?>"
       class="btn confirm-data-link <?= $style ?> <?= $size ?> <?php if ($isBlock): ?>btn-block<?php endif; ?>" <?= $attributes ?>>
        <?= $icon ?> <?= $label ?>
    </a>

<?= ModalConfirmLink::widget([
    'title' => $title,
    'confirm' => $confirm,
    'color' => $modalColor,
    'buttonStyle' => $modalButtonStyle
]) ?>