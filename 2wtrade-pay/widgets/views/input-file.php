<?php
/** @var string $name */
/** @var bool $disabled */
/** @var string $size */
/** @var bool $focus */
/** @var bool $hook */
/** @var string $id */
/** @var string $accept */
/** @var string $attributes */
/** @var string $multiple */
?>

<input class="form-control <?= $size ?> <?php if ($focus): ?>focus<?php endif; ?>"
       <?php if ($id): ?>id="<?= $id ?>"<?php endif; ?>
       <?php if ($name): ?>name="<?= $name ?>"<?php endif; ?>
       <?php if ($disabled): ?>disabled="disabled"<?php endif; ?>
       <?php if ($multiple): ?>multiple<?php endif; ?>
       <?php if ($accept): ?>accept="<?= $accept ?>"<?php endif; ?>
       <?php if ($hook): ?>autocomplete="off" readonly onfocus="this.removeAttribute('readonly')" <?php endif; ?>
    <?= $attributes ?>
       type="file">
