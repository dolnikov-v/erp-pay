<?php
use app\widgets\assets\CountryStorageAsset;
use app\widgets\assets\Select2Asset;

/** @var app\widgets\Select2 $countryCollection */
/** @var app\widgets\Select2 $storageCollection */

CountryStorageAsset::register($this);
Select2Asset::register($this);
?>
<div class="container-countrystorage" data-plugin="countrystorage">
    <div class="range-collection country">
        <div class="labels">
            <label><?=Yii::t('common', 'Страна')?></label>
        </div>
        <?= $countryCollection->run() ?>
    </div>
    <div class="padding">&nbsp;</div>
    <div class="range-collection storage">
        <div class="labels">
            <label><?=Yii::t('common', 'Склад')?></label>
        </div>
        <?= $storageCollection->run() ?>
    </div>
</div>
