<?php
use app\widgets\assets\CountryDeliveryAsset;
use app\widgets\assets\Select2Asset;

/** @var app\widgets\Select2 $countryCollection */
/** @var app\widgets\Select2 $deliveryCollection */

CountryDeliveryAsset::register($this);
Select2Asset::register($this);
?>
<div class="container-countrydelivery" data-plugin="countrydelivery">
    <div class="range-collection country">
        <div class="labels">
            <label><?=Yii::t('common', 'Страна')?></label>
        </div>
        <?= $countryCollection->run() ?>
    </div>
    <div class="padding">&nbsp;</div>
    <div class="range-collection delivery">
        <div class="labels">
            <label><?=Yii::t('common', 'Служба доставки')?></label>
        </div>
        <?= $deliveryCollection->run() ?>
    </div>
</div>
