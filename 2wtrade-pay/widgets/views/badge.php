<?php
/** @var string $label */
/** @var string $style */
/** @var string $size */
/** @var boolean $round */
?>

<span class="badge <?= $style ?> <?= $size ?> <?php if ($round): ?>label-round<?php endif; ?>">
    <?= $label ?>
</span>
