<?php
use app\widgets\assets\Select2Asset;

/** @var string $id */
/** @var string $name */
/** @var string|array $value */
/** @var string $prompt */
/** @var array $items */
/** @var boolean $multiple */
/** @var boolean $disabled */
/** @var string $placeholder */
/** @var string $style */
/** @var integer $length */
/** @var boolean $autoEllipsis */
/** @var array $disabledItems */
/** @var array $defaultValue */

Select2Asset::register($this);

if (!is_array($disabledItems)) {
    $disabledItems = (array) $disabledItems;
}

if (!is_array($value)) {
    $value = (array)$value;
}
?>
<div class="<?= $style ?>">
    <select class="form-control <?php if ($autoEllipsis): ?>select2-auto-ellipsis<?php endif; ?>"
            <?php if ($id): ?>id="<?= $id ?>"<?php endif; ?>
            <?php if ($name): ?>name="<?= $name ?>"<?php endif; ?>
            <?php if ($multiple): ?>multiple<?php endif; ?>
            <?php if ($disabled): ?>disabled<?php endif; ?>
            <?php if ($placeholder): ?>data-placeholder="<?= $placeholder ?>"<?php endif; ?>
            <?php if ($length): ?>data-minimum-results-for-search="<?= $length ?>"<?php endif; ?>
            <?php if ($tags): ?>data-tags="true"  data-token-separators=","<?php endif; ?>
            data-plugin="select2">

        <?php if ($prompt): ?>
            <option value="<?=$defaultValue?>" <?= $value == $defaultValue ? 'selected' : '' ?>><?= $prompt ?></option>
        <?php endif; ?>

        <?php if ($items):?>
            <?php foreach ($items as $key => $item): ?>
                <option value="<?= $key ?>"
                        <?php if (in_array($key, $disabledItems)): ?>disabled="disabled"<?php endif; ?>
                        <?php if (in_array($key, $value)): ?>selected="selected"<?php endif; ?>><?= $item ?></option>
            <?php endforeach; ?>
        <?php endif; ?>
    </select>
</div>
