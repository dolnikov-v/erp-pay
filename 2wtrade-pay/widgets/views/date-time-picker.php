<?php
use app\widgets\assets\DateTimePickerAsset;

/** @var string $widget */

DateTimePickerAsset::register($this);
?>

<?= $widget ?>
