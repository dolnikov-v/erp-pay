<?php

use app\components\widgets\PanelList;
use app\widgets\Panel;
use app\widgets\Label;
use app\helpers\logs\Route;
use app\models\logs\TableLog;
use app\components\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var array $templates
 * @var string $title
 * @var TableLog[] $data
 * @var bool $showLinks
 * @var bool $showData
 * @var string $groupBy
 */

$content = [
    [
        'label' => 'Информация',
        'content' => function ($model) use ($templates) {
            /**
             * @var TableLog $model
             */
            $return = [];

            if ($model->user->id) {
                $return[] = ($model->attributeLabels()['user_id'] ?? Yii::t('common', 'user_id')) . ': ' .
                    Html::a($model->user->username, Url::toRoute(['/access/user/view', 'id' => $model->user->id]));
            }

            $route = Route::getRouteGroup($model->route);
            if ($route) {
                $style = Label::STYLE_PRIMARY;
                if ($route == Route::GROUP_CALL_CENTER) {
                    $style = Label::STYLE_INFO;
                } elseif ($route == Route::GROUP_DELIVERY) {
                    $style = Label::STYLE_SUCCESS;
                }
                $return[] = ($model->attributeLabels()['route'] ?? Yii::t('common', 'route')) . ': ' .
                    Label::widget([
                        'label' => $route != "" ? Route::getGroupDescription($route) : Yii::t('common', 'Неопределено'),
                        'style' => $route != "" ? $style : Label::STYLE_DEFAULT
                    ]);
            }

            if ($model->type) {
                $return[] = ($model->attributeLabels()['created_at'] ?? Yii::t('common', 'created_at')) . ': ' .
                    Label::widget(['label' => Yii::$app->formatter->asFullDatetime($model->created_at)]);
                if (!in_array('type', $templates)) {
                    $return[] = Yii::t('common', 'Тип записи') . ': ' . Label::widget([
                            'label' => $model->type,
                            'style' => (
                            $model->type == TableLog::TYPE_INSERT ? Label::STYLE_SUCCESS :
                                ($model->type == TableLog::TYPE_UPDATE ? Label::STYLE_INFO :
                                    ($model->type == TableLog::TYPE_DELETE ? Label::STYLE_DANGER : ''))
                            )
                        ]);
                }
            }


            return implode('<br/>', $return);
        }
    ],
];

if ($showLinks) {
    $content[] = [
        'attribute' => 'links',
        'content' => function ($model) {
            /**
             * @var TableLog $model
             */
            return $model->linkContent;
        }
    ];
}

if ($showData) {
    $content[] = [
        'attribute' => 'data',
        'content' => function ($model) {
            /**
             * @var TableLog $model
             */
            $return = '';
            $model->prepare();
            if (!empty($model->data) && is_array($model->data)) {
                $provider = new \yii\data\ArrayDataProvider([
                    'allModels' => $model->data,
                ]);
                $return = GridView::widget([
                    'tableOptions' => [
                        'style' => 'padding-top: 0;',
                    ],
                    'dataProvider' => $provider,
                ]);
            }
            return $return;
        }
    ];
}
?>

<style>
    tr > td:first-child {
        width: 300px;
    }
</style>
<?= Panel::widget([
    'title' => $title ?? Yii::t('common', 'Детальный просмотр'),
    'content' => PanelList::widget([
        'data' => $data,
        'groupBy' => $groupBy,
        'titleTemplates' => $templates,
        'titleContent' => $titleContent,
        'content' => $content,
    ])
]); ?>
