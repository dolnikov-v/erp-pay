<?php
/** @var app\widgets\Button[] $buttons */
/** @var string $orientation */
?>

<?php if ($buttons): ?>
    <div class="<?= $orientation ?>" role="group">
        <?php foreach ($buttons as $button): ?>
            <?php if ($button instanceof app\widgets\Button): ?>
                <?= $button->run() ?>
            <?php endif; ?>
        <?php endforeach; ?>
    </div>
<?php endif; ?>
