<?php
use app\widgets\assets\StorageProductAsset;
use app\widgets\assets\Select2Asset;

/** @var app\widgets\Select2 $storageCollection */
/** @var app\widgets\Select2 $productCollection */
/** @var app\widgets\Select2 $productShelfLife */

StorageProductAsset::register($this);
Select2Asset::register($this);
?>
<div class="container-storageproduct" data-plugin="storageproduct">
    <div class="range-collection storage">
        <div class="labels">
            <label><?=Yii::t('common', 'Склад')?></label>
        </div>
        <?= $storageCollection->run() ?>
    </div>
    <div class="padding">&nbsp;</div>
    <div class="range-collection product">
        <div class="labels">
            <label><?=Yii::t('common', 'Товар')?></label>
        </div>
        <?= $productCollection->run() ?>
    </div>
    <div class="padding">&nbsp;</div>
    <div class="range-collection productShelfLife">
        <div class="labels">
            <label><?=Yii::t('common', 'Срок годности')?></label>
        </div>
        <?= $productShelfLife->run() ?>
    </div>
</div>