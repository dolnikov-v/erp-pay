<?php
/** @var string $id ID */
/** @var string $type Тип кнопки */
/** @var string $value Значение кнопки */
/** @var string $icon Иконка */
/** @var string $label Текст кнопки */
/** @var string $style Основной класс */
/** @var string $size Размер */
/** @var string $attributes Атрибуты */
?>

<button <?php if ($id): ?>id="<?= $id ?>"<?php endif; ?>
        class="btn <?= $style ?> <?= $size ?>"
        type="<?= $type ?>"
        <?php if ($value): ?>value="<?= $value ?>"<?php endif; ?>
        <?= $attributes ?>>
    <?php if ($icon): ?>
        <i class="icon <?= $icon ?>"></i>
    <?php endif; ?>
    <?= $label ?>
</button>
