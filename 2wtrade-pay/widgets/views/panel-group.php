<?php 
/** @var \app\widgets\PanelGroup[] $panels */
?>
<div class="panel-group panel-group-continuous" id="panelGroup" aria-multiselectable="false"
     role="tablist">
    <?php foreach ($panels as $panel) : ?>
        <?= $panel->run(); ?>
    <?php endforeach; ?>
</div>
