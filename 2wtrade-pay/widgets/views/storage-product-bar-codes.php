<?php
use app\widgets\assets\StorageProductBarCodesAsset;
use app\widgets\assets\Select2Asset;
use yii\helpers\Html;

/** @var app\widgets\Select2 $storageCollection */
/** @var app\widgets\Select2 $productCollection */
/** @var app\widgets\Select2 $productShelfLife */
/** @var array $storageDocumentsUseBarListsCollection */
/** @var string $nameQuantity */
/** @var string $nameBarList */

StorageProductBarCodesAsset::register($this);
Select2Asset::register($this);
?>
<div class="container-storageproductbarcodes" data-plugin="storageproductbarcodes">
    <div class="range-collection storage">
        <div class="labels">
            <label><?=Yii::t('common', 'Склад')?></label>
        </div>
        <?= $storageCollection->run() ?>
    </div>
    <div class="padding">&nbsp;</div>
    <div class="range-collection product">
        <div class="labels">
            <label><?=Yii::t('common', 'Товар')?></label>
        </div>
        <?= $productCollection->run() ?>
    </div>
    <div class="padding">&nbsp;</div>
    <div class="range-collection productShelfLife">
        <div class="labels">
            <label><?=Yii::t('common', 'Срок годности')?></label>
        </div>
        <?= $productShelfLife->run() ?>
    </div>
    <div class="padding">&nbsp;</div>
    <div class="range-collection quantity">
        <div class="labels">
            <label><?=Yii::t('common', 'Количество')?></label>
        </div>
        <?= Html::textInput($nameQuantity, null, ['class' => 'form-control']) ?>
    </div>
    <div class="range-collection bar-list">
        <?= Html::checkboxList($nameBarList, null, $storageDocumentsUseBarListsCollection, ['class' => 'checkbox']) ?>
    </div>
</div>