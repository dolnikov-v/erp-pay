<?php
use app\modules\media\components\Image;
use app\widgets\Button;
use kartik\file\FileInput;
use yii\web\View;

/** @var string $image */
/** @var string $fileName */
/** @var string $linkId */
/** @var boolean $circleImage */
/** @var boolean $aspectRatio */
/** @var string $nameInput */
/** @var string $urlUpload */
/** @var string $urlCropSave */
/** @var string $cropData */
?>
<?php
$this->registerJs("Cropper.options['nameInput'] = '" . $nameInput . "';", View::POS_END);
$this->registerJs("Cropper.options['cropData'] = '" . $cropData . "';", View::POS_END);
$this->registerJs("Cropper.options['linkId'] = '" . $linkId . "';", View::POS_END);
$this->registerJs("Cropper.options['cropType'] = '" . Image::TYPE_CROP_USER_AVATAR . "';", View::POS_END);
$this->registerJs("Cropper.options['currentFileName'] = '" . $fileName . "';", View::POS_END);
$this->registerJs("Cropper.options['successNotify'] = '" . Yii::t('common', 'Фотография успешно изменена.') . "';", View::POS_END);
$this->registerJs("Cropper.options['failNotify'] = '" . Yii::t('common', 'Не удалось изменить фотографию.') . "';", View::POS_END);
$this->registerJs("Cropper.options['aspectRatio'] = '" . $aspectRatio . "';", View::POS_END);
?>

<div class="row cropper-panel">
    <div class="col-xs-8">
        <div class="wrap-cropper-img">
            <img class="img-responsive" id="cropper_image" src="<?= $image ?>" alt="Picture">
        </div>
    </div>
    <div class="col-xs-4">
        <div class="row margin-top-30">
            <div class="col-xs-12">
                <div class="preview preview_big"></div>
            </div>
        </div>
        <div class="row margin-top-30">
            <div class="col-xs-8">
                <div class="preview preview_small"></div>
            </div>
        </div>
    </div>
</div>
<div class="row margin-top-20 cropper-actions" data-upload-url="<?= $urlUpload ?>" data-crop-save="<?= $urlCropSave ?>">
    <div class="col-lg-4">
        <?= FileInput::widget([
            'id' => 'upload_media',
            'name' => $nameInput,
            'options' => [
                'accept' => 'image/png, image/jpg, image/jpeg'
            ],
            'pluginOptions' => [
                'showPreview' => false,
                'showCaption' => false,
                'showUpload' => false,
                'showRemove' => false,
                'showUploadedThumbs' => false,
                'browseClass' => 'btn btn-sm btn-primary btn-block',
                'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ' . Yii::t('common', 'Загрузить фото'),
                'browseLabel' => '',
                'overwriteInitial' => false,
            ]
        ]);
        ?>
    </div>
    <div class="col-lg-4">
        <?= Button::widget([
            'id' => 'crop-data-save',
            'label' => '<i class="glyphicon glyphicon-ok"></i> ' . Yii::t('common', 'Сохранить'),
            'style' => 'btn-default  btn-sm myClass jcrop-crop width-full',
            'attributes' => [
                'title' => Yii::t('common', 'Сохранить область'),
            ],
        ]) ?>
    </div>
    <div class="col-lg-4">
        <?= Button::widget([
            'id' => 'crop-data-cancel',
            'label' => '<i class="glyphicon glyphicon-remove"></i> ' . Yii::t('common', 'Отмена'),
            'style' => 'btn-default btn-sm myClass jcrop-cancel width-full',
            'attributes' => [
                'title' => Yii::t('common', 'Выбрать область'),
            ],
        ]) ?>
    </div>
</div>
<?php
if ($circleImage) {
    $this->registerCss(
        ".cropper-panel .preview {
            -webkit-border-radius: 100%;
            -moz-border-radius: 100%;
            border-radius: 100%;
            }
            
            .cropper-view-box,
            .cropper-face {
            border-radius: 100%;
            }"
    );
}

?>




