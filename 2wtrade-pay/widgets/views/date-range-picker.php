<?php
use app\widgets\assets\DateTimePickerAsset;
use app\widgets\assets\Select2Asset;

/** @var string $locale */
/** @var string $format */
/** @var app\widgets\InputText $inputFrom */
/** @var app\widgets\InputText $inputTo */
/** @var app\widgets\Select2 $rangeCollection */

DateTimePickerAsset::register($this);
Select2Asset::register($this);
?>

<div class="container-daterangepicker" data-plugin="daterangepicker" data-locale="<?= $locale ?>"
     data-format="<?= $format ?>">
    <div class="inputs-daterangepicker" style="<?php if (!$rangeCollection): ?>width: auto;<?php endif; ?>">
        <div class="input-group">
            <span class="input-group-addon">
                <i class="icon wb-calendar" aria-hidden="true"></i>
            </span>
            <?= $inputFrom->run() ?>
        </div>
        <div class="input-group">
            <span class="input-group-addon">—</span>
            <?= $inputTo->run() ?>
        </div>
    </div>

    <?php if ($rangeCollection): ?>
        <div class="range-collection-daterangepicker">
            <?= $rangeCollection->run() ?>
        </div>
    <?php endif; ?>
</div>
