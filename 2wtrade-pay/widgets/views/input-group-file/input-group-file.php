<?php
use app\widgets\assets\InputGroupFileAsset;

/** @var string $input */
/** @var boolean $right */

InputGroupFileAsset::register($this);

?>

<div class="input-group input-group-file">
    <?= !$right ? $this->render('input-group-btn', ['input' => $input]) : '' ?>

    <input class="form-control" type="text" disabled="disabled" placeholder="<?= Yii::t('common', 'Файл не выбран.') ?>"/>

    <?= $right ? $this->render('input-group-btn', ['input' => $input]) : '' ?>
</div>
