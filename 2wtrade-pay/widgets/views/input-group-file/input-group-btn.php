<?php
/** @var string $input */
?>

<span class="input-group-btn">
    <span class="btn btn-primary btn-file">
        <i class="icon wb-upload" aria-hidden="true"></i>
        <?= $input ?>
    </span>
</span>
