<?php

use app\widgets\assets\MaterialAsset;

/** @var string $label */
/** @var string $style */
/** @var string $subClass */
/** @var bool|string $hint */
/** @var bool $material */
/** @var bool $floating */
/** @var bool $clear */

/** @var string $widget */

if ($floating) {
    MaterialAsset::register($this);
}
?>

<div class="form-group <?= $subClass ?> <?php if ($material): ?>form-material<?php endif; ?> <?php if ($floating): ?>floating<?php endif; ?> <?= $style ?>">
    <?php if ($label && !$floating): ?>
        <label class="control-label"><?= $label ?></label>
    <?php endif; ?>

    <?= $widget ?>

    <?php if ($label && $floating): ?>
        <label class="floating-label"><?= $label ?></label>
    <?php endif; ?>

    <?php if ($hint): ?>
        <div class="hint"><?= $hint ?></div>
    <?php endif; ?>
</div>
