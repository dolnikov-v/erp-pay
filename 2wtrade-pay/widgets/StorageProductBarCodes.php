<?php
namespace app\widgets;

use Yii;
use app\modules\storage\models\StorageDocument;
use app\modules\storage\models\StorageProduct as StorageProductModel;
use app\modules\storage\models\Storage;
use app\modules\bar\models\Bar;

/**
 * Class StorageProductBarCodes
 * @package app\widgets
 */
class StorageProductBarCodes extends Widget
{
    /**
     * @var string
     */
    public $nameStorage;

    /**
     * @var string
     */
    public $nameProduct;

    /**
     * @var string
     */
    public $nameQuantity;

    /**
     * @var string
     */
    public $nameBarList;

    /**
     * @var integer
     */
    public $storage = null;

    /**
     * @var integer
     */
    public $product = null;

    /**
     * @var array
     */
    public $storages = [];

    /**
     * @var array
     */
    public $products = [];

    /**
     * @var boolean
     */
    public $productsDisabled = false;

    /**
     * @var boolean
     */
    public $nameShelfLife = false;

    /**
     * @var array
     */
    public $storageDocumentsUseBarLists;

    /**
     * @var integer
     */
    public $quantity;

    /**
     * @return string
     * @throws \Exception
     */
    public function run()
    {
        $this->storages = Storage::find()
            ->byCountryId(Yii::$app->user->country->id)
            ->orderBy('name')
            ->collection();

        if ($this->storage != null) {
            $productsAll = StorageProductModel::find()
                ->joinWith(['product'])
                ->byStorageId($this->storage)
                ->orderBy('name')
                ->all();

            $result = null;
            if (count($productsAll) > 0) {
                foreach ($productsAll as $productOne) {
                    $result[$productOne->product->id] = $productOne->product->name;
                }
                $this->products = $result;
            }
        }

        if (count($this->products) == 0) {
            $this->productsDisabled = true;
            $this->product = null;
        }

        if ($this->product != null) {
            $subQuery = Bar::find()
                ->select([
                    'id' => Bar::tableName() . '.id',
                ])
                ->where([Bar::tableName() . '.bar_list_id' => StorageDocument::tableName() . '.bar_list_id'])
                ->andWhere(['!=', Bar::tableName() . '.status', Bar::CREATED])
                ->limit(1);

            $storageDocumentsUseBarListsArray = StorageDocument::find()
                ->where(['storage_id_to' => $this->storage])
                ->andWhere(['product_id' => $this->product])
                ->andWhere(['is not', 'bar_list_id', null])
                ->andWhere(['not exists', $subQuery])
                ->asArray()
                ->all();

            foreach ($storageDocumentsUseBarListsArray as $storageDocumentsUseBarListsOne) {
                $this->storageDocumentsUseBarLists[$storageDocumentsUseBarListsOne['bar_list_id']] = $storageDocumentsUseBarListsOne['quantity'];
            }
        }

        if (count($this->storageDocumentsUseBarLists) == 0) {
            $this->storageDocumentsUseBarLists = [];
        }

        return $this->render('storage-product-bar-codes', [
            'storageCollection' => new Select2([
                'items' => $this->storages,
                'prompt' => '—',
                'length' => false,
                'name' => $this->nameStorage,
                'value' => $this->storage,
            ]),
            'productCollection' => new Select2([
                'items' => $this->products,
                'prompt' => '—',
                'length' => false,
                'name' => $this->nameProduct,
                'disabled' => $this->productsDisabled,
                'value' => $this->product,
            ]),
            'productShelfLife' => new Select2([
                'items' => [],
                'prompt' => '—',
                'length' => false,
                'name' => $this->nameShelfLife,
                'disabled' => true,
                'value' => null,
                'defaultValue' => 1
            ]),
            'nameQuantity' => $this->nameQuantity,
            'nameBarList' => $this->nameBarList,
            'storageDocumentsUseBarListsCollection' => $this->storageDocumentsUseBarLists,
        ]);
    }
}