<?php
namespace app\widgets\navbar;

use app\helpers\i18n\Moment;
use Yii;
use yii\base\Widget;

/**
 * Class Clock
 * @package app\widgets\navbar
 */
class Clock extends Widget
{
    /**
     * @var string
     */
    public $time;

    /**
     * @var string
     */
    public $timeFormat;

    /**
     * @var string
     */
    public $date;

    /**
     * @return string
     */
    public function run()
    {
        $this->time = $this->time ? $this->time : Yii::$app->formatter->asFullTime(time());
        $this->timeFormat = $this->timeFormat ? $this->timeFormat : Moment::getTimeFormat();

        $this->date = $this->date ? $this->date : Yii::$app->formatter->asDate(time());

        return $this->render('clock', [
            'time' => $this->time,
            'timeFormat' => $this->timeFormat,
            'date' => $this->date,
        ]);
    }
}
