<?php
namespace app\widgets\navbar;

use app\modules\i18n\models\Language;
use app\widgets\Dropdown;
use Yii;
use yii\helpers\Url;

/**
 * Class LanguageSwitcher
 * @package app\widgets\navbar
 */
class LanguageSwitcher extends Dropdown
{
    private $languages;

    /**
     * @var null|\app\modules\i18n\models\Language
     */
    private $current = null;

    /**
     * @return string
     */
    public function run()
    {
        $this->current = Yii::$app->user->getLanguage();

        $this->languages = Language::find()->active()->all();

        foreach ($this->languages as $language) {
            $this->links[] = [
                'url' => Url::toRoute(['/i18n/language/change', 'locale' => $language->locale]),
                'label' => $language->name,
                'icon' => $language->icon,
            ];
        }

        return $this->render('language-switcher', [
            'label' => $this->current->icon,
            'links' => $this->links,
        ]);
    }
}
