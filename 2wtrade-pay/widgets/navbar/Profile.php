<?php
namespace app\widgets\navbar;

use app\widgets\Dropdown;

/**
 * Class Profile
 * @package app\widgets\navbar
 */
class Profile extends Dropdown
{
    /**
     * @var \app\components\web\User Пользователь
     */
    public $user;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('profile', [
            'type' => $this->type,
            'label' => $this->label,
            'style' => $this->style,
            'size' => $this->size,
            'links' => $this->links,
            'animate' => $this->animate,
            'identity' => $this->user->getIdentity(),
        ]);
    }
}
