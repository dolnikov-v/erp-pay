<?php
namespace app\widgets\navbar;

use app\models\Country;
use app\widgets\Dropdown;
use Yii;

/**
 * Class CountrySwitcher
 * @package app\widgets\navbar
 */
class CountrySwitcher extends Dropdown
{
    /**
     * @return string
     */
    public function run()
    {
        $current = Yii::$app->user->country;

        if (Yii::$app->user->isSuperadmin) {
            $countries = Country::find()->active()->all();
        } else {
            $countries = Yii::$app->user->identity->countries;
        }

        foreach ($countries as &$country) {
            $country->name = Yii::t('common', $country->name);
            unset($country);
        }

        usort($countries, function ($a, $b) {
            if ($a->name == $b->name) {
                return 0;
            }

            return $a->name > $b->name ? 1 : -1;
        });

        foreach ($countries as $country) {
            $this->links[] = [
                'url' => '/' . $country->slug . Yii::$app->request->url,
                'label' => $country->name,
            ];
        }

        return $this->render('country-switcher', [
            'label' => $current->name,
            'links' => $this->links,
        ]);
    }
}
