<?php
//use miloschuman\highcharts\Highcharts;
use app\widgets\navbar\assets\EfficiencyAsset;
use app\helpers\WbIcon;
use app\models\User;

/** @var \yii\web\View $this */
/** @var array $imgsData */

EfficiencyAsset::register($this);
?>

<?php if (is_array($imgsData)) : ?>
    <div class="efficiency">
        <?php foreach ($imgsData as $imgsDataItem) : ?>
            <div>
                <div class="wrap" style="background-image: url(<?= User::getAvatarUrlByUserId($imgsDataItem['leader_id']) ?>)">
                    <div class="inner" style="height: <?= round($imgsDataItem['efficiency']) ?>%"></div>
                </div>
                <div class="percent_back">
                    <span class="percent"><?= Yii::$app->formatter->asDecimal($imgsDataItem['efficiency'], 2) ?> %</span>
                </div>
                <div class="text"><?=$imgsDataItem['name']?></div>
            </div>
        <?php endforeach ?>
    </div>
    <span id="efficiency_detail" class="padding-left-10">
        <br><br>
        <i class="icon <?= WbIcon::MORE_HORIZONTAL ?>" aria-hidden="true"></i>
    </span>
<?php endif; ?>
<?php
/*echo Highcharts::widget([
            'scripts' => ['highcharts-more'],
            'options' => [
                'chart' => [
                    'type' => 'bubble',
                    'height' => 66,
                    'spacing' => [5, 0, 3, 0],
                ],
                'colors' => ['#aaa'],
                'title' => ['text' => ''],
                'credits' => [
                    'enabled' => false,
                ],
                'legend' => [
                    'enabled' => false,
                ],
                'xAxis' => [
                    'labels' => ['enabled' => false],
                    'visible' => false,
                ],
                'yAxis' => [
                    'startOnTick' => false,
                    'endOnTick' => false,
                    'labels' => ['enabled' => false],
                    'visible' => false,
                ],
                'tooltip' => ['enabled' => false],
                'series' => $series
            ],
        ]);*/
?>
