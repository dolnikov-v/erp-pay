<?php
/** @var string $label Иконка */
/** @var array $links Ссылки */
?>

<?php if ($links): ?>
    <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            <span class="flag-icon <?= $label ?>"></span>
        </a>
        <ul class="dropdown-menu" role="menu">
            <?php foreach ($links as $link): ?>
                <li role="presentation">
                    <a href="<?= $link['url'] ?>" role="menuitem"><span
                            class="flag-icon <?= $link['icon'] ?>"></span> <?= $link['label'] ?></a>
                </li>
            <?php endforeach; ?>
        </ul>
    </li>
<?php endif; ?>
