<?php
use app\widgets\navbar\assets\CountrySwitcherAsset;

/** @var string $label Текущая страна */
/** @var array $links Ссылки */

CountrySwitcherAsset::register($this);
?>

<?php if ($links): ?>
    <ul class="nav navbar-toolbar navbar-right">
        <li id="country_switcher_dropdown" class="dropdown">
            <a id="country_switcher_menu" class="dropdown-toggle" data-toggle="dropdown" href="#">
                <?= Yii::t('common', $label) ?>
                <?php if (count($links) > 1): ?><span class="caret"></span><?php endif; ?>
            </a>
            <?php if (count($links) > 1): ?>
                <ul id="country_switcher_dropdown_menu" class="dropdown-menu dropdown-menu-right dropdown-menu-media" role="menu">
                    <li class="dropdown-menu-header" role="presentation">
                        <h5><?= Yii::t('common', 'Доступные страны') ?></h5>
                        <span class="label label-round label-success"><?= count($links) ?></span>
                    </li>

                    <li id="country_switcher_searcher" class="search">
                        <div class="input-search input-group-sm">
                            <button class="input-search-btn" type="submit">
                                <i class="icon wb-search"></i>
                            </button>
                            <input id="country_switcher_searcher_input" class="form-control" type="text"/>
                        </div>
                    </li>

                    <li class="list-group" role="presentation">
                        <div data-role="container">
                            <div data-role="content">
                                <?php foreach ($links as $link): ?>
                                    <a class="list-group-item" href="<?= $link['url'] ?>" role="menuitem" data-name="<?= $link['label'] ?>">
                                        <div class="media">
                                            <div class="media-body">
                                                <h6 class="media-heading"><?= $link['label'] ?></h6>
                                            </div>
                                        </div>
                                    </a>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </li>
                </ul>
            <?php endif; ?>
        </li>
    </ul>
<?php endif; ?>
