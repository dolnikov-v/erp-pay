<?php
/** @var integer $countUnread */

use app\widgets\navbar\assets\NotificationAsset;
use yii\helpers\Html;
use yii\helpers\Url;

NotificationAsset::register($this);
?>

<li class="dropdown notification-widget">
    <a class="dropdown-toggle show-notification-profile"
       data-toggle="dropdown" href="#"
       data-url-get-notifications="<?= Url::toRoute('/profile/notification/get-notifications') ?>"
       data-url-read="<?= Url::toRoute('/profile/notification/read') ?>"
       role="button">
        <i class="icon wb-bell"></i>
        <span class="badge label-count badge-danger up count-notifications"><?= $countUnread ? $countUnread : '' ?></span>
    </a>
    <ul class="dropdown-menu dropdown-menu-right dropdown-menu-media dropdown-menu-notifications" role="menu">
        <li class="dropdown-menu-header" role="presentation">
            <h5 class="text-uppercase"><?= Yii::t('common', 'Оповещения') ?></h5>
            <span class="label label-round label-count label-default">
                <?= Yii::t('common', 'Новых') ?> <span class="count-notifications-inner"><?= $countUnread ?></span>
            </span>
        </li>
        <li class="list-group" role="presentation">
            <div class="container-notify" data-role="container">
                <div class="content-notify" data-role="content">

                </div>
            </div>
        </li>
        <li class="dropdown-menu-footer" role="presentation">
            <?= Html::a('<i class="icon wb-settings" aria-hidden="true"></i>', ['/profile/view', '#' => 'tab1'], ['class' => 'dropdown-menu-footer-btn']) ?>
            <?= Html::a(Yii::t('common', 'Все оповещения'), ['/profile/notification']) ?>
        </li>
    </ul>

    <script id="template_loader_notifications" type="text/x-handlebars-template">
        <div class="loader-notifications">
            <div class="loader loader-default"></div>
        </div>
    </script>
    <script id="template_empty_notifications" type="text/x-handlebars-template">
        <div class="empty-notifications">
            <?= Yii::t('common', 'Нет новых оповещений') ?>
        </div>
    </script>
    <script id="template_notification" type="text/x-handlebars-template">
        <a class="list-group-item widget-notification-item"
           href="javascript:void(0)"
           data-url="<?= Url::toRoute('/profile/notification/read') ?>"
           data-id="{{id}}"
           role="menuitem">
            <div class="media" data-toggle="tooltip" data-placement="left" title="" rel="nofollow"
                 data-original-title="Подсказка слева">
                <div class="media-left padding-right-10">
                    <i class="icon wb-info {{cssClass}} white icon-circle" aria-hidden="true"></i>
                </div>
                <div class="media-body">
                    <h6 class="media-heading margin-top-5">{{description}}</h6>
                    <p class="media-meta">{{country}}</p>
                </div>
            </div>
        </a>
    </script>
</li>
