<?php
/** @var \app\models\User $identity */
?>

<li class="dropdown">
    <a class="navbar-avatar dropdown-toggle"
       data-toggle="dropdown" href="#"
       data-animation="scale-up"
       role="button">
        <span class="avatar avatar-online">
            <img src="<?= $identity->getAvatarUrl(false) ?>" alt="img">
            <i></i>
        </span>
    </a>

    <?php if (!empty($links)): ?>
        <ul class="dropdown-menu" role="menu">
            <li role="presentation" class="text-center font-size-16">
                <?= $identity->username ?>
            </li>
            <li role="presentation" class="text-center font-size-12 text-muted">
                <?= $identity->getRolesNames() ?>
            </li>

            <li class="divider" role="presentation"></li>

            <?php foreach ($links as $link): ?>
                <?php if (is_array($link)): ?>
                    <?php if (isset($link['url'])): ?>
                        <li role="presentation">
                            <a href="<?= $link['url'] ?>" role="menuitem">
                                <?php if (isset($link['icon'])): ?>
                                    <i class="icon <?= $link['icon'] ?>" aria-hidden="true"></i>
                                <?php endif; ?>
                                <?= $link['label'] ?>
                            </a>
                        </li>
                    <?php else: ?>
                        <li role="presentation" class="text-center">
                            <?= $link['label'] ?>
                        </li>
                    <?php endif; ?>
                <?php else: ?>
                    <li class="divider" role="presentation"></li>
                <?php endif; ?>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>
</li>

