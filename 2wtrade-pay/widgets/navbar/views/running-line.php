<?php
/** @var array $params */

?>


<?php
    $style ='';
    if (!empty($params['paddingTop'])) {
        $style .= 'padding-top: ' .$params['paddingTop'] / 4;
        if (!empty($params['paddingTopType'])) {
            $style .= $params['paddingTopType'];
        }
        $style .=  '; ';
    }
    if (!empty($params['marginLeft'])) {
        $style .= 'margin-left: ' .$params['marginLeft'];
        if (!empty($params['marginLeftType'])) {
            $style .= $params['marginLeftType'];
        }
        $style .=  '; ';
    }
    if (!empty($params['textColor'])) {
        $style .= 'color: ' .$params['textColor'];
        $style .=  '; ';
    }
    if (!empty($params['backgroundColor'])) {
        $style .= 'background-color: ' .$params['backgroundColor'];
        $style .=  '; ';
    }
    if (!empty($params['fontSize'])) {
        $style .= 'font-size: ' .$params['fontSize'];
        $style .=  '; ';
    }
    if (!empty($params['fontWeight'])) {
        $style .= 'font-weight: ' .$params['fontWeight'];
        $style .=  '; ';
    }
    if (!empty($params['lineHeight'])) {
        $style .= 'line-height: ' .$params['lineHeight'];
        $style .=  '; ';
    }
?>

<div style="padding-top: 6px; font-size: 16px; text-align: center;">2WTRADE - Worldwide Express Trade</div>

<hr style="width: 100%; height: 1px; background-color: #c2c2c2; position: absolute; top: 10px;">
<marquee scrollamount="<?= $params['speed']; ?>" style="<?= $style; ?>" onMouseOver="this.stop()" onMouseOut="this.start()">
    <?= $params['text']; ?>
</marquee>
</hr>
