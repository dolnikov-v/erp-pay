<?php
namespace app\widgets\navbar\assets;

use yii\web\AssetBundle;

/**
 * Class CountrySwitcherAsset
 * @package app\widgets\navbar\assets
 */
class CountrySwitcherAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/navbar/country-switcher';

    public $js = [
        'country-switcher.js',
    ];

    public $css = [
        'country-switcher.css',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
