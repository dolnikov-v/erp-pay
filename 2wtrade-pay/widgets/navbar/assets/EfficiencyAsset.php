<?php
namespace app\widgets\navbar\assets;

use yii\web\AssetBundle;

/**
 * Class EfficiencyAsset
 * @package app\widgets\navbar\assets
 */
class EfficiencyAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/navbar/efficiency';

    public $js = [
        'efficiency.js',
    ];

    public $css = [
        'efficiency.css',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}