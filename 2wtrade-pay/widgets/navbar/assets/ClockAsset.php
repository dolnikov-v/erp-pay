<?php
namespace app\widgets\navbar\assets;

use yii\web\AssetBundle;

/**
 * Class ClockAsset
 * @package app\widgets\navbar\assets
 */
class ClockAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/navbar/clock';

    public $js = [
        'clock.js',
    ];

    public $css = [
        'clock.css',
    ];

    public $depends = [
        'app\assets\SiteAsset',
        'app\modules\catalog\assets\ATimerAsset',
    ];
}
