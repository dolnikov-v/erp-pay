<?php
namespace app\widgets\navbar\assets;

use yii\web\AssetBundle;

/**
 * Class NotificationAsset
 * @package app\widgets\navbar\assets
 */
class NotificationAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/navbar/notification';

    public $js = [
        'notification.js',
    ];

    public $css = [
        'notification.css',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
