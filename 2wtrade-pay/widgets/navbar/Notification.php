<?php
namespace app\widgets\navbar;

use Yii;
use yii\base\Widget;

/**
 * Class Notification
 * @package app\widgets\navbar
 */
class Notification extends Widget
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('notification', [
            'countUnread' => Yii::$app->user->getCountUnreadNotifications(),
        ]);
    }
}
