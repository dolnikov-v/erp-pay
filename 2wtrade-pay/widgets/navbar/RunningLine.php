<?php
namespace app\widgets\navbar;

use yii\base\Widget;

/**
 * Class RunningLine
 * @package app\widgets\navbar
 */
class RunningLine extends Widget
{

    public $paddingTop = 100;
    public $paddingTopType = '%';
    public $marginLeft = 100;
    public $marginLeftType = '%';
    public $textColor = '#000000';
    public $backgroundColor = '#ffffff';
    public $fontSize = "";
    public $fontWeight = "";
    public $lineHeight = "";
    public $speed = "3";
    public $text = '';


    /**
     * @return string
     */
    public function run()
    {
        return $this->render('running-line', [
            'params' => [
                'paddingTop' => $this->paddingTop,
                'paddingTopType' => $this->paddingTopType,
                'marginLeft' => $this->marginLeft,
                'marginLeftType' => $this->marginLeftType,
                'textColor' => $this->textColor,
                'backgroundColor' => $this->backgroundColor,
                'fontSize' => $this->fontSize,
                'fontWeight' => $this->fontWeight,
                'lineHeight' => $this->lineHeight,
                'speed' => $this->speed,
                'text' => $this->text,
            ],
        ]);
    }
}
