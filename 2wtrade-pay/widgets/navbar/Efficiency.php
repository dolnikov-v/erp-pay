<?php
namespace app\widgets\navbar;

use yii\base\Widget;
use app\modules\reportoperational\models\TeamCountry;
use Yii;

/**
 * Class Efficiency
 * @package app\widgets\navbar
 */
class Efficiency extends Widget
{
    /*public static $maxSize = '97%';
    public static $minSize = '50%';
    public static $x = 0;
    public static $stepX = 10;
    public static $y = 0;
    public static $stepY = 0;
    public static $maxR = 100;
    public static $minR = 40;
    public static $stepR = 30;
    public static $differentEfficiency = 10;*/

    public static $cachedDataProvider;
    //public static $cachedSeries;
    public static $cachedDataForImgs;

    /**
     * @inheritdoc
     */
    public static function begin($config = [])
    {
        self::$cachedDataProvider = Yii::$app->cache->get('efficiency_provider');
        self::$cachedDataForImgs = Yii::$app->cache->get('efficiency_series');

        $result = null;
        if (self::$cachedDataProvider === false || self::$cachedDataForImgs === false) {
            $result = TeamCountry::getEfficiencyData();
        }

        if (self::$cachedDataProvider === false) {
            self::$cachedDataProvider = $result['dataProvider'];
            Yii::$app->cache->set('efficiency_provider', self::$cachedDataProvider, 600);
        }

        if (self::$cachedDataForImgs === false) {
            $teamData = $result['teamData'];
            if (is_array($teamData)) {
                foreach ($teamData as $teamDataKey => $teamDataValue) {
                    $teamData[$teamDataKey]['name'] = mb_substr($teamDataValue['name'], 0, 8);
                    $teamData[$teamDataKey]['efficiency'] = ($teamDataValue['efficiency_all'] ? $teamDataValue['efficiency_ok'] / $teamDataValue['efficiency_all'] * 100 : 0);
                }

                usort($teamData, "self::sortForEfficiency");
            }
            self::$cachedDataForImgs = $teamData;
            Yii::$app->cache->set('efficiency_series', self::$cachedDataForImgs, 600);

        }

        /*if (self::$cachedSeries === false) {
            $teamData = $result['teamData'];
            $series = [];
            if (is_array($teamData)) {
                $minEfficiency = 0;
                $maxEfficiency = 0;
                foreach ($teamData as $teamDataKey => $teamDataValue) {
                    $teamData[$teamDataKey]['efficiency'] = ($teamDataValue['efficiency_all'] ? $teamDataValue['efficiency_ok'] / $teamDataValue['efficiency_all'] * 100 : 0);
                    if ($teamData[$teamDataKey]['efficiency'] < $minEfficiency) {
                        $minEfficiency = $teamData[$teamDataKey]['efficiency'];
                    }
                    if ($teamData[$teamDataKey]['efficiency'] > $maxEfficiency) {
                        $maxEfficiency = $teamData[$teamDataKey]['efficiency'];
                    }
                }

                usort($teamData, "self::sortForEfficiency");

                if (count($teamData) == 0 || count($teamData) == 1) {
                    $stepR = self::$stepR;
                } else {
                    $stepR = (self::$maxR - self::$minR) / (count($teamData) - 1);
                }
                $currentR = self::$maxR;
                // эффективность мало отличается, пусть все радиусы будут одинаковыми
                if (($maxEfficiency - $minEfficiency) < self::$differentEfficiency) {
                    $stepR = 0;
                }
                foreach ($teamData as $teamDataItem) {
                    self::$x += self::$stepX;
                    $series[] = [
                        'dataLabels' => [
                            'enabled' => true,
                            'verticalAlign' => 'bottom',
                            'format' => '{series.name}',
                            'color' => '#fff',
                            'style' => [
                                'textOutline' => '1px solid #555'
                            ]
                        ],
                        'maxSize' => self::$maxSize,
                        'minSize' => self::$minSize,
                        'name' => $teamDataItem['name'],
                        'label' => ['enabled' => false],
                        'data' => [
                            [self::$x, self::$y, $currentR],

                        ],
                        'marker' => [
                            'states' => ['hover' => ['enabled' => false]],
                            'fillColor' => [
                                'radialGradient' => [
                                    'cx' => 0.4,
                                    'cy' => 0.3,
                                    'r' => 0.7
                                ],
                                'stops' => [
                                    [0, '#00ff00'],
                                    [1, '#006600'],
                                ]
                            ]
                        ],
                    ];
                    $currentR -= $stepR;
                    self::$y += self::$stepY;
                }

            }
            self::$cachedSeries = $series;
            Yii::$app->cache->set('efficiency_series', self::$cachedSeries, 1800);

        }*/

        parent::begin($config);
    }

    public static function end()
    {
        parent::end();
    }

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('efficiency', [
            'imgsData' => self::$cachedDataForImgs
        ]);
    }

    /**
     * функция сортировки в usort
     * @param array $a
     * @param array $b
     * @return integer
     */
    private static function sortForEfficiency($a, $b)
    {
        return ($a['efficiency'] > $b['efficiency']) ? -1 : +1;
    }
}