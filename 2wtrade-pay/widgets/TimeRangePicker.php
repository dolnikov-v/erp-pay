<?php

namespace app\widgets;

/**
 * Class TimeRangePicker
 * @package app\widgets
 */
class TimeRangePicker extends Widget
{
    /**
     * @var integer
     */
    public $valueFrom;
    /**
     * @var integer
     */
    public $valueTo;
    /**
     * @var string
     */
    public $nameFrom;
    /**
     * @var string
     */
    public $nameTo;
    /**
     * @var boolean
     */
    public $disabled;

    /**
     * @return string
     */
    public function run()
    {
        $offsetFrom = '';
        $offsetTo = '';

        if ($this->valueFrom) {
            $offsetFrom = date('H:i:s', $this->valueFrom);
        }

        if ($this->valueTo) {
            $offsetTo = date('H:i:s', $this->valueTo);
        }

        return $this->render('time-range-picker', [
            'offsetFrom' => $offsetFrom,
            'offsetTo' => $offsetTo,
            'nameFrom' => $this->nameFrom,
            'nameTo' => $this->nameTo,
            'disabled' => $this->disabled,
        ]);
    }
}