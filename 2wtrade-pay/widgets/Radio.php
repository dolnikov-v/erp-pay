<?php
namespace app\widgets;

/**
 * Class Radio
 * @package app\widgets
 */
class Radio extends Widget
{
    /** @var string */
    public $name;

    /** @var string */
    public $size;

    /** @var boolean */
    public $checked = false;

    /** @var boolean */
    public $disabled = false;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('radio', [
            'id' => $this->id,
            'name' => $this->name,
            'checked' => $this->checked,
            'disabled' => $this->disabled,
            'attributes' => $this->prepareAttributes(),
        ]);
    }
}
