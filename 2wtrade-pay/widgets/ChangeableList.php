<?php

namespace app\widgets;

/**
 * Class ChangeableList
 * @package app\widgets
 */
class ChangeableList extends Widget
{
    /**
     * @var string
     */
    public $label;

    /**
     * @var string | callable
     */
    public $template;

    /**
     * @var string[] | callable
     */
    public $items;

    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $cssClass = 'changeable-list-default-class';

    /**
     * Название js метода, который будет запускаться после добавления новой строки
     * Например, Invoice.init, вызовет Invoice.init() после добавления новой строки
     * @var string
     */
    public $callbackJsFuncAfterAddRow;

    /**
     * @return string
     */
    public function run()
    {
        if (is_callable($this->items)) {
            $this->items = call_user_func($this->items);
        }
        if (is_callable($this->template)) {
            $this->template = call_user_func($this->template);
        }
        return $this->render('changeable-list', [
            'label' => $this->label,
            'template' => $this->template,
            'items' => $this->items,
            'id' => $this->id,
            'class' => $this->cssClass,
            'callbackJsFuncAfterAddRow' => $this->callbackJsFuncAfterAddRow,
        ]);
    }
}