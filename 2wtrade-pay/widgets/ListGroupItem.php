<?php
namespace app\widgets;

/**
 * Class ListGroupItem
 * @package app\widgets
 */
class ListGroupItem extends Widget
{
    /**
     * @var string
     */
    public $content;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('list-group/list-group-item', [
            'content' => $this->content,
        ]);
    }
}
