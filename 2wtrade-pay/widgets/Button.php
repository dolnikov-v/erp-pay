<?php
namespace app\widgets;

/**
 * Class Button
 * @package app\widgets
 */
class Button extends Widget
{
    const STYLE_DEFAULT = 'btn-default';
    const STYLE_PRIMARY = 'btn-primary';
    const STYLE_SUCCESS = 'btn-success';
    const STYLE_INFO = 'btn-info';
    const STYLE_WARNING = 'btn-warning';
    const STYLE_DANGER = 'btn-danger';
    const STYLE_DARK = 'btn-dark';

    const SIZE_LARGE = 'btn-lg';
    const SIZE_SMALL = 'btn-sm';
    const SIZE_TINY = 'btn-xs';

    /**
     * @var string Тип кнопки
     */
    public $type = 'button';

    /**
     * @var string
     */
    public $value;

    /**
     * @var string Иконка
     */
    public $icon;

    /**
     * @var string Текст
     */
    public $label;

    /**
     * @var string Основной класс
     */
    public $style = self::STYLE_DEFAULT;

    /**
     * @var string размер
     */
    public $size;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('button', [
            'id' => $this->id,
            'type' => $this->type,
            'value' => $this->value,
            'icon' => $this->icon,
            'label' => $this->label,
            'style' => $this->style,
            'size' => $this->size,
            'attributes' => $this->prepareAttributes(),
        ]);
    }
}
