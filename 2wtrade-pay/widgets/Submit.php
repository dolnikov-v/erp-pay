<?php
namespace app\widgets;

/**
 * Class Submit
 * @package app\widgets
 */
class Submit extends Button
{
    /**
     * @var string Тип
     */
    public $type = 'submit';
}
