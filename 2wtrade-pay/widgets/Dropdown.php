<?php
namespace app\widgets;

/**
 * Class Dropdown
 * @package app\widgets
 */
class Dropdown extends Button
{
    /**
     * @var string
     */
    public $styleDropdown;

    /**
     * @var array Ссылки
     */
    public $links = [];

    /**
     * @var bool C анимацией
     */
    public $animate = false;

    /**
     * @var bool С правого края
     */
    public $right = false;

    /**
     * @var bool Открытие вверх
     */
    public $dropup = false;

    /**
     * @var bool
     */
    public $bullet = false;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('dropdown', [
            'type' => $this->type,
            'label' => $this->label,
            'style' => $this->style,
            'styleDropdown' => $this->styleDropdown,
            'size' => $this->size,
            'links' => $this->links,
            'animate' => $this->animate,
            'right' => $this->right,
            'dropup' => $this->dropup,
            'bullet' => $this->bullet,
        ]);
    }
}
