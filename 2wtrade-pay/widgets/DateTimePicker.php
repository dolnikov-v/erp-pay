<?php
namespace app\widgets;

use app\helpers\i18n\DateTimePicker as DateTimePickerHelper;
use Yii;

/**
 * Class DateTimePicker
 * @package app\widgets
 */
class DateTimePicker extends Widget
{
    /** @var string */
    public $label;

    /** @var string */
    public $name;

    /** @var string */
    public $format;

    /** @var string */
    public $value;

    /** @var bool */
    public $disabled = false;

    /** @var string */
    public $minDate;

    /** @var string */
    public $maxDate;

    /** @var string */
    public $type;

    /** @var bool */
    public $showTime = false;

    /**
     * @var array
     */
    public $attributes = [];

    /**
     * @return string
     * @throws \Exception
     */
    public function run()
    {
        $format = ($this->type == 'month')
            ? DateTimePickerHelper::getMonthFormat()
            : DateTimePickerHelper::getDateFormat();

        $value = '';
        if ($this->value) {
            $value = ($this->type == 'month')
                ? $this->value
                : ($this->showTime ?  Yii::$app->formatter->asDatetime($this->value) : Yii::$app->formatter->asDate($this->value));
        }

        $attributes = [
            'data-plugin' => 'datetimepicker',
            'data-format' => ($this->format ? $this->format : $format) . ($this->showTime ? ' HH:mm' : ''),
            'data-locale' => DateTimePickerHelper::getLocale()
        ];

        if ($this->minDate) {
            $attributes['data-mindate'] = $this->minDate;
        }

        if ($this->maxDate) {
            $attributes['data-maxdate'] = $this->maxDate;
        }

        $attributes = array_merge($attributes, $this->attributes);

        return $this->render('date-time-picker', [
            'widget' => InputGroup::widget([

                'input' => InputText::widget([
                    'name' => $this->name,
                    'value' => $value,
                    'disabled' => $this->disabled,
                    'attributes' => $attributes
                ]),

                'left' => [
                    Button::widget([
                        'icon' => 'wb-calendar',
                    ]),
                ],
            ]),
        ]);
    }
}
