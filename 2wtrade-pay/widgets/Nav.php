<?php
namespace app\widgets;

/**
 * Class Nav
 * @package app\widgets
 */
class Nav extends Widget
{
    const TYPE_NAV_TABS = 'nav-tabs';
    const TYPE_NAV_PILLS = 'nav-pills';

    const TYPE_TABS_NONE = '';
    const TYPE_TABS_LINE = 'nav-tabs-line';

    /** @var string Тип */
    public $typeNav = self::TYPE_NAV_TABS;

    /** @var string Тип для табов */
    public $typeTabs = self::TYPE_TABS_LINE;

    /**
     * Массив с табами, вида:
     * [
     *      [
     *          'label' => string,
     *          'content' => string, // optional
     *          'url' => string, // optional
     *          'visible' => bool, // optional
     *          'active' => bool, // optional
     *          'icon' => string, // optional
     *      ]
     * ]
     * @var array
     */
    public $tabs;

    /**
     * @var integer
     */
    public $activeTab = 0;

    /**
     * @return array
     */
    public function run()
    {
        return [
            'tabs' => $this->renderTabs(),
            'content' => $this->renderContent(),
        ];
    }

    /**
     * @return string
     */
    public function renderTabs()
    {
        return $this->render('nav/tabs', [
            'id' => $this->id,
            'typeNav' => $this->typeNav,
            'typeTabs' => $this->typeTabs,
            'tabs' => $this->tabs,
            'activeTab' => $this->activeTab,
        ]);
    }

    /**
     * @return string
     */
    public function renderContent()
    {
        return $this->render('nav/content', [
            'tabs' => $this->tabs,
            'activeTab' => $this->activeTab,
        ]);
    }
}
