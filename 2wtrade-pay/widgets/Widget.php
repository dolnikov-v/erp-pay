<?php
namespace app\widgets;

/**
 * Class Widget
 * @package app\widgets
 */
class Widget extends \yii\base\Widget
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var array Атрибуты
     */
    public $attributes = [];

    /**
     * @return string
     */
    protected function prepareAttributes()
    {
        if (isset($this->attributes['disabled']) && $this->attributes['disabled'] === false) {
            unset($this->attributes['disabled']);
        }
        $attributes = array_map(function ($value, $name) {
            return '' . $name . '="' . $value . '"';
        }, $this->attributes, array_keys($this->attributes));

        return implode(' ', $attributes);
    }
}
