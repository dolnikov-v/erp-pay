<?php
namespace app\widgets;

use app\models\Country;
use app\modules\storage\models\Storage;

/**
 * Class CountryStorage
 * @package app\widgets
 */
class CountryStorage extends Widget
{
    /**
     * @var string
     */
    public $nameCountry;

    /**
     * @var string
     */
    public $nameStorage;

    /**
     * @var integer
     */
    public $country = null;

    /**
     * @var integer
     */
    public $storage = null;

    /**
     * @var array
     */
    public $countries = [];

    /**
     * @var array
     */
    public $storages = [];

    /**
     * @var boolean
     */
    public $storagesDisabled = false;

    /**
     * @return string
     * @throws \Exception
     */
    public function run()
    {
        $this->countries = Country::find()
            ->active()
            ->orderBy('name')
            ->collection();

        if ($this->country != null) {
            $this->storages = Storage::find()
                ->byCountryId($this->country)
                ->orderBy('name')
                ->collection();
        }

        if (count($this->storages) == 0) {
            $this->storagesDisabled = true;
            $this->storage = null;
        }

        return $this->render('country-storage', [
            'countryCollection' => new Select2([
                'items' => $this->countries,
                'prompt' => '—',
                'length' => false,
                'name' => $this->nameCountry,
                'value' => $this->country,
            ]),

            'storageCollection' => new Select2([
                'items' => $this->storages,
                'prompt' => '—',
                'length' => false,
                'name' => $this->nameStorage,
                'disabled' => $this->storagesDisabled,
                'value' => $this->storage,
            ]),
        ]);
    }
}
