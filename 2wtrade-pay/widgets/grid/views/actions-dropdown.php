<?php
/** @var yii\web\View $this */
/** @var string $label */
/** @var boolean $withCaret */
/** @var app\widgets\Link[] $links */
/** @var string $attributes */
?>

<div class="dropdown" <?= $attributes ?>>
    <?php if ($label): ?>
        <a class="dropdown-toggle" href="#" role="button" aria-expanded="false" data-toggle="dropdown">
            <span class="dropdown-toggle-label"><?= $label ?></span>

            <?php if ($withCaret): ?>
                <span class="caret"></span>
            <?php endif; ?>
        </a>
    <?php endif; ?>

    <?php if (count($links) > 1): ?>
        <ul class="dropdown-menu bullet dropdown-menu-right" role="menu">
            <?php foreach ($links as $link): ?>
                <li role="presentation">
                    <?= $link->run() ?>
                </li>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>
</div>
