<?php
namespace app\widgets\grid;

use app\widgets\Widget;

/**
 * Class ActionsDropdown
 * @package app\widgets\panel
 */
class ActionsDropdown extends Widget
{
    /** @var string */
    public $label = '';

    /** @var boolean */
    public $withCaret = true;

    /** @var \app\widgets\Link[] */
    public $links = [];

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('actions-dropdown', [
            'label' => $this->label,
            'withCaret' => $this->withCaret,
            'links' => $this->links,
            'attributes' => $this->prepareAttributes(),
        ]);
    }
}
