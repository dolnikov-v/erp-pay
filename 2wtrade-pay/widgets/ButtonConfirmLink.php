<?php
namespace app\widgets;

/**
 * Class ButtonConfirmLink
 *
 * Кнопка с функцией подтверждения в первой версии обычный confirm
 *
 * @package app\widgets
 */
class ButtonConfirmLink extends Button
{
    /**
     * @var string Ссылка для перехода
     */
    public $url;

    /**
     * @var string Основной класс кнопки
     */
    public $style = self::STYLE_DEFAULT;

    /**
     * @var boolean btn-block
     */
    public $isBlock = false;

    /**
     * @var string текст для подтверждения
     */
    public $confirm = '';

    /**
     * @var string заголовок модального окна
     */
    public $title = '';

    /**
     * @var string стиль модального окна
     */
    public $modalColor = Modal::COLOR_WARNING;

    /**
     * @var string стиль кнопки в модальном окне
     */
    public $modalButtonStyle = ButtonLink::STYLE_SUCCESS;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('button-confirm-link', [
            'id' => $this->id,
            'url' => $this->url,
            'style' => $this->style,
            'size' => $this->size,
            'isBlock' => $this->isBlock,
            'icon' => $this->icon,
            'label' => $this->label,
            'confirm' => $this->confirm,
            'title' => $this->title,
            'attributes' => $this->prepareAttributes(),
            'modalColor' => $this->modalColor,
            'modalButtonStyle' => $this->modalButtonStyle
        ]);
    }
}
