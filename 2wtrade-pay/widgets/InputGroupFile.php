<?php
namespace app\widgets;

/**
 * Class InputGroupFile
 * @package app\widgets
 */
class InputGroupFile extends Widget
{
    /**
     * @var InputText
     */
    public $input;

    /**
     * @var boolean
     */
    public $right = true;

    /**
     * @return string
     * @throws \InvalidArgumentException
     */
    public function run()
    {
        return $this->render('input-group-file/input-group-file', [
            'input' => $this->input,
            'right' => $this->right,
        ]);
    }
}
