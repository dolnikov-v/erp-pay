<?php
namespace app\widgets;

/**
 * Class FormGroup
 * @package app\widgets
 */
class FormGroup extends Widget
{
    const STYLE_SUCCESS = 'has-success';
    const STYLE_INFO = 'has-info';
    const STYLE_WARNING = 'has-warning';
    const STYLE_DANGER = 'has-error';

    /**
     * @var
     */
    public $label;

    /**
     * @var
     */
    public $style;

    /**
     * @var
     */
    public $subClass;

    /**
     * @var bool|string
     */
    public $hint = false;

    /**
     * @var bool
     */
    public $material = false;

    /**
     * @var bool
     */
    public $floating = false;

    /**
     * @var
     */
    public $widget;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('form-group', [
            'label' => $this->label,
            'style' => $this->style,
            'subClass' => $this->subClass,
            'hint' => $this->hint,
            'material' => $this->material,
            'floating' => $this->floating,
            'widget' => $this->widget,
        ]);
    }
}
