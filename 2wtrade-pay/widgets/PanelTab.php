<?php
namespace app\widgets;

/**
 * Class PanelTab
 * @package app\widgets
 */
class PanelTab extends Widget
{
    public $id;

    public $title;

    public $content;

    public function run()
    {
        return $this->render('panel-tab', [
            'id' => $this->id,
            'content' => $this->content,
            'title' => $this->title
        ]);
    }
}
