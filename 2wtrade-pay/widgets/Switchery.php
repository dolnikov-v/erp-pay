<?php
namespace app\widgets;

use yii\helpers\ArrayHelper;

/**
 * Class Switchery
 * @package app\widgets
 */
class Switchery extends Checkbox
{
    const SIZE_LARGE = 'large';
    const SIZE_SMALL = 'small';

    const COLOR_DEFAULT = '';
    const COLOR_DARK = '#526069';
    const COLOR_SUCCESS = '#3aa99e';
    const COLOR_WARNING = '#f2a654';
    const COLOR_DANGER = '#f96868';

    public $color = self::COLOR_SUCCESS;

    /**
     * @return string
     */
    public function run()
    {
        $this->attributes = ArrayHelper::merge([
            'data-size' => $this->size,
            'data-color' => self::COLOR_SUCCESS,
            'data-plugin' => 'switchery',
            'data-switchery' => 'true',
        ], $this->attributes);

        return parent::run();
    }
}
