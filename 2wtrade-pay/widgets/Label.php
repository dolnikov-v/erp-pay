<?php
namespace app\widgets;

/**
 * Class Label
 * @package app\widgets
 */
class Label extends Widget
{
    const STYLE_DEFAULT = 'label-default';
    const STYLE_PRIMARY = 'label-primary';
    const STYLE_SUCCESS = 'label-success';
    const STYLE_INFO = 'label-info';
    const STYLE_WARNING = 'label-warning';
    const STYLE_DANGER = 'label-danger';
    const STYLE_DARK = 'label-dark';

    const SIZE_LARGE = 'label-lg';
    const SIZE_SMALL = 'label-sm';

    /**
     * @var string
     */
    public $style = self::STYLE_DEFAULT;

    /**
     * @var string
     */
    public $size;

    /**
     * @var bool
     */
    public $round = false;

    /**
     * @var bool
     */
    public $outline = false;

    /**
     * @var
     */
    public $label;

    /**
     * @var string
     */
    public $title;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('label', [
            'label' => $this->label,
            'style' => $this->style,
            'size' => $this->size,
            'round' => $this->round,
            'outline' => $this->outline,
            'title' => $this->title,
        ]);
    }
}
