<?php
namespace app\widgets;

/**
 * Class Breadcrumbs
 * @package app\widgets
 */
class Breadcrumbs extends \yii\base\Widget
{
    /**
     * @var array Набор ссылок
     */
    public $links = [];

    /**
     * @var bool Со стрелками
     */
    public $arrow = false;

    /**
     * @var bool
     */
    public $fixed = false;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('breadcrumbs', [
            'links' => $this->links,
            'arrow' => $this->arrow,
            'fixed' => $this->fixed,
        ]);
    }
}
