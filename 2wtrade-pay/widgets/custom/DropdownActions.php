<?php
namespace app\widgets\custom;

use app\widgets\Dropdown;

/**
 * Class DropdownActions
 * @package app\widgets\custom
 */
class DropdownActions extends Dropdown
{
    /**
     * @var array Ссылки
     */
    public $links = [];

    /**
     * @var bool C анимацией
     */
    public $animate = false;

    /**
     * @var string Css-класс
     */
    public $style = 'btn-outline btn-default';

    /**
     * @var string Размер кнопок
     */
    public $size = Dropdown::SIZE_SMALL;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('dropdown-actions', [
            'type' => $this->type,
            'label' => $this->label,
            'style' => $this->style,
            'size' => $this->size,
            'links' => $this->links,
            'animate' => $this->animate,
            'right' => $this->right,
        ]);
    }
}
