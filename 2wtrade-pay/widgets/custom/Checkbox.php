<?php
namespace app\widgets\custom;

use app\widgets\Checkbox as CheckboxBase;

/**
 * Class Checkbox
 * @package app\widgets\custom
 */
class Checkbox extends CheckboxBase
{
    const COLOR_EMPTY = '';
    const COLOR_DEFAULT = 'checkbox-default';
    const COLOR_PRIMARY = 'checkbox-primary';
    const COLOR_SUCCESS = 'checkbox-success';
    const COLOR_INFO = 'checkbox-info';
    const COLOR_WARNING = 'checkbox-warning';
    const COLOR_DANGER = 'checkbox-danger';

    /** @var string */
    public $color = self::COLOR_PRIMARY;

    /** @var string */
    public $value;

    /** @var string */
    public $style;

    /** @var string */
    public $label;

    /**
     * If checkbox is unchecked then set $uncheckedValue of an field
     * @var string
     */
    public $uncheckedValue;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('checkbox/checkbox', [
            'color' => $this->color,
            'id' => $this->id,
            'name' => $this->name,
            'value' => $this->value,
            'style' => $this->style,
            'checked' => $this->checked,
            'disabled' => $this->disabled,
            'attributes' => $this->prepareAttributes(),
            'label' => $this->label,
            'uncheckedValue' => $this->uncheckedValue,
        ]);
    }
}
