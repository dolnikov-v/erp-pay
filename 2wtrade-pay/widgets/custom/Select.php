<?php
namespace app\widgets\custom;

use app\widgets\Select as SelectBase;

/**
 * Class Select
 * @package app\widgets\custom
 */
class Select extends SelectBase
{
    const COLOR_EMPTY = '';
    const COLOR_DEFAULT = 'checkbox-default';
    const COLOR_PRIMARY = 'checkbox-primary';
    const COLOR_SUCCESS = 'checkbox-success';
    const COLOR_INFO = 'checkbox-info';
    const COLOR_WARNING = 'checkbox-warning';
    const COLOR_DANGER = 'checkbox-danger';

    /** @var string */
    public $color = self::COLOR_PRIMARY;

    /** @var string */
    public $value;

    /** @var string */
    public $style;

    /** @var string */
    public $label;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('select/select', [
            'color' => $this->color,
            'id' => $this->id,
            'name' => $this->name,
            'value' => $this->value,
            'style' => $this->style,
            'checked' => $this->checked,
            'disabled' => $this->disabled,
            'attributes' => $this->prepareAttributes(),
            'label' => $this->label,
        ]);
    }
}
