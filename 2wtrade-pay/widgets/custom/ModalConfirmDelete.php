<?php
namespace app\widgets\custom;

use app\widgets\Modal;
use Yii;

/**
 * Class ModalConfirmDelete
 * @package app\widgets\custom
 */
class ModalConfirmDelete extends Modal
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('modal-confirm-delete/modal-confirm-delete', [
            'id' => 'modal_confirm_delete',
            'size' => self::SIZE_MEDIUM,
            'color' => self::COLOR_DANGER,
            'title' => Yii::t('common', 'Подтверждение удаления'),
            'close' => true,
            'body' => $this->render('modal-confirm-delete/_body'),
            'footer' => $this->render('modal-confirm-delete/_footer'),
        ]);
    }
}
