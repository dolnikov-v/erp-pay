<?php
namespace app\widgets\custom;

use app\widgets\Radio as RadioBase;

/**
 * Class Radio
 * @package app\widgets\custom
 */
class Radio extends RadioBase
{
    const COLOR_EMPTY = '';
    const COLOR_DEFAULT = 'radio-default';
    const COLOR_PRIMARY = 'radio-primary';
    const COLOR_SUCCESS = 'radio-success';
    const COLOR_INFO = 'radio-info';
    const COLOR_WARNING = 'radio-warning';
    const COLOR_DANGER = 'radio-danger';

    /** @var string */
    public $color = self::COLOR_PRIMARY;

    /** @var string */
    public $value;

    /** @var string */
    public $style;

    /** @var string */
    public $label;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('radio/radio', [
            'color' => $this->color,
            'id' => $this->id,
            'name' => $this->name,
            'value' => $this->value,
            'style' => $this->style,
            'checked' => $this->checked,
            'disabled' => $this->disabled,
            'attributes' => $this->prepareAttributes(),
            'label' => $this->label,
        ]);
    }
}
