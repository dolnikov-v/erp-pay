<?php
/** @var string $color */
use yii\helpers\Html;

/** @var string $id */
/** @var string $name */
/** @var string $value */
/** @var string $style */
/** @var string $checked */
/** @var string $disabled */
/** @var string $attributes */
/** @var string $label */
/** @var string $uncheckedValue */
?>

<div class="checkbox-custom <?= $color ?> <?= $style ?>">
    <?php if (!is_null($uncheckedValue)){
        // add a hidden field so that if the checkbox is not selected, it still submits a value
    echo Html::hiddenInput($name, $uncheckedValue);
    }
    ?>
    <input <?php if ($id): ?>id="<?= $id ?>"<?php endif; ?>
        type="checkbox"
        <?php if ($name): ?>name="<?= $name ?>"<?php endif; ?>
        <?php if ($value): ?>value="<?= $value ?>"<?php endif; ?>
        <?php if ($checked): ?>checked="checked"<?php endif; ?>
        <?php if ($disabled): ?>disabled="disabled"<?php endif; ?>
        <?= $attributes ?>/>

    <?php if ($label): ?>
        <label <?php if ($id): ?>for="<?= $id ?>"<?php endif; ?>><?= $label === true ? '' : $label ?></label>
    <?php endif; ?>
</div>
