<?php
/** @var string $color */
/** @var string $id */
/** @var string $name */
/** @var string $value */
/** @var string $style */
/** @var string $checked */
/** @var string $disabled */
/** @var string $attributes */
/** @var string $label */
?>

<div class="radio-custom <?= $color ?> <?= $style ?>">
    <input <?php if ($id): ?>id="<?= $id ?>"<?php endif; ?>
           type="radio"
           <?php if ($name): ?>name="<?= $name ?>"<?php endif; ?>
           <?php if ($value): ?>value="<?= $value ?>"<?php endif; ?>
           <?php if ($checked): ?>checked="checked"<?php endif; ?>
           <?php if ($disabled): ?>disabled="disabled"<?php endif; ?>
           <?= $attributes ?>/>

    <?php if ($label): ?>
        <label <?php if ($id): ?>for="<?= $id ?>"<?php endif; ?>><?= $label === true ? '' : $label ?></label>
    <?php endif; ?>
</div>
