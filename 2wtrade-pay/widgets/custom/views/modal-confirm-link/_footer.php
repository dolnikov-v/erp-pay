<?php
use app\widgets\Button;
use app\widgets\ButtonLink;

/** @var string $buttonStyle */
?>

<?= Button::widget([
    'label' => Yii::t('common', 'Отмена'),
    'size' => Button::SIZE_SMALL,
    'attributes' => [
        'data-dismiss' => 'modal',
    ],
]) ?>

<?= ButtonLink::widget([
    'id' => 'modal_confirm_data_link',
    'label' => Yii::t('common', 'Подтвердить'),
    'url' => '#',
    'style' => $buttonStyle,
    'size' => Button::SIZE_SMALL,
]) ?>
