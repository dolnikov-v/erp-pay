<?php
use app\widgets\Link;

/** @var string $style Основной класс */
/** @var string $size Размер */
/** @var array $links Ссылки */
/** @var array $animate C анимацией */
/** @var array $right C права */
?>

<?php if (!empty($links)): ?>
    <div class="btn-group">
        <button class="btn dropdown-toggle <?= $style ?> <?= $size ?>" aria-expanded="false" data-toggle="dropdown" type="button">
            <?= Yii::t('common', 'Действия') ?>
            <span class="caret"></span>
        </button>

        <ul class="dropdown-menu <?php if ($animate): ?>animate<?php endif; ?> dropdown-menu-right">
            <?php foreach ($links as $link): ?>
                <?php if (is_array($link)): ?>
                    <?php if (isset($link['url'])): ?>
                        <?php $class = (isset($link['active'])) ? 'active' : '' ?>
                        <li class="<?= $class ?>">
                            <?= Link::widget([
                                'url' => $link['url'],
                                'label' => $link['label'],
                                'style' => isset($link['style']) ? $link['style'] : '',
                                'attributes' => isset($link['attributes']) ? $link['attributes'] : [],
                            ]) ?>
                        </li>
                    <?php else: ?>
                        <li class="dropdown-header"><?= $link['label'] ?></li>
                    <?php endif; ?>
                <?php else: ?>
                    <li class="divider"></li>
                <?php endif; ?>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>
