<?php
use app\widgets\Button;
use app\widgets\ButtonLink;
?>

<?= Button::widget([
    'label' => Yii::t('common', 'Отмена'),
    'size' => Button::SIZE_SMALL,
    'attributes' => [
        'data-dismiss' => 'modal',
    ],
]) ?>

<?= ButtonLink::widget([
    'id' => 'modal_confirm_delete_link',
    'label' => Yii::t('common', 'Удалить'),
    'url' => '#',
    'style' => ButtonLink::STYLE_DANGER,
    'size' => Button::SIZE_SMALL,
]) ?>
