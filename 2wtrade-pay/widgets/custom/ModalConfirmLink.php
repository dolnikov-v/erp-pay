<?php
namespace app\widgets\custom;

use app\widgets\ButtonLink;
use app\widgets\Modal;
use Yii;

/**
 * Class ModalConfirmLink
 * @package app\widgets\custom
 */
class ModalConfirmLink extends Modal
{
    /**
     * @var string
     */
    public $color = self::COLOR_WARNING;

    /**
     * @var string
     */
    public $title = 'Подтверждение';

    /**
     * @var string
     */
    public $confirm = 'Вы уверены?';
    /**
     * @var string
     */
    public $buttonStyle = ButtonLink::STYLE_SUCCESS;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('modal-confirm-link/modal', [
            'id' => 'modal_confirm_data',
            'size' => self::SIZE_MEDIUM,
            'color' => $this->color,
            'title' => $this->title,
            'confirm' => $this->confirm,
            'close' => true,
            'body' => $this->render('modal-confirm-link/_body', [
                'confirm' => $this->confirm
            ]),
            'footer' => $this->render('modal-confirm-link/_footer', [
                'buttonStyle' => $this->buttonStyle
            ]),
        ]);
    }
}
