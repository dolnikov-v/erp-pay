<?php
namespace app\widgets;

/**
 * Class ButtonGroup
 * @package app\widgets
 */
class ButtonGroup extends Widget
{
    const ORIENTATION_HORIZONTAL = 'btn-group';
    const ORIENTATION_VERTICAL = 'btn-group-vertical';

    /**
     * @var \app\widgets\Button[] Кнопки
     */
    public $buttons = [];

    /**
     * @var bool Ориентация
     */
    public $orientation = self::ORIENTATION_HORIZONTAL;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('button-group', [
            'buttons' => $this->buttons,
            'orientation' => $this->orientation,
        ]);
    }
}
