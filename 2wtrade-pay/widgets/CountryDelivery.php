<?php
namespace app\widgets;

use app\models\Country;
use app\modules\delivery\models\Delivery;

/**
 * Class CountryDelivery
 * @package app\widgets
 */
class CountryDelivery extends Widget
{
    /**
     * @var string
     */
    public $nameCountry;

    /**
     * @var string
     */
    public $nameDelivery;

    /**
     * @var integer
     */
    public $country = null;

    /**
     * @var integer
     */
    public $delivery = null;

    /**
     * @var array
     */
    public $countries = [];

    /**
     * @var array
     */
    public $deliveries = [];

    /**
     * @var boolean
     */
    public $deliveriesDisabled = false;

    /**
     * @return string
     * @throws \Exception
     */
    public function run()
    {
        $this->countries = Country::find()
            ->active()
            ->orderBy('name')
            ->collection();

        if ($this->country != null) {
            $this->deliveries = Delivery::find()
                ->active()
                ->byCountryId($this->country)
                ->orderBy('name')
                ->collection();
        }

        if (count($this->deliveries) == 0) {
            $this->deliveriesDisabled = true;
            $this->delivery = null;
        }

        return $this->render('country-delivery', [
            'countryCollection' => new Select2([
                'items' => $this->countries,
                'prompt' => '—',
                'length' => -1,
                'name' => $this->nameCountry,
                'value' => $this->country,
            ]),

            'deliveryCollection' => new Select2([
                'items' => $this->deliveries,
                'prompt' => '—',
                'length' => -1,
                'name' => $this->nameDelivery,
                'disabled' => $this->deliveriesDisabled,
                'value' => $this->delivery,
            ]),
        ]);
    }
}
