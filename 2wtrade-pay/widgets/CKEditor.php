<?php
namespace app\widgets;

use Yii;
use dosamigos\ckeditor\CKEditorWidgetAsset;
use yii\helpers\Json;

class CKEditor extends \dosamigos\ckeditor\CKEditor
{
    /**
     * @var array
     */
    public $tokens;

    /**
     * @var int
     */
    public $height = 36;

    /**
     * @var string
     */
    public $toolbarLocation = 'top';

    public function init()
    {
        $tokens = [];
        if ($this->tokens) {
            foreach ($this->tokens as $code => $name) {
                $tokens[] = [$name, $code];
            }
        }

        $this->preset = 'custom';
        $this->clientOptions = [
            'toolbarLocation' => $this->toolbarLocation,
            'height' => $this->height,
            'toolbar' => [
                ['CreateToken'],
                [
                    'name' => 'clipboard',
                    'items' => ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']
                ],
            ],
            'enterMode' => 2, // CKEDITOR.ENTER_BR
            'UseBROnCarriageReturn' => 1,
            'availableTokens' => $tokens,
            'contentsCss' => Yii::getAlias('@web/vendor/vendor/ckeditor/style/token.css'),
            'tokenStart' => '{',
            'tokenEnd' => '}',
            'removePlugins' =>  'elementspath',
            'extraPlugins' => 'lineutils,widget,token',
            'toolbarCanCollapse'  => false,
        ];

        parent::init();

        if ($this->hasModel()) {
            $attr = $this->attribute;
            $this->options['value'] = nl2br($this->model->$attr);
        }
        $this->options['data']['widget'] = 'ckeditor';
    }

    /**
     * @inheritdoc
     */
    public function registerPlugin()
    {
        $view = $this->getView();

        CKEditorWidgetAsset::register($view);

        $id = $this->options['id'];

        $options = $this->clientOptions !== false && !empty($this->clientOptions)
            ? Json::encode($this->clientOptions)
            : '{}';

        $externalPath = Yii::getAlias('@web/vendor/vendor/ckeditor/plugin');
        $js[] = "CKEDITOR.plugins.addExternal('lineutils', '{$externalPath}/lineutils/', 'plugin.js');";
        $js[] = "CKEDITOR.plugins.addExternal('widgetselection', '{$externalPath}/widgetselection/', 'plugin.js');";
        $js[] = "CKEDITOR.plugins.addExternal('widget', '{$externalPath}/widget/', 'plugin.js');";
        $js[] = "CKEDITOR.plugins.addExternal('token', '{$externalPath}/token/', 'plugin.js');";
        $js[] = "CKEDITOR.replace('$id', $options);";
        $js[] = "dosamigos.ckEditorWidget.registerOnChangeHandler('$id');";

        $view->registerJs(implode("\n", $js));
    }
}

