<?php

namespace app\widgets;

use yii\base\Widget;

/**
 * Class Log
 * @package app\widgets
 */
class Log extends Widget
{
    /**
     * @var null|array
     */
    public $templates = ['revertNum', 'type', 'created_at'];

    /**
     * @var null|string
     */
    public $titleContent = null;

    /**
     * @var array
     */
    public $data;

    /**
     * @var bool
     */
    public $showLinks = false;

    /**
     * @var bool
     */
    public $showData = true;

    /**
     * @var null|string
     */
    public $title;

    /**
     * @var string
     */
    public $groupBy = '_id';

    public function run()
    {
        return $this->render('log', [
            'templates' => $this->templates,
            'data' => $this->data,
            'title' => $this->title ?? null,
            'titleContent' => $this->titleContent,
            'showLinks' => $this->showLinks,
            'showData' => $this->showData,
            'groupBy' => $this->groupBy,
        ]);
    }
}