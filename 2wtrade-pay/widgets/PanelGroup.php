<?php
namespace app\widgets;

/**
 * Class PanelGroup
 * @package app\widgets
 */
class PanelGroup extends Widget
{
    /** @var string */
    public $id;

    /** @var PanelTab[] $panels */
    public $panels = [];

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('@app/widgets/views/panel-group', [
            'panels' => $this->panels,
            'id' => $this->id,
        ]);
    }
}
