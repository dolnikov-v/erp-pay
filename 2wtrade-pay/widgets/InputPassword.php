<?php
namespace app\widgets;

/**
 * Class InputPassword
 * @package app\widgets
 */
class InputPassword extends InputText
{
    /**
     * @var string
     */
    public $type = 'password';
}
