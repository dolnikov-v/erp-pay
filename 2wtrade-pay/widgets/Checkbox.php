<?php
namespace app\widgets;

/**
 * Class Checkbox
 * @package app\widgets
 */
class Checkbox extends Widget
{
    /** @var string */
    public $name;

    /** @var string */
    public $value;

    /** @var string */
    public $size;

    /** @var boolean */
    public $checked = false;

    /** @var boolean */
    public $disabled = false;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('checkbox', [
            'id' => $this->id,
            'name' => $this->name,
            'value' => $this->value,
            'checked' => $this->checked,
            'disabled' => $this->disabled,
            'attributes' => $this->prepareAttributes(),
        ]);
    }
}
