<?php
/* @var $penalty \app\modules\salary\models\Penalty */
?>
Dear Employee!
You have a new penalty.

Type: <?= Yii::t('common', $penalty->penaltyType->name, [], 'en-US') ?>
<?php if ($penalty->penalty_sum_local) : ?>
Amount, <?= $penalty->person->office->country->currency->char_code ?>: <?= Yii::$app->formatter->asDecimal($penalty->penalty_sum_local) ?>
<?php elseif ($penalty->penalty_sum_usd) : ?>
Amount, USD: <?= Yii::$app->formatter->asDecimal($penalty->penalty_sum_usd) ?>
<?php endif; ?>

Date: <?= Yii::$app->formatter->asDate($penalty->date) ?>


Best regards, PhilomelaCall