<?php
/**
 * @var bool $correction
 * @var string $link
 * @var int $orderCount
 */
?>
    <p>
        <?php if ($correction): ?>
            Dear colleagues, we attached the corrected addresses.
            Greetings.
        <?php else: ?>
            Dear Partners!
        <?php endif; ?>
    </p>

<?php if ($link): ?>
    <p>
        Please confirm receiving of <?= $orderCount ?> orders <?= $correction ? 'correction' : '' ?> by clicking the link: <?= $link ?>
    </p>
    <img src="<?= $link ?>" height="1" width="1"/>
<?php endif; ?>