<?php
/* @var \app\modules\delivery\models\Delivery $delivery */

$contact = [];
if (!empty($delivery->country->curatorUser->fullname)) {
    $contact[] = $delivery->country->curatorUser->fullname;
}
if (!empty($delivery->country->curatorUser->email)) {
    $contact[] = $delivery->country->curatorUser->email;
}
?>
<p>Dear <?= $delivery->name ?>!</p>

<p>Please be informed our call-center rechecked some orders you reported as «returned».<br/>
    Some of our clients informed us that they received products despite you reported it as «returned».<br/>
    Please send us corrected status report.<br/>
    In case of any questions please do not hesitate to ask your account
    manager <?= (($contact) ? implode(': ', $contact) : '') ?> .</p>

<p>Regards,<br/>
    2Wtrade team</p>