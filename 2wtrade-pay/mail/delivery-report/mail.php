<?php
/**
 * @var string $delivery
 * @var string $tm_name
 * @var string $tm_email
 * @var \yii\mail\BaseMessage $message
 */

use yii\helpers\Html;

?>
Dear <?= $delivery ?>!<br/><br/>
We noticed that no status reports have been sent to us for past 4 days.<br/><br/>
Under agreement established between our companies status reports should be sent on daily basis.<br/><br/>
Please send status report to e-mail address: status@2wtrade.com.<br/><br/>
This is an automatically generated e-mail – please do not reply to it. If you have any queries regarding your reports please contact your account manager: <?= $tm_name ?><?= ($tm_email !== $tm_name ? ' ' . $tm_email : '') ?>.<br/><br/>
2Wtrade LLP<br/><br/>
<?= Html::img($message->embed(\Yii::getAlias('@app/web/themes/basic/images/logo.png')));?><br/><br/>
This email and all attachments to it are strictly confidential and intended solely for use by the recipient (addressee) to which they are addressed. Disclosure of information contained in this e-mail is not permitted by law. Use of the information contained in this e-mail is allowed in accordance with the requirements of applicable legislation and non-disclosure agreements. If you have received this e-mail by mistake, please notify the sender immediately by e-mail and destroy all copies of this e-mail from your system. If you are not the intended recipient you are notified that disclosing, copying, distributing or taking any action in reliance on the contents of this information is strictly prohibited.