<div class="wrap-message">
    <?php
    /** @var \app\models\UserNotificationQueue $message */
    foreach ($messages as $key => $message): ?>
        <?php $country = $message->userNotification->country; ?>
        <?php $language = $message->userNotification->user->language; ?>
        <?php if ($key == 0):?>
            <?php
            $nameUrl = Yii::t('common', 'У вас {count, number} {count, plural, one{непрочитанное оповещение} few{непрочитанных оповещения} other{непрочитанных оповещений}}.', ['count' => $count], $language);
            $url = Yii::$app->params['domain'] . '/profile/notification/index';
            ?>
            <p><?= \yii\helpers\Html::a($nameUrl, $url); ?></p>
        <?php endif;?>
        <p><?= $key + 1 . ". " . Yii::t('common', $message->userNotification->notification->description, json_decode($message->userNotification->params, true), $language) . ($country ? '. ' . Yii::t('common', $country->name, [], $language) : ''); ?></p>
    <?php endforeach; ?>
</div>