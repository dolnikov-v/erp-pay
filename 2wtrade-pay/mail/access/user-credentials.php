<?php
/**
 * @var \yii\base\View $this
 * @var string $payUsername
 * @var string $payPassword
 * @var string $vpnUserId
 * @var string $vpnPassword
 * @var string $message
 * @var bool $updated
 * @var bool $vpnError
 */
?>

<p>This message was sent automatically when user
    was <?php if (empty($updated)): ?>created<?php else: ?>updated<?php endif; ?> in <a
            href="http://2wtrade-pay.com">http://2wtrade-pay.com</a>.</p>
<div>Access:</div>
<p></p>
<div>&emsp;<a href="http://2wtrade-pay.com">http://2wtrade-pay.com</a></div>
<div>---------------------------------</div>
<div>&emsp;Login: <?= $payUsername ?></div>
<div>&emsp;Password: <?= $payPassword ?></div>
<p></p>

<?php if(!empty($vpnError)): ?>
    <p></p>
<div>Could not automatically create VPN user.</div>
<div>Contact your administrator.</div>
<?php elseif ($vpnUserId && $vpnPassword): ?>
    <p></p>
    <div>&emsp;Instructions for VPN access</div>
    <div>---------------------------------</div>
    <div>&emsp;<a href="http://185.56.232.68/pay.pdf">Instruction</a></div>
    <div>&emsp;<a href="http://185.56.232.68/pay.ovpn">Configuration (instruction also contains this link)</a></div>
    <div>&emsp;Login: <?= $vpnUserId ?></div>
    <div>&emsp;Password: <?= $vpnPassword ?></div>
    <p></p>
<?php endif; ?>

<?php if (!empty($message)): ?>
    <p><?= $message ?></p>
<?php endif; ?>

<div>---------------------------------</div>
<p>"2Wtrade Rus" security service,</p>
<p>Best wishes!</p>