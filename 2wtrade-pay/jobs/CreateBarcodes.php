<?php
namespace app\jobs;

use app\modules\order\models\Order;
use app\modules\order\models\OrderLogisticList;
use app\modules\order\models\OrderLogisticListBarcode;

/**
 * Class CreateBarcodes
 * @package app\jobs
 */
class CreateBarcodes extends BaseJob
{

    /**
     * @var integer
     */
    public $orderLogisticListId;

    /**
     * @param \yii\queue\Queue $queue
     * @return bool
     * @throws \Exception
     */
    public function execute($queue)
    {

        if (!$this->orderLogisticListId) {
            $this->logAddError('No parameter $orderLogisticListId');
            return false;
        }

        $list = OrderLogisticList::findOne($this->orderLogisticListId);

        if (!$list) {
            $this->logAddError('No list found ID = ' . $this->orderLogisticListId);
            return false;
        }

        if ($list->status != OrderLogisticList::STATUS_BARCODING) {
            $this->logAddError('Incorrect status ' . $list->status);
            return false;
        }

        if ($list->orderLogisticListBarcodes) {
            $this->logAddError('Barcodes have already been created');
            return false;
        }

        $transaction = OrderLogisticListBarcode::getDb()->beginTransaction();
        try {

            foreach (array_chunk($list->orders, 5000) as $orders) {

                $arrayForInsert = [];
                $arrayForSend = [];

                foreach ($orders as $order) {

                    /** @var $order Order */

                    $arrayForInsert[] = [
                        'list_id' => $list->id,
                        'order_id ' => $order->id,
                    ];

                    $products = [];
                    foreach ($order->orderProducts as $product) {
                        $products[] = [
                            'id' => $product->product->id,
                            'quantity' => $product->quantity
                        ];
                    }
                    $arrayForSend[] = [
                        'id' => $order->id,
                        'products ' => $products,
                    ];
                }

                $dataForSend = [
                    'cmd' => "list_barcodes_gen",
                    'list_id' => $list->id,
                    'warehouse' => $list->delivery->getStorageExchangeCode(),
                    'orders' => $arrayForSend
                ];

                $client = $this->getSqsClient();

                $client->sendMessage([
                    'MessageBody' => json_encode($dataForSend, JSON_UNESCAPED_UNICODE),
                    'QueueUrl' => $client->getSqsQueueUrl('barcodeRequest')
                ]);

                OrderLogisticListBarcode::getDb()->createCommand()->batchInsert(
                    OrderLogisticListBarcode::tableName(),
                    [
                        'list_id',
                        'order_id'
                    ],
                    $arrayForInsert
                )->execute();
            }

            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            $this->logAddError($e->getMessage());
            return false;
        }

        return true;
    }

}