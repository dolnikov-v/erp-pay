<?php
namespace app\jobs;

use yii\mail\MessageInterface;
use yii\swiftmailer\Message;
use Yii;

/**
 * Class SendMail
 * @package app\jobs
 */
class SendMail extends BaseJob
{
    /**
     * @var MessageInterface|Message
     */
    public $mailObject;

    /**
     * @param \yii\queue\Queue $queue
     * @return bool
     * @throws \Exception
     */
    public function execute($queue)
    {
        if (!$this->actionRun()) {
            throw new \Exception('Exception!');
        }
        return true;
    }

    /**
     * @return bool
     */
    public function actionRun():bool
    {
        if (empty($this->mailObject)) {
            $this->logAddError(Yii::t('common', 'Обязательный объект $mailObject не задан.'));
            return false;
        }
        return $this->mailObject->send();
    }
}