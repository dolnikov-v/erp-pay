<?php

namespace app\jobs;

use app\modules\report\components\invoice\generators\InvoiceFinanceBuilder;
use app\modules\report\components\invoice\generators\InvoiceTableBuilder;
use app\modules\report\models\Invoice;
use Yii;

/**
 * Job для генерации файлов инвойса и их отправки
 * Class InvoiceGenerate
 * @package app\jobs
 */
class InvoiceGenerate extends BaseJob
{
    /**
     * @param \yii\queue\Queue $queue
     * @return bool|mixed
     * @throws \Exception
     */
    public function execute($queue)
    {
        if (!$this->actionRun()) {
            throw new \Exception('Exception!');
        }
        return true;
    }

//---------------------------------------------------------------------------------------

    /**
     * @var int|null
     */
    public $id = null;
    /**
     * @var array
     */
    public $emails = [];

    /**
     * @return bool
     */
    public function actionRun(): bool
    {
        if (!$this->id) {
            $this->logAdd(Yii::t('common', 'Не указан ID для invoice'));
            return false;
        }
        if ($invoice = Invoice::findOne($this->id)) {
            $oldStatus = $invoice->status;
            $invoice->status = Invoice::STATUS_GENERATING;
            $invoice->save(true, ['status']);
            try {
                $pdfGenerator = new InvoiceFinanceBuilder($invoice);
                $excelGenerator = new InvoiceTableBuilder($invoice);
                $invoice->invoice_filename = $pdfGenerator->generate();
                $invoice->orders_filename = $excelGenerator->generate();
                $invoice->status = Invoice::STATUS_GENERATED;
                if (!$invoice->save(true, ['invoice_filename', 'orders_filename', 'status'])) {
                    throw new \Exception($invoice->getFirstErrorAsString());
                };

                if ($this->emails) {
                    try {
                        $invoice->sendToEmail($this->emails);
                    } catch (\Exception $e) {
                        $this->logAdd("Exception : {$e->getMessage()}", ['message_type' => 'exception']);
                    }
                }

                return true;
            } catch (\Exception $e) {
                $invoice->status = $oldStatus;
                $invoice->save(true, ['status']);
                $this->logAdd("Exception : {$e->getMessage()}", ['message_type' => 'exception']);
            }
        }
        return false;
    }
}