<?php
namespace app\jobs;

use app\modules\administration\models\CrontabTaskLogAnswer;

/**
 * Trait CrontabLogTrait
 * @package app\jobs
 */
trait CrontabLogTrait
{
    public $log_id;

    /**
     * @param string $answer
     */
    protected function addError(string $answer)
    {
        $this->addAnswer($answer, CrontabTaskLogAnswer::STATUS_FAIL);
    }

    /**
     * @param string $answer
     * @param string $status [CrontabTaskLogAnswer::STATUS_SUCCESS | CrontabTaskLogAnswer::STATUS_FAIL]
     */
    protected function addAnswer(string $answer, string $status = CrontabTaskLogAnswer::STATUS_SUCCESS)
    {
        if (!empty($this->log_id)) {
            $logAnswer = new CrontabTaskLogAnswer();
            $logAnswer->log_id = $this->log_id;
            $logAnswer->status = $status;
            $logAnswer->answer = $answer;
            $logAnswer->save();
        }
    }
}