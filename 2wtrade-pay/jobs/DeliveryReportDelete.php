<?php

namespace app\jobs;

use app\models\Currency;
use app\modules\deliveryreport\models\DeliveryReport;
use app\modules\order\models\OrderFinanceFact;

/**
 * Class DeliveryReportDelete
 * @package app\jobs
 */
class DeliveryReportDelete extends BaseJob
{

    /**
     * @var integer
     */
    public $deliveryReportId;

    /**
     * @param \yii\queue\Queue $queue
     * @return bool
     * @throws \Exception
     */
    public function execute($queue)
    {
        return $this->run();
    }

    /**
     * @return bool
     * @throws \yii\db\Exception
     */
    public function run()
    {
        if (!$this->deliveryReportId) {
            $this->logAddError('No parameter $deliveryReportId');
            return false;
        }

        $deliveryReport = DeliveryReport::findOne($this->deliveryReportId);

        if (!$deliveryReport) {
            $this->logAddError('No list found ID = ' . $this->orderLogisticListId);
            return false;
        }

        if ($deliveryReport->status != DeliveryReport::STATUS_DELETE) {
            $this->logAddError('Incorrect status ' . $deliveryReport->status);
            return false;
        }

        $transaction = DeliveryReport::getDb()->beginTransaction();
        try {

            $facts = OrderFinanceFact::find()
                ->where(['delivery_report_id' => $deliveryReport->id])
                ->andWhere(['is not', 'payment_id', null])
                ->all();

            $usd = Currency::getUSD();

            foreach ($facts as $fact) {
                /** @var $fact OrderFinanceFact */

                if (!$fact->payment->balance) {
                    $fact->payment->balance = 0;
                }

                foreach (OrderFinanceFact::getCurrencyFields() as $field => $currencyField) {

                    if ($fact->$currencyField == $fact->payment->currency_id) {
                        // валюта в факте совпадает с валютой в платежке
                        $fact->payment->balance += $fact->$field;
                    } else {
                        $rateField = $field . '_currency_rate';

                        $rateToUSD = $fact->$rateField;
                        if ($rateToUSD) {
                            // есть курс к USD в факте
                            $addToBalanceUSD = $fact->$field / $rateToUSD;
                            $rate = $deliveryReport->getRate(
                                $usd->id,
                                $fact->payment->currency_id,
                                $fact->payment->paid_at, $fact->payment);
                            if ($rate) {
                                $fact->payment->balance += $addToBalanceUSD * $rate;
                            }
                        } else {
                            // нет курса в факте
                            $rate = $deliveryReport->getRate(
                                $fact->$currencyField,
                                $fact->payment->currency_id,
                                $fact->payment->paid_at, $fact->payment);
                            if ($rate) {
                                $fact->payment->balance += $fact->$field / $rate;
                            }
                        }
                    }
                }
                $fact->payment->save(true, ['balance']);
            }

            $deliveryReport->delete();

            $transaction->commit();

        } catch (\Exception $e) {
            $transaction->rollBack();
            $this->logAddError($e->getMessage());
            return false;
        }
        return true;
    }
}