<?php
namespace app\jobs;

use Yii;

use app\models\Notification;
use app\models\Product;
use app\models\User;
use app\modules\administration\models\CrontabTaskLogAnswer;
use app\modules\callcenter\models\CallCenter;
use app\modules\delivery\models\Delivery;
use app\modules\order\components\exporter\ExporterFactory;
use app\modules\order\models\OrderClarification;
use app\modules\order\models\OrderStatus;
use app\modules\order\models\search\OrderSearch;
use yii\swiftmailer\Message as SwiftMessage;


/**
 * Class SendOrderClarification
 * @package app\jobs
 */
class SendOrderClarification extends BaseJob
{
    use CrontabLogTrait;

    /**
     * @param \yii\queue\Queue $queue
     * @return bool
     * @throws \Exception
     */
    public function execute($queue)
    {
        if (!$this->actionRun()) {
            throw new \Exception('Exception!');
        }
        return true;
    }

//---------------------------------------------------------------------------------------
    /**
     * @var int|null
     */
    public $id = null;

    /**
     * Отправка на уточнение в КС писем с выгрузкой заказов в файл
     * @return bool
     */
    public function actionRun()
    {
        if (empty($this->id)) {
            return false;
        }

        $done = true;
        $answer = [];
        $records = 0;
        $files = 0;
        try {
            $products = Product::find()->collection();
            $deliveries = Delivery::find()->collection();
            $callCenters = CallCenter::find()->collection();

            $orderClarification = OrderClarification::find()->where(['id' => intval($this->id), 'generated_at' => null])->all();
            if ($orderClarification) {
                $success = true;
                $timeStart = time();
                if ($orderClarification->order_ids) {
                    $modelSearch = new OrderSearch(['countryId' => $orderClarification->country_id, 'userId' => $orderClarification->user_id]);
                    $searchParams = [
                        'NumberFilter' => [
                            'entity' => 'id',
                            'number' => $orderClarification->order_ids
                        ]
                    ];
                    $dataProvider = $modelSearch->search($searchParams);
                    $orderStatuses = OrderStatus::find()->collection($orderClarification->language);
                    $exporterConfig = [
                        'dataProvider' => $dataProvider,
                        'products' => $products,
                        'deliveries' => $deliveries,
                        'callCenters' => $callCenters,
                        'orderStatus' => $orderStatuses,
                    ];
                    $exporter = ExporterFactory::build($orderClarification->type, $exporterConfig);
                    if (!$exporter) {
                        $answer['errors'][] = 'unknown type ' . $orderClarification->type;
                        $success = false;
                    }
                    $result = false;
                    if ($success) {
                        $result = $exporter->sendFile($orderClarification);
                        if (!$result) {
                            $answer['errors'][] = 'file generation error';
                            $success = false;
                        }
                    }
                    if ($success && $result) {
                        $orderClarification->lines = $result['lines'];
                        $orderClarification->filename = $result['filename'];

                        if (trim($orderClarification->delivery_emails) != '') {
                            $user = User::findOne($orderClarification->user_id);

                            $message = new SwiftMessage();
                            $message->setFrom($user->email);
                            $message->setSubject($orderClarification->subject);
                            $message->setTextBody($orderClarification->message);
                            $message->attach($result['path'] . DIRECTORY_SEPARATOR . $result['filename'], ['fileName' => $result['filename']]);

                            $send = false;
                            foreach (explode(',', $orderClarification->delivery_emails) as $email) {
                                if (trim($email) != '') {
                                    $message->setTo($email);
                                    if ($message->send()) {
                                        $send = true;
                                    } else {
                                        $answer['errors'][] = 'send message error';
                                    }
                                }
                            }

                            if ($send) {
                                Yii::$app->notification->send(
                                    Notification::TRIGGER_SEND_ORDER_CLARIFICATION,
                                    [
                                        'emails' => $orderClarification->delivery_emails
                                    ], $orderClarification->country_id, $orderClarification->user_id
                                );
                                $orderClarification->sent_at = time();
                                unlink($result['path'] . DIRECTORY_SEPARATOR . $result['filename']);
                            }
                        }
                        else {
                            unlink($result['path'] . DIRECTORY_SEPARATOR . $result['filename']);
                        }
                    }
                }

                $orderClarification->generated_at = time();

                if ($orderClarification->save()) {
                    $records++;
                    if ($success) {
                        $files++;
                    }
                } else {
                    foreach ($orderClarification->getErrors() as $error) {
                        $answer['errors'][] = $error;
                    }
                }

                $answer['time'][] = 'ID = ' . $orderClarification->id . ' time ' . (time() - $timeStart) . ' s';
            }
        } catch (\Exception $e) {
            $this->addError($e->getMessage());
            $this->logAdd("Exception : {$e->getMessage()}", ['message_type' => 'exception']);
            $done = false;
        }
        if (isset($orderClarifications) && is_array($orderClarifications)) {
            $answer['records'] = sizeof($orderClarifications);
        }
        $answer['updated'] = $records;
        $answer['files'] = $files;

        $this->addAnswer(json_encode($answer), ($done ? CrontabTaskLogAnswer::STATUS_SUCCESS : CrontabTaskLogAnswer::STATUS_FAIL));
        $this->logAdd('Done : ' . json_encode($answer), ['message_type' => ($done ? 'info' : 'error')]);

        return $done;
    }
}