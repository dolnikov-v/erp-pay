<?php

namespace app\jobs;

use app\models\Notification;
use app\models\Product;
use app\modules\callcenter\models\CallCenter;
use app\modules\delivery\models\Delivery;
use app\modules\order\components\exporter\Exporter;
use app\modules\order\components\exporter\ExporterFactory;
use app\modules\order\models\OrderExport;
use app\modules\order\models\OrderStatus;
use app\modules\order\models\search\OrderFinanceFactSearch;
use app\modules\order\models\search\OrderSearch;
use Yii;
use yii\helpers\Url;

/**
 * Job для генерации файлов экспорта заказов
 * Class ExportOrder
 * @package app\jobs
 */
class ExportOrder extends BaseJob
{
    /**
     * @param \yii\queue\Queue $queue
     * @return bool
     * @throws \Exception
     */
    public function execute($queue)
    {
        if (!$this->actionRun()) {
            throw new \Exception('Exception!');
        }
        return true;
    }

//---------------------------------------------------------------------------------------

    /**
     * @var int|null
     */
    public $id = null;

    /**
     * @return bool
     */
    public function actionRun(): bool
    {
        if (empty($this->id)) {
            return false;
        }

        $done = true;
        $answer = [];
        $records = 0;
        $files = 0;

        try {
            $products = Product::find()->collection();
            $deliveries = Delivery::find()->collection();
            $callCenters = CallCenter::find()->collection();

            $orderExport = OrderExport::find()->where(['id' => intval($this->id), 'generated_at' => null])->one();
            if ($orderExport) {
                $timeStart = time();
                $success = true;
                $query = json_decode($orderExport->query, true);
                if ($query['export'] == Exporter::TYPE_FACT_EXCEL) {
                    $modelSearch = new OrderFinanceFactSearch(['countryId' => $orderExport->country_id, 'userId' => $orderExport->user_id]);
                } else {
                    $modelSearch = new OrderSearch(['countryId' => $orderExport->country_id, 'userId' => $orderExport->user_id]);
                }
                $dataProvider = $modelSearch->search($query);
                $orderStatuses = OrderStatus::find()->collection($orderExport->language);
                $exporterConfig = [
                    'dataProvider' => $dataProvider,
                    'products' => $products,
                    'deliveries' => $deliveries,
                    'callCenters' => $callCenters,
                    'orderStatus' => $orderStatuses,
                    'language' => $orderExport->language,
                    'timezoneId' => $orderExport->user->timezone->timezone_id ?? $orderExport->country->timezone->timezone_id
                ];
                $exporter = ExporterFactory::build($orderExport->type, $exporterConfig);
                if (!$exporter) {
                    $answer['errors'][] = 'unknown type ' . $orderExport->type;
                    $success = false;
                }
                $result = false;
                if ($success) {
                    $result = $exporter->sendFile($orderExport);
                    if (!$result) {
                        $answer['errors'][] = 'file generation error';
                        $success = false;
                    }
                }
                if ($success && $result) {
                    $orderExport->lines = $result['lines'];
                    $orderExport->filename = $result['filename'];

                    Yii::$app->notification->send(
                        Notification::TRIGGER_GENERATE_EXPORT_FILE,
                        [
                            'link' => Url::to(['/profile/files/download/', 'id' => $orderExport->id]),
                            'name' => $result['filename']
                        ], $orderExport->country_id, $orderExport->user_id
                    );

                    $orderExport->sent_at = time();
                }

                $orderExport->generated_at = time();

                if ($orderExport->save()) {
                    $records++;
                    if ($success) {
                        $files++;
                    }
                }
                $answer['time'][] = 'ID = ' . $orderExport->id . ' time ' . (time() - $timeStart) . ' s';
            }
        } catch (\Exception $e) {
            $this->logAdd("Exception : {$e->getMessage()}", ['message_type' => 'exception']);
            $done = false;
        }
        $answer['records'] = $records;
        $answer['files'] = $files;

        $this->logAdd('Done : ' . json_encode($answer), ['message_type' => ($done ? 'info' : 'error')]);

        return $done;
    }
}