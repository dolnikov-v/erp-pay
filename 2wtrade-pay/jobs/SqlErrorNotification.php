<?php
namespace app\jobs;

use app\models\SqlErrorLog;
use Yii;
use app\models\Notification;

/**
 * Class SqlErrorNotification
 * @package app\jobs
 */
class SqlErrorNotification extends BaseJob
{
    /**
     * @var SqlErrorLog
     */
    public $sqlErrorLog;

    /**
     * @param \yii\queue\Queue $queue
     * @return bool
     * @throws \Exception
     */
    public function execute($queue)
    {

        // число такиех же ошибок
        $countSimilar = SqlErrorLog::find()
            ->where(['sql_hash' => $this->sqlErrorLog->sql_hash])
            ->count();

        $notify = true;
        $messageText = '';
        if ($countSimilar) {
            $notify = false;

            // когда отправляли уведомления о такой же ошибке
            $notifiedAt = SqlErrorLog::find()
                ->select([
                    'notified_at' => 'notified_at'
                ])
                ->where(['sql_hash' => $this->sqlErrorLog->sql_hash])
                ->orderBy([
                    'notified_at' => SORT_DESC
                ])
                ->limit(1)
                ->scalar();

            if (!$notifiedAt) {
                $notifiedAt = 0;
            }

            // отправляем уведомления, если прошло более часа
            if ((time() - $notifiedAt) > 3600) {
                $notify = true;
                $messageText = 'ERROR #' . $countSimilar . '. ';
            }
        }
        $messageText .= $this->sqlErrorLog->message;

        if ($notify && Yii::$app->notification->send(
                Notification::TRIGGER_SQL_ERROR_EXCEPTION,
                [
                    'message' => $messageText
                ]
            )
        ) {
            SqlErrorLog::updateAll(['notified_at' => time()], ['sql_hash' => $this->sqlErrorLog->sql_hash]);
        }
        return true;
    }

}