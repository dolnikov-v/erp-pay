<?php
namespace app\jobs;

use app\modules\delivery\models\DeliveryChargesCalculator;
use app\modules\delivery\models\DeliveryContract;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\models\Order;
use app\modules\report\models\InvoiceOrder;

/**
 * Class DeliveryCalculateCharges
 * @package app\jobs
 */
class DeliveryCalculateCharges extends BaseJob
{
    use CrontabLogTrait;
    /**
     * @var integer
     */
    public $jobTelegramID;
    /**
     * @var string
     */
    public $jobName = 'DeliveryCalculateCharges';

    /**
     * @return float|int
     */
    public function getTtr()
    {
        return 30 * 60;
    }

    /**
     * @param \yii\queue\Queue $queue
     * @return bool
     */
    public function execute($queue)
    {
        if (!$this->actionRun()) {
            throw new \Exception('Exception!');
        }
        return true;
    }

    /**
     * @return bool
     */
    public function actionRun()
    {
        return $this->actionCalculateCharges();
    }

//---------------------------------------------------------------------------------------

    /**
     * @return array
     */
    public function optionAliases()
    {
        return [
            'id' => 'delivery_id',
        ];
    }

    /**
     * @var integer
     */
    public $delivery_id = null;

    /**
     * Расчет расходов на доставку для заказов
     * @var $cronLog callable|null
     * @return bool
     */
    public function actionCalculateCharges()
    {
        if (!$this->validateParams()) {
            return false;
        }

        date_default_timezone_set('UTC');

        $query = DeliveryContract::find()
            ->innerJoinWith('chargesCalculatorModel', false)
            ->where([DeliveryChargesCalculator::tableName() . '.active' => 1]);
        $query->with(['delivery', 'chargesCalculatorModel']);
        if ($this->delivery_id) {
            $query->andWhere([DeliveryContract::tableName() . '.delivery_id' => $this->delivery_id]);
        }

        $contracts = $query->all();

        if (is_array($contracts)) {
            foreach ($contracts as $contract) {
                try {
                    $chargesCalculator = $contract->getChargesCalculator();
                    $query = Order::find()
                        ->innerJoinWith(['deliveryRequest'])
                        ->joinWith(['financePrediction.invoiceOrder'], false)
                        ->where([
                            DeliveryRequest::tableName() . '.status' => [
                                DeliveryRequest::STATUS_IN_PROGRESS,
                                DeliveryRequest::STATUS_IN_LIST
                            ]
                        ]);
                    $query->andWhere(['is', InvoiceOrder::tableName() . '.invoice_id', null]);
                    $query->andWhere([DeliveryRequest::tableName() . '.delivery_id' => $contract->delivery_id]);
                    if ($contract->date_from) {
                        $query->andWhere([
                            '>=',
                            'COALESCE(' . DeliveryRequest::tableName() . '.second_sent_at, ' . DeliveryRequest::tableName() . '.sent_at, ' . DeliveryRequest::tableName() . '.created_at)',
                            strtotime('midnight', strtotime($contract->date_from))
                        ]);
                    }
                    if ($contract->date_to) {
                        $query->andWhere([
                            '<=',
                            'COALESCE(' . DeliveryRequest::tableName() . '.second_sent_at, ' . DeliveryRequest::tableName() . '.sent_at, ' . DeliveryRequest::tableName() . '.created_at)',
                            strtotime('23:59:59', strtotime($contract->date_to))
                        ]);
                    }
                    $query->with(['deliveryRequest', 'callCenterRequest', 'orderProducts', 'financePrediction']);

                    $chargesCalculator->calculateForBatch($query);
                } catch (\Throwable $e) {
                    $this->errors[] = \Yii::t('common', 'Ошибка: {error}', ['error' => $e->getMessage()]);
                    $this->addError(end($this->errors));
                    $this->logAddError(end($this->errors), ['delivery_id' => $contract->delivery_id]);
                }
            }
        }

        return true;
    }

    /**
     * Validate params
     * @return bool
     */
    private function validateParams()
    {
        if ($this->delivery_id) {
            $this->delivery_id = abs((int)$this->delivery_id);
        }
        return true;
    }
}