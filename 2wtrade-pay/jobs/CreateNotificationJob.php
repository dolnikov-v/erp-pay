<?php

namespace app\jobs;


use app\components\notification\EmailQueue;
use app\components\notification\SkypeQueue;
use app\components\notification\SmsQueue;
use app\components\notification\TelegramQueue;
use app\models\AuthAssignment;
use app\models\Notification as NotificationModel;
use app\models\NotificationRole;
use app\models\NotificationUser;
use app\models\User;
use app\models\UserCountry;
use app\models\UserNotification;
use app\models\UserNotificationSetting;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class CreateNotificationJob
 * @package app\jobs
 */
class CreateNotificationJob extends BaseJob
{
    /**
     * @var string
     */
    public $trigger;

    /**
     * @var null|array
     */
    public $params = null;

    /**
     * @var null|integer
     */
    public $countryId = null, $userId = null;

    /**
     * @param \yii\queue\Queue $queue
     * @return bool|mixed|null
     * @throws \Exception
     */
    public function execute($queue)
    {
        if (isset($params['count'])) {
            //Если текущая метрика меньше предыдущей - не отсылать уведомление
            if (!yii::$app->MetricTrendDetector->checkMetric($this->trigger, $this->params['count'])) {
                return null;
            }
        }

        /**
         * Проверка есть ли уведомление с таким тригером
         */
        if (!$notification = NotificationModel::find()->where(['trigger' => $this->trigger])->one()) {
            return false;
        };

        if ($notification->active != NotificationModel::NOT_ACTIVE) {
            if ($this->userId) {
                if (is_array($this->userId)) {
                    $listUsers = $this->userId;
                }
                else {
                    $listUsers[$this->userId] = $this->userId;
                }
            } else {
                $listUsers = $this->getListUsers($this->trigger, $this->countryId);
            }

            if (!empty($listUsers)) {
                $arrayForInsert = $this->prepareArrayForInsert($this->trigger, $this->countryId, $listUsers, $this->params);
                $arrayForInsert = $this->removeDoubles($arrayForInsert);        // удаляем дубляжи

                foreach ($arrayForInsert as $data) {
                    $model = new UserNotification($data);
                    if ($model->save()) {
                        SmsQueue::add($model->id);
                        EmailQueue::add($model->id);
                        TelegramQueue::add($model->id);
                        SkypeQueue::add($model->id);
                    }
                }
            }
        }
        return true;
    }


    /**
     * @param string $trigger
     * @param integer $countryId
     * @return array
     */
    protected function getListUsers($trigger, $countryId = null)
    {
        $roleByRole = NotificationRole::find()->select(['role'])->where(['trigger' => $trigger]);
        $usersByTriggerRole = AuthAssignment::find()
            ->select([AuthAssignment::tableName() . '.user_id'])
            ->leftJoin(User::tableName(), User::tableName() . '.id=' . AuthAssignment::tableName() . '.user_id')
            ->where(['IN', 'item_name', $roleByRole])
            ->andWhere(['NOT IN', User::tableName() . '.status', [User::STATUS_BLOCKED, User::STATUS_DELETED]])
            ->asArray()
            ->all();

        $usersActive = ArrayHelper::map($usersByTriggerRole, 'user_id', 'user_id');

        $notificationUser = NotificationUser::find()
            ->select([NotificationUser::tableName() . '.user_id', 'active'])
            ->leftJoin(User::tableName(), User::tableName() . '.id=' . NotificationUser::tableName() . '.user_id')
            ->where(['trigger' => $trigger])
            ->andWhere(['NOT IN', User::tableName() . '.status', [User::STATUS_BLOCKED, User::STATUS_DELETED]])
            ->asArray()
            ->all();

        foreach ($notificationUser as $u) {
            if ($u['active']) {
                if (!isset($usersActive[$u['user_id']])) {
                    $usersActive[$u['user_id']] = $u['user_id'];
                }
            } else {
                if (isset($usersActive[$u['user_id']])) {
                    unset($usersActive[$u['user_id']]);
                }
            }
        }

        $notificationUserSetting = UserNotificationSetting::find()
            ->select([UserNotificationSetting::tableName() . '.user_id', 'active'])
            ->leftJoin(User::tableName(), User::tableName() . '.id=' . UserNotificationSetting::tableName() . '.user_id')
            ->where(['trigger' => $trigger])
            ->andWhere(['NOT IN', User::tableName() . '.status', [User::STATUS_BLOCKED, User::STATUS_DELETED]])
            ->asArray()
            ->all();

        foreach ($notificationUserSetting as $u) {
            if ($u['active']) {
                if (!isset($usersActive[$u['user_id']])) {
                    $usersActive[$u['user_id']] = $u['user_id'];
                }
            } else {
                if (isset($usersActive[$u['user_id']])) {
                    unset($usersActive[$u['user_id']]);
                }
            }
        }

        $successUsers = [];

        if (!is_null($countryId)) {

            $usersByCountry = UserCountry::find()
                ->select('user_id')
                ->where(['IN', 'user_id', $usersActive])
                ->andWhere(['country_id' => $countryId])
                ->asArray()
                ->all();

            if ($usersByCountry) {
                $usersByCountry = ArrayHelper::getColumn($usersByCountry, 'user_id');
                $successUsers += $usersByCountry;
            }
            $superAdmins = Yii::$app->getAuthManager()->getUserIdsByRole(User::ROLE_SUPERADMIN);
            foreach ($superAdmins as $admin) {
                if (in_array($admin, $usersActive)) {
                    if (!in_array($admin, $successUsers)) {
                        $successUsers[] = $admin;
                    }
                }
            }
        } else {
            $successUsers = $usersActive;
        }

        return $successUsers;
    }

    /**
     * @param string $trigger
     * @param integer $countryId
     * @param array $users
     * @param array $params
     * @return array
     */
    protected function prepareArrayForInsert($trigger, $countryId, $users, $params = [])
    {
        $list = [];

        foreach ($users as $user) {
            $oneUser = [
                'user_id' => $user,
                'trigger' => $trigger,
                'country_id' => $countryId,
                'params' => json_encode($params, JSON_UNESCAPED_UNICODE),
                'created_at' => time(),
                'updated_at' => time(),
            ];

            $list[] = $oneUser;
        }

        return $list;
    }

    /**
     * Удаление дубляжей сообщения одному и тому же пользователю по роли и по пользователю
     * @param array $inArray
     * @return array $resArray
     */
    protected function removeDoubles($inArray)
    {
        $uniqueMessages = [];

        foreach ($inArray as $in) {
            foreach ($uniqueMessages as $unique) {
                if ($in['user_id'] == $unique['user_id'] &&
                    $in['trigger'] == $unique['trigger'] &&
                    $in['country_id'] == $unique['country_id'] &&
                    $in['params'] == $unique['params']
                ) {
                    continue 2; // скипает текущую итерацию во внешнем цикле
                }
            }
            $uniqueMessages[] = $in;
        }

        return $uniqueMessages;
    }
}