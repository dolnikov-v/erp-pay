<?php

namespace app\jobs;

use app\models\Notification;
use app\modules\report\components\debts\DebtsDaily;
use app\modules\report\components\debts\models\DebtsDailyRequest;
use yii\helpers\Url;

/**
 * Class DeliveryDebtsDynamic
 * @package app\jobs
 */
class DeliveryDebtsDynamic extends BaseJob
{

    /**
     * @var DebtsDailyRequest
     */
    public $debtsDailyRequest;

    /**
     * @var integer
     */
    public $userId;

    /**
     * @var string
     */
    public $backUrl;

    /**
     * @param \yii\queue\Queue $queue
     * @return mixed|void
     * @throws \Exception
     */
    public function execute($queue)
    {
        $debtsDaily = new DebtsDaily();
        $this->debtsDailyRequest->date_from = null; // для обновления с начала времен
        $baseData = $debtsDaily->loadBaseData($this->debtsDailyRequest);
        if ($baseData) {
            if ($debtsDaily->saveBaseData($baseData)) {
                \Yii::$app->notification->send(
                    Notification::TRIGGER_DELIVERY_DEBTS_DYNAMIC_RELOAD,
                    [
                        'link' => $this->backUrl
                    ], null, $this->userId
                );
            }
        }
    }
}