<?php
namespace app\jobs;

use app\components\ComponentTrait;
use yii\base\BaseObject;
use yii\queue\RetryableJobInterface;

/**
 * Class BaseJob
 * @package app\jobs
 */
abstract class BaseJob extends BaseObject implements RetryableJobInterface
{
    use ComponentTrait;

    /**
     * Количество попыток повторения в случае ошибки
     * @var int
     */
    private $numberAttempts = 1;

    /**
     * @var string
     */
    public $inputOptions = '';
    /**
     * Параметры
     * @var array
     */
    public $params = [];
    /**
     * Ошибки для телеграма
     * @var array
     */
    public $errors = [];

    /**
     * Параметры
     * @var string
     */
    public $jobName = 'Undefined name';

    /**
     * @var callable
     */
    protected $logger = null;

    /**
     * Вызов джобы
     * @param \yii\queue\Queue $queue
     * @return mixed
     */
    abstract public function execute($queue);

    /**
     * Максимальное время выполнения в секундах
     * @return float|int
     */
    public function getTtr()
    {
        return 15 * 60;
    }

    /**
     * Проверка попытки повторения при падении
     * @param int $attempt
     * @param \Exception|\Throwable $error
     * @return bool
     */
    public function canRetry($attempt, $error)
    {
        return $attempt < $this->numberAttempts;
    }

    /**
     * @param int $numberAttempts
     * @return BaseJob
     */
    public function setNumberAttempts(int $numberAttempts): BaseJob
    {
        $this->numberAttempts = $numberAttempts;
        return $this;
    }

    /**
     * @param string $message
     * @param array $tags ['tagName'=>'val', ...]
     */
    public function logAdd(string $message, array $tags = [])
    {
        $log = $this->getLogger();
        $log($message, $tags);
    }

    /**
     * @param string $message
     * @param array $tags
     */
    public function logAddError(string $message,  array $tags = [])
    {
        $this->logAdd($message, ['message_type' => 'error'] + $tags);
    }

    /**
     * @return callable
     * @throws \yii\base\InvalidConfigException
     */
    protected function getLogger()
    {
        if (is_null($this->logger)) {
            /** @var Logger $logger */
            $logger = \Yii::$app->get("processingLogger");
            $this->logger = $logger->getLogger([
                'type' => 'job',
                'job' => self::className(),
                'process_id' => getmypid(),
            ]);
        }
        return $this->logger;
    }
}