<?php
namespace app\jobs;

use app\models\Country;
use app\models\Currency;
use app\models\CurrencyRateHistory;
use app\models\VoipExpense;
use Yii;
use yii\helpers\ArrayHelper;
use yii\httpclient\Client;
use yii\httpclient\Response;
use app\models\Notification;

/**
 * Class UpdateVoipExpense
 * @package app\jobs
 */
class UpdateVoipExpense extends BaseJob
{
    use CrontabLogTrait;
    public $logger;
    /**
     * @var integer
     */
    public $jobTelegramID;
    /**
     * @var string
     */
    public $jobName = 'VoipExpense';

    /**
     * @param \yii\queue\Queue $queue
     * @return bool
     */
    public function execute($queue)
    {
        if (!$this->actionRun()) {
            throw new \Exception('Exception!');
        }
        return true;
    }

    /**
     * @return bool
     */
    public function actionRun()
    {
        return $this->voipExpense();
    }

//---------------------------------------------------------------------------------------
    const COLUMN_DIRECTION = 'country';
    const COLUMN_ASR = 'asr';
    const COLUMN_TOTAL_COSTS = 'cost';

    private
        $loginUrl = 'https://my.zorra.com/api/auth/login',
        $userName = '2wtrade@zorra.com',
        $password = '16csj78sl69as',
        $statUrl = 'https://my.zorra.com/api/v1/voice/stats/general',
        $source = 'Main',
        $token,
        $notification = true;

    public
        $startDate = null,
        $endDate = null,
        $specificCountryId = null;

    /**
     * @var Client $client
     */
    private $client;

    /**
     * @param $actionID
     * @return array
     */
    public function options($actionID)
    {
        return ['startDate', 'endDate', 'specificCountryId'];
    }

    /**
     * @return array
     */
    public function optionAliases()
    {
        return [
            'from' => 'startDate',
            'to' => 'endDate',
            'country_id' => 'specificCountryId',
        ];
    }

    /**
     * Parse information from https://my.zorra.com/voice/stats/byCountry
     * @return bool
     */
    public function voipExpense()
    {
        if (!$this->validateParams()) {
            return false;
        }
        $launchedAt = time();
        try {
            $countries = Country::find()->active()->own()->customCollection('id', 'name_en');
            $this->client = new Client();
            if (!$this->auth()) {
                $this->errors[] = Yii::t('common', 'Ошибка авторизации в Api.');
                $this->addError(end($this->errors));
                $this->logAddError(end($this->errors));
                return false;
            }
            $currency = Currency::findByCharCode('EUR');
            if (!$currency || empty($currency->id)) {
                $this->errors[] = Yii::t('common', 'Валюта не найдена.');
                $this->addError(end($this->errors));
                $this->logAddError(end($this->errors));
                return false;
            }
            //Если указана страна то проверяем только в ней
            if ($this->specificCountryId) {
                $countries = [
                    $this->specificCountryId => $countries[$this->specificCountryId]
                ];
            }
            $startDate = $this->startDate;
            $endDate = $this->endDate;
            if ($startDate > $endDate) {
                list($startDate, $endDate) = [$endDate, $startDate];
            }
            $date = $startDate;
            while (strtotime($date) <= strtotime($endDate)) {
                $rate = null;
                try {
                    $rate = CurrencyRateHistory::convertValueToCurrency($currency->id, $date, 1, true);
                } catch (\Exception $e) {
                    $this->errors[] = Yii::t('common', 'Отсутствует курс валюты {char} на {date}', ['char' => $currency->char_code, 'date' => $date]);
                    $this->addError(end($this->errors));
                    $this->logAddError(end($this->errors));
                    if ($this->notification) {
                        return false;
                    }
                }
                if ($rate) {
                    $paymentsData = $this->getExpenses($date);
                    foreach ($countries as $countryId => $nameEn) {
                        if (isset($paymentsData[$nameEn])) {
                            $payment = VoipExpense::find()
                                ->where(['date_stat' => $date, 'country_id' => $countryId])
                                ->one();
                            if (!$payment) {
                                $payment = new VoipExpense();
                            }
                            $payment->amount = $paymentsData[$nameEn][self::COLUMN_TOTAL_COSTS] ?? 0;
                            $payment->amount_usd = ($paymentsData[$nameEn][self::COLUMN_TOTAL_COSTS] && $rate) ? $paymentsData[$nameEn][self::COLUMN_TOTAL_COSTS] * $rate : null;
                            $payment->date_stat = $date;
                            $payment->asr = $paymentsData[$nameEn][self::COLUMN_ASR] ?? 0;
                            $payment->country_id = $countryId;
                            $payment->launched_at = $launchedAt;
                            if (!$payment->save()) {
                                $error = Yii::t('common', "Ошибка сохранения {error}", ['error' => print_r($payment->getErrors(), true)]);
                                $this->errors[] = Yii::t('common', "Ошибка сохранения country_id={country_id} date_stat={date_stat}", ['country_id' => $countryId, 'date_stat' => $date]);
                                $this->addError($error);
                                $this->logAddError($error);
                            };
                        }
                    }
                }
                $date = date("Y-m-d", strtotime("+1 day", strtotime($date)));
            }

            /**
             * Оповещение на отсутствие звонков в течении 3-х дней
             */
            if ($this->notification) {
                $connection = Yii::$app->getDb();
                $command = $connection->createCommand("SELECT c.id, c.name, SUM(w.asr) AS sasr FROM country c
                LEFT JOIN voip_expense w ON w.country_id = c.id AND w.date_stat >= :start_date AND w.date_stat <= :end_date
                WHERE c.active = 1 AND c.is_partner = 0 AND c.is_stop_list = 0
                GROUP BY c.id
                HAVING sasr IS NULL OR sasr = 0",
                    [
                        ':start_date' => date('Y-m-d', strtotime('-3 day')),
                        ':end_date' => date('Y-m-d')
                    ]
                );
                $notificationCountries = $command->queryAll();
                if ($notificationCountries and is_array($notificationCountries)) {
                    foreach ($notificationCountries as $country) {
                        if (!empty($country['id']) && !empty($country['name'])) {
                            Yii::$app->notification->send(
                                Notification::TRIGGER_CALL_CENTER_NO_STATISTIC_LAST_THREE_DAYS,
                                [
                                    'name' => $country['name'],
                                ]
                                , $country['id']);
                        }
                    }
                }
            }
//          ------------------------------------------------------
        } catch (\Exception $e) {
            $this->errors[] = Yii::t('common', 'Неизвестная ошибка!') . ' ' . Yii::t('common', 'Подробнее смотрите в логах.');
            $this->addError($e->getMessage());
            $this->logAddError($e->getMessage());
            return false;
        }
        return true;
    }

    /**
     * @return bool
     */
    private function auth()
    {
        $post = [
            'email' => $this->userName,
            'password' => $this->password,
        ];

        /**
         * @var Response $response
         */
        $response = $this->client->createRequest()
            ->setHeaders([
                'Content-Type' => 'application/x-www-form-urlencoded'
            ])
            ->setMethod('post')
            ->setUrl($this->loginUrl)
            ->setOptions(['sslVerifyPeer' => false])
            ->setData($post)
            ->send();
        $data = $response->getContent();
        if (!empty($data) && is_array($data = json_decode($data, true)) && !empty($data['access_token']) && !empty($data['token_type'])) {
            $this->token = $data['token_type'] . ' ' . $data['access_token'];
            return true;
        }
        $this->errors[] = Yii::t('common', 'Авторизация не выполнена');
        $this->addError(end($this->errors));
        $this->logAddError(end($this->errors));
        return false;
    }

    /**
     * Забирает платежи один раз в день
     * @param $date
     * @return array
     */
    private function getExpenses($date)
    {
        $post = [
            'date_from' => date('Y-m-d\TH:i:s\Z', strtotime($date)),
            'date_to' => date('Y-m-d\TH:i:s\Z', strtotime("+1 day -1 seconds", strtotime($date))),
            'source' => $this->source,
            'group_by' => 'country',
        ];
        $post = json_encode($post);
        /**
         * @var Response $response
         */
        $response = $this->client->createRequest()
            ->setHeaders([
                'Content-Type' => 'application/json',
                'Authorization' => $this->token,
            ])
            ->setMethod('post')
            ->setUrl($this->statUrl)
            ->setContent($post)
            ->setOptions(['sslVerifyPeer' => false])
            ->send();
        $data = json_decode($response->getContent(), true);
        if (!$data || empty($data['data'])) {
            $data = [];
            $this->errors[] = Yii::t('common', 'Данные, полученные от Api, не соответствуют формату.') . ' ' . Yii::t('common', 'Подробнее смотрите в логах.');
            $error = Yii::t('common', 'Данные, полученные от Api, не соответствуют формату.')
                . PHP_EOL . 'Отправленные данные ->' . PHP_EOL  . print_r($post, true) . 'Ответ ->' . PHP_EOL
                . json_encode($response->getContent(), JSON_UNESCAPED_UNICODE);
            $this->addError($error);
            $this->logAddError($error);
        } else {
            $data = $data['data'];
        }
        $result = ArrayHelper::index($data, self::COLUMN_DIRECTION);
        return $result;
    }

    /**
     * Validate console params
     * @return bool
     */
    private function validateParams()
    {
        $success = true;
        if ($this->startDate && !strtotime($this->startDate)) {
            $this->errors[] = Yii::t('common', "Неверный формат даты - from!");
            $this->addError(end($this->errors));
            $this->logAddError(end($this->errors));
            $success = false;
        } elseif (empty($this->startDate)) {
            $this->startDate = date('Y-m-d', strtotime('-1 day'));
        } else {
            $this->notification = false;
        }
        if ($this->endDate && !strtotime($this->endDate)) {
            $this->errors[] = Yii::t('common', "Неверный формат даты - to!");
            $this->addError(end($this->errors));
            $this->logAddError(end($this->errors));
            $success = false;
        } elseif (empty($this->endDate)) {
            $this->endDate = date('Y-m-d');
        }

        return $success;
    }
}