<?php

namespace app\helpers;

use yii\base\BaseObject;

/**
 * Class FontAwesome
 * @package app\helpers
 */
class FontAwesome extends BaseObject
{
    const CAR = 'fa-car';
    const PHONE = 'fa-phone';
    const PHONE_SQUARE = 'fa-phone-square';
    const DATABASE = 'fa-database';
    const CLOUD = 'fa-cloud';
    const GIFT = 'fa-gift';
    const PAPER_PLANE_O = 'fa-paper-plane-o';
    const TRUCK = 'fa-truck';
    const GLOBE = 'fa-globe';
    const LANGUAGE = 'fa-language';
    const TASKS = 'fa-tasks';
    const WRENCH = 'fa-wrench';
    const COG = 'fa-cog';
    const FILE_EXCEL_O = 'fa-file-excel-o';
    const FILE_IMAGE_O = 'fa-file-image-o';
    const FILE_ARCHIVE_O = 'fa-file-archive-o';
    const FILE_TEXT = 'fa-file-text';
    const QR = 'fa-qrcode';
    const BAR_CODE = 'fa-barcode';
    const USD = 'fa-usd';
    const MONEY = 'fa-money';
    const CUBE = 'fa-cube';
    const LIST = 'fa-list';
    const REFRESH = 'fa-refresh';
    const REFRESH_SPIN = 'fa-refresh fa-spin';
    const SPINNER = 'fa-spinner fa-spin';
    const SKYPE = 'fa-skype';
    const AT = 'fa-at';
    const DOWNLOAD = 'fa-download';
    const SHOPPING_CART = 'fa-shopping-cart';
    const CHART_AREA = 'fa-area-chart';
    const BAR_CHART = 'fa-bar-chart';
}
