<?php

namespace app\helpers;

/**
 * Class StringHelper
 * @package app\helpers
 */
class StringHelper
{
    /**
     * Убирает все пробелы в начале и в конце строки
     * @param string $str
     * @return null|string|string[]
     */
    public static function trim(string $str)
    {
        return preg_replace("/(^\s+)|(\s+$)/us", "", $str);
    }
}