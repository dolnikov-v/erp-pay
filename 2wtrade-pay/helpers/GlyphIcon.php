<?php
namespace app\helpers;

use yii\base\BaseObject;

/**
 * Class GlyphIcon
 * @package app\helpers
 */
class GlyphIcon extends BaseObject
{
    const GIFT = 'glyphicon-gift';
    const EARPHONE = 'glyphicon-earphone';
    const PHONE_ALT = 'glyphicon-phone-alt';
    const GLOBE = 'glyphicon-globe';
    const CLOUD = 'glyphicon-cloud';
    const TASKS = 'glyphicon-tasks';
    const PICTURE = 'glyphicon-picture';
    const PHONE = 'glyphicon-phone';
}
