<?php

namespace app\helpers\math;

use app\widgets\Label;
use yii\base\BaseObject;
use optimistex\expression\Expression;
use Exception;

class Formula extends BaseObject
{
    /**
     * @param string $formula
     * @return bool
     */
    public static function check($formula)
    {
        $equation = self::normalize($formula);
        $number = '((?:0|[1-9]\d*)(?:\.\d*)?(?:[eE][+\-]?\d+)?|pi|π)'; // What is a number
        $functions = '(?:sinh?|cosh?|tanh?|acosh?|asinh?|atanh?|exp|log(10)?|deg2rad|rad2deg|sqrt|pow|abs|intval|ceil|floor|round|(mt_)?rand|gmp_fact)'; // Allowed PHP functions
        $operators = '[\/*\^\+-,]'; // Allowed math operators
        $variables = '{([a-zA-Z]+[a-zA-Z0-9._-]+)}';
        $regexp = '/^([+-]?('.$number.'|'.$variables.'|'.$functions.'\s*\((?1)+\)|\((?1)+\))(?:'.$operators.'(?1))?)+$/'; // Final regexp, heavily using recursive patterns

        return preg_match($regexp, $equation) ?: false;
    }

    /**
     * @param string $formula
     * @return string
     */
    private static function normalize($formula)
    {
        $equation = preg_replace('/\s+/', '', $formula);
        return preg_replace('/' . PHP_EOL  . '/', '', $equation);
    }

    /**
     * @param string $formula
     * @param array $variables
     * @return mixed
     */
    public static function execute($formula, $variables)
    {
        $result = null;
        try {
            $e = new Expression();
            foreach (self::getVariables($formula) as $code) {
                $value = isset($variables[$code]) ? $variables[$code] : null;
                $variable = preg_replace('/[^\w\d_]/', '_', $code);
                $formula = str_replace(self::prepareVariable($code), $variable, $formula);
                $e->evaluate("{$variable} = {$value}");
            }
            $result = $e->evaluate(self::normalize($formula));
        } catch (Exception $exception) {

        }
        return $result;
    }

    /**
     * @param string $formula
     * @param bool $withBracket
     * @return array
     */
    public static function getVariables($formula, $withBracket = false)
    {
        $variables = [];
        if ($formula) {
            preg_match_all('/{([a-zA-Z]+[a-zA-Z0-9._-]+)}/', $formula, $matches);
            $index = $withBracket ? 0 : 1;
            if (isset($matches[$index]) && !empty($matches[$index])) {
                $variables = $matches[$index];
            }
        }

        return $variables;
    }

    /**
     * @param string $formula
     * @param array $variables
     * @param bool $useLabel
     * @return mixed
     */
    public static function getFormattedFormula($formula, $variables, $useLabel = true)
    {
        foreach ($variables as $name => $label) {
            $label = ($useLabel) ?
                Label::widget([
                    'label' => $label,
                    'style' => Label::STYLE_WARNING,
                ]) : $label;

            $formula = str_replace('{' . $name . '}', $label, $formula);
        }

        return $formula;
    }

    /**
     * @param string $variable
     * @return string
     */
    public static function prepareVariable($variable)
    {
        return '{' . trim($variable, '{}') . '}';
    }

    /**
     * @param string[] $variables
     * @param string $variable
     * @return array
     */
    protected static function buildVariableDependencies($variables, $variable)
    {
        $items = [];

        $list = (isset($variables[$variable])) ? $variables[$variable] : [];
        foreach ($list as $item) {
            // TODO: Нужно сделать обработку ситуация когда переменная ссылается сама на себя, пока просто пропускаем
            if ($variable == $item) {
                continue;
            }

            $items[] = $item;
            if ($subItems = self::buildVariableDependencies($variables, $item)) {
                $items = array_merge($items, $subItems);
            }
        }

        return $items;
    }

    /**
     * @param string[] $formulas
     * @return array
     */
    public static function findDependentVariables($formulas)
    {
        $ref = [];
        foreach ($formulas as $variable => $formula) {
            $ref[$variable] = self::getVariables($formula);
        }

        $list = [];
        foreach (array_keys($ref) as $variable) {
            $list[$variable] = self::buildVariableDependencies($ref, $variable);
        }

        return $list;
    }

    /**
     * @param string[] $formulas
     * @param string
     * @return array
     */
    public static function getDependentVariables($formulas, $variable)
    {
        $refs = self::findDependentVariables($formulas);
        return (isset($refs[$variable])) ? $refs[$variable] : [];
    }

    /**
     * @param string[] $formulas
     * @param string
     * @return array
     */
    public static function getIndependentVariables($formulas, $variable)
    {
        $variables = [];
        foreach ($formulas as $variable => $formula) {
            $variables = array_merge($variables, self::getVariables($formula));
        }

        $refs = self::findDependentVariables($formulas);
        if (isset($refs[$variable])) {
            $ref = array_diff($variables, $refs[$variable]);
        } else {
            $ref = $variable;
        }

        return $ref;
    }
}