<?php
namespace app\helpers\i18n;

use Yii;

/**
 * Class Formatter
 * @package app\helpers\i18n
 */
class Formatter extends Base
{
    /**
     * @var array
     */
    private static $dateFormats = [
        'ru-RU' => 'php:d.m.Y',
        'en-US' => 'php:m/d/Y',
        'es-ES' => 'php:d.m.Y',
        'tr-TR' => 'php:d.m.Y',
    ];
    /**
     * @var array
     */
    private static $dateFormatsStrict = [
        'ru-RU' => 'php:!d.m.Y',
        'en-US' => 'php:!m/d/Y',
        'es-ES' => 'php:!d.m.Y',
        'tr-TR' => 'php:!d.m.Y',
    ];

    /**
     * @var array
     */
    private static $monthFormats = [
        'ru-RU' => 'php:m.Y',
        'en-US' => 'php:m/Y',
        'es-ES' => 'php:m.Y',
        'tr-TR' => 'php:m.Y',
    ];

    /**
     * @var array
     */
    private static $monthFormatsStrict = [
        'ru-RU' => 'php:!m.Y',
        'en-US' => 'php:!m/Y',
        'es-ES' => 'php:!m.Y',
        'tr-TR' => 'php:!m.Y',
    ];

    /**
     * @var array
     */
    private static $timeFormats = [
        'ru-RU' => 'php:H:i',
        'en-US' => 'php:H:i',
        'es-ES' => 'php:H:i',
        'tr-TR' => 'php:H:i',
    ];

    /**
     * @var array
     */
    private static $datetimeFormats = [
        'ru-RU' => 'php:d.m.Y H:i',
        'en-US' => 'php:m/d/Y H:i',
        'es-ES' => 'php:d.m.Y H:i',
        'tr-TR' => 'php:d.m.Y H:i',
    ];

    /**
     * @var array
     */
    private static $fullTimeFormats = [
        'ru-RU' => 'php:H:i:s',
        'en-US' => 'php:H:i:s',
        'es-ES' => 'php:H:i:s',
        'tr-TR' => 'php:H:i:s',
    ];

    /**
     * @var array
     */
    private static $mysqlDateFormats = [
        'ru-RU' => '%d.%m.%Y',
        'en-US' => '%m/%d/%Y',
        'es-ES' => '%d.%m.%Y',
        'tr-TR' => '%d.%m.%Y',
    ];

    /**
     * @var array
     */
    private static $mysqlMonthFormats = [
        'ru-RU' => '%m.%Y',
        'en-US' => '%m/%Y',
        'es-ES' => '%m.%Y',
        'tr-TR' => '%m.%Y',
    ];

    /**
     * @var array
     */
    private static $shortDateFormats = [
        'ru-RU' => 'php:d.m',
        'en-US' => 'php:m/d',
        'es-ES' => 'php:d.m',
        'tr-TR' => 'php:d.m',
    ];

    /**
     * @var array
     */
    private static $decimalSeparators = [
        'ru-RU' => ',',
        'en-US' => '.',
        'es-ES' => ',',
        'tr-TR' => ',',
    ];

    /**
     * @var array
     */
    private static $thousandSeparators = [
        'ru-RU' => ' ',
        'en-US' => ',',
        'es-ES' => '.',
        'tr-TR' => '.',
    ];

    /**
     * @param null $locale
     * @return mixed|string
     */
    public static function getDateFormat($locale = null)
    {
        return self::getFormat(self::$dateFormats, $locale);
    }

    /**
     * @param null $locale
     * @return mixed|string
     */
    public static function getDateFormatStrict($locale = null)
    {
        return self::getFormat(self::$dateFormatsStrict, $locale);
    }

    /**
     * @param null $locale
     * @return mixed|string
     */
    public static function getTimeFormat($locale = null)
    {
        return self::getFormat(self::$timeFormats, $locale);
    }

    /**
     * @param null $locale
     * @return mixed|string
     */
    public static function getDatetimeFormat($locale = null)
    {
        return self::getFormat(self::$datetimeFormats, $locale);
    }

    /**
     * @param null $locale
     * @return mixed|string
     */
    public static function getFullTimeFormat($locale = null)
    {
        return self::getFormat(self::$fullTimeFormats, $locale);
    }

    /**
     * @param null $locale
     * @return mixed|string
     */
    public static function getMysqlDateFormat($locale = null)
    {
        return self::getFormat(self::$mysqlDateFormats, $locale);
    }

    /**
     * @param null $locale
     * @return mixed|string
     */
    public static function getMysqlMonthFormat($locale = null)
    {
        return self::getFormat(self::$mysqlMonthFormats, $locale);
    }

    /**
     * @param null $locale
     * @return mixed|string
     */
    public static function getMonthFormat($locale = null)
    {
        return self::getFormat(self::$monthFormats, $locale);
    }

    /**
     * @param null $locale
     * @return mixed|string
     */
    public static function getMonthFormatStrict($locale = null)
    {
        return self::getFormat(self::$monthFormatsStrict, $locale);
    }

    /**
     * @param null $locale
     * @return mixed|string
     */
    public static function getShortDateFormat($locale = null)
    {
        return self::getFormat(self::$shortDateFormats, $locale);
    }

    /**
     * @param null $locale
     * @return mixed|string
     */
    public static function getDecimalSeparator($locale = null)
    {
        return self::getFormat(self::$decimalSeparators, $locale);
    }

    /**
     * @param null $locale
     * @return mixed|string
     */
    public static function getThousandSeparator($locale = null)
    {
        return self::getFormat(self::$thousandSeparators, $locale);
    }
}
