<?php
namespace app\helpers\i18n;

use Yii;

/**
 * Class Base
 * @package app\helpers\i18n
 */
class Base
{
    /**
     * @param $formats
     * @param null $language
     * @return mixed|string
     */
    protected static function getFormat($formats, $language = null)
    {
        if (!$language) {
            $language = Yii::$app->language;
        }

        return $formats[$language] ?? array_shift($formats);
    }
}
