<?php
namespace app\helpers\i18n;

use Yii;

/**
 * Class DateTimePicker
 * @package app\helpers\i18n
 */
class DateTimePicker extends Base
{
    /**
     * @var array
     */
    private static $localeFormats = [
        'ru-RU' => 'ru',
        'en-US' => 'en',
        'es-ES' => 'es',
        'tr-TR' => 'tr',
    ];

    /**
     * @return string
     */
    public static function getDateFormat()
    {
        return Moment::getDateFormat();
    }

    /**
     * @return string
     */
    public static function getMonthFormat()
    {
        return Moment::getMonthFormat();
    }


    /**
     * @return string
     */
    public static function getTimeFormat()
    {
        return Moment::getTimeFormat();
    }

    /**
     * @return string
     */
    public static function getLocale()
    {
        return self::getFormat(self::$localeFormats);
    }
}
