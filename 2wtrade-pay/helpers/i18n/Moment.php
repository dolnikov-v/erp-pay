<?php
namespace app\helpers\i18n;

use Yii;

/**
 * Class Moment
 * @package app\helpers\i18n
 */
class Moment extends Base
{
    /**
     * @var array
     */
    private static $dateFormats = [
        'ru-RU' => 'DD.MM.YYYY',
        'en-US' => 'MM/DD/YYYY',
        'es-ES' => 'DD.MM.YYYY',
        'tr-TR' => 'DD.MM.YYYY',
    ];

    /**
     * @var array
     */
    private static $timeFormats = [
        'ru-RU' => 'HH:mm',
        'en-US' => 'HH:mm',
        'es-ES' => 'HH:mm',
        'tr-TR' => 'HH:mm',
    ];

    /**
     * @var array
     */
    private static $datetimeFormats = [
        'ru-RU' => 'DD.MM.YYYY HH:mm',
        'en-US' => 'MM/DD/YYYY HH:mm',
        'es-ES' => 'DD.MM.YYYY HH:mm',
        'tr-TR' => 'DD.MM.YYYY HH:mm',
    ];

    /**
     * @var array
     */
    private static $fullTimeFormats = [
        'ru-RU' => 'HH:mm:ss',
        'en-US' => 'HH:mm:ss',
        'es-ES' => 'HH:mm:ss',
        'tr-TR' => 'HH:mm:ss',
    ];

    /**
     * @var array
     */
    private static $monthFormats = [
        'ru-RU' => 'MM.YYYY',
        'en-US' => 'MM/YYYY',
        'es-ES' => 'MM.YYYY',
        'tr-TR' => 'MM.YYYY',
    ];

    /**
     * @return string
     */
    public static function getDateFormat()
    {
        return self::getFormat(self::$dateFormats);
    }

    /**
     * @return string
     */
    public static function getTimeFormat()
    {
        return self::getFormat(self::$timeFormats);
    }

    /**
     * @return string
     */
    public static function getDatetimeFormat()
    {
        return self::getFormat(self::$datetimeFormats);
    }

    /**
     * @return string
     */
    public static function getFullTimeFormat()
    {
        return self::getFormat(self::$fullTimeFormats);
    }

    /**
     * @return string
     */
    public static function getMonthFormat()
    {
        return self::getFormat(self::$monthFormats);
    }
}
