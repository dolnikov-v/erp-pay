<?php
namespace app\helpers;

use yii\base\BaseObject;
use app\modules\order\models\OrderStatus;

/**
 * Class PayStatus
 * @package app\helpers
 */
class PayStatus extends BaseObject
{
    const STATUS_IN_PROCESSING = '0'; // В процессе
    const STATUS_ACCEPTED = '1'; // Утвержден
    const STATUS_DISCLAIMER = '2'; // Отказ из КЦ
    const STATUS_DOUBLE = '3'; // Дубль
    const STATUS_TRASH = '4'; // Испорчен
    const STATUS_RECALL = '5'; // Перезвонить
    const STATUS_SHIPPED = '6'; // Отправлен
    const STATUS_DELIVERED = '7'; // Доставлен (не используется)
    const STATUS_BUYOUT = '8'; // Выкуп
    const STATUS_NOT_BUYOUT = '9'; // Отказ

    public static $statuses = [
        self::STATUS_IN_PROCESSING => '0',
        self::STATUS_ACCEPTED => '1',
        self::STATUS_DISCLAIMER => '2',
        self::STATUS_DOUBLE => '3',
        self::STATUS_TRASH => '4',
        self::STATUS_RECALL => '5',
        self::STATUS_SHIPPED => '6',
        self::STATUS_DELIVERED => '7',
        self::STATUS_BUYOUT => '8',
        self::STATUS_NOT_BUYOUT => '9',
    ];

    public static $mapStatuses = array(
        OrderStatus::STATUS_SOURCE_LONG_FORM => self::STATUS_IN_PROCESSING,
        OrderStatus::STATUS_SOURCE_SHORT_FORM => self::STATUS_IN_PROCESSING,
        OrderStatus::STATUS_CC_POST_CALL => self::STATUS_IN_PROCESSING,
        OrderStatus::STATUS_CC_RECALL => self::STATUS_RECALL,
        OrderStatus::STATUS_CC_FAIL_CALL => self::STATUS_TRASH,
        OrderStatus::STATUS_CC_APPROVED => self::STATUS_ACCEPTED,
        OrderStatus::STATUS_CC_REJECTED => self::STATUS_DISCLAIMER,
        OrderStatus::STATUS_LOG_ACCEPTED => self::STATUS_SHIPPED,
        OrderStatus::STATUS_LOG_GENERATED => self::STATUS_SHIPPED,
        OrderStatus::STATUS_LOG_SET => self::STATUS_SHIPPED,
        OrderStatus::STATUS_LOG_PASTED => self::STATUS_SHIPPED,
        OrderStatus::STATUS_DELIVERY_ACCEPTED => self::STATUS_SHIPPED,
        OrderStatus::STATUS_DELIVERY_LEFT_WAREHOUSE => self::STATUS_SHIPPED,
        OrderStatus::STATUS_DELIVERY => self::STATUS_SHIPPED,
        OrderStatus::STATUS_DELIVERY_BUYOUT => self::STATUS_BUYOUT,
        OrderStatus::STATUS_DELIVERY_DENIAL => self::STATUS_NOT_BUYOUT,
        OrderStatus::STATUS_DELIVERY_RETURNED => self::STATUS_NOT_BUYOUT,
        OrderStatus::STATUS_FINANCE_MONEY_RECEIVED => self::STATUS_BUYOUT,
        OrderStatus::STATUS_DELIVERY_REJECTED => self::STATUS_SHIPPED, // @todo: была внесена правка STATUS_NOT_BUYOUT -> STATUS_SHIPPED
        OrderStatus::STATUS_DELIVERY_LEFT_OUR_WAREHOUSE => self::STATUS_SHIPPED,
        OrderStatus::STATUS_DELIVERY_REDELIVERY => self::STATUS_SHIPPED,
        OrderStatus::STATUS_DELIVERY_LEFT_THEIR_WAREHOUSE => self::STATUS_SHIPPED,
        OrderStatus::STATUS_LOGISTICS_REJECTED => self::STATUS_NOT_BUYOUT,
        OrderStatus::STATUS_DELIVERY_DELAYED => self::STATUS_SHIPPED,
        OrderStatus::STATUS_CC_TRASH => self::STATUS_TRASH,
        OrderStatus::STATUS_CC_DOUBLE => self::STATUS_DOUBLE,
        OrderStatus::STATUS_DELIVERY_REFUND => self::STATUS_NOT_BUYOUT,
        OrderStatus::STATUS_LOGISTICS_ACCEPTED => self::STATUS_NOT_BUYOUT,
        OrderStatus::STATUS_DELIVERY_LOST => self::STATUS_TRASH,
        OrderStatus::STATUS_CC_INPUT_QUEUE => self::STATUS_IN_PROCESSING,
        OrderStatus::STATUS_DELIVERY_PENDING => self::STATUS_SHIPPED,
    );

    /**
     * @param array|integer $statuses
     * @return array
     */
    public static function getPayStatusesByStatuses($statuses)
    {
        $result = [];

        if (!is_array($statuses)) {
            $statuses = [$statuses];
        }

        foreach (self::$mapStatuses as $status => $payStatus) {
            if (in_array($status, $statuses)) {
                $result[] = $payStatus;
            }
        }

        return array_unique($result);
    }

    /**
     * @param array|integer $payStatuses
     * @return array
     */
    public static function getStatusesByPayStatuses($payStatuses)
    {
        $result = [];

        if (!is_array($payStatuses)) {
            $payStatuses = [$payStatuses];
        }

        foreach (self::$mapStatuses as $status => $payStatus) {
            if (in_array($payStatus, $payStatuses)) {
                $result[] = $status;
            }
        }

        return array_unique($result);
    }
}
