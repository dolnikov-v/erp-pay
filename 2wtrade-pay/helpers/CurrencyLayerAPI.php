<?php

namespace app\helpers;


use app\components\curl\CurlFactory;
use yii\base\BaseObject;

/**
 * Class CurrencyLayerAPI
 * @package app\helpers
 */
class CurrencyLayerAPI extends BaseObject
{
    protected static $url = 'http://apilayer.net/api/';

    /**
     * @param array $currencies ['EUR', 'PLN']
     * @param string $source
     * @param bool $format
     * @return array
     */
    public static function live($currencies = [], $source = 'USD', $format = false)
    {
        $params = [
            'access_key' => \Yii::$app->params['currencyLayerApiKey'],
        ];

        if (!empty($currencies)) {
            $params['currencies'] = implode(',', $currencies);
        }

        if (!empty($source)) {
            $params['source'] = $source;
        }

        if (!empty($format)) {
            $params['format'] = 1;
        }

        $url = self::$url . 'live?' . http_build_query($params);

        $response = self::sendRequest($url);

        $answer = [
            'success' => false,
            'url' => $url,
            'response' => json_encode($response, JSON_UNESCAPED_UNICODE)
        ];

        if ($error = self::checkResponseOnError($response)) {
            $answer['error'] = $error;
        } else {
            $answer['success'] = true;
            $answer['result'] = $response['quotes'];
        }

        return $answer;
    }

    /**
     * @param string $date Дата в строковом формате
     * @param array $currencies ['EUR', 'PLN']
     * @param string $source
     * @param bool $format
     * @return array
     */
    public static function historical($date, $currencies = [], $source = 'USD', $format = false)
    {
        $params = [
            'access_key' => \Yii::$app->params['currencyLayerApiKey'],
            'date' => date('Y-m-d', \Yii::$app->formatter->asTimestamp($date)),
        ];

        if (!empty($currencies)) {
            $params['currencies'] = implode(',', $currencies);
        }

        if (!empty($source)) {
            $params['source'] = $source;
        }

        if (!empty($format)) {
            $params['format'] = 1;
        }

        $url = self::$url . 'historical?' . http_build_query($params);

        $response = self::sendRequest($url);

        $answer = [
            'success' => false,
            'url' => $url,
            'response' => json_encode($response, JSON_UNESCAPED_UNICODE)
        ];

        if ($error = self::checkResponseOnError($response)) {
            $answer['error'] = $error;
        } else {
            $answer['success'] = true;
            $answer['result'] = $response['quotes'];
        }

        return $answer;
    }

    /**
     * @param $from
     * @param $to
     * @param $amount
     * @param null $date
     * @param bool $format
     * @return array
     */
    public static function convert($from, $to, $amount, $date = null, $format = false)
    {
        $params = [
            'access_key' => \Yii::$app->params['currencyLayerApiKey'],
            'from' => $from,
            'to' => $to,
            'amount' => $amount
        ];

        if (!empty($date)) {
            $params['date'] = date('Y-m-d', \Yii::$app->formatter->asTimestamp($date));
        }

        if (!empty($format)) {
            $params['format'] = 1;
        }

        $url = self::$url . 'convert?' . http_build_query($params);

        $response = self::sendRequest($url);

        $answer = [
            'success' => false,
            'url' => $url,
            'response' => json_encode($response, JSON_UNESCAPED_UNICODE)
        ];

        if ($error = self::checkResponseOnError($response)) {
            $answer['error'] = $error;
        } else {
            $answer['success'] = true;
            $answer['result'] = $response['result'];
        }

        return $answer;
    }

    /**
     * @param $startDate
     * @param $endDate
     * @param array $currencies
     * @param string $source
     * @param bool $format
     * @return array
     */
    public static function timeFrame($startDate, $endDate, $currencies = [], $source = 'USD', $format = false)
    {
        $params = [
            'access_key' => \Yii::$app->params['currencyLayerApiKey'],
            'start_date' => date('Y-m-d', \Yii::$app->formatter->asTimestamp($startDate)),
            'end_date' => date('Y-m-d', \Yii::$app->formatter->asTimestamp($endDate)),
        ];

        if (!empty($currencies)) {
            $params['currencies'] = implode(',', $currencies);
        }

        if (!empty($source)) {
            $params['source'] = $source;
        }

        if (!empty($format)) {
            $params['format'] = 1;
        }

        $url = self::$url . 'timeframe?' . http_build_query($params);

        $response = self::sendRequest($url);

        $answer = [
            'success' => false,
            'url' => $url,
            'response' => json_encode($response, JSON_UNESCAPED_UNICODE)
        ];

        if ($error = self::checkResponseOnError($response)) {
            $answer['error'] = $error;
        } else {
            $answer['success'] = true;
            $answer['result'] = $response['quotes'];
        }

        return $answer;
    }

    /**
     * @param $url
     * @return mixed
     */
    protected static function sendRequest($url)
    {
        $curl = CurlFactory::build(CurlFactory::METHOD_GET, $url);

        $curl->setOption(CURLOPT_SSL_VERIFYPEER, false);
        $curl->setOption(CURLOPT_FOLLOWLOCATION, true);
        $curl->setOption(CURLOPT_CONNECTTIMEOUT, 120);
        $curl->setOption(CURLOPT_TIMEOUT, 120);
        $response = $curl->query();
        return json_decode($response, true);
    }

    /**
     * @param $response
     * @return bool|string
     */
    protected static function checkResponseOnError($response)
    {
        if (empty($response)) {
            return "Empty response!";
        }

        if (!isset($response['success'])) {
            return "Unknown response structure!";
        }

        if (!$response['success']) {
            return $response['error']['info'];
        }

        return false;
    }
}