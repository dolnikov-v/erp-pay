<?php
namespace app\helpers\logs;

use yii\base\BaseObject;
use Yii;

/**
 * Class Route
 * @package app\helpers\logs
 */
class Route extends BaseObject
{
    const GROUP_FORM = 'form';
    const GROUP_CALL_CENTER = 'callcenter';
    const GROUP_DELIVERY = 'delivery';

    /**
     * @param string $route
     * @return string
     */
    public static function getRouteGroup($route)
    {
        $list = [
            'order/index/edit' => self::GROUP_FORM,
            'order/index/change-status' => self::GROUP_FORM,
            'order/index/create-call-center-request' => self::GROUP_FORM,
            'order/index/send-in-delivery' => self::GROUP_FORM,
            'order/index/generate-list' => self::GROUP_FORM,
            'order/index/resend-in-delivery' => self::GROUP_FORM,
            'crontab/call-center/create-requests' => self::GROUP_CALL_CENTER,
            'crontab/call-center/send-requests' => self::GROUP_CALL_CENTER,
            'crontab/delivery/send-orders' => self::GROUP_DELIVERY,
            'crontab/delivery/get-orders-info' => self::GROUP_DELIVERY,
        ];

        return (array_key_exists($route, $list)) ? $list[$route] : $route;
    }

    /**
     * @param string $group
     * @return string
     */
    public static function getGroupDescription($group)
    {
        $list = [
            self::GROUP_FORM => Yii::t('common', 'Форма'),
            self::GROUP_CALL_CENTER => Yii::t('common', 'Колл-центр'),
            self::GROUP_DELIVERY => Yii::t('common', 'Служба доставки'),
        ];

        return (array_key_exists($group, $list)) ? $list[$group] : $group;
    }
}
