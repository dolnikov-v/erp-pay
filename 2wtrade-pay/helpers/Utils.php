<?php
namespace app\helpers;

use Yii;
use yii\web\ForbiddenHttpException;

/**
 * Class Utils
 * @package app\helpers
 */
class Utils
{
    /**
     * @return string
     */
    public static function uid()
    {
        return sha1(uniqid('', true) . mt_rand());
    }

    /**
     * @param string $dir
     * @param integer $mode
     * @throws ForbiddenHttpException
     */
    public static function prepareDir($dir, $mode = 0755)
    {
        if (!is_dir($dir)) {
            $oldmask = umask(0);
            if (!mkdir($dir, $mode, true)) {
                umask($oldmask);
                throw new ForbiddenHttpException(Yii::t('common',
                    'Не удалось создать папку "{dir}". Проверьте права доступа.', [
                        'dir' => $dir,
                    ]));
            }
            umask($oldmask);
        }
    }

    /**
     * @param string $dir
     */
    public static function removeDir($dir)
    {
        if ($objects = glob($dir . DIRECTORY_SEPARATOR . "*")) {
            foreach ($objects as $object) {
                is_dir($object) ? self::removeDir($object) : unlink($object);
            }
        }

        rmdir($dir);
    }

    /**
     * Проверка на то что путь до объекта являетс одной из поддиректорий указанной
     *
     * @param string $path Путь до файла или директории
     * @param string $dir Родительская директория
     * @return bool
     */
    public static function checkSubDirOfDir(string $path, string $dir): bool
    {
        $pathDir = dir(dirname($path));
        $dir = dir($dir);
        if (strstr($pathDir->path, $dir->path) !== false) {
            return true;
        }
        return false;
    }

    /**
     *  Проверка на вхождение времени в указанный интервал
     *
     * @param int $from
     * @param int $to
     * @param int|null $time
     * @return bool
     */
    public static function checkIntersectTimeInterval(int $from , int $to, int $time = null): bool
    {
        if (is_null($time)) {
            $time = time();
        }
        return ($time >= $from && $time <= $to) || ($time >= $to && $time <= $from);
    }

    /**
     * Проверка на вхождение времени в дневной интервал
     * Интервал 23:00 - 01:00 является валидным
     *
     * @param int $from
     * @param int $to
     * @param int|null $time
     * @return bool
     */
    public static function checkIntersectCurrentDayTimeInterval(int $from , int $to, int $time = null): bool
    {
        if (is_null($time)) {
            $time = time();
        }
        if ($from < 0) {
            $from += 86400;
        }
        if ($to < 0) {
            $to += 86400;
        }
        $time %= 86400;
        $from %= 86400;
        $to %= 86400;
        return ($time >= $from && $time <= $to) || ($from > $to && ($time >= $from || $time <= $to));
    }
}
