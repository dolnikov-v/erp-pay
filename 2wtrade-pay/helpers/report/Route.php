<?php
namespace app\helpers\report;

use yii\base\BaseObject;

/**
 * Class Route
 * @package app\helpers\report
 */
class Route extends BaseObject
{
    /**
     * @param array $params
     * @return array
     */
    public static function getOrderDateFilter($params)
    {
        $array = [
            'from' => 'dateFrom',
            'to' => 'dateTo',
            'type' => 'dateType',
            'timezone' => 'timezone'
        ];
        $result = [];

        foreach ($params as $key => $param) {
            if (array_key_exists($key, $array)) {
                $result[$array[$key]] = $param;
            }
        }

        return $result;
    }

    /**
     * @param array $params
     * @return array
     */
    public static function getOrderStatusFilter($params)
    {
        foreach ($params as $key => $param) {
            if ($key == 'status') {
                $params[$key] = explode(',', $param);
            }
        }

        return $params;
    }


}
