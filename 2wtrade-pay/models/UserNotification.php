<?php
namespace app\models;

use app\components\db\ActiveRecordLogUpdateTime;
use Yii;

/**
 * Class UserNotification
 * @package app\models
 * @property integer $id
 * @property string $user_id
 * @property string $trigger
 * @property string $params
 * @property string $read
 * @property User $user
 * @property Notification $notification
 * @property UserNotificationQueue $notificationQueue
 * @property Country $country
 */
class UserNotification extends ActiveRecordLogUpdateTime
{
    const UNREAD = 0;
    const READ = 1;
    const NOTIFICATION_LIMIT = 5;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_notification}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'trigger'], 'required'],
            ['trigger', 'string', 'max' => 255],
            [['user_id', 'read'], 'integer'],
            ['params', 'string'],
            ['read', 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'user_id' => Yii::t('common', 'Пользователь'),
            'trigger' => Yii::t('common', 'Имя'),
            'read' => Yii::t('common', 'Прочитано'),
            'params' => Yii::t('common', 'Параметры'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата обновления'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotification()
    {
        return $this->hasOne(Notification::className(), ['trigger' => 'trigger']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotificationQueue()
    {
        return $this->hasOne(UserNotificationQueue::className(), ['notification_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return string
     */
    public function getCssClassIcon()
    {
        switch ($this->notification->type) {
            case Notification::TYPE_PRIMARY:
                return 'bg-primary-600';
            case Notification::TYPE_SUCCESS:
                return 'bg-green-600';
            case Notification::TYPE_INFO:
                return 'bg-blue-600';
            case Notification::TYPE_WARNING:
                return 'bg-orange-600';
            case Notification::TYPE_DANGER:
                return 'bg-red-600';
            default:
                return 'bg-grey-600';
        }
    }
}
