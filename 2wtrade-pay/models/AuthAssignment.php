<?php
namespace app\models;

use app\components\db\ActiveRecordLogUpdateTime;
use Yii;
use yii\db\Exception;

/**
 * Class AuthAssignment
 * @package app\models
 * @property string $item_name
 * @property string $user_id
 * @property integer $created_at
 *
 * @property AuthItem $authItem
 */
class AuthAssignment extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%auth_assignment}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        throw new Exception(Yii::t('common', 'Запрещено прямое изменение объекта.'));
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthItem()
    {
        return $this->hasOne(AuthItem::className(), ['name' => 'item_name']);
    }
}
