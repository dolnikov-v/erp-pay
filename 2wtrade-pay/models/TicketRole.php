<?php

namespace app\models;

use app\models\logs\LinkData;
use Yii;
use app\components\db\ActiveRecordLogUpdateTime;

/**
 * Class TicketRole
 *
 * @property integer $id
 * @property string $role_name
 * @property string $ticket_prefix
 * @property integer $type
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property AuthItem $role
 * @property TicketGroup $ticketPrefix
 */
class TicketRole extends ActiveRecordLogUpdateTime
{
    protected $insertLog = true;
    protected $updateLog = true;
    protected $deleteLog = true;

    /**
     * @return null|string
     */
    public function getLogID()
    {
        return $this->role_name ?? null;
    }

    protected function getLinkData(): ?array
    {
        return [
            new LinkData(['field' => 'ticket_prefix', 'value' => $this->ticket_prefix]),
            new LinkData(['field' => 'type', 'value' => $this->type])
        ];
    }

    const TYPE_INBOX = 1;
    const TYPE_OUTGOING = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ticket_role}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['role_name', 'ticket_prefix'], 'required'],
            [['created_at', 'updated_at', 'type'], 'integer'],
            [['role_name', 'ticket_prefix'], 'string', 'max' => 255],
            [
                ['role_name'],
                'exist',
                'skipOnError' => true,
                'targetClass' => AuthItem::className(),
                'targetAttribute' => ['role_name' => 'name']
            ],
            [
                ['ticket_prefix'],
                'exist',
                'skipOnError' => true,
                'targetClass' => TicketGroup::className(),
                'targetAttribute' => ['ticket_prefix' => 'prefix']
            ],
            ['type', 'default', 'value' => self::TYPE_INBOX],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'role_name' => Yii::t('common', 'Роль'),
            'ticket_prefix' => Yii::t('common', 'Префикс для тикета'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRole()
    {
        return $this->hasOne(AuthItem::className(), ['name' => 'role_name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTicketPrefix()
    {
        return $this->hasOne(TicketGroup::className(), ['prefix' => 'ticket_prefix']);
    }
}
