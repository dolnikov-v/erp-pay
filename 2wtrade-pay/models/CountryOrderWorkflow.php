<?php
namespace app\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\query\CountryOrderWorkflowQuery;
use app\modules\order\models\OrderWorkflow;
use Yii;

/**
 * Class CountryOrderWorkflow
 * @package app\models
 * @property integer $id
 * @property string $country_id
 * @property string $workflow_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property Country $country
 * @property OrderWorkflow $workflow
 */
class CountryOrderWorkflow extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%country_order_workflow}}';
    }

    /**
     * @return CountryOrderWorkflowQuery
     */
    public static function find()
    {
        return new CountryOrderWorkflowQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'workflow_id'], 'required'],
            [
                'country_id',
                'exist',
                'targetClass' => '\app\models\Country',
                'targetAttribute' => 'id',
                'message' => Yii::t('common', 'Неверное значение страны.'),
            ],
            [
                'workflow_id',
                'exist',
                'targetClass' => '\app\modules\order\models\OrderWorkflow',
                'targetAttribute' => 'id',
                'message' => Yii::t('common', 'Неверное значение workflow.'),
            ],
            ['country_id', 'unique', 'targetAttribute' => ['country_id', 'workflow_id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'country_id' => Yii::t('common', 'Страна'),
            'workflow_id' => Yii::t('common', 'Workflow'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkflow()
    {
        return $this->hasOne(OrderWorkflow::className(), ['id' => 'workflow_id']);
    }
}
