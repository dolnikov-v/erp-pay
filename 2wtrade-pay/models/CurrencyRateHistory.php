<?php

namespace app\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\query\CurrencyRateHistoryQuery;
use Yii;
use yii\behaviors\AttributeBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * Class CurrencyRateHistory
 * @package app\models
 * @property integer $id
 * @property integer $currency_id
 * @property float $rate
 * @property float $ask
 * @property float $bid
 * @property string $date
 * @property integer $created_at
 * @property Currency $currency
 */
class CurrencyRateHistory extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%currency_rate_history}}';
    }

    /**
     * @return CurrencyRateHistoryQuery|\yii\db\ActiveQuery
     */
    public static function find()
    {
        return new CurrencyRateHistoryQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['currency_id', 'required'],
            [['currency_id', 'created_at'], 'integer'],
            [['date'], 'string'],
            [['date', 'currency_id'], 'unique', 'targetAttribute' => ['date', 'currency_id'], 'message' => Yii::t('common', 'Курс валюты на эту дату уже есть.')],
            [
                'currency_id',
                'exist',
                'targetClass' => '\app\models\Currency',
                'targetAttribute' => 'id',
                'message' => Yii::t('common', 'Неверное значение валюты.')
            ],

            [['rate', 'ask', 'bid'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'currency_id' => Yii::t('common', 'Валюта'),
            'rate' => Yii::t('common', 'Курс'),
            'ask' => Yii::t('common', 'Курс продажи'),
            'bid' => Yii::t('common', 'Курс покупки'),
            'date' => Yii::t('common', 'Дата'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecordLogUpdateTime::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
            'date' => [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecordLogUpdateTime::EVENT_BEFORE_INSERT => ['date']
                ],
                'value' => function () {
                    return date('Y-m-d');
                }
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    /**
     * @param $currency_id
     * @param $date
     * @param $sum
     * @param bool $toUsd
     * @return float|int
     * @throws \Exception
     */
    public static function convertValueToCurrency($currency_id, $date, $sum, $toUsd = false)
    {
        if (!is_int($date)) {
            $date = strtotime($date);
        }
        /**
         * @var CurrencyRateHistory[] $rates
         */
        $rates = CurrencyRateHistory::find()->where([CurrencyRateHistory::tableName() . '.currency_id' => $currency_id])
            ->andWhere(['<=', CurrencyRateHistory::tableName() . '.date', date('Y-m-d', strtotime("+5days", $date))])
            ->andWhere([
                '>=',
                CurrencyRateHistory::tableName() . '.date',
                date('Y-m-d', strtotime("-5days", $date))
            ])->all();
        if (empty($rates)) {
            throw new \Exception(Yii::t('common',
                'Не удалось определить курс валюты для указанной даты платежа.'));
        }
        $currentRate = array_pop($rates);
        foreach ($rates as $rate) {
            if ($rate->created_at >= $date) {
                $currentRate = $rate;
                break;
            }
        }
        if ($currentRate->rate == 0) {
            throw new \Exception(Yii::t('common',
                'Для данной даты платежа указан некорректный курс валюты.'));
        }

        return $toUsd ? $sum / $currentRate->rate : $sum * $currentRate->rate;
    }

    /**
     * @param int $currencyId
     * @param string $date date('Y-m-d')
     * @return CurrencyRateHistory|array|null|\yii\db\ActiveRecord
     */
    public static function findForDate(int $currencyId, string $date)
    {
        return self::find()
            ->where(['currency_id' => $currencyId, 'date' => $date])
            ->one();
    }
}
