<?php

namespace app\models;

use app\components\db\abstracts\ConnectionInterface;
use app\components\db\ActiveRecordLogUpdateTime;
use app\components\db\models\DirtyData;
use app\models\query\ProductQuery;
use app\modules\storage\models\StoragePart;
use app\modules\storage\models\StorageProduct;
use Yii;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\BaseFileHelper;
use yii\web\UploadedFile;

/**
 * Class Product
 * @package app\models
 * @property integer $id
 * @property string $name
 * @property string $sku
 * @property string $source
 * @property integer $weight
 * @property integer $width
 * @property integer $height
 * @property integer $length
 * @property string $description
 * @property string $image
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $use_days
 * @property integer $active
 * @property bool $shell_on_market
 *
 * @property mixed $sourcesByProduct
 * @property StoragePart[] $storageParts
 * @property ProductPrice[] $price
 * @property ProductPrice[] $prices
 * @property ProductTranslation[] $translation
 * @property StorageProduct[] $storageProducts
 * @property ProductCategory[] $categories
 * @property array $categoryList
 * @property array $categoriesByProduct
 * @property ProductTranslation[] $productTranslations
 */
class Product extends ActiveRecordLogUpdateTime
{
    private static $collectionProducts;

    /**
     * @var array
     */
    public $sources;
    /**
     * @var array
     */
    public $categoryList;


    /**
     * @var UploadedFile
     */
    public $imageFile;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product}}';
    }

    /**
     * @return ProductQuery
     */
    public static function find()
    {
        return new ProductQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'sources'], 'required'],
            ['categoryList', 'required'],
            [['name'], 'filter', 'filter' => 'trim'],
            [['use_days'], 'integer'],
            ['active', 'boolean'],
            ['active', 'default', 'value' => 1],
            ['use_days', 'default', 'value' => 9999],
            ['name', 'string', 'max' => 128, 'tooLong' => Yii::t('common', 'Слишком длинное название.')],
            ['sku', 'string', 'max' => 128, 'tooLong' => 'Error, forign_sku is too long'],
            ['source', 'string'],
            [
                ['name', 'sku'],
                'unique',
                'targetAttribute' => ['name', 'sku'],
            ],
            [['weight', 'width', 'height', 'length'], 'integer', 'min' => 0],
            [['description', 'image'], 'string'],
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, gif'],
            ['sources', 'safe'],
            ['shell_on_market', 'boolean'],
            ['shell_on_market', 'default', 'value' => true],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'name' => Yii::t('common', 'Название'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата обновления'),
            'use_days' => Yii::t('common', 'Срок использования'),
            'sku' => Yii::t('common', 'Номер товара в источнике'),
            'source' => Yii::t('common', 'Источник'),
            'weight' => Yii::t('common', 'Вес (грамм)'),
            'width' => Yii::t('common', 'Ширина (мм)'),
            'height' => Yii::t('common', 'Высота (мм)'),
            'length' => Yii::t('common', 'Длина (мм)'),
            'sources' => Yii::t('common', 'Источники'),
            'categoryList' => Yii::t('common', 'Категории'),
            'description' => Yii::t('common', 'Описание'),
            'image' => Yii::t('common', 'Изображение'),
            'imageFile' => Yii::t('common', 'Изображение'),
            'active' => Yii::t('common', 'Активен'),
            'shell_on_market' => Yii::t('common', 'Продавать на маркете'),
        ];
    }

    /**
     * @return array
     */
    public function getSourcesByProduct()
    {
        return ArrayHelper::map(Source::find()->Joinwith('sourceProducts')->where([SourceProduct::tableName() . '.product_id' => $this->id])->all(), 'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductTranslations()
    {
        return $this->hasMany(ProductTranslation::className(), ['product_id' => 'id']);
    }

    /**
     * @return mixed
     */
    public function upload()
    {
        if ($this->validate() && $this->imageFile) {

            if (!file_exists(self::getPathFile())) {
                BaseFileHelper::createDirectory(self::getPathFile());
            }

            $uniqueFileName = $this->getUniqueName($this->imageFile->baseName) . '.' . $this->imageFile->extension;
            $this->imageFile->saveAs(self::getPathFile($uniqueFileName));
            return $uniqueFileName;
        } else {
            return false;
        }
    }

    /**
     * @param string $image
     * @return string
     */
    public static function getPathFile($image = "")
    {
        $path = Yii::getAlias('@media') . DIRECTORY_SEPARATOR . 'product';
        if ($image) {
            $path .= DIRECTORY_SEPARATOR . $image;
        }
        return $path;
    }

    /**
     * @param string $name
     * @return string
     */
    public function getUniqueName($name)
    {
        return md5($name . microtime());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStorageParts()
    {
        return $this->hasMany(StoragePart::className(), ['product_id' => 'id']);
    }

    /**
     * Works many(by country) after join with language table
     * @return \yii\db\ActiveQuery
     */
    public function getTranslation()
    {
        return $this->hasMany(ProductTranslation::className(), ['product_id' => 'id'])
            ->andOnCondition(['=', ProductTranslation::tableName() . '.language_id', new Expression(Language::tableName() . '.id')]);
    }

    /**
     * @param integer $languageId
     * @param integer $countryId
     * @return \yii\db\ActiveRecord
     */
    public function getTranslationByLanguage(int $languageId, int $countryId)
    {
        return ProductTranslation::find()
            ->where(['product_id' => $this->id])
            ->andWhere(['language_id' => $languageId])
            ->andWhere(['country_id' => $countryId])
            ->one();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrices()
    {
        return $this->hasMany(ProductPrice::className(), ['product_id' => 'id']);
    }

    /**
     * Works only after join with currency table
     * @return \yii\db\ActiveQuery
     */
    public function getPrice()
    {
        return $this->hasMany(ProductPrice::className(), ['product_id' => 'id'])
            ->andOnCondition(['=', ProductPrice::tableName() . '.currency_id', new Expression(Currency::tableName() . '.id')]);
    }

    public function getPhotoByCountry()
    {
        return $this->hasMany(ProductPhoto::className(), ['product_id' => 'id']);
    }

    /**
     * @param integer $currencyId
     * @param integer $countryId
     * @return ProductPrice
     */
    public function getPriceByCurrency(int $currencyId, int $countryId)
    {
        return ProductPrice::find()
            ->where(['product_id' => $this->id])
            ->andWhere(['currency_id' => $currencyId])
            ->andWhere(['country_id' => $countryId])
            ->one();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStorageProducts()
    {
        return $this->hasMany(StorageProduct::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(ProductCategory::className(), ['id' => 'category_id'])
            ->viaTable('product_link_category', ['product_id' => 'id']);
    }

    /**
     * @return array
     */
    public function getCategoriesByProduct ()
    {
        return ArrayHelper::map($this->hasMany(ProductCategory::className(), ['id' => 'category_id'])
            ->viaTable('product_link_category', ['product_id' => 'id'])->asArray()->all(), 'id', 'name');
    }

    /**
     * Получение уникального 3х значного кода товара
     *
     * @return int
     */
    public function getDefaultId(): int
    {
        return ((($this->id & 31744)>>10) + (($this->id & 31744)>>10 < 26 ? 65 : 22)) | ((($this->id & 992)>>5) + ((($this->id & 992)>>5) < 26 ? 65 : 22)) | ((($this->id & 31)>>5) + ((($this->id & 31)>>5) < 26 ? 65 : 22));
    }

    /**
     * @return array
     */
    public static function getCollectionProducts()
    {
        if (self::$collectionProducts === null) {
            self::$collectionProducts = self::find()->orderBy('name')->collection();
        }
        return self::$collectionProducts;
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        if (static::getDb() instanceof ConnectionInterface) {
            $changedAttributes['id'] = $this->id;
            static::getDb()->addBufferChange(new DirtyData([
                'id' => static::clearTableName(),
                'action' => $insert ? DirtyData::ACTION_INSERT : DirtyData::ACTION_UPDATE,
                'data' => $changedAttributes
            ]));
        }

        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        $data = ['id' => $this->id];
        if (static::getDb() instanceof ConnectionInterface) {
            static::getDb()->addBufferChange(new DirtyData([
                'id' => static::clearTableName(),
                'action' => DirtyData::ACTION_DELETE,
                'data' => $data
            ]));
        }
        return parent::afterDelete();
    }
}
