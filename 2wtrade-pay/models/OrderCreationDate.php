<?php
namespace app\models;

use yii\db\ActiveRecord;
use app\modules\order\models\Order;

/**
 * Class OrderCreationDate
 * @package app\models
 * @property integer $order_id
 * @property integer $foreign_create_at
 *
 * @property Order $order
 */

class OrderCreationDate extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_creation_date}}';
    }


    public function rules()
    {
        return [
            ['order_id', 'required'],
            [['order_id', 'foreign_create_at'], 'integer'],
            ['order_id', 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'order_id' => 'Order id',
            'foreign_create_at' => 'Creation date in source',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['order_id' => 'id']);
    }
}