<?php
namespace app\models;

use app\components\db\abstracts\ConnectionInterface;
use app\components\db\ActiveRecordLogUpdateTime;
use app\components\db\models\DirtyData;
use app\models\query\CurrencyQuery;
use Yii;

/**
 * Class Currency
 * @package app\models
 * @property integer $id
 * @property string $name
 * @property string $num_code
 * @property string $char_code
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property CurrencyRate $currencyRate
 */
class Currency extends ActiveRecordLogUpdateTime
{
    /**
     * @var self
     */
    private static $usd;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%currency}}';
    }

    /**
     * @return CurrencyQuery
     */
    public static function find()
    {
        return new CurrencyQuery(get_called_class());
    }

    /**
     * @param string $numCode
     * @return null|\app\models\Currency
     */
    public static function findByNumCode($numCode)
    {
        return Currency::find()
            ->where(['num_code' => $numCode])
            ->one();
    }

    /**
     * @param string $charCode
     * @return null|\app\models\Currency
     */
    public static function findByCharCode($charCode)
    {
        return Currency::find()
            ->where(['char_code' => $charCode])
            ->one();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrencyRate()
    {
        return $this->hasOne(CurrencyRate::className(), ['currency_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'num_code', 'char_code'], 'required'],

            [['name', 'num_code', 'char_code'], 'filter', 'filter' => 'trim'],

            ['name', 'string', 'max' => 128, 'tooLong' => Yii::t('common', 'Слишком длинное название.')],
            ['num_code', 'string', 'max' => 3, 'tooLong' => Yii::t('common', 'Слишком длинный цифровой код.')],
            ['char_code', 'string', 'max' => 3, 'tooLong' => Yii::t('common', 'Слишком длинный буквенный код.')],

            [['num_code', 'char_code'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'name' => Yii::t('common', 'Название'),
            'num_code' => Yii::t('common', 'Цифровой код'),
            'char_code' => Yii::t('common', 'Буквенный код'),
        ];
    }

    /**
     * @param $currency_id
     * @param $date
     * @param $sum
     * @param bool $toUsd
     * @return float|int
     * @throws \Exception
     */
    public static function convertValueToCurrency($currency_id, $date, $sum, $toUsd = false)
    {
        /**
         * @var CurrencyRateHistory[] $rates
         */
        $rates = CurrencyRateHistory::find()->where([CurrencyRateHistory::tableName() . '.currency_id' => $currency_id])
            ->andWhere(['<=', CurrencyRateHistory::tableName() . '.date', date('Y-m-d', strtotime("+5days", $date))])
            ->andWhere([
                '>=',
                CurrencyRateHistory::tableName() . '.date',
                date('Y-m-d', strtotime("-5days", $date))
            ])->all();
        if (empty($rates)) {
            throw new \Exception(Yii::t('common',
                'Не удалось определить курс валюты для указанной даты платежа.'));
        }
        $currentRate = array_pop($rates);
        foreach ($rates as $rate) {
            if ($rate->created_at >= $date) {
                $currentRate = $rate;
                break;
            }
        }
        if ($currentRate->rate == 0) {
            throw new \Exception(Yii::t('common',
                'Для данной даты платежа указан некорректный курс валюты.'));
        }

        return $toUsd ? $sum / $currentRate->rate : $sum * $currentRate->rate;
    }

    /**
     * @return Currency
     */
    public static function getUSD()
    {
        if (!isset(self::$usd)) {
            self::$usd = self::find()->USD()->one();
        }
        return self::$usd;
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        if (static::getDb() instanceof ConnectionInterface) {
            $changedAttributes['id'] = $this->id;
            static::getDb()->addBufferChange(new DirtyData([
                'id' => static::clearTableName(),
                'action' => $insert ? DirtyData::ACTION_INSERT : DirtyData::ACTION_UPDATE,
                'data' => $changedAttributes
            ]));
        }

        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        $data = ['id' => $this->id];
        if (static::getDb() instanceof ConnectionInterface) {
            static::getDb()->addBufferChange(new DirtyData([
                'id' => static::clearTableName(),
                'action' => DirtyData::ACTION_DELETE,
                'data' => $data
            ]));
        }
        return parent::afterDelete();
    }
}
