<?php

namespace app\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\query\LandingQuery;
use Yii;

/**
 * Class Landing
 * @package app\models
 * @property integer $id
 * @property string $url
 * @property integer $created_at
 * @property integer $updated_at
 */
class Landing extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%landing}}';
    }

    /**
     * @return LandingQuery
     */
    public static function find()
    {
        return new LandingQuery(get_called_class());
    }

    /**
     * @param string $url
     * @return string
     */
    public static function prepareUrl($url)
    {
        $url = str_replace(' ', '', $url);
        if (mb_strpos($url, 'http://') === false && mb_strpos($url, 'https://') === false) {
            $url = 'http://' . $url;
        }

        // Адкомбо иногда присылает невалидные урлы
        $url = str_replace('http=>//', '', $url);
        $url = str_replace('https=>//', '', $url);

        $url = str_replace('www.', '', $url);
        $url = str_replace('/order/create', '', $url);
        $url = rtrim(trim($url), '/');

        return $url;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['url'], 'required'],
            [['url'], 'filter', 'filter' => function ($value) {
                return self::prepareUrl($value);
            }],
            ['url', 'string', 'max' => 500, 'tooLong' => Yii::t('common', 'Слишком длинная ссылка.')],
            ['url', 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'url' => Yii::t('common', 'Ссылка'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата обновления'),
        ];
    }
}
