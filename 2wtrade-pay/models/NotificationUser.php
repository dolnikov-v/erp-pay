<?php
namespace app\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\logs\LinkData;
use Yii;

/**
 * Class NotificationUser
 * @package app\models
 * @property integer $id
 * @property string $user_id
 * @property string $trigger
 * @property integer $active
 * @property integer $running_line_active
 * @property Notification $notification
 * @property User $user
 */
class NotificationUser extends ActiveRecordLogUpdateTime
{
    protected $insertLog = true;
    protected $updateLog = true;
    protected $deleteLog = true;

    protected function getLogID()
    {
        return $this->user_id;
    }

    /**
     * @inheritdoc
     */
    protected function prepareAttributesUpdate(?array $changedAttributes, bool $compare = true): ?array
    {
        if (!isset($changedAttributes['trigger'])) {
            $changedAttributes['trigger'] = $this->trigger;
        }
        return parent::prepareAttributesUpdate($changedAttributes, false);
    }

    /**
     * @inheritdoc
     */
    protected function getLinkData(): ?array
    {
        return [
            new LinkData(['field' => 'trigger', 'value' => $this->trigger]),
        ];
    }

    const ACTIVE = 1;
    const NOT_ACTIVE = 0;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%notification_user}}';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'trigger'], 'required'],
            [['trigger'], 'string', 'max' => 255],
            [['user_id','active','running_line_active'], 'integer'],
            [['active'], 'default', 'value' => 1],
            [['running_line_active'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'user_id' => Yii::t('common', 'Пользователь'),
            'trigger' => Yii::t('common', 'Имя'),
            'active'=> Yii::t('common', 'Активность'),
            'running_line_active'=> Yii::t('common', 'В бегущую строку')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserCountry()
    {
        return $this->hasMany(UserCountry::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotification()
    {
        return $this->hasOne(Notification::className(), ['trigger' => 'trigger']);
    }

}
