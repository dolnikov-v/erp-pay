<?php
namespace app\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\query\CalculatedParamQuery;
use app\modules\callcenter\models\CallCenter;
use app\modules\callcenter\models\CallCenterUserOrder;
use app\modules\order\models\Order;
use app\modules\order\models\OrderProduct;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\models\OrderStatus;
use app\modules\callcenter\models\CallCenterWorkTime;
use app\modules\callcenter\models\CallCenterUser;
use app\modules\salary\models\Designation;
use \yii\db\Expression;
use app\modules\report\extensions\Query;
use Yii;

/**
 * Class CalculatedParam
 * @package app\models
 * @property integer $id
 * @property string $name
 * @property string $value
 * @property integer $product_id
 * @property integer $country_id
 * @property integer $call_center_id
 * @property string $params
 * @property integer $cron_launched_at
 *
 * @property Country $country
 * @property Product $product
 * @property CallCenter $callCenter
 */
class CalculatedParam extends ActiveRecordLogUpdateTime
{
    const COUNT_LEADS = 'count_leads'; // число лидов в день
    const COUNT_LEADS_ALL = 'count_leads_all'; // число лидов в день без разбивки по товарам
    const PERCENT_APPROVED = 'percent_approved'; // % аппрува: 7 дн назад по отдельному товару (см заказы по дате отправки в КЦ)
    const PERCENT_BUYOUT = 'percent_buyout'; // % выкупа: 7 дн назад по отдельному товару (см заказы по дате отправки в КС)
    const PERCENT_PRODUCT_BUYOUT = 'percent_product_buyout'; // % выкупа по продукту: N дн назад по отдельному товару (см заказы по дате отправки в КС)
    const PERCENT_REJECT = 'percent_reject'; // % отказа: 7 дн назад по отдельному товару (см заказы по дате отправки в КС)
    const AVG_CHECK_APPROVED = 'avg_check_approved'; // средний чек по апрувам
    const AVG_CHECK_BUYOUT = 'avg_check_buyout'; // средний чек по выкупу
    const AVG_COUNT_PRODUCT = 'avg_count_product'; // среднее кол-во продуктов в заказе
    const PART_PRODUCT_IN_ORDER = 'part_product_in_order'; // доля товара в заказе
    const OPERATORS_DAY = 'operators_day'; // число операторов в 1 рабочий день (среднее за последние 7 дней)
    const AVG_CALLS_ORDER = 'avg_calls_order'; // ср кол-во звонков на заказ за последнюю неделю
    const AVG_CALLS_OPERATOR = 'avg_calls_operator'; // ср кол-во звонков на оператора за последнюю неделю

    const PARAM_AGO_3_DAYS = 3;
    const PARAM_AGO_7_DAYS = 7;
    const PARAM_AGO_15_DAYS = 15;
    const PARAM_AGO_30_DAYS = 30;
    const PARAM_AGO_45_DAYS = 45;

    /*
     * @var array
     * ассоциативный массив такого вида
     * self::$cachedParam[name_country_product] = [
     *      'name' => '',
     *      'value' => '',
     *      'product_id' => '',
     *      'country_id' => '',
     *      'call_center_id' => ''
     * ]
     */
    public static $cachedParam;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%calculated_param}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['value'], 'number'],
            [['product_id', 'country_id', 'call_center_id', 'cron_launched_at'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['params'], 'string', 'max' => 300],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'name' => Yii::t('common', 'Название'),
            'value' => Yii::t('common', 'Значение'),
            'product_id' => Yii::t('common', 'Товар'),
            'country_id' => Yii::t('common', 'Страна'),
            'call_center_id' => Yii::t('common', 'Колл-центр'),
            'params' => Yii::t('common', 'Параметры расчета'),
            'cron_launched_at' => Yii::t('common', 'Дата расчета'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @inheritdoc
     * @return CalculatedParamQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CalculatedParamQuery(get_called_class());
    }

    public static function calculate()
    {
        self::deleteAll(); // очистим все записи, пересчитаем заново
        $ago3Days = strtotime("-" . self::PARAM_AGO_3_DAYS . " days");
        $ago7Days = strtotime("-" . self::PARAM_AGO_7_DAYS . " days");
        $ago15Days = strtotime("-" . self::PARAM_AGO_15_DAYS . " days");
        $ago30Days = strtotime("-" . self::PARAM_AGO_30_DAYS . " days");
        $ago45Days = strtotime("-" . self::PARAM_AGO_45_DAYS . " days");
        $now = time();
        $arrayForInsert = null;
        $param7Days = json_encode([
            'daysAgo' => self::PARAM_AGO_7_DAYS,
        ]);
        $param3Days = json_encode([
            'daysAgo' => self::PARAM_AGO_3_DAYS,
        ]);
        $param15Days = json_encode([
            'daysAgo' => self::PARAM_AGO_15_DAYS,
        ]);
        $param30Days = json_encode([
            'daysAgo' => self::PARAM_AGO_30_DAYS,
        ]);
        $param45Days = json_encode([
            'daysAgo' => self::PARAM_AGO_45_DAYS,
        ]);

        $data = Order::find()
            ->select([
                'count_orders' => new Expression('COUNT(1)'),
                'country_id' => Order::tableName() . '.country_id',
            ])
            ->where(['>=', Order::tableName() . '.created_at', $ago3Days])
            ->groupBy([Order::tableName() . '.country_id'])
            ->asArray()
            ->all();

        if (is_array($data)) {
            foreach ($data as $dataItem) {
                $arrayForInsert[] = [
                    self::COUNT_LEADS_ALL,
                    $dataItem['count_orders'] / self::PARAM_AGO_3_DAYS,
                    null,
                    $dataItem['country_id'],
                    null,
                    $param3Days,
                    $now
                ];
            }
        }

        $data = OrderProduct::find()
            ->joinWith(['order', 'order.deliveryRequest'])
            ->select([
                'count_orders' => 'sum(' . OrderProduct::tableName() . '.quantity)',
                'buyout' => 'sum(if((' . Order::tableName() . '.status_id in (' . join(', ', OrderStatus::getBuyoutList()) . ')), ' . OrderProduct::tableName() . '.quantity, 0))',
                'country_id' => Order::tableName() . '.country_id',
                'product_id' => OrderProduct::tableName() . '.product_id',
            ])
            ->where(['>=', DeliveryRequest::tableName() . '.sent_at', $ago7Days])
            ->andWhere(['is not', OrderProduct::tableName() . '.price', null])
            ->andWhere(['>', OrderProduct::tableName() . '.price', 0])
            ->andWhere(['is not', OrderProduct::tableName() . '.quantity', null])
            ->andWhere(['>', OrderProduct::tableName() . '.quantity', 0])
            ->groupBy([Order::tableName() . '.country_id', OrderProduct::tableName() . '.product_id'])
            ->asArray()
            ->all();

        if (is_array($data)) {
            foreach ($data as $dataItem) {
                $buyoutOrders = 0;
                if (isset($dataItem['count_orders']) && $dataItem['count_orders'] > 0) {
                    $buyoutOrders = $dataItem['buyout'] / $dataItem['count_orders'];
                }
                $arrayForInsert[] = [
                    self::PERCENT_PRODUCT_BUYOUT,
                    $buyoutOrders,
                    $dataItem['product_id'],
                    $dataItem['country_id'],
                    null,
                    $param7Days,
                    $now
                ];
            }
        }

        $data = OrderProduct::find()
            ->joinWith(['order', 'order.deliveryRequest'])
            ->select([
                'count_orders' => 'sum(' . OrderProduct::tableName() . '.quantity)',
                'buyout' => 'sum(if((' . Order::tableName() . '.status_id in (' . join(', ', OrderStatus::getBuyoutList()) . ')), ' . OrderProduct::tableName() . '.quantity, 0))',
                'country_id' => Order::tableName() . '.country_id',
                'product_id' => OrderProduct::tableName() . '.product_id',
            ])
            ->where(['>=', DeliveryRequest::tableName() . '.sent_at', $ago15Days])
            ->andWhere(['is not', OrderProduct::tableName() . '.price', null])
            ->andWhere(['>', OrderProduct::tableName() . '.price', 0])
            ->andWhere(['is not', OrderProduct::tableName() . '.quantity', null])
            ->andWhere(['>', OrderProduct::tableName() . '.quantity', 0])
            ->groupBy([Order::tableName() . '.country_id', OrderProduct::tableName() . '.product_id'])
            ->asArray()
            ->all();

        if (is_array($data)) {
            foreach ($data as $dataItem) {
                $buyoutOrders = 0;
                if (isset($dataItem['count_orders']) && $dataItem['count_orders'] > 0) {
                    $buyoutOrders = $dataItem['buyout'] / $dataItem['count_orders'];
                }
                $arrayForInsert[] = [
                    self::PERCENT_PRODUCT_BUYOUT,
                    $buyoutOrders,
                    $dataItem['product_id'],
                    $dataItem['country_id'],
                    null,
                    $param15Days,
                    $now
                ];
            }
        }

        $data = OrderProduct::find()
            ->joinWith(['order', 'order.deliveryRequest'])
            ->select([
                'count_orders' => 'sum(' . OrderProduct::tableName() . '.quantity)',
                'buyout' => 'sum(if((' . Order::tableName() . '.status_id in (' . join(', ', OrderStatus::getBuyoutList()) . ')), ' . OrderProduct::tableName() . '.quantity, 0))',
                'country_id' => Order::tableName() . '.country_id',
                'product_id' => OrderProduct::tableName() . '.product_id',
            ])
            ->where(['>=', DeliveryRequest::tableName() . '.sent_at', $ago30Days])
            ->andWhere(['is not', OrderProduct::tableName() . '.price', null])
            ->andWhere(['>', OrderProduct::tableName() . '.price', 0])
            ->andWhere(['is not', OrderProduct::tableName() . '.quantity', null])
            ->andWhere(['>', OrderProduct::tableName() . '.quantity', 0])
            ->groupBy([Order::tableName() . '.country_id', OrderProduct::tableName() . '.product_id'])
            ->asArray()
            ->all();

        if (is_array($data)) {
            foreach ($data as $dataItem) {
                $buyoutOrders = 0;
                if (isset($dataItem['count_orders']) && $dataItem['count_orders'] > 0) {
                    $buyoutOrders = $dataItem['buyout'] / $dataItem['count_orders'];
                }
                $arrayForInsert[] = [
                    self::PERCENT_PRODUCT_BUYOUT,
                    $buyoutOrders,
                    $dataItem['product_id'],
                    $dataItem['country_id'],
                    null,
                    $param30Days,
                    $now
                ];
            }
        }

        $data = OrderProduct::find()
            ->joinWith(['order', 'order.deliveryRequest'])
            ->select([
                'count_orders' => 'sum(' . OrderProduct::tableName() . '.quantity)',
                'buyout' => 'sum(if((' . Order::tableName() . '.status_id in (' . join(', ', OrderStatus::getBuyoutList()) . ')), ' . OrderProduct::tableName() . '.quantity, 0))',
                'country_id' => Order::tableName() . '.country_id',
                'product_id' => OrderProduct::tableName() . '.product_id',
            ])
            ->where(['>=', DeliveryRequest::tableName() . '.sent_at', $ago45Days])
            ->andWhere(['is not', OrderProduct::tableName() . '.price', null])
            ->andWhere(['>', OrderProduct::tableName() . '.price', 0])
            ->andWhere(['is not', OrderProduct::tableName() . '.quantity', null])
            ->andWhere(['>', OrderProduct::tableName() . '.quantity', 0])
            ->groupBy([Order::tableName() . '.country_id', OrderProduct::tableName() . '.product_id'])
            ->asArray()
            ->all();

        if (is_array($data)) {
            foreach ($data as $dataItem) {
                $buyoutOrders = 0;
                if (isset($dataItem['count_orders']) && $dataItem['count_orders'] > 0) {
                    $buyoutOrders = $dataItem['buyout'] / $dataItem['count_orders'];
                }
                $arrayForInsert[] = [
                    self::PERCENT_PRODUCT_BUYOUT,
                    $buyoutOrders,
                    $dataItem['product_id'],
                    $dataItem['country_id'],
                    null,
                    $param45Days,
                    $now
                ];
            }
        }

        $products = Product::find()->all();
        if (is_array($products)) {
            foreach ($products as $product) {
                $subQueryProduct = OrderProduct::find()
                    ->select([new Expression('1')])
                    ->where([OrderProduct::tableName() . '.order_id' => new Expression(Order::tableName() . '.id')])
                    ->andWhere([OrderProduct::tableName() . '.product_id' => $product->id])
                    ->andWhere(['is not', OrderProduct::tableName() . '.price', null])
                    ->andWhere(['>', OrderProduct::tableName() . '.price', 0])
                    ->andWhere(['is not', OrderProduct::tableName() . '.quantity', null])
                    ->andWhere(['>', OrderProduct::tableName() . '.quantity', 0])
                    ->limit(1);

                $data = Order::find()
                    ->joinWith(['callCenterRequest'])
                    ->select([
                        'count_orders' => new Expression('COUNT(1)'),
                        'approved' => 'COUNT(' . Order::tableName() . '.status_id IN(' . join(', ', OrderStatus::getApproveList()) . ') OR NULL)',
                        'country_id' => Order::tableName() . '.country_id',
                    ])
                    ->where(['>=', CallCenterRequest::tableName() . '.sent_at', $ago7Days])
                    ->andWhere(['exists', $subQueryProduct])
                    ->groupBy([Order::tableName() . '.country_id'])
                    ->asArray()
                    ->all();

                if (is_array($data)) {
                    foreach ($data as $dataItem) {
                        $approvedOrders = 0;
                        if (isset($dataItem['count_orders']) && $dataItem['count_orders'] > 0) {
                            $approvedOrders = $dataItem['approved'] / $dataItem['count_orders'];
                        }
                        $arrayForInsert[] = [
                            self::PERCENT_APPROVED,
                            $approvedOrders,
                            $product->id,
                            $dataItem['country_id'],
                            null,
                            $param7Days,
                            $now
                        ];
                    }
                }

                $data = Order::find()
                    ->joinWith(['callCenterRequest'])
                    ->select([
                        'avg_check_approved' => 'AVG(' . Order::tableName() . '.price_total)',
                        'country_id' => Order::tableName() . '.country_id',
                    ])
                    ->where(['>=', CallCenterRequest::tableName() . '.sent_at', $ago7Days])
                    ->andWhere([Order::tableName() . '.status_id' => OrderStatus::getApproveList()])
                    ->andWhere(['exists', $subQueryProduct])
                    ->groupBy([Order::tableName() . '.country_id'])
                    ->asArray()
                    ->all();

                if (is_array($data)) {
                    foreach ($data as $dataItem) {
                        $arrayForInsert[] = [
                            self::AVG_CHECK_APPROVED,
                            $dataItem['avg_check_approved'],
                            $product->id,
                            $dataItem['country_id'],
                            null,
                            $param7Days,
                            $now
                        ];
                    }
                }

                /*$data = Order::find()
                    ->joinWith(['deliveryRequest'])
                    ->select([
                        'count_orders' => new Expression('COUNT(1)'),
                        'reject' => 'COUNT(' . Order::tableName() . '.status_id IN(' . join(', ', OrderStatus::getNotBuyoutList()) . ') OR NULL)',
                        'buyout' => 'COUNT(' . Order::tableName() . '.status_id IN(' . join(', ', OrderStatus::getBuyoutList()) . ') OR NULL)',
                        'country_id' => Order::tableName() . '.country_id',
                    ])
                    ->where(['>=', DeliveryRequest::tableName() . '.sent_at', $ago7Days])
                    ->andWhere(['exists', $subQueryProduct])
                    ->groupBy([Order::tableName() . '.country_id'])
                    ->asArray()
                    ->all();

                if (is_array($data)) {
                    foreach ($data as $dataItem) {
                        $buyoutOrders = 0;
                        $rejectOrders = 0;
                        if (isset($dataItem['count_orders']) && $dataItem['count_orders'] > 0) {
                            $buyoutOrders = $dataItem['buyout'] / $dataItem['count_orders'];
                            $rejectOrders = $dataItem['reject'] / $dataItem['count_orders'];
                        }
                        $arrayForInsert[] = [
                            self::PERCENT_BUYOUT,
                            $buyoutOrders,
                            $product->id,
                            $dataItem['country_id'],
                            null,
                            $param7Days,
                            $now
                        ];
                        $arrayForInsert[] = [
                            self::PERCENT_REJECT,
                            $rejectOrders,
                            $product->id,
                            $dataItem['country_id'],
                            null,
                            $param7Days,
                            $now
                        ];
                    }
                }*/

                $data = Order::find()
                    ->joinWith(['deliveryRequest'])
                    ->select([
                        'avg_check_buyout' => 'AVG(' . Order::tableName() . '.price_total + ' . Order::tableName() . '.delivery)',
                        'country_id' => Order::tableName() . '.country_id',
                    ])
                    ->where(['>=', DeliveryRequest::tableName() . '.sent_at', $ago7Days])
                    ->andWhere([Order::tableName() . '.status_id' => OrderStatus::getBuyoutList()])
                    ->andWhere(['exists', $subQueryProduct])
                    ->groupBy([Order::tableName() . '.country_id'])
                    ->asArray()
                    ->all();

                if (is_array($data)) {
                    foreach ($data as $dataItem) {
                        $arrayForInsert[] = [
                            self::AVG_CHECK_BUYOUT,
                            $dataItem['avg_check_buyout'],
                            $product->id,
                            $dataItem['country_id'],
                            null,
                            $param7Days,
                            $now
                        ];
                    }
                }

                $data = Order::find()
                    ->joinWith(['deliveryRequest'])
                    ->select([
                        'avg_check_buyout' => 'AVG(' . Order::tableName() . '.price_total + ' . Order::tableName() . '.delivery)',
                        'country_id' => Order::tableName() . '.country_id',
                    ])
                    ->where(['>=', DeliveryRequest::tableName() . '.sent_at', $ago15Days])
                    ->andWhere([Order::tableName() . '.status_id' => OrderStatus::getBuyoutList()])
                    ->andWhere(['exists', $subQueryProduct])
                    ->groupBy([Order::tableName() . '.country_id'])
                    ->asArray()
                    ->all();

                if (is_array($data)) {
                    foreach ($data as $dataItem) {
                        $arrayForInsert[] = [
                            self::AVG_CHECK_BUYOUT,
                            $dataItem['avg_check_buyout'],
                            $product->id,
                            $dataItem['country_id'],
                            null,
                            $param15Days,
                            $now
                        ];
                    }
                }

                $data = Order::find()
                    ->joinWith(['deliveryRequest'])
                    ->select([
                        'avg_check_buyout' => 'AVG(' . Order::tableName() . '.price_total + ' . Order::tableName() . '.delivery)',
                        'country_id' => Order::tableName() . '.country_id',
                    ])
                    ->where(['>=', DeliveryRequest::tableName() . '.sent_at', $ago30Days])
                    ->andWhere([Order::tableName() . '.status_id' => OrderStatus::getBuyoutList()])
                    ->andWhere(['exists', $subQueryProduct])
                    ->groupBy([Order::tableName() . '.country_id'])
                    ->asArray()
                    ->all();

                if (is_array($data)) {
                    foreach ($data as $dataItem) {
                        $arrayForInsert[] = [
                            self::AVG_CHECK_BUYOUT,
                            $dataItem['avg_check_buyout'],
                            $product->id,
                            $dataItem['country_id'],
                            null,
                            $param30Days,
                            $now
                        ];
                    }
                }

                $data = Order::find()
                    ->joinWith(['deliveryRequest'])
                    ->select([
                        'avg_check_buyout' => 'AVG(' . Order::tableName() . '.price_total + ' . Order::tableName() . '.delivery)',
                        'country_id' => Order::tableName() . '.country_id',
                    ])
                    ->where(['>=', DeliveryRequest::tableName() . '.sent_at', $ago45Days])
                    ->andWhere([Order::tableName() . '.status_id' => OrderStatus::getBuyoutList()])
                    ->andWhere(['exists', $subQueryProduct])
                    ->groupBy([Order::tableName() . '.country_id'])
                    ->asArray()
                    ->all();

                if (is_array($data)) {
                    foreach ($data as $dataItem) {
                        $arrayForInsert[] = [
                            self::AVG_CHECK_BUYOUT,
                            $dataItem['avg_check_buyout'],
                            $product->id,
                            $dataItem['country_id'],
                            null,
                            $param45Days,
                            $now
                        ];
                    }
                }

                $data = Order::find()
                    ->select([
                        'count_orders' => new Expression('COUNT(1)'),
                        'country_id' => Order::tableName() . '.country_id',
                    ])
                    ->where(['>=', Order::tableName() . '.created_at', $ago3Days])
                    ->andWhere(['exists', $subQueryProduct])
                    ->groupBy([Order::tableName() . '.country_id'])
                    ->asArray()
                    ->all();

                if (is_array($data)) {
                    foreach ($data as $dataItem) {
                        $arrayForInsert[] = [
                            self::COUNT_LEADS,
                            $dataItem['count_orders'] / self::PARAM_AGO_3_DAYS,
                            $product->id,
                            $dataItem['country_id'],
                            null,
                            $param3Days,
                            $now
                        ];
                    }
                }
            }
        }

        $subQuery = OrderProduct::find()
            ->joinWith(['order'])
            ->select([
                'sum_product' => 'SUM(' . OrderProduct::tableName() . '.quantity)',
                'country_id' => Order::tableName() . '.country_id',
                'product_id' => OrderProduct::tableName() . '.product_id',
                'order_id' => OrderProduct::tableName() . '.order_id',
            ])
            ->where(['>=', Order::tableName() . '.created_at', $ago7Days])
            ->andWhere(['>', OrderProduct::tableName() . '.price', 0])
            ->andWhere(['is not', OrderProduct::tableName() . '.price', null])
            ->groupBy([Order::tableName() . '.country_id', OrderProduct::tableName() . '.product_id', OrderProduct::tableName() . '.order_id']);

        $query = new Query();
        $query->from(['count_products' => $subQuery]);
        $query->select([
            'avg_count_products' => 'AVG(count_products.sum_product)',
            'country_id' => 'count_products.country_id',
            'product_id' => 'count_products.product_id',
        ]);
        $query->groupBy(['count_products.country_id', 'count_products.product_id']);

        $data = $query->all();
        if (is_array($data)) {
            foreach ($data as $dataItem) {
                $arrayForInsert[] = [
                    self::AVG_COUNT_PRODUCT,
                    $dataItem['avg_count_products'],
                    $dataItem['product_id'],
                    $dataItem['country_id'],
                    null,
                    $param7Days,
                    $now
                ];
            }
        }

        $countries = Country::find()->active()->all();
        if (is_array($countries) && is_array($products)) {
            foreach ($products as $product) {
                foreach ($countries as $country) {
                    $data = OrderProduct::find()
                        ->joinWith(['order'])
                        ->select([
                            'sum_product' => 'SUM(' . OrderProduct::tableName() . '.quantity)',
                            'order_id' => OrderProduct::tableName() . '.order_id',
                        ])
                        ->where(['>=', Order::tableName() . '.created_at', $ago7Days])
                        ->andWhere([Order::tableName() . '.country_id' => $country->id])
                        ->andWhere([OrderProduct::tableName() . '.product_id' => $product->id])
                        ->andWhere(['>', OrderProduct::tableName() . '.price', 0])
                        ->andWhere(['is not', OrderProduct::tableName() . '.price', null])
                        ->groupBy([OrderProduct::tableName() . '.product_id', OrderProduct::tableName() . '.order_id'])
                        ->asArray()
                        ->all();

                    $tempArrayProduct = null;
                    $tempArrayProductAll = null;
                    if (is_array($data)) {
                        foreach ($data as $dataItem) {
                            $tempArrayProduct[] = [
                                'sum_product' => $dataItem['sum_product'],
                                'order_id' => $dataItem['order_id'],
                            ];
                            if (isset($tempArrayProductAll[$dataItem['order_id']])) {
                                $tempArrayProductAll[$dataItem['order_id']] += $dataItem['sum_product'];
                            } else {
                                $tempArrayProductAll[$dataItem['order_id']] = $dataItem['sum_product'];
                            }
                        }
                    }

                    if (is_array($tempArrayProductAll)) {
                        $partProduct = 0;
                        foreach ($tempArrayProduct as $tempArrayProductOne) {
                            $partProduct += $tempArrayProductOne['sum_product'] / $tempArrayProductAll[$tempArrayProductOne['order_id']];
                        }
                        $partProduct /= count($tempArrayProductAll);
                        $arrayForInsert[] = [
                            self::PART_PRODUCT_IN_ORDER,
                            $partProduct,
                            $product->id,
                            $country->id,
                            null,
                            $param7Days,
                            $now
                        ];
                    }
                }
            }
        }

        $data = CallCenterWorkTime::find()
            ->joinWith(['callCenterUser', 'callCenterUser.callCenter', 'callCenterUser.person', 'callCenterUser.person.designation'])
            ->select([
                'id' => 'concat(' . CallCenter::tableName() . '.country_id, "_", ' . CallCenterUser::tableName() . '.callcenter_id)',
                'country_id' => CallCenter::tableName() . '.country_id',
                'call_center_id' => CallCenterUser::tableName() . '.callcenter_id',
                'operators_fact' => 'count(distinct ' . CallCenterUser::tableName() . '.person_id)',
            ])
            ->where(['>=', 'UNIX_TIMESTAMP(' . CallCenterWorkTime::tableName() . '.date)', strtotime("-" . (self::PARAM_AGO_7_DAYS + 1) . " days")])
            ->andWhere(['<=', 'UNIX_TIMESTAMP(' . CallCenterWorkTime::tableName() . '.date)', strtotime("-1 days")])
            ->andWhere(['is not', CallCenterWorkTime::tableName() . '.number_of_calls', null])
            ->andWhere(['>', CallCenterWorkTime::tableName() . '.number_of_calls', 0])
            ->andWhere([Designation::tableName() . '.calc_work_time' => 1])
            ->groupBy(['id'])
            ->asArray()->all();
        if (is_array($data)) {
            foreach ($data as $dataItem) {
                $arrayForInsert[] = [
                    self::OPERATORS_DAY,
                    $dataItem['operators_fact'] / self::PARAM_AGO_7_DAYS,
                    null,
                    $dataItem['country_id'],
                    $dataItem['call_center_id'],
                    $param7Days,
                    $now
                ];
            }
        }

        $subQuery = CallCenterUserOrder::find()
            ->joinWith(['order', 'order.callCenterRequest'])
            ->select([
                'country_id' => Order::tableName() . '.country_id',
                'call_center_id' => CallCenterRequest::tableName() . '.call_center_id',
                'order_id' => CallCenterUserOrder::tableName() . '.order_id',
                'calls' => 'sum(ifnull(' . CallCenterUserOrder::tableName() . '.number_of_calls, 0))',
            ])
            ->where(['>=', Order::tableName() . '.created_at', strtotime("-" . (self::PARAM_AGO_7_DAYS + 1) . " days")])
            ->andWhere(['<=', Order::tableName() . '.created_at', strtotime("-1 days")])
            ->groupBy([
                Order::tableName() . '.country_id',
                CallCenterRequest::tableName() . '.call_center_id',
                CallCenterUserOrder::tableName() . '.order_id'
            ]);

        $query = new Query();
        $query->from(['order_calls' => $subQuery]);
        $query->select([
            'avg_calls' => 'AVG(order_calls.calls)',
            'country_id' => 'order_calls.country_id',
            'call_center_id' => 'order_calls.call_center_id',
        ]);
        $query->groupBy(['order_calls.country_id', 'order_calls.call_center_id']);

        $data = $query->all();
        if (is_array($data)) {
            foreach ($data as $dataItem) {
                $arrayForInsert[] = [
                    self::AVG_CALLS_ORDER,
                    $dataItem['avg_calls'],
                    null,
                    $dataItem['country_id'],
                    $dataItem['call_center_id'],
                    $param7Days,
                    $now
                ];
            }
        }

        $subQuery = CallCenterUserOrder::find()
            ->joinWith(['order', 'order.callCenterRequest'])
            ->select([
                'country_id' => Order::tableName() . '.country_id',
                'call_center_id' => CallCenterRequest::tableName() . '.call_center_id',
                'user_id' => CallCenterUserOrder::tableName() . '.user_id',
                'calls' => 'sum(ifnull(' . CallCenterUserOrder::tableName() . '.number_of_calls, 0))',
            ])
            ->where(['>=', Order::tableName() . '.created_at', strtotime("-" . (self::PARAM_AGO_7_DAYS + 1) . " days")])
            ->andWhere(['<=', Order::tableName() . '.created_at', strtotime("-1 days")])
            ->groupBy([
                Order::tableName() . '.country_id',
                CallCenterRequest::tableName() . '.call_center_id',
                CallCenterUserOrder::tableName() . '.user_id'
            ]);

        $query = new Query();
        $query->from(['user_calls' => $subQuery]);
        $query->select([
            'avg_calls' => 'AVG(user_calls.calls)',
            'country_id' => 'user_calls.country_id',
            'call_center_id' => 'user_calls.call_center_id',
        ]);
        $query->groupBy(['user_calls.country_id', 'user_calls.call_center_id']);

        $data = $query->all();
        if (is_array($data)) {
            foreach ($data as $dataItem) {
                $arrayForInsert[] = [
                    self::AVG_CALLS_OPERATOR,
                    $dataItem['avg_calls'] / self::PARAM_AGO_7_DAYS,
                    null,
                    $dataItem['country_id'],
                    $dataItem['call_center_id'],
                    $param7Days,
                    $now
                ];
            }
        }

        if (is_array($arrayForInsert)) {
            Yii::$app->db->createCommand()->batchInsert(
                self::tableName(),
                [
                    'name',
                    'value',
                    'product_id',
                    'country_id',
                    'call_center_id',
                    'params',
                    'cron_launched_at'
                ],
                $arrayForInsert
            )->execute();
        }

        self::setCachedParam();
    }

    public static function setCachedParam()
    {
        self::$cachedParam = Yii::$app->cache->get('calculated_param');
        if (self::$cachedParam === false) {
            $data = self::find()->all();
            if (is_array($data)) {
                foreach ($data as $dataItem) {
                    self::$cachedParam[$dataItem['name'] . '_' . $dataItem['country_id'] . '_' . $dataItem['product_id'] . '_' . $dataItem['call_center_id']] = [
                        'name' => $dataItem['name'],
                        'value' => $dataItem['value'],
                        'product_id' => $dataItem['product_id'],
                        'country_id' => $dataItem['country_id'],
                        'call_center_id' => $dataItem['call_center_id'],
                    ];
                }
                Yii::$app->cache->set('calculated_param', self::$cachedParam, 86400);
            }
        }
    }

    /*
     * @param string $name
     * @param integer $country_id
     * @param integer $product_id
     * @param integer $call_center_id
     * @return string | boolean
     */
    public static function getParam($name, $country_id = null, $product_id = null, $call_center_id = null)
    {
        if (isset(self::$cachedParam[$name . '_' . $country_id . '_' . $product_id . '_' . $call_center_id])) {
            return self::$cachedParam[$name . '_' . $country_id . '_' . $product_id . '_' . $call_center_id];
        } else {
            $data = self::find()
                ->where(['name' => $name])
                ->andWhere(['country_id' => $country_id])
                ->andWhere(['product_id' => $product_id])
                ->andWhere(['call_center_id' => $call_center_id])
                ->limit(1)
                ->one();
            if (isset($data->value)) {
                return $data->value;
            }
            return false;
        }
    }
}