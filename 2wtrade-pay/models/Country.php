<?php

namespace app\models;

use app\components\db\abstracts\ConnectionInterface;
use app\components\db\models\DirtyData;
use app\models\integration1c\Handbook;
use app\modules\catalog\models\ProductCriticalApprove;
use app\modules\order\models\OrderProduct;
use app\modules\salary\models\BonusType;
use Locale;
use app\components\db\ActiveRecordLogUpdateTime;
use app\models\api\ApiUserDelivery;
use app\models\query\CountryQuery;
use app\modules\callcenter\models\CallCenter;
use app\modules\delivery\models\Delivery;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use app\modules\order\models\OrderWorkflow;
use app\modules\order\models\OrderWorkflowStatus;
use app\modules\order\models\SmsPollQuestions;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class Country
 * @package app\models
 * @property integer $id
 * @property string $slug
 * @property string $char_code
 * @property integer $timezone_id
 * @property integer $currency_id
 * @property integer $language_id
 * @property string $name
 * @property string $name_en
 * @property integer $active
 * @property integer $is_partner
 * @property boolean $is_stop_list
 * @property integer $auto_trash
 * @property integer $created_at
 * @property integer $updated_at
 * @property boolean $brokerage_enabled
 * @property string $sms_notifier_phone_from
 * @property integer $cc_operator_daily_approve_plan
 * @property integer $curator_user_id
 * @property integer $trash_doubles_enabled
 * @property Currency $currency
 * @property Language $language
 * @property CurrencyRate $currencyRate
 * @property Timezone $timezone
 * @property CountryOrderWorkflow[] $orderWorkflows
 * @property OrderWorkflow[] $workflows
 * @property Delivery[] $deliveries
 * @property ApiUserDelivery[] $apiUsersDelivery
 * @property User $curatorUser
 * @property string $order_notification_reply_email
 * @property double $critical_approve_percent
 * @property integer $is_distributor
 * @property bool $sink1c
 *
 * @property Source[] $sources
 */
class Country extends ActiveRecordLogUpdateTime
{
    const BROKERAGE_CUTOFF = 31; // количество дней за которые считать статистику для брокера
    const BROKERAGE_MIN_BUYOUT = 10;

    const TYPE_COUNTRY = 'country';
    const TYPE_PRODUCT = 'product';

    const IS_DISTRIBUTOR = 1;
    const IS_NOT_DISTRIBUTOR = 0;

    const ACTIVE = 1;
    const NOT_ACTIVE = 0;

    /**
     * @var array
     */
    private $allowOrderStatuses;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%country}}';
    }

    /**
     * @var string
     */
    public $approve_type;

    /**
     * @return CountryQuery
     */
    public static function find()
    {
        return new CountryQuery(get_called_class());
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ((static::getDb() instanceof ConnectionInterface) && $this->sink1c) {
            $data = [];
            foreach ($changedAttributes as $key => $val) {
                if (($newVal = $this->getAttribute($key)) != $val) {
                    $data[$key] = $newVal;
                }
            }
            if ($data) {
                $data['id'] = $this->id;
                static::getDb()->addBufferChange(new DirtyData([
                    'id' => static::clearTableName(),
                    'action' => $insert ? DirtyData::ACTION_INSERT : DirtyData::ACTION_UPDATE,
                    'data' => $data
                ]));
            }
        }

        if (yii::$app->user->can('catalog.country.autoapprove')) {
            $post = yii::$app->request->post('ProductCriticalApprove');
            $transaction = yii::$app->db->beginTransaction();
            $errors = false;

            if (!$insert) {
                ProductCriticalApprove::deleteAll(['country_id' => $this->id]);
            }

            if (isset($post['product_id']) && !empty($post['product_id'])) {
                foreach ($post['product_id'] as $num => $data) {
                    if(empty($data) || empty($post['percent'][$num])){
                        continue;
                    }
                    $model = new ProductCriticalApprove();

                    $loadData = [
                        'product_id' => $data,
                        'percent' => $post['percent'][$num],
                        'country_id' => $post['country_id'][$num]
                    ];

                    if (!$model->load($loadData, '') || !$model->save()) {
                        $errors = true;
                        $transaction->rollBack();
                        break;
                    }
                }
            }

            if ($errors) {
                yii::$app->notifier->addNotification(yii::t('common', 'Ошибка сохранения данных критического апрува по продуктам'));
            } else {
                $transaction->commit();
            }
        }

        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'slug', 'char_code', 'timezone_id'], 'required'],
            [['name', 'name_en'], 'string', 'max' => 32],
            [['brokerage_enabled', 'trash_doubles_enabled', 'is_partner', 'auto_trash', 'is_stop_list', 'is_distributor', 'sink1c'], 'boolean'],
            [['trash_doubles_enabled', 'sink1c'], 'default', 'value' => false],
            ['brokerage_enabled', 'default', 'value' => true],
            ['slug', 'string', 'max' => 3],
            ['slug', 'unique'],
            ['slug', 'validateSlug'],
            ['char_code', 'string', 'max' => 3],
            ['sms_notifier_phone_from', 'string', 'max' => 50],
            [['cc_operator_daily_approve_plan', 'curator_user_id'], 'integer'],
            [
                'timezone_id',
                'exist',
                'targetClass' => '\app\models\Timezone',
                'targetAttribute' => 'id',
                'message' => Yii::t('common', 'Неверное значение временной зоны.'),
            ],
            [
                'currency_id',
                'exist',
                'targetClass' => '\app\models\Currency',
                'targetAttribute' => 'id',
                'message' => Yii::t('common', 'Неверное значение валюты.'),
            ],
            [
                'language_id',
                'exist',
                'targetClass' => '\app\models\Language',
                'targetAttribute' => 'id',
                'message' => Yii::t('common', 'Неверное значение языка.'),
            ],
            [
                'curator_user_id',
                'exist',
                'targetClass' => '\app\models\User',
                'targetAttribute' => 'id',
                'message' => Yii::t('common', 'Неверное значение пользователя.'),
            ],
            [['order_notification_reply_email'], 'string', 'max' => 100],
            [['order_notification_reply_email'], 'email'],
            ['critical_approve_percent', 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'name' => Yii::t('common', 'Название'),
            'brokerage_enabled' => Yii::t('common', 'Брокер включён'),
            'active' => Yii::t('common', 'Доступность'),
            'is_partner' => Yii::t('common', 'Партнерство'),
            'is_distributor' => Yii::t('common', 'Дистрибьютор'),
            'is_stop_list' => Yii::t('common', 'В стоп-лист уведомлений'),
            'slug' => Yii::t('common', 'URL сегмент'),
            'char_code' => Yii::t('common', 'Код страны'),
            'timezone_id' => Yii::t('common', 'Временная зона'),
            'currency_id' => Yii::t('common', 'Валюта'),
            'language_id' => Yii::t('common', 'Язык'),
            'sms_notifier_phone_from' => Yii::t('common', 'Номер телефона для отправки СМС уведомлений'),
            'cc_operator_daily_approve_plan' => Yii::t('common', 'Дневная норма апрува для оператора'),
            'curator_user_id' => Yii::t('common', 'Ответственный куратор'),
            'trash_doubles_enabled' => Yii::t('common', 'Трешить дубли'),
            'order_notification_reply_email' => Yii::t('common', 'E-mail для ответа клиента на уведомление'),
            'auto_trash' => Yii::t('common', 'Использовать автотреш'),
            'critical_approve_percent' => Yii::t('common', 'Критичный % апрува'),
            'sink1c' => Yii::t('common', 'Отправка связанных со страной изменений в 1С')
        ];
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateSlug($attribute, $params)
    {
        if (preg_match("/[^a-z0-9]/", $this->slug)) {
            $this->addError($attribute, Yii::t('common',
                'URL сегмент может содержать только буквы английского алфавита в нижнем регистре и цифры от 0 до 9.'));
        }
    }

    public function beforeSave($insert)
    {
        if (!$this->name_en && $this->char_code) {
            $this->name_en = Locale::getDisplayRegion('-' . $this->char_code, 'en');
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTimezone()
    {
        return $this->hasOne(Timezone::className(), ['id' => 'timezone_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductCriticalApproves()
    {
        return $this->hasMany(ProductCriticalApprove::className(), ['country_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrencyRate()
    {
        return $this->hasOne(CurrencyRate::className(), ['currency_id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserCountry()
    {
        return $this->hasMany(UserCountry::className(), ['country_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderWorkflows()
    {
        return $this->hasMany(CountryOrderWorkflow::className(), ['country_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkflows()
    {
        return $this->hasMany(OrderWorkflow::className(), ['id' => 'workflow_id'])
            ->via('orderWorkflows');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveries()
    {
        return $this->hasMany(Delivery::className(), ['country_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActiveDeliveries()
    {
        return $this->hasMany(Delivery::className(), ['country_id' => 'id'])->andWhere(['active' => true]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getListSmsPoll()
    {
        return $this->hasMany(SmsPollQuestions::className(), ['country_id' => 'id'])->andWhere(['is_active' => true]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCuratorUser()
    {
        return $this->hasOne(User::className(), ['id' => 'curator_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSourceCountries()
    {
        return $this->hasMany(SourceCountry::className(), ['country_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSources()
    {
        return $this->hasMany(Source::className(), ['id' => 'source_id'])->via('sourceCountries');
    }


    /**
     * Рейтинг для брокера. доля выкупов за 31 день либо null если было меньше 10 выкупов
     *
     * @return double|null
     */
    public function brokerageScore()
    {
        $ordersApproved = Order::find()
            ->where([Order::tableName() . ".country_id" => $this->id])
            ->andWhere([">=", Order::tableName() . ".status_id", OrderStatus::STATUS_CC_APPROVED])
            ->andWhere(["<>", Order::tableName() . ".status_id", OrderStatus::STATUS_CC_REJECTED])
            ->andWhere([">", Order::tableName() . ".created_at", time() - self::BROKERAGE_CUTOFF * 86400])
            ->count();

        $ordersBuyout = Order::find()
            ->where([Order::tableName() . ".country_id" => $this->id])
            ->andWhere([
                Order::tableName() . ".status_id" => [
                    OrderStatus::STATUS_DELIVERY_BUYOUT,
                    OrderStatus::STATUS_FINANCE_MONEY_RECEIVED
                ]
            ])
            ->andWhere([">", Order::tableName() . ".created_at", time() - self::BROKERAGE_CUTOFF * 86400])
            ->count();

        if ($ordersBuyout < self::BROKERAGE_MIN_BUYOUT) {
            return null;
        }

        return $ordersBuyout / $ordersApproved;
    }

    /**
     * @return array
     */
    public function getAllowOrderStatuses()
    {
        if (is_null($this->allowOrderStatuses)) {
            $this->allowOrderStatuses = [];

            $workflowIds = ArrayHelper::getColumn($this->orderWorkflows, 'workflow_id');

            if ($workflowIds) {
                /** @var OrderWorkflowStatus[] $workflowStatuses */
                $workflowStatuses = OrderWorkflowStatus::find()
                    ->where(['in', 'workflow_id', $workflowIds])
                    ->all();

                foreach ($workflowStatuses as $workflowStatus) {
                    if (array_key_exists($workflowStatus->status_id, $this->allowOrderStatuses)) {
                        $parents = explode(',', $workflowStatus->parents);
                        $this->allowOrderStatuses[$workflowStatus->status_id] = array_merge($this->allowOrderStatuses[$workflowStatus->status_id],
                            $parents);
                        $this->allowOrderStatuses[$workflowStatus->status_id] = array_unique($this->allowOrderStatuses[$workflowStatus->status_id]);
                    } else {
                        $this->allowOrderStatuses[$workflowStatus->status_id] = explode(',', $workflowStatus->parents);
                    }

                    asort($this->allowOrderStatuses[$workflowStatus->status_id]);
                }
            }
        }

        return $this->allowOrderStatuses;
    }

    /**
     * @param integer $statusId
     * @return array|mixed
     */
    public function getNextOrderStatuses($statusId)
    {
        $statuses = [];
        $allowOrderStatuses = $this->getAllowOrderStatuses();
        if (array_key_exists($statusId, $allowOrderStatuses)) {
            $statuses = $allowOrderStatuses[$statusId];
        }
        return $statuses;
    }

    /**
     * @return Country
     */
    public static function getDefault()
    {
        $country = null;

        if (Yii::$app->user->isSuperadmin) {
            $country = self::find()
                ->active()
                ->one();
        } else {
            if (!Yii::$app->user->isGuest) {
                $country = self::find()
                    ->joinWith(['userCountry'])
                    ->where([UserCountry::tableName() . '.user_id' => Yii::$app->user->id])
                    ->active()
                    ->one();
            }
        }

        if (is_null($country)) {
            $country = new Country();
        }

        return $country;
    }

    /**
     * @return Country[]
     */
    public static function getCountriesWithActiveCallCenter()
    {
        $countries = [];

        $callCenters = CallCenter::find()
            ->joinWith('country')
            ->active()
            ->groupBy('country_id')
            ->all();

        foreach ($callCenters as $callCenter) {
            $countries[] = $callCenter->country;
        }

        return $countries;
    }

    public static function getStopListCountryNotification()
    {

        $stopList = self::find()
            ->where(["!=", Country::tableName() . '.is_stop_list', 0])
            ->all();

        return ArrayHelper::getColumn($stopList, 'id');
    }

    /**
     * Список КС, которые определяются брокером в зависимости от указанных параметров, отсортированные п приоритету
     * @deprecated
     * @param integer $countryID
     * @param string|null $customerZip
     * @param string|null $customerCity
     * @param OrderProduct[] $products
     * @param null $packageId
     * @param bool $expressDelivery
     * @param null $orderPrice
     * @return array
     */
    public static function getDeliveriesByBroker($countryID, $customerZip = null, $customerCity = null, $products = [], $packageId = null, $expressDelivery = false, $orderPrice = null)
    {
        $productIds = ArrayHelper::getColumn($products, 'product_id');
        $deliveries = Delivery::find()
            ->byCountryId($countryID)
            ->orderBy(['sort_order' => SORT_ASC])
            ->active()
            ->brokerageActive()
            ->all();

        $resultDeliveries = [];

        foreach ($deliveries as $delivery) {
            $deliveryZipGroup = null;

            if ((!is_null($customerZip) || !is_null($customerCity))&& ($deliveryZipGroup = $delivery->verifyZipCode($customerZip, $customerCity)) === false) {
                continue;
            }

            if (!empty($productIds) && $delivery->canDeliverProducts($productIds, $deliveryZipGroup) === false) {
                continue;
            }

            if (!empty($packageId) && $delivery->canDeliverPackage($packageId) === false) {
                continue;
            }

            if ($orderPrice && $delivery->brokerage_min_order_price && $orderPrice < $delivery->brokerage_min_order_price) {
                continue;
            }

            $s = $delivery->brokerageScore($deliveryZipGroup, $expressDelivery);

            $deliveryPrice = !empty($products) ? $delivery->getCustomDeliveryPrice($products) : null;
            if ($deliveryZipGroup && is_numeric($deliveryZipGroup->price) && $deliveryZipGroup->price) {
                $deliveryPrice = $deliveryZipGroup->price;
            }

            $resultDeliveries[] = [
                'score' => $s,
                'delivery' => $delivery,
                'deliveryPrice' => $deliveryPrice,
            ];
        }
        usort($resultDeliveries, function ($itemA, $itemB) {
            return $itemA['score'] < $itemB['score'];
        });
        return $resultDeliveries;
    }

    /**
     * @param $date
     * @return float|mixed
     */
    public function getMinRateByDate($date)
    {
        $rateHistory = CurrencyRateHistory::find()->select('rate')
            ->where(
                [
                    'date' => $date,
                    'currency_id' => $this->currency_id
                ])->scalar();
        $rateToday = $this->currencyRate->rate;
        return $rateHistory ? min($rateHistory, $rateToday) : $rateToday;
    }


    /**
     * @param $rate
     * @param $bonusType
     * @return float|int
     */
    public function getMaximumBonus($rate, $bonusType)
    {
        if ($this->char_code == 'CL' && $bonusType == BonusType::BONUS_TYPE_PERCENT) {
            // костыль для бонуса Чили
            return ($rate) ? (210000 / $rate) * 4.75 : 0;
        }
        return 0;
    }
}
