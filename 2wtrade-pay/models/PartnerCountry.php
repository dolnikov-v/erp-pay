<?php
namespace app\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\query\PartnerCountryQuery;

/**
 * This is the model class for table "partner_country".
 *
 * @property integer $partner_id
 * @property integer $country_id
 * @property integer $active
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Country $country
 * @property Partner $partner
 */
class PartnerCountry extends ActiveRecordLogUpdateTime
{
    const ACTIVE = 1;
    const NOT_ACTIVE = 0;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%partner_country}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['partner_id', 'country_id'], 'required'],
            [['partner_id', 'country_id', 'active', 'created_at', 'updated_at'], 'integer'],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_id' => 'id']],
            [['partner_id'], 'exist', 'skipOnError' => true, 'targetClass' => Partner::className(), 'targetAttribute' => ['partner_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'partner_id' => 'Partner ID',
            'country_id' => 'Country ID',
            'active' => 'Active',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartner()
    {
        return $this->hasOne(Partner::className(), ['id' => 'partner_id']);
    }

    /**
     * @inheritdoc
     * @return \app\models\query\PartnerCountryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PartnerCountryQuery(get_called_class());
    }
}
