<?php
namespace app\models;

use app\components\db\ActiveRecordLogUpdateTime;

/**
 * Class SourceProduct
 * @package app\models
 * @property integer $source_id
 * @property integer $product_id
 * @property Source $source
 * @property Product $product
 */

class SourceProduct extends ActiveRecordLogUpdateTime
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%source_product}}';
    }


    public function rules()
    {
        return [
            [['source_id', 'product_id'], 'required'],
            [['source_id', 'product_id'], 'integer'],
            [['product_id'], 'unique', 'targetAttribute' => ['product_id', 'source_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'source_id' => 'Source ID',
            'product_id' => 'Product ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSource()
    {
        return $this->hasOne(Source::className(), ['id' => 'source_id']);
    }
}