<?php

namespace app\models;

use app\models\query\ProductCategoryQuery;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "product_category".
 *
 * Class ProductCategory
 * @package app\models
 * @property integer $id
 * @property string $name
 * @property Product[] $products
 */
class ProductCategory extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_category}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'required'],
            ['name', 'string', 'max' => 128, 'tooLong' => Yii::t('common', 'Слишком длинное название.')],
            ['name','unique']
        ];
    }

    /**
     * @return ProductCategoryQuery
     */
    public static function find()
    {
        return new ProductCategoryQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Ид категории'),
            'name' => Yii::t('common', 'Категория'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['id' => 'product_id'])
            ->viaTable('product_link_category', ['category_id' => 'id']);
    }
}
