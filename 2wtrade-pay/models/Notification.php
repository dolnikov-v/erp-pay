<?php

namespace app\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\widgets\Label;
use Yii;

/**
 * Class Notification
 * @package app\models
 * @property integer $id
 * @property string $description
 * @property string $trigger
 * @property string $group
 * @property string $type
 * @property boolean $active
 * @property NotificationRole[] $notificationsRole;
 * @property NotificationUser[] $notificationsUser;
 * @property int $created_at [int(11)]
 * @property int $updated_at [int(11)]
 */
class Notification extends ActiveRecordLogUpdateTime
{
    const ACTIVE = 1;
    const NOT_ACTIVE = 0;

    const TRIGGER_LEAD_ADDED = 'lead.added';
    const TRIGGER_LEAD_NOT_ADDED = 'lead.not.added';
    const TRIGGER_ORDER_NOT_SENT_TO_COURIER = 'order.not.sent.to.courier';
    const TRIGGER_EXCEPTION_SENT_TO_COURIER = 'exception.sent.to.courier';
    const TRIGGER_DONT_GET_ORDER_INFO_FROM_COURIER = 'dont.get.order.info.from.courier';
    const TRIGGER_EXCEPTION_GET_ORDER_INFO_FROM_COURIER = 'exception.get.order.info.from.courier';
    const TRIGGER_CALLCENTER_RESPONSE_ERROR = 'callcenter.response.error';
    const TRIGGER_EXCEPTION_SENT_TO_CALL_CENTER = 'exception.sent.to.call.center';
    const TRIGGER_EXCEPTION_GET_STATUSES_FROM_CALL_CENTER = 'exception.get.statuses.from.call.center';
    const TRIGGER_REPORT_ORDER_DAILY = 'report.order.daily';
    const TRIGGER_REPORT_ORDER_TWO_DAYS_ERROR = 'report.order.two.days.error';
    const TRIGGER_ORDER_NOT_DELIVERED = 'order.not.delivered';
    const TRIGGER_PRODUCT_ON_STORAGE = 'product.on.storage';
    const TRIGGER_REPORT_ADCOMBO_DAILY_INFO = 'report.adcombo.daily-info';
    const TRIGGER_REPORT_ADCOMBO_DAILY_DANGER = 'report.adcombo.daily-danger';
    const TRIGGER_REPORT_ADCOMBO_HOURLY_INFO = 'report.adcombo.hourly.info';
    const TRIGGER_REPORT_ADCOMBO_HOURLY_DANGER = 'report.adcombo.hourly.danger';
    const TRIGGER_DELIVERY_ANOMALY_SUMMARY = 'delivery.status.anomaly';
    const TRIGGER_NO_PRODUCTS_FOR_ORDER = 'no.products.for.order';
    const TRIGGER_REPORT_DELIVERY_REJECTED = 'report.delivery.rejected';
    const TRIGGER_REPORT_CALL_CENTER_COMPARE_INFO = 'report.call-center-compare.info';
    const TRIGGER_REPORT_CALL_CENTER_COMPARE_DANGER = 'report.call-center-compare.danger';
    const TRIGGER_REPORT_STALE_CC_APPROVED = 'report.stale.cc-approved';
    const TRIGGER_REPORT_STALE_DELIVERY_PENDING = 'report.stale.delivery-pendng';
    const TRIGGER_REPORT_STALE_NEW_ORDER = 'report.stale.new-order';
    const TRIGGER_REPORT_NO_TRACK_SENT_ORDERS = 'report.no.track.sent.orders';
    const TRIGGER_REPORT_ORDER_DAILY_FLUCTUATION = 'report.order.daily.fluctuation';
    const TRIGGER_ORDER_LIST_AUTO_GENERATION_ERROR = 'order.list.auto-generation-error';
    const TRIGGER_REPORT_ORDERS_UNSENT = 'report.orders.unsent';                // Уведомления о неотправленных заказах находящизся в колонках 1, 2, 6, 19, 31
    const TRIGGER_REPORT_ORDERS_STATUS_IN_1_2 = 'report.orders.status.in.1.2';  // Скопление в колонках 1 и 2 в сумме по всем странам
    const TRIGGER_REPORT_ORDERS_STATUS_IN_1_2_BY_COUNTRY = 'report.orders.status.in.1.2.by.country'; // Скопление в колонках 1 и 2 с разбивкой по странам
    const TRIGGER_REPORT_ORDERS_STATUS_IN_31 = 'report.orders.status.in.31';    // Скопление заказов в 31й колонке
    const TRIGGER_REPORT_ORDERS_FROZEN = 'report.orders.frozen';    // Скопление заказов в 1,2,6,9,12,14,15,16,19,27,31,32
    const TRIGGER_REPORT_ORDERS_STILL_FROZEN = 'report.orders.still.frozen';    // Скопление заказов в 1,2,6,9,12,14,15,16,19,27,31,32 без улучшений в течении 2 часов
    const TRIGGER_REPORT_NO_ANSWER_FROM_CC = 'report.no.answer.from.cc';        // Не получали больше часа статусов из КЦ по всем странам
    const TRIGGER_GENERATE_EXPORT_FILE = 'generate.export.file';                // Закончена генерация заказанного файла экспорта
    const TRIGGER_SEND_ORDER_CLARIFICATION = 'send.order.clarification';        // Отправлены файлы с заказами на уточнение в КС
    const TRIGGER_LEAD_ABSENT_10MINUTS = 'lead.absent.10minuts';                // NPAY1294 - отсутствие лога success в api_log в течении 10 минут
    const TRIGGER_RAPID_INCREASE_ORDERS_IN_AUTOTRASH = 'report.rapid.increase.orders.in.autotrash'; // Быстрое увеличение количества заказов в автотреше
    const TRIGGER_SMS_BUYOUTS_NOTIFICATION = 'sms.buyouts.notification';        // SMS уведомление директоров о выкупе за 10 дней назад
    const TRIGGER_BUYOUTS_BY_COUNTRY = 'buyouts.by.country';        // Skype уведомление о выкупе за 7/15/30/45 дней назад
    const TRIGGER_ALL_TEAMS_BUYOUTS_NOTIFICATION = 'all.teams.buyouts.notification';        // уведомление по всем командам о выкупе за 7, 15, 30 дней назад
    const TRIGGER_TEAM_BUYOUTS_NOTIFICATION = 'team.buyouts.notification';        // уведомление выкупе по команде за 7, 15, 30 дней назад
    const TRIGGER_CALL_CENTER_API_GET_OPERATOR_WORK_TIME_ERROR = 'call-center.api.get-operator-work-time-error'; // Уведомление о ошибках api колл-центра при импорте отработаного времени операторов
    const TRIGGER_CALL_CENTER_API_GET_OPERATOR_NUMBER_OF_CALL_ERROR = 'call-center.api.get-operator-number-of-call-error'; // Уведомление о ошибках api колл-центра при импорте кол-ва звонков операторов
    const TRIGGER_CALL_CENTER_SMALL_APPROVE = 'cc.small.approve'; // Уведомление о снижении апрува
    const TRIGGER_CALL_CENTER_HOLD_LEADS = 'cc.hold.leads'; // Уведомление необзваниваемых заказах за сутки
    const TRIGGER_CALL_CENTER_NO_APPROVE_LAST_HOUR = 'cc.no.approve.last.hour'; // Уведомление о том что нет апрвувов за последний час
    const TRIGGER_CALL_CENTER_NO_STATISTIC_LAST_THREE_DAYS = 'cc.no.statistic.last.three.days'; // Уведомление о том что нет статистики за последние 3 дня
    const TRIGGER_CALL_CENTER_CHECK_DAY_APPROVE = 'cc.check.day.approve'; // Уведомление сколько из заказов получили апрув на сегодня с начала дня
    const TRIGGER_CALL_CENTER_DAILY_REPORT = 'cc.daily.report'; // Уведомление отчет работы кц
    const TRIGGER_CALL_CENTER_EFFICIENCY_OPERATORS = 'cc.efficiency.operators'; // Уведомление эффективности операторов
    const TRIGGER_CALL_CENTER_REPORT_ACTUALITY = 'ccr.actuality'; // предупреждение о задержке обновления статусов
    const TRIGGER_REPORT_HOLDS_NOTIFICATION = 'report.holds.notification'; // Уведомление о холдах по странам и командам
    const TRIGGER_ADCOMBO_DONT_GET_STATUSES = 'adcombo.dont.get.statuses'; // Уведомление о том, что адкомбо не забирает статусы
    const TRIGGER_MAKE_AUTO_PENALTY = 'make.auto.penalty'; // Выписан автоматический штраф сотруднику
    const TRIGGER_REPORT_ORDERS_STATUS_IN_12_9_37_38 = 'report.orders.status.in.12.9.37.38'; // Скопление в колонках 12, 9, 37, 38 с группировкой по странам и КС
    const TRIGGER_DELIVERY_HAS_NO_CONTRACT = 'delivery.has.no.contract'; // Нет данных по контракту, реквизитам, тарифам
    const TRIGGER_DELIVERY_NO_SEND_BY_DEBTS = 'delivery.no.send.by.debts'; // Запрет отправки заказов если есть не погашенная ДЗ более N дней
    const TRIGGER_DELIVERY_NO_SEND_BY_PROCESS = 'delivery.no.send.by.process'; // Запрет отправки заказов если есть заказы в процессе более N дней
    const NUMBER_ORDERS_TO_NOTICE = 1000;  // число заказов для уведомления разработичкам TRIGGER_REPORT_ORDERS_STATUS_*
    const TRIGGER_ORDER_LOGISTIC_CHECK_SENDING_LIST = 'order.logistic.check.sending.list'; // Проверка часовой задержки отправки листов
    const TRIGGER_CALL_CENTER_REQUESTS_ERROR_SUMMARY = 'call.center.requests.error.summary'; // ошибки заявок в КЦ
    const TRIGGER_DELIVERY_REQUESTS_ERROR_SUMMARY = 'delivery.requests.error.summary'; // ошибки заявок в КС
    const TRIGGER_SQL_ERROR_EXCEPTION = 'sql.error.exception'; // Уведомление об ошибках SQL
    const TRIGGER_CALL_CENTER_GET_STATUSES_ERROR = 'callcenter.getstatuses.error'; // Уведомление при ошибкеп получения статусов из КЦ
    const TRIGGER_DEBTS_NOTIFICATION_FOR_THREE_MONTH = 'debts.notification.three.month'; // Уведомление из ДЗ по 3 последним месяцам
    const TRIGGER_DELIVERY_DEBTS_DYNAMIC_RELOAD = 'delivery.debts.dynamic.reload';       // Закончена обновление данных для динамики дебиторской задолженности
    const TRIGGER_ADCOMBO_PARSER_ERROR = 'adcombo.parser.error'; // Ошибка получения данных из Adcombo
    const TRIGGER_AUTO_CHANGE_DELIVERY_ERROR = 'auto.change.delivery.error'; // Ошибка автоматической смены КС

    const TYPE_PRIMARY = 'primary';
    const TYPE_SUCCESS = 'success';
    const TYPE_INFO = 'info';
    const TYPE_WARNING = 'warning';
    const TYPE_DANGER = 'danger';

    const GROUP_SYSTEM = 'system';
    const GROUP_COMMON = 'common';
    const GROUP_REPORT = 'report';

    /**
     * @return array
     */
    public static function getTypesCollection()
    {
        return [
            self::TYPE_PRIMARY => Yii::t('common', 'Обычный'),
            self::TYPE_SUCCESS => Yii::t('common', 'Успешный'),
            self::TYPE_INFO => Yii::t('common', 'Информационный'),
            self::TYPE_WARNING => Yii::t('common', 'Важный'),
            self::TYPE_DANGER => Yii::t('common', 'Опасный'),
        ];
    }

    /**
     * @return array
     */
    public static function getGroupCollection()
    {
        return [
            self::GROUP_SYSTEM => Yii::t('common', 'Системная'),
            self::GROUP_COMMON => Yii::t('common', 'Обычная'),
            self::GROUP_REPORT => Yii::t('common', 'Отчетная'),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%notification}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description', 'trigger'], 'required'],
            [['description', 'trigger', 'group', 'type'], 'string', 'max' => 255],
            [['trigger'], 'unique'],
            [['active'], 'boolean'],
            [['type'], 'in', 'range' => array_keys(Notification::getTypesCollection())],
            [['group'], 'in', 'range' => array_keys(Notification::getGroupCollection())],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'description' => Yii::t('common', 'Описание'),
            'trigger' => Yii::t('common', 'Имя'),
            'type' => Yii::t('common', 'Тип'),
            'group' => Yii::t('common', 'Группа'),
            'active' => Yii::t('common', 'Доступность'),
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
        ];
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function renderLabel()
    {
        $style = Label::STYLE_PRIMARY;

        if ($this->type == Notification::TYPE_SUCCESS) {
            $style = Label::STYLE_SUCCESS;
        } elseif ($this->type == Notification::TYPE_INFO) {
            $style = Label::STYLE_INFO;
        } elseif ($this->type == Notification::TYPE_WARNING) {
            $style = Label::STYLE_WARNING;
        } elseif ($this->type == Notification::TYPE_DANGER) {
            $style = Label::STYLE_DANGER;
        }

        return Label::widget([
            'label' => Yii::t('common', Notification::getTypesCollection()[$this->type]),
            'style' => $style,
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotificationsUser()
    {
        return $this->hasMany(NotificationUser::className(), ['trigger' => 'trigger']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotificationsRole()
    {
        return $this->hasMany(NotificationRole::className(), ['trigger' => 'trigger']);
    }
}
