<?php

namespace app\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\helpers\Utils;
use yii\helpers\BaseFileHelper;
use Yii;

/**
 * Class Certificate
 * @package app\models
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property bool $is_partner
 * @property integer $country_id
 * @property integer $product_id
 * @property string $country
 * @property string $product
 * @property integer $created_at
 * @property integer $updated_at
 * @property array $file
 */
class Certificate extends ActiveRecordLogUpdateTime
{
    /**
     * @var UploadedFile [] |Null file attribute
     */
    public $files;

    /**
     *  for widget app\modules\widget\widgets\certificates;
     */
    public $status_certificate;

    /**
     *  for widget app\modules\widget\widgets\certificates;
     */
    public $product_names;

    /**
     * @var integer
     */
    public $fileCount;

    /**
     * @var string
     */
    public $country;

    /**
     * @var string
     */
    public $product;

    /**
     * @var array
     */
    public $uploadedFilesNames = [];

    /**
     * @inheritdoc
     */
    public $UploadForm;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_certificate}}';
    }

    /**
     * @return array
     */
    public function getid()
    {
        return $this->hasOne(Certificate::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'country_id', 'product_id'], 'required'],
            ['name', 'string', 'max' => 255],
            ['description', 'string', 'max' => 1000],
            ['is_partner', 'boolean'],
            ['is_partner', 'default', 'value' => true],
            ['certificate_not_needed', 'default', 'value' => false],
            [['country_id', 'product_id'], 'string', 'max' => 255],

            [['files'], 'file', 'extensions' => 'jpg, png, pdf', 'maxFiles' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'name' => Yii::t('common', 'Название'),
            'is_partner' => Yii::t('common', 'Партнёрский сертификат'),
            'certificate_not_needed' => Yii::t('common', 'Сертификат не нужен'),
            'description' => Yii::t('common', 'Описание сертификата'),
            'country_id' => Yii::t('common', 'Страна'),
            'product_id' => Yii::t('common', 'Товар'),
            'availability' => Yii::t('common', 'Наличие товара на складе'),
            'created_at' => Yii::t('common', 'Создан'),
            'updated_at' => Yii::t('common', 'Обновлен'),
            'files' => Yii::t('common', 'Файлы сертификата'),
            'fileCount' => Yii::t('common', 'Файлы сертификата')

        ];
    }

    /**
     * @param string
     * @param bool
     * @return string
     */
    public function prepareName($origfilename, $unicate = false)
    {
        return ($unicate) ? md5($origfilename . microtime()) : $origfilename;
    }

    /**
     * @return bool
     */
    public function upload()
    {
        Utils::prepareDir(Yii::getAlias('@certificatesFolder'));

        if ($this->validate()) {
            foreach ($this->files as $file) {
                $unicfilename = $this->prepareName($file->baseName, true);
                $file->saveAs(Yii::getAlias('@certificatesFolder') . DIRECTORY_SEPARATOR . $unicfilename . '.' . $file->extension);

                $this->uploadedFilesNames[] = [
                    'origfilename' => $this->prepareName($file->baseName) . '.' . $file->extension,
                    'unicfilename' => $unicfilename . '.' . $file->extension
                ];
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Проверка наличия товара в стране (через склад)
     * @return integer
     */
    public function checkProductInCountry($product_id, $country_id)
    {
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("select count(s.id) as availability from storage_part sp left join `storage` s on sp.storage_id = s.id where sp.product_id = :product_id and s.country_id = :country_id",
            [
                ':product_id' => $product_id,
                ':country_id' => $country_id
            ]
        );
        $result = $command->queryAll();
        return $result[0]['availability'];
    }
}
