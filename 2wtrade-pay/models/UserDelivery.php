<?php
namespace app\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\query\UserDeliveryQuery;
use app\modules\access\widgets\Delivery;
use Yii;

/**
 * Class UserDelivery
 * @package app\models
 * @property integer $id
 * @property integer $user_id
 * @property integer $delivery_id
 * @property Delivery $delivery
 * @property integer $active
 * @property integer $created_at
 * @property integer $updated_at
 * @property User $user
 */
class UserDelivery extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_delivery}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'delivery_id'], 'required'],

            [
                'user_id',
                'exist',
                'targetClass' => '\app\models\User',
                'targetAttribute' => 'id',
                'message' => Yii::t('common', 'Неверное значение пользователя.')
            ],
            [
                'delivery_id',
                'exist',
                'targetClass' => '\app\modules\delivery\models\Delivery',
                'targetAttribute' => 'id',
                'message' => Yii::t('common', 'Неверное значение связи со службой доставки.')
            ],

            ['active', 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'user_id' => Yii::t('common', 'Пользователь'),
            'delivery_id' => Yii::t('common', 'Связь со службой доставки'),
        ];
    }

    /**
     * @return UserDeliveryQuery
     */
    public static function find()
    {
        return new UserDeliveryQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDelivery()
    {
        return $this->hasOne(Delivery::className(), ['id' => 'delivery_id']);
    }

}
