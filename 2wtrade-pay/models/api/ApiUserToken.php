<?php
namespace app\models\api;

use app\components\db\ActiveRecordLogUpdateTime;
use app\components\rest\User;
use app\models\query\ApiUserTokenQuery;
use Yii;

/**
 * Class ApiUserToken
 * @property integer $id
 * @property integer $user_id
 * @property string $type
 * @property string $auth_token
 * @property integer $lifetime
 * @property integer $created_at
 * @property integer $updated_at
 * @property ApiUser $user
 */
class ApiUserToken extends ActiveRecordLogUpdateTime
{
    const TYPE_DELIVERY = 'delivery';
    const TYPE_CALL_CENTER = 'call_center';
    const TYPE_SOURCE = 'source';

    /**
     * @return array
     */
    public static function getTypesUser()
    {
        return [
            self::TYPE_DELIVERY => Yii::t('common', 'Служба доставки'),
            self::TYPE_CALL_CENTER => Yii::t('common', 'Колл-центр'),
            self::TYPE_SOURCE => Yii::t('common', 'Источник'),
        ];
    }

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%api_user_token}}';
    }

    /**
     * @return ApiUserTokenQuery
     */
    public static function find()
    {
        return new ApiUserTokenQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['user_id', 'lifetime'], 'integer'],
            [['auth_token'], 'string', 'max' => 100],
            ['type', 'in', 'range' => array_keys(self::getTypesUser())],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'user_id' => Yii::t('common', 'Пользователь'),
            'auth_token' => Yii::t('common', 'Токен'),
            'type' => Yii::t('common', 'Тип'),
            'lifetime' => Yii::t('common', 'Время жизни до'),
            'created_at' => Yii::t('common', 'Дата добавления'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
        ];
    }

    /**
     * @return string
     */
    public function generateToken()
    {
        return sha1(uniqid() . time());
    }

    /**
     * @return int
     */
    public function getLifetimeToken()
    {
        return time() + User::$lifetime;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(ApiUser::className(), ['id' => 'user_id']);
    }
}
