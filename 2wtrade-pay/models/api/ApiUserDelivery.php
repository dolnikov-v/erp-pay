<?php
namespace app\models\api;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\query\ApiUserDeliveryQuery;
use app\modules\delivery\models\Delivery;
use Yii;

/**
 * Class ApiUserDelivery
 * @property integer $id
 * @property integer $user_id
 * @property integer $delivery_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property Delivery $delivery
 */
class ApiUserDelivery extends ActiveRecordLogUpdateTime
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%api_user_delivery}}';
    }

    /**
     * @return ApiUserDeliveryQuery
     */
    public static function find()
    {
        return new ApiUserDeliveryQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['user_id', 'delivery_id'], 'required'],
            [['user_id', 'delivery_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'user_id' => Yii::t('common', 'Пользователь'),
            'delivery_id' => Yii::t('common', 'Служба доставки'),
            'created_at' => Yii::t('common', 'Дата добавления'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDelivery()
    {
        return $this->hasOne(Delivery::className(), ['id' => 'delivery_id']);
    }
}
