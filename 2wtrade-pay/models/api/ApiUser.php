<?php
namespace app\models\api;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\query\ApiUserQuery;
use app\models\Source;
use Yii;

/**
 * Class ApiUser
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property integer $status
 * @property integer $source_id
 * @property string $type
 * @property integer $lifetime
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Source $source
 */
class ApiUser extends ActiveRecordLogUpdateTime
{
    const SCENARIO_INSERT = 'insert';
    const SCENARIO_UPDATE = 'update';

    const STATUS_ACTIVE = 1;
    const STATUS_NO_ACTIVE = 0;

    /**
     * @return array
     */
    public static function getStatuses()
    {
        return [
            self::STATUS_ACTIVE => Yii::t('common', 'Активный'),
            self::STATUS_NO_ACTIVE => Yii::t('common', 'Не активный'),
        ];
    }

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%api_user}}';
    }

    /**
     * @return ApiUserQuery
     */
    public static function find()
    {
        return new ApiUserQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['username', 'required', 'on' => ['insert', 'update']],
            ['password', 'required', 'on' => 'insert'],
            ['password', 'validateSetPassword', 'skipOnEmpty' => false],
            [['username', 'password'], 'string', 'max' => 64],
            [
                'username',
                'unique',
                'targetClass' => '\app\models\api\ApiUser',
                'message' => Yii::t('common', 'Это имя пользователя уже используется.')
            ],
            ['status', 'default', 'value' => 0],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'username' => Yii::t('common', 'Имя пользователя'),
            'password' => Yii::t('common', 'Пароль'),
            'status' => Yii::t('common', 'Статус'),
            'source_id' => Yii::t('common', 'Название источника'),
            'created_at' => Yii::t('common', 'Дата добавления'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
        ];
    }

    /**
     * @param string $attribute
     * @param array $params
     */
    public function validateSetPassword($attribute, $params)
    {
        if ($this->isNewRecord) {
            $this->setPassword($this->$attribute);
        } else {
            if (array_key_exists($attribute, $this->getDirtyAttributes())) {
                $newValue = trim($this->$attribute);
                $hash = $this->getOldAttribute($attribute);

                if (!empty($newValue) && !Yii::$app->security->validatePassword($newValue, $hash)) {
                    $this->setPassword($this->$attribute);
                } else {
                    $this->$attribute = $this->getOldAttribute($attribute);
                }
            }
        }
    }

    /**
     * @param string $password
     * @return boolean
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSource()
    {
        return $this->hasOne(Source::class, ['id' => 'source_id']);
    }
}
