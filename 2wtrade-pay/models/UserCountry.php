<?php
namespace app\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\query\UserCountryQuery;
use Yii;

/**
 * Class UserCountry
 * @package app\models
 * @property integer $id
 * @property integer $user_id
 * @property integer $country_id
 * @property Country $country
 * @property integer $active
 * @property integer $created_at
 * @property integer $updated_at
 * @property User $user
 */
class UserCountry extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_country}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'country_id'], 'required'],

            [
                'user_id',
                'exist',
                'targetClass' => '\app\models\User',
                'targetAttribute' => 'id',
                'message' => Yii::t('common', 'Неверное значение пользователя.')
            ],
            [
                'country_id',
                'exist',
                'targetClass' => '\app\models\Country',
                'targetAttribute' => 'id',
                'message' => Yii::t('common', 'Неверное значение связи со страной.')
            ],

            ['active', 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'user_id' => Yii::t('common', 'Пользователь'),
            'database_id' => Yii::t('common', 'Связь со страной'),
        ];
    }

    /**
     * @return UserCountryQuery
     */
    public static function find()
    {
        return new UserCountryQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

}
