<?php
namespace app\models;

use app\components\db\ActiveRecordLogUpdateTime;
use Yii;
use yii\rbac\Item;

/**
 * Class NotificationRole
 * @package app\models
 * @property integer $id
 * @property string $role
 * @property string $trigger
 * @property integer $running_line_active
 * @property Notification $notification
 * @property AuthItem $authItem
 */
class NotificationRole extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%notification_role}}';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['role', 'trigger'], 'required'],
            [['trigger'], 'string', 'max' => 255],
            [['role'], 'string', 'max' => 32],
            [['running_line_active'], 'integer'],
            [['running_line_active'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'role' => Yii::t('common', 'Роль'),
            'trigger' => Yii::t('common', 'Имя'),
            'running_line_active'=> Yii::t('common', 'В бегущую строку'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotification()
    {
        return $this->hasOne(Notification::className(), ['trigger' => 'trigger']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthItem()
    {
        return $this->hasOne(AuthItem::className(), ['name' => 'role', 'type' => Item::TYPE_ROLE]);
    }

}
