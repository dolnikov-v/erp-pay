<?php
namespace app\models;

use app\components\db\ActiveRecordLogUpdateTime;
use Yii;

/**
 * Class CertificateFile
 * @package app\models
 * @property integer $product_certificate_id
 * @property string $unicfilename
 * @property string $origfilename
 */
class CertificateFile extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_certificate_file}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['origfilename', 'unicfilename', 'product_certificate_id'], 'required'],
            ['origfilename', 'string', 'max' => 500],
            ['unicfilename', 'string', 'max' => 500],
            ['product_certificate_id', 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'origfilename' => Yii::t('common', 'Оригинальное имя файла'),
            'unicfilename' => Yii::t('common', 'Системное имя файла'),
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCertificate()
    {
        return $this->hasOne(Certificate::className(), ['id' => 'product_certificate_id']);
    }
}