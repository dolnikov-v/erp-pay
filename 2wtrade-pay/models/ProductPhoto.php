<?php

namespace app\models;

use app\components\db\ActiveRecordLogUpdateTime;
use Yii;
use yii\helpers\BaseFileHelper;
use yii\web\UploadedFile;

/**
 * Class ProductPhoto
 * @package app\models
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $country_id
 * @property string $image
 */
class ProductPhoto extends ActiveRecordLogUpdateTime
{
    /**
     * @var UploadedFile
     */
    public $imageFile;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_photo}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'country_id'], 'required'],
            ['image', 'string'],
            [['product_id', 'country_id', 'created_at', 'updated_at'], 'integer'],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_id' => 'id']],
            [['country_id'], 'unique', 'targetAttribute' => ['product_id', 'country_id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ИД'),
            'product_id' => Yii::t('common', 'Товар'),
            'country_id' => Yii::t('common', 'Страна'),
            'image' => Yii::t('common', 'Изображение'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата обновления'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return bool|string
     * @throws \yii\base\Exception
     */
    public function upload()
    {
        if ($this->validate() && $this->imageFile) {
            if (!file_exists(self::getPathFile())) {
                BaseFileHelper::createDirectory(self::getPathFile());
            }
            $uniqueFileName = $this->getUniqueName($this->imageFile->baseName) . '.' . $this->imageFile->extension;
            $this->imageFile->saveAs(self::getPathFile($uniqueFileName));
            return $uniqueFileName;
        } else {
            return false;
        }
    }

    /**
     * @param string $image
     * @return string
     */
    public static function getPathFile($image = "")
    {
        $path = Yii::getAlias('@media') . DIRECTORY_SEPARATOR . 'product';
        if ($image) {
            $path .= DIRECTORY_SEPARATOR . $image;
        }
        return $path;
    }

    /**
     * @param string $name
     * @return string
     */
    public function getUniqueName($name)
    {
        return md5($name . microtime());
    }

    /**
     * @return array
     */
    public function getFreeCountries()
    {
        $countries = Yii::$app->user->identity->getAllowCountries();
        if ($countries) {
            foreach (self::find()->where(['product_id' => $this->product_id])->all() as $record) {
                if ($record->country_id != $this->country_id) {
                    unset($countries[$record->country_id]);
                }
            }
            foreach ($countries as $key => $val) {
                $countries[$key] = Yii::t('common', $val);
            }
        }
        return $countries ?? [];
    }
}
