<?php
namespace app\models;

use app\components\db\ActiveRecordLogUpdateTime;
use Yii;


/**
 * Class SourceCountry
 * @package app\models
 * @property integer $source_id
 * @property integer $country_id
 * @property \yii\db\ActiveQuery $source
 * @property \yii\db\ActiveQuery $country
 * @property bool $active
 */

class SourceCountry extends ActiveRecordLogUpdateTime
{

    const INSERT_SCENARIO = 'insert';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%source_country}}';
    }


    public function rules()
    {
        return [
            [['source_id', 'country_id'], 'required'],
            /*[['source_id', 'country_id'],
                'unique', 'targetAttribute' => ['source_id'],
                'on' => 'insert'
            ],*/
            [['source_id', 'country_id', 'active'], 'integer'],
            ['active', 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'source_id' => 'Source ID',
            'country_id' => 'Country ID',
            'active' => 'active',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSource()
    {
        return $this->hasOne(Source::className(), ['id' => 'source_id']);
    }
}