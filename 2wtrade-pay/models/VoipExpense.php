<?php
namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * Class VoipExpense
 * @package app\models
 * @property integer $id
 * @property float $amount
 * @property float $amount_usd
 * @property float $asr
 * @property string $date_stat
 * @property integer $country_id
 * @property integer $launched_at
 */

class VoipExpense extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%voip_expense}}';
    }


    public function rules()
    {
        return [
            [['amount', 'date_stat', 'country_id'], 'required'],
            [['amount'], 'unique', 'targetAttribute' => ['date_stat', 'country_id']],
            [['amount', 'amount_usd', 'asr'], 'double'],
            [['date_stat'], 'safe'],
            [['country_id', 'launched_at'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'amount' => Yii::t('common', 'Сумма'),
            'amount_usd' => Yii::t('common', 'Сумма в USD'),
            'asr' => Yii::t('common', 'ASR'),
            'date_stat' => Yii::t('common', 'Дата'),
            'country_id' => Yii::t('common', 'Страна'),
            'launched_at' => Yii::t('common', 'Дата забора данных'),
        ];
    }
}