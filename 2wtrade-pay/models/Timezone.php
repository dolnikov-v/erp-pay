<?php
namespace app\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\query\TimezoneQuery;
use Yii;

/**
 * Class Timezone
 * @package app\models
 * @property integer $id
 * @property integer $time_offset
 * @property string $name
 * @property string $timezone_id
 *
 * @method static Timezone findOne($condition)
 */
class Timezone extends ActiveRecordLogUpdateTime
{
    const DEFAULT_TIMEZONE = 'UTC';
    const DEFAULT_OFFSET = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%timezone}}';
    }

    /**
     * @return TimezoneQuery
     */
    public static function find()
    {
        return new TimezoneQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'time_offset' => Yii::t('common', 'Смещение'),
            'name' => Yii::t('common', 'Название'),
            'timezone_id' => Yii::t('common', 'Временная зона'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * @return Timezone
     */
    public static function getDefault()
    {
        return self::find()->byTimezoneId(self::DEFAULT_TIMEZONE)->one();
    }
}
