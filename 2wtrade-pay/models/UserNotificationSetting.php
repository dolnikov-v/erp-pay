<?php
namespace app\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\query\UserNotificationSettingQuery;
use Yii;

/**
 * Class UserNotificationSetting
 * @package app\models
 * @property integer $id
 * @property integer $active
 * @property string $trigger
 * @property string $user_id
 * @property string $email_interval
 * @property string $sms_active
 * @property string $telegram_active
 * @property string $skype_active
 * @property Notification $notification
 * @property User $user
 *
 */
class UserNotificationSetting extends ActiveRecordLogUpdateTime
{
    const ACTIVE = 1;
    const NOT_ACTIVE = 0;
    const INTERVAL_ALWAYS = 'always';
    const INTERVAL_HOURLY = 'hourly';
    const INTERVAL_DAILY = 'daily';

    /**
     * @return array
     */
    public static function getIntervalCollection()
    {
        return [
            self::INTERVAL_ALWAYS => Yii::t('common', 'Всегда'),
            self::INTERVAL_HOURLY => Yii::t('common', 'Ежечасно'),
            self::INTERVAL_DAILY => Yii::t('common', 'Ежедневно'),
        ];
    }

    /**
     * @param $data
     * @return mixed|string
     */
    public static function getLabelsInterval($data)
    {
        $listInterval = self::getIntervalCollection();

        if (array_key_exists((string)$data, $listInterval)) {
            return $listInterval[$data];
        } else {
            return Yii::t('common', 'Никогда');
        }
    }

    /**
     * @return array
     */
    public static function getIntervalsForDropdown()
    {
        return [
            [
                'label' => self::getLabelsInterval(false),
                'url' => '',
            ],
            [
                'label' => self::getLabelsInterval(self::INTERVAL_ALWAYS),
                'url' => self::INTERVAL_ALWAYS,
            ],
            [
                'label' => self::getLabelsInterval(self::INTERVAL_HOURLY),
                'url' => self::INTERVAL_HOURLY,
            ],
            [
                'label' => self::getLabelsInterval(self::INTERVAL_DAILY),
                'url' => self::INTERVAL_DAILY,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_notification_setting}}';
    }

    /**
     * @return UserNotificationSettingQuery
     */
    public static function find()
    {
        return new UserNotificationSettingQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'trigger'], 'required'],
            [['trigger'], 'string', 'max' => 255],
            [['user_id', 'active', 'sms_active', 'telegram_active', 'skype_active'], 'integer'],
            [['active'], 'default', 'value' => 1],
            [['sms_active', 'telegram_active', 'skype_active'], 'default', 'value' => 0],
            [['email_interval'], 'in',
                'range' => array_keys(self::getIntervalCollection()),
                'message' => 'Не существует такого интервала'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'user_id' => Yii::t('common', 'Пользователь'),
            'trigger' => Yii::t('common', 'Имя'),
            'active' => Yii::t('common', 'Активность'),
            'email_interval' => Yii::t('common', 'Отправка по Email'),
            'sms_active' => Yii::t('common', 'Отправка по SMS'),
            'telegram_active' => Yii::t('common', 'Отправка в Telegram'),
            'skype_active' => Yii::t('common', 'Отправка в Skype'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotification()
    {
        return $this->hasOne(Notification::className(), ['trigger' => 'trigger']);
    }

}
