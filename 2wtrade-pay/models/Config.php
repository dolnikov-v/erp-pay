<?php
namespace app\models;

use app\components\db\ActiveRecordLogUpdateTime;
use Yii;

/**
 * Class Config
 * @package app\models
 * @property string $name
 * @property string $description
 * @property string $value
 */
class Config extends ActiveRecordLogUpdateTime
{
    const ORDER_STATUSES_LINK_SCHEMA = 'ORDER_STATUSES_LINK_SCHEMA';
    const SCHEMA_DUMP_IGNORE_DATABASES = 'SCHEMA_DUMP_IGNORE_DATABASES';
    const SCHEMA_DUMP_IGNORE_TABLES = 'SCHEMA_DUMP_IGNORE_TABLES';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%config}}';
    }

    /**
     * @param $name
     * @return Config
     */
    public static function findByName($name)
    {
        $result = self::find()
            ->where(['name' => $name])
            ->all();

        if (count($result) == 1) {
            $result = $result[0];
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'required'],
            ['name', 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('common', 'Название'),
            'description' => Yii::t('common', 'Описание'),
            'value' => Yii::t('common', 'Значение'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }
}
