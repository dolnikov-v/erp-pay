<?php
namespace app\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\query\UserActionLogQueryQuery;
use Yii;

/**
 * Class UserActionLogQuery
 * @package app\models
 * @property integer $id
 * @property integer $action_log_id
 * @property string $query
 * @property integer $created_at
 * @property UserActionLog $actionLog
 */
class UserActionLogQuery extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_action_log_query}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['action_log_id', 'query'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'action_log_id' => Yii::t('common', 'Действие'),
            'query' => Yii::t('common', 'Запрос'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecordLogUpdateTime::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }

    /**
     * @return UserActionLogQueryQuery
     */
    public static function find()
    {
        return new UserActionLogQueryQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActionLog()
    {
        return $this->hasOne(UserActionLog::className(), ['id' => 'action_log_id']);
    }
}
