<?php
namespace app\models\search;

use app\models\User;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * Class UserSearch
 * @package app\models
 */
class UserSearch extends User
{
    public $username;
    public $email;
    public $role;
    public $country;
    public $source;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username','email','role', 'country', 'source'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'username' => Yii::t('common', 'Имя пользователя'),
            'email' => Yii::t('common', 'Электронная почта'),
            'role' => Yii::t('common', 'Роль'),
            'country' => Yii::t('common', 'Страна'),
            'source' => Yii::t('common', 'Источник'),
        ];
    }

    /**
     * @param array $params
     * @param bool $withPagination
     * @return ActiveDataProvider
     */
    public function search($params = [], $withPagination = true)
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['id'] = SORT_DESC;
        }

        $query = User::find()
            ->joinWith('roles')
            ->orderBy($sort);

        $query->andWhere('NOT (status & ' . User::STATUS_DELETED . ')');

        $config = [
            'query' => $query,
        ];

        $config['pagination'] = [
            'pageSize' => $withPagination == true ? 50 : 0,
        ];

        $dataProvider = new ActiveDataProvider($config);

        if (!$this->load($params) || !$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['item_name' => $this->role]);
        $query->andFilterWhere(['like', 'username', $this->username]);
        $query->andFilterWhere(['like', 'email', $this->email]);

        if ($this->country) {
            $query->joinWith(['userCountry'])->where(['user_country.country_id' => $this->country]);
        }

        if ($this->source) {
            $query->joinWith(['userSource'])->where(['user_source.source_id' => $this->source]);
        }

        return $dataProvider;
    }
}
