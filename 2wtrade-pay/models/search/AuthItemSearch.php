<?php
namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ArrayDataProvider;
use yii\rbac\Item;

/**
 * Class AuthItemSearch
 * @package app\models
 */
class AuthItemSearch extends Model
{
    const SECTION_HOME = 'home';
    const SECTION_ORDER = 'order';
    const SECTION_STORAGE = 'storage';
    const SECTION_CALL_CENTER = 'callcenter';
    const SECTION_DELIVERY = 'delivery';
    const SECTION_CATALOG = 'catalog';
    const SECTION_I18N = 'i18n';
    const SECTION_ACCESS = 'access';
    const SECTION_ADMINISTRATION = 'administration';
    const SECTION_PROFILE = 'profile';
    const SECTION_REPORT = 'report';

    public $type;

    public $sections = [];
    public $sectionsCollection = [];

    public $section;
    public $name;

    /**
     * @param integer $type
     * @param array $config
     */
    public function __construct($type, $config = [])
    {
        parent::__construct($config);

        $this->type = $type;
        $this->initSections();
    }

    /**
     * Инициализация разделов
     */
    public function initSections()
    {
        $this->sections = [
            self::SECTION_HOME => ['home.'],
            self::SECTION_ORDER => ['order.'],
            self::SECTION_STORAGE => ['storage.'],
            self::SECTION_CALL_CENTER => ['callcenter.'],
            self::SECTION_DELIVERY => ['delivery.'],
            self::SECTION_CATALOG => ['catalog.'],
            self::SECTION_I18N => ['i18n.'],
            self::SECTION_ACCESS => ['access.'],
            self::SECTION_ADMINISTRATION => ['administration.'],
            self::SECTION_PROFILE => ['profile.'],
            self::SECTION_REPORT => ['report.'],
        ];

        $this->sectionsCollection = [
            '' => '—',
            self::SECTION_HOME => Yii::t('common', 'Домашняя страница'),
            self::SECTION_ORDER => Yii::t('common', 'Заказы'),
            self::SECTION_STORAGE => Yii::t('common', 'Склады'),
            self::SECTION_CALL_CENTER => Yii::t('common', 'Колл-центры'),
            self::SECTION_DELIVERY => Yii::t('common', 'Службы доставки'),
            self::SECTION_CATALOG => Yii::t('common', 'Справочники'),
            self::SECTION_I18N => Yii::t('common', 'Интернационализация'),
            self::SECTION_ACCESS => Yii::t('common', 'Доступы'),
            self::SECTION_ADMINISTRATION => Yii::t('common', 'Администрирование'),
            self::SECTION_PROFILE => Yii::t('common', 'Профиль'),
            self::SECTION_REPORT => Yii::t('common', 'Отчеты'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'section'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'section' => Yii::t('common', 'Раздел'),
            'name' => Yii::t('common', 'Операция'),
        ];
    }

    /**
     * @param array $params
     * @param boolean $withPagination
     * @return ArrayDataProvider
     */
    public function search($params = [], $withPagination = true)
    {
        if ($this->type == Item::TYPE_ROLE) {
            $items = Yii::$app->authManager->getRoles();
        } else {
            $items = Yii::$app->authManager->getPermissions();
        }

        $this->load($params);
        $items = $this->applyParams($items);
        $items = $this->applySort($items);

        $config = [
            'allModels' => $items,
            'pagination' => [
                'pageSize' => $withPagination == true ? 20 : 0,
            ],
        ];

        return new ArrayDataProvider($config);
    }

    /**
     * @param $items
     * @return mixed
     */
    private function applyParams($items)
    {
        if (!empty($this->section) && !empty($this->sections[$this->section])) {
            $likes = $this->sections[$this->section];

            foreach ($items as $name => $item) {
                $find = false;

                foreach ($likes as $like) {
                    /** @var \yii\rbac\Role|\yii\rbac\Permission $item */
                    if (mb_strpos($item->name, $like) === 0) {
                        $find = true;

                        break;
                    }
                }

                if (!$find) {
                    unset($items[$name]);
                }
            }
        }

        if (!empty($this->name)) {
            foreach ($items as $name => $item) {
                /** @var \yii\rbac\Role|\yii\rbac\Permission $item */
                if (mb_strpos($item->name, $this->name) === false &&
                    mb_strpos($item->description, $this->name) === false) {
                    unset($items[$name]);
                }
            }
        }

        return $items;
    }

    /**
     * @param $items
     * @return mixed
     */
    private function applySort($items)
    {
        if ($this->type == Item::TYPE_ROLE) {
            uasort($items, function ($a, $b) {
                if ($a->createdAt == $b->createdAt) {
                    return 0;
                }

                return ($a->createdAt > $b->createdAt) ? -1 : 1;
            });
        } else {
            uasort($items, function ($a, $b) {
                return ($a->name < $b->name) ? -1 : 1;
            });
        }

        return $items;
    }
}
