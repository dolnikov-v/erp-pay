<?php

namespace app\models\search;

use app\models\Product;
use app\models\Source;
use app\models\SourceProduct;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * Class ProductSearch
 * @package app\models\search
 *
 */
class ProductSearch extends Product
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'source', 'use_days', 'sku', 'weight', 'width', 'height', 'length'], 'safe']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['id'] = SORT_ASC;
        }

        $query = Product::find()->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
