<?php
namespace app\models\search;

use app\models\UserNotification;
use app\models\Notification;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * Class UserNotificationSearch
 * @package app\models\search
 */
class UserNotificationSearch extends UserNotification
{
    const UNREAD = 'unread';
    const READ = 'read';

    public $dateFrom;
    public $dateTo;
    public $group;
    public $type;
    public $read;
    public $country;

    /**
     * @return array
     */
    public static function getReadCollection()
    {
        return [
            self::UNREAD => Yii::t('common', 'Показывать только новые'),
            self::READ => Yii::t('common', 'Показывать только прочитанные'),
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['dateFrom', 'dateTo', 'group', 'type', 'read', 'country'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата прочтения',
            'country' => 'Страна',
            'group' => 'Группа',
            'type' => 'Тип',
            'read' => 'Уведомления',
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['id'] = SORT_DESC;
        }

        $query = UserNotification::find()
            ->joinWith('notification', 'country')
            ->where(['user_id' => Yii::$app->user->id])
            ->orderBy($sort);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if ($this->read == self::UNREAD) {
            $query->andWhere(['read' => UserNotification::UNREAD]);
        } elseif ($this->read == self::READ) {
            $query->andWhere(['read' => UserNotification::READ]);
        }

        $query->andFilterWhere([UserNotification::tableName() . '.country_id' => $this->country]);

        $query->andFilterWhere([Notification::tableName() . '.group' => $this->group]);
        $query->andFilterWhere([Notification::tableName() . '.type' => $this->type]);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if ($this->dateFrom && $dateFrom = Yii::$app->formatter->asTimestamp($this->dateFrom)) {
            $query->andFilterWhere(['>=', UserNotification::tableName() . '.created_at', $dateFrom]);
        }

        if ($this->dateTo && $dateTo = Yii::$app->formatter->asTimestamp($this->dateTo)) {
            $query->andFilterWhere(['<=', UserNotification::tableName() . '.created_at', $dateTo + 86399]);
        }

        return $dataProvider;
    }
}
