<?php
namespace app\models\search;

use app\models\Certificate;
use app\models\Country;
use app\models\Product;
use app\models\User;
use app\modules\order\models\Order;
use app\modules\order\models\OrderProduct;
use app\models\CertificateFile;
use yii\data\ActiveDataProvider;

/**
 * Class CertificateSearch
 * @package app\models\search
 */
class CertificateSearch extends Certificate
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'product_id', 'country_id'], 'safe']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['id'] = SORT_ASC;
        }

        $query = Certificate::find()
            ->select([
                'id' => Certificate::tableName() . '.id',
                'name' => Certificate::tableName() . '.name',
                'description' => Certificate::tableName() . '.description',
                'country_id' => Country::tableName() . '.id',
                'country' => Country::tableName() . '.name',
                'product_id' => Product::tableName() . '.id',
                'product' => Product::tableName() . '.name',
                'fileCount' => '(select count(id) from ' . CertificateFile::tableName() . ' where ' . CertificateFile::tableName() . '.product_certificate_id = ' . Certificate::tableName() . '.id)',
                'is_partner' => Certificate::tableName() . '.is_partner',
                'certificate_not_needed' => Certificate::tableName() . '.certificate_not_needed',
                'created_at' => Certificate::tableName() . '.created_at',
                'updated_at' => Certificate::tableName() . '.updated_at'
            ])
            ->joinWith(['country', 'product'])
            ->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        if (!$this->load($params) || !$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([Certificate::tableName() . '.id' => $this->id]);
        //filter panel
        $query->andFilterWhere(['LIKE', Certificate::tableName() . '.name', $this->name]);
        $query->andFilterWhere([Certificate::tableName() . '.product_id' => $this->product_id]);
        $query->andFilterWhere([Certificate::tableName() . '.country_id' => $this->country_id]);
        $query->andFilterWhere([Certificate::tableName() . '.country_id' => array_keys(User::getAllowCountries())]);

        return $dataProvider;
    }

    /**
     * @param integer
     * @return \yii\db\ActiveRecord[]
     * Получить спиок товаров имеющих сертификаты по конкретной стране
     * партнёрский или собственный сертификат
     */
    public static function getCertifiedProducts($country_id)
    {
        $query = Certificate::find()
            ->select([
                'certificatename' => Certificate::tableName() . '.name',
                'name' => Product::tableName() . '.name',
                'id' => Product::tableName() . '.id',
                'is_partner' => Certificate::tableName() . '.is_partner',
                'certificate_not_needed' => Certificate::tableName() . '.certificate_not_needed'
            ])
            ->joinWith(['product'])
            ->where(['country_id' => $country_id]);

        return $query->all();
    }

    /**
     * @param integer
     * @return \yii\db\ActiveRecord[]
     * Получить список товаров из заказов, не имеющих сертификаты
     */
    public static function getNoCertifiedProducts($country_id)
    {
        $subQuery = Certificate::find()
            ->select([
                Certificate::tableName() . '.product_id'
            ])
            ->where([Certificate::tableName() . '.country_id' => $country_id]);

        $query = Product::find()
            ->leftJoin(OrderProduct::tableName(), '`order_product`.`product_id` = `product`.`id`')
            ->leftJoin(Order::tableName(), '`order`.`id` = `order_product`.`order_id`')
            ->where([
                'and',
                [Order::tableName() . '.country_id' => $country_id],
                ['not in', Product::tableName() . '.id', $subQuery]
            ])
            ->groupBy(Product::tableName() . '.id');

        return $query->all();

    }
}