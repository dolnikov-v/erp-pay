<?php
namespace app\models\search;

use app\models\Country;
use yii\data\ActiveDataProvider;

/**
 * Class CountrySearch
 * @package app\models\search
 */
class CountrySearch extends Country
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name' , 'char_code', 'is_partner'], 'safe']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['active'] = SORT_DESC;
            $sort['id'] = SORT_ASC;
        }

        $query = Country::find()
            ->with(['timezone', 'currency'])
            ->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        if (!$this->load($params) || !$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'name', $this->name]);
        $query->andFilterWhere(['char_code' => $this->char_code]);
        $query->andFilterWhere(['is_partner' => $this->is_partner]);

        return $dataProvider;
    }
}
