<?php
namespace app\models\search;

use yii\helpers\Html;
use app\models\CertificateFile;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * Class CountrySearch
 * @package app\models\search
 */
class CertificateFileSearch extends CertificateFile
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['id'] = SORT_ASC;
        }

        $query = CertificateFile::find()
            ->select([
                'id' => CertificateFile::tableName() . '.id',
                'origfilename' => CertificateFile::tableName() . '.origfilename',
                'unicfilename' => CertificateFile::tableName() . '.unicfilename',
                'created_at' => CertificateFile::tableName() . '.created_at'
            ])
            ->joinWith(['certificate'])
            ->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        if (!$this->load($params) || !$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([CertificateFile::tableName() . '.product_certificate_id' => $params['CertificateFileSearch']['product_certificate_id']]);

        return $dataProvider;
    }


    /**
     * @param array $htmlOptions
     * @return string
     *
     */
    public function downloadLink($htmlOptions = array())
    {
        return Html::a($this->origfilename, array(yii::getAlias('@certificatesFolder'), 'id' => $this->id), $htmlOptions);
    }
}