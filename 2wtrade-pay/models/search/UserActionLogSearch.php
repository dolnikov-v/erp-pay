<?php
namespace app\models\search;

use app\models\UserActionLog;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * Class ActionLogSearch
 * @property string $dateFrom
 * @property string $dateTo
 * @package app\models
 */
class UserActionLogSearch extends UserActionLog
{
    public $dateFrom;
    public $dateTo;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['url', 'dateFrom', 'dateTo'], 'safe']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['created_at'] = SORT_DESC;
        }

        $query = UserActionLog::find()->orderBy($sort);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['user_id' => $this->user_id]);
        $query->andFilterWhere(['like', 'url', $this->url]);

        if ($this->dateFrom && $dateFrom = Yii::$app->formatter->asTimestamp($this->dateFrom)) {
            $query->andFilterWhere(['>=', 'created_at', $dateFrom]);
        }

        if ($this->dateTo && $dateTo = Yii::$app->formatter->asTimestamp($this->dateTo)) {
            $query->andFilterWhere(['<=', 'created_at', $dateTo + 86399]);
        }

        return $dataProvider;
    }
}
