<?php
namespace app\models\search;

use app\models\Currency;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * Class CurrencySearch
 * @package app\models\search
 */
class CurrencySearch extends Currency
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['id'] = SORT_ASC;
        }

        $query = Currency::find()->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        if (!$this->load($params) || !$this->validate()) {
            return $dataProvider;
        }

        return $dataProvider;
    }
}
