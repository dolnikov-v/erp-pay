<?php
namespace app\models\search;

use app\models\Landing;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * Class LandingSearch
 * @package app\models\search
 */
class LandingSearch extends Landing
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['url', 'safe'],
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['id'] = SORT_ASC;
        }

        $query = Landing::find()->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        if (!$this->load($params) || !$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'url', $this->url]);

        return $dataProvider;
    }
}
