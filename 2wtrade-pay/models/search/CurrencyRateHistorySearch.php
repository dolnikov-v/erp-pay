<?php

namespace app\models\search;

use app\models\CurrencyRateHistory;
use yii\data\ActiveDataProvider;

/**
 * Class CurrencyRateHistorySearch
 * @package app\models\search
 */
class CurrencyRateHistorySearch extends CurrencyRateHistory
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['currency_id', 'integer'],
            ['created_at', 'string'],
        ];
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $sort = [];
        $sort['created_at'] = SORT_DESC;
        if (isset($params['sort'])) {
            $sort = $params['sort'];
            if (strpos($sort, "-") === 0) {
                $sort = substr($sort, 1);
            }
        }
        $query = CurrencyRateHistory::find()->orderBy($sort);
        if ($this->load($params) && $this->validate()) {
            if (!empty($this->created_at)) {
                $query->andFilterWhere(['between', 'created_at', \Yii::$app->formatter->asTimestamp($this->created_at), \Yii::$app->formatter->asTimestamp($this->created_at) + 86399]);
            }
            $query->andFilterWhere(['currency_id' => $this->currency_id]);
        }

        return new ActiveDataProvider([
            'query' => $query
        ]);
    }
}