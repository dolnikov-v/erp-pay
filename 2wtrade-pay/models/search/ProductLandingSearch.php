<?php
namespace app\models\search;

use app\models\ProductLanding;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * Class ProductLandingSearch
 * @package app\models\search
 */
class ProductLandingSearch extends ProductLanding
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'landing_id'], 'safe'],
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['id'] = SORT_ASC;
        }

        $query = ProductLanding::find()->orderBy($sort);

        $query->joinWith([
            'product',
            'landing',
        ]);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        if (!$this->load($params) || !$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['product_id' => $this->product_id]);
        $query->andFilterWhere(['landing_id' => $this->landing_id]);

        return $dataProvider;
    }
}
