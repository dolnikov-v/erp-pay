<?php

namespace app\models;

use Yii;
use app\components\db\ActiveRecordLogUpdateTime;

/**
 * Class TicketGroup
 *
 * @property string $prefix
 * @property string $name
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property TicketRole[] $ticketRoles
 */
class TicketGroup extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ticket_group}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['prefix', 'name'], 'required'],
            [['created_at', 'updated_at'], 'integer'],
            [['prefix', 'name'], 'string', 'max' => 255],
            [['prefix'], 'unique'],
            [['created_at', 'updated_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'prefix' => Yii::t('common', 'Префикс'),
            'name' => Yii::t('common', 'Группа'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTicketRoles()
    {
        return $this->hasMany(TicketRole::className(), ['ticket_prefix' => 'prefix']);
    }
}
