<?php

namespace app\models\logs;


use app\components\base\Model;

/**
 * Class DeleteData
 * @package app\models\logs
 */
class DeleteData extends Model
{
    public $field, $value;
    public function rules()
    {
        return [
            [['field', 'value'], 'required'],
            [['field', 'value'], 'string'],
        ];
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return ['field', 'value'];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'field' => \Yii::t('common', 'Поле'),
            'value' => \Yii::t('common', 'Значение'),
        ];
    }
}