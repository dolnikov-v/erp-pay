<?php

namespace app\models\logs;

use yii\base\Model;
use Yii;

class UpdateData extends Model
{
    public $field, $old, $new;
    public function rules()
    {
        return [
            [['field', 'new'], 'required'],
            [['field', 'new', 'old'], 'string'],
        ];
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return ['field', 'old', 'new'];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'field' => Yii::t('common', 'Поле'),
            'old' => Yii::t('common', 'Старое значение'),
            'new' => Yii::t('common', 'Новое значение'),
        ];
    }
}