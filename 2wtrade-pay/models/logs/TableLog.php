<?php

namespace app\models\logs;

use app\components\grid\GridView;
use app\components\mongodb\ActiveRecord;
use app\models\logs\query\TableLogActiveQuery;
use app\models\User;
use Yii;

/**
 * Class TableLog
 * @package app\components\mongodb
 *
 * @property string $table
 * @property string $route
 * @property integer $user_id
 * @property string|int $id
 * @property string|int $parent_id
 * @property string $type
 * @property string $sessionKey
 * @property User $user
 * @property UpdateData[]|InsertData[]|DeleteData[] $data
 * @property LinkData[]|null $links
 * @property null $linkContent
 */
class TableLog extends ActiveRecord
{
    const TYPE_INSERT = 'insert';
    const TYPE_UPDATE = 'update';
    const TYPE_DELETE = 'delete';

    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return 'table_log';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['table', 'type'], 'required'],
            [['table', 'route', 'key'], 'string'],
            [['user_id'], 'integer'],
            ['type', 'in', 'range' => [self::TYPE_INSERT, self::TYPE_UPDATE, self::TYPE_DELETE]],
            ['data', 'isData'],
            ['links', 'isLinkData'],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return ['_id', 'id', 'table', 'type', 'created_at', 'data', 'links', 'route', 'user_id', 'key', 'parent_id'];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер объекта'),
            'table' => Yii::t('common', 'Таблица'),
            'route' => Yii::t('common', 'Маршрут'),
            'type' => Yii::t('common', 'Тип записи'),
            'user_id' => Yii::t('common', 'Пользователь'),
            'data' => Yii::t('common', 'Измененные данные'),
            'links' => Yii::t('common', 'Ссылки'),
            'created_at' => Yii::t('common', 'Время записи'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        if (!empty($this->table)) {
            $this->table = trim($this->table, "\{\}\%");
        }
        return parent::beforeValidate();
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (isset(Yii::$app->user)) {
            $this->setAttribute('user_id', Yii::$app->user->id);
        }
        $this->setAttribute('key', $this->getSessionKey());
        return parent::beforeSave($insert);
    }

    /**
     * @return array
     */
    public function dataAttributes()
    {
        if ($this->type == self::TYPE_UPDATE) {
            return UpdateData::attributes();
        } elseif ($this->type == self::TYPE_INSERT) {
            return InsertData::attributes();
        } elseif ($this->type == self::TYPE_DELETE) {
            return DeleteData::attributes();
        }
        return [];
    }

    /**
     * @param $attr
     */
    public function isData($attr)
    {
        if ($this->$attr && !is_array($this->$attr)) {
            $this->addError($attr, \Yii::t('common', 'Неверный формат данных.'));
        } else {
            foreach ($this->$attr as $data) {
                if (!(($this->type == self::TYPE_UPDATE && $data instanceof UpdateData) || ($this->type == self::TYPE_INSERT && $data instanceof InsertData) || ($this->type == self::TYPE_DELETE && $data instanceof DeleteData))) {
                    $this->addError($attr, \Yii::t('common', 'Неверный формат данных.'));
                }
            }
        }
    }

    /**
     * @param $attr
     */
    public function isLinkData($attr)
    {
        if ($this->$attr && !is_array($this->$attr)) {
            $this->addError($attr, \Yii::t('common', 'Неверный формат данных.'));
        } else {
            foreach ($this->$attr as $data) {
                if (!($data instanceof LinkData)) {
                    $this->addError($attr, \Yii::t('common', 'Неверный формат данных.'));
                }
            }
        }
    }

    /**
     * prepare attribute 'data' to objects array
     */
    public function prepare()
    {
        if ($this->data && is_array($this->data)) {
            $result = $this->data;
            foreach ($this->data as $key => $data) {
                if (is_array($data)) {
                    if ($this->type == self::TYPE_INSERT) {
                        $result[$key] = new InsertData($data);
                    } elseif ($this->type == self::TYPE_UPDATE) {
                        $result[$key] = new UpdateData($data);
                    } elseif($this->type == self::TYPE_DELETE) {
                        $result[$key] = new DeleteData($data);
                    }
                }
            }
            $this->data = $result;
        }
        if ($this->links && is_array($this->links)) {
            $result = $this->links;
            foreach ($this->links as $key => $data) {
                if (is_array($data)) {
                    $result[$key] = new LinkData($data);
                }
            }
            $this->links = $result;
        }
    }

    /**
     * @return TableLogActiveQuery
     */
    public static function find()
    {
        return new TableLogActiveQuery(get_called_class());
    }
    /**
     * @return User|null
     */
    public function getUser()
    {
        if ($this->user_id) {
            return User::findOne(['id' => $this->user_id]);
        }
        return null;
    }

    /**
     * @return null|string
     * @throws \Exception
     */
    protected function getLinkContent(): ?string
    {
        $return = '';
        $this->prepare();
        if (!empty($this->links) && is_array($this->links)) {
            $provider = new \yii\data\ArrayDataProvider([
                'allModels' => $this->links,
            ]);
            $return = GridView::widget([
                'tableOptions' => [
                    'style' => 'padding-top: 0;',
                ],
                'dataProvider' => $provider,
            ]);
        }
        return $return;
    }
}