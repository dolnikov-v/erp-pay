<?php

namespace app\models\logs;

use yii\base\Model;
use Yii;

class InsertData extends Model
{
    public $field, $new;
    public function rules()
    {
        return [
            [['field', 'new'], 'required'],
            [['field', 'new'], 'string'],
        ];
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return ['field', 'new'];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'field' => Yii::t('common', 'Поле'),
            'new' => Yii::t('common', 'Значение'),
        ];
    }
}