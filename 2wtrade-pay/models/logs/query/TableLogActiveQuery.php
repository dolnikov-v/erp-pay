<?php
namespace app\models\logs\query;

use yii\mongodb\ActiveQuery;

/**
 * Class TableLogActiveQuery
 * @package app\models\logs\query
 */
class TableLogActiveQuery extends ActiveQuery
{
    /**
     * @param string $tableName
     * @return TableLogActiveQuery
     */
    public function byTable(string $tableName)
    {
        return $this->andWhere(['table' => $tableName]);
    }

    /**
     * @param $id
     * @return TableLogActiveQuery
     */
    public function byID($id)
    {
        return $this->andWhere(['id' => $id]);
    }

    /**
     * @param string $field
     * @param $value
     * @return TableLogActiveQuery
     */
    public function byLink(string $field, $value)
    {
        return $this->andWhere(['links.field' => $field, 'links.value' => $value]);
    }

    /**
     * @return \app\models\logs\TableLog[]|null
     */
    public function allSorted()
    {
        return $this->orderBy(['created_at' => SORT_DESC])->all();
    }
}