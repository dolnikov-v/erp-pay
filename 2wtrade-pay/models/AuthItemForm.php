<?php
namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Class AuthItemForm
 * @package app\models
 */
class AuthItemForm extends Model
{
    public $name;
    public $description;

    /**
     * @param string $itemName
     * @return bool
     */
    public static function checkRelation($itemName)
    {
        return AuthAssignment::find()
            ->where(['item_name' => $itemName])
            ->exists();
    }

    /**
     * @param $name
     * @param $description
     * @return \yii\rbac\Permission
     */
    public static function addPermission($name, $description)
    {
        $permission = Yii::$app->authManager->createPermission($name);
        $permission->description = $description;
        Yii::$app->authManager->add($permission);

        return $permission;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['name', 'required', 'on' => 'insert'],
            ['name', 'string', 'max' => '64', 'message' => Yii::t('common', 'Максимальная длина поля «Название» составаляет 64 символа.'), 'on' => 'insert'],
            ['name', 'validateName', 'on' => 'insert'],

            ['description', 'required', 'on' => ['insert', 'update']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('common', 'Название'),
            'description' => Yii::t('common', 'Описание'),
        ];
    }

    /**
     * @param string $attribute
     * @param array $params
     */
    public function validateName($attribute, $params)
    {
        if (empty($this->name)) {
            $this->addError($attribute, Yii::t('common', 'Необходимо указать «Название».'));
        } else {
            $this->name = mb_strtolower($this->name);

            $role = Yii::$app->authManager->getRole($this->name);
            $permission = Yii::$app->authManager->getPermission($this->name);

            if (!empty($role) || !empty($permission)) {
                $this->addError($attribute, Yii::t('common', 'Запись с таким названием уже существует.'));
            } else {
                if (!preg_match("/^[a-z0-9\.]+$/", $this->name)) {
                    $this->addError($attribute, Yii::t('common', 'Поле «Название» должно состоять только из букв латинского алфавита и символа точка.'));
                }

                if (preg_match("/^\./", $this->name)) {
                    $this->addError($attribute, Yii::t('common', 'Поле «Название» не должно начинаться с символа точка.'));
                }
            }
        }
    }
}
