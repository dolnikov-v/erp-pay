<?php
namespace app\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\query\UserSourceQuery;
use Yii;

/**
 * Class UserSource
 * @package app\models
 * @property integer $id
 * @property integer $user_id
 * @property integer $source_id
 * @property integer $active
 * @property integer $created_at
 * @property integer $updated_at
 * @property User $user
 * @property Source $source
 */
class UserSource extends ActiveRecordLogUpdateTime
{
    protected $insertLog = true;
    protected $updateLog = true;
    protected $deleteLog = true;

    protected function getLogID()
    {
        return $this->user_id;
    }

    /**
     * @inheritdoc
     */
    protected function prepareAttributesUpdate(?array $changedAttributes, bool $compare = true): ?array
    {
        if (!isset($changedAttributes['source_id'])) {
            $changedAttributes['source_id'] = $this->source_id;
        }
        return parent::prepareAttributesUpdate($changedAttributes, false);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_source}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'source_id'], 'required'],

            [
                'user_id',
                'exist',
                'targetClass' => '\app\models\User',
                'targetAttribute' => 'id',
                'message' => Yii::t('common', 'Неверное значение пользователя.')
            ],

            [
                'source_id',
                'exist',
                'targetClass' => Source::class,
                'targetAttribute' => 'id',
                'message' => Yii::t('common', 'Неверное значение источника.')
            ],

            ['active', 'default', 'value' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'user_id' => Yii::t('common', 'Пользователь'),
            'source_id' => Yii::t('common', 'Источник'),
        ];
    }

    /**
     * @return UserSourceQuery
     */
    public static function find()
    {
        return new UserSourceQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSource()
    {
        return $this->hasOne(Source::class, ['id' => 'source_id']);
    }
}
