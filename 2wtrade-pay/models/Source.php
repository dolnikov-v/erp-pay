<?php

namespace app\models;

use app\components\db\abstracts\ConnectionInterface;
use app\components\db\ActiveRecordLogUpdateTime;
use app\components\db\models\DirtyData;
use app\models\query\SourceQuery;
use app\modules\finance\models\ReportExpensesCountry;
use app\modules\finance\models\ReportExpensesCountryItemProduct;
use app\modules\finance\models\ReportExpensesItem;
use app\modules\order\models\Lead;
use app\modules\order\models\Order;
use Yii;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

/**
 * Class Source
 * @package app\models
 * @property integer $id
 * @property integer $active
 * @property string $name
 * @property string $unique_system_name
 * @property string $default_call_center_type
 * @property int $use_pseudo_approve
 * @property int $created_at
 * @property int $updated_at
 *
 * @property SourceCountry[] $sourceCountries
 * @property Country[] $countries
 * @property Product[] $products
 * @property SourceProduct[] $sourceProducts
 * @property SourceProduct $sourceProduct
 */
class Source extends ActiveRecordLogUpdateTime
{
    const SOURCE_ADCOMBO = 'adcombo';
    const SOURCE_JEEMPO = 'jeempo';
    const SOURCE_2WSTORE = '2wstore';
    const SOURCE_DIAVITA = 'diavita';
    const SOURCE_WTRADE_TRASH = 'wtrade_trash';
    const SOURCE_2WCALL = '2wcall';
    const SOURCE_2WDISTR = '2wDistr';
    const SOURCE_WTRADE_POST_SALE = 'wtrade_post_sale';
    const SOURCE_MERCADOLIBRE = 'mercadolibre';
    const SOURCE_CALL_CENTER = 'call_center';

    const SOURCE_TEST_1 = 'test_1';
    const SOURCE_MY_EPC = 'my_epc';
    const SOURCE_TEST_3 = 'test_3';
    const SOURCE_TEST_4 = 'test_4';
    const SOURCE_TEST_5 = 'test_5';

    const SOURCE_TOPHOT = 'tophot';


    const DEFAULT_GUARANTEE_OFFSET = 5;

    /**
     * Список системных источников, которые можно привязать к источникам из БД, чтобы можно было их находить в коде по этим константам
     * @return array
     */
    public static function systemSourceList()
    {
        return [
            static::SOURCE_ADCOMBO,
            static::SOURCE_JEEMPO,
            static::SOURCE_2WSTORE,
            static::SOURCE_DIAVITA,
            static::SOURCE_WTRADE_TRASH,
            static::SOURCE_2WCALL,
            static::SOURCE_2WDISTR,
            static::SOURCE_WTRADE_POST_SALE,
            static::SOURCE_MERCADOLIBRE,
            static::SOURCE_CALL_CENTER,
            static::SOURCE_TEST_1,
            static::SOURCE_MY_EPC,
            static::SOURCE_TEST_3,
            static::SOURCE_TEST_4,
            static::SOURCE_TEST_5,
            static::SOURCE_TOPHOT,
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%source}}';
    }


    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 64],
            ['default_call_center_type', 'string', 'max' => 50],
            ['unique_system_name', 'default', 'value' => null],
            ['unique_system_name', 'string', 'max' => 32],
            [['name', 'unique_system_name'], 'unique'],
            [['active'], 'integer'],
            ['use_pseudo_approve', 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Индекс'),
            'name' => Yii::t('common', 'Наименование источника'),
            'active' => Yii::t('common', 'Активность'),
            'use_pseudo_approve' => Yii::t('common', 'Использовать ложный апрув'),
            'default_call_center_type' => Yii::t('common', 'Очередь в КЦ по умолчанию'),
            'unique_system_name' => Yii::t('common', 'Уникальное имя в системе'),
        ];
    }

    /**
     * @return SourceQuery
     */
    public static function find()
    {
        return new SourceQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSourceCountries()
    {
        return $this->hasMany(SourceCountry::className(), ['source_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountries()
    {
        return $this->hasMany(Country::className(), ['id' => 'country_id'])
            ->where(['active' => 1])
            ->via('sourceCountries');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSourceProducts()
    {
        return $this->hasMany(SourceProduct::className(), ['source_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSourceProduct()
    {
        return $this->hasMany(SourceProduct::className(), ['source_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['id' => 'product_id'])
            ->via('sourceProducts');
    }

    /**
     * Подтасовка апрува
     * @param Country|integer $country
     * @param int $guaranteeOffset
     * @param string $timezone
     * @throws \Exception
     */
    public function setPseudoApproveForPreviousDay($country, $guaranteeOffset = self::DEFAULT_GUARANTEE_OFFSET, $timezone = 'Europe/Moscow')
    {

        if (!($country instanceof Country)) {
            $country = Country::findOne($country);
        }

        if (!$this->use_pseudo_approve) {
            return;
        }

        //Теперь всевдоапрув не только по стране, но и по товарам.
        //По стране - дефолтно
        if (!empty($country->critical_approve_percent)) {
            $this->setPseudoApproveForPreviousDayByCountry($country);
        }

        //если по стране не выставлено, а по товарам указано
        if (!empty($country->productCriticalApproves) && empty($country->critical_approve_percent)) {
            $this->setPseudoApproveForPreviousDayByProducts($country);
        }
    }

    /**
     * Псевдо апрувы по товарам
     * @param $country
     * @param int $guaranteeOffset
     * @param string $timezone
     */
    public function setPseudoApproveForPreviousDayByProducts($country, $guaranteeOffset = self::DEFAULT_GUARANTEE_OFFSET, $timezone = 'Europe/Moscow')
    {
        $pseudoApproveByProducts = ArrayHelper::map($country->productCriticalApproves, 'product_id', 'percent');

        foreach ($pseudoApproveByProducts as $critical_product_id => $critical_percent) {
            $productPercents = ArrayHelper::map(ReportExpensesCountryItemProduct::find()
                ->joinWith(['parent.parent', 'parent.country'], false)
                ->where([
                    ReportExpensesCountry::tableName() . '.country_id' => $country->id,
                    ReportExpensesItem::tableName() . '.code' => 'adcombo_gtd_approve_percent',
                    ReportExpensesCountryItemProduct::tableName() . '.product_id' => $critical_product_id
                ])->all(), 'product_id', 'value');

            foreach ($productPercents as $productId => $guaranteePercent) {
                $leadDataInfo = $this->getApproveQuantityForPreviousDay($country->id, $productId, $timezone);

                if (empty($leadDataInfo['common_quantity'])) {
                    continue;
                }

                $approvedPercent = $leadDataInfo['approve_quantity'] / $leadDataInfo['common_quantity'] * 100;


                if ($approvedPercent > $critical_percent) {
                    continue;
                }

                $query = $this->prepareQueryForGetLeadsFromPreviousDay($country->id, $productId, Lead::inProgressStatus(), $timezone);
                $query->select([Lead::tableName() . '.foreign_id']);

                foreach ($query->batch(500) as $leads) {
                    $stop = false;
                    $leadIds = [];
                    foreach ($leads as $lead) {
                        $approvedPercent = $leadDataInfo['approve_quantity'] / $leadDataInfo['common_quantity'] * 100;

                        if ($approvedPercent >= ($guaranteePercent - $guaranteeOffset)) {
                            $stop = true;
                            break;
                        }

                        $leadIds[] = $lead->foreign_id;
                        $leadDataInfo['approve_quantity']++;
                    }

                    if (!empty($leadIds)) {
                        Lead::updateAll(['source_status' => Lead::STATUS_CONFIRMED], [
                            'source_id' => $this->id,
                            'foreign_id' => $leadIds
                        ]);
                    }

                    if ($stop) {
                        break;
                    }
                }
            }
        }
    }

    /**
     * Псевдо апрувы по стране
     * @param $country
     * @param int $guaranteeOffset
     * @param string $timezone
     */
    public function setPseudoApproveForPreviousDayByCountry($country, $guaranteeOffset = self::DEFAULT_GUARANTEE_OFFSET, $timezone = 'Europe/Moscow')
    {
        $productPercents = ArrayHelper::map(ReportExpensesCountryItemProduct::find()
            ->joinWith(['parent.parent', 'parent.country'], false)
            ->where([
                ReportExpensesCountry::tableName() . '.country_id' => $country->id,
                ReportExpensesItem::tableName() . '.code' => 'adcombo_gtd_approve_percent',
            ])->all(), 'product_id', 'value');

        foreach ($productPercents as $productId => $guaranteePercent) {

            $leadDataInfo = $this->getApproveQuantityForPreviousDay($country->id, $productId, $timezone);

            if (empty($leadDataInfo['common_quantity'])) {
                continue;
            }

            $approvedPercent = $leadDataInfo['approve_quantity'] / $leadDataInfo['common_quantity'] * 100;

            if ($approvedPercent > $country->critical_approve_percent) {
                continue;
            }

            $query = $this->prepareQueryForGetLeadsFromPreviousDay($country->id, $productId, Lead::inProgressStatus(), $timezone);
            $query->select([Lead::tableName() . '.foreign_id']);

            foreach ($query->batch(500) as $leads) {
                $stop = false;
                $leadIds = [];
                foreach ($leads as $lead) {
                    /** @var Lead $lead */
                    $approvedPercent = $leadDataInfo['approve_quantity'] / $leadDataInfo['common_quantity'] * 100;
                    if ($approvedPercent >= ($guaranteePercent - $guaranteeOffset)) {
                        $stop = true;
                        break;
                    }

                    $leadIds[] = $lead->foreign_id;
                    $leadDataInfo['approve_quantity']++;
                }
                if (!empty($leadIds)) {
                    Lead::updateAll(['source_status' => Lead::STATUS_CONFIRMED], [
                        'source_id' => $this->id,
                        'foreign_id' => $leadIds
                    ]);
                }
                if ($stop) {
                    break;
                }
            }
        }
    }

    /**
     * @param null $countryId
     * @param integer|array $productIds
     * @param array $statuses
     * @param null $timezone
     * @return ActiveQuery
     */
    protected function prepareQueryForGetLeadsFromPreviousDay($countryId = null, $productIds = null, $statuses = [], $timezone = null)
    {
        $dateTime = new \DateTime('now', $timezone ? new \DateTimeZone($timezone) : null);
        $dateTime->setTime(0, 0, 0);

        $timeTo = $dateTime->getTimestamp();
        $timeFrom = $dateTime->sub(\DateInterval::createFromDateString('1day'))->getTimestamp();
        $query = Lead::find()->where([Lead::tableName() . '.source_id' => $this->id])
            ->andWhere(['>=', Lead::tableName() . '.ts_spawn', $timeFrom])
            ->andWhere(['<', Lead::tableName() . '.ts_spawn', $timeTo]);

        if ($countryId) {
            $query->joinWith(['order'], false)->andWhere([Order::tableName() . '.country_id' => $countryId]);
        }

        if ($productIds) {
            $query->andWhere([Lead::tableName() . '.goods_id' => $productIds]);
        }

        if (!empty($statuses)) {
            $query->andWhere([Lead::tableName() . '.source_status' => $statuses]);
        }

        return $query;
    }

    /**
     * @param integer $countryId
     * @param integer|array $productIds
     * @param string $timezone
     * @return array
     */
    protected function getApproveQuantityForPreviousDay($countryId, $productIds = null, $timezone = null)
    {
        $query = $this->prepareQueryForGetLeadsFromPreviousDay($countryId, $productIds, [], $timezone);
        $query->select([
            'common_quantity' => 'COUNT(' . Lead::tableName() . '.order_id)',
            'approve_quantity' => 'COUNT(' . Lead::tableName() . '.source_status = \'' . Lead::STATUS_CONFIRMED . '\' OR NULL)'
        ]);

        $data = $query->createCommand()->queryOne();

        return $data;
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        if (static::getDb() instanceof ConnectionInterface) {
            $changedAttributes['id'] = $this->id;
            static::getDb()->addBufferChange(new DirtyData([
                'id' => static::clearTableName(),
                'action' => $insert ? DirtyData::ACTION_INSERT : DirtyData::ACTION_UPDATE,
                'data' => $changedAttributes
            ]));
        }

        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        $data = ['id' => $this->id];
        if (static::getDb() instanceof ConnectionInterface) {
            static::getDb()->addBufferChange(new DirtyData([
                'id' => static::clearTableName(),
                'action' => DirtyData::ACTION_DELETE,
                'data' => $data
            ]));
        }
        return parent::afterDelete();
    }
}