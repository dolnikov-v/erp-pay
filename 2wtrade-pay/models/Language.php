<?php

namespace app\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\query\LanguageQuery;
use Yii;

/**
 * This is the model class for table "language".
 *
 * @property integer $id
 * @property string $name
 * @property string $locale
 * @property integer $active
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Country[] $countries
 * @property ProductTranslation[] $productTranslations
 */
class Language extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%language}}';
    }

    /**
     * @return LanguageQuery
     */
    public static function find()
    {
        return new LanguageQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'locale'], 'required'],
            [['active', 'created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 255],
            ['locale', 'string', 'max' => 5, 'tooLong' => Yii::t('common', 'Слишком длинный код.')],
            [['locale'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'name' => Yii::t('common', 'Название'),
            'locale' => Yii::t('common', 'Локаль'),
            'active' => Yii::t('common', 'Активность'),
            'created_at' => Yii::t('common', 'Дата добавления'),
            'updated_at' => Yii::t('common', 'Дата обновления'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountries()
    {
        return $this->hasMany(Country::className(), ['language_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductTranslations()
    {
        return $this->hasMany(ProductTranslation::className(), ['language_id' => 'id']);
    }
}
