<?php
namespace app\models\query;

use app\components\db\ActiveQuery;
use app\models\Landing;

/**
 * Class LandingQuery
 * @package app\models\query
 * @method Landing one($db = null)
 * @method Landing[] all($db = null)
 */
class LandingQuery extends ActiveQuery
{
    /**
     * @var string
     */
    protected $collectionValue = 'url';

    /**
     * @param string $url
     * @param boolean $prepare
     * @return $this
     */
    public function byUrl($url, $prepare = true)
    {
        if ($prepare) {
            $url = Landing::prepareUrl($url);
        }

        return $this->andWhere(['url' => $url]);
    }
}
