<?php
namespace app\models\query;

use app\models\api\ApiUser;
use yii\db\ActiveQuery;

/**
 * Class ApiUserQuery
 * @package app\models\query
 * @method ApiUser one($db = null)
 * @method ApiUser[] all($db = null)
 */
class ApiUserQuery extends ActiveQuery
{
    /**
     * @param $username
     * @return $this
     */
    public function byUsername($username)
    {
        return $this->andWhere(['username' => $username]);
    }
}
