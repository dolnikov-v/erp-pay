<?php
namespace app\models\query;

use yii\db\ActiveQuery;

/**
 * Class UserDeliveryQuery
 * @package app\models\query
 */
class UserDeliveryQuery extends ActiveQuery
{
    /**
     * @param $id
     * @return $this
     */
    public function byUserId($id)
    {
        return $this->andWhere(['user_id' => $id]);
    }

    /**
     * @param null $db
     * @return array|\app\models\UserDelivery[]
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @param null $db
     * @return array|null|\app\models\UserDelivery
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
