<?php
namespace app\models\query;

use app\components\db\ActiveQuery;
use app\models\Product;
use yii\helpers\ArrayHelper;

/**
 * Class ProductQuery
 * @package app\models\query
 * @method Product one($db = null)
 * @method Product[] all($db = null)
 */
class ProductQuery extends ActiveQuery
{
    /**
     * @param null $language
     * @return array
     */
    public function collection($language = null)
    {
        return ArrayHelper::map($this->all(), $this->collectionKey, $this->collectionValue);
    }
}
