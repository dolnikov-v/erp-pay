<?php
namespace app\models\query;

use yii\db\ActiveQuery;

/**
 * Class UserActionLogQueryQuery
 * @package app\models\query
 */
class UserActionLogQueryQuery extends ActiveQuery
{
    /**
     * @param $id
     * @return $this
     */
    public function byActionLogId($id)
    {
        return $this->andWhere(['action_log_id' => $id]);
    }

    /**
     * @param null $db
     * @return array|\app\models\UserActionLogQuery[]
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @param null $db
     * @return array|null|\app\models\UserActionLogQuery
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
