<?php
namespace app\models\query;

use app\components\db\ActiveQuery;
use app\models\Language;

/**
 * Class LanguageQuery
 * @package app\models\query
 * @method Language one($db = null)
 * @method Language[] all($db = null)
 */
class LanguageQuery extends ActiveQuery
{
    /**
     * @param string $locale
     * @return $this
     */
    public function byLocale($locale)
    {
        return $this->andWhere(['locale' => $locale]);
    }
}
