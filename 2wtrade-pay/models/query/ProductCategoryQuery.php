<?php
namespace app\models\query;

use app\components\db\ActiveQuery;
use app\models\ProductCategory;
use yii\helpers\ArrayHelper;

/**
 * Class ProductCategoryQuery
 * @package app\models\query
 * @method ProductCategory one($db = null)
 * @method ProductCategory[] all($db = null)
 */
class ProductCategoryQuery extends ActiveQuery
{
    /**
     * @param null $language
     * @return array
     */
    public function collection($language = null)
    {
        return ArrayHelper::map($this->all(), $this->collectionKey, $this->collectionValue);
    }
}
