<?php
namespace app\models\query;

/**
 * This is the ActiveQuery class for [[\app\models\PartnerCountry]].
 *
 * @see \app\models\PartnerCountry
 */
class PartnerCountryQuery extends \yii\db\ActiveQuery
{
    /**
     * @inheritdoc
     * @return \app\models\PartnerCountry[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\models\PartnerCountry|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
