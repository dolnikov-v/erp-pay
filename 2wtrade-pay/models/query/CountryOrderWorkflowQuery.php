<?php
namespace app\models\query;

use app\components\db\ActiveQuery;
use app\models\CountryOrderWorkflow;

/**
 * Class CountryOrderWorkflowQuery
 * @package app\models\query
 * @method CountryOrderWorkflow one($db = null)
 * @method CountryOrderWorkflow[] all($db = null)
 */
class CountryOrderWorkflowQuery extends ActiveQuery
{
    /**
     * @param integer $countryId
     * @return $this
     */
    public function byCountryId($countryId)
    {
        return $this->andWhere(['country_id' => $countryId]);
    }

    /**
     * @param integer $workflowId
     * @return $this
     */
    public function byWorkflowId($workflowId)
    {
        return $this->andWhere(['workflow_id' => $workflowId]);
    }
}
