<?php

namespace app\models\query;

use app\models\UserSource;
use yii\db\ActiveQuery;


/**
 * Class UserSourceQuery
 * @package app\models\query
 */
class UserSourceQuery extends ActiveQuery
{
    /**
     * @param $id
     * @return $this
     */
    public function byUserId($id)
    {
        return $this->andWhere(['user_id' => $id]);
    }

    /**
     * @param integer $sourceId
     * @return UserSourceQuery
     */
    public function bySourceId($sourceId)
    {
        return $this->andWhere([UserSource::tableName() . '.source_id' => $sourceId]);
    }

    /**
     * @param null $db
     * @return array|\app\models\UserSource[]
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @param null $db
     * @return array|null|\app\models\UserSource
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
