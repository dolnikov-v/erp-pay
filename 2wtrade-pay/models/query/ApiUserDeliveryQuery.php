<?php
namespace app\models\query;

use app\models\api\ApiUserDelivery;
use yii\db\ActiveQuery;

/**
 * Class ApiUserDeliveryQuery
 * @package app\models\query
 * @method ApiUserDelivery one($db = null)
 * @method ApiUserDelivery[] all($db = null)
 */
class ApiUserDeliveryQuery extends ActiveQuery
{

}
