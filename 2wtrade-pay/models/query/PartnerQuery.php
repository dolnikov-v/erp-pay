<?php
namespace app\models\query;

use app\components\db\ActiveQuery;
use app\models\Partner;

/**
 * Class PartnerQuery
 * @package app\models\query
 *
 * @see \app\models\Partner
 */
class PartnerQuery extends ActiveQuery
{
    /**
     * @inheritdoc
     * @return \app\models\Partner[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }


    /**
     * @return $this
     */

    public function def()
    {
        return $this->andWhere([Partner::tableName() . '.default' => 1]);
    }

    /**
     * @inheritdoc
     * @return \app\models\Partner|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
