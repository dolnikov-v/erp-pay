<?php
namespace app\models\query;

use app\components\db\ActiveQuery;
use app\models\ProductLanding;

/**
 * Class ProductLandingQuery
 * @package app\models\query
 * @method ProductLanding one() one($db = null)
 * @method ProductLanding[] all() all($db = null)
 */
class ProductLandingQuery extends ActiveQuery
{
    /**
     * @param $productId
     * @return $this
     */
    public function byProduct($productId)
    {
        return $this->andWhere(['product_id' => $productId]);
    }

    /**
     * @param $landingId
     * @return $this
     */
    public function byLanding($landingId)
    {
        return $this->andWhere(['landing_id' => $landingId]);
    }
}
