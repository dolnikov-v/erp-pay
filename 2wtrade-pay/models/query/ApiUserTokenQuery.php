<?php
namespace app\models\query;

use app\models\api\ApiUserToken;
use yii\db\ActiveQuery;

/**
 * Class ApiUserTokenQuery
 * @package app\models\query
 * @method ApiUserToken one($db = null)
 * @method ApiUserToken[] all($db = null)
 */
class ApiUserTokenQuery extends ActiveQuery
{
    /**
     * @param $token
     * @return $this
     */
    public function byToken($token)
    {
        return $this->andWhere(['auth_token' => $token]);
    }
}
