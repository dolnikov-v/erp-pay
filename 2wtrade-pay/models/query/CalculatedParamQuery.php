<?php
namespace app\models\query;

use app\components\db\ActiveQuery;
use app\models\CalculatedParam;

/**
 * Class CalculatedParamQuery
 * @package app\models\query
 */
class CalculatedParamQuery extends ActiveQuery
{
    /**
     * @inheritdoc
     * @return CalculatedParam[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return CalculatedParam|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}