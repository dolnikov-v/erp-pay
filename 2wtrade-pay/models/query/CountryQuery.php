<?php
namespace app\models\query;

use app\components\db\ActiveQuery;
use app\models\Country;
use app\models\User;

/**
 * Class CountryQuery
 * @package app\models\query
 *
 * @method Country one($db = null)
 * @method Country[] all($db = null)
 */
class CountryQuery extends ActiveQuery
{
    /**
     * @param $code
     * @return $this
     */
    public function byCharCode($code)
    {
        return $this->andWhere(['char_code' => $code]);
    }

    /**
     * @param $slug
     * @return $this
     */
    public function bySlug($slug)
    {
        return $this->andWhere(['slug' => $slug]);
    }

    /**
     * @return $this
     */
    public function active()
    {
        return $this->andWhere([Country::tableName() . '.active' => 1]);
    }

    /**
     * @return $this
     */
    public function notStopped()
    {
        return $this->andWhere([Country::tableName() . '.is_stop_list' => 0]);
    }

    /**
     * returns not partner countries
     * @return $this
     */
    public function own()
    {
        return $this->andWhere([Country::tableName() . '.is_partner' => 0]);
    }

    /**
     * @return $this
     */
    public function notActive()
    {
        return $this->andWhere([Country::tableName() . '.active' => 0]);
    }

    /**
     * @return $this
     */
    public function bySystemUserCountries()
    {
        return $this->andWhere(['id' => array_keys(User::getAllowCountries())]);
    }

    /**
     * @return $this
     */
    public function isPartner()
    {
        return $this->andWhere([Country::tableName() . '.is_partner' => 1]);
    }

    /**
     * @return $this
     */
    public function notPartner()
    {
        return $this->andWhere([Country::tableName() . '.is_partner' => 0]);
    }

    /**
     * @return $this
     */
    public function isDistributor()
    {
        return $this->andWhere([Country::tableName() . '.is_distributor' => Country::IS_DISTRIBUTOR]);
    }

    /**
     * @return $this
     */
    public function notDistributor()
    {
        return $this->andWhere([Country::tableName() . '.is_distributor' => Country::IS_NOT_DISTRIBUTOR]);
    }
}
