<?php

namespace app\models\query;

use app\components\db\ActiveQuery;
use app\models\CountrySmsWorkflow;

/**
 * Class CountrySmsWorkflowQuery
 * @package app\models\query
 * @method CountrySmsWorkflow one($db = null)
 * @method CountrySmsWorkflow[] all($db = null)
 */
class CountrySmsWorkflowQuery extends ActiveQuery
{
    /**
     * @param integer $countryId
     * @return $this
     */
    public function byCountryId($countryId)
    {
        return $this->andWhere(['country_id' => $countryId]);
    }

    /**
     * @param integer $workflowId
     * @return $this
     */
    public function byWorkflowId($workflowId)
    {
        return $this->andWhere(['sms_workflow_id' => $workflowId]);
    }
}
