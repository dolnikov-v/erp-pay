<?php

namespace app\models\query;

use app\components\db\ActiveQuery;
use app\models\CurrencyRateHistory;

/**
 * Class CurrencyRateHistorySearch
 * @package app\models\query
 */
class CurrencyRateHistoryQuery extends ActiveQuery
{
    /**
     * Поиск курса валюты по дате, при этом, если для указанной даты отсутствует курс валюты, то берется ближайший
     * @param string $date
     * @return $this
     */
    public function byDate(string $date)
    {
        $subQuery = CurrencyRateHistory::find()->select([
            'offset' => "MIN(ABS(`date` - DATE('{$date}')))",
            'currency_id'
        ])->groupBy('currency_id');
        $tableName = CurrencyRateHistory::tableName();
        return $this->innerJoin(['nearest_currency_rate_history_date' => $subQuery], "{$tableName}.currency_id = `nearest_currency_rate_history_date`.currency_id AND ABS({$tableName}.date - DATE('{$date}')) = `nearest_currency_rate_history_date`.offset");
    }
}