<?php
namespace app\models\query;

use app\components\db\ActiveQuery;
use app\models\Currency;
use yii\db\Expression;

/**
 * Class TimezoneQuery
 * @package app\models\query
 * @method Currency one($db = null)
 * @method Currency[] all($db = null)
 */
class CurrencyQuery extends ActiveQuery
{
    /**
     * @param string $charCode
     * @return $this
     */
    public function byCharCode($charCode)
    {
        return $this->andWhere(['char_code' => $charCode]);
    }

    /**
     * @param integer $numCode
     * @return $this
     */
    public function byNumCode($numCode)
    {
        return $this->andWhere(['num_code' => $numCode]);
    }

    public function USD()
    {
        return $this->andWhere(['char_code' => 'USD']);
    }

    /**
     * @param float $value
     * @param int $from
     * @param int $to
     * @param null $date
     * @return false|null|string
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    function convertValueToCurrency(float $value, int $from, int $to, $date = null)
    {
        $date = date('Y-m-d', (is_numeric($date) ? $date : strtotime($date)));
        return $this->createCommand()
            ->setSql("SELECT convert_value_to_currency_by_date(:value, :from, :to, :date)")
            ->bindParam(':value', $value)
            ->bindParam(':from', $from)
            ->bindParam(':to', $to)
            ->bindParam(':date', $date)->queryScalar();
    }
}
