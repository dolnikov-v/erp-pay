<?php
namespace app\models\query;

use app\components\db\ActiveQuery;
use Yii;

/**
 * Class UserNotificationSettingQuery
 * @package app\models\query
 */
class UserNotificationSettingQuery extends ActiveQuery
{
    /**
     * @return $this
     */
    public function currentUser()
    {
        return $this->andWhere(['user_id' => Yii::$app->user->id]);
    }
}
