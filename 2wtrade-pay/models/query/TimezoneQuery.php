<?php
namespace app\models\query;

use app\components\db\ActiveQuery;
use app\models\Timezone;

/**
 * Class TimezoneQuery
 * @package app\models\query
 *
 * @method Timezone one() one($db = null)
 * @method Timezone[] all() all($db = null)
 */
class TimezoneQuery extends ActiveQuery
{
    /**
     * @param string $timezoneId
     * @return $this
     */
    public function byTimezoneId($timezoneId)
    {
        return $this->andWhere(['timezone_id' => $timezoneId]);
    }
}
