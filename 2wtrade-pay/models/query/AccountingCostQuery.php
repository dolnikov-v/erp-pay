<?php
namespace app\models\query;

use app\components\db\ActiveQuery;
use app\models\AccountingCost;

/**
 * Class AccountingCostQuery
 * @package app\models\query
 *
 * @see \app\models\AccountingCost
 */
class AccountingCostQuery extends ActiveQuery
{
    /**
     * @param $date
     * @return $this
     */
    public function byDate($date)
    {
        return $this->andWhere([AccountingCost::tableName() . '.date_article' => $date]);
    }
}
