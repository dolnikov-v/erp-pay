<?php
namespace app\models\query;

use app\components\db\ActiveQuery;
use app\models\User;
use app\models\AuthAssignment;
use app\models\UserCountry;
use app\models\UserSource;
use Yii;

/**
 * Class UserQuery
 * @package app\models\query
 *
 * @method User one($db = null)
 * @method User[] all($db = null)
 */
class UserQuery extends ActiveQuery
{
    /**
     * @var string
     */
    protected $collectionValue = 'username';

    /**
     * @return $this
     */
    public function active()
    {
        return $this->andWhere([User::tableName() . '.status' => User::STATUS_DEFAULT]);
    }

    /**
     * @param $countryId
     * @return $this
     */
    public function byCountry($countryId = null)
    {
        if (!$countryId) {
            return $this;
        }
        return $this->leftJoin(UserCountry::tableName(), UserCountry::tableName() . '.user_id=' . User::tableName() . '.id')
            ->andWhere([UserCountry::tableName() . '.country_id' => $countryId]);
    }

    /**
     * @param $sourceId
     * @return $this
     */
    public function bySource($sourceId = null)
    {
        if (!$sourceId) {
            return $this;
        }
        return $this->leftJoin(UserSource::tableName(), UserSource::tableName() . '.user_id=' . User::tableName() . '.id')
            ->andWhere([UserSource::tableName() . '.source_id' => $sourceId]);
    }

    /**
     * @param $role
     * @return $this
     */
    public function byRole($role)
    {
        return $this->leftJoin(AuthAssignment::tableName(), AuthAssignment::tableName() . '.user_id=' . User::tableName() . '.id')
            ->andWhere([AuthAssignment::tableName() . '.item_name' => $role]);
    }

    /**
     * @return $this
     */
    public function byTeam()
    {
        if (Yii::$app->user->isSuperadmin) {
            return $this;
        }
        return $this->andWhere([User::tableName() . '.parent_id' => Yii::$app->user->id]);
    }

    /**
     * @param $id integer
     * @return $this
     */
    public function byLeader($id)
    {
        return $this->andWhere([User::tableName() . '.parent_id' => $id]);
    }

    /**
     * @param $id integer
     * @return $this
     */
    public function byId($id)
    {
        return $this->andWhere([User::tableName() . '.id' => $id]);
    }
}