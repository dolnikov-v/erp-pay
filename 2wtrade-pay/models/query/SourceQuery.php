<?php
namespace app\models\query;

use app\components\db\ActiveQuery;
use app\models\Source;
use yii\helpers\ArrayHelper;

/**
 * Class SourceQuery
 * @package app\models\query
 * @method Source one($db = null)
 * @method Source[] all($db = null)
 */
class SourceQuery extends ActiveQuery
{
    /**
     * @param null $language
     * @return array
     */
    public function collection($language = null)
    {
        return ArrayHelper::map($this->all(), $this->collectionKey, $this->collectionValue);
    }

    /**
     * @return $this
     */
    public function active()
    {
        return $this->andWhere([Source::tableName() . '.active' => 1]);
    }
}
