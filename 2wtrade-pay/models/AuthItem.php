<?php
namespace app\models;

use app\components\db\ActiveRecordLogUpdateTime;
use Yii;
use yii\helpers\ArrayHelper;
use yii\rbac\Item;

/**
 * Class AuthItem
 * @package app\models
 * @property string $name
 * @property string $type
 * @property string $description
 * @property integer $created_at
 * @property integer $updated_at
 * @property Notification[] $notifications
 * @property NotificationRole[] $notificationsRole
 * @property TicketRole[] $ticketsRole
 * @property TicketGroup[] $ticketGroups
 * @property TicketRole[] $outgoingTicketRole
 */
class AuthItem extends ActiveRecordLogUpdateTime
{
    protected $insertLog = true;
    protected $updateLog = true;
    protected $deleteLog = true;

    /**
     * @return null|string
     */
    public function getLogID()
    {
        return $this->name ?? null;
    }

    /**
     * @var string
     */
    public $ticket_group = '';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%auth_item}}';
    }

    /**
     * @param integer $type
     * @param string $key
     * @param string $value
     * @return array
     */
    public static function getCollection($type, $key = 'name', $value = 'description')
    {
        if ($type == Item::TYPE_ROLE) {
            $items = Yii::$app->authManager->getRoles();
        } else {
            $items = Yii::$app->authManager->getPermissions();
        }

        return ArrayHelper::map($items, $key, $value);
    }

    public function rules()
    {
        return [
            [['type', 'name', 'description'], 'required'],
            ['type', 'integer'],
            [['name', 'description'], 'string'],
            [['ticket_group'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('common', 'Название'),
            'description' => Yii::t('common', 'Описание'),
            'ticket_group' => Yii::t('common', 'Тикетная группа')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotificationsRole()
    {
        return $this->hasMany(NotificationRole::className(), ['role' => 'name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotifications()
    {
        return $this->hasMany(Notification::className(), ['trigger' => 'trigger'])->via('notificationsRole');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTicketsRole()
    {
        return $this->hasMany(TicketRole::className(),
            ['role_name' => 'name'])->where(['type' => TicketRole::TYPE_INBOX]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTicketGroups()
    {
        return $this->hasMany(TicketGroup::className(), ['prefix' => 'ticket_prefix'])->via('ticketsRole');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOutgoingTicketRole()
    {
        return $this->hasOne(TicketRole::className(),
            ['role_name' => 'name'])->where(['type' => TicketRole::TYPE_OUTGOING]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOutgoingTicketGroup()
    {
        return $this->hasOne(TicketGroup::className(), ['prefix' => 'ticket_prefix'])->via('outgoingTicketRole');
    }
}
