<?php

namespace app\models\rest;

use app\abstracts\rest\AnswerInterface;

/**
 * Class Answer
 * @package app\models\rest
 */
class Answer implements AnswerInterface
{
    /**
     * @var string
     */
    private $status = self::STATUS_ERROR;

    /**
     * @var null|string
     */
    private $message = null;

    /**
     * @var array
     */
    private $data = [];

    /**
     * @return null|string
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @return null|string
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->status === self::STATUS_SUCCESS;
    }

    /**
     * @param string $status
     * @return Answer
     */
    public function setStatus(string $status): Answer
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @param null|string $message
     * @return Answer
     */
    public function setMessage(?string $message)
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @param array $data
     * @return Answer
     */
    public function setData(array $data): Answer
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @param array $data
     * @return Answer
     */
    public function addData(array $data): Answer
    {
        $this->data = array_merge(is_array($this->data) ? $this->data : [], $data);
        return $this;
    }
}