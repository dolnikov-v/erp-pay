<?php

namespace app\models;

use app\components\ModelTrait;
use Yii;
use yii\mongodb\ActiveRecord;

/**
 * Class SqlErrorLog
 * @package app\models
 *
 * @property string $sql
 * @property string $sql_hash
 * @property string $route
 * @property string $message
 * @property string $trace
 * @property integer $created_at
 * @property integer $notified_at
 */
class SqlErrorLog extends ActiveRecord
{
    use ModelTrait;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }

    /**
     * @return array|string
     */
    public static function collectionName()
    {
        return 'sql_error_log';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['created_at', 'notified_at'], 'integer'],
            [['sql', 'sql_hash', 'route', 'message', 'trace'], 'string']
        ];
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return ['_id', 'created_at', 'notified_at', 'sql', 'sql_hash', 'route', 'message', 'trace'];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'notified_at' => Yii::t('common', 'Дата уведомления'),
            'sql' => Yii::t('common', 'Запрос'),
            'sql_hash' => Yii::t('common', 'Хэш запроса'),
            'route' => Yii::t('common', 'Путь'),
            'message' => Yii::t('common', 'Текст ошибки'),
            'trace' => Yii::t('common', 'След'),
        ];
    }
}