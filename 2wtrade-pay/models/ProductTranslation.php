<?php

namespace app\models;

use app\components\db\ActiveRecordLogUpdateTime;
use Yii;

/**
 * This is the model class for table "product_translation".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $language_id
 * @property string $description
 * @property string $name
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Language $language
 * @property Product $product
 */
class ProductTranslation extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_translation}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'language_id', 'country_id', 'name'], 'required'],
            [['product_id', 'language_id', 'country_id', 'created_at', 'updated_at'], 'integer'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_id' => 'id']],
            [['language_id'], 'unique', 'targetAttribute' => ['product_id', 'language_id', 'country_id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'product_id' => Yii::t('common', 'Товар'),
            'name' => Yii::t('common', 'Название'),
            'description' => Yii::t('common', 'Описание'),
            'language_id' => Yii::t('common', 'Язык'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата обновления'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return array
     */
    public function getFreeCountries()
    {
        $countries = Yii::$app->user->identity->getAllowCountries();
        if ($countries) {
            foreach ($countries as $key => $val) {
                $countries[$key] = Yii::t('common', $val);
            }
        }
        return $countries ?? [];
    }

    /**
     * @return array
     */
    public function getFreeLanguage()
    {
        $language = Language::find()->collection();
        if ($language) {
            foreach ($language as $key => $val) {
                $language[$key] = Yii::t('common', $val);
            }
        }
        return $language ?? [];
    }
}
