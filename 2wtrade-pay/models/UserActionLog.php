<?php
namespace app\models;

use app\components\db\ActiveRecordLogUpdateTime;
use Yii;

/**
 * Class UserActionLog
 * @package app\models
 * @property integer $id
 * @property integer $user_id
 * @property string $url
 * @property integer $http_status
 * @property string $get_data
 * @property string $post_data
 * @property integer $created_at
 * @property User $user
 */
class UserActionLog extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_action_log}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'url'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'user_id' => Yii::t('common', 'Пользователь'),
            'url' => Yii::t('common', 'Адрес перехода'),
            'http_status' => Yii::t('common', 'HTTP статус'),
            'get_data' => Yii::t('common', 'GET'),
            'post_data' => Yii::t('common', 'POST'),
            'created_at' => Yii::t('common', 'Дата добавления'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecordLogUpdateTime::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
