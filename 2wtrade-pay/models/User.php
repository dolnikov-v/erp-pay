<?php

namespace app\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\query\UserQuery;
use app\modules\delivery\models\Delivery;
use app\modules\media\components\Image;
use app\modules\media\models\Media;
use app\modules\order\models\Order;
use Yii;
use yii\base\NotSupportedException;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\IdentityInterface;

/**
 * Class User
 * @package app\models
 * @property integer $id
 * @property string $username
 * @property string $fullname
 * @property string $password
 * @property string $timezone_id
 * @property string $auth_key
 * @property string $email
 * @property string $phone
 * @property string $language
 * @property string $photo_id
 * @property integer $status
 * @property integer $lastact_at
 * @property integer $ping_at
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $force_logout
 * @property boolean $isSuperadmin
 * @property Notification[] $currentNotifications
 * @property array $userNotificationsSettings
 * @property string $ticketRole
 * @property TicketGroup[] $inboxTicketGroups
 * @property array $inboxTicketPrefix
 *
 * @property Country[] $countries
 * @property Delivery[] $deliveries
 * @property array $runningMessages
 * @property Source[] $sources
 * @property string $theme
 * @property string $skin
 * @property string $telegram_chat_id
 * @property integer $parent_id
 * @property integer $person_id
 *
 * @property AuthAssignment[] $roles
 * @property Notification[] $notifications
 * @property Country $activeCountry
 * @property bool $isAdmin
 * @property UserSource[] $userSource
 * @property bool $isDeliveryManager
 * @property string $userTheme
 * @property string $authKey
 * @property null|string $rolesNames
 * @property int $countUnreadNotifications
 * @property UserCountry[] $userCountry
 * @property UserDelivery[] $userDelivery
 * @property array $triggersSetting
 * @property NotificationUser[] $notificationsUser
 * @property array $allThemes
 * @property string $userThemeSkin
 * @property bool $isImplant
 * @property Media $media
 * @property User $parent
 * @property User[] $users
 * @property Timezone $timezone
 */
class User extends ActiveRecordLogUpdateTime implements IdentityInterface
{
    protected $insertLog = true;
    protected $updateLog = true;
    protected $deleteLog = true;

    /**
     * @inheritdoc
     */
    protected function getExcludedAttribute()
    {
        return array_merge(parent::getExcludedAttribute(), ['lastact_at']);
    }

    const SCENARIO_INSERT = 'insert';
    const SCENARIO_UPDATE = 'update';

    const STATUS_DEFAULT = 0x00;
    const STATUS_BLOCKED = 0x01;
    const STATUS_DELETED = 0x02;

    const ROLE_SUPERADMIN = 'superadmin';
    const ROLE_ADMIN = 'admin';
    const ROLE_IMPLANT = 'implant';
    const ROLE_DELIVERY_MANAGER = 'delivery.manager';
    const ROLE_BUSINESS_ANALYST = 'business_analyst';
    const ROLE_SECURITY_MANAGER = 'security.manager';

    const TIME_AFK = 120;
    const TIME_OFFLINE = 600;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'username' => Yii::t('common', 'Имя пользователя'),
            'fullname' => Yii::t('common', 'ФИО (на английском)'),
            'password' => Yii::t('common', 'Пароль'),
            'timezone_id' => Yii::t('common', 'Временная зона'),
            'lastact_at' => Yii::t('common', 'Последний вход'),
            'created_at' => Yii::t('common', 'Дата добавления'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
            'email' => Yii::t('common', 'Email'),
            'phone' => Yii::t('common', 'Телефон'),
            'language' => Yii::t('common', 'Язык'),
            'photo_id' => Yii::t('common', 'Аватар'),
            'roles' => Yii::t('common', 'Роли'),
            'rolesNames' => Yii::t('common', 'Роли'),
            'status' => Yii::t('common', 'Статус'),
            'theme' => Yii::t('common', 'Тема'),
            'skin' => Yii::t('common', 'Расцветка'),
            'telegram_chat_id' => Yii::t('common', 'Telegram ID пользователя'),
            'parent_id' => Yii::t('common', 'Руководитель'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'required', 'on' => ['insert', 'update']],
            [['password', 'email'], 'required', 'on' => 'insert'],

            ['password', 'validateSetPassword', 'skipOnEmpty' => false],

            [['username', 'password'], 'string', 'max' => 64],
            ['phone', 'string', 'max' => 20],
            ['language', 'string', 'max' => 8],
            [['photo_id'], 'integer'],
            [['email'], 'email', 'message' => 'Неверный формат Email'],
            ['timezone_id', 'safe'],

            [
                'username',
                'unique',
                'targetAttribute' => 'username',
                'message' => Yii::t('common', 'Это имя пользователя уже используется.')
            ],

            ['status', 'default', 'value' => self::STATUS_DEFAULT],
            ['force_logout', 'integer'],
            [
                'phone',
                'filter',
                'filter' => function ($value) {
                    return preg_replace("/[^0-9]/", "", $value);
                }
            ],
            ['theme', 'string', 'max' => 64],
            ['skin', 'string', 'max' => 64],
            [['fullname', 'email', 'telegram_chat_id'], 'string', 'max' => 255],
            [
                ['parent_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => ['parent_id' => 'id']
            ],
        ];
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateSetPassword($attribute, $params)
    {
        if ($this->isNewRecord) {
            $this->setPassword($this->$attribute);
        } else {
            if (array_key_exists($attribute, $this->getDirtyAttributes())) {
                $newValue = trim($this->$attribute);
                $hash = $this->getOldAttribute($attribute);

                if (!empty($newValue) && !Yii::$app->security->validatePassword($newValue, $hash)) {
                    $this->setPassword($this->$attribute);

                    if (empty(Yii::$app->user) || $this->id != Yii::$app->user->id) {
                        $this->force_logout = 1;
                    }
                } else {
                    $this->$attribute = $this->getOldAttribute($attribute);
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::find()->where(['user.id' => $id])->one();
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    /**
     * @param \app\models\User $user
     * @return array
     */
    public static function getPingInfo($user)
    {
        $time = time() - $user->lastact_at;

        $label = Yii::t('common', 'На сайте');
        $class = 'label-ping-info label-success';

        if ($time > self::TIME_AFK) {
            $label = Yii::t('common', 'Отошел');
            $class = 'label-ping-info label-warning';

            if ($time > self::TIME_OFFLINE) {
                $label = Yii::t('common', 'Отсутствует');
                $class = 'label-ping-info label-default';
            }
        }

        return [
            'class' => $class,
            'label' => $label,
        ];
    }

    /**
     * @return array
     */
    public static function getAllowCountries()
    {
        if (Yii::$app->user->isSuperadmin) {
            $query = Country::find()->all();
            $countries = ArrayHelper::map($query, 'id', 'name');
        } else {
            $query = UserCountry::find()
                ->joinWith('country')
                ->where(['user_id' => Yii::$app->user->id])
                ->andWhere(['`country`.active' => 1])
                ->all();
            $countries = ArrayHelper::map($query, 'country_id', 'country.name');
        }

        return $countries;
    }

    /**
     * @return boolean
     */
    public static function isAllowCountry()
    {
        if (Yii::$app->user->isSuperadmin) {
            return true;
        } else {
            if (!UserCountry::find()
                ->joinWith(['country'], false)
                ->where(['user_id' => Yii::$app->user->id])
                ->andWhere(['country_id' => Yii::$app->user->country->id])
                ->andWhere(['`country`.active' => 1])
                ->exists()
            ) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param $deliveryId
     * @return boolean
     */
    public static function isAllowDelivery($deliveryId)
    {
        if (Yii::$app->user->isSuperadmin) {
            return true;
        } else {
            if ((
                    UserDelivery::find()
                        ->where(['user_id' => Yii::$app->user->id])
                        ->andWhere(['active' => 1])
                        ->exists()
                    || Yii::$app->user->getIsImplant())
                && !UserDelivery::find()
                    ->where(['user_id' => Yii::$app->user->id])
                    ->andWhere(['delivery_id' => $deliveryId])
                    ->andWhere(['active' => 1])
                    ->exists()
            ) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param $user_id
     * @return array
     */
    public static function getRejectedSources($user_id)
    {
        return UserSource::find()
            ->where([UserSource::tableName() . '.user_id' => $user_id])
            ->andWhere([UserSource::tableName() . '.active' => 0])
            ->select('source_id')
            ->column();
    }


    /**
     * @return string
     */
    public function getUserTheme()
    {

        return $this->theme;
    }

    /**
     * @return string
     */
    public function getUserThemeSkin()
    {

        return $this->skin;
    }

    /**
     * @return array
     */
    public function getAllThemes()
    {

        return [
            'basic' => Yii::t('common', 'Базовая'),
        ];
    }

    /**
     * @param $theme
     * @return array
     */
    public function getAllSkinsOfTheme($theme)
    {

        $skins = [];
        switch ($theme) {
            case 'basic': {
                $skins = [
                    'basic' => Yii::t('common', 'По умолчанию'),
                    'green' => Yii::t('common', 'Зеленая'),
                    'brown' => Yii::t('common', 'Коричневая'),
                    'cyan' => Yii::t('common', 'Голубая'),
                    'grey' => Yii::t('common', 'Серая'),
                    'indigo' => Yii::t('common', 'Индиго'),
                    'orange' => Yii::t('common', 'Оранжевая'),
                    'pink' => Yii::t('common', 'Розовая'),
                    'purple' => Yii::t('common', 'Фиолетовая'),
                    'red' => Yii::t('common', 'Красная'),
                    'teal' => Yii::t('common', 'Изумрудная'),
                    'yellow' => Yii::t('common', 'Желтая'),
                ];
                break;
            }
        }

        return $skins;
    }


    /**
     * @return array
     */
    public function getCurrentNotifications()
    {
        $roles = array_keys(Yii::$app->authManager->getRolesByUser($this->id));

        $roleNotifications = NotificationRole::find()
            ->where(['IN', 'role', $roles])
            ->all();

        $notifyByRole = array_fill_keys(array_unique(ArrayHelper::getColumn($roleNotifications, 'trigger')), 1);

        $modelNotifications = NotificationUser::find()
            ->where(['user_id' => $this->id])
            ->all();

        $userNotifications = ArrayHelper::map($modelNotifications, 'trigger', 'active');
        $triggers = array_merge($notifyByRole, $userNotifications);

        while (($i = array_search(0, $triggers)) !== false) {
            unset($triggers[$i]);
        }

        $currentTriggers = array_keys($triggers);

        return ArrayHelper::index(Notification::find()
            ->where(['IN', 'trigger', $currentTriggers])
            ->all(), 'trigger');
    }

    /**
     * @return integer
     */
    public function getCountUnreadNotifications()
    {
        return UserNotification::find()
            ->where([
                'user_id' => $this->id,
                'read' => UserNotification::UNREAD,
            ])
            ->orderBy('id')
            ->count();
    }

    /**
     * @return array
     */
    public function getTriggersSetting()
    {
        $userNotifications = array_fill_keys(array_keys($this->getCurrentNotifications()), 1);

        $modelNotifications = UserNotificationSetting::find()
            ->where(['user_id' => $this->id])
            ->all();

        $userSettings = ArrayHelper::map($modelNotifications, 'trigger', 'active');

        $triggers = array_merge($userNotifications, $userSettings);

        while (($i = array_search(0, $triggers)) !== false) {
            unset($triggers[$i]);
        }

        return $triggers;
    }

    /**
     * @param bool $original
     * @return string
     */
    public function getAvatarUrl($original = true)
    {
        $avatar = Url::to(Image::DEFAULT_URL_USER_AVATAR);

        if ($this->media) {
            $avatar = $original ? $this->media->getUrlOriginal() : $this->media->getUrlThumbnail();
        }

        return $avatar;
    }

    /**
     * @param integer $user
     * @return string
     */
    public static function getAvatarUrlByUserId($user_id)
    {
        $avatar = Url::to(Image::DEFAULT_URL_USER_AVATAR);

        if (!$user_id) {
            return $avatar;
        }

        $user = self::find()->byId($user_id)->one();

        if ($user === null) {
            return $avatar;
        }

        if ($user->media) {
            $avatar = $user->media->getUrlThumbnail();
        }

        return $avatar;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @param string $password
     * @return boolean
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Проверка на установлен ли статус
     * @param $status
     * @return int
     */
    public function hasStatus($status)
    {
        return $this->status & $status;
    }

    /**
     * Устанавливаем статус
     * @param $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = ($this->status | $status);

        return $this;
    }

    /**
     * Сбрасываем статус
     * @param $status
     * @return $this
     */
    public function unsetStatus($status)
    {
        $this->status = ($this->status & ~$status);

        return $this;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoles()
    {
        return $this->hasMany(AuthAssignment::className(), ['user_id' => 'id']);
    }

    /**
     * @return string|null
     */
    public function getRolesNames()
    {
        $roles = [];

        foreach (Yii::$app->authManager->getRolesByUser($this->id) as $role) {
            $roles[] = $role->name;
        }

        return implode(', ', $roles);
    }

    /**
     * @param null $id
     * @return string
     */
    public static function getRole($id = null)
    {
        if (empty($id)) {
            $id = Yii::$app->user->id;
        }

        $roles = Yii::$app->authManager->getRolesByUser($id);

        if (count($roles) > 0) {

            foreach ($roles as $role) {
                $permissions = Yii::$app->authManager->getPermissionsByRole($role->name);
                if ($role->name == self::ROLE_SUPERADMIN) {
                    return $role->name;
                }
                foreach ($permissions as $permission) {
                    if ($permission->name == 'ticket.ticket.index') {
                        return $role->name . '.' . Yii::$app->user->country->slug;
                    }
                }
            }
        }

        return "";
    }


    /**
     * @return string
     */
    public function getTicketRole()
    {
        if (count($this->roles) > 0) {
            foreach ($this->roles as $role) {
                $permissions = Yii::$app->authManager->getPermissionsByRole($role->item_name);
                $access = false;
                foreach ($permissions as $permission) {
                    if ($permission->name == 'ticket.ticket.index') {
                        $access = true;
                        break;
                    }
                }

                if ($access || $role->item_name == self::ROLE_SUPERADMIN) {
                    $ticketRole = $role->authItem->outgoingTicketRole;
                    if ($ticketRole) {
                        return $ticketRole->ticket_prefix;
                    }
                }
            }
        }

        return "";
    }

    /**
     * @return TicketGroup[]
     */
    public function getInboxTicketGroups()
    {
        $ticketGroups = [];
        if (count($this->roles) > 0) {
            foreach ($this->roles as $role) {
                $groups = $role->authItem->ticketGroups;
                $ticketGroups = array_merge($ticketGroups, $groups);
            }
        }

        return $ticketGroups;
    }

    /**
     * @return array
     */
    public function getInboxTicketPrefix()
    {
        $groups = $this->getInboxTicketGroups();
        $groups = ArrayHelper::getColumn($groups, 'prefix');
        return $groups;
    }

    /**
     * @return bool
     */
    public function getIsSuperadmin()
    {
        return Yii::$app->authManager->getAssignment(User::ROLE_SUPERADMIN, $this->id);
    }

    /**
     * @return bool
     */
    public function getIsAdmin()
    {
        return Yii::$app->authManager->getAssignment(User::ROLE_ADMIN, $this->id);
    }

    /**
     * @return bool
     */
    public function getIsImplant()
    {
        return Yii::$app->authManager->getAssignment(User::ROLE_IMPLANT, $this->id);
    }

    /**
     * @return bool
     */
    public function getIsDeliveryManager()
    {
        return Yii::$app->authManager->getAssignment(User::ROLE_DELIVERY_MANAGER, $this->id);
    }

    /**
     * @return bool
     */
    public function getIsSecurityManager()
    {
        return Yii::$app->authManager->getAssignment(User::ROLE_SECURITY_MANAGER, $this->id);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserCountry()
    {
        return $this->hasMany(UserCountry::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserSource()
    {
        return $this->hasMany(UserSource::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountries()
    {
        return $this->hasMany(Country::className(), ['id' => 'country_id'])
            ->where([Country::tableName() . '.active' => 1])
            ->via('userCountry');
    }

    /**
     * @return Source[]
     */
    public function getSources()
    {
        if (Yii::$app->user->isSuperadmin) {
            $sources = Source::find()->collection();
        } else {
            $sources = Source::find()->where(['not in', 'id' => static::getRejectedSources($this->id)])->collection();
        }

        return $sources;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserDelivery()
    {
        return $this->hasMany(UserDelivery::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveries()
    {
        return $this->hasMany(Delivery::className(), ['id' => 'delivery_id'])
            ->via('userDelivery');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActiveCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id'])
            ->via('userCountry', function ($query) {
                /** @var \yii\db\ActiveQuery $query */

                return $query->andWhere([
                    'active' => 1
                ]);
            });
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotificationsUser()
    {
        return $this->hasMany(NotificationUser::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotifications()
    {
        return $this->hasMany(Notification::className(), ['trigger' => 'trigger'])
            ->via('notificationsUser');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMedia()
    {
        return $this->hasOne(Media::className(), ['id' => 'photo_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(User::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['parent_id' => 'id']);
    }

    /**
     * @return UserQuery
     */
    public static function find()
    {
        return new UserQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTimezone()
    {
        return $this->hasOne(Timezone::class, ['id' => 'timezone_id']);
    }
}
