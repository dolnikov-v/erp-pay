<?php

namespace app\models;

use app\components\db\ActiveRecordLogUpdateTime;
use Yii;

/**
 * This is the model class for table "product_price".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $currency_id
 * @property integer $country_id
 * @property double $price
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Currency $currency
 * @property Product $product
 */
class ProductPrice extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_price}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'currency_id', 'country_id', 'price'], 'required'],
            [['product_id', 'currency_id', 'country_id', 'created_at', 'updated_at'], 'integer'],
            [['price'], 'number'],
            [['currency_id'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::className(), 'targetAttribute' => ['currency_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_id' => 'id']],
            [['currency_id'], 'unique', 'targetAttribute' => ['product_id', 'currency_id', 'country_id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Цена'),
            'product_id' => Yii::t('common', 'Товар'),
            'currency_id' => Yii::t('common', 'Валюта'),
            'country_id' => Yii::t('common', 'Страна'),
            'price' => Yii::t('common', 'Цена'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата обновления'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return array
     */
    public function getFreeCountries()
    {
        $countries = Yii::$app->user->identity->getAllowCountries();
        if ($countries) {
            foreach ($countries as $key => $val) {
                $countries[$key] = Yii::t('common', $val);
            }
        }
        return $countries ?? [];
    }
}
