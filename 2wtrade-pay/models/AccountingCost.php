<?php
namespace app\models;

use app\models\query\AccountingCostQuery;
use app\components\db\ActiveRecordLogUpdateTime;
use Yii;

/**
 * This is the model class for table "accounting_cost".
 *
 * @property integer $id
 * @property string $date_article
 * @property string $code
 * @property string $name
 * @property string $country
 * @property string $subdivision
 * @property double $turnover
 * @property double $incoming
 * @property double $outcoming
 * @property integer $created_at
 * @property integer $updated_at
 */
class AccountingCost extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%accounting_cost}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_article'], 'required'],
            [['date_article'], 'safe'],
            [['turnover', 'incoming', 'outcoming'], 'number'],
            [['created_at', 'updated_at'], 'integer'],
            [['code'], 'string', 'max' => 16],
            [['name', 'subdivision'], 'string', 'max' => 64],
            [['country'], 'string', 'max' => 2],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'date_article' => Yii::t('common', 'Дата'),
            'code' => Yii::t('common', 'Код Статьи'),
            'name' => Yii::t('common', 'Наименование'),
            'country' => Yii::t('common', 'Страна'),
            'subdivision' => Yii::t('common', 'Подразделение'),
            'turnover' => Yii::t('common', 'Сумма Оборот'),
            'incoming' => Yii::t('common', 'Сумма Приход'),
            'outcoming' => Yii::t('common', 'Сумма Расход'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
        ];
    }

    /**
     * @inheritdoc
     * @return AccountingCostQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AccountingCostQuery(get_called_class());
    }
}
