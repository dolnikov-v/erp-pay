<?php
namespace app\models;

use app\components\db\ActiveRecordLogUpdateTime;
use Yii;

/**
 * Class Metric
 * @package app\models
 * @property integer $id
 * @property string $url
 * @property integer $created_at
 * @property integer $updated_at
 */
class Metric extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%metric}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'count'], 'required'],
            ['name', 'string'],
            ['count', 'double']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'name' => Yii::t('common', 'Описание'),
            'count' => Yii::t('common', 'Счётчик'),
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
        ];
    }
}