<?php
namespace app\models;

use app\models\query\PartnerQuery;
use app\components\db\ActiveRecordLogUpdateTime;
use Yii;

/**
 * Class Partner
 * @property integer $id
 * @property integer $source_id
 * @property string $foreign_id
 * @property integer $default
 * @property integer $active
 * @property integer $timezone_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property \yii\db\ActiveQuery $countries
 * @property \yii\db\ActiveQuery $partnerCountries
 * @property \yii\db\ActiveQuery $partnerCountry
 * @property Country $country
 * @property Timezone $timezone
 * @property Source $source
 */
class Partner extends ActiveRecordLogUpdateTime
{
    const ACTIVE = 1;
    const NOT_ACTIVE = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%partner}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['foreign_id', 'timezone_id'], 'required'],
            [['default','active', 'created_at', 'updated_at', 'timezone_id', 'source_id'], 'integer'],
            [['foreign_id'], 'string', 'max' => 100],
            [['source_id'], 'unique', 'targetAttribute' => ['foreign_id', 'source_id']],
            [['timezone_id'], 'exist', 'skipOnError' => true, 'targetClass' => Timezone::className(), 'targetAttribute' => ['timezone_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'source_id' => Yii::t('common', 'Источник'),
            'foreign_id' => Yii::t('common', 'Номер в источнике'),
            'default' => Yii::t('common', 'По умолчанию'),
            'active' => Yii::t('common', 'Активен'),
            'timezone_id' => 'Временная зона',
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSource()
    {
        return $this->hasOne(Source::className(), ['id' => 'source_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartnerCountry()
    {
        return $this->hasMany(PartnerCountry::className(), ['partner_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartnerCountries()
    {
        return $this->hasMany(PartnerCountry::className(), ['partner_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountries()
    {
        return $this->hasMany(Country::className(), ['id' => 'country_id'])
            ->where(['active' => 1])
            ->via('partnerCountry');
    }

    /**
     * @inheritdoc
     * @return PartnerQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PartnerQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTimezone()
    {
        return $this->hasOne(Timezone::className(), ['id' => 'timezone_id']);
    }
}
