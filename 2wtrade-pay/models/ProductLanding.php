<?php
namespace app\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\query\ProductLandingQuery;
use Yii;

/**
 * Class ProductSite
 * @package app\models
 * @property integer $id
 * @property integer $product_id
 * @property integer $landing_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property Product $product
 * @property Landing $landing
 */
class ProductLanding extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_landing}}';
    }

    /**
     * @return ProductLandingQuery
     */
    public static function find()
    {
        return new ProductLandingQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'landing_id'], 'required'],
            [['product_id', 'landing_id'], 'integer'],
            ['product_id', 'unique', 'targetAttribute' => ['product_id', 'landing_id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'product_id' => Yii::t('common', 'Продукт'),
            'landing_id' => Yii::t('common', 'Лендинг'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата обновления'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanding()
    {
        return $this->hasOne(Landing::className(), ['id' => 'landing_id']);
    }
}
