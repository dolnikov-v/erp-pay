<?php
namespace app\models;

use app\components\db\ActiveRecordLogUpdateTime;
use Yii;

/**
 * Class UserNotificationQueue
 * @package app\models
 * @property integer $id
 * @property integer $notification_id
 * @property integer $status_send
 * @property string $interval
 * @property string $type_sending
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Notification $notification
 * @property UserNotification $userNotification
 *
 */
class UserNotificationQueue extends ActiveRecordLogUpdateTime
{
    const SENT = 1;
    const NO_SENT = 0;

    const TYPE_EMAIL = 'email';
    const TYPE_SKYPE = 'skype';
    const TYPE_SMS = 'sms';
    const TYPE_TELEGRAM = 'telegram';

    /**
     * @return array
     */
    public static function getTypesSendingCollection()
    {
        return [
            self::TYPE_EMAIL => Yii::t('common', 'Email'),
            self::TYPE_SKYPE => Yii::t('common', 'Skype'),
            self::TYPE_SMS => Yii::t('common', 'SMS'),
            self::TYPE_TELEGRAM => Yii::t('common', 'Telegram'),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_notification_queue}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['notification_id', 'required'],
            [['notification_id', 'status_send'], 'integer'],
            [['type_sending'], 'in', 'range' => array_keys(self::getTypesSendingCollection())],
            [['interval'], 'in', 'range' => array_keys(UserNotificationSetting::getIntervalCollection())],
            ['interval', 'default', 'value' => UserNotificationSetting::INTERVAL_ALWAYS],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'notification_id' => Yii::t('common', 'Оповещение'),
            'status_send' => Yii::t('common', 'Отправлено'),
            'type_sending' => Yii::t('common', 'Тип отправки'),
            'interval' => Yii::t('common', 'Интервал'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserNotification()
    {
        return $this->hasOne(UserNotification::className(), ['id' => 'notification_id']);
    }

}
