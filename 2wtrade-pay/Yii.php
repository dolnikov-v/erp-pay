<?php
/**
 * Yii bootstrap file.
 * Used for enhanced IDE code autocompletion.
 */
class Yii extends \yii\BaseYii
{
    /**
     * @var BaseApplication|WebApplication|ConsoleApplication the application instance
     */
    public static $app;
}

spl_autoload_register(['Yii', 'autoload'], true, true);
Yii::$classMap = include(__DIR__ . '/vendor/yiisoft/yii2/classes.php');
Yii::$container = new yii\di\Container;

/**
 * Class BaseApplication
 *
 * @property \yii\swiftmailer\Mailer $mailer
 * @property \yii\mongodb\Connection $mongodb
 * @property \app\components\queue\BaseQueue $queue
 * @property \app\components\queue\BaseQueue $queueExport
 * @property \app\components\queue\BaseQueue $queueInvoiceGenerate
 * @property \app\components\queue\QueueTelegram $queueTelegram
 * @property \app\components\queue\BaseQueue $queueTransport
 * @property \app\components\queue\BaseQueue $queueOrders
 * @property \app\components\amazon\sqs\SqsClient $sqsClient
 * @property string $sessionKey
 * @property \app\modules\smsnotification\abstracts\SmsServiceInterface $smsService
 * @property \yii\mutex\Mutex $mutex
 * @property \app\components\MetricTrendDetector $MetricTrendDetector
 * @property \app\modules\callcenter\components\queue\CallCenterQueue $queueCallCenterSend
 * @property \snapsuzun\yii2logger\LoggerInterface $logger
 * @property \app\components\integration1c\SqsOrder $sqsOrder
 */
abstract class BaseApplication extends yii\base\Application
{

}

/**
 * Class WebApplication
 * @property \app\components\web\UrlManager $urlManager
 * @property \app\components\web\User $user
 * @property \app\components\i18n\Formatter $formatter
 * @property \app\components\UserActionLog $userActionLog
 * @property \app\components\Notifier $notifier
 * @property \app\components\Notification $notification
 * @property \app\components\rest\User $apiUser
 * @property \yii\base\Security $security
 *
 */
class WebApplication extends yii\web\Application
{

}

/**
 * Class ConsoleApplication
 * Include only Console application related components here
 *
 */
class ConsoleApplication extends yii\console\Application
{

}
