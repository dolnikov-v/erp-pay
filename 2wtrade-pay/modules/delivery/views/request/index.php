<?php
use app\components\grid\ActionColumn;
use app\components\grid\DateColumn;
use app\components\grid\GridView;
use app\components\grid\IdColumn;
use app\helpers\DataProvider;
use app\modules\delivery\models\DeliveryRequest;
use app\widgets\Label;
use app\widgets\Panel;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\bootstrap\Modal;
use app\widgets\Button;

/** @var yii\web\View $this */
/** @var app\modules\delivery\models\search\DeliveryRequestSearch $modelSearch */
/** @var yii\data\ActiveDataProvider $dataProvider */
/** @var array $deliveriesCollection */

$this->title = Yii::t('common', 'Заявки');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Службы доставки'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'content' => $this->render('_index-filter', [
        'modelSearch' => $modelSearch,
        'deliveriesCollection' => $deliveriesCollection,
    ]),
    'collapse' => true,
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с заявками'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => [
            'class' => 'table table-striped table-hover table-sortable tl-fixed',
        ],
        'columns' => [
            [
                'class' => IdColumn::className(),
            ],
            [
                'attribute' => 'delivery.name',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'order_id',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return Html::a($model->order_id, Url::toRoute([
                        '/order/index/index',
                        'NumberFilter[not][]' => '',
                        'NumberFilter[entity][]' => 'id',
                        'NumberFilter[number][]' => $model->order_id,
                    ]), [
                        'target' => '_blank',
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'status',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    $style = Label::STYLE_DANGER;

                    if ($model->status == DeliveryRequest::STATUS_PENDING) {
                        $style = Label::STYLE_PRIMARY;
                    } elseif ($model->status == DeliveryRequest::STATUS_IN_PROGRESS) {
                        $style = Label::STYLE_INFO;
                    } elseif ($model->status == DeliveryRequest::STATUS_DONE) {
                        $style = Label::STYLE_DEFAULT;
                    }

                    return Label::widget([
                        'label' => DeliveryRequest::getStatusesCollection()[$model->status],
                        'style' => $style,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'api_error',
                'content' => function ($model) {
                    if ($model->status == DeliveryRequest::STATUS_ERROR) {
                        return $model->api_error ? $model->api_error : '—';
                    }

                    return '—';
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'tracking',
                'content' => function ($model) {
                    return $model->tracking ? $model->tracking : '—';
                },
                'enableSorting' => false,
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'updated_at',
            ],
            /**
             * Всплываха с foreign_info
             */
            [
                'content' => function ($model) {
                    Modal::begin([
                        'options' => [
                            'id' => 'del-request-' . $model->id,
                        ],
                        'header' => "<h4>{$model->getAttributeLabel('foreign_info')}</h4>",
                        'footer' =>
                            Button::widget([
                                'label' => Yii::t('common', 'Закрыть'),
                                'size' => Button::SIZE_SMALL,
                                'attributes' => [
                                    'data-dismiss' => 'modal',
                                ],
                            ])
                    ]);
                    echo $model->foreign_info;
                    Modal::end();
                },
                'enableSorting' => false,
                'headerOptions' => [
                    'style' => 'width: 0'
                ]
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'История изменения'),
                        'url' => function ($model) {
                            return Url::toRoute(['/delivery/request/logs', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('delivery.request.logs');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Ответ КС'),
                        'url' => function () {
                            return '#';
                        },
                        'attributes' => function ($model) {
                            return [
                                'data-toggle' => "modal",
                                'data-target' => "#del-request-" . $model->id
                            ];
                        },
                        'can' => function ($model) {
                            if ($model->status == DeliveryRequest::STATUS_ERROR_AFTER_DONE)
                                return true;
                            else return false;
                        }
                    ],
                ],
            ],
        ],
    ]),
    'footer' => LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>
