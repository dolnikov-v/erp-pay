<?php
use app\widgets\Panel;
use yii\helpers\Url;
use app\modules\delivery\widgets\PanelGroupRequestLogs;

/** @var \app\modules\delivery\models\DeliveryRequest[] $logs */

$this->title = Yii::t('common', 'История изменения');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Службы доставки'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Заявки'), 'url' => Url::toRoute('index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'История изменения'),
    'border' => false,
    'content' => PanelGroupRequestLogs::widget([
        'logs' => $logs,
    ]),
]) ?>
