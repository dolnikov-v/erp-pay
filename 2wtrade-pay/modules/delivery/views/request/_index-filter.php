<?php
use app\components\widgets\ActiveForm;
use app\modules\delivery\models\DeliveryRequest;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var app\modules\callcenter\models\search\CallCenterRequestSearch $modelSearch */
/** @var array $deliveriesCollection */
?>

<?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>

<div class="row">
    <div class="col-lg-4">
        <div class="form-group">
            <?= $form->field($modelSearch, 'delivery_id')->select2List($deliveriesCollection, [
                'prompt' => '—',
            ]) ?>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="form-group">
            <?= $form->field($modelSearch, 'status')->select2List(DeliveryRequest::getStatusesCollection(), [
                'prompt' => '—',
            ]) ?>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="form-group">
            <?= $form->field($modelSearch, 'order_id')->textInput() ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')) ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
