<?php
use app\components\grid\ActionColumn;
use app\components\grid\DateColumn;
use app\components\grid\GridView;
use app\helpers\DataProvider;
use app\widgets\Label;
use app\widgets\Panel;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = Yii::t('common', 'API-классы');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Службы доставок'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с API-классами'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function ($model) {
            return [
                'class' => 'tr-vertical-align-middle ' . ($model->active ? '' : 'text-muted'),
            ];
        },
        'columns' => [
            'name',
            [
                'attribute' => 'active',
                'headerOptions' => ['class' => 'text-center width-150'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return Label::widget([
                        'label' => $model->active ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->active ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'can_tracking',
                'headerOptions' => ['class' => 'text-center width-150'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return Label::widget([
                        'label' => $model->can_tracking ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->can_tracking ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'use_amazon_queue',
                'headerOptions' => ['class' => 'text-center width-150'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return Label::widget([
                        'label' => $model->use_amazon_queue ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->use_amazon_queue ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'created_at',
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Просмотреть'),
                        'url' => function ($model) {
                            return Url::toRoute(['/delivery/api-class/view', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('delivery.apiclass.view');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Активировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/delivery/api-class/activate', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('delivery.apiclass.activate') && !$model->active;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Деактивировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/delivery/api-class/deactivate', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('delivery.apiclass.deactivate') && $model->active;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Активировать получение статусов'),
                        'url' => function ($model) {
                            return Url::toRoute(['/delivery/api-class/activate-tracking', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('delivery.apiclass.activatetracking') && !$model->can_tracking;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Деактивировать получение статусов'),
                        'url' => function ($model) {
                            return Url::toRoute(['/delivery/api-class/deactivate-tracking', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('delivery.apiclass.deactivatetracking') && $model->can_tracking;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Использовать очереди Amazon'),
                        'url' => function ($model) {
                            return Url::toRoute(['/delivery/api-class/activate-amazon-queue', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('delivery.apiclass.activateamazonqueue') && !$model->use_amazon_queue;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Не использовать очереди Amazon'),
                        'url' => function ($model) {
                            return Url::toRoute(['/delivery/api-class/deactivate-amazon-queue', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('delivery.apiclass.deactivateamazonqueue') && $model->use_amazon_queue;
                        }
                    ],
                ]
            ]
        ],
    ]),
    'footer' => LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>
