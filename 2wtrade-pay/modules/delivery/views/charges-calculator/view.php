<?php
use app\components\grid\GridView;
use app\widgets\Label;
use app\widgets\Panel;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \app\modules\delivery\models\DeliveryChargesCalculator $model */
/** @var \yii\data\ArrayDataProvider $dataProvider */

$this->title = Yii::t('common', 'Просмотр шаблона по расчету расходов на доставку');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Службы доставки'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Классы шаблонов по расчету расходов'), 'url' => Url::toRoute('/delivery/charges-calculator/index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Шаблон по расчету расходов на доставку'),
    'content' => $this->render('_view-form', [
        'model' => $model,
    ])
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Службы доставки, использующие этот шаблон'),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function ($model) {
            return [
                'class' => 'tr-vertical-align-middle ' . ($model->active ? '' : 'text-muted'),
            ];
        },
        'columns' => [
            [
                'attribute' => 'name',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'active',
                'headerOptions' => ['class' => 'text-center width-150'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return Label::widget([
                        'label' => $model->active ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->active ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
        ]
    ])
]) ?>
