<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/** @var \app\modules\delivery\models\DeliveryChargesCalculator $model */
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['view', 'id' => $model->id])]); ?>
<div class="row">
    <div class="col-lg-6">
        <?= $form->field($model, 'name')->textInput([
            'disabled' => true,
        ]) ?>
    </div>
    <div class="col-lg-6">
        <?= $form->field($model, 'active')->select2List([
            0 => Yii::t('common', 'Нет'),
            1 => Yii::t('common', 'Да'),
        ], [
            'disabled' => true,
        ]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= $form->field($model, 'class_path')->textInput([
            'disabled' => true,
        ]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= $form->link(Yii::t('common', 'Вернуться назад'), Url::toRoute('index')) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
