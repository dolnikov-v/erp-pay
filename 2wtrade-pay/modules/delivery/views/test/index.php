<?php

use yii\web\View;
use app\widgets\Panel;
use app\widgets\Button;
use app\widgets\ButtonLink;
use app\components\widgets\ActiveForm;
use app\modules\delivery\assets\TestAsset;

/**
 * @var yii\web\View $this
 * @var \app\modules\delivery\models\DeliveryRequest $model
 * @var \app\modules\delivery\models\Delivery[] $deliveryServices
 * @var string $result
 */

TestAsset::register($this);

$this->title = Yii::t('common', 'Тестирование апи курьерских служб');
?>

<?php $form = ActiveForm::begin([
    //'action' => Url::toRoute(['send-order']),
    'enableClientValidation' => true,
    'enableAjaxValidation' => true,
]); ?>

<div class="row">
    <div class="col-md-5">
        <div class="form-group">
            <?= $form->field($model, 'delivery_id')->select2List($deliveryServices); ?>
        </div>
    </div>
    <div class="col-md-5">
        <div class="form-group">
            <?= $form->field($model, 'order_id')->textInput(); ?>
        </div>
    </div>
</div>

<div class="row margin-top-20">
    <div class="col-md-6">

    <?= ButtonLink::widget([
        'id' => 'send-order',
        'label' => Yii::t('common', 'Отправить заказ в КС'),
        'url' => '#',
        'style' => ButtonLink::STYLE_SUCCESS,
        'size' => Button::SIZE_SMALL,
        'type' => 'submit',
    ]) ?>
    <?= ButtonLink::widget([
        'id' => 'tracking',
        'label' => Yii::t('common', 'Получить статус заказа'),
        'url' => '#',
        'style' => ButtonLink::STYLE_SUCCESS,
        'size' => Button::SIZE_SMALL,
    ]) ?>
    <?= ButtonLink::widget([
        'id' => 'manifest',
        'label' => Yii::t('common', 'Получить манифест'),
        'url' => '#',
        'style' => ButtonLink::STYLE_SUCCESS,
        'size' => Button::SIZE_SMALL,
    ]) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>

<div class="container">
<div class="row col-md-10 margin-top-50">
        <?= Panel::widget([
            'title'    => Yii::t('common', 'Результаты запроса'),
            'alert' => 'Резльтаты запроса не будут записаны в систему',
            'content' => $result,
        ]) ?>
</div>
</div>