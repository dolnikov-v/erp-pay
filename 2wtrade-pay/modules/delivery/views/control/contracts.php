<?php
use app\components\grid\ActionColumn;
use app\components\grid\DateColumn;
use app\helpers\DataProvider;
use app\widgets\assets\custom\ModalConfirmDeleteAsset;
use app\widgets\ButtonLink;
use app\widgets\custom\ModalConfirmDelete;
use app\widgets\Label;
use app\widgets\Panel;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use app\components\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */
/** @var app\modules\delivery\models\Delivery $modelDelivery */

ModalConfirmDeleteAsset::register($this);

$this->title = Yii::t('common', 'Контракты службы доставки {delivery_name}', ['delivery_name' => $modelDelivery->name]);
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('common', 'Службы доставок'),
    'url' => Url::toRoute('/delivery/control/index')
];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с контрактами'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'summary' => false,
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'id',
                'headerOptions' => ['class' => 'width-50 text-center'],
                'enableSorting' => false,
            ],
            [
                'class' => DateColumn::className(),
                'formatType' => 'Date',
                'attribute' => 'date_from',
                'enableSorting' => false,
            ],
            [
                'class' => DateColumn::className(),
                'formatType' => 'Date',
                'attribute' => 'date_to',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'mutual_settlement',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return Label::widget([
                        'label' => $model->mutual_settlement ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->mutual_settlement ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'charges_calculator_id',
                'value' => function ($model) {
                    return $model->chargesCalculatorModel->name ?? '';
                },
                'enableSorting' => false,
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Просмотр'),
                        'url' => function ($model) {
                            return Url::toRoute(['/delivery/control/contract-view', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('delivery.control.contractview');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/delivery/control/contract-edit', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('delivery.control.contractedit');
                        }
                    ],
                    [
                        'can' => function () {
                            return Yii::$app->user->can('delivery.control.contractdelete') && (
                                    Yii::$app->user->can('delivery.control.contractview')
                                    || Yii::$app->user->can('delivery.control.contractedit')
                                );
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Лог изменений'),
                        'url' => function ($model) use ($modelDelivery) {
                            return Url::toRoute(['/delivery/control/contract-log', 'id' => $model->id, 'delivery' => $modelDelivery->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('delivery.control.contractlog');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить'),
                        'url' => function () {
                            return '#';
                        },
                        'style' => 'confirm-delete-link',
                        'attributes' => function ($model) {
                            return [
                                'data-href' => Url::toRoute(['/delivery/control/contract-delete', 'id' => $model->id]),
                            ];
                        },
                        'can' => function () {
                            return Yii::$app->user->can('delivery.control.contractdelete');
                        }
                    ],
                ]
            ]
        ],
    ]),
    'footer' => (Yii::$app->user->can('delivery.control.contractedit') ?
            ButtonLink::widget([
                'label' => Yii::t('common', 'Добавить контракт'),
                'url' => Url::toRoute(['/delivery/control/contract-edit', 'delivery_id' => $modelDelivery->id]),
                'style' => ButtonLink::STYLE_SUCCESS,
                'size' => ButtonLink::SIZE_SMALL,
            ]) : '') . LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>

<?= ModalConfirmDelete::widget() ?>
