<?php
use app\modules\delivery\models\DeliveryContacts;
use app\modules\delivery\assets\ContactsAsset;

/** @var app\modules\delivery\models\Delivery $model */
/** @var app\modules\delivery\models\DeliveryContacts[] $modelsDeliveryContacts */
/** @var app\components\widgets\ActiveForm $form */
/** @var bool $isView */

ContactsAsset::register($this);
?>

<div class="col-lg-12 ">
    <h4><?= Yii::t('common', 'Ответственные лица') ?></h4>
</div>


<div class="col-lg-12 margin-bottom-20 group-contacts-group">
    <?php if (empty($modelsDeliveryContacts)): ?>
        <?= $this->render('_edit-form-contacts', [
            'form' => $form,
            'model' => new DeliveryContacts(),
            'isView' => $isView
        ]) ?>
    <?php else: ?>
        <?php foreach ($modelsDeliveryContacts as $key => $modelDeliveryContacts): ?>
            <?= $this->render('_edit-form-contacts', [
                'form' => $form,
                'model' => $modelDeliveryContacts,
                'isView' => $isView
            ]) ?>
        <?php endforeach; ?>
    <?php endif; ?>
</div>

