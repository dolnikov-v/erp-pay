<?php
use yii\helpers\Html;
$days = array_combine(range(1, 31), range(1, 31));
?>
<div class="col-sm-10 col-md-10 col-lg-11 margin-bottom-10">
    <div class="row">
        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-5">
                    <?= $form->field($model, 'from')->select2List($days, ['name' => Html::getInputName($model, 'from') . '[]'])->label(false) ?>
                </div>
                <div class="col-sm-1 range-line" style="height: 18px; border-bottom: 1px solid;"></div>
                <div class="col-sm-5">
                    <?= $form->field($model, 'to')->select2List($days, ['name' => Html::getInputName($model, 'to') . '[]', 'value' => $model->to ?? 31])->label(false) ?>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'day')->select2List(['—'] + $days, ['name' => Html::getInputName($model, 'day') . '[]'])->label(false) ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'time')->datePicker([
                'showTime' => true,
                'attributes' => [
                    'data-format' => 'HH:mm',
                ],
                'format' => 'HH:mm',
                'type' => 'month',
                'name' => Html::getInputName($model, 'time') . '[]',
            ])->label(false) ?>
        </div>
    </div>
</div>