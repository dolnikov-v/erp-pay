<?php
use app\widgets\Panel;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \app\modules\delivery\models\Delivery $model */
/** @var \app\modules\delivery\models\DeliveryTiming $timings */
/** @var array $excelBuilderColumns */

$this->title = Yii::t('common', 'Редактирование службы доставки');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Службы доставок'), 'url' => '#'];
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('common', 'Управление'),
    'url' => Url::toRoute('/delivery/control/index')
];
$this->params['breadcrumbs'][] = ['label' => $model->name];

?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Служба доставки'),
    'nav' => new \app\modules\delivery\widgets\DeliveryLinkNav([
        'delivery' => $model
    ]),
    'content' => $this->render('_edit-form-auto-list', [
        'model' => $model,
        'timings' => $timings,
        'isView' => false,
        'excelBuilderColumns' => $excelBuilderColumns
    ])
]) ?>
