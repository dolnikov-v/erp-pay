<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 * @var \app\modules\delivery\models\DeliveryContract $deliveryContract
 * @var \app\modules\delivery\components\charges\ChargesCalculatorAbstract $chargesCalculator
 * @var bool $onlyView
 */

$form = ActiveForm::begin([
    'action' => $onlyView ? '' : Url::toRoute(['edit-charges-values', 'id' => $deliveryContract->id,]),
    'options' => [
        'class' => 'charges-form',
        'enableClientValidation' => false
    ]
])
?>

    <div class="charges-calculator-fields">
        <?= $chargesCalculator->render($form, $onlyView) ?>
    </div>

    <div class="row-hr">
        <hr/>
    </div>

    <div class="row margin-top-20">
        <div class="col-lg-12">
            <?php if ($onlyView): ?>
                <div class="text-right">
                    <?= $form->link(Yii::t('common', 'Назад'), Url::toRoute([
                        'contracts',
                        'id' => $deliveryContract->delivery_id
                    ])) ?>
                </div>
            <?php else: ?>
                <?= $form->submit(Yii::t('common', 'Сохранить изменения')) ?>
                <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute([
                    'contracts',
                    'id' => $deliveryContract->delivery_id
                ])) ?>
            <?php endif; ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>