<?php
use app\modules\delivery\widgets\Storage;
use app\modules\storage\models\Storage as StorageModel;
use app\widgets\Panel;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \app\modules\delivery\models\Delivery $model */

$this->title = Yii::t('common', 'Редактирование службы доставки');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Службы доставок'), 'url' => '#'];
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('common', 'Управление'),
    'url' => Url::toRoute('/delivery/control/index')
];
$this->params['breadcrumbs'][] = ['label' => $model->name];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Список складов'),
    'nav' => new \app\modules\delivery\widgets\DeliveryLinkNav([
        'delivery' => $model
    ]),
    'content' => Storage::widget([
        'model' => $model,
        'storages' => StorageModel::find()->byCountryId(Yii::$app->user->country->id)->all(),
        'storagesByDelivery' => $model->storages,
    ])
]) ?>