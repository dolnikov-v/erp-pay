<?php
use app\widgets\Panel;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \app\modules\delivery\models\Delivery $model */
/** @var \app\modules\delivery\models\DeliveryTiming $timings */
/** @var array $workflows */

/** @var app\modules\delivery\models\DeliveryContacts[] $modelsDeliveryContacts */
/** @var app\modules\delivery\models\DeliverySku[] $modelsDeliverySku */
/** @var array $packagers */
/** @var \app\models\Product[] $products */
/** @var app\modules\delivery\models\DeliveryAutoChangeReason[] $modelsDeliveryAutoChangeReason */
/** @var array $otherDeliveries */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление службы доставки') : Yii::t('common', 'Редактирование службы доставки');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Службы доставок'), 'url' => '#'];
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('common', 'Управление'),
    'url' => Url::toRoute('/delivery/control/index')
];

if (!$model->isNewRecord) {
    $this->params['breadcrumbs'][] = ['label' => $model->name];
}

?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Служба доставки'),
    'alert' => $model->isNewRecord ? Yii::t('common', 'Служба доставки будет добавлена и привязана к активной стране - {country}.', [
        'country' => Yii::$app->user->country->name,
    ]) : '',
    'nav' => new \app\modules\delivery\widgets\DeliveryLinkNav([
        'delivery' => $model
    ]),
    'content' => $this->render('_edit-form', [
        'model' => $model,
        'timings' => $timings,
        'workflows' => $workflows,
        'modelsDeliveryContacts' => $modelsDeliveryContacts,
        'modelsDeliverySku' => $modelsDeliverySku,
        'packagers' => $packagers,
        'products' => $products,
        'modelsDeliveryAutoChangeReason' => $modelsDeliveryAutoChangeReason,
        'otherDeliveries' => $otherDeliveries,
        'isView' => false,
    ])
]) ?>