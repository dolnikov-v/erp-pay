<?php

use app\helpers\WbIcon;
use app\widgets\Button;
use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\components\widgets\ActiveForm $form */
/** @var app\modules\delivery\models\DeliveryAutoChangeReason $model */
/** @var array $otherDeliveries */
/** @var bool $isView */

?>

<div class="row row-delivery-auto-change-reason">

    <?= Html::hiddenInput(Html::getInputName($model, 'id') . '[]', $model->id) ?>

    <div class="col-lg-4">
        <?= $form->field($model, 'to_delivery_id')->select2List($otherDeliveries,
            [
                'length' => 1,
                'prompt' => '-',
                'id' => false,
                'name' => Html::getInputName($model, 'to_delivery_id') . '[]',
                'disabled' => $isView
            ]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'reason')->textInput([
            'id' => false,
            'name' => Html::getInputName($model, 'reason') . '[]',
            'disabled' => $isView
        ]) ?>
    </div>
    <?php if (!$isView): ?>
        <div class="col-lg-2">
            <div class="form-group">
                <div class="control-label">
                    <label>&nbsp;</label>
                </div>
                <?= Button::widget([
                    'type' => 'button',
                    'icon' => WbIcon::MINUS,
                    'style' => Button::STYLE_DANGER . ' btn-delivery-auto-change-reason btn-delivery-auto-change-reason-minus',
                ]) ?>

                <?= Button::widget([
                    'type' => 'button',
                    'icon' => WbIcon::PLUS,
                    'style' => Button::STYLE_SUCCESS . ' btn-delivery-auto-change-reason btn-delivery-auto-change-reason-plus',
                ]) ?>

            </div>
        </div>
    <?php endif; ?>

</div>


