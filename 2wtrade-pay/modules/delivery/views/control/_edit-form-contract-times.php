<?php
use app\helpers\WbIcon;
use app\widgets\Button;
use yii\helpers\Html;
use app\modules\delivery\models\DeliveryContractTime;

/** @var yii\web\View $this */
/** @var app\components\widgets\ActiveForm $form */
/** @var app\modules\delivery\models\DeliveryContractTime $model */
/** @var int $cnt */
?>
<div class="row row-delivery-contract-times">

    <div class="col-lg-3">
        <?= $form->field($model, 'field_name')->select2List(DeliveryContractTime::getTypes(), [
            'id' => false,
            'name' => Html::getInputName($model, 'field_name') . '[]',
        ])->label(Yii::t('common', 'Тип регионов')) ?>
    </div>

    <div class="col-lg-3">
        <?= $form->field($model, 'delivered_from')->textInput([
            'id' => false,
            'name' => Html::getInputName($model, 'delivered_from') . '[]',
        ]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'delivered_to')->textInput([
            'id' => false,
            'name' => Html::getInputName($model, 'delivered_to') . '[]',
        ]) ?>
    </div>

    <div class="col-lg-2">
        <div class="form-group">
            <div class="control-label">
                <label>&nbsp;</label>
            </div>
            <?= Button::widget([
                'type' => 'button',
                'icon' => WbIcon::MINUS,
                'style' => Button::STYLE_DANGER . ' btn-delivery-contract-times btn-delivery-contract-times-minus',
            ]) ?>
        </div>
    </div>

    <div class="col-lg-9">
        <?= $form->field($model, 'field_value')->textarea([
            'id' => false,
            'name' => Html::getInputName($model, 'field_value') . '[]',
        ])->label(Yii::t('common', 'Регионы (список через запятую)')) ?>
    </div>
</div>


