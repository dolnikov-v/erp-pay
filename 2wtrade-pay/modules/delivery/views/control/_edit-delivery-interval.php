<?php

/** @var app\modules\delivery\models\Delivery $model */
/** @var app\components\widgets\ActiveForm $form */
?>

<div class="row margin-top-20">
    <div class="col-lg-12">
        <h4><?= Yii::t('common', 'Интервалы доставки') ?></h4>
        <?= Yii::t('common', 'Интервалы доставки для каждого дня задаются в формате: 9-12;18-19:30. Если в какой-то день доставки нет, то оставить это поле пустым.') ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-6">
        <?= $form->field($model, 'monday')->textInput([
            'value' => $model->getIntervalsOneDay('monday')
        ]) ?>
    </div>
    <div class="col-lg-6">
        <?= $form->field($model, 'tuesday')->textInput([
            'value' => $model->getIntervalsOneDay('tuesday')
        ]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        <?= $form->field($model, 'wednesday')->textInput([
            'value' => $model->getIntervalsOneDay('wednesday')
        ]) ?>
    </div>
    <div class="col-lg-6">
        <?= $form->field($model, 'thursday')->textInput([
            'value' => $model->getIntervalsOneDay('thursday')
        ]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        <?= $form->field($model, 'friday')->textInput([
            'value' => $model->getIntervalsOneDay('friday')
        ]) ?>
    </div>
    <div class="col-lg-6">
        <?= $form->field($model, 'saturday')->textInput([
            'value' => $model->getIntervalsOneDay('saturday')
        ]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        <?= $form->field($model, 'sunday')->textInput([
            'value' => $model->getIntervalsOneDay('sunday')
        ]) ?>
    </div>
</div>
