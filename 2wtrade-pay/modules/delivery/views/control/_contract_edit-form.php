<?php
use app\components\widgets\ActiveForm;
use app\modules\delivery\models\DeliveryChargesCalculator;
use yii\helpers\Url;

/** @var \app\modules\delivery\models\DeliveryContract $model */
/** @var \app\modules\delivery\models\Delivery $modelDelivery */
/** @var \yii\data\ActiveDataProvider $dataProviderFiles */
/** @var bool $onlyView */
/** @var array $requisites */
/** @var \app\modules\catalog\models\RequisiteDelivery[] $allRequisites */
/** @var \app\modules\delivery\models\DeliveryContractTime[] $modelsDeliveryContractTimes */
?>

<?php $form = ActiveForm::begin([
    'options' => [
        'enctype' => 'multipart/form-data',
    ],
    'action' => $onlyView ? '' : Url::toRoute([
        'contract-edit',
        'id' => $model->id,
        'delivery_id' => $modelDelivery->id
    ]),
    'method' => 'post',
    'enableClientValidation' => true
]); ?>

<div class="row">
    <div class="col-lg-12">
        <h4><?= Yii::t('common', 'Шаблон') ?></h4>
    </div>
</div>

<div class="row">
    <div class="col-lg-3">
        <?= $form->field($model, 'charges_calculator_id')->select2List(DeliveryChargesCalculator::find()
            ->collection(), [
            'prompt' => '—',
            'length' => false,
            'disabled' => $onlyView
        ]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-3 no-margin-custom-checkbox">
        <?= $form->field($model, 'mutual_settlement')->checkboxCustom([
            'value' => 1,
            'uncheckedValue' => 0,
            'checked' => $model->mutual_settlement,
        ]) ?>
    </div>
</div>
<div class="row margin-top-20">
    <div class="col-lg-3">
        <?= $form->field($model, 'bank_name')->textInput() ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'bank_account')->textInput() ?>
    </div>
</div>

<br/>

<div class="row">
    <div class="col-lg-12">
        <h4><?= Yii::t('common', 'Период действия') ?></h4>
    </div>
</div>

<div class="row">
    <div class="col-lg-4">
        <?= $form->field($model, 'date_from')->datePicker(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'date_to')->datePicker(['disabled' => $onlyView]) ?>
    </div>
</div>


<?= $this->render('_contract_files_edit', [
    'model' => $model,
    'dataProviderFiles' => $dataProviderFiles,
    'form' => $form,
    'onlyView' => $onlyView,
]) ?>

<div class="row-hr">
    <hr/>
</div>

<div class="row">
    <div class="col-lg-12">
        <h4><?= Yii::t('common', 'Реквизит контракта по умолчанию') ?></h4>
    </div>
</div>

<?= $this->render('_contract_delivery-requisites', [
    'model' => $model,
    'requisites' => $requisites,
    'allRequisites' => $allRequisites,
    'form' => $form,
    'onlyView' => $onlyView,
]) ?>

<div class="row-hr">
    <hr/>
</div>

<div class="row">
    <div class="col-lg-12">
        <h4><?= Yii::t('common', 'Сроки доставки') ?></h4>
    </div>
</div>

<div class="row">
    <div class="col-lg-3">
        <?= $form->field($model, 'delivered_from')->textInput() ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'delivered_to')->textInput() ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'max_delivery_period')->textInput() ?>
    </div>
</div>
<div class="row margin-top-20">
    <?= $this->render('_edit-times', [
        'model' => $model,
        'modelsDeliveryContractTimes' => $modelsDeliveryContractTimes,
        'form' => $form,
        'onlyView' => $onlyView,
    ]) ?>
</div>

<div class="row">
    <div class="col-lg-12">
        <h4><?= Yii::t('common', 'Сроки перечисления платежей') ?></h4>
    </div>
</div>

<div class="row">
    <div class="col-lg-3">
        <?= $form->field($model, 'paid_from')->textInput() ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'paid_to')->textInput() ?>
    </div>
</div>

<div class="row margin-top-20">
    <div class="col-lg-12">
        <?php if ($onlyView): ?>
            <div class="text-right">
                <?= $form->link(Yii::t('common', 'Назад'), Url::toRoute(['contracts', 'id' => $modelDelivery->id])) ?>
            </div>
        <?php else: ?>
            <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить контракт') : Yii::t('common',
                'Сохранить контракт')) ?>
            <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute(['contracts', 'id' => $modelDelivery->id])) ?>
        <?php endif; ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
