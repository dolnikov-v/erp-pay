<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/** @var \app\modules\delivery\models\Delivery $model */
/** @var array $apiClasses */
/** @var bool $isView */

?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute(['edit-system-settings', 'id' => $model->id]),
    'enableClientValidation' => true
]); ?>

<div class="row">
    <div class="col-lg-3">
        <?= $form->field($model, 'api_class_id')->select2List($apiClasses, [
            'prompt' => '—',
            'disabled' => $isView,
            'length' => false
        ]) ?>
    </div>

    <div class="col-lg-9">
        <div class="row">
            <div class="col-lg-12">
                <?= $form->field($model, 'not_send_if_no_contract')->checkboxCustom([
                    'id' => false,
                    'checked' => $model->not_send_if_no_contract,
                    'value' => 1,
                    'uncheckedValue' => 0,
                ]); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-9">
                <?= $form->field($model, 'not_send_if_has_debts')->checkboxCustom([
                    'id' => false,
                    'checked' => $model->not_send_if_has_debts,
                    'value' => 1,
                    'uncheckedValue' => 0,
                ]); ?>
            </div>
            <div class="col-lg-3">
                <?= $form->field($model, 'not_send_if_has_debts_days')->textInput(['disabled' => $isView]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-9">
                <?= $form->field($model, 'not_send_if_in_process')->checkboxCustom([
                    'id' => false,
                    'checked' => $model->not_send_if_in_process,
                    'value' => 1,
                    'uncheckedValue' => 0,
                ]); ?>
            </div>
            <div class="col-lg-3">
                <?= $form->field($model, 'not_send_if_in_process_days')->textInput(['disabled' => $isView]) ?>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-3">
        <?= $form->field($model, 'date_format')->textInput(['disabled' => $isView]) ?>
    </div>
    <div class="col-lg-9">
        <?= $form->field($model, 'amazon_queue_name')->textInput(['disabled' => $isView]) ?>
    </div>
</div>

<div class="row-hr">
    <hr/>
</div>

<div class="row">
    <div class="col-lg-12">
        <?php if (!$isView): ?>
            <?= $form->submit(Yii::t('common', 'Сохранить службу доставки')) ?>
        <?php endif; ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
