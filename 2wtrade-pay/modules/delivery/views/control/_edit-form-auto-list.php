<?php

use app\components\widgets\ActiveForm;
use app\modules\delivery\assets\ChangerTimingAsset;
use app\modules\delivery\assets\EmailListAsset;
use app\modules\delivery\widgets\EmailList;
use app\modules\order\components\ExcelBuilder;
use yii\helpers\Url;

/** @var \app\modules\delivery\models\Delivery $model */
/** @var \app\modules\delivery\models\DeliveryTiming $timings */
/** @var bool $isView */
/** @var array $excelBuilderColumns */

ChangerTimingAsset::register($this);
EmailListAsset::register($this);
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute(['edit-auto-list', 'id' => $model->id]),
    'enableClientValidation' => true
]); ?>
<?php if (!$model->hasApiGenerateList()): ?>
    <div class="row-with-columns">
        <div class="row">
            <div class="col-xs-12 text-center">
                <h4><?= Yii::t('common', 'Экспортируемые колонки') ?></h4>
            </div>
        </div>
        <hr>
        <div class="row">
            <?php foreach ($excelBuilderColumns as $columns): ?>
                <div class="col-xs-<?= ceil(12 / count($excelBuilderColumns)) ?>">
                    <div class="col-choice-columns">
                        <?php foreach ($columns as $column): ?>
                            <?= $form->field($model, 'auto_list_generation_columns')->checkboxCustom([
                                'id' => 'column_' . md5($column),
                                'name' => \yii\helpers\Html::getInputName($model, 'auto_list_generation_columns') . '[]',
                                'value' => $column,
                                'label' => ExcelBuilder::getColumnLabel($column),
                                'checked' => in_array($column, !empty($model->autoListGenerationColumns) ? $model->autoListGenerationColumns : ExcelBuilder::$autoGenerateColumns) ? true : false,
                            ]) ?>
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    <hr>
<?php endif; ?>

<div class="row">
    <div class="col-lg-4">
        <?= $form->field($model, 'auto_list_generation_size')->textInput(['disabled' => $isView]) ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'auto_generating_invoice')->checkboxCustom([
            'value' => 1,
            'checked' => $model->auto_generating_invoice,
            'uncheckedValue' => 0,
            'disabled' => $isView
        ]) ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'auto_generating_barcode')->checkboxCustom([
            'value' => 1,
            'checked' => $model->auto_generating_barcode,
            'uncheckedValue' => 0,
            'disabled' => $isView
        ]) ?>
    </div>
</div>

<div class="row margin-bottom-5">
    <div class="col-lg-12">
        <?= Yii::t('common', 'Список e-mail для автоматической отправки листов') ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <?= EmailList::widget(['emailList' => $model->emailList]) ?>
    </div>
</div>

<div class="row-hr">
    <hr/>
</div>

<div class="row margin-top-20">
    <div class="col-lg-12">
        <?php if (!$isView): ?>
            <?= $form->submit(Yii::t('common', 'Сохранить службу доставки')) ?>
        <?php endif; ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
