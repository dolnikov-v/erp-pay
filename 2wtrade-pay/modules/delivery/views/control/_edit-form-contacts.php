<?php
use app\helpers\WbIcon;
use app\widgets\Button;
use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\components\widgets\ActiveForm $form */
/** @var app\modules\delivery\models\DeliveryContacts $model */
/** @var bool $isView */
?>

<div class="row row-delivery-contacts">

    <?= Html::hiddenInput(Html::getInputName($model, 'id') . '[]', $model->id) ?>

    <div class="col-lg-3">
        <?= $form->field($model, 'job_title')->textInput([
            'id' => false,
            'name' => Html::getInputName($model, 'job_title') . '[]',
            'disabled' => $isView
        ]) ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'full_name')->textInput([
            'id' => false,
            'name' => Html::getInputName($model, 'full_name') . '[]',
            'disabled' => $isView
        ]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'email')->textInput([
            'id' => false,
            'name' => Html::getInputName($model, 'email') . '[]',
            'disabled' => $isView
        ]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'phone')->textInput([
            'id' => false,
            'name' => Html::getInputName($model, 'phone') . '[]',
            'disabled' => $isView
        ]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'mobile_phone')->textInput([
            'id' => false,
            'name' => Html::getInputName($model, 'mobile_phone') . '[]',
            'disabled' => $isView
        ]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'additional_phone')->textInput([
            'id' => false,
            'name' => Html::getInputName($model, 'additional_phone') . '[]',
            'disabled' => $isView
        ]) ?>
    </div>

    <?php if (!$isView): ?>
        <div class="col-lg-2">
            <div class="form-group">
                <div class="control-label">
                    <label>&nbsp;</label>
                </div>
                <?= Button::widget([
                    'type' => 'button',
                    'icon' => WbIcon::MINUS,
                    'style' => Button::STYLE_DANGER . ' btn-delivery-contacts btn-delivery-contacts-minus',
                ]) ?>

                <?= Button::widget([
                    'type' => 'button',
                    'icon' => WbIcon::PLUS,
                    'style' => Button::STYLE_SUCCESS . ' btn-delivery-contacts btn-delivery-contacts-plus',
                ]) ?>

            </div>
        </div>
    <?php endif; ?>
</div>