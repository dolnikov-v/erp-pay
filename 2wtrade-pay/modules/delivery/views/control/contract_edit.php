<?php
use app\widgets\Panel;
use yii\helpers\Url;
use app\modules\delivery\assets\DeliveryContractEditAsset;
use app\widgets\Nav;

/** @var \yii\web\View $this */
/** @var \app\modules\delivery\models\DeliveryContract $model */
/** @var \app\modules\delivery\models\Delivery $modelDelivery */
/** @var \yii\data\ActiveDataProvider $dataProviderFiles */
/** @var \app\modules\delivery\components\charges\ChargesCalculatorAbstract $chargesCalculator */
/** @var bool $onlyView */
/** @var array $requisites */
/** @var \app\modules\catalog\models\RequisiteDelivery[] $allRequisites */
/** @var \app\modules\delivery\models\DeliveryContractTime[] $modelsDeliveryContractTimes */

$contract = $modelDelivery->getActiveContract();
$showDeliveryTabs = false;
if ($model && $contract && $contract->id == $model->id) {
    $showDeliveryTabs = true;
}

if (!isset($onlyView)) {
    $onlyView = false;
}
DeliveryContractEditAsset::register($this);

if ($showDeliveryTabs) {
    $this->title = Yii::t('common', 'Редактирование службы доставки');
    $this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Службы доставок'), 'url' => '#'];
    $this->params['breadcrumbs'][] = [
        'label' => Yii::t('common', 'Управление'),
        'url' => Url::toRoute('/delivery/control/index')
    ];
    $this->params['breadcrumbs'][] = ['label' => $modelDelivery->name];
}
else {
    $this->title = $model->isNewRecord ? Yii::t('common', 'Добавление контракта') : Yii::t('common', 'Редактирование контракта');
    $this->params['breadcrumbs'][] = [
        'label' => Yii::t('common', 'Службы доставок'),
        'url' => Url::toRoute('/delivery/control/index')
    ];
    $this->params['breadcrumbs'][] = [
        'label' => Yii::t('common', 'Контракты службы доставки {delivery_name}', ['delivery_name' => $modelDelivery->name]),
        'url' => Url::toRoute(['contracts', 'id' => $modelDelivery->id])
    ];
    $this->params['breadcrumbs'][] = ['label' => $this->title];
}

$tabs[] = [
    'label' => Yii::t('common', 'Данные контракта'),
    'content' => $this->render('_contract_edit-form', [
        'model' => $model,
        'modelDelivery' => $modelDelivery,
        'dataProviderFiles' => $dataProviderFiles,
        'onlyView' => $onlyView,
        'requisites' => $requisites,
        'allRequisites' => $allRequisites ?? [],
        'modelsDeliveryContractTimes' => $modelsDeliveryContractTimes,
    ]),
];
if (!$model->isNewRecord && $model->chargesCalculatorModel && $model->chargesCalculatorModel->active) {
    $tabs[] = [
        'label' => Yii::t('common', 'Шаблон по расчету расходов на доставку'),
        'content' => $this->render('_edit-form-charges-values', [
            'deliveryContract' => $model,
            'chargesCalculator' => $chargesCalculator,
            'onlyView' => $onlyView,
        ]),
    ];
}

$widgetConfig = [
    'title' => Yii::t('common', 'Контракт'),
    'withBody' => false,
    'content' =>
        Panel::widget([
            'alert' => $model->isNewRecord ? Yii::t('common', 'Контракт будет добавлен и привязан к службе доставки - {delivery_name}', ['delivery_name' => $modelDelivery->name]) : '',
            'nav' => new Nav([
                'tabs' => $tabs
            ])
        ])
];
if ($showDeliveryTabs) {
    $widgetConfig['nav'] = new \app\modules\delivery\widgets\DeliveryLinkNav([
        'delivery' => $modelDelivery
    ]);
}
?>

<?= Panel::widget($widgetConfig)?>
