<?php
use app\components\grid\ActionColumn;
use app\components\grid\DateColumn;
use app\helpers\DataProvider;
use app\widgets\assets\custom\ModalConfirmDeleteAsset;
use app\widgets\ButtonLink;
use app\widgets\custom\ModalConfirmDelete;
use app\widgets\Label;
use app\widgets\Panel;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use app\components\grid\SortableGridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

ModalConfirmDeleteAsset::register($this);

$this->title = Yii::t('common', 'Управление');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Службы доставок'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица со службами доставок'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => SortableGridView::widget([
        'summary' => false,
        'dataProvider' => $dataProvider,
        'rowOptions' => function ($model) {
            return [
                'class' => 'tr-vertical-align-middle ' . ($model->active ? '' : 'text-muted'),
            ];
        },
        'columns' => [
            [
                'attribute' => 'id',
                'headerOptions' => ['class' => 'width-50 text-center'],
                'content' => function ($model) {
                    return $model->id;
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'legal_name',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'name',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'api_class',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return $model->apiClass ? Html::encode($model->apiClass->name) : '—';
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'our_api',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return Label::widget([
                        'label' => $model->our_api ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->our_api ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'workflow',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return $model->workflow ? Html::encode($model->workflow->name) : '—';
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'active',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return Label::widget([
                        'label' => $model->active ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->active ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'brokerage_active',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return Label::widget([
                        'label' => $model->brokerage_active ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->brokerage_active ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'express_delivery',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return Label::widget([
                        'label' => $model->express_delivery ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->express_delivery ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'auto_sending',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return Label::widget([
                        'label' => $model->auto_sending ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->auto_sending ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'can_tracking',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return Label::widget([
                        'label' => $model->apiClass && $model->apiClass->can_tracking && $model->can_tracking ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->apiClass && $model->apiClass->can_tracking && $model->can_tracking ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
                'visible' => Yii::$app->user->can('delivery.control.activatetracking') || Yii::$app->user->can('delivery.control.deactivatetracking'),
            ],
            [
                'attribute' => 'auto_list_generation',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return Label::widget([
                        'label' => $model->auto_list_generation ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->auto_list_generation ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'created_at',
                'enableSorting' => false,
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/delivery/control/edit', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('delivery.control.edit');
                        }
                    ],

                    [
                        'label' => Yii::t('common', 'Активировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/delivery/control/activate', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('delivery.control.activate') && !$model->active;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Деактивировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/delivery/control/deactivate', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('delivery.control.deactivate') && $model->active;
                        }
                    ],

                    [
                        'label' => Yii::t('common', 'Разрешить автоматическую отправку'),
                        'url' => function ($model) {
                            return Url::toRoute(['/delivery/control/activate-auto-sending', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('delivery.control.activateautosending') && !$model->auto_sending;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Запретить автоматическую отправку'),
                        'url' => function ($model) {
                            return Url::toRoute(['/delivery/control/deactivate-auto-sending', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('delivery.control.deactivateautosending') && $model->auto_sending;
                        }
                    ],

                    [
                        'label' => Yii::t('common', 'Разрешить получение статусов по АПИ'),
                        'url' => function ($model) {
                            return Url::toRoute(['/delivery/control/activate-tracking', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('delivery.control.activatetracking') && !$model->can_tracking && $model->apiClass && $model->apiClass->can_tracking;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Запретить получение статусов по АПИ'),
                        'url' => function ($model) {
                            return Url::toRoute(['/delivery/control/deactivate-tracking', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('delivery.control.deactivatetracking') && $model->can_tracking && $model->apiClass && $model->apiClass->can_tracking;
                        }
                    ],

                    [
                        'label' => Yii::t('common', 'Разрешить автоматическую генерацию листов'),
                        'url' => function ($model) {
                            return Url::toRoute(['/delivery/control/activate-auto-list-generation', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('delivery.control.activateautolistgeneration') && !$model->auto_list_generation;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Запретить автоматическую генерацию листов'),
                        'url' => function ($model) {
                            return Url::toRoute(['/delivery/control/deactivate-auto-list-generation', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('delivery.control.deactivateautolistgeneration') && $model->auto_list_generation;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Разрешить забор заказов по нашему API'),
                        'url' => function ($model) {
                            return Url::toRoute(['/delivery/control/activate-our-api', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('delivery.control.activateourapi') && !$model->our_api;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Запретить забор заказов по нашему API'),
                        'url' => function ($model) {
                            return Url::toRoute(['/delivery/control/deactivate-our-api', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('delivery.control.deactivateourapi') && $model->our_api;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Разрешить использование в брокере'),
                        'url' => function ($model) {
                            return Url::toRoute(['/delivery/control/activate-brokerage', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('delivery.control.edit') && !$model->brokerage_active;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Запретить использование в брокере'),
                        'url' => function ($model) {
                            return Url::toRoute(['/delivery/control/deactivate-brokerage', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('delivery.control.edit') && $model->brokerage_active;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Разрешить экспресс заказы'),
                        'url' => function ($model) {
                            return Url::toRoute(['/delivery/control/activate-express', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('delivery.control.activateexpress') && !$model->express_delivery;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Запретить экспресс заказы'),
                        'url' => function ($model) {
                            return Url::toRoute(['/delivery/control/deactivate-express', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('delivery.control.deactivateexpress') && $model->express_delivery;
                        }
                    ],
                    /*[
                        'label' => Yii::t('common', 'Данные для расчета расходов за доставку'),
                        'url' => function ($model) {
                            return Url::toRoute(['/delivery/control/edit-charges-values', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('delivery.control.editchargesvalues') && $model->chargesCalculatorModel && $model->chargesCalculatorModel->active;
                        }
                    ],*/
                    [
                        'label' => Yii::t('common', 'Контракты'),
                        'url' => function ($model) {
                            return Url::toRoute(['/delivery/control/contracts', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('delivery.control.contracts');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Логи'),
                        'url' => function ($model) {
                            return Url::toRoute(['/delivery/control/logs', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('delivery.control.logs');
                        }
                    ],
                    [
                        'can' => function ($model) {
                            return (Yii::$app->user->can('delivery.control.edit')
                                    || (Yii::$app->user->can('delivery.control.activate') && !$model->active)
                                    || (Yii::$app->user->can('delivery.control.deactivate') && $model->active)
                                    || (Yii::$app->user->can('delivery.control.activateautosending') && !$model->auto_sending)
                                    || (Yii::$app->user->can('delivery.control.deactivateautosending') && $model->auto_sending)
                                    || (Yii::$app->user->can('delivery.control.activateautolistgeneration') && !$model->auto_list_generation)
                                    || (Yii::$app->user->can('delivery.control.deactivateautolistgeneration') && $model->auto_list_generation)
                                    || (Yii::$app->user->can('delivery.control.activateourapi') && !$model->our_api)
                                    || (Yii::$app->user->can('delivery.control.deactivateourapi') && $model->our_api)
                                    || (Yii::$app->user->can('delivery.control.editchargesvalues') && $model->our_api)
                                    || (Yii::$app->user->can('delivery.control.logs'))
                                )
                                || Yii::$app->user->can('delivery.control.delete');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить'),
                        'url' => function () {
                            return '#';
                        },
                        'style' => 'confirm-delete-link',
                        'attributes' => function ($model) {
                            return [
                                'data-href' => Url::toRoute(['/delivery/control/delete', 'id' => $model->id]),
                            ];
                        },
                        'can' => function () {
                            return Yii::$app->user->can('delivery.control.delete');
                        }
                    ],
                ]
            ]
        ],
    ]),
    'footer' => (Yii::$app->user->can('delivery.control.add') ?
            ButtonLink::widget([
                'label' => Yii::t('common', 'Добавить службу доставки'),
                'url' => Url::toRoute('/delivery/control/add'),
                'style' => ButtonLink::STYLE_SUCCESS,
                'size' => ButtonLink::SIZE_SMALL,
            ]) : ''),
]) ?>

<?= ModalConfirmDelete::widget() ?>
