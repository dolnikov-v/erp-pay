<?php
use app\components\widgets\ActiveForm;
use app\modules\delivery\assets\ChangerTimingAsset;
use app\modules\delivery\assets\EmailListAsset;
use app\widgets\ChangeableList;
use yii\helpers\Url;
use yii\helpers\Html;

/** @var \app\modules\delivery\models\Delivery $model */
/** @var array $deliveryNotProductsIds */
/** @var array $deliveryPackages */
/** @var app\modules\delivery\models\DeliveryZipcodes[] $modelsDeliveryZipcodes */
/** @var bool $isView */
/** @var app\modules\delivery\models\DeliveryNotRegions $modelNotRegions */
/** @var array $deliveryNotZipcodes */
/** @var array $deliveryNotCities */

EmailListAsset::register($this);
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute(['edit-brokerage', 'id' => $model->id]),
    'enableClientValidation' => true
]); ?>

<div class="row">
    <div class="col-lg-4">
        <?= $form->field($model, 'brokerage_active')->checkboxCustom([
            'value' => 1,
            'uncheckedValue' => 0,
            'checked' => $model->brokerage_active,
        ]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-4">
        <?= $form->field($model, 'brokerage_min_order_price')->textInput() ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'brokerage_daily_minimum')->textInput() ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'brokerage_daily_maximum')->textInput() ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-4">
        <?= $form->field($model, 'non_delivery_products')->select2ListMultiple(
            \app\models\Product::find()->collection(),
            [
                'value' => $deliveryNotProductsIds
            ]); ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'delivery_packages')->select2ListMultiple(
            \app\modules\order\models\Lead::PACKAGE_LABELS,
            [
                'value' => $deliveryPackages
            ]); ?>
    </div>
    <div class="col-lg-4 no-margin-custom-checkbox">
        <?= $form->field($model, 'express_delivery')->checkboxCustom([
            'value' => 1,
            'uncheckedValue' => 0,
            'checked' => $model->express_delivery,
        ]) ?>
    </div>
</div>

<div class="row margin-top-20">
    <?= $this->render('_edit-zipcodes', [
        'model' => $model,
        'modelsDeliveryZipcodes' => $modelsDeliveryZipcodes,
        'form' => $form,
    ]) ?>
</div>


<div class="row">
    <div class="col-lg-10">
        <h4><?= Yii::t('common', 'Регионы вне зоны покрытия') ?></h4>
        <?= Yii::t('common', 'Список почтовых индексов и городов через запятую куда не доставляет служба доставки') ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-10">
        <?= $form->field($modelNotRegions, 'region')->textarea([
            'id' => false,
            'name' => Html::getInputName($modelNotRegions, 'zipcode'),
            'value' => $deliveryNotZipcodes ? implode(',', $deliveryNotZipcodes) : ''
        ])->label(Yii::t('common', 'Индекс')) ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-10">
        <?= $form->field($modelNotRegions, 'region')->textarea([
            'id' => false,
            'name' => Html::getInputName($modelNotRegions, 'city'),
            'value' => $deliveryNotCities ? implode(',', $deliveryNotCities) : ''
        ])->label(Yii::t('common', 'Город')) ?>
    </div>
</div>

<div class="row-hr">
    <hr/>
</div>

<div class="row">
    <div class="col-lg-12">
        <?php if (!$isView): ?>
            <?= $form->submit(Yii::t('common', 'Сохранить службу доставки')) ?>
        <?php endif; ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
