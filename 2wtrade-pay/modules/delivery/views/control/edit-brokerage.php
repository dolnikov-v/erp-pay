<?php
use app\widgets\Panel;
use yii\helpers\Url;
use app\modules\delivery\widgets\DeleteZipCodesModal;
use yii\web\View;

/** @var \yii\web\View $this */
/** @var \app\modules\delivery\models\Delivery $model */
/** @var array $deliveryNotProductsIds */
/** @var array $deliveryPackages */
/** @var app\modules\delivery\models\DeliveryZipcodes[] $modelsDeliveryZipcodes */
/** @var app\modules\delivery\models\DeliveryNotRegions $modelNotRegions */
/** @var array $deliveryNotZipcodes */
/** @var array $deliveryNotCities */

$this->title = Yii::t('common', 'Редактирование службы доставки');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Службы доставок'), 'url' => '#'];
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('common', 'Управление'),
    'url' => Url::toRoute('/delivery/control/index')
];
$this->params['breadcrumbs'][] = ['label' => $model->name];

$this->registerJs("var I18n = {};", View::POS_HEAD);
$this->registerJs("I18n['Удаление индексов завершено.'] = '" . Yii::t('common', 'Удаление индексов завершено.') . "';", View::POS_HEAD);
$this->registerJs("I18n['Не удалось удалить индексы.'] = '" . Yii::t('common', 'Не удалось удалить индексы.') . "';", View::POS_HEAD);
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Брокер'),
    'nav' => new \app\modules\delivery\widgets\DeliveryLinkNav([
        'delivery' => $model
    ]),
    'content' => $this->render('_edit-form-brokerage', [
        'model' => $model,
        'deliveryNotProductsIds' => $deliveryNotProductsIds,
        'deliveryPackages' => $deliveryPackages,
        'modelsDeliveryZipcodes' => $modelsDeliveryZipcodes,
        'isView' => false,
        'modelNotRegions' => $modelNotRegions,
        'deliveryNotZipcodes' => $deliveryNotZipcodes,
        'deliveryNotCities' => $deliveryNotCities,
    ])
]) ?>

<?= DeleteZipCodesModal::widget([
    'delivery_id' => $model->id
]); ?>