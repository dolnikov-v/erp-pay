<?php
use app\widgets\Panel;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \app\modules\delivery\models\Delivery $model */
/** @var \app\modules\delivery\models\DeliveryTiming $timings */
/** @var array $apiClasses */
/** @var array $workflows */

/** @var app\modules\delivery\models\DeliveryContacts[] $modelsDeliveryContacts */
/** @var app\modules\delivery\models\DeliverySku[] $modelsDeliverySku */
/** @var array $packagers */
/** @var \app\models\Product[] $products */

$this->title = Yii::t('common', 'Редактирование службы доставки');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Службы доставок'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Управление'), 'url' => Url::toRoute('/delivery/control/index')];
$this->params['breadcrumbs'][] = ['label' => $model->name];

?>

<?= Panel::widget([
    'title' => Yii::t('common', 'SKU продуктов'),
    'nav' => new \app\modules\delivery\widgets\DeliveryLinkNav([
        'delivery' => $model
    ]),
    'content' => $this->render('_edit-products', [
        'model' => $model,
        'timings' => $timings,
        'apiClasses' => $apiClasses,
        'workflows' => $workflows,
        'modelsDeliveryContacts' => $modelsDeliveryContacts,
        'modelsDeliverySku' => $modelsDeliverySku,
        'packagers' => $packagers,
        'products' => $products,
        'isView' => false
    ])
]) ?>