<?php

use app\widgets\Log;
use yii\helpers\Url;

/**
 * @var \app\models\logs\TableLog[] $changeLog
 * @var integer $deliveryID
 */

$this->title = Yii::t('common', 'Лог изменений');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Службы доставки'), 'url' => Url::toRoute(['index'])];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Контракты'), 'url' => Url::toRoute(['contracts', 'id' => $deliveryID])];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Log::widget([
    'data' => $changeLog,
]); ?>