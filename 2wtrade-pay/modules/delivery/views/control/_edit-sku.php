<?php
use app\modules\delivery\models\DeliverySku;
use app\modules\delivery\assets\SkuAsset;

/** @var app\modules\delivery\models\Delivery $model */
/** @var app\modules\delivery\models\DeliverySku[] $modelsDeliverySku */
/** @var app\components\widgets\ActiveForm $form */
/** @var app\models\Product[] $products */

SkuAsset::register($this);
?>

<div class="col-lg-12 margin-bottom-20 group-sku-group">
    <?php if (empty($modelsDeliverySku)): ?>
        <?= $this->render('_edit-form-sku', [
            'form' => $form,
            'model' => new DeliverySku(),
            'products' => $products,
        ]) ?>
    <?php else: ?>
        <?php foreach ($modelsDeliverySku as $key => $modelDeliverySku): ?>
            <?= $this->render('_edit-form-sku', [
                'form' => $form,
                'model' => $modelDeliverySku,
                'products' => $products,
            ]) ?>
        <?php endforeach; ?>
    <?php endif; ?>
</div>

