<?php

use app\components\widgets\ActiveForm;
use app\modules\delivery\assets\EmailListAsset;
use yii\helpers\Url;

/** @var \app\modules\delivery\models\Delivery $model */
/** @var \app\modules\delivery\models\DeliveryTiming $timings */
/** @var array $workflows */
/** @var app\modules\delivery\models\DeliveryContacts[] $modelsDeliveryContacts */
/** @var array $packagers */
/** @var app\modules\delivery\models\DeliveryAutoChangeReason[] $modelsDeliveryAutoChangeReason */
/** @var array $otherDeliveries */
/** @var bool $isView */

EmailListAsset::register($this);
?>

<?php $form = ActiveForm::begin([
    'action' => $model->isNewRecord ? Url::toRoute(['add']) : Url::toRoute(['edit', 'id' => $model->id]),
    'enableClientValidation' => true
]); ?>

<div class="row">
    <div class="col-lg-8">
        <?= $form->field($model, 'legal_name')->textInput(['disabled' => $isView]) ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'group_name')->textInput(['disabled' => $isView]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-4">
        <?= $form->field($model, 'name')->textInput(['disabled' => $isView]) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'char_code')->textInput(['disabled' => $isView]) ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'url')->textInput(['disabled' => $isView]) ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-6">
        <?= $form->field($model, 'address')->textarea(['disabled' => $isView]) ?>
    </div>
    <div class="col-lg-6">
        <?= $form->field($model, 'comment')->textarea(['disabled' => $isView]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-3">
        <?= $form->field($model, 'workflow_id')->select2List($workflows, [
            'prompt' => '—',
            'disabled' => $isView
        ]) ?>
    </div>
    <div class="col-lg-3 no-margin-custom-checkbox">
        <?= $form->field($model, 'our_api')->checkboxCustom([
            'value' => 1,
            'uncheckedValue' => 0,
            'checked' => $model->our_api,
            'disabled' => $isView
        ]) ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-4">
        <?= $form->field($model, 'packager_id')->select2List($packagers['all'], [
            'prompt' => Yii::t('common', 'Не требуется'),
            'disabledItems' => $packagers['inactive'],
            'disabled' => $isView
        ]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-3">
        <?= $form->field($model, 'packager_send_label')->checkboxCustom([
            'id' => false,
            'checked' => $model->packager_send_label,
            'value' => 1,
            'uncheckedValue' => 0,
        ]); ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'packager_send_invoice')->checkboxCustom([
            'id' => false,
            'checked' => $model->packager_send_invoice,
            'value' => 1,
            'uncheckedValue' => 0,
        ]); ?>
    </div>
</div>

<div class="row margin-top-30">
    <div class="col-lg-12 ">
        <h4><?= Yii::t('common', 'Автоматическая отправка отказов в другую курьерскую службу') ?></h4>
    </div>
</div>

<div class="row">
    <div class="col-lg-3 no-margin-custom-checkbox">
        <?= $form->field($model, 'auto_change_on_reject')->checkboxCustom([
            'value' => 1,
            'uncheckedValue' => 0,
            'checked' => $model->isAutoChangeOnReject(),
            'disabled' => $isView
        ]) ?>
    </div>
</div>

<div class="row margin-top-20">
    <?= $this->render('_edit-auto-change-reason', [
        'model' => $model,
        'modelsDeliveryAutoChangeReason' => $modelsDeliveryAutoChangeReason,
        'form' => $form,
        'otherDeliveries' => $otherDeliveries,
        'isView' => $isView
    ]) ?>
</div>

<div class="row margin-top-20">
    <?= $this->render('_edit-contacts', [
        'model' => $model,
        'modelsDeliveryContacts' => $modelsDeliveryContacts,
        'form' => $form,
        'isView' => $isView
    ]) ?>
</div>

<div class="row-hr">
    <hr/>
</div>

<div class="row margin-top-20">
    <div class="col-lg-12">
        <?php if (!$isView): ?>
            <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить службу доставки') : Yii::t('common',
                'Сохранить службу доставки')) ?>
        <?php endif; ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')) ?>

        <div class="pull-right no-margin-custom-checkbox">
            <?= $form->field($model, 'is_keep')->checkboxCustom([
                'value' => 1,
                'uncheckedValue' => 0,
                'checked' => $model->is_keep,
                'disabled' => $isView
            ]) ?>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>
