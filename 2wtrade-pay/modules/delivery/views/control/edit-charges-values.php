<?php

use app\widgets\Panel;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 * @var \app\modules\delivery\models\Delivery $delivery
 * @var \app\modules\delivery\components\charges\ChargesCalculatorAbstract $chargesCalculator
 */

$this->title = Yii::t('common', '{name}: расходы на доставку', ['name' => $delivery->name]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Службы доставок'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Управление'), 'url' => Url::toRoute('/delivery/control/index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];

?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Шаблон по расчету расходов на доставку'),
    'content' => $this->render('_edit-form-charges-values', ['delivery' => $delivery, 'chargesCalculator' => $chargesCalculator]),
]) ?>
