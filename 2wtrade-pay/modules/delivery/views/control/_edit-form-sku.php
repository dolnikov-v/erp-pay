<?php
use app\helpers\WbIcon;
use app\widgets\Button;
use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\components\widgets\ActiveForm $form */
/** @var app\modules\delivery\models\DeliverySku $model */
/** @var app\models\Product $products */
?>

<div class="row row-delivery-sku">

    <?= Html::hiddenInput(Html::getInputName($model, 'id') . '[]', $model->id) ?>

    <div class="col-lg-4">
        <?= $form->field($model, 'product_id')->select2List($products,
            [
                'length' => 1,
                'prompt' => '-',
                'id' => false,
                'name' => Html::getInputName($model, 'product_id') . '[]',
            ]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'sku')->textInput([
            'id' => false,
            'name' => Html::getInputName($model, 'sku') . '[]',
        ]) ?>
    </div>
    <div class="col-lg-2">
        <div class="form-group">
            <div class="control-label">
                <label>&nbsp;</label>
            </div>
            <?= Button::widget([
                'type' => 'button',
                'icon' => WbIcon::MINUS,
                'style' => Button::STYLE_DANGER . ' btn-delivery-sku btn-delivery-sku-minus',
            ]) ?>

            <?= Button::widget([
                'type' => 'button',
                'icon' => WbIcon::PLUS,
                'style' => Button::STYLE_SUCCESS . ' btn-delivery-sku btn-delivery-sku-plus',
            ]) ?>

        </div>
    </div>

</div>


