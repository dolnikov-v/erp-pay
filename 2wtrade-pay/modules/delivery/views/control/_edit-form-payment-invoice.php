<?php

use app\components\widgets\ActiveForm;
use app\modules\delivery\assets\ChangerTimingAsset;
use app\modules\delivery\assets\EmailListAsset;
use app\modules\delivery\assets\DeliveryInvoiceOptionsAsset;
use app\widgets\ChangeableList;
use yii\helpers\Url;
use yii\helpers\Html;

/** @var \app\modules\delivery\models\Delivery $model */
/** @var \app\modules\delivery\models\DeliveryTiming $timings */
/** @var bool $isView */

EmailListAsset::register($this);
DeliveryInvoiceOptionsAsset::register($this);
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute(['edit-payment-invoice', 'id' => $model->id]),
    'enableClientValidation' => false
]); ?>

<div class="row margin-top-20">
    <div class="col-lg-12">
        <h4><?= Yii::t('common', 'Напоминания о платежах') ?></h4>
    </div>
</div>

<div class="row">
    <div class="col-lg-4">
        <?= $form->field($model, 'payment_reminder_schedule')->select2List($model::getReminderScheduleLabels()) ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'payment_reminder_email_to')->textInput() ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'payment_reminder_email_reply_to')->textInput() ?>
    </div>
</div>
<div class="invoices">
    <div class="row">
        <div class="col-lg-12">
            <h4><?= Yii::t('common', 'Инвойсирование') ?></h4>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6 col-lg-3">
            <?= $form->field($model, 'invoice_currency_id')->select2List(\app\models\Currency::find()->collection(), ['length' => false,]) ?>
        </div>
        <div class="col-sm-6 col-lg-3">
            <?= $form->field($model, 'invoice_round_precision')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-5">
            <div class="invoice-email-list">
                <?= ChangeableList::widget([
                    'label' => '<div class="row"><div class="col-sm-10 col-md-10 col-lg-11"><div class="row"><div class="col-sm-6">Диапозон дней отчета (00:00-23:59)</div><div class="col-sm-6">Число и время отправки (GMT+0)</div></div></div></div>',
                    'template' => function () use ($model, $form) {
                        return $this->render('_edit-invoice-options', [
                            'form' => $form,
                            'model' => (new \app\modules\delivery\models\DeliveryInvoiceOptions()),
                        ]);
                    },
                    'items' => function () use ($model, $form) {
                        $data = [];
                        $options = $model->deliveryInvoiceOptions;
                        foreach ($options as $option) {
                            $data[] = $this->render('_edit-invoice-options', [
                                'form' => $form,
                                'model' => $option,
                            ]);
                        }
                        return $data;
                    },
                    'callbackJsFuncAfterAddRow' => 'InvoiceOptions.callbackClearInput'
                ]) ?>
            </div>
        </div>

        <div class="col-lg-6">
            <?= $form->field($model, 'auto_send_invoice')->checkboxCustom([
                'value' => 1,
                'checked' => $model->auto_send_invoice,
                'uncheckedValue' => 0,
            ]) ?>
            <div class="invoice-email-list">
                <?= ChangeableList::widget([
                    'items' => function () use ($model, $form) {
                        $items = [];
                        foreach ($model->invoiceEmailList as $email) {
                            $items[] = Html::tag('div', $form->field($model, 'invoice_email_list', ['enableClientValidation' => false])
                                ->textInput([
                                    'name' => Html::getInputName($model, 'invoice_email_list') . '[]',
                                    'value' => $email,
                                    'placeholder' => Yii::t('common', 'e-mail'),
                                ])
                                ->label(false), ['class' => 'col-lg-4']);
                        }
                        return $items;
                    },
                    'template' => Html::tag('div', $form->field($model, 'invoice_email_list', ['enableClientValidation' => false])
                        ->textInput([
                            'name' => Html::getInputName($model, 'invoice_email_list') . '[]',
                            'value' => '',
                            'placeholder' => Yii::t('common', 'e-mail')
                        ])
                        ->label(false), ['class' => 'col-lg-8']),
                    'label' => Yii::t('common', 'Список e-mail для отправки инвойсов'),
                    'callbackJsFuncAfterAddRow' => 'EmailList.callbackClearInput'
                ]) ?>
            </div>
        </div>
    </div>
</div>

<div class="row-hr">
    <hr/>
</div>

<div class="row ">
    <div class="col-lg-12">
        <?php if (!$isView): ?>
            <?= $form->submit(Yii::t('common', 'Сохранить службу доставки')) ?>
        <?php endif; ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
