<?php
use app\helpers\WbIcon;
use app\modules\delivery\models\DeliveryContractTime;
use app\widgets\Button;
use app\modules\delivery\assets\ManageContractTimesGroupAsset;

/** @var app\modules\delivery\models\DeliveryContract $model */
/** @var app\modules\delivery\models\DeliveryContractTime[] $modelsDeliveryContractTimes */
/** @var app\components\widgets\ActiveForm $form */
/** @var bool $onlyView */

ManageContractTimesGroupAsset::register($this);
?>

    <div class="col-lg-12 margin-bottom-20 group-contract-times-group">
        <?php if (empty($modelsDeliveryContractTimes)): ?>
            <?= $this->render('_edit-form-contract-times', [
                'form' => $form,
                'model' => new DeliveryContractTime(),
            ]) ?>
        <?php else: ?>
            <?php foreach ($modelsDeliveryContractTimes as $key => $modelsDeliveryContractTime): ?>
                <?= $this->render('_edit-form-contract-times', [
                    'form' => $form,
                    'model' => $modelsDeliveryContractTime,
                    'cnt' => $key
                ]) ?>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>

<?php if (!$onlyView) : ?>
    <div class="col-lg-12 margin-bottom-20">
        <?= Button::widget([
            'type' => 'button',
            'icon' => WbIcon::PLUS,
            'style' => Button::STYLE_SUCCESS . ' btn-delivery-contract-times btn-delivery-contract-times-plus',
        ]) ?>
    </div>
<?php endif; ?>