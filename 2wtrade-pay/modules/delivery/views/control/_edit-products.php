<?php
use app\components\widgets\ActiveForm;
use app\modules\delivery\assets\EmailListAsset;
use yii\helpers\Url;

/** @var \app\modules\delivery\models\Delivery $model */
/** @var app\modules\delivery\models\DeliverySku[] $modelsDeliverySku */
/** @var \app\models\Product[] $products */
/** @var bool $isView */

EmailListAsset::register($this);
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute(['edit-products', 'id' => $model->id]),
    'enableClientValidation' => true
]); ?>

<div class="row">
    <?= $this->render('_edit-sku', [
        'model' => $model,
        'modelsDeliverySku' => $modelsDeliverySku,
        'form' => $form,
        'products' => $products,
    ]) ?>
</div>

<div class="row-hr">
    <hr/>
</div>

<div class="row margin-top-20">
    <div class="col-lg-12">
        <?= $form->submit(Yii::t('common', 'Сохранить службу доставки')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
