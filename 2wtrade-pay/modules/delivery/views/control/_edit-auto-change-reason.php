<?php
use app\modules\delivery\models\DeliveryAutoChangeReason;
use app\modules\delivery\assets\AutoChangeReasonAsset;

/** @var app\modules\delivery\models\Delivery $model */
/** @var app\components\widgets\ActiveForm $form */
/** @var app\modules\delivery\models\DeliveryAutoChangeReason[] $modelsDeliveryAutoChangeReason */
/** @var array $otherDeliveries */
/** @var bool $isView */

AutoChangeReasonAsset::register($this);
?>

<div class="col-lg-12 margin-bottom-20 group-auto-change-reason-group">
    <?php if (empty($modelsDeliveryAutoChangeReason)): ?>
        <?= $this->render('_edit-form-auto-change-reason', [
            'form' => $form,
            'model' => new DeliveryAutoChangeReason(),
            'otherDeliveries' => $otherDeliveries,
            'isView' => $isView
        ]) ?>
    <?php else: ?>
        <?php foreach ($modelsDeliveryAutoChangeReason as $key => $modelDeliveryAutoChangeReason): ?>
            <?= $this->render('_edit-form-auto-change-reason', [
                'form' => $form,
                'model' => $modelDeliveryAutoChangeReason,
                'otherDeliveries' => $otherDeliveries,
                'isView' => $isView
            ]) ?>
        <?php endforeach; ?>
    <?php endif; ?>
</div>

