<?php
use app\widgets\Panel;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \app\modules\delivery\models\Delivery $model */
/** @var \app\modules\delivery\models\DeliveryTiming $timings */
/** @var array $apiClasses */
/** @var array $workflows */
/** @var app\modules\delivery\models\DeliveryZipcodes[] $modelsDeliveryZipcodes */
/** @var app\modules\delivery\models\DeliveryContacts[] $modelsDeliveryContacts */
/** @var app\modules\delivery\models\DeliverySku[] $modelsDeliverySku */
/** @var array $packagers */
/** @var array $deliveryNotProductsIds */
/** @var array $deliveryPackages */
/** @var \app\models\Product[] $products */
/** @var app\modules\delivery\models\DeliveryAutoChangeReason[] $modelsDeliveryAutoChangeReason */
/** @var array $otherDeliveries */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление службы доставки') : Yii::t('common', 'Редактирование службы доставки');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Службы доставок'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Управление'), 'url' => Url::toRoute('/delivery/control/index')];
$this->params['breadcrumbs'][] = ['label' => $model->name];

?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Служба доставки'),
    'nav' => new \app\modules\delivery\widgets\DeliveryLinkNav([
        'delivery' => $model
    ]),
    'content' => $this->render('_edit-form', [
        'model' => $model,
        'apiClasses' => $apiClasses,
        'workflows' => $workflows,
        'timings' => $timings,
        'deliveryNotProductsIds' => $deliveryNotProductsIds,
        'deliveryPackages' => $deliveryPackages,
        'modelsDeliveryZipcodes' => $modelsDeliveryZipcodes,
        'modelsDeliveryContacts' => $modelsDeliveryContacts,
        'modelsDeliverySku' => $modelsDeliverySku,
        'packagers' => $packagers,
        'products' => $products,
        'modelsDeliveryAutoChangeReason' => $modelsDeliveryAutoChangeReason,
        'otherDeliveries' => $otherDeliveries,
        'isView' => true
    ])
]) ?>