<?php

use app\components\widgets\ActiveForm;
use app\modules\delivery\assets\ChangerTimingAsset;
use app\modules\delivery\assets\EmailListAsset;
use app\modules\delivery\widgets\EmailList;
use app\modules\order\components\ExcelBuilder;
use yii\helpers\Url;

/** @var \app\modules\delivery\models\Delivery $model */
/** @var \app\modules\delivery\models\DeliveryTiming $timing */
/** @var bool $isView */
/** @var \app\modules\salary\models\Person $lawyer */

ChangerTimingAsset::register($this);
EmailListAsset::register($this);
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute(['edit-pretension', 'id' => $model->id]),
    'enableClientValidation' => true
]); ?>

<div class="row">
    <div class="col-lg-4">
        <?= $form->field($model, 'send_to_check_undelivery')->checkboxCustom([
            'value' => 1,
            'checked' => $model->send_to_check_undelivery,
            'uncheckedValue' => 0,
            'disabled' => $isView
        ]) ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'auto_pretension_generation')->checkboxCustom([
            'value' => 1,
            'checked' => $model->auto_pretension_generation,
            'uncheckedValue' => 0,
            'disabled' => $isView
        ]) ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'auto_pretension_send')->checkboxCustom([
            'value' => 1,
            'checked' => $model->auto_pretension_send,
            'uncheckedValue' => 0,
            'disabled' => $isView
        ]) ?>
    </div>
</div>

<div class="row margin-top-20">
    <div class="col-lg-12">
        <h4><?= Yii::t('common', 'Время отправки претензии') ?></h4>
    </div>
</div>

<?= $this->render('_edit-timing', ['model' => $timing, 'showButtons' => false, 'showTimingTo' => false]) ?>


<div class="row margin-top-20">

    <p>
        <?= Yii::t('common', 'Email территориального менеджера') ?>: <?= $model->country->curatorUser->email ?? '-' ?>
        <br/ >
        <i><?= Yii::t('common', 'Территориальный менеджер задается в настройках Справочники / Страны / Редактирование / Ответственный куратор') ?></i>
    </p>

    <p>
        <?= Yii::t('common', 'Имя территориального менеджера') ?>: <?= $model->country->curatorUser->fullname ?? '-' ?>
        <br/ >
        <i><?= Yii::t('common', 'Имя территориального менеджера вводится в настройках профиля') ?></i>
    </p>

    <p>
        <?= Yii::t('common', 'Email юриста') ?>: <?= $lawyer->corporate_email ?? '-' ?>
        <br/ >
        <i><?= Yii::t('common', 'Email сотрудника в Зарплатном проекте в Московском офисе с должностью Юрист') ?></i>
    </p>

    <p>
        <?= Yii::t('common', 'Email курьерской службы') ?>:
        <?php foreach ($model->deliveryContacts as $contact) : ?>
            <?= $contact->email ?>
        <?php endforeach; ?>
        <br/ >
        <i><?= Yii::t('common', 'Задаются во вкладке Основная информация в разделе Ответственные лица') ?></i>
    </p>
</div>

<div class="row">
    <div class="col-lg-12">
        <h3><?= Yii::t('common', 'Тексты претензий') ?></h3>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <h4><?= Yii::t('common', '2WTrade: clients are still waiting') ?></h4>
    </div>
</div>

<div class="table-bordered padding-10"><?= $this->render('@app/mail/pretension/clients-still-waiting/mail.php', ['delivery' => $model]) ?></div>

<div class="row margin-top-10">
    <div class="col-lg-12">
        <h4><?= Yii::t('common', '2WTrade: incorrect statuses in report') ?></h4>
    </div>
</div>

<div class="table-bordered padding-10"><?= $this->render('@app/mail/pretension/incorrect-statuses/mail.php', ['delivery' => $model]) ?></div>

<div class="row margin-top-10">
    <div class="col-lg-12">
        <h4><?= Yii::t('common', '2WTrade: changed statuses of orders') ?></h4>
    </div>
</div>

<div class="table-bordered padding-10"><?= $this->render('@app/mail/pretension/changed-statuses/mail.php', ['delivery' => $model]) ?></div>


<div class="row-hr">
    <hr/>
</div>

<div class="row margin-top-20">
    <div class="col-lg-12">
        <?php if (!$isView): ?>
            <?= $form->submit(Yii::t('common', 'Сохранить службу доставки')) ?>
        <?php endif; ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
