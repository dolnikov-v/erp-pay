<?php
use app\components\widgets\ActiveForm;
use app\modules\delivery\assets\ChangerTimingAsset;
use app\modules\delivery\assets\EmailListAsset;
use app\modules\delivery\models\DeliveryTiming;
use yii\helpers\Url;

/** @var \app\modules\delivery\models\Delivery $model */
/** @var \app\modules\delivery\models\DeliveryTiming $timings */
/** @var bool $isView */

ChangerTimingAsset::register($this);
EmailListAsset::register($this);
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute(['edit-timings', 'id' => $model->id]),
    'enableClientValidation' => true
]); ?>

<div class="row">
    <div class="col-lg-3">
        <?= $form->field($model, 'delivered_from')->textInput() ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'delivered_to')->textInput() ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <h4><?= Yii::t('common', 'Время отправки') ?></h4>
    </div>
</div>

<div id="row_delivery_timings">
    <?php foreach ($timings as $timing): ?>
        <?= $this->render('_edit-timing', ['model' => $timing]) ?>
    <?php endforeach; ?>
</div>

<?= $this->render('_edit-timing', [
    'model' => new DeliveryTiming(),
]) ?>

<?= $this->render('_edit-delivery-interval', [
    'model' => $model,
    'form' => $form,
]) ?>

<div class="row-hr">
    <hr/>
</div>

<div class="row">
    <div class="col-lg-12">
        <?php if (!$isView): ?>
            <?= $form->submit(Yii::t('common', 'Сохранить службу доставки')) ?>
        <?php endif; ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
