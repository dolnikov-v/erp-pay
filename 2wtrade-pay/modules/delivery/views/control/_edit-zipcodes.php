<?php
use app\helpers\WbIcon;
use app\modules\delivery\models\DeliveryZipcodes;
use app\widgets\Button;
use app\modules\delivery\assets\ManageZipcodesGroupAsset;

/** @var app\modules\delivery\models\Delivery $model */
/** @var app\modules\delivery\models\DeliveryZipcodes[] $modelsDeliveryZipcodes */
/** @var app\components\widgets\ActiveForm $form */

ManageZipcodesGroupAsset::register($this);
?>

<div class="col-lg-12 ">
    <h4><?= Yii::t('common', 'Информация о почтовых индексах') ?></h4>
    <?= Yii::t('common', 'Список почтовых индексов через запятую (если пустой - обслуживается вся страна)') ?>
</div>

<div class="col-lg-12 margin-bottom-20 group-zipcodes-group">
    <?php if (empty($modelsDeliveryZipcodes)): ?>
        <?= $this->render('_edit-form-zipcodes', [
            'form' => $form,
            'model' => new DeliveryZipcodes(),
            'cnt' => 0
        ]) ?>
    <?php else: ?>
        <?php foreach ($modelsDeliveryZipcodes as $key => $modelDeliveryZipcodes): ?>
            <?= $this->render('_edit-form-zipcodes', [
                'form' => $form,
                'model' => $modelDeliveryZipcodes,
                'cnt' => $key
            ]) ?>
        <?php endforeach; ?>
    <?php endif; ?>
</div>

<div class="col-lg-12 margin-bottom-20">
    <?= Button::widget([
        'type' => 'button',
        'icon' => WbIcon::PLUS,
        'style' => Button::STYLE_SUCCESS . ' btn-delivery-zipcodes btn-delivery-zipcodes-plus',
    ]) ?>
</div>
