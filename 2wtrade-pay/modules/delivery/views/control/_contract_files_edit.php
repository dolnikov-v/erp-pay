<?php
use \app\modules\delivery\models\DeliveryContractFile;
use app\modules\delivery\assets\DeliveryContractFileAsset;
use app\components\grid\GridView;
use app\components\grid\DateColumn;
use app\components\grid\ActionColumn;
use yii\helpers\Url;
use yii\helpers\Html;
use app\widgets\assets\custom\ModalConfirmDeleteAsset;
use app\widgets\custom\ModalConfirmDelete;

/** @var \app\modules\delivery\models\DeliveryContract $model */
/** @var \yii\data\ActiveDataProvider $dataProviderFiles */
/** @var app\components\widgets\ActiveForm $form */
/** @var bool $onlyView */

DeliveryContractFileAsset::register($this);
ModalConfirmDeleteAsset::register($this);
?>

<div class="row">
    <div class="col-lg-12">
        <h4><?= Yii::t('common', 'Файлы') ?></h4>
    </div>
</div>

<?= GridView::widget([
    'summary' => false,
    'dataProvider' => $dataProviderFiles,
    'columns' => [
        [
            'attribute' => 'file_name',
            'format' => 'html',
            'enableSorting' => false,
            'value' => function ($model) {
                return Html::a($model['file_name'],
                    Url::toRoute(['/media/delivery-contract/file/' . $model->id]),
                    ['target' => '_blank']);
            }
        ],
        [
            'class' => DateColumn::className(),
            'attribute' => 'created_at',
            'enableSorting' => false,
        ],
        [
            'class' => ActionColumn::className(),
            'items' => [
                [
                    'label' => Yii::t('common', 'Удалить'),
                    'url' => function () {
                        return '#';
                    },
                    'style' => 'confirm-delete-link',
                    'attributes' => function ($itemFile) {
                        return [
                            'data-href' => Url::toRoute([
                                '/delivery/control/contract-file-delete',
                                'id' => $itemFile->id
                            ]),
                        ];
                    },
                    'can' => function () {
                        return Yii::$app->user->can('delivery.control.contractfiledelete');
                    }
                ],
            ]
        ]
    ],
]);
?>

<?= ModalConfirmDelete::widget() ?>

<?php if (!$onlyView): ?>
    <div class="row">
        <div class="col-lg-12 group-contract-file">
            <?= $this->render('_contract_file_edit-form', [
                'model' => new DeliveryContractFile(),
                'modelContract' => $model,
            ]) ?>
        </div>
    </div>
<?php endif; ?>
