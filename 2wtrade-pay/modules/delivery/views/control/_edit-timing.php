<?php
use app\modules\delivery\widgets\ChangerTiming;

/** @var \app\modules\delivery\models\DeliveryTiming $model */
/** @var bool $showButtons */
/** @var bool $showTimingTo */

$options = [];
$options['model'] = $model;
if (isset($showButtons)) {
    $options['showButtons'] = $showButtons;
}
if (isset($showTimingTo)) {
    $options['showTimingTo'] = $showTimingTo;
}
?>

<div class="row row-delivery-timing">
    <div class="col-lg-12">
        <?= ChangerTiming::widget($options) ?>
    </div>
</div>
