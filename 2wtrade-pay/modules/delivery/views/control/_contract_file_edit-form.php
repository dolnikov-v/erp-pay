<?php
use app\helpers\WbIcon;
use app\widgets\Button;
use yii\helpers\Html;
use app\widgets\InputGroupFile;
use app\widgets\InputText;

/** @var yii\web\View $this */
/** @var app\components\widgets\ActiveForm $form */
/** @var app\modules\delivery\models\DeliveryContractFile $model */
/** @var \app\modules\delivery\models\DeliveryContract $modelContract */
?>

<div class="row row-delivery-contract-file">

    <div class="col-lg-6">
        <?= InputGroupFile::widget([
            'right' => false,
            'input' => InputText::widget([
                'id' => false,
                'type' => 'file',
                'name' => Html::getInputName($model, 'loadFile') . '[]',
            ]),
        ]) ?>
    </div>

    <div class="col-lg-4">
        <div class="form-group">
            <?= Button::widget([
                'type' => 'button',
                'icon' => WbIcon::MINUS,
                'style' => Button::STYLE_DANGER . ' btn-delivery-contract-file-minus',
            ]) ?>

            <?= Button::widget([
                'type' => 'button',
                'icon' => WbIcon::PLUS,
                'style' => Button::STYLE_SUCCESS . ' btn-delivery-contract-file-plus',
            ]) ?>

        </div>
    </div>

</div>


