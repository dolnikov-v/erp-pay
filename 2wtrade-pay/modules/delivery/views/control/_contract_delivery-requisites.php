<?php
/** @var \app\modules\delivery\models\DeliveryContract $model */
/** @var array $requisites */
/** @var \app\modules\catalog\models\RequisiteDelivery[] $allRequisites */
/** @var \yii\bootstrap\ActiveForm $form */

/** @var boolean $onlyView */

use app\widgets\custom\Radio;
use app\widgets\Select2;
use yii\helpers\ArrayHelper;
use app\helpers\WbIcon;
use app\widgets\Button;
use yii\helpers\Html;

$invoiceTPLSCollection = \app\modules\catalog\models\RequisiteDelivery::getInvoiceTPLSCollection();
$tmp = [];
foreach ($allRequisites as $requisite) {
    $tmp[$requisite->id] = $requisite->name . ' (' . ($invoiceTPLSCollection[$requisite->invoice_tpl] ?? '') . ')';
}
$allRequisites = $tmp;
?>

<div class="group-requisites">
    <?php if (!empty($requisites)): ?>

        <?php foreach ($requisites as $requisite): ?>
            <?php /** @var \app\modules\catalog\models\RequisiteDelivery $requisite */ ?>
            <div class="row">
                <div class="col-lg-1 width-50">
                    <?= Radio::widget([
                        'name' => 'DeliveryContract[requisite_delivery_id]',
                        'value' => $requisite->id,
                        'label' => '&nbsp;',
                        'checked' => $model->requisite_delivery_id == $requisite->id,
                        'disabled' => $onlyView
                    ]); ?>
                    <?= Html::hiddenInput('RequisiteDeliveryLink[]', $requisite->id) ?>
                </div>
                <div class="col-lg-1 width-50 padding-10">
                    <?= $requisite->id; ?>
                </div>
                <div class="col-lg-2 padding-10"><?= $requisite->name; ?>
                    (<?= $invoiceTPLSCollection[$requisite->invoice_tpl] ?? ''; ?>)
                </div>
                <div class="col-lg-2 padding-10"><?= $requisite->city; ?></div>
                <div class="col-lg-2 padding-10"><?= $requisite->beneficiary_bank; ?></div>
                <div class="col-lg-2 padding-10"><?= $requisite->bank_account; ?></div>

                <?php if (!$onlyView): ?>
                    <div class="col-lg-2">
                        <?= Button::widget([
                            'type' => 'button',
                            'icon' => WbIcon::MINUS,
                            'style' => Button::STYLE_DANGER . ' btn-delivery-requisites btn-delivery-requisites-minus',
                        ]) ?>
                    </div>
                <?php endif; ?>

            </div>

        <?php endforeach; ?>

    <?php endif; ?>

    <?php if (!$onlyView): ?>
        <div class="row" style="display: none">
            <div class="col-lg-1 width-50">
                <?= Radio::widget([
                    'name' => 'DeliveryContract[requisite_delivery_id]',
                    'value' => 0,
                    'label' => '&nbsp;',
                    'checked' => false,
                    'disabled' => $onlyView
                ]); ?>
                <?= Html::hiddenInput('RequisiteDeliveryLink[]', 0) ?>
            </div>
            <div class="col-lg-1 width-50 padding-10 col-id"></div>
            <div class="col-lg-2 padding-10 col-name"></div>
            <div class="col-lg-2 padding-10"></div>
            <div class="col-lg-2 padding-10"></div>
            <div class="col-lg-2 padding-10"></div>
            <div class="col-lg-2">
                <?= Button::widget([
                    'type' => 'button',
                    'icon' => WbIcon::MINUS,
                    'style' => Button::STYLE_DANGER . ' btn-delivery-requisites btn-delivery-requisites-minus',
                ]) ?>
            </div>
        </div>

    <?php endif; ?>

</div>

<div class="alert alert-warning" role="alert"
     id="no_requisites" <?php if ($requisites): ?> style="display: none" <?php endif; ?>>
    <?= yii::t('common', 'Нет реквизитов добавленных к контракту'); ?>
</div>

<?php if (!empty($allRequisites)): ?>
    <div class="row">
        <div class="col-lg-12 mt15">
            <h4><?= Yii::t('common', 'Добавить реквизит к контракту') ?></h4>
        </div>
    </div>

    <div class="requisites-list-element row">
        <div class="form-group col-lg-2">
            <?= Select2::widget([
                'id' => 'select-delivery-requisites-plus',
                'items' => $allRequisites,
                'disabledItems' => array_keys(ArrayHelper::index($requisites, 'id'))
            ]) ?>
        </div>
        <button class="btn btn-success btn-delivery-requisites-plus" type="button">
            <i class="icon wb-plus"></i>
        </button>
    </div>
<?php else: ?>
    <div class="alert alert-warning" role="alert">
        <?= yii::t('common', 'Реквизиты не обнаружены. Необходимо добавить в систему реквизиты: Справочники->Реквизиты КС'); ?>
    </div>
<?php endif; ?>
