<?php

use yii\helpers\Url;
use app\widgets\Log;

/** @var \app\modules\delivery\models\Delivery $model */

$this->title = Yii::t('common', 'История изменения службы доставки {name}', ['name' => $model->name]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Службы доставки'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Управление'), 'url' => Url::toRoute('index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<div class="panel panel panel-bordered ">
    <ul class="panel nav nav-tabs" role="tablist">
        <li class="active"><a href="#tabChange" aria-controls="tabChange" role="tab" data-toggle="tab">Delivery</a></li>
        <li><a href="#tabDelivery" aria-controls="tabDelivery" role="tab" data-toggle="tab">Contract</a></li>
        <li><a href="#tabSku" aria-controls="tabSku" role="tab" data-toggle="tab">Sku</a></li>
        <li><a href="#tabTiming" aria-controls="tabTiming" role="tab" data-toggle="tab">Timing</a></li>
        <li><a href="#tabZipcodes" aria-controls="tabZipcodes" role="tab" data-toggle="tab">Zipcodes</a></li>
        <li><a href="#tabNotProductsByZip" aria-controls="tabNotProductsByZip" role="tab" data-toggle="tab">NotProductsByZip</a></li>
        <li><a href="#tabNotProducts" aria-controls="tabNotProducts" role="tab" data-toggle="tab">NotProducts</a></li>
        <li><a href="#tabAutoChangeReason" aria-controls="tabAutoChangeReason" role="tab" data-toggle="tab">AutoChangeReason</a></li>
    </ul>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="tabChange">
            <?= Log::widget([
                'title' => Yii::t('common', 'История изменения'),
                'data' => $change,
            ]); ?>
        </div>
        <div role="tabpanel" class="tab-pane" id="tabDelivery">
            <?= Log::widget([
                'data' => $contracts,
                'titleContent' => 'Контракт №{contract.id} ({contract.delivery.name})',
            ]); ?>
        </div>
        <div role="tabpanel" class="tab-pane" id="tabSku">
            <?= Log::widget([
                'data' => $sku,
                'titleContent' => 'Sku №{id}',
                'showLinks' => true
            ]); ?>
        </div>
        <div role="tabpanel" class="tab-pane" id="tabTiming">
            <?= Log::widget([
                'data' => $timing,
                'groupBy' => 'key',
                'templates' => ['revertNum', 'created_at'],
            ]); ?>
        </div>
        <div role="tabpanel" class="tab-pane" id="tabZipcodes">
            <?= Log::widget([
                'data' => $zipcodes,
                'groupBy' => 'key',
            ]); ?>
        </div>
        <div role="tabpanel" class="tab-pane" id="tabNotProductsByZip">
            <?= Log::widget([
                'data' => $notProductsByZip,
                'templates' => ['revertNum', 'created_at'],
                'showLinks' => true,
                'groupBy' => 'key',
            ]); ?>
        </div>
        <div role="tabpanel" class="tab-pane" id="tabNotProducts">
            <?= Log::widget([
                'data' => $notProducts,
                'templates' => ['revertNum', 'created_at'],
                'showLinks' => true,
                'showData' => true,
                'groupBy' => 'key',
            ]); ?>
        </div>
        <div role="tabpanel" class="tab-pane" id="tabAutoChangeReason">
            <?= Log::widget([
                'data' => $autoChangeReasons,
                'templates' => ['revertNum', 'created_at'],
                'showLinks' => true,
                'showData' => true,
                'groupBy' => 'key',
            ]); ?>
        </div>
    </div>
</div>