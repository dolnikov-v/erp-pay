<?php
use app\helpers\WbIcon;
use app\widgets\Button;
use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\components\widgets\ActiveForm $form */
/** @var app\modules\delivery\models\DeliveryZipcodes $model */
/** @var int $cnt */
?>
<br/>
<div class="row row-delivery-zipcodes">

    <?= Html::hiddenInput(Html::getInputName($model, 'id') . '[]', $model->id, ['class' => 'delivery-zipcodes-id']) ?>


    <div class="col-lg-3">
        <?= $form->field($model, 'name')->textInput([
            'id' => false,
            'name' => Html::getInputName($model, 'name') . '[]',
        ]) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'max_term_days')->textInput([
            'id' => false,
            'name' => Html::getInputName($model, 'max_term_days') . '[]',
        ]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'price')->textInput([
            'id' => false,
            'name' => Html::getInputName($model, 'price') . '[]',
        ]) ?>
    </div>

    <div class="col-lg-2">
        <?= $form->field($model, 'brokerage_daily_maximum')->textInput([
            'id' => false,
            'name' => Html::getInputName($model, 'brokerage_daily_maximum') . '[]',
        ]) ?>
    </div>

    <div class="col-lg-2">
        <div class="form-group">
            <div class="control-label">
                <label>&nbsp;</label>
            </div>
            <?= Button::widget([
                'type' => 'button',
                'icon' => WbIcon::MINUS,
                'style' => Button::STYLE_DANGER . ' btn-delivery-zipcodes btn-delivery-zipcodes-minus',
            ]) ?>
        </div>
    </div>

    <div class="col-lg-10">
        <?= $form->field($model, 'notProductIDs')->select2ListMultiple(
            \app\models\Product::find()->where(['active' => 1])->collection(),
            [
                'name' => Html::getInputName($model, 'notProductIDs') . "[". $cnt ."]"
            ]); ?>
    </div>

    <div class="col-lg-10">
        <?= $form->field($model, 'zipcodes')->textarea([
            'id' => false,
            'name' => Html::getInputName($model, 'zipcodes') . '[]',
        ]) ?>
    </div>

    <div class="col-lg-2">
        <div class="form-group">
            <div class="control-label">
                <label>&nbsp;</label>
            </div>
            <?= Button::widget([
                'type' => 'button',
                'label' => Yii::t('common', 'Удалить индексы'),
                'style' => Button::STYLE_WARNING . ' btn-delivery-zipcodes-open-modal',
            ]) ?>
        </div>
    </div>

    <div class="col-lg-10">
        <?= $form->field($model, 'cities')->textarea([
            'id' => false,
            'name' => Html::getInputName($model, 'cities') . '[]',
        ]) ?>
    </div>
    <div class="col-lg-2">
        <div class="form-group">
            <div class="control-label">
                <label>&nbsp;</label>
            </div>
            <?= Button::widget([
                'type' => 'button',
                'label' => Yii::t('common', 'Удалить города'),
                'style' => Button::STYLE_WARNING . ' btn-delivery-cities-open-modal',
            ]) ?>
        </div>
    </div>
</div>


