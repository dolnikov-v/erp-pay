<?php

namespace app\modules\delivery\helpers\models;

/**
 * Class PreparedProduct
 * @property integer $id
 * @property string $name
 * @property integer $quantity
 * @property integer $sumPrice
 * @property integer $price
 * @property integer $weight
 * @package app\modules\delivery\helpers\models
 */
class PreparedProduct
{

}