<?php

namespace app\modules\delivery\helpers;

use app\modules\delivery\helpers\models\PreparedProduct;
use app\modules\order\models\Order;

/**
 * Trait DeliveryApiHelper
 * @package app\modules\delivery\helpers
 */
trait DeliveryApiHelper
{
    public $charCode;
    public $errorList = [];
    public $pathDir;
    public $response;


    /**
     * * Сохранение логов в папке runtime/logs/delivery/courier-api . char_code/
     * @param $msg
     * @param string $type
     * @param bool $err
     * @return bool
     */
    public function saveLog($msg, $type = 'send', $err = false)
    {
        $path = \Yii::getAlias('@logs') . DIRECTORY_SEPARATOR . 'delivery' . DIRECTORY_SEPARATOR . get_class($this) . $this->charCode . DIRECTORY_SEPARATOR . $type . DIRECTORY_SEPARATOR;

        $fileName = $path . date('Y-m-d') . ($err ? '.err' : '.log');

        if (!is_dir($path)) {
            mkdir($path, 0777, true);
        }

        if (is_array($msg)) {
            $msg = print_r($msg, 1);
        }

        $msg = "==================== START " . date('h:i:s') . " ====================" . PHP_EOL . $msg;
        $msg .= PHP_EOL . "==================== STOP ====================" . PHP_EOL;
        if (file_put_contents($fileName, $msg, FILE_APPEND)) {
            return true;
        }
        return false;
    }

    /**
     * Получение объекта текущей страны по чаркоду, пример для индии aipex-api/src/IN/AipexApiIN.php
     * @param string|bool $charCode
     * @param string $dir //название api курьерской службы, пример aipex-api
     * @return bool
     */
    public function getCurrentClassCountry($charCode = false, $dir)
    {
        if (!$dir && !$charCode || $charCode == '') {
            return false;
        }
        $path = $dir . DIRECTORY_SEPARATOR . $charCode . DIRECTORY_SEPARATOR . get_class($this) . $charCode . '.php';
        if (file_exists($path)) {
            try {
                require_once($path);
                $currentCountryObjName = get_class($this) . $charCode;
                if (class_exists($currentCountryObjName)) {
                    $currentCountryObj = new $currentCountryObjName();
                    return $currentCountryObj;
                }
            } catch (\Exception $e) {
                $this->errorList[] = $e->getMessage();
            }
        }
        return false;
    }


    /**
     * Implode array $this->errorList
     * @return string
     */
    public function getError()
    {
        $message = implode(',', $this->errorList);
        return $message;
    }

    /**
     * Print response and request data for DB in cs_send_response field
     * @param $request string | array
     * @param $response string | array
     * @return string
     */
    public function printResponse($request, $response)
    {
        $this->response = "---Request---" . PHP_EOL . PHP_EOL;
        $this->response .= json_encode($request, true) . PHP_EOL;
        $this->response .= "---Response---" . PHP_EOL . PHP_EOL;
        return $this->response .= json_encode($response, true);
    }

    /**
     * @param Order | array $order
     * @return array
     */
    public function setTrackingParams($order)
    {
        if (is_array($order)) {
            $this->charCode = $order['country_char_code'];
            return [
                'tracking' => $order['tracking'],
                'order_id' => $order['order_id'],
                'status_id' => $order['status_id'],
            ];
        }
        $this->charCode = $order->country->char_code;
        return [
            'tracking' => $order->deliveryRequest->tracking,
            'order_id' => $order->id,
            'status_id' => $order->status_id,
        ];
    }

    /**
     * @param $response
     * @param string $filename
     */
    protected function writeTokenFile($response, $filename = null)
    {
        if (isset($filename)) {
            $file = fopen($filename, "w");
        } else {
            $file = fopen(static::TOKEN_FILENAME, "w");
        }
        fwrite($file, $response);
        fclose($file);
    }

    /**
     * @param Order $order
     * @return PreparedProduct[]
     */
    public function prepareProductList($order)
    {
        $preparedProducts = [];
        //$deliverySkuList = $this->deliveryRequest->delivery->productSkuList;
        foreach ($order->orderProducts as $orderProduct) {
            $key = $orderProduct->product_id . '_' . $orderProduct->price;
            if (!isset($preparedProducts[$key])) {
                $preparedProducts[$key] = new PreparedProduct();
                $preparedProducts[$key]->name = $orderProduct->product->name;
                $preparedProducts[$key]->weight = $orderProduct->product->weight;
                $preparedProducts[$key]->quantity = 0;
                $preparedProducts[$key]->sumPrice = 0;
                $preparedProducts[$key]->price = 0;
                $preparedProducts[$key]->id = $orderProduct->product_id;
            }
            $preparedProducts[$key]->quantity += $orderProduct->quantity;
            $preparedProducts[$key]->sumPrice += $orderProduct->price * $orderProduct->quantity;
            if (!empty($orderProduct->price)) {
                $preparedProducts[$key]->price = $orderProduct->price;
            }
        }

        return $preparedProducts;
    }
}