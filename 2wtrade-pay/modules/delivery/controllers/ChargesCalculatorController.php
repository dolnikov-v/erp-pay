<?php

namespace app\modules\delivery\controllers;


use app\components\web\Controller;
use app\modules\delivery\models\DeliveryChargesCalculator;
use app\modules\delivery\models\search\DeliveryChargesCalculatorSearch;
use Yii;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

/**
 * Class ChargesTemplateController
 * @package app\modules\delivery\controllers
 */
class ChargesCalculatorController extends Controller
{
    public function actionIndex()
    {
        $searchModel = new DeliveryChargesCalculatorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionView($id)
    {
        $data['model'] = $this->findModel($id);

        $orders = [
            'active' => SORT_DESC,
            'id' => SORT_ASC,
        ];

        $deliveries = $data['model']->deliveries;
        ArrayHelper::multisort($deliveries, array_keys($orders), array_values($orders));

        $data['dataProvider'] = new ArrayDataProvider([
            'allModels' => $deliveries,
        ]);

        return $this->render('view', $data);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionActivate($id)
    {
        $model = $this->findModel($id);

        $model->active = 1;

        if ($model->save(true, ['active'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Шаблон успешно активирован.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionDeactivate($id)
    {
        $model = $this->findModel($id);

        $model->active = 0;

        if ($model->save(true, ['active'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Шаблон успешно деактивирован.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param integer $id
     * @return DeliveryChargesCalculator
     * @throws NotFoundHttpException
     */
    private function findModel($id)
    {
        $model = DeliveryChargesCalculator::findOne($id);

        if (!$model) {
            throw new NotFoundHttpException(Yii::t('common', 'API-класс не найден.'));
        }

        return $model;
    }
}