<?php

namespace app\modules\delivery\controllers;

use app\components\filters\AjaxFilter;
use app\components\Notifier;
use app\components\web\Controller;
use app\models\logs\InsertData;
use app\models\logs\TableLog;
use app\models\Product;
use app\modules\catalog\models\RequisiteDelivery;
use app\modules\catalog\models\RequisiteDeliveryLink;
use app\modules\delivery\components\control\DeliveryControl;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryApiClass;
use app\modules\delivery\models\DeliveryAutoChangeReason;
use app\modules\delivery\models\DeliveryContacts;
use app\modules\delivery\models\DeliveryContract;
use app\modules\delivery\models\DeliveryContractFile;
use app\modules\delivery\models\DeliveryContractTime;
use app\modules\delivery\models\DeliveryInvoiceOptions;
use app\modules\delivery\models\DeliveryNotProducts;
use app\modules\delivery\models\DeliveryNotProductsByZip;
use app\modules\delivery\models\DeliveryNotRegions;
use app\modules\delivery\models\DeliveryPackage;
use app\modules\delivery\models\DeliverySku;
use app\modules\delivery\models\DeliveryStorage;
use app\modules\delivery\models\DeliveryTiming;
use app\modules\delivery\models\DeliveryZipcodes;
use app\modules\delivery\models\log\DeliveryContractTableLog;
use app\modules\delivery\models\log\DeliveryNotProductByZipTableLog;
use app\modules\delivery\models\log\DeliveryNotProductTableLog;
use app\modules\delivery\models\search\DeliveryContractFileSearch;
use app\modules\delivery\models\search\DeliveryContractSearch;
use app\modules\delivery\models\search\DeliverySearch;
use app\modules\order\components\ExcelBuilder;
use app\modules\order\models\OrderWorkflow;
use app\modules\packager\models\Packager;
use app\modules\salary\models\Person;
use app\modules\storage\models\Storage;
use himiklab\sortablegrid\SortableGridAction;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class ControlController
 * @package app\modules\delivery\controllers
 */
class ControlController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => ['set-storage', 'set-zip-codes', 'set-cities'],
            ]
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new DeliverySearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return string|Response
     * @throws \yii\db\Exception
     */
    public function actionAdd()
    {
        $model = new Delivery();
        $model->loadDefaultValues();
        $modelsDeliveryContacts = [];

        if ($model->load(Yii::$app->request->post())) {
            $transaction = Yii::$app->db->beginTransaction();

            $model->country_id = Yii::$app->user->country->id;

            if ($model->save()) {

                $success = true;
                $deliveryControl = new DeliveryControl();
                if (!$deliveryControl->saveContactsFromPost($model, Yii::$app->request->post('DeliveryContacts'), $modelsDeliveryContacts)) {
                    $success = false;
                    Yii::$app->notifier->addNotificationsByModel($model);
                }

                if ($success) {
                    $transaction->commit();
                    Yii::$app->notifier->addNotification(Yii::t('common', 'Служба доставки успешно добавлена.'), 'success');

                    return $this->redirect(Url::toRoute('index'));
                }
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }

            $transaction->rollBack();
        }

        return $this->render('edit', $this->getModelData($model));
    }

    /**
     * @param $id
     * @return string|Response
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     */
    public function actionEdit($id)
    {
        $model = $this->findModel($id);
        $modelsDeliveryContacts = $this->findContacts($id);
        $modelsDeliveryAutoChangeReason = is_null($id) ? [] : $this->findDeliveryAutoChangeReason($id);

        if ($model->load(Yii::$app->request->post())) {
            $transaction = Yii::$app->db->beginTransaction();

            $model->country_id = Yii::$app->user->country->id;
            $model->loadFlags(Yii::$app->request->post('Delivery') ?? []);
            if ($model->save()) {

                $success = true;
                $deliveryControl = new DeliveryControl();
                if (!$deliveryControl->saveAutoChangeReasonsFromPost($model, Yii::$app->request->post('DeliveryAutoChangeReason'), $modelsDeliveryAutoChangeReason)) {
                    $success = false;
                    Yii::$app->notifier->addNotificationsByModel($model);
                }

                if ($success && !$deliveryControl->saveContactsFromPost($model, Yii::$app->request->post('DeliveryContacts'), $modelsDeliveryContacts)) {
                    $success = false;
                    Yii::$app->notifier->addNotificationsByModel($model);
                }

                if ($success) {
                    $transaction->commit();
                    Yii::$app->notifier->addNotification(Yii::t('common', 'Служба доставки успешно сохранена.'), 'success');
                    return $this->redirect(Url::toRoute('index'));
                }
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }

            $transaction->rollBack();
        }

        return $this->render('edit', $this->getModelData($model));
    }

    /**
     * @param null $id
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionEditSystemSettings($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            $properties = [
                'api_class_id',
                'date_format',
                'amazon_queue_url',
                'not_send_if_no_contract',
                'not_send_if_has_debts',
                'not_send_if_has_debts_days',
                'not_send_if_in_process',
                'not_send_if_in_process_days',
            ];

            if ($model->save(true, $properties)) {
                Yii::$app->notifier->addNotification(Yii::t('common', 'Служба доставки успешно сохранена.'), 'success');
                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }

        return $this->render('edit-system-settings', $this->getModelData($model));
    }

    /**
     * @param null $id
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        return $this->render('view', $this->getModelData($model));
    }

    /**
     * @param null $id
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionEditAutoList($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            $properties = [
                'auto_list_generation_columns',
                'auto_list_generation_size',
                'auto_generating_invoice',
                'auto_generating_barcode',
                'list_send_to_emails'
            ];

            if ($model->save(true, $properties)) {
                Yii::$app->notifier->addNotification(Yii::t('common', 'Служба доставки успешно сохранена.'), 'success');
                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }
        $excelColumnsPartQuantity = 4;
        $partSize = ceil(count(ExcelBuilder::$columns) / $excelColumnsPartQuantity);
        $columns = [];
        foreach (range(0, $excelColumnsPartQuantity - 1) as $part) {
            $columns[] = array_slice(ExcelBuilder::$columns, $partSize * $part, $partSize);
        }
        return $this->render('edit-auto-list', array_merge($this->getModelData($model), ['excelBuilderColumns' => $columns]));
    }

    /**
     * @param null $id
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionEditPretension($id)
    {
        $model = $this->findModel($id);

        $timing = new DeliveryTiming([
            'offset_from' => $model->pretension_send_offset_from,
            'offset_to' => $model->pretension_send_offset_to,
        ]);
        $timing->isNewRecord = false;

        if ($model->load(Yii::$app->request->post())) {

            $postTimings = Yii::$app->request->post('Timing');
            $postTimingsFrom = $postTimings['from'][0] ?? null;

            $model->pretension_send_offset_from = $postTimingsFrom ? (strtotime($postTimingsFrom) - strtotime(date('Y-m-d 00:00:00'))) : null;
            $model->pretension_send_offset_to = (!is_null($model->pretension_send_offset_from) ? ($model->pretension_send_offset_from + 600) : null);

            $properties = [
                'send_to_check_undelivery',
                'auto_pretension_generation',
                'auto_pretension_send',
                'pretension_send_offset_from',
                'pretension_send_offset_to',
            ];

            if ($model->save(true, $properties)) {
                Yii::$app->notifier->addNotification(Yii::t('common', 'Служба доставки успешно сохранена.'), 'success');
                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }

        return $this->render('edit-pretension', array_merge($this->getModelData($model), [
            'timing' => $timing,
            'lawyer' => Person::getMoscowLawyer(),
        ]));
    }

    /**
     * @param $id
     * @return string|Response
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     */
    public function actionEditTimings($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            $transaction = Yii::$app->db->beginTransaction();

            DeliveryTiming::deleteAll(['delivery_id' => $model->id]);

            $log = new TableLog();
            $log->table = DeliveryTiming::clearTableName();
            $log->id = $model->id;
            $log->type = TableLog::TYPE_DELETE;
            $log->save();

            $success = true;
            $postTimings = Yii::$app->request->post('Timing');
            $postTimingsFrom = array_key_exists('from', $postTimings) ? $postTimings['from'] : [];
            $postTimingsTo = array_key_exists('to', $postTimings) ? $postTimings['to'] : [];

            if ($postTimingsFrom && is_array($postTimingsFrom) && $postTimingsTo && is_array($postTimingsTo)) {
                foreach ($postTimingsFrom as $key => $postTimingFrom) {
                    if (array_key_exists($key, $postTimingsTo)) {
                        $postTimingTo = $postTimingsTo[$key];

                        if ($postTimingFrom != '' && $postTimingTo != '') {
                            $timing = new DeliveryTiming();
                            $timing->delivery_id = $model->id;
                            $timing->offset_from = strtotime($postTimingFrom) - strtotime(date('Y-m-d 00:00:00'));
                            $timing->offset_to = strtotime($postTimingTo) - strtotime(date('Y-m-d 00:00:00'));

                            if (!$timing->save()) {
                                $success = false;
                                Yii::$app->notifier->addNotificationsByModel($timing);
                            } else {
                                $log = new TableLog();
                                $log->table = DeliveryTiming::clearTableName();
                                $log->id = $model->id;
                                $log->data = [
                                    new InsertData(['field' => 'offset_from', 'new' => $timing->offset_from]),
                                    new InsertData(['field' => 'offset_to', 'new' => $timing->offset_to]),
                                ];
                                $log->type = TableLog::TYPE_INSERT;
                                $log->save();
                            }
                        }
                    }
                }
            }

            $jsonStr = $model->getJsonDeliveryIntervals();
            if ($jsonStr === false) {
                $success = false;
                Yii::$app->notifier->addNotificationsByModel($model);
            } else {
                $model->delivery_intervals = $jsonStr;
                if (!$model->save()) {
                    $success = false;
                    Yii::$app->notifier->addNotificationsByModel($model);
                }
            }

            if ($success) {
                $transaction->commit();
                Yii::$app->notifier->addNotification(Yii::t('common', 'Служба доставки успешно сохранена.'), 'success');
                return $this->redirect(Url::toRoute(['edit-timings', 'id' => $id]));
            } else {
                $transaction->rollBack();
            }
        }

        return $this->render('edit-timings', $this->getModelData($model));
    }

    /**
     * @param null $id
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionEditPaymentInvoice($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            $properties = [
                'payment_reminder_schedule',
                'payment_reminder_email_to',
                'payment_reminder_email_reply_to',
                'invoice_email_list',
                'auto_send_invoice',
                'invoice_currency_id',
                'invoice_round_precision',
            ];

            if ($model->save(true, $properties)) {
                Yii::$app->notifier->addNotification(Yii::t('common', 'Служба доставки успешно сохранена.'), 'success');
                DeliveryInvoiceOptions::deleteAll(['delivery_id' => $model->id]);
                if ($data = Yii::$app->request->post('DeliveryInvoiceOptions')) {
                    $error = false;
                    for ($i = 0; $i < count($data['day']); $i++) {
                        if (!empty($data['day'][$i]) && !empty($data['time'][$i])) {
                            $option = new DeliveryInvoiceOptions();
                            $option->load([
                                'day' => $data['day'][$i],
                                'time' => $data['time'][$i],
                                'from' => $data['from'][$i],
                                'to' => $data['to'][$i],
                                'delivery_id' => $model->id
                            ], '');
                            if (!$option->save()) {
                                $error = true;
                                Yii::$app->notifier->addNotificationsByModel($option);
                            }
                        }
                    }
                    if (!$error && isset($option)) {
                        Yii::$app->notifier->addNotification(Yii::t('common', 'Опции инвойсирования сохранены.'), 'success');
                    }
                }
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }

        return $this->render('edit-payment-invoice', $this->getModelData($model));
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\Exception
     * @throws \yii\db\StaleObjectException
     */
    public function actionEditBrokerage($id)
    {
        $model = $this->findModel($id);

        $deliveryNotProductsIds = ArrayHelper::getColumn(DeliveryNotProducts::findAll(['delivery_id' => $id]), 'product_id');
        $deliveryPackages = ArrayHelper::getColumn(DeliveryPackage::findAll(['delivery_id' => $id]), 'package_id');

        $deliveryNotZipCodes = ArrayHelper::getColumn(DeliveryNotRegions::find()
            ->byDeliveryId($id)
            ->isZipCode()
            ->all(), 'region');
        $deliveryNotCities = ArrayHelper::getColumn(DeliveryNotRegions::find()
            ->byDeliveryId($id)
            ->isCity()
            ->all(), 'region');

        $modelsDeliveryZipCodes = $this->findZipcodes($id);

        if ($model->load(Yii::$app->request->post())) {

            $transaction = Yii::$app->db->beginTransaction();
            $post = Yii::$app->request->post('Delivery');
            $postDeliveryNotRegions = Yii::$app->request->post('DeliveryNotRegions');

            $properties = [
                'brokerage_active',
                'brokerage_daily_minimum',
                'brokerage_daily_maximum',
                'express_delivery',
                'brokerage_min_order_price',
            ];

            if ($model->save(true, $properties)) {

                $success = true;

                $deliveryControl = new DeliveryControl();
                if (!$deliveryControl->saveZipCodesFromPost($model, Yii::$app->request->post('DeliveryZipcodes'), $modelsDeliveryZipCodes)) {
                    $success = false;
                    Yii::$app->notifier->addNotificationsByModel($model);
                }

                // Не доставляемые товары
                if (isset($post['non_delivery_products'])) {
                    foreach ($post['non_delivery_products'] as $product) {
                        $key = array_search($product, $deliveryNotProductsIds);
                        if ($key !== false) {
                            // был такой уже
                            unset($deliveryNotProductsIds[$key]);
                        } else {
                            $deliveryNotProducts = new DeliveryNotProducts();
                            $deliveryNotProducts->delivery_id = $model->id;
                            $deliveryNotProducts->product_id = $product;
                            $deliveryNotProducts->save();
                        }
                    }
                }

                if ($deliveryNotProductsIds) {
                    $deliveryNotProducts = DeliveryNotProducts::find()->where([
                        'delivery_id' => $model->id,
                        'product_id' => $deliveryNotProductsIds,
                    ])->all();
                    foreach ($deliveryNotProducts as $deliveryNotProduct) {
                        $deliveryNotProduct->delete();
                    }
                }

                // Акции
                if (isset($post['delivery_packages'])) {
                    foreach ($post['delivery_packages'] as $package) {
                        $key = array_search($package, $deliveryPackages);
                        if ($key !== false) {
                            // был такой уже
                            unset($deliveryPackages[$key]);
                        } else {
                            $deliveryPackage = new DeliveryPackage();
                            $deliveryPackage->delivery_id = $model->id;
                            $deliveryPackage->package_id = $package;
                            $deliveryPackage->save();
                        }
                    }
                }

                if ($deliveryPackages) {
                    // удаление тех, которые не прилетели в post
                    $deliveryPackagesDeleted = DeliveryPackage::find()->where([
                        'delivery_id' => $model->id,
                        'package_id' => $deliveryPackages
                    ])->all();
                    foreach ($deliveryPackagesDeleted as $deliveryPackageDeleted) {
                        $deliveryPackageDeleted->delete();
                    }
                }

                // Регионы вне зоны покрытия
                if (isset($postDeliveryNotRegions['zipcode'])) {
                    foreach (explode(',', $postDeliveryNotRegions['zipcode']) as $zipCode) {
                        $zipCode = trim($zipCode);
                        $key = array_search($zipCode, $deliveryNotZipCodes);
                        if ($key !== false && $zipCode !== '') {
                            unset($deliveryNotZipCodes[$key]);
                        } else {
                            $deliveryNotRegions = new DeliveryNotRegions();
                            $deliveryNotRegions->delivery_id = $model->id;
                            $deliveryNotRegions->type = DeliveryNotRegions::TYPE_ZIPCODE;
                            $deliveryNotRegions->region = $zipCode;
                            $deliveryNotRegions->save();
                        }
                    }
                }
                if ($deliveryNotZipCodes) {
                    $notRegionsDeleted = DeliveryNotRegions::find()->where([
                        'delivery_id' => $model->id,
                        'region' => $deliveryNotZipCodes,
                        'type' => DeliveryNotRegions::TYPE_ZIPCODE,
                    ])->all();
                    foreach ($notRegionsDeleted as $notRegionsDel) {
                        $notRegionsDel->delete();
                    }
                }

                if (isset($postDeliveryNotRegions['city'])) {
                    foreach (explode(',', $postDeliveryNotRegions['city']) as $city) {
                        $city = trim($city);
                        $key = array_search($city, $deliveryNotCities);
                        if ($key !== false && $city !== '') {
                            unset($deliveryNotCities[$key]);
                        } else {
                            $deliveryNotRegions = new DeliveryNotRegions();
                            $deliveryNotRegions->delivery_id = $model->id;
                            $deliveryNotRegions->type = DeliveryNotRegions::TYPE_CITY;
                            $deliveryNotRegions->region = $city;
                            $deliveryNotRegions->save();
                        }
                    }
                }
                if ($deliveryNotCities) {
                    $notRegionsDeleted = DeliveryNotRegions::find()->where([
                        'delivery_id' => $model->id,
                        'region' => $deliveryNotCities,
                        'type' => DeliveryNotRegions::TYPE_CITY,
                    ])->all();
                    foreach ($notRegionsDeleted as $notRegionsDel) {
                        $notRegionsDel->delete();
                    }
                }

                if ($success) {
                    $transaction->commit();
                    Yii::$app->notifier->addNotification(Yii::t('common', 'Служба доставки успешно сохранена.'), 'success');
                    return $this->redirect(Url::toRoute('index'));
                }
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
            $transaction->rollBack();
        }

        $data = $this->getModelData($model);
        $data['modelNotRegions'] = new DeliveryNotRegions();
        $data['deliveryNotZipcodes'] = $deliveryNotZipCodes;
        $data['deliveryNotCities'] = $deliveryNotCities;

        return $this->render('edit-brokerage', $data);
    }

    /**
     * @param null $id
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionEditStorage($id)
    {
        $model = $this->findModel($id);

        return $this->render('edit-storage', $this->getModelData($model));
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws HttpException
     * @throws NotFoundHttpException
     */
    public function actionEditProducts($id)
    {
        $model = $this->findModel($id);
        $modelsDeliverySku = $this->findSku($id);

        if (Yii::$app->request->post('DeliverySku')) {
            $deliveryControl = new DeliveryControl();
            if ($deliveryControl->saveSkuFromPost($model, Yii::$app->request->post('DeliverySku'), $modelsDeliverySku)) {
                Yii::$app->notifier->addNotification(Yii::t('common', 'Служба доставки успешно сохранена.'), 'success');
                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }
        return $this->render('edit-products', $this->getModelData($model));
    }

    /**
     * @param $model Delivery
     * @return array
     */
    private function getModelData($model)
    {
        $id = $model->id;

        $modelsDeliveryZipCodes = is_null($id) ? [] : $this->findZipcodes($id);
        $modelsDeliveryContacts = is_null($id) ? [] : $this->findContacts($id);
        $modelsDeliverySku = is_null($id) ? [] : $this->findSku($id);
        $modelsDeliveryAutoChangeReason = is_null($id) ? [] : $this->findDeliveryAutoChangeReason($id);

        $deliveryNotProductsIds = [];
        $deliveryPackages = [];

        if ($id) {
            $deliveryNotProductsIds = ArrayHelper::getColumn(DeliveryNotProducts::findAll(['delivery_id' => $id]), 'product_id');
            $deliveryPackages = ArrayHelper::getColumn(DeliveryPackage::findAll(['delivery_id' => $id]), 'package_id');
        }

        $data = [
            'model' => $model,
        ];

        $data['products'] = Product::find()->orderBy(['name' => SORT_ASC])->collection();

        $data['timings'] = $model->timings;

        $data['apiClasses'] = DeliveryApiClass::find()
            ->active()
            ->collection();

        $data['workflows'] = OrderWorkflow::find()
            ->active()
            ->collection();

        $data['deliveryNotProductsIds'] = $deliveryNotProductsIds;
        $data['deliveryPackages'] = $deliveryPackages;
        $data['modelsDeliveryZipcodes'] = $modelsDeliveryZipCodes;
        $data['modelsDeliveryContacts'] = $modelsDeliveryContacts;
        $data['modelsDeliverySku'] = $modelsDeliverySku;

        $packagers = Packager::find()
            ->byCountryId(Yii::$app->user->country->id)
            ->all();

        $data['packagers'] = [
            'all' => [],
            'inactive' => []
        ];
        foreach ($packagers as $packager) {
            $data['packagers']['all'][$packager->id] = $packager->name;
            if (!$packager->active) {
                $data['packagers']['inactive'][] = $packager->id;
            }
        }

        $data['modelsDeliveryAutoChangeReason'] = $modelsDeliveryAutoChangeReason;

        $otherDelivery = Delivery::find()
            ->byCountryId(Yii::$app->user->country->id)
            ->orderBy(['name' => SORT_ASC]);

        if ($id) {
            $otherDelivery->andWhere(['<>', 'id', $id]);
        }
        $data['otherDeliveries'] = $otherDelivery->collection();

        return $data;
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionActivate($id)
    {
        $model = $this->findModel($id);

        $model->active = 1;

        if ($model->save(true, ['active'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Служба доставки успешно активирована.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionDeactivate($id)
    {
        $model = $this->findModel($id);

        $model->active = 0;

        if ($model->save(true, ['active'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Служба доставки успешно деактивирована.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionActivateAutoSending($id)
    {
        $model = $this->findModel($id);

        if (!$model->our_api && (!$model->getApiClass()->exists() || !$model->canSendOrder())) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Метод отправки отсутствует'));
            return $this->redirect(Url::toRoute('index'));
        }

        $model->auto_sending = 1;

        if ($model->save(true, ['auto_sending'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', '«Автоматическая отправка» успешно разрешена.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionActivateAutoListGeneration($id)
    {
        $model = $this->findModel($id);

        $model->auto_list_generation = 1;

        if ($model->save(true, ['auto_list_generation'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', '«Автоматическая генерация листов» успешно разрешена.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionActivateOurApi($id)
    {
        $model = $this->findModel($id);

        $model->our_api = 1;

        if ($model->save(true, ['our_api'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Забор заказов по нашему API успешно разрешен.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionActivateBrokerage($id)
    {
        $model = $this->findModel($id);

        $model->brokerage_active = 1;

        if ($model->save(true, ['brokerage_active'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Использование брокера успешно разрешено.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionDeactivateAutoSending($id)
    {
        $model = $this->findModel($id);

        $model->auto_sending = 0;

        if ($model->save(true, ['auto_sending'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', '«Автоматическая отправка» успешно запрещена.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionDeactivateAutoListGeneration($id)
    {
        $model = $this->findModel($id);

        $model->auto_list_generation = 0;

        if ($model->save(true, ['auto_list_generation'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', '«Автоматическая генерация листов» успешно запрещена.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionDeactivateOurApi($id)
    {
        $model = $this->findModel($id);

        $model->our_api = 0;

        if ($model->save(true, ['our_api'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Забор заказов по нашему API успешно запрещен.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionDeactivateBrokerage($id)
    {
        $model = $this->findModel($id);

        $model->brokerage_active = 0;

        if ($model->save(true, ['brokerage_active'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Использование брокера успешно запрещено.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionActivateExpress($id)
    {
        $model = $this->findModel($id);

        $model->express_delivery = 1;

        if ($model->save(true, ['express_delivery'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Работа с экспресс заказами разрешена.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionDeactivateExpress($id)
    {
        $model = $this->findModel($id);

        $model->express_delivery = 0;

        if ($model->save(true, ['express_delivery'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Работа с экспресс заказами запрещена.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        try {
            if ($model->delete()) {
                Yii::$app->notifier->addNotification(Yii::t('common', 'Служба доставки успешно удалена.'), 'success');
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        } catch (\Exception $e) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Служба доставки использовалась. Она не может быть удалена.'), Notifier::TYPE_DANGER);
        } finally {
            return $this->redirect(['index']);
        }
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function actionContractDelete($id)
    {
        $model = $this->findModelDeliveryContract($id);
        $deliveryId = $model->delivery_id;

        if ($model->delete()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Контракт службы доставки успешно удален.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('control/contracts/' . $deliveryId));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function actionContractFileDelete($id)
    {
        $model = $this->findModelDeliveryContractFile($id);
        $contractId = $model->delivery_contract_id;

        if ($model->delete()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Файл контракта службы доставки успешно удален.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('control/contract-edit/' . $contractId));
    }

    /**
     * @return array
     * @throws BadRequestHttpException
     */
    public function actionSetStorage()
    {
        $deliveryId = (int)Yii::$app->request->post('delivery_id');
        $storageId = (int)Yii::$app->request->post('storage_id');

        /** @var Delivery $delivery */
        $delivery = Delivery::find()
            ->where(['id' => $deliveryId])
            ->andWhere(['country_id' => Yii::$app->user->country->id])
            ->one();

        /** @var Storage $storage */
        $storage = Storage::find()
            ->where(['id' => $storageId])
            ->andWhere(['country_id' => Yii::$app->user->country->id])
            ->one();

        if (!$delivery || !$storage) {
            throw new BadRequestHttpException(Yii::t('common', 'Обращение к несуществущей службе доставке или складу.'));
        }

        $value = Yii::$app->request->post('value');

        switch ($value) {
            case 'on':
                $exists = DeliveryStorage::find()
                    ->byDeliveryId($delivery->id)
                    ->byStorageId($storage->id)
                    ->exists();

                if (!$exists) {
                    $delivery->link('storages', $storage);
                }
                break;
            case 'off':
                $delivery->unlink('storages', $storage, true);
                break;
        }

        return [
            'success' => true,
        ];
    }

    /**
     * @return array
     */
    public function actionSetZipCodes()
    {
        $deliveryId = (int)Yii::$app->request->post('delivery_id');
        $deliveryZipCodesId = (int)Yii::$app->request->post('zipcodes_id');
        $zipCodes = Yii::$app->request->post('zip');

        $delivery = Delivery::find()
            ->where(['id' => $deliveryId])
            ->andWhere(['country_id' => Yii::$app->user->country->id])
            ->one();

        if (!$delivery) {
            die();
        }

        $deliveryZipCodes = DeliveryZipcodes::find()
            ->where(['id' => $deliveryZipCodesId])
            ->byDeliveryId($deliveryId)
            ->one();

        if (!$deliveryZipCodes) {
            die();
        }

        /** @var DeliveryZipcodes $deliveryZipCodes */
        $deliveryZipCodes->zipcodes = $zipCodes;

        if ($deliveryZipCodes->save()) {
            return [
                'success' => true,
            ];
        }
        return [];
    }

    /**
     * @return array
     */
    public function actionSetCities()
    {
        $deliveryId = (int)Yii::$app->request->post('delivery_id');
        $deliveryZipCodesId = (int)Yii::$app->request->post('zipcodes_id');
        $cities = Yii::$app->request->post('cities');

        $delivery = Delivery::find()
            ->where(['id' => $deliveryId])
            ->andWhere(['country_id' => Yii::$app->user->country->id])
            ->one();

        if (!$delivery) {
            die();
        }

        $deliveryZipCodes = DeliveryZipcodes::find()
            ->where(['id' => $deliveryZipCodesId])
            ->byDeliveryId($deliveryId)
            ->one();

        if (!$deliveryZipCodes) {
            die();
        }

        /** @var DeliveryZipcodes $deliveryZipCodes */
        $deliveryZipCodes->cities = $cities;

        if ($deliveryZipCodes->save()) {
            return [
                'success' => true,
            ];
        }
        return [];
    }

    /**
     * @param integer $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionLogs(int $id)
    {
        $change = TableLog::find()->byTable(Delivery::clearTableName())->byID($id)->allSorted();
        $contracts = DeliveryContractTableLog::find()
            ->byTable(DeliveryContract::clearTableName())
            ->byLink('delivery_id', $id)
            ->allSorted();
        $sku = TableLog::find()->byTable(DeliverySku::clearTableName())->byLink('delivery_id', $id)->allSorted();
        $timing = TableLog::find()->byTable(DeliveryTiming::clearTableName())->byID($id)->allSorted();
        $zipCodes = TableLog::find()
            ->byTable(DeliveryZipcodes::clearTableName())
            ->byLink('delivery_id', $id)
            ->allSorted();
        $notProductsByZip = DeliveryNotProductByZipTableLog::find()
            ->byTable(DeliveryNotProductsByZip::clearTableName())
            ->byLink('delivery_id', $id)
            ->allSorted();
        $notProducts = DeliveryNotProductTableLog::find()
            ->byTable(DeliveryNotProducts::clearTableName())
            ->byLink('delivery_id', $id)
            ->allSorted();
        $autoChangeReasons = TableLog::find()
            ->byTable(DeliveryAutoChangeReason::clearTableName())
            ->byLink('from_delivery_id', $id)
            ->allSorted();

        $model = $this->findModel($id);

        return $this->render('logs', [
            'model' => $model,
            'change' => $change,
            'contracts' => $contracts,
            'sku' => $sku,
            'timing' => $timing,
            'zipcodes' => $zipCodes,
            'notProductsByZip' => $notProductsByZip,
            'notProducts' => $notProducts,
            'autoChangeReasons' => $autoChangeReasons,
        ]);
    }

    /**
     * @param $id
     * @return array|Response
     * @throws NotFoundHttpException
     */
    public function actionEditChargesValues($id)
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
        }
        $modelContract = $this->findModelDeliveryContract($id);
        $chargesCalculator = $modelContract->getChargesCalculator();
        $response = [
            'status' => 'fail',
            'message' => '',
        ];
        try {
            $data = Yii::$app->request->post();
            if ($chargesCalculator->load($data)) {
                if (!$chargesCalculator->validate()) {
                    if (!Yii::$app->request->isAjax) {
                        Yii::$app->notifier->addNotificationsByModel($chargesCalculator);
                    } else {
                        $response['message'] = $chargesCalculator->getFirstErrorAsString();
                    }
                } else {
                    $chargesDate = [];
                    foreach ($chargesCalculator->getAttributes() as $key => $val) {
                        if (!is_object($val)) {
                            $chargesDate[$key] = $val;
                        }
                    }
                    $modelContract->chargesValues = [$chargesCalculator->formName() => $chargesDate];
                    if ($modelContract->save(true, ['charges_values'])) {
                        if (isset($data['load_from_file'])) {
                            $modelContract->loadChargesFromFile($data);
                            $response = [
                                'status' => 'success',
                                'message' => Yii::t('common', 'Значения для расчета расходов на доставку успешно считаны из файла.'),
                            ];
                        } else {
                            if (!Yii::$app->request->isAjax) {
                                Yii::$app->notifier->addNotification(Yii::t('common', 'Значения для расчета расходов на доставку успешно изменены'), 'success');
                            } else {
                                $response = [
                                    'status' => 'success',
                                    'message' => Yii::t('common', 'Значения для расчета расходов на доставку автосохранены.'),
                                ];
                            }
                        }
                    } else {
                        if (!Yii::$app->request->isAjax) {
                            Yii::$app->notifier->addNotificationsByModel($modelContract);
                        } else {
                            $response['message'] = $modelContract->getFirstErrorAsString();
                        }
                    }
                }
            }
        } catch (\Throwable $e) {
            if (!Yii::$app->request->isAjax) {
                Yii::$app->notifier->addNotification($e->getMessage());
            } else {
                $response['message'] = $e->getMessage();
            }
        }
        if (Yii::$app->request->isAjax) {
            return $response;
        }
        return $this->redirect(Url::toRoute(['contract-edit', 'id' => $modelContract->id]));
    }

    /**
     * @param integer $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionContracts($id)
    {
        $model = $this->findModel($id);
        $modelSearch = new DeliveryContractSearch();
        $dataProvider = $modelSearch->searchByDeliveryId($model->id);

        return $this->render('contracts', [
            'dataProvider' => $dataProvider,
            'modelDelivery' => $model,
        ]);
    }

    /**
     * @param null|integer $id
     * @param null|integer $delivery_id
     * @return string|\yii\web\Response
     * @throws HttpException
     * @throws \yii\db\Exception
     */
    public function actionContractEdit($id = null, $delivery_id = null)
    {
        if ($id) {
            $model = $this->findModelDeliveryContract($id);
            $modelDelivery = $this->findModel($model->delivery_id);
        } else {
            $model = new DeliveryContract();
            $modelDelivery = $this->findModel($delivery_id);
            $model->delivery_id = $modelDelivery->id;
        }
        $modelsDeliveryContractTimes = is_null($id) ? [] : $this->findContactTimes($id);

        $modelFilesSearch = new DeliveryContractFileSearch();
        $dataProviderFiles = $modelFilesSearch->searchByDeliveryContractId($id);
        $chargesCalculator = null;
        if ($model->charges_calculator_id) {
            $chargesCalculator = $model->getChargesCalculator();
        }


        $requisites = [];
        if ($id && $model->requisiteDeliveryLinks) {
            foreach ($model->requisiteDeliveryLinks as $link) {
                /** @var RequisiteDeliveryLink $link */
                $requisites[$link->requisite_delivery_id] = $link->requisiteDelivery;
            }
        }

        $post = Yii::$app->request->post();

        if ($model->load($post)) {

            $transaction = Yii::$app->db->beginTransaction();
            $isNewRecord = $model->isNewRecord;

            $model->date_from = $model->date_from ? Yii::$app->formatter->asDate($model->date_from, 'php:Y-m-d') : null;
            $model->date_to = $model->date_to ? Yii::$app->formatter->asDate($model->date_to, 'php:Y-m-d') : null;

            if ($model->save()) {
                $success = true;
                $deliveryControl = new DeliveryControl();
                if (!$deliveryControl->saveContractTimesFromPost($model, Yii::$app->request->post('DeliveryContractTime'))) {
                    $success = false;
                    Yii::$app->notifier->addNotificationsByModel($model);
                }

                $resultUpload = $model->upload();

                // реквизиты
                if (isset($post['RequisiteDeliveryLink'])) {
                    foreach ($post['RequisiteDeliveryLink'] as $requisite) {
                        if ($requisite) {
                            if (isset($requisites[$requisite])) {
                                // был такой уже
                                unset($requisites[$requisite]);
                            } else {
                                $requisiteDeliveryLink = new RequisiteDeliveryLink();
                                $requisiteDeliveryLink->delivery_id = $model->delivery_id;
                                $requisiteDeliveryLink->country_id = $model->delivery->country_id;
                                $requisiteDeliveryLink->delivery_contract_id = $model->id;
                                $requisiteDeliveryLink->requisite_delivery_id = $requisite;
                                $requisiteDeliveryLink->save();
                            }
                        }
                    }
                }

                if ($requisites) {
                    // удаление тех, которые не прилетели в post
                    RequisiteDeliveryLink::deleteAll([
                        'delivery_contract_id' => $model->id,
                        'requisite_delivery_id' => array_keys($requisites),
                    ]);
                }

                if ($success && $resultUpload === true) {
                    $transaction->commit();
                    Yii::$app->notifier->addNotification($isNewRecord ? Yii::t('common', 'Контракт успешно добавлен.') : Yii::t('common', 'Контракт успешно сохранен.'), 'success');
                    return $this->redirect(Url::toRoute('control/contract-edit/' . $model->id));
                } else {
                    Yii::$app->notifier->addNotification($resultUpload);
                }
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
            $transaction->rollBack();
        }

        return $this->render('contract_edit', [
            'model' => $model,
            'modelDelivery' => $modelDelivery,
            'dataProviderFiles' => $dataProviderFiles,
            'delivery' => $modelDelivery,
            'chargesCalculator' => $chargesCalculator,
            'requisites' => $requisites,
            'allRequisites' => RequisiteDelivery::find()->all(),
            'modelsDeliveryContractTimes' => $modelsDeliveryContractTimes,
            'onlyView' => false,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionContractView($id)
    {
        $model = $this->findModelDeliveryContract($id);
        $modelDelivery = $this->findModel($model->delivery_id);

        $modelFilesSearch = new DeliveryContractFileSearch();
        $dataProviderFiles = $modelFilesSearch->searchByDeliveryContractId($id);
        $chargesCalculator = null;
        if ($model->charges_calculator_id) {
            $chargesCalculator = $model->getChargesCalculator();
        }

        $requisites = [];

        if ($id) {
            foreach ($model->getRequisiteDeliveryLinks()->all() as $link) {
                /** @var RequisiteDeliveryLink $link */
                $requisites[] = $link->getRequisiteDelivery()->one();
            }
        }

        if (empty($requisites)) {
            $requisites = RequisiteDelivery::find()->all();
        }

        $modelsDeliveryContractTimes = is_null($id) ? [] : $this->findContactTimes($id);

        return $this->render('contract_edit', [
            'model' => $model,
            'modelDelivery' => $modelDelivery,
            'dataProviderFiles' => $dataProviderFiles,
            'delivery' => $modelDelivery,
            'chargesCalculator' => $chargesCalculator,
            'requisites' => $requisites,
            'modelsDeliveryContractTimes' => $modelsDeliveryContractTimes,
            'onlyView' => true,
        ]);
    }

    /**
     * @param int $id
     * @return string
     */
    public function actionContractLog(int $id)
    {
        $changeLog = TableLog::find()->byTable(DeliveryContract::clearTableName())->byID($id)->allSorted();
        $deliveryID = Yii::$app->request->get('delivery', null);

        return $this->render('_contract_log', [
            'changeLog' => $changeLog,
            'deliveryID' => $deliveryID,
        ]);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionActivateTracking($id)
    {
        $model = $this->findModel($id);

        $model->can_tracking = 1;

        if ($model->save(true, ['can_tracking'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Получение статусов активировано'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionDeactivateTracking($id)
    {
        $model = $this->findModel($id);

        $model->can_tracking = 0;

        if ($model->save(true, ['can_tracking'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Получение статусов деактивировано.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param integer $id
     * @return DeliveryZipcodes[]
     */
    protected function findZipcodes($id)
    {
        return DeliveryZipcodes::find()->byDeliveryId($id)->all();
    }

    /**
     * @param integer $id
     * @return DeliveryContractTime[]
     */
    protected function findContactTimes($id)
    {
        $data = DeliveryContractTime::find()->where(['contract_id' => $id])->all();

        $tmp = [];
        foreach ($data as $row) {
            /** @var $row DeliveryContractTime */
            $index = $row->field_name . '_' . $row->delivered_from . '_' . $row->delivered_to;
            if (!isset($tmp[$index])) {
                $tmp[$index] = [
                    'delivered_from' => $row->delivered_from,
                    'delivered_to' => $row->delivered_to,
                    'field_name' => $row->field_name,
                    'field_value' => [$row->field_value],
                ];
            } else {
                $tmp[$index]['field_value'][] = $row->field_value;
            }
        }
        $return = [];
        foreach ($tmp as $item) {
            $item['field_value'] = is_array($item['field_value']) ? implode(',', $item['field_value']) : '';
            $return[] = new DeliveryContractTime($item);
        }
        return $return;
    }

    /**
     * @param integer $id
     * @return DeliveryContacts[]
     */
    protected function findContacts($id)
    {
        return DeliveryContacts::find()->byDeliveryId($id)->all();
    }

    /**
     * @param integer $id
     * @return DeliverySku[]
     */
    protected function findSku($id)
    {
        return DeliverySku::find()->byDeliveryId($id)->all();
    }

    /**
     * @param int $id
     * @return DeliveryAutoChangeReason[]
     */
    protected function findDeliveryAutoChangeReason(int $id): array
    {
        return DeliveryAutoChangeReason::find()->where(['from_delivery_id' => $id])->all();
    }

    /**
     * @param integer $id
     * @return Delivery
     * @throws NotFoundHttpException
     */
    private function findModel($id)
    {
        $model = Delivery::findOne($id);

        if (!$model || $model->country_id != Yii::$app->user->country->id) {
            throw new NotFoundHttpException(Yii::t('common', 'Служба доставки не найдена.'), 404);
        }

        return $model;
    }

    /**
     * @param integer $id
     * @return DeliveryContract
     * @throws NotFoundHttpException
     */
    private function findModelDeliveryContract($id)
    {
        $model = DeliveryContract::findOne($id);

        if (!$model) {
            throw new NotFoundHttpException(Yii::t('common', 'Контракт службы доставки не найден.'), 404);
        }

        return $model;
    }

    /**
     * @param integer $id
     * @return DeliveryContractFile
     * @throws NotFoundHttpException
     */
    private function findModelDeliveryContractFile($id)
    {
        $model = DeliveryContractFile::findOne($id);

        if (!$model) {
            throw new NotFoundHttpException(Yii::t('common', 'Файл контракта службы доставки не найден.'), 404);
        }

        return $model;
    }

    /**
     * SortableGridView::widget
     * Add action in the controller
     * https://github.com/himiklab/yii2-sortable-grid-view-widget
     * @return array
     */
    public function actions()
    {
        return [
            'sort' => [
                'class' => SortableGridAction::className(),
                'modelName' => Delivery::className(),
            ],
        ];
    }
}
