<?php

namespace app\modules\delivery\controllers;

use app\components\web\Controller;
use app\modules\delivery\models\DeliveryApiClass;
use app\modules\delivery\models\search\DeliveryApiClassSearch;
use Yii;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

/**
 * Class ApiClassController
 * @package app\modules\delivery\controllers
 */
class ApiClassController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new DeliveryApiClassSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionView($id)
    {
        $data['model'] = $this->findModel($id);

        $orders = [
            'active' => SORT_DESC,
            'id' => SORT_ASC,
        ];

        $deliveries = $data['model']->deliveries;
        ArrayHelper::multisort($deliveries, array_keys($orders), array_values($orders));

        $data['dataProvider'] = new ArrayDataProvider([
            'allModels' => $deliveries,
        ]);

        return $this->render('view', $data);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionActivate($id)
    {
        $model = $this->findModel($id);

        $model->active = 1;

        if ($model->save(true, ['active'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'API-класс успешно активирован.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionDeactivate($id)
    {
        $model = $this->findModel($id);

        $model->active = 0;

        if ($model->save(true, ['active'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'API-класс успешно деактивирован.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param integer $id
     * @return \yii\web\Response
     */
    public function actionActivateAmazonQueue($id)
    {
        $model = $this->findModel($id);

        $model->use_amazon_queue = 1;

        if ($model->save(true, ['use_amazon_queue'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Использование Амазоновских очередей активировано.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param integer $id
     * @return \yii\web\Response
     */
    public function actionDeactivateAmazonQueue($id)
    {
        $model = $this->findModel($id);

        $model->use_amazon_queue = 0;

        if ($model->save(true, ['use_amazon_queue'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Использование Амазоновских очередей деактивировано.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionActivateTracking($id)
    {
        $model = $this->findModel($id);

        $model->can_tracking = 1;

        if ($model->save(true, ['can_tracking'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Получение статусов активировано'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param integer $id
     * @return \yii\web\Response
     */
    public function actionDeactivateTracking($id)
    {
        $model = $this->findModel($id);

        $model->can_tracking = 0;

        if ($model->save(true, ['can_tracking'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Получение статусов деактивировано.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param integer $id
     * @return DeliveryApiClass
     * @throws NotFoundHttpException
     */
    private function findModel($id)
    {
        $model = DeliveryApiClass::findOne($id);

        if (!$model) {
            throw new NotFoundHttpException(Yii::t('common', 'API-класс не найден.'));
        }

        return $model;
    }
}
