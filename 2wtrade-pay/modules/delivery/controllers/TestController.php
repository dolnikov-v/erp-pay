<?php

namespace app\modules\delivery\controllers;

use app\modules\delivery\components\api\TransmitterAdaptee;
use Yii;
use yii\web\Controller;
use yii\helpers\ArrayHelper;
use app\components\filters\AjaxFilter;
use app\components\widgets\ActiveForm;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryRequest;

class TestController extends Controller
{

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => ['send-order', 'tracking', 'manifest'],
            ]
        ];
    }

    public function actionIndex()
    {
        $result = '';
        $deliveryServices = ArrayHelper::map(Delivery::find()->byCountryId(Yii::$app->user->country->id)->asArray()->indexBy('name')->all(), 'id', 'name');
        $model = new DeliveryRequest();

        return $this->render('index', [
            'deliveryServices' => $deliveryServices,
            'result' => $result,
            'model' => $model,
        ]);
    }

    public function actionSendOrder()
    {
        $deliveryRequest = new DeliveryRequest();
        $deliveryRequest->load(Yii::$app->request->post());
        if (!$deliveryRequest->validate(['order_id'])) {
            return ActiveForm::validate($deliveryRequest);
        }
        $result = $deliveryRequest->sendOrderViaApiTransmitter();
        if ($result['status'] == 'success') {
            $result['message'] = print_r($result['data'], true);
        }
        return $result;
    }

    public function actionTracking()
    {
        $deliveryRequest = new DeliveryRequest();
        $deliveryRequest->load(Yii::$app->request->post());
        $result = $deliveryRequest->getOrderInfoViaApiTransmitter();
        if ($result['status'] == 'success') {
            $result['message'] = print_r($result['data'], true);
        }
        return $result;
    }

    public function actionManifest()
    {

        $post = Yii::$app->request->post();

        $deliveryId = $post['DeliveryRequest']['delivery_id'] ?? 0;
        $orderId = $post['DeliveryRequest']['order_id'] ?? 0;

        if ($deliveryId && $orderId) {
            $delivery = Delivery::findOne(['id' => $deliveryId]);
            $ids = array_map('trim', explode(',', $orderId));

            if ($delivery && $ids) {
                $manifest = (new TransmitterAdaptee($delivery))->generateManifestByOrders($ids);
                $result['message'] = print_r($manifest, true);
                return $result;
            }
            else {
                return ['message' => Yii::t('common', 'Некорректные параметры')];
            }
        }
        return ['message' => Yii::t('common', 'Нет входящих параметров')];
    }
}