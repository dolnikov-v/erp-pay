<?php
namespace app\modules\delivery\controllers;

use app\components\web\Controller;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\delivery\models\DeliveryRequestLog;
use app\modules\delivery\models\search\DeliveryRequestSearch;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * Class RequestController
 * @package app\modules\delivery\controllers
 */
class RequestController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new DeliveryRequestSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        $deliveriesCollection = Delivery::find()
            ->where([Delivery::tableName() . '.country_id' => Yii::$app->user->country->id])
            ->active()
            ->collection();

        return $this->render('index', [
            'modelSearch' => $modelSearch,
            'dataProvider' => $dataProvider,
            'deliveriesCollection' => $deliveriesCollection,
        ]);
    }

    /**
     * @param integer $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionLogs($id)
    {
        $model = $this->findModel($id);

        $logs = $this->findModelLogs($model->id);

        return $this->render('logs', [
            'logs' => $logs,
        ]);
    }

    /**
     * @param integer $id
     * @return DeliveryRequest
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        $model = DeliveryRequest::find()
            ->joinWith(['logs'])
            ->where([DeliveryRequest::tableName() . '.id' => $id])
            ->one();

        if (!$model) {
            throw new NotFoundHttpException(Yii::t('common', 'Заявка не найдена.'));
        }

        return $model;
    }

    /**
     * @param $id
     * @return DeliveryRequestLog[]
     */
    protected function findModelLogs($id)
    {
        return DeliveryRequestLog::find()
            ->joinWith('user')
            ->where(['delivery_request_id' => $id])
            ->all();
    }
}
