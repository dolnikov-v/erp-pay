<?php
namespace app\modules\delivery\abstracts;

use app\modules\order\models\Order;

/**
 * Interface DeliveryApiClassLabelInterface
 * @package app\modules\delivery\abstracts
 */
interface DeliveryApiClassLabelInterface
{
    /**
     * Получение файла этикетки для заказа
     * @param Order $order
     * @return string|false
     */
    public function generateLabelByOrder($order);
}
