<?php
namespace app\modules\delivery\abstracts;

use app\modules\order\models\Order;

/**
 * Interface DeliveryApiClassManifestInterface
 * @package app\modules\delivery\abstracts
 */
interface DeliveryApiClassManifestInterface
{
    /**
     * Получение файла манифеста по номерам заказов
     * @param $orderIds
     * @param array|null $extra
     * @return mixed
     */
    public function generateManifestByOrders($orderIds, ?array $extra = null);
}
