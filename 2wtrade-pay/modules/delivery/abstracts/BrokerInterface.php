<?php

namespace app\modules\delivery\abstracts;

/**
 * Interface BrokerInterface
 * @package app\modules\delivery\abstracts
 *
 */
interface BrokerInterface
{
    /**
     * @param BrokerRequestInterface $brokerRequest
     * @return BrokerResultInterface[]
     */
    public function getAvailableDeliveries(BrokerRequestInterface $brokerRequest): array;
}