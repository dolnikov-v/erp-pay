<?php

namespace app\modules\delivery\abstracts;

/**
 * Interface BrokerResultInterface
 * @package app\modules\delivery\abstracts
 *
 * @property bool $enough
 * @property integer $score
 * @property integer $delivery_id
 * @property string $delivery_name
 * @property float $delivery_price
 * @property bool $express
 * @property integer $min_delivery_days
 * @property integer $max_delivery_days
 */
interface BrokerResultInterface
{
    /**
     * @return bool
     */
    public function getEnough(): bool;

    /**
     * @return int
     */
    public function getScore(): int;

    /**
     * @return int
     */
    public function getDeliveryId(): int;

    /**
     * @return string
     */
    public function getDeliveryName(): string;

    /**
     * @return float
     */
    public function getDeliveryPrice(): ?float;

    /**
     * @return bool
     */
    public function getExpress(): bool;

    /**
     * @return int
     */
    public function getMinDeliveryDays(): ?int;

    /**
     * @return int
     */
    public function getMaxDeliveryDays(): ?int;
}