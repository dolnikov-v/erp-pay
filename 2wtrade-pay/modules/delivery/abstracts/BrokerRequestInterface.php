<?php

namespace app\modules\delivery\abstracts;

/**
 * Interface BrokerRequestInterface
 * @package app\modules\delivery\abstracts
 *
 * @property int $country_id
 * @property int $package_id
 * @property array $products;
 * @property string $customer_zip;
 * @property string $customer_city;
 * @property string $customer_province;
 * @property int $partner_id;
 * @property int $express;
 * @property float $order_price;
 * @property float $shipping_price;
 *
 */
interface BrokerRequestInterface
{
    /**
     * @return int
     */
    public function getCountry(): int;

    /**
     * @return int
     */
    public function getPackageId(): ?int;

    /**
     * @return array
     */
    public function getProducts(): ?array;

    /**
     * @return array
     */
    public function getProductIds(): ?array;

    /**
     * @return string
     */
    public function getCustomerZip(): ?string;

    /**
     * @return string
     */
    public function getCustomerCity(): ?string;

    /**
     * @return string
     */
    public function getCustomerProvince(): ?string;

    /**
     * @return int
     */
    public function getPartnerId(): ?int;

    /**
     * @return int
     */
    public function getExpress(): ?int;

    /**
     * @return float
     */
    public function getOrderPrice(): ?float;

    /**
     * @return float
     */
    public function getShippingPrice(): ?float;
}