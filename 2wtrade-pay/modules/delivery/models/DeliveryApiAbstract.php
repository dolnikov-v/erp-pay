<?php
namespace app\modules\delivery\models;

/**
 * Class DeliveryApiAbstract
 * @package app\modules\delivery\models
 */
abstract class DeliveryApiAbstract
{
    const ERROR = true;

    /**
     * Api class have sendOrder method
     */
    const SEND_ORDER = false;

    /**
     * @param \app\modules\order\models\Order | bool $order
     * @return array
     */
    abstract public function sendOrder($order = false);

    /**
     * @param bool | $order Order
     * @return array
     */
    abstract public function getOrderInfo($order = false);
}
