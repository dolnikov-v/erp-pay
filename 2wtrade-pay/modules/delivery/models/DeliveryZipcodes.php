<?php

namespace app\modules\delivery\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\logs\LinkData;
use app\modules\delivery\models\query\DeliveryZipcodesQuery;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use app\modules\callcenter\models\CallCenterRequest;
use Yii;
use yii\db\Expression;

/**
 * Class DeliveryZipcodes
 * @package app\modules\delivery\models
 * @property integer $id
 * @property string $name
 * @property string $zipcodes
 * @property string $cities
 * @property double $price
 * @property integer $max_term_days
 * @property integer $delivery_id
 * @property integer $brokerage_daily_maximum
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Delivery $delivery
 */
class DeliveryZipcodes extends ActiveRecordLogUpdateTime
{
    protected $insertLog = true;
    protected $updateLog = true;
    protected $deleteLog = true;

    protected function getLinkData(): ?array
    {
        return [
            new LinkData(['field' => 'delivery_id', 'value' => (int)$this->delivery_id])
        ];
    }

    /*
     * Старое распределение максимального срока доставки, дней
     */
    const OLD_MAX_TERM_DAYS = 15;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%delivery_zipcodes}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['zipcodes', 'cities'], 'string'],
            ['zipcodes', 'filter', 'filter' => [$this, 'filterZipcodes']],
            ['zipcodes', 'validateZipCodes'],
            ['cities', 'filter', 'filter' => [$this, 'filterCities']],
            ['cities', 'validateCities'],
            [['price'], 'double'],
            [['delivery_id', 'max_term_days'], 'integer'],
            [['brokerage_daily_maximum'], 'integer'],
            [['name'], 'string', 'max' => 200],
        ];
    }

    /**
     * Поиск и вывод информации о дублях
     *
     * @param string $attribute
     */
    public function validateZipCodes($attribute)
    {
        $query = self::find()->byDeliveryId($this->delivery_id);
        if ($this->id) {
            $query->andWhere(['<>', 'id', $this->id]);
        }
        $otherZipCodes = $query->all();
        if ($otherZipCodes) {
            $duplicates = [];
            $codesToSave = array_flip(explode(",", $this->$attribute));
            foreach ($otherZipCodes as $zipCodes) {
                if ($zipCodes->zipcodes) {
                    $codesExists = array_flip(explode(",", $zipCodes->zipcodes));
                    if ($codesExists) {
                        foreach ($codesToSave as $zip => $v) {
                            if (isset($codesExists[$zip])) {
                                $duplicates[] = $zip;
                            }
                        }
                    }
                }
            }
            if ($duplicates) {
                $this->addError($attribute, Yii::t('common', 'Дубли индексов в другой группе: ' . implode(",", $duplicates)));
            }
        }
    }


    /**
     * Приводит в порядок список почтовых индексов
     *
     * Убирает дубликаты, пробелы, сортирует
     *
     * @param string $zipcodes
     * @return string
     */
    public function filterZipcodes($zipcodes)
    {
        $codes = array_map("trim", explode(",", $zipcodes));
        $codes = array_filter(array_flip(array_flip($codes))); // удаляем дубликаты и пустые значения
        sort($codes, SORT_NATURAL);
        return implode(",", $codes);
    }


    /**
     * Поиск и вывод информации о дублях
     *
     * @param string $attribute
     */
    public function validateCities($attribute)
    {
        $query = self::find()->byDeliveryId($this->delivery_id);
        if ($this->id) {
            $query->andWhere(['<>', 'id', $this->id]);
        }
        $otherCities = $query->all();
        if ($otherCities) {
            $duplicates = [];
            $codesToSave = array_flip(explode(",", $this->$attribute));
            foreach ($otherCities as $cities) {
                if ($cities->cities) {
                    $codesExists = array_flip(explode(",", $cities->cities));
                    if ($codesExists) {
                        foreach ($codesToSave as $city => $v) {
                            if (isset($codesExists[$city])) {
                                $duplicates[] = $city;
                            }
                        }
                    }
                }
            }
            if ($duplicates) {
                $this->addError($attribute, Yii::t('common', 'Дубли городов в другой группе: ' . implode(",", $duplicates)));
            }
        }
    }


    /**
     * Приводит в порядок список городов
     *
     * Убирает дубликаты, пробелы, сортирует
     *
     * @param string $cities
     * @return string
     */
    public function filterCities($cities)
    {
        $cities = mb_strtolower(strtr($cities, ["\r" => " ", "\n" => " "]));
        $codes = array_map("trim", explode(",", $cities));
        $codes = array_filter(array_flip(array_flip($codes))); // удаляем дубликаты и пустые значения
        sort($codes, SORT_NATURAL);
        return implode(",", $codes);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'name' => Yii::t('common', 'Название группы'),
            'zipcodes' => Yii::t('common', 'Индексы'),
            'cities' => Yii::t('common', 'Города'),
            'price' => Yii::t('common', 'Стоимость доставки'),
            'max_term_days' => Yii::t('common', 'Максимальный срок доставки'),
            'delivery_id' => Yii::t('common', 'Служба доставки'),
            'brokerage_daily_maximum' => Yii::t('common', 'Лимит заказов за день'),
            'created_at' => Yii::t('common', 'Дата добавления'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
            'notProductIDs' => Yii::t('common', 'Не доставляемые товары'),
        ];
    }

    /**
     * Подсчёт апрувнутых заказов
     * @return integer кол-во апрувнутых заказов за день
     */
    public function getOrdersApprovedOneDay()
    {
        $countOrders = Order::find()
            ->joinWith(['callCenterRequest', 'deliveryRequest'])
            ->where(['COALESCE(' . DeliveryRequest::tableName() . '.delivery_id, ' . Order::tableName() . ".pending_delivery_id)" => $this->delivery_id])
            ->andWhere([">=", Order::tableName() . ".status_id", OrderStatus::STATUS_CC_APPROVED])
            ->andWhere(["<>", Order::tableName() . ".status_id", OrderStatus::STATUS_CC_REJECTED])
            ->andWhere(new Expression('(FIND_IN_SET(' . Order::tableName() . '.customer_zip, "' . $this->zipcodes . '") OR FIND_IN_SET(' . Order::tableName() . '.customer_city, "' . $this->cities . '"))'))
            ->andWhere([">", CallCenterRequest::tableName() . ".approved_at", strtotime('today midnight UTC')])
            ->count();

        return intval($countOrders);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDelivery()
    {
        return $this->hasOne(Delivery::className(), ['delivery_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotProducts()
    {
        return $this->hasMany(DeliveryNotProductsByZip::className(), ['zipcodes_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotProductIDs()
    {
        return $this->getNotProducts()->select('product_id')->column();
    }

    /**
     * @inheritdoc
     * @return DeliveryZipcodesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DeliveryZipcodesQuery(get_called_class());
    }
}