<?php

namespace app\modules\delivery\models;


use app\components\db\ActiveRecordLogUpdateTime;
use app\models\logs\LinkData;
use app\models\Product;
use Yii;

/**
 * This is the model class for table "delivery_not_products".
 *
 * Какие товары курьерка не доставляет
 *
 * @property integer $id
 * @property integer $delivery_id
 * @property integer $product_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Delivery $delivery
 * @property Product $product
 */
class DeliveryNotProducts extends ActiveRecordLogUpdateTime
{
    protected $insertLog = true;
    protected $deleteLog = true;

    /**
     * @inheritdoc
     */
    protected function getLinkData(): ?array
    {
        return [
            new LinkData(['field' => 'delivery_id', 'value' => (int)$this->delivery_id]),
            new LinkData(['field' => 'product_id', 'value' => (int)$this->product_id]),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%delivery_not_products}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['delivery_id', 'product_id'], 'required'],
            [['delivery_id', 'product_id', 'created_at', 'updated_at'], 'integer'],
            [['delivery_id'], 'exist', 'skipOnError' => true, 'targetClass' => Delivery::className(), 'targetAttribute' => ['delivery_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'delivery_id' => Yii::t('common', 'Служба доставки'),
            'product_id' => Yii::t('common', 'Не доставляемый товар'),
            'created_at' => Yii::t('common', 'Дата добавления'),
            'updated_at' => Yii::t('common', 'Дата обновления'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDelivery()
    {
        return $this->hasOne(Delivery::className(), ['id' => 'delivery_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}
