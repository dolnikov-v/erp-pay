<?php

namespace app\modules\delivery\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\helpers\Utils;
use app\models\api\ApiUserDelivery;
use app\models\Country;
use app\models\Notification;
use app\models\PartnerCountry;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\delivery\abstracts\DeliveryApiClassLabelInterface;
use app\modules\delivery\abstracts\DeliveryApiClassManifestInterface;
use app\modules\delivery\components\api\Transmitter;
use app\modules\delivery\models\query\DeliveryQuery;
use app\modules\order\abstracts\OrderProductInterface;
use app\modules\order\models\Order;
use app\modules\order\models\OrderProduct;
use app\modules\order\models\OrderStatus;
use app\modules\order\models\OrderWorkflow;
use app\modules\packager\models\Packager;
use app\modules\report\models\ReportDeliveryDebts;
use app\modules\storage\models\Storage;
use Yii;
use himiklab\sortablegrid\SortableGridBehavior;
use yii\base\InvalidParamException;
use yii\helpers\ArrayHelper;

/**
 * Class Delivery
 * @package app\modules\delivery\models
 * @property integer $id
 * @property integer $country_id
 * @property string $name
 * @property string $group_name
 * @property string $char_code
 * @property string $url
 * @property integer $api_class_id
 * @property string $comment
 * @property integer $active
 * @property integer $auto_sending
 * @property integer $can_tracking
 * @property integer $workflow_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $is_keep
 * @property integer $sent_notification_no_contract_at
 * @property integer $sent_notification_has_debts_at
 * @property integer $sent_notification_in_process_at
 * @property string $payment_reminder_schedule // как часто отправлять напоминание о платежах (end-of-week/twice-a-month/end-of-month)
 * @property string $payment_reminder_email_to //  кому отправлять, через запятую
 * @property string $payment_reminder_email_reply_to //  кому отвечать (попадет в заголовок reply-to уведомления)
 * @property integer $payment_reminder_sent_at // unixtime последнего отосланного уведомления
 * @property integer $brokerage_daily_minimum // брокер будет слать новые заказы сюда и игнорировать статистику выкупов пока не наберется хотябы это количество заказов в курьерку
 * @property integer $brokerage_daily_maximum // брокер не будет слать новые заказы, даже если статистика хорошая, если за тукущий день тут уже больше заказов чем daily_maximum
 * @property integer $brokerage_stats_cutoff // количество дней, за которые подсчитывается статистика для брокера
 * @property integer $brokerage_active // активность для брокера
 * @property integer $brokerage_min_order_price
 * @property integer $auto_list_generation
 * @property integer $auto_generating_invoice
 * @property integer $auto_generating_barcode
 * @property string $list_send_to_emails
 * @property integer $auto_list_generation_size
 * @property string $auto_list_generation_columns
 * @property string $invoice_email_list // список email для отправки инвойсов (через ,)
 * @property integer $sort_order
 * @property string[] $emailList
 * @property Country $country
 * @property PartnerCountry $partnerCountry
 * @property DeliveryApiClass $apiClass
 * @property Storage[] $storages
 * @property Transmitter $apiTransmitter
 * @property OrderWorkflow $workflow
 * @property DeliveryTiming[] $timings
 * @property ApiUserDelivery[] $userDelivery
 * @property integer $our_api // Забор заказов по нашему API
 * @property string $delivery_intervals // Интервалы доставки, хранятся в JSON [0] => [...] - массив для понедельника, ..., [6] => [...] - массив для воскресенья
 * @property string $date_format
 * @property integer $packager_id
 * @property string $amazon_queue_name
 * @property string $charges_values
 * @property integer $express_delivery
 * @property string $legal_name
 * @property string $address
 * @property DeliveryInvoiceOptions $deliveryInvoiceOptions
 * @property boolean $auto_send_invoice
 * @property string $packager_send_invoice
 * @property string $packager_send_label
 * @property integer $delivered_from
 * @property integer $delivered_to
 * @property integer $not_send_if_no_contract
 * @property integer $not_send_if_has_debts
 * @property integer $not_send_if_has_debts_days
 * @property integer $not_send_if_in_process
 * @property integer $not_send_if_in_process_days
 * @property Packager $packager
 * @property DeliverySku[] $productSku
 * @property array $productSkuList
 * @property callable $reminderScheduler
 * @property \yii\db\ActiveQuery $deliveryStorages
 * @property bool|string $jsonDeliveryIntervals
 * @property int $ordersApprovedOneDay
 * @property array $chargesValues
 * @property DeliveryContract[] $deliveryContracts
 * @property DeliveryContacts[] $deliveryContacts
 * @property array $invoiceEmailList
 * @property array $autoListGenerationColumns
 * @property DeliveryPartner[] $partners
 * @property DeliveryNotRegions[] $deliveryNotRegions
 * @property integer $send_to_check_undelivery
 * @property integer $auto_pretension_generation
 * @property integer $auto_pretension_send
 * @property integer $pretension_send_offset_from
 * @property integer $pretension_send_offset_to
 * @property integer $flags
 * @property DeliveryAutoChangeReason[] $deliveryAutoChangeReasons
 *
 * @property integer $invoice_currency_id
 * @property integer $invoice_round_precision
 */
class Delivery extends ActiveRecordLogUpdateTime
{
    protected $insertLog = true;
    protected $updateLog = true;
    protected $deleteLog = true;

    const REMINDER_SCHEDULE_NEVER = 'never';
    const REMINDER_SCHEDULE_END_OF_WEEK = 'end-of-week';
    const REMINDER_SCHEDULE_TWICE_A_MONTH = 'twice-a-month';
    const REMINDER_SCHEDULE_END_OF_MONTH = 'end-of-month';

    const REMINDER_HALF_MONTH = 15; // 15-е всегда середина месяца
    const REMINDER_NUMBER = 3; // сколько всего напоминаний слать для каждого платежа
    const REMINDER_SEND_AFTER_HOUR = 8; // когда можно начинать отсылать (по локальному времени курьерки)
    const REMINDER_END_OF_WEEK = 5; // пятница - конец недели

    const BROKERAGE_MIN_BUYOUT = 10; // если за период выкупов меньше, то считаем статистику ненадежной и игнорируем при распределении заказов

    /**
     * Флаг для автоматической передачи заказов в альтернативную курьерскую службу при отклонении (19 статус)
     */
    const FLAG_AUTO_CHANGE_DELIVERY_ON_REJECT = 1;

    /*
    Новые флаги задавать так:
    const FLAG_2 = 2;
    const FLAG_3 = 4;
    const FLAG_4 = 8;
     */

    /**
     * @var Delivery[]
     */
    protected static $requestDeliveries = [];

    /**
     * @var Transmitter
     */
    protected $apiTransmitter;

    /**
     * @var string
     * понедельник 0
     */
    public $monday;

    /**
     * @var string
     * вторник 1
     */
    public $tuesday;

    /**
     * @var string
     * среда 2
     */
    public $wednesday;

    /**
     * @var string
     * четверг 3
     */
    public $thursday;

    /**
     * @var string
     * пятница 4
     */
    public $friday;

    /**
     * @var string
     * суббота 5
     */
    public $saturday;

    /**
     * @var string
     * воскресенье 6
     */
    public $sunday;

    /**
     * @var array
     */
    public $mapDays = [
        'monday' => '0',
        'tuesday' => '1',
        'wednesday' => '2',
        'thursday' => '3',
        'friday' => '4',
        'saturday' => '5',
        'sunday' => '6',
    ];

    protected $productSkuList = null;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%delivery}}';
    }

    /**
     * @return array
     */
    public static function getReminderScheduleLabels()
    {
        return [
            self::REMINDER_SCHEDULE_NEVER => Yii::t('common', 'Не отправлять'),
            self::REMINDER_SCHEDULE_END_OF_WEEK => Yii::t('common', 'В конце недели'),
            self::REMINDER_SCHEDULE_TWICE_A_MONTH => Yii::t('common', 'Дважды в месяц'),
            self::REMINDER_SCHEDULE_END_OF_MONTH => Yii::t('common', 'В конце месяца'),
        ];
    }

    /**
     * @return DeliveryQuery
     */
    public static function find()
    {
        return new DeliveryQuery(get_called_class());
    }

    /**
     * @return array
     */
    public static function getFlagsMapping()
    {
        return [
            self::FLAG_AUTO_CHANGE_DELIVERY_ON_REJECT => 'auto_change_on_reject',
        ];
    }

    /**
     * @param array $post
     */
    public function loadFlags(array $post)
    {
        if (is_null($this->flags)) {
            $this->flags = 0;
        }
        foreach (self::getFlagsMapping() as $const => $property) {
            if ($flag = (int)$post[$property] ?? 0) {
                $this->flags |= $const;
            } else {
                $this->flags &= ~$const;
            }
        }
    }

    /**
     * @param int $flag
     * @return bool
     */
    public function isFlagSet(int $flag): bool
    {
        if ($this->flags & $flag) {
            return true;
        }
        return false;
    }

    /**
     * @return bool
     */
    public function isAutoChangeOnReject(): bool
    {
        return $this->isFlagSet(self::FLAG_AUTO_CHANGE_DELIVERY_ON_REJECT);
    }

    /**
     * @deprecated тут не должно быть этой функции
     * @param Delivery|integer $delivery
     * @param Order $order
     * @param boolean $manual
     * @return array
     */
    public static function createRequest($delivery, $order, $manual = true)
    {
        $result = [
            'status' => 'fail',
            'message' => '',
            'delivery_request_id' => null,
        ];

        if ($delivery instanceof Delivery) {
            $deliveryId = $delivery->id;
        } else {
            $deliveryId = $delivery;

            if (array_key_exists($deliveryId, self::$requestDeliveries)) {
                $delivery = self::$requestDeliveries[$deliveryId];
            } else {
                $delivery = Delivery::find()
                    ->joinWith([
                        'workflow',
                    ])
                    ->where([Delivery::tableName() . '.id' => $deliveryId])
                    ->byCountryId($order->country_id)
                    ->one();
            }
        }

        if ($delivery) {

            //проверка существования sendOrder() в классе КС
            /*if ($delivery instanceof Delivery && self::issetMethodApi($delivery, 'sendOrder') === false) {
                return [
                    'status' => 'fail',
                    'message' => Yii::t('common', 'Курьерская служба не имеет апи для отправки заказов')
                ];
            }*/

            if (!array_key_exists($deliveryId, self::$requestDeliveries)) {
                self::$requestDeliveries[$deliveryId] = $delivery;
            }

            $success = true;
            $transaction = Yii::$app->db->getTransaction() ? null : Yii::$app->db->beginTransaction();

            if ($manual) {
                if ($delivery->active) {
                    $result = self::createRequestChanges($delivery, $order);
                    $success = $result['status'] == 'fail' ? false : true;
                } else {
                    $success = false;
                    $result['message'] = Yii::t('common', 'Курьерская служба недоступна.');
                }
            } else {
                if ($delivery->auto_sending) {
                    $result = self::createRequestChanges($delivery, $order);
                    $success = $result['status'] == 'fail' ? false : true;
                } else {
                    $order->route = Yii::$app->controller->route;
                    $order->pending_delivery_id = $deliveryId;

                    if (!$order->save(true, ['pending_delivery_id'])) {
                        $success = false;
                        $result['message'] = array_shift($order->getFirstErrors());
                    }
                }
            }

            if ($transaction) {
                if ($success) {
                    $transaction->commit();
                } else {
                    $transaction->rollBack();
                }
            }

            $result['status'] = $success ? 'success' : 'fail';
        } else {
            $result['message'] = Yii::t('common', 'Курьерская служба не найдена.');
        }

        return $result;
    }

    /**
     * @param Order $order
     * @param float $deliveryPrice
     * @param bool $hold
     * @throws \Exception|\yii\db\Exception|\Throwable
     * @return bool
     */
    public function prepareOrderToSend($order, $deliveryPrice = null, $hold = false)
    {
        $hasContractRequisiteTariff = $this->hasContractRequisiteTariff();
        if (!$hasContractRequisiteTariff['status']) {

            if (!$this->sent_notification_no_contract_at ||
                $this->sent_notification_no_contract_at < strtotime('-1 day')
            ) {
                Yii::$app->notification->send(
                    Notification::TRIGGER_DELIVERY_HAS_NO_CONTRACT,
                    [
                        'delivery' => $this->name,
                    ],
                    $this->country_id
                );

                $this->sent_notification_no_contract_at = time();
                $this->save(false, ['sent_notification_no_contract_at']);
            }
            return false;
        }

        if ($this->hasDebts()) {
            if (!$this->sent_notification_has_debts_at ||
                $this->sent_notification_has_debts_at < strtotime('-1 day')
            ) {
                Yii::$app->notification->send(
                    Notification::TRIGGER_DELIVERY_NO_SEND_BY_DEBTS,
                    [
                        'delivery' => $this->name,
                        'days' => $this->not_send_if_has_debts_days,
                    ],
                    $this->country_id
                );

                $this->sent_notification_has_debts_at = time();
                $this->save(false, ['sent_notification_has_debts_at']);
            }
            return false;
        }

        $countOldOrdersInProcess = $this->countOldOrdersInProcess();
        if ($countOldOrdersInProcess) {
            if (!$this->sent_notification_in_process_at ||
                $this->sent_notification_in_process_at < strtotime('-1 day')
            ) {
                Yii::$app->notification->send(
                    Notification::TRIGGER_DELIVERY_NO_SEND_BY_PROCESS,
                    [
                        'delivery' => $this->name,
                        'orders' => $countOldOrdersInProcess,
                        'days' => $this->not_send_if_in_process_days,
                    ],
                    $this->country_id
                );

                $this->sent_notification_in_process_at = time();
                $this->save(false, ['sent_notification_in_process_at']);
            }
            return false;
        }

        if ($hold || $this->shouldDeferOrder($order)) {
            $order->status_id = OrderStatus::STATUS_LOG_DEFERRED;
            $order->pending_delivery_id = $this->id;
        } elseif ($order->shouldUsePackageService($this)) {
            // $order->initPackageService(false, $this->id); вот тут временно отключаю, т.к. получается заказ должен сперва по API уйти в КС и получить трек
        } elseif ($this->shouldSendOrderThroughList($order)) {
            $order->pending_delivery_id = $this->id;
        } else {
            if ($order->deliveryRequest && $this->auto_sending) {
                $order->status_id = OrderStatus::STATUS_DELIVERY_PENDING;
                $order->deliveryRequest->status = DeliveryRequest::STATUS_PENDING;
                $order->deliveryRequest->delivery_id = $this->id;
                if (!$order->deliveryRequest->save(false, ['status', 'delivery_id'])) {
                    throw new \Exception($order->deliveryRequest->getFirstErrorAsString());
                }
            } else {
                if ($order->deliveryRequest) {
                    if ($order->deliveryRequest->delete() === false) {
                        throw new \Exception($order->deliveryRequest->getFirstErrorAsString());
                    }
                }
                $order->pending_delivery_id = $this->id;
                Delivery::createRequest($this, $order, false);
            }
        }

        if (!is_null($deliveryPrice)) {
            $order->delivery = $deliveryPrice;
        }
        if (!$order->save(false)) {
            throw new \Exception($order->getFirstErrorAsString());
        }
        return true;
    }

    /**
     * @param Order $order
     * @return bool
     */
    public function canDeliverOrderNow($order)
    {
        $contract = $this->getActiveContract();
        return empty($contract) || empty($contract->max_delivery_period) || $order->delivery_time_from <= strtotime("+{$contract->max_delivery_period}days");
    }

    /**
     * @param Order $order
     * @return bool
     */
    public function shouldDeferOrder($order)
    {
        return (!$this->canDeliverOrderNow($order) && (($this->workflow && $this->workflow->canChangeStatus($order->status_id, OrderStatus::STATUS_LOG_DEFERRED)) || $order->canChangeStatusTo(OrderStatus::STATUS_LOG_DEFERRED)));
    }

    /**
     * @param Order $order
     * @return bool
     */
    public function shouldSendOrderThroughList($order)
    {
        return $this->workflow && $this->workflow->canChangeStatus($order->status_id, OrderStatus::STATUS_LOG_ACCEPTED);
    }

    /**
     * Проверка существования метода у класса курьеской службы
     * @param Delivery $delivery
     * @param string $method
     * @return bool
     */
    public static function issetMethodApi($delivery, $method)
    {
        if ($delivery instanceof Delivery && isset($delivery->apiClass) && !empty($delivery->apiClass)) {
            $apiClass = $delivery->getApiTransmitter()->getAdaptee()->getApiClass();
            if (is_object($apiClass) && method_exists($apiClass, $method)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Проверка существования метода sendOrder у класса курьеской службы
     * @return bool
     */
    public function canSendOrder()
    {
        if ($this->apiClass) {
            try {
                $apiClass = $this->getApiTransmitter()->getAdaptee()->getApiClass();
                return $apiClass::SEND_ORDER || $this->our_api;
            } catch (InvalidParamException $e) {
                if ($this->our_api) {
                    return true;
                }
                throw $e;
            }
        }
        return false;
    }


    /**
     * Проверем есть ли долги по дебиторской задолженности
     * @return boolean
     */
    public function hasDebts()
    {
        // исключаем партнеров и дистрибьюторов и если выключен флаг
        if ($this->country->is_partner || $this->country->is_distributor || !$this->not_send_if_has_debts) {
            return false;
        }

        $endSumDollars = ReportDeliveryDebts::getEndSumByDelivery($this->id, $this->not_send_if_has_debts_days);
        if ($endSumDollars) {
            return true;
        }

        return false;
    }

    /**
     * Проверем есть ли заказы в процессе отправленные более 46 дней назад
     * @return integer число таких заказов
     */
    public function countOldOrdersInProcess()
    {
        // исключаем партнеров и дистрибьюторов и если выключен флаг
        if ($this->country->is_partner || $this->country->is_distributor || !$this->not_send_if_in_process) {
            return 0;
        }

        return Order::find()
            ->joinWith('deliveryRequest', false)
            ->where([
                DeliveryRequest::tableName() . '.delivery_id' => $this->id,
            ])
            ->andWhere([
                '<=',
                'COALESCE(' . DeliveryRequest::tableName() . '.second_sent_at, ' . DeliveryRequest::tableName() . '.sent_at, ' . DeliveryRequest::tableName() . '.created_at)',
                strtotime('- ' . $this->not_send_if_in_process_days . ' days')
            ])
            ->andWhere([
                'not in',
                Order::tableName() . '.status_id',
                [
                    OrderStatus::STATUS_DELIVERY_RETURNED,
                    OrderStatus::STATUS_FINANCE_MONEY_RECEIVED,
                    OrderStatus::STATUS_CLIENT_REFUSED,
                ]
            ])
            ->count();
    }

    /**
     * @return array
     */
    public function hasContractRequisiteTariff()
    {
        // исключаем партнеров и дистрибьюторов и если выключен флаг
        if ($this->country->is_partner || $this->country->is_distributor || !$this->not_send_if_no_contract) {
            return ['status' => true, 'message' => ''];
        }

        $contract = DeliveryContract::find()
            ->byDeliveryId($this->id)
            ->byDates(date('Y-m-d'), date('Y-m-d'))
            ->one();

        if (!$contract) {
            return ['status' => false, 'message' => Yii::t('common', 'Нет контракта')];
        }

        if (!$contract->requisiteDeliveryLinks) {
            return ['status' => false, 'message' => Yii::t('common', 'Нет реквизитов в контракте')];
        }

        if (!$contract->chargesValues) {
            return ['status' => false, 'message' => Yii::t('common', 'Нет тарифов')];
        }

        if (!isset($contract->chargesCalculatorModel->name)) {
            return ['status' => false, 'message' => Yii::t('common', 'Нет имени шаблона расчета')];
        }

        if (!isset($contract->chargesValues[$contract->chargesCalculatorModel->name])) {
            return ['status' => false, 'message' => Yii::t('common', 'Нет данных по имени шаблона расчета')];
        }

        foreach ($contract->chargesValues[$contract->chargesCalculatorModel->name] as $key => $value) {
            if (is_array($value)) {
                foreach ($value as $k => $v) {
                    if ($v) {
                        return ['status' => true, 'message' => ''];
                    }
                }
            } else {
                if ($value) {
                    return ['status' => true, 'message' => ''];
                }
            }
        }
        return ['status' => false, 'message' => Yii::t('common', 'Нет тарифов в шаблоне')];
    }


    /**
     * @return bool
     */
    public function hasCustomDeliveryPrice()
    {
        // @TODO Надо привести все это дело в нормальный вид, ибо рассчет стоимости доставки в АПИ-классе не комильфо
        if ($this->apiClass && $this->apiClass->active) {
            $apiClass = $this->getApiTransmitter()->getAdaptee()->getApiClass();

            if (method_exists($apiClass, 'getDeliveryPrice')) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param $delivery
     * @param $order
     * @param bool $manual
     * @return array
     * @throws \yii\db\Exception
     */

    public static function createRequestInList($delivery, $order, $manual = true)
    {
        $result = [
            'status' => 'fail',
            'message' => '',
            'delivery_request_id' => null,
        ];

        if ($delivery instanceof Delivery) {
            $deliveryId = $delivery->id;
        } else {
            $deliveryId = $delivery;

            if (array_key_exists($deliveryId, self::$requestDeliveries)) {
                $delivery = self::$requestDeliveries[$deliveryId];
            } else {
                $delivery = Delivery::find()
                    ->joinWith([
                        'workflow',
                    ])
                    ->where([Delivery::tableName() . '.id' => $deliveryId])
                    ->byCountryId($order->country_id)
                    ->one();
            }
        }

        if ($delivery) {
            if (!array_key_exists($deliveryId, self::$requestDeliveries)) {
                self::$requestDeliveries[$deliveryId] = $delivery;
            }

            $success = true;
            $transaction = Yii::$app->db->getTransaction() ? null : Yii::$app->db->beginTransaction();

            if ($manual) {
                if ($delivery->active) {
                    $result = self::createRequestChangesInList($delivery, $order);
                    $success = $result['status'] == 'fail' ? false : true;
                } else {
                    $success = false;
                    $result['message'] = Yii::t('common', 'Курьерская служба недоступна.');
                }
            }

            if ($transaction) {
                if ($success) {
                    $transaction->commit();
                } else {
                    $transaction->rollBack();
                }
            }

            $result['status'] = $success ? 'success' : 'fail';
        } else {
            $result['message'] = Yii::t('common', 'Курьерская служба не найдена.');
        }

        return $result;
    }

    /**
     * @param Delivery $delivery
     * @param Order $order
     * @return array
     */
    protected static function createRequestChanges($delivery, $order)
    {
        $result = [
            'status' => 'fail',
            'message' => '',
            'delivery_request_id' => null,
        ];

        $canChange = false;

        if ($delivery->workflow_id) {
            $nextStatuses = $delivery->workflow->getNextStatuses($order->status_id);

            if (in_array(OrderStatus::STATUS_DELIVERY_PENDING,
                    $nextStatuses) || in_array(OrderStatus::STATUS_LOG_DEFERRED, $nextStatuses)
            ) {
                $canChange = true;
            }
        } elseif ($order->canChangeStatusTo(OrderStatus::STATUS_DELIVERY_PENDING) || $order->canChangeStatusTo(OrderStatus::STATUS_LOG_DEFERRED)) {
            $canChange = true;
        }

        $contract = $delivery->getActiveContract();
        if (!$contract) {
            $canChange = false;
        }

        if ($canChange) {
            $success = true;

            $order->route = Yii::$app->controller->route;

            if (!empty($contract->max_delivery_period) && $order->delivery_time_from > strtotime("+{$contract->max_delivery_period}days") && $order->canChangeStatusTo(OrderStatus::STATUS_LOG_DEFERRED)) {
                $order->status_id = OrderStatus::STATUS_LOG_DEFERRED;
                $order->pending_delivery_id = $delivery->id;
            } else {
                $order->status_id = OrderStatus::STATUS_DELIVERY_PENDING;
            }

            if (!$order->save(true, ['status_id', 'pending_delivery_id'])) {
                $success = false;
                $result['message'] = array_shift($order->getFirstErrors());
            }

            if ($success && $order->status_id != OrderStatus::STATUS_LOG_DEFERRED) {
                $request = new DeliveryRequest();
                $request->delivery_id = $delivery->id;
                $request->order_id = $order->id;
                $request->status = DeliveryRequest::STATUS_PENDING;

                if ($request->save()) {
                    $result['delivery_request_id'] = $request->id;
                } else {
                    $success = false;
                    $result['message'] = array_shift($request->getFirstErrors());
                }
            }

            $result['status'] = $success ? 'success' : 'fail';
        } else {
            $result['message'] = Yii::t('common', 'Неразрешенное значение статуса.');
        }

        return $result;
    }

    /**
     * возвращает true если $now в пределах трех рабочих дней до последнего числа месяца или до середины месяца и в этот день еще не отправляли
     *
     * @param \DateTime $sentAt
     * @param \DateTime $now
     * @return boolean
     */
    protected static function schedulerTwiceAMonth($sentAt, $now)
    {
        $halfMonth = new \DateTime (static::REMINDER_HALF_MONTH . $now->format(" F Y"), $now->getTimezone());

        return static::schedulerSpecificDate($sentAt, $now, $halfMonth) || static::schedulerEndOfMonth($sentAt, $now);
    }

    /**
     * возвращает true если $now в пределах трех рабочих дней до последнего числа месяца и в этот день еще не отправляли
     *
     * @param \DateTime $sentAt
     * @param \DateTime $now
     * @return boolean
     */
    protected static function schedulerEndOfMonth($sentAt, $now)
    {
        $daysInMonth = (int)$now->format("t");

        $targetDate = new \DateTime ($daysInMonth . $now->format(" F Y"), $now->getTimezone());

        return static::schedulerSpecificDate($sentAt, $now, $targetDate);
    }

    /**
     * возвращает true если $now в пределах трех рабочих дней до конкретной даты (включительно)
     *
     * @param \DateTime $sentAt
     * @param \DateTime $now
     * @param \DateTime $targetDate
     *
     * @return boolean
     */
    protected static function schedulerSpecificDate($sentAt, $now, $targetDate)
    {
        $checkDate = clone $targetDate;
        $checkDate->setTime(static::REMINDER_SEND_AFTER_HOUR, 0, 0);
        $checkDate->add(new \DateInterval("P1D"));

        $daysLeft = static::REMINDER_NUMBER;

        // если сегодня уже отправляли, то точно не надо больше ничего слать
        if ($sentAt->format("j F Y") == $now->format("j F Y")) {
            return false;
        }

        while ($daysLeft > 0) {
            $checkDate->sub(new \DateInterval("P1D"));

            // проверяем что не выходной
            $dayOfWeek = (int)$checkDate->format("N");

            if ($dayOfWeek > static::REMINDER_END_OF_WEEK) {
                continue; // выходной. просто пропускаем
            }

            // проверяем не наш ли это день, игнорируем время
            if ($checkDate->format("j F Y") != $now->format("j F Y")) {
                $daysLeft--; // будний день, но еще не настал. пропускаем
                continue;
            }

            // день - наш
            // убеждаемся что ещё не слишком рано
            if ($now < $checkDate) {
                return false;
            }
            return true;
        }

        // до нужной даты еще слишком далеко
        return false;
    }

    /**
     * Возвращает true, если $now - пятница, четверг или среда, и если в этот день отправки ещё не было
     *
     * @param \DateTime $sentAt время отсылки предыдущего уведомления
     * @param \DateTime $now
     * @return boolean
     */
    protected static function schedulerEndOfWeek($sentAt, $now)
    {
        $dayStart = clone $now;
        $dayStart->setTime(static::REMINDER_SEND_AFTER_HOUR, 0, 0);

        $dayOfWeek = (int)$dayStart->format("N");

        // проверяем нужно ли вообще сегодня отправлять
        if ($dayOfWeek > static::REMINDER_END_OF_WEEK || $dayOfWeek <= static::REMINDER_END_OF_WEEK - static::REMINDER_NUMBER) {
            return false;
        }

        // проверяем не отправили ли мы уже сегодня
        $daySent = clone $sentAt;
        $daySent->setTime(static::REMINDER_SEND_AFTER_HOUR, 0, 0);

        if ($daySent == $dayStart) {
            return false; // уже отправляли
        }

        // проверяем не рановато-ли сегодня еще отправлять
        if ($dayStart > $now) {
            return false;
        }

        return true;
    }

    /**
     * @param Delivery $delivery
     * @param Order $order
     * @return array
     */
    protected static function createRequestChangesInList($delivery, $order)
    {
        $result = [
            'status' => 'fail',
            'message' => '',
            'delivery_request_id' => null,
        ];

        $success = true;

        if (!DeliveryRequest::find()->byOrderId($order->id)->exists()) {
            $request = new DeliveryRequest();
            $request->delivery_id = $delivery->id;
            $request->order_id = $order->id;
            $request->status = DeliveryRequest::STATUS_IN_LIST;
            $request->sent_at = time();

            if ($request->save()) {
                $result['delivery_request_id'] = $request->id;
            } else {
                $success = false;
                $result['message'] = array_shift($request->getFirstErrors());
            }

        }

        $result['status'] = $success ? 'success' : 'fail';

        return $result;
    }

    /**
     * Подсчёт апрувнутых заказов
     * @return integer кол-во апрувнутых заказов за день
     */
    public function getOrdersApprovedOneDay()
    {
        $countOrders = Order::find()
            ->joinWith(['callCenterRequest', 'deliveryRequest'])
            ->where(['COALESCE(' . DeliveryRequest::tableName() . '.delivery_id, ' . Order::tableName() . ".pending_delivery_id)" => $this->id])
            ->andWhere([">=", Order::tableName() . ".status_id", OrderStatus::STATUS_CC_APPROVED])
            ->andWhere(["<>", Order::tableName() . ".status_id", OrderStatus::STATUS_CC_REJECTED])
            ->andWhere([">", CallCenterRequest::tableName() . ".approved_at", strtotime('today midnight UTC')])
            ->count();

        return intval($countOrders);
    }

    /**
     * Рейтинг для брокера.
     * Предпочтение отдается курьеркам выставленным вручную выше в /delivery/control/index
     * sort_order будет минимальный
     * чтобы не править Lead::compareAndSaveRequest рейтинг вычисляется как 1...0
     * учитывается лимит brokerage_daily_minimum
     * @param DeliveryZipcodes $deliveryZipGroup группа ZIP кодов
     * @param bool $expressDelivery учитывать флаг в настройках КС $expressDelivery express_delivery
     * @return number|null
     */
    public function brokerageScore($deliveryZipGroup = null, $expressDelivery = false)
    {
        $score = 0;

        $deliveries = Delivery::find()
            ->byCountryId($this->country_id)
            ->orderBy(['sort_order' => SORT_ASC])
            ->active()
            ->brokerageActive()
            ->all();

        for ($i = 0; $i < sizeof($deliveries); $i++) {
            if ($deliveries[$i]->id == $this->id) {
                $score = (sizeof($deliveries) - $i) / sizeof($deliveries);
                break;
            }
        }

        if ($expressDelivery) {
            // если нам нужна экспресс доставка
            if ($this->express_delivery) {
                // и КС может ее обрабатывать экспрессы, сильно увеличим ее шансы на выборку
                $score = $score * sizeof($deliveries) + 0.01;
            } else {
                // и КС не может ее обрабатывать экспрессы, уменьшим ее шансы на выборку
                $score = $score / sizeof($deliveries);
            }
        }

        $ordersApprovedOneDayInDelivery = $this->getOrdersApprovedOneDay();

        if ($ordersApprovedOneDayInDelivery < $this->brokerage_daily_minimum) {
            $score += 1;
        } else {
            if ($ordersApprovedOneDayInDelivery > $this->brokerage_daily_maximum && $this->brokerage_daily_maximum) {
                $score -= 1;
            }
        }
        //посчитать рейтинг в зависимости от лимитов группы зипкодов курьерки
        if ($deliveryZipGroup) {
            $ordersApprovedOneDayInDeliveryByZipGroup = empty($deliveryZipGroup->zipcodes)
                ? $ordersApprovedOneDayInDelivery
                : $deliveryZipGroup->getOrdersApprovedOneDay();

            if ($ordersApprovedOneDayInDeliveryByZipGroup > $deliveryZipGroup->brokerage_daily_maximum && $deliveryZipGroup->brokerage_daily_maximum) {
                $score -= 1;
            }
        }

        return $score;
    }


    /**
     * проверяет что курьерка может доставить товары
     * по умолчанию любая КС доставляет все товары
     * но если товары указаны в стоп листе, то нет
     *
     * @param array $productIds идентификаторы продуктов
     * @return boolean
     */
    public function canDeliverProducts($productIds, $zipGroup = null)
    {
        if (!$productIds) {
            return true;
        }

        $stop = DeliveryNotProducts::find()->where([
            'delivery_id' => $this->id,
            'product_id' => $productIds
        ])->count();

        if ($stop) {
            return false;
        } elseif ($zipGroup) {
            if (DeliveryNotProductsByZip::find()
                ->where(['zipcodes_id' => $zipGroup, 'product_id' => $productIds])->count()
            ) {
                return false;
            }
        }

        return true;
    }

    /**
     * проверяет что курьерка может работать по акциям
     *
     * @param integer $packageId акция
     * @return boolean
     */
    public function canDeliverPackage($packageId)
    {

        if (!$packageId) {
            // акция не задана, пусть можно
            return true;
        }

        $availablePackages = DeliveryPackage::find()->where([
            'delivery_id' => $this->id
        ])->all();

        if (!$availablePackages) {
            // не заданы ограничения в КС, все можно
            return true;
        }

        foreach ($availablePackages as $package) {
            if ($package->package_id == $packageId) {
                // акция есть в настройках КС
                return true;
            }
        }

        return false;
    }

    /**
     * @param Order $order
     * @param OrderProduct|null $products
     * @param integer|null $package
     * @return boolean
     */
    public function canDeliveryOrder($order, $products = null, $package = null)
    {
        if (is_null($products)) {
            $products = $order->orderProducts;
        }
        $can = $this->canDeliverProducts(ArrayHelper::getColumn($products, 'product_id'));
        if ($package) {
            $can &= $this->canDeliverPackage($package);
        }

        return $can;
    }

    /**
     * проверяет что курьерка обслуживает почтовый индекс $zip
     * возвращает группу индексов курьерки или true если не задана стоимость
     * если не доставляет то вернется false
     *
     * @param string $zip
     * @param string|null $city
     * @return mixed
     */
    public function verifyZipCode($zip, $city = null)
    {

        if (DeliveryNotRegions::find()->byDeliveryId($this->id)->byZipCode($zip)->exists()) {
            return false;
        }

        if ($city) {
            if (DeliveryNotRegions::find()->byDeliveryId($this->id)->byCity($city)->exists()) {
                return false;
            }
        }

        $zipcodes = DeliveryZipcodes::find()->byDeliveryId($this->id)->all();

        foreach ($zipcodes as $zipcode) {
            if (empty($zipcode->zipcodes) && empty($zipcode->cities)) {
                //если в брокере есть группа без индексов, то считать, что курьерка доставлят по всей стране.
                if ($zipcode) {
                    $return = $zipcode;
                }
            }

            $codes = explode(",", $zipcode->zipcodes);
            if (!is_null($zip) && in_array(trim(mb_strtolower($zip)), $codes)) {
                if ($zipcode) {
                    $return = $zipcode;
                }
            } elseif ($zipcode && !empty($zipcode->cities) && !empty($city)) {
                if (in_array(trim(mb_strtolower($city)), explode(",", $zipcode->cities))) {
                    $return = $zipcode;
                }
            }
        }

        return isset($return) ? $return : false;
    }

    /**
     * Проверяет не пора ли отсылать очередное напоминание о платеже
     *
     * @param null|integer $now unixtime который считать текущим
     * @return boolean true если пора отправлять напоминалку
     */
    public function isTimeToSendReminder($now = null)
    {
        if (is_null($now)) {
            $now = time();
        }

        $scheduler = $this->getReminderScheduler();

        // внимание! игнорируем нашу самописную машинерию часовых поясов и пользуемся общесистемной
        $timezone = new \DateTimeZone ($this->country->timezone->timezone_id);

        $current = new \DateTime ();
        $current->setTimestamp($now);
        $current->setTimezone($timezone);

        $sent = new \DateTime ();
        $sent->setTimestamp((int)$this->payment_reminder_sent_at);
        $sent->setTimezone($timezone);

        return $scheduler($sent, $current);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'name', 'char_code', 'legal_name'], 'required'],
            [
                'country_id',
                'exist',
                'targetClass' => '\app\models\Country',
                'targetAttribute' => 'id',
                'message' => Yii::t('common', 'Неверное значение страны.'),
            ],
            [
                'api_class_id',
                'exist',
                'targetClass' => '\app\modules\delivery\models\DeliveryApiClass',
                'targetAttribute' => 'id',
                'message' => Yii::t('common', 'Неверное значение API-класса.'),
            ],
            [
                'workflow_id',
                'exist',
                'targetClass' => '\app\modules\order\models\OrderWorkflow',
                'targetAttribute' => 'id',
                'message' => Yii::t('common', 'Неверное значение workflow.'),
            ],
            [['name', 'char_code', 'comment'], 'filter', 'filter' => 'trim'],
            ['name', 'string', 'max' => 100],
            ['char_code', 'string', 'max' => 3],
            ['char_code', 'validateCharCode'],
            ['comment', 'string', 'max' => 1000],
            [['list_send_to_emails', 'invoice_email_list'], 'validateEmailList'],
            [['list_send_to_emails', 'invoice_email_list', 'legal_name', 'group_name'], 'string'],
            ['active', 'default', 'value' => 0],
            ['brokerage_active', 'default', 'value' => 1],
            [['active', 'brokerage_active'], 'number', 'min' => 0, 'max' => 1],
            [['auto_sending', 'express_delivery'], 'default', 'value' => 0],
            ['auto_list_generation', 'default', 'value' => 0],
            [['auto_sending', 'express_delivery'], 'number', 'min' => 0, 'max' => 1],
            [
                ['auto_list_generation', 'auto_generating_invoice', 'auto_generating_barcode'],
                'number',
                'min' => 0,
                'max' => 1
            ],
            [['is_keep', 'our_api'], 'number', 'min' => 0, 'max' => 1],
            [['payment_reminder_email_to', 'payment_reminder_email_reply_to'], 'string', 'max' => 2048],
            // todo::validate emails
            ['payment_reminder_schedule', 'default', 'value' => self::REMINDER_SCHEDULE_NEVER],
            ['payment_reminder_schedule', 'in', 'range' => array_keys(self::getReminderScheduleLabels())],
            [
                [
                    'brokerage_daily_minimum',
                    'brokerage_daily_maximum',
                    'invoice_currency_id',
                    'invoice_round_precision'
                ],
                'integer',
                'min' => 0
            ],
            [['brokerage_daily_minimum', 'brokerage_daily_maximum'], 'default', 'value' => 0],
            ['brokerage_stats_cutoff', 'integer', 'min' => 0],
            [
                [
                    'auto_list_generation_size',
                    'sort_order',
                    'sent_notification_no_contract_at',
                    'delivered_from',
                    'delivered_to',
                    'sent_notification_has_debts_at',
                    'sent_notification_in_process_at',
                    'not_send_if_no_contract',
                    'not_send_if_has_debts',
                    'not_send_if_has_debts_days',
                    'not_send_if_in_process',
                    'not_send_if_in_process_days',
                    'brokerage_min_order_price',
                    'send_to_check_undelivery',
                    'auto_pretension_generation',
                    'auto_pretension_send',
                    'pretension_send_offset_from',
                    'pretension_send_offset_to',
                    'flags'
                ],
                'integer'
            ],
            ['brokerage_stats_cutoff', 'default', 'value' => 31],
            [['not_send_if_has_debts_days', 'not_send_if_in_process_days'], 'default', 'value' => 45],
            ['delivery_intervals', 'safe'],
            [
                ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'],
                'match',
                'pattern' => '/^[0-9\:;\-]*$/',
                'message' => Yii::t('common', 'Допустимые символы: : ; - и цифры')
            ],
            [['date_format'], 'string', 'max' => 50],
            [['packager_id'], 'integer'],
            [['packager_id'], 'exist', 'targetClass' => Packager::className(), 'targetAttribute' => 'id'],
            [['amazon_queue_name', 'url'], 'string', 'max' => 255],
            [['address'], 'string', 'max' => 500],
            [['charges_values', 'auto_list_generation_columns'], 'string'],
            [['can_tracking', 'auto_send_invoice', 'packager_send_invoice', 'packager_send_label'], 'boolean'],
            [
                [
                    'can_tracking',
                    'auto_send_invoice',
                    'not_send_if_no_contract',
                ],
                'default',
                'value' => 1
            ],
            [
                [
                    'not_send_if_has_debts',
                    'not_send_if_in_process'
                ],
                'default',
                'value' => 0
            ],
            ['url', 'url', 'defaultScheme' => 'http'],
            [
                ['pretension_send_offset_from'],
                'required',
                'when' => function ($model) {
                    return ($model->auto_pretension_generation || $model->auto_pretension_send);
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'country_id' => Yii::t('common', 'Страна'),
            'name' => Yii::t('common', 'Название'),
            'group_name' => Yii::t('common', 'Группа КС'),
            'char_code' => Yii::t('common', 'Буквенный код'),
            'comment' => Yii::t('common', 'Комментарий'),
            'active' => Yii::t('common', 'Доступность'),
            'auto_sending' => Yii::t('common', 'Автоматическая отправка'),
            'created_at' => Yii::t('common', 'Дата добавления'),
            'storages' => Yii::t('common', 'Склады'),
            'api_class_id' => Yii::t('common', 'API-класс'),
            'api_class' => Yii::t('common', 'API-класс'),
            'workflow_id' => Yii::t('common', 'Приоритетное workflow'),
            'workflow' => Yii::t('common', 'Приоритетное workflow'),
            'is_keep' => Yii::t('common', 'Удерживает за свои услуги'),
            'payment_reminder_email_to' => Yii::t('common', 'Адреса (через запятую)'),
            'payment_reminder_email_reply_to' => Yii::t('common', 'Обратные адреса (через запятую)'),
            'payment_reminder_schedule' => Yii::t('common', 'Расписание отправки'),
            'brokerage_enabled' => Yii::t('common', 'Брокер включён'),
            'brokerage_stats_cutoff' => Yii::t('common', 'Срок статистики для брокера (в днях)'),
            'brokerage_daily_minimum' => Yii::t('common', 'Минимальное количество заказов за день'),
            'brokerage_daily_maximum' => Yii::t('common', 'Лимит заказов за день (0 - неограничено)'),
            'brokerage_active' => Yii::t('common', 'Используется в брокере'),
            'brokerage_min_order_price' => Yii::t('common', 'Минимальная стоимость заказа в местной валюте'),
            'list_send_to_emails' => Yii::t('common', 'Список e-mail для автоматической отправки листов'),
            'auto_list_generation' => Yii::t('common', 'Автоматическая генерация листа'),
            'auto_list_generation_size' => Yii::t('common', 'Максимальный размер листа'),
            'our_api' => Yii::t('common', 'Забор заказов по нашему API'),
            'monday' => Yii::t('common', 'понедельник'),
            'tuesday' => Yii::t('common', 'вторник'),
            'wednesday' => Yii::t('common', 'среда'),
            'thursday' => Yii::t('common', 'четверг'),
            'friday' => Yii::t('common', 'пятница'),
            'saturday' => Yii::t('common', 'суббота'),
            'sunday' => Yii::t('common', 'воскресенье'),
            'delivery_intervals' => Yii::t('common', 'Интервалы доставки'),
            'sort_order' => Yii::t('common', 'Порядок сортировки'),
            'date_format' => Yii::t('common', 'Формат присылаемых дат(в формате php)'),
            'auto_generating_invoice' => Yii::t('common', 'Автоматическая генерация инвойсов'),
            'auto_generating_barcode' => Yii::t('common', 'Автоматическая генерация штрих-кодов'),
            'packager_id' => Yii::t('common', 'Служба упаковки'),
            'amazon_queue_name' => Yii::t('common', 'Название очереди в амазоне'),
            'non_delivery_products' => Yii::t('common', 'Не доставляемые товары'),
            'delivery_packages' => Yii::t('common', 'Доставляет по акциям'),
            'express_delivery' => Yii::t('common', 'Работает с экспресс заказами'),
            'charges_values' => Yii::t('common', 'Значения переменных расходов'),
            'can_tracking' => Yii::t('common', 'Получение статусов по АПИ'),
            'url' => Yii::t('common', 'Сайт'),
            'address' => Yii::t('common', 'Адрес'),
            'invoice_email_list' => Yii::t('common', 'Список e-mail для отправки инвойсов'),
            'legal_name' => Yii::t('common', 'Юридическое название'),
            'sent_notification_no_contract_at' => Yii::t('common', 'Отправлено уведомление о незаполненности контракта'),
            'sent_notification_has_debts_at' => Yii::t('common', 'Отправлено уведомление о дебиторской задолженности'),
            'sent_notification_in_process_at' => Yii::t('common', 'Отправлено уведомление о заказах в процессе'),
            'auto_send_invoice' => Yii::t('common', 'Автоматическая отправка инвойсов по расписанию'),
            'packager_send_invoice' => Yii::t('common', 'Отправлять накладные'),
            'packager_send_label' => Yii::t('common', 'Отправлять этикетки'),
            'delivered_from' => Yii::t('common', 'Срок доставки от (в днях)'),
            'delivered_to' => Yii::t('common', 'Срок доставки до (в днях)'),
            'not_send_if_no_contract' => Yii::t('common', 'Включить запрет отправки заказов если не заполнен контракт'),
            'not_send_if_has_debts' => Yii::t('common', 'Включить запрет отправки заказов если есть не погашенная ДЗ более'),
            'not_send_if_has_debts_days' => Yii::t('common', 'дней'),
            'not_send_if_in_process' => Yii::t('common', 'Включить запрет отправки заказов если есть заказы "в процессе" более'),
            'not_send_if_in_process_days' => Yii::t('common', 'дней'),
            'invoice_currency_id' => Yii::t('common', 'Валюта инвойса'),
            'invoice_round_precision' => Yii::t('common', 'Точность округления'),
            'send_to_check_undelivery' => Yii::t('common', 'Отправка отказов в check_undelivery'),
            'auto_pretension_generation' => Yii::t('common', 'Автогенерация листов претензий с отправкой менеджеру'),
            'auto_pretension_send' => Yii::t('common', 'Автоматическая отправка листов претензий в службу доставки'),
            'pretension_send_offset_from' => Yii::t('common', 'Время отправки претензии'),
            'pretension_send_offset_to' => Yii::t('common', 'Время отправки претензии, до'),
            'auto_change_on_reject' => Yii::t('common', 'Отправлять отказы'),
        ];
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateCharCode($attribute, $params)
    {
        if (preg_match("/[^A-Z]/", $this->char_code)) {
            $this->addError($attribute, Yii::t('common',
                'Буквенный код может содержать только буквы английского алфавита в верхнем регистре.'));
        }
    }

    /**
     * @param $attribute
     */
    public function validateEmailList($attribute)
    {
        $this->$attribute = $this->emailListToString($this->$attribute);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveryInvoiceOptions()
    {
        return $this->hasMany(DeliveryInvoiceOptions::className(), ['delivery_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApiClass()
    {
        return $this->hasOne(DeliveryApiClass::className(), ['id' => 'api_class_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartnerCountry()
    {
        return $this->hasOne(PartnerCountry::className(), ['country_id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveryStorages()
    {
        return $this->hasMany(DeliveryStorage::className(), ['delivery_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveryContract()
    {
        return $this->hasMany(DeliveryContract::className(), ['delivery_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStorages()
    {
        return $this->hasMany(Storage::className(), ['id' => 'storage_id'])
            ->via('deliveryStorages');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkflow()
    {
        return $this->hasOne(OrderWorkflow::className(), ['id' => 'workflow_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTimings()
    {
        return $this->hasMany(DeliveryTiming::className(), ['delivery_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserDelivery()
    {
        return $this->hasMany(ApiUserDelivery::className(), ['delivery_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPackager()
    {
        return $this->hasOne(Packager::className(), ['id' => 'packager_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductSku()
    {
        return $this->hasMany(DeliverySku::className(), ['delivery_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveryContracts()
    {
        return $this->hasMany(DeliveryContract::className(), ['delivery_id' => 'id'])->inverseOf('delivery');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveryContacts()
    {
        return $this->hasMany(DeliveryContacts::className(), ['delivery_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveryRequests()
    {
        return $this->hasMany(DeliveryRequest::className(), ['delivery_id' => 'id'])->inverseOf('delivery');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveryAutoChangeReasons()
    {
        return $this->hasMany(DeliveryAutoChangeReason::className(), ['from_delivery_id' => 'id']);
    }

    /**
     * @return Transmitter
     */
    public function getApiTransmitter()
    {
        if (empty($this->apiTransmitter)) {
            $this->apiTransmitter = new Transmitter($this);
        }

        return $this->apiTransmitter;
    }

    /**
     * @return bool
     */
    public function hasApiGenerateList()
    {
        if ($this->apiClass && $this->apiClass->active) {
            $apiClass = $this->getApiTransmitter()->getAdaptee()->getApiClass();

            if (method_exists($apiClass, 'generateList')) {
                return true;
            }
        }

        return false;
    }


    /**
     * @return bool
     */
    public function hasApiBatchGetOrdersInfo()
    {
        if ($this->apiClass && $this->apiClass->active) {
            $apiClass = $this->getApiTransmitter()->getAdaptee()->getApiClass();

            if (method_exists($apiClass, 'batchGetOrdersInfo')) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function hasApiBatchGetOrdersInfoForArray()
    {
        if ($this->apiClass && $this->apiClass->active) {
            return $this->getApiTransmitter()->hasBatchGetOrderInfoForArray();
        }

        return false;
    }

    /**
     * @return bool
     */
    public function hasApiGetOrdersInfoForArray()
    {
        if ($this->apiClass && $this->apiClass->active) {
            return $this->getApiTransmitter()->hasGetOrderInfoForArray();
        }

        return false;
    }


    /**
     * @return bool
     */
    public function hasApiGetSortedOrderIds()
    {
        if ($this->apiClass && $this->apiClass->active) {
            return $this->getApiTransmitter()->hasGetGetSortedOrderIds();
        }

        return false;
    }


    /**
     * @return bool
     */
    public function hasApiCanGetOrderInfoForList()
    {
        if ($this->apiClass && $this->apiClass->active) {
            $apiClass = $this->getApiTransmitter()->getAdaptee()->getApiClass();

            if (method_exists($apiClass, 'canGetOrderInfoForList')) {
                return true;
            }
        }

        return false;
    }

    public function canGetOrderInfoForListViaApiTransmitter()
    {
        if (!$this->hasApiCanGetOrderInfoForList()) {
            return false;
        }
        return $this->getApiTransmitter()->canGetOrderInfoForList();
    }


    public function getBatchOrdersInfoViaApiTransmitter(&$requests)
    {
        if (!$this->hasApiBatchGetOrdersInfo()) {
            return false;
        }
        return $this->getApiTransmitter()->batchGetOrdersInfo($requests);
    }

    public function getBatchOrderInfoForArrayViaApiTransmitter($orders)
    {
        if (!$this->hasApiBatchGetOrdersInfoForArray()) {
            return false;
        }
        return $this->getApiTransmitter()->batchGetOrderInfoForArray($orders);
    }

    public function getOrderInfoForArrayViaApiTransmitter($order)
    {
        if (!$this->hasApiGetOrdersInfoForArray()) {
            return false;
        }
        return $this->getApiTransmitter()->getOrderInfoForArray($order);
    }

    /**
     * Получение кастомной стоимости доставки
     * @param OrderProductInterface[] $orderProducts
     * @return array|bool
     */
    public function getCustomDeliveryPrice($orderProducts)
    {
        if (!$this->hasCustomDeliveryPrice()) {
            return null;
        }
        return $this->getApiTransmitter()->getCustomDeliveryPrice($orderProducts);
    }


    /**
     * @return bool
     */
    public function hasApiGenerateLabel()
    {
        if ($this->apiClass && $this->apiClass->active) {
            $apiClass = $this->getApiTransmitter()->getAdaptee()->getApiClass();

            if (method_exists($apiClass, 'generateLabel')) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function hasApiGenerateLabelByOrder()
    {
        if ($this->apiClass && $this->apiClass->active) {
            $apiClass = $this->getApiTransmitter()->getAdaptee()->getApiClass();
            if ($apiClass instanceof DeliveryApiClassLabelInterface) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function hasApiGenerateManifestByOrders()
    {
        if ($this->apiClass && $this->apiClass->active) {
            $apiClass = $this->getApiTransmitter()->getAdaptee()->getApiClass();
            if ($apiClass instanceof DeliveryApiClassManifestInterface) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param integer $currentOffset
     * @return boolean
     */
    public function allowedTiming($currentOffset)
    {
        if (empty($this->timings)) {
            return true;
        }

        foreach ($this->timings as $timing) {
            if ($currentOffset >= $timing->offset_from && $timing->offset_to >= $currentOffset) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param string $str
     * @return array
     */
    protected static function extractEmailsFromString($str)
    {
        $emails = preg_split("/[, ;]+/", $str);
        return array_filter($emails, function ($email) {
            return filter_var($email, FILTER_VALIDATE_EMAIL);
        });
    }

    /**
     * @param string|string[] $value
     * @return string
     */
    protected function emailListToString($value)
    {
        if (!is_array($value)) {
            $value = [$value];
        }

        $buffer = [];
        foreach ($value as $email) {
            $buffer = array_merge($buffer, self::extractEmailsFromString($email));
        }

        return implode(',', $buffer);
    }

    /**
     * @return string[]
     */
    public function getEmailList()
    {
        return self::extractEmailsFromString($this->list_send_to_emails);
    }

    /**
     * @param string|string[] $list
     */
    public function setEmailList($list)
    {
        $this->list_send_to_emails = $this->emailListToString($list);
    }

    /**
     * @return string[]
     */
    public function getInvoiceEmailList()
    {
        return self::extractEmailsFromString($this->invoice_email_list);
    }

    /**
     * @param string|string[] $list
     */
    public function setInvoiceEmailList($list)
    {
        $this->invoice_email_list = $this->emailListToString($list);
    }

    /**
     * @return array|null
     */
    public function getProductSkuList()
    {
        if (is_null($this->productSkuList)) {
            $this->productSkuList = ArrayHelper::map($this->getProductSku()->all(), 'product_id', 'sku');
        }
        return $this->productSkuList;
    }

    /**
     * разбор строки с интервалами доставки, проверка правильности интервалов, возврат строки JSON либо false
     *
     * @return boolean|string
     */
    public function getJsonDeliveryIntervals()
    {
        $resultArray = null;
        $result = $this->getJsonDeliveryIntervalsOneDay('monday', $this->monday);
        if ($result === false) {
            return false;
        } elseif ($result !== true) {
            $resultArray[$this->mapDays['monday']] = $result;
        }

        $result = $this->getJsonDeliveryIntervalsOneDay('tuesday', $this->tuesday);
        if ($result === false) {
            return false;
        } elseif ($result !== true) {
            $resultArray[$this->mapDays['tuesday']] = $result;
        }

        $result = $this->getJsonDeliveryIntervalsOneDay('wednesday', $this->wednesday);
        if ($result === false) {
            return false;
        } elseif ($result !== true) {
            $resultArray[$this->mapDays['wednesday']] = $result;
        }

        $result = $this->getJsonDeliveryIntervalsOneDay('thursday', $this->thursday);
        if ($result === false) {
            return false;
        } elseif ($result !== true) {
            $resultArray[$this->mapDays['thursday']] = $result;
        }

        $result = $this->getJsonDeliveryIntervalsOneDay('friday', $this->friday);
        if ($result === false) {
            return false;
        } elseif ($result !== true) {
            $resultArray[$this->mapDays['friday']] = $result;
        }

        $result = $this->getJsonDeliveryIntervalsOneDay('saturday', $this->saturday);
        if ($result === false) {
            return false;
        } elseif ($result !== true) {
            $resultArray[$this->mapDays['saturday']] = $result;
        }

        $result = $this->getJsonDeliveryIntervalsOneDay('sunday', $this->sunday);
        if ($result === false) {
            return false;
        } elseif ($result !== true) {
            $resultArray[$this->mapDays['sunday']] = $result;
        }

        return json_encode($resultArray);
    }

    /**
     * @param string $day
     * @return string
     */
    public function getIntervalsOneDay($day)
    {
        $intervals = json_decode($this->delivery_intervals, true);
        try {
            return implode(';', $intervals[$this->mapDays[$day]]);
        } catch (\Exception $e) {
            return '';
        }
    }

    /**
     * текущий выбранный в настройках планировщик
     *
     * @return callable function ($sentAt, $now)
     */
    protected function getReminderScheduler()
    {
        $schedulers = [
            self::REMINDER_SCHEDULE_NEVER => function ($sentAt, $now) {
                return false;
            },
            self::REMINDER_SCHEDULE_END_OF_WEEK => [$this, 'schedulerEndOfWeek'],
            self::REMINDER_SCHEDULE_TWICE_A_MONTH => [$this, 'schedulerTwiceAMonth'],
            self::REMINDER_SCHEDULE_END_OF_MONTH => [$this, 'schedulerEndOfMonth'],
        ];

        return $schedulers[$this->payment_reminder_schedule];
    }

    /**
     * разбор строки с интервалами доставки, проверка правильности интервалов - для одного дня
     * @param string $day
     * @param string $value
     * @return boolean|array
     */
    private function getJsonDeliveryIntervalsOneDay($day, $value)
    {
        try {
            $value = trim($value, ';');
            if (!$value) {
                return true;
            }
            $intervalsArray = null;
            $intervals = explode(';', $value);
            foreach ($intervals as $interval) {
                $intervalPair = explode('-', $interval);
                if (count($intervalPair) != 2) {
                    $this->addError($day, Yii::t('common', 'Интервалы доставки не состоят из пары значений'));
                    return false;
                }
                $intervalPairHM = explode(':', $intervalPair[0]);
                if (count($intervalPairHM) == 2) {
                    if ($intervalPairHM[0] < 0 || $intervalPairHM[0] > 23) {
                        $this->addError($day, Yii::t('common', 'Часы от 0 до 23'));
                        return false;
                    }
                    if ($intervalPairHM[1] < 0 || $intervalPairHM[1] > 59) {
                        $this->addError($day, Yii::t('common', 'Минуты от 0 до 59'));
                        return false;
                    }
                } else {
                    if ($intervalPairHM[0] < 0 || $intervalPairHM[0] > 23) {
                        $this->addError($day, Yii::t('common', 'Часы от 0 до 23'));
                        return false;
                    }
                }

                $intervalsArray[] = $interval;
            }
            return $intervalsArray;
        } catch (\Exception $e) {
            $this->addError($day, Yii::t('common', $e->getMessage()));
            return false;
        }
    }

    /**
     * @return mixed
     */
    public function getChargesValues()
    {
        return json_decode($this->charges_values, true);
    }

    /**
     * @param $array
     */
    public function setChargesValues($array)
    {
        $this->charges_values = json_encode($array);
    }

    /**
     * @return array
     */
    public function getAutoListGenerationColumns(): array
    {
        $columns = json_decode($this->auto_list_generation_columns, true);
        if (empty($columns)) {
            $columns = [];
        }
        return $columns;
    }

    /**
     * @param array|null $columns
     * @return $this
     */
    public function setAutoListGenerationColumns(?array $columns)
    {
        $this->auto_list_generation_columns = json_encode($columns, JSON_UNESCAPED_UNICODE);
        return $this;
    }

    /*
     * SortableGridView::widget
     * Add new behavior in the AR model
     * https://github.com/himiklab/yii2-sortable-grid-view-widget
     * */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['sort'] = [
            'class' => SortableGridBehavior::className(),
            'sortableAttribute' => 'sort_order'
        ];
        return $behaviors;
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        if (is_array($this->auto_list_generation_columns)) {
            $this->setAutoListGenerationColumns($this->auto_list_generation_columns);
        }
        return parent::beforeValidate();
    }

    /**
     * @return self[]
     */
    public static function getUserDeliveries()
    {
        return self::find()
            ->joinWith(['country'])
            ->select([
                'id' => Delivery::tableName() . '.id',
                'name' => 'concat(' . Country::tableName() . '.name, " ", ' . Delivery::tableName() . '.name)',
            ])
            ->byCountryId(Yii::$app->user->getCountry()->id)
            ->active()
            ->all();
    }

    /**
     * @param $delivery_id
     * @param $from
     * @param $to
     * @return DeliveryContract|array|null
     */
    public function getActiveContractByDelivery($delivery_id, $from, $to)
    {
        $to = !is_null($to) ? $to : time();
        return DeliveryContract::find()
            ->byDeliveryId($delivery_id)
            ->byDates(
                $from ? Yii::$app->formatter->asDate($from, 'php:Y-m-d') : null,
                Yii::$app->formatter->asDate($to, 'php:Y-m-d')
            )
            ->orderBy(['id' => SORT_DESC])
            ->one();
    }

    /**
     * @return DeliveryContract|null
     */
    public function getActiveContract()
    {
        return DeliveryContract::find()
            ->byDeliveryId($this->id)
            ->byDates(date('Y-m-d'), date('Y-m-d'))
            ->one();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartners()
    {
        return $this->hasMany(DeliveryPartner::className(), ['delivery_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveryNotRegions()
    {
        return $this->hasMany(DeliveryNotRegions::className(), ['delivery_id' => 'id']);
    }

    /**
     * @return mixed
     */
    public function getStorageExchangeCode()
    {

        if ($this->storages) {
            foreach ($this->storages as $storage) {
                return $storage->getExchangeCode();
            }
        }

        $defaultStorage = Storage::find()->byCountryId($this->country_id)->isDefault()->one();
        if ($defaultStorage) {
            return $defaultStorage->getExchangeCode();
        }

        return 0;
    }

    /**
     * @return Storage|false
     */
    public function getOurStorage()
    {
        if ($this->storages) {
            foreach ($this->storages as $storage) {
                if ($storage->isTypeOur()) {
                    return $storage;
                }
            }
        }
        return false;
    }

    /**
     * @return bool
     */
    public function isTimeToMakePretension()
    {
        if (!$this->auto_pretension_generation || is_null($this->pretension_send_offset_from) || is_null($this->pretension_send_offset_to)) {
            return false;
        }

        return Utils::checkIntersectCurrentDayTimeInterval($this->pretension_send_offset_from, $this->pretension_send_offset_to);
    }
}
