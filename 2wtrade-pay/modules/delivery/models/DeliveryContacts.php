<?php
namespace app\modules\delivery\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\modules\delivery\models\query\DeliveryContactsQuery;
use Yii;

/**
 * Class DeliveryContacts
 * @package app\modules\delivery\models
 * @property integer $id
 * @property integer $delivery_id
 * @property string $job_title
 * @property double $email
 * @property string $full_name
 * @property string $phone
 * @property string $mobile_phone
 * @property string $additional_phone
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Delivery $delivery
 */
class DeliveryContacts extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%delivery_contacts}}';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['delivery_id'], 'integer'],
            [['email'], 'email'],
            [['job_title'], 'string'],
            [['full_name'], 'string'],
            [['phone'], 'string'],
            [['mobile_phone'], 'string'],
            [['additional_phone'], 'string'],
            [
                'phone',
                'filter',
                'filter' => function ($value) {
                    return preg_replace("/[^0-9]/", "", $value);
                }
            ],
            [
                'mobile_phone',
                'filter',
                'filter' => function ($value) {
                    return preg_replace("/[^0-9]/", "", $value);
                }
            ],
            [
                'additional_phone',
                'filter',
                'filter' => function ($value) {
                    return preg_replace("/[^0-9]/", "", $value);
                }
            ],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'job_title' => Yii::t('common', 'Должность'),
            'email' => Yii::t('common', 'Электронная почта'),
            'phone' => Yii::t('common', 'Телефон'),
            'mobile_phone' => Yii::t('common', 'Мобильный телефон'),
            'additional_phone' => Yii::t('common', 'Дополнительный телефон'),
            'full_name' => Yii::t('common', 'Фамилия, имя, отчество'),
            'created_at' => Yii::t('common', 'Дата добавления'),
            'updated_at' => Yii::t('common', 'Дата изменения')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDelivery()
    {
        return $this->hasOne(Delivery::className(), ['id' => 'delivery_id']);
    }

    /**
     * @inheritdoc
     * @return DeliveryContactsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DeliveryContactsQuery(get_called_class());
    }
}