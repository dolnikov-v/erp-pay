<?php
namespace app\modules\delivery\models\query;

use app\modules\delivery\models\DeliveryContractFile;
use app\components\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[DeliveryContractFile]].
 *
 * @see DeliveryContractFile
 */
class DeliveryContractFileQuery extends ActiveQuery
{
    /**
     * @inheritdoc
     * @return DeliveryContractFile[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return DeliveryContractFile|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @param integer $deliveryContractId
     * @return $this
     */
    public function byDeliveryContract($deliveryContractId)
    {
        return $this->andWhere([DeliveryContractFile::tableName() . '.delivery_contract_id' => $deliveryContractId]);
    }
}