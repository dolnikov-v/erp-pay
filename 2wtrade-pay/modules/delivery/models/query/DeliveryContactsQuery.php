<?php
namespace app\modules\delivery\models\query;

use app\components\db\ActiveQuery;

/**
 * Class DeliveryContactsQuery
 * @package app\modules\delivery\models\query
 */
class DeliveryContactsQuery extends ActiveQuery
{
    /**
     * @param integer $deliveryId
     * @return $this
     */
    public function byDeliveryId($deliveryId)
    {
        return $this->andWhere(['delivery_id' => $deliveryId]);
    }
}