<?php

namespace app\modules\delivery\models\query;


use app\components\mongodb\ResponseActiveQuery;

/**
 * Class DeliveryRequestSendResponseQuery
 * @package app\modules\delivery\models\query
 */
class DeliveryRequestSendResponseQuery extends ResponseActiveQuery
{
    const GROUP_FIELD = 'delivery_request_id';
}