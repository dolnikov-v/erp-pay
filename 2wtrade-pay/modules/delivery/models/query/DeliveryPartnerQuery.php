<?php
namespace app\modules\delivery\models\query;

use app\components\db\ActiveQuery;
use app\modules\delivery\models\DeliveryPartner;

/**
 * Class DeliveryRequestQuery
 * @package app\modules\delivery\models\query
 * @method DeliveryPartner one($db = null)
 * @method DeliveryPartner[] all($db = null)
 */
class DeliveryPartnerQuery extends ActiveQuery
{
    /**
     * @param integer $deliveryId
     * @return $this
     */
    public function byDeliveryId($deliveryId)
    {
        return $this->andWhere(['delivery_id' => $deliveryId]);
    }

    /**
     * @param integer $countryId
     * @return $this
     */
    public function byCountryId($countryId)
    {
        $this->joinWith('delivery');
        return $this->andWhere(['delivery.country_id' => $countryId]);
    }
}
