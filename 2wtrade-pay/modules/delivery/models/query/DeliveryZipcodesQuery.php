<?php
namespace app\modules\delivery\models\query;

use app\components\db\ActiveQuery;

/**
 * Class DeliveryZipcodesQuery
 * @package app\modules\delivery\models\query
 */
class DeliveryZipcodesQuery extends ActiveQuery
{
    /**
     * @param integer $deliveryId
     * @return $this
     */
    public function byDeliveryId($deliveryId)
    {
        return $this->andWhere(['delivery_id' => $deliveryId]);
    }
}