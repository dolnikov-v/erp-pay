<?php
namespace app\modules\delivery\models\query;

use app\components\db\ActiveQuery;
use app\modules\delivery\models\DeliveryRequest;
use yii\helpers\ArrayHelper;

/**
 * Class DeliveryRequestQuery
 * @package app\modules\delivery\models\query
 * @method DeliveryRequest one($db = null)
 * @method DeliveryRequest[] all($db = null)
 */
class DeliveryRequestQuery extends ActiveQuery
{
    /**
     * @param integer $deliveryId
     * @return $this
     */
    public function byDeliveryId($deliveryId)
    {
        return $this->andWhere(['delivery_id' => $deliveryId]);
    }

    /**
     * @param integer $orderId
     * @return $this
     */
    public function byOrderId($orderId)
    {
        return $this->andWhere(['order_id' => $orderId]);
    }

    /**
     * @param string $status
     * @return $this
     */
    public function byStatus($status)
    {
        return $this->andWhere(['status' => $status]);
    }

    /**
     * @param integer $countryId
     * @return $this
     */
    public function byCountryId($countryId)
    {
        $this->joinWith('delivery');
        return $this->andWhere(['delivery.country_id' => $countryId]);
    }
}
