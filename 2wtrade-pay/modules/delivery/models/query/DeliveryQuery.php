<?php
namespace app\modules\delivery\models\query;

use app\components\db\ActiveQuery;
use app\models\PartnerCountry;
use app\modules\delivery\models\Delivery;
use yii\helpers\ArrayHelper;

/**
 * Class DeliveryQuery
 * @package app\modules\delivery\models\query
 * @method Delivery one($db = null)
 * @method Delivery[] all($db = null)
 */
class DeliveryQuery extends ActiveQuery
{
    /**
     * @var string
     */
    protected $collectionKey = 'id';

    /**
     * @var string
     */
    protected $collectionValue = 'name';

    /**
     * @param integer $countryId
     * @return $this
     */
    public function byCountryId($countryId)
    {
        return $this->andWhere([Delivery::tableName() . '.country_id' => $countryId]);
    }

    /**
     * @param integer $partnerId
     * @return $this
     */
    public function byPartnerId($partnerId)
    {
        return $this
            ->joinWith('partnerCountry')
            ->andWhere([PartnerCountry::tableName() . '.partner_id' => $partnerId]);
    }

    /**
     * @return $this
     */
    public function active()
    {
        return $this->andWhere([Delivery::tableName() . '.active' => 1]);
    }

    /**
     * @return $this
     */
    public function brokerageActive()
    {
        return $this->andWhere([Delivery::tableName() . '.brokerage_active' => 1]);
    }

    /**
     * @return $this
     */
    public function notActive()
    {
        return $this->andWhere([Delivery::tableName() . '.active' => 0]);
    }

    /**
     * @return $this
     */
    public function haveApi()
    {
        return $this->andFilterWhere([
            'or',
            Delivery::tableName() . '.api_class_id is not null',
            Delivery::tableName() . '.our_api = 1'
        ]);
    }

    /**
     * @return $this
     */
    public function autoSending()
    {
        return $this->andFilterWhere([
            'or',
            Delivery::tableName() . '.auto_sending = 1',
            Delivery::tableName() . '.our_api = 1',
            Delivery::tableName() . '.auto_list_generation = 1'
        ]);
    }
    /**
     * @return $this
     */
    public function notAutoSending()
    {
        return $this->andWhere(['auto_sending' => 0]);
    }

    /**
     * @return $this
     */
    public function autoListGeneration()
    {
        return $this->andWhere(['auto_list_generation' => 1]);
    }

    /**
     * @return $this
     */
    public function notAutoListGeneration()
    {
        return $this->andWhere(['auto_list_generation' => 0]);
    }

    /**
     * @param null $language
     * @return array
     */
    public function collection($language = null)
    {
        return ArrayHelper::map($this->all(), $this->collectionKey, $this->collectionValue);
    }

    /**
     * @param array|int $id
     * @return $this
     */
    public function byId($id)
    {
        if (!$id) return $this;
        return $this->andWhere([Delivery::tableName() . '.id' => $id]);
    }

    /**
     * @return $this
     */
    public function expressDelivery()
    {
        return $this->andWhere([Delivery::tableName() . '.express_delivery' => 1]);
    }

    /**
     * @return $this
     */
    public function autoPretensionListGeneration()
    {
        return $this->andWhere(['auto_pretension_generation' => 1]);
    }
}
