<?php

namespace app\modules\delivery\models\query;


use app\components\db\ActiveQuery;

/**
 * Class DeliveryChargesCalculatorQuery
 * @package app\modules\delivery\models\query
 */
class DeliveryChargesCalculatorQuery extends ActiveQuery
{
    /**
     * @return $this
     */
    public function active()
    {
        return $this->andWhere(['active' => 1]);
    }

    /**
     * @return $this
     */
    public function notActive()
    {
        return $this->andWhere(['active' => 0]);
    }
}