<?php
namespace app\modules\delivery\models\query;

use app\components\db\ActiveQuery;

/**
 * Class DeliveryStorageQuery
 * @package app\modules\delivery\models\query
 */
class DeliveryStorageQuery extends ActiveQuery
{
    /**
     * @param integer $deliveryId
     * @return $this
     */
    public function byDeliveryId($deliveryId)
    {
        return $this->andWhere(['delivery_id' => $deliveryId]);
    }

    /**
     * @param integer $storageId
     * @return $this
     */
    public function byStorageId($storageId)
    {
        return $this->andWhere(['storage_id' => $storageId]);
    }
}
