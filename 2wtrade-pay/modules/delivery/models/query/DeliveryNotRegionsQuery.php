<?php
namespace app\modules\delivery\models\query;

use app\components\db\ActiveQuery;
use app\modules\delivery\models\DeliveryNotRegions;

/**
 * Class DeliveryNotRegionsQuery
 * @package app\modules\delivery\models\query
 */
class DeliveryNotRegionsQuery extends ActiveQuery
{
    /**
     * @param integer $deliveryId
     * @return $this
     */
    public function byDeliveryId($deliveryId)
    {
        return $this->andWhere(['delivery_id' => $deliveryId]);
    }

    /**
     * @return $this
     */
    public function isZipCode()
    {
        return $this->andWhere(['type' => DeliveryNotRegions::TYPE_ZIPCODE]);
    }

    /**
     * @return $this
     */
    public function isCity()
    {
        return $this->andWhere(['type' => DeliveryNotRegions::TYPE_CITY]);
    }

    /**
     * @param string $zip
     * @return $this
     */
    public function byZipCode($zip)
    {
        return $this->andWhere(['region' => $zip])->isZipCode();
    }

    /**
     * @param string $city
     * @return $this
     */
    public function byCity($city)
    {
        return $this->andWhere(['region' => $city])->isCity();
    }
}