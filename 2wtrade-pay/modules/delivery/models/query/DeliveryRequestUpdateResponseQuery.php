<?php

namespace app\modules\delivery\models\query;


use app\components\mongodb\ResponseActiveQuery;

/**
 * Class DeliveryRequestUpdateQuery
 * @package app\modules\delivery\models\query
 */
class DeliveryRequestUpdateResponseQuery extends ResponseActiveQuery
{
    const GROUP_FIELD = 'delivery_request_id';
}