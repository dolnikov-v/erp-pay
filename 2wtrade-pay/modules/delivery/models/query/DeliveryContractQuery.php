<?php
namespace app\modules\delivery\models\query;

use app\modules\delivery\models\DeliveryContract;
use app\components\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[DeliveryContract]].
 *
 * @see DeliveryContract
 */
class DeliveryContractQuery extends ActiveQuery
{
    /**
     * @inheritdoc
     * @return DeliveryContract[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return DeliveryContract|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @param integer $deliveryId
     * @return $this
     */
    public function byDeliveryId($deliveryId)
    {
        if ($deliveryId) {
            return $this->andWhere([DeliveryContract::tableName() . '.delivery_id' => $deliveryId]);
        }
        return $this;
    }

    /**
     * @param $dateFrom
     * @param $dateTo
     * @return $this
     */
    public function byDates($dateFrom, $dateTo)
    {
        $firstCaseCondition = DeliveryContract::tableName() . '.date_from IS NOT NULL AND ' . DeliveryContract::tableName() . ".date_to IS NOT NULL";
        $secondCaseCondition = DeliveryContract::tableName() . '.date_from IS NOT NULL AND ' . DeliveryContract::tableName() . ".date_to IS NULL";
        $thirdCaseCondition = DeliveryContract::tableName() . '.date_from IS NULL AND ' . DeliveryContract::tableName() . ".date_to IS NOT NULL";

        $firstCaseCheck = ($dateFrom ? "'{$dateFrom}'" : 'NULL') . " >= " . DeliveryContract::tableName() . ".date_from AND " . ($dateTo ? "'{$dateTo}'" : 'NULL') . " <= " . DeliveryContract::tableName() . ".date_to";
        $secondCaseCheck = ($dateFrom ? "'{$dateFrom}'" : 'NULL') . " >= " . DeliveryContract::tableName() . ".date_from";
        $thirdCaseCheck = ($dateTo ? "'{$dateTo}'" : 'NULL') . " <= " . DeliveryContract::tableName() . ".date_to";

        return $this->andOnCondition("CASE WHEN {$firstCaseCondition} THEN {$firstCaseCheck} WHEN {$secondCaseCondition} THEN {$secondCaseCheck} WHEN {$thirdCaseCondition} THEN {$thirdCaseCheck} ELSE 1 END");
    }
}