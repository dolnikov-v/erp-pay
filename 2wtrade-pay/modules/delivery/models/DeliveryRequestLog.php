<?php
namespace app\modules\delivery\models;

use app\components\db\ActiveRecordLogCreateTime;
use app\models\User;
use app\modules\order\models\Order;
use Yii;

/**
 * Class DeliveryRequestLog
 * @property integer $id
 * @property integer $delivery_request_id
 * @property string $user_id
 * @property string $route
 * @property string $group_id
 * @property string $field
 * @property string $old
 * @property string $new
 * @property string $comment
 * @property integer $created_at
 * @property Order $order
 */
class DeliveryRequestLog extends ActiveRecordLogCreateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%delivery_request_log}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['delivery_request_id', 'group_id', 'field', 'old', 'new'], 'required'],
            [['delivery_request_id', 'created_at', 'user_id'], 'integer'],
            [['field', 'old', 'new'], 'string', 'max' => 255],
            [['comment'], 'string', 'max' => 500],
            [
                'delivery_request_id',
                'exist',
                'skipOnError' => true,
                'targetClass' => DeliveryRequest::className(),
                'targetAttribute' => ['delivery_request_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'order_id' => Yii::t('common', 'Заказ'),
            'group_id' => Yii::t('common', 'Группа'),
            'field' => Yii::t('common', 'Поле'),
            'old' => Yii::t('common', 'Старое значение'),
            'new' => Yii::t('common', 'Новое значение'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'user_id' => Yii::t('common', 'Пользователь'),
            'route' => Yii::t('common', 'Действие'),
            'comment' => Yii::t('common', 'Комментарий'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
