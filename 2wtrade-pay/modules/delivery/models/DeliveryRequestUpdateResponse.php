<?php

namespace app\modules\delivery\models;


use app\components\ModelTrait;
use app\modules\delivery\models\query\DeliveryRequestUpdateResponseQuery;
use yii\mongodb\ActiveRecord;

/**
 * Class DeliveryRequestUpdateResponse
 * @package app\modules\delivery\models
 *
 * @property integer $delivery_request_id
 * @property integer $created_at
 * @property string $request
 * @property string $response
 */
class DeliveryRequestUpdateResponse extends ActiveRecord
{
    use ModelTrait;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }

    /**
     * @return DeliveryRequestUpdateResponseQuery
     */
    public static function find()
    {
        return new DeliveryRequestUpdateResponseQuery(get_called_class());
    }

    /**
     * @return array|string
     */
    public static function collectionName()
    {
        return 'delivery_request_update_response';
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return ['_id', 'delivery_request_id', 'created_at', 'response', 'request'];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['delivery_request_id', 'created_at'], 'integer'],
            [['response', 'request'], 'string']
        ];
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($insert) {
            DeliveryRequest::updateAll(['last_update_info_at' => $this->created_at], ['id' => $this->delivery_request_id]);
        }
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        // Принудительно конвертируем респонсы в строку
        if ($this->response && !is_string($this->response)) {
            $this->response = json_encode($this->response, JSON_UNESCAPED_UNICODE);
        }
        return parent::beforeValidate();
    }
}