<?php
namespace app\modules\delivery\models;

/**
 * Class DeliveryApiLog
 * @package app\modules\delivery\models
 */
class DeliveryApiLog
{
    const TYPE_TEXT_LOG_FILE = 'log';
    const TYPE_JSON_LOG_FILE = 'json';

    const DEFAULT_TYPE = 'send';

    /**
     * @var
     */
    public $path;
    /**
     * @var
     */
    public $fileName;
    /**
     * @var
     */
    public $filePath;

    /**
     * @param string $text
     * @param $className
     * @return bool
     */
    public function saveTextLog($text = '', $className, $type = self::DEFAULT_TYPE)
    {
        $this->setPath($className,$type = self::DEFAULT_TYPE);

        $this->createLogFile('text');

        if (is_array($text)) {
            $text = print_r($text, 1);
        }

        $msg = "==================== START " . date('h:i:s') . " ====================" . PHP_EOL;
        $msg .= $text . PHP_EOL;
        $msg .= PHP_EOL . "==================== STOP ====================" . PHP_EOL;

        $this->saveFileLog($msg);

    }

    /**
     * @param array $text
     * @param $className
     */
    public function saveJSONLog($text = [], $className, $type = self::DEFAULT_TYPE)
    {
        $this->setPath($className, $type);

        $this->createLogFile('json');

        $text = json_encode($text);

        $this->saveFileLog($text);
    }

    /**
     * @param string $className
     */
    public function setPath($className = 'default', $type = self::DEFAULT_TYPE)
    {
        $this->path = __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'runtime' . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'delivery' . DIRECTORY_SEPARATOR . $className . DIRECTORY_SEPARATOR . $type . DIRECTORY_SEPARATOR;
    }

    /**
     * @param string $type
     */
    protected function setFileName($type = '')
    {
        $extension = $this->getType($type);
        $this->fileName = date('Y-m-d') . '.' . $extension;
    }

    /**
     * @param string $type
     * @return string
     */
    protected function getType($type = '')
    {
        switch ($type) {
            case 'text':
                $extension = self::TYPE_TEXT_LOG_FILE;
                break;
            case 'json':
                $extension = self::TYPE_JSON_LOG_FILE;
                break;
            default:
                $extension = self::TYPE_TEXT_LOG_FILE;
                break;
        }

        return $extension;
    }

    /**
     * set file path
     */
    protected function setFilePath()
    {
        $this->filePath = $this->path . $this->fileName;
    }

    /**
     * check and create log directory
     */
    private function createDir()
    {
        if (!file_exists($this->path)) {
            mkdir($this->path, 0777, true);
        }
    }

    /**
     * @param string $text
     */
    protected function saveFileLog($text = '')
    {
        file_put_contents($this->filePath, $text, FILE_APPEND);
    }

    /**
     * @param string $type
     */
    protected function createLogFile($type = '')
    {
        $this->setFileName($type);

        $this->setFilePath();

        $this->createDir();
    }

}