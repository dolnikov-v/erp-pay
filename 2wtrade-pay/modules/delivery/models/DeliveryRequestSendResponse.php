<?php

namespace app\modules\delivery\models;


use app\components\ModelTrait;
use app\modules\delivery\models\query\DeliveryRequestSendResponseQuery;
use yii\mongodb\ActiveRecord;

/**
 * Class DeliveryRequestSendResponse
 * @package app\modules\delivery\models
 *
 * @property integer $delivery_request_id
 * @property integer $created_at
 * @property string $response
 */
class DeliveryRequestSendResponse extends ActiveRecord
{
    use ModelTrait;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }

    /**
     * @return DeliveryRequestSendResponseQuery
     */
    public static function find()
    {
        return new DeliveryRequestSendResponseQuery(get_called_class());
    }

    /**
     * @return array|string
     */
    public static function collectionName()
    {
        return 'delivery_request_send_response';
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return ['_id', 'delivery_request_id', 'created_at', 'response'];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['delivery_request_id', 'created_at'], 'integer'],
            [['response'], 'string']
        ];
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($insert) {
            // Обновлении времени последнего полечений респонса
            DeliveryRequest::updateAll(['cs_send_response_at' => $this->created_at], ['id' => $this->delivery_request_id]);
        }
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        // Принудительно конвертируем респонсы в строку
        if ($this->response && !is_string($this->response)) {
            $this->response = json_encode($this->response, JSON_UNESCAPED_UNICODE);
        }
        return parent::beforeValidate();
    }
}