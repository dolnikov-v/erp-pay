<?php
namespace app\modules\delivery\models;

/**
 * Class DeliveryApiResponse
 * @package app\modules\delivery\models
 */
class DeliveryApiResponse
{
    const STATUS_SUCCESS = 'success';
    const STATUS_FAIL = 'fail';
    const ERROR = true;

    /**
     * @param string $message
     * @param string $response
     * @param string $typeError
     * @return array
     */
    public static function fail($message = 'Unknown error.', $typeError = DeliveryRequest::API_ERROR_SYSTEM, $response = '')
    {
        return [
            'status' => self::STATUS_FAIL,
            'message' => $message,
            'typeError' => $typeError,
            'data' => [
                'cs_response' => $response
            ]
        ];
    }

    /**
     * @param array $data
     * @return array
     */
    public static function success($data = [])
    {
        return [
            'status' => self::STATUS_SUCCESS,
            'data' => $data,
        ];
    }
}
