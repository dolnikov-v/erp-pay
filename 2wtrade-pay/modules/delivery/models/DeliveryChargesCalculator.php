<?php

namespace app\modules\delivery\models;


use app\components\ModelTrait;
use app\modules\delivery\models\query\DeliveryChargesCalculatorQuery;
use Yii;
use yii\db\ActiveRecord;

/**
 * Class DeliveryChargesTemplate
 * @package app\modules\delivery\models
 * @property integer $id
 * @property string $name
 * @property string $class_path
 * @property boolean $active
 * @property integer $created_at
 */
class DeliveryChargesCalculator extends ActiveRecord
{

    use ModelTrait;

    public $deliveries;

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%delivery_charges_calculator}}';
    }

    /**
     * @return DeliveryChargesCalculatorQuery
     */
    public static function find()
    {
        return new DeliveryChargesCalculatorQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['name', 'class_path'], 'required'],
            [['name', 'class_path'], 'string', 'max' => 100],
            [['name', 'class_path'], 'filter', 'filter' => 'trim'],
            ['active', 'boolean'],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'name' => Yii::t('common', 'Название шаблона'),
            'class_path' => Yii::t('common', 'Класс'),
            'created_at' => Yii::t('common', 'Дата добавления'),
            'active' => Yii::t('common', 'Доступность'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveryContracts()
    {
        return $this->hasMany(DeliveryContract::className(), ['charges_calculator_id' => 'id']);
    }
}