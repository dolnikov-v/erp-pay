<?php

namespace app\modules\delivery\models;


use app\components\db\ActiveRecordLogUpdateTime;
use app\models\logs\LinkData;
use app\models\Product;
use Yii;

/**
 * This is the model class for table "delivery_not_products_by_zip".
 *
 * Какие товары курьерка не доставляет
 *
 * @property integer $zipcodes_id
 * @property integer $product_id
 * @property integer $created_at
 *
 * @property Product $product
 */
class DeliveryNotProductsByZip extends ActiveRecordLogUpdateTime
{
    protected $insertLog = true;
    protected $deleteLog = true;

    /**
     * @inheritdoc
     */
    protected function getLinkData(): ?array
    {
        return [
            new LinkData(['field' => 'delivery_id', 'value' => (int)$this->deliveryZipcodes->delivery_id]),
            new LinkData(['field' => 'zipcodes_id', 'value' => (int)$this->zipcodes_id]),
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecordLogUpdateTime::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%delivery_not_products_by_zip}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['zipcodes_id', 'product_id'], 'required'],
            [['zipcodes_id', 'product_id', 'created_at'], 'integer'],
            [['zipcodes_id'], 'exist', 'skipOnError' => true, 'targetClass' => DeliveryZipcodes::className(), 'targetAttribute' => ['zipcodes_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'zipcodes_id' => Yii::t('common', 'Номер zipcodes'),
            'product_id' => Yii::t('common', 'Не доставляемый товар'),
            'created_at' => Yii::t('common', 'Дата добавления'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveryZipcodes()
    {
        return $this->hasOne(DeliveryZipcodes::className(), ['id' => 'zipcodes_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}
