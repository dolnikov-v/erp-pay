<?php

namespace app\modules\delivery\models;

use app\components\mongodb\ActiveRecord;

/**
 * Class DeliveryRequest
 * copy of DeliveryRequest
 * @package app\modules\delivery\models
 */
class DeliveryRequestDeleted extends ActiveRecord
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * @return array|string
     */
    public static function collectionName()
    {
        return 'delivery_request_deleted';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['delivery_id', 'order_id'], 'required'],
            [['status', 'tracking', 'foreign_info', 'response_info', 'comment', 'api_error', 'api_error_type', 'finance_tracking', 'partner_name', 'unshipping_reason'], 'string'],
            [['delivery_id', 'order_id', 'last_update_info_at', 'created_at', 'cron_launched_at', 'sent_at', 'second_sent_at', 'sent_clarification_at', 'returned_at', 'approved_at', 'accepted_at', 'paid_at', 'done_at', 'delivery_partner_id', 'cs_send_response_at'], 'integer'],
        ];
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return [
            '_id',
            'delivery_id',
            'order_id',
            'status',
            'tracking',
            'foreign_info',
            'last_update_info_at',
            'response_info',
            'comment',
            'api_error',
            'api_error_type',
            'cron_launched_at',
            'sent_at',
            'second_sent_at',
            'sent_clarification_at',
            'returned_at',
            'approved_at',
            'accepted_at',
            'paid_at',
            'done_at',
            'finance_tracking',
            'partner_name',
            'delivery_partner_id',
            'cs_send_response_at',
            'unshipping_reason',
            'created_at'
        ];
    }

    /**
     * @return DeliveryRequestDeleted|\yii\mongodb\ActiveQuery
     */
    public static function find()
    {
        return new DeliveryRequestDeleted(get_called_class());
    }

    /**
     * @param DeliveryRequest $deliveryRequest
     * @return bool
     */
    public static function copy($deliveryRequest)
    {
        $deliveryRequestDeleted = new self();
        $deliveryRequestDeleted->setAttributes($deliveryRequest->getAttributes(null, ['created_at']));
        $deliveryRequestDeleted->setAttribute('created_at', $deliveryRequest->updated_at);

        return $deliveryRequestDeleted->save(false);
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        return ActiveRecord::afterSave($insert, $changedAttributes);
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        return ActiveRecord::beforeSave($insert);
    }

    public function beforeDelete()
    {
        return ActiveRecord::beforeDelete();
    }
}