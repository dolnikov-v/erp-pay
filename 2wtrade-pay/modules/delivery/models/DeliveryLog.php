<?php

namespace app\modules\delivery\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\User;
use Yii;

/**
 * Class DeliveryLog
 * @package app\modules\delivery\models
 *
 * @property integer $id
 * @property integer $delivery_id
 * @property integer $user_id
 * @property string $group_id
 * @property string $field
 * @property string $old
 * @property string $new
 *
 * @property Delivery $delivery
 * @property User $user
 */
class DeliveryLog extends ActiveRecordLogUpdateTime
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%delivery_log}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecordLogUpdateTime::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['delivery_id', 'field'], 'required'],
            [['delivery_id', 'user_id', 'created_at'], 'integer'],
            [['field', 'old', 'new', 'group_id'], 'string', 'max' => 255],
            [
                ['delivery_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Delivery::className(),
                'targetAttribute' => ['delivery_id' => 'id']
            ],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'delivery_id' => Yii::t('common', 'Служба доставки'),
            'group_id' => Yii::t('common', 'Группа'),
            'field' => Yii::t('common', 'Поле'),
            'old' => Yii::t('common', 'Старое значение'),
            'new' => Yii::t('common', 'Новое значение'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'user_id' => Yii::t('common', 'Пользователь'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery | Delivery
     */
    public function getDelivery()
    {
        return $this->hasOne(Delivery::className(), ['id' => 'delivery_id']);
    }

    /**
     * @return \yii\db\ActiveQuery | User
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
