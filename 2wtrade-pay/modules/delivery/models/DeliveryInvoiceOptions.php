<?php

namespace app\modules\delivery\models;

use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "delivery_invoice_options".
 *
 * @property integer $id
 * @property integer $delivery_id
 * @property integer $from
 * @property integer $to
 * @property integer $day
 * @property string $time
 *
 * @property Delivery $delivery
 */
class DeliveryInvoiceOptions extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%delivery_invoice_options}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['day', 'from', 'to', 'delivery_id', 'time'], 'required'],
            [['from', 'to'], 'integer', 'min' => 1, 'max' => 31],
            [['day', 'from'], 'default', 'value' => 1],
            ['to', 'default', 'value' => 31],
            ['time', 'string', 'max' => 5],
            ['time', 'default', 'value' => '00:00'],
            ['time', 'time', 'format' => 'php:H:i'],
            ['delivery_id', 'exist', 'skipOnError' => true, 'targetClass' => Delivery::className(), 'targetAttribute' => ['delivery_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'delivery_id' => Yii::t('common', 'КС'),
            'day' => Yii::t('common', 'День отправки'),
            'time' => Yii::t('common', 'Время отправки'),
            'from' => Yii::t('common', 'День начала отчета'),
            'to' => Yii::t('common', 'День конца отчета'),
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDelivery()
    {
        return $this->hasOne(Delivery::className(), ['id' => 'delivery_id']);
    }
}
