<?php
namespace app\modules\delivery\models;

use app\modules\delivery\models\query\DeliveryContractFileQuery;
use app\components\db\ActiveRecordLogUpdateTime;
use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "delivery_contract_file".
 *
 * @property integer $id
 * @property string $file_name
 * @property string $system_filename
 * @property integer $delivery_contract_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property DeliveryContract $deliveryContract
 */
class DeliveryContractFile extends ActiveRecordLogUpdateTime
{
    /**
     * @var UploadedFile
     */
    public $loadFile;

    /**
     * @var string
     */
    public $filePath;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%delivery_contract_file}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['delivery_contract_id'], 'required'],
            [['delivery_contract_id', 'created_at', 'updated_at'], 'integer'],
            [['file_name', 'system_filename'], 'string', 'max' => 100],
            [['loadFile'], 'file', 'checkExtensionByMimeType' => false, 'skipOnEmpty' => false, 'extensions' => 'xls,xlsx,doc,docx,pdf,jpg,jpeg,png'],
            //[['delivery_contract_id'], 'exist', 'skipOnError' => true, 'targetClass' => DeliveryContract::className(), 'targetAttribute' => ['delivery_contract_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'file_name' => Yii::t('common', 'Файл'),
            'delivery_contract_id' => Yii::t('common', 'Номер контракта'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
            'system_filename' => Yii::t('common', 'Имя системного файла')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveryContract()
    {
        return $this->hasOne(DeliveryContract::className(), ['id' => 'delivery_contract_id']);
    }

    /**
     * @inheritdoc
     * @return DeliveryContractFileQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DeliveryContractFileQuery(get_called_class());
    }

    public function delete()
    {
        $contract = $this->deliveryContract;
        $fileName = $this->system_filename;
        $result = parent::delete();
        if ($result) {
            unlink($contract->getUploadPath(true) . DIRECTORY_SEPARATOR . $fileName);
        }
        return $result;
    }
}