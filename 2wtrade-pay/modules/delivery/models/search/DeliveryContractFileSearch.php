<?php
namespace app\modules\delivery\models\search;

use app\modules\delivery\models\DeliveryContractFile;
use yii\data\ActiveDataProvider;

/**
 * Class DeliveryContractFileSearch
 * @package app\modules\delivery\models\search
 */
class DeliveryContractFileSearch extends DeliveryContractFile
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $query = DeliveryContractFile::find()
            ->orderBy(['created_at' => SORT_ASC]);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        return $dataProvider;
    }

    /**
     * @param integer $deliveryContractId
     * @return ActiveDataProvider
     */
    public function searchByDeliveryContractId($deliveryContractId = null)
    {
        $query = DeliveryContractFile::find()
            ->byDeliveryContract($deliveryContractId)
            ->orderBy(['created_at' => SORT_ASC]);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);
        return $dataProvider;
    }
}