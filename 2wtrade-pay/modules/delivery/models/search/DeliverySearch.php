<?php
namespace app\modules\delivery\models\search;

use app\modules\delivery\models\Delivery;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * Class DeliverySearch
 * @package app\modules\delivery\models\search
 */
class DeliverySearch extends Delivery
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $query = Delivery::find()
            ->with('storages')
            ->joinWith([
                'apiClass',
                'workflow',
            ])
            ->where(['country_id' => Yii::$app->user->country->id])
            ->orderBy(['sort_order' => SORT_ASC]);

        $config = [
            'query' => $query,
            'pagination' => false
        ];

        $dataProvider = new ActiveDataProvider($config);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        return $dataProvider;
    }
}
