<?php

namespace app\modules\delivery\models\search;


use app\modules\delivery\models\DeliveryChargesCalculator;
use yii\data\ActiveDataProvider;

/**
 * Class DeliveryChargesTemplateSearch
 * @package app\modules\delivery\models\search
 */
class DeliveryChargesCalculatorSearch extends DeliveryChargesCalculator
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['active'] = SORT_DESC;
            $sort['id'] = SORT_ASC;
        }

        $query = DeliveryChargesCalculator::find()
            ->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
        $dataProvider->pagination->pageSize = 0;
        return $dataProvider;
    }
}