<?php
namespace app\modules\delivery\models\search;

use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryRequest;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * Class DeliveryRequestSearch
 * @package app\modules\delivery\models\search
 */
class DeliveryRequestSearch extends DeliveryRequest
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['delivery_id', 'order_id'], 'integer'],
            ['status', 'in', 'range' => array_keys(self::getStatusesCollection())],
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['id'] = SORT_DESC;
        }

        $query = DeliveryRequest::find()
            ->joinWith('delivery')
            ->where([Delivery::tableName() . '.country_id' => Yii::$app->user->country->id])
            ->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['delivery_id' => $this->delivery_id]);
        $query->andFilterWhere(['order_id' => $this->order_id]);
        $query->andFilterWhere(['status' => $this->status]);

        return $dataProvider;
    }
}
