<?php
namespace app\modules\delivery\models\search;

use app\modules\delivery\models\DeliveryContract;
use yii\data\ActiveDataProvider;

/**
 * Class DeliveryContractSearch
 * @package app\modules\delivery\models\search
 */
class DeliveryContractSearch extends DeliveryContract
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $query = DeliveryContract::find()
            ->orderBy(['date_from' => SORT_DESC]);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        return $dataProvider;
    }

    /**
     * @param integer $deliveryId
     * @return ActiveDataProvider
     */
    public function searchByDeliveryId($deliveryId = null)
    {
        $query = DeliveryContract::find()
            ->byDeliveryId($deliveryId)
            ->orderBy(['date_from' => SORT_DESC]);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);
        return $dataProvider;
    }
}