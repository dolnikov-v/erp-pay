<?php
namespace app\modules\delivery\models\search;

use app\modules\delivery\models\DeliveryApiClass;
use yii\data\ActiveDataProvider;

/**
 * Class DeliveryApiClassSearch
 * @package app\modules\delivery\models\search
 */
class DeliveryApiClassSearch extends DeliveryApiClass
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['active'] = SORT_DESC;
            $sort['id'] = SORT_ASC;
        }

        $query = DeliveryApiClass::find()
//            ->joinWith('deliveries')
            ->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
        $dataProvider->pagination->pageSize = 50;
        return $dataProvider;
    }
}
