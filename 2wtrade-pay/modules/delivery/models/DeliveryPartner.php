<?php

namespace app\modules\delivery\models;


use app\components\db\ActiveRecordLogUpdateTime;
use app\modules\delivery\models\query\DeliveryPartnerQuery;

/**
 * Class DeliveryPartner
 * @package app\modules\delivery\models
 *
 * @property integer $id
 * @property integer $delivery_id
 * @property string $name
 * @property integer $created_at
 *
 * @property Delivery $delivery
 */
class DeliveryPartner extends ActiveRecordLogUpdateTime
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecordLogUpdateTime::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%delivery_partner}}';
    }

    /**
     * @return DeliveryPartnerQuery
     */
    public static function find()
    {
        return new DeliveryPartnerQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['delivery_id', 'integer'],
            ['name', 'string', 'max' => 100],
            ['created_at', 'safe']
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => \Yii::t('common', '#'),
            'delivery_id' => \Yii::t('common', 'Идентификатор службы доставки'),
            'name' => \Yii::t('common', 'Название'),
            'created_at' => \Yii::t('common', 'Дата создания')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDelivery()
    {
        return $this->hasOne(Delivery::className(), ['id' => 'delivery_id']);
    }
}