<?php

namespace app\modules\delivery\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\modules\{
    callcenter\models\CallCenterCheckRequest, catalog\models\UnBuyoutReason, catalog\models\UnBuyoutReasonMapping, delivery\components\charges\ChargesCalculatorAbstract, delivery\models\query\DeliveryRequestQuery, order\models\Order, order\models\OrderStatus, packager\models\OrderPackaging, storage\models\Storage
};
use Yii;

/**
 * Class DeliveryRequest
 * @package app\modules\delivery\models
 *
 * !!!!!!!!!!!!!! НЕ ЗАБЫВАЕМ ДОБАВЛЯТЬ ПОЛЯ В DeliveryRequestDeleted !!!!!!!!!!!!!!
 *
 * @property integer $id
 * @property integer $delivery_id
 * @property integer $order_id
 * @property string $status
 * @property string $tracking
 * @property string $foreign_info // данные из апи, ответственные за последнюю смену статуса
 * @property string $last_update_info_at // дата последнего ответа КС при обновлении статусов
 * @property string $cs_send_response_at // дата последнего ответа КС при получении заказа
 * @property string $response_info // ответ от апи, нужно для хранения инфы от КС, используется не во всех классах
 * @property string $comment
 * @property string $api_error
 * @property string $api_error_type
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $cron_launched_at
 * @property integer $sent_at
 * @property integer $second_sent_at
 * @property integer $sent_clarification_at
 * @property integer $returned_at
 * @property integer $approved_at
 * @property integer $accepted_at
 * @property integer $paid_at
 * @property integer $done_at
 * @property string $finance_tracking
 * @property string $partner_name
 * @property integer $delivery_partner_id
 * @property string $unshipping_reason
 * @property Delivery $delivery
 * @property Order $order
 * @property Storage $storage
 * @property DeliveryRequestLog[] $logs
 * @property DeliveryPartner $deliveryPartner
 *
 * @property UnBuyoutReasonMapping $unBuyoutReasonMapping
 * @property DeliveryRequestUnBuyoutReason $deliveryRequestUnBuyoutReason
 * @property ChargesCalculatorAbstract $chargesCalculator
 * @property DeliveryRequestSendResponse $lastSendResponse
 * @property DeliveryRequestUpdateResponse $lastUpdateResponse
 */
class DeliveryRequest extends ActiveRecordLogUpdateTime
{
    const STATUS_PENDING = 'pending';               // еще не отослан в курьерский апи
    const STATUS_IN_PROGRESS = 'in_progress';       // отослан, нужна регулярная проверка статуса
    const STATUS_ERROR = 'error';                   // что-то сломалось до того как доставка завершилась
    const STATUS_DONE = 'done';                     // благополучно доставлено (или возвращено), дальнейшей смены статуса не предполагается
    const STATUS_ERROR_AFTER_DONE = 'done_error';   // дальнейшая смена статуса все-таки произошла
    const STATUS_IN_LIST = 'in_list';               // заказ был добавлен в лист и отправлен вручную
    const STATUS_COMPLETION = 'completion';         // комлектовка службой упаковки
    const STATUS_COMPLETED = 'completed';           // укомплектовано службой упаковки
    const STATUS_RETURN = 'return';                 // возврат

    const API_ERROR_SYSTEM = 'system';
    const API_WARNING_SYSTEM = 'system_warning';
    const API_ERROR_COURIER = 'courier';

    /**
     * @var string Выполняемый экшен, при изменение заявки
     */
    public $route;

    /**
     * @var string Комментарий для лога
     */
    public $commentForLog;

    /**
     * @var boolean Создание логов
     */
    public $enableLog = true;

    /**
     * @var null|DeliveryRequestSendResponse
     */
    protected $_lastSendResponse = null;

    /**
     * @var null|DeliveryRequestUpdateResponse
     */
    protected $_lastUpdateResponse = null;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%delivery_request}}';
    }

    /**
     * @return DeliveryRequestQuery
     */
    public static function find()
    {
        return new DeliveryRequestQuery(get_called_class());
    }

    /**
     * @return array
     */
    public static function getStatusesCollection()
    {
        return [
            self::STATUS_PENDING => Yii::t('common', 'Ожидает'),
            self::STATUS_IN_PROGRESS => Yii::t('common', 'Выполняется'),
            self::STATUS_ERROR => Yii::t('common', 'Ошибка'),
            self::STATUS_DONE => Yii::t('common', 'Завершена'),
            self::STATUS_ERROR_AFTER_DONE => Yii::t('common', 'Завершена, но что-то не так'),
            self::STATUS_IN_LIST => Yii::t('common', 'Добавлен в лист'),
            self::STATUS_COMPLETION => Yii::t('common', 'Комлектовка службой упаковки'),
            self::STATUS_COMPLETED => Yii::t('common', 'Укомплектовано службой упаковки'),
            self::STATUS_RETURN => Yii::t('common', 'Возврат'),
        ];
    }

    /**
     * @return array
     */
    public static function getTypeApiError()
    {
        return [
            self::API_ERROR_SYSTEM => Yii::t('common', 'Системная'),
            self::API_ERROR_COURIER => Yii::t('common', 'Служба доставки'),
        ];
    }

    /**
     * @return array
     */
    public static function getUnshippingReasons()
    {
        return UnBuyoutReason::find()->orderBy(['name' => SORT_ASC])->collection();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['delivery_id', 'order_id', 'status'], 'required'],
            [
                [
                    'delivery_id',
                    'order_id',
                    'last_update_info_at',
                    'created_at',
                    'updated_at',
                    'cron_launched_at',
                    'sent_at',
                    'second_sent_at',
                    'sent_clarification_at',
                    'returned_at',
                    'approved_at',
                    'accepted_at',
                    'paid_at',
                    'done_at',
                    'delivery_partner_id',
                    'cs_send_response_at'
                ],
                'integer'
            ],
            [['status', 'foreign_info', 'api_error_type'], 'string'],
            [['tracking', 'response_info', 'api_error'], 'string', 'max' => 255],
            [['comment', 'partner_name'], 'string', 'max' => 100],
            [['finance_tracking'], 'string', 'max' => 30],
            [['unshipping_reason'], 'string', 'max' => 32],
            [['order_id'], 'unique'],
            [
                ['delivery_partner_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => DeliveryPartner::className(),
                'targetAttribute' => ['delivery_partner_id' => 'id']
            ],
            [
                'delivery_id',
                'exist',
                'targetClass' => '\app\modules\delivery\models\Delivery',
                'targetAttribute' => 'id',
                'message' => Yii::t('common', 'Неверное значение курьерской службы.'),
            ],
            [
                'order_id',
                'exist',
                'targetClass' => '\app\modules\order\models\Order',
                'targetAttribute' => 'id',
                'message' => Yii::t('common', 'Неверное значение заказа.'),
            ],
            ['status', 'default', 'value' => self::STATUS_PENDING],
            ['status', 'in', 'range' => array_keys(self::getStatusesCollection())],
            ['api_error_type', 'in', 'range' => array_keys(self::getTypeApiError())],
            [['tracking', 'comment', 'api_error', 'finance_tracking', 'partner_name'], 'filter', 'filter' => 'trim'],
            [
                [
                    'sent_at',
                    'second_sent_at',
                    'sent_clarification_at',
                    'returned_at',
                    'approved_at',
                    'accepted_at',
                    'paid_at',
                    'done_at'
                ],
                'compare',
                'compareValue' => strtotime('-6 months', $this->created_at),
                'operator' => '>=',
                'type' => 'number',
                'when' => function ($model, $attribute) {
                    return $model->{$attribute} !== null;
                }
            ],
            [
                [
                    'last_update_info_at',
                    'cs_send_response_at',
                    'cron_launched_at',
                    'sent_at',
                    'second_sent_at',
                    'sent_clarification_at',
                    'returned_at',
                    'approved_at',
                    'accepted_at',
                    'paid_at',
                    'done_at'
                ],
                'compare',
                'compareValue' => strtotime("+1 day"),
                'operator' => '<=',
                'type' => 'number',
                'when' => function ($model, $attribute) {
                    return $model->{$attribute} !== null;
                }
            ],
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($this->enableLog) {
            $groupId = uniqid();

            $dirtyAttributes = $this->getDirtyAttributes();

            foreach ($dirtyAttributes as $key => $item) {
                if (in_array($key, ['cron_launched_at', 'updated_at'])) {
                    continue;
                }
                if ($item != $this->getOldAttribute($key)) {
                    $old = (string)$this->getOldAttribute($key);

                    $log = new DeliveryRequestLog();
                    $log->delivery_request_id = $this->id;
                    $log->group_id = $groupId;
                    $log->field = $key;
                    $log->old = mb_substr($old, 0, 255);
                    $log->new = mb_substr($item, 0, 255);
                    $log->user_id = isset(Yii::$app->user) ? Yii::$app->user->id : null;
                    $log->route = $this->route ?: isset(Yii::$app->controller) ? Yii::$app->controller->getRoute() : null;
                    $log->comment = $this->commentForLog;
                    $log->save();
                }
            }
        }

        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'delivery_id' => Yii::t('common', 'Служба доставки'),
            'order_id' => Yii::t('common', 'Заказ'),
            'status' => Yii::t('common', 'Статус'),
            'foreign_status' => Yii::t('common', 'Внешний статус'),
            'foreign_info' => Yii::t('common', 'Ответ курьерской службы'),
            'response_info' => Yii::t('common', 'Ответ от апи'),
            'cs_send_response_at' => Yii::t('common', 'Дата получения ответа от службы доставки при получинее заказа'),
            'last_update_info_at' => Yii::t('common', 'Дата последнего ответа от службы доставки о статусе доставки'),
            'tracking' => Yii::t('common', 'Трекер'),
            'comment' => Yii::t('common', 'Комментарий'),
            'api_error' => Yii::t('common', 'Ошибка от API'),
            'api_error_type' => Yii::t('common', 'Тип ошибки от API'),
            'created_at' => Yii::t('common', 'Дата добавления'),
            'updated_at' => Yii::t('common', 'Дата обновления'),
            'cron_launched_at' => Yii::t('common', 'Дата запуска крона'),
            'sent_at' => Yii::t('common', 'Дата отправки'),
            'second_sent_at' => Yii::t('common', 'Дата повторной отправки'),
            'sent_clarification_at' => Yii::t('common', 'Дата отправки на уточнение'),
            'delivery.name' => Yii::t('common', 'Служба доставки'),
            'partner_name' => Yii::t('common', 'Партнер КС'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDelivery()
    {
        return $this->hasOne(Delivery::className(), ['id' => 'delivery_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStorage()
    {
        return $this->hasOne(Storage::className(), ['delivery_id' => 'delivery_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogs()
    {
        return $this->hasMany(DeliveryRequestLog::className(), ['delivery_request_id' => 'id'])
            ->orderBy([
                DeliveryRequestLog::tableName() . '.created_at' => SORT_DESC,
            ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnBuyoutReasonMapping()
    {
        return $this->hasOne(UnBuyoutReasonMapping::className(), ['id' => 'reason_mapping_id'])
            ->via('deliveryRequestUnBuyoutReason');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveryRequestUnBuyoutReason()
    {
        return $this->hasOne(DeliveryRequestUnBuyoutReason::className(), ['delivery_request_id' => 'id']);
    }

    /**
     * Работает только при явном обращении и при joinWith() с флагом eagerLoading, установленным в false
     * Хз, как сделать так, чтобы можно было это дело в with() исползовать
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveryContract()
    {
        $query = $this->hasOne(DeliveryContract::className(), ['delivery_id' => 'delivery_id']);

        $requestTimeSend = 'DATE(FROM_UNIXTIME(COALESCE(' . DeliveryRequest::tableName() . '.second_sent_at, ' . DeliveryRequest::tableName() . '.sent_at, ' . DeliveryRequest::tableName() . '.created_at)))';
        $firstCaseCondition = DeliveryContract::tableName() . '.date_from IS NOT NULL AND ' . DeliveryContract::tableName() . ".date_to IS NOT NULL";
        $secondCaseCondition = DeliveryContract::tableName() . '.date_from IS NOT NULL AND ' . DeliveryContract::tableName() . ".date_to IS NULL";
        $thirdCaseCondition = DeliveryContract::tableName() . '.date_from IS NULL AND ' . DeliveryContract::tableName() . ".date_to IS NOT NULL";

        $firstCaseCheck = "{$requestTimeSend} >= " . DeliveryContract::tableName() . ".date_from AND {$requestTimeSend}" . ' <= ' . DeliveryContract::tableName() . '.date_to';
        $secondCaseCheck = "{$requestTimeSend} >= " . DeliveryContract::tableName() . '.date_from';
        $thirdCaseCheck = "{$requestTimeSend} <= " . DeliveryContract::tableName() . '.date_to';

        $query->andOnCondition("CASE WHEN {$firstCaseCondition} THEN {$firstCaseCheck} WHEN {$secondCaseCondition} THEN {$secondCaseCheck} WHEN {$thirdCaseCondition} THEN {$thirdCaseCheck} ELSE 1 END");

        return $query;
    }

    /**
     * @return array
     */
    public function sendOrderViaApiTransmitter()
    {
        return $this->delivery->apiTransmitter->sendOrder($this->order);
    }

    /**
     * @return array
     */
    public function getOrderInfoViaApiTransmitter()
    {
        return $this->delivery->apiTransmitter->getOrderInfo($this->order);
    }

    /**
     * Помечает для всех экспортируемых заказов дату отправки на уточнение
     * @param array $orderIds
     * @return boolean
     */
    public static function sendClarification($orderIds)
    {
        self::updateAll(
            ['sent_clarification_at' => time()],
            ['IN', 'order_id', $orderIds]
        );
        return true;
    }

    /**
     * @param DeliveryRequest $deliveryRequest
     * @return boolean
     */
    public function copyAttributes($deliveryRequest)
    {
        $changed = false;
        foreach ($deliveryRequest->attributes() as $attribute) {
            if ($deliveryRequest->getOldAttribute($attribute) != $this->$attribute) {
                $this->setAttribute($attribute, $deliveryRequest->getOldAttribute($attribute));
                $changed = true;
            }
        }
        return $changed;
    }


    /**
     * @param string $partnerName
     */
    public function setDeliveryPartnerId($partnerName = '') {
        $this->delivery_partner_id = null;
        if ($partnerName) {
            $deliveryPartner = DeliveryPartner::find()
                ->byDeliveryId($this->delivery_id)
                ->andWhere(['name' => $partnerName])
                ->one();

            if (!$deliveryPartner) {
                $deliveryPartner = new DeliveryPartner();
                $deliveryPartner->delivery_id = $this->delivery_id;
                $deliveryPartner->name = $partnerName;
                $deliveryPartner->save();
            }

            if ($deliveryPartner) {
                $this->delivery_partner_id = $deliveryPartner->id;
            }
        }
    }

    /**
     * Принимает и сохраняет внешнюю причину недоставки
     * @param string $foreignReason
     * @return boolean
     * @throws \Exception
     */
    public function saveUnBuyoutReason($foreignReason = '')
    {
        $foreignReason = trim($foreignReason);
        if (!empty($foreignReason)) {

            if (empty($this->unBuyoutReasonMapping) || $this->unBuyoutReasonMapping->foreign_reason != $foreignReason) {
                $unBuyoutReasonMapping = UnBuyoutReasonMapping::find()
                    ->where(['foreign_reason' => $foreignReason])
                    ->one();
                if (empty($unBuyoutReasonMapping)) {
                    $unBuyoutReasonMapping = new UnBuyoutReasonMapping();
                    $unBuyoutReasonMapping->foreign_reason = (string)$foreignReason;
                    if (!$unBuyoutReasonMapping->save()) {
                        throw new \Exception($unBuyoutReasonMapping->getFirstErrorAsString());
                    }
                }
                $deliveryRequestUnBuyoutReason = $this->deliveryRequestUnBuyoutReason;
                if (empty($deliveryRequestUnBuyoutReason)) {
                    $deliveryRequestUnBuyoutReason = new DeliveryRequestUnBuyoutReason();
                    $deliveryRequestUnBuyoutReason->delivery_request_id = $this->id;
                }
                $deliveryRequestUnBuyoutReason->reason_mapping_id = $unBuyoutReasonMapping->id;
                if (!$deliveryRequestUnBuyoutReason->save()) {
                    throw new \Exception($deliveryRequestUnBuyoutReason->getFirstErrorAsString());
                }

                // в момент получения причины недоставки = invalid address
                if ($unBuyoutReasonMapping->reason->id == UnBuyoutReason::ADDRESS_ERROR_ID) {

                    $checkIds = [];
                    $checkIds[] = $this->order->id;
                    if ($this->order->duplicate_order_id) {
                        $checkIds[] = $this->order->duplicate_order_id;
                    }

                    //если еще не отправлялся на проверку адреса
                    if (!CallCenterCheckRequest::find()
                        ->where(['order_id' => $checkIds])
                        ->andWhere(['type' => CallCenterCheckRequest::TYPE_CHECK_ADDRESS])
                        ->exists()
                    ) {

                        // если заказ в статусах 16, 17, 27
                        if ($this->order->status_id == OrderStatus::STATUS_DELIVERY_DENIAL ||
                            $this->order->status_id == OrderStatus::STATUS_DELIVERY_RETURNED ||
                            $this->order->status_id == OrderStatus::STATUS_DELIVERY_REFUND
                        ) {
                            // создаем копию заказа
                            $orderCopy = $this->order->duplicate(true);
                            $orderCopy->status_id = OrderStatus::STATUS_CC_CHECKING;
                            if (!$orderCopy->save()) {
                                $logAnswerAnswer['errors'][] = $orderCopy->getFirstErrorAsString();
                                throw new \Exception($orderCopy->getFirstErrorAsString());
                            } else {
                                // отправляем эту копию в очередь на проверку адреса
                                $orderCheckHistory = new CallCenterCheckRequest();
                                $orderCheckHistory->order_id = $orderCopy->id;
                                $orderCheckHistory->user_id = Yii::$app->user->id;
                                $orderCheckHistory->status_id = $this->order->status_id;
                                $orderCheckHistory->type = CallCenterCheckRequest::TYPE_CHECK_ADDRESS;
                                $orderCheckHistory->status = CallCenterCheckRequest::STATUS_PENDING;
                                $orderCheckHistory->call_center_id = $this->order->callCenterRequest->call_center_id;
                                if (!$orderCheckHistory->save()) {
                                    throw new \Exception($orderCheckHistory->getFirstErrorAsString());
                                }
                            }
                        }
                    }
                }
            }
        }
        return true;
    }

    /**
     * @return \app\modules\delivery\components\charges\ChargesCalculatorAbstract|null
     */
    public function getChargesCalculator()
    {
        $contracts = $this->delivery->deliveryContracts;
        $date = date('Y-m-d', $this->second_sent_at ?? $this->sent_at ?? $this->created_at);

        $chargesCalculator = null;
        foreach ($contracts as $contract) {
            if (($contract->date_from <= $date && $date <= $contract->date_to && !empty($contract->date_from) && !empty($contract->date_to))
                || (empty($contract->date_from) && !empty($contract->date_to) && $date <= $contract->date_to)
                || (empty($contract->date_to) && !empty($contract->date_from) && $date >= $contract->date_from)
                || (empty($contract->date_from) && empty($contract->date_to))
            ) {
                if ($contract->chargesCalculatorModel) {
                    $chargesCalculator = $contract->chargesCalculator;
                    break;
                }
            }
        }
        return $chargesCalculator;
    }

    /**
     * @return DeliveryRequestSendResponse
     */
    public function getLastSendResponse()
    {
        if (is_null($this->_lastSendResponse)) {
            $this->_lastSendResponse = DeliveryRequestSendResponse::find()
                ->where(['delivery_request_id' => $this->id])
                ->orderBy(['created_at' => SORT_DESC])
                ->one();
        }
        return $this->_lastSendResponse;
    }

    /**
     * @return DeliveryRequestUpdateResponse
     */
    public function getLastUpdateResponse()
    {
        if (is_null($this->_lastUpdateResponse)) {
            $this->_lastUpdateResponse = DeliveryRequestUpdateResponse::find()
                ->where(['delivery_request_id' => $this->id])
                ->orderBy(['created_at' => SORT_DESC])
                ->one();
        }
        return $this->_lastUpdateResponse;
    }

    /**
     * @return DeliveryRequestUpdateResponse[]
     */
    public function getUpdateResponses()
    {
        return DeliveryRequestUpdateResponse::find()
            ->where(['delivery_request_id' => $this->id])
            ->orderBy(['created_at' => SORT_DESC])->all();
    }

    /**
     * @return DeliveryRequestSendResponse[]
     */
    public function getSendResponses()
    {
        return DeliveryRequestSendResponse::find()
            ->where(['delivery_request_id' => $this->id])
            ->orderBy(['created_at' => SORT_DESC])->all();
    }

    /**
     * @param DeliveryRequestSendResponse $response
     * @return $this
     */
    public function setLastSendResponse(DeliveryRequestSendResponse $response)
    {
        $this->_lastSendResponse = $response;
        return $this;
    }

    /**
     * @param DeliveryRequestUpdateResponse $response
     * @return $this
     */
    public function setLastUpdateResponse(DeliveryRequestUpdateResponse $response)
    {
        $this->_lastUpdateResponse = $response;
        return $this;
    }

    /**
     * Добавление нового апдейт респонса
     *
     * @param string $response
     * @return DeliveryRequestUpdateResponse
     */
    public function addUpdateResponse(string $response): ?DeliveryRequestUpdateResponse
    {
        if(!DeliveryRequestUpdateResponse::find()->where(['delivery_request_id' => $this->id, 'response' => $response])->exists()) {
            $lastUpdateResponse = new DeliveryRequestUpdateResponse([
                'delivery_request_id' => $this->id,
                'response' => $response
            ]);
            $this->setLastUpdateResponse($lastUpdateResponse);
            return $lastUpdateResponse;
        }
        return null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveryPartner()
    {
        return $this->hasOne(DeliveryPartner::className(), ['id' => 'delivery_partner_id']);
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        // Создание копии реквеста при удалении
        if (!DeliveryRequestDeleted::copy($this)) {
            return false;
        }

        if ($this->order->status_id == OrderStatus::STATUS_LOG_ACCEPTED ||
            $this->order->status_id == OrderStatus::STATUS_LOGISTICS_REJECTED
        ) {
            $params = [
                'order_id' => $this->order->id,
                'delivery_id' => $this->delivery_id,
                'packager_id' => $this->delivery->packager_id,
            ];
            $orderPackaging = OrderPackaging::findOne($params);
            if ($orderPackaging) {
                try {
                    $orderPackaging->delete();
                } catch (\Throwable $e) {
                    $logger = Yii::$app->get('processingLogger');
                    $cronlog = $logger->getLogger([
                        'order_id' => $this->id,
                        'type' => 'delete_order_packaging',
                    ]);
                    $cronlog($e->getMessage());
                }
            }
        }

        return parent::beforeDelete();
    }
}
