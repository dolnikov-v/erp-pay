<?php
namespace app\modules\delivery\models\log;


/**
 * Class DeliveryNotProductTableLog
 * @package app\modules\delivery\models\log
 */
class DeliveryNotProductTableLog extends \app\models\logs\TableLog
{
    /**
     * @inheritdoc
     */
    protected function getLinkContent(): ?string
    {
        $links = [];
        foreach ($this->links as $link) {
            if ($link['field'] != 'delivery_id') {
                $links[] = $link;
            }
        }
        $this->links = $links;
        return parent::getLinkContent();
    }
}