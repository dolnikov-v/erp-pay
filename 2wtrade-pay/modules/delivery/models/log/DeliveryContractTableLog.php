<?php
namespace app\modules\delivery\models\log;

use app\modules\delivery\models\DeliveryContract;

/**
 * Class DeliveryContractTableLog
 * @package app\modules\delivery\models\log
 */
class DeliveryContractTableLog extends \app\models\logs\TableLog
{
    public function getContract()
    {
        return DeliveryContract::findOne(['id' => $this->id]);
    }
}