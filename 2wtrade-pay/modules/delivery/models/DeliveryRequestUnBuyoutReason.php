<?php

namespace app\modules\delivery\models;

use app\components\ModelTrait;
use app\modules\catalog\models\UnBuyoutReasonMapping;
use Yii;

/**
 * This is the model class for table "delivery_request_unbuyout_reason".
 *
 * @property integer $delivery_request_id
 * @property integer $reason_mapping_id
 *
 * @property DeliveryRequest $deliveryRequest
 * @property UnBuyoutReasonMapping $reasonMapping
 */
class DeliveryRequestUnBuyoutReason extends \yii\db\ActiveRecord
{
    use ModelTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%delivery_request_unbuyout_reason}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['delivery_request_id', 'reason_mapping_id'], 'required'],
            [['delivery_request_id', 'reason_mapping_id'], 'integer'],
            [['delivery_request_id'], 'exist', 'skipOnError' => true, 'targetClass' => DeliveryRequest::className(), 'targetAttribute' => ['delivery_request_id' => 'id']],
            [['reason_mapping_id'], 'exist', 'skipOnError' => true, 'targetClass' => UnBuyoutReasonMapping::className(), 'targetAttribute' => ['reason_mapping_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'delivery_request_id' => Yii::t('common', 'Номер'),
            'reason_mapping_id' => Yii::t('common', 'Внешняя причина'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveryRequest()
    {
        return $this->hasOne(DeliveryRequest::className(), ['id' => 'delivery_request_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReasonMapping()
    {
        return $this->hasOne(UnBuyoutReasonMapping::className(), ['id' => 'reason_mapping_id']);
    }
}
