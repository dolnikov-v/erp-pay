<?php
namespace app\modules\delivery\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\Country;
use app\modules\delivery\components\api\Transmitter;
use app\modules\delivery\models\query\DeliveryQuery;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use app\modules\order\models\OrderWorkflow;
use app\modules\storage\models\Storage;
use Yii;

/**
 * Class DeliveryTiming
 * @package app\modules\delivery\models
 * @property integer $id
 * @property integer $delivery_id
 * @property string $offset_from
 * @property string $offset_to
 * @property integer $created_at
 * @property integer $updated_at
 * @property Delivery $delivery
 */
class DeliveryTiming extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%delivery_timing}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['delivery_id', 'offset_from', 'offset_to'], 'required'],
            [
                'delivery_id',
                'exist',
                'targetClass' => '\app\modules\delivery\models\Delivery',
                'targetAttribute' => 'id',
                'message' => Yii::t('common', 'Неверное значение службы доставки.'),
            ],
            ['offset_from', 'number', 'min' => 0, 'max' => 86399],
            ['offset_to', 'number', 'min' => 0, 'max' => 86399],
            ['offset_from', 'validateOffset']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'delivery_id' => Yii::t('common', 'Служба доставки'),
        ];
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateOffset($attribute, $params)
    {
        if ($this->offset_from > $this->offset_to) {
            $this->addError($attribute, Yii::t('common', 'Неправильный интервал времени отправки.'));
        } elseif (($this->offset_from + 5 * 60) > $this->offset_to) {
            $this->addError($attribute, Yii::t('common', 'Минимальный интервал времени составляет 5 минут.'));
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDelivery()
    {
        return $this->hasOne(Delivery::className(), ['id' => 'delivery_id']);
    }
}
