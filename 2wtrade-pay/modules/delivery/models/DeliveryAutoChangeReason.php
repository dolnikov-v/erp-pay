<?php

namespace app\modules\delivery\models;

use app\components\db\ActiveRecord;
use app\models\logs\LinkData;
use Yii;

/**
 * This is the model class for table "delivery_auto_change_reason".
 *
 * @property integer $id
 * @property integer $from_delivery_id
 * @property integer $to_delivery_id
 * @property string $reason
 *
 * @property Delivery $fromDelivery
 * @property Delivery $toDelivery
 */
class DeliveryAutoChangeReason extends ActiveRecord
{
    protected $insertLog = true;
    protected $updateLog = true;
    protected $deleteLog = true;

    /**
     * @return array
     */
    protected function getLinkData(): array
    {
        return [
            new LinkData(['field' => 'from_delivery_id', 'value' => (int)$this->from_delivery_id])
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%delivery_auto_change_reason}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['from_delivery_id', 'to_delivery_id'], 'required', 'enableClientValidation' => false],
            [['from_delivery_id', 'to_delivery_id'], 'integer'],
            [['reason'], 'string', 'max' => 255],
            [
                ['from_delivery_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Delivery::className(),
                'targetAttribute' => ['from_delivery_id' => 'id']
            ],
            [
                ['to_delivery_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Delivery::className(),
                'targetAttribute' => ['to_delivery_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'from_delivery_id' => Yii::t('common', 'Из курьерской службы'),
            'to_delivery_id' => Yii::t('common', 'В другую курьерскую службу'),
            'reason' => Yii::t('common', 'Причина отказа содержит'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFromDelivery()
    {
        return $this->hasOne(Delivery::className(), ['id' => 'from_delivery_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getToDelivery()
    {
        return $this->hasOne(Delivery::className(), ['id' => 'to_delivery_id']);
    }
}
