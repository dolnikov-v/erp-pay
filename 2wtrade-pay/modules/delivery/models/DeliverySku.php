<?php

namespace app\modules\delivery\models;

use app\models\logs\LinkData;
use Yii;
use app\models\Product;
use app\components\db\ActiveRecordLogUpdateTime;
use app\modules\delivery\models\query\DeliverySkuQuery;

/**
 * This is the model class for table "delivery_sku".
 *
 * @property integer $id
 * @property integer $delivery_id
 * @property integer $product_id
 * @property string $sku
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Delivery $delivery
 * @property Product $product
 */
class DeliverySku extends ActiveRecordLogUpdateTime
{
    protected $insertLog = true;
    protected $updateLog = true;
    protected $deleteLog = true;

    /**
     * @return array|null
     */
    protected function getLinkData(): ?array
    {
        return [
            new LinkData(['field' => 'delivery_id', 'value' => (int)$this->delivery_id]),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%delivery_sku}}';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['delivery_id', 'product_id', 'created_at', 'updated_at'], 'integer'],
            [['sku'], 'string', 'max' => 255],
            [
                'delivery_id',
                'exist',
                'targetClass' => Delivery::className(),
                'targetAttribute' => 'id',
                'message' => Yii::t('common', 'Неверное значение службы доставки.'),
            ],
            [
                'product_id',
                'exist',
                'targetClass' => Product::className(),
                'targetAttribute' => 'id',
                'message' => Yii::t('common', 'Неверное значение продукта.'),
            ],
            [['product_id'], 'unique', 'targetAttribute' => ['product_id', 'delivery_id'], 'message' => Yii::t('common', 'SKU на этот продукт уже есть')]
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'delivery_id' => Yii::t('common', 'Служба доставки'),
            'product_id' => Yii::t('common', 'Продукт'),
            'sku' => Yii::t('common', 'SKU'),
            'created_at' => Yii::t('common', 'Дата добавления'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDelivery()
    {
        return $this->hasOne(Delivery::className(), ['id' => 'delivery_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @inheritdoc
     * @return DeliverySkuQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DeliverySkuQuery(get_called_class());
    }
}