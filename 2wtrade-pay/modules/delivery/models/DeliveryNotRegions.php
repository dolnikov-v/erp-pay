<?php

namespace app\modules\delivery\models;

use \app\components\db\ActiveRecord;
use app\modules\delivery\models\query\DeliveryNotRegionsQuery;
use Yii;

/**
 * This is the model class for table "delivery_not_regions".
 *
 * @property integer $id
 * @property integer $delivery_id
 * @property string $type
 * @property string $region
 * @property string $city
 *
 * @property Delivery $delivery
 */
class DeliveryNotRegions extends ActiveRecord
{
    const TYPE_ZIPCODE = 'zipcode';
    const TYPE_CITY = 'city';

    public function behaviors()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%delivery_not_regions}}';
    }

    /**
     * @inheritdoc
     * @return DeliveryNotRegionsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DeliveryNotRegionsQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['delivery_id'], 'integer'],
            [['type'], 'string', 'max' => 10],
            [['region'], 'string', 'max' => 100],
            [
                ['delivery_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Delivery::className(),
                'targetAttribute' => ['delivery_id' => 'id']
            ],
            ['type', 'in', 'range' => array_keys(self::getTypes())],
        ];
    }

    /**
     * @return array
     */
    public static function getTypes()
    {
        return [
            self::TYPE_ZIPCODE => Yii::t('common', 'Индекс'),
            self::TYPE_CITY => Yii::t('common', 'Город'),
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'delivery_id' => Yii::t('common', 'Служба доставки'),
            'type' => Yii::t('common', 'Тип региона'),
            'region' => Yii::t('common', 'Регион'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDelivery()
    {
        return $this->hasOne(Delivery::className(), ['id' => 'delivery_id']);
    }
}
