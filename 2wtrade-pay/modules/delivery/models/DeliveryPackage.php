<?php

namespace app\modules\delivery\models;


use app\components\db\ActiveRecordLogUpdateTime;
use Yii;

/**
 * This is the model class for table "delivery_package".
 *
 * По каким акциям работает курьерка
 *
 * @property integer $id
 * @property integer $delivery_id
 * @property integer $package_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Delivery $delivery
 */
class DeliveryPackage extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%delivery_package}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['delivery_id', 'package_id'], 'required'],
            [['delivery_id', 'package_id', 'created_at', 'updated_at'], 'integer'],
            [['delivery_id'], 'exist', 'skipOnError' => true, 'targetClass' => Delivery::className(), 'targetAttribute' => ['delivery_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'delivery_id' => Yii::t('common', 'Служба доставки'),
            'package_id' => Yii::t('common', 'Акция'),
            'created_at' => Yii::t('common', 'Дата добавления'),
            'updated_at' => Yii::t('common', 'Дата обновления'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDelivery()
    {
        return $this->hasOne(Delivery::className(), ['id' => 'delivery_id']);
    }
}
