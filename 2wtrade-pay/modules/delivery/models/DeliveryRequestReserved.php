<?php

namespace app\modules\delivery\models;

/**
 * Class DeliveryRequestReserved
 * copy DeliveryRequest
 * @package app\modules\delivery\models
 */
class DeliveryRequestReserved extends DeliveryRequest
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%delivery_request_reserve}}';
    }

    /**
     * @param DeliveryRequest $deliveryRequest
     * @return bool
     */
    public static function copy($deliveryRequest)
    {
        $deliveryRequestReserved = new self();
        $deliveryRequestReserved->copyAttributes($deliveryRequest);
        return $deliveryRequestReserved->save(false);
    }
}