<?php

namespace app\modules\delivery\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\modules\delivery\models\query\DeliveryApiClassQuery;
use Yii;

/**
 * Class DeliveryApiClass
 * @package app\modules\delivery\models
 * @property integer $id
 * @property string $name
 * @property string $class_path
 * @property integer $active
 * @property integer $can_tracking
 * @property int $use_amazon_queue
 * @property int $amazon_queue_name
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $fullClassPath
 * @property Delivery[] $deliveries
 */
class DeliveryApiClass extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%delivery_api_class}}';
    }

    /**
     * @return DeliveryApiClassQuery
     */
    public static function find()
    {
        return new DeliveryApiClassQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'class_path'], 'required'],
            [['name', 'class_path'], 'filter', 'filter' => 'trim'],
            ['name', 'string', 'max' => 100],
            ['class_path', 'string', 'max' => 100],
            ['active', 'default', 'value' => 0],
            ['active', 'number', 'min' => 0, 'max' => 1],
            ['use_amazon_queue', 'default', 'value' => 0],
            [['use_amazon_queue', 'can_tracking'], 'boolean'],
            ['can_tracking', 'default', 'value' => 1],
            ['amazon_queue_name', 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'name' => Yii::t('common', 'Название'),
            'class_path' => Yii::t('common', 'Класс'),
            'active' => Yii::t('common', 'Доступность'),
            'created_at' => Yii::t('common', 'Дата добавления'),
            'use_amazon_queue' => Yii::t('common', 'Использует очереди Amazon'),
            'amazon_queue_name' => Yii::t('common', 'Название очереди Amazon'),
            'can_tracking' => Yii::t('common', 'Получение статусов'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveries()
    {
        return $this->hasMany(Delivery::className(), ['api_class_id' => 'id']);
    }

    /**
     * @return string
     */
    public function getFullClassPath()
    {
        $classPath = str_replace('/', DIRECTORY_SEPARATOR, $this->class_path);
        $classPath = str_replace('\\', DIRECTORY_SEPARATOR, $classPath);

        return Yii::getAlias('@deliveryApiClasses') . $classPath;
    }
}
