<?php

namespace app\modules\delivery\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\helpers\Utils;
use app\models\logs\LinkData;
use app\modules\catalog\models\RequisiteDeliveryLink;
use app\modules\delivery\components\charges\ChargesCalculatorAbstract;
use app\modules\delivery\components\charges\ChargesCalculatorFactory;
use app\modules\delivery\models\query\DeliveryContractQuery;
use app\modules\delivery\widgets\charges\helpers\DeliveryChargesForRegionFromTable;
use PHPExcel_IOFactory;
use Yii;
use yii\helpers\Html;
use yii\web\UploadedFile;

/**
 * This is the model class for table "delivery_contract".
 *
 * @property integer $id
 * @property integer $charges_calculator_id
 * @property string $date_from
 * @property string $date_to
 * @property string $charges_values
 * @property integer $delivery_id
 * @property boolean $mutual_settlement
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $requisite_delivery_id
 * @property integer $delivered_from
 * @property integer $delivered_to
 * @property integer $paid_from
 * @property integer $paid_to
 * @property string $bank_name
 * @property string $bank_account
 * @property integer $max_delivery_period
 *
 * @property Delivery $delivery
 * @property DeliveryContractFile[] $deliveryContractFiles
 * @property array $chargesValues
 *
 * @property ChargesCalculatorAbstract $chargesCalculator
 * @property DeliveryChargesCalculator $chargesCalculatorModel
 * @property DeliveryRequest[] $deliveryRequests
 * @property RequisiteDeliveryLink[] $requisiteDeliveryLinks
 * @property string $currencyCharCode
 *
 * @property DeliveryContractTime[] $deliveryContractTimes
 */
class DeliveryContract extends ActiveRecordLogUpdateTime
{
    protected $insertLog = true;
    protected $updateLog = true;
    protected $deleteLog = true;

    /**
     * @return array|null
     */
    protected function getLinkData(): ?array
    {
        return [
            new LinkData(['field' => 'delivery_id', 'value' => (int)$this->delivery_id])
        ];
    }

    const MUTUAL_SETTLEMENT_ON = 1;
    const MUTUAL_SETTLEMENT_OFF = 0;

    /**
     * @var ChargesCalculatorAbstract
     */
    protected $chargesCalculator;

    /**
     * @var UploadedFile
     */
    public $loadFile;

    /**
     * @return ChargesCalculatorAbstract
     */
    public function getChargesCalculator()
    {
        if (empty($this->chargesCalculator)) {
            $this->chargesCalculator = ChargesCalculatorFactory::build($this);
        }

        return $this->chargesCalculator;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%delivery_contract}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_from', 'date_to'], 'safe'],
            [['charges_values'], 'string'],
            [['bank_name', 'bank_account'], 'string', 'max' => 255],
            [['delivery_id'], 'required'],
            [['mutual_settlement'], 'boolean'],
            [
                [
                    'delivery_id',
                    'created_at',
                    'updated_at',
                    'charges_calculator_id',
                    'requisite_delivery_id',
                    'delivered_from',
                    'delivered_to',
                    'paid_from',
                    'paid_to',
                    'max_delivery_period'
                ],
                'integer'
            ],
            [['date_from', 'date_to'], 'validateDates', 'skipOnEmpty' => false],
            [
                ['loadFile'],
                'file',
                'checkExtensionByMimeType' => false,
                'extensions' => 'xls,xlsx,csv,ods'
            ],
            [
                'charges_calculator_id',
                'exist',
                'targetClass' => DeliveryChargesCalculator::className(),
                'targetAttribute' => 'id',
                'message' => Yii::t('common', 'Неверное значение шаблона по расчету расходов.'),
            ],
            [
                ['delivery_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Delivery::className(),
                'targetAttribute' => ['delivery_id' => 'id']
            ],
        ];
    }

    /**
     * @param string $attribute
     */
    public function validateDates($attribute)
    {
        if ($this->date_from && $this->date_to) {
            if ($this->date_from >= $this->date_to) {
                $this->addError($attribute, Yii::t('common', 'Дата начала превышает или равна Дате окончания'));
            }
        }

        // период [-oo; +oo]
        $contractQuery = self::find()
            ->byDeliveryId($this->delivery_id)
            ->andWhere(['is', 'date_from', null])
            ->andWhere(['is', 'date_to', null]);
        if (!$this->isNewRecord) {
            $contractQuery->andWhere(['!=', 'id', $this->id]);
        }
        $contractFind = $contractQuery->limit(1)->one();
        if ($contractFind !== null) {
            $this->addError($attribute, Yii::t('common', 'Такой период уже используется в контракте {id}', ['id' => $contractFind->id]));
        }

        // период [-oo; N] ищем date_from<=N или date_to<=N
        $contractQuery = self::find()
            ->byDeliveryId($this->delivery_id)
            ->andWhere(['is', 'date_from', null])
            ->andWhere(['is not', 'date_to', null])
            ->andWhere([
                'or',
                [
                    'and',
                    "'$this->date_from' is not null",
                    ['>=', 'date_to', $this->date_from],
                ],
                [
                    'and',
                    "'$this->date_to' is not null",
                    ['>=', 'date_to', $this->date_to],
                ]
            ]);
        if (!$this->isNewRecord) {
            $contractQuery->andWhere(['!=', 'id', $this->id]);
        }
        $contractFind = $contractQuery->limit(1)->one();
        if ($contractFind !== null) {
            $this->addError($attribute, Yii::t('common', 'Такой период уже используется в контракте {id}', ['id' => $contractFind->id]));
        }

        // период [N; +oo] ищем date_from>=N или date_to>=N
        $contractQuery = self::find()
            ->byDeliveryId($this->delivery_id)
            ->andWhere(['is not', 'date_from', null])
            ->andWhere(['is', 'date_to', null])
            ->andWhere([
                'or',
                [
                    'and',
                    "'$this->date_from' is not null",
                    ['<=', 'date_from', $this->date_from],
                ],
                [
                    'and',
                    "'$this->date_to' is not null",
                    ['<=', 'date_from', $this->date_to],
                ]
            ]);
        if (!$this->isNewRecord) {
            $contractQuery->andWhere(['!=', 'id', $this->id]);
        }
        $contractFind = $contractQuery->limit(1)->one();
        if ($contractFind !== null) {
            $this->addError($attribute, Yii::t('common', 'Такой период уже используется в контракте {id}', ['id' => $contractFind->id]));
        }

        // период [N; M] ищем (date_from>=N и date_from<=M) или (date_to>=N и date_to<=M)
        $contractQuery = self::find()
            ->byDeliveryId($this->delivery_id)
            ->andWhere(['is not', 'date_from', null])
            ->andWhere(['is not', 'date_to', null])
            ->andWhere([
                'or',
                [
                    'and',
                    "'$this->date_from' is not null",
                    ['>=', 'date_from', $this->date_from],
                    ['<=', 'date_to', $this->date_from]
                ],
                [
                    'and',
                    "'$this->date_to' is not null",
                    ['>=', 'date_from', $this->date_to],
                    ['<=', 'date_to', $this->date_to]
                ]
            ]);
        if (!$this->isNewRecord) {
            $contractQuery->andWhere(['!=', 'id', $this->id]);
        }
        $contractFind = $contractQuery->limit(1)->one();
        if ($contractFind !== null) {
            $this->addError($attribute, Yii::t('common', 'Такой период уже используется в контракте {id}', ['id' => $contractFind->id]));
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'date_from' => Yii::t('common', 'Дата начала'),
            'date_to' => Yii::t('common', 'Дата окончания'),
            'charges_values' => Yii::t('common', 'Значения переменных расходов'),
            'delivery_id' => Yii::t('common', 'Номер службы доставки'),
            'mutual_settlement' => Yii::t('common', 'Взаиморасчет'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
            'charges_calculator_id' => Yii::t('common', 'Шаблон по расчету расходов на доставку'),
            'requisite_delivery_id' => Yii::t('common', 'Реквизит КС по умолчанию'),
            'delivered_from' => Yii::t('common', 'Срок доставки от (в днях)'),
            'delivered_to' => Yii::t('common', 'Срок доставки до (в днях)'),
            'paid_from' => Yii::t('common', 'Срок перечисления платежа от (в днях)'),
            'paid_to' => Yii::t('common', 'Срок перечисления платежа до (в днях)'),
            'bank_name' => Yii::t('common', 'Название банка'),
            'bank_account' => Yii::t('common', 'Номер аккаунта в банке'),
            'max_delivery_period' => Yii::t('common', 'Максимальное время доставки (в днях)'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDelivery()
    {
        return $this->hasOne(Delivery::className(), ['id' => 'delivery_id']);
    }

    /**
     * Работает только при явном обращении и при joinWith() с флагом eagerLoading, установленным в false
     * Хз, как сделать так, чтобы можно было это дело в with() исползовать
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveryRequests()
    {
        $query = $this->hasMany(DeliveryRequest::className(), ['delivery_id' => 'delivery_id']);

        $requestTimeSend = 'DATE(FROM_UNIXTIME(COALESCE(' . DeliveryRequest::tableName() . '.second_sent_at, ' . DeliveryRequest::tableName() . '.sent_at, ' . DeliveryRequest::tableName() . '.created_at)))';
        $firstCaseCondition = self::tableName() . '.date_from IS NOT NULL AND ' . self::tableName() . ".date_to IS NOT NULL";
        $secondCaseCondition = self::tableName() . '.date_from IS NOT NULL AND ' . self::tableName() . ".date_to IS NULL";
        $thirdCaseCondition = self::tableName() . '.date_from IS NULL AND ' . self::tableName() . ".date_to IS NOT NULL";

        $firstCaseCheck = "{$requestTimeSend} >= " . self::tableName() . ".date_from AND {$requestTimeSend}" . ' <= ' . self::tableName() . '.date_to';
        $secondCaseCheck = "{$requestTimeSend} >= " . self::tableName() . '.date_from';
        $thirdCaseCheck = "{$requestTimeSend} <= " . self::tableName() . '.date_to';

        $query->andOnCondition("CASE WHEN {$firstCaseCondition} THEN {$firstCaseCheck} WHEN {$secondCaseCondition} THEN {$secondCaseCheck} WHEN {$thirdCaseCondition} THEN {$thirdCaseCheck} ELSE 1 END");

        return $query;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveryContractFiles()
    {
        return $this->hasMany(DeliveryContractFile::className(), ['delivery_contract_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChargesCalculatorModel()
    {
        return $this->hasOne(DeliveryChargesCalculator::className(), ['id' => 'charges_calculator_id']);
    }

    /**
     * @inheritdoc
     * @return DeliveryContractQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DeliveryContractQuery(get_called_class());
    }

    /**
     * @param boolean $prepareDir
     * @return string
     */
    public function getUploadPath($prepareDir = false)
    {
        $dir = Yii::getAlias('@uploads') . DIRECTORY_SEPARATOR . 'delivery_contract_files';

        if ($prepareDir) {
            Utils::prepareDir($dir);
        }

        return $dir;
    }

    /**
     * @return string|boolean
     */
    public function upload()
    {
        $inputFileName = Html::getInputName(new DeliveryContractFile(), 'loadFile');
        $result = true;
        $i = 0;
        /**
         * @var UploadedFile $loadFile
         */
        $loadFile = UploadedFile::getInstanceByName($inputFileName . '[' . $i . ']');
        while ($loadFile instanceof UploadedFile) {
            $contractFile = new DeliveryContractFile();
            $contractFile->loadFile = $loadFile;
            $contractFile->delivery_contract_id = $this->id;
            if ($contractFile->validate()) {
                $contractFile->system_filename = $this->id . time() . $i . '.' . $contractFile->loadFile->extension;
                $contractFile->file_name = $contractFile->loadFile->name;
                $contractFile->filePath = $this->getUploadPath(true) . DIRECTORY_SEPARATOR . $contractFile->system_filename;
                if (!$contractFile->loadFile->saveAs($contractFile->filePath)) {
                    $result = Yii::t('common', 'Не удалось загрузить файл на сервер');
                    break;
                }
                if (!$contractFile->save(false)) {
                    $result = Yii::t('common', 'Не удалось сохранить контракт');
                    break;
                }
            } else {
                $result = $contractFile->getFirstErrorAsString();
                break;
            }
            $i++;
            $loadFile = UploadedFile::getInstanceByName($inputFileName . '[' . $i . ']');
        }
        return $result;
    }

    /**
     * @return mixed
     */
    public function getChargesValues()
    {
        return json_decode($this->charges_values, true);
    }

    /**
     * @param $array
     */
    public function setChargesValues($array)
    {
        unset($array['_csrf']);
        $this->charges_values = json_encode($array);
    }

    public function delete()
    {
        $contractFiles = DeliveryContractFile::find()->byDeliveryContract($this->id)->all();
        $filesPath = null;
        if (is_array($contractFiles)) {
            foreach ($contractFiles as $contractFile) {
                $filesPath[] = $contractFile['file_name'];
            }
        }
        $result = parent::delete();
        if ($result) {
            if (is_array($filesPath)) {
                $path = $this->getUploadPath(true) . DIRECTORY_SEPARATOR;
                foreach ($filesPath as $filePath) {
                    unlink($path . $filePath);
                }
            }
        }
        return $result;
    }

    /**
     * @param $data
     * @throws \Exception
     */
    public function loadChargesFromFile($data)
    {
        set_time_limit(0);
        $this->loadFile = UploadedFile::getInstanceByName(DeliveryChargesForRegionFromTable::fileFieldName());
        if (empty($this->loadFile)) {
            throw new \Exception(Yii::t('common', 'Файл не указан.'));
        }
        if (!$this->validate(['loadFile'])) {
            throw new \Exception($this->getFirstErrorAsString());
        }

        // удаляем из массива с данными файл
        unset($data[DeliveryChargesForRegionFromTable::fileFieldName()]);

        $regionAttribute = $data[DeliveryChargesForRegionFromTable::formName()][DeliveryChargesForRegionFromTable::regionAttributeContainerName()];
        if (!isset($data[DeliveryChargesForRegionFromTable::formName()][$regionAttribute])) {
            throw new \Exception(Yii::t('common', 'Не указана колонка с аттрибутами региона (индекс, и т.д.)'));
        }

        $columnMap = [
            $regionAttribute => $data[DeliveryChargesForRegionFromTable::formName()][$regionAttribute]
        ];

        $chargesAttributes = $data[DeliveryChargesForRegionFromTable::formName()][DeliveryChargesForRegionFromTable::chargesAttributesContainerName()];

        foreach ($chargesAttributes as $chargesAttribute) {
            $columnMap[$chargesAttribute] = $data[DeliveryChargesForRegionFromTable::formName()][$chargesAttribute];
        }

        $excelFile = PHPExcel_IOFactory::load($this->loadFile->tempName);
        $worksheet = $excelFile->getActiveSheet();
        $i = 2;

        $chargesCalculator = $this->getChargesCalculator();
        $chargesValues = $this->getChargesValues();
        $regionCharges = [];
        $lastRowNumber = $worksheet->getHighestRow();

        // проходим по файлу и формируем блоки
        while ($i <= $lastRowNumber) {
            $values = [];
            $key = [];
            foreach ($columnMap as $fieldName => $column) {
                $value = 0;
                if (!empty($column) && $chargesCalculator->hasProperty($fieldName)) {
                    $value = trim($worksheet->getCell($column . $i)->getValue());
                    if (empty($value)) {
                        $value = 0;
                    }
                }
                if ($fieldName != $regionAttribute) {
                    $key[] = $value;
                }
                $values[$fieldName] = $value;
            }
            if (!isset($values[$regionAttribute])) {
                continue;
            }

            $key = implode('_', $key);
            if (empty($values[$regionAttribute])) {
                continue;
            }
            if (!isset($regionCharges[$key])) {
                $regionCharges[$key] = $values;
                $regionCharges[$key][$regionAttribute] = array_map('trim', explode(',', $regionCharges[$key][$regionAttribute]));
            } else {
                $regionCharges[$key][$regionAttribute] = array_merge($regionCharges[$key][$regionAttribute], array_map('trim', explode(',', $values[$regionAttribute])));
            }
            $i++;
        }
        $excelFile->disconnectWorksheets();

        // добавляем сформированные блоки
        foreach ($regionCharges as $key => $values) {
            $values[$regionAttribute] = implode(',', array_unique($values[$regionAttribute]));
            foreach ($values as $attribute => $value) {
                if (!isset($chargesValues[$chargesCalculator->formName()][$attribute])) {
                    $chargesValues[$chargesCalculator->formName()][$attribute] = [];
                }
                $chargesValues[$chargesCalculator->formName()][$attribute][] = $value;
            }
        }

        // удаляем пустые блоки
        if (isset($chargesValues[$chargesCalculator->formName()][$regionAttribute]) && !empty($chargesValues[$chargesCalculator->formName()][$regionAttribute])) {
            foreach (array_keys($chargesValues[$chargesCalculator->formName()][$regionAttribute]) as $key) {
                $flag = false;
                foreach ($chargesAttributes as $chargesAttribute) {
                    if (!empty($chargesValues[$chargesCalculator->formName()][$chargesAttribute][$key])) {
                        $flag = true;
                        continue;
                    }
                }
                if (!$flag) {
                    unset($chargesValues[$chargesCalculator->formName()][$regionAttribute][$key]);
                    foreach ($chargesAttributes as $chargesAttribute) {
                        unset($chargesValues[$chargesCalculator->formName()][$chargesAttribute][$key]);
                    }
                }
            }
        }

        // на всякий, валидируем новые данные
        $chargesCalculator->load($chargesValues);
        if (!$chargesCalculator->validate()) {
            throw new \Exception($chargesCalculator->getFirstErrorAsString());
        }

        $this->chargesValues = $chargesValues;
        if (!$this->save(true, ['charges_values'])) {
            throw new \Exception($this->getFirstErrorAsString());
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequisiteDeliveryLinks()
    {
        return $this->hasMany(RequisiteDeliveryLink::className(), ['delivery_contract_id' => 'id']);
    }

    /***
     * Поиск кода валюты в переменных значениях контракта
     * @return string|null
     */
    public function getCurrencyCharCode()
    {
        if ($this->chargesValues) {
            foreach ($this->chargesValues as $key => $value) {
                if (is_array($value) && isset($value['currency_char_code'])) {
                    return $value['currency_char_code'];
                }
            }
        }
        return null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveryContractTimes()
    {
        return $this->hasMany(DeliveryContractTime::className(), ['contract_id' => 'id']);
    }

    /***
     * @param string $zip
     * @param string $city
     * @param string $province
     * @return array ['delivered_from', 'delivered_to']
     */
    public function getDeliveredTimesByRegion($zip = '', $city = '', $province = '')
    {
        return DeliveryContractTime::find()
            ->select([
                'delivered_from' => 'IFNULL(MIN(' . DeliveryContractTime::tableName() . '.delivered_from), :default_from)',
                'delivered_to' => 'IFNULL(MAX(' . DeliveryContractTime::tableName() . '.delivered_to), :default_to)',
            ])
            ->where(['contract_id' => $this->id])
            ->andWhere([
                'or',
                [
                    'and',
                    ['=', 'field_name', 'customer_zip'],
                    ['=', 'field_value', $zip]
                ],
                [
                    'and',
                    ['=', 'field_name', 'customer_city'],
                    ['=', 'field_value', $city]
                ],
                [
                    'and',
                    ['=', 'field_name', 'customer_province'],
                    ['=', 'field_value', $province]
                ],
            ])->addParams([
                ':default_from' => $this->delivered_from ?: $this->delivery->delivered_from,
                ':default_to' => $this->delivered_to ?: $this->delivery->delivered_to])
            ->asArray()
            ->one();
    }

}