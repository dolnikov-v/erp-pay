<?php

namespace app\modules\delivery\models;

use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "delivery_contract_time".
 *
 * @property integer $id
 * @property integer $contract_id
 * @property integer $delivered_from
 * @property double $delivered_to
 * @property string $field_name
 * @property string $field_value
 *
 * @property DeliveryContract $contract
 */
class DeliveryContractTime extends ActiveRecord
{
    const GROUP_TYPE_ZIP = 'customer_zip';
    const GROUP_TYPE_CITY = 'customer_city';
    const GROUP_TYPE_PROVINCE = 'customer_province';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%delivery_contract_time}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['contract_id'], 'required'],
            [['contract_id', 'delivered_from'], 'integer'],
            [['delivered_to'], 'number'],
            [['field_name', 'field_value'], 'string', 'max' => 255],
            [['contract_id'], 'exist', 'skipOnError' => true, 'targetClass' => DeliveryContract::className(), 'targetAttribute' => ['contract_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'contract_id' => Yii::t('common', 'Контракт'),
            'delivered_from' => Yii::t('common', 'Срок доставки от (в днях)'),
            'delivered_to' => Yii::t('common', 'Срок доставки до (в днях)'),
            'field_name' => Yii::t('common', 'Название поля'),
            'field_value' => Yii::t('common', 'Значение поля'),
        ];
    }

    /**
     * @return array
     */
    public static function getTypes()
    {
        return [
            self::GROUP_TYPE_ZIP => Yii::t('common', 'Индекс'),
            self::GROUP_TYPE_CITY => Yii::t('common', 'Город'),
            self::GROUP_TYPE_PROVINCE => Yii::t('common', 'Провинция'),
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContract()
    {
        return $this->hasOne(DeliveryContract::className(), ['id' => 'contract_id']);
    }
}
