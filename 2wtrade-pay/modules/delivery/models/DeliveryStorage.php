<?php
namespace app\modules\delivery\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\modules\delivery\models\query\DeliveryStorageQuery;
use app\modules\storage\models\Storage;
use Yii;

/**
 * Class DeliveryStorage
 * @package app\modules\delivery\models
 * @property integer $id
 * @property string $delivery_id
 * @property string $storage_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property Delivery $delivery
 * @property Storage $storage
 */
class DeliveryStorage extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%delivery_storage}}';
    }

    /**
     * @return DeliveryStorageQuery
     */
    public static function find()
    {
        return new DeliveryStorageQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['delivery_id', 'storage_id'], 'required'],
            [
                'delivery_id',
                'exist',
                'targetClass' => '\app\modules\delivery\models\Delivery',
                'targetAttribute' => 'id',
                'message' => Yii::t('common', 'Неверное значение службы доставки.'),
            ],
            [
                'storage_id',
                'exist',
                'targetClass' => '\app\modules\storage\models\Storage',
                'targetAttribute' => 'id',
                'message' => Yii::t('common', 'Неверное значение склада.'),
            ],
            ['storage_id', 'unique', 'targetAttribute' => ['delivery_id', 'storage_id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'delivery_id' => Yii::t('common', 'Служба доставки'),
            'storage_id' => Yii::t('common', 'Склад'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDelivery()
    {
        return $this->hasOne(Delivery::className(), ['id' => 'delivery_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStorage()
    {
        return $this->hasOne(Storage::className(), ['id' => 'storage_id']);
    }
}
