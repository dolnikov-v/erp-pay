<?php

namespace app\modules\delivery;

use app\modules\delivery\abstracts\BrokerInterface;

/**
 * Class DeliveryModule
 * @package app\modules\delivery
 *
 * @property BrokerInterface $broker
 */
class DeliveryModule extends \app\components\base\Module
{

}
