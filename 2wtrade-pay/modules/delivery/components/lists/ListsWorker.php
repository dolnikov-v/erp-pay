<?php

namespace app\modules\delivery\components\lists;

use app\helpers\Utils;
use app\models\User;
use app\modules\administration\models\CrontabTaskLog;
use app\modules\administration\models\CrontabTaskLogAnswer;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\components\ExcelBuilder;
use app\modules\order\components\ExcelBuilderFactory;
use app\modules\order\components\InvoiceBuilder;
use app\modules\order\models\Order;
use app\modules\order\models\OrderLogisticList;
use app\modules\order\models\OrderLogisticListExcel;
use app\modules\order\models\OrderLogisticListInvoice;
use app\modules\order\models\OrderLogisticListInvoiceArchive;
use app\modules\order\models\OrderLogisticListRequest;
use app\modules\order\models\OrderStatus;
use app\components\base\Component;
use yii\db\ActiveQuery;
use Yii;
use yii\base\Exception;
use app\models\Notification;

/**
 * Class ListsWorker
 * @package app\modules\delivery\components\lists
 */
class ListsWorker extends Component
{
    /** За сколько секунд до отправки стоит начинать генерировать файлы */
    const TIME_OFFSET_FOR_GENERATE = 0;

    /** Интервал времени (секунды) на отправку листов по умолчанию */
    const DEFAULT_TIME_INTERVAL_TO_SEND = 3600;

    /** Время (секунды) начала отправки листов по умолчанию */
    const DEFAULT_START_TIME_TO_SEND = 3600;

    /**
     * @var bool
     */
    public $express = false;

    /**
     * @var bool
     */
    public $correction = false;

    /**
     * @var bool
     */
    public $timeToGenerate = false;

    /**
     * @var bool
     */
    public $timeToSend = false;

    /**
     * @var Delivery
     */
    public $delivery;

    /**
     * @var OrderLogisticListExcel[]
     */
    public $lists;

    public $timeBorder = 0;

    /**
     * @var array
     */
    public $ordersIds = [];

    /**
     * Инициализация таймингов
     */
    public function init()
    {
        if ($this->express || $this->correction) {
            $this->timeToGenerate = true;
            $this->timeToSend = true;
            $this->timeBorder = time();
        } else {
            if (!empty($this->delivery->timings)) {
                foreach ($this->delivery->timings as $timing) {
                    $this->checkTimings($timing->offset_from, $timing->offset_to);
                }
            } else {
                $this->checkTimings(static::DEFAULT_START_TIME_TO_SEND, static::DEFAULT_START_TIME_TO_SEND + abs(static::DEFAULT_TIME_INTERVAL_TO_SEND));
            }

            if (!$this->timeToSend) {
                // если есть листы для отправки созданные более часа назад и по какой-то причине еще не отправленные, то отравляем
                if ($this->buildQueryForFindPreparedToSentLists()->andWhere([
                    '<=',
                    OrderLogisticListExcel::tableName() . '.created_at',
                    time() - abs(static::TIME_OFFSET_FOR_GENERATE)
                ])->exists()) {
                    $this->timeToSend = true;
                }
            }
        }
    }

    /**
     * Проверка времени
     *
     * @param int $from
     * @param int $to
     */
    protected function checkTimings(int $from, int $to)
    {
        if (Utils::checkIntersectCurrentDayTimeInterval($from - abs(static::TIME_OFFSET_FOR_GENERATE), $to)) {
            $this->timeToGenerate = true;
            $this->timeBorder = strtotime('midnight') + ($from - abs(static::TIME_OFFSET_FOR_GENERATE));
        }
        if (Utils::checkIntersectCurrentDayTimeInterval($from, $to)) {
            $this->timeToSend = true;
        }
    }

    /**
     * Генерация листов
     * @param $log CrontabTaskLog
     * @param $forceGenerate boolean
     * @return boolean
     * @throws \yii\db\Exception
     */
    public function generate($log, $forceGenerate = false)
    {
        if (!$this->timeToGenerate && !$forceGenerate) {
            return false;
        }

        $query = Order::find()->byCountryId($this->delivery->country_id);
        if ($this->ordersIds) {
            $query->andWhere([
                Order::tableName() . '.id' => $this->ordersIds,
            ]);
        } else {
            $statuses = OrderStatus::getPreviousStatusesForStatus(OrderStatus::STATUS_LOG_GENERATED,
                $this->delivery->country);
            $query->andWhere([
                Order::tableName() . '.status_id' => $statuses,
            ]);
            $query->andWhere(['<', Order::tableName() . '.updated_at', $this->timeBorder]);
        }
        $query->joinWith([
            'callCenterRequest',
            'deliveryRequest',
        ]);

        $query->andWhere([
            'or',
            [
                'and',
                [Order::tableName() . '.pending_delivery_id' => $this->delivery->id],
                ['is', DeliveryRequest::tableName() . '.delivery_id', null]
            ],
            [
                DeliveryRequest::tableName() . '.delivery_id' => $this->delivery->id
            ]
        ]);

        if ($this->express) {
            $query->expressDelivery();
        }

        if (!empty($this->delivery->auto_list_generation_size)) {
            $query->limit($this->delivery->auto_list_generation_size);
        }

        while ($query->count() > 0) {

            $badIds = [];

            $list = new OrderLogisticList();
            $list->country_id = $this->delivery->country_id;
            $list->delivery_id = $this->delivery->id;
            $list->user_id = 1;

            $commit = false;
            $successIds = [];
            $hasError = false;

            $logAnswer = new CrontabTaskLogAnswer();
            $logAnswer->log_id = $log->id;
            $logAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
            $logAnswer->data = [
                'country_id' => $this->delivery->country_id,
                'delivery_id' => $this->delivery->id
            ];
            $logAnswerAnswer = [
                'country_id' => $this->delivery->country_id,
                'delivery_id' => $this->delivery->id
            ];

            $transaction = Yii::$app->db->beginTransaction();
            $errorMsg = "";
            try {

                $batchSize = 500;

                foreach ($query->batch($batchSize) as $models) {
                    /**
                     * @var $models Order[]
                     */
                    foreach ($models as $model) {

                        if ($this->correction) {
                            $successIds[] = $model->id;
                        } elseif ($model->canChangeStatusTo(OrderStatus::STATUS_LOG_GENERATED)) {
                            $model->status_id = OrderStatus::STATUS_LOG_GENERATED;

                            $model->route = 'crontab/delivery/generate' . ($this->express) ? '-express' : '' . '-lists';

                            if (!Delivery::createRequestInList($this->delivery->id, $model)) {
                                $hasError = true;

                                $logAnswerAnswer = [
                                    'error' => 'Ошибка при создании заявок для листа.',
                                    'order_id' => $model->id,
                                    'country_id' => $this->delivery->country_id,
                                    'delivery_id' => $this->delivery->id
                                ];
                                $badIds[] = $model->id;
                                break;
                            }

                            if ($model->save(true, ['status_id'])) {
                                $successIds[] = $model->id;
                            }
                        } else {
                            $badIds[] = $model->id;
                        }
                    }
                    if ($hasError) {
                        break;
                    }
                }

                if (count($badIds) > 0) {
                    $query->andWhere(['not in', Order::tableName() . '.id', $badIds]);
                }

                if ($successIds && !$hasError) {
                    $todayStart = Yii::$app->formatter->asTimestamp(date('Y-m-d 00:00:00'));
                    $todayEnd = $todayStart + 86399;

                    $todayCount = OrderLogisticList::find()
                        ->where(['country_id' => $this->delivery->country_id])
                        ->andWhere(['delivery_id' => $this->delivery->id])
                        ->andWhere(['>=', 'created_at', $todayStart])
                        ->andWhere(['<=', 'created_at', $todayEnd])
                        ->count();

                    $list->number = $todayCount + 1;
                    $list->ids = implode(',', $successIds);
                    if ($this->correction) {
                        $list->type = OrderLogisticList::TYPE_CORRECTION;
                    }
                    $logAnswer->data = [
                        'ids' => $list->ids,
                        'country_id' => $this->delivery->country_id,
                        'delivery_id' => $this->delivery->id
                    ];

                    if ($list->save()) {
                        $commit = true;
                        $logAnswerAnswer = [
                            'message' => 'Лист успешно сгенерирован.',
                            'country_id' => $this->delivery->country_id,
                            'delivery_id' => $this->delivery->id
                        ];

                        foreach ($this->delivery->emailList as $emailTo) {
                            if (!User::find()->where(['email' => $emailTo,
                                                      'status' => [
                                                          User::STATUS_BLOCKED,
                                                          User::STATUS_DELETED,
                                                          User::STATUS_BLOCKED | User::STATUS_DELETED
                                                      ]
                            ])->exists()) {
                                $orderLogisticListRequest = OrderLogisticListRequest::find()
                                    ->byList($list->id)
                                    ->byEmail($emailTo)
                                    ->one();
                                if (!$orderLogisticListRequest) {
                                    $orderLogisticListRequest = new OrderLogisticListRequest();
                                    $orderLogisticListRequest->list_id = $list->id;
                                    $orderLogisticListRequest->email_to = $emailTo;
                                }
                                $orderLogisticListRequest->status = OrderLogisticListRequest::STATUS_PENDING;
                                $orderLogisticListRequest->save();
                            }
                        }

                        $transaction->commit();
                    } else {
                        $errorMsg = "Не удалось сгенерировать лист.";
                        $transaction->rollBack();
                        $logAnswer->save();
                        throw new Exception('Не удалось сгенерировать лист.');
                    }
                }
                $logAnswer->answer = json_encode($logAnswerAnswer, JSON_UNESCAPED_UNICODE);
                $logAnswer->data = json_encode($logAnswer->data,
                    JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
            } catch (\Throwable $e) {
                $transaction->rollBack();
                if (!empty($errorMsg)) {
                    Yii::$app->notification->send(Notification::TRIGGER_ORDER_LIST_AUTO_GENERATION_ERROR, [
                        'delivery' => $this->delivery->name,
                        'error' => $errorMsg,
                    ], $this->delivery->country_id);
                }
                $logAnswerAnswer = [
                    'error' => $e->getMessage(),
                    'country_id' => $this->delivery->country_id,
                    'delivery_id' => $this->delivery->id
                ];
                $logAnswer->answer = json_encode($logAnswerAnswer, JSON_UNESCAPED_UNICODE);
                $logAnswer->save();
                break;
            }

            // Совсем не идеальное решение, но пока оставляем так, лень переписывать
            // Спим пару сек, бывает, что транзакция не успевает примениться (ебучая репликация!!!)
            sleep(3);
            try {

                if ($commit) {

                    $waitingForSend = 1;
                    if ($this->delivery->auto_generating_invoice == 1) {
                        try {
                            $result = $list->buildInvoices();
                            if ($result['status'] != InvoiceBuilder::STATUS_SUCCESS) {
                                throw new \Exception($result['message']);
                            }
                        } catch (\Throwable $e) {
                            $errorMsg = "Лист #{$list->id}. Возникла ошибка при генерации инвойса. Лист не может быть отправлен. - 
                            {$this->delivery->country->name} - {$this->delivery->name} - {$list->id}: " . $e->getMessage();
                            $waitingForSend = 0;
                        }
                    }

                    if ($this->delivery->hasApiGenerateList()) {
                        $excelList = $this->delivery->apiTransmitter->downloadList($list);
                        $excelList->waiting_for_send = $waitingForSend;
                        $excelList->save();
                    } else {
                        $builder = ExcelBuilderFactory::build($list, OrderLogisticListExcel::FORMAT_XLSX,
                            !empty($this->delivery->autoListGenerationColumns) ? $this->delivery->autoListGenerationColumns : ExcelBuilder::$autoGenerateColumns, $waitingForSend);
                        $result = $builder->generate();

                        if ($result['status'] != 'success') {
                            $errorMsg = "Возникла ошибка при генерации excel файла. Лист не может быть создан.";
                            throw new Exception($result['message']);
                        }
                    }

                    $logAnswer->status = CrontabTaskLogAnswer::STATUS_SUCCESS;
                    $logAnswer->save();
                }

            } catch (\Throwable $e) {
                if (!empty($errorMsg)) {
                    Yii::$app->notification->send(Notification::TRIGGER_ORDER_LIST_AUTO_GENERATION_ERROR, [
                        'delivery' => $this->delivery->name,
                        'error' => $errorMsg,
                    ], $this->delivery->country_id);
                }
                $logAnswerAnswer = [
                    'error' => $e->getMessage(),
                    'country_id' => $this->delivery->country_id,
                    'delivery_id' => $this->delivery->id
                ];
                $logAnswer->answer = json_encode($logAnswerAnswer, JSON_UNESCAPED_UNICODE);
                $logAnswer->save();
                break;
            }

            if (!empty($errorMsg)) {
                Yii::$app->notification->send(Notification::TRIGGER_ORDER_LIST_AUTO_GENERATION_ERROR, [
                    'delivery' => $this->delivery->name,
                    'error' => $errorMsg,
                ], $this->delivery->country_id);
            }
        }

        return true;
    }

    /**
     * Отправка листов
     * @param $log CrontabTaskLog
     * @param $forceSend boolean
     * @return boolean
     */
    public function send($log, $forceSend = false)
    {
        if (!$this->timeToSend && !$forceSend) {
            return false;
        }

        $query = $this->buildQueryForFindPreparedToSentLists();

        $query->with(['list']);
        $this->lists = $query->all();

        // Разрываем соединение, на случай его отваливания
        if ($this->getMailer()->getTransport()->isStarted()) {
            $this->getMailer()->getTransport()->stop();
        }

        $counter = 1;
        foreach ($this->lists as $list) {

            $errors = [];
            $orderLogisticListRequests = OrderLogisticListRequest::find()->byList($list->list_id)->pending()->all();
            $successSends = 0;

            foreach ($orderLogisticListRequests as $orderLogisticListRequest) {
                $logAnswer = new CrontabTaskLogAnswer();
                $logAnswer->log_id = $log->id;
                $logAnswer->data = json_encode([
                    'delivery_id' => $this->delivery->id,
                    'list_id' => $list->list->id,
                    'ids' => $list->list->ids,
                ]);

                try {
                    $message = $orderLogisticListRequest->buildEmailMessageForLogisticList($list, $counter, count($this->lists));
                    if ($message->send()) {
                        $logAnswer->status = CrontabTaskLogAnswer::STATUS_SUCCESS;
                        $logAnswer->answer = json_encode([
                            'country_id' => $this->delivery->country_id,
                            'delivery_id' => $this->delivery->id,
                            'list_id' => $list->list->id,
                            'ids' => $list->list->ids,
                            'emails' => $orderLogisticListRequest->email_to,
                        ]);
                        $orderLogisticListRequest->status = OrderLogisticListRequest::STATUS_SENT;
                        if (!$orderLogisticListRequest->save(true, ['status'])) {
                            throw new \Exception($orderLogisticListRequest->getFirstErrorAsString());
                        }
                        $successSends++;
                    } else {
                        throw new \Exception('Не удалось отправить сообщение.');
                    }
                } catch (\Throwable $e) {
                    $logAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
                    $logAnswer->answer = json_encode([
                        'error' => $e->getMessage(),
                        'country_id' => $this->delivery->country_id,
                        'delivery_id' => $this->delivery->id,
                        'list_id' => $list->list->id,
                        'ids' => $list->list->ids,
                        'emails' => $orderLogisticListRequest->email_to,
                    ]);
                    $orderLogisticListRequest->status = OrderLogisticListRequest::STATUS_ERROR;
                    $orderLogisticListRequest->save(true, ['status']);
                }
                $logAnswer->save();
                sleep(1);
            }

            if ($successSends == count($orderLogisticListRequests)) {
                try {
                    $list->list->confirmSend(!$this->correction);
                } catch (\Throwable $e) {
                    $errors[] = $e->getMessage();
                }
            }
            if ($errors) {
                $logAnswer = new CrontabTaskLogAnswer();
                $logAnswer->log_id = $log->id;
                $logAnswer->status = CrontabTaskLogAnswer::STATUS_FAIL;
                $logAnswer->data = json_encode($errors);
                $logAnswer->save();
            }

            $counter++;
        }
        return true;
    }

    /**
     * @return ActiveQuery
     */
    protected function buildQueryForFindPreparedToSentLists()
    {
        $query = OrderLogisticListExcel::find()
            ->joinWith(['list'])
            ->where([OrderLogisticListExcel::tableName() . '.waiting_for_send' => 1])
            ->andWhere([OrderLogisticList::tableName() . '.delivery_id' => $this->delivery->id]);

        if ($this->delivery->auto_generating_invoice == 1) {
            $query->joinWith(['list.invoiceArchives']);
            $query->andWhere([
                'or',
                [
                    'and',
                    [OrderLogisticListInvoiceArchive::tableName() . '.format' => OrderLogisticListInvoice::FORMAT_DOCS],
                    [OrderLogisticListInvoiceArchive::tableName() . '.template' => OrderLogisticListInvoice::TEMPLATE_TWO]
                ],
                ['is', OrderLogisticListInvoiceArchive::tableName() . '.id', null]
            ]);
        }

        return $query;
    }
}
