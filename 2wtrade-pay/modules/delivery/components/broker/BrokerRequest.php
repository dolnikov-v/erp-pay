<?php

namespace app\modules\delivery\components\broker;

use app\components\base\Model;
use app\modules\delivery\abstracts\BrokerRequestInterface;
use app\modules\order\abstracts\OrderProductInterface;

/**
 * Class BrokerRequest
 * @package app\modules\delivery\components\broker
 */
class BrokerRequest extends Model implements BrokerRequestInterface
{

    /**
     * @var int
     */
    public $country_id;

    /**
     * @var int
     */
    public $package_id;

    /**
     * @var array
     */
    public $products;

    /**
     * @var string
     */
    public $customer_zip = null;

    /**
     * @var string
     */
    public $customer_city = null;

    /**
     * @var string
     */
    public $customer_province = null;

    /**
     * @var int
     */
    public $partner_id;

    /**
     * @var float
     */
    public $order_price = null;

    /**
     * @var float
     */
    public $shipping_price = null;

    /**
     * @var int
     */
    public $express;

    public function init()
    {
        $this->setProducts($this->products);
        parent::init();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['country_id'], 'required'],
            [['country_id', 'partner_id', 'package_id'], 'integer'],
            [['customer_zip', 'customer_city', 'customer_province'], 'string'],
            [['order_price', 'shipping_price'], 'double'],
            ['express', 'number'],
            [
                'products',
                'safe'
            ]
        ];
    }

    /**
     * @return int
     */
    public function getCountry(): int
    {
        return $this->country_id;
    }

    /**
     * @return int
     */
    public function getPackageId(): ?int
    {
        return $this->package_id;
    }

    /**
     * @return OrderProductInterface[]
     */
    public function getProducts(): ?array
    {
        return $this->products;
    }

    /**
     * @param array $products
     * @return bool
     */
    public function setProducts($products): bool
    {
        $return = [];
        if ($products) {
            foreach ($products as $product) {
                if (is_array($product)) {
                    $orderProduct = new BrokerRequestProduct([
                        'product_id' => $product['product_id'],
                        'price' => $product['price'] ?? 0,
                        'quantity' => $product['quantity'] ?? 0
                    ]);
                    $return[] = $orderProduct;
                }
                if ($product instanceof OrderProductInterface) {
                    $orderProduct = new BrokerRequestProduct([
                        'product_id' => $product->getProductId(),
                        'price' => $product->getPrice(),
                        'quantity' => $product->getQuantity()
                    ]);
                    $return[] = $orderProduct;
                }
            }
        }
        $this->products = $return;
        return true;
    }

    /**
     * @return array
     */
    public function getProductIds(): ?array
    {
        $return = [];
        foreach ($this->getProducts() as $product) {
            $return[$product->getProductId()] = $product->getProductId();
        }
        return $return;
    }

    /**
     * @return string
     */
    public function getCustomerZip(): ?string
    {
        return $this->customer_zip;
    }

    /**
     * @return string
     */
    public function getCustomerCity(): ?string
    {
        return $this->customer_city;
    }

    /**
     * @return string
     */
    public function getCustomerProvince(): ?string
    {
        return $this->customer_province;
    }

    /**
     * @return int
     */
    public function getPartnerId(): ?int
    {
        return $this->partner_id;
    }

    /**
     * @return int
     */
    public function getExpress(): ?int
    {
        return $this->express;
    }

    /**
     * @return float
     */
    public function getOrderPrice(): ?float
    {
        return ($this->order_price == '') ? null : $this->order_price;
    }

    /**
     * @return float
     */
    public function getShippingPrice(): ?float
    {
        return ($this->shipping_price == '') ? null : $this->shipping_price;
    }
}