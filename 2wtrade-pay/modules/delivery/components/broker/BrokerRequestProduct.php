<?php

namespace app\modules\delivery\components\broker;

use app\components\base\Model;
use app\modules\order\abstracts\OrderProductInterface;

/**
 * Class BrokerRequestProduct
 * @package app\modules\delivery\components\broker
 *
 * @property int $product_id
 * @property float price
 * @property int quantity
 */
class BrokerRequestProduct extends Model implements OrderProductInterface
{
    /**
     * @var int
     */
    public $product_id;

    /**
     * @var float
     */
    public $price;

    /**
     * @var int
     */
    public $quantity;

    /**
     * @return int
     */
    public function getProductId(): int {
        return $this->product_id;
    }

    /**
     * @return float
     */
    public function getPrice(): float {
        return $this->price;
    }

    /**
     * @return int
     */
    public function getQuantity(): int {
        return $this->quantity;
    }
}