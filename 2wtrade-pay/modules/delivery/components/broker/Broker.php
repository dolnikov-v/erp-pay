<?php

namespace app\modules\delivery\components\broker;

use app\components\base\Component;
use app\modules\delivery\abstracts\BrokerInterface;
use app\modules\delivery\abstracts\BrokerRequestInterface;
use app\modules\delivery\abstracts\BrokerResultInterface;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryContract;
use app\modules\delivery\models\DeliveryNotProducts;
use app\modules\delivery\models\DeliveryNotProductsByZip;
use app\modules\delivery\models\DeliveryNotRegions;
use app\modules\delivery\models\DeliveryPackage;
use app\modules\delivery\models\DeliveryZipcodes;
use Yii;
use yii\di\Instance;
use yii\helpers\ArrayHelper;
use yii\redis\Connection;

/**
 * Class Broker
 * @package app\modules\delivery\components\broker
 */
class Broker extends Component implements BrokerInterface
{
    /**
     * @var Connection|string|array
     */
    public $redis = 'redis';

    /**
     * @var int
     */
    public $cacheTime = 6;

    /**
     * @var int
     */
    public $mutexTimeOut = 5;

    /**
     * @var string
     */
    public $cachePrefix = 'delivery.broker.countries';

    /**
     * @var string
     */
    public $mutexPrefix = 'delivery.broker.countries.mutex';

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->redis = Instance::ensure($this->redis, Connection::class);
    }

    /**
     * @param int $countryId
     * @return mixed
     */
    protected function readFromCache(int $countryId)
    {
        $json = $this->redis->hget($this->cachePrefix, $countryId);
        if ($json) {
            return json_decode($json, true);
        }
        return false;
    }

    /**
     * @param int $countryId
     * @param $data
     */
    protected function saveToCache(int $countryId, array $data)
    {
        $this->redis->hset($this->cachePrefix, $countryId, json_encode($data));
        $this->redis->expire($this->cachePrefix, $this->cacheTime);
    }

    /***
     * @param int $countryId
     * @return array
     */
    protected function getBaseData(int $countryId)
    {

        $response = [];

        $deliveries = Delivery::find()
            ->byCountryId($countryId)
            ->orderBy(['sort_order' => SORT_ASC])
            ->active()
            ->brokerageActive()
            ->all();
        foreach ($deliveries as $delivery) {

            $contract = DeliveryContract::find()
                ->select(['id'])
                ->byDeliveryId($delivery->id)
                ->byDates(date('Y-m-d'), date('Y-m-d'))
                ->scalar();

            if (!$contract) {
                continue;
            }

            $data = [];
            $data['id'] = $delivery->id;
            $data['name'] = $delivery->name;
            $data['express'] = $delivery->express_delivery;
            $data['min_order_price'] = $delivery->brokerage_min_order_price;
            $data['contract_id'] = $contract;

            $data['not_zipcodes'] = DeliveryNotRegions::find()
                ->select('region')
                ->byDeliveryId($delivery->id)
                ->isZipCode()
                ->asArray()
                ->column();

            $data['not_cities'] = DeliveryNotRegions::find()
                ->select('region')
                ->byDeliveryId($delivery->id)
                ->isCity()
                ->asArray()
                ->column();

            $deliveryZipcodes = DeliveryZipcodes::find()
                ->select([
                    'id',
                    'zipcodes',
                    'price',
                    'max_term_days',
                    'brokerage_daily_maximum',
                    'cities'
                ])
                ->byDeliveryId($delivery->id)
                ->asArray()
                ->all();

            $tmp = [];
            foreach ($deliveryZipcodes as $deliveryZipcode) {
                $tmp[$deliveryZipcode['id']] = $deliveryZipcode;
                $tmp[$deliveryZipcode['id']]['not_products'] = DeliveryNotProductsByZip::find()
                    ->select([
                        'product_id'
                    ])
                    ->where(['zipcodes_id' => $deliveryZipcode['id']])
                    ->column();
            }

            $data['delivery_zipcode_groups'] = $tmp;

            $data['not_products'] = DeliveryNotProducts::find()
                ->select('product_id')
                ->where(['delivery_id' => $delivery->id])
                ->column();


            $data['packages'] = DeliveryPackage::find()
                ->select('package_id')
                ->where(['delivery_id' => $delivery->id])
                ->column();

            $response[] = $data;

        }
        return $response;
    }

    /**
     * @param array $countryData
     * @param BrokerRequestInterface $brokerRequest
     * @return BrokerResultInterface[]
     */
    protected function search(array $countryData, BrokerRequestInterface $brokerRequest)
    {
        if (!$countryData) {
            return [];
        }

        $response = [];
        foreach ($countryData as $deliveryData) {

            if (!empty($deliveryData['not_zipcodes']) && $brokerRequest->getCustomerZip()) {
                // проверяем зип код в исключениях
                if (array_search($brokerRequest->getCustomerZip(), $deliveryData['not_zipcodes']) !== false) {
                    continue;
                }
            }

            if (!empty($deliveryData['not_cities']) && $brokerRequest->getCustomerCity()) {
                // проверяем город в исключениях
                if (array_search($brokerRequest->getCustomerCity(), $deliveryData['not_cities']) !== false) {
                    continue;
                }
            }

            if (!empty($deliveryData['packages']) && !empty($brokerRequest->getPackageId())) {
                // проверяем акцию в списке работает по акциям
                if (!array_search($brokerRequest->getPackageId(), $deliveryData['packages']) !== false) {
                    continue;
                }
            }

            if (!empty($deliveryData['not_products']) && !empty($brokerRequest->getProductIds())) {
                // проверяем продукты в целом
                $noDeliveryByProduct = false;
                foreach ($brokerRequest->getProductIds() as $productId) {
                    if (array_search($productId, $deliveryData['not_products']) !== false) {
                        $noDeliveryByProduct = true;
                    }
                }
                if ($noDeliveryByProduct) {
                    continue;
                }
            }

            // найдем группу настроек по которой возможна доставка если указан зип или город для которого ищем
            $activeZipcodeGroup = null;
            if ($brokerRequest->getCustomerZip() != '' || $brokerRequest->getCustomerCity() != '') {
                if ($deliveryData['delivery_zipcode_groups']) {
                    foreach ($deliveryData['delivery_zipcode_groups'] as $zipcodeGroup) {
                        if (!$activeZipcodeGroup) {
                            if (empty($zipcodeGroup['zipcodes']) && empty($zipcodeGroup['cities'])) {
                                //если в брокере есть группа без индексов, то считать, что курьерка доставлят по всей стране.
                                $activeZipcodeGroup = $zipcodeGroup;
                            }

                            $codes = explode(",", $zipcodeGroup['zipcodes']);
                            if ($brokerRequest->getCustomerZip() != '' && in_array(trim(mb_strtolower($brokerRequest->getCustomerZip())), $codes)) {
                                $activeZipcodeGroup = $zipcodeGroup;
                            } elseif (!empty($zipcodeGroup['cities']) && !empty($brokerRequest->getCustomerCity())) {
                                if (in_array(trim(mb_strtolower($brokerRequest->getCustomerCity())), explode(",", $zipcodeGroup['cities']))) {
                                    $activeZipcodeGroup = $zipcodeGroup;
                                }
                            }

                            if ($activeZipcodeGroup) {
                                if (!empty($activeZipcodeGroup['not_products']) && !empty($brokerRequest->getProductIds())) {
                                    // проверяем продукты в группе


                                    foreach ($brokerRequest->getProductIds() as $productId) {
                                        if (array_search($productId, $activeZipcodeGroup['not_products']) !== false) {
                                            // не подходит
                                            $activeZipcodeGroup = null;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (!$activeZipcodeGroup) {
                        // нет доставки
                        continue;
                    }
                }
            }


            // нужно считать score пока ничего лучше чем загрузить объекты и использовать существующие методы
            $delivery = Delivery::findOne(['id' => $deliveryData['id']]);
            if (!$delivery) {
                continue;
            }

            $deliveryZipGroup = null;
            if ($activeZipcodeGroup) {
                $deliveryZipGroup = DeliveryZipcodes::findOne(['id' => $activeZipcodeGroup['id']]);
                if (!$deliveryZipGroup) {
                    continue;
                }
            }

            $contract = DeliveryContract::findOne(['id' => $deliveryData['contract_id']]);
            if (!$contract) {
                continue;
            }

            // достаточно ли заказов на сегодня
            $enoughOrders = 0;
            $ordersApprovedOneDayInDelivery = $delivery->getOrdersApprovedOneDay();
            if ($ordersApprovedOneDayInDelivery > $delivery->brokerage_daily_maximum && $delivery->brokerage_daily_maximum) {
                $enoughOrders = 1;
            }

            if ($deliveryZipGroup) {
                $ordersApprovedOneDayInDeliveryByZipGroup = empty($deliveryZipGroup->zipcodes)
                    ? $ordersApprovedOneDayInDelivery
                    : $deliveryZipGroup->getOrdersApprovedOneDay();

                if ($ordersApprovedOneDayInDeliveryByZipGroup > $deliveryZipGroup->brokerage_daily_maximum && $deliveryZipGroup->brokerage_daily_maximum) {
                    $enoughOrders = 1;
                }
            }

            // кастомная цена доставки
            $deliveryPrice = !empty($brokerRequest->getProducts()) ? $delivery->getCustomDeliveryPrice($brokerRequest->getProducts()) : null;
            if ($deliveryZipGroup && is_numeric($deliveryZipGroup->price) && $deliveryZipGroup->price) {
                $deliveryPrice = $deliveryZipGroup->price;
            }

            if ($brokerRequest->getOrderPrice() && $deliveryData['min_order_price']) {
                $checkDeliveryPrice = $deliveryPrice;
                if (!is_null($brokerRequest->getShippingPrice())) {
                    $checkDeliveryPrice = $brokerRequest->getShippingPrice();
                }
                if (($brokerRequest->getOrderPrice() + $checkDeliveryPrice) < $deliveryData['min_order_price']) {
                    // не соответствует минимальной цене доставки
                    continue;
                }
            }

            // время доставки по регионам
            $deliveredTimes = $contract->getDeliveredTimesByRegion($brokerRequest->getCustomerZip(), $brokerRequest->getCustomerCity(), $brokerRequest->getCustomerProvince());

            $brokerResult = new BrokerResult([
                'enough' => $enoughOrders,
                'express' => $delivery->express_delivery,
                'score' => $delivery->sort_order,
                'delivery_id' => $delivery->id,
                'delivery_name' => $delivery->name,
                'delivery_price' => $deliveryPrice,
                'min_delivery_days' => $delivery->express_delivery ? 0 : (empty($deliveredTimes['delivered_from']) && $deliveredTimes['delivered_from'] !== 0 ? null : $deliveredTimes['delivered_from']),
                'max_delivery_days' => empty($deliveredTimes['delivered_to']) && $deliveredTimes['delivered_to'] !== 0 ? null : $deliveredTimes['delivered_to'],
            ]);
            $response[] = $brokerResult;
        }

        if ($response) {

            $sortKeys = [];
            $sortKeys['enough'] = SORT_ASC;
            if ($brokerRequest->getExpress()) {
                $sortKeys['express'] = SORT_DESC;
            }
            $sortKeys['score'] = SORT_ASC;
            ArrayHelper::multisort($response, array_keys($sortKeys), array_values($sortKeys));
        }

        return $response;
    }


    /**
     * @param BrokerRequestInterface $brokerRequest
     * @return BrokerResultInterface[]
     */
    public function getAvailableDeliveries(BrokerRequestInterface $brokerRequest): array
    {
        $countryData = $this->readFromCache($brokerRequest->getCountry());
        if ($countryData === false || $countryData === null) {

            $mutexName = $this->mutexPrefix . '.' . $brokerRequest->getCountry();

            if ($this->lockWrite($mutexName)) {
                try {
                    $countryData = $this->readFromCache($brokerRequest->getCountry());
                    if ($countryData === false || $countryData === null) {
                        $countryData = $this->getBaseData($brokerRequest->getCountry());
                        $this->saveToCache($brokerRequest->getCountry(), $countryData);
                    }
                } finally {
                    $this->redis->del($mutexName);
                }
            } else {
                $countryData = $this->getBaseData($brokerRequest->getCountry());
            }


        }
        return $this->search($countryData, $brokerRequest);
    }

    /**
     * @param string $mutexName
     * @return bool
     */
    private function lockWrite($mutexName)
    {
        $waitTime = 0;
        $timeout = $this->mutexTimeOut * 1000000;
        while (!$this->redis->set($mutexName, true, 'NX', 'EX', $this->mutexTimeOut * 3)) {
            $waitTime += 10000;
            if ($waitTime > $timeout) {
                return false;
            }
            usleep(10000);
        }
        return true;
    }
}