<?php

namespace app\modules\delivery\components\broker;

use app\components\base\Model;
use app\modules\delivery\abstracts\BrokerResultInterface;

/**
 * Class Broker
 * @package app\modules\delivery\components\broker
 *
 * @property string $country
 */
class BrokerResult extends Model implements BrokerResultInterface
{

    /**
     * @var bool
     */
    public $enough;

    /**
     * @var int
     */
    public $score;

    /**
     * @var int
     */
    public $delivery_id;

    /**
     * @var string
     */
    public $delivery_name;

    /**
     * @var float
     */
    public $delivery_price;

    /**
     * @var bool
     */
    public $express;

    /**
     * @var int
     */
    public $min_delivery_days;

    /**
     * @var int
     */
    public $max_delivery_days;


    /**
     * @return bool
     */
    public function getEnough(): bool
    {
        return $this->enough;
    }

    /**
     * @return int
     */
    public function getScore(): int
    {
        return $this->score;
    }

    /**
     *
     * @return int
     */
    public function getDeliveryId(): int
    {
        return $this->delivery_id;
    }

    /**
     * @return string
     */
    public function getDeliveryName(): string
    {
        return $this->delivery_name;
    }

    /**
     * @return float
     */
    public function getDeliveryPrice(): ?float
    {
        return $this->delivery_price;
    }

    /**
     * @return bool
     */
    public function getExpress(): bool
    {
        return $this->express;
    }

    /**
     * @return int
     */
    public function getMinDeliveryDays(): ?int
    {
        return $this->min_delivery_days;
    }

    /**
     * @return int
     */
    public function getMaxDeliveryDays(): ?int
    {
        return $this->max_delivery_days;
    }
}