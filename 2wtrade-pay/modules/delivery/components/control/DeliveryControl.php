<?php

namespace app\modules\delivery\components\control;

use app\models\Notification;
use app\modules\delivery\abstracts\BrokerResultInterface;
use app\modules\delivery\components\broker\BrokerRequest;
use app\modules\delivery\DeliveryModule;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryAutoChangeReason;
use app\modules\delivery\models\DeliveryContacts;
use app\modules\delivery\models\DeliveryContract;
use app\modules\delivery\models\DeliveryContractTime;
use app\modules\delivery\models\DeliveryNotProductsByZip;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\delivery\models\DeliveryRequestDeleted;
use app\modules\delivery\models\DeliverySku;
use app\modules\delivery\models\DeliveryZipcodes;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use Yii;
use yii\base\Component;
use yii\db\Exception;
use yii\db\Expression;

/**
 * Class DeliveryControl
 * @package app\modules\delivery\components\control
 */
class DeliveryControl extends Component
{

    /**
     * Должен ли быть отправлен в другую КС
     * @param Order $order
     * @return bool|int
     */
    public function shouldSendOrderToOtherDelivery(Order $order)
    {
        if ($order->status_id == OrderStatus::STATUS_DELIVERY_REJECTED &&
            $order->deliveryRequest instanceof DeliveryRequest &&
            $order->deliveryRequest->delivery->isAutoChangeOnReject()) {
            $newDeliveryId = (int)DeliveryAutoChangeReason::find()
                ->select('to_delivery_id')
                ->where(['from_delivery_id' => $order->deliveryRequest->delivery_id])
                ->andWhere(':error like ' . new Expression("CONCAT('%', reason, '%')"), [':error' => $order->deliveryRequest->api_error])
                ->scalar();

            if ($newDeliveryId) {
                if (!DeliveryRequestDeleted::find()->where([
                    'delivery_id' => $newDeliveryId,
                    'order_id' => $order->id,
                ])->exists()) {
                    return $newDeliveryId;
                }
            }
        }
        return false;
    }

    /**
     * Отправить заказ в другую КС если это возможно по Брокеру
     * @param Order $order
     * @param int $newDeliveryId
     * @return bool
     */
    public function sendOrderToOtherDelivery(Order $order, int $newDeliveryId): bool
    {

        $delivery = Delivery::findOne(['id' => $newDeliveryId]);
        if (!$delivery) {
            return false;
        }

        $transaction = DeliveryRequest::getDb()->beginTransaction();
        try {

            if ($order->deliveryRequest instanceof DeliveryRequest) {
                if ($order->deliveryRequest->id == $delivery->id) {
                    $transaction->commit();
                    return true;
                }
                try {
                    $order->deliveryRequest->delete();
                } catch (\Throwable $exception) {
                    $transaction->rollBack();
                    Yii::$app->notification->send(
                        Notification::TRIGGER_AUTO_CHANGE_DELIVERY_ERROR,
                        [
                            'order' => $order->id,
                            'delivery' => $delivery->name . ' (#' . $delivery->id . ')',
                            'error' => $exception->getMessage(),
                        ]
                    );
                    return false;
                }
            }

            $canDelivery = false;

            // брокер
            $brokerRequest = new BrokerRequest([
                'country_id' => $order->country_id,
                'products' => $order->orderProducts,
                'customer_zip' => $order->customer_zip,
                'customer_city' => $order->customer_city,
                'customer_province' => $order->customer_province,
                'express' => $order->orderExpressDelivery ? true : false,
                'order_price' => $order->price_total,
            ]);

            /** @var DeliveryModule $deliveryModule */
            $deliveryModule = Yii::$app->getModule('delivery');
            $brokerResults = $deliveryModule->broker->getAvailableDeliveries($brokerRequest);
            if ($brokerResults) {
                /** @var BrokerResultInterface $brokerResult */
                foreach ($brokerResults as $brokerResult) {
                    if ($delivery->id == $brokerResult->getDeliveryId()) {
                        $canDelivery = true;
                        break;
                    }
                }
            }

            if (!$canDelivery) {
                Yii::$app->notification->send(
                    Notification::TRIGGER_AUTO_CHANGE_DELIVERY_ERROR,
                    [
                        'order' => $order->id,
                        'delivery' => $delivery->name . ' (#' . $delivery->id . ')',
                        'error' => Yii::t('common', 'Служба доставки не доставляет по Брокеру'),
                    ]
                );
            }

            if ($canDelivery) {
                $order->status_id = OrderStatus::STATUS_CC_APPROVED;
                $order->pending_delivery_id = $delivery->id;
                $order->route = Yii::$app->controller->route;
                $order->commentForLog = 'Auto change delivery by reason';

                try {
                    if ($this->createDeliveryRequestForOrder($order, $delivery)) {
                        $transaction->commit();
                        return true;
                    } else {
                        Yii::$app->notification->send(
                            Notification::TRIGGER_AUTO_CHANGE_DELIVERY_ERROR,
                            [
                                'order' => $order->id,
                                'delivery' => $delivery->name . ' (#' . $delivery->id . ')',
                                'error' => Yii::t('common', 'Не удалось создать заявку'),
                            ]
                        );
                    }
                } catch (\Exception $exception) {
                    Yii::$app->notification->send(
                        Notification::TRIGGER_AUTO_CHANGE_DELIVERY_ERROR,
                        [
                            'order' => $order->id,
                            'delivery' => $delivery->name . ' (#' . $delivery->id . ')',
                            'error' => $exception->getMessage(),
                        ]
                    );
                }
            }
        } catch (Exception $exception) {
            Yii::$app->notification->send(
                Notification::TRIGGER_AUTO_CHANGE_DELIVERY_ERROR,
                [
                    'order' => $order->id,
                    'delivery' => $delivery->name . ' (#' . $delivery->id . ')',
                    'error' => $exception->getMessage(),
                ]
            );
        }
        try {
            $transaction->rollBack();
        } catch (Exception $exception) {
            return false;
        }
        return false;
    }

    /**
     * @param Order $order
     * @param Delivery $delivery
     * @return bool
     * @throws \Exception
     */
    public function createDeliveryRequestForOrder(Order $order, Delivery $delivery): bool
    {
        if (!$delivery->auto_sending) {
            $order->route = Yii::$app->controller->route;
            $order->pending_delivery_id = $delivery->id;

            if (!$order->save(true, ['pending_delivery_id'])) {
                throw new Exception($order->getFirstErrorAsString());
            }

            return true;
        }

        $canChangeByWorkflow = false;
        if ($delivery->workflow_id) {
            $nextStatuses = $delivery->workflow->getNextStatuses($order->status_id);
            if (in_array(OrderStatus::STATUS_DELIVERY_PENDING, $nextStatuses) ||
                in_array(OrderStatus::STATUS_LOG_DEFERRED, $nextStatuses)) {
                $canChangeByWorkflow = true;
            }
        } elseif (
            $order->canChangeStatusTo(OrderStatus::STATUS_DELIVERY_PENDING) ||
            $order->canChangeStatusTo(OrderStatus::STATUS_LOG_DEFERRED)) {
            $canChangeByWorkflow = true;
        }

        if (!$canChangeByWorkflow) {
            throw new Exception(Yii::t('common', 'Неразрешенное значение статуса.'));
        }

        $contract = $delivery->getActiveContract();
        if (!$contract) {
            throw new Exception(Yii::t('common', 'Нет контракта.'));
        }

        if (!empty($contract->max_delivery_period) &&
            $order->delivery_time_from > strtotime("+{$contract->max_delivery_period}days") &&
            $order->canChangeStatusTo(OrderStatus::STATUS_LOG_DEFERRED)) {
            $order->status_id = OrderStatus::STATUS_LOG_DEFERRED;
            $order->pending_delivery_id = $delivery->id;
        } else {
            $order->status_id = OrderStatus::STATUS_DELIVERY_PENDING;
        }

        if (!$order->save(true, ['status_id', 'pending_delivery_id'])) {
            throw new Exception($order->getFirstErrorAsString());
        }

        if ($order->status_id != OrderStatus::STATUS_LOG_DEFERRED) {
            $request = new DeliveryRequest();
            $request->delivery_id = $delivery->id;
            $request->order_id = $order->id;
            $request->status = DeliveryRequest::STATUS_PENDING;

            if (!$request->save()) {
                throw new Exception($request->getFirstErrorAsString());
            }
        }
        return true;
    }


    /**
     * @param Delivery $delivery
     * @param array $post
     * @param DeliveryAutoChangeReason[] $currentDeliveryAutoChangeReason
     * @return bool
     */
    public function saveAutoChangeReasonsFromPost(Delivery &$delivery, ?array $post, ?array $currentDeliveryAutoChangeReason): bool
    {
        if (!$post) {
            return true;
        }
        $multiData = [];
        foreach ($post as $k => $reasonData) {
            if (is_array($reasonData)) {
                foreach ($reasonData as $num => $data) {
                    $multiData[$num][$k] = $data;
                }
            }
        }

        $saved = [];
        foreach ($multiData as $k => $data) {
            $id = $data['id'];

            $model = null;

            if ($id) {
                foreach ($currentDeliveryAutoChangeReason as $current) {
                    if ($current->id == $id) {
                        $model = $current;
                        break;
                    }
                }
            }

            if (!($model instanceof DeliveryAutoChangeReason)) {
                $model = new DeliveryAutoChangeReason();
                $model->from_delivery_id = $delivery->id;
            }

            if (empty($data['to_delivery_id'])) {
                continue;
            }

            $model->to_delivery_id = $data['to_delivery_id'];
            $model->reason = $data['reason'];

            if ($model->save()) {
                $saved[] = $model->id;
            } else {
                $error = $model->getFirstErrorAsString();
                $delivery->addError('autoChangeReason', $error);
            }
        }

        foreach ($currentDeliveryAutoChangeReason as $current) {
            if (!in_array($current->id, $saved)) {
                try {
                    $current->delete();
                } catch (\Throwable $exception) {
                    $delivery->addError('autoChangeReason', $exception->getMessage());
                }
            }
        }

        return $delivery->hasErrors() ? false : true;
    }

    /**
     * @param Delivery $delivery
     * @param array $post
     * @param DeliveryContacts[] $currentDeliveryContacts
     * @return bool
     */
    public function saveContactsFromPost(Delivery &$delivery, ?array $post, ?array $currentDeliveryContacts): bool
    {
        $multiData = [];
        foreach ($post as $k => $contactsData) {
            if (is_array($contactsData)) {
                foreach ($contactsData as $num => $data) {
                    $multiData[$num][$k] = $data;
                }
            }
        }

        $saved = [];
        foreach ($multiData as $k => $data) {
            $id = $data['id'];

            $model = null;

            if ($id) {
                foreach ($currentDeliveryContacts as $currentDeliveryContact) {
                    if ($currentDeliveryContact->id == $id) {
                        $model = $currentDeliveryContact;
                        break;
                    }
                }
            } else {
                $model = new DeliveryContacts();
                $model->delivery_id = $delivery->id;
            }

            $model->job_title = $data['job_title'];
            $model->full_name = $data['full_name'];
            $model->email = $data['email'];
            $model->phone = $data['phone'];
            $model->mobile_phone = $data['mobile_phone'];
            $model->additional_phone = $data['additional_phone'];

            if ($model->save()) {
                $saved[] = $model->id;
            } else {
                $error = $model->getErrorsAsArray()[0];
                $delivery->addError('contacts', $error);
            }
        }

        foreach ($currentDeliveryContacts as $currentDeliveryContact) {
            if (!in_array($currentDeliveryContact->id, $saved)) {
                try {
                    $currentDeliveryContact->delete();
                } catch (\Throwable $exception) {
                    $delivery->addError('contacts', $exception->getMessage());
                }
            }
        }

        return $delivery->hasErrors() ? false : true;
    }

    /**
     * @param Delivery $delivery
     * @param array $post
     * @param DeliveryZipcodes[] $currentDeliveryZipCodes
     * @return bool
     */
    public function saveZipCodesFromPost(Delivery &$delivery, ?array $post, ?array $currentDeliveryZipCodes): bool
    {

        $multiData = [];
        foreach ($post as $k => $zipCodeData) {
            if (is_array($zipCodeData)) {
                foreach ($zipCodeData as $num => $data) {
                    $multiData[$num][$k] = $data;
                }
            }
        }

        $saved = [];
        foreach ($multiData as $k => $data) {
            $id = $data['id'];

            $model = null;

            if ($id) {
                foreach ($currentDeliveryZipCodes as $currentDeliveryZipCode) {
                    if ($currentDeliveryZipCode->id == $id) {
                        $model = $currentDeliveryZipCode;
                        break;
                    }
                }
            } else {
                $model = new DeliveryZipcodes();
                $model->delivery_id = $delivery->id;
            }

            $model->name = $data['name'];
            $model->price = $data['price'];
            $model->zipcodes = $data['zipcodes'];
            $model->cities = $data['cities'];
            $model->max_term_days = $data['max_term_days'];
            $model->brokerage_daily_maximum = $data['brokerage_daily_maximum'];

            if ($model->save()) {
                $saved[] = $model->id;
                $notProductsByZipForRemove = DeliveryNotProductsByZip::find()
                    ->where(['zipcodes_id' => $model->id])
                    ->all();
                // Чтобы сохранились логи, пробегаем по всем записям
                foreach ($notProductsByZipForRemove as $notProductsByZipForRemoveRecord) {
                    try {
                        $notProductsByZipForRemoveRecord->delete();
                    } catch (\Throwable $exception) {
                        $delivery->addError('zipcodes', $exception->getMessage());
                    }
                }
                if (!empty($data['notProductIDs'])) {
                    foreach ($data['notProductIDs'] as $item) {
                        $notProduct = new DeliveryNotProductsByZip([
                            'zipcodes_id' => $model->id,
                            'product_id' => $item
                        ]);
                        if (!$notProduct->save()) {
                            $error = $notProduct->getErrorsAsArray()[0];
                            $delivery->addError('notProductIDs', $error);
                            break;
                        }
                    }
                }
            } else {
                $error = $model->getErrorsAsArray()[0];
                $delivery->addError('zipcodes', $error);
            }
        }

        foreach ($currentDeliveryZipCodes as $currentDeliveryZipCode) {
            if (!in_array($currentDeliveryZipCode->id, $saved)) {
                try {
                    $currentDeliveryZipCode->delete();
                } catch (\Throwable $exception) {
                    $delivery->addError('zipcodes', $exception->getMessage());
                }
            }
        }


        return $delivery->hasErrors() ? false : true;
    }

    /**
     * @param Delivery $delivery
     * @param array $post
     * @param DeliverySku[] $currentDeliverySku
     * @return bool
     */
    public function saveSkuFromPost(Delivery &$delivery, ?array $post, ?array $currentDeliverySku): bool
    {
        if (!$post) {
            return true;
        }
        $multiData = [];
        foreach ($post as $k => $skuData) {
            if (is_array($skuData)) {
                foreach ($skuData as $num => $data) {
                    $multiData[$num][$k] = $data;
                }
            }
        }

        $saved = [];
        foreach ($multiData as $k => $data) {
            $id = $data['id'];

            $model = null;

            if ($id) {
                foreach ($currentDeliverySku as $current) {
                    if ($current->id == $id) {
                        $model = $current;
                        break;
                    }
                }
            } else {
                $model = new DeliverySku();
                $model->delivery_id = $delivery->id;
            }

            if (!isset($data['product_id'])) {
                continue;
            }

            $model->product_id = $data['product_id'];
            $model->sku = $data['sku'];

            if ($model->save()) {
                $saved[] = $model->id;
            } else {
                $error = $model->getErrorsAsArray()[0];
                $delivery->addError('sku', $error);
            }
        }

        foreach ($currentDeliverySku as $current) {
            if (!in_array($current->id, $saved)) {
                try {
                    $current->delete();
                } catch (\Throwable $exception) {
                    $delivery->addError('sku', $exception->getMessage());
                }
            }
        }

        return $delivery->hasErrors() ? false : true;
    }

    /**
     * @param DeliveryContract $deliveryContract
     * @param array $post
     * @throws \yii\db\Exception
     * @return bool
     */
    public function saveContractTimesFromPost(DeliveryContract &$deliveryContract, ?array $post): bool
    {
        $multiData = [];
        foreach ($post as $k => $lineData) {
            foreach ($lineData as $num => $data) {
                $multiData[$num][$k] = $data;
            }
        }
        DeliveryContractTime::deleteAll(['contract_id' => $deliveryContract->id]);
        if ($multiData) {
            $insert = [];
            foreach ($multiData as $k => $data) {
                if (isset($data['field_value']) && is_string($data['field_value'])) {
                    foreach (explode(',', $data['field_value']) as $value) {
                        if (trim($value) && (!empty($data['delivered_from']) || !empty($data['delivered_to']))) {
                            $insert[$data['field_name'] . '_' . trim($value) . '_' . $data['delivered_from'] . '_' . $data['delivered_to']] = [
                                'contract_id' => $deliveryContract->id,
                                'field_name' => $data['field_name'],
                                'field_value' => trim($value),
                                'delivered_from' => !$data['delivered_from'] ? null : $data['delivered_from'],
                                'delivered_to' => !$data['delivered_to'] ? null : $data['delivered_to'],
                            ];
                        }
                    }
                }
            }
            Yii::$app->db->createCommand()->batchInsert(
                DeliveryContractTime::tableName(),
                [
                    'contract_id',
                    'field_name',
                    'field_value',
                    'delivered_from',
                    'delivered_to',
                ],
                $insert
            )->execute();
        }
        return true;
    }
}