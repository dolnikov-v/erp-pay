<?php
namespace app\modules\delivery\components\api;

use app\helpers\Utils;
use app\modules\delivery\models\Delivery;
use app\modules\order\abstracts\OrderProductInterface;
use app\modules\order\models\Order;
use app\modules\order\models\OrderLogisticList;
use app\modules\order\models\OrderLogisticListExcel;
use app\modules\order\models\OrderProduct;
use Yii;
use yii\base\Exception;

/**
 * Class Transmitter
 * @package app\modules\delivery\components\api
 */
class Transmitter implements TransmitterTargetInterface
{
    const STATUS_SUCCESS = 'success';
    const STATUS_FAIL = 'fail';

    /**
     * @var TransmitterAdaptee
     */
    protected $adaptee;

    /**
     * @param Delivery $delivery
     * @param string $classPath
     */
    public function __construct($delivery, $classPath = null)
    {
        $this->adaptee = new TransmitterAdaptee($delivery, $classPath);
    }

    /**
     * @return TransmitterAdaptee
     */
    public function getAdaptee()
    {
        return $this->adaptee;
    }

    /**
     * @param Order $order
     * @return array
     */
    public function sendOrder($order)
    {
        return $this->adaptee->sendOrder($order);
    }

    /**
     * @param Order $order
     * @return array
     */
    public function getOrderInfo($order)
    {
        return $this->adaptee->getOrderInfo($order);
    }

    /**
     * @param Order[] $orders
     * @return array
     */
    public function batchGetOrdersInfo(&$requests)
    {
        return $this->adaptee->batchGetOrdersInfo($requests);
    }

    /**
     * @param array $order
     * @return array
     */
    public function getOrderInfoForArray($order)
    {
        return $this->adaptee->getOrderInfoForArray($order);
    }

    /**
     * @param array $orders
     * @return array
     */
    public function batchGetOrderInfoForArray($orders)
    {
        return $this->adaptee->batchGetOrderInfoForArray($orders);
    }

    /**
     * @return bool
     */
    public function canGetOrderInfoForList()
    {
        return $this->adaptee->canGetOrderInfoForList();
    }

    /**
     * @return bool
     */
    public function hasBatchGetOrderInfoForArray()
    {
        $apiClass = $this->adaptee->getApiClass();

        return method_exists($apiClass, 'batchGetOrderInfoForArray');
    }

    /**
     * @return bool
     */
    public function hasGetOrderInfoForArray()
    {
        $apiClass = $this->adaptee->getApiClass();
        return method_exists($apiClass, 'getOrderInfoForArray');
    }

    /**
     * @return bool
     */
    public function hasGetGetSortedOrderIds()
    {
        $apiClass = $this->adaptee->getApiClass();
        return method_exists($apiClass, 'getSortedOrderIds');
    }

    /**
     * @param OrderProductInterface[] $orderProducts
     * @return array
     */
    public function getCustomDeliveryPrice($orderProducts)
    {
        return $this->adaptee->getCustomDeliveryPrice($orderProducts);
    }

    /**
     * @param array $ids
     * @return array
     */
    public function getSortedOrderIds($ids)
    {
        $apiClass = $this->adaptee->getApiClass();
        if (method_exists($apiClass, 'getSortedOrderIds')) {
            return $this->adaptee->getSortedOrderIds($ids);
        }
        return $ids;
    }

    /**
     * @param OrderLogisticList $list
     * @return OrderLogisticListExcel
     * @throws Exception
     */
    public function downloadList($list)
    {
        $listExcel = $list->actualListExcel;

        if (!$listExcel) {
            $fileName = Utils::uid() . '.tmp';
            $filePath = OrderLogisticList::getDownloadPath($fileName, OrderLogisticList::DOWNLOAD_TYPE_EXCELS,
                    true) . DIRECTORY_SEPARATOR . $fileName;

            $result = $this->adaptee->generateList($list->orders, $filePath, false);

            if (isset($result['status']) && $result['status'] == self::STATUS_SUCCESS) {
                $listExcel = new OrderLogisticListExcel();
                $listExcel->list_id = $list->id;
                $listExcel->orders_hash = $list->getOrdersHash();
                $listExcel->system_file_name = $fileName;
                $listExcel->delivery_file_name = isset($result['data']['fileName']) ? $result['data']['fileName'] : '';
                $listExcel->user_id = isset(Yii::$app->user) ? Yii::$app->user->id : null;

                if (!$listExcel->save()) {
                    throw new Exception(Yii::t('common', 'Не удалось сохранить Excel-файл.'));
                }
            } else {
                throw new Exception(Yii::t('common', 'API-класс ответил с ошибкой:' . $result['message']));
            }
        }

        return $listExcel;
    }

    /**
     * @param OrderLogisticList $list
     * @return string $fileName
     * @throws Exception
     */
    protected function generateLabelFile($list)
    {
        $filePath = OrderLogisticList::getDownloadPath('', OrderLogisticList::DOWNLOAD_TYPE_LABELS, false);
        $result = $this->adaptee->generateLabel($list->orders, $filePath, false);
        if (isset($result['status']) && $result['status'] == self::STATUS_SUCCESS) {
            $fileName = $result['data'];
            $list->label = $fileName;
            if (!$list->save()) {
                throw new Exception(Yii::t('common', 'Не удалось сгенерировать этикетки.'));
            }
        } else {
            throw new Exception(Yii::t('common', 'API-класс ответил с ошибкой.'));
        }

        return $fileName;
    }

    /**
     * @param OrderLogisticList $list
     * @return string $fileName
     * @throws Exception
     */
    public function generateLabel($list)
    {
        $fileName = $list->label;
        if (!$fileName || !file_exists($list->getLabelFilePath())) {
            $fileName = $this->generateLabelFile($list);
        }

        return $fileName;
    }

    /**
     * @param OrderLogisticList $list
     * @return OrderLogisticListExcel
     * @throws Exception
     */
    public function sendList($list)
    {
        $listExcel = $list->actualListExcel;

        if (!$listExcel) {
            $listExcel = $this->downloadList($list);
        }
        $fileName = $listExcel->system_file_name;
        $filePath = OrderLogisticList::getDownloadPath($fileName, OrderLogisticList::DOWNLOAD_TYPE_EXCELS,
                false) . DIRECTORY_SEPARATOR . $fileName;
        $result = $this->adaptee->generateList($list->orders, $filePath, true);
        if (isset($result['status']) && $result['status'] == self::STATUS_SUCCESS) {
            $listExcel->sent_user_id = isset(Yii::$app->user) ? Yii::$app->user->id : null;
            $listExcel->sent_at = time();

            if (!$listExcel->save()) {
                throw new Exception(Yii::t('common', 'Не удалось сохранить Excel-файл.'));
            }
        } else {
            throw new Exception(Yii::t('common', 'API-класс ответил с ошибкой.'));
        }

        return $listExcel;
    }
}
