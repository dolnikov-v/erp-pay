<?php
namespace app\modules\delivery\components\api;

use app\modules\order\models\Order;

/**
 * Interface TransmitterInterface
 * @package app\modules\delivery\components\api
 */
interface TransmitterInterface
{
    /**
     * @param Order $order
     * @return boolean
     */
    public function send($order);

    /**
     * @param Order $order
     * @return integer
     */
    public function getStatus($order);

}
