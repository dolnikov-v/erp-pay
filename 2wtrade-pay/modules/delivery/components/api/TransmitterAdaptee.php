<?php

namespace app\modules\delivery\components\api;

use app\modules\delivery\abstracts\DeliveryApiClassLabelInterface;
use app\modules\delivery\abstracts\DeliveryApiClassManifestInterface;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryApiAbstract;
use app\modules\order\abstracts\OrderProductInterface;
use app\modules\order\models\Order;
use app\modules\order\models\OrderProduct;
use Yii;
use yii\base\InvalidParamException;

/**
 * Class TransmitterAdaptee
 * @package app\modules\delivery\components\api
 */
class TransmitterAdaptee implements TransmitterAdapteeInterface
{
    /**
     * @var Delivery
     */
    protected $delivery;

    /**
     * @var \stdClass
     */
    protected $apiClass;

    /**
     * @param Delivery $delivery
     * @param string $classPath
     */
    public function __construct($delivery, $classPath = null)
    {
        $this->delivery = $delivery;
        $this->initApiClass($classPath);
    }

    /**
     * @return \stdClass
     */
    public function getApiClass()
    {
        return $this->apiClass;
    }

    /**
     * @param Order $order
     * @return array
     */
    public function sendOrder($order)
    {
        return $this->apiClass->sendOrder($order);
    }

    /**
     * @param Order $order
     * @return array
     */
    public function getOrderInfo($order)
    {
        return $this->apiClass->getOrderInfo($order);
    }

    /**
     * @param Order[] &$orders
     * @return array
     */
    public function batchGetOrdersInfo(&$requests)
    {
        return $this->apiClass->batchGetOrdersInfo($requests);
    }

    /**
     * @return bool
     */
    public function canGetOrderInfoForList()
    {
        return $this->apiClass->canGetOrderInfoForList();
    }

    /**
     * @param array $order
     * @return array
     */
    public function getOrderInfoForArray($order)
    {
        return $this->apiClass->getOrderInfoForArray($order);
    }

    /**
     * @param array $orders
     * @return array
     */
    public function batchGetOrderInfoForArray($orders)
    {
        return $this->apiClass->batchGetOrderInfoForArray($orders);
    }

    /**
     * @param Order[] $orders
     * @param string $filePath
     * @param boolean $send
     * @return array
     */
    public function generateList($orders, $filePath, $send = false)
    {
        return $this->apiClass->generateList($orders, $filePath, $send);
    }

    /**
     * @param Order[] $orders
     * @param string $filePath
     * @param boolean $send
     * @return array
     */
    public function generateLabel($orders, $filePath, $send = false)
    {
        return $this->apiClass->generateLabel($orders, $filePath, $send);
    }

    /**
     * @param Order $order
     * @return string|false
    */
    public function generateLabelByOrder($order)
    {
        if ($this->apiClass instanceof DeliveryApiClassLabelInterface) {
            return $this->apiClass->generateLabelByOrder($order);
        }
        return false;
    }

    /**
     * @param array $orderIds
     * @param null $extra
     * @return string|false
     */
    public function generateManifestByOrders($orderIds, $extra = null)
    {
        if ($this->apiClass instanceof DeliveryApiClassManifestInterface) {
            return $this->apiClass->generateManifestByOrders($orderIds, $extra);
        }
        return false;
    }


    /**
     * @param OrderProductInterface[] $orderProducts
     * @return array
     */
    public function getCustomDeliveryPrice($orderProducts)
    {
        return $this->apiClass->getDeliveryPrice($orderProducts);
    }

    /**
     * @param array $ids
     * @return array
     */
    public function getSortedOrderIds($ids)
    {
        return $this->apiClass->getSortedOrderIds($ids);
    }

    /**
     * @param null $path
     */
    protected function initApiClass($path = null)
    {
        if (!is_null($path)) {
            $classPath = str_replace('/', DIRECTORY_SEPARATOR, $path);
            $classPath = str_replace('\\', DIRECTORY_SEPARATOR, $classPath);

            $classPath = Yii::getAlias('@deliveryApiClasses') . $classPath;
        } else if ($this->delivery) {
            if (empty($this->delivery->apiClass)) {
                throw new InvalidParamException(Yii::t('common', 'Отсутствует API-класс для взаимодействия со службой доставки.'));
            }
            $classPath = $this->delivery->apiClass->getFullClassPath();
        }

        if (!isset($classPath) || !file_exists($classPath)) {
            throw new InvalidParamException(Yii::t('common', 'Неверно указан путь до API-класса.'));
        }

        $this->apiClass = $this->createApiClassObject($classPath);
    }

    /**
     * Получение объекта апи-класса
     * @param string $classPath
     * @return DeliveryApiAbstract
     */
    protected function createApiClassObject(string $classPath)
    {
        $classInfo = pathinfo($classPath);
        $className = $classInfo['filename'];
        $classDir = rtrim($classInfo['dirname'], '/\\');
        // Если есть отдельный файл с апи классом для страны текущей КС, то подключаем его
        try {
            if (file_exists($countryClassPath = $classDir . DIRECTORY_SEPARATOR . $this->delivery->country->char_code . DIRECTORY_SEPARATOR . ($countryClassName = $className . $this->delivery->country->char_code) . '.php')) {
                require_once($countryClassPath);
            }
            // Проверяем, что в подключенном файле для конкретной страны есть класс
            if (class_exists($countryClassName)) {
                return new $countryClassName();
            }
        } catch (\Throwable $e) {
            Yii::error($e->getMessage());
        }
        // Если не нашли конкретный класс для страны, берем дефолтный
        require_once($classPath);
        return new $className();
    }
}
