<?php
namespace app\modules\delivery\components\api;

use app\modules\order\models\Order;

/**
 * Interface TransmitterAdapteeInterface
 * @package app\modules\delivery\components\api
 */
interface TransmitterAdapteeInterface
{
    /**
     * @param Order $order
     * @return array
     */
    public function sendOrder($order);

    /**
     * @param Order $order
     * @return array
     */
    public function getOrderInfo($order);

    /**
     * @param Order[] $orders
     * @param string $filePath
     * @param boolean $send
     * @return array
     */
    public function generateList($orders, $filePath, $send = false);
}
