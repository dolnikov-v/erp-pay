<?php
namespace app\modules\delivery\components\api;

use app\modules\order\models\Order;
use app\modules\order\models\OrderLogisticList;
use app\modules\order\models\OrderLogisticListExcel;
use yii\base\Exception;

/**
 * Interface TransmitterTargetInterface
 * @package app\modules\delivery\components\api
 */
interface TransmitterTargetInterface
{
    /**
     * @param Order $order
     * @return array
     */
    public function sendOrder($order);

    /**
     * @param Order $order
     * @return array
     */
    public function getOrderInfo($order);

    /**
     * @param OrderLogisticList $list
     * @return OrderLogisticListExcel
     * @throws Exception
     */
    public function downloadList($list);

    /**
     * @param OrderLogisticList $list
     * @return OrderLogisticListExcel
     * @throws Exception
     */
    public function sendList($list);
}
