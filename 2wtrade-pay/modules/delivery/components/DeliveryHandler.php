<?php

namespace app\modules\delivery\components;


use app\models\Notification;
use app\modules\delivery\components\api\Transmitter;
use app\modules\delivery\components\control\DeliveryControl;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\delivery\models\DeliveryRequestSendResponse;
use app\modules\order\models\OrderStatus;
use app\modules\packager\models\OrderPackaging;
use Yii;
use yii\base\Component;

/**
 * Class Delivery
 * @package app\modules\delivery\components
 */
class DeliveryHandler extends Component
{
    /**
     * @param DeliveryRequest $request
     * @param array $response
     * @throws \Exception
     * @return array
     */
    public static function saveResponseAfterSending($request, $response)
    {
        $answer = [
            'status' => 'fail',
            'messages' => [],
            'error' => '',
        ];

        $sendResponse = null;
        if (isset($response['data']['cs_response']) && !empty($response['data']['cs_response'])) {
            $sendResponse = new DeliveryRequestSendResponse([
                'delivery_request_id' => $request->id,
                'response' => $response['data']['cs_response']
            ]);
            $request->setLastSendResponse($sendResponse);

            $answer['messages'][] = "received delivery response: " . $response['data']['cs_response'];
        }

        if (isset($response['status']) && $response['status'] == Transmitter::STATUS_SUCCESS) {
            $request->sent_at = time();
            $request->order->status_id = OrderStatus::STATUS_DELIVERY_ACCEPTED;

            if (isset($response['data']['response_info'])) {
                $request->response_info = $response['data']['response_info'];
            }

            $request->status = DeliveryRequest::STATUS_IN_PROGRESS;
            $request->tracking = (string)$response['data']['tracking'];
        } else {
            $request->order->status_id = OrderStatus::STATUS_DELIVERY_REJECTED;
            $request->status = DeliveryRequest::STATUS_ERROR;

            $request->api_error = isset($response['message']) ? $response['message'] : 'Неизвестная ошибка.';
            $request->api_error_type = DeliveryRequest::API_ERROR_SYSTEM;

            if (isset($response['typeError']) && $response['typeError'] == DeliveryRequest::API_ERROR_COURIER) {
                $request->api_error_type = DeliveryRequest::API_ERROR_COURIER;
            }

            Yii::$app->notification->send(
                Notification::TRIGGER_ORDER_NOT_SENT_TO_COURIER,
                [
                    'order_id' => $request->order_id,
                    'country' => $request->order->country->name,
                    'delivery' => $request->delivery->name,
                    'error' => $request->api_error,
                ],
                $request->order->country_id
            );

            $deliveryControl = new DeliveryControl();
            if ($otherDeliveryId = $deliveryControl->shouldSendOrderToOtherDelivery($request->order)) {
                $deliveryControl->sendOrderToOtherDelivery($request->order, $otherDeliveryId);
            }

        }

        /** @var DeliveryRequest $requestCheck */
        $requestCheck = DeliveryRequest::findOne($request->id);

        if ($requestCheck && $requestCheck->status == DeliveryRequest::STATUS_PENDING) {
            $request->order->route = Yii::$app->controller->route;
            if (!$request->order->save(false, ['status_id'])) {
                throw new \Exception($request->order->getFirstErrorAsString());
            }

            if ($request->order->status_id == OrderStatus::STATUS_DELIVERY_ACCEPTED) {
                $request->order->initPackageService(true, $request->delivery_id);
            }

            $request->route = Yii::$app->controller->route;

            if (!$request->save()) {
                throw new \Exception($request->getFirstErrorAsString());
            }

            if ($sendResponse && !$sendResponse->save()) {
                throw new \Exception($sendResponse->getFirstErrorAsString());
            }

            $answer['status'] = 'success';
        } else {
            $answer['error'] = 'Во время выполнения запроса, сменился статус заявки.';
            $answer['messages'][] = "delivery request status changed while processing the request. new status: `{$requestCheck->status}`";
        }
        return $answer;
    }

    /**
     * @param DeliveryRequest $request
     * @return array
     */
    public static function prepareDataForSendToUpdateQueue($request)
    {
        return [
            'order_id' => $request->order_id,
            'tracking' => $request->tracking,
            'status_id' => $request->order->status_id,
            'response_info' => $request->response_info,
            'accepted_at' => $request->accepted_at,
            'returned_at' => $request->returned_at,
            'approved_at' => $request->approved_at,
            'partner_name' => $request->deliveryPartner->name,
            'finance_tracking' => $request->finance_tracking,
            'country_char_code' => $request->order->country->char_code,
            'country_slug' => $request->order->country->slug,
            'unshipping_reason' => $request->unshipping_reason,
        ];
    }

    /**
     * @return array
     */
    public static function getDeferredStatuses()
    {
        return [
            OrderStatus::STATUS_DELIVERY_BUYOUT,
            OrderStatus::STATUS_FINANCE_MONEY_RECEIVED,
            OrderStatus::STATUS_DELIVERY_RETURNED,
            OrderStatus::STATUS_DELIVERY_REFUND
        ];
    }
}