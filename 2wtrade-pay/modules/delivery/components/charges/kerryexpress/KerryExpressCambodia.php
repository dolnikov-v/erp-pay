<?php

namespace app\modules\delivery\components\charges\kerryexpress;

use app\modules\delivery\components\charges\ChargesCalculatorAbstract;
use app\modules\delivery\widgets\charges\kerryexpress\KerryExpressCambodiaWidget;
use app\modules\order\models\OrderFinance;
use app\modules\order\models\OrderFinancePrediction;
use app\modules\order\models\OrderProduct;
use Yii;

/**
 * Class KerryExpressCambodia
 * @package app\modules\delivery\components\charges\kerryexpress
 */
class KerryExpressCambodia extends ChargesCalculatorAbstract
{
    public $zipcodes = [];
    public $delivery_fees = [];
    public $return_fees = [0, 0];
    public $storage_fee = 0;
    public $price_fulfilment = 0;
    public $price_package = 0;
    public $cod_fee = 0;
    public $currency_char_code = 'USD';
    public $cod_vat = 0;
    public $oversea_handle = 0;

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            ['zipcodes', 'each', 'rule' => ['string']],
            ['zipcodes', 'each', 'rule' => ['filter', 'filter' => [$this, 'filterZipcodes']]],
            [['delivery_fees', 'return_fees'], 'each', 'rule' => ['number', 'min' => 0]],
            [['currency_char_code', 'vat_type'], 'string'],
            [['cod_fee', 'cod_vat', 'oversea_handle'], 'number', 'min' => 0, 'max' => 100],
            [['price_package', 'price_fulfilment', 'storage_fee'], 'number', 'min' => 0],
        ]);
    }

    /**
     * @param \app\modules\order\models\Order $order
     * @return OrderFinancePrediction
     */
    public function calculate($order)
    {
        $finance = $this->getOrderFinance($order);
        $delivery_fee = 0;
        $return_fee = 0;
        $index = $this->getIndexByZipcode($this->zipcodes, $order->customer_zip);
        if (!is_null($index)) {
            if (isset($this->delivery_fees[$index])) {
                $delivery_fee = doubleval($this->delivery_fees[$index]);
            } else {
                $delivery_fee = 0;
            }
            if (isset($this->return_fees[$index])) {
                $return_fee = doubleval($this->return_fees[$index]);
            } else {
                $return_fee = 0;
            }
        }

        $finance->price_storage = $finance->price_cod = $finance->price_delivery = $finance->price_cod_service =
        $finance->price_delivery_return = $finance->additional_prices = 0;

        $finance->price_package = doubleval($this->price_package);
        $num = OrderProduct::find()->select('quantity')->where(['order_id' => $order->id])->sum('quantity');
        $finance->price_fulfilment = doubleval($this->price_fulfilment * $num);

        $finance->price_delivery = $delivery_fee;
        if ($this->isOrderBuyout($order)) {
            $finance->price_cod = $order->price_total + $order->delivery;
            $finance->price_cod_service = $finance->price_cod * (doubleval($this->cod_fee) / 100);
        } else {
            if ($this->isOrderReturnToSender($order)) {
                $finance->price_delivery_return = $return_fee;
            }
        }

        $rate = $this->getCurrencyRate();
        $fee = $finance->price_delivery + $finance->price_package + $finance->price_fulfilment +
            $finance->price_delivery_return + $finance->price_cod_service / $rate;
        $finance->price_vat = $fee * (doubleval($this->cod_vat) / 100);

        if ($finance->price_cod) {
            $finance->additional_prices = ($finance->price_cod - $fee - $finance->price_vat) *
                doubleval($this->oversea_handle / 100);
        }

        $currency = $this->getCurrency();
        $currencyId = null;
        if (isset($currency)) {
            $currencyId = $currency->id;
        }

        $finance->price_delivery_currency_id = $finance->price_delivery_return_currency_id =
        $finance->price_vat_currency_id = $finance->price_fulfilment_currency_id = $finance->price_package_currency_id =
            $finance->price_storage_currency_id = $currencyId;

        return parent::afterCalculate($finance);
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @param $onlyView
     * @return string
     */
    public function render($form, $onlyView = false)
    {
        return KerryExpressCambodiaWidget::widget([
            'form' => $form,
            'chargesCalculator' => $this,
            'onlyView' => $onlyView,
            'currencies' => self::$currencies
        ]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'zipcodes' => Yii::t('common', 'Индексы региона'),
            'price_fulfilment' => Yii::t('common', 'Стоимость обслуживания заказа (за 1 ед. товара)'),
            'price_package' => Yii::t('common', 'Стоимость упаковки (за 1 заказ)'),
            'cod_fee' => Yii::t('common', 'Плата за наложенный платеж (% от COD)'),
            'price_delivery' => Yii::t('common', 'Стоимость доставки'),
            'price_delivery_return' => Yii::t('common', 'Стоимость возврата'),
            'cod_vat' => Yii::t('common', 'НДС %'),
            'oversea_handle' => Yii::t('common', 'Посредник (Oversea handle) (% от (COD-fee-НДС))'),
            'storage_fee' => Yii::t('common', 'Стоимость аренды склада (в месяц)'),
            'currency_char_code' => Yii::t('common', 'Валюта'),

            'delivery_fees' => \Yii::t('common', 'Стоимость доставки при выкупе'),
            'return_fees' => \Yii::t('common', 'Стоимость доставки при невыкупе'),
        ];
    }

    /**
     * @return array|null
     */
    protected function getOrdersCountByMonths()
    {
        if (is_null($this->countOfOrderByPeriods)) {
            $this->countOfOrderByPeriods = parent::getOrdersCountByMonths();
        }
        return $this->countOfOrderByPeriods;
    }

    /**
     * @param $field
     * @return array
     */
    public function getCalculatingTypeForField($field)
    {
        $answer = [];

        switch ($field) {
            case OrderFinancePrediction::COLUMN_PRICE_COD_SERVICE:
                $answer = [
                    'type' => static::CALCULATING_TYPE_PERCENT,
                    'visible_formula' => "$this->cod_fee%",
                    'fields' => [
                        OrderFinancePrediction::COLUMN_PRICE_COD_SERVICE,
                    ],
                ];
                break;
            case OrderFinancePrediction::COLUMN_PRICE_VAT:
                $answer['type'] = static::CALCULATING_TYPE_PERCENT;
                $answer['visible_formula'] = "$this->cod_vat%";
                $answer['fields'] = [
                    OrderFinancePrediction::COLUMN_PRICE_DELIVERY,
                    OrderFinancePrediction::COLUMN_PRICE_DELIVERY_RETURN,
                    OrderFinancePrediction::COLUMN_PRICE_PACKAGE,
                    OrderFinancePrediction::COLUMN_PRICE_FULFILMENT,
                    OrderFinancePrediction::COLUMN_PRICE_COD_SERVICE,
                ];
                break;
            case OrderFinancePrediction::COLUMN_PRICE_STORAGE:
                {
                    $answer = [
                        'type' => static::CALCULATING_TYPE_MONTHLY_CHARGE,
                        'charge' => doubleval($this->storage_fee),
                        'currency_id' => $this->getCurrency()->id,
                        'currency_rate' => $this->getCurrency()->currencyRate->rate,
                    ];
                    break;
                }
            default:
                return parent::getCalculatingTypeForField($field);
        }
        return $answer;
    }
}