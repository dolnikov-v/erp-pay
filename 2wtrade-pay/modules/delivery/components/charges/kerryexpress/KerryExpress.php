<?php

namespace app\modules\delivery\components\charges\kerryexpress;

use app\modules\delivery\components\charges\ChargesCalculatorAbstract;
use app\modules\delivery\widgets\charges\kerryexpress\KerryExpressWidget;
use app\modules\order\models\OrderFinancePrediction;
use Yii;

/**
 * Class KerryExpress
 * @package app\modules\delivery\components\charges\kerryexpress
 */
class KerryExpress extends ChargesCalculatorAbstract
{
    public $zipcodes = [];
    public $delivery00_fees = [];
    public $delivery10_fees = [];
    public $delivery12_fees = [];
    public $delivery14_fees = [];
    public $delivery16_fees = [];
    public $delivery18_fees = [];
    public $return_fee = 0;
    public $buyout_fee = 0;
    public $storage_area = 0;
    public $storage_fee = 0;
    public $cod_fee = 0;
    public $cod_vat = 0;
    public $delivery_fee_vat = 0;
    public $currency_char_code;

    protected $countOfOrderByPeriods = null;

    protected function getDeliveryFee()
    {
        return $this->delivery00_fees;
    }

    /**
     * @param \app\modules\order\models\Order $order
     * @return OrderFinancePrediction
     */
    public function calculate($order)
    {
        $finance = $this->getOrderFinance($order);
        $index = $this->getIndexByZipcode($this->zipcodes, $order->customer_zip);

        $delivery_fee = 0;

        $weight = $this->getWeightOfParcel($order) / 1000;

        if (!is_null($index)) {
            if ($weight < 1) {
                $delivery_fee = doubleval($this->delivery00_fees[$index] ?? 0);
            }
            if ($weight >= 1 && $weight < 1.2) {
                $delivery_fee = doubleval($this->delivery10_fees[$index] ?? 0);
            }
            if ($weight >= 1.2 && $weight < 1.4) {
                $delivery_fee = doubleval($this->delivery12_fees[$index] ?? 0);
            }
            if ($weight >= 1.4 && $weight < 1.6) {
                $delivery_fee = doubleval($this->delivery14_fees[$index] ?? 0);
            }
            if ($weight >= 1.6 && $weight < 1.8) {
                $delivery_fee = doubleval($this->delivery16_fees[$index] ?? 0);
            }
            if ($weight >= 1.8) {
                $delivery_fee = doubleval($this->delivery18_fees[$index] ?? 0);
            }
        }

        $finance->price_cod = 0;
        $finance->price_storage = 0;
        $finance->price_delivery = 0;
        $finance->price_cod_service = 0;
        $finance->price_delivery_return = 0;

        $cod_vat = 0;
        if ($this->isOrderBuyout($order)) {
            $finance->price_cod = $order->price_total + $order->delivery;
            $finance->price_delivery = $delivery_fee + doubleval($this->buyout_fee);
            $finance->price_cod_service = ($finance->price_cod) * (doubleval($this->cod_fee) / 100);
            $cod_vat = ($finance->price_cod) * (doubleval($this->cod_vat) / 100);
        } else {
            if ($this->isOrderReturnToSender($order)) {
                $finance->price_delivery_return = $delivery_fee + doubleval($this->return_fee);
            }
        }
        $delivery_fee_vat = ($finance->price_delivery + $finance->price_delivery_return) * (doubleval($this->delivery_fee_vat) / 100);

        // налог на услуги сервиса может быть в произвольной валюте $currencyId
        $rate = $this->getCurrencyRate();
        $finance->price_vat = $cod_vat + $delivery_fee_vat * $rate;


        $currency = $this->getCurrency();
        $currencyId = null;
        if (isset($currency)) {
            $currencyId = $currency->id;
        }

        $finance->price_delivery_currency_id = $finance->price_delivery_return_currency_id = $currencyId;

        return parent::afterCalculate($finance);
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @param $onlyView
     * @return string
     */
    public function render($form, $onlyView = false)
    {
        return KerryExpressWidget::widget([
            'form' => $form,
            'chargesCalculator' => $this,
            'onlyView' => $onlyView,
            'currencies' => self::$currencies
        ]);
    }

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            ['zipcodes', 'each', 'rule' => ['string']],
            ['zipcodes', 'each', 'rule' => ['filter', 'filter' => [$this, 'filterZipcodes']]],
            ['delivery00_fees', 'each', 'rule' => ['number', 'min' => 0]],
            ['delivery10_fees', 'each', 'rule' => ['number', 'min' => 0]],
            ['delivery12_fees', 'each', 'rule' => ['number', 'min' => 0]],
            ['delivery14_fees', 'each', 'rule' => ['number', 'min' => 0]],
            ['delivery16_fees', 'each', 'rule' => ['number', 'min' => 0]],
            ['delivery18_fees', 'each', 'rule' => ['number', 'min' => 0]],
            [['return_fee', 'buyout_fee', 'storage_area', 'storage_fee'], 'number', 'min' => 0],
            [['cod_fee', 'cod_vat', 'delivery_fee_vat'], 'number', 'min' => 0, 'max' => 100],
            [['currency_char_code'], 'string'],
        ]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            // 1)
            'zipcodes' => Yii::t('common', 'Индексы региона'),
            'delivery00_fees' => Yii::t('common', 'Стоимость доставки 0.0-1.0 кг'),
            'delivery10_fees' => Yii::t('common', 'Стоимость доставки 1.0-1.2 кг'),
            'delivery12_fees' => Yii::t('common', 'Стоимость доставки 1.2-1.4 кг'),
            'delivery14_fees' => Yii::t('common', 'Стоимость доставки 1.4-1.6 кг'),
            'delivery16_fees' => Yii::t('common', 'Стоимость доставки 1.6-1.8 кг'),
            'delivery18_fees' => Yii::t('common', 'Стоимость доставки 1.8-2.0 кг'),
            // 2)
            'return_fee' => Yii::t('common', 'Стоимость отказа'),
            // 3)
            'buyout_fee' => Yii::t('common', 'Стоимость выкупа'),
            // 4)
            'storage_area' => Yii::t('common', 'Площадь склада (м2)'),
            'storage_fee' => Yii::t('common', 'Стоимость аренды склада (1м2)'),
            // 5)
            'cod_fee' => Yii::t('common', 'COD fee (%)'),
            // 6)
            'cod_vat' => Yii::t('common', 'НДС на COD (%)'),
            // 7)
            'delivery_fee_vat' => Yii::t('common', 'НДС на delivery fee (%)'),

            'currency_char_code' => Yii::t('common', 'Валюта'),
        ];
    }

    /**
     * @return array|null
     */
    protected function getOrdersCountByMonths()
    {
        if (is_null($this->countOfOrderByPeriods)) {
            $this->countOfOrderByPeriods = parent::getOrdersCountByMonths();
        }
        return $this->countOfOrderByPeriods;
    }

    /**
     * @param $field
     * @return array
     */
    public function getCalculatingTypeForField($field)
    {
        $answer = [];

        switch ($field) {
            case OrderFinancePrediction::COLUMN_PRICE_COD_SERVICE:
                $answer = [
                    'type' => static::CALCULATING_TYPE_PERCENT,
                    'visible_formula' => "$this->cod_fee%",
                    'fields' => [
                        OrderFinancePrediction::COLUMN_PRICE_COD,
                    ],
                ];
                break;
            case OrderFinancePrediction::COLUMN_PRICE_VAT:
                $answer['type'] = static::CALCULATING_TYPE_PERCENT;
                $answer['visible_formula'] = "$this->cod_vat% + $this->delivery_fee_vat%";
                $answer['fields'] = [
                    OrderFinancePrediction::COLUMN_PRICE_COD,
                    OrderFinancePrediction::COLUMN_PRICE_DELIVERY,
                    OrderFinancePrediction::COLUMN_PRICE_DELIVERY_RETURN,
                ];
                break;
            case OrderFinancePrediction::COLUMN_PRICE_STORAGE: {
                $answer = [
                    'type' => static::CALCULATING_TYPE_MONTHLY_CHARGE,
                    'charge' => doubleval($this->storage_area) * doubleval($this->storage_fee),
                    'currency_id' => $this->getCurrency()->id,
                    'currency_rate' => $this->getCurrency()->currencyRate->rate,
                ];
                break;
            }
            default:
                return parent::getCalculatingTypeForField($field);
        }
        return $answer;
    }
}