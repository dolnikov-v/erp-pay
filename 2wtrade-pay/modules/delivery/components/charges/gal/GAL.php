<?php

namespace app\modules\delivery\components\charges\gal;


use app\modules\delivery\components\charges\ChargesCalculatorAbstract;
use app\modules\delivery\widgets\charges\helpers\DeliveryChargesForRegion;
use app\modules\order\models\OrderFinancePrediction;
use Yii;

/**
 * Class GAL
 * @package app\modules\delivery\components\charges\gal
 */
class GAL extends ChargesCalculatorAbstract
{
    /**
     * @var array
     */
    public $zipcodes = [];

    /**
     * @var array
     */
    public $delivery_fees = [];

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            ['zipcodes', 'each', 'rule' => ['string']],
            ['delivery_fees', 'each', 'rule' => ['number', 'min' => 0]],
            ['zipcodes', 'each', 'rule' => ['filter', 'filter' => [$this, 'filterZipcodes']]],
            [['currency_char_code'], 'string'],
        ]);
    }

    /**
     * @param \app\modules\order\models\Order $order
     * @return OrderFinancePrediction
     */
    public function calculate($order)
    {
        $finance = $this->getOrderFinance($order);
        $finance->price_delivery = 0;
        $index = $this->getIndexByZipcode($this->zipcodes, $order->customer_zip);
        if (!is_null($index)) {
            if (isset($this->delivery_fees[$index])) {
                $finance->price_delivery = doubleval($this->delivery_fees[$index]);
            } else {
                $finance->price_delivery = 0;
            }
        }
        if ($this->isOrderBuyout($order)) {
            $finance->price_cod = $order->delivery + $order->price_total;
        } else {
            $finance->price_cod = 0;
        }
        return parent::afterCalculate($finance);
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @param $onlyView
     * @return string
     */
    public function render($form, $onlyView = false)
    {
        return DeliveryChargesForRegion::widget([
            'model' => $this,
            'onlyView' => $onlyView,
            'regionAttribute' => 'zipcodes',
            'chargesAttributes' => ['delivery_fees'],
            'form' => $form,
            'currencies' => self::$currencies
        ]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'zipcodes' => Yii::t('common', 'Индексы регионов'),
            'delivery_fees' => Yii::t('common', 'Цена за доставку'),
        ];
    }
}