<?php

namespace app\modules\delivery\components\charges;

use app\models\Currency;
use app\modules\delivery\widgets\charges\helpers\CommonDeliveryCharges;
use app\modules\order\models\OrderFinancePrediction;
use yii\helpers\ArrayHelper;

/**
 * Class DifferentDeliveryChargeByStatus
 * @package app\modules\delivery\components\charges
 */
class DifferentDeliveryChargeByStatus extends ChargesCalculatorAbstract
{
    /**
     * @var int
     */
    public $delivery_fee = 0;
    /**
     * @var int
     */
    public $return_fee = 0;
    /**
     * @var int
     */
    public $cod_fee = 0;
    /**
     * @var int
     */
    public $vat_percent = 0;

    /**
     * @var int
     */
    public $storage_fee = 0;

    /**
     * @var int
     */
    public $storage_area = 0;


    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['delivery_fee', 'return_fee', 'storage_fee', 'storage_area'], 'number', 'min' => 0],
            [['cod_fee', 'vat_percent'], 'number', 'min' => 0, 'max' => 100],
            [['currency_char_code', 'vat_type'], 'string'],
        ]);
    }

    /**
     * @param \app\modules\order\models\Order $order
     * @return \app\modules\order\models\OrderFinancePrediction
     */
    public function calculate($order)
    {
        $finance = $this->getOrderFinance($order);
        $currency = $this->getCurrency();
        $currencyId = null;
        if (isset($currency)) {
            $currencyId = $currency->id;
        }
        $finance->price_delivery = doubleval($this->delivery_fee);
        if ($this->isOrderBuyout($order)) {
            $finance->price_cod = $order->delivery + $order->price_total;
            $finance->price_cod_service = $finance->price_cod * (doubleval($this->cod_fee) / 100);
            if ($this->vat_type == static::VAT_TYPE_COD) {
                $finance->price_vat = $finance->price_cod * (doubleval($this->vat_percent) / 100);
            }
        } else {
            if ($this->isOrderReturnToSender($order)) {
                $finance->price_delivery = 0;
                $finance->price_delivery_return = doubleval($this->return_fee);
            }
            $finance->price_cod = 0;
            $finance->price_cod_service = 0;
            if ($this->vat_type == static::VAT_TYPE_COD) {
                $finance->price_vat = 0;
            }
        }
        if ($this->vat_type == static::VAT_TYPE_SERVICE) {
            $chargesSum = $finance->price_delivery + $finance->price_delivery_return + $finance->price_storage + $finance->price_fulfilment + $finance->price_packing + $finance->price_package;
            $finance->price_vat = $chargesSum * (doubleval($this->vat_percent) / 100);
            $finance->price_vat_currency_id = $currencyId;
        }

        $finance->price_delivery_currency_id = $finance->price_delivery_return_currency_id = $currencyId;

        return parent::afterCalculate($finance);
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @param $onlyView
     * @return string
     */
    public function render($form, $onlyView = false)
    {
        return CommonDeliveryCharges::widget([
            'form' => $form,
            'model' => $this,
            'items' => [
                [
                    'attribute' => 'delivery_fee',
                    'options' => [
                        'disabled' => $onlyView
                    ],
                ],
                [
                    'attribute' => 'return_fee',
                    'options' => [
                        'disabled' => $onlyView
                    ],
                ],
                [
                    'attribute' => 'cod_fee',
                    'options' => [
                        'disabled' => $onlyView
                    ],
                ],
                [
                    'attribute' => 'vat_percent',
                    'options' => [
                        'disabled' => $onlyView
                    ],
                ],
                [
                    'attribute' => 'vat_type',
                    'type' => 'select2List',
                    'items' => static::getVatTypes(),
                    'options' => [
                        'disabled' => $onlyView
                    ],
                ],
                [
                    'attribute' => 'storage_area',
                    'options' => [
                        'disabled' => $onlyView,
                    ]
                ],
                [
                    'attribute' => 'storage_fee',
                    'options' => [
                        'disabled' => $onlyView,
                    ]
                ],
                [
                    'attribute' => 'currency_char_code',
                    'type' => 'select2List',
                    'items' => ArrayHelper::map(Currency::find()->all(), 'char_code', 'name'),
                    'options' => [
                        'length' => false,
                        'disabled' => $onlyView,
                    ]
                ],
            ]
        ]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'delivery_fee' => \Yii::t('common', 'Стоимость доставки при выкупе'),
            'return_fee' => \Yii::t('common', 'Стоимость доставки при невыкупе'),
            'storage_area' => \Yii::t('common', 'Размер склада (м2/палет)'),
            'storage_fee' => \Yii::t('common', 'Стоимость склада (1м2/палет)')
        ]);
    }

    /**
     * @param $field
     * @return array
     */
    public function getCalculatingTypeForField($field)
    {
        $answer = [];
        switch ($field) {
            case OrderFinancePrediction::COLUMN_PRICE_COD_SERVICE:
                $answer = [
                    'type' => static::CALCULATING_TYPE_PERCENT,
                    'visible_formula' => "$this->cod_fee%",
                    'fields' => [
                        OrderFinancePrediction::COLUMN_PRICE_COD,
                    ],
                ];
                break;
            case OrderFinancePrediction::COLUMN_PRICE_VAT:
                $answer['type'] = static::CALCULATING_TYPE_PERCENT;
                $answer['visible_formula'] = "$this->vat_percent%";
                if ($this->vat_type == static::VAT_TYPE_COD) {
                    $answer['fields'] = [
                        OrderFinancePrediction::COLUMN_PRICE_COD,
                    ];
                } else {
                    $answer['fields'] = [
                        OrderFinancePrediction::COLUMN_PRICE_DELIVERY,
                        OrderFinancePrediction::COLUMN_PRICE_DELIVERY_RETURN,
                        OrderFinancePrediction::COLUMN_PRICE_STORAGE,
                        OrderFinancePrediction::COLUMN_PRICE_FULFILMENT,
                        OrderFinancePrediction::COLUMN_PRICE_PACKAGE,
                        OrderFinancePrediction::COLUMN_PRICE_PACKING,
                        OrderFinancePrediction::COLUMN_PRICE_DELIVERY_BACK,
                        OrderFinancePrediction::COLUMN_PRICE_REDELIVERY,
                        OrderFinancePrediction::COLUMN_PRICE_ADDRESS_CORRECTION,
                    ];
                }
                break;
            case OrderFinancePrediction::COLUMN_PRICE_STORAGE:
                $answer = [
                    'type' => ChargesCalculatorAbstract::CALCULATING_TYPE_MONTHLY_CHARGE,
                    'charge' => doubleval($this->storage_fee) * doubleval($this->storage_area),
                    'currency_id' => $this->getCurrency()->id,
                    'currency_rate' => $this->getCurrency()->currencyRate->rate,
                ];
                break;
            default:
                $answer['type'] = static::CALCULATING_TYPE_NUMBER;
                break;
        }

        return $answer;
    }
}