<?php

namespace app\modules\delivery\components\charges\starken;


use app\models\Currency;
use app\modules\delivery\components\charges\ChargesCalculatorAbstract;
use app\modules\delivery\widgets\charges\helpers\CommonDeliveryCharges;
use app\modules\delivery\widgets\charges\helpers\DeliveryChargesForRegion;
use app\modules\order\models\OrderFinancePrediction;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class Starken
 * @package app\modules\delivery\components\charges\starken
 */
class Starken extends ChargesCalculatorAbstract
{
    /**
     * @var array
     */
    public $zipcodes = [];
    /**
     * @var array
     */
    public $delivery_fees = [];
    /**
     * @var int
     */
    public $return_fee = 0;
    /**
     * @var int
     */
    public $cod_fee = 0;
    /**
     * @var int
     */
    public $vat_percent = 0;

    public $vat_type = self::VAT_TYPE_SERVICE;

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            ['zipcodes', 'each', 'rule' => ['string']],
            ['zipcodes', 'each', 'rule' => ['filter', 'filter' => [$this, 'filterZipcodes']]],
            ['delivery_fees', 'each', 'rule' => ['number', 'min' => 0]],
            [['vat_percent'], 'number', 'min' => 0, 'max' => 100],
            [['return_fee', 'cod_fee',], 'number', 'min' => 0],
            [['currency_char_code'], 'string'],
        ]);
    }

    /**
     * @param \app\modules\order\models\Order $order
     * @return \app\modules\order\models\OrderFinancePrediction
     */
    public function calculate($order)
    {
        $finance = $this->getOrderFinance($order);
        $delivery_fee = 0;

        if ($index = $this->getIndexByZipcode($this->zipcodes, $order->customer_city)) {
            $delivery_fee = isset($this->delivery_fees[$index]) ? $this->delivery_fees[$index] : 0;
        }

        $currency = $this->getCurrency();
        $currencyId = null;
        if (isset($currency)) {
            $currencyId = $currency->id;
        }
        $finance->price_delivery = doubleval($delivery_fee);
        if ($this->isOrderBuyout($order)) {
            $finance->price_cod = $order->price_total + $order->delivery;
            $finance->price_cod_service = doubleval($this->cod_fee);
        } else {
            if ($this->isOrderReturnToSender($order)) {
                $finance->price_delivery = doubleval($this->return_fee);
            }
            $finance->price_cod = 0;
            $finance->price_cod_service = 0;
        }

        $finance->price_vat = 0;
        $finance->price_delivery *= 1 + (doubleval($this->vat_percent) / 100);
        $finance->price_cod_service *= 1 + (doubleval($this->vat_percent) / 100);

        $finance->price_delivery_currency_id = $currencyId;

        return parent::afterCalculate($finance);
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @param $onlyView
     * @return string
     */
    public function render($form, $onlyView = false)
    {
        return CommonDeliveryCharges::widget([
                'form' => $form,
                'model' => $this,
                'items' => [
                    [
                        'attribute' => 'return_fee',
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'cod_fee',
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'vat_percent',
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'currency_char_code',
                        'type' => 'select2List',
                        'items' => ArrayHelper::map(Currency::find()->all(), 'char_code', 'name'),
                        'options' => [
                            'length' => false,
                            'disabled' => $onlyView,
                        ]
                    ]
                ]
            ]) . DeliveryChargesForRegion::widget([
                'form' => $form,
                'model' => $this,
                'onlyView' => $onlyView,
                'regionAttribute' => 'zipcodes',
                'chargesAttributes' => ['delivery_fees']
            ]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'cod_fee' => \Yii::t('common', 'Плата за наложенный платеж'),
            'return_fee' => \Yii::t('common', 'Стоимость доставки при невыкупе'),
            'delivery_fees' => \Yii::t('common', 'Стоимость доставки при выкупе'),
            'zipcodes' => Yii::t('common', 'Города'),
        ]);
    }


    /**
     * @param $field
     * @return array
     */
    public function getCalculatingTypeForField($field)
    {
        $answer = [];
        switch ($field) {
            case OrderFinancePrediction::COLUMN_PRICE_COD_SERVICE:
                $answer = [
                    'type' => static::CALCULATING_TYPE_PERCENT,
                    'visible_formula' => "$this->cod_fee% * {$this->vat_percent}%",
                    'fields' => [
                        OrderFinancePrediction::COLUMN_PRICE_COD,
                    ],
                ];
                break;
            default:
                $answer['type'] = static::CALCULATING_TYPE_NUMBER;
                break;
        }

        return $answer;
    }
}