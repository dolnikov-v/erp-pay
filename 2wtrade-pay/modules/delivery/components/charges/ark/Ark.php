<?php

namespace app\modules\delivery\components\charges\ark;


use app\models\Currency;
use app\modules\delivery\components\charges\ChargesCalculatorAbstract;
use app\modules\delivery\widgets\charges\helpers\CommonDeliveryCharges;
use app\modules\delivery\widgets\charges\helpers\DeliveryChargesForRegion;
use app\modules\order\models\OrderFinancePrediction;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class Ark
 * @package app\modules\delivery\components\charges\ark
 */
class Ark extends ChargesCalculatorAbstract
{
    /**
     * @var array
     */
    public $zipcodes = [];

    /**
     * @var array
     */
    public $delivery_fees = [];

    /**
     * @var array
     */
    public $discounts_fees_1k = [];
    /**
     * @var array
     */
    public $discounts_fees_more_1k = [];

    /**
     * @var int
     */
    public $discount_fee_1k = 0;

    /**
     * @var int
     */
    public $discount_fee_more_1k = 0;

    /**
     * @var int
     */
    public $cod_fee = 0;

    /**
     * @var int
     */
    public $vat_percent = 0;

    /**
     * @var int
     */
    public $fulfillment_fee = 0;


    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            ['zipcodes', 'each', 'rule' => ['string']],
            ['zipcodes', 'each', 'rule' => ['filter', 'filter' => [$this, 'filterZipcodes']]],
            [
                ['delivery_fees', 'discounts_fees_1k', 'discounts_fees_more_1k'],
                'each',
                'rule' => ['number', 'min' => 0]
            ],
            [['currency_char_code', 'vat_type'], 'string'],
            [['cod_fee', 'vat_percent', 'discount_fee_1k', 'discount_fee_more_1k'], 'number', 'min' => 0, 'max' => 100],
            [['fulfillment_fee'], 'number', 'min' => 0],
        ]);
    }

    /**
     * @param \app\modules\order\models\Order $order
     * @return OrderFinancePrediction
     */
    public function calculate($order)
    {
        $finance = $this->getOrderFinance($order);
        $delivery_fee = 0;
        $discount_1k = null;
        $discount_more_1k = null;

        $index = $this->getIndexByZipcode($this->zipcodes, $order->customer_zip);
        if (!is_null($index)) {
            $delivery_fee = doubleval($this->delivery_fees[$index] ?? 0);
            $discount_1k = $this->discounts_fees_1k[$index] ?? null;
            $discount_more_1k = $this->discounts_fees_more_1k[$index] ?? null;
        }


        $day = $this->getOrderSendingMonth($order, 'd.m.Y');
        $days = $this->getOrdersCountByDays();

        $dayOrdersCount = $days[$day] ?? 1;

        if ($dayOrdersCount < 1000) {
            if (is_null($discount_1k)) {
                $delivery_fee -= $delivery_fee * (doubleval($this->discount_fee_1k) / 100);
            } else {
                $delivery_fee -= doubleval($discount_1k);
            }
        } else {
            if (is_null($discount_more_1k)) {
                $delivery_fee -= $delivery_fee * (doubleval($this->discount_fee_more_1k) / 100);
            } else {
                $delivery_fee -= doubleval($discount_more_1k);
            }
        }

        $weight = $this->getWeightOfParcel($order, true);
        $currency = $this->getCurrency();
        $currencyId = null;
        if (isset($currency)) {
            $currencyId = $currency->id;
        }
        $finance->price_delivery = $delivery_fee * $weight;

        $finance->price_fulfilment = doubleval($this->fulfillment_fee);

        if ($this->isOrderBuyout($order)) {
            $finance->price_cod = $order->delivery + $order->price_total;
            $finance->price_cod_service = $finance->price_cod * (doubleval($this->cod_fee) / 100);
        } else {
            $finance->price_cod = 0;
            $finance->price_cod_service = 0;
        }
        $chargesSum = $finance->price_delivery + $finance->price_delivery_return + $finance->price_storage + $finance->price_fulfilment + $finance->price_packing + $finance->price_package;
        $finance->price_vat = $chargesSum * (doubleval($this->vat_percent) / 100);

        $finance->price_fulfilment_currency_id = $finance->price_delivery_currency_id = $finance->price_vat_currency_id = $currencyId;

        return parent::afterCalculate($finance);
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @param $onlyView
     * @return string
     */
    public function render($form, $onlyView = false)
    {
        return CommonDeliveryCharges::widget([
                'form' => $form,
                'model' => $this,
                'items' => [
                    [
                        'attribute' => 'fulfillment_fee',
                        'options' => [
                            'disabled' => $onlyView
                        ]
                    ],
                    [
                        'attribute' => 'discount_fee_1k',
                        'options' => [
                            'disabled' => $onlyView
                        ]
                    ],
                    [
                        'attribute' => 'discount_fee_more_1k',
                        'options' => [
                            'disabled' => $onlyView
                        ]
                    ],
                    [
                        'length' => 3,
                    ],
                    [
                        'attribute' => 'cod_fee',
                        'options' => [
                            'disabled' => $onlyView
                        ]
                    ],
                    [
                        'attribute' => 'vat_percent',
                        'options' => [
                            'disabled' => $onlyView
                        ]
                    ],
                    [
                        'attribute' => 'currency_char_code',
                        'type' => 'select2List',
                        'items' => ArrayHelper::map(Currency::find()->all(), 'char_code', 'name'),
                        'length' => 2,
                        'options' => [
                            'length' => false,
                            'disabled' => $onlyView,
                        ]
                    ],
                ]
            ])
            . DeliveryChargesForRegion::widget([
                'form' => $form,
                'model' => $this,
                'onlyView' => $onlyView,
                'regionAttribute' => 'zipcodes',
                'chargesAttributes' => ['delivery_fees', 'discounts_fees_1k', 'discounts_fees_more_1k']
            ]);
    }

    /**
     * @return array
     */
    protected function customAttributeLabels()
    {
        return [
            'discounts_fees_1k' => Yii::t('common', 'Сумма скидки (заказов в день менее 1000)'),
            'discounts_fees_more_1k' => Yii::t('common', 'Сумма скидки (заказов в день более 1000)'),
            'discount_fee_1k' => Yii::t('common', 'Процент скидки (заказов в день менее 1000)(%)'),
            'discount_fee_more_1k' => Yii::t('common', 'Процент скидки (заказов в день более 1000)(%)'),
        ];
    }

    /**
     * @param $field
     * @return array
     */
    public function getCalculatingTypeForField($field)
    {
        $answer = [];
        switch ($field) {
            case OrderFinancePrediction::COLUMN_PRICE_COD_SERVICE:
                $answer = [
                    'type' => static::CALCULATING_TYPE_PERCENT,
                    'visible_formula' => "$this->cod_fee%",
                    'fields' => [
                        OrderFinancePrediction::COLUMN_PRICE_COD,
                    ],
                ];
                break;
            case OrderFinancePrediction::COLUMN_PRICE_VAT:
                $answer['type'] = static::CALCULATING_TYPE_PERCENT;
                $answer['visible_formula'] = "$this->vat_percent%";
                if ($this->vat_type == static::VAT_TYPE_COD) {
                    $answer['fields'] = [
                        OrderFinancePrediction::COLUMN_PRICE_COD,
                    ];
                } else {
                    $answer['fields'] = [
                        OrderFinancePrediction::COLUMN_PRICE_DELIVERY,
                        OrderFinancePrediction::COLUMN_PRICE_DELIVERY_RETURN,
                        OrderFinancePrediction::COLUMN_PRICE_STORAGE,
                        OrderFinancePrediction::COLUMN_PRICE_FULFILMENT,
                        OrderFinancePrediction::COLUMN_PRICE_PACKAGE,
                        OrderFinancePrediction::COLUMN_PRICE_PACKING,
                    ];
                }
                break;
            default:
                $answer['type'] = static::CALCULATING_TYPE_NUMBER;
                break;
        }

        return $answer;
    }
}