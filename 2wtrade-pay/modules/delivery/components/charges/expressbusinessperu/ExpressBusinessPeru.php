<?php

namespace app\modules\delivery\components\charges\expressbusinessperu;

use app\models\Currency;
use app\modules\delivery\components\charges\ChargesCalculatorAbstract;
use app\modules\delivery\widgets\charges\helpers\CommonDeliveryCharges;
use app\modules\order\models\OrderFinancePrediction;
use yii\helpers\ArrayHelper;

/**
 * Class ExpressBusinessPeru
 * @package app\modules\delivery\components\charges
 */
class ExpressBusinessPeru extends ChargesCalculatorAbstract
{
    public $delivery_fee_1 = 0;
    public $delivery_fee_2 = 0;
    public $vat_percent = 0;


    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['delivery_fee_1', 'delivery_fee_2'], 'number', 'min' => 0],
            [['vat_percent'], 'number', 'min' => 0, 'max' => 100],
            [['currency_char_code'], 'string'],
        ]);
    }

    /**
     * @param \app\modules\order\models\Order $order
     * @return \app\modules\order\models\OrderFinancePrediction
     */
    public function calculate($order)
    {
        $finance = $this->getOrderFinance($order);
        $weight = $this->getWeightOfParcel($order);
        $delivery_fee = doubleval($this->delivery_fee_1);
        if ($weight > 300) {
            $delivery_fee = doubleval($this->delivery_fee_2);
        }
        $currency = $this->getCurrency();
        $currencyId = null;
        if (isset($currency)) {
            $currencyId = $currency->id;
        }
        $finance->price_delivery = $delivery_fee;

        if ($this->isOrderBuyout($order)) {
            $finance->price_cod = $order->price_total + $order->delivery;
            $finance->price_vat = $finance->price_cod * (doubleval($this->vat_percent) / 100);
        } else {
            $finance->price_cod = 0;
            $finance->price_vat = 0;
        }

        $finance->price_delivery_currency_id = $currencyId;

        return parent::afterCalculate($finance);
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @param $onlyView
     * @return string
     */
    public function render($form, $onlyView = false)
    {
        return CommonDeliveryCharges::widget([
            'form' => $form,
            'model' => $this,
            'items' => [
                [
                    'attribute' => 'delivery_fee_1',
                    'options' => [
                        'disabled' => $onlyView,
                    ]
                ],
                [
                    'attribute' => 'delivery_fee_2',
                    'options' => [
                        'disabled' => $onlyView,
                    ]
                ],
                [
                    'attribute' => 'vat_percent',
                    'options' => [
                        'disabled' => $onlyView,
                    ]
                ],
                [
                    'attribute' => 'currency_char_code',
                    'type' => 'select2List',
                    'items' => ArrayHelper::map(Currency::find()->all(), 'char_code', 'name'),
                    'options' => [
                        'length' => false,
                        'disabled' => $onlyView,
                    ]
                ]
            ]
        ]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'delivery_fee_1' => \Yii::t('common', 'Стоимость доставки (300г.)'),
            'delivery_fee_2' => \Yii::t('common', 'Стоимость доставки (более 300г.)')
        ]);
    }


    /**
     * @param $field
     * @return array
     */
    public function getCalculatingTypeForField($field)
    {
        $answer = [];
        switch ($field) {
            case OrderFinancePrediction::COLUMN_PRICE_VAT:
                $answer['type'] = static::CALCULATING_TYPE_PERCENT;
                $answer['visible_formula'] = "$this->vat_percent%";
                $answer['fields'] = [
                    OrderFinancePrediction::COLUMN_PRICE_COD,
                ];
                break;
            default:
                $answer['type'] = static::CALCULATING_TYPE_NUMBER;
                break;
        }

        return $answer;
    }
}