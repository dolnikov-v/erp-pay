<?php

namespace app\modules\delivery\components\charges\estafeta;


use app\models\Currency;
use app\modules\delivery\components\charges\ChargesCalculatorAbstract;
use app\modules\delivery\widgets\charges\helpers\CommonDeliveryCharges;
use app\modules\order\models\OrderFinancePrediction;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class Estafeta
 * @package app\modules\delivery\components\charges\estafeta
 */
class Estafeta extends ChargesCalculatorAbstract
{

    /**
     * @var int
     */
    public $delivery_fee_1500 = 0;

    /**
     * @var int
     */
    public $delivery_fee_2000 = 0;

    /**
     * @var int
     */
    public $delivery_fee_2500 = 0;

    /**
     * @var int
     */
    public $return_fee_1500 = 0;
    /**
     * @var int
     */
    public $return_fee_2000 = 0;
    /**
     * @var int
     */
    public $return_fee_2500 = 0;

    /**
     * @var int
     */
    public $vat_percent = 0;

    /**
     * @var string
     */
    public $currency_char_code = 'USD';

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [
                [
                    'delivery_fee_1500',
                    'delivery_fee_2000',
                    'delivery_fee_2500',
                    'return_fee_1500',
                    'return_fee_2000',
                    'return_fee_2500'
                ],
                'number',
                'min' => 0
            ],
            [['vat_percent'], 'number', 'min' => 0, 'max' => 100],
            [['currency_char_code'], 'string'],
        ]);
    }

    /**
     * @param \app\modules\order\models\Order $order
     * @return OrderFinancePrediction
     */
    public function calculate($order)
    {
        $finance = $this->getOrderFinance($order);

        $periods = $this->getOrdersCountByMonths();
        $month = $this->getOrderSendingMonth($order);
        $delivery_fee = doubleval($this->delivery_fee_1500);
        $return_fee = doubleval($this->return_fee_1500);

        if (isset($periods[$month])) {
            if ($periods[$month] > 1500) {
                $delivery_fee = doubleval($this->delivery_fee_2000);
                $return_fee = doubleval($this->return_fee_2000);
            } elseif ($periods[$month] > 2000) {
                $delivery_fee = doubleval($this->delivery_fee_2500);
                $return_fee = doubleval($this->return_fee_2500);
            }
        }

        $currency = $this->getCurrency();
        $currencyId = null;
        if (isset($currency)) {
            $currencyId = $currency->id;
        }
        $finance->price_delivery = $delivery_fee;
        if ($this->isOrderBuyout($order)) {
            $finance->price_cod = $order->price_total + $order->delivery;
            $finance->price_vat = $finance->price_cod * (doubleval($this->vat_percent) / 100);
        } else {
            if ($this->isOrderReturnToSender($order)) {
                $finance->price_delivery = $return_fee;
            }
            $finance->price_vat = 0;
            $finance->price_cod = 0;
        }

        $finance->price_delivery_currency_id = $currencyId;

        return parent::afterCalculate($finance);
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @param $onlyView
     * @return string
     */
    public function render($form, $onlyView = false)
    {
        return CommonDeliveryCharges::widget([
            'form' => $form,
            'model' => $this,
            'items' => [
                [
                    'attribute' => 'delivery_fee_1500',
                    'options' => [
                        'disabled' => $onlyView,
                    ]
                ],
                [
                    'attribute' => 'delivery_fee_2000',
                    'options' => [
                        'disabled' => $onlyView,
                    ]
                ],
                [
                    'attribute' => 'delivery_fee_2500',
                    'options' => [
                        'disabled' => $onlyView,
                    ]
                ],
                [
                    'attribute' => 'return_fee_1500',
                    'options' => [
                        'disabled' => $onlyView,
                    ]
                ],
                [
                    'attribute' => 'return_fee_2000',
                    'options' => [
                        'disabled' => $onlyView,
                    ]
                ],
                [
                    'attribute' => 'return_fee_2500',
                    'options' => [
                        'disabled' => $onlyView,
                    ]
                ],
                [
                    'attribute' => 'vat_percent',
                    'options' => [
                        'disabled' => $onlyView,
                    ]
                ],
                [
                    'attribute' => 'currency_char_code',
                    'type' => 'select2List',
                    'items' => ArrayHelper::map(Currency::find()->all(), 'char_code', 'name'),
                    'options' => [
                        'length' => false,
                        'disabled' => $onlyView,
                    ]
                ],
            ],
        ]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'delivery_fee_1500' => Yii::t('common', 'Стоимость доставки (1-1500 заказов в месяц)'),
            'delivery_fee_2000' => Yii::t('common', 'Стоимость доставки (1501-2000 заказов в месяц)'),
            'delivery_fee_2500' => Yii::t('common', 'Стоимость доставки (более 2000 заказов в месяц)'),
            'currency_char_code' => Yii::t('common', 'Валюта'),
        ]);
    }


    /**
     * @param $field
     * @return array
     */
    public function getCalculatingTypeForField($field)
    {
        $answer = [];
        switch ($field) {
            case OrderFinancePrediction::COLUMN_PRICE_VAT:
                $answer['type'] = static::CALCULATING_TYPE_PERCENT;
                $answer['visible_formula'] = "$this->vat_percent%";
                $answer['fields'] = [
                    OrderFinancePrediction::COLUMN_PRICE_COD,
                ];
                break;
            default:
                $answer['type'] = static::CALCULATING_TYPE_NUMBER;
                break;
        }

        return $answer;
    }
}