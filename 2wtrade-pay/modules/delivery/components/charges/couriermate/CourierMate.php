<?php

namespace app\modules\delivery\components\charges\couriermate;


use app\modules\delivery\components\charges\ChargesCalculatorAbstract;
use app\modules\order\models\OrderFinancePrediction;

/**
 * Class CourierMate
 * @package app\modules\delivery\components\charges\couriermate
 */
class CourierMate extends ChargesCalculatorAbstract
{
    public $delivery_fees = [];
    public $zipcodes = [];
    public $storage_fee = 0;
    public $printing_fee = 0;
    public $packing_fee = 0;
    public $cod_fee = 0;

    protected $countOfOrderByPeriods = null;

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            ['zipcodes', 'each', 'rule' => ['string']],
            ['zipcodes', 'each', 'rule' => ['filter', 'filter' => [$this, 'filterZipcodes']]],
            [['delivery_fees'], 'each', 'rule' => ['number', 'min' => 0]],
            [['printing_fee', 'packing_fee', 'storage_fee'], 'number', 'min' => 0],
            [['cod_fee'], 'number', 'min' => 0, 'max' => 100],
            [['currency_char_code'], 'string'],
        ]);
    }

    /**
     * @param \app\modules\order\models\Order $order
     * @return OrderFinancePrediction
     */
    public function calculate($order)
    {
        $finance = $this->getOrderFinance($order);
        $delivery_fee = 0;
        $index = $this->getIndexByZipcode($this->zipcodes, $order->customer_zip);
        if (!is_null($index)) {
            $delivery_fee = doubleval($this->delivery_fees[$index]);
        }

        $finance->price_storage = 0;
        $finance->price_delivery = $delivery_fee;
        $finance->price_fulfilment = doubleval($this->printing_fee);

        if($this->isOrderBuyout($order)) {
            $finance->price_cod = $order->delivery + $order->price_total;
            $finance->price_cod_service = $finance->price_cod * (doubleval($this->cod_fee) / 100);
        } else {
            $finance->price_cod = 0;
            $finance->price_cod_service = 0;
        }

        return parent::afterCalculate($finance);
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @param bool $onlyView
     */
    public function render($form, $onlyView = false)
    {
        // TODO: Implement render() method.
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        $currencyCode = $this->delivery->country->currency->char_code;
        return [
            'zipcodes' => \Yii::t('common', 'Индексы региона'),
            'delivery_fees' => \Yii::t('common', 'Стоимость доставки ({currency})', ['currency' => $currencyCode]),
            'storage_fee' => \Yii::t('common', 'Полная стоимость аренды склада ({currency})', ['currency' => $currencyCode]),
            'printing_fee' => \Yii::t('common', 'Стоимость этикетки ({currency})', ['currency' => $currencyCode]),
            'packing_fee' => \Yii::t('common', 'Стоимость обслуживания 100 заказов ({currency})', ['currency' => $currencyCode]),
            'cod_fee' => \Yii::t('common', 'Плата за наложенный платеж (%)')
        ];
    }

    /**
     * @return array|null
     */
    protected function getOrdersCountByMonths()
    {
        if (is_null($this->countOfOrderByPeriods)) {
            $this->countOfOrderByPeriods = parent::getOrdersCountByMonths();
        }
        return $this->countOfOrderByPeriods;
    }


    /**
     * @param $field
     * @return array
     */
    public function getCalculatingTypeForField($field)
    {
        $answer = [];
        switch ($field) {
            case OrderFinancePrediction::COLUMN_PRICE_COD_SERVICE:
                $answer = [
                    'type' => static::CALCULATING_TYPE_PERCENT,
                    'visible_formula' => "$this->cod_fee%",
                    'fields' => [
                        OrderFinancePrediction::COLUMN_PRICE_COD,
                    ],
                ];
                break;
            case OrderFinancePrediction::COLUMN_PRICE_STORAGE:
                $answer = [
                    'type' => ChargesCalculatorAbstract::CALCULATING_TYPE_MONTHLY_CHARGE,
                    'charge' => doubleval($this->storage_fee),
                    'currency_id' => $this->getCurrency()->id,
                    'currency_rate' => $this->getCurrency()->currencyRate->rate,
                ];
                break;
            case OrderFinancePrediction::COLUMN_PRICE_PACKING:
                $answer = [
                    'type' => ChargesCalculatorAbstract::CALCULATING_TYPE_MONTHLY_CHARGE,
                    'charge' => doubleval($this->packing_fee),
                    'currency_id' => $this->getCurrency()->id,
                    'currency_rate' => $this->getCurrency()->currencyRate->rate,
                ];
                break;
            default:
                $answer['type'] = static::CALCULATING_TYPE_NUMBER;
                break;
        }

        return $answer;
    }
}