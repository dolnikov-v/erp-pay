<?php

namespace app\modules\delivery\components\charges\pcp;

use app\modules\delivery\components\charges\ChargesCalculatorAbstract;
use app\modules\delivery\widgets\charges\pcp\PCPWidget;
use app\modules\order\models\OrderFinancePrediction;
use Yii;

/**
 * Class PCP
 * @package app\modules\delivery\components\charges\pcp
 */
class PCP extends ChargesCalculatorAbstract
{
    /**
     * @var integer
     */
    public $cod_fee = 0;
    /**
     * @var int
     */
    public $price_vat = 0;
    /**
     * @var array
     */
    public $zipcodes = [];
    /**
     * @var array
     */
    public $delivery_fees = [];

    public $vat_type = self::VAT_TYPE_COD;

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['cod_fee', 'price_vat'], 'number', 'min' => 0, 'max' => 100],
            ['zipcodes', 'each', 'rule' => ['string']],
            ['zipcodes', 'each', 'rule' => ['filter', 'filter' => [$this, 'filterZipcodes']]],
            ['delivery_fees', 'each', 'rule' => ['number', 'min' => 0]],
            [['currency_char_code'], 'string'],
        ]);
    }

    /**
     * @param \app\modules\order\models\Order $order
     * @return OrderFinancePrediction
     */
    public function calculate($order)
    {
        $finance = $this->getOrderFinance($order);
        $delivery_fee = 0;
        $index = $this->getIndexByZipcode($this->zipcodes, $order->customer_zip);
        if (!empty($index)) {
            $delivery_fee = $this->delivery_fees[$index] ?? 0;
        }

        $weight = $this->getWeightOfParcel($order, true);

        $finance->price_delivery = doubleval($delivery_fee) * $weight;
        if (!$this->isOrderBuyout($order)) {
            $finance->price_cod = 0;
            $finance->price_cod_service = 0;
            $finance->price_vat = 0;
        } else {
            $finance->price_cod = $order->price_total + $order->delivery;
            $finance->price_cod_service = $finance->price_cod * (doubleval($this->cod_fee) / 100);
            $finance->price_vat = $finance->price_cod * (doubleval($this->price_vat) / 100);
        }
        return parent::afterCalculate($finance);
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @param $onlyView
     * @return string
     */
    public function render($form, $onlyView = false)
    {
        return PCPWidget::widget(['form' => $form, 'chargesCalculator' => $this, 'currencies' => self::$currencies, 'onlyView' => $onlyView]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'cod_fee' => Yii::t('common', 'Плата за наложенный платеж (%)'),
            'price_vat' => Yii::t('common', 'НДС (%)'),
            'zipcodes' => Yii::t('common', 'Индексы региона'),
            'delivery_fees' => Yii::t('common', 'Цена за доставку 1кг ({currency})', ['currency' => $this->delivery->country->currency->char_code]),
        ];
    }


    /**
     * @param $field
     * @return array
     */
    public function getCalculatingTypeForField($field)
    {
        $answer = [];
        switch ($field) {
            case OrderFinancePrediction::COLUMN_PRICE_COD_SERVICE:
                $answer = [
                    'type' => static::CALCULATING_TYPE_PERCENT,
                    'visible_formula' => "$this->cod_fee%",
                    'fields' => [
                        OrderFinancePrediction::COLUMN_PRICE_COD,
                    ],
                ];
                break;
            case OrderFinancePrediction::COLUMN_PRICE_VAT:
                $answer['type'] = static::CALCULATING_TYPE_PERCENT;
                $answer['visible_formula'] = "$this->price_vat%";
                if ($this->vat_type == static::VAT_TYPE_COD) {
                    $answer['fields'] = [
                        OrderFinancePrediction::COLUMN_PRICE_COD,
                    ];
                } else {
                    $answer['fields'] = [
                        OrderFinancePrediction::COLUMN_PRICE_DELIVERY,
                        OrderFinancePrediction::COLUMN_PRICE_DELIVERY_RETURN,
                        OrderFinancePrediction::COLUMN_PRICE_STORAGE,
                        OrderFinancePrediction::COLUMN_PRICE_FULFILMENT,
                        OrderFinancePrediction::COLUMN_PRICE_PACKAGE,
                        OrderFinancePrediction::COLUMN_PRICE_PACKING,
                        OrderFinancePrediction::COLUMN_PRICE_DELIVERY_BACK,
                        OrderFinancePrediction::COLUMN_PRICE_REDELIVERY,
                        OrderFinancePrediction::COLUMN_PRICE_ADDRESS_CORRECTION,
                    ];
                }
                break;
            default:
                $answer['type'] = static::CALCULATING_TYPE_NUMBER;
                break;
        }

        return $answer;
    }
}