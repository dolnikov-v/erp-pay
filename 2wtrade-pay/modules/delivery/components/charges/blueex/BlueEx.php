<?php

namespace app\modules\delivery\components\charges\blueex;


use app\modules\delivery\components\charges\ChargesCalculatorAbstract;
use app\modules\delivery\widgets\charges\blueex\BlueExWidget;
use app\modules\order\models\OrderFinancePrediction;
use Yii;

/**
 * Class BlueEx
 * @package app\modules\delivery\components\charges\blueex
 */
class BlueEx extends ChargesCalculatorAbstract
{
    public $packaging_fee = 0;
    public $storage_fee = 0;
    public $delivery_fees = [];
    public $delivery_fees_1kg = [];
    public $delivery_fees_add_kg = [];
    public $zipcodes = [];
    public $vat_percent = 0;
    public $cod_fee = 0;

    /**
     * @var string
     */
    public $currency_char_code = 'USD';

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            ['zipcodes', 'each', 'rule' => ['string']],
            ['zipcodes', 'each', 'rule' => ['filter', 'filter' => [$this, 'filterZipcodes']]],
            [['delivery_fees', 'delivery_fees_1kg', 'delivery_fees_add_kg'], 'each', 'rule' => ['number', 'min' => 0]],
            [['packing_fee', 'storage_fee'], 'number', 'min' => 0],
            [['vat_percent', 'cod_fee'], 'number', 'min' => 0, 'max' => 100],
            [['currency_char_code'], 'string'],
        ]);
    }

    /**
     * @param \app\modules\order\models\Order $order
     * @return OrderFinancePrediction
     */
    public function calculate($order)
    {
        $finance = $this->getOrderFinance($order);

        $weight = $this->getWeightOfParcel($order);
        $delivery_fee_500g = 0;
        $delivery_fee_1kg = 0;
        $delivery_fee_add_kg = 0;

        $index = $this->getIndexByZipcode($this->zipcodes, $order->customer_zip);
        if (!is_null($index)) {
            $delivery_fee_500g = doubleval($this->delivery_fees[$index] ?? 0);
            $delivery_fee_1kg = doubleval($this->delivery_fees_1kg[$index] ?? 0);
            $delivery_fee_add_kg = doubleval($this->delivery_fees_add_kg[$index] ?? 0);
        }

        if ($weight <= 500) {
            $delivery_fee = $delivery_fee_500g;
        } elseif ($weight <= 1000) {
            $delivery_fee = $delivery_fee_1kg;
        } else {
            $delivery_fee = $delivery_fee_1kg + ((ceil($weight / 1000) - 1) * $delivery_fee_add_kg);
        }

        $productCount = 0;

        foreach ($order->orderProducts as $product) {
            $productCount += $product->quantity;
        }

        $currency = $this->getCurrency();
        $currencyId = null;
        if (isset($currency)) {
            $currencyId = $currency->id;
        }
        $finance->price_storage = $productCount * doubleval($this->storage_fee);
        $finance->price_delivery = $delivery_fee;
        $finance->price_packing = doubleval($this->packaging_fee);

        if ($this->isOrderBuyout($order)) {
            $finance->price_cod = $order->delivery + $order->price_total;
            $finance->price_cod_service = $finance->price_cod * (doubleval($this->cod_fee) / 100);
            $finance->price_vat = $finance->price_cod * (doubleval($this->vat_percent) / 100);
        } else {
            $finance->price_cod = 0;
            $finance->price_cod_service = 0;
            $finance->price_vat = 0;
        }

        $finance->price_storage_currency_id = $finance->price_delivery_currency_id = $finance->price_packing_currency_id = $currencyId;

        return parent::afterCalculate($finance);
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @param $onlyView
     * @return string
     */
    public function render($form, $onlyView = false)
    {
        return BlueExWidget::widget([
            'form' => $form,
            'chargesCalculator' => $this,
            'currencies' => self::$currencies,
            'onlyView' => $onlyView
        ]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'packaging_fee' => Yii::t('common', 'Плата за упаковку', ['currency' => $this->delivery->country->currency->char_code]),
            'storage_fee' => Yii::t('common', 'Плата за хранение одной единицы товара', ['currency' => $this->delivery->country->currency->char_code]),
            'delivery_fees' => Yii::t('common', 'Плата за доставку заказа весом менее 0,5кг.', ['currency' => $this->delivery->country->currency->char_code]),
            'delivery_fees_1kg' => Yii::t('common', 'Плата за доставку заказа весом 1кг.', ['currency' => $this->delivery->country->currency->char_code]),
            'delivery_fees_add_kg' => Yii::t('common', 'Плата за доставку каждого доп. кг.', ['currency' => $this->delivery->country->currency->char_code]),
            'zipcodes' => Yii::t('common', 'Индексы региона'),
            'cod_fee' => Yii::t('common', 'Плата на наложенный платеж (%)'),
            'vat_percent' => Yii::t('common', 'НДС (%)'),
            'currency_char_code' => Yii::t('common', 'Валюта'),
        ];
    }


    /**
     * @param $field
     * @return array
     */
    public function getCalculatingTypeForField($field)
    {
        $answer = [];
        switch ($field) {
            case OrderFinancePrediction::COLUMN_PRICE_COD_SERVICE:
                $answer = [
                    'type' => static::CALCULATING_TYPE_PERCENT,
                    'visible_formula' => "$this->cod_fee%",
                    'fields' => [
                        OrderFinancePrediction::COLUMN_PRICE_COD,
                    ],
                ];
                break;
            case OrderFinancePrediction::COLUMN_PRICE_VAT:
                $answer['type'] = static::CALCULATING_TYPE_PERCENT;
                $answer['visible_formula'] = "$this->vat_percent%";
                $answer['fields'] = [
                    OrderFinancePrediction::COLUMN_PRICE_COD,
                ];
                break;
            default:
                $answer['type'] = static::CALCULATING_TYPE_NUMBER;
                break;
        }

        return $answer;
    }
}