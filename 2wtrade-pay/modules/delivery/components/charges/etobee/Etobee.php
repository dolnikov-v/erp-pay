<?php

namespace app\modules\delivery\components\charges\etobee;

use app\modules\delivery\components\charges\ChargesCalculatorAbstract;
use app\modules\delivery\widgets\charges\etobee\EtobeeWidget;
use app\modules\order\models\OrderFinancePrediction;
use Yii;

/**
 * Class Etobee
 * @package app\modules\delivery\components\charges\etobee
 */
class Etobee extends ChargesCalculatorAbstract
{
    public $cod_fee = 0;
    public $inbound_fee = 0;
    public $outbound_fee = 0;
    public $storage_fee = 0;
    public $zipcodes = [];
    public $delivery_fees = [];

    protected $countOfOrderByPeriods = null;

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['inbound_fee', 'outbound_fee', 'storage_fee'], 'number', 'min' => 0],
            [['cod_fee'], 'number', 'min' => 0, 'max' => 100],
            ['zipcodes', 'each', 'rule' => ['string']],
            ['delivery_fees', 'each', 'rule' => ['number', 'min' => 0]],
            ['zipcodes', 'each', 'rule' => ['filter', 'filter' => [$this, 'filterZipcodes']]],
            [['currency_char_code'], 'string'],
        ]);
    }

    /**
     * @param \app\modules\order\models\Order $order
     * @return OrderFinancePrediction
     */
    public function calculate($order)
    {
        $finance = $this->getOrderFinance($order);
        $zipcode = trim($order->customer_zip);
        $delivery_fee = 0;
        if (!empty($zipcode)) {
            foreach ($this->zipcodes as $key => $zipcodes) {
                if (in_array($zipcode, explode(',', $zipcodes))) {
                    $delivery_fee = doubleval($this->delivery_fees[$key]);
                    break;
                }
            }
        }


        $finance->price_storage = 0;
        $finance->price_fulfilment = doubleval($this->inbound_fee);
        $finance->price_package = doubleval($this->outbound_fee);
        $finance->price_delivery = $delivery_fee;
        if($this->isOrderBuyout($order)) {
            $finance->price_cod = $order->price_total + $order->delivery;
            $finance->price_cod_service = $finance->price_cod * (doubleval($this->cod_fee) / 100);
        } else {
            $finance->price_cod = 0;
            $finance->price_cod_service = 0;
        }
        return parent::afterCalculate($finance);
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @param $onlyView
     * @return string
     */
    public function render($form, $onlyView = false)
    {
        return EtobeeWidget::widget(['form' => $form, 'chargesCalculator' => $this, 'currencies' => self::$currencies, 'onlyView' => $onlyView]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        $currencyCode = $this->delivery->country->currency->char_code;
        return [
            'cod_fee' => Yii::t('common', 'Плата за наложенный платеж (%)'),
            'inbound_fee' => Yii::t('common', 'Плата за входящий заказ ({currency})', ['currency' => $currencyCode]),
            'outbound_fee' => Yii::t('common', 'Плата за исходящий заказ ({currency})', ['currency' => $currencyCode]),
            'storage_fee' => Yii::t('common', 'Плата за аренду склада ({currency})', ['currency' => $currencyCode]),
            'delivery_fees' => Yii::t('common', 'Цена за доставку ({currency})', ['currency' => $currencyCode]),
            'zipcodes' => Yii::t('common', 'Индексы регионов'),
        ];
    }

    /**
     * @return array|null
     */
    protected function getOrdersCountByMonths()
    {
        if (is_null($this->countOfOrderByPeriods)) {
            $this->countOfOrderByPeriods = parent::getOrdersCountByMonths();
        }
        return $this->countOfOrderByPeriods;
    }


    /**
     * @param $field
     * @return array
     */
    public function getCalculatingTypeForField($field)
    {
        $answer = [];
        switch ($field) {
            case OrderFinancePrediction::COLUMN_PRICE_COD_SERVICE:
                $answer = [
                    'type' => static::CALCULATING_TYPE_PERCENT,
                    'visible_formula' => "$this->cod_fee%",
                    'fields' => [
                        OrderFinancePrediction::COLUMN_PRICE_COD,
                    ],
                ];
                break;
            case OrderFinancePrediction::COLUMN_PRICE_STORAGE:
                $answer = [
                    'type' => ChargesCalculatorAbstract::CALCULATING_TYPE_MONTHLY_CHARGE,
                    'charge' => doubleval($this->storage_fee),
                    'currency_id' => $this->getCurrency()->id,
                    'currency_rate' => $this->getCurrency()->currencyRate->rate,
                ];
                break;
            default:
                $answer['type'] = static::CALCULATING_TYPE_NUMBER;
                break;
        }

        return $answer;
    }
}