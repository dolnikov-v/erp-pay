<?php

namespace app\modules\delivery\components\charges\yst;


use app\models\Currency;
use app\modules\delivery\components\charges\ChargesCalculatorAbstract;
use app\modules\delivery\widgets\charges\helpers\CommonDeliveryCharges;
use app\modules\delivery\widgets\charges\helpers\DeliveryChargesForRegion;
use app\modules\order\models\OrderFinancePrediction;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class YST
 * @package app\modules\delivery\components\charges\yst
 */
class YST extends ChargesCalculatorAbstract
{
    public $zipcodes = [];
    public $delivery_fees_1kg = [];
    public $delivery_fees_add_kg = [];
    public $add_return_fees = [];

    public $return_fee_3kg = 0;

    public $storage_fee = 0; // TODO стоимость указана за 1 паллет, хз как считать количество паллетов

    public $fulfilment_fee_3kg = 0;

    public $vat_percent = 0;
    public $cod_fee_1000 = 0;
    public $cod_fee_2000 = 0;
    public $cod_fee_3000 = 0;
    public $cod_fee_over_3000 = 0;

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            ['zipcodes', 'each', 'rule' => ['string']],
            ['zipcodes', 'each', 'rule' => ['filter', 'filter' => [$this, 'filterZipcodes']]],
            [['delivery_fees_1kg', 'delivery_fees_add_kg', 'add_return_fees'], 'each', 'rule' => ['number', 'min' => 0]],
            [['currency_char_code', 'vat_type'], 'string'],
            [['vat_percent'], 'number', 'min' => 0, 'max' => 100],
            [
                [
                    'return_fee_3kg',
                    'storage_fee',
                    'fulfilment_fee_3kg',
                    'cod_fee_1000',
                    'cod_fee_2000',
                    'cod_fee_3000',
                    'cod_fee_over_3000'
                ],
                'number',
                'min' => 0
            ],
        ]);
    }


    protected function getDeliveryFee()
    {
        return $this->delivery_fees_1kg;
    }

    /**
     * @param \app\modules\order\models\Order $order
     * @return \app\modules\order\models\OrderFinancePrediction
     */
    public function calculate($order)
    {
        $finance = $this->getOrderFinance($order);
        $delivery_fee_1kg = 0;
        $delivery_fee_add_kg = 0;
        $add_return_fee = 0;

        $index = $this->getIndexByZipcode($this->zipcodes, $order->customer_zip);

        if (!is_null($index)) {
            $delivery_fee_1kg = $this->delivery_fees_1kg[$index] ?? 0;
            $delivery_fee_add_kg = $this->delivery_fees_add_kg[$index] ?? 0;
            $add_return_fee = $this->add_return_fees[$index] ?? 0;
        }

        $weight = $this->getWeightOfParcel($order, true);
        $count = $this->getCountOfParcel($order);

        $totalPrice = $order->price_total + $order->delivery;

        $return_fee = $this->return_fee_3kg + $add_return_fee;
        $fulfilment_fee = $this->fulfilment_fee_3kg * $count;

        if ($totalPrice <= 1000) {
            $cod_fee = $this->cod_fee_1000;
        } elseif ($totalPrice <= 2000) {
            $cod_fee = $this->cod_fee_2000;
        } elseif ($totalPrice <= 3000) {
            $cod_fee = $this->cod_fee_3000;
        } else {
            $cod_fee = $this->cod_fee_over_3000;
        }

        $currency = $this->getCurrency();
        $currencyId = null;
        if (isset($currency)) {
            $currencyId = $currency->id;
        }
        $finance->price_delivery = doubleval($delivery_fee_1kg) + max(0, $weight - 1) * doubleval($delivery_fee_add_kg);
        $finance->price_delivery *= 1.18 * 1.18;
        $finance->price_fulfilment = doubleval($fulfilment_fee);
        if ($this->isOrderBuyout($order)) {
            $finance->price_cod = $totalPrice;
            $finance->price_cod_service = doubleval($cod_fee);
            $finance->price_delivery_return = 0;
        } else {
            if ($this->isOrderReturnToSender($order)) {
                $finance->price_delivery_return = doubleval($return_fee);
            } else {
                $finance->price_delivery_return = 0;
            }
            $finance->price_cod = 0;
            $finance->price_cod_service = 0;
        }

        $finance->price_vat = ($finance->price_delivery + $finance->price_delivery_return + $finance->price_fulfilment + $finance->price_storage) * (doubleval($this->vat_percent) / 100);

        $finance->price_delivery_currency_id = $finance->price_fulfilment_currency_id = $finance->price_delivery_return_currency_id = $finance->price_vat_currency_id = $currencyId;

        return parent::afterCalculate($finance);
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @param $onlyView
     * @return string
     */
    public function render($form, $onlyView = false)
    {
        return CommonDeliveryCharges::widget([
                'form' => $form,
                'model' => $this,
                'items' => [
                    [
                        'attribute' => 'fulfilment_fee_3kg',
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'return_fee_3kg',
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'cod_fee_1000',
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'cod_fee_2000',
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'cod_fee_3000',
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'cod_fee_over_3000',
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'storage_fee',
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'vat_percent',
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'currency_char_code',
                        'type' => 'select2List',
                        'items' => ArrayHelper::map(Currency::find()->all(), 'char_code', 'name'),
                        'length' => 2,
                        'options' => [
                            'length' => false,
                            'disabled' => $onlyView,
                        ]
                    ],
                ]
            ])
            . DeliveryChargesForRegion::widget([
                'form' => $form,
                'model' => $this,
                'onlyView' => $onlyView,
                'regionAttribute' => 'zipcodes',
                'chargesAttributes' => ['delivery_fees_1kg', 'delivery_fees_add_kg', 'add_return_fees']
            ]);
    }

    /**
     * @return array
     */
    public function customAttributeLabels()
    {
        return [
            'delivery_fees_1kg' => Yii::t('common', 'Стоимость доставки (1кг)'),
            'delivery_fees_add_kg' => Yii::t('common', 'Стоимость доставки (более 1кг за каждый кг.)'),
            'add_return_fees' => Yii::t('common', 'Доп. стоимость возврата'),
            'return_fee_3kg' => Yii::t('common', 'Стоимость возврата'),
            'storage_fee' => Yii::t('common', 'Стоимость хранения (1 паллет)'),
            'fulfilment_fee_3kg' => Yii::t('common', 'Стоимость обслуживания заказа (за 1 шт. товара)'),
            'cod_fee_1000' => Yii::t('common', 'Плата за наложенный платеж (COD менее 1000RM)'),
            'cod_fee_2000' => Yii::t('common', 'Плата за наложенный платеж (COD менее 2000RM)'),
            'cod_fee_3000' => Yii::t('common', 'Плата за наложенный платеж (COD менее 3000RM)'),
            'cod_fee_over_3000' => Yii::t('common', 'Плата за наложенный платеж (COD более 3000RM)'),
        ];
    }


    /**
     * @param $field
     * @return array
     */
    public function getCalculatingTypeForField($field)
    {
        $answer = [];
        switch ($field) {
            case OrderFinancePrediction::COLUMN_PRICE_VAT:
                $answer['type'] = static::CALCULATING_TYPE_PERCENT;
                $answer['visible_formula'] = "$this->vat_percent%";
                if ($this->vat_type == static::VAT_TYPE_COD) {
                    $answer['fields'] = [
                        OrderFinancePrediction::COLUMN_PRICE_COD,
                    ];
                } else {
                    $answer['fields'] = [
                        OrderFinancePrediction::COLUMN_PRICE_DELIVERY,
                        OrderFinancePrediction::COLUMN_PRICE_DELIVERY_RETURN,
                        OrderFinancePrediction::COLUMN_PRICE_STORAGE,
                        OrderFinancePrediction::COLUMN_PRICE_FULFILMENT,
                        OrderFinancePrediction::COLUMN_PRICE_PACKAGE,
                        OrderFinancePrediction::COLUMN_PRICE_PACKING,
                        OrderFinancePrediction::COLUMN_PRICE_DELIVERY_BACK,
                        OrderFinancePrediction::COLUMN_PRICE_REDELIVERY,
                        OrderFinancePrediction::COLUMN_PRICE_ADDRESS_CORRECTION,
                    ];
                }
                break;
            default:
                $answer['type'] = static::CALCULATING_TYPE_NUMBER;
                break;
        }

        return $answer;
    }
}