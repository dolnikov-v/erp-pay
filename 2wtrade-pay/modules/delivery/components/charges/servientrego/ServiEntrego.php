<?php

namespace app\modules\delivery\components\charges\servientrego;

use app\modules\delivery\components\charges\ChargesCalculatorAbstract;
use app\modules\delivery\widgets\charges\servientrego\ServiEntregoWidget;
use app\modules\order\models\OrderFinancePrediction;
use yii\helpers\ArrayHelper;

/**
 * Class ServiEntrego
 * @package app\modules\delivery\components\charges\servientrego
 */
class ServiEntrego extends ChargesCalculatorAbstract
{
    public $zipcodes = [];
    public $delivery_fees = [];
    public $cod_fee = 0;
    public $vat_percent = 0;

    public $vat_type = self::VAT_TYPE_SERVICE;

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            ['zipcodes', 'each', 'rule' => ['string']],
            ['zipcodes', 'each', 'rule' => ['filter', 'filter' => [$this, 'filterZipcodes']]],
            ['delivery_fees', 'each', 'rule' => ['number', 'min' => 0]],
            [['cod_fee', 'vat_percent'], 'number', 'min' => 0, 'max' => 100],
            [['currency_char_code'], 'string'],
        ]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'zipcodes' => \Yii::t('common', 'Индексы региона'),
            'delivery_fees' => \Yii::t('common', 'Плата за доставку ({currency})', ['currency' => $this->delivery->country->currency->char_code]),
            'cod_fee' => \Yii::t('common', 'Плата за наложенный платеж (%)'),
        ]);
    }

    /**
     * @param \app\modules\order\models\Order $order
     * @return OrderFinancePrediction
     */
    public function calculate($order)
    {
        $finance = $this->getOrderFinance($order);
        $finance->price_delivery = 0;
        $index = $this->getIndexByZipcode($this->zipcodes, $order->customer_zip);
        if (!is_null($index)) {
            $finance->price_delivery = doubleval($this->delivery_fees[$index] ?? 0);
        }

        if ($this->isOrderBuyout($order)) {
            $finance->price_cod = $order->delivery + $order->price_total;
            $finance->price_cod_service = $finance->price_cod * (doubleval($this->cod_fee) / 100);
        } else {
            $finance->price_cod = 0;
            $finance->price_cod_service = 0;
        }

        $finance->price_vat = ($finance->price_delivery) * (doubleval($this->vat_percent) / 100);
        return parent::afterCalculate($finance);
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @param $onlyView
     * @return string
     */
    public function render($form, $onlyView = false)
    {
        return ServiEntregoWidget::widget([
            'chargesCalculator' => $this,
            'form' => $form,
            'currencies' => self::$currencies,
            'onlyView' => $onlyView
        ]);
    }

    /**
     * @param $field
     * @return array
     */
    public function getCalculatingTypeForField($field)
    {
        $answer = [];
        switch ($field) {
            case OrderFinancePrediction::COLUMN_PRICE_COD_SERVICE:
                $answer = [
                    'type' => static::CALCULATING_TYPE_PERCENT,
                    'visible_formula' => "$this->cod_fee%",
                    'fields' => [
                        OrderFinancePrediction::COLUMN_PRICE_COD,
                    ],
                ];
                break;
            case OrderFinancePrediction::COLUMN_PRICE_VAT:
                $answer['type'] = static::CALCULATING_TYPE_PERCENT;
                $answer['visible_formula'] = "$this->vat_percent%";
                if ($this->vat_type == static::VAT_TYPE_COD) {
                    $answer['fields'] = [
                        OrderFinancePrediction::COLUMN_PRICE_COD,
                    ];
                } else {
                    $answer['fields'] = [
                        OrderFinancePrediction::COLUMN_PRICE_DELIVERY,
                        OrderFinancePrediction::COLUMN_PRICE_DELIVERY_RETURN,
                        OrderFinancePrediction::COLUMN_PRICE_STORAGE,
                        OrderFinancePrediction::COLUMN_PRICE_FULFILMENT,
                        OrderFinancePrediction::COLUMN_PRICE_PACKAGE,
                        OrderFinancePrediction::COLUMN_PRICE_PACKING,
                        OrderFinancePrediction::COLUMN_PRICE_DELIVERY_BACK,
                        OrderFinancePrediction::COLUMN_PRICE_REDELIVERY,
                        OrderFinancePrediction::COLUMN_PRICE_ADDRESS_CORRECTION,
                    ];
                }
                break;
            default:
                $answer['type'] = static::CALCULATING_TYPE_NUMBER;
                break;
        }

        return $answer;
    }
}