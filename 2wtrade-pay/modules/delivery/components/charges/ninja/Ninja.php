<?php

namespace app\modules\delivery\components\charges\ninja;

use app\modules\delivery\components\charges\ChargesCalculatorAbstract;
use app\modules\order\models\Order;
use app\modules\order\models\OrderFinancePrediction;
use Yii;

/**
 * Class Ninja
 * @package app\modules\delivery\components\charges\ninja
 */
abstract class Ninja extends ChargesCalculatorAbstract
{
    /**
     * @var int
     */
    public $price_vat = 0;
    /**
     * @var int
     */
    public $cod_fee = 0;
    /**
     * @var int
     */
    public $delivery_fee = 0;
    /**
     * @var int
     */
    public $packing_fee = 0;

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['price_vat', 'cod_fee', 'delivery_fee', 'packing_fee',], 'number', 'min' => 0],
            [['price_vat', 'cod_fee'], 'number', 'min' => 0, 'max' => 100],
            [['currency_char_code'], 'string'],
        ]);
    }

    /**
     * @param \yii\db\ActiveQuery $query
     */
    public function calculateForBatch($query)
    {
        foreach ($query->batch(1000) as $orders) {
            /**
             * @var Order[] $orders
             */
            foreach ($orders as $order) {
                $finance = $this->calculate($order);
                $finance->save();
            }
        }
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'price_vat' => Yii::t('common', 'НДС (%)'),
            'cod_fee' => Yii::t('common', 'Плата за наложенный платеж (%)'),
            'delivery_fee' => Yii::t('common', 'Доставка одного заказа'),
            'packing_fee' => Yii::t('common', 'Обслуживание одного заказа'),
            'currency_char_code' => Yii::t('common', 'Валюта'),
        ];
    }


    /**
     * @param $field
     * @return array
     */
    public function getCalculatingTypeForField($field)
    {
        $answer = [];
        switch ($field) {
            case OrderFinancePrediction::COLUMN_PRICE_COD_SERVICE:
                $answer = [
                    'type' => static::CALCULATING_TYPE_PERCENT,
                    'visible_formula' => "$this->cod_fee%",
                    'fields' => [
                        OrderFinancePrediction::COLUMN_PRICE_COD,
                    ],
                ];
                break;
            case OrderFinancePrediction::COLUMN_PRICE_VAT:
                $answer['type'] = static::CALCULATING_TYPE_PERCENT;
                $answer['visible_formula'] = "$this->price_vat%";
                if ($this->vat_type == static::VAT_TYPE_COD) {
                    $answer['fields'] = [
                        OrderFinancePrediction::COLUMN_PRICE_COD,
                    ];
                } else {
                    $answer['fields'] = [
                        OrderFinancePrediction::COLUMN_PRICE_DELIVERY,
                        OrderFinancePrediction::COLUMN_PRICE_DELIVERY_RETURN,
                        OrderFinancePrediction::COLUMN_PRICE_STORAGE,
                        OrderFinancePrediction::COLUMN_PRICE_FULFILMENT,
                        OrderFinancePrediction::COLUMN_PRICE_PACKAGE,
                        OrderFinancePrediction::COLUMN_PRICE_PACKING,
                        OrderFinancePrediction::COLUMN_PRICE_DELIVERY_BACK,
                        OrderFinancePrediction::COLUMN_PRICE_REDELIVERY,
                        OrderFinancePrediction::COLUMN_PRICE_ADDRESS_CORRECTION,
                    ];
                }
                break;
            default:
                $answer['type'] = static::CALCULATING_TYPE_NUMBER;
                break;
        }

        return $answer;
    }
}