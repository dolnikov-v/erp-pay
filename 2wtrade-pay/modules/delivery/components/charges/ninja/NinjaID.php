<?php

namespace app\modules\delivery\components\charges\ninja;


use app\modules\delivery\widgets\charges\ninja\NinjaIDWidget;
use app\modules\order\models\OrderFinancePrediction;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class NinjaID
 * @package app\modules\delivery\components\charges\ninja
 */
class NinjaID extends Ninja
{
    /**
     * @var array
     */
    public $zipcodes = [];

    /**
     * @var array
     */
    public $delivery_fees = [];

    /**
     * @return array
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            ['zipcodes', 'each', 'rule' => ['string']],
            ['zipcodes', 'each', 'rule' => ['filter', 'filter' => [$this, 'filterZipcodes']]],
            ['delivery_fees', 'each', 'rule' => ['number', 'min' => 0]]
        ]);
    }

    /**
     * @param \app\modules\order\models\Order $order
     * @return OrderFinancePrediction
     */
    public function calculate($order)
    {
        $finance = $this->getOrderFinance($order);
        $zipcode = trim($order->customer_zip);
        $delivery_fee = 0;
        if (!empty($zipcode)) {
            foreach ($this->zipcodes as $key => $zipcodes) {
                if (in_array($zipcode, explode(',', $zipcodes))) {
                    $delivery_fee = doubleval($this->delivery_fees[$key] ?? 0);
                    break;
                }
            }
        }
        $finance->price_fulfilment = doubleval($this->packing_fee);
        $finance->price_delivery = $delivery_fee;
        if ($this->isOrderBuyout($order)) {
            $finance->price_cod = $order->price_total + $order->delivery;
            $finance->price_cod_service = $finance->price_cod * (doubleval($this->cod_fee) / 100);
            $finance->price_delivery_return = 0;
            $finance->price_vat = $finance->price_cod * (doubleval($this->price_vat) / 100);
        } else {
            if ($this->isOrderReturnToSender($order)) {
                $finance->price_delivery_return = $finance->price_delivery;
            } else {
                $finance->price_delivery_return = 0;
            }
            $finance->price_cod = 0;
            $finance->price_cod_service = 0;
            $finance->price_vat = 0;
        }
        return parent::afterCalculate($finance);
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @param $onlyView
     * @return string
     */
    public function render($form, $onlyView = false)
    {
        return NinjaIDWidget::widget(['form' => $form, 'chargesCalculator' => $this, 'currencies' => self::$currencies, 'onlyView' => $onlyView]);
    }

    /**
     * Приводит в порядок список почтовых индексов
     *
     * Убирает дубликаты, пробелы, сортирует
     *
     * @param string $zipcodes
     * @return string
     */
    public function filterZipcodes($zipcodes)
    {
        $codes = array_map("trim", explode(",", $zipcodes));
        $codes = array_flip(array_flip($codes)); // удаляем дубликаты
        sort($codes, SORT_NATURAL);
        return implode(",", $codes);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'zipcodes' => Yii::t('common', 'Индексы региона'),
            'delivery_fees' => Yii::t('common', 'Цена за доставку'),
        ]);
    }
}