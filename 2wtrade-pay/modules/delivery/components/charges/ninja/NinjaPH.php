<?php

namespace app\modules\delivery\components\charges\ninja;

use app\modules\delivery\widgets\charges\ninja\NinjaPHWidget;
use app\modules\order\models\OrderFinancePrediction;
use yii\helpers\ArrayHelper;

/**
 * Class NinjaPH
 * @package app\modules\delivery\components\charges\ninja
 */
class NinjaPH extends Ninja
{
    /**
     * @var int
     */
    public $delivery_fee_1kg = 0;

    /**
     * @var int
     */
    public $delivery_fee_add_kg = 0;

    public $vat_type = self::VAT_TYPE_SERVICE;

    /**
     * @return array
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['delivery_fee_1kg', 'delivery_fee_add_kg'], 'number', 'min' => 0],
        ]);
    }

    /**
     * @param \app\modules\order\models\Order $order
     * @return OrderFinancePrediction
     */
    public function calculate($order)
    {
        $finance = $this->getOrderFinance($order);
        $finance->price_fulfilment = doubleval($this->packing_fee);
        $weight = $this->getWeightOfParcel($order, true);
        $finance->price_delivery = doubleval($this->delivery_fee_1kg);
        if ($weight > 1) {
            $finance->price_delivery += ($weight - 1) * doubleval($this->delivery_fee_add_kg);
        }
        if ($this->isOrderBuyout($order)) {
            $finance->price_cod = $order->price_total + $order->delivery;
            $finance->price_cod_service = $finance->price_cod * (doubleval($this->cod_fee) / 100);
        } else {
            $finance->price_cod = 0;
            $finance->price_cod_service = 0;
        }

        $finance->price_vat = ($finance->price_delivery + $finance->price_storage + $finance->price_fulfilment) * (doubleval($this->price_vat) / 100);
        return parent::afterCalculate($finance);
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @param $onlyView
     * @return string
     */
    public function render($form, $onlyView = false)
    {
        return NinjaPHWidget::widget([
            'form' => $form,
            'chargesCalculator' => $this,
            'currencies' => self::$currencies,
            'onlyView' => $onlyView,
        ]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'delivery_fee_1kg' => \Yii::t('common', 'Плата за доставку 1кг и менее'),
            'delivery_fee_add_kg' => \Yii::t('common', 'Плата за доставку каждого доп. кг'),
        ]);
    }
}