<?php

namespace app\modules\delivery\components\charges\ninja;

use app\modules\delivery\widgets\charges\ninja\NinjaMYWidget;
use app\modules\order\models\OrderFinancePrediction;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class NinjaMY
 * @package app\modules\delivery\components\charges\ninja
 */
class NinjaMY extends Ninja
{
    /**
     * @var int
     */
    public $delivery_fee_1kg = 0;
    /**
     * @var int
     */
    public $delivery_fee_2kg = 0;
    /**
     * @var int
     */
    public $delivery_fee_3kg = 0;
    /**
     * @var int
     */
    public $delivery_fee_add_kg = 0;

    public $vat_type = self::VAT_TYPE_SERVICE;

    /**
     * @var int
     */
    public $cod_fee_min = 0;

    /**
     * @return array
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [
                ['delivery_fee_1kg', 'delivery_fee_2kg', 'delivery_fee_3kg', 'delivery_fee_add_kg', 'cod_fee_min'],
                'number',
                'min' => 0
            ],
        ]);
    }

    /**
     * @param \app\modules\order\models\Order $order
     * @return OrderFinancePrediction
     */
    public function calculate($order)
    {
        $finance = $this->getOrderFinance($order);
        $finance->price_fulfilment = doubleval($this->packing_fee);
        $finance->price_packing = 0;

        $weight = $this->getWeightOfParcel($order, true);
        $weightChar = "delivery_fee_{$weight}kg";
        if (isset($this->$weightChar)) {
            $delivery_fee = doubleval($this->$weightChar);
        } else {
            $delivery_fee = doubleval($this->delivery_fee_3kg) + ($weight - 3) * doubleval($this->delivery_fee_add_kg);
        }
        $finance->price_delivery = $delivery_fee;

        if (!$this->isOrderBuyout($order)) {
            $finance->price_cod = 0;
            $finance->price_cod_service = 0;
        } else {
            $finance->price_cod = $order->price_total + $order->delivery;
            $finance->price_cod_service = max(doubleval($this->cod_fee_min), $finance->price_cod * (doubleval($this->cod_fee) / 100));
        }

        $finance->price_vat = ($finance->price_delivery + $finance->price_fulfilment + $finance->price_storage) * (doubleval($this->price_vat) / 100);
        return parent::afterCalculate($finance);
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @param $onlyView
     * @return string
     */
    public function render($form, $onlyView = false)
    {
        return NinjaMYWidget::widget([
            'form' => $form,
            'chargesCalculator' => $this,
            'currencies' => self::$currencies,
            'onlyView' => $onlyView,
        ]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'delivery_fee_1kg' => Yii::t('common', 'Цена за 1кг'),
            'delivery_fee_2kg' => Yii::t('common', 'Цена за 2кг'),
            'delivery_fee_3kg' => Yii::t('common', 'Цена за 3кг'),
            'delivery_fee_add_kg' => Yii::t('common', 'Цена за каждый доп. кг'),
            'cod_fee_min' => Yii::t('common', 'Мин. плата за наложенный платеж'),
        ]);
    }
}