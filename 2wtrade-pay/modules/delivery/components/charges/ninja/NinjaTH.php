<?php

namespace app\modules\delivery\components\charges\ninja;

use app\models\Currency;
use app\modules\delivery\components\charges\ChargesCalculatorAbstract;
use app\modules\delivery\widgets\charges\helpers\CommonDeliveryCharges;
use app\modules\delivery\widgets\charges\helpers\DeliveryChargesForRegion;
use app\modules\order\models\Order;
use app\modules\order\models\OrderFinancePrediction;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class NinjaTH
 * @package app\modules\delivery\components\charges\ninja
 */
class NinjaTH extends ChargesCalculatorAbstract
{
    /**
     * @var int
     */
    public $cod_fee_min = 0;
    public $cod_fee = 0;
    public $vat_percent = 0;
    public $zipcodes = [];
    public $delivery_fees_1kg = [];
    public $delivery_fees_add_kg = [];
    public $fulfillment_fee = 0;

    public $vat_type = self::VAT_TYPE_SERVICE;

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            ['zipcodes', 'each', 'rule' => ['string']],
            ['zipcodes', 'each', 'rule' => ['filter', 'filter' => [$this, 'filterZipcodes']]],
            [['delivery_fees_1kg', 'delivery_fees_add_kg'], 'each', 'rule' => ['number', 'min' => 0]],
            [['cod_fee', 'vat_percent'], 'number', 'min' => 0, 'max' => 100],
            [['currency_char_code'], 'string'],
            [['cod_fee_min'], 'number', 'min' => 0]
        ]);
    }

    protected function getDeliveryFee()
    {
        return $this->delivery_fees_1kg;
    }

    /**
     * @param Order $order
     * @return OrderFinancePrediction
     */
    public function calculate($order)
    {
        $finance = $this->getOrderFinance($order);

        $delivery_fee_1kg = 0;
        $delivery_fee_add_kg = 0;
        $index = $this->getIndexByZipcode($this->zipcodes, $order->customer_zip);
        if (!is_null($index)) {
            $delivery_fee_1kg = $this->delivery_fees_1kg[$index] ?? 0;
            $delivery_fee_add_kg = $this->delivery_fees_add_kg[$index] ?? 0;
        }

        $weight = $this->getWeightOfParcel($order, true);
        $currency = $this->getCurrency();
        $currencyId = null;
        if (isset($currency)) {
            $currencyId = $currency->id;
        }
        $finance->price_delivery = (doubleval($delivery_fee_1kg) + max(0, $weight - 1) * doubleval($delivery_fee_add_kg));
        $finance->price_fulfilment = doubleval($this->fulfillment_fee);
        if ($this->isOrderBuyout($order)) {
            $finance->price_cod = $order->delivery + $order->price_total;
            $rate = $this->getCurrencyRate();
            $item1 = $finance->price_cod * (doubleval($this->cod_fee) / 100);
            $item2 = doubleval($this->cod_fee_min) * $rate;
            if ($item1 > $item2) {
                $finance->price_cod_service = $item1;
            } else {
                $finance->price_cod_service = doubleval($this->cod_fee_min);
                $finance->price_cod_service_currency_id = $currencyId;
            }
        } else {
            $finance->price_cod = 0;
            $finance->price_cod_service = 0;
        }
        $finance->price_vat = ($finance->price_delivery + $finance->price_fulfilment) * (doubleval($this->vat_percent) / 100);

        $finance->price_delivery_currency_id = $finance->price_fulfilment_currency_id = $finance->price_vat_currency_id = $currencyId;
        return parent::afterCalculate($finance);
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @param $onlyView
     * @return string
     */
    public function render($form, $onlyView = false)
    {
        return CommonDeliveryCharges::widget([
                'form' => $form,
                'model' => $this,
                'items' => [
                    [
                        'attribute' => 'fulfillment_fee',
                        'length' => 2,
                        'options' => ['disabled' => $onlyView]
                    ],
                    ['cod_fee', 'options' => ['disabled' => $onlyView]],
                    ['cod_fee_min', 'options' => ['disabled' => $onlyView]],
                    [
                        'attribute' => 'vat_percent',
                        'length' => 2,
                        'options' => ['disabled' => $onlyView]
                    ],
                    [
                        'attribute' => 'currency_char_code',
                        'type' => 'select2List',
                        'items' => ArrayHelper::map(Currency::find()->all(), 'char_code', 'name'),
                        'length' => 2,
                        'options' => [
                            'length' => false,
                            'disabled' => $onlyView,
                        ]
                    ],

                ]
            ]) . DeliveryChargesForRegion::widget([
                'form' => $form,
                'model' => $this,
                'onlyView' => $onlyView,
                'regionAttribute' => 'zipcodes',
                'chargesAttributes' => ['delivery_fees_1kg', 'delivery_fees_add_kg']
            ]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'cod_fee_min' => \Yii::t('common', 'Минимальная плата за наложенный платеж'),
            'delivery_fees_1kg' => \Yii::t('common', 'Стоимость доставки (до 1кг.)'),
            'delivery_fees_add_kg' => \Yii::t('common', 'Стоимость доставки (за кажд. доп. 1кг.)'),
        ]);
    }


    /**
     * @param $field
     * @return array
     */
    public function getCalculatingTypeForField($field)
    {
        $currency = $this->getCurrency();
        $answer = [];
        switch ($field) {
            case OrderFinancePrediction::COLUMN_PRICE_COD_SERVICE:
                $answer = [
                    'type' => static::CALCULATING_TYPE_PERCENT,
                    'visible_formula' => Yii::t('common', 'МИН({val1}, {val2}', [
                        'val1' => "{$this->cod_fee}%",
                        'val2' => "{$this->cod_fee_min} {$currency->char_code}"
                    ]),
                    'fields' => [
                        OrderFinancePrediction::COLUMN_PRICE_COD,
                    ],
                ];
                break;
            case OrderFinancePrediction::COLUMN_PRICE_VAT:
                $answer['type'] = static::CALCULATING_TYPE_PERCENT;
                $answer['visible_formula'] = "$this->vat_percent%";
                if ($this->vat_type == static::VAT_TYPE_COD) {
                    $answer['fields'] = [
                        OrderFinancePrediction::COLUMN_PRICE_COD,
                    ];
                } else {
                    $answer['fields'] = [
                        OrderFinancePrediction::COLUMN_PRICE_DELIVERY,
                        OrderFinancePrediction::COLUMN_PRICE_DELIVERY_RETURN,
                        OrderFinancePrediction::COLUMN_PRICE_STORAGE,
                        OrderFinancePrediction::COLUMN_PRICE_FULFILMENT,
                        OrderFinancePrediction::COLUMN_PRICE_PACKAGE,
                        OrderFinancePrediction::COLUMN_PRICE_PACKING,
                        OrderFinancePrediction::COLUMN_PRICE_DELIVERY_BACK,
                        OrderFinancePrediction::COLUMN_PRICE_REDELIVERY,
                        OrderFinancePrediction::COLUMN_PRICE_ADDRESS_CORRECTION,
                    ];
                }
                break;
            default:
                $answer['type'] = static::CALCULATING_TYPE_NUMBER;
                break;
        }

        return $answer;
    }
}