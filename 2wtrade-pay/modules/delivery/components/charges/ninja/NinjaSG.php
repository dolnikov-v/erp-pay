<?php

namespace app\modules\delivery\components\charges\ninja;

use app\modules\delivery\widgets\charges\ninja\NinjaSGWidget;
use app\modules\order\models\OrderFinancePrediction;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class NinjaSG
 * @package app\modules\delivery\components\charges\ninja
 */
class NinjaSG extends Ninja
{
    /**
     * @var int
     */
    public $storage_fee_per_m3 = 0;
    /**
     * @var int
     */
    public $packing_fee_size_a = 0;
    /**
     * @var int
     */
    public $packing_fee_size_b = 0;
    /**
     * @var int
     */
    public $packing_fee_size_c = 0;

    /**
     * @var int
     */
    public $delivery_fee_size_a = 0;
    /**
     * @var int
     */
    public $delivery_fee_size_b = 0;
    /**
     * @var int
     */
    public $delivery_fee_size_c = 0;
    /**
     * @var int
     */
    public $delivery_fee_size_d = 0;

    public $vat_type = self::VAT_TYPE_SERVICE;

    /**
     * @return array
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [
                [
                    'storage_fee_per_m3',
                    'packing_fee_size_a',
                    'packing_fee_size_b',
                    'packing_fee_size_c',
                    'delivery_fee_size_a',
                    'delivery_fee_size_b',
                    'delivery_fee_size_c',
                    'delivery_fee_size_d'
                ],
                'number',
                'min' => 0
            ],
        ]);
    }

    /**
     * @param \app\modules\order\models\Order $order
     * @return OrderFinancePrediction
     */
    public function calculate($order)
    {
        $finance = $this->getOrderFinance($order);
        $weight = $this->getWeightOfParcel($order, true);
        $delivery_fee = $this->delivery_fee_size_d;
        $packing_fee = $this->packing_fee_size_c;

        // Точных размеров упаковки не знаем, поэтому рассчитываем стоимость доставки только в зависимости от веса
        if ($weight <= 4) {
            $delivery_fee = $this->delivery_fee_size_a;
            $packing_fee = $this->packing_fee_size_a;
        } elseif ($weight <= 10) {
            $delivery_fee = $this->delivery_fee_size_b;
            $packing_fee = $this->packing_fee_size_b;
        } elseif ($weight <= 20) {
            $delivery_fee = $this->delivery_fee_size_c;
        }

        $finance->price_fulfilment = doubleval($packing_fee);
        $finance->price_packing = 0;
        $finance->price_delivery = doubleval($delivery_fee);

        $volume = $this->getVolumeOfParcel($order);
        $finance->price_storage = doubleval($this->storage_fee_per_m3) * $volume;

        if ($this->isOrderBuyout($order)) {
            $finance->price_cod = $order->delivery + $order->price_total;
        } else {
            $finance->price_cod = 0;
        }

        $finance->price_cod_service = (doubleval($this->cod_fee) / 100) * $finance->price_cod;
        $finance->price_vat = ($finance->price_fulfilment + $finance->price_delivery + $finance->price_storage) * (doubleval($this->price_vat) / 100);

        return parent::afterCalculate($finance);
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @param $onlyView
     * @return string
     */
    public function render($form, $onlyView = false)
    {
        return NinjaSGWidget::widget([
            'form' => $form,
            'chargesCalculator' => $this,
            'currencies' => self::$currencies,
            'onlyView' => $onlyView,
        ]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'storage_fee_per_m3' => Yii::t('common', 'Плата за хранение 1м3'),
            'packing_fee_size_a' => Yii::t('common', 'Плата за упаковку товаров весом 0-4кг или размерами 0-80см'),
            'packing_fee_size_b' => Yii::t('common', 'Плата за упаковку товаров весом 5-10кг или размерами 81-120см'),
            'packing_fee_size_c' => Yii::t('common', 'Плата за упаковку товаров весом 11-30кг или размерами 121-300см'),
            'delivery_fee_size_a' => Yii::t('common', 'Плата за доставку заказа весом 0-4кг или размерами 0-80см'),
            'delivery_fee_size_b' => Yii::t('common', 'Плата за доставку заказа весом 5-10кг или размерами 81-120см'),
            'delivery_fee_size_c' => Yii::t('common', 'Плата за доставку заказа весом 11-20кг или размерами 121-200см'),
            'delivery_fee_size_d' => Yii::t('common', 'Плата за доставку заказа весом 20-30кг или размерами 201-300см'),
        ]);
    }
}