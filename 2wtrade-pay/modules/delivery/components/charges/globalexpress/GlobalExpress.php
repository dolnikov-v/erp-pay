<?php

namespace app\modules\delivery\components\charges\globalexpress;


use app\modules\delivery\components\charges\ChargesCalculatorAbstract;
use app\modules\delivery\widgets\charges\globalexpress\GlobalExpressWidget;
use app\modules\order\models\OrderFinancePrediction;
use Yii;

/**
 * Class GlobalExpress
 * @package app\modules\delivery\components\charges\globalexpress
 */
class GlobalExpress extends ChargesCalculatorAbstract
{
    /**
     * @var array
     */
    public $delivery_fees = [];
    /**
     * @var array
     */
    public $zipcodes = [];
    /**
     * @var int
     */
    public $packing_fee = 0;
    /**
     * @var int
     */
    public $package_fee = 0;
    /**
     * @var int
     */
    public $return_fee = 0;
    /**
     * @var int
     */
    public $delivery_to_storage_fee = 0;

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['packing_fee', 'package_fee', 'return_fee', 'delivery_to_storage_fee'], 'number', 'min' => 0],
            ['zipcodes', 'each', 'rule' => ['string']],
            ['zipcodes', 'each', 'rule' => ['filter', 'filter' => [$this, 'filterZipcodes']]],
            [['delivery_fees'], 'each', 'rule' => ['number', 'min' => 0]],
            [['currency_char_code'], 'string'],
        ]);
    }

    /**
     * @param \app\modules\order\models\Order $order
     * @return OrderFinancePrediction
     */
    public function calculate($order)
    {
        $finance = $this->getOrderFinance($order);

        $delivery_fee = 0;
        $index = $this->getIndexByZipcode($this->zipcodes, $order->customer_zip);
        if (!is_null($index)) {
            $delivery_fee = doubleval($this->delivery_fees[$index] ?? 0);
        }

        $finance->price_packing = doubleval($this->packing_fee) + doubleval($this->delivery_to_storage_fee);
        $finance->price_package = doubleval($this->package_fee);
        $finance->price_delivery = $delivery_fee;

        if($this->isOrderBuyout($order)) {
            $finance->price_cod = $order->price_total + $order->delivery;
            $finance->price_delivery_return = 0;
        } else {
            if($this->isOrderReturnToSender($order)) {
                $finance->price_delivery_return = doubleval($this->return_fee);
            } else {
                $finance->price_delivery_return = 0;
            }
            $finance->price_cod = 0;
        }

        return parent::afterCalculate($finance);
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @param $onlyView
     * @return string
     */
    public function render($form, $onlyView = false)
    {
        return GlobalExpressWidget::widget(['form' => $form, 'chargesCalculator' => $this, 'currencies' => self::$currencies, 'onlyView' => $onlyView]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'zipcodes' => Yii::t('common', 'Индексы региона'),
            'delivery_fees' => Yii::t('common', 'Цена за доставку'),
            'packing_fee' => Yii::t('common', 'Стоимость принятия и обработки заказа'),
            'package_fee' => Yii::t('common', 'Стоимость упаковки'),
            'delivery_to_storage_fee' => Yii::t('common', 'Стоимость доставки на склад'),
            'return_fee' => Yii::t('common', 'Стоимость возврата заказа'),
        ];
    }
}