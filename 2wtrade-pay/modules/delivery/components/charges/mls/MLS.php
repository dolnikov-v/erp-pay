<?php

namespace app\modules\delivery\components\charges\mls;


use app\modules\delivery\components\charges\ChargesCalculatorAbstract;
use app\modules\delivery\widgets\charges\helpers\CommonDeliveryCharges;
use app\modules\delivery\widgets\charges\helpers\DeliveryChargesForRegion;

/**
 * Class MLS
 * @package app\modules\delivery\components\charges\mls
 */
class MLS extends ChargesCalculatorAbstract
{
    public $zipcodes = [];

    public $delivery_fee_upto_9kg = [];
    public $delivery_fee_add_kg = [];

    public $fulfilment_fee = 0;

    // @todo ХЗ, как рассчитывать количество паллетов за месяц
    public $monthly_storage_per_pallet = 0;
    public $in_out_per_pallet = 0;

    public $transfer_rate = 0;

    public $vat_percent = 0;

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['fulfilment_fee', 'monthly_storage_per_pallet', 'in_out_per_pallet'], 'number', 'min' => 0],
            [['transfer_rate', 'vat_percent'], 'number', 'min' => 0, 'max' => 100],
            ['zipcodes', 'each', 'rule' => ['filter', 'filter' => [$this, 'filterZipcodes']]],
            [['delivery_fee_upto_9kg', 'delivery_fee_add_kg'], 'each', 'rule' => ['number', 'min' => 0]],
        ]);
    }

    protected function getDeliveryFee()
    {
        return $this->delivery_fee_upto_9kg;
    }

    /**
     * @param \app\modules\order\models\Order $order
     * @return \app\modules\order\models\OrderFinancePrediction
     */
    public function calculate($order)
    {
        $finance = $this->getOrderFinance($order);

        $finance->price_fulfilment = doubleval($this->fulfilment_fee);
        $deliveryFeeUpTo9kg = 0;
        $deliveryFeeAddKg = 0;
        $index = $this->getIndexByZipcode($this->zipcodes, $order->customer_zip);
        if (is_null($index)) {
            $deliveryFeeUpTo9kg = $this->delivery_fee_upto_9kg[$index] ?? 0;
            $deliveryFeeAddKg = $this->delivery_fee_add_kg[$index] ?? 0;
        }
        $weight = $this->getWeightOfParcel($order, true);
        $finance->price_delivery = doubleval($deliveryFeeUpTo9kg) + max($weight - 9, 0) * doubleval($deliveryFeeAddKg);
        $finance->price_vat = ($finance->price_delivery + $finance->price_storage + $finance->price_fulfilment) * (doubleval($this->vat_percent) / 100);
        $currency = $this->getCurrency();
        $currencyId = null;
        if (isset($currency)) {
            $currencyId = $currency->id;
        }
        $finance->price_fulfilment_currency_id = $finance->price_delivery_currency_id = $finance->price_storage_currency_id = $finance->price_vat_currency_id = $currencyId;
        return parent::afterCalculate($finance);
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @param bool $onlyView
     * @return string
     */
    public function render($form, $onlyView = false)
    {
        return CommonDeliveryCharges::widget([
                'form' => $form,
                'model' => $this,
                'items' => [
                    [
                        'attribute' => 'fulfilment_fee',
                        'options' => [
                            'disabled' => $onlyView,
                        ]
                    ],
                    [
                        'attribute' => 'monthly_storage_per_pallet',
                        'options' => [
                            'disabled' => $onlyView,
                        ]
                    ],
                    [
                        'attribute' => 'in_out_per_pallet',
                        'options' => [
                            'disabled' => $onlyView,
                        ]
                    ],
                    [
                        'attribute' => 'transfer_rate',
                        'options' => [
                            'disabled' => $onlyView,
                        ]
                    ],
                    [
                        'attribute' => 'vat_percent',
                        'options' => [
                            'disabled' => $onlyView,
                        ]
                    ],
                ],
            ]) . DeliveryChargesForRegion::widget([
                'onlyView' => $onlyView,
                'form' => $form,
                'model' => $this,
                'regionAttribute' => 'zipcodes',
                'chargesAttributes' => ['delivery_fee_upto_9kg', 'delivery_fee_add_kg'],
            ]);
    }

    /**
     * @return array
     */
    protected function customAttributeLabels()
    {
        return [
            'delivery_fee_upto_9kg' => \Yii::t('common', 'Стоимость доставки (до 9кг.)'),
            'delivery_fee_add_kg' => \Yii::t('common', 'Стоимость доставки (доп. кг.)'),
            'monthly_storage_per_pallet' => \Yii::t('common', 'Стоимость хранения (за паллет)'),
            'in_out_per_pallet' => \Yii::t('common', 'Стоимость обслуживания паллета'),
            'transfer_rate' => \Yii::t('common', '% за перевод средств'),
            'fulfilment_fee' => \Yii::t('common', 'Плата за обслуживание заказа')
        ];
    }

    /**
     * @param $debtSum
     * @return int
     */
    public function calculateAdditionalChargesForCommonDebt($debtSum)
    {
        return $this->transfer_rate ? doubleval($this->transfer_rate) : 0;
    }
}