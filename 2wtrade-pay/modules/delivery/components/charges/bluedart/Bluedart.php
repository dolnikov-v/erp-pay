<?php

namespace app\modules\delivery\components\charges\bluedart;


use app\modules\delivery\components\charges\ChargesCalculatorAbstract;
use app\modules\delivery\widgets\charges\bluedart\BluedartWidget;
use app\modules\order\models\OrderFinancePrediction;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class Bluedart
 * @package app\modules\delivery\components\charges\bluedart
 */
class Bluedart extends ChargesCalculatorAbstract
{
    public $zipcodes = [];
    public $delivery_fees = [];
    public $delivery_fees_add_kg = [];
    public $cod_fee = 0;
    public $return_fee = 0;
    public $vat_percent = 0;

    /**
     * Для всех стран PFC выставляет плату за доставку в своей валюте
     * @var string
     */
    public $currency_char_code = 'EUR';

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['cod_fee', 'vat_percent'], 'number', 'min' => 0, 'max' => 100],
            [['return_fee'], 'number', 'min' => 0],
            ['zipcodes', 'each', 'rule' => ['string']],
            ['zipcodes', 'each', 'rule' => ['filter', 'filter' => [$this, 'filterZipcodes']]],
            ['delivery_fees', 'each', 'rule' => ['number', 'min' => 0]],
            ['delivery_fees_add_kg', 'each', 'rule' => ['number', 'min' => 0]],
            [['currency_char_code'], 'string'],
        ]);
    }

    /**
     * @param \app\modules\order\models\Order $order
     * @return OrderFinancePrediction
     */
    public function calculate($order)
    {
        $finance = $this->getOrderFinance($order);
        $delivery_fee = 0;
        $delivery_fee_add_kg = 0;

        if ($index = $this->getIndexByZipcode($this->zipcodes, $order->customer_zip)) {
            $delivery_fee = doubleval($this->delivery_fees[$index] ?? 0);
            $delivery_fee_add_kg = doubleval($this->delivery_fees_add_kg[$index] ?? 0);
        }

        $weight = ceil($this->getWeightOfParcel($order) / 500);
        $currency = $this->getCurrency();
        $currencyId = null;
        if (isset($currency)) {
            $currencyId = $currency->id;
        }
        $finance->price_delivery = $delivery_fee;
        if ($weight > 1) {
            $finance->price_delivery += ($weight - 1) * $delivery_fee_add_kg;
        }

        if ($this->isOrderBuyout($order)) {
            $finance->price_cod = $order->delivery + $order->price_total;
            $finance->price_cod_service = $finance->price_cod * (doubleval($this->cod_fee) / 100);
            $finance->price_delivery_return = 0;
            $finance->price_vat = $finance->price_cod * (doubleval($this->vat_percent) / 100);
        } else {
            if ($this->isOrderReturnToSender($order)) {
                $finance->price_delivery_return = doubleval($this->return_fee);
            }
            $finance->price_cod = 0;
            $finance->price_cod_service = 0;
            $finance->price_vat = 0;
        }

        $finance->price_delivery_currency_id = $finance->price_delivery_return = $currencyId;

        return parent::afterCalculate($finance);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'zipcodes' => Yii::t('common', 'Индексы региона'),
            'delivery_fees' => Yii::t('common', 'Стоимость доставки заказа весом менее 500г'),
            'delivery_fees_add_kg' => Yii::t('common', 'Доплата за каждый последующие 500г'),
            'return_fee' => Yii::t('common', 'Доплата за возврат товара'),
            'currency_char_code' => Yii::t('common', 'Валюта'),
        ]);
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @var $onlyView
     * @return string
     */
    public function render($form, $onlyView = false)
    {
        return BluedartWidget::widget([
            'chargesCalculator' => $this,
            'form' => $form,
            'onlyView' => $onlyView,
            'currencies' => self::$currencies
        ]);
    }

    /**
     * @param $field
     * @return array
     */
    public function getCalculatingTypeForField($field)
    {
        $answer = [];
        switch ($field) {
            case OrderFinancePrediction::COLUMN_PRICE_COD_SERVICE:
                $answer = [
                    'type' => static::CALCULATING_TYPE_PERCENT,
                    'visible_formula' => "$this->cod_fee%",
                    'fields' => [
                        OrderFinancePrediction::COLUMN_PRICE_COD,
                    ],
                ];
                break;
            case OrderFinancePrediction::COLUMN_PRICE_VAT:
                $answer['type'] = static::CALCULATING_TYPE_PERCENT;
                $answer['visible_formula'] = "$this->vat_percent%";
                $answer['fields'] = [
                    OrderFinancePrediction::COLUMN_PRICE_COD,
                ];
                break;
            default:
                $answer['type'] = static::CALCULATING_TYPE_NUMBER;
                break;
        }

        return $answer;
    }
}