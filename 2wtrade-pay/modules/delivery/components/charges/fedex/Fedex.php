<?php

namespace app\modules\delivery\components\charges\fedex;


use app\models\Currency;
use app\modules\delivery\components\charges\ChargesCalculatorAbstract;
use app\modules\delivery\widgets\charges\helpers\CommonDeliveryCharges;
use app\modules\delivery\widgets\charges\helpers\DeliveryChargesForRegion;
use app\modules\order\models\OrderFinancePrediction;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class Fedex
 * @package app\modules\delivery\components\charges\fedex
 */
class Fedex extends ChargesCalculatorAbstract
{
    public $zipcodes = [];
    public $cod_fee = 0;
    public $delivery_fees_1kg = [];
    public $delivery_fees_2kg = [];
    public $delivery_fees_3kg = [];
    public $delivery_fees_4kg = [];
    public $delivery_fees_5kg = [];
    public $delivery_fees_6kg = [];
    public $delivery_fees_7kg = [];
    public $delivery_fees_8kg = [];
    public $delivery_fees_9kg = [];
    public $delivery_fees_10kg = [];
    public $delivery_fees_11kg = [];
    public $delivery_fees_12kg = [];
    public $delivery_fees_13kg = [];
    public $delivery_fees_14kg = [];
    public $delivery_fees_15kg = [];
    public $delivery_fees_16kg = [];
    public $delivery_fees_17kg = [];
    public $delivery_fees_18kg = [];
    public $delivery_fees_19kg = [];
    public $delivery_fees_20kg = [];
    public $delivery_fees_over_20kg = [];

    /**
     * @var string
     */
    public $currency_char_code = 'USD';

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            ['zipcodes', 'each', 'rule' => ['string']],
            ['zipcodes', 'each', 'rule' => ['filter', 'filter' => [$this, 'filterZipcodes']]],
            [
                [
                    'delivery_fees_1kg',
                    'delivery_fees_2kg',
                    'delivery_fees_3kg',
                    'delivery_fees_4kg',
                    'delivery_fees_5kg',
                    'delivery_fees_6kg',
                    'delivery_fees_7kg',
                    'delivery_fees_8kg',
                    'delivery_fees_9kg',
                    'delivery_fees_10kg',
                    'delivery_fees_11kg',
                    'delivery_fees_12kg',
                    'delivery_fees_13kg',
                    'delivery_fees_14kg',
                    'delivery_fees_15kg',
                    'delivery_fees_16kg',
                    'delivery_fees_17kg',
                    'delivery_fees_18kg',
                    'delivery_fees_19kg',
                    'delivery_fees_20kg',
                    'delivery_fees_over_20kg',
                ],
                'each',
                'rule' => ['number', 'min' => 0]
            ],
            [['currency_char_code'], 'string'],
        ]);
    }

    /**
     * @param \app\modules\order\models\Order $order
     * @return OrderFinancePrediction
     */
    public function calculate($order)
    {
        $finance = $this->getOrderFinance($order);
        $weight = $this->getWeightOfParcel($order, true);

        $index = $this->getIndexByZipcode($this->zipcodes, $order->customer_zip);
        if (!is_null($index)) {
            if ($weight > 20) {
                $finance->price_delivery = (doubleval($this->delivery_fees_20kg[$index] ?? 0)) + ((doubleval($this->delivery_fees_over_20kg[$index] ?? 0)) * ($weight - 20));
            } else {
                $finance->price_delivery = doubleval($this->{"delivery_fees_{$weight}kg"}[$index] ?? 0);
            }
        } else {
            if ($weight > 20) {
                $finance->price_delivery = (!empty($this->delivery_fees_20kg) ? doubleval(max($this->delivery_fees_20kg)) : 0) + ((!empty($this->delivery_fees_over_20kg) ? doubleval(max($this->delivery_fees_over_20kg)) : 0) * ($weight - 20));
            } else {
                $finance->price_delivery = !empty($this->{"delivery_fees_{$weight}kg"}) ? doubleval(max($this->{"delivery_fees_{$weight}kg"})) : 0;
            }
        }
        $currency = $this->getCurrency();
        $currencyId = null;
        if (isset($currency)) {
            $currencyId = $currency->id;
        }
        if ($this->isOrderBuyout($order)) {
            $finance->price_cod = $order->delivery + $order->price_total;
            $finance->price_cod_service = doubleval($this->cod_fee);
        } else {
            $finance->price_cod = 0;
            $finance->price_cod_service = 0;
        }

        $finance->price_delivery_currency_id = $finance->price_cod_service_currency_id = $currencyId;

        return parent::afterCalculate($finance);
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @param $onlyView
     * @return string
     */
    public function render($form, $onlyView = false)
    {
        return CommonDeliveryCharges::widget([
                'form' => $form,
                'model' => $this,
                'items' => [
                    [
                        'attribute' => 'cod_fee',
                        'options' => [
                            'disabled' => $onlyView,
                        ]
                    ],
                    [
                        'attribute' => 'currency_char_code',
                        'type' => 'select2List',
                        'items' => ArrayHelper::map(Currency::find()->all(), 'char_code', 'name'),
                        'options' => [
                            'length' => false,
                            'disabled' => $onlyView
                        ]
                    ],
                ],
            ]) . DeliveryChargesForRegion::widget([
                'model' => $this,
                'onlyView' => $onlyView,
                'regionAttribute' => 'zipcodes',
                'chargesAttributes' => [
                    'delivery_fees_1kg',
                    'delivery_fees_2kg',
                    'delivery_fees_3kg',
                    'delivery_fees_4kg',
                    'delivery_fees_5kg',
                    'delivery_fees_6kg',
                    'delivery_fees_7kg',
                    'delivery_fees_8kg',
                    'delivery_fees_9kg',
                    'delivery_fees_10kg',
                    'delivery_fees_11kg',
                    'delivery_fees_12kg',
                    'delivery_fees_13kg',
                    'delivery_fees_14kg',
                    'delivery_fees_15kg',
                    'delivery_fees_16kg',
                    'delivery_fees_17kg',
                    'delivery_fees_18kg',
                    'delivery_fees_19kg',
                    'delivery_fees_20kg',
                    'delivery_fees_over_20kg',
                ],
                'form' => $form
            ]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'zipcodes' => Yii::t('common', 'Индексы региона'),
            'delivery_fees_1kg' => Yii::t('common', 'Стоимость доставки (1 кг.)'),
            'delivery_fees_2kg' => Yii::t('common', 'Стоимость доставки (2 кг.)'),
            'delivery_fees_3kg' => Yii::t('common', 'Стоимость доставки (3 кг.)'),
            'delivery_fees_4kg' => Yii::t('common', 'Стоимость доставки (4 кг.)'),
            'delivery_fees_5kg' => Yii::t('common', 'Стоимость доставки (5 кг.)'),
            'delivery_fees_6kg' => Yii::t('common', 'Стоимость доставки (6 кг.)'),
            'delivery_fees_7kg' => Yii::t('common', 'Стоимость доставки (7 кг.)'),
            'delivery_fees_8kg' => Yii::t('common', 'Стоимость доставки (8 кг.)'),
            'delivery_fees_9kg' => Yii::t('common', 'Стоимость доставки (9 кг.)'),
            'delivery_fees_10kg' => Yii::t('common', 'Стоимость доставки (10 кг.)'),
            'delivery_fees_11kg' => Yii::t('common', 'Стоимость доставки (11 кг.)'),
            'delivery_fees_12kg' => Yii::t('common', 'Стоимость доставки (12 кг.)'),
            'delivery_fees_13kg' => Yii::t('common', 'Стоимость доставки (13 кг.)'),
            'delivery_fees_14kg' => Yii::t('common', 'Стоимость доставки (14 кг.)'),
            'delivery_fees_15kg' => Yii::t('common', 'Стоимость доставки (15 кг.)'),
            'delivery_fees_16kg' => Yii::t('common', 'Стоимость доставки (16 кг.)'),
            'delivery_fees_17kg' => Yii::t('common', 'Стоимость доставки (17 кг.)'),
            'delivery_fees_18kg' => Yii::t('common', 'Стоимость доставки (18 кг.)'),
            'delivery_fees_19kg' => Yii::t('common', 'Стоимость доставки (19 кг.)'),
            'delivery_fees_20kg' => Yii::t('common', 'Стоимость доставки (20 кг.)'),
            'delivery_fees_over_20kg' => Yii::t('common', 'Стоимость доставки (за каждый доп. кг.)'),
        ]);
    }


    /**
     * @param $field
     * @return array
     */
    public function getCalculatingTypeForField($field)
    {
        $answer = [];
        switch ($field) {
            default:
                $answer['type'] = static::CALCULATING_TYPE_NUMBER;
                break;
        }

        return $answer;
    }
}