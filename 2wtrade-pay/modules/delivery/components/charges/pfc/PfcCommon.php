<?php

namespace app\modules\delivery\components\charges\pfc;

use app\models\Currency;
use app\models\CurrencyRate;
use app\modules\delivery\components\charges\ChargesCalculatorAbstract;
use app\modules\delivery\widgets\charges\pfc\PfcCommonWidget;
use app\modules\order\models\OrderFinancePrediction;
use Yii;
use yii\base\InvalidParamException;
use yii\helpers\ArrayHelper;

/**
 * Class PfcCommon
 * @package app\modules\delivery\components\charges\pfc
 */
class PfcCommon extends ChargesCalculatorAbstract
{
    /**
     * @var int
     */
    public $warehouse_fee = 0;
    /**
     * @var int
     */
    public $shipment_fee = 0;

    /**
     * Для всех стран PFC выставляет плату за доставку в своей валюте
     * @var string
     */
    public $currency_char_code = 'CNY';

    /**
     * @var int
     */
    public $return_fee = 0;

    /**
     * Плата за сбор денег в %
     * @var int
     */
    public $cod_fee = 0;

    /**
     * Такса в %
     * @var int
     */
    public $tax_fee = 0;

    /**
     * @var int
     */
    public $packing_fee = 0;

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['warehouse_fee', 'shipment_fee', 'return_fee', 'packing_fee'], 'number', 'min' => 0],
            [['currency_char_code', 'vat_type'], 'string'],
            [['cod_fee', 'tax_fee'], 'number', 'min' => 0, 'max' => 100],
        ]);
    }

    /**
     * @param \app\modules\order\models\Order $order
     * @return OrderFinancePrediction
     */
    public function calculate($order)
    {
        $finance = $this->getOrderFinance($order);

        $volume = $this->getVolumeOfParcel($order);
        $currency = $this->getCurrency();
        $currencyId = null;
        if (isset($currency)) {
            $currencyId = $currency->id;
        }
        $finance->price_storage = $volume * doubleval($this->warehouse_fee);
        $finance->price_delivery = doubleval($this->shipment_fee);
        $finance->price_packing = doubleval($this->packing_fee);
        if ($this->isOrderReturnToSender($order)) {
            $finance->price_delivery_return = doubleval($this->return_fee);
            $finance->price_cod = 0;
            $finance->price_cod_service = 0;
            if ($this->vat_type == static::VAT_TYPE_COD) {
                $finance->price_vat = 0;
            }
        } elseif ($this->isOrderBuyout($order)) {
            $finance->price_cod = $order->price_total + $order->delivery;
            $finance->price_cod_service = ($order->price_total + $order->delivery) * (doubleval($this->cod_fee) / 100);
            $finance->price_delivery_return = 0;
            if ($this->vat_type == static::VAT_TYPE_COD) {
                $finance->price_vat = $finance->price_cod * (doubleval($this->tax_fee) / 100);
            }
        } else {
            $finance->price_cod = 0;
            $finance->price_cod_service = 0;
            if ($this->vat_type == static::VAT_TYPE_COD) {
                $finance->price_vat = 0;
            }
            $finance->price_delivery_return = 0;
        }
        $rate = $this->getCurrencyRate();
        if ($this->vat_type == static::VAT_TYPE_SERVICE) {
            $finance->price_vat = ($finance->price_delivery + $finance->price_delivery_return + $finance->price_storage + $finance->price_packing + $finance->price_cod_service / $rate) * (doubleval($this->tax_fee) / 100);
            $finance->price_vat_currency_id = $currencyId;
        }

        $finance->price_delivery_currency_id = $finance->price_storage_currency_id = $finance->price_packing_currency_id = $finance->price_delivery_return_currency_id = $currencyId;

        return parent::afterCalculate($finance);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'warehouse_fee' => Yii::t('common', 'Плата за хранение одного куб. м.'),
            'shipment_fee' => Yii::t('common', 'Плата за доставку одного заказа'),
            'currency_char_code' => Yii::t('common', 'Валюта'),
            'return_fee' => Yii::t('common', 'Плата за возврат'),
            'cod_fee' => Yii::t('common', 'Плата за наложенный платеж (%)'),
            'tax_fee' => Yii::t('common', 'НДС (%)'),
            'packing_fee' => Yii::t('common', 'Плата за упаковку товара'),
        ]);
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @param $onlyView
     * @return string
     */
    public function render($form, $onlyView = false)
    {
        return PfcCommonWidget::widget([
            'form' => $form,
            'values' => $this->deliveryContract->chargesValues,
            'chargesCalculator' => $this,
            'currencies' => self::$currencies,
            'onlyView' => $onlyView,
        ]);
    }

    /**
     * @param $field
     * @return array
     */
    public function getCalculatingTypeForField($field)
    {
        $answer = [];
        switch ($field) {
            case OrderFinancePrediction::COLUMN_PRICE_COD_SERVICE:
                $answer = [
                    'type' => static::CALCULATING_TYPE_PERCENT,
                    'visible_formula' => "$this->cod_fee%",
                    'fields' => [
                        OrderFinancePrediction::COLUMN_PRICE_COD,
                    ],
                ];
                break;
            case OrderFinancePrediction::COLUMN_PRICE_VAT:
                $answer['type'] = static::CALCULATING_TYPE_PERCENT;
                $answer['visible_formula'] = "$this->tax_fee%";
                if ($this->vat_type == static::VAT_TYPE_COD) {
                    $answer['fields'] = [
                        OrderFinancePrediction::COLUMN_PRICE_COD,
                    ];
                } else {
                    $answer['fields'] = [
                        OrderFinancePrediction::COLUMN_PRICE_DELIVERY,
                        OrderFinancePrediction::COLUMN_PRICE_DELIVERY_RETURN,
                        OrderFinancePrediction::COLUMN_PRICE_STORAGE,
                        OrderFinancePrediction::COLUMN_PRICE_FULFILMENT,
                        OrderFinancePrediction::COLUMN_PRICE_PACKAGE,
                        OrderFinancePrediction::COLUMN_PRICE_PACKING,
                        OrderFinancePrediction::COLUMN_PRICE_DELIVERY_BACK,
                        OrderFinancePrediction::COLUMN_PRICE_REDELIVERY,
                        OrderFinancePrediction::COLUMN_PRICE_ADDRESS_CORRECTION,
                        OrderFinancePrediction::COLUMN_PRICE_COD_SERVICE,
                    ];
                }
                break;
            default:
                $answer['type'] = static::CALCULATING_TYPE_NUMBER;
                break;
        }

        return $answer;
    }
}