<?php

namespace app\modules\delivery\components\charges\pfc;

use app\modules\order\models\Order;
use app\modules\order\models\OrderFinancePrediction;
use yii\helpers\ArrayHelper;

/**
 * Class PfcCN
 * @package app\modules\delivery\components\charges\pfc
 */
class PfcCN extends PfcCommon
{
    /**
     * @param \yii\db\ActiveQuery $query
     */
    public function calculateForBatch($query)
    {
        foreach ($query->batch(1000) as $orders) {
            /**
             * @var Order[] $orders
             */
            foreach ($orders as $order) {
                $finance = $this->calculate($order);
                $finance->price_storage = 0;
                if ($this->vat_type == static::VAT_TYPE_SERVICE) {
                    $rate = $this->getCurrencyRate();
                    $finance->price_vat = ($finance->price_delivery + $finance->price_delivery_return + $finance->price_storage + $finance->price_packing + $finance->price_cod_service / $rate) * (doubleval($this->tax_fee) / 100);
                }
                $finance->save();
            }
            unset($orders);
        }
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'warehouse_fee' => \Yii::t('common', 'Стоимость аренды склада (в месяц)'),
        ]);
    }

    /**
     * @param $field
     * @return array
     */
    public function getCalculatingTypeForField($field)
    {
        $answer = [];

        switch ($field) {
            case OrderFinancePrediction::COLUMN_PRICE_STORAGE: {
                $answer = [
                    'type' => static::CALCULATING_TYPE_MONTHLY_CHARGE,
                    'charge' => doubleval($this->warehouse_fee),
                    'currency_id' => $this->getCurrency()->id,
                    'currency_rate' => $this->getCurrency()->currencyRate->rate,
                ];
                break;
            }
            default:
                return parent::getCalculatingTypeForField($field);
        }
        return $answer;
    }
}