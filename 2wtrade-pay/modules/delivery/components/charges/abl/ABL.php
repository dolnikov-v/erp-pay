<?php

namespace app\modules\delivery\components\charges\abl;

use app\modules\delivery\components\charges\ChargesCalculatorAbstract;
use app\modules\delivery\widgets\charges\abl\ABLWidget;
use app\modules\order\models\OrderFinancePrediction;
use Yii;

/**
 * Class ABL
 * @package app\modules\delivery\components\charges\abl
 */
class ABL extends ChargesCalculatorAbstract
{
    public $cod_fee = 0;
    public $fulfilment_fee = 0;
    public $delivery_fee = 0;
    public $default_weight = 0;
    public $delivery_fee_for_add_weight = 0;
    public $return_fee = 0;
    public $vat_percent = 0;

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['cod_fee', 'vat_percent'], 'number', 'min' => 0, 'max' => 100],
            [
                ['fulfilment_fee', 'delivery_fee', 'default_weight', 'delivery_fee_for_add_weight', 'return_fee'],
                'number',
                'min' => 0
            ],
            [['currency_char_code'], 'string'],
        ]);
    }

    /**
     * @param \app\modules\order\models\Order $order
     * @return OrderFinancePrediction
     */
    public function calculate($order)
    {
        $finance = $this->getOrderFinance($order);
        $weight = $this->getWeightOfParcel($order, true);
        $finance->price_delivery = doubleval($this->delivery_fee);
        if ($weight > doubleval($this->default_weight)) {
            $finance->price_delivery += ($weight - doubleval($this->default_weight)) * doubleval($this->delivery_fee_for_add_weight);
        }
        $finance->price_fulfilment = doubleval($this->fulfilment_fee);
        if ($this->isOrderBuyout($order)) {
            $finance->price_cod = $order->price_total + $order->delivery;
            $finance->price_cod_service = $finance->price_cod * (doubleval($this->cod_fee) / 100);
            $finance->price_delivery_return = 0;
        } else {
            $finance->price_cod = 0;
            $finance->price_cod_service = 0;
            if ($this->isOrderReturnToSender($order)) {
                $finance->price_delivery_return = doubleval($this->return_fee);
            }
        }
        $finance->price_vat = ($finance->price_delivery + $finance->price_fulfilment + $finance->price_cod_service) * (doubleval($this->vat_percent) / 100);
        return parent::afterCalculate($finance);
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @var $onlyView
     * @return string
     */
    public function render($form, $onlyView = false)
    {
        return ABLWidget::widget([
            'form' => $form,
            'chargesCalculator' => $this,
            'onlyView' => $onlyView,
            'currencies' => self::$currencies
        ]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'cod_fee' => Yii::t('common', 'Плата на наложенный платеж (%)'),
            'fulfilment_fee' => Yii::t('common', 'Плата за обработку заказа ({currency})', ['currency' => $this->delivery->country->currency->char_code]),
            'delivery_fee' => Yii::t('common', 'Плата за доставку посылки по стандартному тарифу ({currency})', ['currency' => $this->delivery->country->currency->char_code]),
            'default_weight' => Yii::t('common', 'Ограничение веса в стандартном тарифе (кг)'),
            'delivery_fee_for_add_weight' => Yii::t('common', 'Плата за каждый кг сверх ограничения ({currency})', ['currency' => $this->delivery->country->currency->char_code]),
            'return_fee' => Yii::t('common', 'Плата за возврат товара ({currency})', ['currency' => $this->delivery->country->currency->char_code]),
            'vat_percent' => Yii::t('common', 'НДС (%)'),
        ];
    }

    /**
     * @param $field
     * @return array
     */
    public function getCalculatingTypeForField($field)
    {
        $answer = [];
        switch ($field) {
            case OrderFinancePrediction::COLUMN_PRICE_COD_SERVICE:
                $answer = [
                    'type' => static::CALCULATING_TYPE_PERCENT,
                    'visible_formula' => "$this->cod_fee%",
                    'fields' => [
                        OrderFinancePrediction::COLUMN_PRICE_COD,
                    ],
                ];
                break;
            case OrderFinancePrediction::COLUMN_PRICE_VAT:
                $answer['type'] = static::CALCULATING_TYPE_PERCENT;
                $answer['visible_formula'] = "$this->vat_percent%";
                $answer['fields'] = [
                    OrderFinancePrediction::COLUMN_PRICE_DELIVERY,
                    OrderFinancePrediction::COLUMN_PRICE_DELIVERY_RETURN,
                    OrderFinancePrediction::COLUMN_PRICE_STORAGE,
                    OrderFinancePrediction::COLUMN_PRICE_FULFILMENT,
                    OrderFinancePrediction::COLUMN_PRICE_PACKAGE,
                    OrderFinancePrediction::COLUMN_PRICE_PACKING,
                    OrderFinancePrediction::COLUMN_PRICE_COD_SERVICE,
                ];
                break;
            default:
                $answer['type'] = static::CALCULATING_TYPE_NUMBER;
                break;
        }

        return $answer;
    }
}