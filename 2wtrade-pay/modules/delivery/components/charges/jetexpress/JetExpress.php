<?php

namespace app\modules\delivery\components\charges\jetexpress;


use app\models\Currency;
use app\modules\delivery\components\charges\ChargesCalculatorAbstract;
use app\modules\delivery\widgets\charges\helpers\CommonDeliveryCharges;
use app\modules\order\models\OrderFinancePrediction;
use yii\helpers\ArrayHelper;

/**
 * Class JetExpress
 * @package app\modules\delivery\components\charges\jetexpress
 */
class JetExpress extends ChargesCalculatorAbstract
{
    public $delivery_fee = 0;

    public $storage_fee = 0;

    public $fulfilment_fee = 0;

    public $package_fee = 0;

    public $return_fee = 0;

    public $cod_fee = 0;

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['delivery_fee', 'storage_fee', 'fulfilment_fee', 'package_fee', 'return_fee'], 'number', 'min' => 0],
            [['cod_fee'], 'number', 'min' => 0, 'max' => 100],
            [['currency_char_code'], 'string'],
        ]);
    }

    public function calculate($order)
    {
        $finance = $this->getOrderFinance($order);
        $currency = $this->getCurrency();
        $currencyId = null;
        if (isset($currency)) {
            $currencyId = $currency->id;
        }
        $finance->price_delivery = doubleval($this->delivery_fee);
        $finance->price_fulfilment = doubleval($this->fulfilment_fee);
        $finance->price_package = doubleval($this->package_fee);
        $finance->price_delivery_return = 0;
        if ($this->isOrderBuyout($order)) {
            $finance->price_cod = $order->delivery + $order->price_total;
            $finance->price_cod_service = $finance->price_cod * (doubleval($this->cod_fee) / 100);
        } else {
            if ($this->isOrderReturnToSender($order)) {
                $finance->price_delivery_return = doubleval($this->return_fee);
            }
            $finance->price_cod = 0;
            $finance->price_cod_service = 0;
        }

        $finance->price_delivery_currency_id = $finance->price_fulfilment_currency_id = $finance->price_package_currency_id = $finance->price_delivery_return_currency_id = $currencyId;

        return parent::afterCalculate($finance);
    }

    /**
     * @return array
     */
    protected function customAttributeLabels()
    {
        return [
            'storage_fee' => \Yii::t('common', 'Плата за аренду склада'),
            'fulfilment_fee' => \Yii::t('common', 'Стоимость обслуживания заказа')
        ];
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @param $onlyView
     * @return string
     */
    public function render($form, $onlyView = false)
    {
        return CommonDeliveryCharges::widget([
            'form' => $form,
            'model' => $this,
            'items' => [
                [
                    'attribute' => 'fulfilment_fee',
                    'options' => [
                        'disabled' => $onlyView,
                    ]
                ],
                [
                    'attribute' => 'package_fee',
                    'options' => [
                        'disabled' => $onlyView,
                    ]
                ],
                [
                    'attribute' => 'delivery_fee',
                    'options' => [
                        'disabled' => $onlyView
                    ],
                ],
                [
                    'attribute' => 'return_fee',
                    'options' => [
                        'disabled' => $onlyView,
                    ]
                ],
                [
                    'attribute' => 'cod_fee',
                    'options' => [
                        'disabled' => $onlyView,
                    ]
                ],
                [
                    'attribute' => 'storage_fee',
                    'options' => [
                        'disabled' => $onlyView,
                    ]
                ],
                [
                    'attribute' => 'currency_char_code',
                    'type' => 'select2List',
                    'items' => ArrayHelper::map(Currency::find()->all(), 'char_code', 'name'),
                    'options' => [
                        'length' => false,
                        'disabled' => $onlyView,
                    ]
                ]
            ]
        ]);
    }

    /**
     * @param $field
     * @return array
     */
    public function getCalculatingTypeForField($field)
    {
        $answer = [];
        switch ($field) {
            case OrderFinancePrediction::COLUMN_PRICE_COD_SERVICE:
                $answer = [
                    'type' => static::CALCULATING_TYPE_PERCENT,
                    'visible_formula' => "$this->cod_fee%",
                    'fields' => [
                        OrderFinancePrediction::COLUMN_PRICE_COD,
                    ],
                ];
                break;
            case OrderFinancePrediction::COLUMN_PRICE_STORAGE:
                $answer = [
                    'type' => ChargesCalculatorAbstract::CALCULATING_TYPE_MONTHLY_CHARGE,
                    'charge' => doubleval($this->storage_fee),
                    'currency_id' => $this->getCurrency()->id,
                    'currency_rate' => $this->getCurrency()->currencyRate->rate,
                ];
                break;
            default:
                $answer['type'] = static::CALCULATING_TYPE_NUMBER;
                break;
        }

        return $answer;
    }
}