<?php

namespace app\modules\delivery\components\charges\acommerce;

use app\modules\delivery\components\charges\ChargesCalculatorAbstract;
use app\modules\delivery\widgets\charges\acommerce\AcommerceTHWidget;
use app\modules\order\models\OrderFinancePrediction;
use Yii;

/**
 * Class AcommerceTH
 * @package app\modules\delivery\components\charges\acommerce
 */
class AcommerceTH extends ChargesCalculatorAbstract
{
    public $handling_xs = 0;
    public $handling_s = 0;
    public $handling_m = 0;
    public $handling_l = 0;
    public $handling_xl = 0;

    public $storage_xs = 0;
    public $storage_s = 0;
    public $storage_m = 0;
    public $storage_l = 0;
    public $storage_xl = 0;

    public $fulfilment_fee = 0;

    public $packaging_aaa = 0;
    public $packaging_aa = 0;
    public $packaging_a = 0;
    public $packaging_b = 0;
    public $packaging_c = 0;
    public $packaging_d = 0;
    public $packaging_e = 0;
    public $packaging_f = 0;
    public $packaging_g = 0;
    public $packaging_xl = 0;
    public $packaging_xxl = 0;

    public $labelling_fee = 0;
    public $repack_fee = 0;
    public $fragile_fee = 0;
    public $return_fee = 0;
    public $vendor_return_handling = 0;

    public $delivery_fee_1kg = [];
    public $delivery_fee_2kg = [];
    public $delivery_fee_3kg = [];
    public $delivery_fee_4kg = [];
    public $delivery_fee_5kg = [];
    public $delivery_fee_6kg = [];
    public $delivery_fee_7kg = [];
    public $delivery_fee_8kg = [];
    public $delivery_fee_9kg = [];
    public $delivery_fee_10kg = [];
    public $delivery_fee_15kg = [];
    public $delivery_fee_20kg = [];
    public $delivery_fee_more_20kg = [];

    public $cod_fee = 0;
    public $vat_percent = 0;

    public $zipcodes = [];

    protected function getDeliveryFee()
    {
        return $this->delivery_fee_1kg;
    }

    /**
     * Объемы возможных упаковок
     * @var array
     */
    protected static $packagingVolumes = [
        'packaging_aaa' => 735,
        'packaging_aa' => 1440,
        'packaging_a' => 1740,
        'packaging_b' => 4095,
        'packaging_c' => 6765,
        'packaging_d' => 11270,
        'packaging_e' => 16523,
        'packaging_f' => 18000,
        'packaging_g' => 28210,
    ];

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [
                [
                    'handling_xs',
                    'handling_s',
                    'handling_m',
                    'handling_l',
                    'handling_xl',
                    'storage_xs',
                    'storage_s',
                    'storage_m',
                    'storage_l',
                    'storage_xl',
                    'fulfilment_fee',
                    'packaging_aaa',
                    'packaging_aa',
                    'packaging_a',
                    'packaging_b',
                    'packaging_c',
                    'packaging_d',
                    'packaging_e',
                    'packaging_f',
                    'packaging_g',
                    'packaging_xl',
                    'packaging_xxl',
                    'labelling_fee',
                    'repack_fee',
                    'fragile_fee',
                    'return_fee',
                    'vendor_return_handling',
                ],
                'number',
                'min' => 0
            ],
            [['cod_fee', 'vat_percent'], 'number', 'min' => 0, 'max' => 100],
            ['zipcodes', 'each', 'rule' => ['string']],
            ['zipcodes', 'each', 'rule' => ['filter', 'filter' => [$this, 'filterZipcodes']]],
            [
                [
                    'delivery_fee_1kg',
                    'delivery_fee_2kg',
                    'delivery_fee_3kg',
                    'delivery_fee_4kg',
                    'delivery_fee_5kg',
                    'delivery_fee_6kg',
                    'delivery_fee_7kg',
                    'delivery_fee_8kg',
                    'delivery_fee_9kg',
                    'delivery_fee_10kg',
                    'delivery_fee_15kg',
                    'delivery_fee_20kg',
                    'delivery_fee_more_20kg',
                ],
                'each',
                'rule' => ['number', 'min' => 0]
            ],
            [['currency_char_code'], 'string'],
        ]);
    }

    /**
     * @param \app\modules\order\models\Order $order
     * @return OrderFinancePrediction
     */
    public function calculate($order)
    {
        $finance = $this->getOrderFinance($order);
        $weight = $this->getWeightOfParcel($order);
        if ($weight < 500) {
            $finance->price_fulfilment = doubleval($this->handling_xs);
            $finance->price_storage = doubleval($this->storage_xs);
        } elseif ($weight < 1000) {
            $finance->price_fulfilment = doubleval($this->handling_s);
            $finance->price_storage = doubleval($this->storage_s);
        } elseif ($weight < 2000) {
            $finance->price_fulfilment = doubleval($this->handling_m);
            $finance->price_storage = doubleval($this->storage_m);
        } elseif ($weight < 5000) {
            $finance->price_fulfilment = doubleval($this->handling_l);
            $finance->price_storage = doubleval($this->storage_l);
        } else {
            $finance->price_fulfilment = doubleval($this->handling_xl);
            $finance->price_storage = doubleval($this->storage_xl);
        }
        $finance->price_fulfilment += doubleval($this->fulfilment_fee);

        $finance->price_package = doubleval($this->labelling_fee) + doubleval($this->fragile_fee) + doubleval($this->repack_fee);

        $finance->price_packing = max(doubleval($this->packaging_xl), doubleval($this->packaging_xxl), doubleval($this->packaging_g), doubleval($this->packaging_aaa), doubleval($this->packaging_aa), doubleval($this->packaging_a), doubleval($this->packaging_b), doubleval($this->packaging_c), doubleval($this->packaging_d), doubleval($this->packaging_e), doubleval($this->packaging_f));
        $volume = 0;
        foreach ($order->orderProducts as $product) {
            $volume += $product->quantity * (($product->product->width / 10) * ($product->product->height / 10) * ($product->product->length / 10));
        }
        $maxKey = 'packaging_aaa';
        $maxVolume = 0;

        foreach (self::$packagingVolumes as $key => $packageVolume) {
            if ($packageVolume > $volume && $maxVolume < $packageVolume) {
                $maxKey = $key;
                $maxVolume = $packageVolume;
            }
        }
        if (!empty($maxVolume)) {
            $finance->price_packing = doubleval($this->$maxKey);
        }

        $weight = ceil($weight / 1000);
        $delivery_fee_1kg = max($this->delivery_fee_1kg);
        $delivery_fee_2kg = max($this->delivery_fee_2kg);
        $delivery_fee_3kg = max($this->delivery_fee_3kg);
        $delivery_fee_4kg = max($this->delivery_fee_4kg);
        $delivery_fee_5kg = max($this->delivery_fee_5kg);
        $delivery_fee_6kg = max($this->delivery_fee_6kg);
        $delivery_fee_7kg = max($this->delivery_fee_7kg);
        $delivery_fee_8kg = max($this->delivery_fee_8kg);
        $delivery_fee_9kg = max($this->delivery_fee_9kg);
        $delivery_fee_10kg = max($this->delivery_fee_10kg);
        $delivery_fee_15kg = max($this->delivery_fee_15kg);
        $delivery_fee_20kg = max($this->delivery_fee_20kg);
        $delivery_fee_more_20kg = max($this->delivery_fee_more_20kg);
        $index = $this->getIndexByZipcode($this->zipcodes, $order->customer_zip);
        if (!is_null($index)) {
            $delivery_fee_1kg = $this->delivery_fee_1kg[$index];
            $delivery_fee_2kg = $this->delivery_fee_2kg[$index];
            $delivery_fee_3kg = $this->delivery_fee_3kg[$index];
            $delivery_fee_4kg = $this->delivery_fee_4kg[$index];
            $delivery_fee_5kg = $this->delivery_fee_5kg[$index];
            $delivery_fee_6kg = $this->delivery_fee_6kg[$index];
            $delivery_fee_7kg = $this->delivery_fee_7kg[$index];
            $delivery_fee_8kg = $this->delivery_fee_8kg[$index];
            $delivery_fee_9kg = $this->delivery_fee_9kg[$index];
            $delivery_fee_10kg = $this->delivery_fee_10kg[$index];
            $delivery_fee_15kg = $this->delivery_fee_15kg[$index];
            $delivery_fee_20kg = $this->delivery_fee_20kg[$index];
            $delivery_fee_more_20kg = $this->delivery_fee_more_20kg[$index];
        }
        $deliveryVar = "delivery_fee_{$weight}kg";
        if (isset($$deliveryVar)) {
            $delivery_fee = $$deliveryVar;
        } elseif ($weight > 10 && $weight <= 15) {
            $delivery_fee = $delivery_fee_15kg;
        } elseif ($weight > 15 && $weight <= 20) {
            $delivery_fee = $delivery_fee_20kg;
        } else {
            $delivery_fee = doubleval($delivery_fee_20kg) + (($weight - 20) * doubleval($delivery_fee_more_20kg));
        }
        $finance->price_delivery = doubleval($delivery_fee);

        if ($this->isOrderBuyout($order)) {
            $finance->price_cod = $order->price_total + $order->delivery;
            $finance->price_cod_service = $finance->price_cod * (doubleval($this->cod_fee) / 100);
            $finance->price_delivery_return = 0;
            $finance->price_vat = $finance->price_cod * (doubleval($this->vat_percent) / 100);
        } else {
            if ($this->isOrderReturnToSender($order)) {
                $finance->price_delivery_return = doubleval($this->return_fee) + doubleval($this->vendor_return_handling);
            } else {
                $finance->price_delivery_return = 0;
            }
            $finance->price_cod = 0;
            $finance->price_cod_service = 0;
            $finance->price_vat = 0;
        }

        return parent::afterCalculate($finance);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'handling_xs' => Yii::t('common', 'Обработка заказа весом до 0,5кг ({currency})', ['currency' => $this->delivery->country->currency->char_code]),
            'handling_s' => Yii::t('common', 'Обработка заказа весом 0,5-1кг ({currency})', ['currency' => $this->delivery->country->currency->char_code]),
            'handling_m' => Yii::t('common', 'Обработка заказа весом 1-2кг ({currency})', ['currency' => $this->delivery->country->currency->char_code]),
            'handling_l' => Yii::t('common', 'Обработка заказа весом 2-5кг ({currency})', ['currency' => $this->delivery->country->currency->char_code]),
            'handling_xl' => Yii::t('common', 'Обработка заказа весом 5-10кг ({currency})', ['currency' => $this->delivery->country->currency->char_code]),
            'storage_xs' => Yii::t('common', 'Хранение заказа весом до 0,5кг ({currency})', ['currency' => $this->delivery->country->currency->char_code]),
            'storage_s' => Yii::t('common', 'Хранение заказа весом 0,5-1кг ({currency})', ['currency' => $this->delivery->country->currency->char_code]),
            'storage_m' => Yii::t('common', 'Хранение заказа весом 1-2кг ({currency})', ['currency' => $this->delivery->country->currency->char_code]),
            'storage_l' => Yii::t('common', 'Хранение заказа весом 2-5кг ({currency})', ['currency' => $this->delivery->country->currency->char_code]),
            'storage_xl' => Yii::t('common', 'Хранение заказа весом 5-10кг ({currency})', ['currency' => $this->delivery->country->currency->char_code]),
            'fulfilment_fee' => Yii::t('common', 'Обслуживание заказа ({currency})', ['currency' => $this->delivery->country->currency->char_code]),
            'packaging_aaa' => Yii::t('common', 'Упаковка для заказа (8.9*14*5.9) ({currency})', ['currency' => $this->delivery->country->currency->char_code]),
            'packaging_aa' => Yii::t('common', 'Упаковка для заказа (10*18*8) ({currency})', ['currency' => $this->delivery->country->currency->char_code]),
            'packaging_a' => Yii::t('common', 'Упаковка для заказа (14.5*20*6) ({currency})', ['currency' => $this->delivery->country->currency->char_code]),
            'packaging_b' => Yii::t('common', 'Упаковка для заказа (17.5*26*9) ({currency})', ['currency' => $this->delivery->country->currency->char_code]),
            'packaging_c' => Yii::t('common', 'Упаковка для заказа (20.5*30*11) ({currency})', ['currency' => $this->delivery->country->currency->char_code]),
            'packaging_d' => Yii::t('common', 'Упаковка для заказа (23*35*14) ({currency})', ['currency' => $this->delivery->country->currency->char_code]),
            'packaging_e' => Yii::t('common', 'Упаковка для заказа (31*41*13) ({currency})', ['currency' => $this->delivery->country->currency->char_code]),
            'packaging_f' => Yii::t('common', 'Упаковка для заказа (25*40*18) ({currency})', ['currency' => $this->delivery->country->currency->char_code]),
            'packaging_g' => Yii::t('common', 'Упаковка для заказа (31*45.5*20) ({currency})', ['currency' => $this->delivery->country->currency->char_code]),
            'packaging_xl' => Yii::t('common', 'Упаковка для заказа (**) ({currency})', ['currency' => $this->delivery->country->currency->char_code]),
            'packaging_xxl' => Yii::t('common', 'Упаковка для заказа (**) ({currency})', ['currency' => $this->delivery->country->currency->char_code]),
            'labelling_fee' => Yii::t('common', 'Наклейка этикетки ({currency})', ['currency' => $this->delivery->country->currency->char_code]),
            'repack_fee' => Yii::t('common', 'Упаковывание товаров ({currency})', ['currency' => $this->delivery->country->currency->char_code]),
            'return_fee' => Yii::t('common', 'Возврат товаров ({currency})', ['currency' => $this->delivery->country->currency->char_code]),
            'cod_fee' => Yii::t('common', 'Налог на наложенный платеж (%)'),
            'vat_percent' => Yii::t('common', 'НДС (%)'),
            'zipcodes' => Yii::t('common', 'Индексы региона'),
            'delivery_fee_1kg' => Yii::t('common', 'Доставка менее 1кг ({currency})', ['currency' => $this->delivery->country->currency->char_code]),
            'delivery_fee_2kg' => Yii::t('common', 'Доставка 1-2кг ({currency})', ['currency' => $this->delivery->country->currency->char_code]),
            'delivery_fee_3kg' => Yii::t('common', 'Доставка 2-3кг ({currency})', ['currency' => $this->delivery->country->currency->char_code]),
            'delivery_fee_4kg' => Yii::t('common', 'Доставка 3-4кг ({currency})', ['currency' => $this->delivery->country->currency->char_code]),
            'delivery_fee_5kg' => Yii::t('common', 'Доставка 4-5кг ({currency})', ['currency' => $this->delivery->country->currency->char_code]),
            'delivery_fee_6kg' => Yii::t('common', 'Доставка 5-6кг ({currency})', ['currency' => $this->delivery->country->currency->char_code]),
            'delivery_fee_7kg' => Yii::t('common', 'Доставка 6-7кг ({currency})', ['currency' => $this->delivery->country->currency->char_code]),
            'delivery_fee_8kg' => Yii::t('common', 'Доставка 7-8кг ({currency})', ['currency' => $this->delivery->country->currency->char_code]),
            'delivery_fee_9kg' => Yii::t('common', 'Доставка 8-9кг ({currency})', ['currency' => $this->delivery->country->currency->char_code]),
            'delivery_fee_10kg' => Yii::t('common', 'Доставка 9-10кг ({currency})', ['currency' => $this->delivery->country->currency->char_code]),
            'delivery_fee_15kg' => Yii::t('common', 'Доставка 10-15кг ({currency})', ['currency' => $this->delivery->country->currency->char_code]),
            'delivery_fee_20kg' => Yii::t('common', 'Доставка 15-20кг ({currency})', ['currency' => $this->delivery->country->currency->char_code]),
            'delivery_fee_more_20kg' => Yii::t('common', 'За каждый последующий кг ({currency})', ['currency' => $this->delivery->country->currency->char_code]),
            'fragile_fee' => Yii::t('common', 'Плата за хрупкий груз ({currency})', ['currency' => $this->delivery->country->currency->char_code]),
            'vendor_return_handling' => Yii::t('common', 'Обработка возвратов ({currency})', ['currency' => $this->delivery->country->currency->char_code]),
        ];
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @param $onlyView
     * @return string
     */
    public function render($form, $onlyView = false)
    {
        return AcommerceTHWidget::widget(['form' => $form, 'chargesCalculator' => $this, 'currencies' => self::$currencies, 'onlyView' => $onlyView]);
    }


    /**
     * @param $field
     * @return array
     */
    public function getCalculatingTypeForField($field)
    {
        $answer = [];
        switch ($field) {
            case OrderFinancePrediction::COLUMN_PRICE_COD_SERVICE:
                $answer = [
                    'type' => static::CALCULATING_TYPE_PERCENT,
                    'visible_formula' => "$this->cod_fee%",
                    'fields' => [
                        OrderFinancePrediction::COLUMN_PRICE_COD,
                    ],
                ];
                break;
            case OrderFinancePrediction::COLUMN_PRICE_VAT:
                $answer['type'] = static::CALCULATING_TYPE_PERCENT;
                $answer['visible_formula'] = "$this->vat_percent%";
                $answer['fields'] = [
                    OrderFinancePrediction::COLUMN_PRICE_COD,
                ];
                break;
            default:
                $answer['type'] = static::CALCULATING_TYPE_NUMBER;
                break;
        }

        return $answer;
    }
}