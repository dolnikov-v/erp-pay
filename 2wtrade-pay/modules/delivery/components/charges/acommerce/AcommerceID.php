<?php

namespace app\modules\delivery\components\charges\acommerce;

use app\modules\delivery\components\charges\ChargesCalculatorAbstract;
use app\modules\delivery\widgets\charges\acommerce\AcommerceIDWidget;
use app\modules\order\models\OrderFinancePrediction;
use Yii;

/**
 * Class AcommerceID
 * @package app\modules\delivery\components\charges\acommerce
 */
class AcommerceID extends ChargesCalculatorAbstract
{
    public $cod_fee = 0;
    public $insurance_percent = 0;
    public $storage_price = 0;
    public $delivery_fees = [];
    public $zipcodes = [];

    protected $countOfOrderByPeriods = null;

    /**
     * @param \app\modules\order\models\Order $order
     * @return OrderFinancePrediction
     */
    public function calculate($order)
    {
        $finance = $this->getOrderFinance($order);
        $index = $this->getIndexByZipcode($this->zipcodes, $order->customer_zip);
        $delivery_fee = 0;
        if (!is_null($index)) {
            $delivery_fee = doubleval($this->delivery_fees[$index] ?? 0);
        }

        $finance->price_storage = 0;
        $finance->price_delivery = $delivery_fee;

        if ($this->isOrderBuyout($order)) {
            $finance->price_cod = $order->price_total + $order->delivery;
            $finance->price_cod_service = $finance->price_cod * (doubleval($this->cod_fee) / 100);
            $finance->price_cod_service += ($order->price_total + $order->delivery) * (doubleval($this->insurance_percent) / 100);
        } else {
            $finance->price_cod = 0;
            $finance->price_cod_service = 0;
        }

        return parent::afterCalculate($finance);
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @param $onlyView
     * @return string
     */
    public function render($form, $onlyView = false)
    {
        return AcommerceIDWidget::widget([
            'form' => $form,
            'chargesCalculator' => $this,
            'onlyView' => $onlyView,
            'currencies' => self::$currencies
        ]);
    }

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['cod_fee', 'insurance_percent'], 'number', 'min' => 0, 'max' => 100],
            [['storage_price'], 'number', 'min' => 0],
            ['zipcodes', 'each', 'rule' => ['string']],
            ['zipcodes', 'each', 'rule' => ['filter', 'filter' => [$this, 'filterZipcodes']]],
            ['delivery_fees', 'each', 'rule' => ['number', 'min' => 0]],
            [['currency_char_code'], 'string'],
        ]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'cod_fee' => Yii::t('common', 'Плата за наложенный платеж (%)'),
            'insurance_percent' => Yii::t('common', 'Страховка (%)'),
            'storage_price' => Yii::t('common', 'Плата за аренду склада'),
            'zipcodes' => Yii::t('common', 'Индексы региона'),
            'delivery_fees' => Yii::t('common', 'Цена за доставку'),
        ];
    }

    /**
     * @return array|null
     */
    protected function getOrdersCountByMonths()
    {
        if (is_null($this->countOfOrderByPeriods)) {
            $this->countOfOrderByPeriods = parent::getOrdersCountByMonths();
        }
        return $this->countOfOrderByPeriods;
    }

    /**
     * @param $field
     * @return array
     */
    public function getCalculatingTypeForField($field)
    {
        $answer = [];
        switch ($field) {
            case OrderFinancePrediction::COLUMN_PRICE_COD_SERVICE:
                $answer = [
                    'type' => static::CALCULATING_TYPE_PERCENT,
                    'visible_formula' => "{$this->cod_fee}% + {$this->insurance_percent}%",
                    'fields' => [
                        OrderFinancePrediction::COLUMN_PRICE_COD,
                    ],
                ];
                break;
            case OrderFinancePrediction::COLUMN_PRICE_STORAGE:
                $answer = [
                    'type' => ChargesCalculatorAbstract::CALCULATING_TYPE_MONTHLY_CHARGE,
                    'charge' => doubleval($this->storage_price),
                    'currency_id' => $this->getCurrency()->id,
                    'currency_rate' => $this->getCurrency()->currencyRate->rate,
                ];
                break;
            default:
                $answer['type'] = static::CALCULATING_TYPE_NUMBER;
                break;
        }

        return $answer;
    }
}