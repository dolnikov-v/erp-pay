<?php

namespace app\modules\delivery\components\charges;

use app\modules\order\models\OrderFinancePrediction;
use Yii;

/**
 * Class DifferentDeliveryChargeByCity
 * @package app\modules\delivery\components\charges
 */
class DifferentDeliveryChargeByCity extends DifferentDeliveryChargeByZip
{
    /**
     * @var \app\modules\order\models\Order
     */
    protected $order;

    /**
     * @param \app\modules\order\models\Order $order
     * @return OrderFinancePrediction
     */
    public function calculate($order)
    {
        $this->order = $order;
        return parent::calculate($order);
    }

    /**
     * @param array $zipcodes
     * @param string $customer_city
     * @return int|null|string
     */
    public function getIndexByZipcode($zipcodes = [], $customer_city = '')
    {
       return parent::getIndexByZipcode($zipcodes, mb_strtolower($this->order->customer_city, 'UTF-8'));
    }

    /**
     * @param string $zipcodes
     * @return string
     */
    public function filterZipcodes($zipcodes)
    {
        $zipcodes = mb_strtolower($zipcodes, 'UTF-8');
        return parent::filterZipcodes($zipcodes);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'zipcodes' => Yii::t('common', 'Города'),
        ]);
    }
}