<?php

namespace app\modules\delivery\components\charges\atri;


use app\models\Currency;
use app\modules\delivery\components\charges\OnlyDeliveryCharge;
use app\modules\delivery\widgets\charges\helpers\CommonDeliveryCharges;
use yii\helpers\ArrayHelper;

/**
 * Class Atri
 * @package app\modules\delivery\components\charges\atri
 */
class Atri extends OnlyDeliveryCharge
{
    /**
     * @param \app\modules\order\models\Order $order
     * @return \app\modules\order\models\OrderFinancePrediction
     */
    public function calculate($order)
    {
        $finance = parent::calculate($order);
        $weight = $this->getWeightOfParcel($order, true);
        $finance->price_delivery *= $weight;
        $finance->price_delivery_return *= $weight;
        return parent::afterCalculate($finance);
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @param $onlyView
     * @return string
     */
    public function render($form, $onlyView = false)
    {
        return CommonDeliveryCharges::widget([
            'form' => $form,
            'model' => $this,
            'items' => [
                [
                    'attribute' => 'delivery_fee',
                    'options' => [
                        'disabled' => $onlyView
                    ]
                ],
                [
                    'attribute' => 'return_fee',
                    'options' => [
                        'disabled' => $onlyView
                    ]
                ],
                [
                    'attribute' => 'fulfillment_fee',
                    'options' => [
                        'disabled' => $onlyView
                    ]
                ],
                [
                    'attribute' => 'cod_fee',
                    'options' => [
                        'disabled' => $onlyView
                    ]
                ],
                [
                    'attribute' => 'vat_percent',
                    'options' => [
                        'disabled' => $onlyView
                    ]
                ],
                [
                    'attribute' => 'currency_char_code',
                    'type' => 'select2List',
                    'items' => ArrayHelper::map(Currency::find()->all(), 'char_code', 'name'),
                    'length' => 2,
                    'options' => [
                        'length' => false,
                        'disabled' => $onlyView
                    ]
                ]
            ]
        ]);
    }
}