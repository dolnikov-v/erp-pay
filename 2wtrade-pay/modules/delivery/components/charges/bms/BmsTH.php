<?php

namespace app\modules\delivery\components\charges\bms;

use app\modules\delivery\components\charges\ChargesCalculatorAbstract;
use app\modules\delivery\widgets\charges\bms\BmsTHWidget;
use app\modules\order\models\Order;
use app\modules\order\models\OrderFinancePrediction;
use Yii;
use yii\db\ActiveQuery;

/**
 * Class BmsTH
 * @package app\modules\delivery\components\charges\bms
 */
class BmsTH extends ChargesCalculatorAbstract
{
    /**
     * Один продукт / менее 500 заказов в месяц
     * @var float
     */
    public $delivery_fee_one_prod_less_five = 0;

    /**
     * Два продукт / менее 500 заказов в месяц
     * @var float
     */
    public $delivery_fee_two_prod_less_five = 0;

    /**
     * Три продукт / менее 500 заказов в месяц
     * @var float
     */
    public $delivery_fee_three_prod_less_five = 0;
    /**
     * Один продукт / более 500 заказов в месяц
     * @var float
     */
    public $delivery_fee_one_prod_more_five = 0;
    /**
     * Два продукт / более 500 заказов в месяц
     * @var float
     */
    public $delivery_fee_two_prod_more_five = 0;
    /**
     * Три продукт / более 500 заказов в месяц
     * @var float
     */
    public $delivery_fee_three_prod_more_five = 0;

    /**
     * Один продукт / более 1000 заказов в месяц
     * @var float
     */
    public $delivery_fee_one_prod_more_1k = 0;
    /**
     * Два продукт / более 1000 заказов в месяц
     * @var float
     */
    public $delivery_fee_two_prod_more_1k = 0;
    /**
     * Три продукт / более 1000 заказов в месяц
     * @var float
     */
    public $delivery_fee_three_prod_more_1k = 0;

    /**
     * Один продукт / менее 500 заказов в месяц
     * @var float
     */
    public $return_fee_one_prod_less_five = 0;

    /**
     * Два продукт / менее 500 заказов в месяц
     * @var float
     */
    public $return_fee_two_prod_less_five = 0;

    /**
     * Три продукт / менее 500 заказов в месяц
     * @var float
     */
    public $return_fee_three_prod_less_five = 0;
    /**
     * Один продукт / более 500 заказов в месяц
     * @var float
     */
    public $return_fee_one_prod_more_five = 0;
    /**
     * Два продукт / более 500 заказов в месяц
     * @var float
     */
    public $return_fee_two_prod_more_five = 0;
    /**
     * Три продукт / более 500 заказов в месяц
     * @var float
     */
    public $return_fee_three_prod_more_five = 0;

    /**
     * Один продукт / более 1000 заказов в месяц
     * @var float
     */
    public $return_fee_one_prod_more_1k = 0;
    /**
     * Два продукт / более 1000 заказов в месяц
     * @var float
     */
    public $return_fee_two_prod_more_1k = 0;
    /**
     * Три продукт / более 1000 заказов в месяц
     * @var float
     */
    public $return_fee_three_prod_more_1k = 0;

    /**
     * @var int
     */
    public $fulfilment_fee = 0;

    /**
     * @var int
     */
    public $bms_acommerce_bms = 0;

    /**
     * @var int
     */
    public $storage_fee = 0;

    /**
     * НДС %
     * @var float
     */
    public $vat_percent = 0;

    public function rules()
    {
        return array_merge(parent::rules(), [
            [[
                'delivery_fee_one_prod_less_five',
                'delivery_fee_two_prod_less_five',
                'delivery_fee_three_prod_less_five',
                'delivery_fee_one_prod_more_five',
                'delivery_fee_one_prod_more_five',
                'delivery_fee_three_prod_more_five',
                'delivery_fee_one_prod_more_1k',
                'delivery_fee_two_prod_more_1k',
                'delivery_fee_three_prod_more_1k',
                'return_fee_one_prod_less_five',
                'return_fee_two_prod_less_five',
                'return_fee_three_prod_less_five',
                'return_fee_one_prod_more_five',
                'return_fee_one_prod_more_five',
                'return_fee_three_prod_more_five',
                'return_fee_one_prod_more_1k',
                'return_fee_two_prod_more_1k',
                'return_fee_three_prod_more_1k',
                'fulfilment_fee',
                'storage_fee',
                'bms_acommerce_bms',
            ], 'number', 'min' => 0],
            [['vat_percent'], 'number', 'min' => 0, 'max' => 100],
            [['currency_char_code'], 'string'],
        ]);
    }

    /**
     * @param Order $order
     * @return null
     */
    public function calculate($order)
    {
        return null;
    }

    /**
     * @param ActiveQuery $query
     */
    public function calculateForBatch($query)
    {
        if (!$query->count()) {
            return;
        }
        $query->with(['callCenterRequest']);
        $periods = $this->getOrdersCountByMonths();

        foreach ($query->batch(1000) as $orders) {
            /**
             * @var Order[] $orders
             */
            foreach ($orders as $order) {
                $productCount = 0;
                foreach ($order->orderProducts as $product) {
                    $productCount += $product->quantity;
                }
                $date = $this->getOrderSendingMonth($order);

                $prod = 'three_prod';
                $orderCount = 'less_five';

                if (isset($periods[$date]) && $periods[$date] >= 500) {
                    if ($periods[$date] > 1000) {
                        $orderCount = 'more_1k';
                    } else {
                        $orderCount = 'more_five';
                    }
                }

                if ($productCount == 1) {
                    $prod = 'one_prod';
                } elseif ($productCount <= 3) {
                    $prod = 'two_prod';
                }

                $deliveryVar = "delivery_fee_{$prod}_{$orderCount}";
                $returnVar = "return_fee_{$prod}_{$orderCount}";
                $deliveryFee = doubleval($this->$deliveryVar);
                $returnFee = doubleval($this->$returnVar);

                $finance = $this->getOrderFinance($order);

                $finance->price_delivery = $deliveryFee;

                if ($this->isOrderBuyout($order)) {
                    $finance->price_cod = $order->price_total + $order->delivery;
                    $finance->price_delivery_return = 0;
                    $finance->price_vat = $finance->price_cod * (doubleval($this->vat_percent) / 100);
                } else {
                    if ($this->isOrderReturnToSender($order)) {
                        $finance->price_delivery_return = $returnFee;
                    }
                    $finance->price_cod = 0;
                    $finance->price_cod_service = 0;
                    $finance->price_vat = 0;
                }
                $finance->price_storage = 0;
                $finance->price_fulfilment = 0;
                $finance->price_packing = 0;
                $finance->save();
            }
        }

    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @param $onlyView
     * @return string
     */
    public function render($form, $onlyView = false)
    {
        return BmsTHWidget::widget(['form' => $form, 'values' => $this->deliveryContract->chargesValues, 'chargesCalculator' => $this, 'currencies' => self::$currencies, 'onlyView' => $onlyView]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        $currencyCode = $this->delivery->country->currency->char_code;
        return [
            'delivery_fee_one_prod_less_five' => Yii::t('common', 'Плата за доставку: 1 шт. в заказе, менее 500 заказов в месяц ({currency})', ['currency' => $currencyCode]),
            'delivery_fee_two_prod_less_five' => Yii::t('common', 'Плата за доставку: 2-3 шт. в заказе, менее 500 заказов в месяц ({currency})', ['currency' => $currencyCode]),
            'delivery_fee_three_prod_less_five' => Yii::t('common', 'Плата за доставку: 3-6 шт. в заказе, менее 500 заказов в месяц ({currency})', ['currency' => $currencyCode]),
            'delivery_fee_one_prod_more_five' => Yii::t('common', 'Плата за доставку: 1 шт. в заказе, более 500 заказов в месяц ({currency})', ['currency' => $currencyCode]),
            'delivery_fee_two_prod_more_five' => Yii::t('common', 'Плата за доставку: 2-3 шт. в заказе, более 500 заказов в месяц ({currency})', ['currency' => $currencyCode]),
            'delivery_fee_three_prod_more_five' => Yii::t('common', 'Плата за доставку: 3-6 шт. в заказе, более 500 заказов в месяц ({currency})', ['currency' => $currencyCode]),
            'delivery_fee_one_prod_more_1k' => Yii::t('common', 'Плата за доставку: 1 шт. в заказе, более 1000 заказов в месяц ({currency})', ['currency' => $currencyCode]),
            'delivery_fee_two_prod_more_1k' => Yii::t('common', 'Плата за доставку: 2-3 шт. в заказе, более 1000 заказов в месяц ({currency})', ['currency' => $currencyCode]),
            'delivery_fee_tree_prod_more_1k' => Yii::t('common', 'Плата за доставку: 3-6 шт. в заказе, более 1000 заказов в месяц ({currency})', ['currency' => $currencyCode]),
            'return_fee_one_prod_less_five' => Yii::t('common', 'Плата за возврат: 1 шт. в заказе, менее 500 заказов в месяц ({currency})', ['currency' => $currencyCode]),
            'return_fee_two_prod_less_five' => Yii::t('common', 'Плата за возврат: 2-3 шт. в заказе, менее 500 заказов в месяц ({currency})', ['currency' => $currencyCode]),
            'return_fee_three_prod_less_five' => Yii::t('common', 'Плата за возврат: 3-6 шт. в заказе, менее 500 заказов в месяц ({currency})', ['currency' => $currencyCode]),
            'return_fee_one_prod_more_five' => Yii::t('common', 'Плата за возврат: 1 шт. в заказе, более 500 заказов в месяц ({currency})', ['currency' => $currencyCode]),
            'return_fee_two_prod_more_five' => Yii::t('common', 'Плата за возврат: 2-3 шт. в заказе, более 500 заказов в месяц ({currency})', ['currency' => $currencyCode]),
            'return_fee_three_prod_more_five' => Yii::t('common', 'Плата за возврат: 3-6 шт. в заказе, более 500 заказов в месяц ({currency})', ['currency' => $currencyCode]),
            'return_fee_one_prod_more_1k' => Yii::t('common', 'Плата за возврат: 1 шт. в заказе, более 1000 заказов в месяц ({currency})', ['currency' => $currencyCode]),
            'return_fee_two_prod_more_1k' => Yii::t('common', 'Плата за возврат: 2-3 шт. в заказе, более 1000 заказов в месяц ({currency})', ['currency' => $currencyCode]),
            'return_fee_three_prod_more_1k' => Yii::t('common', 'Плата за возврат: 3-6 шт. в заказе, более 1000 заказов в месяц ({currency})', ['currency' => $currencyCode]),
            'vat_percent' => Yii::t('common', 'НДС (% от общей суммы)'),
            'bms_acommerce_bms' => Yii::t('common', 'Ежемесячная плата BMS - aCommerce - BMS ({currency})', ['currency' => $currencyCode]),
            'storage_fee' => Yii::t('common', 'Ежемесячная плата за склад ({currency})', ['currency' => $currencyCode]),
            'fulfilment_fee' => Yii::t('common', 'Ежемесячная плата за обслуживание ({currency})', ['currency' => $currencyCode]),
        ];
    }


    /**
     * @param $field
     * @return array
     */
    public function getCalculatingTypeForField($field)
    {
        $answer = [];
        switch ($field) {
            case OrderFinancePrediction::COLUMN_PRICE_VAT:
                $answer['type'] = static::CALCULATING_TYPE_PERCENT;
                $answer['visible_formula'] = "$this->vat_percent%";
                $answer['fields'] = [
                    OrderFinancePrediction::COLUMN_PRICE_COD,
                ];
                break;
            case OrderFinancePrediction::COLUMN_PRICE_STORAGE:
                $answer = [
                    'type' => ChargesCalculatorAbstract::CALCULATING_TYPE_MONTHLY_CHARGE,
                    'charge' => doubleval($this->storage_fee),
                    'currency_id' => $this->getCurrency()->id,
                    'currency_rate' => $this->getCurrency()->currencyRate->rate,
                ];
                break;
            case OrderFinancePrediction::COLUMN_PRICE_FULFILMENT:
                $answer = [
                    'type' => ChargesCalculatorAbstract::CALCULATING_TYPE_MONTHLY_CHARGE,
                    'charge' => doubleval($this->fulfilment_fee),
                    'currency_id' => $this->getCurrency()->id,
                    'currency_rate' => $this->getCurrency()->currencyRate->rate,
                ];
                break;
            case OrderFinancePrediction::COLUMN_PRICE_PACKING:
                $answer = [
                    'type' => ChargesCalculatorAbstract::CALCULATING_TYPE_MONTHLY_CHARGE,
                    'charge' => doubleval($this->bms_acommerce_bms),
                    'currency_id' => $this->getCurrency()->id,
                    'currency_rate' => $this->getCurrency()->currencyRate->rate,
                ];
                break;
            default:
                $answer['type'] = static::CALCULATING_TYPE_NUMBER;
                break;
        }

        return $answer;
    }
}