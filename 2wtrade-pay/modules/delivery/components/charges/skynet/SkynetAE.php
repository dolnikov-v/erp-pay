<?php

namespace app\modules\delivery\components\charges\skynet;


use app\models\Currency;
use app\modules\delivery\widgets\charges\helpers\CommonDeliveryCharges;
use yii\helpers\ArrayHelper;

/**
 * Class SkynetAE
 * @package app\modules\delivery\components\charges\skynet
 */
class SkynetAE extends Skynet
{
    /**
     * @param $order
     * @return mixed
     */
    protected function getDeliveryCost($order)
    {
        $weight = $this->getWeightOfParcel($order, true);
        $addKgs = max(0, $weight - 5);
        return doubleval($this->delivery_fee_500gr) + $addKgs * doubleval($this->delivery_fee_add_kg);
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @param $onlyView
     * @return string
     */
    public function render($form, $onlyView = false)
    {
        return CommonDeliveryCharges::widget([
            'form' => $form,
            'model' => $this,
            'items' => [
                [
                    'delivery_fee_500gr',
                    'options' => [
                        'disabled' => $onlyView
                    ],
                ],
                [
                    'delivery_fee_add_kg',
                    'options' => [
                        'disabled' => $onlyView
                    ],
                ],
                [
                    'length' => 6
                ],
                [
                    'cod_fee',
                    'options' => [
                        'disabled' => $onlyView
                    ],
                ],
                [
                    'vat_percent',
                    'options' => [
                        'disabled' => $onlyView
                    ],
                ],
                [
                    'attribute' => 'currency_char_code',
                    'type' => 'select2List',
                    'items' => ArrayHelper::map(Currency::find()->all(), 'char_code', 'name'),
                    'options' => [
                        'length' => false,
                        'disabled' => $onlyView,
                    ]
                ],
            ]
        ]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'delivery_fee_500gr' => \Yii::t('common', 'Стоимость доставки (до 5кг.)'),
        ]);
    }
}