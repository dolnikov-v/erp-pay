<?php

namespace app\modules\delivery\components\charges\skynet;

use app\models\Currency;
use app\modules\delivery\components\charges\ChargesCalculatorAbstract;
use app\modules\delivery\widgets\charges\helpers\CommonDeliveryCharges;
use app\modules\order\models\OrderFinancePrediction;
use yii\helpers\ArrayHelper;

/**
 * Class Skynet
 * @package app\modules\delivery\components\charges\skynet
 */
class Skynet extends ChargesCalculatorAbstract
{
    public $delivery_fee_500gr;
    public $delivery_fee_add_500gr;
    public $delivery_fee_add_kg;
    public $cod_fee;
    public $vat_percent;

    public $vat_type = self::VAT_TYPE_SERVICE;

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['delivery_fee_500gr', 'delivery_fee_add_500gr', 'delivery_fee_add_kg'], 'number', 'min' => 0],
            [['cod_fee', 'vat_percent'], 'number', 'min' => 0, 'max' => 100],
            ['currency_char_code', 'string'],
        ]);
    }

    /**
     * @param \app\modules\order\models\Order $order
     * @return OrderFinancePrediction
     */
    public function calculate($order)
    {
        $finance = $this->getOrderFinance($order);

        $finance->price_delivery = $this->getDeliveryCost($order);
        $currency = $this->getCurrency();
        $currencyId = null;
        if (isset($currency)) {
            $currencyId = $currency->id;
        }

        if ($this->isOrderBuyout($order)) {
            $finance->price_cod = $order->delivery + $order->price_total;
            $finance->price_cod_service = $finance->price_cod * (doubleval($this->cod_fee) / 100);
        } else {
            $finance->price_cod = 0;
            $finance->price_cod_service = 0;
        }
        $finance->price_vat = $finance->price_delivery * (doubleval($this->vat_percent) / 100);

        $finance->price_delivery_currency_id = $finance->price_vat_currency_id = $currencyId;

        return parent::afterCalculate($finance);
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @param $onlyView
     * @return string
     */
    public function render($form, $onlyView = false)
    {
        return CommonDeliveryCharges::widget([
            'form' => $form,
            'model' => $this,
            'items' => [
                [
                    'delivery_fee_500gr',
                    'options' => [
                        'disabled' => $onlyView
                    ],
                ],
                [
                    'delivery_fee_add_500gr',
                    'options' => [
                        'disabled' => $onlyView
                    ],
                ],
                [
                    'delivery_fee_add_kg',
                    'options' => [
                        'disabled' => $onlyView
                    ],
                ],
                [
                    'length' => 3
                ],
                [
                    'cod_fee',
                    'options' => [
                        'disabled' => $onlyView
                    ],
                ],
                [
                    'vat_percent',
                    'options' => [
                        'disabled' => $onlyView
                    ],
                ],
                [
                    'attribute' => 'currency_char_code',
                    'type' => 'select2List',
                    'items' => ArrayHelper::map(Currency::find()->all(), 'char_code', 'name'),
                    'options' => [
                        'length' => false,
                        'disabled' => $onlyView
                    ]
                ],
            ]
        ]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'delivery_fee_500gr' => \Yii::t('common', 'Стоимость доставки (до 500гр.)'),
            'delivery_fee_add_500gr' => \Yii::t('common', 'Стоимость доставки (за кажд. доп. 500гр.)'),
            'delivery_fee_add_kg' => \Yii::t('common', 'Стоимость доставки (за 1кг при весе свыше 5кг.)'),
        ]);
    }

    /**
     * @param $order
     * @return mixed
     */
    protected function getDeliveryCost($order)
    {
        $weight = $this->getWeightOfParcel($order);
        $parts = ceil(min(4500, max(0, $weight - 500)) / 500);
        return doubleval($this->delivery_fee_500gr) + doubleval($this->delivery_fee_add_500gr) * $parts + ceil(max(0, $weight - 5000) / 1000) * doubleval($this->delivery_fee_add_kg);
    }

    /**
     * @param $field
     * @return array
     */
    public function getCalculatingTypeForField($field)
    {
        $answer = [];
        switch ($field) {
            case OrderFinancePrediction::COLUMN_PRICE_COD_SERVICE:
                $answer = [
                    'type' => static::CALCULATING_TYPE_PERCENT,
                    'visible_formula' => "$this->cod_fee%",
                    'fields' => [
                        OrderFinancePrediction::COLUMN_PRICE_COD,
                    ],
                ];
                break;
            case OrderFinancePrediction::COLUMN_PRICE_VAT:
                $answer['type'] = static::CALCULATING_TYPE_PERCENT;
                $answer['visible_formula'] = "$this->vat_percent%";
                if ($this->vat_type == static::VAT_TYPE_COD) {
                    $answer['fields'] = [
                        OrderFinancePrediction::COLUMN_PRICE_COD,
                    ];
                } else {
                    $answer['fields'] = [
                        OrderFinancePrediction::COLUMN_PRICE_DELIVERY,
                        OrderFinancePrediction::COLUMN_PRICE_DELIVERY_RETURN,
                        OrderFinancePrediction::COLUMN_PRICE_STORAGE,
                        OrderFinancePrediction::COLUMN_PRICE_FULFILMENT,
                        OrderFinancePrediction::COLUMN_PRICE_PACKAGE,
                        OrderFinancePrediction::COLUMN_PRICE_PACKING,
                        OrderFinancePrediction::COLUMN_PRICE_DELIVERY_BACK,
                        OrderFinancePrediction::COLUMN_PRICE_REDELIVERY,
                        OrderFinancePrediction::COLUMN_PRICE_ADDRESS_CORRECTION,
                    ];
                }
                break;
            default:
                $answer['type'] = static::CALCULATING_TYPE_NUMBER;
                break;
        }

        return $answer;
    }
}