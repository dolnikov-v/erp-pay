<?php

namespace app\modules\delivery\components\charges;

use app\modules\delivery\models\DeliveryContract;
use yii\base\InvalidParamException;


/**
 * Class ChargesCalculatorFactory
 * @package app\modules\delivery\components\charges
 */
class ChargesCalculatorFactory
{
    /**
     * @param DeliveryContract $deliveryContract
     * @return ChargesCalculatorAbstract
     */
    public static function build($deliveryContract)
    {
        if (!$deliveryContract->chargesCalculatorModel) {
            throw new InvalidParamException(\Yii::t('common', 'Не указан шаблон для расчета расходов на доставку.'));
        }

        if (!class_exists($deliveryContract->chargesCalculatorModel->class_path)) {
            throw new InvalidParamException(\Yii::t('common', 'Неверно указан путь до класса'));
        }
        $className = $deliveryContract->chargesCalculatorModel->class_path;
        $calculatorClass = new $className([
            'delivery' => $deliveryContract->delivery,
            'deliveryContract' => $deliveryContract
        ]);
        return $calculatorClass;
    }
}