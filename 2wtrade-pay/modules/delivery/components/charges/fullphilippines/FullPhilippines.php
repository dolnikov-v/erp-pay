<?php

namespace app\modules\delivery\components\charges\fullphilippines;

use app\models\Currency;
use app\modules\delivery\components\charges\OnlyDeliveryCharge;
use app\modules\delivery\widgets\charges\helpers\CommonDeliveryCharges;
use app\modules\order\models\OrderFinancePrediction;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class FullPhilippines
 * @package app\modules\delivery\components\charges\fullphilippines
 */
class FullPhilippines extends OnlyDeliveryCharge
{
    public $cod_fee_min = 0;

    /**
     * @return array
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['cod_fee_min'], 'number', 'min' => 0],
        ]);
    }

    /**
     * @param \app\modules\order\models\Order $order
     * @return OrderFinancePrediction
     */
    public function calculate($order)
    {
        $rate = $this->getCurrencyRate();
        $currency = $this->getCurrency();
        $currencyId = null;
        if ($currency) {
            $currencyId = $currency->id;
        }
        $finance = parent::calculate($order);
        if ($this->isOrderBuyout($order)) {
            $item2 = $finance->price_cod * (doubleval($this->cod_fee) / 100);
            if ((doubleval($this->cod_fee_min) * $rate) > $item2) {
                $finance->price_cod_service = doubleval($this->cod_fee_min);
                $finance->price_cod_service_currency_id = $currencyId;
                $finance->price_vat = round(($finance->price_fulfilment + $finance->price_delivery_return + $finance->price_cod_service) * (doubleval($this->vat_percent) / 100));
            } else {
                $finance->price_cod_service = $item2;
                $finance->price_vat = round(($finance->price_fulfilment + $finance->price_delivery_return + ($finance->price_cod_service / $rate)) * (doubleval($this->vat_percent) / 100));
            }
        } else {
            $finance->price_vat = round(($finance->price_fulfilment + $finance->price_delivery_return + $finance->price_cod_service) * (doubleval($this->vat_percent) / 100));
        }

        return parent::afterCalculate($finance);
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @param $onlyView
     * @return string
     */
    public function render($form, $onlyView = false)
    {
        return CommonDeliveryCharges::widget([
            'form' => $form,
            'model' => $this,
            'items' => [
                [
                    'attribute' => 'delivery_fee',
                    'options' => [
                        'disabled' => $onlyView,
                    ]
                ],
                [
                    'attribute' => 'return_fee',
                    'options' => [
                        'disabled' => $onlyView,
                    ]
                ],
                [
                    'attribute' => 'fulfillment_fee',
                    'options' => [
                        'disabled' => $onlyView,
                    ]
                ],
                [
                    'attribute' => 'cod_fee',
                    'options' => [
                        'disabled' => $onlyView,
                    ]
                ],
                [
                    'attribute' => 'cod_fee_min',
                    'options' => [
                        'disabled' => $onlyView,
                    ]
                ],
                [
                    'attribute' => 'vat_percent',
                    'options' => [
                        'disabled' => $onlyView,
                    ]
                ],
                [
                    'attribute' => 'currency_char_code',
                    'type' => 'select2List',
                    'items' => ArrayHelper::map(Currency::find()->all(), 'char_code', 'name'),
                    'length' => 2,
                    'options' => [
                        'length' => false,
                        'disabled' => $onlyView,
                    ]
                ]
            ]
        ]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'cod_fee_min' => Yii::t('common', 'Минимальная плата за наложенный платеж'),
        ]);
    }


    /**
     * @param $field
     * @return array
     */
    public function getCalculatingTypeForField($field)
    {
        $answer = [];
        $currency = $this->getCurrency();
        switch ($field) {
            case OrderFinancePrediction::COLUMN_PRICE_COD_SERVICE:
                $answer = [
                    'type' => static::CALCULATING_TYPE_PERCENT,
                    'visible_formula' => Yii::t('common', 'МИН({val1}, {val2}', [
                        'val1' => "{$this->cod_fee}%",
                        'val2' => "{$this->cod_fee_min} {$currency->char_code}"
                    ]),
                    'fields' => [
                        OrderFinancePrediction::COLUMN_PRICE_COD,
                    ],
                ];
                break;
            case OrderFinancePrediction::COLUMN_PRICE_VAT:
                $answer['type'] = static::CALCULATING_TYPE_PERCENT;
                $answer['visible_formula'] = "$this->vat_percent%";
                $answer['fields'] = [
                    OrderFinancePrediction::COLUMN_PRICE_COD_SERVICE,
                    OrderFinancePrediction::COLUMN_PRICE_FULFILMENT,
                    OrderFinancePrediction::COLUMN_PRICE_DELIVERY_RETURN,
                ];
                break;
            default:
                $answer['type'] = static::CALCULATING_TYPE_NUMBER;
                break;
        }

        return $answer;
    }
}