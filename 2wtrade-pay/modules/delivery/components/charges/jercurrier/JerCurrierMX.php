<?php

namespace app\modules\delivery\components\charges\jercurrier;


use app\models\Currency;
use app\modules\delivery\components\charges\ChargesCalculatorAbstract;
use app\modules\delivery\widgets\charges\helpers\CommonDeliveryCharges;
use app\modules\order\models\OrderFinancePrediction;
use yii\helpers\ArrayHelper;

/**
 * Class JerCurrierMX
 * @package app\modules\delivery\components\charges\jercurrier
 */
class JerCurrierMX extends ChargesCalculatorAbstract
{
    public $return_fee;
    public $delivery_buyout_rate_0;
    public $delivery_buyout_rate_61;
    public $delivery_buyout_rate_65;
    public $delivery_buyout_rate_70;
    public $vat_included = 1;
    public $currency_char_code = 'MXN';

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [
                [
                    'return_fee',
                    'delivery_buyout_rate_0',
                    'delivery_buyout_rate_61',
                    'delivery_buyout_rate_65',
                    'delivery_buyout_rate_70'
                ],
                'number',
                'min' => 0
            ],
            [['currency_char_code'], 'string'],
        ]);
    }

    /**
     * @param \app\modules\order\models\Order $order
     * @return \app\modules\order\models\OrderFinancePrediction
     */
    public function calculate($order)
    {
        $buyoutByPeriods = $this->getBuyoutOrdersCountByMonths();
        $sentByPeriods = $this->getOrdersCountByMonths();
        $month = $this->getOrderSendingMonth($order);

        $finance = $this->getOrderFinance($order);
        $currency = $this->getCurrency();
        $currencyId = null;
        if (isset($currency)) {
            $currencyId = $currency->id;
        }

        $delivery_fee = $this->delivery_buyout_rate_0;
        if (isset($buyoutByPeriods[$month]) && isset($sentByPeriods[$month]) && $sentByPeriods[$month] > 0) {
            $rate = ceil($buyoutByPeriods[$month] / $sentByPeriods[$month]);
            if ($rate > 61 && $rate <= 64) {
                $delivery_fee = $this->delivery_buyout_rate_61;
            }
            if ($rate > 65 && $rate <= 69) {
                $delivery_fee = $this->delivery_buyout_rate_65;
            }
            if ($rate >= 70) {
                $delivery_fee = $this->delivery_buyout_rate_70;
            }
        }
        $finance->price_delivery = doubleval($delivery_fee);
        if ($this->isOrderBuyout($order)) {
            $finance->price_cod = $order->delivery + $order->price_total;
        } else {
            if ($this->isOrderReturnToSender($order)) {
                $finance->price_delivery_return = doubleval($this->return_fee);
            }
            $finance->price_cod = 0;
        }

        $finance->price_delivery_currency_id = $finance->price_delivery_return_currency_id = $currencyId;

        return parent::afterCalculate($finance);
    }


    /**
     * @param \app\components\widgets\ActiveForm $form
     * @param bool $onlyView
     * @return string
     * @throws \Exception
     */
    public function render($form, $onlyView = false)
    {
        return CommonDeliveryCharges::widget([
            'form' => $form,
            'model' => $this,
            'items' => [
                [
                    'attribute' => 'currency_char_code',
                    'type' => 'select2List',
                    'items' => ArrayHelper::map(Currency::find()->all(), 'char_code', 'name'),
                    'options' => [
                        'length' => false,
                        'disabled' => $onlyView,
                    ]
                ],
                [
                    'attribute' => 'return_fee',
                    'options' => [
                        'disabled' => $onlyView
                    ],
                ],
                [
                    'attribute' => 'delivery_buyout_rate_0',
                    'options' => [
                        'disabled' => $onlyView
                    ],
                ],
                [
                    'attribute' => 'delivery_buyout_rate_61',
                    'options' => [
                        'disabled' => $onlyView
                    ],
                ],
                [
                    'attribute' => 'delivery_buyout_rate_65',
                    'options' => [
                        'disabled' => $onlyView
                    ],
                ],
                [
                    'attribute' => 'delivery_buyout_rate_70',
                    'options' => [
                        'disabled' => $onlyView
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'return_fee' => \Yii::t('common', 'Стоимость возврата'),
            'delivery_buyout_rate_0' => \Yii::t('common', 'Стоимость доставки при выкупе менее 60%'),
            'delivery_buyout_rate_61' => \Yii::t('common', 'Стоимость доставки при выкупе 61-64%'),
            'delivery_buyout_rate_65' => \Yii::t('common', 'Стоимость доставки при выкупе 65-69%'),
            'delivery_buyout_rate_70' => \Yii::t('common', 'Стоимость доставки при выкупе выше 70%'),
        ]);
    }
}