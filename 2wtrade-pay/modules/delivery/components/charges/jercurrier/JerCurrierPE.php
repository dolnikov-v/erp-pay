<?php

namespace app\modules\delivery\components\charges\jercurrier;

use app\models\Currency;
use app\modules\catalog\models\UnBuyoutReason;
use app\modules\delivery\components\charges\ChargesCalculatorAbstract;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\delivery\widgets\charges\helpers\CommonDeliveryCharges;
use app\modules\delivery\widgets\charges\helpers\DeliveryChargesArrayProperty;
use app\modules\delivery\widgets\charges\helpers\DeliveryChargesForRegion;
use app\modules\order\models\Order;
use app\modules\order\models\OrderFinancePrediction;
use app\modules\order\models\OrderStatus;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class JerCurrierPE
 * @package app\modules\delivery\components\charges
 */
class JerCurrierPE extends ChargesCalculatorAbstract
{
    /**
     * @var array
     */
    public $zipcodes = [];

    /**
     * @var array
     */
    public $delivery_fees = [];

    /**
     * @var array
     */
    public $delivery_fees_sunday = [];

    /**
     * @var array
     */
    public $return_fees = [];

    /**
     * @var array
     */
    public $return_fees_sunday = [];

    /**
     * @var int
     */
    public $cod_fee = 0;

    /**
     * @var array
     */
    public $cod_fees = [];

    /**
     * @var int
     */
    public $vat_percent = 0;

    /**
     * @var int
     */
    public $fulfillment_fee = 0;

    /**
     * @var int
     */
    public $packing_fee = 0;

    /**
     * @var int
     */
    public $packed_fee = 0;

    /**
     * @var int
     */
    public $storage_fee = 0;

    /**
     * @var int
     */
    public $storage_area = 0;

    /**
     * @var array
     */
    public $unbuyout_reasons = [];

    /**
     * @var array
     */
    public $price_for_reasons = [];

    /**
     * @var array
     */
    public $delivery_fees_array = [];

    /**
     * @var array
     */
    public $percent_buyout_from = [];

    /**
     * @var array
     */
    public $percent_buyout_to = [];

    /**
     * @var array
     */
    public $enable_percent_buyout = [];

    /**
     * @var array
     */
    protected $statistic = [];

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            ['zipcodes', 'each', 'rule' => ['string']],
            ['zipcodes', 'each', 'rule' => ['filter', 'filter' => [$this, 'filterZipcodes']]],
            [
                ['delivery_fees', 'delivery_fees_sunday', 'return_fees', 'price_for_reasons', 'return_fees_sunday'],
                'each',
                'rule' => ['number', 'min' => 0]
            ],
            [['currency_char_code', 'vat_type'], 'string'],
            [['cod_fee', 'vat_percent'], 'number', 'min' => 0, 'max' => 100],
            [['cod_fees'], 'each', 'rule' => ['number', 'min' => 0, 'max' => 100]],
            [['fulfillment_fee', 'packing_fee', 'packed_fee', 'storage_fee', 'storage_area'], 'number', 'min' => 0],
            ['unbuyout_reasons', 'each', 'rule' => ['each', 'rule' => ['number']]],
            [['price_for_reasons', 'unbuyout_reasons'], 'clearDefault'],
            [['price_for_reasons'], 'checkFullRow'],
            [['enable_percent_buyout'], 'each', 'rule' => ['boolean']],
            [['enable_percent_buyout'], 'filter', 'filter' => [$this, 'arrayCheckboxClean']],
            [['enable_percent_buyout'], 'overlap', 'skipOnEmpty' => false, 'skipOnError' => false],
            [
                [
                    'delivery_fees_array',
                    'percent_buyout_from',
                    'percent_buyout_to'
                ],
                'each',
                'rule' => ['each', 'rule' => ['number', 'min' => 0]]
            ],
            [
                [
                    'delivery_fees_array',
                    'percent_buyout_from',
                    'percent_buyout_to'
                ],
                'validateArrayRequired',
                'skipOnEmpty' => false,
                'skipOnError' => false
            ],
            [
                ['percent_buyout_from', 'percent_buyout_to'],
                'validateArrayPercent',
                'skipOnEmpty' => false,
                'skipOnError' => false
            ],
        ]);
    }

    /**
     * Проверка на пересечение дат внутри региона
     * @param $attribute
     */
    public function overlap($attribute)
    {
        foreach ($this->delivery_fees_array as $region => $items) {
            if ($this->enable_percent_buyout[$region]) {
                foreach ($items as $key => $val) {
                    if ($this->percent_buyout_from[$region][$key] >= $this->percent_buyout_to[$region][$key]) {
                        $this->addError($attribute, Yii::t('common', 'Диапозоны процентов неверен.'));
                    }
                    for ($i = $key + 1; $i <= count($this->percent_buyout_from[$region]); $i++) {
                        if (!($this->percent_buyout_to[$region][$key] <= $this->percent_buyout_from[$region][$i] || $this->percent_buyout_from[$region][$key] >= $this->percent_buyout_to[$region][$i])) {
                            $this->addError($attribute, Yii::t('common', 'Диапозоны процентов пересекаются.'));
                        }
                    }
                }
            }
        }
    }

    /**
     * Удаляем дефолтные 0 если checked для соблюдения индексов
     * @param $values
     * @return array
     */
    public function arrayCheckboxClean($values)
    {
        $result = [];
        foreach ($values as $key => $val) {
            if (empty($values[$key + 1])) {
                $result[] = $val;
            }
        }
        return $result;
    }

    /**
     * @param $attribute
     */
    public function validateArrayRequired($attribute)
    {
        foreach ($this->$attribute as $key => $items) {
            if ($this->enable_percent_buyout[$key] && in_array('', $items)) {
                $this->addError($attribute, Yii::t('common', 'Поле «{attribute}» обязательно к заполнению.', ['attribute' => $this->attributeLabels()[$attribute] ?? $attribute]));
            }
        }
    }

    /**
     * @param $attribute
     */
    public function validateArrayPercent($attribute)
    {
        foreach ($this->$attribute as $key => $items) {
            if ($this->enable_percent_buyout[$key]) {
                foreach ($items as $val) {
                    if ($val < 0) {
                        $this->addError($attribute, Yii::t('common', 'Значение «{attribute}» должно быть не меньше 0.', ['attribute' => $this->attributeLabels()[$attribute] ?? $attribute]));
                    }
                    if ($val > 100) {
                        $this->addError($attribute, Yii::t('common', 'Значение «{attribute}» не должно превышать 100.', ['attribute' => $this->attributeLabels()[$attribute] ?? $attribute]));
                    }
                }
            }
        }
    }

    /**
     * @param string $attribute
     */
    public function checkFullRow(string $attribute)
    {
        $checkAttr = ['unbuyout_reasons'];
        foreach ($this->{$attribute} as $key => $value) {
            foreach ($checkAttr as $attr) {
                if (empty($this->$attr[$key])) {
                    $this->addError($attribute, Yii::t('common', 'Поле «{attribute}» не может быть пустым.', ['attribute' => $this->attributeLabels()[$attr] ?? $attr]));
                }
            }
        }
    }

    /**
     * @param Order $order
     * @return float|null
     */
    private function getPercentDelivery(Order $order)
    {
        $dateTime = (new \DateTime())->setTimestamp($order->delivery_time_from ?? $order->deliveryRequest->approved_at ?? $order->deliveryRequest->sent_at ?? time())
            ->setTimezone(new \DateTimeZone($order->country->timezone->timezone_id))
            ->modify('midnight');
        $dateOrder = $dateTime->format('Y-m-d');
        if (empty($this->statistic[$dateOrder])) {
            $query = Order::find()
                ->innerJoinWith(['deliveryRequest'])
                ->where([DeliveryRequest::tableName() . '.delivery_id' => $this->delivery->id]);
            $query->andWhere([
                '>=',
                'COALESCE(' . Order::tableName() . '.delivery_time_from, ' . DeliveryRequest::tableName() . '.approved_at)',
                $dateTime->getTimestamp()
            ]);
            $query->andWhere([
                '<',
                'COALESCE(' . Order::tableName() . '.delivery_time_from, ' . DeliveryRequest::tableName() . '.approved_at)',
                $dateTime->modify('tomorrow')->getTimestamp()
            ]);
            $allOrders = $query->count();
            $percent = 0;
            if ($allOrders > 0) {
                $query->andWhere([
                    Order::tableName() . '.status_id' => [
                        OrderStatus::STATUS_DELIVERY_BUYOUT,
                        OrderStatus::STATUS_FINANCE_MONEY_RECEIVED,
                    ]
                ]);
                $percent = ($query->count() / $allOrders) * 100;
            }
            $this->statistic[$dateOrder] = $percent;
        }
        return $this->statistic[$dateOrder] ?? 0;
    }

    /**
     * @param \app\modules\order\models\Order $order
     * @return OrderFinancePrediction
     */
    public function calculate($order)
    {
        $finance = $this->getOrderFinance($order);
        $delivery_fee = 0;
        $return_fee = 0;
        $cod_fee = $this->cod_fee;
        $index = $this->getIndexByZipcode($this->zipcodes, $order->customer_zip);
        if (!is_null($index)) {
            $dayOfWeek = null;
            if ($dateBuyout = $order->delivery_time_from ?? $order->deliveryRequest->approved_at ?? $order->deliveryRequest->sent_at ?? null) {
                $dayOfWeek = (new \DateTime())->setTimezone(new \DateTimeZone($order->country->timezone->timezone_id))
                    ->setTimestamp($dateBuyout)
                    ->format('N');
            }
            if ($dayOfWeek && $dayOfWeek == 7) {
                $deliverySunday = doubleval($this->delivery_fees_sunday[$index]);
                $returnFeeSunday = isset($this->return_fees_sunday[$index]) ? doubleval($this->return_fees_sunday[$index]) : null;
                $percentBuyout = $this->getPercentDelivery($order);
                foreach ($this->percent_buyout_from[$index] as $key => $value) {
                    if (doubleval($value) < $percentBuyout && $percentBuyout >= doubleval($this->percent_buyout_to[$index])) {
                        $deliverySunday = doubleval($this->delivery_fees_array[$index][$key]);
                        break;
                    }
                }
            }
            if (isset($deliverySunday)) {
                $delivery_fee = $deliverySunday;
            } elseif (isset($this->delivery_fees[$index])) {
                $delivery_fee = doubleval($this->delivery_fees[$index]);
            } else {
                $delivery_fee = 0;
            }
            if (isset($returnFeeSunday)) {
                $return_fee = $returnFeeSunday;
            } elseif (isset($this->return_fees[$index])) {
                $return_fee = doubleval($this->return_fees[$index]);
            } else {
                $return_fee = 0;
            }
            if (isset($this->cod_fees[$index])) {
                $cod_fee = doubleval($this->cod_fees[$index]);
            }
        }

        $currency = $this->getCurrency();
        $currencyId = null;
        if (isset($currency)) {
            $currencyId = $currency->id;
        }

        $finance->price_storage = 0;
        $finance->price_delivery = $delivery_fee;
        $finance->price_fulfilment = doubleval($this->fulfillment_fee);
        $finance->price_packing = doubleval($this->packing_fee);
        $finance->price_package = doubleval($this->packed_fee);
        $finance->price_delivery_return = 0;
        if ($this->isOrderBuyout($order)) {
            $finance->price_cod = $order->delivery + $order->price_total;
            $finance->price_cod_service = $finance->price_cod * (doubleval($cod_fee) / 100);
            if ($this->vat_type == static::VAT_TYPE_COD) {
                $finance->price_vat = $finance->price_cod * (doubleval($this->vat_percent) / 100);
            }
        } else {
            if ($this->isOrderReturnToSender($order)) {
                $finance->price_delivery = 0;
                $finance->price_delivery_return = doubleval($return_fee);
                if ($reasonID = $order->deliveryRequest->unBuyoutReasonMapping->reason_id) {
                    foreach ($this->unbuyout_reasons as $key => $reasonIDs) {
                        if (in_array($reasonID, $reasonIDs) && isset($this->price_for_reasons[$key])) {
                            $finance->price_delivery_return = doubleval($this->price_for_reasons[$key]);
                            break;
                        }
                    }
                }
            }
            $finance->price_cod = 0;
            $finance->price_cod_service = 0;
            if ($this->vat_type == static::VAT_TYPE_COD) {
                $finance->price_vat = 0;
            }
        }
        if ($this->vat_type == static::VAT_TYPE_SERVICE) {
            $chargesSum = $finance->price_delivery + $finance->price_delivery_return + $finance->price_storage + $finance->price_fulfilment + $finance->price_packing + $finance->price_package;
            $finance->price_vat = $chargesSum * (doubleval($this->vat_percent) / 100);
            $finance->price_vat_currency_id = $currencyId;
        }

        $finance->price_delivery_currency_id = $finance->price_fulfilment_currency_id = $finance->price_packing_currency_id = $finance->price_package_currency_id = $finance->price_storage_currency_id = $currencyId;

        return parent::afterCalculate($finance);
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @param bool $onlyView
     * @return string
     * @throws \Exception
     */
    public function render($form, $onlyView = false)
    {
        return CommonDeliveryCharges::widget([
                'form' => $form,
                'model' => $this,
                'items' => [
                    [
                        'attribute' => 'fulfillment_fee',
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'packing_fee',
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'packed_fee',
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'cod_fee',
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'vat_percent',
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'vat_type',
                        'type' => 'select2List',
                        'items' => static::getVatTypes(),
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'currency_char_code',
                        'type' => 'select2List',
                        'items' => ArrayHelper::map(Currency::find()->all(), 'char_code', 'name'),
                        'length' => 2,
                        'options' => [
                            'length' => false,
                            'disabled' => $onlyView
                        ]
                    ],
                    [
                        'length' => 3,
                    ],
                    [
                        'attribute' => 'storage_area',
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'storage_fee',
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                ]
            ])
            . DeliveryChargesArrayProperty::widget([
                'tabName' => Yii::t('common', 'Стоимость возврата по причинам невыкупа'),
                'form' => $form,
                'model' => $this,
                'onlyView' => $onlyView,
                'arrayProperties' => [
                    [
                        'attribute' => 'unbuyout_reasons',
                        'type' => 'select2ListMultiple',
                        'items' => ArrayHelper::map(UnBuyoutReason::find()->all(),
                            'id', 'name'),
                        'length' => 4,
                        'options' => [
                            'disabled' => $onlyView,
                        ],
                    ],
                    [
                        'attribute' => 'price_for_reasons',
                    ],
                ]
            ])
            . DeliveryChargesForRegion::widget([
                'form' => $form,
                'model' => $this,
                'onlyView' => $onlyView,
                'regionAttribute' => 'zipcodes',
                'chargesAttributes' => [
                    'delivery_fees',
                    'delivery_fees_sunday',
                    'return_fees_sunday',
                    'return_fees',
                    'cod_fees',
                    'enable_percent_buyout',
                ],
                'chargesArrayAttributes' => [
                    [
                        'attribute' => 'delivery_fees_array',
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'percent_buyout_from',
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'percent_buyout_to',
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                ]
            ]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'return_fees' => Yii::t('common', 'Стоимость возврата'),
            'packing_fee' => Yii::t('common', 'Стоимость упаковывания'),
            'packed_fee' => Yii::t('common', 'Стоимость упаковки'),
            'storage_area' => Yii::t('common', 'Площадь склада (м2)'),
            'storage_fee' => Yii::t('common', 'Стоимость аренды склада (1м2)'),
            'cod_fees' => Yii::t('common', 'Наложенный платеж (%)'),
            'unbuyout_reasons' => Yii::t('common', 'Причины невыкупа'),
            'price_for_reasons' => Yii::t('common', 'Стоимость возврата'),
            'delivery_fees_sunday' => Yii::t('common', 'Стоимость доставки в вс.'),
            'delivery_fees_array' => Yii::t('common', 'Стоимость доставки в вс.'),
            'percent_buyout_from' => Yii::t('common', '% выкупа от (>)'),
            'percent_buyout_to' => Yii::t('common', '% выкупа до (<=)'),
            'enable_percent_buyout' => Yii::t('common', 'Учитывать % выкупа в воскресенье'),
            'return_fees_sunday' => Yii::t('common', 'Стоимость возврата в вс.'),
        ]);
    }

    /**
     * @param $field
     * @return array
     */
    public function getCalculatingTypeForField($field)
    {
        $answer = [];
        switch ($field) {
            case OrderFinancePrediction::COLUMN_PRICE_COD_SERVICE:
                $answer = [
                    'type' => static::CALCULATING_TYPE_PERCENT,
                    'visible_formula' => "$this->cod_fee%",
                    'fields' => [
                        OrderFinancePrediction::COLUMN_PRICE_COD,
                    ],
                ];
                break;
            case OrderFinancePrediction::COLUMN_PRICE_VAT:
                $answer['type'] = static::CALCULATING_TYPE_PERCENT;
                $answer['visible_formula'] = "$this->vat_percent%";
                if ($this->vat_type == static::VAT_TYPE_COD) {
                    $answer['fields'] = [
                        OrderFinancePrediction::COLUMN_PRICE_COD,
                    ];
                } else {
                    $answer['fields'] = [
                        OrderFinancePrediction::COLUMN_PRICE_DELIVERY,
                        OrderFinancePrediction::COLUMN_PRICE_DELIVERY_RETURN,
                        OrderFinancePrediction::COLUMN_PRICE_STORAGE,
                        OrderFinancePrediction::COLUMN_PRICE_FULFILMENT,
                        OrderFinancePrediction::COLUMN_PRICE_PACKAGE,
                        OrderFinancePrediction::COLUMN_PRICE_PACKING,
                        OrderFinancePrediction::COLUMN_PRICE_DELIVERY_BACK,
                        OrderFinancePrediction::COLUMN_PRICE_REDELIVERY,
                        OrderFinancePrediction::COLUMN_PRICE_ADDRESS_CORRECTION,
                    ];
                }
                break;
            case OrderFinancePrediction::COLUMN_PRICE_STORAGE:
                $answer = [
                    'type' => ChargesCalculatorAbstract::CALCULATING_TYPE_MONTHLY_CHARGE,
                    'charge' => doubleval($this->storage_fee) * doubleval($this->storage_area),
                    'currency_id' => $this->getCurrency()->id,
                    'currency_rate' => $this->getCurrency()->currencyRate->rate,
                ];
                break;
            default:
                $answer['type'] = static::CALCULATING_TYPE_NUMBER;
                break;
        }

        return $answer;
    }

    /**
     * @inheritdoc
     */
    public function calculateForBatch($query)
    {
        $this->statistic = [];
        parent::calculateForBatch($query);
    }
}