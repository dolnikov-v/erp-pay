<?php

namespace app\modules\delivery\components\charges;

use app\models\Currency;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\delivery\widgets\charges\helpers\CommonDeliveryCharges;
use app\modules\delivery\widgets\charges\helpers\DeliveryChargesForRegion;
use app\modules\order\models\Order;
use app\modules\order\models\OrderFinancePrediction;
use app\modules\order\models\OrderStatus;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class DeliveryChargeByZip
 * @package app\modules\delivery\components\charges
 */
class DeliveryChargeByZip extends ChargesCalculatorAbstract
{
    /**
     * @var array
     */
    public $zipcodes = [];

    /**
     * @var array
     */
    public $delivery_fees = [];

    /**
     * @var array
     */
    public $delivery_fees_array = [];

    /**
     * @var array
     */
    public $return_fees = [];

    /**
     * @var array
     */
    public $return_fees_array = [];

    /**
     * @var array
     */
    public $percent_buyout_from = [];

    /**
     * @var array
     */
    public $percent_buyout_to = [];

    /**
     * @var array
     */
    public $day_from = [];

    /**
     * @var array
     */
    public $day_to = [];

    /**
     * @var array
     */
    public $enable_percent_buyout = [];

    /**
     * @var int
     */
    public $cod_fee = 0;

    /**
     * @var array
     */
    public $cod_fees = [];

    /**
     * @var array
     */
    public $cod_fees_array = [];

    /**
     * @var int
     */
    public $vat_percent = 0;

    /**
     * @var int
     */
    public $fulfillment_fee = 0;

    /**
     * @var int
     */
    public $packing_fee = 0;

    /**
     * @var int
     */
    public $packed_fee = 0;

    /**
     * @var int
     */
    public $storage_fee = 0;

    /**
     * @var int
     */
    public $storage_area = 0;

    /**
     * @var array
     */
    private $statistic = [];

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            ['zipcodes', 'each', 'rule' => ['string']],
            ['zipcodes', 'each', 'rule' => ['filter', 'filter' => [$this, 'filterZipcodes']]],
            [['delivery_fees', 'return_fees'], 'each', 'rule' => ['number', 'min' => 0]],
            [['cod_fees'], 'each', 'rule' => ['number', 'min' => 0, 'max' => 100]],
            [['currency_char_code', 'vat_type'], 'string'],
            [['enable_percent_buyout'], 'each', 'rule' => ['boolean']],
            [['enable_percent_buyout'], 'filter', 'filter' => [$this, 'arrayCheckboxClean']],
            [['day_from', 'day_to'], 'each', 'rule' => ['each', 'rule' => ['integer', 'min' => 1, 'max' => 31]]],
            [['day_from'], 'overlap', 'skipOnEmpty' => false, 'skipOnError' => false],
            [['cod_fee', 'vat_percent'], 'number', 'min' => 0, 'max' => 100],
            [['fulfillment_fee', 'packing_fee', 'packed_fee', 'storage_fee', 'storage_area'], 'number', 'min' => 0],
            [
                [
                    'return_fees_array',
                    'delivery_fees_array',
                    'cod_fees_array',
                    'percent_buyout_from',
                    'percent_buyout_to'
                ],
                'each',
                'rule' => ['each', 'rule' => ['number', 'min' => 0]]
            ],
            [
                [
                    'return_fees_array',
                    'delivery_fees_array',
                    'cod_fees_array',
                    'percent_buyout_from',
                    'percent_buyout_to'
                ],
                'validateArrayRequired',
                'skipOnEmpty' => false,
                'skipOnError' => false
            ],
            [
                ['percent_buyout_from', 'percent_buyout_to'],
                'validateArrayPercent',
                'skipOnEmpty' => false,
                'skipOnError' => false
            ],
        ]);
    }

    /**
     * Проверка на пересечение дат внутри региона
     * @param $attribute
     */
    public function overlap($attribute)
    {
        foreach ($this->$attribute as $region => $items) {
            if ($this->enable_percent_buyout[$region]) {

                $tmp = [];
                foreach ($items as $key => $val) {
                    if ($val > $this->day_to[$region][$key]) {
                        $this->addError($attribute, Yii::t(
                            'common',
                            'Значение «{attribute}» не должно превышать значение «{day_to}».',
                            [
                                'attribute' => $this->attributeLabels()[$attribute] ?? $attribute,
                                'day_to' => $this->attributeLabels()['day_to'] ?? 'day_to',
                            ]));
                    }
                    if ($this->percent_buyout_from[$region][$key] >= $this->percent_buyout_to[$region][$key]) {
                        $this->addError($attribute, Yii::t('common', 'Диапозоны процентов неверен.'));
                    }
                    $tmp[] = [
                        'day_from' => $this->day_from[$region][$key],
                        'day_to' => $this->day_to[$region][$key],
                        'percent_from' => $this->percent_buyout_from[$region][$key],
                        'percent_to' => $this->percent_buyout_to[$region][$key]
                    ];
                }
                foreach ($tmp as $line => $value) {
                    foreach ($tmp as $line2 => $value2) {
                        if ($line != $line2 &&
                            (
                                ($value['day_from'] >= $value2['day_from'] && $value['day_to'] <= $value2['day_to']) ||
                                ($value2['day_from'] >= $value['day_from'] && $value2['day_to'] <= $value['day_to'])
                            ) &&
                            (
                                ($value['percent_from'] >= $value2['percent_from'] && $value['percent_to'] <= $value2['percent_to']) ||
                                ($value2['percent_from'] >= $value['percent_from'] && $value2['percent_to'] <= $value['percent_to'])
                            )
                        ) {
                            $this->addError($attribute, Yii::t('common', 'Диапозоны процентов пересекаются.'));
                            break 2;
                        }
                    }
                }
            }
        }
    }

    /**
     * Удаляем дефолтные 0 если checked для соблюдения индексов
     * @param $values
     * @return array
     */
    public function arrayCheckboxClean($values)
    {
        $result = [];
        foreach ($values as $key => $val) {
            if (empty($values[$key + 1])) {
                $result[] = $val;
            }
        }
        return $result;
    }

    /**
     * @param $attribute
     */
    public function validateArrayRequired($attribute)
    {
        foreach ($this->$attribute as $key => $items) {
            if ($this->enable_percent_buyout[$key] && in_array('', $items)) {
                $this->addError($attribute, Yii::t('common', 'Поле «{attribute}» обязательно к заполнению.', ['attribute' => $this->attributeLabels()[$attribute] ?? $attribute]));
            }
        }
    }

    /**
     * @param $attribute
     */
    public function validateArrayPercent($attribute)
    {
        foreach ($this->$attribute as $key => $items) {
            if ($this->enable_percent_buyout[$key]) {
                foreach ($items as $val) {
                    if ($val < 0) {
                        $this->addError($attribute, Yii::t('common', 'Значение «{attribute}» должно быть не меньше 0.', ['attribute' => $this->attributeLabels()[$attribute] ?? $attribute]));
                    }
                    if ($val > 100) {
                        $this->addError($attribute, Yii::t('common', 'Значение «{attribute}» не должно превышать 100.', ['attribute' => $this->attributeLabels()[$attribute] ?? $attribute]));
                    }
                }
            }
        }
    }

    /**
     * @param int $index
     * @param int $delivery_id
     * @param int|null $date_order
     * @return int|null
     */
    public function getPercentDelivery(int $index, int $delivery_id, int $date_order = null)
    {
        if (!$date_order) {
            return null;
        }

        foreach ($this->day_from[$index] as $key => $day_from) {
            if ($day_from > date('j', $date_order)) {
                continue;
            }
            $day_to = $this->day_to[$index][$key];
            if ($day_to < date('j', $date_order)) {
                continue;
            }

            if (!isset($this->statistic[$delivery_id][date('Y-m', $date_order)][$day_from.'_'.$day_to])) {
                $query = Order::find()
                    ->innerJoinWith(['deliveryRequest'])
                    ->where([DeliveryRequest::tableName() . '.delivery_id' => $delivery_id]);
                $query->andWhere([
                    '>=',
                    'COALESCE(' . DeliveryRequest::tableName() . '.second_sent_at, ' . DeliveryRequest::tableName() . '.sent_at, ' . DeliveryRequest::tableName() . '.created_at)',
                    strtotime('midnight', strtotime(date($day_from . '-m-Y', $date_order)))
                ]);
                $query->andWhere([
                    '<=',
                    'COALESCE(' . DeliveryRequest::tableName() . '.second_sent_at, ' . DeliveryRequest::tableName() . '.sent_at, ' . DeliveryRequest::tableName() . '.created_at)',
                    strtotime('23:59:59', strtotime(date($day_to . '-m-Y', $date_order)))
                ]);
                $allOrders = $query->count();
                $percent = 0;
                if ($allOrders > 0) {
                    $query->andWhere([
                        Order::tableName() . '.status_id' => [
                            OrderStatus::STATUS_DELIVERY_BUYOUT,
                            OrderStatus::STATUS_FINANCE_MONEY_RECEIVED,
                        ]
                    ]);
                    $percent = ($query->count() / $allOrders) * 100;
                }
                $this->statistic[$delivery_id][date('Y-m', $date_order)][$day_from.'_'.$day_to] = $percent;
            }

            $percent = $this->statistic[$delivery_id][date('Y-m', $date_order)][$day_from.'_'.$day_to];

            if ($this->percent_buyout_from[$index][$key] <= $percent && $percent < $this->percent_buyout_to[$index][$key]) {
                return $key;
            }
        }
        return null;
    }

    /**
     * @param \app\modules\order\models\Order $order
     * @return OrderFinancePrediction
     */
    public function calculate($order)
    {
        $delivery = $order->deliveryRequest;
        $finance = $this->getOrderFinance($order);
        $delivery_fee = 0;
        $return_fee = 0;
        $index = $this->getIndexByZipcode($this->zipcodes, $order->customer_zip);
        $cod_fee = $this->cod_fee;
        if (!is_null($index)) {
            if (isset($this->delivery_fees[$index])) {
                $delivery_fee = doubleval($this->delivery_fees[$index]);
            }
            if (isset($this->return_fees[$index])) {
                $return_fee = doubleval($this->return_fees[$index]);
            }
            if (isset($this->cod_fees[$index])) {
                $cod_fee = doubleval($this->cod_fees[$index]);
            }
            if ($this->enable_percent_buyout[$index]) {
                $param_id = $this->getPercentDelivery($index, $this->delivery->id, $delivery['second_sent_at'] ?? $delivery['sent_at'] ?? $delivery['created_at'] ?? null);
                if ($param_id !== null) {
                    if (!empty($this->delivery_fees_array[$index][$param_id])) {
                        $delivery_fee = doubleval($this->delivery_fees_array[$index][$param_id]);
                    }
                    if (!empty($this->return_fees_array[$index][$param_id])) {
                        $return_fee = doubleval($this->return_fees_array[$index][$param_id]);
                    }
                    if (!empty($this->cod_fees_array[$index][$param_id])) {
                        $cod_fee = doubleval($this->cod_fees_array[$index][$param_id]);
                    }
                }
            }
        }

        $currency = $this->getCurrency();
        $currencyId = null;
        if (isset($currency)) {
            $currencyId = $currency->id;
        }

        $finance->price_storage = 0;
        $finance->price_delivery = $delivery_fee;
        $finance->price_fulfilment = doubleval($this->fulfillment_fee);
        $finance->price_packing = doubleval($this->packing_fee);
        $finance->price_package = doubleval($this->packed_fee);
        $finance->price_delivery_return = 0;
        if ($this->isOrderBuyout($order)) {
            $finance->price_cod = $order->delivery + $order->price_total;
            $finance->price_cod_service = $finance->price_cod * (doubleval($cod_fee) / 100);
            if ($this->vat_type == static::VAT_TYPE_COD) {
                $finance->price_vat = $finance->price_cod * (doubleval($this->vat_percent) / 100);
            }
        } else {
            if ($this->isOrderReturnToSender($order)) {
                $finance->price_delivery_return = doubleval($return_fee);
            }
            $finance->price_cod = 0;
            $finance->price_cod_service = 0;
            if ($this->vat_type == static::VAT_TYPE_COD) {
                $finance->price_vat = 0;
            }
        }
        if ($this->vat_type == static::VAT_TYPE_SERVICE) {
            $chargesSum = $finance->price_delivery + $finance->price_delivery_return + $finance->price_storage + $finance->price_fulfilment + $finance->price_packing + $finance->price_package;
            $finance->price_vat = $chargesSum * (doubleval($this->vat_percent) / 100);
            $finance->price_vat_currency_id = $currencyId;
        }

        $finance->price_delivery_currency_id = $finance->price_fulfilment_currency_id = $finance->price_packing_currency_id = $finance->price_package_currency_id = $finance->price_storage_currency_id = $currencyId;

        return parent::afterCalculate($finance);
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @param bool $onlyView
     * @return string
     * @throws \Exception
     */
    public function render($form, $onlyView = false)
    {
        return CommonDeliveryCharges::widget([
                'form' => $form,
                'model' => $this,
                'items' => [
                    [
                        'attribute' => 'fulfillment_fee',
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'packing_fee',
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'packed_fee',
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'cod_fee',
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'vat_percent',
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'vat_type',
                        'type' => 'select2List',
                        'items' => static::getVatTypes(),
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'currency_char_code',
                        'type' => 'select2List',
                        'items' => ArrayHelper::map(Currency::find()->all(), 'char_code', 'name'),
                        'length' => 2,
                        'options' => [
                            'length' => false,
                            'disabled' => $onlyView
                        ]
                    ],
                    [
                        'length' => 3,
                    ],
                    [
                        'attribute' => 'storage_area',
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'storage_fee',
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                ]
            ])
            . DeliveryChargesForRegion::widget([
                'form' => $form,
                'model' => $this,
                'onlyView' => $onlyView,
                'regionAttribute' => 'zipcodes',
                'chargesAttributes' => ['delivery_fees', 'return_fees', 'cod_fees', 'enable_percent_buyout'],
                'chargesArrayAttributes' => [
                    [
                        'attribute' => 'delivery_fees_array',
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'return_fees_array',
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'cod_fees_array',
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'percent_buyout_from',
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'percent_buyout_to',
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'day_from',
                        'type' => 'select2List',
                        'items' => array_combine(range(1, 31), range(1, 31)),
                        'length' => 2,
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'day_to',
                        'type' => 'select2List',
                        'items' => array_combine(range(1, 31), range(1, 31)),
                        'length' => 2,
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                ]
            ]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'return_fees' => Yii::t('common', 'Стоимость возврата'),
            'cod_fees' => Yii::t('common', 'Наложенный платеж (%)'),
            'packing_fee' => Yii::t('common', 'Стоимость упаковывания'),
            'packed_fee' => Yii::t('common', 'Стоимость упаковки'),
            'storage_area' => Yii::t('common', 'Площадь склада (м2)'),
            'storage_fee' => Yii::t('common', 'Стоимость аренды склада (1м2)'),
            'delivery_fees_array' => Yii::t('common', 'Стоимость доставки'),
            'return_fees_array' => Yii::t('common', 'Стоимость возврата'),
            'cod_fees_array' => Yii::t('common', 'Наложенный платеж'),
            'percent_buyout_from' => Yii::t('common', '% выкупа от (>=)'),
            'percent_buyout_to' => Yii::t('common', '% выкупа до (<)'),
            'day_from' => Yii::t('common', 'Расчетный период с'),
            'day_to' => Yii::t('common', 'Расчетный период по'),
            'enable_percent_buyout' => Yii::t('common', 'Учитывать % выкупа'),
        ]);
    }

    /**
     * @param $field
     * @return array
     */
    public function getCalculatingTypeForField($field)
    {
        $answer = [];
        switch ($field) {
            case OrderFinancePrediction::COLUMN_PRICE_COD_SERVICE:
                $answer = [
                    'type' => static::CALCULATING_TYPE_PERCENT,
                    'visible_formula' => "$this->cod_fee%",
                    'fields' => [
                        OrderFinancePrediction::COLUMN_PRICE_COD,
                    ],
                ];
                break;
            case OrderFinancePrediction::COLUMN_PRICE_VAT:
                $answer['type'] = static::CALCULATING_TYPE_PERCENT;
                $answer['visible_formula'] = "$this->vat_percent%";
                if ($this->vat_type == static::VAT_TYPE_COD) {
                    $answer['fields'] = [
                        OrderFinancePrediction::COLUMN_PRICE_COD,
                    ];
                } else {
                    $answer['fields'] = [
                        OrderFinancePrediction::COLUMN_PRICE_DELIVERY,
                        OrderFinancePrediction::COLUMN_PRICE_DELIVERY_RETURN,
                        OrderFinancePrediction::COLUMN_PRICE_STORAGE,
                        OrderFinancePrediction::COLUMN_PRICE_FULFILMENT,
                        OrderFinancePrediction::COLUMN_PRICE_PACKAGE,
                        OrderFinancePrediction::COLUMN_PRICE_PACKING,
                        OrderFinancePrediction::COLUMN_PRICE_DELIVERY_BACK,
                        OrderFinancePrediction::COLUMN_PRICE_REDELIVERY,
                        OrderFinancePrediction::COLUMN_PRICE_ADDRESS_CORRECTION,
                    ];
                }
                break;
            case OrderFinancePrediction::COLUMN_PRICE_STORAGE:
                $answer = [
                    'type' => ChargesCalculatorAbstract::CALCULATING_TYPE_MONTHLY_CHARGE,
                    'charge' => doubleval($this->storage_fee) * doubleval($this->storage_area),
                    'currency_id' => $this->getCurrency()->id,
                    'currency_rate' => $this->getCurrency()->currencyRate->rate,
                ];
                break;
            default:
                $answer['type'] = static::CALCULATING_TYPE_NUMBER;
                break;
        }

        return $answer;
    }
}