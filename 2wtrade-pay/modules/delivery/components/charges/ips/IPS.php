<?php

namespace app\modules\delivery\components\charges\ips;


use app\models\Currency;
use app\modules\delivery\components\charges\ChargesCalculatorAbstract;
use app\modules\delivery\widgets\charges\helpers\CommonDeliveryCharges;
use app\modules\order\models\OrderFinancePrediction;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class IPS
 * @package app\modules\delivery\components\charges\ips
 */
class IPS extends ChargesCalculatorAbstract
{
    public $delivery_fee = 0;
    public $storage_fee = 0; // TODO хранят в м3, хз как рассчитать
    public $packing_fee = 0;
    public $package_fee = 0;
    public $management_fee = 0;
    public $cod_fee = 0;
    public $vat_percent = 0;
    public $cod_fee_add = 0;

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [
                ['delivery_fee', 'storage_fee', 'packing_fee', 'package_fee', 'management_fee', 'cod_fee_add'],
                'number',
                'min' => 0
            ],
            [['cod_fee', 'vat_percent'], 'number', 'min' => 0, 'max' => 100],
            [['currency_char_code', 'vat_type'], 'string'],
        ]);
    }

    /**
     * @param \app\modules\order\models\Order $order
     * @return \app\modules\order\models\OrderFinancePrediction
     */
    public function calculate($order)
    {
        $finance = $this->getOrderFinance($order);

        $currency = $this->getCurrency();
        $currencyId = null;
        if (isset($currency)) {
            $currencyId = $currency->id;
        }
        $finance->price_delivery = doubleval($this->delivery_fee);
        $finance->price_packing = doubleval($this->packing_fee);
        $finance->price_package = doubleval($this->package_fee);

        $finance->price_fulfilment = 0;

        if ($this->isOrderBuyout($order)) {
            $rate = $this->getCurrencyRate();
            $finance->price_cod = $order->price_total + $order->delivery;
            $finance->price_cod_service = doubleval($this->cod_fee_add) * $rate + $finance->price_cod * (doubleval($this->cod_fee) / 100);
        } else {
            $finance->price_cod = 0;
            $finance->price_cod_service = 0;
        }

        $finance->price_vat = ($finance->price_delivery + $finance->price_packing + $finance->price_package + $finance->price_fulfilment + $finance->price_storage) * ($this->vat_percent / 100);

        $finance->price_delivery_currency_id = $finance->price_packing_currency_id = $finance->price_package_currency_id = $finance->price_fulfilment_currency_id = $currencyId;

        return parent::afterCalculate($finance);
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @param $onlyView
     * @return string
     */
    public function render($form, $onlyView = false)
    {
        return CommonDeliveryCharges::widget([
            'form' => $form,
            'model' => $this,
            'items' => [
                [
                    'attribute' => 'delivery_fee',
                    'options' => [
                        'disabled' => $onlyView
                    ],
                ],
                [
                    'attribute' => 'packing_fee',
                    'options' => [
                        'disabled' => $onlyView
                    ],
                ],
                [
                    'attribute' => 'package_fee',
                    'options' => [
                        'disabled' => $onlyView
                    ],
                ],
                [
                    'attribute' => 'storage_fee',
                    'options' => [
                        'disabled' => $onlyView
                    ],
                ],
                [
                    'attribute' => 'management_fee',
                    'options' => [
                        'disabled' => $onlyView
                    ],
                ],
                [
                    'attribute' => 'cod_fee',
                    'options' => [
                        'disabled' => $onlyView
                    ],
                ],
                [
                    'attribute' => 'cod_fee_add',
                    'options' => [
                        'disabled' => $onlyView
                    ],
                ],
                [
                    'attribute' => 'vat_percent',
                    'options' => [
                        'disabled' => $onlyView
                    ],
                ],
                [
                    'attribute' => 'currency_char_code',
                    'type' => 'select2List',
                    'items' => ArrayHelper::map(Currency::find()->all(), 'char_code', 'name'),
                    'length' => 2,
                    'options' => [
                        'length' => false,
                        'disabled' => $onlyView,
                    ]
                ],
            ],
        ]);
    }


    /**
     * @return array
     */
    public function customAttributeLabels()
    {
        return [
            'storage_fee' => \Yii::t('common', 'Стимость хранения (м3)'),
            'management_fee' => \Yii::t('common', 'Месячная стоимость обслуживания'),
            'cod_fee_add' => \Yii::t('common', 'Доп. наложенный платеж'),
        ];
    }

    /**
     * @param $field
     * @return array
     */
    public function getCalculatingTypeForField($field)
    {
        $currency = $this->getCurrency();
        $answer = [];
        switch ($field) {
            case OrderFinancePrediction::COLUMN_PRICE_COD_SERVICE:
                $answer = [
                    'type' => static::CALCULATING_TYPE_PERCENT,
                    'visible_formula' => Yii::t('common', '{val1} + {val2}', [
                        'val1' => "{$this->cod_fee_add} {$currency->char_code}",
                        'val2' => "{$this->cod_fee}%",
                    ]),
                    'fields' => [
                        OrderFinancePrediction::COLUMN_PRICE_COD,
                    ],
                ];
                break;
            case OrderFinancePrediction::COLUMN_PRICE_FULFILMENT:
                $answer = [
                    'type' => ChargesCalculatorAbstract::CALCULATING_TYPE_MONTHLY_CHARGE,
                    'charge' => doubleval($this->management_fee),
                    'currency_id' => $this->getCurrency()->id,
                    'currency_rate' => $this->getCurrency()->currencyRate->rate,
                ];
                break;
            default:
                $answer['type'] = static::CALCULATING_TYPE_NUMBER;
                break;
        }

        return $answer;
    }
}