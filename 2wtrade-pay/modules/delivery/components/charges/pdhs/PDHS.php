<?php

namespace app\modules\delivery\components\charges\pdhs;


use app\modules\delivery\components\charges\ChargesCalculatorAbstract;

/**
 * Class PDHS
 * @package app\modules\delivery\components\charges\pdhs
 */
class PDHS extends ChargesCalculatorAbstract
{
    /**
     * @param \app\modules\order\models\Order $order
     * @return \app\modules\order\models\OrderFinancePrediction
     */
    public function calculate($order)
    {
        $finance = $this->getOrderFinance($order);

        return parent::afterCalculate($finance);
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @param $onlyView
     * @return string
     */
    public function render($form, $onlyView = false)
    {
        return '';
    }
}