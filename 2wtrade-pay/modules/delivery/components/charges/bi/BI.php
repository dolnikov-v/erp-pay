<?php

namespace app\modules\delivery\components\charges\bi;

use app\modules\delivery\components\charges\ChargesCalculatorAbstract;
use app\modules\delivery\widgets\charges\bi\BIWidget;
use app\modules\order\models\Order;
use app\modules\order\models\OrderFinancePrediction;
use Yii;

/**
 * Class BI
 * @package app\modules\delivery\components\charges\bi
 */
class BI extends ChargesCalculatorAbstract
{
    public $zone_a = '';
    public $zone_b = '';
    public $zone_c = '';
    public $zone_d = '';

    public $zone_a_shipment_a_weight_a = 0;
    public $zone_a_shipment_a_weight_b = 0;
    public $zone_a_shipment_b_weight_a = 0;
    public $zone_a_shipment_b_weight_b = 0;
    public $zone_a_return = 0;

    public $zone_b_shipment_a_weight_a = 0;
    public $zone_b_shipment_a_weight_b = 0;
    public $zone_b_shipment_b_weight_a = 0;
    public $zone_b_shipment_b_weight_b = 0;
    public $zone_b_return = 0;

    public $zone_c_shipment_a_weight_a = 0;
    public $zone_c_shipment_a_weight_b = 0;
    public $zone_c_shipment_b_weight_a = 0;
    public $zone_c_shipment_b_weight_b = 0;
    public $zone_c_return = 0;

    public $zone_d_shipment_a_weight_a = 0;
    public $zone_d_shipment_a_weight_b = 0;
    public $zone_d_shipment_b_weight_a = 0;
    public $zone_d_shipment_b_weight_b = 0;
    public $zone_d_return = 0;

    public $vat_percent = 0;

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [
                [
                    'zone_a_shipment_a_weight_a',
                    'zone_a_shipment_a_weight_b',
                    'zone_a_shipment_b_weight_a',
                    'zone_a_shipment_b_weight_b',
                    'zone_a_return',
                    'zone_b_shipment_a_weight_a',
                    'zone_b_shipment_a_weight_b',
                    'zone_b_shipment_b_weight_a',
                    'zone_b_shipment_b_weight_b',
                    'zone_b_return',
                    'zone_c_shipment_a_weight_a',
                    'zone_c_shipment_a_weight_b',
                    'zone_c_shipment_b_weight_a',
                    'zone_c_shipment_b_weight_b',
                    'zone_c_return',
                    'zone_d_shipment_a_weight_a',
                    'zone_d_shipment_a_weight_b',
                    'zone_d_shipment_b_weight_a',
                    'zone_d_shipment_b_weight_b',
                    'zone_d_return',
                ],
                'number',
                'min' => 0
            ],
            [['zone_a', 'zone_b', 'zone_c', 'zone_d'], 'string'],
            ['vat_percent', 'number', 'min' => 0, 'max' => 100],
            [['currency_char_code'], 'string'],
        ]);
    }

    /**
     * @param Order $order
     * @return null
     */
    public function calculate($order)
    {
        $periods = $this->getBuyoutOrdersCountByMonths();
        $zone = 'zone_d';
        $shipment = 'shipment_a';
        $weight_word = 'weight_b';

        $zoneA = array_map('trim', explode(',', $this->zone_a));
        $zoneB = array_map('trim', explode(',', $this->zone_b));
        $zoneC = array_map('trim', explode(',', $this->zone_c));
        $zipcode = trim($order->customer_zip);

        if (in_array($zipcode, $zoneA)) {
            $zone = 'zone_a';
        } elseif (in_array($zipcode, $zoneB)) {
            $zone = 'zone_b';
        } elseif (in_array($zipcode, $zoneC)) {
            $zone = 'zone_c';
        }

        $weight = $this->getWeightOfParcel($order);

        if ($weight < 2100) {
            $weight_word = 'weight_a';
        }

        $date = $this->getOrderSendingMonth($order);

        if (isset($periods[$date]) && $periods[$date] > 10000) {
            $shipment = 'shipment_b';
        }

        $deliveryVar = "{$zone}_{$shipment}_{$weight_word}";
        $returnVar = "{$zone}_return";

        $finance = $this->getOrderFinance($order);
        $finance->price_delivery = doubleval($this->$deliveryVar);
        if ($this->isOrderBuyout($order)) {
            $finance->price_cod = $order->price_total + $order->delivery;
            $finance->price_delivery_return = 0;
        } else {
            if ($this->isOrderReturnToSender($order)) {
                $finance->price_delivery_return = doubleval($this->$returnVar);
            } else {
                $finance->price_delivery_return = 0;
            }
            $finance->price_cod = 0;
        }
        $chargesSum = $finance->price_delivery + $finance->price_delivery_return;
        $finance->price_vat = $chargesSum * (doubleval($this->vat_percent) / 100);
        return $this->afterCalculate($finance);
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @param bool $onlyView
     * @return string
     * @throws \Exception
     */
    public function render($form, $onlyView = false)
    {
        return BIWidget::widget(['form' => $form, 'chargesCalculator' => $this, 'currencies' => self::$currencies, 'onlyView' => $onlyView]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'zone_a_shipment_a_weight_a' => Yii::t('common', 'Стоимость доставки, Зона А, менее 10000 заказов в месяц, вес менее 2кг'),
            'zone_a_shipment_a_weight_b' => Yii::t('common', 'Стоимость доставки, Зона А, менее 10000 заказов в месяц, вес более 2кг'),
            'zone_a_shipment_b_weight_a' => Yii::t('common', 'Стоимость доставки, Зона А, более 10000 заказов в месяц, вес менее 2кг'),
            'zone_a_shipment_b_weight_b' => Yii::t('common', 'Стоимость доставки, Зона А, более 10000 заказов в месяц, вес более 2кг'),
            'zone_a_return' => Yii::t('common', 'Стоимость возврата, Зона A'),
            'zone_b_shipment_a_weight_a' => Yii::t('common', 'Стоимость доставки, Зона B, менее 10000 заказов в месяц, вес менее 2кг'),
            'zone_b_shipment_a_weight_b' => Yii::t('common', 'Стоимость доставки, Зона B, менее 10000 заказов в месяц, вес более 2кг'),
            'zone_b_shipment_b_weight_a' => Yii::t('common', 'Стоимость доставки, Зона B, более 10000 заказов в месяц, вес менее 2кг'),
            'zone_b_shipment_b_weight_b' => Yii::t('common', 'Стоимость доставки, Зона B, более 10000 заказов в месяц, вес более 2кг'),
            'zone_b_return' => Yii::t('common', 'Стоимость возврата, Зона B'),
            'zone_c_shipment_a_weight_a' => Yii::t('common', 'Стоимость доставки, Зона C, менее 10000 заказов в месяц, вес менее 2кг'),
            'zone_c_shipment_a_weight_b' => Yii::t('common', 'Стоимость доставки, Зона C, менее 10000 заказов в месяц, вес более 2кг'),
            'zone_c_shipment_b_weight_a' => Yii::t('common', 'Стоимость доставки, Зона C, более 10000 заказов в месяц, вес менее 2кг'),
            'zone_c_shipment_b_weight_b' => Yii::t('common', 'Стоимость доставки, Зона C, более 10000 заказов в месяц, вес более 2кг'),
            'zone_c_return' => Yii::t('common', 'Стоимость возврата, Зона C'),
            'zone_d_shipment_a_weight_a' => Yii::t('common', 'Стоимость доставки, Зона D, менее 10000 заказов в месяц, вес менее 2кг'),
            'zone_d_shipment_a_weight_b' => Yii::t('common', 'Стоимость доставки, Зона D, менее 10000 заказов в месяц, вес более 2кг'),
            'zone_d_shipment_b_weight_a' => Yii::t('common', 'Стоимость доставки, Зона D, более 10000 заказов в месяц, вес менее 2кг'),
            'zone_d_shipment_b_weight_b' => Yii::t('common', 'Стоимость доставки, Зона D, более 10000 заказов в месяц, вес более 2кг'),
            'zone_d_return' => Yii::t('common', 'Стоимость возврата, Зона D'),
            'zone_a' => Yii::t('common', 'Индексы, входящие в зону A'),
            'zone_b' => Yii::t('common', 'Индексы, входящие в зону B'),
            'zone_c' => Yii::t('common', 'Индексы, входящие в зону C'),
            'zone_d' => Yii::t('common', 'Индексы, входящие в зону D'),
            'vat_percent' => Yii::t('common', 'НДС(%)')
        ];
    }

    /**
     * @param $field
     * @return array
     */
    public function getCalculatingTypeForField($field)
    {
        $answer = [];
        switch ($field) {
            case OrderFinancePrediction::COLUMN_PRICE_VAT:
                $answer['type'] = static::CALCULATING_TYPE_PERCENT;
                $answer['visible_formula'] = "$this->vat_percent%";
                $answer['fields'] = [
                    OrderFinancePrediction::COLUMN_PRICE_DELIVERY,
                    OrderFinancePrediction::COLUMN_PRICE_DELIVERY_RETURN,
                    OrderFinancePrediction::COLUMN_PRICE_STORAGE,
                    OrderFinancePrediction::COLUMN_PRICE_FULFILMENT,
                    OrderFinancePrediction::COLUMN_PRICE_PACKAGE,
                    OrderFinancePrediction::COLUMN_PRICE_PACKING,
                    OrderFinancePrediction::COLUMN_PRICE_DELIVERY_BACK,
                    OrderFinancePrediction::COLUMN_PRICE_REDELIVERY,
                    OrderFinancePrediction::COLUMN_PRICE_ADDRESS_CORRECTION,
                ];
                break;
            default:
                $answer['type'] = static::CALCULATING_TYPE_NUMBER;
                break;
        }

        return $answer;
    }
}