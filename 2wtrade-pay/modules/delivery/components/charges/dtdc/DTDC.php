<?php

namespace app\modules\delivery\components\charges\dtdc;

use app\models\Currency;
use app\modules\delivery\components\charges\ChargesCalculatorAbstract;
use app\modules\delivery\widgets\charges\helpers\CommonDeliveryCharges;
use app\modules\order\models\OrderFinancePrediction;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class DTDC
 * @package app\modules\delivery\components\charges\dtdc
 */
class DTDC extends ChargesCalculatorAbstract
{
    public $storage_fee = 0;
    public $stock_in_fee = 0;
    public $stock_out_fee = 0;
    public $labeling_fee = 0;
    public $delivery_fee = 0;
    public $delivery_fee_add_kg;

    protected $countOfOrderByPeriods = null;

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [
                ['storage_fee', 'stock_in_fee', 'stock_out_fee', 'labeling_fee', 'delivery_fee', 'delivery_fee_add_kg'],
                'number',
                'min' => 0
            ],
            [['currency_char_code'], 'string'],
        ]);
    }

    /**
     * @param \app\modules\order\models\Order $order
     * @return OrderFinancePrediction
     */
    public function calculate($order)
    {
        $finance = $this->getOrderFinance($order);
        $currency = $this->getCurrency();
        $currencyId = null;
        if (isset($currency)) {
            $currencyId = $currency->id;
        }

        $finance->price_storage = 0;
        $finance->price_packing = doubleval($this->labeling_fee);
        foreach ($order->orderProducts as $product) {
            $finance->price_packing += ($product->quantity * doubleval($this->stock_in_fee) + $product->quantity * doubleval($this->stock_out_fee));
        }
        $weight = $this->getWeightOfParcel($order);

        $finance->price_delivery = (doubleval($this->delivery_fee) + ($weight > 3000 ? (ceil(($weight - 3000) / 500) * $this->delivery_fee_add_kg) : 0));

        if ($this->isOrderBuyout($order)) {
            $finance->price_cod = $order->price_total + $order->delivery;
        } else {
            $finance->price_cod = 0;
            $finance->price_cod_service = 0;
        }

        $finance->price_storage_currency_id = $finance->price_delivery_currency_id = $finance->price_packing_currency_id = $currencyId;

        return parent::afterCalculate($finance);
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @param $onlyView
     * @return string
     */
    public function render($form, $onlyView = false)
    {
        return CommonDeliveryCharges::widget([
            'form' => $form,
            'model' => $this,
            'items' => [
                [
                    'attribute' => 'stock_in_fee',
                    'options' => [
                        'disabled' => $onlyView,
                    ]
                ],
                [
                    'attribute' => 'stock_out_fee',
                    'options' => [
                        'disabled' => $onlyView,
                    ]
                ],
                [
                    'attribute' => 'labeling_fee',
                    'options' => [
                        'disabled' => $onlyView,
                    ]
                ],
                [
                    'attribute' => 'delivery_fee',
                    'options' => [
                        'disabled' => $onlyView,
                    ]
                ],
                [
                    'attribute' => 'delivery_fee_add_kg',
                    'options' => [
                        'disabled' => $onlyView,
                    ]
                ],
                [
                    'attribute' => 'storage_fee',
                    'options' => [
                        'disabled' => $onlyView,
                    ]
                ],
                [
                    'attribute' => 'currency_char_code',
                    'type' => 'select2List',
                    'items' => ArrayHelper::map(Currency::find()->all(), 'char_code', 'name'),
                    'options' => [
                        'length' => false,
                        'disabled' => $onlyView,
                    ]
                ]
            ]
        ]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'storage_fee' => Yii::t('common', 'Стоимость аренды склада за месяц'),
            'stock_in_fee' => Yii::t('common', 'Плата за принятие заказа на склад'),
            'stock_out_fee' => Yii::t('common', 'Плата за выпуск товара со склада'),
            'labeling_fee' => Yii::t('common', 'Плата за наклейку этикетки'),
            'delivery_fee' => Yii::t('common', 'Плата за доставку посылки весом 3кг'),
            'delivery_fee_add_kg' => Yii::t('common', 'Плата за каждый доп. 500гр'),
        ];
    }

    /**
     * @return array|null
     */
    protected function getOrdersCountByMonths()
    {
        if (is_null($this->countOfOrderByPeriods)) {
            $this->countOfOrderByPeriods = parent::getOrdersCountByMonths();
        }
        return $this->countOfOrderByPeriods;
    }

    /**
     * @param $field
     * @return array
     */
    public function getCalculatingTypeForField($field)
    {
        $answer = [];
        switch ($field) {
            case OrderFinancePrediction::COLUMN_PRICE_STORAGE:
                $answer = [
                    'type' => ChargesCalculatorAbstract::CALCULATING_TYPE_MONTHLY_CHARGE,
                    'charge' => doubleval($this->storage_fee),
                    'currency_id' => $this->getCurrency()->id,
                    'currency_rate' => $this->getCurrency()->currencyRate->rate,
                ];
                break;
            default:
                $answer['type'] = static::CALCULATING_TYPE_NUMBER;
                break;
        }

        return $answer;
    }
}