<?php

namespace app\modules\delivery\components\charges\dtdc;


use app\models\Currency;
use app\modules\delivery\components\charges\ChargesCalculatorAbstract;
use app\modules\delivery\widgets\charges\helpers\CommonDeliveryCharges;
use app\modules\delivery\widgets\charges\helpers\DeliveryChargesForRegion;
use app\modules\order\models\OrderFinancePrediction;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class DTDCIN
 * @package app\modules\delivery\components\charges\dtdc
 */
class DTDCIN extends ChargesCalculatorAbstract
{
    public $zipcodes = [];
    public $delivery_fees_500gr = [];
    public $delivery_fees_each_500gr = [];
    public $delivery_fees_up_to_5kg = [];
    public $return_fee;
    public $cod_fee;
    public $vat_percent;
    public $cod_fee_min;

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            ['zipcodes', 'each', 'rule' => ['string']],
            ['zipcodes', 'each', 'rule' => ['filter', 'filter' => [$this, 'filterZipcodes']]],
            [
                ['delivery_fees_500gr', 'delivery_fees_each_500gr', 'delivery_fees_up_to_5kg'],
                'each',
                'rule' => ['number', 'min' => 0]
            ],
            [['currency_char_code'], 'string'],
            [['cod_fee_min', 'return_fee'], 'number', 'min' => 0],
            [['cod_fee', 'vat_percent'], 'number', 'min' => 0, 'max' => 100],
        ]);
    }

    protected function getDeliveryFee()
    {
        return $this->delivery_fees_500gr;
    }

    /**
     * @param \app\modules\order\models\Order $order
     * @return \app\modules\order\models\OrderFinancePrediction
     */
    public function calculate($order)
    {
        $finance = $this->getOrderFinance($order);
        $delivery_fee_500gr = 0;
        $delivery_fee_each_500gr = 0;
        $delivery_fee_up_to_5kg = 0;
        $index = $this->getIndexByZipcode($this->zipcodes, $order->customer_zip);
        if (!is_null($index)) {
            $delivery_fee_500gr = doubleval($this->delivery_fees_500gr[$index] ?? 0);
            $delivery_fee_each_500gr = doubleval($this->delivery_fees_each_500gr[$index] ?? 0);
            $delivery_fee_up_to_5kg = doubleval($this->delivery_fees_up_to_5kg[$index] ?? 0);
        }

        $weight = $this->getWeightOfParcel($order);

        if ($weight > 5000) {
            $weight = ceil($weight / 1000);
            $finance->price_delivery += $weight * $delivery_fee_up_to_5kg;
        } else {
            $parts = ceil($weight / 500);
            $finance->price_delivery = $delivery_fee_500gr + --$parts * $delivery_fee_each_500gr;
        }
        $currency = $this->getCurrency();
        $currencyId = null;
        if (isset($currency)) {
            $currencyId = $currency->id;
        }
        $finance->price_delivery_return = 0;
        if ($this->isOrderBuyout($order)) {
            $rate = $this->getCurrencyRate();
            $finance->price_cod = $order->price_total + $order->delivery;
            $item2 = $finance->price_cod * (doubleval($this->cod_fee) / 100);
            if ((doubleval($this->cod_fee_min) * $rate) > $item2) {
                $finance->price_cod_service = doubleval($this->cod_fee_min);
                $finance->price_cod_service_currency_id = $currencyId;
            } else {
                $finance->price_cod_service = $item2;
            }
        } else {
            if ($this->isOrderReturnToSender($order)) {
                $finance->price_delivery_return = doubleval($this->return_fee);
            }
            $finance->price_cod = 0;
            $finance->price_cod_service = 0;
        }

        $finance->price_delivery_currency_id = $currencyId;

        return parent::afterCalculate($finance);
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @param $onlyView
     * @return string
     */
    public function render($form, $onlyView = false)
    {
        return CommonDeliveryCharges::widget([
                'form' => $form,
                'model' => $this,
                'items' => [
                    [
                        'attribute' => 'return_fee',
                        'length' => 2,
                        'options' => [
                            'disabled' => $onlyView,
                        ]
                    ],
                    [
                        'cod_fee',
                        'options' => [
                            'disabled' => $onlyView,
                        ]
                    ],
                    [
                        'cod_fee_min',
                        'options' => [
                            'disabled' => $onlyView,
                        ]
                    ],
                    [
                        'attribute' => 'vat_percent',
                        'length' => 2,
                        'options' => [
                            'disabled' => $onlyView,
                        ]
                    ],
                    [
                        'attribute' => 'currency_char_code',
                        'type' => 'select2List',
                        'items' => ArrayHelper::map(Currency::find()->all(), 'char_code', 'name'),
                        'length' => 2,
                        'options' => [
                            'length' => false,
                            'disabled' => $onlyView,
                        ]
                    ],
                ]
            ]) . DeliveryChargesForRegion::widget([
                'form' => $form,
                'model' => $this,
                'onlyView' => $onlyView,
                'regionAttribute' => 'zipcodes',
                'chargesAttributes' => ['delivery_fees_500gr', 'delivery_fees_each_500gr', 'delivery_fees_up_to_5kg']
            ]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'delivery_fees_500gr' => Yii::t('common', 'Плата за доставку (500гр.)'),
            'delivery_fees_each_500gr' => Yii::t('common', 'Плата за доставку (каждые 500гр.)'),
            'delivery_fees_up_to_5kg' => Yii::t('common', 'Плата за доставку (за 1кг, при весе более 5кг.)'),
            'cod_fee_min' => Yii::t('common', 'Минимальная плата за наложенный платеж')
        ]);
    }

    /**
     * @param $field
     * @return array
     */
    public function getCalculatingTypeForField($field)
    {
        $currency = $this->getCurrency();
        $answer = [];
        switch ($field) {
            case OrderFinancePrediction::COLUMN_PRICE_COD_SERVICE:
                $answer = [
                    'type' => static::CALCULATING_TYPE_PERCENT,
                    'visible_formula' => Yii::t('common', 'МИН({val1}, {val2}', [
                        'val1' => "{$this->cod_fee}%",
                        'val2' => "{$this->cod_fee_min} {$currency->char_code}"
                    ]),
                    'fields' => [
                        OrderFinancePrediction::COLUMN_PRICE_COD,
                    ],
                ];
                break;
            default:
                $answer['type'] = static::CALCULATING_TYPE_NUMBER;
                break;
        }

        return $answer;
    }
}