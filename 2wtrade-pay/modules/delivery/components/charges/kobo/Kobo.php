<?php

namespace app\modules\delivery\components\charges\kobo;


use app\models\Currency;
use app\modules\delivery\components\charges\ChargesCalculatorAbstract;
use app\modules\delivery\components\charges\DeliveryChargeByZip;
use app\modules\delivery\widgets\charges\helpers\CommonDeliveryCharges;
use app\modules\delivery\widgets\charges\helpers\DeliveryChargesForRegion;
use app\modules\order\models\OrderFinancePrediction;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class Kobo
 * @package app\modules\delivery\components\charges\kobo
 */
class Kobo extends DeliveryChargeByZip
{
    public $delivery_fees_05kg = [];
    public $delivery_fees_1kg = [];
    public $delivery_fees_15kg = [];
    public $delivery_fees_2kg = [];
    public $delivery_fees_25kg = [];
    public $delivery_fees_3kg = [];
    public $delivery_fees_35kg = [];

    public $vat_type = self::VAT_TYPE_SERVICE;

    /**
     * @return array
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [
                [
                    'delivery_fees_05kg',
                    'delivery_fees_1kg',
                    'delivery_fees_15kg',
                    'delivery_fees_2kg',
                    'delivery_fees_25kg',
                    'delivery_fees_3kg',
                    'delivery_fees_35kg'
                ],
                'each',
                'rule' => ['number', 'min' => 0]
            ]
        ]);
    }


    protected function getDeliveryFee()
    {
        return $this->delivery_fees_05kg;
    }

    /**
     * @param \app\modules\order\models\Order $order
     * @return \app\modules\order\models\OrderFinancePrediction
     */
    public function calculate($order)
    {
        $finance = $this->getOrderFinance($order);
        $weight = $this->getWeightOfParcel($order);
        $delivery_fee_05kg = 0;
        $delivery_fee_1kg = 0;
        $delivery_fee_15kg = 0;
        $delivery_fee_2kg = 0;
        $delivery_fee_25kg = 0;
        $delivery_fee_3kg = 0;
        $delivery_fee_35kg = 0;
        $index = $this->getIndexByZipcode($this->zipcodes, $order->customer_zip);

        if (!is_null($index)) {
            $delivery_fee_05kg = doubleval($this->delivery_fees_05kg[$index] ?? 0);
            $delivery_fee_1kg = doubleval($this->delivery_fees_1kg[$index] ?? 0);
            $delivery_fee_15kg = doubleval($this->delivery_fees_15kg[$index] ?? 0);
            $delivery_fee_2kg = doubleval($this->delivery_fees_2kg[$index] ?? 0);
            $delivery_fee_25kg = doubleval($this->delivery_fees_25kg[$index] ?? 0);
            $delivery_fee_3kg = doubleval($this->delivery_fees_3kg[$index] ?? 0);
            $delivery_fee_35kg = doubleval($this->delivery_fees_35kg[$index] ?? 0);
        }

        if ($weight <= 500) {
            $finance->price_delivery = $delivery_fee_05kg;
        } elseif ($weight <= 1000) {
            $finance->price_delivery = $delivery_fee_1kg;
        } elseif ($weight <= 1500) {
            $finance->price_delivery = $delivery_fee_15kg;
        } elseif ($weight <= 2000) {
            $finance->price_delivery = $delivery_fee_2kg;
        } elseif ($weight <= 2500) {
            $finance->price_delivery = $delivery_fee_25kg;
        } elseif ($weight <= 3000) {
            $finance->price_delivery = $delivery_fee_3kg;
        } else {
            $finance->price_delivery = $delivery_fee_35kg;
        }

        $currency = $this->getCurrency();
        $currencyId = null;
        if (isset($currency)) {
            $currencyId = $currency->id;
        }
        $finance->price_storage = 0;

        $finance->price_fulfilment = doubleval($this->fulfillment_fee);
        if ($this->isOrderBuyout($order)) {
            $finance->price_cod = $order->price_total + $order->delivery;
            $finance->price_cod_service = $finance->price_cod * (doubleval($this->cod_fee) / 100);
        } else {
            $finance->price_cod = 0;
            $finance->price_cod_service = 0;
        }
        $chargesSum = $finance->price_delivery + $finance->price_delivery_return + $finance->price_storage + $finance->price_fulfilment + $finance->price_packing + $finance->price_package;
        $finance->price_vat = $chargesSum * (doubleval($this->vat_percent) / 100);

        $finance->price_delivery_currency_id = $finance->price_storage_currency_id = $finance->price_fulfilment_currency_id = $finance->price_vat_currency_id = $currencyId;

        return parent::afterCalculate($finance);
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @param $onlyView
     * @return string
     */
    public function render($form, $onlyView = false)
    {
        return CommonDeliveryCharges::widget([
                'form' => $form,
                'model' => $this,
                'items' => [
                    [
                        'attribute' => 'fulfillment_fee',
                        'length' => 2,
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'cod_fee',
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'vat_percent',
                        'length' => 2,
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'storage_fee',
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'currency_char_code',
                        'type' => 'select2List',
                        'items' => ArrayHelper::map(Currency::find()->all(), 'char_code', 'name'),
                        'length' => 2,
                        'options' => [
                            'length' => false,
                            'disabled' => $onlyView,
                        ]
                    ],
                ]
            ])
            . DeliveryChargesForRegion::widget([
                'form' => $form,
                'model' => $this,
                'onlyView' => $onlyView,
                'regionAttribute' => 'zipcodes',
                'chargesAttributes' => [
                    'delivery_fees_05kg',
                    'delivery_fees_1kg',
                    'delivery_fees_15kg',
                    'delivery_fees_2kg',
                    'delivery_fees_25kg',
                    'delivery_fees_3kg',
                    'delivery_fees_35kg'
                ]
            ]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'delivery_fees_05kg' => Yii::t('common', 'Стоимость доставки (0,5кг.)'),
            'delivery_fees_1kg' => Yii::t('common', 'Стоимость доставки (1кг.)'),
            'delivery_fees_15kg' => Yii::t('common', 'Стоимость доставки (1,5кг.)'),
            'delivery_fees_2kg' => Yii::t('common', 'Стоимость доставки (2кг.)'),
            'delivery_fees_25kg' => Yii::t('common', 'Стоимость доставки (2,5кг.)'),
            'delivery_fees_3kg' => Yii::t('common', 'Стоимость доставки (3кг.)'),
            'delivery_fees_35kg' => Yii::t('common', 'Стоимость доставки (3,5кг.)'),
            'storage_fee' => Yii::t('common', 'Стоимость аренды склада (за 1 год)')
        ]);
    }

    /**
     * @param $field
     * @return array
     */
    public function getCalculatingTypeForField($field)
    {
        $answer = [];
        switch ($field) {
            case OrderFinancePrediction::COLUMN_PRICE_STORAGE:
                $answer = [
                    'type' => ChargesCalculatorAbstract::CALCULATING_TYPE_MONTHLY_CHARGE,
                    'charge' => doubleval($this->storage_fee) / 12,
                    'currency_id' => $this->getCurrency()->id,
                    'currency_rate' => $this->getCurrency()->currencyRate->rate,
                ];
                break;
            default:
                $answer = parent::getCalculatingTypeForField($field);
                break;
        }

        return $answer;
    }
}