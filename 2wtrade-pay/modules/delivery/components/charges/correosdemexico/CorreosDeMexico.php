<?php

namespace app\modules\delivery\components\charges\correosdemexico;


use app\models\Currency;
use app\modules\delivery\components\charges\ChargesCalculatorAbstract;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\delivery\widgets\charges\helpers\CommonDeliveryCharges;
use app\modules\order\models\OrderFinancePrediction;
use yii\helpers\ArrayHelper;

/**
 * Class CorreosDeMexico
 * @package app\modules\delivery\components\charges\correosdemexico
 */
class CorreosDeMexico extends ChargesCalculatorAbstract
{
    public $delivery_fee = 0;
    public $delivery_fee1 = 0;
    public $delivery_fee2 = 0;
    public $delivery_fee3 = 0;
    public $delivery_fee4 = 0;
    public $delivery_fee5 = 0;
    public $delivery_fee6 = 0;

    public $vat_percent = 0;
    public $return_fee;

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [
                [
                    'delivery_fee',
                    'delivery_fee1',
                    'delivery_fee2',
                    'delivery_fee3',
                    'delivery_fee4',
                    'delivery_fee5',
                    'delivery_fee6',
                    'return_fee'
                ],
                'number',
                'min' => 0
            ],
            [['vat_percent'], 'number', 'min' => 0, 'max' => 100],
            [['currency_char_code'], 'string'],
        ]);
    }

    /**
     * @param \app\modules\order\models\Order $order
     * @return OrderFinancePrediction
     */
    public function calculate($order)
    {

        $count = 0;
        $deliveryID = $order->deliveryRequest->delivery_id ?? $order->pending_delivery_id;
        if ($deliveryID) {
            $sent = max($order->deliveryRequest->second_sent_at ?? 0, $order->deliveryRequest->sent_at ?? 0, $order->deliveryRequest->created_at ?? 0, $order->created_at);

            $count = DeliveryRequest::find()
                ->byDeliveryId($deliveryID)
                ->andWhere([
                    'between',
                    'COALESCE(' . DeliveryRequest::tableName() . '.second_sent_at,' . DeliveryRequest::tableName() . '.sent_at, ' . DeliveryRequest::tableName() . '.created_at)',
                    strtotime('first day of this month 00:00:00', $sent),
                    strtotime('last day of this month 23:23:59', $sent),
                ])
                ->count();
        }


        $finance = $this->getOrderFinance($order);

        $currency = $this->getCurrency();
        $currencyId = null;
        if (isset($currency)) {
            $currencyId = $currency->id;
        }


        $finance->price_delivery = doubleval($this->delivery_fee);
        if ($count >= 250 && $count < 500) {
            $finance->price_delivery = doubleval($this->delivery_fee);
        }
        if ($count >= 500 && $count < 750) {
            $finance->price_delivery = doubleval($this->delivery_fee1);
        }
        if ($count >= 750 && $count < 1000) {
            $finance->price_delivery = doubleval($this->delivery_fee2);
        }
        if ($count >= 1000 && $count < 5000) {
            $finance->price_delivery = doubleval($this->delivery_fee3);
        }
        if ($count >= 5000 && $count < 10000) {
            $finance->price_delivery = doubleval($this->delivery_fee4);
        }
        if ($count >= 10000 && $count < 15000) {
            $finance->price_delivery = doubleval($this->delivery_fee5);
        }
        if ($count >= 15000) {
            $finance->price_delivery = doubleval($this->delivery_fee6);
        }

        $finance->price_cod = $finance->price_delivery_return = 0;
        if ($this->isOrderBuyout($order)) {
            $finance->price_cod = $order->delivery + $order->price_total;
        } elseif ($this->isOrderReturnToSender($order)) {
            $finance->price_delivery_return = doubleval($this->return_fee);
        }

        $finance->price_vat = ($finance->price_delivery + $finance->price_delivery_return) * (doubleval($this->vat_percent) / 100);

        $finance->price_delivery_currency_id = $finance->price_delivery_return_currency_id = $finance->price_vat_currency_id = $currencyId;

        return parent::afterCalculate($finance);
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @param $onlyView
     * @return string
     */
    public function render($form, $onlyView = false)
    {
        return CommonDeliveryCharges::widget([
            'form' => $form,
            'model' => $this,
            'items' => [
                [
                    'attribute' => 'delivery_fee',
                    'options' => [
                        'disabled' => $onlyView,
                    ]
                ],
                [
                    'attribute' => 'delivery_fee1',
                    'options' => [
                        'disabled' => $onlyView,
                    ]
                ],
                [
                    'attribute' => 'delivery_fee2',
                    'options' => [
                        'disabled' => $onlyView,
                    ]
                ],
                [
                    'attribute' => 'delivery_fee3',
                    'options' => [
                        'disabled' => $onlyView,
                    ]
                ],
                [
                    'attribute' => 'delivery_fee4',
                    'options' => [
                        'disabled' => $onlyView,
                    ]
                ],
                [
                    'attribute' => 'delivery_fee5',
                    'options' => [
                        'disabled' => $onlyView,
                    ]
                ],
                [
                    'attribute' => 'delivery_fee6',
                    'options' => [
                        'disabled' => $onlyView,
                    ]
                ],
                [
                    'attribute' => 'vat_percent',
                    'options' => [
                        'disabled' => $onlyView,
                    ]
                ],
                [
                    'attribute' => 'return_fee',
                    'options' => [
                        'disabled' => $onlyView,
                    ]
                ],
                [
                    'attribute' => 'currency_char_code',
                    'type' => 'select2List',
                    'items' => ArrayHelper::map(Currency::find()->all(), 'char_code', 'name'),
                    'options' => [
                        'length' => false,
                        'disabled' => $onlyView,
                    ]
                ]
            ]
        ]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'delivery_fee' => \Yii::t('common', 'Стоимость доставки при кол-ве заказов от 250 до 500 в месяц'),
            'delivery_fee1' => \Yii::t('common', 'Стоимость доставки при кол-ве заказов от 500 до 750'),
            'delivery_fee2' => \Yii::t('common', 'Стоимость доставки при кол-ве заказов от 750 до 1000'),
            'delivery_fee3' => \Yii::t('common', 'Стоимость доставки при кол-ве заказов от 1000 до 5000'),
            'delivery_fee4' => \Yii::t('common', 'Стоимость доставки при кол-ве заказов от 5000 до 10000'),
            'delivery_fee5' => \Yii::t('common', 'Стоимость доставки при кол-ве заказов от 10000 до 15000'),
            'delivery_fee6' => \Yii::t('common', 'Стоимость доставки при кол-ве заказов более 15000'),
            'return_fee' => \Yii::t('common', 'Стоимость возврата'),
        ]);
    }


    /**
     * @param $field
     * @return array
     */
    public function getCalculatingTypeForField($field)
    {
        $answer = [];
        switch ($field) {
            case OrderFinancePrediction::COLUMN_PRICE_VAT:
                $answer['type'] = static::CALCULATING_TYPE_PERCENT;
                $answer['visible_formula'] = "$this->vat_percent%";
                $answer['fields'] = [
                    OrderFinancePrediction::COLUMN_PRICE_COD,
                ];
                break;
            default:
                $answer['type'] = static::CALCULATING_TYPE_NUMBER;
                break;
        }

        return $answer;
    }
}