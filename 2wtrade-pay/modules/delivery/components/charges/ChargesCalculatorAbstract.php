<?php

namespace app\modules\delivery\components\charges;

use app\components\ModelTrait;
use app\components\widgets\ActiveForm;
use app\models\Currency;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryContract;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\models\Order;
use app\modules\order\models\OrderFinancePrediction;
use app\modules\order\models\OrderStatus;
use Yii;
use yii\base\Model;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use app\models\CurrencyRate;
use yii\base\InvalidParamException;

/**
 * Class ChargesTemplateAbstract
 * @package app\modules\delivery\components\charges
 */
abstract class ChargesCalculatorAbstract extends Model
{
    use ModelTrait;

    const VAT_TYPE_COD = 'vat_cod';
    const VAT_TYPE_SERVICE = 'vat_service';

    const CALCULATING_TYPE_NUMBER = 'calculating_type_number';
    const CALCULATING_TYPE_PERCENT = 'calculating_type_percent';
    const CALCULATING_TYPE_FORMULA = 'calculating_type_formula'; // TODO пока пропускаю, надо будет потом реализовать
    const CALCULATING_TYPE_MONTHLY_CHARGE = 'calculating_type_monthly_charge';

    /**
     * @var Delivery
     */
    public $delivery;

    /**
     * @var DeliveryContract
     */
    public $deliveryContract;

    /**
     * @var string
     */
    public $currency_char_code = null;

    /**
     * @var null|double
     */
    protected $currencyRate = null;

    /**
     * @var null|Currency
     */
    protected $_currency = null;

    /**
     * @var null|array
     */
    protected $countOrdersByMonth = null;

    /**
     * @var null|array
     */
    protected $countOrdersByDays = null;

    /**
     * @var null
     */
    protected $countBuyoutsByMonth = null;

    /**
     * @var string
     */
    public $vat_type = self::VAT_TYPE_COD;

    /**
     * @var array
     */
    public static $currencies;

    /**
     * @var bool
     */
    public $vat_included = false;


    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['vat_included'], 'boolean'],
        ];
    }

    /**
     * @throws \Exception
     */
    public function init()
    {
        if (is_numeric($this->delivery)) {
            $this->delivery = Delivery::findOne($this->delivery);
        }
        if (!$this->delivery) {
            throw new \Exception(Yii::t('common', 'Указана некорректная служба доставки.'));
        }
        if (is_numeric($this->deliveryContract)) {
            $this->deliveryContract = DeliveryContract::findOne($this->deliveryContract);
        }
        if (!$this->deliveryContract) {
            throw new \Exception(Yii::t('common', 'Указан некорректный контракт службы доставки.'));
        }
        $this->load($this->deliveryContract->chargesValues);
        if (is_null($this->currency_char_code) && isset(Yii::$app->user)) {
            $this->currency_char_code = Yii::$app->user->country->currency->char_code;
        }

        if (self::$currencies === null) {
            self::$currencies = ArrayHelper::map(Currency::find()->all(), 'char_code', 'name');
        }
    }

    /**
     * @param Order $order
     * @return OrderFinancePrediction
     */
    public abstract function calculate($order);


    /**
     * @param OrderFinancePrediction $finance
     * @return OrderFinancePrediction
     */
    public function afterCalculate($finance) {
        if ($this->vat_included) {
            $finance->vat_included = $finance->price_vat;
            $finance->price_vat = 0;
        }
        else {
            $finance->vat_included = 0;
        }
        return $finance;
    }

    /**
     * @param ActiveQuery $query
     */
    public function calculateForBatch($query)
    {
        foreach ($query->batch(1000) as $orders) {
            /**
             * @var Order[] $orders
             */
            foreach ($orders as $order) {
                $finance = $this->calculate($order);
                $finance->save();
            }
        }
    }

    /**
     * @param $debtSum
     * @return int
     */
    public function calculateAdditionalChargesForCommonDebt($debtSum)
    {
        return 0;
    }

    /**
     * @var ActiveForm $form
     * @var bool $onlyView
     * @return string
     */
    public abstract function render($form, $onlyView = false);

    /**
     * @param Order $order
     * @return OrderFinancePrediction
     */
    protected function getOrderFinance($order)
    {
        $orderFinance = $order->financePrediction;
        $currencyId = $this->delivery->country->currency->id;

        if (!$orderFinance) {
            $orderFinance = new OrderFinancePrediction([
                'order_id' => $order->id,
            ]);
        }
        foreach (OrderFinancePrediction::getCurrencyFields() as $key => $val) {
            $orderFinance->$key = null;
            $orderFinance->$val = $currencyId;
        }
        $orderFinance->price_cod_currency_id = $order->price_currency ?? $currencyId;
        $orderFinance->price_cod_service_currency_id = $order->price_currency ?? $currencyId;
        $orderFinance->price_vat_currency_id = $order->price_currency ?? $currencyId;
        $orderFinance->vat_included = $this->vat_included;
        return $orderFinance;
    }

    /**
     * Количество заказов в каждом месяце
     * Возвращает ('month' => 'count')
     * @return array
     */
    protected function getOrdersCountByMonths()
    {
        if (!is_null($this->countOrdersByMonth)) {
            return $this->countOrdersByMonth;
        }
        $countQuery = Order::find()->joinWith(['deliveryRequest', 'callCenterRequest']);
        $countQuery->where([DeliveryRequest::tableName() . '.delivery_id' => $this->delivery->id]);
        $countQuery->andWhere([
            "not in",
            DeliveryRequest::tableName() . '.status',
            [DeliveryRequest::STATUS_PENDING, DeliveryRequest::STATUS_ERROR]
        ]);
        $countQuery->select([
            'month' => 'DATE_FORMAT(FROM_UNIXTIME(COALESCE(' . DeliveryRequest::tableName() . '.second_sent_at, ' . DeliveryRequest::tableName() . '.sent_at, ' . CallCenterRequest::tableName() . '.approved_at, ' . Order::tableName() . '.created_at)), "%m.%Y")',
            'count' => "COUNT(" . Order::tableName() . ".id)",
        ]);
        $countQuery->groupBy(['month']);
        $countQuery->asArray();
        $periods = $countQuery->createCommand()->queryAll();

        $periods = ArrayHelper::map($periods, 'month', 'count');
        $this->countOrdersByMonth = $periods;
        return $periods;
    }

    /**
     * Количество заказов в каждом месяце
     * Возвращает ('day' => 'count')
     * @return array
     */
    protected function getOrdersCountByDays()
    {
        if (!is_null($this->countOrdersByDays)) {
            return $this->countOrdersByDays;
        }
        $countQuery = Order::find()->joinWith(['deliveryRequest', 'callCenterRequest']);
        $countQuery->where([DeliveryRequest::tableName() . '.delivery_id' => $this->delivery->id]);
        $countQuery->andWhere([
            "not in",
            DeliveryRequest::tableName() . '.status',
            [DeliveryRequest::STATUS_PENDING, DeliveryRequest::STATUS_ERROR]
        ]);
        $countQuery->select([
            'day' => 'DATE_FORMAT(FROM_UNIXTIME(COALESCE(' . DeliveryRequest::tableName() . '.second_sent_at, ' . DeliveryRequest::tableName() . '.sent_at, ' . DeliveryRequest::tableName() . '.created_at, ' . CallCenterRequest::tableName() . '.approved_at, ' . Order::tableName() . '.created_at)), "%d.%m.%Y")',
            'count' => "COUNT(" . Order::tableName() . ".id)",
        ]);
        $countQuery->groupBy(['day']);
        $countQuery->asArray();
        $periods = $countQuery->createCommand()->queryAll();

        $periods = ArrayHelper::map($periods, 'day', 'count');
        $this->countOrdersByDays = $periods;
        return $periods;
    }

    /**
     * Количество выкупленных заказов в каждом месяце
     * Возвращает ('month' => 'count')
     * @return array
     */
    protected function getBuyoutOrdersCountByMonths()
    {
        if (!is_null($this->countBuyoutsByMonth)) {
            return $this->countBuyoutsByMonth;
        }
        $countQuery = Order::find()->joinWith(['deliveryRequest', 'callCenterRequest']);
        $countQuery->where([DeliveryRequest::tableName() . '.delivery_id' => $this->delivery->id]);
        $countQuery->andWhere([
            "not in",
            DeliveryRequest::tableName() . '.status',
            [DeliveryRequest::STATUS_PENDING, DeliveryRequest::STATUS_ERROR]
        ]);
        $countQuery->andWhere([Order::tableName() . '.status_id' => OrderStatus::getBuyoutList()]);
        $countQuery->select([
            'month' => 'DATE_FORMAT(FROM_UNIXTIME(COALESCE(' . DeliveryRequest::tableName() . '.second_sent_at, ' . DeliveryRequest::tableName() . '.sent_at, ' . DeliveryRequest::tableName() . '.created_at, ' . CallCenterRequest::tableName() . '.approved_at, ' . Order::tableName() . '.created_at)), "%m.%Y")',
            'count' => "COUNT(" . Order::tableName() . ".id)",
        ]);
        $countQuery->groupBy(['month']);
        $countQuery->asArray();
        $periods = $countQuery->createCommand()->queryAll();

        $periods = ArrayHelper::map($periods, 'month', 'count');
        $this->countBuyoutsByMonth = $periods;
        return $periods;
    }

    /**
     * @param Order $order
     * @param string|null $format
     * @return false|string
     */
    protected function getOrderSendingMonth($order, $format = null)
    {
        $date = $order->created_at;

        if ($order->deliveryRequest && $order->deliveryRequest->second_sent_at) {
            $date = $order->deliveryRequest->second_sent_at;
        } elseif ($order->deliveryRequest && $order->deliveryRequest->sent_at) {
            $date = $order->deliveryRequest->sent_at;
        } elseif ($order->callCenterRequest && $order->callCenterRequest->approved_at) {
            $date = $order->callCenterRequest->approved_at;
        }
        if (!$format) {
            $format = 'm.Y';
        }
        $date = date($format, $date);

        return $date;
    }

    /**
     * @param Order $order
     * @param boolean $toKg
     * @return integer
     */
    protected function getWeightOfParcel($order, $toKg = false)
    {
        $weight = 0;
        foreach ($order->orderProducts as $product) {
            $weight += $product->quantity * $product->product->weight;
        }

        if ($toKg) {
            $weight = ceil($weight / 1000);
        }
        return $weight;
    }

    /**
     * @param Order $order
     * @return integer
     */
    protected function getCountOfParcel($order)
    {
        $count = 0;
        foreach ($order->orderProducts as $product) {
            $count += $product->quantity;
        }

        return $count;
    }

    /**
     * Приводит в порядок список почтовых индексов
     *
     * Убирает дубликаты, пробелы, сортирует
     *
     * @param string $zipcodes
     * @return string
     */
    public function filterZipcodes($zipcodes)
    {
        $codes = array_map("trim", explode(",", $zipcodes));
        $codes = array_filter(array_flip(array_flip($codes))); // удаляем дубликаты и пустые значения
        sort($codes, SORT_NATURAL);
        return implode(",", $codes);
    }


    /**
     * @return mixed|null
     */
    protected function getDeliveryFee()
    {
        if ($this->hasProperty('delivery_fees')) {
            return $this->delivery_fees;
        }
        return null;
    }

    /**
     * @param array $zipcodeGroups
     * @param string $zipcode
     * @return int|null|string
     */
    protected function getIndexByZipcode($zipcodeGroups, $zipcode)
    {
        $zipcode = strtolower(trim($zipcode));
        if (!empty($zipcode)) {
            foreach ($zipcodeGroups as $key => $values) {
                $array = array_map(function ($item) {
                    return trim(strtolower(($item)));
                }, explode(',', $values));
                if (in_array($zipcode, $array)) {
                    return $key;
                }
            }
        }

        // Если индекс не удалось определить, возьмем тот где минимальные цены за доставку
        $deliveryFee = $this->getDeliveryFee();
        if ($deliveryFee && is_array($deliveryFee)) {
            $deliveryFee = array_filter($deliveryFee, function ($element) {
                return !empty($element);
            });
            $index = array_search(min($deliveryFee), $deliveryFee);
            if ($index !== false) {
                return $index;
            }
        }

        return null;
    }

    /**
     * @param $order
     * @return bool
     */
    protected function isOrderReturnToSender($order)
    {
        return in_array($order->status_id, OrderStatus::getOnlyNotBuyoutInDeliveryList());
    }

    /**
     * @param $order
     * @return bool
     */
    protected function isOrderBuyout($order)
    {
        return in_array($order->status_id, OrderStatus::getBuyoutList());
    }

    /**
     * Общий объем продуктов в заказе в м3
     * @param $order
     * @return int
     */
    protected function getVolumeOfParcel($order)
    {
        $volume = 0;
        foreach ($order->orderProducts as $product) {
            $volume += $product->quantity * (($product->product->weight / 1000) * ($product->product->height / 1000) * ($product->product->length / 1000));
        }

        return $volume;
    }

    /**
     * @return float|null
     */
    protected function getCurrencyRate()
    {
        if (!$this->currency_char_code) {
            return 1;
        }

        if (is_null($this->currencyRate)) {
            $currency = CurrencyRate::find()
                ->joinWith(['currency'])
                ->where([Currency::tableName() . '.char_code' => $this->currency_char_code])
                ->one();
            if (!$currency) {
                throw new InvalidParamException(Yii::t('common', 'Указана несуществующая валюта для конвертации: {char_code}', ['char_code' => $this->currency_char_code]));
            } else {
                $this->currencyRate = $this->delivery->country->currencyRate->rate / $currency->rate;
            }
        }

        return $this->currencyRate;
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge([
            'zipcodes' => Yii::t('common', 'Индексы регионов'),
            'delivery_fees' => Yii::t('common', 'Стоимость доставки'),
            'currency_char_code' => Yii::t('common', 'Валюта'),
            'delivery_fee' => Yii::t('common', 'Стоимость доставки'),
            'cod_fee' => Yii::t('common', 'Плата на наложенный платеж (%)'),
            'vat_percent' => Yii::t('common', 'НДС (%)'),
            'return_fee' => Yii::t('common', 'Стоимость возврата товара'),
            'fulfillment_fee' => Yii::t('common', 'Стоимость обслуживания заказа'),
            'vat_type' => Yii::t('common', 'Тип НДС'),
            'packing_fee' => Yii::t('common', 'Стоимость упаковывания'),
            'package_fee' => Yii::t('common', 'Стоимость упаковки'),
            'vat_included' => Yii::t('common', 'НДС включен в стоимость'),
        ], $this->customAttributeLabels());
    }

    /**
     * @return array
     */
    public static function getVatTypes()
    {
        return [
            self::VAT_TYPE_COD => Yii::t('common', 'НДС на COD'),
            self::VAT_TYPE_SERVICE => Yii::t('common', 'НДС на услуги службы доставки')
        ];
    }

    /**
     * @return array
     */
    protected function customAttributeLabels()
    {
        return [];
    }

    /**
     * @return Currency|null
     */
    public function getCurrency()
    {
        if (is_null($this->_currency)) {
            if (is_null($this->currency_char_code)) {
                $this->_currency = $this->delivery->country->currency;
            } else {
                $this->_currency = Currency::find()->where(['char_code' => $this->currency_char_code])->one();
            }
        }

        return $this->_currency;
    }

    /**
     * @param $field
     * @return array
     */
    public function getCalculatingTypeForField($field)
    {
        return [
            'type' => static::CALCULATING_TYPE_NUMBER
        ];
    }

    /**
     * Для блока множественных значений (необходим как фильтр всем аттрибутам в этом блоке)
     * @param string $attribute
     */
    public function clearDefault(string $attribute)
    {
//        из-за multiselect пришлось добавить отрицательный индекс для дефолта
        unset($this->{$attribute}[-1]);
        foreach ($this->{$attribute} as $value) {
            if ($value == '') {
                $this->addError($attribute, Yii::t('common', 'Поле «{attribute}» не может быть пустым.', ['attribute' => $this->attributeLabels()[$attribute] ?? $attribute]));
            }
        }
    }
}
