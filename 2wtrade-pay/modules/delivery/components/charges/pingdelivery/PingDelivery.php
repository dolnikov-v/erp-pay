<?php

namespace app\modules\delivery\components\charges\pingdelivery;


use app\models\Currency;
use app\modules\delivery\components\charges\ChargesCalculatorAbstract;
use app\modules\delivery\widgets\charges\helpers\CommonDeliveryCharges;
use app\modules\order\models\OrderFinancePrediction;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class PingDelivery
 * @package app\modules\delivery\components\charges\pingdelivery
 */
class PingDelivery extends ChargesCalculatorAbstract
{
    /**
     * @var int
     */
    public $delivery_fee = 0;
    /**
     * @var int
     */
    public $cod_fee = 0;
    /**
     * @var int
     */
    public $cod_transfer_fee = 0;
    /**
     * @var int
     */
    public $fulfillment_fee = 0;

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['delivery_fee', 'fulfillment_fee'], 'number', 'min' => 0],
            [['cod_fee', 'cod_transfer_fee'], 'number', 'min' => 0, 'max' => 100],
            [['currency_char_code'], 'string'],
        ]);
    }

    /**
     * @param \app\modules\order\models\Order $order
     * @return OrderFinancePrediction
     */
    public function calculate($order)
    {
        $finance = $this->getOrderFinance($order);
        $currency = $this->getCurrency();
        $currencyId = null;
        if ($currency) {
            $currencyId = $currency->id;
        }
        $finance->price_delivery = doubleval($this->delivery_fee);
        $finance->price_fulfilment = doubleval($this->fulfillment_fee);

        if ($this->isOrderBuyout($order)) {
            $finance->price_cod = $order->delivery + $order->price_total;
            $finance->price_cod_service = $finance->price_cod * (doubleval($this->cod_fee) / 100);
        } else {
            $finance->price_cod = 0;
            $finance->price_cod_service = 0;
        }

        $finance->price_delivery_currency_id = $finance->price_fulfilment_currency_id = $currencyId;

        return parent::afterCalculate($finance);
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @param $onlyView
     * @return string
     */
    public function render($form, $onlyView = false)
    {
        return CommonDeliveryCharges::widget([
            'form' => $form,
            'model' => $this,
            'items' => [
                [
                    'attribute' => 'currency_char_code',
                    'type' => 'select2List',
                    'items' => ArrayHelper::map(Currency::find()->all(), 'char_code', 'name'),
                    'options' => [
                        'length' => false,
                        'disabled' => $onlyView,
                    ]
                ],
                [
                    'attribute' => 'fulfillment_fee',
                    'options' => [
                        'disabled' => $onlyView
                    ],
                ],
                [
                    'attribute' => 'delivery_fee',
                    'options' => [
                        'disabled' => $onlyView
                    ],
                ],
                [
                    'attribute' => 'cod_fee',
                    'options' => [
                        'disabled' => $onlyView
                    ],
                ],
                [
                    'attribute' => 'cod_transfer_fee',
                    'options' => [
                        'disabled' => $onlyView
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'cod_transfer_fee' => Yii::t('common', 'Процент за вывод средств (%)'),
            'fulfillment_fee' => Yii::t('common', 'Стоимость обработки заказа'),
        ]);
    }

    /**
     * @param $debtSum
     * @return mixed
     */
    public function calculateAdditionalChargesForCommonDebt($debtSum)
    {
        return $debtSum * ($this->cod_transfer_fee / 100);
    }


    /**
     * @param $field
     * @return array
     */
    public function getCalculatingTypeForField($field)
    {
        $answer = [];
        switch ($field) {
            case OrderFinancePrediction::COLUMN_PRICE_COD_SERVICE:
                $answer = [
                    'type' => static::CALCULATING_TYPE_PERCENT,
                    'visible_formula' => "$this->cod_fee%",
                    'fields' => [
                        OrderFinancePrediction::COLUMN_PRICE_COD,
                    ],
                ];
                break;
            default:
                $answer['type'] = static::CALCULATING_TYPE_NUMBER;
                break;
        }

        return $answer;
    }
}