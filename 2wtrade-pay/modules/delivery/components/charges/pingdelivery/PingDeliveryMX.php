<?php

namespace app\modules\delivery\components\charges\pingdelivery;


use app\models\Currency;
use app\modules\delivery\widgets\charges\helpers\CommonDeliveryCharges;
use yii\helpers\ArrayHelper;

/**
 * Class PingDeliveryMX
 * @package app\modules\delivery\components\charges\pingdelivery
 */
class PingDeliveryMX extends PingDelivery
{
    public $delivery_buyout_rate_1 = 0;
    public $delivery_buyout_rate_2 = 0;
    public $delivery_buyout_rate_3 = 0;
    public $delivery_buyout_rate_4 = 0;
    public $delivery_buyout_rate_5 = 0;
    public $delivery_buyout_rate_6 = 0;
    public $delivery_buyout_rate_7 = 0;
    public $delivery_buyout_rate_8 = 0;
    public $delivery_buyout_rate_9 = 0;

    /**
     * @return array
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [
                [
                    'delivery_buyout_rate_1',
                    'delivery_buyout_rate_2',
                    'delivery_buyout_rate_3',
                    'delivery_buyout_rate_4',
                    'delivery_buyout_rate_5',
                    'delivery_buyout_rate_6',
                    'delivery_buyout_rate_7',
                    'delivery_buyout_rate_8',
                    'delivery_buyout_rate_9'
                ],
                'number',
                'min' => 0
            ]
        ]);
    }

    /**
     * @param \app\modules\order\models\Order $order
     * @return \app\modules\order\models\OrderFinancePrediction
     */
    public function calculate($order)
    {
        $buyoutByPeriods = $this->getBuyoutOrdersCountByMonths();
        $sentByPeriods = $this->getOrdersCountByMonths();
        $month = $this->getOrderSendingMonth($order);

        $finance = $this->getOrderFinance($order);
        $currency = $this->getCurrency();
        $currencyId = null;
        if (isset($currency)) {
            $currencyId = $currency->id;
        }
        $finance->price_fulfilment = doubleval($this->fulfillment_fee);

        $delivery_fee = $this->delivery_buyout_rate_1;
        if (isset($buyoutByPeriods[$month]) && isset($sentByPeriods[$month])) {
            $rate = ceil(($buyoutByPeriods[$month] / $sentByPeriods[$month]) * 10);
            if (isset($this->{"delivery_buyout_rate_{$rate}"})) {
                $delivery_fee = $this->{"delivery_buyout_rate_{$rate}"};
            } elseif ($rate > 9) {
                $delivery_fee = $this->delivery_buyout_rate_9;
            }
        }
        $finance->price_delivery = doubleval($delivery_fee);
        if ($this->isOrderBuyout($order)) {
            $finance->price_cod = $order->delivery + $order->price_total;
            $finance->price_cod_service = $finance->price_cod * (doubleval($this->cod_fee) / 100);
        } else {
            $finance->price_cod = 0;
            $finance->price_cod_service = 0;
        }

        $finance->price_delivery_currency_id = $finance->price_fulfilment_currency_id = $currencyId;

        return parent::afterCalculate($finance);
    }


    /**
     * @param \app\components\widgets\ActiveForm $form
     * @param $onlyView
     * @return string
     */
    public function render($form, $onlyView = false)
    {
        return CommonDeliveryCharges::widget([
            'form' => $form,
            'model' => $this,
            'items' => [
                [
                    'attribute' => 'currency_char_code',
                    'type' => 'select2List',
                    'items' => ArrayHelper::map(Currency::find()->all(), 'char_code', 'name'),
                    'options' => [
                        'length' => false,
                        'disabled' => $onlyView,
                    ]
                ],
                [
                    'attribute' => 'fulfillment_fee',
                    'options' => [
                        'disabled' => $onlyView
                    ],
                ],
                [
                    'attribute' => 'delivery_buyout_rate_1',
                    'options' => [
                        'disabled' => $onlyView
                    ],
                ],
                [
                    'attribute' => 'delivery_buyout_rate_2',
                    'options' => [
                        'disabled' => $onlyView
                    ],
                ],
                [
                    'attribute' => 'delivery_buyout_rate_3',
                    'options' => [
                        'disabled' => $onlyView
                    ],
                ],
                [
                    'attribute' => 'delivery_buyout_rate_4',
                    'options' => [
                        'disabled' => $onlyView
                    ],
                ],
                [
                    'attribute' => 'delivery_buyout_rate_5',
                    'options' => [
                        'disabled' => $onlyView
                    ],
                ],
                [
                    'attribute' => 'delivery_buyout_rate_6',
                    'options' => [
                        'disabled' => $onlyView
                    ],
                ],
                [
                    'attribute' => 'delivery_buyout_rate_7',
                    'options' => [
                        'disabled' => $onlyView
                    ],
                ],
                [
                    'attribute' => 'delivery_buyout_rate_8',
                    'options' => [
                        'disabled' => $onlyView
                    ],
                ],
                [
                    'attribute' => 'delivery_buyout_rate_9',
                    'options' => [
                        'disabled' => $onlyView
                    ],
                ],
                [
                    'attribute' => 'cod_fee',
                    'options' => [
                        'disabled' => $onlyView
                    ],
                ],
                [
                    'attribute' => 'cod_transfer_fee',
                    'options' => [
                        'disabled' => $onlyView
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'delivery_buyout_rate_1' => \Yii::t('common', 'Стоимость доставки при выкупе менее 10%'),
            'delivery_buyout_rate_2' => \Yii::t('common', 'Стоимость доставки при выкупе менее 20%'),
            'delivery_buyout_rate_3' => \Yii::t('common', 'Стоимость доставки при выкупе менее 30%'),
            'delivery_buyout_rate_4' => \Yii::t('common', 'Стоимость доставки при выкупе менее 40%'),
            'delivery_buyout_rate_5' => \Yii::t('common', 'Стоимость доставки при выкупе менее 50%'),
            'delivery_buyout_rate_6' => \Yii::t('common', 'Стоимость доставки при выкупе менее 60%'),
            'delivery_buyout_rate_7' => \Yii::t('common', 'Стоимость доставки при выкупе менее 70%'),
            'delivery_buyout_rate_8' => \Yii::t('common', 'Стоимость доставки при выкупе менее 80%'),
            'delivery_buyout_rate_9' => \Yii::t('common', 'Стоимость доставки при выкупе менее 90%'),
        ]);
    }
}