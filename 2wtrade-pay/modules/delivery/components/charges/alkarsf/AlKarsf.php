<?php

namespace app\modules\delivery\components\charges\alkarsf;


use app\models\Currency;
use app\modules\delivery\components\charges\ChargesCalculatorAbstract;
use app\modules\delivery\widgets\charges\helpers\CommonDeliveryCharges;
use app\modules\delivery\widgets\charges\helpers\DeliveryChargesForRegion;
use app\modules\order\models\OrderFinancePrediction;
use yii\helpers\ArrayHelper;

/**
 * Class AlKarsf
 * @package app\modules\delivery\components\charges\alkarsf
 */
class AlKarsf extends ChargesCalculatorAbstract
{
    public $zipcodes = [];
    public $delivery_fee_10kg = [];
    public $delivery_fee_add_100g = [];

    public $return_fee_5kg = 0;
    public $return_fee_add_100g = 0;

    public $storage_fee = 0;

    /**
     * @var int
     */
    public $monthly_administrative_charges = 0;

    /**
     * @var int
     */
    public $inventory_report_fee = 0;

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            ['zipcodes', 'each', 'rule' => ['string']],
            ['zipcodes', 'each', 'rule' => ['filter', 'filter' => [$this, 'filterZipcodes']]],
            [['delivery_fee_10kg', 'delivery_fee_add_100g'], 'each', 'rule' => ['number', 'min' => 0]],
            [
                [
                    'return_fee_5kg',
                    'return_fee_add_100g',
                    'storage_fee',
                    'monthly_administrative_charges',
                    'inventory_report_fee'
                ],
                'number',
                'min' => 0
            ],
            [['currency_char_code'], 'string'],
        ]);
    }

    protected function getDeliveryFee()
    {
        return $this->delivery_fee_10kg;
    }

    /**
     * @param \app\modules\order\models\Order $order
     * @return \app\modules\order\models\OrderFinancePrediction
     */
    public function calculate($order)
    {
        $finance = $this->getOrderFinance($order);
        $weight = $this->getWeightOfParcel($order, true);

        $delivery_fee_10kg = 0;
        $delivery_fee_add_100g = 0;
        $index = $this->getIndexByZipcode($this->zipcodes, $order->customer_zip);
        if (!empty($index)) {
            $delivery_fee_10kg = doubleval($this->delivery_fee_10kg[$index] ?? 0);
            $delivery_fee_add_100g = doubleval($this->delivery_fee_add_100g[$index] ?? 0);
        }

        $delivery_fee = $delivery_fee_10kg + ($weight > 10 ? ceil(($weight - 10) / 0.1) * $delivery_fee_add_100g : 0);
        $return_fee = doubleval($this->return_fee_5kg) + ($weight > 5 ? ceil(($weight - 5) / 0.1) * doubleval($this->return_fee_add_100g) : 0);

        $currency = $this->getCurrency();
        $currencyId = null;
        if (isset($currency)) {
            $currencyId = $currency->id;
        }
        $finance->price_storage = 0;
        $finance->price_fulfilment = 0;

        $finance->price_delivery = $delivery_fee;
        $finance->price_delivery_return = 0;
        if ($this->isOrderBuyout($order)) {
            $finance->price_cod = $order->price_total + $order->delivery;
        } else {
            if ($this->isOrderReturnToSender($order)) {
                $finance->price_delivery_return = $return_fee;
            }
            $finance->price_cod = 0;
        }

        $finance->price_storage_currency_id = $finance->price_fulfilment_currency_id = $finance->price_delivery_currency_id = $finance->price_delivery_return_currency_id = $currencyId;

        return parent::afterCalculate($finance);
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @param $onlyView
     * @return string
     */
    public function render($form, $onlyView = false)
    {
        return CommonDeliveryCharges::widget([
                'form' => $form,
                'model' => $this,
                'items' => [
                    [
                        'attribute' => 'currency_char_code',
                        'type' => 'select2List',
                        'items' => ArrayHelper::map(Currency::find()->all(), 'char_code', 'name'),
                        'options' => [
                            'length' => false,
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'return_fee_5kg',
                        'options' => [
                            'disabled' => $onlyView
                        ]
                    ],
                    [
                        'attribute' => 'return_fee_add_100g',
                        'options' => [
                            'disabled' => $onlyView
                        ]
                    ],
                    [
                        'attribute' => 'storage_fee',
                        'options' => [
                            'disabled' => $onlyView
                        ]
                    ],
                    [
                        'attribute' => 'inventory_report_fee',
                        'options' => [
                            'disabled' => $onlyView
                        ]
                    ],
                    [
                        'attribute' => 'monthly_administrative_charges',
                        'options' => [
                            'disabled' => $onlyView
                        ]
                    ]
                ],
            ]) . DeliveryChargesForRegion::widget([
                'form' => $form,
                'model' => $this,
                'onlyView' => $onlyView,
                'regionAttribute' => 'zipcodes',
                'chargesAttributes' => ['delivery_fee_10kg', 'delivery_fee_add_100g']
            ]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'delivery_fee_10kg' => \Yii::t('common', 'Стоимость доставки (менее 10кг)'),
            'delivery_fee_add_100g' => \Yii::t('common', 'Стоимость доставки (за каждые 100гр сверху)'),
            'return_fee_5kg' => \Yii::t('common', 'Стоимость возврата (менее 5кг)'),
            'return_fee_add_100g' => \Yii::t('common', 'Стоимость возврата (за каждые 100гр сверху)'),
            'storage_fee' => \Yii::t('common', 'Стоимость аренды склада'),
            'inventory_report_fee' => \Yii::t('common', 'Ежемесячная плата за отчеты'),
            'monthly_administrative_charges' => \Yii::t('common', 'Ежемесячная плата за обслуживание')
        ]);
    }

    /**
     * @param $field
     * @return array
     */
    public function getCalculatingTypeForField($field)
    {
        $answer = [];
        switch ($field) {
            case OrderFinancePrediction::COLUMN_PRICE_FULFILMENT:
                $answer = [
                    'type' => ChargesCalculatorAbstract::CALCULATING_TYPE_MONTHLY_CHARGE,
                    'charge' => (doubleval($this->monthly_administrative_charges) + doubleval($this->inventory_report_fee)),
                    'currency_id' => $this->getCurrency()->id,
                    'currency_rate' => $this->getCurrency()->currencyRate->rate,
                ];
                break;
            case OrderFinancePrediction::COLUMN_PRICE_STORAGE:
                $answer = [
                    'type' => ChargesCalculatorAbstract::CALCULATING_TYPE_MONTHLY_CHARGE,
                    'charge' => doubleval($this->storage_fee),
                    'currency_id' => $this->getCurrency()->id,
                    'currency_rate' => $this->getCurrency()->currencyRate->rate,
                ];
                break;
            default:
                $answer['type'] = static::CALCULATING_TYPE_NUMBER;
                break;
        }

        return $answer;
    }
}