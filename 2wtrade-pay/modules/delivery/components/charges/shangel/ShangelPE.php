<?php

namespace app\modules\delivery\components\charges\Shangel;

use app\models\Currency;
use app\modules\delivery\components\charges\DeliveryChargeByZip;
use app\modules\delivery\widgets\charges\helpers\CommonDeliveryCharges;
use app\modules\delivery\widgets\charges\helpers\DeliveryChargesForRegion;
use app\modules\order\models\OrderFinancePrediction;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class ShangelPE
 * @package app\modules\delivery\components\charges
 */
class ShangelPE extends DeliveryChargeByZip
{
    /**
     * @var array
     */
    public $delivery_fees_weekend = [];

    /**
     * @var array
     */
    public $delivery_fees_array_weekend = [];

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['delivery_fees_weekend'], 'each', 'rule' => ['number', 'min' => 0]],
            [['delivery_fees_array_weekend'], 'each', 'rule' => ['each', 'rule' => ['number', 'min' => 0]]],
        ]);
    }

    /**
     * @param \app\modules\order\models\Order $order
     * @return OrderFinancePrediction
     */
    public function calculate($order)
    {
        $delivery = $order->deliveryRequest;
        $finance = $this->getOrderFinance($order);
        $delivery_fee = 0;
        $return_fee = 0;
        $index = $this->getIndexByZipcode($this->zipcodes, $order->customer_zip);
        $cod_fee = $this->cod_fee;
        if (!is_null($index)) {

            $dayOfWeek = null;
            if ($dateBuyout = $order->delivery_time_from ?? $order->deliveryRequest->approved_at ?? $order->deliveryRequest->sent_at ?? 0) {
                $dayOfWeek = (new \DateTime())->setTimezone(new \DateTimeZone($order->country->timezone->timezone_id))->setTimestamp($dateBuyout)->format('N');
            }

            if ($dayOfWeek && $dayOfWeek == 6) {
                $delivery_fee = doubleval($this->delivery_fees_weekend[$index]);
            } elseif (isset($this->delivery_fees[$index])) {
                $delivery_fee = doubleval($this->delivery_fees[$index]);
            }

            if (isset($this->return_fees[$index])) {
                $return_fee = doubleval($this->return_fees[$index]);
            }
            if (isset($this->cod_fees[$index])) {
                $cod_fee = doubleval($this->cod_fees[$index]);
            }
            if ($this->enable_percent_buyout[$index]) {

                $param_id = $this->getPercentDelivery($index, $this->delivery->id, $delivery['second_sent_at'] ?? $delivery['sent_at'] ?? $delivery['created_at'] ?? null);
                if ($param_id !== null) {
                    if (!empty($this->delivery_fees_array[$index][$param_id])) {

                        if ($dayOfWeek && $dayOfWeek == 6) {
                            $delivery_fee = doubleval($this->delivery_fees_array_weekend[$index][$param_id]);
                        }
                        else {
                            $delivery_fee = doubleval($this->delivery_fees_array[$index][$param_id]);
                        }
                    }
                    if (!empty($this->return_fees_array[$index][$param_id])) {
                        $return_fee = doubleval($this->return_fees_array[$index][$param_id]);
                    }
                    if (!empty($this->cod_fees_array[$index][$param_id])) {
                        $cod_fee = doubleval($this->cod_fees_array[$index][$param_id]);
                    }
                }
            }
        }

        $currency = $this->getCurrency();
        $currencyId = null;
        if (isset($currency)) {
            $currencyId = $currency->id;
        }

        $finance->price_storage = 0;
        $finance->price_delivery = $delivery_fee;
        $finance->price_fulfilment = doubleval($this->fulfillment_fee);
        $finance->price_packing = doubleval($this->packing_fee);
        $finance->price_package = doubleval($this->packed_fee);
        $finance->price_delivery_return = 0;
        if ($this->isOrderBuyout($order)) {
            $finance->price_cod = $order->delivery + $order->price_total;
            $finance->price_cod_service = $finance->price_cod * (doubleval($cod_fee) / 100);
            if ($this->vat_type == static::VAT_TYPE_COD) {
                $finance->price_vat = $finance->price_cod * (doubleval($this->vat_percent) / 100);
            }
        } else {
            if ($this->isOrderReturnToSender($order)) {
                $finance->price_delivery_return = doubleval($return_fee);
                $finance->price_delivery = 0;
            }
            $finance->price_cod = 0;
            $finance->price_cod_service = 0;
            if ($this->vat_type == static::VAT_TYPE_COD) {
                $finance->price_vat = 0;
            }
        }
        if ($this->vat_type == static::VAT_TYPE_SERVICE) {
            $chargesSum = $finance->price_delivery + $finance->price_delivery_return + $finance->price_storage + $finance->price_fulfilment + $finance->price_packing + $finance->price_package;
            $finance->price_vat = $chargesSum * (doubleval($this->vat_percent) / 100);
            $finance->price_vat_currency_id = $currencyId;
        }

        $finance->price_delivery_currency_id = $finance->price_fulfilment_currency_id = $finance->price_packing_currency_id = $finance->price_package_currency_id = $finance->price_storage_currency_id = $currencyId;

        return parent::afterCalculate($finance);
    }


    /**
     * @param \app\components\widgets\ActiveForm $form
     * @param bool $onlyView
     * @return string
     * @throws \Exception
     */
    public function render($form, $onlyView = false)
    {
        return CommonDeliveryCharges::widget([
                'form' => $form,
                'model' => $this,
                'items' => [
                    [
                        'attribute' => 'fulfillment_fee',
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'packing_fee',
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'packed_fee',
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'cod_fee',
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'vat_percent',
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'vat_type',
                        'type' => 'select2List',
                        'items' => static::getVatTypes(),
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'currency_char_code',
                        'type' => 'select2List',
                        'items' => ArrayHelper::map(Currency::find()->all(), 'char_code', 'name'),
                        'length' => 2,
                        'options' => [
                            'length' => false,
                            'disabled' => $onlyView
                        ]
                    ],
                    [
                        'length' => 3,
                    ],
                    [
                        'attribute' => 'storage_area',
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'storage_fee',
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                ]
            ])
            . DeliveryChargesForRegion::widget([
                'form' => $form,
                'model' => $this,
                'onlyView' => $onlyView,
                'regionAttribute' => 'zipcodes',
                'chargesAttributes' => [
                    'delivery_fees',
                    'delivery_fees_weekend',
                    'return_fees',
                    'cod_fees',
                    'enable_percent_buyout'
                ],
                'chargesArrayAttributes' => [
                    [
                        'attribute' => 'delivery_fees_array',
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'delivery_fees_array_weekend',
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'return_fees_array',
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'cod_fees_array',
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'percent_buyout_from',
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'percent_buyout_to',
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'day_from',
                        'type' => 'select2List',
                        'items' => array_combine(range(1, 31), range(1, 31)),
                        'length' => 2,
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'day_to',
                        'type' => 'select2List',
                        'items' => array_combine(range(1, 31), range(1, 31)),
                        'length' => 2,
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                ]
            ]);
    }


    /**
     * @return array
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'delivery_fees_weekend' => Yii::t('common', 'Стоимость доставки в сб.'),
            'delivery_fees_array_weekend' => Yii::t('common', 'Стоимость доставки в сб.'),
        ]);
    }
}