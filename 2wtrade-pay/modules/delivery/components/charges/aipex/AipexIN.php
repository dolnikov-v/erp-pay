<?php

namespace app\modules\delivery\components\charges\aipex;


use app\models\Currency;
use app\modules\delivery\components\charges\ChargesCalculatorAbstract;
use app\modules\delivery\widgets\charges\helpers\CommonDeliveryCharges;
use app\modules\delivery\widgets\charges\helpers\DeliveryChargesForRegion;
use app\modules\order\models\OrderFinancePrediction;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class AipexIN
 * @package app\modules\delivery\components\charges\aipex
 */
class AipexIN extends ChargesCalculatorAbstract
{
    public $zipcodes = [];
    public $delivery_fees_1_5kg = [];
    public $delivery_fees_5_10kg = [];
    public $delivery_fees_10_15kg = [];
    public $delivery_fees_15_20kg = [];
    public $delivery_fees_20_50kg = [];
    public $delivery_fees_over_50kg = [];

    public $cod_fee;
    public $cod_fee_min;

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            ['zipcodes', 'each', 'rule' => ['string']],
            ['zipcodes', 'each', 'rule' => ['filter', 'filter' => [$this, 'filterZipcodes']]],
            [
                [
                    'delivery_fees_1_5kg',
                    'delivery_fees_5_10kg',
                    'delivery_fees_10_15kg',
                    'delivery_fees_15_20kg',
                    'delivery_fees_20_50kg',
                    'delivery_fees_over_50kg'
                ],
                'each',
                'rule' => ['number', 'min' => 0]
            ],
            [['currency_char_code'], 'string'],
            [['cod_fee_min'], 'number', 'min' => 0],
            [['cod_fee'], 'number', 'min' => 0, 'max' => 100],
        ]);
    }

    protected function getDeliveryFee()
    {
        return $this->delivery_fees_1_5kg;
    }

    /**
     * @param \app\modules\order\models\Order $order
     * @return \app\modules\order\models\OrderFinancePrediction
     */
    public function calculate($order)
    {
        $finance = $this->getOrderFinance($order);
        $weight = $this->getWeightOfParcel($order, true);
        $delivery_fee_1_5kg = 0;
        $delivery_fee_5_10kg = 0;
        $delivery_fee_10_15kg = 0;
        $delivery_fee_15_20kg = 0;
        $delivery_fee_20_50kg = 0;
        $delivery_fee_over_50kg = 0;
        $index = $this->getIndexByZipcode($this->zipcodes, $order->customer_zip);

        if (!is_null($index)) {
            $delivery_fee_1_5kg = $this->delivery_fees_1_5kg[$index] ?? 0;
            $delivery_fee_5_10kg = $this->delivery_fees_5_10kg[$index] ?? 0;
            $delivery_fee_10_15kg = $this->delivery_fees_10_15kg[$index] ?? 0;
            $delivery_fee_15_20kg = $this->delivery_fees_15_20kg[$index] ?? 0;
            $delivery_fee_20_50kg = $this->delivery_fees_20_50kg[$index] ?? 0;
            $delivery_fee_over_50kg = $this->delivery_fees_over_50kg[$index] ?? 0;
        }

        if ($weight < 5) {
            $finance->price_delivery = $delivery_fee_1_5kg;
        } elseif ($weight < 10) {
            $finance->price_delivery = $delivery_fee_5_10kg;
        } elseif ($weight < 15) {
            $finance->price_delivery = $delivery_fee_10_15kg;
        } elseif ($weight < 20) {
            $finance->price_delivery = $delivery_fee_15_20kg;
        } elseif ($weight <= 50) {
            $finance->price_delivery = $delivery_fee_20_50kg;
        } else {
            $finance->price_delivery = $delivery_fee_over_50kg;
        }

        $finance->price_delivery = doubleval($finance->price_delivery);
        $currency = $this->getCurrency();
        $currencyId = null;
        if (isset($currency)) {
            $currencyId = $currency->id;
        }

        if ($this->isOrderBuyout($order)) {
            $rate = $this->getCurrencyRate();
            $finance->price_cod = $order->price_total + $order->delivery;
            $item2 = $finance->price_cod * (doubleval($this->cod_fee) / 100);
            if (($this->cod_fee_min * $rate) > $item2) {
                $finance->price_cod_service = doubleval($this->cod_fee_min);
                $finance->price_cod_service_currency_id = $currencyId;
            } else {
                $finance->price_cod_service = $item2;
            }
        } else {
            $finance->price_cod = 0;
            $finance->price_cod_service = 0;
        }

        $finance->price_delivery_currency_id = $currencyId;

        return parent::afterCalculate($finance);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'delivery_fees_1_5kg' => Yii::t('common', 'Стоимость доставки (1-5кг.)'),
            'delivery_fees_5_10kg' => Yii::t('common', 'Стоимость доставки (5-10кг.)'),
            'delivery_fees_10_15kg' => Yii::t('common', 'Стоимость доставки (10-15кг.)'),
            'delivery_fees_15_20kg' => Yii::t('common', 'Стоимость доставки (15-20кг.)'),
            'delivery_fees_20_50kg' => Yii::t('common', 'Стоимость доставки (20-50кг.)'),
            'delivery_fees_over_50kg' => Yii::t('common', 'Стоимость доставки (более 50кг.)'),
            'cod_fee_min' => Yii::t('common', 'Минимальная плата за наложенный платеж')
        ]);
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @param bool $onlyView
     * @return string
     */
    public function render($form, $onlyView = false)
    {
        return CommonDeliveryCharges::widget([
                'model' => $this,
                'form' => $form,
                'items' => [
                    [
                        'attribute' => 'cod_fee_min',
                        'length' => 4,
                        'options' => [
                            'disabled' => $onlyView
                        ]
                    ],
                    [
                        'attribute' => 'cod_fee',
                        'options' => [
                            'disabled' => $onlyView,
                        ]
                    ],
                    [
                        'attribute' => 'currency_char_code',
                        'type' => 'select2List',
                        'items' => ArrayHelper::map(Currency::find()->all(), 'char_code', 'name'),
                        'length' => 2,
                        'options' => [
                            'length' => false,
                            'disabled' => $onlyView,
                        ]
                    ]
                ]
            ]) . DeliveryChargesForRegion::widget([
                'model' => $this,
                'form' => $form,
                'onlyView' => $onlyView,
                'regionAttribute' => 'zipcodes',
                'chargesAttributes' => [
                    'delivery_fees_1_5kg',
                    'delivery_fees_5_10kg',
                    'delivery_fees_10_15kg',
                    'delivery_fees_15_20kg',
                    'delivery_fees_20_50kg',
                    'delivery_fees_over_50kg'
                ]
            ]);
    }


    /**
     * @param $field
     * @return array
     */
    public function getCalculatingTypeForField($field)
    {
        $currency = $this->getCurrency();
        $answer = [];
        switch ($field) {
            case OrderFinancePrediction::COLUMN_PRICE_COD_SERVICE:
                $answer = [
                    'type' => static::CALCULATING_TYPE_PERCENT,
                    'visible_formula' => Yii::t('common', 'МИН({val1}, {val2}', [
                        'val1' => "{$this->cod_fee}%",
                        'val2' => "{$this->cod_fee_min} {$currency->char_code}"
                    ]),
                    'fields' => [
                        OrderFinancePrediction::COLUMN_PRICE_COD,
                    ],
                ];
                break;
            default:
                $answer['type'] = static::CALCULATING_TYPE_NUMBER;
                break;
        }

        return $answer;
    }
}