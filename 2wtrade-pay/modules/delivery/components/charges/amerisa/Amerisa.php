<?php

namespace app\modules\delivery\components\charges\amerisa;


use app\models\Currency;
use app\modules\delivery\components\charges\ChargesCalculatorAbstract;
use app\modules\delivery\widgets\charges\helpers\CommonDeliveryCharges;
use app\modules\order\models\OrderFinancePrediction;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class Amerisa
 * @package app\modules\delivery\components\charges\amerisa
 */
class Amerisa extends ChargesCalculatorAbstract
{
    /**
     * @var int
     */
    public $fulfillment_fee = 0;
    /**
     * @var int
     */
    public $delivery_fee_less_1k = 0;
    /**
     * @var int
     */
    public $delivery_fee_more_1k = 0;
    /**
     * @var int
     */
    public $return_fee = 0;
    /**
     * @var int
     */
    public $cod_fee = 0;

    /**
     * @var int
     */
    public $vat_percent = 0;

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['fulfillment_fee', 'delivery_fee_less_1k', 'delivery_fee_more_1k', 'return_fee'], 'number', 'min' => 0],
            [['cod_fee', 'vat_percent'], 'number', 'min' => 0, 'max' => 100],
            [['currency_char_code'], 'string'],
        ]);
    }

    /**
     * @param \app\modules\order\models\Order $order
     * @return OrderFinancePrediction
     */
    public function calculate($order)
    {
        $finance = $this->getOrderFinance($order);

        $currency = $this->getCurrency();
        $currencyId = null;
        if (isset($currency)) {
            $currencyId = $currency->id;
        }
        $finance->price_fulfilment = doubleval($this->fulfillment_fee);

        $periods = $this->getOrdersCountByMonths();
        $month = $this->getOrderSendingMonth($order);
        $finance->price_delivery = (isset($periods[$month]) && $periods[$month] >= 1000 ? doubleval($this->delivery_fee_more_1k) : doubleval($this->delivery_fee_less_1k));
        if ($this->isOrderBuyout($order)) {
            $finance->price_cod = $order->delivery + $order->price_total;
            $finance->price_cod_service = $finance->price_cod * (doubleval($this->cod_fee) / 100);
        } else {
            if ($this->isOrderReturnToSender($order)) {
                $finance->price_delivery = doubleval($this->return_fee);
            }
            $finance->price_cod = 0;
            $finance->price_cod_service = 0;
        }
        $rate = $this->getCurrencyRate();
        $finance->price_vat = ($finance->price_delivery + $finance->price_fulfilment + ($finance->price_cod_service / $rate)) * (doubleval($this->vat_percent) / 100);

        $finance->price_fulfilment_currency_id = $finance->price_delivery_currency_id = $finance->price_vat_currency_id = $currencyId;

        return parent::afterCalculate($finance);
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @param $onlyView
     * @return string
     */
    public function render($form, $onlyView = false)
    {
        return CommonDeliveryCharges::widget([
            'form' => $form,
            'model' => $this,
            'items' => [
                [
                    'attribute' => 'currency_char_code',
                    'type' => 'select2List',
                    'items' => ArrayHelper::map(Currency::find()->all(), 'char_code', 'name'),
                    'options' => [
                        'length' => false,
                        'disabled' => $onlyView,
                    ]
                ],
                [
                    'attribute' => 'fulfillment_fee',
                    'options' => [
                        'disabled' => $onlyView
                    ]
                ],
                [
                    'attribute' => 'delivery_fee_less_1k',
                    'options' => [
                        'disabled' => $onlyView
                    ]
                ],
                [
                    'attribute' => 'delivery_fee_more_1k',
                    'options' => [
                        'disabled' => $onlyView
                    ]
                ],
                [
                    'attribute' => 'return_fee',
                    'options' => [
                        'disabled' => $onlyView
                    ]
                ],
                [
                    'attribute' => 'cod_fee',
                    'options' => [
                        'disabled' => $onlyView
                    ]
                ],
                [
                    'attribute' => 'vat_percent',
                    'options' => [
                        'disabled' => $onlyView
                    ]
                ]
            ],
        ]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'delivery_fee_less_1k' => Yii::t('common', 'Стоимость доставки (1-999 заказов)'),
            'delivery_fee_more_1k' => Yii::t('common', 'Стоимость доставки (более 1000 заказов)'),
            'return_fee' => Yii::t('common', 'Стоимость доставки (невыкуп)'),
        ]);
    }

    /**
     * @param $field
     * @return array
     */
    public function getCalculatingTypeForField($field)
    {
        $answer = [];
        switch ($field) {
            case OrderFinancePrediction::COLUMN_PRICE_COD_SERVICE:
                $answer = [
                    'type' => static::CALCULATING_TYPE_PERCENT,
                    'visible_formula' => "$this->cod_fee%",
                    'fields' => [
                        OrderFinancePrediction::COLUMN_PRICE_COD,
                    ],
                ];
                break;
            case OrderFinancePrediction::COLUMN_PRICE_VAT:
                $answer['type'] = static::CALCULATING_TYPE_PERCENT;
                $answer['visible_formula'] = "$this->vat_percent%";
                $answer['fields'] = [
                    OrderFinancePrediction::COLUMN_PRICE_DELIVERY,
                    OrderFinancePrediction::COLUMN_PRICE_DELIVERY_RETURN,
                    OrderFinancePrediction::COLUMN_PRICE_STORAGE,
                    OrderFinancePrediction::COLUMN_PRICE_FULFILMENT,
                    OrderFinancePrediction::COLUMN_PRICE_PACKAGE,
                    OrderFinancePrediction::COLUMN_PRICE_PACKING,
                    OrderFinancePrediction::COLUMN_PRICE_COD_SERVICE,
                ];
                break;
            default:
                $answer['type'] = static::CALCULATING_TYPE_NUMBER;
                break;
        }

        return $answer;
    }
}