<?php

namespace app\modules\delivery\components\charges\ups;

use app\components\db\ActiveQuery;
use app\models\Currency;
use app\modules\delivery\models\DeliveryContract;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\delivery\widgets\charges\helpers\CommonDeliveryCharges;
use app\modules\delivery\widgets\charges\helpers\DeliveryChargesForRegion;
use app\modules\order\models\Order;
use app\modules\order\models\OrderFinancePrediction;
use Yii;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * Class UpsMX
 * @package app\modules\delivery\components\charges
 */
class UpsMX extends Ups
{
    /**
     * @var array
     */
    public $deliveryDeduction1 = [];

    /**
     * @var array
     */
    public $deliveryDeduction2 = [];

    /**
     * @var array
     */
    public $deliveryDeduction3 = [];

    /**
     * @var array
     */
    public $dataByMonth = [];

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['deliveryDeduction1', 'deliveryDeduction2', 'deliveryDeduction3'], 'each', 'rule' => ['number', 'min' => 0, 'max' => 100]],
        ]);
    }

    /**
     * @param \app\modules\order\models\Order $order
     * @param bool $considerSumPrice
     * @return OrderFinancePrediction
     */
    public function calculate($order, $considerSumPrice = false)
    {
        $finance = $this->getOrderFinance($order);
        $delivery_fee = $this->delivery_fees[0] ?? 0;
        $return_fee = $this->return_fees[0] ?? 0;
        $index = $this->getIndexByZipcode($this->zipcodes, $order->customer_zip);
        if (!is_null($index)) {
            if (isset($this->delivery_fees[$index])) {
                $delivery_fee = doubleval($this->delivery_fees[$index]);
            }
            if (isset($this->return_fees[$index])) {
                $return_fee = doubleval($this->return_fees[$index]);
            }
        }

        $currency = $this->getCurrency();
        $currencyId = null;
        if (isset($currency)) {
            $currencyId = $currency->id;
        }

        if ($considerSumPrice) {
            $deliveryRequestTime = $order->deliveryRequest->second_sent_at ?? $order->deliveryRequest->sent_at ?? $order->deliveryRequest->created_at;
            $month = date('m', $deliveryRequestTime);
            if (!empty($this->dataByMonth[$month])) {
                $monthSum = $this->dataByMonth[$month];
            } else {
                $contract = DeliveryContract::find()
                    ->byDeliveryId($order->deliveryRequest->delivery_id)
                    ->byDates(date('Y-m-d', $deliveryRequestTime), date('Y-m-d', $deliveryRequestTime))
                    ->one();

                if (empty($contract->date_from) || $contract->date_from < date("Y-{$month}-01")) {
                    $start = strtotime(date("01.{$month}.Y"));
                } else {
                    $start = strtotime($contract->date_from);
                }
                if (empty($contract->date_to) || $contract->date_to > date("Y-{$month}-t")) {
                    $end = strtotime(date("t.{$month}.Y"));
                } else {
                    $end = strtotime($contract->date_to);
                }
                $dataMonth = OrderFinancePrediction::find()->alias('ofp')
                    ->select(new Expression('sum(`convertToCurrencyByCountry`(ofp.`price_delivery`, ofp.`price_delivery_currency_id`, :currency_report)) as sum_price_delivery,
                sum(`convertToCurrencyByCountry`(ofp.`price_delivery_return`, ofp.`price_delivery_return_currency_id`, :currency_report)) as sum_price_delivery_return', [':currency_report' => intval(1)]))
                    ->innerJoin(DeliveryRequest::clearTableName() . ' dr', new Expression('dr.order_id = ofp.order_id 
                and COALESCE(dr.second_sent_at, dr.sent_at, dr.created_at) >= :start_contract
                and COALESCE(dr.second_sent_at, dr.sent_at, dr.created_at) < :end_contract and dr.delivery_id = :delivery_id',
                        [':start_contract' => $start, 'end_contract' => $end, ':delivery_id' => $order->deliveryRequest->delivery_id]))
                    ->asArray()->one();
                $monthSum = (float)($dataMonth['sum_price_delivery'] ?? 0) + (float)($dataMonth['sum_price_delivery_return'] ?? 0);
                $this->dataByMonth[$month] = $monthSum;
            }
            if ($monthSum < 600000 && is_numeric($this->deliveryDeduction1)) {
                $delivery_fee = $delivery_fee * (1 - (float)$this->deliveryDeduction1 / 100);
            } elseif ($monthSum < 1000000 && is_numeric($this->deliveryDeduction2)) {
                $delivery_fee = $delivery_fee * (1 - (float)$this->deliveryDeduction2 / 100);
            } elseif ($monthSum >= 1000000 && is_numeric($this->deliveryDeduction3)) {
                $delivery_fee = $delivery_fee * (1 - (float)$this->deliveryDeduction3 / 100);
            }
        }

        $finance->price_delivery = $delivery_fee;
        $finance->price_cod = $finance->price_delivery_return = $finance->price_cod_service = $finance->price_vat = 0;
        if ($this->isOrderBuyout($order)) {
            $finance->price_cod = $order->delivery + $order->price_total;
        } else {
            if ($this->isOrderReturnToSender($order)) {
                $finance->price_delivery_return = doubleval($return_fee);
            }
        }

        $finance->price_delivery_currency_id = $finance->price_cod_currency_id = $finance->price_delivery_return_currency_id = $currencyId;

        return parent::afterCalculate($finance);
    }

    /**
     * @param ActiveQuery $query
     */
    public function calculateForBatch($query)
    {
        $this->dataByMonth = [];
        foreach ($query->batch(1000) as $orders) {
            /**
             * @var Order[] $orders
             */
            foreach ($orders as $order) {
                $finance = $this->calculate($order);
                $finance->save();
            }
        }
        foreach ($query->batch(1000) as $orders) {
            foreach ($orders as $order) {
                $finance = $this->calculate($order, true);
                $finance->save();
            }
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @param bool $onlyView
     * @return string
     * @throws \Exception
     */
    public function render($form, $onlyView = false)
    {
        return CommonDeliveryCharges::widget([
                'form' => $form,
                'model' => $this,
                'items' => [
                    [
                        'attribute' => 'currency_char_code',
                        'type' => 'select2List',
                        'items' => ArrayHelper::map(Currency::find()->all(), 'char_code', 'name'),
                        'length' => 2,
                        'options' => [
                            'length' => false,
                            'disabled' => $onlyView
                        ]
                    ],
                ]
            ])
            . DeliveryChargesForRegion::widget([
                'form' => $form,
                'model' => $this,
                'onlyView' => $onlyView,
                'regionAttribute' => 'zipcodes',
                'chargesAttributes' => [
                    'delivery_fees',
                    'return_fees',
                    'deliveryDeduction1',
                    'deliveryDeduction2',
                    'deliveryDeduction3'
                ],
            ]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'deliveryDeduction1' => Yii::t('common', 'Вычет % при расходах КС <600к'),
            'deliveryDeduction2' => Yii::t('common', 'Вычет % при расходах КС <1кк'),
            'deliveryDeduction3' => Yii::t('common', 'Вычет % при расходах КС >=1кк'),
        ]);
    }
}