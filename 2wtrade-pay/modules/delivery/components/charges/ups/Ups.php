<?php

namespace app\modules\delivery\components\charges\ups;

use app\models\Currency;
use app\modules\delivery\components\charges\ChargesCalculatorAbstract;
use app\modules\delivery\widgets\charges\helpers\CommonDeliveryCharges;
use app\modules\delivery\widgets\charges\helpers\DeliveryChargesForRegion;
use app\modules\order\models\OrderFinancePrediction;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class Ups
 * @package app\modules\delivery\components\charges
 */
class Ups extends ChargesCalculatorAbstract
{
    /**
     * @var array
     */
    public $zipcodes = [];

    /**
     * @var array
     */
    public $delivery_fees = [];

    /**
     * @var array
     */
    public $return_fees = [];

    /**
     * @var string
     */
    public $currency_char_code = null;

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            ['zipcodes', 'each', 'rule' => ['string']],
            ['zipcodes', 'each', 'rule' => ['filter', 'filter' => [$this, 'filterZipcodes']]],
            [['delivery_fees', 'return_fees'], 'each', 'rule' => ['number', 'min' => 0]],
            [['currency_char_code', 'vat_type'], 'string'],
        ]);
    }

    /**
     * @param \app\modules\order\models\Order $order
     * @return OrderFinancePrediction
     */
    public function calculate($order)
    {
        $finance = $this->getOrderFinance($order);
        $delivery_fee = $this->delivery_fees[0] ?? 0;
        $return_fee = $this->return_fees[0] ?? 0;
        $index = $this->getIndexByZipcode($this->zipcodes, $order->customer_zip);
        if (!is_null($index)) {
            if (isset($this->delivery_fees[$index])) {
                $delivery_fee = doubleval($this->delivery_fees[$index]);
            }
            if (isset($this->return_fees[$index])) {
                $return_fee = doubleval($this->return_fees[$index]);
            }
        }

        $currency = $this->getCurrency();
        $currencyId = null;
        if (isset($currency)) {
            $currencyId = $currency->id;
        }

        $finance->price_delivery = $delivery_fee;
        $finance->price_cod = $finance->price_delivery_return = $finance->price_cod_service = $finance->price_vat = 0;
        if ($this->isOrderBuyout($order)) {
            $finance->price_cod = $order->delivery + $order->price_total;
        } else {
            if ($this->isOrderReturnToSender($order)) {
                $finance->price_delivery_return = doubleval($return_fee);
            }
        }

        $finance->price_delivery_currency_id = $finance->price_cod_currency_id = $finance->price_delivery_return_currency_id = $currencyId;

        return parent::afterCalculate($finance);
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @param bool $onlyView
     * @return string
     * @throws \Exception
     */
    public function render($form, $onlyView = false)
    {
        return CommonDeliveryCharges::widget([
                'form' => $form,
                'model' => $this,
                'items' => [
                    [
                        'attribute' => 'currency_char_code',
                        'type' => 'select2List',
                        'items' => ArrayHelper::map(Currency::find()->all(), 'char_code', 'name'),
                        'length' => 2,
                        'options' => [
                            'length' => false,
                            'disabled' => $onlyView
                        ]
                    ],
                ]
            ])
            . DeliveryChargesForRegion::widget([
                'form' => $form,
                'model' => $this,
                'onlyView' => $onlyView,
                'regionAttribute' => 'zipcodes',
                'chargesAttributes' => ['delivery_fees', 'return_fees'],
            ]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'return_fees' => Yii::t('common', 'Стоимость возврата'),
            'delivery_fees' => Yii::t('common', 'Стоимость доставки'),
        ]);
    }

    /**
     * @param $field
     * @return array
     */
    public function getCalculatingTypeForField($field)
    {
        $answer = [];
        switch ($field) {
            default:
                $answer['type'] = static::CALCULATING_TYPE_NUMBER;
                break;
        }

        return $answer;
    }
}