<?php

namespace app\modules\delivery\components\charges\nex;


use app\modules\delivery\components\charges\ChargesCalculatorAbstract;
use app\modules\delivery\widgets\charges\nex\NexWidget;
use app\modules\order\models\OrderFinancePrediction;
use Yii;

/**
 * Class Nex
 * @package app\modules\delivery\components\charges\nex
 */
class Nex extends ChargesCalculatorAbstract
{
    public $cod_fee = 0;
    public $package_fee = 0;
    public $delivery_fees = [];
    public $zipcodes = [];
    public $storage_fee = 0;
    public $insurance_percent = 0;
    public $vat_percent = 0;

    protected $countOfOrderByPeriods = null;

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['cod_fee', 'insurance_percent', 'vat_percent'], 'number', 'min' => 0, 'max' => 100],
            [['package_fee', 'storage_fee'], 'number', 'min' => 0],
            ['zipcodes', 'each', 'rule' => ['string']],
            ['zipcodes', 'each', 'rule' => ['filter', 'filter' => [$this, 'filterZipcodes']]],
            ['delivery_fees', 'each', 'rule' => ['number', 'min' => 0]],
            [['currency_char_code'], 'string'],
        ]);
    }

    /**
     * @param \app\modules\order\models\Order $order
     * @return OrderFinancePrediction
     */
    public function calculate($order)
    {
        $finance = $this->getOrderFinance($order);
        $delivery_fee = 0;
        $index = $this->getIndexByZipcode($this->zipcodes, $order->customer_zip);
        if (!empty($index)) {
            $delivery_fee = doubleval($this->delivery_fees[$index] ?? 0);
        }

        $finance->price_storage = 0;

        $finance->price_package = doubleval($this->package_fee);
        $finance->price_delivery = $delivery_fee;
        if (!$this->isOrderBuyout($order)) {
            $finance->price_cod = 0;
            $finance->price_cod_service = 0;
            $finance->price_vat = 0;
        } else {
            $finance->price_cod = $order->price_total + $order->delivery;
            $finance->price_cod_service = $finance->price_cod * (doubleval($this->cod_fee) / 100);
            $finance->price_vat = $finance->price_cod * (doubleval($this->vat_percent) / 100);
        }
        return parent::afterCalculate($finance);
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @param $onlyView
     * @return string
     */
    public function render($form, $onlyView = false)
    {
        return NexWidget::widget(['form' => $form, 'chargesCalculator' => $this, 'currencies' => self::$currencies, 'onlyView' => $onlyView]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        $currencyCode = $this->delivery->country->currency->char_code;
        return [
            'cod_fee' => Yii::t('common', 'Плата за наложенный платеж (%)'),
            'vat_percent' => Yii::t('common', 'НДС (%)'),
            'package_fee' => Yii::t('common', 'Цена за упаковку ({currency})', ['currency' => $currencyCode]),
            'storage_fee' => Yii::t('common', 'Плата за аренду склада ({currency})', ['currency' => $currencyCode]),
            'delivery_fees' => Yii::t('common', 'Цена за доставку ({currency})', ['currency' => $currencyCode]),
            'insurance_percent' => Yii::t('common', 'Страховка (%)'),
            'zipcodes' => Yii::t('common', 'Индексы региона'),
        ];
    }


    /**
     * @param $field
     * @return array
     */
    public function getCalculatingTypeForField($field)
    {
        $answer = [];
        switch ($field) {
            case OrderFinancePrediction::COLUMN_PRICE_COD_SERVICE:
                $answer = [
                    'type' => static::CALCULATING_TYPE_PERCENT,
                    'visible_formula' => "$this->cod_fee%",
                    'fields' => [
                        OrderFinancePrediction::COLUMN_PRICE_COD,
                    ],
                ];
                break;
            case OrderFinancePrediction::COLUMN_PRICE_VAT:
                $answer['type'] = static::CALCULATING_TYPE_PERCENT;
                $answer['visible_formula'] = "$this->vat_percent%";
                $answer['fields'] = [
                    OrderFinancePrediction::COLUMN_PRICE_COD,
                ];
                break;
            case OrderFinancePrediction::COLUMN_PRICE_STORAGE:
                $answer = [
                    'type' => ChargesCalculatorAbstract::CALCULATING_TYPE_MONTHLY_CHARGE,
                    'charge' => doubleval($this->storage_fee),
                    'currency_id' => $this->getCurrency()->id,
                    'currency_rate' => $this->getCurrency()->currencyRate->rate,
                ];
                break;
            default:
                $answer['type'] = static::CALCULATING_TYPE_NUMBER;
                break;
        }

        return $answer;
    }
}