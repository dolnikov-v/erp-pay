<?php

namespace app\modules\delivery\components\charges;

use app\models\Currency;
use app\modules\delivery\widgets\charges\helpers\CommonDeliveryCharges;
use app\modules\delivery\widgets\charges\helpers\DeliveryChargesForRegion;
use app\modules\order\models\OrderFinancePrediction;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class DeliveryChargeByCity
 * @package app\modules\delivery\components\charges
 */
class DeliveryChargeByCity extends ChargesCalculatorAbstract
{
    /**
     * @var array
     */
    public $zipcodes = [];

    /**
     * @var array
     */
    public $delivery_fees = [];

    /**
     * @var array
     */
    public $return_fees = [];

    /**
     * @var int
     */
    public $cod_fee = 0;

    /**
     * @var array
     */
    public $cod_fees = [];

    /**
     * @var int
     */
    public $vat_percent = 0;

    /**
     * @var int
     */
    public $fulfillment_fee = 0;

    /**
     * @var int
     */
    public $packing_fee = 0;

    /**
     * @var int
     */
    public $packed_fee = 0;

    /**
     * @var int
     */
    public $storage_fee = 0;

    /**
     * @var int
     */
    public $storage_area = 0;

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            ['zipcodes', 'each', 'rule' => ['string']],
            ['zipcodes', 'each', 'rule' => ['filter', 'filter' => [$this, 'filterZipcodes']]],
            [['delivery_fees', 'return_fees'], 'each', 'rule' => ['number', 'min' => 0]],
            [['currency_char_code', 'vat_type'], 'string'],
            [['cod_fee', 'vat_percent'], 'number', 'min' => 0, 'max' => 100],
            [['cod_fees'], 'each', 'rule' => ['number', 'min' => 0, 'max' => 100]],
            [['fulfillment_fee', 'packing_fee', 'packed_fee', 'storage_fee', 'storage_area'], 'number', 'min' => 0],
        ]);
    }

    /**
     * @param \app\modules\order\models\Order $order
     * @return OrderFinancePrediction
     */
    public function calculate($order)
    {
        $finance = $this->getOrderFinance($order);
        $delivery_fee = 0;
        $return_fee = 0;
        $cod_fee = $this->cod_fee;
        $index = $this->getIndexByZipcode($this->zipcodes, $order->customer_city);
        if (!is_null($index)) {
            if (isset($this->delivery_fees[$index])) {
                $delivery_fee = doubleval($this->delivery_fees[$index]);
            } else {
                $delivery_fee = 0;
            }
            if (isset($this->return_fees[$index])) {
                $return_fee = doubleval($this->return_fees[$index]);
            } else {
                $return_fee = 0;
            }
            if (isset($this->cod_fees[$index])) {
                $cod_fee = doubleval($this->cod_fees[$index]);
            }
        }

        $currency = $this->getCurrency();
        $currencyId = null;
        if (isset($currency)) {
            $currencyId = $currency->id;
        }

        $finance->price_storage = 0;
        $finance->price_delivery = $delivery_fee;
        $finance->price_fulfilment = doubleval($this->fulfillment_fee);
        $finance->price_packing = doubleval($this->packing_fee);
        $finance->price_package = doubleval($this->packed_fee);
        $finance->price_delivery_return = 0;
        if ($this->isOrderBuyout($order)) {
            $finance->price_cod = $order->delivery + $order->price_total;
            $finance->price_cod_service = $finance->price_cod * (doubleval($cod_fee) / 100);
            if ($this->vat_type == static::VAT_TYPE_COD) {
                $finance->price_vat = $finance->price_cod * (doubleval($this->vat_percent) / 100);
            }
        } else {
            if ($this->isOrderReturnToSender($order)) {
                $finance->price_delivery_return = doubleval($return_fee);
            }
            $finance->price_cod = 0;
            $finance->price_cod_service = 0;
            if ($this->vat_type == static::VAT_TYPE_COD) {
                $finance->price_vat = 0;
            }
        }
        if ($this->vat_type == static::VAT_TYPE_SERVICE) {
            $chargesSum = $finance->price_delivery + $finance->price_delivery_return + $finance->price_storage + $finance->price_fulfilment + $finance->price_packing + $finance->price_package;
            $finance->price_vat = $chargesSum * (doubleval($this->vat_percent) / 100);
            $finance->price_vat_currency_id = $currencyId;
        }

        $finance->price_delivery_currency_id = $finance->price_fulfilment_currency_id = $finance->price_packing_currency_id = $finance->price_package_currency_id = $finance->price_storage_currency_id = $currencyId;

        return parent::afterCalculate($finance);
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @param bool $onlyView
     * @return string
     * @throws \Exception
     */
    public function render($form, $onlyView = false)
    {
        return CommonDeliveryCharges::widget([
                'form' => $form,
                'model' => $this,
                'items' => [
                    [
                        'attribute' => 'fulfillment_fee',
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'packing_fee',
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'packed_fee',
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'cod_fee',
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'vat_percent',
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'vat_type',
                        'type' => 'select2List',
                        'items' => static::getVatTypes(),
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'currency_char_code',
                        'type' => 'select2List',
                        'items' => ArrayHelper::map(Currency::find()->all(), 'char_code', 'name'),
                        'length' => 2,
                        'options' => [
                            'length' => false,
                            'disabled' => $onlyView
                        ]
                    ],
                    [
                        'length' => 3,
                    ],
                    [
                        'attribute' => 'storage_area',
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                    [
                        'attribute' => 'storage_fee',
                        'options' => [
                            'disabled' => $onlyView
                        ],
                    ],
                ]
            ])
            . DeliveryChargesForRegion::widget([
                'form' => $form,
                'model' => $this,
                'onlyView' => $onlyView,
                'regionAttribute' => 'zipcodes',
                'chargesAttributes' => ['delivery_fees', 'return_fees', 'cod_fees']
            ]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'return_fees' => Yii::t('common', 'Стоимость возврата'),
            'packing_fee' => Yii::t('common', 'Стоимость упаковывания'),
            'packed_fee' => Yii::t('common', 'Стоимость упаковки'),
            'storage_area' => Yii::t('common', 'Площадь склада (м2)'),
            'storage_fee' => Yii::t('common', 'Стоимость аренды склада (1м2)'),
            'cod_fees' => Yii::t('common', 'Наложенный платеж (%)'),
            'zipcodes' => Yii::t('common', 'Города'),
        ]);
    }

    /**
     * @param $field
     * @return array
     */
    public function getCalculatingTypeForField($field)
    {
        $answer = [];
        switch ($field) {
            case OrderFinancePrediction::COLUMN_PRICE_COD_SERVICE:
                $answer = [
                    'type' => static::CALCULATING_TYPE_PERCENT,
                    'visible_formula' => "$this->cod_fee%",
                    'fields' => [
                        OrderFinancePrediction::COLUMN_PRICE_COD,
                    ],
                ];
                break;
            case OrderFinancePrediction::COLUMN_PRICE_VAT:
                $answer['type'] = static::CALCULATING_TYPE_PERCENT;
                $answer['visible_formula'] = "$this->vat_percent%";
                if ($this->vat_type == static::VAT_TYPE_COD) {
                    $answer['fields'] = [
                        OrderFinancePrediction::COLUMN_PRICE_COD,
                    ];
                } else {
                    $answer['fields'] = [
                        OrderFinancePrediction::COLUMN_PRICE_DELIVERY,
                        OrderFinancePrediction::COLUMN_PRICE_DELIVERY_RETURN,
                        OrderFinancePrediction::COLUMN_PRICE_STORAGE,
                        OrderFinancePrediction::COLUMN_PRICE_FULFILMENT,
                        OrderFinancePrediction::COLUMN_PRICE_PACKAGE,
                        OrderFinancePrediction::COLUMN_PRICE_PACKING,
                        OrderFinancePrediction::COLUMN_PRICE_DELIVERY_BACK,
                        OrderFinancePrediction::COLUMN_PRICE_REDELIVERY,
                        OrderFinancePrediction::COLUMN_PRICE_ADDRESS_CORRECTION,
                    ];
                }
                break;
            case OrderFinancePrediction::COLUMN_PRICE_STORAGE:
                $answer = [
                    'type' => ChargesCalculatorAbstract::CALCULATING_TYPE_MONTHLY_CHARGE,
                    'charge' => doubleval($this->storage_fee) * doubleval($this->storage_area),
                    'currency_id' => $this->getCurrency()->id,
                    'currency_rate' => $this->getCurrency()->currencyRate->rate,
                ];
                break;
            default:
                $answer['type'] = static::CALCULATING_TYPE_NUMBER;
                break;
        }

        return $answer;
    }
}