<?php
namespace app\modules\delivery\widgets;

use app\modules\delivery\models\Delivery;
use app\modules\storage\models\Storage as StorageModel;
use yii\base\Widget;

/**
 * Class Storage
 * @package app\modules\delivery\widgets
 */
class Storage extends Widget
{
    /**
     * @var Delivery
     */
    public $model;

    /**
     * @var StorageModel[]
     */
    public $storages;

    /**
     * @var StorageModel[]
     */
    public $storagesByDelivery;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('storage', [
            'model' => $this->model,
            'storages' => $this->storages,
            'storagesByDelivery' => $this->storagesByDelivery,
        ]);
    }
}
