<?php

namespace app\modules\delivery\widgets;


use app\widgets\Widget;

/**
 * Class EmailList
 * @package app\modules\delivery\widgets
 */
class EmailList extends Widget
{
    /**
     * @var string[]
     */
    public $emailList;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('email-list/index', ['emailList' => $this->emailList]);
    }
}
