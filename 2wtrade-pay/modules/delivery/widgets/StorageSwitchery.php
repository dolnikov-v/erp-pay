<?php
namespace app\modules\delivery\widgets;

use app\widgets\Switchery;

/**
 * Class StorageSwitchery
 * @package app\modules\delivery\widgets
 */
class StorageSwitchery extends Switchery
{
    /**
     * @return string
     * @throws \Exception
     */
    public function run()
    {
        return $this->render('storage-switchery', [
            'widget' => Switchery::widget([
                'checked' => $this->checked,
                'disabled' => $this->disabled,
                'attributes' => [
                    'data-widget' => 'storage-switchery',
                ],
            ]),
        ]);
    }
}
