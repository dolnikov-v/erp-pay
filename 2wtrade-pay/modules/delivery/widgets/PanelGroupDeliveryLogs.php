<?php

namespace app\modules\delivery\widgets;

use app\modules\delivery\models\DeliveryLog;
use app\widgets\PanelGroup;
use app\widgets\PanelTab;
use Yii;
use yii\data\ArrayDataProvider;

/**
 * Class PanelGroupDeliveryLogs
 * @package app\modules\delivery\widgets
 */
class PanelGroupDeliveryLogs extends PanelGroup
{
    /**
     * @var DeliveryLog[]
     */
    public $logs;

    /**
     * @return string
     */
    public function run()
    {
        $this->preparePanels();

        return parent::run();
    }

    /**
     * Подготовка табов
     */
    protected function preparePanels()
    {
        $groups = [];

        foreach ($this->logs as $item) {
            $groups[$item->group_id][] = $item;
        }

        $number = 0;

        foreach ($groups as $key => $items) {
            $dataProvider = new ArrayDataProvider([
                'allModels' => $items
            ]);

            $panel = new PanelTab([
                'id' => 'panel_tab_' . $key,
                'title' => Yii::t('common', 'Изменение #{number}', ['number' => count($groups) - $number]),
                'content' => $this->render('panel-group-delivery-logs/index', [
                    'dataProvider' => $dataProvider
                ])
            ]);

            $number++;
            $this->panels[] = $panel;
        }
    }
}
