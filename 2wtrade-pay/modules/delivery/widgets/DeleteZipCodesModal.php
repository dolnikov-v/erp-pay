<?php
namespace app\modules\delivery\widgets;

use app\widgets\Widget;

/**
 * Class DeleteZipCodesModal
 * @package app\modules\delivery\widgets
 */
class DeleteZipCodesModal extends Widget
{

    public $delivery_id;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('delete-zip-codes/modal', [
            'delivery_id' => $this->delivery_id
        ]);
    }
}
