<?php
/**
 * @var \app\modules\delivery\components\charges\globalexpress\GlobalExpress $model
 * @var \app\components\widgets\ActiveForm $form
 * @var array $currencies
 * @var bool $onlyView
 */
use app\modules\delivery\widgets\charges\helpers\DeliveryChargesForRegion;

?>

<div class="row">
    <div class="col-lg-12">
        <h3><?= Yii::t('common', 'Общие тарифы') ?></h3>
    </div>
</div>

<div class="row margin-top-10 margin-bottom-20">
    <div class="col-lg-3">
        <?= $form->field($model, 'packing_fee')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'package_fee')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'delivery_to_storage_fee')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'return_fee')->textInput(['disabled' => $onlyView]) ?>
    </div>
</div>

<?= DeliveryChargesForRegion::widget([
    'form' => $form,
    'model' => $model,
    'onlyView' => $onlyView,
    'regionAttribute' => 'zipcodes',
    'chargesAttributes' => ['delivery_fees']
]) ?>
<div class="row">
    <div class="col-lg-2">
        <?= $form->field($model, 'currency_char_code')->select2List($currencies, [
            'length' => false,
            'disabled' => $onlyView
        ]) ?>
    </div>
</div>
