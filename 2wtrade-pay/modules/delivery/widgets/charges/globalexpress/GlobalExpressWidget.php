<?php

namespace app\modules\delivery\widgets\charges\globalexpress;

use app\modules\delivery\widgets\charges\ChargesWidget;

/**
 * Class GlobalExpressWidget
 * @package app\modules\delivery\widgets\charges\globalexpress
 */
class GlobalExpressWidget extends ChargesWidget
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('global-express-widget', ['form' => $this->form, 'model' => $this->chargesCalculator, 'currencies' => $this->currencies, 'onlyView' => $this->onlyView]);
    }
}