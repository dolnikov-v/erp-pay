<?php

namespace app\modules\delivery\widgets\charges\bluedart;

use app\modules\delivery\widgets\charges\ChargesWidget;

/**
 * Class BluedartWidget
 * @package app\modules\delivery\widgets\charges\bluedart
 */
class BluedartWidget extends ChargesWidget
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('bluedart-widget', ['model' => $this->chargesCalculator, 'form' => $this->form, 'currencies' => $this->currencies, 'onlyView' => $this->onlyView]);
    }
}