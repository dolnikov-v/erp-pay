<?php
/**
 * @var \app\modules\delivery\components\charges\bluedart\Bluedart $model
 * @var \app\components\widgets\ActiveForm $form
 * @var array $currencies
 * @var bool $onlyView
 */
use app\modules\delivery\widgets\charges\helpers\DeliveryChargesForRegion;

?>

<div class="row">
    <div class="col-lg-12">
        <h3><?= Yii::t('common', 'Общие тарифы') ?></h3>
    </div>
</div>
<div class="row margin-top-10 margin-bottom-20">
    <div class="col-lg-3">
        <?= $form->field($model, 'return_fee')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'cod_fee')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'vat_percent')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'currency_char_code')->select2List($currencies, ['disabled' => $onlyView]) ?>
    </div>
</div>

<?= DeliveryChargesForRegion::widget(['form' => $form, 'model' => $model, 'regionAttribute' => 'zipcodes', 'chargesAttributes' => ['delivery_fees', 'delivery_fees_add_kg'], 'onlyView' => $onlyView]) ?>
