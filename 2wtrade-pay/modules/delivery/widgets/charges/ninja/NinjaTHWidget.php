<?php

namespace app\modules\delivery\widgets\charges\ninja;


use app\modules\delivery\widgets\charges\ChargesWidget;

/**
 * Class NinjaTHWidget
 * @package app\modules\delivery\widgets\charges\ninja
 */
class NinjaTHWidget extends ChargesWidget
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('ninja-th-widget', ['form' => $this->form, 'model' => $this->chargesCalculator, 'currencies' => $this->currencies]);
    }
}