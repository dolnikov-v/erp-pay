<?php

namespace app\modules\delivery\widgets\charges\ninja;


use app\modules\delivery\widgets\charges\ChargesWidget;

/**
 * Class NinjaPHWidget
 * @package app\modules\delivery\widgets\charges\ninja
 */
class NinjaPHWidget extends ChargesWidget
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('ninja-ph-widget', ['form' => $this->form, 'model' => $this->chargesCalculator, 'currencies' => $this->currencies, 'onlyView' => $this->onlyView]);
    }
}