<?php
/**
 * @var \app\modules\delivery\components\charges\ninja\NinjaTH $model
 * @var \app\components\widgets\ActiveForm $form
 * @var array $currencies
 */

use app\modules\delivery\assets\DeliveryChargesForRegionAsset;
use app\modules\delivery\widgets\charges\helpers\DeliveryChargesForRegion;

DeliveryChargesForRegionAsset::register($this);
?>

    <div class="row">
        <div class="col-lg-12">
            <h3><?= Yii::t('common', 'Общие тарифы') ?></h3>
        </div>
    </div>
    <div class="row margin-top-10 margin-bottom-20">
        <div class="col-lg-2">
            <?= $form->field($model, 'packing_fee')->textInput() ?>
        </div>
        <div class="col-lg-2">
            <?= $form->field($model, 'cod_fee')->textInput() ?>
        </div>
        <div class="col-lg-2">
            <?= $form->field($model, 'cod_fee_min')->textInput() ?>
        </div>
        <div class="col-lg-2">
            <?= $form->field($model, 'delivery_discount')->textInput() ?>
        </div>
        <div class="col-lg-2">
            <?= $form->field($model, 'price_vat')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-2">
            <?= $form->field($model, 'currency_char_code')->select2List($currencies, ['length' => false]) ?>
        </div>
    </div>
<?= DeliveryChargesForRegion::widget(['form' => $form, 'model' => $model, 'regionAttribute' => 'zipcodes', 'chargesAttributes' => ['delivery_fees']]) ?>