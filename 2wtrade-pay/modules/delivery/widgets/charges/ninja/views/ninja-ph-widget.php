<?php
/**
 * @var \app\modules\delivery\components\charges\ninja\NinjaPH $model
 * @var \app\components\widgets\ActiveForm $form
 * @var array $currencies
 * @var bool $onlyView
 */
?>

<div class="row">
    <div class="col-lg-12">
        <h3><?= Yii::t('common', 'Стоимость доставки в зависимости от веса') ?></h3>
    </div>
</div>
<div class="row">
    <div class="col-lg-3">
        <?= $form->field($model, 'packing_fee')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'delivery_fee_1kg')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'delivery_fee_add_kg')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'price_vat')->textInput(['disabled' => $onlyView]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-2">
        <?= $form->field($model, 'currency_char_code')->select2List($currencies, [
            'length' => false,
            'disabled' => $onlyView
        ]) ?>
    </div>
</div>