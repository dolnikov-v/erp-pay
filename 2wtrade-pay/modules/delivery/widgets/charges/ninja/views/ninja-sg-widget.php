<?php
/**
 * @var \app\modules\delivery\components\charges\ninja\NinjaSG $model
 * @var \app\components\widgets\ActiveForm $form
 * @var array $currencies
 * @var bool $onlyView
 */
?>

<div class="row">
    <div class="col-lg-3">
        <?= $form->field($model, 'storage_fee_per_m3')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'packing_fee_size_a')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'packing_fee_size_b')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'packing_fee_size_c')->textInput(['disabled' => $onlyView]) ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-3">
        <?= $form->field($model, 'delivery_fee_size_a')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'delivery_fee_size_b')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'delivery_fee_size_c')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'delivery_fee_size_d')->textInput(['disabled' => $onlyView]) ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-3">
        <?= $form->field($model, 'cod_fee')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'price_vat')->textInput(['disabled' => $onlyView]) ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-2">
        <?= $form->field($model, 'currency_char_code')->select2List($currencies, [
            'length' => false,
            'disabled' => $onlyView
        ]) ?>
    </div>
</div>
