<?php
/**
 * @var \app\modules\delivery\components\charges\ninja\NinjaMY $model
 * @var \app\components\widgets\ActiveForm $form
 * @var array $currencies
 * @var bool $onlyView
 */
?>

<div class="row">
    <div class="col-lg-12">
        <h3><?= Yii::t('common', 'Стоимость доставки в зависимости от веса') ?></h3>
    </div>
</div>
<div class="row">
    <div class="col-lg-3">
        <?= $form->field($model, 'delivery_fee_1kg')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'delivery_fee_2kg')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'delivery_fee_3kg')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'delivery_fee_add_kg')->textInput(['disabled' => $onlyView]) ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <h3><?= Yii::t('common', 'Общие тарифы') ?></h3>
    </div>
</div>

<div class="row">
    <div class="col-lg-3">
        <?= $form->field($model, 'packing_fee')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'cod_fee_min')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'cod_fee')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'price_vat')->textInput(['disabled' => $onlyView]) ?>
    </div>
</div>