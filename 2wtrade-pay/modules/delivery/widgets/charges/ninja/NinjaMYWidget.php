<?php

namespace app\modules\delivery\widgets\charges\ninja;

use app\modules\delivery\widgets\charges\ChargesWidget;

/**
 * Class NinjaMYWidget
 * @package app\modules\delivery\widgets\charges\ninja
 */
class NinjaMYWidget extends ChargesWidget
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('ninja-my-widget', ['form' => $this->form, 'model' => $this->chargesCalculator, 'currencies' => $this->currencies, 'onlyView' => $this->onlyView]);
    }
}