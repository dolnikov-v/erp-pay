<?php

namespace app\modules\delivery\widgets\charges\ninja;

use app\modules\delivery\widgets\charges\ChargesWidget;

/**
 * Class NinjaSGWidget
 * @package app\modules\delivery\widgets\charges\ninja
 */
class NinjaSGWidget extends ChargesWidget
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('ninja-sg-widget', ['form' => $this->form, 'model' => $this->chargesCalculator, 'currencies' => $this->currencies, 'onlyView' => $this->onlyView]);
    }
}