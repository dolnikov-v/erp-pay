<?php

namespace app\modules\delivery\widgets\charges\ninja;


use app\modules\delivery\widgets\charges\ChargesWidget;

/**
 * Class NinjaID
 * @package app\modules\delivery\widgets\charges\ninja
 */
class NinjaIDWidget extends ChargesWidget
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('ninja-id-widget', ['form' => $this->form, 'model' => $this->chargesCalculator, 'currencies' => $this->currencies, 'onlyView' => $this->onlyView]);
    }
}