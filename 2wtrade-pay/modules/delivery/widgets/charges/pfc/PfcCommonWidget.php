<?php

namespace app\modules\delivery\widgets\charges\pfc;

use app\modules\delivery\widgets\charges\ChargesWidget;

/**
 * Class PfcCommonWidget
 * @package app\modules\delivery\widgets\charges\pfc
 */
class PfcCommonWidget extends ChargesWidget
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('pfc-common-widget/index', ['form' => $this->form, 'values' => $this->values, 'chargesCalculator' => $this->chargesCalculator, 'currencies' => $this->currencies, 'onlyView' => $this->onlyView]);
    }
}