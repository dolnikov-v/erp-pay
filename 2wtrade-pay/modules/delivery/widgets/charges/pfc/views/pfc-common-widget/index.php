<?php

use app\modules\delivery\components\charges\ChargesCalculatorAbstract;

/**
 * @var \app\components\widgets\ActiveForm $form
 * @var array $values
 * @var \app\modules\delivery\components\charges\pfc\PfcCommon $chargesCalculator
 * @var array $currencies
 * @var bool $onlyView
 */

?>

<div class="row">
    <div class="col-lg-2">
        <?= $form->field($chargesCalculator, 'warehouse_fee')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($chargesCalculator, 'packing_fee')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($chargesCalculator, 'shipment_fee')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($chargesCalculator, 'return_fee')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($chargesCalculator, 'cod_fee')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($chargesCalculator, 'tax_fee')->textInput(['disabled' => $onlyView]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-3">
        <?= $form->field($chargesCalculator, 'vat_type')
            ->select2List(ChargesCalculatorAbstract::getVatTypes(), ['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($chargesCalculator, 'currency_char_code')
            ->select2List($currencies, ['disabled' => $onlyView, 'length' => false]) ?>
    </div>
</div>