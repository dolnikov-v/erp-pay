<?php

namespace app\modules\delivery\widgets\charges\fullphilippines;

use app\modules\delivery\widgets\charges\ChargesWidget;

/**
 * Class FullPhilippinesWidget
 * @package app\modules\delivery\widgets\charges\fullphilippines
 */
class FullPhilippinesWidget extends ChargesWidget
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('fullphilippines-widget', ['form' => $this->form, 'model' => $this->chargesCalculator]);
    }
}