<?php

namespace app\modules\delivery\widgets\charges\couriermate;


use app\modules\delivery\widgets\charges\ChargesWidget;

/**
 * Class CourierMateWidget
 * @package app\modules\delivery\widgets\charges\couriermate
 */
class CourierMateWidget extends ChargesWidget
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('courier-mate-widget', ['form' => $this->form, 'model' => $this->chargesCalculator]);
    }
}