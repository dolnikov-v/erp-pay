<?php
/**
 * @var \app\modules\delivery\components\charges\couriermate\CourierMate $model
 * @var \app\components\widgets\ActiveForm $form
 */
use app\modules\delivery\widgets\charges\helpers\DeliveryChargesForRegion;

?>

    <div class="row">
        <div class="col-lg-12">
            <h3><?= Yii::t('common', 'Общие тарифы') ?></h3>
        </div>
    </div>
    <div class="row margin-top-10 margin-bottom-20">
        <div class="col-lg-3">
            <?= $form->field($model, 'printing_fee')->textInput() ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($model, 'packing_fee')->textInput() ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($model, 'cod_fee')->textInput() ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($model, 'storage_fee')->textInput() ?>
        </div>
    </div>

<?= DeliveryChargesForRegion::widget(['form' => $form, 'model' => $model, 'regionAttribute' => 'zipcodes', 'chargesAttributes' => ['delivery_fees']]); ?>