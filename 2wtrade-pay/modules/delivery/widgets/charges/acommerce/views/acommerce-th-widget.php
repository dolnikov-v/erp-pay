<?php
/**
 * @var \app\modules\delivery\components\charges\acommerce\AcommerceTH $model
 * @var \app\components\widgets\ActiveForm $form
 * @var array $currencies
 * @var bool $onlyView
 */
use app\modules\delivery\widgets\charges\helpers\DeliveryChargesForRegion;

?>
<div class="row">
    <div class="col-lg-12">
        <h3><?= Yii::t('common', 'Общие тарифы') ?></h3>
    </div>
</div>
<div class="row margin-top-10">
    <div class="col-lg-2">
        <?= $form->field($model, 'handling_xs')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'handling_s')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'handling_m')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'handling_l')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'handling_xl')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'fulfilment_fee')->textInput(['disabled' => $onlyView]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-2">
        <?= $form->field($model, 'storage_xs')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'storage_s')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'storage_m')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'storage_l')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'storage_xl')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'labelling_fee')->textInput(['disabled' => $onlyView]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-2">
        <?= $form->field($model, 'packaging_aaa')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'packaging_aa')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'packaging_a')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'packaging_b')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'packaging_c')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'packaging_d')->textInput(['disabled' => $onlyView]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-2">
        <?= $form->field($model, 'packaging_e')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'packaging_f')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'packaging_g')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'packaging_xl')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'packaging_xxl')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'repack_fee')->textInput(['disabled' => $onlyView]) ?>
    </div>
</div>

<div class="row margin-bottom-20">
    <div class="col-lg-2">
        <?= $form->field($model, 'fragile_fee')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'return_fee')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'cod_fee')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'vat_percent')->textInput(['disabled' => $onlyView]) ?>
    </div>
</div>
<?= DeliveryChargesForRegion::widget([
    'form' => $form,
    'model' => $model,
    'onlyView' => $onlyView,
    'regionAttribute' => 'zipcodes',
    'chargesAttributes' => [
        'delivery_fee_1kg',
        'delivery_fee_2kg',
        'delivery_fee_3kg',
        'delivery_fee_4kg',
        'delivery_fee_5kg',
        'delivery_fee_6kg',
        'delivery_fee_7kg',
        'delivery_fee_8kg',
        'delivery_fee_9kg',
        'delivery_fee_10kg',
        'delivery_fee_15kg',
        'delivery_fee_20kg',
        'delivery_fee_more_20kg'
    ]
]); ?>
<div class="row">
    <div class="col-lg-2">
        <?= $form->field($model, 'currency_char_code')->select2List($currencies, [
            'length' => false,
            'disabled' => $onlyView
        ]) ?>
    </div>
</div>
