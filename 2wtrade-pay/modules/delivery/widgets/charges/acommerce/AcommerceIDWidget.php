<?php

namespace app\modules\delivery\widgets\charges\acommerce;

use app\modules\delivery\widgets\charges\ChargesWidget;

/**
 * Class AcommerceIDWidget
 * @package app\modules\delivery\widgets\charges\acommerce
 */
class AcommerceIDWidget extends ChargesWidget
{
    /**
     *
     */
    public function run()
    {
        return $this->render('acommerce-id-widget', ['form' => $this->form, 'model' => $this->chargesCalculator, 'onlyView' => $this->onlyView, 'currencies' => $this->currencies]);
    }
}