<?php

namespace app\modules\delivery\widgets\charges\acommerce;

use app\modules\delivery\widgets\charges\ChargesWidget;

/**
 * Class AcommerceTHWidget
 * @package app\modules\delivery\widgets\charges\acommerce
 */
class AcommerceTHWidget extends ChargesWidget
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('acommerce-th-widget', ['form' => $this->form, 'model' => $this->chargesCalculator, 'currencies' => $this->currencies, 'onlyView' => $this->onlyView]);
    }
}