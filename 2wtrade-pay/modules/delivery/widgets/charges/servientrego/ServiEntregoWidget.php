<?php

namespace app\modules\delivery\widgets\charges\servientrego;

use app\modules\delivery\widgets\charges\ChargesWidget;

/**
 * Class ServiEntregoWidget
 * @package app\modules\delivery\widgets\charges\servientrego
 */
class ServiEntregoWidget extends ChargesWidget
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('servientrego-widget', ['model' => $this->chargesCalculator, 'form' => $this->form, 'currencies' => $this->currencies, 'onlyView' => $this->onlyView]);
    }
}