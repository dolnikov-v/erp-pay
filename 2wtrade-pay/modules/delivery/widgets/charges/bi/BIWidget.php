<?php

namespace app\modules\delivery\widgets\charges\bi;

use app\modules\delivery\widgets\charges\ChargesWidget;

/**
 * Class BIWidget
 * @package app\modules\delivery\widgets\charges\bi
 */
class BIWidget extends ChargesWidget
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('bi-widget/index', ['form' => $this->form, 'chargesCalculator' => $this->chargesCalculator, 'currencies' => $this->currencies, 'onlyView' => $this->onlyView]);
    }
}