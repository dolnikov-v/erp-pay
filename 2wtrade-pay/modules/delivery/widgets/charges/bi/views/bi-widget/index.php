<?php
/**
 * @var \app\components\widgets\ActiveForm $form
 * @var \app\modules\delivery\components\charges\bi\BI $chargesCalculator
 * @var array $currencies
 * @var bool $onlyView
 */
?>

<div class="row">
    <div class="col-lg-12">
        <h3><?= Yii::t('common', 'Зона A') ?></h3>
    </div>
</div>
<div class="row">
    <div class="col-lg-6 text-center">
        <h4><?= Yii::t('common', 'Менее 10000 заказов в месяц') ?></h4>
    </div>
    <div class="col-lg-6 text-center">
        <h4><?= Yii::t('common', 'Более 10000 заказов в месяц') ?></h4>
    </div>
</div>
<div class="row">
    <div class="col-lg-3">
        <?= $form->field($chargesCalculator, 'zone_a_shipment_a_weight_a')
            ->label(Yii::t('common', 'Вес менее 2.1кг'))
            ->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($chargesCalculator, 'zone_a_shipment_a_weight_b')
            ->label(Yii::t('common', 'Вес более 2.1кг'))
            ->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($chargesCalculator, 'zone_a_shipment_b_weight_a')
            ->label(Yii::t('common', 'Вес менее 2.1кг'))
            ->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($chargesCalculator, 'zone_a_shipment_b_weight_b')
            ->label(Yii::t('common', 'Вес более 2.1кг'))
            ->textInput(['disabled' => $onlyView]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-3">
        <?= $form->field($chargesCalculator, 'zone_a_return')->textInput(['disabled' => $onlyView]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= $form->field($chargesCalculator, 'zone_a')->textarea(['disabled' => $onlyView]) ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <h3><?= Yii::t('common', 'Зона B') ?></h3>
    </div>
</div>
<div class="row">
    <div class="col-lg-6 text-center">
        <h4><?= Yii::t('common', 'Менее 10000 заказов в месяц') ?></h4>
    </div>
    <div class="col-lg-6 text-center">
        <h4><?= Yii::t('common', 'Более 10000 заказов в месяц') ?></h4>
    </div>
</div>
<div class="row">
    <div class="col-lg-3">
        <?= $form->field($chargesCalculator, 'zone_b_shipment_a_weight_a')
            ->label(Yii::t('common', 'Вес менее 2.1кг'))
            ->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($chargesCalculator, 'zone_b_shipment_a_weight_b')
            ->label(Yii::t('common', 'Вес более 2.1кг'))
            ->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($chargesCalculator, 'zone_b_shipment_b_weight_a')
            ->label(Yii::t('common', 'Вес менее 2.1кг'))
            ->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($chargesCalculator, 'zone_b_shipment_b_weight_b')
            ->label(Yii::t('common', 'Вес более 2.1кг'))
            ->textInput(['disabled' => $onlyView]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-3">
        <?= $form->field($chargesCalculator, 'zone_b_return')->textInput(['disabled' => $onlyView]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= $form->field($chargesCalculator, 'zone_b')->textarea(['disabled' => $onlyView]) ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <h3><?= Yii::t('common', 'Зона C') ?></h3>
    </div>
</div>
<div class="row">
    <div class="col-lg-6 text-center">
        <h4><?= Yii::t('common', 'Менее 10000 заказов в месяц') ?></h4>
    </div>
    <div class="col-lg-6 text-center">
        <h4><?= Yii::t('common', 'Более 10000 заказов в месяц') ?></h4>
    </div>
</div>
<div class="row">
    <div class="col-lg-3">
        <?= $form->field($chargesCalculator, 'zone_c_shipment_a_weight_a')
            ->label(Yii::t('common', 'Вес менее 2.1кг'))
            ->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($chargesCalculator, 'zone_c_shipment_a_weight_b')
            ->label(Yii::t('common', 'Вес более 2.1кг'))
            ->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($chargesCalculator, 'zone_c_shipment_b_weight_a')
            ->label(Yii::t('common', 'Вес менее 2.1кг'))
            ->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($chargesCalculator, 'zone_c_shipment_b_weight_b')
            ->label(Yii::t('common', 'Вес более 2.1кг'))
            ->textInput(['disabled' => $onlyView]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-3">
        <?= $form->field($chargesCalculator, 'zone_c_return')->textInput(['disabled' => $onlyView]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= $form->field($chargesCalculator, 'zone_c')->textarea(['disabled' => $onlyView]) ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <h3><?= Yii::t('common', 'Зона D') ?></h3>
    </div>
</div>
<div class="row">
    <div class="col-lg-6 text-center">
        <h4><?= Yii::t('common', 'Менее 10000 заказов в месяц') ?></h4>
    </div>
    <div class="col-lg-6 text-center">
        <h4><?= Yii::t('common', 'Более 10000 заказов в месяц') ?></h4>
    </div>
</div>
<div class="row">
    <div class="col-lg-3">
        <?= $form->field($chargesCalculator, 'zone_d_shipment_a_weight_a')
            ->label(Yii::t('common', 'Вес менее 2.1кг'))
            ->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($chargesCalculator, 'zone_d_shipment_a_weight_b')
            ->label(Yii::t('common', 'Вес более 2.1кг'))
            ->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($chargesCalculator, 'zone_d_shipment_b_weight_a')
            ->label(Yii::t('common', 'Вес менее 2.1кг'))
            ->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($chargesCalculator, 'zone_d_shipment_b_weight_b')
            ->label(Yii::t('common', 'Вес более 2.1кг'))
            ->textInput(['disabled' => $onlyView]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-3">
        <?= $form->field($chargesCalculator, 'zone_d_return')->textInput(['disabled' => $onlyView]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= $form->field($chargesCalculator, 'zone_d')->textarea(['disabled' => $onlyView]) ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-3">
        <?= $form->field($chargesCalculator, 'vat_percent')->textInput(['disabled' => $onlyView]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-2">
        <?= $form->field($chargesCalculator, 'currency_char_code')->select2List($currencies, [
            'length' => false,
            'disabled' => $onlyView
        ]) ?>
    </div>
</div>
