<?php

namespace app\modules\delivery\widgets\charges\etobee;

use app\modules\delivery\widgets\charges\ChargesWidget;

/**
 * Class EtobeeWidget
 * @package app\modules\delivery\widgets\charges\etobee
 */
class EtobeeWidget extends ChargesWidget
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('etobee-widget', ['form' => $this->form, 'model' => $this->chargesCalculator, 'currencies' => $this->currencies, 'onlyView' => $this->onlyView]);
    }
}