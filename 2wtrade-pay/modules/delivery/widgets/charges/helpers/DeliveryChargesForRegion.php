<?php

namespace app\modules\delivery\widgets\charges\helpers;


use app\components\widgets\ActiveForm;
use app\widgets\Widget;
use yii\base\Model;

/**
 * Class DeliveryChargesForRegion
 * @package app\modules\delivery\widgets\charges\helpers
 */
class DeliveryChargesForRegion extends Widget
{
    /**
     * @var Model
     */
    public $model;
    /**
     * @var string
     */
    public $regionAttribute;
    /**
     * @var array
     */
    public $chargesAttributes;
    /**
     * @var array
     */
    public $chargesArrayAttributes = [];

    /**
     * @var ActiveForm
     */
    public $form;

    /**
     * @var array
     */
    public $currencies;

    /**
     * @var bool
     */
    public $onlyView = false;

    /**
     * @return string
     */
    public function run()
    {
        $chargesAttributes = [];
        $counter = 0;
        foreach ($this->chargesAttributes as $attribute) {
            $index = (int)($counter / 6);
            $counter++;
            if (!isset($chargesAttributes[$index])) {
                $chargesAttributes[$index] = [];
            }
            $chargesAttributes[$index][] = $attribute;
        }

        $chargesArrayAttributes = [];
        $cnt = 0;
        $row = 0;
        foreach ($this->chargesArrayAttributes as $item) {
                if (is_string($item)) {
                    $item = ['attribute' => $item];
                }
                if (!isset($item['attribute']) && isset($item[0])) {
                    $item['attribute'] = $item[0];
                }
                if (!isset($item['length'])) {
                    $item['length'] = 2;
                }
                if (!isset($item['type'])) {
                    $item['type'] = 'textInput';
                }
                if ($cnt + $item['length'] > 12) {
                    $row++;
                    $cnt = 0;
                }
                $chargesArrayAttributes[$row][] = $item;
                $cnt += $item['length'];
        }
        return $this->render('delivery-charges-for-region/index', [
            'model' => $this->model,
            'regionAttribute' => $this->regionAttribute,
            'chargesAttributes' => $chargesAttributes,
            'chargesArrayAttributes' => $chargesArrayAttributes,
            'form' => $this->form,
            'currencies' => $this->currencies,
            'widget' => $this,
            'onlyView' => $this->onlyView,
        ]);
    }
}