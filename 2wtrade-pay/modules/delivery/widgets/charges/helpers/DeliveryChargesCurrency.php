<?php

namespace app\modules\delivery\widgets\charges\helpers;

use app\models\Currency;
use app\modules\delivery\widgets\charges\ChargesWidget;
use yii\helpers\ArrayHelper;

/**
 * Class DeliveryChargesForRegionWithCurrency
 * @package app\modules\delivery\widgets\charges\helpers
 */
class DeliveryChargesCurrency extends ChargesWidget
{
    /**
     * @var string
     */
    public $attribute;

    /**
     * @return string
     */
    public function run()
    {
        $currencies = Currency::find()->all();
        $currencies = ArrayHelper::map($currencies, 'char_code', 'name');
        return $this->render('delivery-charges-currency/index', ['form' => $this->form, 'model' => $this->chargesCalculator, 'currencies' => $currencies, 'attribute' => $this->attribute]);
    }
}