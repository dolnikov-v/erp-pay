<?php

namespace app\modules\delivery\widgets\charges\helpers;

use app\components\widgets\ActiveForm;
use app\widgets\Widget;
use yii\base\Model;
use Yii;

/**
 * Class DeliveryChargesArrayProperty
 * @package app\modules\delivery\widgets\charges\helpers
 */
class DeliveryChargesArrayProperty extends Widget
{
    /**
     * @var string
     */
    public $tabName;
    /**
     * @var Model
     */
    public $model;
    /**
     * @var array
     */
    public $arrayProperties = [];

    /**
     * @var ActiveForm
     */
    public $form;

    /**
     * @var bool
     */
    public $onlyView = false;

    /**
     * @return string
     */
    public function run()
    {
        $arrayProperties = [];
        $cnt = 0;
        $row = 0;
        foreach ($this->arrayProperties as $item) {
                if (is_string($item)) {
                    $item = ['attribute' => $item];
                }
                if (!isset($item['attribute']) && isset($item[0])) {
                    $item['attribute'] = $item[0];
                }
                if (!isset($item['length'])) {
                    $item['length'] = 2;
                }
                if (!isset($item['type'])) {
                    $item['type'] = 'textInput';
                }
                if ($cnt + $item['length'] > 12) {
                    $row++;
                    $cnt = 0;
                }
                $arrayProperties[$row][] = $item;
                $cnt += $item['length'];
        }
        return $this->render('delivery-charges-array-property/index', [
            'model' => $this->model,
            'arrayProperties' => $arrayProperties,
            'form' => $this->form,
            'widget' => $this,
            'onlyView' => $this->onlyView,
            'tabName' => $this->tabName ?? Yii::t('common', 'Множественные свойства')
        ]);
    }
}