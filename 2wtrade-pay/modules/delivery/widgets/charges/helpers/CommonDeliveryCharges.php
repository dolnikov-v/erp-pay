<?php

namespace app\modules\delivery\widgets\charges\helpers;


use app\components\widgets\ActiveForm;
use app\modules\delivery\components\charges\ChargesCalculatorAbstract;
use app\widgets\Widget;
use Yii;

/**
 * Class CommonDeliveryCharges
 * @package app\modules\delivery\widgets\charges\helpers
 */
class CommonDeliveryCharges extends Widget
{
    /**
     * @var ChargesCalculatorAbstract
     */
    public $model;

    /**
     * @var ActiveForm
     */
    public $form;

    /**
     * @var null
     */
    public $label = null;

    /**
     * @var array
     */
    public $items;

    /**
     * @return string
     */
    public function run()
    {
        if (empty($this->label)) {
            $this->label = Yii::t('common', 'Общие тарифы');
        }

        $items = [];

        if (isset($this->model->vat_included)) {
            $this->items[] = [
                'attribute' => 'vat_included',
                'type' => 'checkboxCustom',
                'options' => [
                    'value' => 1,
                    'checked' => $this->model->vat_included,
                    'uncheckedValue' => 0,
                    'disabled' => $this->items[0]['options']['disabled'] ?? false,
                ]
            ];
        }

        $cnt = 0;
        $row = 0;
        foreach ($this->items as $item) {
            if (is_string($item)) {
                $item = ['attribute' => $item];
            }
            if (!isset($item['attribute']) && isset($item[0])) {
                $item['attribute'] = $item[0];
            }
            if (!isset($item['length'])) {
                $item['length'] = 3;
            }

            if (!isset($item['type'])) {
                $item['type'] = 'textInput';
            }
            if ($cnt + $item['length'] > 12) {
                $row++;
                $cnt = 0;
            }
            $items[$row][] = $item;
            $cnt += $item['length'];
        }

        return $this->render('common-delivery-charges/index', ['form' => $this->form, 'model' => $this->model, 'label' => $this->label, 'items' => $items]);
    }
}