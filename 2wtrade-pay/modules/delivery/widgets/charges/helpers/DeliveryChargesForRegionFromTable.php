<?php

namespace app\modules\delivery\widgets\charges\helpers;


use app\components\widgets\ActiveForm;
use app\modules\delivery\models\DeliveryContract;
use app\widgets\Widget;

/**
 * Class DeliveryChargesForRegionFormTable
 * @package app\modules\delivery\widgets\charges\helpers
 */
class DeliveryChargesForRegionFromTable extends Widget
{
    /**
     * @var DeliveryContract
     */
    public $model;

    /**
     * @var ActiveForm
     */
    public $form;

    /**
     * @var DeliveryChargesForRegion
     */
    public $deliveryChargesForRegionWidget;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('delivery-charges-for-region-from-table/modal', [
            'url' => $this->form->action,
            'model' => $this->model,
            'form' => $this->form,
            'deliveryChargesForRegionWidget' => $this->deliveryChargesForRegionWidget,
        ]);
    }

    /**
     * @return string
     */
    public static function formName()
    {
        return 'DeliveryChargesForRegionFromTable';
    }

    /**
     * @return string
     */
    public static function fileFieldName()
    {
        return self::formName() . '[file]';
    }

    /**
     * @return string
     */
    public static function regionAttributeContainerName()
    {
        return 'regionAttribute';
    }

    /**
     * @return string
     */
    public static function chargesAttributesContainerName()
    {
        return 'chargesAttributes';
    }
}