<?php
/**
 * @var \app\components\widgets\ActiveForm $form
 * @var \app\modules\delivery\components\charges\ChargesCalculatorAbstract $model
 * @var array $currencies
 * @var string $attribute
 */
?>

<div class="row">
    <div class="col-lg-12">
        <?= Yii::t('common', 'Валюта тарифов') ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-3">
        <?= $form->field($model, $attribute)->select2List($currencies) ?>
    </div>
</div>