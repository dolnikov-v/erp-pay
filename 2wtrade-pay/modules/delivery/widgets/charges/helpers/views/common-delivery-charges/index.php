<?php
/**
 * @var array $items
 * @var \app\components\widgets\ActiveForm $form
 * @var \app\modules\delivery\components\charges\ChargesCalculatorAbstract $model
 * @var string $label
 */

?>

<div class="margin-bottom-20">
    <div class="row">
        <div class="col-lg-12">
            <h3>
                <?= $label ?>
            </h3>
        </div>
    </div>
    <?php foreach ($items as $rowItems): ?>
        <div class="row">
            <?php foreach ($rowItems as $item): ?>
                <div class="col-lg-<?= $item['length'] ?>">
                    <?php if (isset($item['attribute'])): ?>
                        <?php if (isset($item['items'])): ?>
                            <?= $form->field($model, $item['attribute'])->{$item['type']}($item['items'], $item['options'] ?? []) ?>
                        <?php else: ?>
                            <?= $form->field($model, $item['attribute'])->{$item['type']}($item['options'] ?? []) ?>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            <?php endforeach; ?>
        </div>
    <?php endforeach; ?>
</div>
