<?php
/**
 * @var \yii\web\View $this
 * @var \yii\base\Model $model
 * @var array $arrayProperties
 * @var \app\components\widgets\ActiveForm $form
 * @var \app\modules\delivery\widgets\charges\helpers\DeliveryChargesForRegion $widget
 * @var bool $onlyView
 * @var string $tabName
 */

use app\helpers\WbIcon;
use app\widgets\Button;
use yii\helpers\Html;
use app\modules\delivery\assets\ChargesArrayPropertyAsset;

ChargesArrayPropertyAsset::register($this);

$onlyView = (!isset($onlyView) ? true : $onlyView);
?>

<div class="row">
    <?php if (!empty($tabName)): ?>
        <div class="col-lg-12">
            <h4><?= $tabName ?></h4>
        </div>
    <?php endif; ?>
    <div class="col-lg-12 array-properties">
        <?php $countAttr = 0;
        for ($rowID = -1; $rowID < $countAttr; $rowID++): ?>
            <div class="row<?= ($rowID == -1 ? ' array-properties-default' : '') ?>">
                <div class="col-lg-11">
                    <?php foreach ($arrayProperties as $row): ?>
                        <div class="row">
                            <?php foreach ($row as $attribute): ?>
                                <?php
                                if ($rowID == -1 && $countAttr < count($model->{$attribute['attribute']})):
                                    $countAttr = count($model->{$attribute['attribute']});
                                endif;
                                $name = Html::getInputName($model, $attribute['attribute']) . "[{$rowID}]";
                                if ($rowID == -1):
                                    echo $form->field($model, $attribute['attribute'])->hiddenInput(['name' => $name, 'value' => ''])->label(false);
                                endif;
                                $name = ($rowID == -1 ? '_' : '') . $name;
                                $value = $model->{$attribute['attribute']}[$rowID] ?? 0;
                                $param = ['value' => $value, 'name' => $name];
                                ?>
                                <div class="col-lg-<?= $attribute['length'] ?? 2 ?>">
                                    <?php if (isset($attribute['items'])): ?>
                                        <?= $form->field($model, $attribute['attribute'])->{$attribute['type']}($attribute['items'], ($attribute['options'] ?? []) + $param) ?>
                                    <?php else: ?>
                                        <?= $form->field($model, $attribute['attribute'])->{$attribute['type']}(($attribute['options'] ?? []) + $param) ?>
                                    <?php endif; ?>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="col-lg-1 margin-top-25 margin-bottom-20">
                    <?php if (!$onlyView): ?>
                        <?= Button::widget([
                            'type' => 'button',
                            'icon' => WbIcon::MINUS,
                            'style' => Button::STYLE_DANGER . ' btn-minus',
                        ]) ?>
                    <?php endif; ?>
                </div>
            </div>
        <?php endfor; ?>

        <?php if (!$onlyView): ?>
            <div class="row margin-bottom-20 charges-panel">
                <div class="col-lg-12">
                    <?= Button::widget([
                        'type' => 'button',
                        'icon' => WbIcon::PLUS,
                        'style' => Button::STYLE_SUCCESS . ' btn-plus',
                    ]) ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>