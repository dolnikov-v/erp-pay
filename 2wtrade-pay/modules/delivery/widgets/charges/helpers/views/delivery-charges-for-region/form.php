<?php

use app\helpers\WbIcon;
use app\widgets\Button;
use yii\helpers\Html;
use app\modules\delivery\assets\ChargesArrayAttributesAsset;

/**
 * @var \yii\base\Model $model
 * @var string $regionAttribute
 * @var array $chargesAttributes
 * @var \app\components\widgets\ActiveForm $form
 * @var integer $index
 * @var bool $onlyView
 */
?>

<div class="row-delivery-charges-for-region">
    <div class="row">
        <div class="col-lg-11">
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="row-header-delivery-charges-for-region"><?= Yii::t('common', 'Регион #{number}', ['number' => $index ? $index + 1 : 1]) ?></h4>
                </div>
            </div>
            <?php foreach ($chargesAttributes as $attributes): ?>
                <div class="row">
                    <?php foreach ($attributes as $attribute): ?>
                        <div class="col-lg-2">
                            <?php if ($attribute == 'enable_percent_buyout') : ?>
                                <div class="margin-top-35">
                                    <?= $form->field($model, $attribute)->checkboxCustom([
                                        'id' => false,
                                        'name' => Html::getInputName($model, $attribute) . '[]',
                                        'checked' => ($index || $index === 0) ? $model->$attribute[$index] : false,
                                        'value' => 1,
                                        'uncheckedValue' => 0,
                                        'disabled' => $onlyView,
                                    ]) ?>
                                </div>
                            <?php else: ?>
                                <?= $form->field($model, $attribute)->textInput([
                                    'id' => false,
                                    'name' => Html::getInputName($model, $attribute) . '[]',
                                    'value' => ($index || $index === 0) && !empty($model->$attribute[$index]) ? $model->$attribute[$index] : '',
                                    'disabled' => $onlyView,
                                ]) ?>
                            <?php endif; ?>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endforeach; ?>
            <?php if (!empty($chargesArrayAttributes)): ?>
                <?php ChargesArrayAttributesAsset::register($this); ?>
                <?= $this->render('_array-attributes', [
                    'form' => $form,
                    'model' => $model,
                    'chargesArrayAttributes' => $chargesArrayAttributes,
                    'index' => $index,
                    'onlyView' => $onlyView,
                    'show' => $model->enable_percent_buyout[$index] ?? false
                ]) ?>
            <?php endif; ?>
            <div class="row">
                <div class="col-lg-12">
                    <?= $form->field($model, $regionAttribute)->textarea([
                        'id' => false,
                        'name' => Html::getInputName($model, $regionAttribute) . '[]',
                        'value' => ($index || $index === 0) ? $model->$regionAttribute[$index] : '',
                        'disabled' => $onlyView,
                    ]) ?>
                </div>
            </div>
        </div>
        <?php if (!$onlyView): ?>
            <div class="col-lg-1 margin-top-30">
                <div class="form-group">
                    <div class="control-label">
                        <label>&nbsp;</label>
                    </div>
                    <?= Button::widget([
                        'type' => 'button',
                        'icon' => WbIcon::MINUS,
                        'style' => Button::STYLE_DANGER . ' btn-delivery-charges-for-region btn-delivery-charges-for-region-minus',
                    ]) ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>
