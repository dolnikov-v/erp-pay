<?php
/**
 * @var \yii\web\View $this
 * @var \yii\base\Model $model
 * @var string $regionAttribute
 * @var array $chargesAttributes
 * @var \app\components\widgets\ActiveForm $form
 * @var array $currencies
 * @var \app\modules\delivery\widgets\charges\helpers\DeliveryChargesForRegion $widget
 * @var bool $onlyView
 */
use app\helpers\WbIcon;
use app\modules\delivery\assets\DeliveryChargesForRegionAsset;
use app\modules\delivery\widgets\charges\helpers\DeliveryChargesForRegionFromTable;
use app\widgets\Button;
use app\widgets\ButtonProgress;
use yii\helpers\Html;
use yii\web\View;

$this->registerJs("if(typeof I18n == 'undefined'){ var I18n = {}; }", View::POS_HEAD);
$this->registerJs("I18n['Регион #{number}'] = '" . Yii::t('common', 'Регион #{number}') . "';", View::POS_HEAD);

DeliveryChargesForRegionAsset::register($this);
?>
<div class="row margin-bottom-10">
    <div class="col-lg-12">
        <h3><?= Yii::t('common', 'Тарифы в зависимости от регионов') ?></h3>
    </div>
</div>
<?php if (!$onlyView): ?>
    <div class="row margin-bottom-10">
        <div class="col-lg-12">
            <?= ButtonProgress::widget([
                'id' => 'btn_upload_charges_for_region_from_file',
                'style' => ButtonProgress::STYLE_PRIMARY,
                'label' => Yii::t('common', 'Загрузить тарифы из файла'),
            ]) ?>
        </div>
    </div>
<?php endif; ?>

<div class="row margin-bottom-10">
    <div class="col-lg-12 help-block">
        <?= Yii::t('common', 'По умолчанию берется максимальный тариф') ?>
    </div>
</div>

<div class="group-delivery-charges-for-region"
     data-template="<?= Html::encode($this->render('form', [
             'form' => $form,
             'model' => $model,
             'regionAttribute' => $regionAttribute,
             'chargesAttributes' => $chargesAttributes,
             'chargesArrayAttributes' => $chargesArrayAttributes,
             'index' => null,
             'onlyView' => $onlyView,
         ])) ?>">
    <?php if ($model->$regionAttribute): ?>
        <?php foreach ($model->$regionAttribute as $key => $value): ?>
            <?= $this->render('form', [
                'form' => $form,
                'model' => $model,
                'regionAttribute' => $regionAttribute,
                'chargesAttributes' => $chargesAttributes,
                'chargesArrayAttributes' => $chargesArrayAttributes,
                'index' => $key,
                'onlyView' => $onlyView,
            ]) ?>
        <?php endforeach; ?>
    <?php else: ?>
        <?= $this->render('form', [
            'form' => $form,
            'model' => $model,
            'regionAttribute' => $regionAttribute,
            'chargesAttributes' => $chargesAttributes,
            'index' => null,
            'onlyView' => $onlyView,
        ]) ?>
    <?php endif; ?>
</div>
<?php if (!$onlyView): ?>
    <div class="row margin-bottom-20">
        <div class="col-lg-12">
            <?= Button::widget([
                'type' => 'button',
                'icon' => WbIcon::PLUS,
                'style' => Button::STYLE_SUCCESS . ' btn-delivery-charges-by-region btn-delivery-charges-by-region-plus',
            ]) ?>
        </div>
    </div>
<?php endif; ?>
<?php if (!$onlyView): ?>
    <?= DeliveryChargesForRegionFromTable::widget([
        'model' => $model,
        'form' => $form,
        'deliveryChargesForRegionWidget' => $widget,
    ]) ?>
<?php endif; ?>
