<?php

use app\helpers\WbIcon;
use app\widgets\Button;
use yii\helpers\Html;

/**
 * @var \app\modules\delivery\components\charges\DeliveryChargeByZip $model
 * @var string $regionAttribute
 * @var array $chargesAttributes
 * @var \app\components\widgets\ActiveForm $form
 * @var integer $index
 * @var bool $onlyView
 * @var bool $show
 * @var array $chargesArrayAttributes
 */

$show = (!isset($show) ? true : $show);
?>
<div class="col-lg-12 charges-container row <?= (!$show ? ' hide' : '') ?>">
<?php $countAttr = 1;
for ($rowID = 0; $rowID < $countAttr + 1; $rowID++): ?>
    <div class="row <?= ($rowID == $countAttr ?  ' hide charges-array-attr-default': '') ?>">
        <div class="col-lg-11">
            <?php foreach ($chargesArrayAttributes as $rowAttributes): ?>
                <div class="row">
                    <?php foreach ($rowAttributes as $attribute):
                        if (isset($model->{$attribute['attribute']}[$index]) && $countAttr < count($model->{$attribute['attribute']}[$index])):
                            $countAttr = count($model->{$attribute['attribute']}[$index]);
                        endif;
                        $name = ($rowID == $countAttr ?  '_': '') . Html::getInputName($model, $attribute['attribute']) . "[{$index}]" . '[]';
                        ?>
                        <div class="col-lg-<?= $attribute['length'] ?? 2 ?>">
                            <?php if ($attribute['type'] == 'select2List'): ?>
                                <?= $form->field($model, $attribute['attribute'])->select2List($attribute['items'], [
                                    'name' => $name,
                                    'value' => $model->{$attribute['attribute']}[$index][$rowID] ?? '',
                                    'disabled' => $onlyView
                                ])->label($rowID == 0 ? null : false); ?>
                            <?php elseif ($attribute['type'] == 'checkbox'): ?>
                                <div class="margin-top-35">
                                    <?= $form->field($model, $attribute['attribute'])->checkboxCustom([
                                        'name' => $name,
                                        'checked' => $model->{$attribute['attribute']}[$index][$rowID] ?? false,
                                        'value' => 1,
                                        'disabled' => $onlyView
                                    ]); ?>
                                </div>
                            <?php else: ?>
                                <?= $form->field($model, $attribute['attribute'])->textInput([
                                    'name' => $name,
                                    'value' => $model->{$attribute['attribute']}[$index][$rowID] ?? '',
                                    'disabled' => $onlyView,
                                ]); ?>
                            <?php endif; ?>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="col-lg-1 margin-top-25 margin-bottom-20">
            <?php if (!$onlyView): ?>
                <?= Button::widget([
                    'type' => 'button',
                    'icon' => WbIcon::MINUS,
                    'style' => Button::STYLE_DANGER . ' btn-percent-buyout-minus',
                ]) ?>
            <?php endif; ?>
        </div>
    </div>
    <?php if (!$onlyView && $rowID == ($countAttr - 1)): ?>
        <div class="row charges-panel">
            <div class="col-lg-offset-11 col-lg-1">
                <?= Button::widget([
                    'type' => 'button',
                    'icon' => WbIcon::PLUS,
                    'style' => Button::STYLE_SUCCESS . ' btn-percent-buyout-plus margin-top-5',
                ]) ?>
            </div>
        </div>
    <?php endif; ?>
<?php endfor; ?>
</div>
