<?php

use app\widgets\Modal;

/**
 * @var \app\modules\delivery\components\charges\ChargesCalculatorAbstract $model
 * @var \yii\bootstrap\ActiveForm $form
 * @var \app\modules\delivery\widgets\charges\helpers\DeliveryChargesForRegion $deliveryChargesForRegionWidget
 */

?>

<?= Modal::widget([
    'id' => 'modal_upload_charges_for_region_from_file',
    'title' => Yii::t('common', 'Загрузка тарифов из файла'),
    'body' => $this->render('_modal-body', [
        'url' => $form->action,
        'model' => $model,
        'form' => $form,
        'deliveryChargesForRegionWidget' => $deliveryChargesForRegionWidget,
    ]),
]) ?>