<?php

use app\modules\delivery\widgets\charges\helpers\DeliveryChargesForRegionFromTable;
use app\widgets\Button;
use app\widgets\ButtonProgress;
use app\widgets\InputGroupFile;
use app\widgets\InputText;
use yii\helpers\Html;

/**
 * @var \app\modules\delivery\components\charges\ChargesCalculatorAbstract $model
 * @var \yii\bootstrap\ActiveForm $form
 * @var \app\modules\delivery\widgets\charges\helpers\DeliveryChargesForRegion $deliveryChargesForRegionWidget
 */

?>

<div class="modal-upload-charges-for-region-from-file">
    <div class="start-block">
        <div class="row start-block-text">
            <div class="col-lg-12 text-center">
                <?= Yii::t('common', 'Укажите в каких колонках находятся соответствующие данные и прикрепите файл с индексами и тарифами.') ?>
            </div>
        </div>
        <div class="row margin-top-20">
            <div class="col-lg-12 text-center">
                <h5>
                    <?= Yii::t('common', 'Укажите буквы колонок, в которых содержатся соответствующие данные:') ?>
                </h5>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
                <?= $model->getAttributeLabel($deliveryChargesForRegionWidget->regionAttribute) ?>
            </div>
            <div class="col-lg-4">
                <?= InputText::widget([
                    'name' => DeliveryChargesForRegionFromTable::formName() . "[{$deliveryChargesForRegionWidget->regionAttribute}]"
                ]) ?>
                <?= Html::hiddenInput(DeliveryChargesForRegionFromTable::formName() . '[' . DeliveryChargesForRegionFromTable::regionAttributeContainerName() . ']', $deliveryChargesForRegionWidget->regionAttribute) ?>
            </div>
        </div>
        <?php foreach ($deliveryChargesForRegionWidget->chargesAttributes as $chargesAttribute): ?>
            <div class="row padding-top-5">
                <div class="col-lg-8">
                    <?= $model->getAttributeLabel($chargesAttribute) ?>
                </div>
                <div class="col-lg-4">
                    <?= InputText::widget([
                        'name' => DeliveryChargesForRegionFromTable::formName() . "[{$chargesAttribute}]"
                    ]) ?>
                    <?= Html::hiddenInput(DeliveryChargesForRegionFromTable::formName() . '[' . DeliveryChargesForRegionFromTable::chargesAttributesContainerName() . '][]', $chargesAttribute) ?>
                </div>
            </div>
        <?php endforeach; ?>
        <div class="row margin-top-20">
            <div class="col-lg-12 text-center">
                <h5><?= Yii::t('common', 'Укажите файл с таблицей тарифов:') ?></h5>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 file-field-container">
                <?= InputGroupFile::widget([
                    'right' => false,
                    'input' => InputText::widget([
                        'type' => 'file',
                        'name' => DeliveryChargesForRegionFromTable::fileFieldName(),
                    ]),
                ]) ?>
            </div>
        </div>

        <div class="row-with-btn-start padding-top-20 text-right">
            <?= ButtonProgress::widget([
                'id' => 'btn_start_upload_charges_for_region_from_file',
                'label' => Yii::t('common', 'Загрузить'),
                'style' => Button::STYLE_SUCCESS,
                'size' => Button::SIZE_SMALL,
            ]) ?>

            <?= Button::widget([
                'label' => Yii::t('common', 'Закрыть'),
                'size' => Button::SIZE_SMALL,
                'attributes' => [
                    'data-dismiss' => 'modal',
                ],
            ]) ?>
        </div>
    </div>
</div>
