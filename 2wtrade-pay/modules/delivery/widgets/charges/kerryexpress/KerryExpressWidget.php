<?php

namespace app\modules\delivery\widgets\charges\kerryexpress;

use app\modules\delivery\widgets\charges\ChargesWidget;

/**
 * Class KerryExpressWidget
 * @package app\modules\delivery\widgets\charges\acommerce
 */
class KerryExpressWidget extends ChargesWidget
{
    /**
     *
     */
    public function run()
    {
        return $this->render('kerry-express-widget', [
            'form' => $this->form,
            'model' => $this->chargesCalculator,
            'onlyView' => $this->onlyView,
            'currencies' => $this->currencies
        ]);
    }
}