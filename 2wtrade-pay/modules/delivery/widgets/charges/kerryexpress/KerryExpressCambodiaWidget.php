<?php

namespace app\modules\delivery\widgets\charges\kerryexpress;

use app\modules\delivery\widgets\charges\ChargesWidget;

/**
 * Class KerryExpressCambodiaWidget
 * @package app\modules\delivery\widgets\charges\acommerce
 */
class KerryExpressCambodiaWidget extends ChargesWidget
{
    /**
     *
     */
    public function run()
    {
        return $this->render('kerry-express-cambodia-widget', [
            'form' => $this->form,
            'model' => $this->chargesCalculator,
            'onlyView' => $this->onlyView,
            'currencies' => $this->currencies
        ]);
    }
}