<?php
/**
 * @var \app\modules\delivery\components\charges\kerryexpress\KerryExpressCambodia $model
 * @var \app\components\widgets\ActiveForm $form
 * @var array $currencies
 * @var bool $onlyView
 */

use app\modules\delivery\assets\DeliveryChargesForRegionAsset;
use app\modules\delivery\widgets\charges\helpers\DeliveryChargesForRegion;

DeliveryChargesForRegionAsset::register($this);
?>
<div class="row">
    <div class="col-lg-12">
        <h3><?= Yii::t('common', 'Общие тарифы') ?></h3>
    </div>
</div>
<div class="row row-same-height">
    <div class="col-lg-3 col-lg-height col-bottom">
        <?= $form->field($model, 'price_fulfilment')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-3 col-lg-height col-bottom">
        <?= $form->field($model, 'price_package')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-3 col-lg-height col-bottom">
        <?= $form->field($model, 'cod_fee')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-3 col-lg-height col-bottom">
        <?= $form->field($model, 'cod_vat')->textInput(['disabled' => $onlyView]) ?>
    </div>
</div>
<div class="row row-same-height">
    <div class="col-lg-3 col-lg-height col-bottom">
        <?= $form->field($model, 'storage_fee')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-3 col-lg-height col-bottom">
        <?= $form->field($model, 'oversea_handle')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-3 col-lg-height col-bottom">
        <?= $form->field($model, 'currency_char_code')->select2List($currencies, [
            'length' => false,
            'disabled' => $onlyView
        ]) ?>
    </div>
    <div class="col-lg-3">
    </div>
</div>
<?= DeliveryChargesForRegion::widget([
    'form' => $form,
    'model' => $model,
    'regionAttribute' => 'zipcodes',
    'chargesAttributes' => [
        'delivery_fees',
        'return_fees',
    ],
    'onlyView' => $onlyView
]) ?>