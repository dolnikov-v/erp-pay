<?php
/**
 * @var \app\modules\delivery\components\charges\kerryexpress\KerryExpress $model
 * @var \app\components\widgets\ActiveForm $form
 * @var array $currencies
 * @var bool $onlyView
 */

use app\modules\delivery\assets\DeliveryChargesForRegionAsset;
use app\modules\delivery\widgets\charges\helpers\DeliveryChargesForRegion;

DeliveryChargesForRegionAsset::register($this);
?>
<div class="row">
    <div class="col-lg-12">
        <h3><?= Yii::t('common', 'Общие тарифы') ?></h3>
    </div>
</div>
<div class="row margin-top-10 margin-bottom-20">
    <div class="col-lg-2">
        <?= $form->field($model, 'return_fee')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'buyout_fee')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'storage_area')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'storage_fee')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'cod_fee')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'cod_vat')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'delivery_fee_vat')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'currency_char_code')->select2List($currencies, [
            'length' => false,
            'disabled' => $onlyView
        ]) ?>
    </div>
</div>
<?= DeliveryChargesForRegion::widget([
    'form' => $form,
    'model' => $model,
    'regionAttribute' => 'zipcodes',
    'chargesAttributes' => [
        'delivery00_fees',
        'delivery10_fees',
        'delivery12_fees',
        'delivery14_fees',
        'delivery16_fees',
        'delivery18_fees',
    ],
    'onlyView' => $onlyView
]) ?>
