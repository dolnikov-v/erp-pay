<?php

namespace app\modules\delivery\widgets\charges\nex;

use app\modules\delivery\widgets\charges\ChargesWidget;

/**
 * Class NexWidget
 * @package app\modules\delivery\widgets\charges\nex
 */
class NexWidget extends ChargesWidget
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('nex-widget', ['form' => $this->form, 'model' => $this->chargesCalculator, 'currencies' => $this->currencies, 'onlyView' => $this->onlyView]);
    }
}