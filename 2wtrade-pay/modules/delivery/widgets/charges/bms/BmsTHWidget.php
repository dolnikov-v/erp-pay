<?php

namespace app\modules\delivery\widgets\charges\bms;


use app\modules\delivery\widgets\charges\ChargesWidget;

/**
 * Class BmsTHWidget
 * @package app\modules\delivery\widgets\charges\bms
 */
class BmsTHWidget extends ChargesWidget
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('bms-th-widget/index', ['form' => $this->form, 'values' => $this->values, 'chargesCalculator' => $this->chargesCalculator, 'currencies' => $this->currencies, 'onlyView' => $this->onlyView]);
    }
}