<?php
/**
 * @var \app\components\widgets\ActiveForm $form
 * @var array $values
 * @var \app\modules\delivery\components\charges\ChargesCalculatorAbstract $chargesCalculator
 * @var array $currencies
 * @var bool $onlyView
 */

?>

<div class="row">
    <div class="col-lg-12">
        <h3><?= Yii::t('common', '< 500 шт. заказов в месяц') ?></h3>
    </div>
</div>
<div class="row margin-top-10">
    <div class="col-lg-6 text-center">
        <h4><?= Yii::t('common', 'Плата за доставку') ?></h4>
    </div>
    <div class="col-lg-6 text-center">
        <h4><?= Yii::t('common', 'Плата за возврат') ?></h4>
    </div>
</div>

<div class="row">
    <div class="col-lg-2">
        <?= $form->field($chargesCalculator, 'delivery_fee_one_prod_less_five')
            ->textInput(['disabled' => $onlyView])
            ->label(Yii::t('common', '1 шт. в заказе ({currency})', ['currency' => $chargesCalculator->delivery->country->currency->char_code])) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($chargesCalculator, 'delivery_fee_two_prod_less_five')
            ->textInput(['disabled' => $onlyView])
            ->label(Yii::t('common', '2-3 шт. в заказе ({currency})', ['currency' => $chargesCalculator->delivery->country->currency->char_code])) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($chargesCalculator, 'delivery_fee_three_prod_less_five')
            ->textInput(['disabled' => $onlyView])
            ->label(Yii::t('common', 'более 3 шт. в заказе ({currency})', ['currency' => $chargesCalculator->delivery->country->currency->char_code])) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($chargesCalculator, 'return_fee_one_prod_less_five')
            ->textInput(['disabled' => $onlyView])
            ->label(Yii::t('common', '1 шт. в заказе ({currency})', ['currency' => $chargesCalculator->delivery->country->currency->char_code])) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($chargesCalculator, 'return_fee_two_prod_less_five')
            ->textInput(['disabled' => $onlyView])
            ->label(Yii::t('common', '2-3 шт. в заказе ({currency})', ['currency' => $chargesCalculator->delivery->country->currency->char_code])) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($chargesCalculator, 'return_fee_three_prod_less_five')
            ->textInput(['disabled' => $onlyView])
            ->label(Yii::t('common', 'более 3 шт. в заказе ({currency})', ['currency' => $chargesCalculator->delivery->country->currency->char_code])) ?>
    </div>
</div>

<div class="row margin-top-10">
    <div class="col-lg-12">
        <h3><?= Yii::t('common', '500 - 1000 шт. заказов в месяц') ?></h3>
    </div>
</div>
<div class="row margin-top-10">
    <div class="col-lg-6 text-center">
        <h4><?= Yii::t('common', 'Плата за доставку') ?></h4>
    </div>
    <div class="col-lg-6 text-center">
        <h4><?= Yii::t('common', 'Плата за возврат') ?></h4>
    </div>
</div>

<div class="row">
    <div class="col-lg-2">
        <?= $form->field($chargesCalculator, 'delivery_fee_one_prod_more_five')
            ->textInput(['disabled' => $onlyView])
            ->label(Yii::t('common', '1 шт. в заказе ({currency})', ['currency' => $chargesCalculator->delivery->country->currency->char_code])) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($chargesCalculator, 'delivery_fee_two_prod_more_five')
            ->textInput(['disabled' => $onlyView])
            ->label(Yii::t('common', '2-3 шт. в заказе ({currency})', ['currency' => $chargesCalculator->delivery->country->currency->char_code])) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($chargesCalculator, 'delivery_fee_three_prod_more_five')
            ->textInput(['disabled' => $onlyView])
            ->label(Yii::t('common', 'более 3 шт. в заказе ({currency})', ['currency' => $chargesCalculator->delivery->country->currency->char_code])) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($chargesCalculator, 'return_fee_one_prod_more_five')
            ->textInput(['disabled' => $onlyView])
            ->label(Yii::t('common', '1 шт. в заказе ({currency})', ['currency' => $chargesCalculator->delivery->country->currency->char_code])) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($chargesCalculator, 'return_fee_two_prod_more_five')
            ->textInput(['disabled' => $onlyView])
            ->label(Yii::t('common', '2-3 шт. в заказе ({currency})', ['currency' => $chargesCalculator->delivery->country->currency->char_code])) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($chargesCalculator, 'return_fee_three_prod_more_five')
            ->textInput(['disabled' => $onlyView])
            ->label(Yii::t('common', 'более 3 шт. в заказе ({currency})', ['currency' => $chargesCalculator->delivery->country->currency->char_code])) ?>
    </div>
</div>

<div class="row margin-top-10">
    <div class="col-lg-12">
        <h3><?= Yii::t('common', '> 1000 шт. заказов в месяц') ?></h3>
    </div>
</div>
<div class="row margin-top-10">
    <div class="col-lg-6 text-center">
        <h4><?= Yii::t('common', 'Плата за доставку') ?></h4>
    </div>
    <div class="col-lg-6 text-center">
        <h4><?= Yii::t('common', 'Плата за возврат') ?></h4>
    </div>
</div>

<div class="row">
    <div class="col-lg-2">
        <?= $form->field($chargesCalculator, 'delivery_fee_one_prod_more_1k')
            ->textInput(['disabled' => $onlyView])
            ->label(Yii::t('common', '1 шт. в заказе ({currency})', ['currency' => $chargesCalculator->delivery->country->currency->char_code])) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($chargesCalculator, 'delivery_fee_two_prod_more_1k')
            ->textInput(['disabled' => $onlyView])
            ->label(Yii::t('common', '2-3 шт. в заказе ({currency})', ['currency' => $chargesCalculator->delivery->country->currency->char_code])) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($chargesCalculator, 'delivery_fee_three_prod_more_1k')
            ->textInput(['disabled' => $onlyView])
            ->label(Yii::t('common', 'более 3 шт. в заказе ({currency})', ['currency' => $chargesCalculator->delivery->country->currency->char_code])) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($chargesCalculator, 'return_fee_one_prod_more_1k')
            ->textInput(['disabled' => $onlyView])
            ->label(Yii::t('common', '1 шт. в заказе ({currency})', ['currency' => $chargesCalculator->delivery->country->currency->char_code])) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($chargesCalculator, 'return_fee_two_prod_more_1k')
            ->textInput(['disabled' => $onlyView])
            ->label(Yii::t('common', '2-3 шт. в заказе ({currency})', ['currency' => $chargesCalculator->delivery->country->currency->char_code])) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($chargesCalculator, 'return_fee_three_prod_more_1k')
            ->textInput(['disabled' => $onlyView])
            ->label(Yii::t('common', 'более 3 шт. в заказе ({currency})', ['currency' => $chargesCalculator->delivery->country->currency->char_code])) ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <h3><?= Yii::t('common', 'Дополнительные расходы') ?></h3>
    </div>
</div>

<div class="row margin-top-10">
    <div class="col-lg-3">
        <?= $form->field($chargesCalculator, 'vat_percent')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($chargesCalculator, 'fulfilment_fee')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($chargesCalculator, 'storage_fee')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($chargesCalculator, 'bms_acommerce_bms')->textInput(['disabled' => $onlyView]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-2">
        <?= $form->field($chargesCalculator, 'currency_char_code')->select2List($currencies, [
            'length' => false,
            'disabled' => $onlyView
        ]) ?>
    </div>
</div>
