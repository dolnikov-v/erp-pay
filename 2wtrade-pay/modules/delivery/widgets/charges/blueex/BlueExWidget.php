<?php

namespace app\modules\delivery\widgets\charges\blueex;

use app\modules\delivery\widgets\charges\ChargesWidget;

/**
 * Class BlueExWidget
 * @package app\modules\delivery\widgets\charges\blueex
 */
class BlueExWidget extends ChargesWidget
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('blueex-widget', [
            'form' => $this->form,
            'model' => $this->chargesCalculator,
            'currencies' => $this->currencies,
            'onlyView' => $this->onlyView
        ]);
    }
}