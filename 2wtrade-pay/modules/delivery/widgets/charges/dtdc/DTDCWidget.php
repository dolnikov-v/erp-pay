<?php

namespace app\modules\delivery\widgets\charges\dtdc;

use app\modules\delivery\widgets\charges\ChargesWidget;

/**
 * Class DTDCWidget
 * @package app\modules\delivery\widgets\charges\dtdc
 */
class DTDCWidget extends ChargesWidget
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('dtdc-widget', ['form' => $this->form, 'model' => $this->chargesCalculator]);
    }
}