<?php
/**
 * @var \app\modules\delivery\components\charges\dtdc\DTDC $model
 * @var \app\components\widgets\ActiveForm $form
 */

?>

<div class="row">
    <div class="col-lg-2">
        <?= $form->field($model, 'stock_in_fee')->textInput() ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'stock_out_fee')->textInput() ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'labeling_fee')->textInput() ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'delivery_fee')->textInput() ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'delivery_fee_add_kg')->textInput() ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'storage_fee')->textInput() ?>
    </div>
</div>