<?php

namespace app\modules\delivery\widgets\charges\pcp;


use app\modules\delivery\widgets\charges\ChargesWidget;

/**
 * Class PCPWidget
 * @package app\modules\delivery\widgets\charges\pcp
 */
class PCPWidget extends ChargesWidget
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('pcp-widget', ['form' => $this->form, 'model' => $this->chargesCalculator, 'currencies' => $this->currencies, 'onlyView' => $this->onlyView]);
    }
}