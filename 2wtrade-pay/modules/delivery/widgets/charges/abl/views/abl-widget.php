<?php
/**
 * @var \app\modules\delivery\components\charges\abl\ABL $model
 * @var \app\components\widgets\ActiveForm $form
 * @var array $currencies
 * @var bool $onlyView
 */
?>

<div class="row">
    <div class="col-lg-2">
        <?= $form->field($model, 'fulfilment_fee')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'delivery_fee')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'default_weight')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'delivery_fee_for_add_weight')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'return_fee')->textInput(['disabled' => $onlyView]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-2">
        <?= $form->field($model, 'cod_fee')->textInput(['disabled' => $onlyView]) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'vat_percent')->textInput(['disabled' => $onlyView]) ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-2">
        <?= $form->field($model, 'currency_char_code')->select2List($currencies, [
            'length' => false,
            'disabled' => $onlyView
        ]) ?>
    </div>
</div>
