<?php

namespace app\modules\delivery\widgets\charges\abl;

use app\modules\delivery\widgets\charges\ChargesWidget;

/**
 * Class ABLWidget
 * @package app\modules\delivery\widgets\charges\abl
 */
class ABLWidget extends ChargesWidget
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('abl-widget', ['form' => $this->form, 'model' => $this->chargesCalculator, 'onlyView' => $this->onlyView, 'currencies' => $this->currencies]);
    }
}