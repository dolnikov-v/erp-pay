<?php

namespace app\modules\delivery\widgets\charges;

use app\components\widgets\ActiveForm;
use app\modules\delivery\components\charges\ChargesCalculatorAbstract;
use app\widgets\Widget;

/**
 * Class ChargesWidget
 * @package app\modules\delivery\widgets\charges
 */
abstract class ChargesWidget extends Widget
{
    /**
     * @var ActiveForm
     */
    public $form;

    /**
     * @var array
     */
    public $values;

    /**
     * @var ChargesCalculatorAbstract
     */
    public $chargesCalculator;

    /**
     * @var array
     */
    public $currencies;

    /**
     * @var bool
     */
    public $onlyView = false;
}