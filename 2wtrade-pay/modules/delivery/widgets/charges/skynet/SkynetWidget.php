<?php

namespace app\modules\delivery\widgets\charges\skynet;


use app\modules\delivery\widgets\charges\ChargesWidget;

/**
 * Class SkynetWidget
 * @package app\modules\delivery\widgets\charges\skynet
 */
class SkynetWidget extends ChargesWidget
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('skynet-widget', ['form' => $this->form, 'model' => $this->chargesCalculator]);
    }
}