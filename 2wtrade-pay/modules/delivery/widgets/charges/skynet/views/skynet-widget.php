<?php
/**
 * @var \app\modules\delivery\components\charges\skynet\Skynet $model
 * @var \app\components\widgets\ActiveForm $form
 */
?>

<div class="row">
    <div class="col-lg-3">
        <?= $form->field($model, 'delivery_fee')->textInput() ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'return_fee')->textInput() ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'storage_fee')->textInput() ?>
    </div>
</div>
