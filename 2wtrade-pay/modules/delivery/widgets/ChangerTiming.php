<?php
namespace app\modules\delivery\widgets;

use app\modules\delivery\models\DeliveryTiming;
use yii\base\Widget;

/**
 * Class ChangerTiming
 * @package app\modules\delivery\widgets
 */
class ChangerTiming extends Widget
{
    /**
     * @var DeliveryTiming
     */
    public $model;

    /**
     * @var bool
     */
    public $showButtons = true;

    /**
     * @var bool
     */
    public $showTimingTo = true;

    /**
     * @return string
     */
    public function run()
    {
        $offsetFrom = '';
        $offsetTo = '';

        if (!$this->model->isNewRecord) {
            $offsetFrom = date('H:i:s', $this->model->offset_from);
            $offsetTo = date('H:i:s', $this->model->offset_to);
        }

        return $this->render('changer-timing/index', [
            'model' => $this->model,
            'offsetFrom' => $offsetFrom,
            'offsetTo' => $offsetTo,
            'showButtons' => $this->showButtons,
            'showTimingTo' => $this->showTimingTo,
        ]);
    }
}
