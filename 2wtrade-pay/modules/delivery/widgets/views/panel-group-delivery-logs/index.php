<?php
use app\components\grid\DateColumn;
use app\components\grid\GridView;

/** @var \yii\data\ArrayDataProvider $dataProvider */
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'user.username',
        [
            'label' => Yii::t('common', 'Поле'),
            'content' => function($model) {
                return nl2br($model->field);
            }
        ],
        [
            'label' => Yii::t('common', 'Старое значение'),
            'content' => function($model) {
                return nl2br($model->old);
            }
        ],
        [
            'label' => Yii::t('common', 'Новое значение'),
            'content' => function($model) {
                return nl2br($model->new);
            }
        ],
        [
            'class' => DateColumn::className(),
            'attribute' => 'created_at',
            'enableSorting' => false,
        ]
    ]
]) ?>
