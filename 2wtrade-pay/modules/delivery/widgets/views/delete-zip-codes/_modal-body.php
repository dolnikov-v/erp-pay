<?php
use app\components\widgets\ActiveForm;
use app\widgets\Button;
use app\widgets\ProgressBar;
use yii\helpers\Url;
use yii\helpers\Html;

/** @var string $url */
/** @var integer $delivery_id */
?>

<?php $form = ActiveForm::begin([
    'enableClientValidation' => false,
    'action' => Url::toRoute('index'),
    'method' => 'post'
]); ?>

<div class="row-with-text-start">
    <div class="text-center">
        <?= Yii::t('common', 'Введите список индексов для удаления из группы') ?>
    </div>
    <div class="row help-block">
        <div class="col-xs-1"></div>
        <div class="col-xs-10">
            <?= Html::textarea(
                'zip-codes-to-delete',
                '',
                [
                    'id' => 'zip-codes-to-delete',
                    'class' => 'form-control',
                    'data-delivery' => $delivery_id
                ]
            ) ?>
        </div>
        <div class="col-xs-1"></div>
    </div>

</div>


<div class="padding-top-20">
    <div class="row-with-btn-start text-right">
        <?= Button::widget([
            'id' => 'start_delete_zip_codes',
            'label' => Yii::t('common', 'Удалить'),
            'style' => Button::STYLE_SUCCESS,
            'size' => Button::SIZE_SMALL,
        ]) ?>

        <?= Button::widget([
            'label' => Yii::t('common', 'Закрыть'),
            'size' => Button::SIZE_SMALL,
            'attributes' => [
                'data-dismiss' => 'modal',
            ],
        ]) ?>
    </div>

</div>

<?php ActiveForm::end(); ?>
