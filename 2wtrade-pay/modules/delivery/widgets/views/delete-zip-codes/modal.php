<?php
use app\widgets\Modal;

/** @var string $url */
/** @var integer $delivery_id */
?>

<?= Modal::widget([
    'id' => 'modal_delete_zip_codes',
    'title' => Yii::t('common', 'Удалить индексы'),
    'body' => $this->render('_modal-body', [
        'delivery_id' => $delivery_id
    ]),
]) ?>
<?= Modal::widget([
    'id' => 'modal_delete_cities',
    'title' => Yii::t('common', 'Удалить города'),
    'body' => $this->render('_modal-body-cities', [
        'delivery_id' => $delivery_id
    ]),
]) ?>
