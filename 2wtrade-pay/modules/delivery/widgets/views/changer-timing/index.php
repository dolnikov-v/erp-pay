<?php
use app\helpers\i18n\Moment;

/** @var \app\modules\delivery\models\DeliveryTiming $model */
/** @var string $offsetFrom */
/** @var string $offsetTo */
/** @var bool $showButtons */
/** @var bool $showTimingTo */
?>

<div class="container-changer-timing">
    <div class="inputs-changer-timing">
        <div class="input-group">
            <span class="input-group-addon">
                <i class="icon wb-time" aria-hidden="true"></i>
            </span>
            <input class="form-control input-timing no-init" name="Timing[from][]" type="text" data-format="<?= Moment::getFullTimeFormat() ?>"
                   value="<?= $offsetFrom ?>"/>
        </div>

        <?php if ($showTimingTo): ?>

            <div class="input-group">
                <span class="input-group-addon">—</span>
                <input class="form-control input-timing no-init" name="Timing[to][]" type="text" data-format="<?= Moment::getFullTimeFormat() ?>"
                       value="<?= $offsetTo ?>"/>
            </div>

        <?php endif; ?>
    </div>

    <?php if ($showButtons): ?>

        <?php if ($model->isNewRecord): ?>
            <button class="btn btn-success btn-changer-timing add-changer-timing" type="button">
                <i class="icon wb-plus"></i>
            </button>
        <?php endif; ?>

        <button class="btn btn-danger btn-changer-timing delete-changer-timing" type="button"
                <?php if ($model->isNewRecord): ?>style="display: none;"<?php endif; ?>>
            <i class="icon wb-minus"></i>
        </button>

    <?php endif; ?>

    <span class="changer-timing-warning"><?= Yii::t('common', 'Время по гринвичу (GMT +00:00).') ?></span>
</div>
