<?php
use app\modules\delivery\widgets\StorageSwitchery;
use app\modules\storage\models\Storage;

/** @var app\modules\delivery\models\Delivery $model */
/** @var Storage[] $storages */
/** @var Storage[] $storagesByDelivery */
?>
<table class="table table-striped table-hover" data-delivery-id="<?= $model->id ?>">
    <thead>
    <tr>
        <th><?= Yii::t('common', 'Название') ?></th>
        <th class="text-center width-150"><?= Yii::t('common', 'Вкл/Выкл') ?></th>
    </tr>
    </thead>
    <tbody>
    <?php if (!empty($storages)) : ?>
        <?php foreach ($storages as $storage) : ?>
            <tr>
                <td><?= Yii::t('common', $storage->name) ?></td>
                <td class="text-center" data-storage-id="<?= $storage->id ?>">
                    <?= StorageSwitchery::widget([
                        'checked' => call_user_func(function () use ($storage, $storagesByDelivery) {
                            foreach ($storagesByDelivery as $storageByDelivery) {
                                if ($storageByDelivery->id == $storage->id) {
                                    return true;
                                }
                            }

                            return false;
                        }),
                    ]); ?>
                </td>
            </tr>
        <?php endforeach; ?>
    <?php else: ?>
        <tr>
            <td class="text-center" colspan="2"><?= Yii::t('common', 'Склады отсутствуют') ?></td>
        </tr>
    <?php endif ?>
    </tbody>
</table>
