<?php
/** @var $this \yii\web\View */
/** @var $emailList string[] */

?>

<div class="container-email-list">
    <?php foreach ($emailList as $email): ?>
        <div class="email-list-element row">
            <div class="form-group col-lg-4">
                <input class="form-control" name="Delivery[list_send_to_emails][]" type="text" value="<?= $email ?>">
            </div>
            <button class="btn btn-danger btn-email-list delete-email-element" type="button">
                <i class="icon wb-minus"></i>
            </button>
        </div>
    <?php endforeach; ?>
    <div class="email-list-element row">
        <div class="form-group col-lg-4">
            <input class="form-control" name="Delivery[list_send_to_emails][]" type="text">
        </div>
        <button class="btn btn-success btn-email-list add-email-element" type="button">
            <i class="icon wb-plus"></i>
        </button>
        <button class="btn btn-danger btn-email-list delete-email-element" type="button" style="display: none;">
            <i class="icon wb-minus"></i>
        </button>
    </div>
</div>
