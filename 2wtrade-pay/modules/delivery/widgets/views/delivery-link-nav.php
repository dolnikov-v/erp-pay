<?php

/** @var string $id */
/** @var string $typeNav */
/** @var string $typeTabs */
/** @var array $tabs */

?>

<ul <?php if ($id): ?>id="<?= $id ?>"<?php endif; ?> class="nav link-nav-tabs <?= $typeNav ?> <?= $typeTabs ?>"
    data-plugin="nav-tabs">
    <?php foreach ($tabs as $key => $tab): ?>
        <?php if ($tab['can']): ?>
            <li class="<?php if ($tab['active']): ?>active<?php endif; ?>">
                <a href="<?= $tab['url'] ?>"><?= $tab['label'] ?></a>
            </li>
        <?php endif; ?>
    <?php endforeach; ?>
</ul>
