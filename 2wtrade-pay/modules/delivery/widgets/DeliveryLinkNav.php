<?php
namespace app\modules\delivery\widgets;

use app\modules\delivery\models\Delivery;
use app\widgets\Nav;
use Yii;
use yii\helpers\Url;

/**
 * Class DeliveryLinkNav
 * @package app\modules\delivery\widgets
 */
class DeliveryLinkNav extends Nav
{
    /**
     * @var Delivery;
     */
    public $delivery;


    public function init()
    {
        $actionId = Yii::$app->controller->action->id;

        $contract = $this->delivery->getActiveContract();
        $contractId = $contract->id ?? 0;

        $this->tabs = [
            [
                'label' => Yii::t('common', 'Просмотр'),
                'url' => Url::toRoute(['view', 'id' => $this->delivery->id]),
                'can' => Yii::$app->user->can('delivery.control.view') && $this->delivery->id,
                'active' => $actionId == 'view'
            ],
            [
                'label' => Yii::t('common', 'Основная информация'),
                'url' => Url::toRoute(['edit', 'id' => $this->delivery->id]),
                'can' => Yii::$app->user->can('delivery.control.edit'),
                'active' => $actionId == 'edit'
            ],
            [
                'label' => Yii::t('common', 'Системные настройки'),
                'url' => Url::toRoute(['edit-system-settings', 'id' => $this->delivery->id]),
                'can' => Yii::$app->user->can('delivery.control.editsystemsettings') && $this->delivery->id,
                'active' => $actionId == 'edit-system-settings'
            ],
            [
                'label' => Yii::t('common', 'Листы'),
                'url' => Url::toRoute(['edit-auto-list', 'id' => $this->delivery->id]),
                'can' => Yii::$app->user->can('delivery.control.editautolist') && $this->delivery->id,
                'active' => $actionId == 'edit-auto-list'
            ],
            [
                'label' => Yii::t('common', 'Сроки'),
                'url' => Url::toRoute(['edit-timings', 'id' => $this->delivery->id]),
                'can' => Yii::$app->user->can('delivery.control.edittimings') && $this->delivery->id,
                'active' => $actionId == 'edit-timings'
            ],
            [
                'label' => Yii::t('common', 'Претензии'),
                'url' => Url::toRoute(['edit-pretension', 'id' => $this->delivery->id]),
                'can' => Yii::$app->user->can('delivery.control.editpretension') && $this->delivery->id,
                'active' => $actionId == 'edit-pretension'
            ],
            [
                'label' => Yii::t('common', 'Платежи / инвойсирование'),
                'url' => Url::toRoute(['edit-payment-invoice', 'id' => $this->delivery->id]),
                'can' => Yii::$app->user->can('delivery.control.editpaymentinvoice') && $this->delivery->id,
                'active' => $actionId == 'edit-payment-invoice'
            ],
            [
                'label' => Yii::t('common', 'Брокер'),
                'url' => Url::toRoute(['edit-brokerage', 'id' => $this->delivery->id]),
                'can' => Yii::$app->user->can('delivery.control.editbrokerage') && $this->delivery->id,
                'active' => $actionId == 'edit-brokerage'
            ],
            [
                'label' => Yii::t('common', 'Продукты'),
                'url' => Url::toRoute(['edit-products', 'id' => $this->delivery->id]),
                'can' => Yii::$app->user->can('delivery.control.editproducts') && $this->delivery->id,
                'active' => $actionId == 'edit-products'
            ],
            [
                'label' => Yii::t('common', 'Склады'),
                'url' => Url::toRoute(['edit-storage', 'id' => $this->delivery->id]),
                'can' => Yii::$app->user->can('delivery.control.editstorage') && $this->delivery->id,
                'active' => $actionId == 'edit-storage'
            ],
            [
                'label' => Yii::t('common', 'Активный контракт'),
                'url' => Url::toRoute(['control/contract-edit', 'id' => $contractId]),
                'can' => Yii::$app->user->can('delivery.control.contractedit') && $this->delivery->id && $contractId,
                'active' => $actionId == 'contract-edit'
            ],
        ];
    }

    /**
     * @return string
     */
    public function renderTabs()
    {
        return $this->render('delivery-link-nav', [
            'id' => $this->id,
            'typeNav' => $this->typeNav,
            'typeTabs' => $this->typeTabs,
            'tabs' => $this->tabs,
        ]);
    }

    /**
     * @return string
     */
    public function renderContent()
    {
        return '';
    }
}
