<?php

namespace app\modules\delivery\assets;


use yii\web\AssetBundle;

/**
 * Class DeliveryInvoiceOptionsAsset
 * @package app\modules\delivery\assets
 */
class DeliveryInvoiceOptionsAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/delivery/delivery-invoice-options';

    public $js = [
        'delivery-invoice-options.js',
    ];

    public $css = [

    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
