<?php
namespace app\modules\delivery\assets;

use yii\web\AssetBundle;

/**
 * Class ContactsAsset
 * @package app\modules\delivery\assets
 */
class ContactsAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/delivery/delivery-contacts';

    public $js = [
        'delivery-contacts.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
