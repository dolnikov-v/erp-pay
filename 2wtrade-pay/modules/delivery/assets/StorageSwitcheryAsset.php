<?php
namespace app\modules\delivery\assets;

use yii\web\AssetBundle;

/**
 * Class StorageSwitcheryAsset
 * @package app\modules\delivery\assets
 */
class StorageSwitcheryAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/delivery/storage-switchery';

    public $js = [
        'storage-switchery.js',
    ];

    public $depends = [
        'app\assets\vendor\SwitcheryAsset',
    ];
}
