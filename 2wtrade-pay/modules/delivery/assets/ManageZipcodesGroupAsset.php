<?php
namespace app\modules\delivery\assets;

use yii\web\AssetBundle;

/**
 * Class ManageZipcodesGroupAsset
 * @package app\modules\delivery\assets
 */
class ManageZipcodesGroupAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/delivery/manage-zipcodes-group';

    public $js = [
        'manage-zipcodes-group.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
