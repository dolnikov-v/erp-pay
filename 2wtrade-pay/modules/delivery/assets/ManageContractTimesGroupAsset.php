<?php
namespace app\modules\delivery\assets;

use yii\web\AssetBundle;

/**
 * Class ManageContractTimesGroupAsset
 * @package app\modules\delivery\assets
 */
class ManageContractTimesGroupAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/delivery/manage-contract-times-group';

    public $js = [
        'manage-contract-times-group.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
