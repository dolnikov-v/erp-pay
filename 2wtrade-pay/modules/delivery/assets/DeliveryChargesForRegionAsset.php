<?php

namespace app\modules\delivery\assets;


use yii\web\AssetBundle;

/**
 * Class DeliveryChargesForRegionAsset
 * @package app\modules\delivery\assets
 */
class DeliveryChargesForRegionAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/delivery/delivery-charges-for-region';

    public $js = [
        'delivery-charges-for-region.js',
    ];

    public $css = [
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
        '\app\assets\vendor\BootstrapLaddaAsset',
    ];
}