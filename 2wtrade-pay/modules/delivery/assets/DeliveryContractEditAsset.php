<?php
namespace app\modules\delivery\assets;

use yii\web\AssetBundle;

/**
 * Class DeliveryContractEditAsset
 * @package app\modules\delivery\assets
 */
class DeliveryContractEditAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/delivery/delivery-contract';

    public $js = [
        'delivery-contract.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
        'app\widgets\assets\DatePickerAsset',
        'app\widgets\assets\InputGroupFileAsset',
    ];
}