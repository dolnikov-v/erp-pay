<?php
namespace app\modules\delivery\assets;

use yii\web\AssetBundle;

/**
 * Class ChargesArrayPropertyAsset
 * @package app\modules\delivery\assets
 */
class ChargesArrayPropertyAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/delivery/charges-array-property';

    public $js = [
        'changer-array-property.js',
    ];

    public $css = [
        'changer-array-property.css',
    ];
}
