<?php
namespace app\modules\delivery\assets;

use yii\web\AssetBundle;

/**
 * Class SkuAsset
 * @package app\modules\delivery\assets
 */
class SkuAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/delivery/delivery-sku';

    public $js = [
        'delivery-sku.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
