<?php
namespace app\modules\delivery\assets;

use yii\web\AssetBundle;

/**
 * Class DeliveryContractFileAsset
 * @package app\modules\delivery\assets
 */
class DeliveryContractFileAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/delivery/delivery-contract-file';

    public $js = [
        'delivery-contract-file.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}