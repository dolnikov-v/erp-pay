<?php
namespace app\modules\delivery\assets;

use yii\web\AssetBundle;

/**
 * Class AutoChangeReasonAsset
 * @package app\modules\delivery\assets
 */
class AutoChangeReasonAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/delivery/delivery-auto-change-reason';

    public $js = [
        'delivery-auto-change-reason.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
