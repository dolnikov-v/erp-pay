<?php
namespace app\modules\delivery\assets;

use yii\web\AssetBundle;

class TestAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/delivery/test';

    public $js = [
        'index.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
