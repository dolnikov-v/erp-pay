<?php
namespace app\modules\delivery\assets;

use yii\web\AssetBundle;

/**
 * Class ChangerTimingAsset
 * @package app\modules\delivery\assets
 */
class ChangerTimingAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/delivery/changer-timing';

    public $js = [
        'changer-timing.js',
    ];

    public $css = [
        'changer-timing.css',
    ];

    public $depends = [
        'app\widgets\assets\DatePickerAsset',
        'app\widgets\assets\DateTimePickerAsset',
    ];
}
