<?php
namespace app\modules\delivery\assets;

use yii\web\AssetBundle;

/**
 * Class ChargesArrayAttributesAsset
 * @package app\modules\delivery\assets
 */
class ChargesArrayAttributesAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/delivery/charges-array-attributes';

    public $js = [
        'changer-array-attribute.js',
    ];
}
