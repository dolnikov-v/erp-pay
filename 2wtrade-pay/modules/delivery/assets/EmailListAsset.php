<?php

namespace app\modules\delivery\assets;


use yii\web\AssetBundle;

/**
 * Class EmailListAsset
 * @package app\modules\delivery\assets
 */
class EmailListAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/delivery/email-list';

    public $js = [
        'email-list.js',
    ];

    public $css = [
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
