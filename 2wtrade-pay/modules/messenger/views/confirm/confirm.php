<?php

/**
 * @var \app\modules\messenger\abstracts\AnswerInterface $answer
 */
?>

<div <?= ($answer->isSuccess() ? '' : 'class="alert-danger"') ?>>
    <p>
        <?= $answer->getMessage() ?>
    </p>
</div>
