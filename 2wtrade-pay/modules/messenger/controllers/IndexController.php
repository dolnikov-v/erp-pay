<?php

namespace app\modules\messenger\controllers;

use app\modules\auth\components\filters\AccessControl;
use app\modules\messenger\models\Associative;
use app\components\rest\Controller;
use Yii;

/**
 * Class IndexController
 * @package app\modules\messenger\controllers
 */
class IndexController extends Controller
{
    /**
     * @return bool
     */
    public function actionIndex()
    {
        $data = array_merge(Yii::$app->getRequest()->get(), Yii::$app->getRequest()->post());

        $this->module->handlerWebhook(new Associative($data))->getAttributes();

        Yii::$app->response->statusCode = 200;
        return true;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        Yii::$app->user->enableSession = false;
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                    ],
                ],
            ],
        ];
    }
}