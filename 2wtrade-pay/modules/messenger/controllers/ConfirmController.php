<?php

namespace app\modules\messenger\controllers;

use app\modules\auth\components\filters\AccessControl;
use app\modules\messenger\abstracts\AnswerInterface;
use app\modules\messenger\models\Associative;
use app\components\web\Controller;
use Yii;

/**
 * Class ConfirmController
 * @package app\modules\messenger\controllers
 */
class ConfirmController extends Controller
{
    public $layout = 'unregister';

    /**
     * @return string
     */
    public function actionIndex()
    {
        $data = array_merge(Yii::$app->getRequest()->get(), Yii::$app->getRequest()->post());
        /** @var AnswerInterface $answer */
        $answer = $this->module->confirm(new Associative($data));
        if ($answer->isSuccess() && !$answer->getMessage()) {
            $answer->setMessage(Yii::t('common', 'Подписка подтверждена.'));
        }
        return $this->render('confirm', [
            'answer' => $answer,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        Yii::$app->user->enableSession = false;
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                    ],
                ],
            ],
        ];
    }
}