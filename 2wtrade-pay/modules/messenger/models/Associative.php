<?php
namespace app\modules\messenger\models;

use app\modules\messenger\abstracts\AssociativeInterface;
use yii\base\DynamicModel;

/**
 * Class Associative
 * @package app\modules\messenger\models
 */
class Associative extends DynamicModel implements AssociativeInterface
{
    /**
     * @inheritdoc
     */
    public function getAttribute(string $name, $default = null)
    {
        return isset($this->$name) ? $this->$name : $default;
    }

    /**
     * @param null $names
     * @param array $except
     * @return array
     */
    public function getAttributes($names = null, $except = [])
    {
        if ($names === null) {
            $names = $this->attributes();
        }

        $attributes = [];
        foreach ($names as $key) {
            if ($val = $this->getAttribute($key)) {
                $attributes[$key] = $val;
            }
        }
        foreach ($except as $name) {
            unset($attributes[$name]);
        }

        return $attributes;
    }
}