<?php

namespace app\modules\messenger\models;

use app\components\ModelTrait;
use app\modules\messenger\abstracts\BaseMessageInterface;
use yii\base\Model;

/**
 * Class BaseMessage
 *
 * @property array $attachments
 * @property string $text
 * @property string $serviceType
 * @property array $options
 */
class BaseMessage extends Model implements BaseMessageInterface
{
    use ModelTrait;

    /**
     * @var array
     */
    public $attachments;
    protected $options;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['serviceType', 'text'], 'required'],
            [['text', 'serviceType'], 'string'],
            ['serviceType', 'in', 'range' => [static::SERVICE_TYPE_SKYPE]],
        ];
    }

    /**
     * @return null|string
     */
    public function getText(): ?string
    {
        return $this->text ?? null;
    }

    /**
     * @param string $text
     * @return BaseMessage
     */
    public function setText(string $text): BaseMessage
    {
        $this->text = $text;
        return $this;
    }

    /**
     * @return string
     */
    public function getServiceType(): string
    {
        return $this->serviceType ?? static::SERVICE_TYPE_SKYPE;
    }

    /**
     * @param string $serviceType
     * @return BaseMessage
     */
    public function setServiceType(string $serviceType): BaseMessage
    {
        $this->serviceType = $serviceType;
        return $this;
    }

    /**
     * @param array $options
     * @return BaseMessage
     */
    public function setOptions(array $options): BaseMessage
    {
        $this->options = $options;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getOptions(): ?array
    {
        return $this->options ?? null;
    }
}