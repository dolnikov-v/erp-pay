<?php

namespace app\modules\messenger\models;

use app\modules\messenger\abstracts\AnswerInterface;
use yii\base\Model;

/**
 * Class Answer
 * @package app\modules\messenger\models
 *
 * @property string $status
 * @property string $message
 * @property array $data
 */
class Answer extends Model implements AnswerInterface
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['status', 'required'],
            [['status', 'message'], 'string'],
            ['status', 'default', 'value' => static::STATUS_ERROR],
            ['status', 'in', 'range' => [static::STATUS_SUCCESS, static::STATUS_WARNING, static::STATUS_ERROR]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return ['status', 'message', 'data'];
    }

    /**
     * @var string
     */
    protected $status;
    /**
     * @var string
     */
    protected $message;
    /**
     * @var array
     */
    protected $data;

    public function init()
    {
        $this->status = static::STATUS_ERROR;
        parent::init();
    }

    /**
     * @param string $status
     * @return Answer
     */
    public function setStatus(string $status): Answer
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @param string $message
     * @return Answer
     */
    public function setMessage(string $message): Answer
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @param array $data
     * @return Answer
     */
    public function setData(array $data): Answer
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @return null|string
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data ?? [];
    }

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return !$this->validate() || $this->status == static::STATUS_ERROR ? false : true;
    }
}