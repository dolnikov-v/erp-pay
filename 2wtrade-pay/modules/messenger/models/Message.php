<?php

namespace app\modules\messenger\models;

use app\components\ModelTrait;
use app\modules\messenger\abstracts\AccountInterface;
use app\modules\messenger\abstracts\MessageInterface;
use yii\base\Model;

/**
 * Class Message
 *
 * @property Account $from
 * @property Account $to
 * @property Account $reply
 * @property array $attachments
 * @property string $text
 * @property string $type
 * @property string $serviceType
 * @property array $options
 */
class Message extends Model implements MessageInterface
{
    use ModelTrait;

    /**
     * @var array
     */
    public $attachments;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['serviceType', 'to', 'text'], 'required'],
            [['from', 'to', 'reply'], 'checkAccount'],
            [['text', 'type', 'serviceType'], 'string'],
            ['serviceType', 'in', 'range' => [static::SERVICE_TYPE_SKYPE]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return ['to', 'text', 'from', 'reply'];
    }

    /**
     * @param $attribute
     */
    public function checkAccount($attribute): void
    {
        if (!($this->$attribute instanceof Account)) {
            $this->addError($attribute, \Yii::t('common', 'Недопустимый тип.'));
        } elseif (!$this->$attribute->validate()) {
            $this->addError($attribute, $this->$attribute->getErrors());
        }
    }

    /**
     * @return null|string
     */
    public function getText(): ?string
    {
        return $this->text ?? null;
    }

    /**
     * @param string $text
     * @return Message
     */
    public function setText(string $text): Message
    {
        $this->text = $text;
        return $this;
    }

    /**
     * @return AccountInterface|null
     */
    public function getReply(): ?AccountInterface
    {
        return $this->reply ?? null;
    }

    /**
     * @param Account|array $reply
     * @return Message
     */
    public function setReply($reply): Message
    {
        if (!($reply instanceof Account)) {
            $reply = new Account($reply);
        }
        $this->reply = $reply;
        return $this;
    }

    /**
     * @return AccountInterface|null
     */
    public function getTo(): ?AccountInterface
    {
        return $this->to ?? null;
    }

    /**
     * @param Account|array $to
     * @return Message
     */
    public function setTo($to): Message
    {
        if (!($to instanceof Account)) {
            $to = new Account($to);
        }
        $this->to = $to;
        return $this;
    }

    /**
     * @return AccountInterface|null
     */
    public function getFrom(): ?AccountInterface
    {
        return $this->from ?? null;
    }

    /**
     * @param Account|array $from
     * @return Message
     */
    public function setFrom($from): Message
    {
        if (!($from instanceof Account)) {
            $from = new Account($from);
        }
        $this->from = $from;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getType(): ?string
    {
        return $this->type ?? null;
    }

    /**
     * @param string $type
     * @return Message
     */
    public function setType(string $type): Message
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getServiceType(): string
    {
        return $this->serviceType ?? static::SERVICE_TYPE_SKYPE;
    }

    /**
     * @param string $serviceType
     * @return Message
     */
    public function setServiceType(string $serviceType): Message
    {
        $this->serviceType = $serviceType;
        return $this;
    }

    /**
     * @param array $options
     * @return Message
     */
    public function setOptions(array $options): Message
    {
        $this->options = $options;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getOptions(): ?array
    {
        return $this->options ?? null;
    }

}