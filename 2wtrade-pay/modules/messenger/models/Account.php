<?php

namespace app\modules\messenger\models;

use app\modules\messenger\abstracts\AccountInterface;
use yii\base\Model;

/**
 * Class Account
 * @package app\modules\messenger\models
 *
 * @property string $address
 * @property string $name
 * @property int $userID
 */
class Account extends Model implements AccountInterface
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['address', 'name'], 'string'],
            ['userID', 'integer'],
            ['address', 'required', 'when' => function ($model) {
                return empty($model->userID);
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return ['address', 'name'];
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address): void
    {
        $this->address = (string)$address;
    }

    /**
     * @return int|null
     */
    public function getUserID(): ?int
    {
        return $this->userID;
    }

    /**
     * @param int $userID
     */
    public function setUserID(int $userID): void
    {
        $this->userID = $userID;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param null|string $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }
}