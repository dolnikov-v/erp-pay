<?php

namespace app\modules\messenger\modules\skype\models;

use app\components\ModelTrait;
use app\modules\messenger\abstracts\AccountInterface;
use app\modules\messenger\abstracts\MessageInterface;
use \app\modules\messenger\models\Account as BaseAccount;
use yii\base\Model;

/**
 * Class Message
 *
 * @property string $type
 * @property string $text
 * @property Account $from
 * @property Account $recipient
 * @property Account $conversation
 * @property string $replyToId
 * @property string $serviceUrl
 */
class Message extends Model implements MessageInterface
{
    use ModelTrait;

    const TYPE_MESSAGE = 'message';
    const TYPE_EVENT = 'event';
    const TYPE_UPDATE = 'conversationUpdate';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'text'], 'required'],
            [['type', 'text', 'replyToId', 'serviceUrl'], 'string'],
            ['type', 'in', 'range' => [static::TYPE_MESSAGE, static::TYPE_EVENT, static::TYPE_UPDATE]],
            [['from', 'recipient', 'conversation'], 'checkAccount'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return ['type', 'from', 'conversation', 'recipient', 'text', 'replyToId', 'serviceUrl'];
    }

    /**
     * @param $attribute
     */
    public function checkAccount($attribute): void
    {
        if (!($this->$attribute instanceof Account)) {
            $this->addError($attribute, \Yii::t('common', 'Недопустимый тип'));
        } elseif (!$this->$attribute->validate()) {
            $this->addError($attribute, $this->$attribute->getErrors());
        }
    }

    /**
     * @param string $type
     * @return Message
     */
    public function setType(string $type): self
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getType(): ?string
    {
        return $this->type ?? null;
    }

    /**
     * @param mixed $from
     * @return Message
     */
    public function setFrom($from)
    {
        if (!($from instanceof Account)) {
            $from = new Account($from);
        }
        $this->from = $from;
        return $this;
    }

    /**
     * @return AccountInterface
     */
    public function getFrom(): AccountInterface
    {
        return $this->from ?? new Account();
    }

    /**
     * @param mixed $conversation
     * @return Message
     */
    public function setConversation($conversation)
    {
        if (!($conversation instanceof Account)) {
            $conversation = new Account($conversation);
        }
        $this->conversation = $conversation;
        return $this;
    }

    /**
     * @return AccountInterface|null
     */
    public function getConversation(): ?AccountInterface
    {
        return $this->conversation ?? null;
    }

    /**
     * @param $recipient
     * @return Message
     */
    public function setRecipient($recipient): Message
    {
        if (!($recipient instanceof Account)) {
            $recipient = new Account($recipient);
        }
        $this->recipient = $recipient;
        return $this;
    }

    /**
     * @return AccountInterface|null
     */
    public function getRecipient(): ?AccountInterface
    {
        return $this->recipient ?? null;
    }

    /**
     * @param mixed $text
     * @return Message
     */
    public function setText(string $text): Message
    {
        $this->text = $text;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getText(): ?string
    {
        return $this->text ?? null;
    }

    /**
     * @param string $replyToId
     * @return Message
     */
    public function setReplyToId(string $replyToId): Message
    {
        $this->replyToId = $replyToId;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getReplyToId(): ?string
    {
        return $this->replyToId ?? null;
    }

    /**
     * @inheritdoc
     */
    public function getReply(): ?AccountInterface
    {
        return $this->getReplyToId() ? (new BaseAccount(['address' => $this->getReplyToId()])) : null;
    }

    /**
     * @inheritdoc
     */
    public function getTo(): ?AccountInterface
    {
        $result = null;
        if ($account = $this->getRecipient()) {
            $result = new BaseAccount([
                'address' => $account->getAddress() ?? null,
                'name' => $account->getName() ?? null,
            ]);
        }
        return $result;
    }

    /**
     * @inheritdoc
     */
    public function getServiceType(): string
    {
        return static::SERVICE_TYPE_SKYPE;
    }

    /**
     * @param string $serviceUrl
     * @return Message
     */
    public function setServiceUrl(string $serviceUrl): Message
    {
        $this->serviceUrl = $serviceUrl;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getServiceUrl(): ?string
    {
        return $this->serviceUrl ?? null;
    }
}