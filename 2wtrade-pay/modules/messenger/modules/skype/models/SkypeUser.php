<?php

namespace app\modules\messenger\modules\skype\models;

use app\components\db\ActiveRecordLogUpdateTime;

/**
 * This is the model class for table "skype_user".
 *
 * @property integer $id
 * @property string $skype_id
 * @property string $skype_name
 * @property string $conversation_id
 * @property string $bot_id
 * @property string $bot_name
 * @property string $service_url
 * @property string $answer
 * @property integer $created_at
 * @property integer $updated_at
 */
class SkypeUser extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%skype_user}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'integer'],
            [['skype_id', 'skype_name', 'conversation_id', 'bot_id', 'bot_name', 'answer', 'service_url'], 'string'],
            [['skype_id', 'skype_name', 'conversation_id', 'bot_id'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'skype_id' => 'Skype ID',
            'skype_name' => 'Skype Name',
            'conversation_id' => 'conversation_id',
            'bot_id' => 'bot_id',
            'bot_name' => 'bot_name',
            'service_url' => 'service_url',
            'answer' => 'Answer',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSkypeUserLinks()
    {
        return $this->hasMany(SkypeUserLink::className(), ['skype_user_id' => 'id']);
    }
}