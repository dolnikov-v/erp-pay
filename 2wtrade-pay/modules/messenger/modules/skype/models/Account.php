<?php

namespace app\modules\messenger\modules\skype\models;

use app\modules\messenger\abstracts\AccountInterface;
use yii\base\Model;

/**
 * Class Account
 * @package app\modules\messenger\modules\skype\models
 *
 * @property string $id
 * @property string $name
 */
class Account extends Model implements AccountInterface
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'name'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return ['id', 'name'];
    }

    /**
     * @return null|string
     */
    public function getId(): ?string
    {
        return $this->id ?? null;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = (string)$id;
    }

    /**
     * @inheritdoc
     */
    public function getAddress(): ?string
    {
        return $this->getId();
    }

    /**
     * @return null|string
     */
    public function getName(): ?string
    {
        return $this->name ?? null;
    }

    /**
     * @param null|string $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }
}