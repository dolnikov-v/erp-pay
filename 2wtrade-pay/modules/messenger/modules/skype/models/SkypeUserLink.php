<?php

namespace app\modules\messenger\modules\skype\models;

use app\components\db\ActiveRecord;
use app\models\User;

/**
 * This is the model class for table "skype_user_link".
 *
 * @property integer $user_id
 * @property integer $skype_user_id
 */
class SkypeUserLink extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%skype_user_link}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'skype_user_id'], 'integer'],
            [['user_id', 'skype_user_id'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'Пользователь',
            'skype_user_id' => 'Skype аккаунт',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSkypeUser()
    {
        return $this->hasOne(User::className(), ['id' => 'skype_user_id']);
    }
}