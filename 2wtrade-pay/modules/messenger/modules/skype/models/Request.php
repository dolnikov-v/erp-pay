<?php

namespace app\modules\messenger\modules\skype\models;

/**
 * Class Request
 * @package app\modules\messenger\modules\skype\models
 *
 * @property string $serviceUrl
 * @property string $channelId
 * @property string $timestamp
 * @property string $id
 */
class Request extends Message
{
    const CHANNEL_WEBCHAT = 'webchat';
    const CHANNEL_SKYPE = 'skype';

    public function __construct(array $config = [])
    {
        foreach ($config as $key => $val) {
            try {
                $this->$key;
            } catch (\Throwable $e) {
                unset($config[$key]);
            }
        }
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(
            parent::rules(),
            [
                [['channelId', 'timestamp', 'id'], 'string'],
                ['channelId', 'in', 'range' => [static::CHANNEL_WEBCHAT, static::CHANNEL_SKYPE]],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return array_merge(
            parent::attributes(),
            ['channelId', 'timestamp', 'id']
        );
    }

    /**
     * @return null|string
     */
    public function getChannelId(): ?string
    {
        return $this->channelId ?? null;
    }

    /**
     * @param string $channelId
     */
    public function setChannelId(string $channelId): void
    {
        $this->channelId = $channelId;
    }

    /**
     * @return null|string
     */
    public function getTimestamp(): ?string
    {
        return $this->timestamp ?? null;
    }

    /**
     * @param string $timestamp
     */
    public function setTimestamp(string $timestamp): void
    {
        $this->timestamp = $timestamp;
    }

    /**
     * @return null|string
     */
    public function getId(): ?string
    {
        return $this->id ?? null;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }
}