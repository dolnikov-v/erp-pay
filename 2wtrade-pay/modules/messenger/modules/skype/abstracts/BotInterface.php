<?php

namespace app\modules\messenger\modules\skype\abstracts;

interface BotInterface
{
    /**
     * @return string
     */
    public function getToken(): string;
}