<?php

namespace app\modules\messenger\modules\skype\transport;

use app\modules\messenger\abstracts\BaseMessageInterface;
use app\modules\messenger\abstracts\MessageInterface;
use app\modules\messenger\abstracts\TransportInterface;
use app\modules\messenger\modules\skype\abstracts\BotInterface;
use app\modules\messenger\modules\skype\models\Message;
use Yii;
use yii\base\Component;
use yii\caching\Cache;
use yii\di\Instance;
use yii\redis\Connection;

/**
 * Class SkypeTransport
 * @package app\components\skype\transport
 */
class SkypeTransport extends Component implements TransportInterface, BotInterface
{
    const GRANT_TYPE = 'client_credentials';

    /**
     * @var null|string
     */
    protected static $token;

    /**
     * @var string
     */
    public $tokenUrl = 'https://login.microsoftonline.com/botframework.com/oauth2/v2.0/token';
    /**
     * @var string
     */
    public $botName;
    /**
     * @var string
     */
    public $botID;
    /**
     * для личных и адресованных сообщений - https://smba.trafficmanager.net/apis/, а для просмотра чата без указания адресата - https://webchat.botframework.com/
     * @var string
     */
    public $defaultServiceUrl = 'https://smba.trafficmanager.net/apis/';
    /**
     * @var string
     */
    public $clientID;
    /**
     * @var string
     */
    public $clientSecret;
    /**
     * @var string
     */
    public $scope = 'https://api.botframework.com/.default';
    /**
     * @var string
     */
    public $componentName = 'SkypeBot';

    /**
     * @var yii\caching\Cache
     */
    public $cache = 'cache';

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->cache = Instance::ensure($this->cache, Cache::class);
    }

    /**
     * @inheritdoc
     */
    public function getToken(): string
    {
        return $this->botID;
    }

    /**
     * @return null|string
     * @throws \Exception
     */
    protected function auth(): ?string
    {
        static::$token = $this->cache->get($this->componentName);
        if (static::$token === false) {
            $result = file_get_contents($this->tokenUrl, false, stream_context_create([
                'http' => [
                    'method' => 'POST',
                    'header' => 'Content-type: application/x-www-form-urlencoded',
                    'content' => http_build_query([
                        'client_id' => $this->clientID,
                        'client_secret' => $this->clientSecret,
                        'grant_type' => static::GRANT_TYPE,
                        'scope' => $this->scope,
                    ])
                ]
            ]));
            $tokenAnswer = json_decode($result, TRUE);
            if (!empty($tokenAnswer['access_token'])) {
                static::$token = $tokenAnswer['access_token'];
                $this->cache->set($this->componentName, static::$token, $tokenAnswer['expires_in']);
            } else {
                throw new \Exception(Yii::t('common', 'Не удалось получить токен! Сообщение: {message}', ['message' => $result]));
            }
        }
        return static::$token;
    }

    /**
     * @inheritdoc
     */
    public function send(MessageInterface $message): bool
    {
        if (!is_a($message, Message::class)) {
            throw new \Exception(Yii::t('common', 'Не корректный объект сообщения.'));
        }
        if (empty($this->defaultServiceUrl)) {
            throw new \Exception(Yii::t('common', 'Отсутствует обязательная опция {option} компонента {component}.', ['option' => 'defaultServiceUrl', 'component' => 'SkypeTransport']));
        }
        /**
         * @var Message $message
         */
        $message->setType(Message::TYPE_MESSAGE);
        $message->setFrom(['name' => $this->botName, 'id' => $this->botID]);
        $message->setConversation(['id' => $this->botID]);
        if (!$message->validate()) {
            throw new \Exception($message->getFirstErrorAsString());
        }
        $token = $this->auth();

        $dataString = json_encode($message);

        $url = $this->defaultServiceUrl . 'v3/conversations/' . $message->getRecipient()->getAddress() . '/activities/';
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Content-Type: application/json',
                'Authorization: Bearer ' . $token . '',
                'Content-Length: ' . strlen($dataString)
            ]
        );
        $result = curl_exec($ch);
        $answer = json_decode($result, true);
        if (empty($answer['id'])) {
            throw new \Exception(!empty($answer['error']['massage']) ? $answer['error']['massage'] : $result);
        }
        return true;
    }

    /**
     * @inheritdoc
     */
    public function sendAll(BaseMessageInterface $message): bool
    {
        throw new \Exception(Yii::t('common', 'Метод массовой рассылки не поддерживается API'));
    }
}