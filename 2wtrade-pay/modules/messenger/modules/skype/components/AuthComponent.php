<?php

namespace app\modules\messenger\modules\skype\components;

use app\modules\messenger\abstracts\AccountInterface;
use app\modules\messenger\abstracts\AnswerInterface;
use app\modules\messenger\models\Answer;
use app\modules\messenger\modules\skype\models\Account;
use app\modules\messenger\modules\skype\models\Request;
use app\modules\messenger\modules\skype\models\SkypeUser;
use app\modules\messenger\modules\skype\models\SkypeUserLink;
use yii\base\Component;
use Yii;

/**
 * Class AuthComponent
 * @package app\modules\messenger\modules\skype\components
 */
class AuthComponent extends Component
{
    public $userTable = 'user';

    private $skypeUser;

    /**
     * @param string $skypeID
     * @param string $botID
     * @return bool
     */
    public function isAuth(string $skypeID, string $botID): bool
    {
        if ($user = SkypeUser::find()->innerJoinWith('skypeUserLinks')->where([
            SkypeUser::tableName() . '.skype_id' => $skypeID,
            SkypeUser::tableName() . '.bot_id' => $botID,
        ])->one()) {
            return true;
        }
        return false;
    }

    /**
     * @param string $botID
     * @return array|null
     */
    public function getAllUsers(string $botID): ?array
    {
        $result = [];
        if ($users = SkypeUser::find()->innerJoinWith('skypeUserLinks')->where([
            SkypeUser::tableName() . '.bot_id' => $botID,
        ])->all()) {
            foreach ($users as $user) {
                $result[] = new Account([
                    'id' => $user->skype_id,
                    'name' => $user->skype_name
                ]);
            }
        }
        return $result;
    }

    /**
     * @param string $botID
     * @param int $userID
     * @return AccountInterface|null
     */
    public function getAddress(string $botID, int $userID): ?AccountInterface
    {
        $this->skypeUser = $account = null;
        if ($this->skypeUser = SkypeUser::find()->innerJoinWith('skypeUserLinks')->where([
            SkypeUser::tableName() . '.bot_id' => $botID,
            SkypeUserLink::tableName() . '.user_id' => $userID
        ])->one()) {
            $account = new Account([
                'id' => $this->skypeUser->skype_id,
                'name' => $this->skypeUser->skype_name
            ]);
        }
        return $account;
    }

    /**
     * @param Request $request
     * @param string $email
     * @return AnswerInterface
     * @throws \yii\db\Exception
     */
    public function registration(Request $request, string $email): AnswerInterface
    {
        $answer = new Answer();
        if ($request->getChannelId() !== Request::CHANNEL_SKYPE) {
            $answer->setMessage(Yii::t('common', 'Доступна регистрация только в личных чатах.'));
        } elseif (!$request->getFrom() || !$request->getFrom()->validate()) {
            $answer->setMessage(Yii::t('common', 'Данные аккаунта не корректны.'));
        } else {
            if ($userID = $this->getUserID($email)) {
                if ($this->getAddress($request->getRecipient()->getAddress(), $userID)) {
                    $answer->setStatus(Answer::STATUS_SUCCESS);
                } else {
                    $skypeUser = new SkypeUser([
                        'skype_id' => $request->getFrom()->getAddress(),
                        'skype_name' => $request->getFrom()->getName(),
                        'conversation_id' => $request->getFrom()->getAddress(),
                        'bot_id' => $request->getRecipient()->getAddress(),
                        'bot_name' => $request->getRecipient()->getName(),
                        'service_url' => $request->getServiceUrl(),
                        'answer' => json_encode($request, JSON_UNESCAPED_UNICODE),
                        'service_url' => $request->getServiceUrl(),
                    ]);
                    if (!$skypeUser->save()) {
                        $answer->setMessage($skypeUser->getFirstErrorAsString());
                    } else {
                        $link = new SkypeUserLink([
                            'skype_user_id' => $skypeUser->id,
                            'user_id' => $userID,
                        ]);
                        if ($link->save()) {
                            $answer->setStatus(Answer::STATUS_SUCCESS);
                        } else {
                            $answer->setMessage($link->getFirstErrorAsString());
                        }
                    }
                }
            } else {
                $answer->setMessage(Yii::t('common', 'Пользователь не найден.'));
            }
        }
        return $answer;
    }

    /**
     * @param string $botID
     * @param string $skypeID
     * @return AnswerInterface
     */
    public function logout(string $botID, string $skypeID): AnswerInterface
    {
        $answer = new Answer();
        $user = SkypeUser::find()->innerJoinWith('skypeUserLinks')->where([
            SkypeUser::tableName() . '.bot_id' => $botID,
            SkypeUser::tableName() . '.skype_id' => $skypeID
        ])->one();
        if ($user) {
            SkypeUserLink::deleteAll(['skype_user_id' => $user->id]);
            $answer->setStatus(Answer::STATUS_SUCCESS)->setMessage('Ваша связь с текущим чатом стёрта из всех баз, не осталось ни каких улик.');
        } else {
            $answer->setMessage('Пользователь не найден.');
        }
        return $answer;
    }

    /**
     * @param string $email
     * @return int|null
     * @throws \yii\db\Exception
     */
    public function getUserID(string $email): ?int
    {
        $userID = null;
        if ($user = Yii::$app->getDb()->createCommand("SELECT id FROM {$this->userTable} WHERE email = :email", [':email' => $email])->queryOne()) {
            $userID = $user['id'];
        }
        return $userID;
    }
}