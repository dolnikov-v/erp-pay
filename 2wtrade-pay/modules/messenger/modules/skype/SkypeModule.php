<?php

namespace app\modules\messenger\modules\skype;

use app\models\User;
use app\modules\messenger\abstracts\AnswerInterface;
use app\modules\messenger\abstracts\AssociativeInterface;
use app\modules\messenger\abstracts\BaseMessageInterface;
use app\modules\messenger\abstracts\ConfirmInterface;
use app\modules\messenger\abstracts\MessageInterface;
use app\modules\messenger\abstracts\TransportInterface;
use app\modules\messenger\abstracts\WebhookInterface;
use app\modules\messenger\models\Answer;
use app\modules\messenger\modules\skype\components\AuthComponent;
use app\modules\messenger\modules\skype\models\Message;
use app\modules\messenger\modules\skype\models\Request;
use Yii;
use yii\base\Module;
use yii\di\Instance;
use yii\mail\BaseMailer;
use yii\redis\Connection;
use \app\modules\messenger\modules\skype\transport\SkypeTransport;

/**
 * Class Skype
 * @package app\components\skype
 */
class SkypeModule extends Module implements TransportInterface, WebhookInterface, ConfirmInterface
{
    const REDIS_CHANNEL_REGISTRATION = 'registration';

    /**
     * @var Connection
     */
    public $redis = 'redis';

    /**
     * @var string
     */
    public $redisChannel = 'serviceSkype';

    /**
     * @var SkypeTransport
     */
    public $transport;

    /**
     * @var BaseMailer
     */
    public $mailer = 'mailer';

    /**
     * @var AuthComponent
     */
    public $auth;

    /**
     * @var string
     */
    public $registrationLink;

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->redis = Instance::ensure($this->redis, Connection::class);
        $this->mailer = Instance::ensure($this->mailer, BaseMailer::class);
        $this->transport = $this->get('transport');
        $this->auth = $this->get('auth');
    }

    /**
     * @inheritdoc
     */
    public function send(MessageInterface $message): bool
    {
        if (!$message->getTo()->getAddress()) {
            if ($account = $this->auth->getAddress($this->transport->botID, $message->getTo()->getUserID())) {
                $message->getTo()->setAddress($account->getAddress());
                $message->getTo()->setName($account->getName());
            } else {
                return false;
            }
        }
        $skypeMessage = new Message();
        $skypeMessage->text = $message->getText();
        $skypeMessage->recipient = [
            'id' => $message->getTo()->getAddress(),
            'name' => $message->getTo()->getName(),
        ];
        if ($message->getReply()) {
            $skypeMessage->replyToId = $message->getReply()->getAddress();
        }
        return $this->transport->send($skypeMessage);
    }

    /**
     * @inheritdoc
     */
    public function sendAll(BaseMessageInterface $message): bool
    {
        foreach ($this->auth->getAllUsers($this->transport->botID) as $account) {
            $send = new Message([
                'recipient' => $account,
                'text' => $message->getText(),
            ]);
            $this->transport->send($send);
        }
        return true;
    }

    /**
     * @param AssociativeInterface $requestDynamic
     * @return AnswerInterface
     * @throws \yii\db\Exception
     */
    public function handlerWebhook(AssociativeInterface $requestDynamic): AnswerInterface
    {
        $result = new Answer();

        if (!($botAddress = $requestDynamic->getAttribute('recipient')) || empty($botAddress['id']) || $this->transport->getToken() !== $botAddress['id']) {
            $result->setMessage(Yii::t('common', 'Неизвестный отправитель.'));
        } elseif (($text = $requestDynamic->getAttribute('text')) && is_string($text)) {
            $commands = explode(' ', $text);

            $request = new Request($requestDynamic->getAttributes());
            $isAuth = $this->auth->isAuth($request->getFrom()->getAddress(), $this->transport->botID);

            switch ($command = array_shift($commands)) {
                case 'help':
                    $result->setStatus(Answer::STATUS_SUCCESS)->setMessage(
                        "help - справка\nlogin my@email.com - авторизация\nlogout - отписаться от бота"
                    );
                    break;
                case 'login':
                    if ($isAuth) {
                        $result->setMessage(Yii::t('common', 'Вы уже авторизованы в текущем чате.'));
                    } elseif (preg_match("/[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*\.[a-z]{2,}/i", $text, $matches)) {
                        $login = $matches[0];
                        if ($record = $this->redis->get($this->redisChannel . ':' . static::REDIS_CHANNEL_REGISTRATION . ':' . $login)) {
                            $result->setMessage(Yii::t('common', 'Письмо с подтверждением уже было выслано.'));
                        } elseif ($this->auth->getUserID($login)) {
                            $key = uniqid();
                            $mail = $this->mailer->compose('@app/modules/messenger/views/email/registration', [
                                'key' => $key,
                                'email' => $login,
                                'url' => Yii::$app->params['domain'] . "/messenger/{$this->registrationLink}?service={$this->id}&email={$login}&key={$key}",
                                'service' => $this->id,
                                'botName' => $this->transport->botName
                            ])
                                ->setFrom(Yii::$app->params['noReplyEmail'])
                                ->setTo($login)
                                ->setSubject(Yii::t('common', 'Подтверждение подписки на рассылку Skype'));
                            if ($mail->send()) {
                                $this->redis->set($this->redisChannel . ':' . static::REDIS_CHANNEL_REGISTRATION . ':' . $login, json_encode(
                                    [
                                        'email' => $login,
                                        'key' => $key,
                                        'request' => $request,
                                    ],
                                    JSON_UNESCAPED_UNICODE
                                ), 'NX', 'EX', 60 * 60 * 24 * 7);
                                $result->setStatus(Answer::STATUS_SUCCESS);
                                $result->setMessage(Yii::t('common', 'На Ваш почтовый ящих выслано письмо для подтверждения подписки.'));
                            } else {
                                $result->setMessage(Yii::t('common', 'Не удалось отправить сообщение на почту.'));
                            }
                        } else {
                            $result->setMessage(Yii::t('common', 'Пользователь не найден.'));
                        }
                    } else {
                        $result->setMessage(Yii::t('common', 'Не корректный Формат сообщения. Пример: {format}', ['format' => 'login my_work@email.com']));
                    }
                    break;
                case 'logout':
                    $result = $this->auth->logout($this->transport->botID, $request->getFrom()->getAddress());
                    break;
                default:
                    $result->setStatus(AnswerInterface::STATUS_WARNING);
                    $result->setMessage(Yii::t('common', 'Команда "{command}"отсутствует. Для списка доступных команд наберите "help".', ['command' => $command]));
            }

            if ($result->getMessage()) {
                $message = new Message([
                    'from' => $request->getRecipient(),
                    'recipient' => $request->getFrom(),
                    'text' => (!$result->isSuccess() ? Yii::t('common', 'ОШИБКА!') : '') . ' ' . $result->getMessage()
                ]);

                $this->transport->send($message);
            }
        } else {
            $result->setMessage(Yii::t('common', 'Пустое сообщение.'));
        }

        return $result;
    }

    /**
     * @param AssociativeInterface $request
     * @return AnswerInterface
     * @throws \yii\db\Exception
     */
    public function confirm(AssociativeInterface $request): AnswerInterface
    {
        $result = new Answer();
        if (($email = $request->getAttribute('email')) & ($key = $request->getAttribute('key'))) {
            if ($record = $this->redis->get($this->redisChannel . ':' . static::REDIS_CHANNEL_REGISTRATION . ':' . $email)) {
                $record = json_decode($record);
                if ($record->key == $key) {
                    $result = $this->auth->registration(new Request((array)$record->request), $record->email);
                    if ($result->isSuccess()) {
                        $this->redis->del($this->redisChannel . ':' . static::REDIS_CHANNEL_REGISTRATION . ':' . $email);
                    }
                } else {
                    $result->setMessage(Yii::t('common', 'Неправильная ссылка.'));
                }
            } else {
                $result->setMessage(Yii::t('common', 'Запись об ожидании подтверждения регистрации не найдена. Возможно срок действия ссылки истёк.'));
            }
        } else {
            $result->setMessage(Yii::t('common', 'Неправильная ссылка.'));
        }
        return $result;
    }
}