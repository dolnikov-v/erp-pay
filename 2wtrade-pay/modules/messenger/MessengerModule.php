<?php

namespace app\modules\messenger;

use app\components\base\Module;

use app\modules\auth\components\filters\AccessControl;
use app\modules\messenger\abstracts\AnswerInterface;
use app\modules\messenger\abstracts\AssociativeInterface;
use app\modules\messenger\abstracts\BaseMessageInterface;
use app\modules\messenger\abstracts\ConfirmInterface;
use app\modules\messenger\abstracts\MessageInterface;
use app\modules\messenger\abstracts\TransportInterface;
use app\modules\messenger\abstracts\WebhookInterface;
use app\modules\messenger\models\Answer;
use Yii;

/**
 * Class MessengerModule
 * @package app\modules\marketplace
 */
class MessengerModule extends Module implements TransportInterface, WebhookInterface, ConfirmInterface
{
    // флаг для логирования вебхуков
    const FLAG_LOG_WEBHOOK = 1;
    // флаг для логирования подтверждений
    const FLAG_LOG_CONFIRM = 2;

    /**
     * @var callable
     */
    protected $logger = null;

    /**
     * набор флагов, по окторым будет логироваться. Пример: FLAG_LOG_WEBHOOK | FLAG_LOG_CONFIRM | ...
     * @var bool
     */
    public $logFlags = null;

    /**
     * @return callable
     * @throws \yii\base\InvalidConfigException
     */
    protected function getLogger()
    {
        if (is_null($this->logger)) {
            /** @var Logger $logger */
            $logger = \Yii::$app->get("processingLogger");
            $this->logger = $logger->getLogger([
                'type' => 'action',
                'route' => 'app/modules/messenger',
                'process_id' => getmypid(),
            ]);
        }
        return $this->logger;
    }

    /**
     * @param string $message
     * @param array $tags ['tagName'=>'val', ...]
     *
     * @throws \yii\base\InvalidConfigException
     */
    public function logAdd(string $message, array $tags = [])
    {
        $log = $this->getLogger();
        $log($message, $tags);
    }

    /**
     * @inheritdoc
     */
    public function send(MessageInterface $message): bool
    {
        $module = $this->getModuleForBack($message);
        return $module->send($message);
    }

    /**
     * @inheritdoc
     */
    public function sendAll(BaseMessageInterface $message): bool
    {
        $module = $this->getModuleForBack($message);
        return $module->sendAll($message);
    }

    /**
     * @param BaseMessageInterface $message
     * @return TransportInterface
     * @throws \Exception
     */
    private function getModuleForBack(BaseMessageInterface $message): TransportInterface
    {
        if (!$message->validate()) {
            throw new \Exception($message->getFirstErrorAsString());
        }
        if (($module = $this->getModule($message->getServiceType())) && ($module instanceof TransportInterface)) {
            return $module;
        }
        throw new \Exception(\Yii::t('common', 'Отсутствует модуль для передачи сообщений.'));
    }

    /**
     * @inheritdoc
     */
    public function handlerWebhook(AssociativeInterface $request): AnswerInterface
    {
        $result = new Answer();
        if ($service = $request->getAttribute('service')) {
            if (($module = $this->getModule($service)) && is_a($module, WebhookInterface::class)) {
                /**
                 * @var WebhookInterface $module
                 */
                $result = $module->handlerWebhook($request);
            } else {
                $result->setMessage(Yii::t('common', 'Отсутствует сервис обработки запроса.'));
            }
        } else {
            $result->setMessage(Yii::t('common', 'Не указан сервис обработки запроса.'));
        }
        if ($this->logFlags & static::FLAG_LOG_WEBHOOK) {
            $this->logAdd(json_encode(['request' => $request->getAttributes(), 'answer' => $result->getAttributes()], JSON_UNESCAPED_UNICODE));
        }
        return $result;
    }

    /**
     * @inheritdoc
     */
    public function confirm(AssociativeInterface $request): AnswerInterface
    {
        $result = new Answer();
        if ($service = $request->getAttribute('service')) {
            if (($module = $this->getModule($service)) && is_a($module, WebhookInterface::class)) {
                /**
                 * @var ConfirmInterface $module
                 */
                $result = $module->confirm($request);
            } else {
                $result->setMessage(Yii::t('common', 'Отсутствует сервис обработки запроса.'));
            }
        } else {
            $result->setMessage(Yii::t('common', 'Не указан сервис обработки запроса.'));
        }
        if ($this->logFlags & static::FLAG_LOG_CONFIRM) {
            $this->logAdd(json_encode(['request' => $request->getAttributes(), 'answer' => $result->getAttributes()], JSON_UNESCAPED_UNICODE));
        }
        return $result;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        Yii::$app->user->enableSession = false;
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                    ],
                ],
            ],
        ];
    }
}