<?php

namespace app\modules\messenger\abstracts;

interface TransportInterface
{
    /**
     * @param MessageInterface $message
     * @return bool
     * @throws \Exception
     */
    public function send(MessageInterface $message): bool;

    /**
     * @param BaseMessageInterface $message
     * @return bool
     * @throws \Exception
     */
    public function sendAll(BaseMessageInterface $message):bool;
}