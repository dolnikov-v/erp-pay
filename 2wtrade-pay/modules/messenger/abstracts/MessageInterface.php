<?php

namespace app\modules\messenger\abstracts;

interface MessageInterface extends BaseMessageInterface
{
    /**
     * @return AccountInterface|null
     */
    public function getFrom(): ?AccountInterface;
    /**
     * @return AccountInterface|null
     */
    public function getTo(): ?AccountInterface;
    /**
     * @return AccountInterface|null
     */
    public function getReply(): ?AccountInterface;
}