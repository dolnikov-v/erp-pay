<?php

namespace app\modules\messenger\abstracts;

interface AccountInterface
{
    public function validate($attributeNames = null, $clearErrors = true);

    /**
     * @return string
     */
    public function getAddress();
    /**
     * @return string
     */
    public function getName();
}