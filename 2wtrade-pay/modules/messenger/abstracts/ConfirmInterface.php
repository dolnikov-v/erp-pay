<?php

namespace app\modules\messenger\abstracts;

interface ConfirmInterface
{
    /**
     * @param AssociativeInterface $request
     * @return AnswerInterface
     */
    public function confirm(AssociativeInterface $request): AnswerInterface;
}