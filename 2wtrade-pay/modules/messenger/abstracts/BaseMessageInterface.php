<?php

namespace app\modules\messenger\abstracts;

interface BaseMessageInterface
{
    const SERVICE_TYPE_SKYPE = 'serviceSkype';

    public function validate($attributeNames = null, $clearErrors = true);
    public function getFirstErrorAsString();

    /**
     * @return string
     */
    public function getServiceType(): string;

    /**
     * @return null|string
     */
    public function getText(): ?string;
}