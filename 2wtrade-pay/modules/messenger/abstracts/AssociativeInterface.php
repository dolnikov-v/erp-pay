<?php

namespace app\modules\messenger\abstracts;

/**
 * Interface AssociativeInterface
 * @package app\modules\messenger\abstracts
 */
interface AssociativeInterface
{
    public function getAttributes($names = null, $except = []);

    /**
     * @param string $name
     * @param null $default
     * @return mixed
     */
    public function getAttribute(string $name, $default = null);
}