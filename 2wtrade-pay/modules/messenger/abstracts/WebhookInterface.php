<?php

namespace app\modules\messenger\abstracts;

interface WebhookInterface
{
    /**
     * @param AssociativeInterface $request
     * @return AnswerInterface
     */
    public function handlerWebhook(AssociativeInterface $request): AnswerInterface;
}