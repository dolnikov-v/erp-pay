<?php

namespace app\modules\salary\controllers;

use app\components\web\Controller;
use app\modules\salary\models\Office;
use app\modules\salary\models\OfficeTax;
use Yii;
use yii\helpers\Url;
use yii\web\HttpException;

/**
 * Class OfficeTaxController
 * @package app\modules\salary\controllers
 */
class OfficeTaxController extends Controller
{
    /**
     * @param null $id
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionEdit($id = null)
    {

        $officeId = Yii::$app->request->get('office_id');

        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new OfficeTax();
            if ($officeId) {
                $model->office_id = $officeId;
            }
        }

        $post = Yii::$app->request->post();

        if ($model->load($post)) {
            $isNewRecord = $model->isNewRecord;
            if ($model->save()) {
                Yii::$app->notifier->addNotification($isNewRecord ? Yii::t('common', 'Налоговая ставка успешно добавлена.') : Yii::t('common', 'Налоговая ставка успешно сохранена.'), 'success');
                return $this->redirect(Url::toRoute(['office/edit-tax/', 'id' => $model->office_id]));
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }

        $offices = Office::find()->collection();

        return $this->render('edit', [
            'model' => $model,
            'offices' => $offices,
            'officeId' => $officeId,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws HttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $model = $this->getModel($id);

        $officeId = $model->office_id;
        if ($model->delete()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Налоговая ставка успешно удалена.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute(['office/edit-tax/', 'id' => $officeId]));
    }

    /**
     * @param integer $id
     * @return OfficeTax
     * @throws HttpException
     */
    private function getModel($id)
    {

        $model = OfficeTax::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Налоговая ставка не найдена.'));
        }

        return $model;
    }
}
