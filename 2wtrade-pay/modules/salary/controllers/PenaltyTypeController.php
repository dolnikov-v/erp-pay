<?php
namespace app\modules\salary\controllers;

use app\components\web\Controller;
use app\modules\salary\models\PenaltyType;
use app\models\Currency;
use app\modules\salary\models\search\PenaltyTypeSearch;
use Yii;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

/**
 * Class PenaltyTypeController
 * @package app\modules\salary\controllers
 */
class PenaltyTypeController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new PenaltyTypeSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param null $id
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new PenaltyType();
            $model->loadDefaultValues();
        }

        $post = Yii::$app->request->post();

        if ($model->load($post)) {
            $isNewRecord = $model->isNewRecord;

            $model->active = isset($post['PenaltyType']['active']) ?: 0;

            if ($isNewRecord) {
                $model->created_by = Yii::$app->user->id;
            }
            $model->updated_by = Yii::$app->user->id;

            if ($model->type != PenaltyType::PENALTY_TYPE_PERCENT) {
                $model->percent = null;
            }

            if ($model->type != PenaltyType::PENALTY_TYPE_SUM) {
                $model->sum = null;
            }

            if ($model->save()) {
                Yii::$app->notifier->addNotification($isNewRecord ? Yii::t('common', 'Тип штрафа успешно добавлен.') : Yii::t('common', 'Тип штрафа успешно сохранен.'), 'success');
                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }


        $types = PenaltyType::getTypes();

        $currencies = Currency::find()->collection();

        return $this->render('edit', [
            'model' => $model,
            'types' => $types,
            'currencies' => $currencies,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionActivate($id)
    {
        $model = $this->getModel($id);

        $model->active = 1;

        if ($model->save(true, ['active'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Тип штрафа успешно активирован.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionDeactivate($id)
    {
        $model = $this->getModel($id);

        $model->active = 0;

        if ($model->save(true, ['active'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Тип штрафа успешно деактивирован.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param integer $id
     * @return PenaltyType
     * @throws NotFoundHttpException
     */
    private function getModel($id)
    {
        $model = PenaltyType::findOne($id);

        if (!$model) {
            throw new NotFoundHttpException(Yii::t('common', 'Тип штрафа не найден.'));
        }

        return $model;
    }
}
