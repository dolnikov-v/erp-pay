<?php

namespace app\modules\salary\controllers;

use app\components\web\Controller;
use app\modules\catalog\models\ExternalSource;
use app\modules\catalog\models\ExternalSourceRole;
use app\modules\salary\models\Designation;
use app\modules\salary\models\DesignationExtensionRole;
use app\modules\salary\models\search\DesignationSearch;
use Yii;
use yii\db\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * Class DesignationController
 * @package app\modules\salary\controllers
 */
class DesignationController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new DesignationSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'modelSearch' => $modelSearch,
        ]);
    }

    /**
     * @param null $id
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new Designation();
            $model->loadDefaultValues();
        }

        $post = Yii::$app->request->post();

        if ($model->load($post)) {
            $isNewRecord = $model->isNewRecord;

            $model->fileJobDescription = UploadedFile::getInstance($model, 'fileJobDescription');
            if (Yii::$app->user->can('salary.designation.deletejobdescriptionfile') && $model->job_description_file = $model->upload('fileJobDescription')) {
                $model->fileJobDescription = null;
            } else {
                $model->job_description_file = $isNewRecord ? null : $model->getOldAttribute('job_description_file');
            }

            if ($model->save()) {
                Yii::$app->notifier->addNotification($isNewRecord ? Yii::t('common', 'Должность успешно добавлена.') : Yii::t('common', 'Должность успешно сохранена.'), 'success');
                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }

        $external_source = ExternalSource::getCollectionExternalSources();
        $designationExtensionRole = $model->isNewRecord ? [] : $model->designationExtensionRole;

        return $this->render('edit', [
            'model' => $model,
            'external_source' => is_array($external_source) ? $external_source : [],
            'designationExtensionRole' => ArrayHelper::getColumn($model->designationExtensionRole, 'external_source_role_id')
        ]);
    }

    /**
     * @param null $id
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionView($id)
    {
        $model = $this->getModel($id);

        $external_source = ExternalSource::getCollectionExternalSources();

        return $this->render('view', [
            'model' => $model,
            'external_source' => is_array($external_source) ? $external_source : [],
            'designationExtensionRole' => ArrayHelper::getColumn($model->designationExtensionRole, 'external_source_role_id')
        ]);
    }


    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionActivate($id)
    {
        $model = $this->getModel($id);

        $model->active = 1;

        if ($model->save(true)) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Должность успешно активирована.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionDeactivate($id)
    {
        $model = $this->getModel($id);

        $model->active = 0;

        if ($model->save(true)) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Должность успешно деактивирована.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }


    /**
     * @param null $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $model = $this->getModel($id);

        $file = $model->job_description_file;
        if ($model->delete()) {
            if ($file && is_file(Designation::getPathFile($file))) {
                unlink(Designation::getPathFile($file));
            }
            Yii::$app->notifier->addNotification(Yii::t('common', 'Должность успешно удалена.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param null $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionDeleteJobDescriptionFile($id)
    {
        $model = $this->getModel($id);
        if ($model->job_description_file && is_file(Designation::getPathFile($model->job_description_file))) {
            unlink(Designation::getPathFile($model->job_description_file));
        }
        $model->job_description_file = null;
        if ($model->save()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Должность успешно сохранена.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute(['edit', 'id' => $id]));
    }

    /**
     * @param integer $id
     * @return Designation
     * @throws NotFoundHttpException
     */
    private function getModel($id)
    {
        $model = Designation::find()
            ->joinWith('designationExtensionRole')
            ->where([Designation::tableName() . '.id' => $id])
            ->one();

        if (!$model) {
            throw new NotFoundHttpException(Yii::t('common', 'Должность не найдена.'));
        }

        return $model;
    }

    /**
     * Управление привязкой ролей сторонних сервисов для должности
     * Данные передаются по Ajax - операции доступны только у умеющейся модели должности
     * для новой модели - данные сохраняются в момент создания новой модели должности
     * @return string
     */
    public function actionManageExternalRoles()
    {
        $designation_id = yii::$app->request->post('designation_id');
        $external_source_id = yii::$app->request->post('external_source_id');
        $external_source_role_id = yii::$app->request->post('external_source_role_id');
        $state = yii::$app->request->post('state');

        //добавить привязку
        if ($state == ExternalSourceRole::STATE_ADD) {
            $modelQuery = DesignationExtensionRole::find()->where([
                'designation_id' => $designation_id,
                'external_source_id' => $external_source_id,
                'external_source_role_id' => $external_source_role_id
            ]);
            //если по какой-либо причине у нас такой привязка влруг появилась
            if (!$modelQuery->exists()) {
                $model = new DesignationExtensionRole();
                $model->designation_id = $designation_id;
                $model->external_source_id = $external_source_id;
                $model->external_source_role_id = $external_source_role_id;

                if (!$model->save()) {
                    $result = [
                        'success' => false,
                        'message' => yii::t('common', 'Ошибка : {error}', [
                            'error' => json_encode($model->getErrors(), JSON_UNESCAPED_UNICODE)
                        ])
                    ];
                }
            }
        } else {
            //удалить привязку
            $modelQuery = DesignationExtensionRole::find()->where([
                'designation_id' => $designation_id,
                'external_source_id' => $external_source_id,
                'external_source_role_id' => $external_source_role_id
            ]);

            if ($modelQuery->exists()) {
                $model = $modelQuery->one();

                if (!$model->delete()) {
                    $result = [
                        'success' => false,
                        'message' => yii::t('common', 'Ошибка : {error}', [
                            'error' => json_encode($model->getErrors(), JSON_UNESCAPED_UNICODE)
                        ])
                    ];
                }
            }
        }

        if (!isset($result)) {
            $result = [
                'success' => true,
                'message' => yii::t('common', 'Операция выполнена успешно')
            ];
        }

        return json_encode($result, JSON_UNESCAPED_UNICODE);
    }
}