<?php
namespace app\modules\salary\controllers;

use app\components\filters\AjaxFilter;
use app\components\web\Controller;
use app\models\Country;
use app\modules\callcenter\models\CallCenter;
use app\modules\callcenter\models\CallCenterUser;
use app\modules\salary\models\Designation;
use app\modules\salary\models\MonthlyStatement;
use app\modules\salary\models\MonthlyStatementData;
use app\modules\salary\models\Office;
use app\modules\salary\models\Person;
use app\modules\salary\models\WorkingShift;
use app\modules\salary\models\search\MonthlyStatementDataSearch;
use app\modules\salary\models\search\MonthlyStatementSearch;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

/**
 * Class MonthlyStatementController
 * @package app\modules\salary\controllers
 */
class MonthlyStatementController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => [
                    'delete-persons',
                ],
            ]
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new MonthlyStatementSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param null $id
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new MonthlyStatement();
            $model->loadDefaultValues();
            $model->active = 1;

            $model->date_from = date('Y-m-01');
            $model->date_to = date('Y-m-t');
        }

        $post = Yii::$app->request->post();

        if ($model->load($post)) {
            $isNewRecord = $model->isNewRecord;

            $model->active = isset($post['MonthlyStatement']['active']) ?: 0;
            $model->date_from = $model->date_from ? Yii::$app->formatter->asDate($model->date_from, 'php:Y-m-d') : null;
            $model->date_to = $model->date_to ? Yii::$app->formatter->asDate($model->date_to, 'php:Y-m-d') : null;

            $error = false;
            $transaction = Yii::$app->db->beginTransaction();
            try {
                if (!$model->save()) {
                    Yii::$app->notifier->addNotificationsByModel($model);
                    $error = true;
                } else {
                    if ($isNewRecord) {

                        $persons = $model->selectPersons();

                        ArrayHelper::multisort($persons,
                            ['parent_id', $model->office->calc_salary_local ? 'salary_local' : 'salary_usd', 'active', 'name'],
                            [SORT_ASC, SORT_DESC, SORT_DESC, SORT_ASC]);

                        $model->savePersons($persons);
                    }
                }
            } catch (\yii\web\HttpException $e) {
                $transaction->rollBack();
                Yii::$app->notifier->addNotificationsByModel($model);
                throw $e;
            }

            if (!$error) {
                $transaction->commit();
                Yii::$app->notifier->addNotification($isNewRecord ? Yii::t('common', 'График успешно добавлен.') : Yii::t('common', 'График успешно сохранен.'), 'success');
                return $this->redirect(Url::toRoute('monthly-statement/edit/' . $model->id));
            }
        }

        $renderParams = [
            'model' => $model,
        ];

        if ($id) {

            $inputRequest = Yii::$app->request->queryParams;

            $modelDataSearch = new MonthlyStatementDataSearch();
            $modelDataSearch->statement_id = $id;
            $dataProvider = $modelDataSearch->search($inputRequest);

            $renderParams['modelDataSearch'] = $modelDataSearch;
            $renderParams['dataProvider'] = $dataProvider;

        }

        return $this->render('edit', $renderParams);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionActivate($id)
    {
        $model = $this->getModel($id);

        $model->active = 1;

        if ($model->save(true, ['active'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'График успешно активирован.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionDeactivate($id)
    {
        $model = $this->getModel($id);

        $model->active = 0;

        if ($model->save(true, ['active'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'График успешно деактивирован.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param null $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $model = $this->getModel($id);

        if ($model->delete()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'График удален.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param integer $id
     * @return MonthlyStatement
     * @throws NotFoundHttpException
     */
    private function getModel($id)
    {
        $model = MonthlyStatement::findOne($id);

        if ($model->created_at && !Yii::$app->user->can('salary.monthlystatement.deleteclosed')) {
            throw new NotFoundHttpException(Yii::t('common', 'График закрыт.'));
        }

        if (!$model) {
            throw new NotFoundHttpException(Yii::t('common', 'График не найден.'));
        }

        return $model;
    }


    /**
     * @param integer $id
     * @return MonthlyStatementData
     * @throws NotFoundHttpException
     */
    private function getDataModel($id)
    {
        $model = MonthlyStatementData::findOne($id);

        if (!$model) {
            throw new NotFoundHttpException(Yii::t('common', 'Сотрудник не найден.'));
        }

        return $model;
    }

    /**
     * @throws NotFoundHttpException
     * @return array
     */
    public function actionDeletePersons()
    {

        $response = [
            'status' => 'fail',
            'message' => Yii::t('common', "Ничего не сделали")
        ];

        $id = Yii::$app->request->post('id');
        $statement_id = Yii::$app->request->post('statement_id');

        $model = $this->getModel($statement_id);
        $dataModel = $this->getDataModel($id);

        $success = true;

        if (!$dataModel->delete()) {
            $success = false;
            $response['message'] = Yii::t('common', 'Не удалось удалить сотрудника');
        }

        if ($success) {
            $response['status'] = 'success';
            $response['message'] = Yii::t('common', 'Операция выполнена успешно');
        }

        return $response;
    }
}
