<?php
namespace app\modules\salary\controllers;

use app\components\filters\AjaxFilter;
use app\components\web\Controller;
use app\modules\salary\models\Office;
use app\modules\salary\models\BonusType;
use app\modules\salary\models\Person;
use app\modules\salary\models\Bonus;
use app\modules\salary\models\search\BonusSearch;
use Yii;
use yii\helpers\Url;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;

/**
 * Class BonusController
 * @package app\modules\salary\controllers
 */
class BonusController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => [
                    'get-types',
                    'delete-bonuses',
                ],
            ]
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $inputRequest = Yii::$app->request->queryParams;

        $modelSearch = new BonusSearch();

        $dataProvider = $modelSearch->search($inputRequest);
        $dataProvider->pagination->pageSize = Yii::$app->request->get('per-page');

        return $this->render('index', [
            'modelSearch' => $modelSearch,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * @param null $id
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new Bonus();
            $model->date = date('Y-m-d');
        }

        $post = Yii::$app->request->post();
        if ($model->load($post)) {

            $model->office_id = $post['Bonus']['office_id'];

            $isNewRecord = $model->isNewRecord;

            $saveModels = [];
            if ($isNewRecord) {
                // возможно добавление бонусов сразу нескольким сотрудникам
                foreach ($post['Bonus']['person_id'] as $personId) {
                    $newModel = new Bonus();
                    $newModel->attributes = $model->attributes;
                    $newModel->person_id = $personId;
                    $saveModels[] = $newModel;
                }
            }

            if (!$isNewRecord) {
                $saveModels[] = $model;
            }

            $ok = true;
            foreach ($saveModels as $saveModel) {

                if (!$saveModel->save()) {
                    $ok = false;
                    Yii::$app->notifier->addNotificationsByModel($saveModel);
                }
            }

            if ($ok) {
                Yii::$app->notifier->addNotification($isNewRecord ? Yii::t('common', 'Бонус успешно добавлен.') : Yii::t('common', 'Бонус успешно сохранен.'), 'success');
                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }

        }

        $offices = Office::find()
            ->active()
            ->bySystemUserCountries()
            ->collection();

        /** @var array $offices */
        reset($offices);
        $firstOffice = key($offices);
        $officeId = $firstOffice;
        if (!$model->isNewRecord) {
            $officeId = $model->person->office_id;
        }

        $BonusTypeQuery = BonusType::find();
        $BonusTypeQuery->andWhere([
            'or',
            ['is', 'office_id', null],
            ['=', 'office_id', $officeId]
        ]);
        if ($id) {
            $BonusTypeQuery->activeOrById($model->bonus_type_id);
        } else {
            $BonusTypeQuery->active();
        }
        $bonusTypes = $BonusTypeQuery->all();

        $persons = Person::getCollection($officeId);

        return $this->render('edit', [
            'model' => $model,
            'bonusTypes' => $bonusTypes,
            'persons' => $persons,
            'offices' => $offices,
        ]);
    }

    /**
     * @param null $id
     * @return string
     * @throws HttpException
     */
    public function actionDelete($id)
    {
        $model = $this->getModel($id);

        if ($model->delete()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Бонус успешно удален.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param integer $id
     * @return Bonus
     * @throws HttpException
     */
    private function getModel($id)
    {
        $model = Bonus::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Бонус не найден.'));
        }

        return $model;
    }

    /**
     * @return array|bool
     * @throws NotFoundHttpException
     */
    public function actionGetTypes()
    {
        $officeId = Yii::$app->request->get('office_id');

        $Office = Office::findOne($officeId);

        if ($Office) {
            return BonusType::find()
                ->select([
                    'id',
                    'name',
                    'percent',
                    'sum',
                    'sum_local',
                    'type'
                ])
                ->andWhere([
                    'or',
                    ['is', 'office_id', null],
                    ['=', 'office_id', $officeId]
                ])
                ->active()
                ->asArray()
                ->all();
        } else {
            throw new NotFoundHttpException(Yii::t('common', 'Офис не найден.'));
        }
    }

    /**
     * @throws HttpException
     * @return array
     */
    public function actionDeleteBonuses()
    {

        $response = [
            'status' => 'fail',
            'message' => Yii::t('common', "Ничего не сделали")
        ];

        $id = Yii::$app->request->post('id');

        $model = $this->getModel($id);

        $success = false;
        if ($model) {
            if ($model->delete()) {
                $success = true;
            }
        }

        if ($success) {
            $response['status'] = 'success';
            $response['message'] = Yii::t('common', 'Операция выполнена успешно');
        }

        return $response;
    }
}