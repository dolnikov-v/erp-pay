<?php
namespace app\modules\salary\controllers;

use app\components\web\Controller;
use app\modules\salary\models\Holiday;
use app\modules\salary\models\search\HolidaySearch;
use Yii;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

/**
 * Class HolidayController
 * @package app\modules\salary\controllers
 */
class HolidayController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $inputRequest = Yii::$app->request->queryParams;

        $modelSearch = new HolidaySearch();


        if (!$inputRequest) {
            $inputRequest['HolidaySearch'] = [
                'dateFrom' => Yii::$app->formatter->asDate(date('Y-01-01')),
                'dateTo' => Yii::$app->formatter->asDate(date('Y-12-31')),
            ];
        }

        $dataProvider = $modelSearch->search($inputRequest);
        $dataProvider->pagination->pageSize = yii::$app->request->get('per-page');

        return $this->render('index', [
            'modelSearch' => $modelSearch,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * @param null $id
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new Holiday();
            $model->loadDefaultValues();
            $model->date = date('Y-m-d');
        }

        $post = Yii::$app->request->post();

        if ($model->load($post)) {
            $isNewRecord = $model->isNewRecord;

            $model->date = Yii::$app->formatter->asDate($model->date, 'php:Y-m-d');

            if ($model->save()) {
                Yii::$app->notifier->addNotification($isNewRecord ? Yii::t('common', 'Государственный праздник успешно добавлен.') : Yii::t('common', 'Государственный праздник успешно сохранен.'), 'success');
                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }

        return $this->render('edit', [
            'model' => $model,
        ]);
    }

    /**
     * @param integer $id
     * @return Holiday
     * @throws NotFoundHttpException
     */
    private function getModel($id)
    {
        $model = Holiday::findOne($id);

        if (!$model) {
            throw new NotFoundHttpException(Yii::t('common', 'Государственный праздник не найден.'));
        }

        return $model;
    }

    /**
     * @param null $id
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionDelete($id)
    {
        $model = $this->getModel($id);

        if ($model->delete()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Государственный праздник удален.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

}
