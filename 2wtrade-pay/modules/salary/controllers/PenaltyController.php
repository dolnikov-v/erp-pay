<?php

namespace app\modules\salary\controllers;

use app\components\filters\AjaxFilter;
use app\components\web\Controller;
use app\modules\salary\models\Office;
use app\modules\salary\models\Penalty;
use app\modules\salary\models\PenaltyType;
use app\modules\salary\models\Person;
use app\modules\salary\models\search\PenaltySearch;
use Yii;
use yii\helpers\Url;
use yii\web\HttpException;

/**
 * Class PenaltyController
 * @package app\modules\salary\controllers
 */
class PenaltyController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => [
                    'delete-penalties',
                ],
            ]
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $inputRequest = Yii::$app->request->queryParams;

        $modelSearch = new PenaltySearch();

        $dataProvider = $modelSearch->search($inputRequest);
        $dataProvider->pagination->pageSize = yii::$app->request->get('per-page');

        return $this->render('index', [
            'modelSearch' => $modelSearch,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * @param null $id
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new Penalty();
            $model->date = date('Y-m-d');
        }

        $post = Yii::$app->request->post();
        if ($model->load($post)) {

            $model->office_id = $post['Penalty']['office_id'];

            $isNewRecord = $model->isNewRecord;

            $saveModels = [];
            if ($isNewRecord) {
                // возможно добавление штрафов сразу нескольким сотрудникам
                foreach ($post['Penalty']['person_id'] as $personId) {
                    $newModel = new Penalty();
                    $newModel->attributes = $model->attributes;
                    $newModel->person_id = $personId;
                    $saveModels[] = $newModel;
                }
            }

            if (!$isNewRecord) {
                $saveModels[] = $model;
            }

            $ok = true;
            foreach ($saveModels as $saveModel) {
                if (!$saveModel->save()) {
                    $ok = false;
                    Yii::$app->notifier->addNotificationsByModel($saveModel);
                }
            }

            if ($ok) {
                Yii::$app->notifier->addNotification($isNewRecord ? Yii::t('common', 'Штраф успешно добавлен.') : Yii::t('common', 'Штраф успешно сохранен.'), 'success');
                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }

        }

        $PenaltyTypeQuery = PenaltyType::find();
        if ($id) {
            $PenaltyTypeQuery->activeOrById($model->penalty_type_id);
        } else {
            $PenaltyTypeQuery->active();
        }
        $penaltyTypes = $PenaltyTypeQuery->all();

        $offices = Office::find()
            ->active()
            ->bySystemUserCountries()
            ->collection();

        /** @var array $offices */
        reset($offices);
        $firstOffice = key($offices);
        $officeId = $firstOffice;
        if (!$model->isNewRecord) {
            $officeId = $model->person->office_id;
        }

        $persons = Person::getCollection($officeId);

        return $this->render('edit', [
            'model' => $model,
            'penaltyTypes' => $penaltyTypes,
            'persons' => $persons,
            'offices' => $offices,
        ]);
    }

    /**
     * @param null $id
     * @return string
     * @throws HttpException
     */
    public function actionDelete($id)
    {
        $model = $this->getModel($id);

        if ($model->delete()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Штраф успешно удален.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param integer $id
     * @return Penalty
     * @throws HttpException
     */
    private function getModel($id)
    {
        $model = Penalty::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Штраф не найден.'));
        }

        return $model;
    }

    /**
     * @throws HttpException
     * @return array
     */
    public function actionDeletePenalties()
    {

        $response = [
            'status' => 'fail',
            'message' => Yii::t('common', "Ничего не сделали")
        ];

        $id = Yii::$app->request->post('id');

        $model = $this->getModel($id);

        $success = false;
        if ($model) {
            if ($model->delete()) {
                $success = true;
            }
        }

        if ($success) {
            $response['status'] = 'success';
            $response['message'] = Yii::t('common', 'Операция выполнена успешно');
        }

        return $response;
    }
}
