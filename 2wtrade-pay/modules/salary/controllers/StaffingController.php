<?php
namespace app\modules\salary\controllers;

use app\components\filters\AjaxFilter;
use app\components\web\Controller;
use app\modules\salary\models\BonusType;
use app\modules\salary\models\Designation;
use app\modules\salary\models\Office;
use app\modules\salary\models\Person;
use app\modules\salary\models\search\PersonSearch;
use app\modules\salary\models\search\StaffingSearch;
use app\modules\salary\models\Staffing;
use Yii;
use yii\helpers\Url;
use yii\web\HttpException;

/**
 * Class StaffingController
 * @package app\modules\salary\controllers
 */
class StaffingController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => [
                    'get-job-description',
                    'get-options-by-office',
                ],
            ]
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {

        $officesQuery = Office::find()
            ->bySystemUserCountries()
            ->active();

        $offices = $officesQuery->collection();
        $typesCollection = Office::getTypes();

        $officesCollection = [];
        $types = [];
        foreach ($officesQuery->all() as $office) {
            $officesCollection[$office->type][$office->id] = Yii::t('common', $office->name);
            $types[$office->type] = $office->type;
        }

        foreach ($typesCollection as $type => $name) {
            if (!isset($types[$type])) {
                unset($typesCollection[$type]);
            }
        }

        $inputRequest = Yii::$app->request->queryParams;

        if (!isset($inputRequest['PersonSearch']['activeSearch'])) {
            $inputRequest['PersonSearch']['activeSearch'] = PersonSearch::ACTIVE;
        }
        if (!isset($inputRequest['PersonSearch']['approvedSearch'])) {
            $inputRequest['PersonSearch']['approvedSearch'] = PersonSearch::ACTIVE;
        }

        $modelSearch = new StaffingSearch();
        $dataProvider = $modelSearch->search($inputRequest);
        $dataProvider->pagination->pageSize = $dataProvider->totalCount;

        $alert = '';
        if (!$modelSearch->office_id) {
            $alert = Yii::t('common', 'Выберите офис');
        }


        $tips = [];
        $operators = [];
        $seniorOperators = [];
        $seniorOperatorDesignationId = 0;

        foreach ($dataProvider->allModels as $model) {
            if (!$model['staffing_id']) {
                $tips[$model['office_id']][$model['designation_id']]['staffing_id'] = Yii::t('common', 'Нет должности в норме');
            } else {
                if ($model['jobs'] && $model['office_jobs_number'] && $model['jobs'] > $model['office_jobs_number']) {
                    $tips[$model['office_id']][$model['designation_id']]['staffing_id'] = Yii::t('common', 'Недостаточно рабочих мест ({number}) в офисе для должности',
                        ['number' => $model['jobs'] - $model['office_jobs_number']]);
                }
            }

            if ($model['fact'] > $model['norm']) {
                $tips[$model['office_id']][$model['designation_id']]['fact'] = Yii::t('common', 'Необходимо уменьшение {number} штатных единиц',
                    ['number' => $model['fact'] - $model['norm']]);
            }
            if ($model['fact'] < $model['norm']) {
                $tips[$model['office_id']][$model['designation_id']]['fact'] = Yii::t('common', 'Необходимо увеличение {number} штатных единиц',
                    ['number' => $model['norm'] - $model['fact']]);
            }

            if ($model['calc_work_time']) {
                // операторы, посчитать общее число
                // TODO исходя из потока лидов http://2wtrade-pay.com/in/report/forecast-lead/index (пока не готово)

                $operators[$model['office_id']] = isset($operators[$model['office_id']]) ? +$model['fact'] : $model['fact'];
            }

            if ($model['senior_operator']) {
                // Старший оператор продаж
                $seniorOperators[$model['office_id']]['fact'] = isset($seniorOperators[$model['office_id']]['fact']) ? +$model['fact'] : $model['fact'];
                $seniorOperators[$model['office_id']]['norm'] = isset($seniorOperators[$model['office_id']]['norm']) ? +$model['norm'] : $model['norm'];
                $seniorOperatorDesignationId = $model['designation_id'];
            }

            if ($model['designation_id'] && $model['office_id']) {
                $persons = Person::find()
                    ->active()
                    ->byOfficeId($model['office_id'])
                    ->byDesignationId($model['designation_id'])
                    ->all();

                if ($persons) {
                    foreach ($persons as $person) {

                        if ($model['staffing_id']) {
                            if ($model['calc_salary_local']) {
                                if ($model['salarylocal'] != $person->salary_local) {
                                    $tips[$model['office_id']][$model['designation_id']]['salarylocal'][] =
                                        Yii::t('common', 'Сотрудник {person} имеет оклад {real}, должен быть {plan}', [
                                            'person' => $person->name,
                                            'real' => $person->salary_local,
                                            'plan' => $model['salarylocal'],
                                        ]);
                                }
                            }

                            if ($model['calc_salary_usd']) {
                                if ($model['salaryusd'] != $person->salary_usd) {
                                    $tips[$model['office_id']][$model['designation_id']]['salaryusd'][] =
                                        Yii::t('common', 'Сотрудник {person} имеет оклад {real}, должен быть {plan}', [
                                            'person' => $person->name,
                                            'real' => $person->salary_usd,
                                            'plan' => $model['salaryusd'],
                                        ]);
                                }
                            }
                        }

                        if ($person->activityWarning) {

                            $tips[$model['office_id']][$model['designation_id']]['activity'][] =
                                $person->activityWarning === true ?
                                    Yii::t('common', 'Сотрудник {person} не работал', [
                                        'person' => $person->name
                                    ])
                                    :
                                    Yii::t('common', 'Сотрудник {person} не активен более {days} дней', [
                                        'person' => $person->name,
                                        'days' => $person->activityWarning
                                    ]);
                        }
                    }
                }
            }
        }

        if ($seniorOperatorDesignationId) {
            foreach ($operators as $officeId => $operatorsNumber) {

                // старших операторов в норме
                $normSenior = floor($operatorsNumber / 5);
                $factSenior = $seniorOperators[$officeId]['fact'] ?? 0;

                $delta = $normSenior - $factSenior;

                // если не задали, то у нас 9 старших операторов в норме
                $seniorOperators[$officeId]['norm'] = $seniorOperators[$officeId]['norm'] ?? 9;

                if ($delta > $seniorOperators[$officeId]['norm']) {
                    $delta = $seniorOperators[$officeId]['norm'];
                }

                if ($normSenior > 1 && $delta > 0) {
                    $tips[$officeId][$seniorOperatorDesignationId]['fact'] = Yii::t('common', 'Необходимо увеличение {number} штатных единиц',
                        ['number' => $delta]);
                }
            }
        }

        $sizeOfTips = 0;
        foreach ($tips as $officeId => $officeTips) {
            foreach ($officeTips as $designationId => $designationTips) {
                foreach ($designationTips as $tipKey => $tipText) {
                    if (is_array($tipText)) {
                        $sizeOfTips += sizeof($tipText);
                    } else {
                        $sizeOfTips++;
                    }
                }
            }
        }

        return $this->render('index', [
            'modelSearch' => $modelSearch,
            'dataProvider' => $dataProvider,
            'tips' => $tips,
            'sizeOfTips' => $sizeOfTips,
            'offices' => $offices,
            'officesCollection' => $officesCollection,
            'typesCollection' => $typesCollection,
            'alert' => $alert,
        ]);
    }


    /**
     * @param null $id
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionEdit($id = null)
    {

        $officeId = Yii::$app->request->get('office_id');
        $back = Yii::$app->request->get('back') ?? Yii::$app->request->post('back');

        $staffingBonuses = [];
        if ($id) {
            $model = $this->getModel($id);
            $staffingBonuses = $model->salaryStaffingBonuses;
        } else {
            $model = new Staffing();
            $model->payment_type = Staffing::PAYMENT_TYPE_FIXED;
            if ($officeId) {
                $model->office_id = $officeId;
            }
        }

        $post = Yii::$app->request->post();

        if ($model->load($post)) {
            $isNewRecord = $model->isNewRecord;

            if ($model->save()) {
                $success = true;

                if (!$model->saveStaffingBonusFromPost(Yii::$app->request->post('StaffingBonus'), $staffingBonuses)) {
                    $success = false;
                    Yii::$app->notifier->addNotificationsByModel($model);
                } else {
                    Yii::$app->notifier->addNotification($isNewRecord ? Yii::t('common', 'Должность успешно добавлена.') : Yii::t('common', 'Должность успешно сохранена.'), 'success');
                }

                if ($success) {
                    if ($back == 'staffing') {
                        return $this->redirect(Url::to([
                            'staffing/index',
                            'StaffingSearch' => [
                                'office_id' => $model->office_id
                            ],
                        ]));
                    }
                    if (!$officeId) {
                        $officeId = $model->office_id;
                    }
                    return $this->redirect(Url::toRoute(['office/edit-staffing/', 'id' => $officeId]));
                }
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }

        $offices = Office::find()
            ->bySystemUserCountries()
            ->collection();

        $designations = Designation::find()
            ->where(['active' => 1])
            ->orderBy(['name' => SORT_ASC])
            ->collection();

        $staffings = Staffing::find()
            ->byOfficeId($model->office_id)
            ->andWhere(['<>', Staffing::tableName() . '.id', $model->id])
            ->orderBy(['salary_usd' => SORT_DESC])
            ->collection();

        $bonusTypes = BonusType::find()
            ->byOfficeId($model->office_id)
            ->collection();

        return $this->render('edit', [
            'model' => $model,
            'offices' => $offices,
            'designations' => $designations,
            'staffings' => $staffings,
            'bonusTypes' => $bonusTypes,
            'officeId' => $officeId,
            'back' => $back,
            'staffingBonuses' => $staffingBonuses,

        ]);
    }

    /**
     * @param null $id
     * @return string
     * @throws HttpException
     */
    public function actionDelete($id)
    {
        $back = Yii::$app->request->get('back') ?? Yii::$app->request->post('back');
        $model = $this->getModel($id);

        $officeId = $model->office_id;
        if ($model->delete()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Должность успешно удалена.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }
        if ($back == 'staffing') {
            return $this->redirect(Url::to([
                'staffing/index',
                'StaffingSearch' => [
                    'office_id' => $model->office_id
                ],
                'activeTab' => 1
            ]));
        }
        return $this->redirect(Url::toRoute(['office/edit-staffing/', 'id' => $officeId]));
    }

    /**
     * @param integer $id
     * @return Staffing
     * @throws HttpException
     */
    private function getModel($id)
    {
        $model = Staffing::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Должность не найдена.'));
        }

        return $model;
    }

    /**
     * @return array
     */
    public function actionGetJobDescription()
    {
        $response = [
            'status' => 'fail',
            'message' => Yii::t('common', "Ничего не сделали")
        ];
        $id = Yii::$app->request->get('id');
        if ($id) {
            $designation = Designation::findOne($id);
            if ($designation) {

                $file = '';
                if (Yii::$app->user->can('media.salarydesignation.jobdescription') && $designation->job_description_file != '') {
                    $file = '/media/salary-designation/job-description/' . $designation->job_description_file;
                }

                $response = [
                    'status' => 'success',
                    'file' => $file,
                    'text' => (string)$designation->job_description_text,
                ];
            }
        }
        return $response;
    }

    /**
     * @return array
     */
    public function actionGetOptionsByOffice()
    {
        $response = [
            'status' => 'fail',
            'message' => Yii::t('common', "Ничего не сделали")
        ];
        $officeId = Yii::$app->request->get('office_id');
        $id = Yii::$app->request->get('id');
        if ($id) {
            $office = Office::find()->byId($officeId)->bySystemUserCountries()->one();
            if ($office) {
                $staffings = Staffing::find()
                    ->joinWith('designation')
                    ->byOfficeId($office->id)
                    ->andWhere(['<>', Staffing::tableName() . '.id', $id])
                    ->orderBy([Staffing::tableName() . '.salary_usd' => SORT_DESC])
                    ->all();

                $return = [];
                foreach ($staffings as $staffing) {
                    if ($staffing->designation_id && isset($staffing->designation->name)) {
                        $return[] = [
                            'id' => $staffing->id,
                            'name' => $staffing->designation->name
                        ];
                    }
                }

                $bonusTypes = BonusType::find()
                    ->select(['id', 'name'])
                    ->byOfficeId($office->id)
                    ->all();

                $response['staffings'] = $return;
                $response['bonus_types'] = $bonusTypes;
            }
        }
        return $response;
    }
}
