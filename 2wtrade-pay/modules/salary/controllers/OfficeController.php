<?php

namespace app\modules\salary\controllers;

use app\models\logs\TableLog;
use app\components\web\Controller;
use app\models\Country;
use app\modules\callcenter\models\CallCenter;
use app\modules\salary\models\Office;
use app\modules\salary\models\OfficeExpense;
use app\modules\salary\models\OfficeTax;
use app\modules\salary\models\Staffing;
use app\modules\salary\models\WorkingShift;
use app\modules\salary\models\search\OfficeSearch;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\HttpException;

/**
 * Class OfficeController
 * @package app\modules\salary\controllers
 */
class OfficeController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new OfficeSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * @param int $id
     * @return string
     */
    public function actionLog(int $id)
    {
        $changeLog = TableLog::find()->byTable(Office::clearTableName())->byID($id)->allSorted();
        $tax = TableLog::find()->byTable(OfficeTax::clearTableName())->byLink('office_id', $id)->allSorted();
        $staffing = TableLog::find()->byTable(Staffing::clearTableName())->byLink('office_id', $id)->allSorted();
        $workingShift = TableLog::find()->byTable(WorkingShift::clearTableName())->byLink('office_id', $id)->allSorted();

        return $this->render('log', [
            'changeLog' => $changeLog,
            'tax' => $tax,
            'staffing' => $staffing,
            'workingShift' => $workingShift,
        ]);
    }

    /**
     * @param null $id
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionEditStaffing($id)
    {
        $model = $this->getModel($id);

        $query = Staffing::find()
            ->where(['office_id' => $id])
            ->orderBy(['id' => SORT_ASC]);

        $config = [
            'query' => $query,
        ];

        $renderParams = [
            'model' => $model,
            'staffingDataProvider' => new ActiveDataProvider($config)
        ];

        return $this->render('edit-staffing', $renderParams);
    }

    /**
     * @param null $id
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionEditWorking($id)
    {
        $model = $this->getModel($id);

        $query = WorkingShift::find()
            ->byOfficeId($id)
            ->orderBy(['id' => SORT_ASC]);

        $config = [
            'query' => $query,
        ];

        $renderParams = [
            'model' => $model,
            'workDataProvider' => new ActiveDataProvider($config)
        ];

        return $this->render('edit-working', $renderParams);
    }

    /**
     * @param null $id
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionEditDirection($id)
    {
        $model = $this->getModel($id);
        if ($model->type != Office::TYPE_CALL_CENTER) {
            throw new HttpException(404, Yii::t('common', 'Офис не найден.'));
        }

        $query = CallCenter::find()
            ->byOfficeId($id)
            ->orderBy(['id' => SORT_ASC]);

        $config = [
            'query' => $query,
        ];
        $renderParams = [
            'model' => $model,
            'directionDataProvider' => new ActiveDataProvider($config)
        ];

        return $this->render('edit-direction', $renderParams);
    }

    /**
     * @param null $id
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionEditTax($id)
    {
        $model = $this->getModel($id);

        $query = OfficeTax::find()
            ->where(['office_id' => $id])
            ->orderBy(['id' => SORT_ASC]);

        $config = [
            'query' => $query,
        ];

        $renderParams = [
            'model' => $model,
            'taxDataProvider' => new ActiveDataProvider($config)
        ];

        return $this->render('edit-tax', $renderParams);
    }

    /**
     * @param null $id
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionEditExpenses($id)
    {
        $model = $this->getModel($id);

        if ($model->load(Yii::$app->request->post())) {

            if ($model->save()) {
                Yii::$app->notifier->addNotification(Yii::t('common', 'Офис успешно сохранен.'), 'success');

                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }

        $query = OfficeExpense::find()
            ->where(['office_id' => $id])
            ->orderBy(['id' => SORT_ASC]);

        $config = [
            'query' => $query,
        ];

        $renderParams = [
            'model' => $model,
            'expenseDataProvider' => new ActiveDataProvider($config)
        ];

        return $this->render('edit-expenses', $renderParams);
    }

    /**
     * @param null $id
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionEditGreeting($id)
    {
        $model = $this->getModel($id);

        if ($model->load(Yii::$app->request->post())) {

            if ($model->save()) {
                Yii::$app->notifier->addNotification(Yii::t('common', 'Офис успешно сохранен.'), 'success');

                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }

        $renderParams = [
            'model' => $model,
        ];

        return $this->render('edit-greeting', $renderParams);
    }

    /**
     * @param null $id
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new Office();
            $model->loadDefaultValues();
        }

        $post = Yii::$app->request->post();

        if ($model->load(Yii::$app->request->post())) {
            $isNewRecord = $model->isNewRecord;

            if (isset($post['Office']['work_time'])) {
                $post['Office']['work_time']['from'] = strtotime($post['Office']['work_time']['from']) - strtotime('midnight');
                $post['Office']['work_time']['to'] = strtotime($post['Office']['work_time']['to']) - strtotime('midnight');
                $model->work_time = json_encode($post['Office']['work_time']);
            }

            if ($model->save()) {
                Yii::$app->notifier->addNotification($isNewRecord ? Yii::t('common', 'Офис успешно добавлен.') : Yii::t('common', 'Офис успешно сохранен.'), 'success');

                if ($isNewRecord) {
                    return $this->redirect(Url::toRoute('office/edit/' . $model->id));
                }
                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }

        $countries = Country::find()->active()->orderBy(['name' => SORT_ASC])->collection();

        $renderParams = [
            'model' => $model,
            'countries' => $countries
        ];

        return $this->render('edit', $renderParams);
    }

    /**
     * @param null $id
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionView($id)
    {
        $model = $this->getModel($id);

        $countries = Country::find()->active()->orderBy(['name' => SORT_ASC])->collection();

        $renderParams = [
            'model' => $model,
            'countries' => $countries
        ];

        return $this->render('view', $renderParams);
    }

    /**
     * @param null $id
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionEditSystem($id = null)
    {
        $model = $this->getModel($id);
        if ($model->load(Yii::$app->request->post())) {

            if ($model->save()) {
                Yii::$app->notifier->addNotification(Yii::t('common', 'Офис успешно сохранен.'), 'success');

                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }

        $renderParams = [
            'model' => $model,
        ];

        return $this->render('edit-system', $renderParams);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionActivate($id)
    {
        $model = $this->getModel($id);

        $model->active = 1;

        if ($model->save(true, ['active'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Офис успешно активирован.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionDeactivate($id)
    {
        $model = $this->getModel($id);

        $model->active = 0;

        if ($model->save(true, ['active'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Офис успешно деактивирован.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param integer $id
     * @return Office
     * @throws HttpException
     */
    private function getModel($id)
    {
        $model = Office::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Офис не найден.'));
        }

        return $model;
    }
}
