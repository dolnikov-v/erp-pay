<?php
namespace app\modules\salary\controllers;

use app\components\web\Controller;
use app\modules\salary\models\Office;
use app\modules\salary\models\search\PersonSearch;
use app\modules\salary\models\WorkingShift;
use app\modules\salary\models\WorkTimePlanGenerate;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\HttpException;

/**
 * Class WorkingShiftController
 * @package app\modules\salary\controllers
 */
class WorkingShiftController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {

        $activeTab = Yii::$app->request->get('activeTab', 0);

        $inputRequest = Yii::$app->request->queryParams;

        $offices = Office::find()->bySystemUserCountries()->collection();
        reset($offices);
        $firstOfficeId = key($offices);

        $inputRequest['PersonSearch']['office_id'] = $inputRequest['PersonSearch']['office_id'] ?? $firstOfficeId;
        if (!isset($offices[$inputRequest['PersonSearch']['office_id']])) {
            $inputRequest['PersonSearch']['office_id'] = $firstOfficeId;
        }
        if (!isset($inputRequest['PersonSearch']['activeSearch'])) {
            $inputRequest['PersonSearch']['activeSearch'] = PersonSearch::ACTIVE;
        }
        if (!isset($inputRequest['PersonSearch']['approvedSearch'])) {
            $inputRequest['PersonSearch']['approvedSearch'] = PersonSearch::ACTIVE;
        }

        $personSearch = new PersonSearch();

        $dataPersonProvider = $personSearch->search($inputRequest);
        $dataPersonProvider->pagination->pageSize = yii::$app->request->get('per-page');

        $query = WorkingShift::find()->byOfficeId($inputRequest['PersonSearch']['office_id']);
        $dataWorkingShiftProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $this->render('index', [
            'personSearch' => $personSearch,
            'dataPersonProvider' => $dataPersonProvider,
            'dataWorkingShiftProvider' => $dataWorkingShiftProvider,
            'activeTab' => $activeTab,
            'offices' => $offices
        ]);

    }

    /**
     * @param null $id
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionEdit($id = null)
    {

        $back = Yii::$app->request->get('back') ?? Yii::$app->request->post('back');
        $officeId = Yii::$app->request->get('office_id');

        if ($id) {
            $model = $this->getModel($id);
            $safeOfficeId = $model->office_id;
        } else {
            $model = new WorkingShift();
            if ($officeId) {
                $model->office_id = $officeId;
            }
            $model->working_mon = $model->working_tue = $model->working_wed = $model->working_thu = $model->working_fri = '10:00-19:00';
            $model->lunch_time = '14:00-15:00';
        }

        $post = Yii::$app->request->post();

        if ($model->load($post)) {
            $isNewRecord = $model->isNewRecord;
            if (!$isNewRecord && isset($safeOfficeId)) {
                $model->office_id = $safeOfficeId;
            }

            $model->working_mon = isset($post['WorkingShift']['chworking_mon']) && $post['WorkingShift']['working_mon'] != '' ? $post['WorkingShift']['working_mon'] : '';
            $model->working_tue = isset($post['WorkingShift']['chworking_tue']) && $post['WorkingShift']['working_tue'] != '' ? $post['WorkingShift']['working_tue'] : '';
            $model->working_wed = isset($post['WorkingShift']['chworking_wed']) && $post['WorkingShift']['working_wed'] != '' ? $post['WorkingShift']['working_wed'] : '';
            $model->working_thu = isset($post['WorkingShift']['chworking_thu']) && $post['WorkingShift']['working_thu'] != '' ? $post['WorkingShift']['working_thu'] : '';
            $model->working_fri = isset($post['WorkingShift']['chworking_fri']) && $post['WorkingShift']['working_fri'] != '' ? $post['WorkingShift']['working_fri'] : '';
            $model->working_sat = isset($post['WorkingShift']['chworking_sat']) && $post['WorkingShift']['working_sat'] != '' ? $post['WorkingShift']['working_sat'] : '';
            $model->working_sun = isset($post['WorkingShift']['chworking_sun']) && $post['WorkingShift']['working_sun'] != '' ? $post['WorkingShift']['working_sun'] : '';
            $model->default = isset($post['WorkingShift']['default']) ?: 0;

            if ($model->save()) {
                if (!empty($post['generateDateFrom']) && !empty($post['generateDateTo'])) {
                    $generator = new WorkTimePlanGenerate();
                    $generator->dateFrom = $post['generateDateFrom'];
                    $generator->dateTo = $post['generateDateTo'];
                    $generator->working_shift_id = $model->id;
                    if ($generator->validate()) {
                        $generator->generate();
                    }
                }
                Yii::$app->notifier->addNotification($isNewRecord ? Yii::t('common', 'Рабочая смена успешно добавлена.') : Yii::t('common', 'Рабочая смена успешно сохранена.'), 'success');
                if ($back == 'working-shift') {
                    return $this->redirect(Url::to([
                        'working-shift/index',
                        'PersonSearch' => [
                            'office_id' => $model->office_id
                        ],
                        'activeTab' => 1
                    ]));
                }
                return $this->redirect(Url::toRoute(['office/edit-working/', 'id' => $model->office_id]));
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }

        $offices = Office::find()->collection();

        return $this->render('edit', [
            'model' => $model,
            'offices' => $offices,
            'officeId' => $officeId,
            'back' => $back,
        ]);
    }

    /**
     * @param null $id
     *
     * @return string
     * @throws HttpException
     * @throws \Exception
     * @throws \yii\db\StaleObjectException
     */
    public
    function actionDelete($id)
    {
        $back = Yii::$app->request->get('back') ?? Yii::$app->request->post('back');
        $model = $this->getModel($id);

        $officeId = $model->office_id;
        if ($model->delete()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Рабочая смена успешно удалена.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }
        if ($back == 'working-shift') {
            return $this->redirect(Url::to([
                'working-shift/index',
                'PersonSearch' => [
                    'office_id' => $officeId
                ],
                'activeTab' => 1
            ]));
        }
        return $this->redirect(Url::toRoute(['office/edit-working/', 'id' => $officeId]));
    }

    /**
     * @param integer $id
     * @return WorkingShift
     * @throws HttpException
     */
    private
    function getModel($id)
    {
        $model = WorkingShift::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Рабочая смена не найдена.'));
        }

        return $model;
    }
}
