<?php

namespace app\modules\salary\controllers;

use app\components\web\Controller;
use app\modules\salary\models\Office;
use app\modules\salary\models\OfficeExpense;
use Yii;
use yii\helpers\Url;
use yii\web\HttpException;
use yii\web\UploadedFile;

/**
 * Class OfficeExpenseController
 * @package app\modules\salary\controllers
 */
class OfficeExpenseController extends Controller
{
    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws HttpException
     * @throws \yii\base\Exception
     */
    public function actionEdit($id = null)
    {

        $officeId = Yii::$app->request->get('office_id');

        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new OfficeExpense();
            if ($officeId) {
                $model->office_id = $officeId;
            }
        }

        $post = Yii::$app->request->post();

        if ($model->load($post)) {

            $model->date_from = $model->date_from ? Yii::$app->formatter->asDate($model->date_from, 'php:Y-m-d') : null;
            $model->date_to = $model->date_to ? Yii::$app->formatter->asDate($model->date_to, 'php:Y-m-d') : null;

            $isNewRecord = $model->isNewRecord;

            $model->file = UploadedFile::getInstance($model, 'file');

            if ($model->filename = $model->upload('file')) {
                $model->file = null;
            } else {
                $model->filename = $isNewRecord ? null : $model->getOldAttribute('filename');
            }

            if ($model->save()) {
                Yii::$app->notifier->addNotification($isNewRecord ? Yii::t('common', 'Расход успешно добавлен.') : Yii::t('common', 'Расход успешно сохранен.'), 'success');
                return $this->redirect(Url::toRoute(['office/edit-expenses/', 'id' => $model->office_id]));
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }

        $offices = Office::find()->collection();

        return $this->render('edit', [
            'model' => $model,
            'offices' => $offices,
            'officeId' => $officeId,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws HttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $model = $this->getModel($id);

        if (is_file(OfficeExpense::getPathFile($model->filename))) {
            unlink(OfficeExpense::getPathFile($model->filename));
        }

        $officeId = $model->office_id;
        if ($model->delete()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Расход успешно удален.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute(['office/edit-expenses/', 'id' => $officeId]));
    }


    /**
     * @param null $id
     * @return string
     * @throws HttpException
     */
    public function actionDeleteFile($id)
    {
        $model = $this->getModel($id);
        if (is_file(OfficeExpense::getPathFile($model->filename))) {
            unlink(OfficeExpense::getPathFile($model->filename));
        }
        $model->filename = null;
        if ($model->save()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Расход успешно сохранен.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }
        return $this->redirect(Url::toRoute('office-expense/edit/' . $id));
    }

    /**
     * @param integer $id
     * @return OfficeExpense
     * @throws HttpException
     */
    private
    function getModel($id)
    {

        $model = OfficeExpense::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Расход не найден.'));
        }

        return $model;
    }
}
