<?php
namespace app\modules\salary\controllers;

use app\components\web\Controller;
use app\modules\salary\models\BonusType;
use app\models\Currency;
use app\modules\salary\models\search\BonusTypeSearch;
use Yii;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

/**
 * Class BonusTypeController
 * @package app\modules\salary\controllers
 */
class BonusTypeController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $inputRequest = Yii::$app->request->queryParams;

        $modelSearch = new BonusTypeSearch();

        $dataProvider = $modelSearch->search($inputRequest);
        $dataProvider->pagination->pageSize = yii::$app->request->get('per-page');

        return $this->render('index', [
            'modelSearch' => $modelSearch,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * @param null $id
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new BonusType();
            $model->loadDefaultValues();
        }

        $post = Yii::$app->request->post();

        if ($model->load($post)) {
            $isNewRecord = $model->isNewRecord;

            $model->active = isset($post['BonusType']['active']) ?: 0;

            if ($isNewRecord) {
                $model->created_by = Yii::$app->user->id;
            }
            $model->updated_by = Yii::$app->user->id;

            if (
                $model->type != BonusType::BONUS_TYPE_PERCENT &&
                $model->type != BonusType::BONUS_TYPE_PERCENT_WORK_HOURS &&
                $model->type != BonusType::BONUS_TYPE_PERCENT_CALC_SALARY
            ) {
                $model->percent = null;
            }

            if ($model->type != BonusType::BONUS_TYPE_SUM && $model->type != BonusType::BONUS_TYPE_SUM_WORK_HOURS) {
                $model->sum = null;
                $model->sum_local = null;
            }

            if ($model->save()) {
                Yii::$app->notifier->addNotification($isNewRecord ? Yii::t('common', 'Тип бонуса успешно добавлен.') : Yii::t('common', 'Тип бонуса успешно сохранен.'), 'success');
                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }


        $types = BonusType::getTypes();

        $currencies = Currency::find()->collection();

        return $this->render('edit', [
            'model' => $model,
            'types' => $types,
            'currencies' => $currencies,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionActivate($id)
    {
        $model = $this->getModel($id);

        $model->active = 1;

        if ($model->save(true, ['active'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Тип бонуса успешно активирован.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionDeactivate($id)
    {
        $model = $this->getModel($id);

        $model->active = 0;

        if ($model->save(true, ['active'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Тип бонуса успешно деактивирован.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param integer $id
     * @return BonusType
     * @throws NotFoundHttpException
     */
    private function getModel($id)
    {
        $model = BonusType::findOne($id);

        if (!$model) {
            throw new NotFoundHttpException(Yii::t('common', 'Тип бонуса не найден.'));
        }

        return $model;
    }
}
