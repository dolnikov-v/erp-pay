<?php

namespace app\modules\salary\controllers;

use app\models\Country;
use app\modules\salary\models\Designation;
use app\modules\salary\models\Office;
use app\modules\salary\models\Person;
use app\modules\salary\models\PersonImportData;
use app\modules\salary\models\WorkingShift;
use Yii;
use PHPExcel_IOFactory;
use app\components\web\Controller;
use app\modules\salary\models\PersonImport;
use app\modules\salary\models\search\PersonImportSearch;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

class PersonImportController extends Controller
{
    /**
     * @param PersonImport $model
     * @return string|bool
     */
    protected function saveModel($model)
    {
        $isNewRecord = $model->isNewRecord;

        $error = false;
        $model->upload();
        $transaction = Yii::$app->db->beginTransaction();
        if ($model->save()) {
            $result = $this->loadDataFromFile($model);
            if ($result['status'] == 'success' && $data = $result['data']) {

                if ($model->cell_position) {
                    $map = [];
                    $positions = $model->getPositionMap();
                    foreach ($data as $item) {
                        if ($key = $item['cell_position']) {
                            $map[$key] = ($positions && isset($positions[$key])) ? $positions[$key] : null;
                        }
                    }
                    ksort($map);
                    $model->setPositionMap($map);
                }

                if ($model->cell_country) {
                    $countryList = Country::find()
                        ->select(['id', 'name_en' => 'LOWER(name_en)'])
                        ->active()
                        ->asArray()
                        ->indexBy('name_en')
                        ->all();

                    $countries = $model->getCountryMap();
                    $map = [];
                    foreach ($data as $item) {
                        if ($key = $item['cell_country']) {
                            $lower = strtolower($key);
                            $map[$key] = ($countries && isset($countries[$key])) ? $countries[$key] : ((isset($countryList[$lower])) ? $countryList[$lower] : null);
                        }
                    }
                    ksort($map);
                    $model->setCountryMap($map);
                }

                if ($model->save()) {

                    foreach ($data as $key => $row) {

                        if ($model->cell_login && strlen($row['cell_login']) <= 3) {
                            $row['cell_login'] = '';
                        }
                        if ($model->cell_id && $row['cell_id']) {
                            $row['cell_id'] = intval($row['cell_id']);
                        } elseif ($model->cell_login && $row['cell_login'] &&
                            preg_match("/\d+$/", $row['cell_login'], $matches) && $matches
                        ) {
                            $row['cell_id'] = $matches[0];
                        }

                        $params = [
                            'row_number' => $key,
                            'import_id' => $model->id,
                        ];

                        $record = null;
                        if (!$isNewRecord && $record = PersonImportData::findOne($params)) {
                            $existRows[] = $key;
                        }
                        if (!$record) {
                            $record = new PersonImportData($params);
                        }
                        foreach ($model->getColumnMapRef() as $prop => $ref) {
                            $value = ArrayHelper::getValue($row, $prop);
                            $record->$ref = ($model->$prop && strlen($value) > 0) ? strval($value) : null;
                        }
                        if (!$record->user_id && $model->cell_login && !$model->cell_id) {
                            $value = ArrayHelper::getValue($row, 'cell_id');
                            $record->user_id = (strlen($value) > 0) ? strval($value) : null;
                        }
                        $record->head_id = null;

                        if (!$record->save()) {
                            $error = $record->getFirstErrors();
                            break;
                        }
                    }

                    if (!$isNewRecord && !empty($existRows)) {
                        PersonImportData::deleteAll(['and',
                            ['import_id' => $model->id],
                            ['not', ['row_number' => $existRows]],
                        ]);
                    }

                } else {
                    $error = $model->getFirstErrors();
                }
            } else {
                $error = Yii::t('common', 'Не удалось имортировать файл или в файле нет данных');
            }
        } else {
            $error = $model->getFirstErrors();
        }

        if ($error !== false) {
            $transaction->rollBack();
        } else {
            $transaction->commit();
        }

        if (is_array($error)) {
            $error = implode(',', $error);
        }

        return $error ?: true;
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionIndex()
    {
        $model = new PersonImport();

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());
            $model->status = PersonImport::STATUS_PENDING;
            $result = $this->saveModel($model);
            if ($result === true) {
                Yii::$app->notifier->addNotification(Yii::t('common', 'Файл импорта успешно загружен.'), 'success');
                return $this->redirect(['process', 'id' => $model->id]);
            } else {
                Yii::$app->notifier->addNotification(Yii::t('common', $result), 'danger');
            }
        } else {
            $model->row_begin = 1;
            $model->cell_login = 3;
            $model->cell_full_name = 2;
            $model->cell_country = 1;
            $model->cell_position = 4;
            $model->cell_salary = 6;
            $model->cell_salary_usd = 5;
        }

        $searchForm = new PersonImportSearch();
        $dataProvider = $searchForm->search(Yii::$app->request->get());

        return $this->render('index', [
            'model' => $model,
            'history' => [
                'searchForm' => $searchForm,
                'dataProvider' => $dataProvider,
            ]
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionDownloadImportFile($id)
    {
        $file = $this->findModel($id)->file_name;

        if (file_exists($file) && is_file($file)) {
            Yii::$app->response->sendFile($file);
            Yii::$app->end();
        } else {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Не удалось найти файл'), 'danger');
        }
        return $this->redirect(['process', 'id' => $id]);
    }

    /**
     * @param int $id
     * @return string
     */
    public function actionProcess($id)
    {
        $model = $this->findModel($id);

        if ($post = Yii::$app->request->post()) {
            if ($model->isImported()) {
                Yii::$app->notifier->addNotification(Yii::t('common', 'Файл уже импортирован'), 'success');
                return $this->redirect(['process', 'id' => $model->id, '#' => 'tab1']);
            }

            if (isset($post['changeImport'])) {

                $model->load($post);
                $model->status = PersonImport::STATUS_PENDING;
                $result = $this->saveModel($model);
                if ($result === true) {
                    return $this->refresh();
                } else {
                    Yii::$app->notifier->addNotification(Yii::t('common', $result), 'danger');
                }

            } else {

                $positions = ArrayHelper::getValue($post, 'positions');
                if ($map = $model->getPositionMap()) {
                    foreach ($map as $key => &$value) {
                        if (isset($positions[$key])) {
                            $value = $positions[$key];
                        }
                    }
                    unset($value);
                    $model->setPositionMap($map);
                }

                $countries = ArrayHelper::getValue($post, 'countries');
                if ($map = $model->getCountryMap()) {
                    foreach ($map as $key => &$value) {
                        if (isset($countries[$key])) {
                            $value = $countries[$key];
                        }
                    }
                    unset($value);
                    $model->setCountryMap($map);
                }

                if ($model->load($post) && $model->save()) {
                    return $this->refresh();
                }
                Yii::$app->notifier->addNotification(Yii::t('common', $model->getFirstErrorAsString()), 'danger');
            }
        }

        $officeCollection = Office::find()->orderBy(['name' => SORT_ASC])->collection();
        $positionCollection = Designation::find()->orderBy(['name' => SORT_ASC])->collection();
        $countryCollection = Country::find()->active()->orderBy(['name' => SORT_ASC])->collection();

        $models = PersonImportData::find()
            ->where(['import_id' => $model->id])
            ->indexBy('id')
            ->all();

        foreach ($models as $importData) {
            $importData->setScenarioForCheckImport();
            $importData->validate();
        }

        $dataProvider = new ArrayDataProvider(['models' => $models]);

        $fileTeamLeadList = [];
        $officeTeamLeadList = [];
        if ($model->cell_head) {
            foreach ($models as $dataModel) {
                if ($dataModel->isTeamLead()) {
                    $fileTeamLeadList[$dataModel->id] = $dataModel->person_full_name;
                }
            }

            if ($model->office_id) {
                $teamLeads = Person::find()
                    ->active()
                    ->isTeamLead()
                    ->byOfficeId($model->office_id)
                    ->asArray()
                    ->all();

                $officeTeamLeadList = ArrayHelper::map($teamLeads, 'id', 'name');
            }

            foreach ($models as $dataModel) {
                if ($dataModel->isTeamLead()) {
                    $fileTealLeadList[$dataModel->id] = $dataModel->person_full_name;
                }
            }
        }

        $workingShiftList = ($model->office_id) ? WorkingShift::find()
            ->byOfficeId($model->office_id)
            ->isNotDefault()
            ->collection() : [];

        return $this->render('process', [
            'model' => $model,
            'dataProvider' => $dataProvider,
            'positionCollection' => $positionCollection,
            'officeCollection' => $officeCollection,
            'countryCollection' => $countryCollection,
            'fileTeamLeadList' => $fileTeamLeadList,
            'officeTeamLeadList' => $officeTeamLeadList,
            'workingShiftList' => $workingShiftList,
        ]);
    }

    public function actionImport($id)
    {
        $model = $this->findModel($id);

        if ($model->isImported()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Файл уже импортирован'), 'success');
            return $this->redirect(['process', 'id' => $model->id, '#' => 'tab1']);
        }

        $records = PersonImportData::find()
            ->where([
                'status' => PersonImportData::STATUS_PENDING,
                'import_id' => $model->id,
            ])
            ->indexBy('id')
            ->all();

        $error = false;
        foreach ($records as $record) {
            $record->setScenarioForCheckImport();
            if (!$record->validate()) {
                $error = $record->getFirstErrors();
                break;
            }
        }

        if (!$error) {

            $transaction = Yii::$app->db->beginTransaction();
            foreach ($records as $record) {

                $params = [
                    'office_id' => $model->office_id,
                    'name' => $record->person_full_name,
                ];
                $person = Person::findOne($params) ?: new Person($params);

                if ($record->getPositionId()) {
                    $person->designation_id = $record->getPositionId();
                }
                if ($record->person_salary) {
                    $person->salary_local = $record->person_salary;
                }
                if ($record->person_salary_usd) {
                    $person->salary_usd = $record->person_salary_usd;
                }
                if ($record->person_salary_hourly) {
                    $person->salary_hour_local = $record->person_salary_hourly;
                }
                if ($record->person_salary_hourly_usd) {
                    $person->salary_hour_usd = $record->person_salary_hourly_usd;
                }

                // если у сотрудника не указана ЗП в месяц, а указана ЗП в час, то выставим ему схему начисления зп почасовую
                if (!$person->salary_local && !$person->salary_usd && ($person->salary_hour_usd || $person->salary_hour_local)) {
                    $person->salary_scheme = Person::SALARY_SCHEME_HOURLY;
                }

                if ($record->person_passport) {
                    $person->passport_id_number = $record->person_passport;
                }
                if ($record->person_email) {
                    $person->corporate_email = $record->person_email;
                }
                if ($record->person_skype) {
                    $person->skype = $record->person_skype;
                }
                if ($record->person_phone) {
                    $person->phone = $record->person_phone;
                }
                if ($record->person_termination_date) {
                    $person->active = 0;
                } elseif ($person->isNewRecord) {
                    $person->active = 1;
                }
                if ($record->person_employment_date) {
                    $person->start_date = $record->person_employment_date;
                }
                if ($record->person_termination_date) {
                    $person->dismissal_date = $record->person_termination_date;
                }
                if ($record->working_shift_id) {
                    $person->working_shift_id = $record->working_shift_id;
                }
                if ($record->person_account_number) {
                    $person->account_number = $record->person_account_number;
                }
                if ($person->save()) {
                    $record->person = $person;
                    $user = $record->getUser();
                    if ($user && $user->person_id != $person->id) {
                        $user->person_id = $person->id;
                        if (!$user->save()) {
                            $error = $user->getFirstErrors();
                        }
                    }
                } else {
                    $error = $person->getFirstErrors();
                }

                if (!$error) {
                    $record->status = $record::STATUS_IMPORT;
                    if (!$record->save()) {
                        $error = $record->getFirstErrors();
                    }
                }
                if ($error) {
                    break;
                }
            }

            if (!$error) {
                foreach ($records as $record) {
                    if ($record->isTeamLead() && $record->person->parent_id) {
                        $record->person->parent_id = null;
                        if (!$record->person->save()) {
                            $error = $record->person->getFirstErrors();
                            break;
                        }
                    } elseif ($record->person_head || $record->head_id) {
                        $personalId = $record->head_id;
                        if (!$personalId) {
                            if ($teamLead = $record->getTeamLead()) {
                                if ($person = $teamLead->getPerson()) {
                                    $personalId = $person->id;
                                }
                            } elseif ($person = $record->getOfficeTeamLead()) {
                                $personalId = $person->id;
                            }
                        }
                        if ($personalId) {
                            $record->person->parent_id = $personalId;
                            if (!$record->person->save()) {
                                $error = $record->person->getFirstErrors();
                                break;
                            }
                        } else {
                            $error[] = Yii::t('common', '{row_number}: Не удалось найти руководителя', ['row_number' => $record->row_number]);
                            break;
                        }
                    }
                }
            }

            if (!$error) {
                $model->status = $model::STATUS_IMPORTED;
                if (!$model->save()) {
                    $error = $model->getFirstErrors();
                }
            }
            if (!$error) {
                $transaction->commit();
            } else {
                $transaction->rollBack();
            }
        }

        if (!$error) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Импорт успешно завершен'), 'success');
        } else {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Неудалось импортировать записи. Ошибки: {errors}', ['errors' => ($error && is_array($error)) ? join('<br>', $error) : '']), 'danger');
        }

        return $this->redirect(['process', 'id' => $id, '#' => 'tab1']);
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     */
    public function actionRecordEdit($id)
    {
        $model = $this->findRecordModel($id);

        if ($model->import->isImported()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Файл уже импортирован'), 'success');
            return $this->redirect(['process', 'id' => $model->import_id, '#' => 'tab1']);
        }

        if ($post = Yii::$app->request->post()) {
            if ($model->load($post) && $model->save()) {
                Yii::$app->notifier->addNotification(Yii::t('common', 'Запись успешно сохранена.'), 'success');
                return $this->redirect(['process', 'id' => $model->import_id, '#' => 'tab1']);
            }
            Yii::$app->notifier->addNotification(Yii::t('common', implode(',', $model->getFirstErrors())), 'danger');
        }

        $models = PersonImportData::find()
            ->where(['import_id' => $model->import->id])
            ->indexBy('id')
            ->all();

        $list = [];
        $designations = Designation::find()
            ->where(['<>', 'name', 'Оператор'])
            ->indexBy('id')
            ->all();

        foreach ($model->import->getPositionMap() as $position => $id) {
            if (isset($designations[$id])) {
                $list[] = $position;
            }
        }
        $headList = [];
        if ($list) {
            foreach ($models as $dataModel) {
                if (in_array($dataModel->person_position, $list)) {
                    $headList[$dataModel->id] = $dataModel->person_full_name;
                }
            }
        }

        $workingShiftList = ($model->import->office_id) ? WorkingShift::find()
            ->byOfficeId($model->import->office_id)
            ->isNotDefault()
            ->collection() : [];

        return $this->render('record-edit', [
            'model' => $model,
            'headList' => $headList,
            'workingShiftList' => $workingShiftList,
        ]);
    }

    /**
     * @param $id
     * @param $type
     * @return \yii\web\Response
     */
    public function actionRecordGroupApprovedHead($id, $type)
    {
        $model = $this->findModel($id);
        if ($model->isImported()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Файл уже импортирован'), 'success');
            return $this->redirect(['process', 'id' => $model->id]);
        }

        $head = Yii::$app->request->get('head');
        $rows = Yii::$app->request->get('rows');
        $records = PersonImportData::findAll([
            'id' => $rows,
            'import_id' => $model->id,
        ]);

        $params = null;
        $value = null;
        if ($type == 'file') {
            $params = 'head';
            if ($head && $record = PersonImportData::findOne(['id' => $head])) {
                $value = $record->person_full_name;
            }
        } elseif ($type == 'office') {
            $params = 'head_id';
            if ($head && $person = Person::findOne(['id' => $head])) {
                $value = $person->id;
            }
        }

        if ($params) {
            $transaction = Yii::$app->db->beginTransaction();

            $error = false;
            foreach ($records as $record) {
                try {
                    $record->$params = $value;
                    $error = !$record->save();
                } catch (\Exception $e) {
                    $error = true;
                }

                if ($error) {
                    break;
                }
            }

            if ($error) {
                $transaction->rollBack();
            } else {
                $transaction->commit();
            }

        } else {
            $error = true;
        }

        if ($error) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Не удалось выполнить действие.'), 'danger');
        } else {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Действие успешно выполнено.'), 'success');
        }

        return $this->redirect(['process', 'id' => $model->id, '#' => 'tab1']);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionRecordGroupSetWorkingShift($id)
    {
        $model = $this->findModel($id);
        if ($model->isImported()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Файл уже импортирован'), 'success');
            return $this->redirect(['process', 'id' => $model->id]);
        }

        $workingShift = Yii::$app->request->get('workingShift');
        $rows = Yii::$app->request->get('rows');
        $records = PersonImportData::findAll([
            'id' => $rows,
            'import_id' => $model->id,
        ]);

        $transaction = Yii::$app->db->beginTransaction();

        $error = false;
        foreach ($records as $record) {
            try {
                $record->working_shift_id = $workingShift;
                $error = !$record->save();
            } catch (\Exception $e) {
                $error = true;
            }

            if ($error) {
                break;
            }
        }

        if ($error) {
            $transaction->rollBack();
        } else {
            $transaction->commit();
        }

        if ($error) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Не удалось выполнить действие.'), 'danger');
        } else {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Действие успешно выполнено.'), 'success');
        }

        return $this->redirect(['process', 'id' => $model->id, '#' => 'tab1']);
    }

    /**
     * @param $id
     * @return string
     */
    public function actionRecordExclude($id)
    {
        $model = $this->findRecordModel($id);
        if ($model->import->isImported()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Файл уже импортирован'), 'success');
            return $this->redirect(['process', 'id' => $model->id, '#' => 'tab1']);
        }
        $model->status = $model::STATUS_IGNORE;
        if ($model->save()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Запись успешно исключена из импорта.'), 'success');
        } else {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Не удалось исключить запись из импорта.'), 'success');
        }

        return $this->redirect(['process', 'id' => $model->import_id, '#' => 'tab1']);
    }

    /**
     * @param $id
     * @return string
     */
    public function actionRecordGroupExclude($id)
    {
        $model = $this->findModel($id);
        if ($model->isImported()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Файл уже импортирован'), 'success');
            return $this->redirect(['process', 'id' => $model->id]);
        }
        $rows = Yii::$app->request->get('rows');
        $records = PersonImportData::findAll([
            'id' => $rows,
            'import_id' => $model->id,
        ]);

        $transaction = Yii::$app->db->beginTransaction();

        $error = false;
        foreach ($records as $record) {
            try {
                $record->status = PersonImportData::STATUS_IGNORE;
                $error = !$record->save();
            } catch (\Exception $e) {
                $error = true;
            }

            if ($error) {
                break;
            }
        }

        if ($error) {
            $transaction->rollBack();
            Yii::$app->notifier->addNotification(Yii::t('common', 'Не удалось выполнить действие.'), 'danger');
        } else {
            $transaction->commit();
            Yii::$app->notifier->addNotification(Yii::t('common', 'Действие успешно выполнено.'), 'success');
        }

        return $this->redirect(['process', 'id' => $model->id]);
    }

    /**
     * @param $id
     * @return string
     */
    public function actionRecordGroupInclude($id)
    {
        $model = $this->findModel($id);
        if ($model->isImported()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Файл уже импортирован'), 'success');
            return $this->redirect(['process', 'id' => $model->id]);
        }
        $rows = Yii::$app->request->get('rows');
        $records = PersonImportData::findAll(['id' => $rows]);

        $transaction = Yii::$app->db->beginTransaction();

        $error = false;
        foreach ($records as $record) {
            try {
                $record->status = PersonImportData::STATUS_PENDING;
                $error = !$record->save();
            } catch (\Exception $e) {
                $error = true;
            }

            if ($error) {
                break;
            }
        }

        if ($error) {
            $transaction->rollBack();
            Yii::$app->notifier->addNotification(Yii::t('common', 'Не удалось выполнить действие.'), 'danger');
        } else {
            $transaction->commit();
            Yii::$app->notifier->addNotification(Yii::t('common', 'Действие успешно выполнено.'), 'success');
        }

        return $this->redirect(['process', 'id' => $model->id]);
    }

    /**
     * @param $id
     * @return string
     */
    public function actionRecordInclude($id)
    {
        $model = $this->findRecordModel($id);

        if ($model->import->isImported()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Файл уже импортирован'), 'success');
            return $this->redirect(['process', 'id' => $model->id]);
        }

        $model->status = $model::STATUS_PENDING;
        if ($model->save()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Запись успешно включена в импорт.'), 'success');
        } else {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Не удалось включить запись в импорт.'), 'success');
        }

        return $this->redirect(['process', 'id' => $model->import_id]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if ($model->isImported()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Файл уже импортирован'), 'success');
            return $this->redirect(['index']);
        }

        if ($model->delete()) {
            if (file_exists($model->file_name) && is_file($model->file_name)) {
                unlink($model->file_name);
            }
            Yii::$app->notifier->addNotification(Yii::t('common', 'Файл импорта успешно удален'), 'success');
        } else {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Не удалось включить'), 'success');
        }

        return $this->redirect(['index']);
    }

    /**
     * @param PersonImport $model
     * @return array
     */
    private function loadDataFromFile($model)
    {
        try {
            $rowIdx = 0;
            $loadStr = $model->row_begin;
            $objReader = PHPExcel_IOFactory::createReaderForFile($model->file_name);
//            $objReader->setReadDataOnly(true);

            $objPHPExcel = $objReader->load($model->file_name);
            /* считываем только с 1-го листа */
            $objPHPExcel->setActiveSheetIndex(0);
            $aSheet = $objPHPExcel->getActiveSheet();

            $data = [];
            /* читаем строки из файла в $data */
            foreach ($aSheet->getRowIterator($loadStr) as $worksheetRow) {
                $rowIdx = $worksheetRow->getRowIndex();
                $error = null;

                $row = [];
                foreach ($model->getColumnFields() as $field) {
                    if ($model->$field) {
                        $colIdx = $model->$field - 1;
                        $cell = $aSheet->getCellByColumnAndRow($colIdx, $rowIdx);
                        if ($cell instanceof \PHPExcel_Cell) {
                            $value = $cell->getValue();
                            if ($value && \PHPExcel_Shared_Date::isDateTime($cell)) {
                                $value = date('Y-m-d', \PHPExcel_Shared_Date::ExcelToPHP($value));
                            }
                            if (is_string($value)) {
                                $value = trim($value);
                            }
                        } else {
                            $value = null;
                        }
                        $row[$field] = $value;
                    }
                }

                if (join('', $row) != '') {
                    $data[$rowIdx] = $row;
                    $loadStr++;
                }
            }
            unset($objPHPExcel, $objReader);

            $response = [
                'status' => 'success',
                'data' => $data,
            ];
        } catch (\Exception $e) {
            $response = [
                'status' => 'fail',
                'message' => '(' . Yii::t('common', 'Строка') . ': ' . $rowIdx . ') ' . $e->getMessage(),
            ];
        }

        return $response;
    }

    /**
     * @param $id
     * @return PersonImportData
     * @throws NotFoundHttpException
     */
    protected function findRecordModel($id)
    {
        $model = PersonImportData::findOne($id);

        if (!$model) {
            throw new NotFoundHttpException(Yii::t('common', 'Запись из файла не найдена.'), 404);
        }

        return $model;
    }

    /**
     * @param $id
     * @return PersonImport
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        $model = PersonImport::findOne($id);

        if (!$model) {
            throw new NotFoundHttpException(Yii::t('common', 'Запись не  найдена.'), 404);
        }

        return $model;
    }
}