<?php
namespace app\modules\salary\controllers;

use app\components\web\Controller;
use app\modules\salary\models\MonthlyStatement;
use app\modules\salary\models\MonthlyStatementData;
use app\modules\salary\models\Person;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

/**
 * Class MonthlyStatementDataController
 * @package app\modules\salary\controllers
 */
class MonthlyStatementDataController extends Controller
{
    /**
     * @param null $id
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionEdit($id = null)
    {
        $post = Yii::$app->request->post();

        $statementId = $post['MonthlyStatementData']['statement_id'] ?? Yii::$app->request->get('statement_id');

        $parent = $this::getParent($statementId);

        $persons = [];
        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new MonthlyStatementData();
            if ($statementId) {
                $model->statement_id = $statementId;
            }

            $persons = Person::find()
                ->leftJoin(
                    MonthlyStatementData::tableName(),
                    Person::tableName() . '.id=' . MonthlyStatementData::tableName() . '.person_id and ' . MonthlyStatementData::tableName() . '.statement_id=' . $parent->id
                )
                ->byOfficeId($parent->office_id)
                ->andWhere([
                    'or',
                    ['<=', Person::tableName() . '.start_date', date("Y-m-d", strtotime($parent->date_to))],
                    ['is', Person::tableName() . '.start_date', null]
                ])
                ->andWhere([
                    'or',
                    ['>=', 'DATE_FORMAT(' . Person::tableName() . '.dismissal_date, "%Y-%m")', date("Y-m", strtotime($parent->date_to))],
                    ['is', Person::tableName() . '.dismissal_date', null]
                ])
                ->andWhere([
                    'is', MonthlyStatementData::tableName() . '.person_id', null
                ])
                ->orderBy([
                    Person::tableName() . '.name' => SORT_ASC,
                ])
                ->collection();
        }


        if ($model->load($post)) {
            $isNewRecord = $model->isNewRecord;

            $class = 'MonthlyStatementData';

            if ($isNewRecord) {
                // возможно добавление сразу нескольких
                if (isset($post[$class]['person_id'])) {
                    try {
                        $parent->savePersons($parent->selectPersons($post[$class]['person_id']));
                        Yii::$app->notifier->addNotification(Yii::t('common', 'Сотрудник добавлен в график.'), 'success');
                        $this->redirect(Url::toRoute('monthly-statement/edit/' . $parent->id));
                    } catch (\yii\web\HttpException $e) {
                        throw $e;
                    }
                }
            } else {

                $model->active = isset($post[$class]['active']) ? 1 : 0;
                $model->designation_team_lead = isset($post[$class]['designation_team_lead']) ? 1 : 0;
                $model->designation_calc_work_time = isset($post[$class]['designation_calc_work_time']) ? 1 : 0;

                $i = 1;
                while ($i <= 7) {
                    $property = 'working_' . $i;
                    $model->$property = isset($post[$class]['ch' . $property]) && $post[$class][$property] != '' ? $post[$class][$property] : '';
                    $i++;
                }

                $model->start_date = $model->start_date ? Yii::$app->formatter->asDate($model->start_date, 'php:Y-m-d') : null;
                $model->dismissal_date = $model->dismissal_date ? Yii::$app->formatter->asDate($model->dismissal_date, 'php:Y-m-d') : null;

                if ($model->save()) {
                    Yii::$app->notifier->addNotification(Yii::t('common', 'Сотрудник сохранен в графике.'), 'success');
                    return $this->redirect(Url::toRoute('monthly-statement/edit/' . $parent->id));
                } else {
                    Yii::$app->notifier->addNotificationsByModel($model);
                }
            }

        }
        return $this->render('edit', [
            'model' => $model,
            'statementId' => $statementId,
            'parent' => $parent,
            'persons' => $persons,
        ]);
    }

    /**
     * @param null $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionDelete($id)
    {

        $statementId = Yii::$app->request->get('statement_id');

        $parent = $this::getParent($statementId);

        $model = $this->getModel($id);

        if ($model->delete()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Сотрудник удален из графика.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('monthly-statement/edit/' . $parent->id));
    }

    /**
     * @param integer $id
     * @return MonthlyStatement
     * @throws NotFoundHttpException
     */
    private function getParent($id)
    {
        $model = MonthlyStatement::findOne($id);

        if (!$model) {
            throw new NotFoundHttpException(Yii::t('common', 'График не найден.'));
        }

        return $model;
    }

    /**
     * @param integer $id
     * @return MonthlyStatementData
     * @throws NotFoundHttpException
     */
    private function getModel($id)
    {
        $model = MonthlyStatementData::findOne($id);

        if (!$model) {
            throw new NotFoundHttpException(Yii::t('common', 'Сотрудник не найден.'));
        }

        return $model;
    }
}
