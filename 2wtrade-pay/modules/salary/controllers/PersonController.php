<?php

namespace app\modules\salary\controllers;

use app\components\filters\AjaxFilter;
use app\components\web\Controller;
use app\helpers\Utils;
use app\models\logs\TableLog;
use app\modules\callcenter\models\CallCenter;
use app\modules\callcenter\models\CallCenterUser;
use app\modules\callcenter\models\search\CallCenterUserSearch;
use app\modules\salary\models\Bonus;
use app\modules\salary\models\BonusType;
use app\modules\salary\models\Designation;
use app\modules\salary\models\MonthlyStatementData;
use app\modules\salary\models\Office;
use app\modules\salary\models\Penalty;
use app\modules\salary\models\PenaltyType;
use app\modules\salary\models\Person;
use app\modules\salary\models\PersonLanguage;
use app\modules\salary\models\PersonLink;
use app\modules\salary\models\PersonLog;
use app\modules\salary\models\WorkingShift;
use app\modules\salary\models\search\PersonSearch;
use app\modules\salary\models\WorkTimePlanGenerate;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * Class PersonController
 * @package app\modules\salary\controllers
 */
class PersonController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => [
                    'get-parents',
                    'get-persons',
                    'get-shifts',
                    'change-team-leader',
                    'set-role',
                    'set-work',
                    'activate-persons',
                    'deactivate-persons',
                    'approve-persons',
                    'notapprove-persons',
                    'set-bonus',
                    'set-penalty',
                    'get-person-by-order-id',
                ],
            ]
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {

        $inputRequest = Yii::$app->request->queryParams;

        $modelSearch = new PersonSearch();

        $dataProvider = $modelSearch->search($inputRequest);
        $dataProvider->pagination->pageSize = yii::$app->request->get('per-page');

        return $this->render('index', [
            'modelSearch' => $modelSearch,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * @param null $id
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionEdit($id = null)
    {

        $personLanguageIds = [];
        if ($id) {
            $model = $this->getModel($id);
            $personLanguageIds = ArrayHelper::getColumn($model->languages, 'language_id');
        } else {
            $model = new Person();
            $model->loadDefaultValues();
        }

        $callCenterUserSearch = new CallCenterUserSearch();
        $personLink = new PersonLink();

        $post = Yii::$app->request->post();

        if ($model->load($post)) {
            $isNewRecord = $model->isNewRecord;

            $personLanguageIds = $post['Person']['languages'] ?? [];

            $model->birth_date = $model->birth_date ? Yii::$app->formatter->asDate($model->birth_date, 'php:Y-m-d') : null;
            $model->start_date = $model->start_date ? Yii::$app->formatter->asDate($model->start_date, 'php:Y-m-d') : null;
            $model->dismissal_date = $model->dismissal_date ? Yii::$app->formatter->asDate($model->dismissal_date, 'php:Y-m-d') : null;

            if ($model->dismissal_date) {
                $model->active = 0;
            }

            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            if (Yii::$app->user->can('salary.person.edit.photo') && $model->photo = $model->upload('imageFile')) {
                $model->imageFile = null;
            } else {
                $model->photo = $isNewRecord ? null : $model->getOldAttribute('photo');
            }

            $model->imageFileAgreement = UploadedFile::getInstance($model, 'imageFileAgreement');
            if (Yii::$app->user->can('salary.person.edit.photoagreement') && $model->photo_agreement = $model->upload('imageFileAgreement')) {
                $model->imageFileAgreement = null;
            } else {
                $model->photo_agreement = $isNewRecord ? null : $model->getOldAttribute('photo_agreement');
            }

            if ($model->save()) {

                $savedLanguageIds = ArrayHelper::getColumn($model->languages, 'language_id');
                $savedLanguageIds = array_combine($savedLanguageIds, $savedLanguageIds);

                foreach ($personLanguageIds as $language) {
                    if (isset($savedLanguageIds[$language])) {
                        //прилетело как было в Post
                        unset($savedLanguageIds[$language]);
                    } else {
                        // новая строчка
                        $new = new PersonLanguage();
                        $new->person_id = $model->id;
                        $new->language_id = $language;
                        $new->save();
                    }
                }
                //удаляем те, которые не прилетели в Post
                if ($savedLanguageIds) {
                    PersonLanguage::deleteAll([
                        'person_id' => $model->id,
                        'language_id' => $savedLanguageIds
                    ]);
                }

                Yii::$app->notifier->addNotification($isNewRecord ? Yii::t('common', 'Сотрудник успешно добавлен.') : Yii::t('common', 'Сотрудник успешно сохранен.'), 'success');
                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }

        if ($callCenterUserSearch->load($post)) {
            $error = false;
            if (isset($post['CallCenterUserSearch']['user_login'])) {
                foreach ($post['CallCenterUserSearch']['user_login'] as $userId) {
                    $callCenterUser = CallCenterUser::findOne($userId);
                    if ($callCenterUser && !$callCenterUser->person_id) {
                        $callCenterUser->person_id = $id;
                        if (!$callCenterUser->save()) {
                            Yii::$app->notifier->addNotificationsByModel($callCenterUser);
                            $error = true;
                        }
                    }
                }
            }
            if (!$error) {
                return $this->redirect(Url::toRoute('person/edit/' . $id));
            }
        }

        if ($personLink->load($post)) {
            if (!$personLink->save()) {
                Yii::$app->notifier->addNotificationsByModel($personLink);
            } else {
                return $this->redirect(Url::toRoute('person/edit/' . $id));
            }
        }

        $salarySchemes = Person::getSalarySchemes(true);
        $parentUsers = Person::getPatentsCollection($model->office_id);

        $renderParams = [
            'model' => $model,
            'salarySchemes' => $salarySchemes,
            'parentUsers' => $parentUsers,
        ];

        $renderParams['personLanguageIds'] = $personLanguageIds;
        if ($id) {

            $callCenters = CallCenter::find()
                ->byOfficeId($model->office_id)
                ->collection(true);

            asort($callCenters);

            /** @var array $callCenters */
            reset($callCenters);
            $firstCallCenter = key($callCenters);


            // для привязки к сотруднику у которого уже есть логины в новом КЦ, логин должен совпадать
            $onlyOneLoginInNewCallCenter = CallCenterUser::find()
                ->select('user_login')
                ->joinWith('callCenter')
                ->byPersonId($id)
                ->andWhere([CallCenter::tableName() . '.is_amazon_query' => 1])
                ->limit(1)
                ->scalar();

            $callCenterUsersQuery = CallCenterUser::find()
                ->byCallCenterId($firstCallCenter)
                ->byFreePersonId();

            if ($onlyOneLoginInNewCallCenter) {
                $callCenterUsersQuery->andWhere(['user_login' => $onlyOneLoginInNewCallCenter]);
            }

            $callCenterUsers = $callCenterUsersQuery->collection();

            $renderParams['userDataProvider'] = new ActiveDataProvider([
                'query' => CallCenterUser::find()
                    ->byPersonId($id)
                    ->orderBy(['id' => SORT_ASC]),
            ]);
            $renderParams['linkDataProvider'] = new ActiveDataProvider([
                'query' => PersonLink::find()
                    ->byPersonId($id)
                    ->orderBy(['id' => SORT_ASC]),
            ]);
            $renderParams['callCenterUserSearch'] = $callCenterUserSearch;
            $renderParams['callCenters'] = $callCenters;
            $renderParams['callCenterUsers'] = $callCenterUsers;
            $renderParams['personLink'] = $personLink;


            $linkTypes = PersonLink::getTypeCollection();

            foreach ($model->personLinks as $link) {
                unset($linkTypes[$link->type]);
            }

            $renderParams['linkTypes'] = $linkTypes;
        }

        return $this->render('edit', $renderParams);
    }

    /**
     * @param null $id
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionView($id)
    {

        $model = $this->getModel($id);

        if (!Office::find()->byId($model->office_id)->bySystemUserCountries()->exists()) {
            throw new HttpException(404, Yii::t('common', 'Офис сотрудника не доступен.'));
        }

        $personLanguageIds = ArrayHelper::getColumn($model->languages, 'language_id');
        $salarySchemes = Person::getSalarySchemes(true);
        $parentUsers = Person::getPatentsCollection($model->office_id);

        $renderParams = [
            'model' => $model,
            'salarySchemes' => $salarySchemes,
            'parentUsers' => $parentUsers,
            'personLanguageIds' => $personLanguageIds
        ];

        $renderParams['userDataProvider'] = new ActiveDataProvider([
            'query' => CallCenterUser::find()
                ->byPersonId($id)
                ->orderBy(['id' => SORT_ASC]),
        ]);
        $renderParams['linkDataProvider'] = new ActiveDataProvider([
            'query' => PersonLink::find()
                ->byPersonId($id)
                ->orderBy(['id' => SORT_ASC]),
        ]);

        return $this->render('view', $renderParams);
    }

    /**
     * @param null $id
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionDetails($id)
    {

        $model = $this->getModel($id);

        if (!Office::find()->byId($model->office_id)->bySystemUserCountries()->exists()) {
            throw new HttpException(404, Yii::t('common', 'Офис сотрудника не доступен.'));
        }

        $monthlyStatementDataProvider = new ActiveDataProvider([
            'query' => MonthlyStatementData::find()
                ->byPerson($model->id)
                ->withFile()
                ->orderBy(['id' => SORT_DESC]),
        ]);

        return $this->render('details', [
            'model' => $model,
            'monthlyStatementDataProvider' => $monthlyStatementDataProvider,
        ]);
    }

    /**
     * @param null $id
     * @return string
     * @throws HttpException
     */
    public function actionDelete($id)
    {
        $model = $this->getModel($id);

        if (is_file(Person::getPathFile($model->photo))) {
            unlink(Person::getPathFile($model->photo));
        }

        if (is_file(Person::getPathFile($model->photo_agreement))) {
            unlink(Person::getPathFile($model->photo_agreement));
        }

        if ($model->delete()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Сотрудник успешно удален.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param null $id
     * @return string
     * @throws HttpException
     */
    public function actionDeletePhoto($id)
    {
        if (Yii::$app->user->can('salary.person.edit.photo')) {

            $model = $this->getModel($id);
            if (is_file(Person::getPathFile($model->photo))) {
                unlink(Person::getPathFile($model->photo));
            }
            $model->photo = null;
            if ($model->save()) {
                Yii::$app->notifier->addNotification(Yii::t('common', 'Сотрудник успешно сохранен.'), 'success');
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }
        return $this->redirect(Url::toRoute('person/edit/' . $id));
    }

    /**
     * @param null $id
     * @return string
     * @throws HttpException
     */
    public function actionDeletePhotoAgreement($id)
    {
        if (Yii::$app->user->can('salary.person.edit.photoagreement')) {

            $model = $this->getModel($id);
            if (is_file(Person::getPathFile($model->photo_agreement))) {
                unlink(Person::getPathFile($model->photo_agreement));
            }
            $model->photo_agreement = null;
            if ($model->save()) {
                Yii::$app->notifier->addNotification(Yii::t('common', 'Сотрудник успешно сохранен.'), 'success');
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }

        return $this->redirect(Url::toRoute('person/edit/' . $id));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionActivate($id)
    {
        $model = $this->getModel($id);

        $model->active = 1;

        if ($model->save(true, ['active'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Сотрудник успешно активирован.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionDeactivate($id)
    {
        $model = $this->getModel($id);

        $attributes = ['active', 'dismissal_date', 'dismissal_comment'];

        $post = Yii::$app->request->post();

        $model->dismissal_date = isset($post['person_dismissal_date']) ? Yii::$app->formatter->asDate($post['person_dismissal_date'], 'php:Y-m-d') : date('Y-m-d');
        $model->dismissal_comment = trim($post['person_dismissal_comment'] ?? '');

        if ($model->dismissal_comment == '') {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Введите причину увольнения'));
        } else {
            $model->imageFileDismissal = UploadedFile::getInstanceByName('person_dismissal_file');
            if ($model->imageFileDismissal instanceof UploadedFile) {
                if ($model->dismissal_file = $model->upload('imageFileDismissal')) {
                    $attributes[] = 'dismissal_file';
                }
            }
            $model->active = 0;

            $transaction = Yii::$app->db->beginTransaction();
            try {
                if ($model->save(true, $attributes)) {
                    $model->blockCallCenterUsers();
                    $res = $model->makeDismissalBonus();
                    if (!empty($res['errors'])) {
                        $transaction->rollBack();
                        Yii::$app->notifier->addNotification(implode(' ', $res['errors']));
                    } else {
                        $transaction->commit();
                        Yii::$app->notifier->addNotification(Yii::t('common', 'Сотрудник успешно уволен.'), 'success');
                        return $this->redirect(Url::toRoute('index'));
                    }
                } else {
                    $transaction->rollBack();
                    Yii::$app->notifier->addNotificationsByModel($model);
                }
            } catch (\Exception $e) {
                $transaction->rollBack();
                Yii::$app->notifier->addNotification($e->getMessage(), 'danger');
            }
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionApprove($id)
    {
        $model = $this->getModel($id);

        $model->approved = 1;

        if ($model->save(true, ['approved'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Сотрудник успешно подтвержден.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionNotapprove($id)
    {
        $model = $this->getModel($id);

        $model->approved = 0;

        if ($model->save(true, ['approved'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Сотруднику успешно убрано подтверждение.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param null $id
     * @param null $user
     * @return string
     * @throws HttpException
     */
    public function actionUnlink($id, $user)
    {
        $model = CallCenterUser::find()->where(['id' => $user])->byPersonId($id)->one();

        if ($model) {
            $model->person_id = null;
            if ($model->save()) {
                Yii::$app->notifier->addNotification(Yii::t('common', 'Пользователь успешно отвязан.'), 'success');
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }
        return $this->redirect(Url::toRoute('person/edit/' . $id));
    }

    /**
     * @param null $id
     * @return string
     * @throws HttpException
     */
    public function actionDeleteLink($id)
    {
        $model = PersonLink::findOne($id);
        if ($model instanceof PersonLink) {
            $person = $this->getModel($model->person_id);
            if ($person instanceof Person) {
                if ($model->delete()) {
                    Yii::$app->notifier->addNotification(Yii::t('common', 'Пользователь успешно отвязан.'), 'success');
                } else {
                    Yii::$app->notifier->addNotificationsByModel($model);
                }
                return $this->redirect(Url::toRoute('person/edit/' . $person->id));
            }
        }
        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param null $id
     * @param null $user
     * @return string
     * @throws HttpException
     */
    public function actionLink($id, $user)
    {
        $model = CallCenterUser::find()->where(['id' => $user])->byPersonId(null)->one();

        if ($model) {
            $model->person_id = $id;
            if ($model->save()) {
                Yii::$app->notifier->addNotification(Yii::t('common', 'Пользователь успешно привязан.'), 'success');
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }
        return $this->redirect(Url::toRoute('person/edit/' . $id));
    }

    /**
     * @return array|bool
     * @throws NotFoundHttpException
     */
    public function actionGetParents()
    {
        $officeId = Yii::$app->request->get('office_id');

        $Office = Office::findOne($officeId);

        if ($Office) {
            return Person::getPatentsCollection($Office->id, true);
        } else {
            throw new NotFoundHttpException(Yii::t('common', 'Офис не найден.'));
        }
    }

    /**
     * @return array|bool
     * @throws NotFoundHttpException
     */
    public function actionGetPersons()
    {
        $officeId = Yii::$app->request->get('office_id');

        $Office = Office::findOne($officeId);

        if ($Office) {
            return Person::getCollection($Office->id, true);
        } else {
            throw new NotFoundHttpException(Yii::t('common', 'Офис не найден.'));
        }
    }

    /**
     * @return array
     */
    public function actionGetShifts()
    {
        $response = [
            'status' => 'fail',
            'message' => Yii::t('common', "Ничего не сделали")
        ];
        $id = Yii::$app->request->get('office_id');
        if ($id) {
            $office = Office::findOne($id);
            if ($office) {
                $workingShifts = WorkingShift::find()
                    ->select(['id', 'name'])
                    ->byOfficeId($office->id)
                    ->orderBy(['name' => SORT_ASC])
                    ->asArray()
                    ->all();
                $response['shifts'] = $workingShifts;
            }
        }
        return $response;
    }

    /**
     * @return array
     */
    public function actionGetPersonByOrderId()
    {
        $response = [
            'status' => 'fail',
            'office_id' => '',
            'person_id' => '',
            'office_persons' => '',
        ];
        if ($orderId = Yii::$app->request->get('order_id')) {
            $model = PersonSearch::findPersonByOrderId($orderId);
            if ($model) {
                $response['status'] = 'success';
                $response['office_id'] = $model->office_id;
                $response['person_id'] = $model->id;
                $response['office_persons'] = Person::getCollection($model->office_id, true);
            }
        }

        return $response;
    }

    /**
     * @param integer $id
     * @return Person
     * @throws HttpException
     */
    private function getModel($id)
    {
        $model = Person::find()->bySystemUserCountries()->byId($id)->one();

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Сотрудник не найден.'));
        }

        return $model;
    }

    public function actionExport()
    {

        $inputRequest = Yii::$app->request->queryParams;

        $modelSearch = new PersonSearch();

        $dataProvider = $modelSearch->search($inputRequest);

        $excel = new \PHPExcel();
        $sheet = $excel->getSheet(0);
        $sheet->setTitle(Yii::t('common', 'Сотрудники'));

        $row = 1;
        $i = 0;
        $sheet->setCellValueByColumnAndRow($i++, $row, Yii::t('common', 'Номер'));
        $sheet->setCellValueByColumnAndRow($i++, $row, Yii::t('common', 'Офис'));
        $sheet->setCellValueByColumnAndRow($i++, $row, Yii::t('common', 'Направление'));
        $sheet->setCellValueByColumnAndRow($i++, $row, Yii::t('common', 'ФИО'));
        $sheet->setCellValueByColumnAndRow($i++, $row, Yii::t('common', 'Должность'));
        $sheet->setCellValueByColumnAndRow($i++, $row, Yii::t('common', 'Руководитель'));
        $sheet->setCellValueByColumnAndRow($i++, $row, Yii::t('common', 'Логин'));
        $sheet->setCellValueByColumnAndRow($i++, $row, Yii::t('common', 'Дата рождения'));
        $sheet->setCellValueByColumnAndRow($i++, $row, Yii::t('common', 'Дата приема на работу'));
        $sheet->setCellValueByColumnAndRow($i++, $row, Yii::t('common', 'Дата увольнения'));
        if (Yii::$app->user->can('view_salary_person_numbers')) {
            $sheet->setCellValueByColumnAndRow($i++, $row, Yii::t('common', 'Номер счета'));
            $sheet->setCellValueByColumnAndRow($i++, $row, Yii::t('common', 'Номер ID/Паспорта'));
        }
        $sheet->setCellValueByColumnAndRow($i++, $row, Yii::t('common', 'Активен'));
        $sheet->setCellValueByColumnAndRow($i++, $row, Yii::t('common', 'Подтвержден'));
        $sheet->setCellValueByColumnAndRow($i++, $row, Yii::t('common', 'Сумма з/п в местной валюте'));
        $sheet->setCellValueByColumnAndRow($i++, $row, Yii::t('common', 'Сумма з/п в USD'));
        $sheet->setCellValueByColumnAndRow($i++, $row, Yii::t('common', 'Корпоративная почта'));
        $sheet->setCellValueByColumnAndRow($i++, $row, Yii::t('common', 'Skype'));
        $sheet->setCellValueByColumnAndRow($i++, $row, Yii::t('common', 'Телефон'));
        $sheet->setCellValueByColumnAndRow($i++, $row, Yii::t('common', 'Рабочая смена'));
        $sheet->setCellValueByColumnAndRow($i++, $row, Yii::t('common', 'Пн'));
        $sheet->setCellValueByColumnAndRow($i++, $row, Yii::t('common', 'Вт'));
        $sheet->setCellValueByColumnAndRow($i++, $row, Yii::t('common', 'Ср'));
        $sheet->setCellValueByColumnAndRow($i++, $row, Yii::t('common', 'Чт'));
        $sheet->setCellValueByColumnAndRow($i++, $row, Yii::t('common', 'Пт'));
        $sheet->setCellValueByColumnAndRow($i++, $row, Yii::t('common', 'Сб'));
        $sheet->setCellValueByColumnAndRow($i++, $row, Yii::t('common', 'Вс'));
        $sheet->setCellValueByColumnAndRow($i++, $row, Yii::t('common', 'Обед'));
        $row++;

        foreach ($dataProvider->query->all() as $item) {
            $i = 0;
            $sheet->setCellValueByColumnAndRow($i++, $row, $item->id);
            $sheet->setCellValueByColumnAndRow($i++, $row, $item->office_id ? $item->office->name : '');
            $directions = [];
            $logins = [];
            if ($item->callCenterUsers) {
                foreach ($item->callCenterUsers as $callCenterUser) {
                    $directions[] = $callCenterUser->callCenter->country->name ?? '-';
                    $logins[] = $callCenterUser->user_login ?? $callCenterUser->user_id;
                }
            }
            $sheet->setCellValueByColumnAndRow($i++, $row, implode(", ", $directions));
            $sheet->setCellValueByColumnAndRow($i++, $row, $item->name);
            $sheet->setCellValueByColumnAndRow($i++, $row, $item->designation_id && $item->designation ? $item->designation->name : '');
            $sheet->setCellValueByColumnAndRow($i++, $row, $item->parent_id ? $item->parent->name : '');
            $sheet->setCellValueByColumnAndRow($i++, $row, implode(", ", $logins));
            $sheet->setCellValueByColumnAndRow($i++, $row, $item->birth_date ? Yii::$app->formatter->asDate($item->birth_date) : '');
            $sheet->setCellValueByColumnAndRow($i++, $row, $item->start_date ? Yii::$app->formatter->asDate($item->start_date) : '');
            $sheet->setCellValueByColumnAndRow($i++, $row, $item->dismissal_date ? Yii::$app->formatter->asDate($item->dismissal_date) : '');
            if (Yii::$app->user->can('view_salary_person_numbers')) {
                $sheet->setCellValueByColumnAndRow($i++, $row, $item->account_number);
                $sheet->setCellValueByColumnAndRow($i++, $row, $item->passport_id_number);
            }
            $sheet->setCellValueByColumnAndRow($i++, $row, $item->active ? Yii::t('common', 'да') : Yii::t('common', 'нет'));
            $sheet->setCellValueByColumnAndRow($i++, $row, $item->approved ? Yii::t('common', 'да') : Yii::t('common', 'нет'));
            $sheet->setCellValueByColumnAndRow($i++, $row, $item->salary_local);
            $sheet->setCellValueByColumnAndRow($i++, $row, $item->salary_usd);
            $sheet->setCellValueByColumnAndRow($i++, $row, $item->corporate_email);
            $sheet->setCellValueByColumnAndRow($i++, $row, $item->skype);
            $sheet->setCellValueByColumnAndRow($i++, $row, $item->phone);
            $sheet->setCellValueByColumnAndRow($i++, $row, $item->working_shift_id ? $item->workingShift->name : '');
            $sheet->setCellValueByColumnAndRow($i++, $row, $item->working_shift_id ? $item->workingShift->working_mon : '');
            $sheet->setCellValueByColumnAndRow($i++, $row, $item->working_shift_id ? $item->workingShift->working_tue : '');
            $sheet->setCellValueByColumnAndRow($i++, $row, $item->working_shift_id ? $item->workingShift->working_wed : '');
            $sheet->setCellValueByColumnAndRow($i++, $row, $item->working_shift_id ? $item->workingShift->working_thu : '');
            $sheet->setCellValueByColumnAndRow($i++, $row, $item->working_shift_id ? $item->workingShift->working_fri : '');
            $sheet->setCellValueByColumnAndRow($i++, $row, $item->working_shift_id ? $item->workingShift->working_sat : '');
            $sheet->setCellValueByColumnAndRow($i++, $row, $item->working_shift_id ? $item->workingShift->working_sun : '');
            $sheet->setCellValueByColumnAndRow($i++, $row, $item->working_shift_id ? $item->workingShift->lunch_time : '');
            $row++;
        }

        foreach (range('A', 'Y') as $columnID) {
            $excel->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        $dir = Yii::getAlias("@runtime") . DIRECTORY_SEPARATOR . "persons";
        Utils::prepareDir($dir);

        $filename = $dir . DIRECTORY_SEPARATOR . date('Y-m-d') . '_' . Yii::t('common', 'persons') . '_' . ($modelSearch->office->country->name_en ?? '') . ".xlsx";

        $objWriter = \PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        $objWriter->save($filename);

        Yii::$app->response->sendFile($filename);

    }

    /**
     * @throws ForbiddenHttpException
     * @return array
     */
    public function actionChangeTeamLeader()
    {

        $response = [
            'status' => 'fail',
            'message' => Yii::t('common', "Ничего не сделали")
        ];

        $id = Yii::$app->request->post('id');
        $parentId = Yii::$app->request->post('teamLeader');

        $teamLeader = $this->getModel($parentId);

        $model = $this->getModel($id);

        $success = true;

        $model->parent_id = $teamLeader->id;

        if (!$model->save()) {
            $success = false;
            $response['message'] = Yii::t('common', 'Не удалось сменить руководителя');
        }

        if ($success) {
            $response['status'] = 'success';
            $response['message'] = Yii::t('common', 'Операция выполнена успешно');
        }

        return $response;
    }

    /**
     * @throws ForbiddenHttpException
     * @return array
     */
    public function actionDeactivatePersons()
    {

        $response = [
            'status' => 'fail',
            'message' => Yii::t('common', "Ничего не сделали")
        ];

        $id = Yii::$app->request->post('id');
        $date = Yii::$app->request->post('date');

        $model = $this->getModel($id);

        $success = true;

        if ($model->active != Person::NOT_ACTIVE) {
            $model->active = Person::NOT_ACTIVE;
            $model->dismissal_date = $date ? Yii::$app->formatter->asDate($date, 'php:Y-m-d') : date('Y-m-d');

            $transaction = Yii::$app->db->beginTransaction();
            if ($model->save(true)) {
                try {
                    $model->blockCallCenterUsers();
                    $res = $model->makeDismissalBonus();
                    if (!empty($res['errors'])) {
                        $success = false;
                    } else {
                        $transaction->commit();
                        $success = true;
                    }
                } catch (\Exception $e) {
                    $transaction->rollBack();
                    $success = false;
                }
            } else {
                $transaction->rollBack();
                $success = false;
            }

            if ($success === false) {
                $response['message'] = Yii::t('common', 'Не удалось деактивировать сотрудника');
            }
        } else {
            $success = false;
            $response['message'] = Yii::t('common', 'Сотрудник уже деактивирован');
        }

        if ($success) {
            $response['status'] = 'success';
            $response['message'] = Yii::t('common', 'Операция выполнена успешно');
        }

        return $response;
    }

    /**
     * @throws ForbiddenHttpException
     * @return array
     */
    public function actionActivatePersons()
    {

        $response = [
            'status' => 'fail',
            'message' => Yii::t('common', "Ничего не сделали")
        ];

        $id = Yii::$app->request->post('id');
        $model = $this->getModel($id);

        $success = true;

        if ($model->active != Person::ACTIVE) {
            $model->active = Person::ACTIVE;
            $model->dismissal_date = null;
            if (!$model->save()) {
                $success = false;
                $response['message'] = Yii::t('common', 'Не удалось активировать сотрудника');
            }
        } else {
            $success = false;
            $response['message'] = Yii::t('common', 'Сотрудник уже активен');
        }
        if ($success) {
            $response['status'] = 'success';
            $response['message'] = Yii::t('common', 'Операция выполнена успешно');
        }
        return $response;
    }

    /**
     * @throws ForbiddenHttpException
     * @return array
     */
    public function actionNotapprovePersons()
    {

        $response = [
            'status' => 'fail',
            'message' => Yii::t('common', "Ничего не сделали")
        ];

        $id = Yii::$app->request->post('id');

        $model = $this->getModel($id);

        $success = true;

        if ($model->approved != Person::NOT_ACTIVE) {
            $model->approved = Person::NOT_ACTIVE;
            if (!$model->save()) {
                $success = false;
                $response['message'] = Yii::t('common', 'Не удалось убрать подтверждение у сотрудника');
            }
        } else {
            $success = false;
            $response['message'] = Yii::t('common', 'У сотрудника уже убрано подтверждение');
        }

        if ($success) {
            $response['status'] = 'success';
            $response['message'] = Yii::t('common', 'Операция выполнена успешно');
        }

        return $response;
    }

    /**
     * @throws ForbiddenHttpException
     * @return array
     */
    public function actionApprovePersons()
    {

        $response = [
            'status' => 'fail',
            'message' => Yii::t('common', "Ничего не сделали")
        ];

        $id = Yii::$app->request->post('id');
        $model = $this->getModel($id);

        $success = true;

        if ($model->approved != Person::ACTIVE) {
            $model->approved = Person::ACTIVE;
            if (!$model->save()) {
                $success = false;
                $response['message'] = Yii::t('common', 'Не удалось подтвердить сотрудника');
            }
        } else {
            $success = false;
            $response['message'] = Yii::t('common', 'Сотрудник уже подтвержден');
        }
        if ($success) {
            $response['status'] = 'success';
            $response['message'] = Yii::t('common', 'Операция выполнена успешно');
        }
        return $response;
    }

    /**
     * @throws ForbiddenHttpException
     * @return array
     */
    public function actionSetRole()
    {
        $id = Yii::$app->request->post('id');

        $model = $this->getModel($id);

        $success = false;

        $designation = Designation::findOne(Yii::$app->request->post('role'));

        if ($designation) {
            $model->designation_id = $designation->id;
            if ($model->save()) {
                $success = true;
            }
        }

        if (!$success) {
            $response['status'] = 'fail';
            $response['message'] = Yii::t('common', 'Не удалось установить должность');
        } else {
            $response['status'] = 'success';
            $response['message'] = Yii::t('common', 'Операция выполнена успешно');
        }

        return $response;
    }


    /**
     * @throws ForbiddenHttpException
     * @return array
     */
    public function actionSetWork()
    {

        $id = Yii::$app->request->post('id');
        $dateFrom = Yii::$app->request->post('date_from') ?? '';
        $dateTo = Yii::$app->request->post('date_to') ?? '';

        $model = $this->getModel($id);

        $success = false;

        $workingShift = WorkingShift::find()
            ->byId(Yii::$app->request->post('work'))
            ->byOfficeId($model->office_id)
            ->one();

        if ($workingShift) {
            $model->working_shift_id = $workingShift->id;
            if ($model->save()) {
                if ($dateFrom != '' && $dateTo != '') {
                    $generator = new WorkTimePlanGenerate();
                    $generator->dateFrom = $dateFrom;
                    $generator->dateTo = $dateTo;
                    $generator->person_id = $model->id;
                    $generator->working_shift_id = $model->working_shift_id;
                    if ($generator->validate()) {
                        if ($generator->generate()) {
                            $success = true;
                        } else {
                            $response['message'] = Yii::t('common', 'Ошибка генерации.');
                        }
                    } else {
                        $response['message'] = $generator->getFirstErrorAsString();
                    }
                } else {
                    $success = true;
                }
            } else {
                $response['message'] = $model->getFirstErrorAsString();
            }
        }

        if (!$success) {
            $response['status'] = 'fail';
            $response['message'] = $response['message'] ?? Yii::t('common', 'Не удалось установить рабочуюю смену');
        } else {
            $response['status'] = 'success';
            $response['message'] = Yii::t('common', 'Операция выполнена успешно');
        }

        return $response;
    }

    /**
     * @throws ForbiddenHttpException
     * @return array
     */
    public function actionSetBonus()
    {

        $id = Yii::$app->request->post('id');

        $model = $this->getModel($id);

        $success = false;

        $bonusType = BonusType::find()->byId(Yii::$app->request->post('type'))->byOfficeId($model->office_id)->one();

        if ($bonusType) {
            $bonus = new Bonus();
            $bonus->person_id = $model->id;
            $bonus->bonus_type_id = $bonusType->id;
            $bonus->date = Yii::$app->request->post('date');
            $bonus->bonus_percent = Yii::$app->request->post('percent');
            $bonus->bonus_sum_usd = Yii::$app->request->post('sum_usd');
            $bonus->bonus_sum_local = Yii::$app->request->post('sum_local');
            $bonus->bonus_hour = Yii::$app->request->post('hour');
            $bonus->bonus_percent_hour = Yii::$app->request->post('percent_hour');
            $bonus->comment = Yii::$app->request->post('comment');

            if ($bonus->save()) {
                $success = true;
            }
        }

        if (!$success) {
            $response['status'] = 'fail';
            $response['message'] = Yii::t('common', 'Не удалось назначить бонус');
        } else {
            $response['status'] = 'success';
            $response['message'] = Yii::t('common', 'Операция выполнена успешно');
        }

        return $response;
    }

    /**
     * @throws ForbiddenHttpException
     * @return array
     */
    public function actionSetPenalty()
    {

        $id = Yii::$app->request->post('id');

        $model = $this->getModel($id);

        $success = false;

        $penaltyType = PenaltyType::find()->byId(Yii::$app->request->post('type'))->one();

        if ($penaltyType) {
            $penalty = new Penalty();
            $penalty->person_id = $model->id;
            $penalty->penalty_type_id = $penaltyType->id;
            $penalty->date = Yii::$app->request->post('date');
            $penalty->penalty_percent = Yii::$app->request->post('percent');
            $penalty->penalty_sum_usd = Yii::$app->request->post('sum_usd');
            $penalty->penalty_sum_local = Yii::$app->request->post('sum_local');
            $penalty->comment = Yii::$app->request->post('comment');

            if ($penalty->save()) {
                $success = true;
            }
        }

        if (!$success) {
            $response['status'] = 'fail';
            $response['message'] = Yii::t('common', 'Не удалось назначить штраф');
        } else {
            $response['status'] = 'success';
            $response['message'] = Yii::t('common', 'Операция выполнена успешно');
        }

        return $response;
    }

    /**
     * @param integer $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionLogs($id)
    {
        $model = $this->getModel($id);

        $logs = $this->findModelLogs($model->id);

        $childLog = TableLog::find()->byTable(CallCenterUser::clearTableName())->byLink('person_id', $model->id)->allSorted();

        return $this->render('logs', [
            'logs' => $logs,
            'childLog' => $childLog,
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return PersonLog[]
     */
    protected function findModelLogs($id)
    {
        return PersonLog::find()
            ->joinWith('user')
            ->where([PersonLog::tableName() . '.person_id' => $id])
            ->orderBy([
                'id' => SORT_DESC
            ])
            ->all();
    }
}