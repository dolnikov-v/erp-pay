<?php
namespace app\modules\salary\controllers;

use app\components\filters\AjaxFilter;
use app\components\web\Controller;
use app\modules\salary\models\Person;
use app\modules\salary\models\WorkingShift;
use app\modules\salary\models\WorkTimePlan;
use app\modules\salary\models\WorkTimePlanGenerate;
use app\modules\salary\models\search\WorkTimePlanSearch;
use Yii;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

/**
 * Class WorkTimePlanController
 * @package app\modules\salary\controllers
 */
class WorkTimePlanController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => [
                    'delete-list',
                    'get-times',
                    'get-shifts'
                ],
            ]
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $inputRequest = Yii::$app->request->queryParams;

        $modelSearch = new WorkTimePlanSearch();

        $dataProvider = $modelSearch->search($inputRequest);
        $dataProvider->pagination->pageSize = yii::$app->request->get('per-page');

        return $this->render('index', [
            'modelSearch' => $modelSearch,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * @param null $id
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->getModel($id);
            $model->start_at = date('H:i', $model->start_at);
            $model->end_at = date('H:i', $model->end_at);
        } else {
            $model = new WorkTimePlan();
            $model->loadDefaultValues();

            $model->date = date('Y-m-d');
            $model->lunch_time = '14:00-15:00';
            $model->start_at = '10:00';
            $model->end_at = '19:00';
        }

        $post = Yii::$app->request->post();

        if ($model->load($post)) {
            $isNewRecord = $model->isNewRecord;
            $model->date = $model->date ? Yii::$app->formatter->asDate($model->date, 'php:Y-m-d') : null;
            if ($model->save()) {
                Yii::$app->notifier->addNotification($isNewRecord ? Yii::t('common', 'План успешно добавлен.') : Yii::t('common', 'План успешно сохранен.'), 'success');
                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }

        $persons = Person::find()->bySystemUserCountries()->orderBy(['name' => SORT_ASC])->collection();

        $renderParams = [
            'model' => $model,
            'persons' => $persons
        ];

        return $this->render('edit', $renderParams);
    }

    /**
     * @param null $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $model = $this->getModel($id);

        if ($model->delete()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'План удален.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param integer $id
     * @return WorkTimePlan
     * @throws NotFoundHttpException
     */
    private function getModel($id)
    {
        $model = WorkTimePlan::findOne($id);

        if (!$model) {
            throw new NotFoundHttpException(Yii::t('common', 'План не найден.'));
        }

        return $model;
    }

    /**
     * @return string
     */
    public function actionGenerate()
    {

        $model = new WorkTimePlanGenerate();
        $model->dateFrom = date('Y-m-01');
        $model->dateTo = date('Y-m-t');


        $post = Yii::$app->request->post();
        if ($model->load($post)) {
            if ($model->validate()) {
                if ($model->generate()) {
                    Yii::$app->notifier->addNotification(Yii::t('common', 'План успешно сгенерирован.'), 'success');
                    return $this->redirect(Url::toRoute('work-time-plan/index'));
                } else {
                    Yii::$app->notifier->addNotification(Yii::t('common', 'Ошибка генерации.'), 'danger');
                }
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }

        $renderParams = [
            'model' => $model,
        ];

        return $this->render('generate', $renderParams);
    }

    /**
     * @throws NotFoundHttpException
     * @return array
     */
    public function actionDeleteList()
    {

        $response = [
            'status' => 'fail',
            'message' => Yii::t('common', "Ничего не сделали")
        ];

        $id = Yii::$app->request->post('id');

        $model = $this->getModel($id);

        if ($model->delete()) {
            $response['status'] = 'success';
            $response['message'] = Yii::t('common', 'Операция выполнена успешно');
        } else {
            $response['message'] = Yii::t('common', 'Не удалось удалить');
        }

        return $response;
    }

    /**
     * @return array
     */
    public function actionGetTimes()
    {
        $response = [
            'status' => 'fail',
            'message' => Yii::t('common', "Ничего не сделали")
        ];
        $id = Yii::$app->request->get('working_shift_id');
        $date = Yii::$app->request->get('date');
        if ($id) {
            $workingShift = WorkingShift::findOne($id);

            $startAt = $endAt = '';
            $workingDayName = 'working_' . strtolower(date('D', strtotime($date)));
            if ($workingShift->hasAttribute($workingDayName) && $workingShift->$workingDayName != '') {
                $workingTime = explode('-', $workingShift->$workingDayName);
                $startAt = $workingTime[0] ?? '';
                $endAt = $workingTime[1] ?? '';
            }

            if ($workingShift) {
                $response = [
                    'status' => 'success',
                    'start_at' => $startAt,
                    'end_at' => $endAt,
                    'lunch_time' => $workingShift->lunch_time
                ];
            }
        }
        return $response;
    }

    /**
     * @return array
     */
    public function actionGetShifts()
    {
        $response = [
            'status' => 'fail',
            'message' => Yii::t('common', "Ничего не сделали")
        ];
        $id = Yii::$app->request->get('person_id');
        if ($id) {
            $person = Person::findOne($id);
            if ($person) {
                $workingShifts = WorkingShift::find()
                    ->select(['id', 'name'])
                    ->byOfficeId($person->office_id)
                    ->orderBy(['name' => SORT_ASC])
                    ->asArray()
                    ->all();
                $response['shifts'] = $workingShifts;
            }
        }
        return $response;
    }
}
