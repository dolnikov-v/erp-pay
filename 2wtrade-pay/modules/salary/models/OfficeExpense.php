<?php

namespace app\modules\salary\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\Currency;
use app\modules\salary\models\query\OfficeExpenseQuery;
use Yii;
use yii\helpers\BaseFileHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "salary_office_expense".
 *
 * @property integer $id
 * @property integer $office_id
 * @property string $type
 * @property double $expense
 * @property integer $currency_id
 * @property string $filename
 * @property string $date_from
 * @property string $date_to
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Office $office
 * @property Currency $currency
 */
class OfficeExpense extends ActiveRecordLogUpdateTime
{

    /**
     * @var UploadedFile
     */
    public $file;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%salary_office_expense}}';
    }

    /**
     * @return OfficeExpenseQuery
     */
    public static function find()
    {
        return new OfficeExpenseQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['expense', 'type'], 'required'],
            [['office_id', 'currency_id', 'created_at', 'updated_at'], 'integer'],
            [['date_from', 'date_to'], 'safe'],
            [['expense'], 'number'],
            [['type', 'filename'], 'string', 'max' => 255],
            [['date_from', 'date_to'], 'validateDates', 'skipOnEmpty' => false],
            [['file'], 'file', 'skipOnEmpty' => true],
            [
                ['office_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Office::className(),
                'targetAttribute' => ['office_id' => 'id']
            ],
            [
                ['currency_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Currency::className(),
                'targetAttribute' => ['currency_id' => 'id']
            ],
        ];
    }

    /**
     * @param string $attribute
     */
    public function validateDates($attribute)
    {
        if ($this->date_from && $this->date_to) {
            if ($this->date_from >= $this->date_to) {
                $this->addError($attribute, Yii::t('common', 'Дата начала превышает или равна Дате окончания'));
            }
        }
        // период [-oo; +oo]
        $query = self::find()
            ->where([
                'office_id' => $this->office_id,
                'type' => $this->type,
            ])
            ->andWhere(['is', 'date_from', null])
            ->andWhere(['is', 'date_to', null]);
        if (!$this->isNewRecord) {
            $query->andWhere(['!=', 'id', $this->id]);
        }
        $find = $query->limit(1)->one();
        if ($find !== null) {
            $this->addError($attribute, Yii::t('common', 'Такой период уже используется в расходе {id}', ['id' => $find->id]));
        }

        // период [-oo; N] ищем date_from<=N или date_to<=N
        $query = self::find()
            ->where([
                'office_id' => $this->office_id,
                'type' => $this->type,
            ])
            ->andWhere(['is', 'date_from', null])
            ->andWhere(['is not', 'date_to', null])
            ->andWhere([
                'or',
                [
                    'and',
                    "'$this->date_from' is not null",
                    ['>=', 'date_to', $this->date_from],
                ],
                [
                    'and',
                    "'$this->date_to' is not null",
                    ['>=', 'date_to', $this->date_to],
                ]
            ]);
        if (!$this->isNewRecord) {
            $query->andWhere(['!=', 'id', $this->id]);
        }
        $find = $query->limit(1)->one();
        if ($find !== null) {
            $this->addError($attribute, Yii::t('common', 'Такой период уже используется в расходе {id}', ['id' => $find->id]));
        }

        // период [N; +oo] ищем date_from>=N или date_to>=N
        $query = self::find()
            ->where([
                'office_id' => $this->office_id,
                'type' => $this->type,
            ])
            ->andWhere(['is not', 'date_from', null])
            ->andWhere(['is', 'date_to', null])
            ->andWhere([
                'or',
                [
                    'and',
                    "'$this->date_from' is not null",
                    ['<=', 'date_from', $this->date_from],
                ],
                [
                    'and',
                    "'$this->date_to' is not null",
                    ['<=', 'date_from', $this->date_to],
                ]
            ]);
        if (!$this->isNewRecord) {
            $query->andWhere(['!=', 'id', $this->id]);
        }
        $find = $query->limit(1)->one();
        if ($find !== null) {
            $this->addError($attribute, Yii::t('common', 'Такой период уже используется в расходе {id}', ['id' => $find->id]));
        }

        // период [N; M] ищем (date_from>=N и date_from<=M) или (date_to>=N и date_to<=M)
        $query = self::find()
            ->where([
                'office_id' => $this->office_id,
                'type' => $this->type,
            ])
            ->andWhere(['is not', 'date_from', null])
            ->andWhere(['is not', 'date_to', null])
            ->andWhere([
                'or',
                [
                    'and',
                    "'$this->date_from' is not null",
                    ['>=', 'date_from', $this->date_from],
                    ['<=', 'date_to', $this->date_from]
                ],
                [
                    'and',
                    "'$this->date_to' is not null",
                    ['>=', 'date_from', $this->date_to],
                    ['<=', 'date_to', $this->date_to]
                ]
            ]);
        if (!$this->isNewRecord) {
            $query->andWhere(['!=', 'id', $this->id]);
        }
        $find = $query->limit(1)->one();
        if ($find !== null) {
            $this->addError($attribute, Yii::t('common', 'Такой период уже используется в расходе {id}', ['id' => $find->id]));
        }
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'office_id' => Yii::t('common', 'Офис'),
            'type' => Yii::t('common', 'Вид расхода'),
            'expense' => Yii::t('common', 'Значение расхода'),
            'currency_id' => Yii::t('common', 'Валюта'),
            'filename' => Yii::t('common', 'Документ'),
            'date_from' => Yii::t('common', 'Дата начала'),
            'date_to' => Yii::t('common', 'Дата окончания'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOffice()
    {
        return $this->hasOne(Office::className(), ['id' => 'office_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    /**
     * @param string $file
     * @return string
     */
    public static function getPathFile($file = "")
    {
        $path = Yii::getAlias('@media') . DIRECTORY_SEPARATOR . 'office-expense';
        if ($file) {
            $path .= DIRECTORY_SEPARATOR . $file;
        }
        return $path;
    }

    /**
     * @param string $name
     * @return string
     */
    public function getUniqueName($name)
    {
        return md5($name . microtime());
    }

    /**
     * @param string $attribute
     * @return bool|string
     * @throws \yii\base\Exception
     */
    public function upload($attribute = 'file')
    {
        if ($this->validate() && $this->$attribute) {

            if (!file_exists(self::getPathFile())) {
                BaseFileHelper::createDirectory(self::getPathFile());
            }

            $uniqueFileName = $this->getUniqueName($this->$attribute->baseName) . '.' . $this->$attribute->extension;
            $this->$attribute->saveAs(self::getPathFile($uniqueFileName));
            return $uniqueFileName;
        } else {
            return false;
        }
    }
}
