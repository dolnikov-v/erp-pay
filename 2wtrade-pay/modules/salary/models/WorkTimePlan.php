<?php

namespace app\modules\salary\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\modules\salary\models\query\WorkTimePlanQuery;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "salary_work_time_plan".
 *
 * @property integer $id
 * @property integer $person_id
 * @property integer $working_shift_id
 * @property string $date
 * @property string $work_hours
 * @property string $lunch_time
 * @property integer $start_at
 * @property integer $end_at
 *
 * @property Person $person
 * @property WorkingShift $workingShift
 */
class WorkTimePlan extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%salary_work_time_plan}}';
    }

    /**
     * @return WorkTimePlanQuery
     */
    public static function find()
    {
        return new WorkTimePlanQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['person_id', 'start_at', 'end_at'], 'required'],
            [['person_id', 'working_shift_id', 'created_at', 'updated_at'], 'integer'],
            [['date'], 'safe'],
            [['work_hours'], 'number'],
            [['lunch_time'], 'string', 'max' => 255],
            [['person_id'], 'exist', 'skipOnError' => true, 'targetClass' => Person::className(), 'targetAttribute' => ['person_id' => 'id']],
            [['lunch_time'], 'match', 'pattern' => '/^\d{1,2}:\d{2}-\d{1,2}:\d{2}$/i',
                'message' => Yii::t('common', 'Значение "{attribute}" не верно, пример: 10:00-19:00')],
            [['start_at', 'end_at'], 'match', 'pattern' => '/^\d{1,2}:\d{2}$/i',
                'message' => Yii::t('common', 'Значение "{attribute}" не верно, пример: 10:00')],
            [['date'], 'required', 'enableClientValidation' => false],
            [['person_id'], 'unique',
                'targetAttribute' => ['person_id', 'date'],
                'message' => Yii::t('common', 'План на эту дату уже есть у сотрудника')
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'person_id' => Yii::t('common', 'Сотрудник'),
            'working_shift_id' => Yii::t('common', 'Рабочая смена'),
            'date' => Yii::t('common', 'Дата'),
            'work_hours' => Yii::t('common', 'Рабочие часы'),
            'lunch_time' => Yii::t('common', 'Время обеда'),
            'start_at' => Yii::t('common', 'Начало работы'),
            'end_at' => Yii::t('common', 'Окончание работы'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerson()
    {
        return $this->hasOne(Person::className(), ['id' => 'person_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkingShift()
    {
        return $this->hasOne(WorkingShift::className(), ['id' => 'working_shift_id']);
    }

    public function beforeSave($insert)
    {
        $this->work_hours = self::calcHours($this->start_at . ':' . $this->end_at) - self::calcHours($this->lunch_time);
        $this->start_at = strtotime($this->date . ' ' . $this->start_at);
        $this->end_at = $this->start_at + $this->work_hours * 3600;
        return parent::beforeSave($insert);
    }

    /**
     * Считает часы из строки вида 10:00-19:00
     * @param string $time
     * @return float|int
     */
    public static function calcHours($time)
    {
        if ($time == '') {
            return 0;
        }
        preg_match('/(\d+):(\d+)\D*(\d+):(\d+)/', $time, $matches);
        list(, $startHour, $startMin, $endHour, $endMin) = $matches;
        if ($endHour < $startHour) {
            $endHour = 24 + $endHour;
        }
        $totalMinutes = ($endHour * 60 + $endMin - ($startHour * 60 + $startMin));
        $return = $totalMinutes / 60;
        return $return;
    }


    /**
     * Получить план по сотруднику за период
     *
     * @param $personId
     * @param $dateFrom
     * @param $dateTo
     * @param null $dateStart
     * @param null $dateDismissal
     * @return array
     */
    public static function getPlanList($personId, $dateFrom, $dateTo, $dateStart = null, $dateDismissal = null) {
        $s = strtotime($dateStart);
        $d = strtotime($dateDismissal);
        $f = strtotime($dateFrom);
        $t = strtotime($dateTo);

        // определим пределы за которые должны считать ЗП
        if (!$s) $s = $f;
        $f = max($s, $f);
        if (!$d) $d = $t;
        $t = min($d, $t);

        // есть ли у нас план для сотрудника на эти даты?
        $queryPlan = WorkTimePlan::find()
            ->byPersonId($personId)
            ->andWhere(['>=', 'date', date('Y-m-d', $f)])
            ->andWhere(['<=', 'date', date('Y-m-d', $t)])
            ->asArray();
        $planTimes = ArrayHelper::map($queryPlan->all(), 'date', 'work_hours');

        if (!$planTimes) {
            // план не заполняли, сгенерим
            $plan = new WorkTimePlanGenerate();
            $plan->person_id = $personId;
            $plan->dateFrom = date('Y-m-d', $f);
            $plan->dateTo = date('Y-m-d', $t);
            $plan->generate();
            $planTimes = ArrayHelper::map($queryPlan->all(), 'date', 'work_hours');
        }

        return $planTimes;
    }

}