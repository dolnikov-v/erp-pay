<?php
namespace app\modules\salary\models\query;

use app\components\db\ActiveQuery;
use app\modules\salary\models\Holiday;
use app\models\User;
use Yii;

/**
 * Class HolidayQuery
 * @package app\modules\salary\models\query
 * @method Holiday one($db = null)
 * @method Holiday[] all($db = null)
 */
class HolidayQuery extends ActiveQuery
{

    /**
     * @param integer $countryId
     * @return $this
     */
    public function byCountryId($countryId = null)
    {
        if ($countryId) {
            return $this->andWhere(['country_id' => $countryId]);
        }
        return $this;
    }

    /**
     * @param integer $id
     * @return $this
     */
    public function byId($id = null)
    {
        if ($id) {
            return $this->andWhere(['id' => $id]);
        }
        return $this;
    }

    /**
     * @return $this
     */
    public function bySystemUserCountries()
    {
        if (Yii::$app->user->isSuperadmin) {
            return $this;
        }
        return $this->andWhere(['country_id' => array_keys(User::getAllowCountries())]);
    }

    /**
     * @param string $date
     * @return $this
     */
    public function byDate($date = null)
    {
        if ($date) {
            return $this->andWhere(['date' => date('Y-m-d', strtotime($date))]);
        }
        return $this;
    }
}
