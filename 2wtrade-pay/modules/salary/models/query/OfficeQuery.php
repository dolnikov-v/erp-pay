<?php
namespace app\modules\salary\models\query;

use app\components\db\ActiveQuery;
use app\modules\callcenter\models\CallCenter;
use app\modules\salary\models\Office;
use app\models\User;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class OfficeQuery
 * @package app\modules\salary\models\query
 * @method Office one($db = null)
 * @method Office[] all($db = null)
 */
class OfficeQuery extends ActiveQuery
{

    /**
     * @return $this
     */
    public function active()
    {
        return $this->andWhere([Office::tableName() . '.active' => 1]);
    }

    /**
     * @return $this
     */
    public function callCenter()
    {
        return $this->andWhere([Office::tableName() . '.type' => 'call_center']);
    }

    /**
     * @param integer $countryId
     * @return $this
     */
    public function byCountryId($countryId)
    {
        return $this->andWhere(['country_id' => $countryId]);
    }

    /**
     * @param integer|array $id
     * @return $this
     */
    public function byId($id = null)
    {
        if ($id) {
            return $this->andWhere([Office::tableName() . '.id' => $id]);
        }
        return $this;
    }

    /**
     * @param integer|array $name
     * @return $this
     */
    public function byName($name = null)
    {
        if ($name) {
            return $this->andWhere(['like', 'name', $name]);
        }
        return $this;
    }

    /**
     * @return $this
     */
    public function bySystemUserCountries()
    {
        if (Yii::$app->user->isSuperadmin) {
            return $this;
        }

        $officeTypes = [];
        if (Yii::$app->user->can('office_view_type_call_center')) {
            $officeTypes[] = Office::TYPE_CALL_CENTER;
        }
        if (Yii::$app->user->can('office_view_type_storage')) {
            $officeTypes[] = Office::TYPE_STORAGE;
        }
        if (Yii::$app->user->can('office_view_type_branch')) {
            $officeTypes[] = Office::TYPE_BRANCH;
        }
        if (Yii::$app->user->can('office_view_type_office')) {
            $officeTypes[] = Office::TYPE_OFFICE;
        }

        if (!Yii::$app->user->can('limit_call_center_persons_by_direction')) {
            // для всех случаев кроме ограничения по направлениям
            $this->andWhere(['country_id' => array_keys(User::getAllowCountries())]);
        }

        if (Yii::$app->user->can('limit_call_center_persons_by_direction')) {
            // country.curator может только смотреть те офисы, в которых есть хотябы одно направление ему доступное
            $relatedOffices = ArrayHelper::getColumn(CallCenter::find()->bySystemUserCountries()->joinWith('callCenterToOffices')->asArray()->all(), 'callCenterToOffices');
            $officeIds = [];
            foreach ($relatedOffices as $tmp) {
                foreach ($tmp as $related) {
                    $officeIds[$related['office_id']] = $related['office_id'];
                }
            }
            if (!$officeIds) {
                $officeIds[] = -1;
            }

            $this->andWhere(
                [
                    'or',
                    ['id' => $officeIds],
                    [
                        'and',
                        ['country_id' => array_keys(User::getAllowCountries())]
                    ]
                ]
            );
        }

        if ($officeTypes) {
            $this->andWhere(['type' => $officeTypes]);
        }

        return $this;
    }
}
