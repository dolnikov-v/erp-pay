<?php
namespace app\modules\salary\models\query;

use app\components\db\ActiveQuery;
use app\modules\salary\models\PersonLink;

/**
 * Class PersonLinkQuery
 * @package app\modules\salary\models\query
 * @method PersonLink one($db = null)
 * @method PersonLink[] all($db = null)
 */
class PersonLinkQuery extends ActiveQuery
{
    /**
     * @param $id
     * @return $this
     */
    public function byForeignId($id)
    {
        return $this->andWhere([PersonLink::tableName() . '.foreign_id' => $id]);
    }

    /**
     * @return $this
     */
    public function isCrocoTime()
    {
        return $this->andWhere([PersonLink::tableName() . '.type' => PersonLink::TYPE_CROCOTIME]);
    }

    /**
     * @return $this
     */
    public function isERP()
    {
        return $this->andWhere([PersonLink::tableName() . '.type' => PersonLink::TYPE_ERP]);
    }

    /**
     * @param integer $personId
     * @return $this
     */
    public function byPersonId($personId)
    {
        return $this->andWhere([PersonLink::tableName() . '.person_id' => $personId]);
    }
}