<?php
namespace app\modules\salary\models\query;

use app\components\db\ActiveQuery;
use app\modules\salary\models\OfficeTax;

/**
 * Class OfficeTaxQuery
 * @package app\modules\salary\models\query
 * @method OfficeTax one($db = null)
 * @method OfficeTax[] all($db = null)
 */
class OfficeTaxQuery extends ActiveQuery
{
    /**
     * @param integer $officeId
     * @return $this
     */
    public function byOfficeId($officeId)
    {
        return $this->andWhere(['office_id' => $officeId]);
    }
}
