<?php
namespace app\modules\salary\models\query;

use app\components\db\ActiveQuery;
use app\modules\salary\models\PersonImport;

/**
 * Class PersonImportQuery
 * @package app\modules\salary\models\query
 * @method PersonImport one($db = null)
 * @method PersonImport[] all($db = null)
 */
class PersonImportQuery extends ActiveQuery
{

}
