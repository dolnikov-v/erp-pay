<?php
namespace app\modules\salary\models\query;

use app\components\db\ActiveQuery;
use app\modules\salary\models\BonusType;

/**
 * Class BonusTypeQuery
 * @package app\modules\salary\models\query
 * @method BonusType one($db = null)
 * @method BonusType[] all($db = null)
 */
class BonusTypeQuery extends ActiveQuery
{
    /**
     * @var string
     */
    protected $collectionKey = 'id';

    /**
     * @var string
     */
    protected $collectionValue = 'name';

    /**
     * @return $this
     */
    public function active()
    {
        return $this->andWhere([BonusType::tableName() . '.active' => 1]);
    }

    /**
     * @return $this
     */
    public function dismissalPay()
    {
        return $this->andWhere([BonusType::tableName() . '.is_dismissal_pay' => 1]);
    }

    /**
     * @param integer $id
     * @return $this
     */
    public function byId($id)
    {
        return $this->andWhere([BonusType::tableName() . '.id' => $id]);
    }

    /**
     * @param integer $id
     * @return $this
     */
    public function activeOrById($id)
    {
        return $this->andWhere(
            ['or',
                [BonusType::tableName() . '.active' => 1],
                [BonusType::tableName() . '.id' => $id]
            ]
        );
    }

    /**
     * @return $this
     */
    public function notActive()
    {
        return $this->andWhere(['active' => 0]);
    }

    /**
     * @param integer $officeId
     * @return $this
     */
    public function byOfficeId($officeId = null)
    {
        if ($officeId) {
            return $this->andWhere([
                'or',
                ['=', BonusType::tableName() . '.office_id', $officeId],
                ['is', BonusType::tableName() . '.office_id', null]
            ]);
        }
        return $this;
    }
}
