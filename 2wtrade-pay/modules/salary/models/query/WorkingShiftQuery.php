<?php
namespace app\modules\salary\models\query;

use app\components\db\ActiveQuery;
use app\modules\salary\models\WorkingShift;

/**
 * Class WorkingShiftQuery
 * @package app\modules\salary\models\query
 * @method WorkingShift one($db = null)
 * @method WorkingShift[] all($db = null)
 */
class WorkingShiftQuery extends ActiveQuery
{

    /**
     * @param integer $officeId
     * @return $this
     */
    public function byOfficeId($officeId)
    {
        return $this->andWhere(['office_id' => $officeId]);
    }

    /**
     * @return $this
     */
    public function isDefault() {
        return $this->andWhere(['default' => 1]);
    }

    /**
     * @param integer $id
     * @return $this
     */
    public function byId($id)
    {
        return $this->andWhere(['id' => $id]);
    }

    /**
     * @return $this
     */
    public function isNotDefault() {
        return $this->andWhere(['default' => 0]);
    }
}
