<?php
namespace app\modules\salary\models\query;

use app\components\db\ActiveQuery;
use app\modules\salary\models\OfficeExpense;

/**
 * Class OfficeExpenseQuery
 * @package app\modules\salary\models\query
 * @method OfficeExpense one($db = null)
 * @method OfficeExpense[] all($db = null)
 */
class OfficeExpenseQuery extends ActiveQuery
{
    /**
     * @param integer $officeId
     * @return $this
     */
    public function byOfficeId($officeId)
    {
        return $this->andWhere(['office_id' => $officeId]);
    }

    /**
     * @param $type
     * @return $this
     */
    public function byType($type)
    {
        return $this->andWhere(['type' => $type]);
    }

    /**
     * @param $dateFrom
     * @param $dateTo
     * @return $this
     */
    public function byDates($dateFrom, $dateTo)
    {
        $firstCaseCondition = OfficeExpense::tableName() . '.date_from IS NOT NULL AND ' . OfficeExpense::tableName() . ".date_to IS NOT NULL";
        $secondCaseCondition = OfficeExpense::tableName() . '.date_from IS NOT NULL AND ' . OfficeExpense::tableName() . ".date_to IS NULL";
        $thirdCaseCondition = OfficeExpense::tableName() . '.date_from IS NULL AND ' . OfficeExpense::tableName() . ".date_to IS NOT NULL";

        $firstCaseCheck = ($dateFrom ? "'{$dateFrom}'" : 'NULL') . " >= " . OfficeExpense::tableName() . ".date_from AND " . ($dateTo ? "'{$dateTo}'" : 'NULL') . " <= " . OfficeExpense::tableName() . ".date_to";
        $secondCaseCheck = ($dateFrom ? "'{$dateFrom}'" : 'NULL') . " >= " . OfficeExpense::tableName() . ".date_from";
        $thirdCaseCheck = ($dateTo ? "'{$dateTo}'" : 'NULL') . " <= " . OfficeExpense::tableName() . ".date_to";

        return $this->andOnCondition("CASE WHEN {$firstCaseCondition} THEN {$firstCaseCheck} WHEN {$secondCaseCondition} THEN {$secondCaseCheck} WHEN {$thirdCaseCondition} THEN {$thirdCaseCheck} ELSE 1 END");
    }
}
