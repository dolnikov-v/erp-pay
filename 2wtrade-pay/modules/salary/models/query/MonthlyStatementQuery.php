<?php
namespace app\modules\salary\models\query;

use app\components\db\ActiveQuery;
use app\modules\callcenter\models\CallCenter;
use app\modules\salary\models\MonthlyStatement;
use app\models\User;
use app\modules\salary\models\Office;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class MonthlyStatementQuery
 * @package app\modules\salary\models\query
 * @method MonthlyStatement one($db = null)
 * @method MonthlyStatement[] all($db = null)
 */
class MonthlyStatementQuery extends ActiveQuery
{
    /**
     * @param null $language
     * @return array
     */
    public function collection($language = null)
    {
        $collection = [];
        foreach ($this->asArray()->all() as $model) {
            $collection[$model['id']] = Yii::$app->formatter->asDate($model['date_from']) . ' - ' . Yii::$app->formatter->asDate($model['date_to']) .
                ($model['closed_at'] ? ' ' . Yii::t('common', 'закрыт') : '');
        }
        return $collection;
    }

    /**
     * @return array
     */
    public function list()
    {
        $collection = [];
        foreach ($this->asArray()->all() as $model) {
            $collection[] = [
                'id' => $model['id'],
                'name' => Yii::$app->formatter->asDate($model['date_from']) . ' - ' . Yii::$app->formatter->asDate($model['date_to']) .
                    ($model['closed_at'] ? ' ' . Yii::t('common', 'закрыт') : '')
            ];
        }
        return $collection;
    }

    /**
     * @return $this
     */
    public function active()
    {
        return $this->andWhere([MonthlyStatement::tableName() . '.active' => 1]);
    }

    /**
     * @return $this
     */
    public function closed()
    {
        return $this->andWhere(['is not', MonthlyStatement::tableName() . '.closed_at', null]);
    }

    /**
     * @return $this
     */
    public function notSent()
    {
        return $this->andWhere([MonthlyStatement::tableName() . '.sent_at' => null]);
    }

    /**
     * @param integer $id
     * @return $this
     */
    public function byId($id = null)
    {
        if ($id) {
            return $this->andWhere(['id' => $id]);
        }
        return $this;
    }

    /**
     * @param integer $officeId
     * @return $this
     */
    public function byOfficeId($officeId = null)
    {
        if ($officeId) {
            return $this->andWhere(['office_id' => $officeId]);
        }
        return $this;
    }

    /**
     * @return $this
     */
    public function bySystemUserCountries()
    {
        if (
            Yii::$app->user->can('limit_call_center_persons_by_office') ||
            Yii::$app->user->can('limit_salary_persons_by_storage')
        ) {
            $officeIds = array_keys(Office::find()->bySystemUserCountries()->collection());
            return $this->byOfficeId($officeIds);
        } else if (Yii::$app->user->can('limit_call_center_persons_by_direction')) {
            $relatedOffices = ArrayHelper::getColumn(CallCenter::find()
                ->bySystemUserCountries()
                ->joinWith('callCenterToOffices')
                ->asArray()
                ->all(), 'callCenterToOffices');
            $officeIds = [];
            foreach ($relatedOffices as $tmp) {
                foreach ($tmp as $related) {
                    $officeIds[$related['office_id']] = $related['office_id'];
                }
            }
            if (!$officeIds) {
                $officeIds[] = -1;
            }
            return $this->andWhere(['office_id' => $officeIds]);
        }
        return $this;
    }
}
