<?php
namespace app\modules\salary\models\query;

use app\components\db\ActiveQuery;
use app\modules\salary\models\PersonGreetingCard;

/**
 * Class PersonGreetingCardQuery
 * @package app\modules\salary\models\query
 * @method PersonGreetingCard one($db = null)
 * @method PersonGreetingCard[] all($db = null)
 */
class PersonGreetingCardQuery extends ActiveQuery
{
    /**
     * @return $this
     */
    public function pending()
    {
        return $this->andWhere([PersonGreetingCard::tableName() . '.status' => PersonGreetingCard::STATUS_PENDING]);
    }
}
