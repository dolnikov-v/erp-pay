<?php
namespace app\modules\salary\models\query;

use app\components\db\ActiveQuery;
use app\modules\salary\models\Designation;
use app\modules\salary\models\Office;
use app\modules\salary\models\Person;
use app\modules\salary\models\PersonLink;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * Class PersonQuery
 * @package app\modules\salary\models\query
 * @method Person one($db = null)
 * @method Person[] all($db = null)
 */
class PersonQuery extends ActiveQuery
{
    /**
     * @return $this
     */
    public function active()
    {
        return $this->andWhere([Person::tableName() . '.active' => 1]);
    }

    /**
     * @return $this
     */
    public function notActive()
    {
        return $this->andWhere(['active' => 0]);
    }

    /**
     * @param integer $officeId
     * @return $this
     */
    public function byOfficeId($officeId)
    {
        if ($officeId) {
            return $this->andWhere([Person::tableName() . '.office_id' => $officeId]);
        } else {
            return $this;
        }
    }

    /**
     * @param integer $designationId
     * @return $this
     */
    public function byDesignationId($designationId)
    {
        if ($designationId) {
            return $this->andWhere([Person::tableName() . '.designation_id' => $designationId]);
        } else {
            return $this;
        }
    }

    /**
     * @param array $designationName
     * @return $this
     */
    public function byDesignationName($designationName)
    {
        return $this->andWhere([
            Person::tableName() . '.designation_id' => (new Query())->select('id')
                ->from(Designation::tableName())
                ->where(['name' => $designationName])
        ]);
    }

    /**
     * @return $this
     */
    public function isTeamLead()
    {
        $designationIds = Designation::find()->where([Designation::tableName() . '.team_lead' => 1])->asArray()->all();
        return $this->andWhere([Person::tableName() . '.designation_id' => ArrayHelper::getColumn($designationIds, 'id')]);
    }

    /**
     * @return $this
     */
    public function isTeamLeadOrSupervisor()
    {
        $designationIds = Designation::find()
            ->where([Designation::tableName() . '.supervisor' => 1])
            ->orWhere([Designation::tableName() . '.team_lead' => 1])
            ->asArray()
            ->all();
        return $this->andWhere([Person::tableName() . '.designation_id' => ArrayHelper::getColumn($designationIds, 'id')]);
    }

    /**
     * @return $this
     */
    public function isOperator()
    {
        $designationIds = Designation::find()->where([Designation::tableName() . '.calc_work_time' => 1])->asArray()->all();
        return $this->andWhere([Person::tableName() . '.designation_id' => ArrayHelper::getColumn($designationIds, 'id')]);
    }


    /**
     * @return $this
     */
    public function bySystemUserCountries()
    {
        $officeIds = array_keys(Office::find()->bySystemUserCountries()->collection());
        return $this->andWhere(['office_id' => $officeIds]);
    }

    /**
     * @return array
     */
    public function list()
    {
        return $this->select(['id', 'name'])
            ->asArray()
            ->all();
    }

    /**
     * @param integer|[] $id
     * @return $this
     */
    public function byId($id)
    {
        return $this->andWhere([Person::tableName() . '.id' => $id]);
    }

    /**
     * Return array of crocotime_employee_id indexed by crocotime_employee_id
     * @return array
     */
    public function crocotimeIdList()
    {
        return ArrayHelper::map((PersonLink::find()->isCrocoTime()->all()), 'foreign_id', 'foreign_id');
    }

    /**
     * @param $id
     * @return $this
     */
    public function byCrocotimeId($id)
    {
        $this->joinWith('personLinks');
        return $this->andWhere([PersonLink::tableName() . '.foreign_id' => $id])
            ->andWhere([PersonLink::tableName() . '.type' => PersonLink::TYPE_CROCOTIME]);
    }

    /**
     * @param integer $workingShiftId
     * @return $this
     */
    public function byWorkingShift($workingShiftId)
    {
        if ($workingShiftId) {
            return $this->andWhere([Person::tableName() . '.working_shift_id' => $workingShiftId]);
        } else {
            return $this;
        }
    }
}
