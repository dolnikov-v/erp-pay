<?php
namespace app\modules\salary\models\query;

use app\components\db\ActiveQuery;
use app\modules\salary\models\Designation;
use app\modules\salary\models\Staffing;
use yii\helpers\ArrayHelper;

/**
 * Class StaffingQuery
 * @package app\modules\salary\models\query
 * @method Staffing one($db = null)
 * @method Staffing[] all($db = null)
 */
class StaffingQuery extends ActiveQuery
{
    /**
     * @var string
     */
    protected $collectionKey = 'id';

    /**
     * @var string
     */
    protected $collectionValue = 'designation.name';

    /**
     * @param null $language
     * @return array
     */
    public function collection($language = null)
    {
        $collection = ArrayHelper::map($this->all(), $this->collectionKey, $this->collectionValue);

        foreach ($collection as &$item) {
            $item = \Yii::t('common', $item);
        }

        return $collection;
    }

    /**
     * @param integer $officeId
     * @return $this
     */
    public function byOfficeId($officeId)
    {
        if ($officeId) {
            return $this->andWhere([Staffing::tableName() . '.office_id' => $officeId]);
        } else {
            return $this;
        }
    }

    /**
     * @param integer $designationId
     * @return $this
     */
    public function byDesignationId($designationId)
    {
        if ($designationId) {
            return $this->andWhere([Staffing::tableName() . '.designation_id' => $designationId]);
        } else {
            return $this;
        }
    }

    /*
     * @return $this
     */
    public function isOperator()
    {
        return $this->andWhere([Designation::tableName() . ".calc_work_time" => 1]);
    }
}
