<?php
namespace app\modules\salary\models\query;

use app\components\db\ActiveQuery;
use app\modules\salary\models\MonthlyStatementData;

/**
 * Class MonthlyStatementDataQuery
 * @package app\modules\salary\models\query
 * @method MonthlyStatementData one($db = null)
 * @method MonthlyStatementData[] all($db = null)
 */
class MonthlyStatementDataQuery extends ActiveQuery
{

    /**
     * @param $id
     * @return $this
     */
    public function byStatement($id)
    {
        return $this->andWhere([MonthlyStatementData::tableName() . '.statement_id' => $id]);
    }

    /**
     * @return $this
     */
    public function notSent()
    {
        return $this->andWhere([MonthlyStatementData::tableName() . '.sent_at' => null]);
    }

    /**
     * @param $id
     * @return $this
     */
    public function byPerson($id)
    {
        return $this->andWhere([MonthlyStatementData::tableName() . '.person_id' => $id]);
    }

    /**
     * @param $file
     * @return $this
     */
    public function byFile($file)
    {
        return $this->andWhere([MonthlyStatementData::tableName() . '.salary_sheet_file' => $file]);
    }

    /**
     * @return $this
     */
    public function withFile()
    {
        return $this->andWhere(['is not', MonthlyStatementData::tableName() . '.salary_sheet_file', null]);
    }
}
