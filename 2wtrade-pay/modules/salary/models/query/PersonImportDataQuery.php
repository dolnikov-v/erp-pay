<?php
namespace app\modules\salary\models\query;

use app\components\db\ActiveQuery;
use app\modules\salary\models\PersonImportData;

/**
 * Class PersonImportDataQuery
 * @package app\modules\salary\models\query
 * @method PersonImportData one($db = null)
 * @method PersonImportData[] all($db = null)
 */
class PersonImportDataQuery extends ActiveQuery
{

}
