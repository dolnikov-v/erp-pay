<?php

namespace app\modules\salary\models\query;

use app\components\db\ActiveQuery;
use app\modules\salary\models\Designation;
use app\modules\salary\models\Person;
use yii\db\Query;

/**
 * Class DesignationQuery
 * @package app\modules\salary\models\query
 * @method Designation one($db = null)
 * @method Designation[] all($db = null)
 */
class DesignationQuery extends ActiveQuery
{
    /**
     * @return $this
     */
    public function active()
    {
        return $this->andWhere([Designation::tableName() . '.active' => 1]);
    }

    /**
     * @param integer $officeId
     * @return $this
     */
    public function byOfficeId($officeId)
    {
        $designationIds = (new Query())->select('designation_id')
            ->from(Person::tableName())
            ->where(
                [
                    'office_id' => $officeId,
                    'active' => 1
                ])
            ->distinct()->column();
        return $this->andWhere([
            Designation::tableName() . '.id' => $designationIds
        ]);
    }
}
