<?php
namespace app\modules\salary\models\query;

use app\components\db\ActiveQuery;
use app\modules\salary\models\WorkTimePlan;

/**
 * Class WorkTimePlanQuery
 * @package app\modules\salary\models\query
 * @method WorkTimePlan one($db = null)
 * @method WorkTimePlan[] all($db = null)
 */
class WorkTimePlanQuery extends ActiveQuery
{
    /**
     * @param integer $personId
     * @return $this
     */
    public function byPersonId($personId)
    {
        if ($personId) {
            return $this->andWhere([WorkTimePlan::tableName() . '.person_id' => $personId]);
        } else {
            return $this;
        }
    }

    /**
     * @param string $date
     * @return $this
     */
    public function byDate($date)
    {
        if ($date) {
            return $this->andWhere([WorkTimePlan::tableName() . '.date' => $date]);
        } else {
            return $this;
        }
    }

}
