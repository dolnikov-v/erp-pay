<?php

namespace app\modules\salary\models\query;

use app\components\db\ActiveQuery;
use app\modules\salary\models\Penalty;
use app\modules\salary\models\PenaltyType;

/**
 * Class PenaltyTypeQuery
 * @package app\modules\salary\models\query
 * @method PenaltyType one($db = null)
 * @method PenaltyType[] all($db = null)
 */
class PenaltyTypeQuery extends ActiveQuery
{
    /**
     * @var string
     */
    protected $collectionKey = 'id';

    /**
     * @var string
     */
    protected $collectionValue = 'name';

    /**
     * @return $this
     */
    public function active()
    {
        return $this->andWhere([PenaltyType::tableName() . '.active' => 1]);
    }

    /**
     * @param integer $id
     * @return $this
     */
    public function byId($id)
    {
        return $this->andWhere([PenaltyType::tableName() . '.id' => $id]);
    }

    /**
     * @param integer $id
     * @return $this
     */
    public function activeOrById($id)
    {
        return $this->andWhere(
            ['or',
                [PenaltyType::tableName() . '.active' => 1],
                [PenaltyType::tableName() . '.id' => $id]
            ]
        );
    }

    /**
     * @return $this
     */
    public function notActive()
    {
        return $this->andWhere(['active' => 0]);
    }

    /**
     * @param $trigger
     * @return $this
     */
    public function byTrigger($trigger)
    {
        return $this->andWhere([PenaltyType::tableName() . '.trigger' => $trigger]);
    }

}
