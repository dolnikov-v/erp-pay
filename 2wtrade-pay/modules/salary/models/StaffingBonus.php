<?php

namespace app\modules\salary\models;

use app\components\ModelTrait;
use \yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "salary_staffing_bonus".
 *
 * @property integer $id
 * @property integer $day
 * @property integer $staffing_id
 * @property integer $bonus_type_id
 *
 * @property BonusType $bonusType
 * @property Staffing $staffing
 */
class StaffingBonus extends ActiveRecord
{
    use ModelTrait;

    const DAY_LAST = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%salary_staffing_bonus}}';
    }

    /**
     * @return array
     */
    public static function getDays()
    {
        $return[self::DAY_LAST] = Yii::t('common', 'последний день месяца');
        $i = 1;
        while ($i <= 31) {
            $return[$i] = $i;
            $i++;
        }
        return $return;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['day', 'staffing_id', 'bonus_type_id'], 'required', 'enableClientValidation' => false],
            [['day'], 'number', 'min' => 0],
            [['day'], 'number', 'max' => 31],
            [['staffing_id', 'bonus_type_id'], 'integer'],
            [['staffing_id', 'bonus_type_id', 'day'], 'unique', 'targetAttribute' => ['staffing_id', 'bonus_type_id', 'day']],
            [
                ['bonus_type_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => BonusType::className(),
                'targetAttribute' => ['bonus_type_id' => 'id']
            ],
            [
                ['staffing_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Staffing::className(),
                'targetAttribute' => ['staffing_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'day' => Yii::t('common', 'День месяца'),
            'staffing_id' => Yii::t('common', 'Должность в штатном расписании'),
            'bonus_type_id' => Yii::t('common', 'Тип бонуса'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBonusType()
    {
        return $this->hasOne(BonusType::className(), ['id' => 'bonus_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStaffing()
    {
        return $this->hasOne(Staffing::className(), ['id' => 'staffing_id']);
    }
}
