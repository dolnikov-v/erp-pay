<?php

namespace app\modules\salary\models;

use app\modules\callcenter\models\CallCenter;
use app\modules\callcenter\models\CallCenterUser;
use app\modules\salary\models\query\PersonImportDataQuery;
use Yii;
use app\components\db\ActiveRecordLogUpdateTime;
use yii\helpers\ArrayHelper;

/**
 * Class PersonImportData
 * @package app\modules\salary\models
 * @property integer $id
 * @property PersonImport $import
 * @property integer $import_id
 * @property integer $row_number
 * @property integer $user_id
 * @property string $user_login
 * @property string $person_full_name
 * @property string $person_position
 * @property string $person_salary
 * @property string $person_salary_usd
 * @property string $person_salary_hourly
 * @property string $person_salary_hourly_usd
 * @property string $person_passport
 * @property string $person_head
 * @property string $person_email
 * @property string $person_skype
 * @property string $person_phone
 * @property string $person_employment_date
 * @property string $person_termination_date
 * @property string $person_termination_reason
 * @property string $person_account_number
 * @property string $country
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $head_id
 * @property integer $working_shift_id
 * @property WorkingShift $workingShift
 * @property Person $head
 */
class PersonImportData extends ActiveRecordLogUpdateTime
{
    const STATUS_PENDING = 0;
    const STATUS_IMPORT = 1;
    const STATUS_IMPORT_ONLY_PERSON = 2;
    const STATUS_IGNORE = 3;
    const STATUS_ERROR = 4;

    const SCENARIO_CHECK_IMPORT = 'scenario_check_import';
    const SCENARIO_CHECK_IMPORT_ONLY_PERSON = 'scenario_check_import_only_person';

    /**
     * @var array
     */
    static private $teamLeads;

    public $importErrors = [];
    public $importWarning = [];

    /**
     * @var Person
     */
    public $person;

    /**
     * @var CallCenterUser
     */
    public $user;

    /**
     * @var CallCenterUser[]
     */
    public $_users;

    /**
     * @var Person[]
     */
    public $_persons;

    /**
     * @var Person
     */
    public $_officeTeamLead;

    /**
     * @var PersonImport
     */
    public $_teamLeads;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%salary_person_import_data}}';
    }

    /**
     * @return PersonImportDataQuery
     */
    public static function find()
    {
        return new PersonImportDataQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['import_id', 'row_number'], 'required'],
            [['import_id', 'row_number', 'created_at', 'updated_at', 'status', 'head_id', 'user_id', 'working_shift_id'], 'integer'],
            [
                [
                    'user_login', 'person_full_name', 'country', 'person_position', 'person_salary', 'person_salary_usd',
                    'person_passport', 'person_head', 'person_email', 'person_skype', 'person_phone', 'person_employment_date',
                    'person_termination_date', 'person_salary_hourly', 'person_salary_hourly_usd',
                ],
                'string', 'max' => 100
            ],
            [['person_termination_reason'], 'string', 'max' => 256],
            [['person_full_name'], 'required', 'on' => [self::SCENARIO_CHECK_IMPORT, self::SCENARIO_CHECK_IMPORT_ONLY_PERSON]],
            [['person_full_name'], 'validUnique', 'on' => [self::SCENARIO_CHECK_IMPORT, self::SCENARIO_CHECK_IMPORT_ONLY_PERSON]],
            ['country', 'validCountry', 'on' => [self::SCENARIO_CHECK_IMPORT, self::SCENARIO_CHECK_IMPORT_ONLY_PERSON]],
            ['person_head', 'validHead', 'skipOnEmpty' => true, 'on' => [self::SCENARIO_CHECK_IMPORT, self::SCENARIO_CHECK_IMPORT_ONLY_PERSON]],
            ['person_position', 'validPosition', 'on' => [self::SCENARIO_CHECK_IMPORT, self::SCENARIO_CHECK_IMPORT_ONLY_PERSON]],
            ['user', 'validUser', 'skipOnEmpty' => false, 'on' => [self::SCENARIO_CHECK_IMPORT]],
            [['user_login', 'user_id'], 'validUniqueUserIdentifier', 'skipOnEmpty' => true, 'on' => self::SCENARIO_CHECK_IMPORT],
            [['person_salary', 'person_salary_usd', 'person_salary_hourly', 'person_salary_hourly_usd'], 'validSalary', 'skipOnError' => true, 'skipOnEmpty' => false, 'on' => [self::SCENARIO_CHECK_IMPORT, self::SCENARIO_CHECK_IMPORT_ONLY_PERSON]],
            [['person_employment_date', 'person_termination_date'], 'validDateFormat', 'skipOnEmpty' => true, 'on' => [self::SCENARIO_CHECK_IMPORT, self::SCENARIO_CHECK_IMPORT_ONLY_PERSON]],
            ['working_shift_id', 'exist', 'targetClass' => WorkingShift::className(), 'targetAttribute' => 'id', 'filter' => ['office_id' => $this->import->office_id]],
            [['person_account_number'], 'string', 'max' => 50],
        ];
    }

    /**
     * @return array
     */
    public static function getTeamLeadList()
    {
        if (is_null(self::$teamLeads)) {
            $teams = Designation::find()
                ->where(['team_lead' => 1])
                ->asArray()
                ->all();

            self::$teamLeads = $teams ? ArrayHelper::getColumn($teams, 'id') : [];
        }

        return self::$teamLeads;
    }

    /**
     * @return bool
     */
    public function isTeamLead()
    {
        return $this->getPositionId() && in_array($this->getPositionId(), self::getTeamLeadList());
    }

    public function validSalary($attribute)
    {
        $map = [
            'cell_salary' => ['person_salary', 'salary_local'],
            'cell_salary_usd' => ['person_salary_usd', 'salary_usd'],
            'cell_salary_hourly' => ['person_salary_hourly', 'salary_hour_local'],
            'cell_salary_hourly_usd' => ['person_salary_hourly_usd', 'salary_hour_usd'],
        ];

        $fields = [];
        foreach ($map as $key => $value) {
            if ($this->import->$key) {
                $fields[$value[0]] = $value[1];
            }
        }

        if ($fields) {
            $valid = false;
            foreach ($fields as $importField => $personField) {
                if ($this->$importField || ($this->getPerson() && $this->getPerson()->$personField)) {
                    $valid = true;
                    break;
                }
            }
            if (!$valid) {
                $this->addError($attribute, Yii::t('common', 'Не указана "{attr}"', ['attr' => $this->getAttributeLabel($attribute)]));
            }
        }
    }

    public function validHead($attribute)
    {
        if (!$this->errors && !$this->head_id) {

            if ($this->person_head) {
                $count = count($this->getTeamLeads());
                if ($count > 1) {
                    $this->addError($attribute, Yii::t('common', 'В файле присутствует несколько тимлидов с указаным именем'));
                } elseif (!$count && !$this->getOfficeTeamLead()) {
                    $this->addError($attribute, Yii::t('common', 'Не удалось найти тимлида c указаным именем'));
                }
            } else {
                $this->addError($attribute, Yii::t('common', 'Не указан тимлид'));
            }
        }
    }

    public function validUser($attribute)
    {
        if (!$this->errors && $this->hasUserIdentifier() && !$this->getUser()) {
            if ($this->getUsers()) {
                $this->addError($attribute, Yii::t('common', 'Найдено больше одного пользователя'));
            } else {
                $this->addError($attribute, Yii::t('common', 'Не удалось определить пользователя'));
            }
        }
    }

    public function validPosition($attribute)
    {
        if ($this->import->cell_position) {
            if (!$this->person_position && ($this->getPerson()) && !$this->getPerson()->designation_id) {
                $this->addError($attribute, Yii::t('common', 'Не указана должность'));
            } elseif (!$this->getPositionId()) {
                $this->addError($attribute, Yii::t('common', 'Не удалось определить должность'));
            }
        }
    }

    public function validCountry($attribute)
    {
        if ($this->import->cell_country && $this->hasUserIdentifier()) {
            if (!$this->country) {
                $this->addError($attribute, Yii::t('common', 'Не указана страна'));
            } elseif (!$this->getCountryId()) {
                $this->addError($attribute, Yii::t('common', 'Не удалось определить страну'));
            }
        }
    }

    public function validUniqueUserIdentifier($attribute)
    {
        $exists = self::find()
            ->andWhere(['import_id' => $this->import_id])
            ->andWhere([$attribute => $this->$attribute])
            ->andWhere(['country' => $this->country])
            ->andWhere(['not', ['status' => self::STATUS_IGNORE]])
            ->andWhere(['not', ['id' => $this->id]])
            ->exists();

        if ($exists) {
            $this->addError($attribute, Yii::t('common', 'Не уникальное значение "{attr}"', ['attr' => $this->getAttributeLabel($attribute)]));
        }
    }

    public function validUnique($attribute)
    {
        $exists = self::find()
            ->andWhere(['import_id' => $this->import_id])
            ->andWhere([$attribute => $this->$attribute])
            ->andWhere(['not', ['status' => self::STATUS_IGNORE]])
            ->andWhere(['not', ['id' => $this->id]])
            ->exists();

        if ($exists) {
            $this->addError($attribute, Yii::t('common', 'Не уникальное значение "{attr}"', ['attr' => $this->getAttributeLabel($attribute)]));
        }
    }

    public function validDateFormat($attribute)
    {
        $time = strtotime($this->$attribute);
        if (!$time || $this->$attribute != date('Y-m-d', $time)) {
            $this->addError($attribute, Yii::t('common', 'Неправильный формат даты: "{attr}"', ['attr' => $this->getAttributeLabel($attribute)]));
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'row_number' => Yii::t('common', 'Номер строки'),
            'user_login' => Yii::t('common', 'Логин'),
            'user_id' => Yii::t('common', 'Номер пользователя'),
            'person_full_name' => Yii::t('common', 'ФИО'),
            'person_country' => Yii::t('common', 'Страна'),
            'person_position' => Yii::t('common', 'Должность'),
            'person_salary' => Yii::t('common', 'Зарплата за месяц'),
            'person_salary_usd' => Yii::t('common', 'Зарплата за месяц (USD)'),
            'person_salary_hourly' => Yii::t('common', 'Зарплата за час'),
            'person_salary_hourly_usd' => Yii::t('common', 'Зарплата за час(USD)'),
            'person_passport' => Yii::t('common', 'Паспорт'),
            'person_head' => Yii::t('common', 'Руководитель'),
            'person_employment_date' => Yii::t('common', 'Дата трудоустройства'),
            'person_termination_date' => Yii::t('common', 'Дата увольнения'),
            'person_termination_reason' => Yii::t('common', 'Причина увольнения'),
            'person_account_number' => Yii::t('common', 'Номер счета'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
            'status' =>  Yii::t('common', 'Статус'),
            'head_id' => Yii::t('common', 'Назначенный руководитель'),
            'working_shift_id' => Yii::t('common', 'Рабочая смена'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImport()
    {
        return $this->hasOne(PersonImport::className(), ['id' => 'import_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkingShift()
    {
        return $this->hasOne(WorkingShift::className(), ['id' => 'working_shift_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHead()
    {
        return $this->hasOne(Person::className(), ['id' => 'head_id']);
    }

    /**
     * @return mixed
     */
    public function getCountryId()
    {
        return ArrayHelper::getValue($this->import->getCountryMap(), $this->country);
    }

    /**
     * @return mixed
     */
    public function getPositionId()
    {
        return ArrayHelper::getValue($this->import->getPositionMap(), $this->person_position);
    }

    /**
     * @return Person[]
     */
    public function getPersons()
    {
        if (is_null($this->_persons)) {
            if ($this->import->office_id && $this->person_full_name) {
                $this->_persons = Person::find()
                    ->where([
                        'name' => $this->person_full_name,
                        'office_id' => $this->import->office_id,
                    ])
                    ->all();
            } else {
                $this->_persons = [];
            }
        }

        return $this->_persons;
    }

    /**
     * @return Person|null
     */
    public function getPerson()
    {
        $persons = $this->getPersons();
        if ($persons && count($persons) == 1) {
            return $persons[0];
        }
        return null;
    }

    /**
     * @return PersonImportData|null
     */
    public function getTeamLead()
    {
        $teamLeads = $this->getTeamLeads();
        if ($teamLeads && count($teamLeads) == 1) {
            return $teamLeads[0];
        }
        return null;
    }

    /**
     * @return PersonImportData[]
     */
    public function getTeamLeads()
    {
        if (is_null($this->_teamLeads) && $this->person_head) {
            $this->_teamLeads = [];

            $records = self::find()
                ->where([
                    'person_full_name' => $this->person_head,
                    'import_id' => $this->import_id,
                ])
                ->andWhere(['not', ['status' => self::STATUS_IGNORE]])
                ->all();

            if ($records) {
                foreach ($records as $record) {
                    if ($record->isTeamLead()) {
                        $this->_teamLeads[] = $record;
                    }
                }
            }
        }

        return $this->_teamLeads;
    }

    /**
     * @return Person|false
     */
    public function getOfficeTeamLead()
    {
        if (is_null($this->_officeTeamLead) && $this->person_head && $this->import->office_id && !$this->getTeamLeads()) {
            $this->_officeTeamLead = Person::find()
                ->where(['name' => $this->person_head])
                ->byOfficeId($this->import->office_id)
                ->isTeamLead()
                ->one() ?: false;
        }

        return $this->_officeTeamLead;
    }

    /**
     * @return CallCenterUser|null
     */
    public function getUser()
    {
        $users = $this->getUsers();
        if ($users && count($users) == 1) {
            return $users[0];
        }
        return null;
    }

    /**
     * @return CallCenterUser[]
     */
    public function getUsers()
    {
        if (is_null($this->_users)) {
            if ($this->getCountryId() && $this->hasUserIdentifier()) {

                if (($this->import->cell_login && $this->user_login) && $this->user_id) {
                    $condition = ['or',
                        [CallCenterUser::tableName() . '.user_login' => $this->user_login],
                        [CallCenterUser::tableName() . '.user_id' => $this->user_id],
                    ];
                } elseif($this->import->cell_login && $this->user_login) {
                    $condition = [CallCenterUser::tableName() . '.user_login' => $this->user_login];
                } else {
                    $condition = [CallCenterUser::tableName() . '.user_id' => $this->user_id];
                }

                $this->_users = CallCenterUser::find()
                    ->joinWith('callCenter')
                    ->where([CallCenter::tableName() . '.country_id' => $this->getCountryId()])
                    ->andWhere($condition)
                    ->active()
                    ->all();
            } else {
                $this->_users = [];
            }
        }

        return $this->_users;
    }

    public function hasUserIdentifier()
    {
        return (($this->import->cell_login && ($this->user_login || $this->user_id)) || $this->import->cell_id && $this->user_id);
    }

    public function setScenarioForCheckImport()
    {
        $this->scenario = $this->hasUserIdentifier() ? self::SCENARIO_CHECK_IMPORT : self::SCENARIO_CHECK_IMPORT_ONLY_PERSON;
    }

    public function afterValidate()
    {
        if (in_array($this->scenario, [self::SCENARIO_CHECK_IMPORT, self::SCENARIO_CHECK_IMPORT_ONLY_PERSON])) {
            if (!$this->hasErrors() && !$this->getPerson() && !($this->person_position && ($this->person_salary_usd || $this->person_salary || $this->person_salary_hourly || $this->person_salary_hourly_usd))) {
                $this->addError(null, Yii::t('common', 'Для импорта нового сотрудника, должны быть указаны поля: «ФИО», «Должность», «Зарплата»'));
            }
            if ($this->hasErrors()) {
                $this->importErrors = $this->getFirstErrors();
            }
        }

        parent::afterValidate(); // TODO: Change the autogenerated stub
    }

}