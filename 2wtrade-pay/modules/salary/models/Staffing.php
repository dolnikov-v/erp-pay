<?php

namespace app\modules\salary\models;

use app\models\logs\LinkData;
use app\modules\salary\models\query\StaffingQuery;
use Yii;
use app\components\db\ActiveRecordLogUpdateTime;

/**
 * This is the model class for table "salary_staffing".
 *
 * @property integer $id
 * @property integer $office_id
 * @property integer $designation_id
 * @property integer $max_persons
 * @property integer $min_jobs
 * @property double $salary_usd
 * @property double $salary_local
 * @property string $comment
 * @property string $payment_type
 * @property string $percent_base
 * @property integer $base_staffing_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Designation $designation
 * @property Staffing $baseStaffing
 * @property Office $office
 * @property Person[] $people
 * @property StaffingBonus[] $salaryStaffingBonuses
 * @property BonusType[] $bonusTypes
 */
class Staffing extends ActiveRecordLogUpdateTime
{
    protected $insertLog = true;
    protected $updateLog = true;
    protected $deleteLog = true;

    /**
     * @return array|null
     */
    protected function getLinkData(): ?array
    {
        return [
            new LinkData(['field' => 'office_id', 'value' => (int)$this->office_id]),
        ];
    }

    const PAYMENT_TYPE_FIXED = 'fixed';
    const PAYMENT_TYPE_PERCENT = 'percent';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%salary_staffing}}';
    }

    /**
     * @return array
     */
    public static function getPaymentTypes()
    {
        return [
            self::PAYMENT_TYPE_FIXED => Yii::t('common', 'Фиксированный'),
            self::PAYMENT_TYPE_PERCENT => Yii::t('common', '% от базового'),
        ];
    }

    /**
     * @return StaffingQuery
     */
    public static function find()
    {
        return new StaffingQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['office_id', 'designation_id'], 'required'],
            [
                [
                    'office_id',
                    'designation_id',
                    'max_persons',
                    'min_jobs',
                    'base_staffing_id',
                    'created_at',
                    'updated_at'
                ],
                'integer'
            ],
            [['comment'], 'string', 'max' => 255],
            [['payment_type'], 'string', 'max' => 10],
            [
                ['designation_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Designation::className(),
                'targetAttribute' => ['designation_id' => 'id']
            ],
            [
                ['office_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Office::className(),
                'targetAttribute' => ['office_id' => 'id']
            ],
            [['office_id', 'designation_id'], 'unique', 'targetAttribute' => ['office_id', 'designation_id']],
            [['salary_usd', 'salary_local', 'percent_base'], 'number'],
            [
                ['base_staffing_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => self::className(),
                'targetAttribute' => ['base_staffing_id' => 'id']
            ],
            [
                ['base_staffing_id', 'percent_base'],
                'required',
                'when' => function ($model) {
                    return
                        (
                            $model->payment_type == self::PAYMENT_TYPE_PERCENT
                        );
                },
                'whenClient' => "function (attribute, value) {
                    return ($('#staffing-payment_type').val() == 'percent');
                }"
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'office_id' => Yii::t('common', 'Офис'),
            'designation_id' => Yii::t('common', 'Должность'),
            'max_persons' => Yii::t('common', 'Максимальное количество человек'),
            'min_jobs' => Yii::t('common', 'Минимальное количество рабочих мест '),
            'salary_usd' => Yii::t('common', 'Оклад в USD'),
            'salary_local' => Yii::t('common', 'Оклад в местной валюте'),
            'comment' => Yii::t('common', 'Комментарий'),
            'payment_type' => Yii::t('common', 'Тип оплаты'),
            'percent_base' => Yii::t('common', '% от оклада базовой должности'),
            'base_staffing_id' => Yii::t('common', 'Базовая должность'),
            'created_at' => Yii::t('common', 'Дата добавления'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDesignation()
    {
        return $this->hasOne(Designation::className(), ['id' => 'designation_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOffice()
    {
        return $this->hasOne(Office::className(), ['id' => 'office_id']);
    }

    /**
     * @return Person[]
     */
    public function getPeople()
    {
        return Person::find()->byOfficeId($this->office_id)->byDesignationId($this->designation_id)->active()->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBaseStaffing()
    {
        return $this->hasOne(self::className(), ['id' => 'base_staffing_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalaryStaffingBonuses()
    {
        return $this->hasMany(StaffingBonus::className(), ['staffing_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBonusTypes()
    {
        return $this->hasMany(BonusType::className(), ['id' => 'bonus_type_id'])
            ->viaTable('salary_staffing_bonus', ['staffing_id' => 'id']);
    }

    public function beforeSave($insert)
    {
        if ($this->payment_type == self::PAYMENT_TYPE_PERCENT) {

            $this->salary_usd = $this->salary_local = 0;
            if ($this->base_staffing_id && $this->baseStaffing instanceof Staffing) {
                $this->salary_usd = $this->baseStaffing->salary_usd * $this->percent_base / 100;
                $this->salary_local = $this->baseStaffing->salary_local * $this->percent_base / 100;
            }
        }
        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    /**
     * @param array
     * @param StaffingBonus[] $currentStaffingBonuses
     * @return boolean
     */
    public function saveStaffingBonusFromPost($post, $currentStaffingBonuses)
    {

        $multiData = [];
        if ($post) {
            foreach ($post as $k => $bonusData) {
                foreach ($bonusData as $num => $data) {
                    $multiData[$num][$k] = $data;
                }
            }
        }

        $saved = [];
        $error = false;
        foreach ($multiData as $k => $data) {
            $id = $data['id'];

            $model = null;

            if ($id) {
                foreach ($currentStaffingBonuses as $item) {
                    if ($item->id == $id) {
                        $model = $item;
                        break;
                    }
                }
            } else {
                $model = new StaffingBonus();
                $model->staffing_id = $this->id;
            }

            if (empty($data['bonus_type_id'])) {
                continue;
            }

            $model->bonus_type_id = $data['bonus_type_id'];
            $model->day = $data['day'];

            if ($model->save()) {
                $saved[] = $model->id;
            } else {
                $this->addError('bonusTypes', $model->getFirstErrorAsString());
                $error = true;
                break;
            }
        }

        if (!$error) {
            foreach ($currentStaffingBonuses as $currentStaffingBonus) {
                if (!in_array($currentStaffingBonus->id, $saved)) {
                    $currentStaffingBonus->delete();
                }
            }
        }

        return $this->hasErrors() ? false : true;
    }
}
