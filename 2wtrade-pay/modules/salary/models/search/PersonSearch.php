<?php
namespace app\modules\salary\models\search;

use app\modules\salary\models\Office;
use app\modules\salary\models\Person;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\callcenter\models\CallCenterUser;
use MongoDB\Driver\Query;
use yii\data\ActiveDataProvider;
use Yii;

/**
 * Class PersonSearch
 * @package app\modules\salary\models\search
 */
class PersonSearch extends Person
{
    const ALL = -1;
    const ACTIVE_WITH_DATE = 3;
    const NOT_ACTIVE_WITHOUT_DATE = 4;

    const NO_TEAMLEAD = -1;

    public $user_login;
    public $user_id;
    public $types_approved = [];
    public $types_active = [];
    public $approvedSearch = self::ALL;
    public $activeSearch = self::ALL;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->types_approved = [
            self::ALL => Yii::t('common', 'Все'),
            self::ACTIVE => Yii::t('common', 'Только подтвержденные'),
            self::NOT_ACTIVE => Yii::t('common', 'Только не подтвержденные'),
        ];

        $this->types_active = [
            self::ALL => Yii::t('common', 'Все'),
            self::ACTIVE => Yii::t('common', 'Только активные'),
            self::NOT_ACTIVE => Yii::t('common', 'Только не активные'),
            self::ACTIVE_WITH_DATE => Yii::t('common', 'Активные, дата увольнения не пустая'),
            self::NOT_ACTIVE_WITHOUT_DATE => Yii::t('common', 'Не активные, дата увольнения пустая'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'office_id', 'designation_id', 'parent_id', 'user_login', 'user_id', 'approvedSearch', 'activeSearch'], 'safe']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['active'] = SORT_DESC;
            $sort['parent_id'] = SORT_ASC;
            $sort['salary_local'] = SORT_DESC;
            $sort['salary_usd'] = SORT_DESC;
            $sort['name'] = SORT_ASC;
        }

        $query = Person::find()
            ->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $condition = $this->prepareConditionsByLines('name');
        if ($condition) {
            $query->andFilterWhere($condition);
        }

        $conditionLogin = $this->prepareConditionsByLines('user_login');
        $conditionId = $this->prepareConditionsByLines('user_login');

        if ($conditionLogin || $conditionId) {
            $query->joinWith('callCenterUsers');
            $query->groupBy(Person::tableName() . '.id');
        }
        if ($conditionLogin) {
            $query->andFilterWhere($conditionLogin);
        }
        if ($conditionId) {
            $query->andFilterWhere($conditionId);
        }

        $query->andFilterWhere(['=', 'office_id', $this->office_id]);

        $officeIds = array_keys(Office::find()->bySystemUserCountries()->collection());
        if (sizeof($officeIds) == 1) {
            $this->office_id = $officeIds[0];
        }
        if (!$officeIds) {
            $officeIds[] = -1;
        }

        $query->andFilterWhere([Person::tableName() . '.office_id' => $officeIds]);

        if ($this->parent_id == self::NO_TEAMLEAD) {

            $subQuery = Person::find()
                ->select(['id'])
                ->byOfficeId($this->office_id)
                ->isTeamLead()
                ->active();

            $query->andWhere([
                'or',
                ['is', Person::tableName() . '.parent_id', null],
                ['not in', Person::tableName() . '.parent_id', $subQuery],
            ]);
        } else {
            $query->andFilterWhere(['=', Person::tableName() . '.parent_id', $this->parent_id]);
        }
        $query->andFilterWhere(['=', Person::tableName() . '.designation_id', $this->designation_id]);
        if ($this->approvedSearch == self::ACTIVE) {
            $query->andWhere([Person::tableName() . '.approved' => self::ACTIVE]);
        }
        if ($this->approvedSearch == self::NOT_ACTIVE) {
            $query->andWhere([Person::tableName() . '.approved' => self::NOT_ACTIVE]);
        }

        switch ($this->activeSearch) {
            case self::ACTIVE:
                $query->andWhere([Person::tableName() . '.active' => self::ACTIVE]);
                break;
            case self::NOT_ACTIVE:
                $query->andWhere([Person::tableName() . '.active' => self::NOT_ACTIVE]);
                break;
            case self::ACTIVE_WITH_DATE:
                $query->andWhere([Person::tableName() . '.active' => self::ACTIVE]);
                $query->andWhere(['is not', Person::tableName() . '.dismissal_date', null]);
                break;
            case self::NOT_ACTIVE_WITHOUT_DATE:
                $query->andWhere([Person::tableName() . '.active' => self::NOT_ACTIVE]);
                $query->andWhere(['is', Person::tableName() . '.dismissal_date', null]);
                break;
        }

        return $dataProvider;
    }


    /**
     * @param $property string
     * @return array|false
     */
    public function prepareConditionsByLines($property)
    {
        if (!isset($this->$property)) {
            return false;
        }

        $lines = $this->$property;

        $names = preg_split('/$\R?^/m', $lines);
        if ($names) {
            $orName = [];
            $orName[] = 'or';
            foreach ($names as $name) {
                if (trim($name) != "") {
                    $words = explode(' ', trim($name));
                    $tmp = [];
                    $tmp[] = 'and';
                    foreach ($words as $word) {
                        $tmp[] = ['like', $property, $word];
                    }
                    $orName[] = $tmp;
                }
            }
            return $orName;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return
            parent::attributeLabels() +
            [
                'user_login' => Yii::t('common', 'Логин пользователя в КЦ'),
                'user_id' => Yii::t('common', 'Идентификатор пользователя в КЦ'),
                'approvedSearch' => Yii::t('common', 'Подтвержденные'),
                'activeSearch' => Yii::t('common', 'Активность'),
            ];
    }


    /**
     * @param $orderId
     * @return Person|null
     */
    public static function findPersonByOrderId($orderId)
    {
        $callCenterRequest = CallCenterRequest::find()->byOrderId($orderId)->one();
        if (!$callCenterRequest) {
            return null;
        }
        $model = self::find()->joinWith('callCenterUsers')
            ->where([
                CallCenterUser::tableName() . '.user_id' => $callCenterRequest->last_foreign_operator,
                CallCenterUser::tableName() . '.callcenter_id' => $callCenterRequest->call_center_id
            ])->one();
        return $model;
    }
}