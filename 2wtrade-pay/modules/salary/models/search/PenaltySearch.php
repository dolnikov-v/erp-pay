<?php

namespace app\modules\salary\models\search;

use app\modules\salary\models\Office;
use app\modules\salary\models\Penalty;
use app\modules\salary\models\Person;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * Class PenaltySearch
 * @package app\modules\salary\models\search
 */
class PenaltySearch extends Penalty
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['person', 'person_id', 'office_id'], 'safe']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['id'] = SORT_DESC;
        }

        $query = Penalty::find()
            ->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
        $officeIds = array_keys(Office::find()->bySystemUserCountries()->collection());

        $query->joinWith('person');
        $query->andFilterWhere(['like', Person::tableName() . '.name', $this->person_id]);
        $query->andFilterWhere(['in', Person::tableName() . '.office_id', $officeIds]);
        $query->andFilterWhere(['=', Person::tableName() . '.office_id', $this->office_id]);
        $query->with(['order.country', 'person.office', 'penaltyType']);
        return $dataProvider;
    }
}
