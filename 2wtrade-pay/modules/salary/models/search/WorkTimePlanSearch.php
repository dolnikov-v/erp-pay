<?php
namespace app\modules\salary\models\search;

use app\modules\salary\models\Office;
use app\modules\salary\models\Person;
use app\modules\salary\models\WorkTimePlan;
use yii\data\ActiveDataProvider;

/**
 * Class WorkTimePlanSearch
 * @package app\modules\salary\models\search
 */
class WorkTimePlanSearch extends WorkTimePlan
{
    public $office_id;
    public $dateFrom;
    public $dateTo;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['person_id', 'office_id', 'dateFrom', 'dateTo'], 'safe']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['id'] = SORT_DESC;
        }

        $query = WorkTimePlan::find()
            ->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([WorkTimePlan::tableName() . '.person_id' => $this->person_id]);
        $query->joinWith('person');
        $officeIds = array_keys(Office::find()->bySystemUserCountries()->collection());
        $query->andFilterWhere(['in', Person::tableName() . '.office_id', $officeIds]);
        $query->andFilterWhere(['=', Person::tableName() . '.office_id', $this->office_id]);

        if ($this->dateFrom && $this->dateTo) {
            $query->andFilterWhere(['between', WorkTimePlan::tableName() . '.date',
                date("Y-m-d", strtotime($this->dateFrom)),
                date("Y-m-d", strtotime($this->dateTo)),
            ]);
        }
        return $dataProvider;
    }
}
