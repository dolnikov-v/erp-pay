<?php
namespace app\modules\salary\models\search;

use app\modules\salary\models\PenaltyType;
use yii\data\ActiveDataProvider;

/**
 * Class PenaltyTypeSearch
 * @package app\modules\salary\models\search
 */
class PenaltyTypeSearch extends PenaltyType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        $sort['active'] = SORT_DESC;
        if (empty($params['sort'])) {
            $sort['id'] = SORT_ASC;
        }

        $query = PenaltyType::find()
            ->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        return $dataProvider;
    }
}
