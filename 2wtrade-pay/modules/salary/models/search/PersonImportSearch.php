<?php

namespace app\modules\salary\models\search;

use Yii;
use app\modules\salary\models\PersonImport;
use yii\data\ActiveDataProvider;

class PersonImportSearch extends PersonImport
{
    public $date_from;
    public $date_to;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_from', 'date_to'], 'string']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];
        if (!empty($params['sort'])) {
            if (substr($params['sort'], 0, 1) == '-') {
                $prop = substr($params['sort'], 1);
                $order = SORT_DESC;
            } else {
                $prop = $params['sort'];
                $order = SORT_ASC;
            }
            $sort = [$prop => $order];
        }
        if (!$sort) {
            $sort['created_at'] = SORT_DESC;
        }

        $query = PersonImport::find()
            ->orderBy($sort);

        $this->load($params);

        if ($this->validate()) {

            if ($this->date_from != null) {
                $timeFrom = Yii::$app->formatter->asTimestamp($this->date_from);
                if ($timeFrom) {
                    $query->andWhere(['>=', 'created_at', $timeFrom]);
                }
            }

            if ($this->date_to != null) {
                $timeTo = Yii::$app->formatter->asTimestamp($this->date_to);
                if ($timeTo) {
                    $query->andWhere(['<=', 'created_at', $timeTo + 86399]);
                }
            }
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }
}