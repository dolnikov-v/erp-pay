<?php
namespace app\modules\salary\models\search;

use app\modules\salary\models\MonthlyStatement;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * Class salaryMonthlyStatementSearch
 * @package app\modules\salary\models\search
 */
class MonthlyStatementSearch extends MonthlyStatement
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        $sort['active'] = SORT_DESC;
        if (empty($params['sort'])) {
            $sort['id'] = SORT_DESC;
        }

        $query = MonthlyStatement::find()
            ->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        return $dataProvider;
    }
}
