<?php
namespace app\modules\salary\models\search;

use app\modules\salary\models\Designation;
use app\modules\salary\models\Office;
use app\modules\salary\models\Person;
use app\modules\salary\models\Staffing;
use yii\data\ActiveDataProvider;
use Yii;
use yii\data\ArrayDataProvider;
use yii\db\Expression;
use yii\db\Query;

/**
 * Class StaffingSearch
 * @package app\modules\salary\models\search
 */
class StaffingSearch extends Staffing
{

    public $type;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['office_id', 'type'], 'safe']
        ];
    }

    /**
     * @param array $params
     * @return ArrayDataProvider
     */
    public function search($params = [])
    {
        $this->load($params);
        if (!$this->office_id) {
            return new ArrayDataProvider([
                'allModels' => []
            ]);
        }

        $subQueryStaffing = Staffing::find()
            ->select([
                'staffing_id' => 'id',
                'office_id',
                'designation_id',
                'fact' => new Expression('0'),
                'norm' => 'max_persons',
                'jobs' => 'min_jobs',
                'salaryusd' => 'salary_usd',
                'salarylocal' => 'salary_local',
                'description' => 'comment',
                'payment_type' => 'payment_type',
                'percent_base' => 'percent_base',
                'base_staffing_id' => 'base_staffing_id',
            ])
            ->groupBy([
                'office_id',
                'designation_id',
            ]);

        $subQueryPerson = Person::find()
            ->select([
                'staffing_id' => new Expression('0'),
                'office_id',
                'designation_id',
                'fact' => 'COUNT(id)',
                'norm' => new Expression('0'),
                'jobs' => new Expression('0'),
                'salaryusd' => new Expression('0'),
                'salarylocal' => new Expression('0'),
                'description' => new Expression('""'),
                'payment_type' => new Expression('""'),
                'percent_base' => new Expression('0'),
                'base_staffing_id' => new Expression('0'),
            ])
            ->where(['active' => 1])
            ->groupBy([
                'office_id',
                'designation_id',
            ]);


        $query = new Query();
        $query
            ->select([
                'staffing_id' => 'SUM(staffing_id)',
                'office_id',
                'office_name' => Office::tableName() . '.name',
                'office_jobs_number' => Office::tableName() . '.jobs_number',
                'hide_staffing_salary' => Office::tableName() . '.hide_staffing_salary',
                'calc_salary_local' => Office::tableName() . '.calc_salary_local',
                'calc_salary_usd' => Office::tableName() . '.calc_salary_usd',
                'designation_id',
                'designation_name' => Designation::tableName() . '.name',
                'calc_work_time' => Designation::tableName() . '.calc_work_time',
                'senior_operator' => Designation::tableName() . '.senior_operator',
                'fact' => 'SUM(fact)',
                'norm' => 'SUM(norm)',
                'jobs' => 'SUM(jobs)',
                'salaryusd' => 'SUM(salaryusd)',
                'salarylocal' => 'SUM(salarylocal)',
                'comment' => 'MAX(description)',
                'payment_type' => 'MAX(payment_type)',
                'percent_base' => 'MAX(percent_base)',
                'base_staffing_id' => 'MAX(base_staffing_id)',
            ])
            ->from([
                'u' => $subQueryStaffing->union($subQueryPerson)
            ])
            ->leftJoin(Office::tableName(), Office::tableName() . '.id =  u.office_id')
            ->leftJoin(Designation::tableName(), Designation::tableName() . '.id =  u.designation_id')
            // только для активных офисов
            ->where([Office::tableName() . '.active' => 1])
            ->groupBy([
                'office_id',
                'designation_id',
            ])
            ->orderBy([
                'office_name' => SORT_ASC,
                'salaryusd' => SORT_DESC,
                'salarylocal' => SORT_DESC,
                'designation_name' => SORT_DESC,
            ]);


        if ($this->office_id) {
            $query->andFilterWhere(['=', 'office_id', $this->office_id]);
        }
        if ($this->type) {
            $query->andFilterWhere(['=', Office::tableName() . '.type', $this->type]);
        }

        $officeIds = array_keys(Office::find()->bySystemUserCountries()->collection());
        if (sizeof($officeIds) == 1) {
            $this->office_id = $officeIds[0];
        }
        if (!$officeIds) {
            $officeIds[] = -1;
        }

        $query->andFilterWhere(['office_id' => $officeIds]);

        $config = [
            'allModels' => $query->all()
        ];

        $dataProvider = new ArrayDataProvider($config);


        return $dataProvider;
    }
}