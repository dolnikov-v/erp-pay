<?php
namespace app\modules\salary\models\search;

use app\modules\salary\models\Office;
use app\modules\salary\models\Bonus;
use app\modules\salary\models\Person;
use yii\data\ActiveDataProvider;

/**
 * Class BonusSearch
 * @package app\modules\salary\models\search
 */
class BonusSearch extends Bonus
{
    public $dateFrom;
    public $dateTo;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['person', 'person_id', 'office_id', 'dateFrom', 'dateTo'], 'safe']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['id'] = SORT_DESC;
        }

        $query = Bonus::find()
            ->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
        $officeIds = array_keys(Office::find()->bySystemUserCountries()->collection());

        $query->joinWith('person');
        $query->andFilterWhere(['like', Person::tableName() . '.name', $this->person_id]);
        $query->andFilterWhere(['in', Person::tableName() . '.office_id', $officeIds]);
        $query->andFilterWhere(['=', Person::tableName() . '.office_id', $this->office_id]);

        if ($this->dateFrom && $this->dateTo) {
            $query->andFilterWhere(['between', Bonus::tableName() . '.date',
                date("Y-m-d", strtotime($this->dateFrom)),
                date("Y-m-d", strtotime($this->dateTo)),
            ]);
        }
        return $dataProvider;
    }
}
