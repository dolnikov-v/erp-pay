<?php
namespace app\modules\salary\models\search;

use app\modules\salary\models\Designation;
use yii\data\ActiveDataProvider;
use Yii;

/**
 * Class DesignationSearch
 * @package app\modules\salary\models\search
 */
class DesignationSearch extends Designation
{
    const ALL = -1;
    const ACTIVE = 1;
    const NOT_ACTIVE = 0;

    public $types_active = [];
    public $activeSearch = self::ALL;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'activeSearch'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->types_active = [
            self::ALL => Yii::t('common', 'Все'),
            self::ACTIVE => Yii::t('common', 'Только активные'),
            self::NOT_ACTIVE => Yii::t('common', 'Только не активные'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return parent::attributeLabels() + ['activeSearch' => Yii::t('common', 'Активность')];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        $sort['active'] = SORT_DESC;
        if (empty($params['sort'])) {
            $sort['name'] = SORT_ASC;
        }

        $query = Designation::find()
            ->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        $this->load($params);

        $query->andFilterWhere(['like', 'name', $this->name]);

        switch ($this->activeSearch) {
            case self::ACTIVE:
                $query->andWhere([Designation::tableName() . '.active' => self::ACTIVE]);
                break;
            case self::NOT_ACTIVE:
                $query->andWhere([Designation::tableName() . '.active' => self::NOT_ACTIVE]);
                break;
        }

        if (!$this->validate()) {
            return $dataProvider;
        }

        return $dataProvider;
    }
}
