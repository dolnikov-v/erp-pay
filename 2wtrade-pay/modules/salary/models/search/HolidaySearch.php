<?php
namespace app\modules\salary\models\search;

use app\modules\salary\models\Holiday;
use yii\data\ActiveDataProvider;

/**
 * Class HolidaySearch
 * @package app\modules\salary\models\search
 */
class HolidaySearch extends Holiday
{
    public $dateFrom;
    public $dateTo;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'dateFrom', 'dateTo'], 'safe']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['date'] = SORT_ASC;
        }

        $query = Holiday::find()
            ->bySystemUserCountries()
            ->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
        $query->byCountryId($this->country_id);
        if ($this->dateFrom && $this->dateTo) {
            $query->andFilterWhere(['between', Holiday::tableName() . '.date',
                date("Y-m-d", strtotime($this->dateFrom)),
                date("Y-m-d", strtotime($this->dateTo)),
            ]);
        }
        return $dataProvider;
    }
}
