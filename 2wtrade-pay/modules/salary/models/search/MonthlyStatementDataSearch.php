<?php
namespace app\modules\salary\models\search;

use app\modules\salary\models\MonthlyStatementData;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * Class MonthlyStatementDataSearch
 * @package app\modules\salary\models\search
 */
class MonthlyStatementDataSearch extends MonthlyStatementData
{
    /**
     * @var bool
     */
    public $use_strict_name;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'parent_name', 'country_names', 'designation', 'call_center_user_logins', 'use_strict_name'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $labels = [
            'use_strict_name' => Yii::t('common', 'Использовать строгое соответствие имен'),
        ];

        return array_merge(parent::attributeLabels(), $labels);
    }

    /**
     * Приведение специфических символов в имени к привиальным символам
     * @param $string
     * @return string
     */
    private function normalizeName($string)
    {
        // flick diacritics off of their letters
        return preg_replace('~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml|caron);~i', '$1', htmlentities($string, ENT_COMPAT, 'UTF-8'));
    }

    /**
     * @return array
     */
    public function getNotFoundNames()
    {
        $names = [];

        $normalizeSearchNames = [];
        foreach ($this->getNames() as $name) {
            $normalizeSearchNames[] = $this->normalizeName($name);
        }

        if ($normalizeSearchNames) {
            $normalizePersonNames = [];
            foreach (ArrayHelper::getColumn($this->search()->getModels(), 'name') as $name) {
                $normalizePersonNames[] = $this->normalizeName($name);
            }
            $names = array_udiff($normalizeSearchNames, $normalizePersonNames, 'strcasecmp');
        }

        return $names;
    }

    /**
     * @return array
     */
    public function getNames()
    {
        $list = preg_split('/$\R+?^/m', $this->name);
        $names = [];
        foreach ($list as $name) {
            $name = trim($name);
            if ($name !== '') {
                $names[] = $name;
            }
        }

        return $names;
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['active'] = SORT_DESC;
            $sort['id'] = SORT_ASC;
        }

        $query = MonthlyStatementData::find()
            ->orderBy($sort);

        $config = [
            'query' => $query,
            'pagination' => false
        ];

        $dataProvider = new ActiveDataProvider($config);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }


        if ($names = $this->getNames()) {
            $orName = [];
            $orName[] = 'or';

            $operation = ($this->use_strict_name) ? '=' : 'like';
            foreach ($names as $name) {

                if (!$this->use_strict_name) {
                    $words = explode(' ', trim($name));
                    $tmp = [];
                    $tmp[] = 'and';
                    foreach ($words as $word) {
                        $tmp[] = ['like', 'name', $word];
                    }
                    $orName[] = $tmp;
                }
                else {
                    $orName[] = [$operation, 'name', $name];
                }
            }
            $query->andFilterWhere($orName);
        }

        $query->andFilterWhere(['statement_id' => $this->statement_id]);

        if ($this->parent_name) {
            $query->andFilterWhere(['like', 'parent_name', $this->parent_name]);
        }
        if ($this->country_names) {
            $query->andFilterWhere(['like', 'country_names', $this->country_names]);
        }
        if ($this->designation) {
            $query->andFilterWhere(['like', 'designation', $this->designation]);
        }
        if ($this->call_center_user_logins) {
            $query->andFilterWhere(['like', 'call_center_user_logins', $this->call_center_user_logins]);
        }

        return $dataProvider;
    }
}
