<?php
namespace app\modules\salary\models\search;

use app\modules\salary\models\BonusType;
use yii\data\ActiveDataProvider;

/**
 * Class BonusTypeSearch
 * @package app\modules\salary\models\search
 */
class BonusTypeSearch extends BonusType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['office_id'], 'safe']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        $sort['active'] = SORT_DESC;
        if (empty($params['sort'])) {
            $sort['id'] = SORT_ASC;
        }

        $query = BonusType::find()
            ->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
        if ($this->office_id) {
            $query->andWhere([
                'or',
                ['=', BonusType::tableName() . '.office_id', $this->office_id],
                ['is', BonusType::tableName() . '.office_id', null]
            ]);
        }
        return $dataProvider;
    }
}
