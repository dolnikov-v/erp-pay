<?php

namespace app\modules\salary\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\modules\catalog\models\ExternalSource;
use app\modules\catalog\models\ExternalSourceRole;
use app\modules\salary\models\query\DesignationQuery;

use Yii;
use yii\helpers\BaseFileHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "salary_designation".
 *
 * @property integer $id
 * @property string $name
 * @property integer $active
 * @property integer $team_lead
 * @property integer $calc_work_time
 * @property integer $supervisor
 * @property integer $senior_operator
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $comment
 * @property double $salary_usd
 * @property string $job_description_text
 * @property string $job_description_file
 *
 * @property Person[] $people
 * @property Staffing[] $staffing
 */
class Designation extends ActiveRecordLogUpdateTime
{

    /**
     * @var UploadedFile
     */
    public $fileJobDescription;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%salary_designation}}';
    }

    /**
     * @return DesignationQuery
     */
    public static function find()
    {
        return new DesignationQuery(get_called_class());
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        $transaction = yii::$app->db->beginTransaction();

        $externalRoles = yii::$app->request->post('externalRoles');

        if ($externalRoles) {
            if ($this->saveExternalRoles($externalRoles, $insert)) {
                $transaction->commit();
            } else {
                $transaction->rollBack();
            }
        }

        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * Сохранение ролей сторонних источников в таблицу расширения таблицы должностей
     * @param $data
     * @return bool
     */
    public function saveExternalRoles($data, $insert)
    {
        //если обновляем - очистим старые привязки
        if (!$insert) {
            $delete = DesignationExtensionRole::deleteAll([
                'designation_id' => $this->id
            ]);
        }

        //определение наличия external_source
        foreach ($data as $external_source_id => $role) {
            foreach ($role as $id => $state) {
                //добавляем
                if ($state == ExternalSourceRole::STATE_ADD) {
                    $model = new DesignationExtensionRole();
                    $model->designation_id = $this->id;
                    $model->external_source_id = $external_source_id;
                    $model->external_source_role_id = $id;

                    if (!$model->save()) {
                        $this->addError('id', Yii::t('common', 'Ошибка : {error}', ['error' => json_encode($model->getErrors(), JSON_UNESCAPED_UNICODE)]));
                        return false;
                    }
                }
            }
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['active', 'created_at', 'updated_at', 'team_lead', 'calc_work_time', 'supervisor', 'senior_operator'],
                'integer'
            ],
            [['name', 'comment', 'job_description_file'], 'string', 'max' => 255],
            [['job_description_text'], 'string'],
            [['salary_usd'], 'number'],
            [['fileJobDescription'], 'file', 'skipOnEmpty' => true],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'name' => Yii::t('common', 'Наименование должности'),
            'comment' => Yii::t('common', 'Комментарий'),
            'salary_usd' => Yii::t('common', 'Оклад в USD (для информации)'),
            'active' => Yii::t('common', 'Доступность'),
            'team_lead' => Yii::t('common', 'Тимлид'),
            'calc_work_time' => Yii::t('common', 'Считать ЗП с учетом отработанных часов (Оператор)'),
            'senior_operator' => Yii::t('common', 'Старший оператор'),
            'supervisor' => Yii::t('common', 'Супервайзер'),
            'job_description_text' => Yii::t('common', 'Должностные инструкции, текст'),
            'job_description_file' => Yii::t('common', 'Должностные инструкции, файл'),
            'created_at' => Yii::t('common', 'Дата добавления'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeople()
    {
        return $this->hasMany(Person::className(), ['designation_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDesignationExtensionRole()
    {
        return $this->hasMany(DesignationExtensionRole::className(), ['designation_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStaffing()
    {
        return $this->hasMany(Staffing::className(), ['designation_id' => 'id']);
    }

    public function beforeSave($insert)
    {
        //echo '<pre>' . print_r(yii::$app->request->post(), 1) . '</pre>';
        //exit;
        if (!$this->created_at) {
            $this->created_at = time();
        }
        return parent::beforeSave($insert);
    }

    public function beforeDelete()
    {
        if ($this->people) {
            $this->addError('id', Yii::t('common', 'Нельзя удалить должность, т.к. она есть у сотрудников'));
            return false;
        }
        if ($this->staffing) {
            $this->addError('id', Yii::t('common', 'Нельзя удалить должность, т.к. она есть в штатном расписании'));
            return false;
        }
        return parent::beforeDelete();
    }

    /**
     * @param string $file
     * @return string
     */
    public static function getPathFile($file = "")
    {
        $path = Yii::getAlias('@media') . DIRECTORY_SEPARATOR . 'salary-designation';
        if ($file) {
            $path .= DIRECTORY_SEPARATOR . $file;
        }
        return $path;
    }

    /**
     * @param string $name
     * @return string
     */
    public function getUniqueName($name)
    {
        return md5($name . microtime());
    }

    /**
     * @param string $attribute
     * @return mixed
     */
    public function upload($attribute = 'fileJobDescription')
    {
        if ($this->validate() && $this->$attribute) {

            if (!file_exists(self::getPathFile())) {
                BaseFileHelper::createDirectory(self::getPathFile());
            }

            $uniqueFileName = $this->getUniqueName($this->$attribute->baseName) . '.' . $this->$attribute->extension;
            $this->$attribute->saveAs(self::getPathFile($uniqueFileName));
            return $uniqueFileName;
        } else {
            return false;
        }
    }
}