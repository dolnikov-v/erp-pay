<?php

namespace app\modules\salary\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\Currency;
use app\models\User;
use app\modules\salary\models\query\PenaltyTypeQuery;
use Yii;

/**
 * This is the model class for table "salary_penalty_type".
 *
 * @property integer $id
 * @property string $trigger
 * @property string $name
 * @property integer $type
 * @property integer $currency_id
 * @property double $sum
 * @property integer $percent
 * @property integer $active
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Currency $currency
 * @property User $createdByUser
 * @property User $updatedByUser
 */
class PenaltyType extends ActiveRecordLogUpdateTime
{

    const PENALTY_AUTO_10_MIN_LATE_ID = 2;
    const PENALTY_AUTO_SOCIAL_MEDIA_ID = 3;

    /**
     * опоздание на 10 минут и больше = штраф
     */
    const PENALTY_AUTO_10_MIN_LATE_LIMIT = 10;

    /**
     * использование социальных сетей и прочих сайтов более 5 минут в день
     */
    const PENALTY_AUTO_SOCIAL_MEDIA_LIMIT = 5;

    const PENALTY_TYPE_SUM = 'sum';
    const PENALTY_TYPE_PERCENT = 'percent';
    const PENALTY_TYPE_OTHER = 'other';

    const TRIGGER_INCORRECT_ORDER_DATA = 'incorrect_order_data';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%salary_penalty_type}}';
    }

    /**
     * @return PenaltyTypeQuery
     */
    public static function find()
    {
        return new PenaltyTypeQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'type'], 'required'],
            [['sum'], 'number'],
            [['percent', 'currency_id', 'active', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'integer'],
            [['name', 'type', 'trigger'], 'string', 'max' => 255],
            [['active'], 'default', 'value' => 1],
            [['currency_id'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::className(), 'targetAttribute' => ['currency_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
            ['sum', 'required', 'when' => function ($model) {
                return $model->type == self::PENALTY_TYPE_SUM;
            }, 'whenClient' => "function (attribute, value) {
                return $('#penaltytype-type').val() == 'sum';
            }"],
            ['percent', 'required', 'when' => function ($model) {
                return $model->type == self::PENALTY_TYPE_PERCENT;
            }, 'whenClient' => "function (attribute, value) {
                return $('#penaltytype-type').val() == 'percent';
            }"],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'name' => Yii::t('common', 'Название'),
            'type' => Yii::t('common', 'Тип штрафа'),
            'percent' => Yii::t('common', 'Процент штрафа'),
            'sum' => Yii::t('common', 'Сумма штрафа в USD'),
            'currency_id' => Yii::t('common', 'Валюта'),
            'active' => Yii::t('common', 'Активность'),
            'created_by' => Yii::t('common', 'Кто создал'),
            'updated_by' => Yii::t('common', 'Кто изменил'),
            'created_at' => Yii::t('common', 'Дата добавления'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
            'trigger' => Yii::t('common', 'Идентификатор'),
        ];
    }

    /**
     * @return array
     */
    public static function getTypes()
    {
        return [
            self::PENALTY_TYPE_SUM => Yii::t('common', 'Фиксированная сумма'),
            self::PENALTY_TYPE_PERCENT => Yii::t('common', 'Процент от оклада'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedByUser()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedByUser()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @param $str
     * @return string|null
     */
    public static function getTriggerByString($str)
    {
        $map = [
            'incorrect_order_data' => self::TRIGGER_INCORRECT_ORDER_DATA,
        ];

        return isset($map[$str]) ? $map[$str] : $str;
    }
}