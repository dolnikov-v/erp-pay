<?php

namespace app\modules\salary\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\User;
use app\modules\order\models\Order;
use Yii;
use yii\helpers\ArrayHelper;
use app\jobs\SendMail;

/**
 * This is the model class for table "salary_penalty".
 *
 * @property integer $id
 * @property integer $person_id
 * @property integer $order_id
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $penalty_type_id
 * @property integer $penalty_percent
 * @property double $penalty_sum_local
 * @property double $penalty_sum_usd
 * @property string $comment
 * @property string $date
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property User $createdBy
 * @property User $updatedBy
 * @property PenaltyType $penaltyType
 * @property Person $person
 */
class Penalty extends ActiveRecordLogUpdateTime
{
    public $office_id;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%salary_penalty}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['person_id', 'penalty_type_id'], 'required'],
            [
                [
                    'person_id',
                    'created_by',
                    'updated_by',
                    'penalty_type_id',
                    'penalty_percent',
                    'created_at',
                    'updated_at',
                    'order_id'
                ],
                'integer'
            ],
            [['penalty_sum_local', 'penalty_sum_usd'], 'number'],
            [['comment'], 'string', 'max' => 255],
            ['date', 'safe'],
            [
                ['created_by'],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => ['created_by' => 'id']
            ],
            [
                ['penalty_type_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => PenaltyType::className(),
                'targetAttribute' => ['penalty_type_id' => 'id']
            ],
            [
                ['person_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Person::className(),
                'targetAttribute' => ['person_id' => 'id']
            ],
            [
                ['updated_by'],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => ['updated_by' => 'id']
            ],
            ['date', 'required', 'enableClientValidation' => false],
            [
                'order_id',
                'exist',
                'skipOnError' => true,
                'targetClass' => '\app\modules\order\models\Order',
                'targetAttribute' => 'id',
                'message' => Yii::t('common', 'Заказ с указанным номером не существует.'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'office_id' => Yii::t('common', 'Офис'),
            'person_id' => Yii::t('common', 'Сотрудник'),
            'penalty_type_id' => Yii::t('common', 'Тип штрафа'),
            'penalty_percent' => Yii::t('common', 'Процент штрафа'),
            'penalty_sum_local' => Yii::t('common', 'Сумма штрафа в местной валюте'),
            'penalty_sum_usd' => Yii::t('common', 'Сумма штрафа в USD'),
            'comment' => Yii::t('common', 'Комментарий'),
            'date' => Yii::t('common', 'Дата штрафа'),
            'created_by' => Yii::t('common', 'Кто создал'),
            'updated_by' => Yii::t('common', 'Кто изменил'),
            'created_at' => Yii::t('common', 'Дата добавления'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
            'order_id' => Yii::t('common', 'Номер заказа'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPenaltyType()
    {
        return $this->hasOne(PenaltyType::className(), ['id' => 'penalty_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerson()
    {
        return $this->hasOne(Person::className(), ['id' => 'person_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    public function beforeValidate()
    {
        $this->penalty_sum_local = str_replace(',', '.', $this->penalty_sum_local);
        $this->penalty_sum_usd = str_replace(',', '.', $this->penalty_sum_usd);

        return parent::beforeValidate();
    }

    public function beforeSave($insert)
    {

        if (is_a(Yii::$app, 'yii\web\Application')) {

            if ($insert == self::EVENT_BEFORE_INSERT) {
                $this->created_by = Yii::$app->user->id;
            }

            $this->updated_by = Yii::$app->user->id;
        }
        $this->date = $this->date ? Yii::$app->formatter->asDate($this->date, 'php:Y-m-d') : null;
        if ($this->penaltyType->type == PenaltyType::PENALTY_TYPE_PERCENT) {
            // случай когда штрафуют на процент от оклада, посчитаем конкретную сумму

            if ($this->person->salary_local) {
                $this->penalty_sum_local = $this->person->salary_local * $this->penalty_percent / 100;
            }
            if ($this->person->salary_usd) {
                $this->penalty_sum_usd = $this->person->salary_usd * $this->penalty_percent / 100;
            }
        }

        $currencyId = $this->person->office->currency_id ? $this->person->office->currency_id : $this->person->office->country->currency_id;

        if (!$this->penalty_sum_usd) {
            $this->penalty_sum_usd = Bonus::getUSDSum($this->penalty_sum_local, $currencyId);
        }
        if (!$this->penalty_sum_local) {
            $this->penalty_sum_local = Bonus::getLocalSum($this->penalty_sum_usd, $currencyId);
        }

        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        if ($insert && $this->person->corporate_email) {
            $emailsOfSupervisors = ArrayHelper::getColumn(
                Person::find()
                    ->select(['corporate_email'])
                    ->byOfficeId($this->person->office_id)
                    ->isTeamLeadOrSupervisor()
                    ->andWhere(['is not', 'corporate_email', null])
                    ->andWhere(['<>', 'corporate_email', ''])
                    ->active()
                    ->distinct()
                    ->all(),
                'corporate_email');


            $emailsOfHeads = ArrayHelper::getColumn(
                Person::find()
                    ->select(['corporate_email'])
                    ->byDesignationName([
                        "Руководитель отдела технической поддержки и информационной безопасности",
                        "Операционный директор",
                        "Руководитель казначейства",
                        "Технический директор",
                    ])
                    ->andWhere(['is not', 'corporate_email', null])
                    ->andWhere(['<>', 'corporate_email', ''])
                    ->active()
                    ->distinct()
                    ->all(),
                'corporate_email');

            $mailObject = Yii::$app->mailer->compose(['text' => '@app/mail/person-penalty/mail'], ['penalty' => $this])
                ->setFrom(Yii::$app->params['noReplyEmail'])
                ->setTo($this->person->corporate_email)
                ->setSubject("You have a new penalty");

            if ($emailsOfSupervisors) {
                $mailObject->setCc($emailsOfSupervisors);
            }

            if ($emailsOfHeads) {
                $mailObject->setBcc($emailsOfHeads);
            }

            $this->queueTransport->push(new SendMail(['mailObject' => $mailObject]));

        }
        return parent::afterSave($insert, $changedAttributes);
    }


    /**
     * @param $personID integer
     * @param $date string
     * @param $penaltyType PenaltyType
     * @param $comment string
     * @return mixed
     */
    public static function createPenalty($personID, $date, $penaltyType, $comment = '')
    {
        if (self::find()->where([
            'person_id' => $personID,
            'date' => $date,
            'penalty_type_id' => $penaltyType->id
        ])->one()
        ) {
            return false;
        }
        $penalty = new self();
        $penalty->person_id = $personID;
        $penalty->penalty_type_id = $penaltyType->id;
        $penalty->penalty_sum_usd = $penaltyType->sum;
        $penalty->date = $date;
        $penalty->comment = $comment;
        if (!$penalty->save()) {
            return $penalty->getFirstErrorAsString();
        }
        return true;
    }
}
