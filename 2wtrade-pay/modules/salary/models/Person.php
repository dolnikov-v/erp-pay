<?php
namespace app\modules\salary\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\User;
use app\modules\callcenter\models\CallCenterUser;
use app\modules\callcenter\models\CallCenterWorkTime;
use app\modules\salary\models\query\PersonQuery;
use Yii;
use yii\helpers\BaseFileHelper;
use yii\web\UploadedFile;

/**
 * Class Person
 * @package app\modules\salary\models
 *
 * This is the model class for table "salary_person".
 *
 * @property integer $id
 * @property string $name
 * @property integer $designation_id
 * @property integer $parent_id
 * @property integer $office_id
 * @property string $birth_date
 * @property string $start_date
 * @property string $dismissal_date
 * @property string $dismissal_comment
 * @property string $dismissal_file
 * @property string $photo
 * @property string $photo_agreement
 * @property string $salary_scheme
 * @property double $salary_local
 * @property double $salary_usd
 * @property double $salary_hour_local
 * @property double $salary_hour_usd
 * @property integer $active
 * @property integer $approved
 * @property string $passport_id_number
 * @property string $corporate_email
 * @property string $phone
 * @property string $skype
 * @property integer $working_shift_id
 * @property string $account_number
 * @property string $bank_name
 * @property string $bank_address
 * @property string $bank_swift_code
 * @property integer $crocotime_employee_id
 * @property string $address
 * @property string $gender
 * @property string $experience
 * @property double $insurance_coefficient
 * @property double $pension_contribution
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Person $parent
 * @property Office $office
 * @property Designation $designation
 * @property WorkingShift $workingShift
 * @property PersonLanguage[] $languages
 * @property PersonLink[] $personLinks
 * @property PersonLink $personLinkCrocoTime
 * @property array $birthdayTemplateReplacements
 * @property CallCenterUser[] $callCenterUsers
 * @property PersonLink $personLinkERP
 */
class Person extends ActiveRecordLogUpdateTime
{

    const ACTIVE = 1;
    const NOT_ACTIVE = 0;

    const SALARY_SCHEME_HOURLY = 'hourly';
    const SALARY_SCHEME_PER_DAY = 'per_day';
    const SALARY_SCHEME_PER_MONTH = 'per_month';

    const GENDER_EMPTY = '';
    const GENDER_FEMALE = 'female';
    const GENDER_MALE = 'male';

    const DATE_WARNING_LIMIT = 6;

    /**
     * @var UploadedFile
     */
    public $imageFile;

    /**
     * @var UploadedFile
     */
    public $imageFileAgreement;

    /**
     * @var UploadedFile
     */
    public $imageFileDismissal;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%salary_person}}';
    }

    /**
     * @return PersonQuery
     */
    public static function find()
    {
        return new PersonQuery(get_called_class());
    }


    /**
     * @return array
     */
    public static function getGenders()
    {
        return [
            self::GENDER_EMPTY => Yii::t('common', '-'),
            self::GENDER_FEMALE => Yii::t('common', 'Женщина'),
            self::GENDER_MALE => Yii::t('common', 'Мужчина'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'office_id'], 'required'],
            [
                ['designation_id', 'office_id', 'active', 'approved', 'created_at', 'updated_at', 'working_shift_id'],
                'integer'
            ],
            [
                [
                    'name',
                    'photo',
                    'photo_agreement',
                    'salary_scheme',
                    'passport_id_number',
                    'corporate_email',
                    'phone',
                    'skype',
                    'address',
                    'gender',
                    'experience',
                    'dismissal_comment',
                    'dismissal_file',
                    'bank_name',
                    'bank_address',
                    'bank_swift_code',
                ],
                'string',
                'max' => 255
            ],
            [['active'], 'default', 'value' => 0],
            [['approved'], 'default', 'value' => 0],
            [['active', 'approved'], 'number', 'min' => 0, 'max' => 1],
            [['insurance_coefficient'], 'default', 'value' => 1],
            [['insurance_coefficient'], 'number', 'min' => 1],
            [['pension_contribution'], 'number', 'min' => 0, 'max' => 100],
            [['imageFile', 'imageFileAgreement'], 'file', 'skipOnEmpty' => true],
            [['birth_date', 'start_date', 'dismissal_date'], 'safe'],
            [['salary_local', 'salary_usd', 'salary_hour_local', 'salary_hour_usd', 'insurance_coefficient', 'pension_contribution'], 'number'],
            [
                ['office_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Office::className(),
                'targetAttribute' => ['office_id' => 'id']
            ],
            [
                ['designation_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Designation::className(),
                'targetAttribute' => ['designation_id' => 'id']
            ],
            [
                ['parent_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Person::className(),
                'targetAttribute' => ['parent_id' => 'id']
            ],
            [['corporate_email'], 'email', 'message' => 'Неверный формат Email'],
            [
                ['working_shift_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => WorkingShift::className(),
                'targetAttribute' => ['working_shift_id' => 'id']
            ],
            [['account_number'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'name' => Yii::t('common', 'ФИО'),
            'office_id' => Yii::t('common', 'Офис'),
            'designation_id' => Yii::t('common', 'Должность'),
            'parent_id' => Yii::t('common', 'Руководитель'),
            'birth_date' => Yii::t('common', 'Дата рождения'),
            'start_date' => Yii::t('common', 'Дата приема на работу'),
            'dismissal_date' => Yii::t('common', 'Дата увольнения'),
            'dismissal_comment' => Yii::t('common', 'Причина увольнения'),
            'dismissal_file' => Yii::t('common', 'Документ (заявление) увольнения'),
            'photo' => Yii::t('common', 'Скан паспорта'),
            'photo_agreement' => Yii::t('common', 'Скан договора'),
            'salary_scheme' => Yii::t('common', 'Схема начисления з/п'),
            'salary_local' => Yii::t('common', 'Сумма з/п за месяц в местной валюте'),
            'salary_usd' => Yii::t('common', 'Сумма з/п за месяц в USD'),
            'salary_hour_local' => Yii::t('common', 'Сумма з/п за час в местной валюте'),
            'salary_hour_usd' => Yii::t('common', 'Сумма з/п за час в USD'),
            'active' => Yii::t('common', 'Активен'),
            'approved' => Yii::t('common', 'Подтвержден'),
            'passport_id_number' => Yii::t('common', 'Номер ID/Паспорта'),
            'corporate_email' => Yii::t('common', 'Корпоративная почта'),
            'phone' => Yii::t('common', 'Телефон'),
            'skype' => Yii::t('common', 'Skype'),
            'working_shift_id' => Yii::t('common', 'Рабочая смена'),
            'account_number' => Yii::t('common', 'Номер счета'),
            'bank_name' => Yii::t('common', 'Название банка'),
            'bank_address' => Yii::t('common', 'Адрес банка'),
            'bank_swift_code' => Yii::t('common', 'СВИФТ–код банка'),
            'address' => Yii::t('common', 'Домашний адрес'),
            'gender' => Yii::t('common', 'Пол'),
            'languages' => Yii::t('common', 'Языки'),
            'experience' => Yii::t('common', 'Опыт работы'),
            'insurance_coefficient' => 'Коэф. страхового взноса',
            'pension_contribution' => 'Пенсионной взнос (%)',
            'created_at' => Yii::t('common', 'Дата добавления'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
        ];
    }

    public function beforeSave($insert)
    {

        if (!$this->office_id) {
            $this->working_shift_id = null;
        } else if (!$this->working_shift_id) {
            $workingShift = WorkingShift::find()->byOfficeId($this->office_id)->isDefault()->one();
            if ($workingShift) {
                $this->working_shift_id = $workingShift->id;
            }
        }

        if (!$insert) {
            $groupId = uniqid();
            $dirtyAttributes = $this->getDirtyAttributes();

            // Меняем смену в CrocoTime
            /*if ( $this->crocotime_employee_id
                 && $this->isAttributeChanged( 'working_shift_id' )
                 && ($scheduleId = $this->workingShift->crocotime_id)) {
                $crocotimeApi = new CrocotimeApi();
                if ( !$crocotimeApi->updateEmployeeSchedule( $this->crocotime_employee_id, $scheduleId ) ) {
                    $this->addError('id', Yii::t('common', 'Не удалось изменить рабочую смену в Crocotime'));
                };
            };*/

            foreach ($dirtyAttributes as $key => $item) {
                if ($item != $this->getOldAttribute($key)) {
                    $old = (string)$this->getOldAttribute($key);

                    $log = new PersonLog();
                    $log->person_id = $this->id;
                    $log->user_id = isset(Yii::$app->user) ? Yii::$app->user->id : null;
                    $log->group_id = $groupId;
                    $log->field = $key;
                    $log->old = mb_substr($old, 0, 250);
                    $log->new = mb_substr($item, 0, 250);
                    $log->save();
                }
            }
        }
        return parent::beforeSave($insert);
    }

    /**
     * @param string $photo
     * @return string
     */
    public static function getPathFile($photo = "")
    {
        $path = Yii::getAlias('@media') . DIRECTORY_SEPARATOR . 'salary-person';
        if ($photo) {
            $path .= DIRECTORY_SEPARATOR . $photo;
        }
        return $path;
    }

    /**
     * @param string $name
     * @return string
     */
    public function getUniqueName($name)
    {
        return md5($name . microtime());
    }

    /**
     * @param string $attribute
     * @return mixed
     */
    public function upload($attribute = 'imageFile')
    {
        if ($this->validate() && $this->$attribute) {

            if (!file_exists(self::getPathFile())) {
                BaseFileHelper::createDirectory(self::getPathFile());
            }

            $uniqueFileName = $this->getUniqueName($this->$attribute->baseName) . '.' . $this->$attribute->extension;
            $this->$attribute->saveAs(self::getPathFile($uniqueFileName));
            return $uniqueFileName;
        } else {
            return false;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCallCenterUsers()
    {
        return $this->hasMany(CallCenterUser::className(), ['person_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOffice()
    {
        return $this->hasOne(Office::className(), ['id' => 'office_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDesignation()
    {
        return $this->hasOne(Designation::className(), ['id' => 'designation_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Person::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkingShift()
    {
        return $this->hasOne(WorkingShift::className(), ['id' => 'working_shift_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguages()
    {
        return $this->hasMany(PersonLanguage::className(), ['person_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonLinks()
    {
        return $this->hasMany(PersonLink::className(), ['person_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonLinkCrocoTime()
    {
        return $this->hasOne(PersonLink::className(), ['person_id' => 'id'])
            ->where(['type' => PersonLink::TYPE_CROCOTIME]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonLinkERP()
    {
        return $this->hasOne(PersonLink::className(), ['person_id' => 'id'])->where(['type' => PersonLink::TYPE_ERP]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserERP()
    {
        return $this->hasOne(User::className(), ['id' => 'foreign_id'])
            ->via('personLinkERP');
    }

    /**
     * @param string $foreignId
     * @return boolean|string true или строка ошибки
     */
    public function savePersonLinkCrocoTime($foreignId)
    {
        if ($this->personLinkCrocoTime instanceof PersonLink) {
            $this->personLinkCrocoTime->foreign_id = (string)$foreignId;
            if (!$this->personLinkCrocoTime->save()) {
                return $this->personLinkCrocoTime->getFirstErrorAsString();
            }
        } else {
            $new = new PersonLink();
            $new->foreign_id = (string)$foreignId;
            $new->person_id = $this->id;
            if (!$new->save()) {
                return $new->getFirstErrorAsString();
            }
        }
        return true;
    }

    /**
     * @param boolean $empty
     * @return array
     */
    public static function getSalarySchemes($empty = false)
    {
        $schemes = [
            0 => '-',
            self::SALARY_SCHEME_HOURLY => Yii::t('common', 'Почасовая'),
            //self::SALARY_SCHEME_PER_DAY => Yii::t('common', 'За день'),
            self::SALARY_SCHEME_PER_MONTH => Yii::t('common', 'За месяц'),
        ];
        if (!$empty) {
            unset($schemes[0]);
        }
        return $schemes;
    }

    /**
     * @param integer $officeId
     * @param boolean $all
     * @return array
     */
    public static function getPatentsCollection($officeId = null, $all = false)
    {
        $query = Person::find()->select(
            [
                'id' => Person::tableName() . '.id',
                'name' => 'CONCAT(' . Person::tableName() . '.name, IF(' . Person::tableName() . '.active = 1, ""," (' . Yii::t('common', 'уволен') . ')"))'
            ])
            ->joinWith('designation')
            ->where([Designation::tableName() . '.team_lead' => 1])
            ->byOfficeId($officeId)
            ->bySystemUserCountries()
            ->orderBy([
                'name' => SORT_ASC
            ]);

        if ($all) {
            return $query->asArray()->all();
        }
        return $query->collection();
    }

    /**
     * @param integer $officeId
     * @param boolean $all
     * @return array
     */
    public static function getCollection($officeId = null, $all = false)
    {
        $query = Person::find()->select(
            [
                'id',
                'name' => 'name'
            ])
            ->byOfficeId($officeId)
            ->orderBy([
                'name' => SORT_ASC
            ]);

        if ($all) {
            return $query->asArray()->all();
        }
        return $query->collection();
    }

    /**
     * @return boolean
     */
    function isOperator()
    {
        return ($this->designation instanceof Designation && $this->designation->calc_work_time);
    }

    /**
     * @return boolean
     */
    function isSupervisor()
    {
        return ($this->designation instanceof Designation && $this->designation->supervisor);
    }

    function afterSave($insert, $changedAttributes)
    {

        if ($insert) {
            // TODO Перенести на фоновое выполнение при появлении очереди задач
            $plan = new WorkTimePlanGenerate();
            $plan->dateFrom = $this->start_date ?? date('Y-m-d', strtotime('+1day'));
            $plan->dateTo = date('Y-m-t');
            $plan->person_id = $this->id;
            $plan->generate();
        }
        else if (
            (isset($changedAttributes["dismissal_date"]) && $changedAttributes["dismissal_date"] != $this->dismissal_date) ||
            (isset($changedAttributes["working_shift_id"]) && $changedAttributes["working_shift_id"] != $this->working_shift_id)
        ) {

            $plan = new WorkTimePlanGenerate();
            $plan->dateFrom = date('Y-m-d', strtotime('+1day'));
            $plan->dateTo = $this->dismissal_date ?? date('Y-m-t');
            $plan->person_id = $this->id;
            $plan->generate();

            if ($this->dismissal_date) {
                WorkTimePlan::deleteAll([
                    'and',
                    ['=', 'person_id', $this->id],
                    ['>', 'date', $this->dismissal_date]
                ]);
            }
        }

        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
    }


    /**
     * @return array
     */
    public function getBirthdayTemplateReplacements()
    {
        $tmp = explode('-', $this->birth_date);
        $date = date('Y') . '-' . $tmp[1] . '-' . $tmp[2];
        return [
            '/{\*NAME\*}/' => $this->name,
            '/{\*DATE\*}/' => Yii::$app->formatter->asDate($date),
        ];
    }

    /**
     * @param $holiday Holiday
     * @return array
     */
    public function getHolidayTemplateReplacements($holiday)
    {
        return [
            '/{\*NAME\*}/' => $this->name,
            '/{\*DATE\*}/' => Yii::$app->formatter->asDate($holiday->date),
            '/{\*HOLIDAY\*}/' => $holiday->name,
        ];
    }


    /**
     * @return string
     */
    public function makeBirthdayGreetingCardSubject()
    {
        return preg_replace(
            array_keys($this->getBirthdayTemplateReplacements()),
            array_values($this->getBirthdayTemplateReplacements()),
            $this->office->birthday_greeting_card_template_subject
        );
    }

    /**
     * @return string
     */
    public function makeBirthdayGreetingCardMessage()
    {
        return preg_replace(
            array_keys($this->getBirthdayTemplateReplacements()),
            array_values($this->getBirthdayTemplateReplacements()),
            $this->office->birthday_greeting_card_template_message
        );
    }

    /**
     * @param $holiday Holiday
     * @return string
     */
    public function makeHolidayGreetingCardSubject($holiday)
    {
        return preg_replace(
            array_keys($this->getHolidayTemplateReplacements($holiday)),
            array_values($this->getHolidayTemplateReplacements($holiday)),
            $holiday->greeting_card_template_subject
        );
    }

    /**
     * @param $holiday Holiday
     * @return string
     */
    public function makeHolidayGreetingCardMessage($holiday)
    {
        return preg_replace(
            array_keys($this->getHolidayTemplateReplacements($holiday)),
            array_values($this->getHolidayTemplateReplacements($holiday)),
            $holiday->greeting_card_template_message
        );
    }

    /**
     * @return bool|string
     */
    public function getPingInfo()
    {
        return $this->hasOne(CallCenterWorkTime::className(), ['call_center_user_id' => 'id'])
            ->select('date')
            ->via('callCenterUsers')
            ->andWhere(['>', CallCenterWorkTime::tableName() . '.time', 0])
            ->orderBy([CallCenterWorkTime::tableName() . '.date' => SORT_DESC])
            ->limit(1)
            ->scalar();
    }

    /**
     * Принимает на входе время-строку вида 8:00-17:00 и определит работает ли в этот время сотрудник
     *
     * @param $wortTimeLine string
     * @return bool
     */
    public function isWorkingOnTime($wortTimeLine)
    {
        if ($wortTimeLine == '') {
            return false;
        }
        if (!isset($this->office->country->timezone->timezone_id)) {
            return false;
        }
        $ar = explode('-', $wortTimeLine);
        if (!isset($ar[1]) || !isset($ar[1])) {
            return false;
        }
        $zone = new \DateTimeZone($this->office->country->timezone->timezone_id);
        $dateTime = new \DateTime('now', $zone);
        $dateTime0 = new \DateTime($ar[0], $zone);
        $dateTime1 = new \DateTime($ar[1], $zone);
        if ($dateTime->getTimestamp() >= $dateTime0->getTimestamp() && $dateTime->getTimestamp() <= $dateTime1->getTimestamp()) {
            return true;
        }
        return false;
    }


    /**
     * Факт активности в системах
     * @return mixed
     */
    public function getActivityWarning()
    {
        if ($this->userERP) {
            if (isset($this->userERP->lastact_at)) {
                $daysAFK = (time() - $this->userERP->lastact_at) / (60 * 60 * 24);
                if ($daysAFK > Person::DATE_WARNING_LIMIT) {
                    return floor($daysAFK);
                }
            } else {
                return true;
            }
        }

        if ($this->callCenterUsers) {
            $date = new \DateTime($this->pingInfo);
            $now = new \DateTime();
            if (!$this->pingInfo) {
                return true;
            }
            if ($date->diff($now)->format("%d") > self::DATE_WARNING_LIMIT) {
                return $date->diff($now)->format("%d");
            }
        }
        return false;
    }

    public function blockCallCenterUsers()
    {
        $foreignIds = [];
        foreach ($this->callCenterUsers as $centerUser) {
            // тут нужно в очередь на API КЦ
            $centerUser->active = 0;
            if ($centerUser->save()) {
                if ($centerUser->callCenter->is_amazon_query) {
                    $foreignIds[$centerUser->user_id] = $centerUser->user_id;
                }
            }
        }
        if ($foreignIds) {
            CallCenterUser::sendToBlockInCallCenterQueue($foreignIds);
        }
    }


    /**
     * @param $bonusTypeId
     * @return double
     */
    public function getBonusSumInYearByType($bonusTypeId)
    {
        return Bonus::find()
            ->select(['sum' => 'SUM(bonus_sum_usd)'])
            ->where([
                'person_id' => $this->id,
                'bonus_type_id' => $bonusTypeId
            ])
            ->andWhere([
                'between',
                'date',
                date('Y-01-01'),
                date('Y-m-d')
            ])
            ->scalar();
    }

    /**
     * @param BonusType $bonusType
     * @param null $date
     * @param null $rate
     * @param int $maximumBonus
     * @return array ['count' => число успешно добавленных бонусов, 'errors' => ошибки]
     */
    public function makeMonthlyBonus($bonusType, $date = null, $rate = null, $maximumBonus = 0)
    {
        $count = 0;
        $errors = [];

        if (!$date) {
            $date = date('Y-m-d');
        }
        if (!$rate) {
            $rate = $this->office->country->getMinRateByDate($date);
        }

        $personYearlySumUSD = 0;
        if ($maximumBonus) {
            // костыль для бонуса Чили
            // есть ограничение по максимуму бонусов
            $personYearlySumUSD = $this->getBonusSumInYearByType($bonusType->id);
        }

        if (!Bonus::find()->where([
            'person_id' => $this->id,
            'bonus_type_id' => $bonusType->id
        ])->andWhere(
            ['between', 'date', date('Y-m-01'), $date]
        )
            ->exists()
        ) {
            $bonus = new bonus();
            $bonus->person_id = $this->id;
            $bonus->bonus_type_id = $bonusType->id;

            switch ($bonusType->type) {
                case BonusType::BONUS_TYPE_SUM_WORK_HOURS:
                case BonusType::BONUS_TYPE_SUM:
                    if ($bonusType->sum) {
                        $bonus->bonus_sum_usd = $bonusType->sum;
                    }
                    if ($bonusType->sum_local) {
                        $bonus->bonus_sum_local = $bonusType->sum_local;
                    }
                    break;
                case BonusType::BONUS_TYPE_PERCENT:
                    $bonus->bonus_percent = $bonusType->percent;

                    if ($personYearlySumUSD && $this->office->country->char_code == 'CL') {
                        // костыль для бонуса Чили
                        $bonus->bonus_sum_usd = 0;
                        if ($this->salary_usd) {
                            $bonus->bonus_sum_usd = $this->salary_usd * $bonus->bonus_percent / 100;
                        } else {
                            if ($this->salary_local) {
                                $sumLocal = $this->salary_local * $bonus->bonus_percent / 100;
                                $bonus->bonus_sum_usd = $rate ? ($sumLocal / $rate) : 0;
                            }
                        }

                        if ($personYearlySumUSD + $bonus->bonus_sum_usd > $maximumBonus) {
                            $bonus->bonus_sum_usd = $maximumBonus - $personYearlySumUSD;
                        }
                        $bonus->bonus_sum_local = $bonus->bonus_sum_usd * $rate;
                        $bonus->bonus_percent = null;
                    }

                    break;
                case BonusType::BONUS_TYPE_HOUR;
                    $bonus->bonus_percent_hour = $bonusType->percent;
                    break;
                case BonusType::BONUS_TYPE_PERCENT_CALC_SALARY;
                case BonusType::BONUS_TYPE_PERCENT_WORK_HOURS;
                    $bonus->bonus_percent = $bonusType->percent;
                    break;
            }

            $bonus->date = $date;
            $bonus->comment = 'Auto Bonus';

            if (!$bonus->save()) {
                $errors[] = $bonus->getFirstErrorAsString();
            } else {
                $count++;
            }
        }
        return [
            'count' => $count,
            'errors' => $errors
        ];
    }

    /**
     * @return array
     */
    public function makeDismissalBonus()
    {
        $errors = [];
        $bonusTypes = BonusType::find()
            ->byOfficeId($this->office_id)
            ->active()
            ->dismissalPay()
            ->all();
        if ($bonusTypes) {
            foreach ($bonusTypes as $bonusType) {

                if (!Bonus::find()->where([
                    'person_id' => $this->id,
                    'bonus_type_id' => $bonusType->id
                ])->andWhere(
                    ['between', 'date', date('Y-m-01'), $this->dismissal_date]
                )
                    ->exists()
                ) {
                    $bonus = new Bonus();
                    $bonus->person_id = $this->id;
                    $bonus->bonus_type_id = $bonusType->id;
                    $bonus->date = $this->dismissal_date;
                    if ($bonusType->sum_local) {
                        $bonus->bonus_sum_local = $bonusType->sum_local;
                    }
                    if ($bonusType->sum) {
                        $bonus->bonus_sum_usd = $bonusType->sum;
                    }
                    if ($bonusType->percent) {
                        $bonus->bonus_percent = $bonusType->percent;
                    }
                    $bonus->comment = 'Dismissal Pay';

                    if (!$bonus->save()) {
                        $errors[] = $bonus->getFirstErrorAsString();
                    }
                }
            }
        }

        // Начисляем ежемесячные бонусы раньше срока

        $staffingBonusesQuery = StaffingBonus::find()
            ->joinWith('staffing', false)
            ->andWhere([Staffing::tableName().'.office_id' => $this->office_id])
            ->andWhere([Staffing::tableName().'.designation_id' => $this->designation_id]);
        $staffingBonuses = $staffingBonusesQuery->all();

        $rate = $this->office->country->getMinRateByDate($this->dismissal_date);
        foreach ($staffingBonuses as $staffingBonus) {
            /** @var $staffingBonus StaffingBonus */
            $maximumBonus = $this->office->country->getMaximumBonus($rate, $staffingBonus->bonusType->type);
            $result = $this->makeMonthlyBonus($staffingBonus->bonusType, $this->dismissal_date, $rate, $maximumBonus);
            if ($result['errors']) {
                $errors = array_merge($errors, $result['errors']);
            }
        }
        return [
            'errors' => $errors
        ];
    }

    /**
     * @param bool $inCurrentYear
     * @return int|mixed
     */
    public function getDaysWorked($inCurrentYear = false)
    {
        if ($this->start_date && $this->dismissal_date) {
            $dateFrom = new \DateTime($inCurrentYear ? max($this->start_date, date('Y-01-01')) : $this->start_date);
            $dateTo = new \DateTime($this->dismissal_date);
            $interval = $dateFrom->diff($dateTo);
            return $interval->days;
        }
        return 0;
    }

    /**
     * @return Person
     */
    public static function getMoscowLawyer()
    {
        return Person::find()
            ->active()
            ->byOfficeId(Office::MOSCOW_ID)
            ->byDesignationName(["Юрист"])
            ->one();
    }
}