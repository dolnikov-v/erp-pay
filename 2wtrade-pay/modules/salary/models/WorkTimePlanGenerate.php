<?php

namespace app\modules\salary\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;


class WorkTimePlanGenerate extends WorkTimePlan
{
    public $office_id;
    public $person_id;
    public $working_shift_id;
    public $dateFrom;
    public $dateTo;

    /**
     * @var bool запуск для генерации на месяц для всех офисов
     */
    public $global = false;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dateFrom', 'dateTo', 'person_id', 'office_id', 'working_shift_id'], 'safe'],
            [['dateFrom', 'dateTo'], 'required', 'enableClientValidation' => false],
            ['dateTo', 'validateDates'],
        ];
    }

    public function validateDates()
    {
        if (strtotime($this->dateTo) < strtotime($this->dateFrom)) {
            $this->addError('dateFrom', Yii::t('common', 'Дата начала должна быть раньше окончания'));
            $this->addError('dateTo', Yii::t('common', 'Дата окончания должна быть после начала'));
        }
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'office_id' => Yii::t('common', 'Офис'),
            'person_id' => Yii::t('common', 'Сотрудник'),
            'working_shift_id' => Yii::t('common', 'Сотрудник'),
            'type' => Yii::t('common', 'Переписать существующие графики'),
            'dateFrom' => Yii::t('common', 'Дата начала'),
            'dateTo' => Yii::t('common', 'Дата окончания'),
        ];
    }

    /**
     * @return array
     */
    public function getGeneratedWorkPlansByInterval()
    {
        $groupedExistsWorkPlans = [];

        $existWorkPlans = ArrayHelper::index(WorkTimePlan::find()->where(
            ['between', 'date',
                strtotime($this->dateFrom),
                strtotime($this->dateTo),
            ])->asArray()->all(), 'date', 'person_id');


        foreach ($existWorkPlans as $person_id => $plan) {
            $groupedExistsWorkPlans[$person_id] = array_keys($plan);
        }

        return $groupedExistsWorkPlans;
    }

    /**
     * @param bool $safeExists - crontab_task version = true, web = false
     * @return bool
     * @throws \Exception
     */
    public function generate($safeExists = false)
    {
        $persons = [];

        //Для исключения затирания ранее сгенерированных планов
        //соберём информацию
        if ($safeExists) {
            $groupedExistsWorkPlans = $this->getGeneratedWorkPlansByInterval();
        }

        if (!$this->global) {
            if (!$this->person_id) {
                $persons = Person::find()
                    ->byOfficeId($this->office_id)
                    ->byWorkingShift($this->working_shift_id)
                    ->active()
                    ->all();
            }

            if ($this->person_id) {
                $persons = Person::find()->byId($this->person_id)->all();
                if ($persons && isset($persons[0]) && !$persons[0]->working_shift_id) {
                    return false;
                }
            }
        }

        if ($this->global) {
            $persons = Person::find()->byOfficeId($this->office_id)->active()->all();
        }

        if (!$persons) {
            return false;
        }

        if (!$safeExists) {
            WorkTimePlan::deleteAll([
                'and',
                ['person_id' => ArrayHelper::getColumn($persons, 'id')],
                ['between', 'date',
                    date("Y-m-d", strtotime($this->dateFrom)),
                    date("Y-m-d", strtotime($this->dateTo)),
                ]
            ]);
        }

        $data = [];

        $holidays = ArrayHelper::index(
            Holiday::find()
                ->where([
                    'and',
                    ['>=', 'date', date("Y-m-d", strtotime($this->dateFrom))],
                    ['<=', 'date', date("Y-m-d", strtotime($this->dateTo))]
                ])
                ->asArray()
                ->all(),
            'date',
            'country_id'
        );

        foreach ($persons as $person) {
            if ($person->workingShift) {
                $workingShift = $person->workingShift;
            } else {
                $workingShift = WorkingShift::find()->isDefault()->byOfficeId($person->office_id)->one();
            }

            if (!$workingShift) {
                if (sizeof($persons) == 1) {
                    return false;
                }
                continue;
            }

            $date = date('Y-m-d', strtotime($this->dateFrom));

            $end_date = $this->dateTo;

            while (strtotime($date) <= strtotime($end_date)) {
                if (
                    ($date >= $person['start_date'] || $person['start_date'] == '') &&
                    ($date <= $person['dismissal_date'] || $person['dismissal_date'] == '') &&
                    !isset($holidays[$person->office->country_id][date('Y-m-d', strtotime($date))])
                ) {
                    $workingDayName = 'working_' . strtolower(date('D', strtotime($date)));

                    if ($workingShift->hasAttribute($workingDayName) && $workingShift->$workingDayName != '') {
                        $workingTime = explode('-', $workingShift->$workingDayName);

                        if (sizeof($workingTime) == 2) {
                            $startAt = strtotime($date . ' ' . $workingTime[0]);
                            $endAt = strtotime($date . ' ' . $workingTime[1]);
                            $workHours = WorkTimePlan::calcHours($workingShift->$workingDayName) - WorkTimePlan::calcHours($workingShift->lunch_time);

                            //кроновская версия
                            //обходим ранее созданные планы
                            if ($safeExists) {
                                $date_work_plane = date('Y-m-d', strtotime($date));

                                //нет ранее созданных рабочих планов за указанный период
                                if (!isset($groupedExistsWorkPlans[$person->id])) {
                                    $data[] = [
                                        'person_id' => $person->id,
                                        'working_shift_id' => $workingShift->id,
                                        'date' => $date_work_plane,
                                        'work_hours' => $workHours,
                                        'lunch_time' => $workingShift->lunch_time,
                                        'start_at' => $startAt,
                                        'end_at' => $endAt,
                                        'created_at' => time(),
                                        'updated_at' => time()
                                    ];
                                } else {
                                    //есть ранее созданные рабочие планы, обойдём их стороной
                                    if (!in_array($date_work_plane, $groupedExistsWorkPlans[$person->id])) {
                                        $data[] = [
                                            'person_id' => $person->id,
                                            'working_shift_id' => $workingShift->id,
                                            'date' => $date_work_plane,
                                            'work_hours' => $workHours,
                                            'lunch_time' => $workingShift->lunch_time,
                                            'start_at' => $startAt,
                                            'end_at' => $endAt,
                                            'created_at' => time(),
                                            'updated_at' => time()
                                        ];
                                    }
                                }
                            } else {
                                //web версия генерации рабочих планов
                                $data[] = [
                                    'person_id' => $person->id,
                                    'working_shift_id' => $workingShift->id,
                                    'date' => date('Y-m-d', strtotime($date)),
                                    'work_hours' => $workHours,
                                    'lunch_time' => $workingShift->lunch_time,
                                    'start_at' => $startAt,
                                    'end_at' => $endAt,
                                    'created_at' => time(),
                                    'updated_at' => time()
                                ];
                            }
                        }
                    }
                }
                $date = date("Y-m-d", strtotime("+1 day", strtotime($date)));
            }
        }

        try {
            if ($data) {
                Yii::$app->db->createCommand()->batchInsert(
                    WorkTimePlan::tableName(),
                    [
                        'person_id',
                        'working_shift_id',
                        'date',
                        'work_hours',
                        'lunch_time',
                        'start_at',
                        'end_at',
                        'created_at',
                        'updated_at',
                    ],
                    $data
                )->execute();
            }
            return true;
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
