<?php

namespace app\modules\salary\models;

use app\helpers\Utils;
use app\modules\salary\models\query\PersonImportQuery;
use Yii;
use app\components\db\ActiveRecordLogUpdateTime;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * Class PersonImport
 * @package app\modules\salary\models
 * @property integer $id
 * @property string $file_name
 * @property string $original_file_name
 * @property integer $office_id
 * @property integer $row_begin
 * @property integer $cell_id
 * @property integer $cell_login
 * @property integer $cell_full_name
 * @property integer $cell_country
 * @property integer $cell_position
 * @property integer $cell_salary
 * @property integer $cell_salary_usd
 * @property integer $cell_salary_hourly
 * @property integer $cell_salary_hourly_usd
 * @property integer $cell_passport
 * @property integer $cell_head
 * @property integer $cell_skype
 * @property integer $cell_phone
 * @property integer $cell_email
 * @property integer $cell_employment_date
 * @property integer $cell_termination_reason
 * @property integer $cell_termination_date
 * @property integer $cell_account_number
 * @property string $position_map
 * @property string $country_map
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $status
 */
class PersonImport extends ActiveRecordLogUpdateTime
{
    const STATUS_PENDING = 0;
    const STATUS_IMPORTED = 1;

    public $loadFile;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%salary_person_import}}';
    }

    /**
     * @return PersonImportQuery
     */
    public static function find()
    {
        return new PersonImportQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['loadFile'], 'file', 'checkExtensionByMimeType' => false, 'extensions' => 'xls, xlsx'],
            [['file_name', 'status'], 'required'],
            [['row_begin', 'cell_full_name'], 'required'],
            ['cell_country', 'required', 'when' => function ($model) {return $model->cell_login || $model->cell_id;}, 'message' => Yii::t('common', 'Необходимо заполнить «{attribute}», для идентификации пользователя'), 'whenClient' => "function (attribute, value) {return $('#{$this->formName(true)}-cell_login').val() || $('#{$this->formName(true)}-cell_id').val();}"],
            [['office_id', 'status', 'row_begin', 'cell_id', 'cell_login',  'cell_full_name', 'cell_country', 'cell_position', 'cell_salary_hourly_usd', 'cell_salary_hourly', 'cell_salary_usd', 'cell_salary', 'cell_passport', 'cell_head', 'cell_email', 'cell_skype', 'cell_phone', 'cell_employment_date', 'cell_termination_date', 'cell_termination_reason',  'cell_account_number', 'created_at', 'updated_at'], 'integer'],
            ['office_id', 'exist', 'targetClass' => Office::className(), 'targetAttribute' => 'id'],
            [['position_map', 'country_map'], 'string'],
            [['original_file_name', 'file_name'], 'string', 'max' => 200],
        ];
    }

    /**
     * @param boolean $prepareDir
     * @return string
     */
    public function getUploadPath($prepareDir = false)
    {
        $dir = Yii::getAlias('@uploads') . DIRECTORY_SEPARATOR . 'call-center-person-import';

        if ($prepareDir) {
            Utils::prepareDir($dir);
        }

        return $dir;
    }

    /**
     * @return boolean
     */
    public function upload()
    {
        $this->loadFile = UploadedFile::getInstance($this, 'loadFile');
        if ($this->loadFile) {
            $filename = $this->getUploadPath(true) . DIRECTORY_SEPARATOR . time() . '.' . $this->loadFile->extension;
            if ($this->loadFile->saveAs($filename)) {
                $this->file_name = $filename;
                $this->original_file_name = $this->loadFile->name;
                return true;
            }
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'file_name' => Yii::t('common', 'Имя файла'),
            'original_file_name' => Yii::t('common', 'Имя файла'),
            'office_id' => Yii::t('common', 'Офис'),
            'row_begin' => Yii::t('common', 'Строка начала данных в файле'),
            'cell_id' => Yii::t('common', 'ID: столбец в файле'),
            'cell_login' => Yii::t('common', 'Логин: столбец в файле'),
            'cell_full_name' => Yii::t('common', 'ФИО: столбец в файле'),
            'cell_country' => Yii::t('common', 'Страна: столбец в файле'),
            'cell_position' => Yii::t('common', 'Должность: столбец в файле'),
            'cell_salary' => Yii::t('common', 'Зарплата за месяц: столбец в файле'),
            'cell_salary_usd' => Yii::t('common', 'Зарплата за месяц(USD): столбец в файле'),
            'cell_salary_hourly' => Yii::t('common', 'Зарплата за час: столбец в файле'),
            'cell_salary_hourly_usd' => Yii::t('common', 'Зарплата за час(USD): столбец в файле'),
            'cell_passport' => Yii::t('common', 'Паспорт: столбец в файле'),
            'cell_head' => Yii::t('common', 'Руководитель: столбец в файле'),
            'cell_email' => Yii::t('common', 'E-mail: столбец в файле'),
            'cell_skype' => Yii::t('common', 'Skype: столбец в файле'),
            'cell_phone' => Yii::t('common', 'Телефон: столбец в файле'),
            'cell_employment_date' => Yii::t('common', 'Дата трудоустройства: столбец в файле'),
            'cell_termination_date' => Yii::t('common', 'Дата увольнения: столбец в файле'),
            'cell_termination_reason' => Yii::t('common', 'Причина увольнения: столбец в файле'),
            'cell_account_number' => Yii::t('common', 'Номер счета: столбец в файле'),
            'position_map' => Yii::t('common', 'Маппинг должностей'),
            'country_map' => Yii::t('common', 'Маппинг стран'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'status' => Yii::t('common', 'Статус'),
        ];
    }

    public function getOffice()
    {
        return $this->hasOne(Office::className(), ['id' => 'office_id']);
    }

    /**
     * @return array|bool
     */
    public function getPositionMap()
    {
        return ($this->position_map) ? json_decode($this->position_map, true) : [];
    }

    /**
     * @param array $map
     */
    public function setPositionMap($map)
    {
        $this->position_map = ($map) ? json_encode($map) : '';
    }

    /**
     * @return array|bool
     */
    public function getCountryMap()
    {
        return ($this->country_map) ? json_decode($this->country_map, true) : [];
    }

    /**
     * @param array $map
     */
    public function setCountryMap($map)
    {
        $this->country_map = ($map) ? json_encode($map) : '';
    }

    /**
     * @return array
     */
    public function getColumnFields()
    {
        return [
            'cell_id', 'cell_login', 'cell_full_name', 'cell_country', 'cell_head',
            'cell_position', 'cell_salary', 'cell_salary_usd', 'cell_passport',
            'cell_email', 'cell_skype', 'cell_employment_date', 'cell_termination_date',
            'cell_termination_reason', 'cell_account_number', 'cell_salary_hourly', 'cell_salary_hourly_usd',
        ];
    }

    /**
     * @return array
     */
    public function getColumnMapRef()
    {
        return [
            'cell_id' => 'user_id',
            'cell_login' => 'user_login',
            'cell_full_name' => 'person_full_name',
            'cell_country' => 'country',
            'cell_head' => 'person_head',
            'cell_position'  => 'person_position',
            'cell_salary' => 'person_salary',
            'cell_salary_usd' => 'person_salary_usd',
            'cell_salary_hourly' => 'person_salary_hourly',
            'cell_salary_hourly_usd' => 'person_salary_hourly_usd',
            'cell_passport' => 'person_passport',
            'cell_email' => 'person_email',
            'cell_skype' => 'person_skype',
            'cell_employment_date' => 'person_employment_date',
            'cell_termination_date' => 'person_termination_date',
            'cell_termination_reason' => 'person_termination_reason',
            'cell_account_number' => 'person_account_number',
        ];
    }

    public static function statusList()
    {
        return [
            self::STATUS_PENDING => Yii::t('common', 'Ожидает импорта'),
            self::STATUS_IMPORTED => Yii::t('common', 'Импортирован'),
        ];
    }

    public function getStatusLabel()
    {
        return ArrayHelper::getValue(self::statusList(), $this->status);
    }

    public function isImported()
    {
        return $this->status == self::STATUS_IMPORTED;
    }

    public function formName($lower = false)
    {
        return $lower ? strtolower(parent::formName()) : parent::formName();
    }
}