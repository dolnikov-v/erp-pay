<?php

namespace app\modules\salary\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\Country;
use app\models\Currency;
use app\modules\callcenter\models\CallCenter;
use app\modules\callcenter\models\CallCenterToOffice;
use app\modules\callcenter\models\CallCenterUser;
use app\modules\callcenter\models\CallCenterWorkTime;
use app\modules\salary\models\query\OfficeQuery;
use Yii;
use yii\db\Query;

/**
 * This is the model class for table "salary_office".
 *
 * @property integer $id
 * @property integer $country_id
 * @property string $name
 * @property integer $active
 * @property string $type
 * @property integer $working_hours_per_week
 * @property integer $calc_salary_local
 * @property integer $calc_salary_usd
 * @property double $min_salary_local
 * @property double $min_salary_usd
 * @property double $holidays_coefficient
 * @property double $extra_days_coefficient
 * @property double $holiday_hours
 * @property integer $crocotime_parent_group_id
 * @property integer $count_fixed_norm
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $work_time
 * @property integer $currency_id
 * @property string $birthday_greeting_card_template_subject
 * @property string $birthday_greeting_card_template_message
 * @property integer $auto_penalty
 * @property string $address
 * @property double $expense_rent
 * @property double $expense_account_services
 * @property double $expense_internet
 * @property double $expense_stationery
 * @property double $expense_utilities
 * @property double $expense_tax
 * @property double $expense_office_supplies
 * @property double $expense_office_expenses
 * @property double $expense_telephony_office
 * @property double $expense_security
 * @property double $expense_other
 * @property double $expense_cleaning
 * @property double $expense_electricity
 * @property double $expense_legal_expenses
 * @property double $expense_courier_services
 * @property double $expense_it_support
 * @property double $expense_legal_service
 * @property double $expense_bank_fees
 * @property double $expense_representation
 * @property integer $expense_rent_currency_id
 * @property integer $expense_account_services_currency_id
 * @property integer $expense_internet_currency_id
 * @property integer $expense_stationery_currency_id
 * @property integer $expense_utilities_currency_id
 * @property integer $expense_tax_currency_id
 * @property integer $expense_office_supplies_currency_id
 * @property integer $expense_office_expenses_currency_id
 * @property integer $expense_telephony_office_currency_id
 * @property integer $expense_security_currency_id
 * @property integer $expense_other_currency_id
 * @property integer $expense_cleaning_currency_id
 * @property integer $expense_electricity_currency_id
 * @property integer $expense_legal_expenses_currency_id
 * @property integer $expense_courier_services_currency_id
 * @property integer $expense_it_support_currency_id
 * @property integer $expense_legal_service_currency_id
 * @property integer $expense_bank_fees_currency_id
 * @property integer $expense_representation_currency_id
 * @property integer $jobs_number
 * @property integer $hide_staffing_salary
 * @property integer $technical_equipment_mikrotik
 * @property integer $technical_equipment_video
 * @property integer $technical_equipment_internet
 * @property integer $norm_hours_per_workplace
 *
 * @property Country $country
 * @property CallCenter[] $callCenters
 * @property Person[] $people
 * @property WorkingShift[] $workingShifts
 * @property Currency $currency
 * @property array $workTime
 * @property \yii\db\ActiveQuery $callCenterToOffices
 * @property OfficeTax[] $taxes
 * @property Staffing[] $staffing
 * @property OfficeExpense[] $officeExpenses
 */
class Office extends ActiveRecordLogUpdateTime
{
    protected $insertLog = true;
    protected $updateLog = true;

    const TYPE_CALL_CENTER = 'call_center';
    const TYPE_STORAGE = 'storage';
    const TYPE_BRANCH = 'branch';
    const TYPE_OFFICE = 'office';

    const MOSCOW_ID = 32;
    const NSK_ID = 41;

    /**
     * @var float
     */
    protected $todayWorkHoursOperators;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%salary_office}}';
    }

    /**
     * @return OfficeQuery
     */
    public static function find()
    {
        return new OfficeQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'country_id'], 'required'],
            [
                [
                    'country_id',
                    'active',
                    'working_hours_per_week',
                    'calc_salary_local',
                    'calc_salary_usd',
                    'crocotime_parent_group_id',
                    'count_fixed_norm',
                    'created_at',
                    'updated_at',
                    'currency_id',
                    'auto_penalty',
                    'jobs_number',
                    'hide_staffing_salary',
                    'technical_equipment_mikrotik',
                    'technical_equipment_video',
                    'technical_equipment_internet',
                    'norm_hours_per_workplace',
                    'expense_rent_currency_id',
                    'expense_account_services_currency_id',
                    'expense_internet_currency_id',
                    'expense_stationery_currency_id',
                    'expense_utilities_currency_id',
                    'expense_tax_currency_id',
                    'expense_office_supplies_currency_id',
                    'expense_office_expenses_currency_id',
                    'expense_telephony_office_currency_id',
                    'expense_security_currency_id',
                    'expense_other_currency_id',
                    'expense_cleaning_currency_id',
                    'expense_electricity_currency_id',
                    'expense_legal_expenses_currency_id',
                    'expense_courier_services_currency_id',
                    'expense_it_support_currency_id',
                    'expense_legal_service_currency_id',
                    'expense_bank_fees_currency_id',
                    'expense_representation_currency_id',
                ],
                'integer'
            ],
            [['birthday_greeting_card_template_message'], 'string'],
            [
                ['name', 'type', 'work_time', 'birthday_greeting_card_template_subject', 'address'],
                'string',
                'max' => 255
            ],
            [['active'], 'default', 'value' => 1],
            [['calc_salary_local'], 'default', 'value' => 1],
            [['min_salary_local', 'min_salary_usd', 'holiday_hours'], 'number'],
            [['holidays_coefficient'], 'default', 'value' => 2],
            [['holiday_hours'], 'default', 'value' => 9],
            [['extra_days_coefficient'], 'default', 'value' => 1],
            [
                [
                    'auto_penalty',
                    'hide_staffing_salary',
                    'technical_equipment_mikrotik',
                    'technical_equipment_video',
                    'technical_equipment_internet'
                ],
                'default',
                'value' => 0
            ],
            [['holidays_coefficient', 'extra_days_coefficient'], 'number', 'min' => 0],
            [
                ['country_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Country::className(),
                'targetAttribute' => ['country_id' => 'id']
            ],
            [
                ['currency_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Currency::className(),
                'targetAttribute' => ['currency_id' => 'id']
            ],
            [
                [
                    'expense_rent',
                    'expense_account_services',
                    'expense_internet',
                    'expense_stationery',
                    'expense_utilities',
                    'expense_tax',
                    'expense_office_supplies',
                    'expense_office_expenses',
                    'expense_telephony_office',
                    'expense_security',
                    'expense_other',
                    'expense_cleaning',
                    'expense_electricity',
                    'expense_legal_expenses',
                    'expense_courier_services',
                    'expense_it_support',
                    'expense_legal_service',
                    'expense_bank_fees',
                    'expense_representation',
                ],
                'double'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'country_id' => Yii::t('common', 'Фактическая страна'),
            'name' => Yii::t('common', 'Название'),
            'active' => Yii::t('common', 'Доступность'),
            'type' => Yii::t('common', 'Тип'),
            'working_hours_per_week' => Yii::t('common', 'Количество рабочих часов в неделю по КЗоТ'),
            'calc_salary_local' => Yii::t('common', 'Считать ЗП в местной валюте'),
            'calc_salary_usd' => Yii::t('common', 'Считать ЗП в USD'),
            'min_salary_local' => Yii::t('common', 'Минимальный размер оплаты труда в местной валюте'),
            'min_salary_usd' => Yii::t('common', 'Минимальный размер оплаты труда в USD'),
            'holidays_coefficient' => Yii::t('common', 'Множитель ЗП для работы в праздники'),
            'extra_days_coefficient' => Yii::t('common', 'Множитель ЗП для переработки'),
            'holiday_hours' => Yii::t('common', 'Часы для вычета из КЗоТ в праздничные дни'),
            'crocotime_parent_group_id' => Yii::t('common', 'Номер в crocotime'),
            'count_fixed_norm' => Yii::t('common', 'Рассчитывать по фикс норме часов'),
            'created_at' => Yii::t('common', 'Дата добавления'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
            'work_time' => Yii::t('common', 'Время работы (GMT+0)'),
            'currency_id' => Yii::t('common', 'Валюта'),
            'birthday_greeting_card_template_subject' => Yii::t('common', 'Тема'),
            'birthday_greeting_card_template_message' => Yii::t('common', 'Текст'),
            'auto_penalty' => Yii::t('common', 'Включить автоштрафы'),
            'address' => Yii::t('common', 'Адрес'),

            'expense_rent' => Yii::t('common', 'Аренда'),
            'expense_account_services' => Yii::t('common', 'Бухгалтерские услуги'),
            'expense_internet' => Yii::t('common', 'Интернет'),
            'expense_stationery' => Yii::t('common', 'Канц товары'),
            'expense_utilities' => Yii::t('common', 'Коммунальные услуги'),
            'expense_tax' => Yii::t('common', 'Налоги'),
            'expense_office_supplies' => Yii::t('common', 'Питьевая вода'),
            'expense_office_expenses' => Yii::t('common', 'Офисные расходы'),
            'expense_telephony_office' => Yii::t('common', 'Офисный телефон'),
            'expense_security' => Yii::t('common', 'Охрана'),
            'expense_other' => Yii::t('common', 'Кухонные принадлежности, продукты'),
            'expense_cleaning' => Yii::t('common', 'Уборка'),
            'expense_electricity' => Yii::t('common', 'Электричество'),
            'expense_legal_expenses' => Yii::t('common', 'Юридические услуги'),
            'expense_courier_services' => Yii::t('common', 'Курьерская доставка'),
            'expense_it_support' => Yii::t('common', 'IT поддержка'),
            'expense_legal_service' => Yii::t('common', 'Услуга легального представителя'),
            'expense_bank_fees' => Yii::t('common', 'Банковские комиссии'),
            'expense_representation' => Yii::t('common', 'Представительские расходы'),

            'expense_rent_currency_id' => Yii::t('common', 'Валюта'),
            'expense_account_services_currency_id' => Yii::t('common', 'Валюта'),
            'expense_internet_currency_id' => Yii::t('common', 'Валюта'),
            'expense_stationery_currency_id' => Yii::t('common', 'Валюта'),
            'expense_utilities_currency_id' => Yii::t('common', 'Валюта'),
            'expense_tax_currency_id' => Yii::t('common', 'Валюта'),
            'expense_office_supplies_currency_id' => Yii::t('common', 'Валюта'),
            'expense_office_expenses_currency_id' => Yii::t('common', 'Валюта'),
            'expense_telephony_office_currency_id' => Yii::t('common', 'Валюта'),
            'expense_security_currency_id' => Yii::t('common', 'Валюта'),
            'expense_other_currency_id' => Yii::t('common', 'Валюта'),
            'expense_cleaning_currency_id' => Yii::t('common', 'Валюта'),
            'expense_electricity_currency_id' => Yii::t('common', 'Валюта'),
            'expense_legal_expenses_currency_id' => Yii::t('common', 'Валюта'),
            'expense_courier_services_currency_id' => Yii::t('common', 'Валюта'),
            'expense_it_support_currency_id' => Yii::t('common', 'Валюта'),
            'expense_legal_service_currency_id' => Yii::t('common', 'Валюта'),
            'expense_bank_fees_currency_id' => Yii::t('common', 'Валюта'),
            'expense_representation_currency_id' => Yii::t('common', 'Валюта'),

            'jobs_number' => Yii::t('common', 'Количество рабочих мест'),
            'hide_staffing_salary' => Yii::t('common', 'Скрывать оклады в штатном расписании'),
            'technical_equipment_mikrotik' => Yii::t('common', 'Оборудован микротиком'),
            'technical_equipment_video' => Yii::t('common', 'Оборудован видеокамерами'),
            'technical_equipment_internet' => Yii::t('common', 'Оборудован 2 линиями интернета'),
            'norm_hours_per_workplace' => Yii::t('common', 'Норма часов на одно рабочее место'),
        ];
    }

    /**
     * @return array
     */
    public static function getTypes()
    {
        return [
            self::TYPE_CALL_CENTER => Yii::t('common', 'Колл-центр'),
            self::TYPE_STORAGE => Yii::t('common', 'Склад'),
            self::TYPE_BRANCH => Yii::t('common', 'Branch'),
            self::TYPE_OFFICE => Yii::t('common', 'Офис'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCallCenters()
    {
        return $this->hasMany(CallCenter::className(), ['id' => 'call_center_id'])
            ->via('callCenterToOffices');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeople()
    {
        return $this->hasMany(Person::className(), ['office_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCallCenterToOffices()
    {
        return $this->hasMany(CallCenterToOffice::className(), ['office_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkingShifts()
    {
        return $this->hasMany(WorkingShift::className(), ['office_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaxes()
    {
        return $this->hasMany(OfficeTax::className(), ['office_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStaffing()
    {
        return $this->hasMany(Staffing::className(), ['office_id' => 'id']);
    }

    /**
     * @param $ids
     * @return Office[]
     */
    public static function findBoundOffices($ids)
    {
        return self::find()
            ->where(['crocotime_parent_group_id' => $ids])
            ->asArray()
            ->indexBy('crocotime_parent_group_id')
            ->all();
    }

    /**
     * Returns timeZone name by crocotime_parent_group_id
     * @param $officeId
     * @return string
     */
    public static function getTimezoneByCrocotimeOffice($officeId)
    {
        return Office::findOne(['crocotime_parent_group_id' => $officeId])->country->timezone->timezone_id;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfficeExpenses()
    {
        return $this->hasMany(OfficeExpense::className(), ['office_id' => 'id']);
    }

    /**
     * @return array
     */
    public function getWorkTime()
    {
        return json_decode($this->work_time, true);
    }

    /**
     * @param $time
     */
    public function setWorkTime($time)
    {
        $this->work_time = json_encode($time);
    }

    /**
     * @return array
     */
    public static function getWeekDays()
    {
        return [
            Yii::t('common', 'Воскресенье'),
            Yii::t('common', 'Понедельник'),
            Yii::t('common', 'Вторник'),
            Yii::t('common', 'Среда'),
            Yii::t('common', 'Четверг'),
            Yii::t('common', 'Пятница'),
            Yii::t('common', 'Суббота'),
        ];
    }

    /***
     * @return array
     */
    public static function expensesFields()
    {
        return [
            'expense_rent',
            'expense_account_services',
            'expense_internet',
            'expense_stationery',
            'expense_utilities',
            'expense_tax',
            'expense_office_supplies',
            'expense_office_expenses',
            'expense_telephony_office',
            'expense_security',
            'expense_other',
            'expense_cleaning',
            'expense_electricity',
            'expense_legal_expenses',
            'expense_courier_services',
            'expense_it_support',
            'expense_legal_service',
            'expense_bank_fees',
            'expense_representation',
        ];
    }

    /***
     * @return array
     */
    public static function getExpensesFieldsList()
    {
        $return = [];

        $office = new self();
        $labels = $office->attributeLabels();

        foreach (self::expensesFields() as $key) {
            $return[$key] = $labels[$key];
        }
        return $return;
    }

    /***
     * @param $field
     * @param null $dateFrom
     * @param null $dateTo
     * @return float|int|mixed
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    public function getExpenseValue($field, $dateFrom = null, $dateTo = null)
    {
        if (!$this->hasProperty($field)) {
            return 0;
        }
        $officeExpense = OfficeExpense::find()
            ->byOfficeId($this->id)
            ->byType($field)
            ->byDates($dateFrom, $dateTo)
            ->one();

        if ($officeExpense) {
            $expenseValue = $officeExpense->expense;
            $expenseCurrency = $officeExpense->currency;
        } else {
            $expenseValue = $this->$field;
            if (!$this->hasProperty($field. '_currency_id')) {
                return 0;
            }
            $expenseCurrency = Currency::findOne($this->{$field. '_currency_id'});
        }

        $rate = Currency::find()->convertValueToCurrency(1, Currency::getUSD()->id, $expenseCurrency->id, $dateTo);
        if (!$rate) {
            $rate = $expenseCurrency->currencyRate->rate ?? 1;
        }

        return $expenseValue / $rate;
    }

    /**
     * @return bool
     */
    public function isLinkedWithCrocotime()
    {
        return $this->crocotime_parent_group_id || false;
    }


    /**
     * @return integer
     */
    private function getWorkTimeFrom()
    {
        return $this->workTime['from'] ?? 0;
    }

    /**
     * @param bool $nextDay
     * @return int|mixed
     */
    private function getWorkTimeTo($nextDay = true)
    {
        $workTimeTo = $this->workTime['to'] ?? 0;
        // если рабочий день заканчивается уже в след сутках
        if ($nextDay && $this->getWorkTimeFrom() > $workTimeTo) {
            $workTimeTo += 24 * 60 * 60;
        }
        return $workTimeTo;
    }

    /**
     * офис работает $timeOffset минут и раб день еще не закончился
     * @param integer $timeOffsetAfterStartWork
     * @param integer $timeOffsetAfterEndWork
     * @return boolean
     */
    private function validateWorkTime($timeOffsetAfterStartWork = 0, $timeOffsetAfterEndWork = 0)
    {
        $workTimeFrom = $this->getWorkTimeFrom();
        $workTimeTo = $this->getWorkTimeTo();

        $currentTime = time() - strtotime("midnight");

        if ($currentTime >= ($workTimeFrom + $timeOffsetAfterStartWork * 60) && $currentTime <= ($workTimeTo + $timeOffsetAfterEndWork * 60)) {
            return true;
        }
        return false;
    }

    /**
     * @return array
     */
    private function getWorkDays()
    {
        return $this->workTime['days'] ?? [];
    }

    /**
     * Офис работает по определенным дням
     * @return boolean
     */
    private function validateWorkDay()
    {
        $dateTime = $this->getDateTime();
        $currentDay = $dateTime->format('w');

        $days = $this->getWorkDays();
        if ($days) {
            if (in_array($currentDay, $days)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Офис не работает в праздники
     * @return bool
     */
    private function validateHoliday()
    {
        $dateTime = $this->getDateTime();
        if (Holiday::find()
            ->where(['date' => $dateTime->format('Y-m-d'), 'country_id' => $this->country_id])
            ->exists()
        ) {
            return false;
        }
        return true;
    }


    /**
     * @return \DateTime
     */
    private function getDateTime()
    {
        return new \DateTime('now', new \DateTimeZone($this->country->timezone->timezone_id));
    }

    /**
     * Офисы которые работают 60 минут
     *
     * @param integer $timeOffsetAfterStartWork
     * @param integer $timeOffsetAfterEndWork
     * @param bool $useHolidays учитывать праздники
     * @return Office[]
     */
    public static function getListWorkingOffices($timeOffsetAfterStartWork = 60, $timeOffsetAfterEndWork = 0, $useHolidays = true)
    {
        $return = [];

        $offices = self::find()
            ->with(['callCenters.country', 'country.timezone'])
            ->active()
            ->andWhere(['type' => Office::TYPE_CALL_CENTER])
            ->all();

        foreach ($offices as $office) {
            if (!isset($office->country->timezone->timezone_id)) {
                continue;
            }

            if (!empty($office->workTime)) {

                if (!$office->validateWorkTime($timeOffsetAfterStartWork, $timeOffsetAfterEndWork)) {
                    continue;
                }

                if (!$office->validateWorkDay()) {
                    continue;
                }
            }

            if ($useHolidays && !$office->validateHoliday()) {
                continue;
            }

            $return[] = $office;
        }

        return $return;
    }


    /**
     * @return int
     */
    private function getTimeOffset()
    {
        return isset($this->country->timezone->time_offset) ? ($this->country->timezone->time_offset) : 0;
    }

    /**
     * @return string
     */
    private function getTimeZone()
    {
        $timeOffset = $this->getTimeOffset();
        return 'GMT' . ($timeOffset < 0 ? '' : '+') . +$timeOffset / 3600;
    }

    /**
     * Режим работы офиса с локальным часовым поясом 10:00-22:00 GMT+8
     * @return string
     */
    public function getWorkingHours()
    {
        return Yii::$app->formatter->asTime($this->getWorkTimeFrom() + $this->getTimeOffset(), 'php:H:i') . '-' .
            Yii::$app->formatter->asTime($this->getWorkTimeTo(false) + $this->getTimeOffset(), 'php:H:i') . ' ' . $this->getTimeZone();

    }

    /**
     * @return int
     */
    public function getJobsNumber()
    {
        return $this->jobs_number ?? 0;
    }

    /**
     * @return int
     */
    public static function getTodaySecondsPast()
    {
        $GMT = new \DateTimeZone('GMT');
        $dateTimeNow = new \DateTime('now', $GMT);
        $dateTimeToday = new \DateTime('today midnight', $GMT);
        return $dateTimeNow->getTimestamp() - $dateTimeToday->getTimestamp();
    }

    /**
     * @param bool $takeActual
     * @return float|int
     */
    public function getNormHoursPerWorkplace($takeActual = true)
    {
        $norm = $this->norm_hours_per_workplace ?? 0;
        if ($takeActual) {
            $workTimePast = round((self::getTodaySecondsPast() - $this->getWorkTimeFrom()) / 3600, 1);
            if ($workTimePast < $norm) {
                return $workTimePast;
            }
        }
        return $norm;
    }

    /**
     * @return int
     */
    public function getStaffingOperatorsNumber()
    {
        return Staffing::find()
            ->select(['max_persons'])
            ->joinWith('designation')
            ->byOfficeId($this->id)
            ->isOperator()
            ->scalar();
    }

    /**
     * Часы сегодня работающих операторов
     * В старом КЦ у нас все логины считаются, в новом только один
     * @return string
     */
    public function getTodayWorkHoursOperators()
    {
        if (!is_null($this->todayWorkHoursOperators)) {
            return $this->todayWorkHoursOperators;
        }

        $oldCallCenterHours = CallCenterWorkTime::find()
            ->select(['hours' => 'SUM(time)/3600'])
            ->joinWith([
                'callCenterUser',
                'callCenterUser.callCenter',
                'callCenterUser.person',
                'callCenterUser.person.designation'
            ])
            ->where([CallCenterWorkTime::tableName() . '.date' => date('Y-m-d')])
            ->andWhere(['>', CallCenterWorkTime::tableName() . '.time', 0])
            ->andWhere([Person::tableName() . '.office_id' => $this->id])
            ->andWhere([Designation::tableName() . '.calc_work_time' => 1])
            ->andWhere([Person::tableName() . '.active' => 1])
            ->andWhere([CallCenter::tableName() . '.is_amazon_query' => 0])
            ->scalar();

        $subQuery = CallCenterWorkTime::find()
            ->select([CallCenterWorkTime::tableName() . '.time'])
            ->joinWith([
                'callCenterUser',
                'callCenterUser.callCenter',
                'callCenterUser.person',
                'callCenterUser.person.designation'
            ])
            ->where([CallCenterWorkTime::tableName() . '.date' => date('Y-m-d')])
            ->andWhere(['>', CallCenterWorkTime::tableName() . '.time', 0])
            ->andWhere([Person::tableName() . '.office_id' => $this->id])
            ->andWhere([Designation::tableName() . '.calc_work_time' => 1])
            ->andWhere([Person::tableName() . '.active' => 1])
            ->andWhere([CallCenter::tableName() . '.is_amazon_query' => 1])
            ->groupBy([CallCenterUser::tableName() . '.user_id']);

        $query = new Query();
        $newCallCenterHours = $query
            ->select(['hours' => 'SUM(time)/3600'])
            ->from(['s' => $subQuery])
            ->scalar();

        $this->todayWorkHoursOperators = $oldCallCenterHours + $newCallCenterHours;
        return $this->todayWorkHoursOperators;
    }
}
