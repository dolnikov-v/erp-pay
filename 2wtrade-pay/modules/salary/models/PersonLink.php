<?php

namespace app\modules\salary\models;

use app\modules\salary\models\query\PersonLinkQuery;
use Yii;

/**
 * This is the model class for table "salary_person_link".
 *
 * @property integer $id
 * @property integer $person_id
 * @property string $type
 * @property string $foreign_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Person $person
 */
class PersonLink extends \app\components\db\ActiveRecordLogUpdateTime
{

    const TYPE_CROCOTIME = 'crocotime';
    const TYPE_CRM = '2wcall';
    const TYPE_ERP = 'erp';
    const TYPE_MERCURY = 'mercury';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%salary_person_link}}';
    }

    /**
     * @return PersonLinkQuery
     */
    public static function find()
    {
        return new PersonLinkQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['person_id', 'foreign_id'], 'required'],
            [['person_id', 'created_at', 'updated_at'], 'integer'],
            [['type', 'foreign_id'], 'string', 'max' => 255],
            [['person_id'], 'exist', 'skipOnError' => true, 'targetClass' => Person::className(), 'targetAttribute' => ['person_id' => 'id']],
            ['type', 'in', 'range' => array_keys(self::getTypeCollection())],
            [['foreign_id'], 'unique', 'targetAttribute' => ['person_id', 'type', 'foreign_id']],
        ];
    }

    /**
     * @return array
     */
    public static function getTypeCollection()
    {
        return [
            self::TYPE_CROCOTIME => Yii::t('common', 'CrocoTime'),
            self::TYPE_CRM => Yii::t('common', 'CRM'),
            self::TYPE_ERP => Yii::t('common', 'ERP'),
            self::TYPE_MERCURY => Yii::t('common', 'Mercury'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'person_id' => Yii::t('common', 'Сотрудник'),
            'type' => Yii::t('common', 'Тип'),
            'foreign_id' => Yii::t('common', 'Внешний номер'),
            'created_at' => Yii::t('common', 'Дата добавления'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerson()
    {
        return $this->hasOne(Person::className(), ['id' => 'person_id']);
    }
}