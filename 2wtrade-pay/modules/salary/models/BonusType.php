<?php

namespace app\modules\salary\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\User;
use app\modules\salary\models\query\BonusTypeQuery;
use Yii;

/**
 * This is the model class for table "salary_bonus_type".
 *
 * @property integer $id
 * @property integer $office_id
 * @property string $name
 * @property integer $type
 * @property double $sum
 * @property double $sum_local
 * @property integer $percent
 * @property integer $active
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $is_dismissal_pay
 *
 * @property User $createdByUser
 * @property User $updatedByUser
 * @property Office $office
 */
class BonusType extends ActiveRecordLogUpdateTime
{
    const BONUS_TYPE_SUM = 'sum';
    const BONUS_TYPE_PERCENT = 'percent';
    const BONUS_TYPE_OTHER = 'other';
    const BONUS_TYPE_HOUR = 'hour';
    const BONUS_TYPE_PERCENT_CALC_SALARY = 'percent_calc_salary';
    const BONUS_TYPE_PERCENT_WORK_HOURS = 'percent_work_hours';
    const BONUS_TYPE_SUM_WORK_HOURS = 'sum_work_hours';
    const BONUS_TYPE_13_SALARY = '13_salary';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%salary_bonus_type}}';
    }

    /**
     * @return BonusTypeQuery
     */
    public static function find()
    {
        return new BonusTypeQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'type'], 'required'],
            [['sum', 'sum_local'], 'number'],
            [['office_id', 'percent', 'active', 'created_by', 'updated_by', 'created_at', 'updated_at', 'is_dismissal_pay'], 'integer'],
            [['name', 'type'], 'string', 'max' => 255],
            [['active'], 'default', 'value' => 1],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
            ['sum', 'required', 'when' => function ($model) {
                return
                    (
                        $model->type == self::BONUS_TYPE_SUM
                    );
            },
            'whenClient' => "function (attribute, value) {
                return $('#bonustype-type').val() == 'sum';
            }"],
            [['sum', 'sum_local'], 'required', 'when' => function ($model) {
                return
                    (
                        $model->type == self::BONUS_TYPE_SUM_WORK_HOURS &&
                        !($model->sum || $model->sum_local)
                    );
            },
            'whenClient' => "function (attribute, value) {
                return ($('#bonustype-type').val() == 'sum_local' && !($('#bonustype-sum').val() > 0 || $('#bonustype-sum_local').val() > 0));
            }"],
            ['percent', 'required', 'when' => function ($model) {
                return
                    (
                        $model->type == self::BONUS_TYPE_PERCENT ||
                        $model->type == self::BONUS_TYPE_PERCENT_CALC_SALARY ||
                        $model->type == self::BONUS_TYPE_PERCENT_WORK_HOURS
                    );
            }, 'whenClient' => "function (attribute, value) {
                return 
                    (
                        $('#bonustype-type').val() == 'percent'
                        $('#bonustype-type').val() == 'percent_calc_salary'
                        $('#bonustype-type').val() == 'percent_work_hours'
                    );
            }"],
            [['office_id'], 'exist', 'skipOnError' => true, 'targetClass' => Office::className(), 'targetAttribute' => ['office_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'office_id' => Yii::t('common', 'Офис'),
            'name' => Yii::t('common', 'Название'),
            'type' => Yii::t('common', 'Тип бонуса'),
            'percent' => Yii::t('common', 'Процент бонуса'),
            'sum' => Yii::t('common', 'Сумма бонуса в USD'),
            'sum_local' => Yii::t('common', 'Сумма бонуса в местной валюте'),
            'active' => Yii::t('common', 'Активность'),
            'created_by' => Yii::t('common', 'Кто создал'),
            'updated_by' => Yii::t('common', 'Кто изменил'),
            'created_at' => Yii::t('common', 'Дата добавления'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
            'is_dismissal_pay' => Yii::t('common', 'Выходное пособие'),
        ];
    }

    /**
     * @return array
     */
    public static function getTypes()
    {
        return [
            self::BONUS_TYPE_SUM => Yii::t('common', 'Фиксированная сумма'),
            self::BONUS_TYPE_HOUR => Yii::t('common', 'Дополнительные часы'),
            self::BONUS_TYPE_PERCENT => Yii::t('common', 'Процент от оклада'),
            self::BONUS_TYPE_PERCENT_CALC_SALARY => Yii::t('common', 'Процент от расчетной ЗП'),
            self::BONUS_TYPE_PERCENT_WORK_HOURS => Yii::t('common', 'Процент от отработанных часов'),
            self::BONUS_TYPE_SUM_WORK_HOURS => Yii::t('common', 'Расчет от количества отработанных дней (сумма бонуса в день)'),
            self::BONUS_TYPE_13_SALARY => Yii::t('common', 'Процент от 13-ой ЗП: оклад +  надбавка (пропорционально отработанным дням в году)'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedByUser()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedByUser()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOffice()
    {
        return $this->hasOne(Office::className(), ['id' => 'office_id']);
    }
}