<?php

namespace app\modules\salary\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\modules\salary\models\query\PersonGreetingCardQuery;
use Yii;

/**
 * This is the model class for table "salary_person_greeting_card".
 *
 * @property integer $id
 * @property integer $person_id
 * @property string $email_to
 * @property string $subject
 * @property string $message
 * @property string $status
 * @property integer $send_at
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Person $person
 */
class PersonGreetingCard extends ActiveRecordLogUpdateTime
{
    const STATUS_PENDING = 'pending';       // ожидает отправки
    const STATUS_SENT = 'sent';             // отправлен
    const STATUS_ERROR = 'error';           // ошибка

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%salary_person_greeting_card}}';
    }

    /**
     * @return PersonGreetingCardQuery
     */
    public static function find()
    {
        return new PersonGreetingCardQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['person_id', 'message', 'subject'], 'required'],
            [['person_id', 'send_at', 'created_at', 'updated_at'], 'integer'],
            [['message'], 'string'],
            [['email_to', 'subject', 'status'], 'string', 'max' => 255],
            [['person_id'], 'exist', 'skipOnError' => true, 'targetClass' => Person::className(), 'targetAttribute' => ['person_id' => 'id']],
            ['status', 'default', 'value' => self::STATUS_PENDING],
            ['status', 'in', 'range' => array_keys(self::getStatusesCollection())],
        ];
    }

    /**
     * @return array
     */
    public static function getStatusesCollection()
    {
        return [
            self::STATUS_PENDING => Yii::t('common', 'Ожидает отправки'),
            self::STATUS_SENT => Yii::t('common', 'Отправлен'),
            self::STATUS_ERROR => Yii::t('common', 'Ошибка'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'person_id' => Yii::t('common', 'Сотрудник'),
            'email_to' => Yii::t('common', 'Email'),
            'subject' => Yii::t('common', 'Тема'),
            'message' => Yii::t('common', 'Текст'),
            'status' => Yii::t('common', 'Статус'),
            'send_at' => Yii::t('common', 'Дата отправки'),
            'created_at' => Yii::t('common', 'Дата добавления'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerson()
    {
        return $this->hasOne(Person::className(), ['id' => 'person_id']);
    }
}
