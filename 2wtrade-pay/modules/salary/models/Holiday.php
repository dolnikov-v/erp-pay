<?php

namespace app\modules\salary\models;

use app\modules\salary\models\query\HolidayQuery;
use app\components\db\ActiveRecordLogUpdateTime;
use app\models\Country;
use Yii;

/**
 * This is the model class for table "salary_holiday".
 *
 * @property integer $id
 * @property integer $country_id
 * @property string $date
 * @property string $name
 * @property string $greeting_card_template_subject
 * @property string $greeting_card_template_message
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Country $country
 */
class Holiday extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%salary_holiday}}';
    }

    /**
     * @return HolidayQuery
     */
    public static function find()
    {
        return new HolidayQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id'], 'required'],
            [['country_id', 'created_at', 'updated_at'], 'integer'],
            [['date'], 'safe'],
            [['greeting_card_template_message'], 'string'],
            [['name', 'greeting_card_template_subject'], 'string', 'max' => 255],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_id' => 'id']],
            ['date', 'required', 'enableClientValidation' => false],
            [['country_id', 'date'], 'unique', 'targetAttribute' => ['country_id', 'date']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'country_id' => Yii::t('common', 'Страна'),
            'name' => Yii::t('common', 'Название'),
            'date' => Yii::t('common', 'Дата'),
            'greeting_card_template_subject' => Yii::t('common', 'Тема'),
            'greeting_card_template_message' => Yii::t('common', 'Текст'),
            'created_at' => Yii::t('common', 'Дата добавления'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }
}
