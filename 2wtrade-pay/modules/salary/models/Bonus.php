<?php

namespace app\modules\salary\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\CurrencyRate;
use app\models\User;
use app\modules\callcenter\models\CallCenterUser;
use app\modules\callcenter\models\CallCenterWorkTime;
use Yii;

/**
 * This is the model class for table "salary_bonus".
 *
 * @property integer $id
 * @property integer $person_id
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $bonus_type_id
 * @property integer $bonus_percent
 * @property double $bonus_sum_local
 * @property double $bonus_sum_usd
 * @property double $bonus_hour
 * @property double $bonus_percent_hour
 * @property string $comment
 * @property string $date
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property User $createdBy
 * @property User $updatedBy
 * @property BonusType $bonusType
 * @property Person $person
 */
class Bonus extends ActiveRecordLogUpdateTime
{
    public $office_id;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%salary_bonus}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['person_id', 'bonus_type_id'], 'required'],
            [['person_id', 'created_by', 'updated_by', 'bonus_type_id', 'bonus_percent', 'created_at', 'updated_at'], 'integer'],
            [['bonus_sum_local', 'bonus_sum_usd', 'bonus_hour', 'bonus_percent_hour'], 'number'],
            [['comment'], 'string', 'max' => 255],
            ['date', 'safe'],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['bonus_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => BonusType::className(), 'targetAttribute' => ['bonus_type_id' => 'id']],
            [['person_id'], 'exist', 'skipOnError' => true, 'targetClass' => Person::className(), 'targetAttribute' => ['person_id' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
            ['date', 'required', 'enableClientValidation' => false],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'office_id' => Yii::t('common', 'Офис'),
            'person_id' => Yii::t('common', 'Сотрудник'),
            'bonus_type_id' => Yii::t('common', 'Тип бонуса'),
            'bonus_percent' => Yii::t('common', 'Процент бонуса'),
            'bonus_sum_local' => Yii::t('common', 'Сумма бонуса в местной валюте'),
            'bonus_sum_usd' => Yii::t('common', 'Сумма бонуса в USD'),
            'bonus_hour' => Yii::t('common', 'Дополнительные часы'),
            'bonus_percent_hour' => Yii::t('common', 'Процент отработанных часов'),
            'comment' => Yii::t('common', 'Комментарий'),
            'date' => Yii::t('common', 'Дата бонуса'),
            'created_by' => Yii::t('common', 'Кто создал'),
            'updated_by' => Yii::t('common', 'Кто изменил'),
            'created_at' => Yii::t('common', 'Дата добавления'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBonusType()
    {
        return $this->hasOne(BonusType::className(), ['id' => 'bonus_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerson()
    {
        return $this->hasOne(Person::className(), ['id' => 'person_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    public function beforeValidate()
    {
        $this->bonus_sum_local = str_replace(',', '.', $this->bonus_sum_local);
        $this->bonus_sum_usd = str_replace(',', '.', $this->bonus_sum_usd);
        $this->bonus_hour = str_replace(',', '.', $this->bonus_hour);
        $this->bonus_percent_hour = str_replace(',', '.', $this->bonus_percent_hour);

        return parent::beforeValidate();
    }

    public function beforeSave($insert)
    {
        if (is_a(Yii::$app, 'yii\web\Application')) {
            if ($insert == self::EVENT_BEFORE_INSERT) {
                $this->created_by = Yii::$app->user->id;
            }
            $this->updated_by = Yii::$app->user->id;
        }
        $this->date = $this->date ? Yii::$app->formatter->asDate($this->date, 'php:Y-m-d') : null;

        $currencyId = $this->person->office->currency_id ? $this->person->office->currency_id : $this->person->office->country->currency_id;

        switch ($this->bonusType->type) {
            case BonusType::BONUS_TYPE_PERCENT:
                // случай когда бонусуют на процент от оклада, посчитаем конкретную сумму, а процент записывать не будем

                if ($this->bonus_percent) {
                    // если ранее не посчитаны суммы
                    $calcLocal = true;
                    $calcUSD = true;
                    if ($this->id) {
                        $calcLocal = false;
                        $calcUSD = false;
                        if ($this->bonus_percent && !$this->bonus_sum_local) {
                            $calcLocal = true;
                        }
                        if ($this->bonus_percent && !$this->bonus_sum_usd) {
                            $calcUSD = true;
                        }
                    }
                    if ($calcLocal && $this->person->salary_local) {
                        $this->bonus_sum_local = $this->person->salary_local * $this->bonus_percent / 100;
                    }
                    if ($calcUSD && $this->person->salary_usd) {
                        $this->bonus_sum_usd = $this->person->salary_usd * $this->bonus_percent / 100;
                    }

                    if (!$this->bonus_sum_usd) {
                        $this->bonus_sum_usd = self::getUSDSum($this->bonus_sum_local, $currencyId);
                    }
                    if (!$this->bonus_sum_local) {
                        $this->bonus_sum_local = self::getLocalSum($this->bonus_sum_usd, $currencyId);
                    }
                }
                $this->bonus_percent = null;
                $this->bonus_percent_hour = null;
                $this->bonus_hour = null;
                break;
            case BonusType::BONUS_TYPE_SUM:
                if (!$this->bonus_sum_usd) {
                    $this->bonus_sum_usd = self::getUSDSum($this->bonus_sum_local, $currencyId);
                }
                if (!$this->bonus_sum_local) {
                    $this->bonus_sum_local = self::getLocalSum($this->bonus_sum_usd, $currencyId);
                }
                $this->bonus_percent = null;
                $this->bonus_percent_hour = null;
                $this->bonus_hour = null;
                break;
            case BonusType::BONUS_TYPE_SUM_WORK_HOURS:
                $from = date("Y-m-01", strtotime($this->date));
                $to = date("Y-m-t", strtotime($this->date));

                if ($this->person->designation->calc_work_time) {
                    //оператор
                    $count = CallCenterWorkTime::find()
                        ->innerJoin(CallCenterUser::tableName(), CallCenterUser::tableName() . '.id = ' . CallCenterWorkTime::tableName() . '.call_center_user_id')
                        ->where(['person_id' => $this->person_id])
                        ->andWhere('date BETWEEN :start AND :end AND time > 0', [':start' => $from, ':end' => $to])
                        ->groupBy('date')->count();
                } else {
                    $count = count(WorkTimePlan::getPlanList($this->person_id, $from, $to));
                }

                if (!$this->bonus_sum_usd) {
                    $this->bonus_sum_usd = self::getUSDSum($this->bonus_sum_local, $currencyId);
                }
                if (!$this->bonus_sum_local) {
                    $this->bonus_sum_local = self::getLocalSum($this->bonus_sum_usd, $currencyId);
                }
                $this->bonus_sum_usd = $this->bonus_sum_usd * $count;
                $this->bonus_sum_local = $this->bonus_sum_local * $count;
                $this->bonus_percent = null;
                $this->bonus_percent_hour = null;
                $this->bonus_hour = null;
                break;
            case BonusType::BONUS_TYPE_PERCENT_CALC_SALARY:
                // бонус к итоговой ЗП в % храним в $this->bonus_percent
                $this->bonus_sum_usd = null;
                $this->bonus_sum_local = null;
                $this->bonus_hour = null;
                $this->bonus_percent_hour = null;
                break;
            case BonusType::BONUS_TYPE_PERCENT_WORK_HOURS:
                // бонус к итоговым часом в %
                $this->bonus_percent = null;
                $this->bonus_sum_usd = null;
                $this->bonus_sum_local = null;
                $this->bonus_hour = null;
                break;
            case BonusType::BONUS_TYPE_HOUR:
                // бонус к отработанным часам
                $this->bonus_percent = null;
                $this->bonus_sum_usd = null;
                $this->bonus_sum_local = null;
                $this->bonus_percent_hour = null;
                break;

            case BonusType::BONUS_TYPE_13_SALARY:

                $daysWorked = $this->person->getDaysWorked(true);

                $daysInYear = 360; // для расчетов взять такое число дней в году

                if ($this->person->salary_local) {
                    $this->bonus_sum_local = $daysWorked / $daysInYear * ($this->person->salary_local + $this->bonus_sum_local) * $this->bonus_percent / 100;
                }
                if ($this->person->salary_usd) {
                    $this->bonus_sum_usd = $daysWorked / $daysInYear * ($this->person->salary_usd + $this->bonus_sum_usd) * $this->bonus_percent / 100;
                }

                if (!$this->bonus_sum_usd) {
                    $this->bonus_sum_usd = self::getUSDSum($this->bonus_sum_local, $currencyId);
                }
                if (!$this->bonus_sum_local) {
                    $this->bonus_sum_local = self::getLocalSum($this->bonus_sum_usd, $currencyId);
                }
                $this->bonus_percent = null;
                break;
        }

        return parent::beforeSave($insert);
    }

    /***
     * @param float $sumUSD
     * @param integer $currencyID
     * @return float
     */
    public static function getLocalSum($sumUSD, $currencyID)
    {
        if (!$sumUSD) {
            return 0;
        }

        $currencyRate = CurrencyRate::find()
            ->joinWith('currency')
            ->where([CurrencyRate::tableName() . '.currency_id' => $currencyID])
            ->one();

        if (!$currencyRate) {
            return $sumUSD;
        }

        return $sumUSD * $currencyRate->rate;
    }

    /***
     * @param float $sumLocal
     * @param integer $currencyID
     * @return float
     */
    public static function getUSDSum($sumLocal, $currencyID)
    {
        if (!$sumLocal) {
            return 0;
        }

        $currencyRate = CurrencyRate::find()
            ->joinWith('currency')
            ->where([CurrencyRate::tableName() . '.currency_id' => $currencyID])
            ->one();

        if (!$currencyRate) {
            return $sumLocal;
        }

        return $sumLocal / $currencyRate->rate;
    }
}
