<?php

namespace app\modules\salary\models;

use app\helpers\Utils;
use app\models\Country;
use app\modules\callcenter\models\CallCenter;
use app\modules\callcenter\models\CallCenterUser;
use app\modules\salary\models\query\MonthlyStatementQuery;
use app\components\db\ActiveRecordLogUpdateTime;
use Yii;

/**
 * This is the model class for table "salary_monthly_statement".
 *
 * @property integer $id
 * @property string $date_from
 * @property string $date_to
 * @property integer $active
 * @property integer $office_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $closed_at
 * @property integer $closed_by
 * @property integer $sent_at
 *
 * @property Office $office
 * @property MonthlyStatementData[] $monthlyStatementDatas
 */
class MonthlyStatement extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%salary_monthly_statement}}';
    }

    /**
     * @return MonthlyStatementQuery
     */
    public static function find()
    {
        return new MonthlyStatementQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['office_id'], 'required'],
            [['date_from', 'date_to'], 'date', 'format' => 'php:Y-m-d'],
            [['active', 'office_id', 'created_at', 'updated_at', 'closed_at', 'closed_by', 'sent_at'], 'integer'],
            [['active'], 'default', 'value' => 1],
            [
                ['office_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Office::className(),
                'targetAttribute' => ['office_id' => 'id']
            ],
            [['date_from', 'date_to'], 'required', 'enableClientValidation' => false],
            //['date_to', 'validateDates'],
            [
                ['office_id', 'date_from', 'date_to'],
                'unique',
                'targetAttribute' => ['office_id', 'date_from', 'date_to']
            ]
        ];
    }

    public function validateDates()
    {
        if (strtotime($this->date_to) <= strtotime($this->date_from)) {
            $this->addError('date_from', Yii::t('common', 'Дата начала периода должна быть раньше окончания'));
            $this->addError('date_to', Yii::t('common', 'Дата окончания периода должна быть после начала'));
        } else if (date('Y-m', strtotime($this->date_from)) != date('Y-m', strtotime($this->date_to))) {
            $this->addError('date_from', Yii::t('common', 'Интервал дат допустим только в пределах одного месяца'));
            $this->addError('date_to', Yii::t('common', 'Интервал дат допустим только в пределах одного месяца'));
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'date_from' => Yii::t('common', 'Дата начала периода'),
            'date_to' => Yii::t('common', 'Дата окончания периода'),
            'active' => Yii::t('common', 'Активность'),
            'office_id' => Yii::t('common', 'Офис'),
            'created_at' => Yii::t('common', 'Дата добавления'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
            'closed_at' => Yii::t('common', 'Дата закрытия'),
            'closed_by' => Yii::t('common', 'Кем закрыт'),
            'sent_at' => Yii::t('common', 'Дата отправки'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOffice()
    {
        return $this->hasOne(Office::className(), ['id' => 'office_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMonthlyStatementDatas()
    {
        return $this->hasMany(MonthlyStatementData::className(), ['statement_id' => 'id']);
    }

    /**
     * @param array $personIds
     * @return array
     */
    public function selectPersons($personIds = [])
    {
        $query = Person::find()
            ->select([
                'person_id' => Person::tableName() . '.id',
                'office' => Office::tableName() . '.name',
                'name' => Person::tableName() . '.name',
                'lunch_time' => WorkingShift::tableName() . '.lunch_time',
                'start_date' => Person::tableName() . '.start_date',
                'dismissal_date' => Person::tableName() . '.dismissal_date',
                'active' => Person::tableName() . '.active',
                'parent_id' => Person::tableName() . '.parent_id',
                'parent_name' => 'parent.name',
                'parent_designation' => 'parent_designation.name',
                'designation' => Designation::tableName() . '.name',
                'designation_id' => Designation::tableName() . '.id',
                'designation_team_lead' => Designation::tableName() . '.team_lead',
                'designation_calc_work_time' => Designation::tableName() . '.calc_work_time',
                'working_1' => WorkingShift::tableName() . '.working_mon',
                'working_2' => WorkingShift::tableName() . '.working_tue',
                'working_3' => WorkingShift::tableName() . '.working_wed',
                'working_4' => WorkingShift::tableName() . '.working_thu',
                'working_5' => WorkingShift::tableName() . '.working_fri',
                'working_6' => WorkingShift::tableName() . '.working_sat',
                'working_7' => WorkingShift::tableName() . '.working_sun',
                'call_center_user_logins' => 'GROUP_CONCAT(DISTINCT ' . CallCenterUser::tableName() . '.user_login)',
                'call_center_user_ids' => 'GROUP_CONCAT(DISTINCT ' . CallCenterUser::tableName() . '.id)',
                'call_center_oper_ids' => 'GROUP_CONCAT(DISTINCT ' . CallCenterUser::tableName() . '.user_id)',
                'call_center_id_oper_id' => 'GROUP_CONCAT(DISTINCT CONCAT(' . CallCenterUser::tableName() . '.callcenter_id,"_",' . CallCenterUser::tableName() . '.user_id))',
                'country_names' => 'GROUP_CONCAT(DISTINCT ' . Country::tableName() . '.name)',
                'salary_local' => Person::tableName() . '.salary_local',
                'salary_usd' => Person::tableName() . '.salary_usd',
                'salary_hour_local' => Person::tableName() . '.salary_hour_local',
                'salary_hour_usd' => Person::tableName() . '.salary_hour_usd',
                'salary_scheme' => Person::tableName() . '.salary_scheme',
            ])
            ->leftJoin(Office::tableName(), Office::tableName() . '.id=' . Person::tableName() . '.office_id')
            ->leftJoin(CallCenterUser::tableName(), CallCenterUser::tableName() . '.person_id=' . Person::tableName() . '.id')
            ->leftJoin(Designation::tableName(), Designation::tableName() . '.id=' . Person::tableName() . '.designation_id')
            ->leftJoin(CallCenter::tableName(), CallCenter::tableName() . '.id=' . CallCenterUser::tableName() . '.callcenter_id')
            ->leftJoin(Country::tableName(), Country::tableName() . '.id=' . CallCenter::tableName() . '.country_id')
            ->leftJoin(Person::tableName() . ' as parent', Person::tableName() . '.parent_id=parent.id')
            ->leftJoin(Designation::tableName() . ' as parent_designation', 'parent.designation_id=parent_designation.id')
            ->leftJoin(WorkingShift::tableName(), WorkingShift::tableName() . '.id=' . Person::tableName() . '.working_shift_id')
            ->byOfficeId($this->office_id)
            ->andWhere([
                'or',
                ['<=', Person::tableName() . '.start_date', date("Y-m-d", strtotime($this->date_to))],
                ['is', Person::tableName() . '.start_date', null]
            ])
            ->andWhere([
                'or',
                [
                    '>=',
                    'DATE_FORMAT(' . Person::tableName() . '.dismissal_date, "%Y-%m")',
                    date("Y-m", strtotime($this->date_to))
                ],
                ['is', Person::tableName() . '.dismissal_date', null]
            ])
            ->groupBy([Person::tableName() . '.id'])
            ->orderBy([
                Person::tableName() . '.parent_id' => SORT_ASC,
                Person::tableName() . '.active' => SORT_DESC,
                Person::tableName() . '.name' => SORT_ASC,
            ]);

        if ($personIds) {
            $query->andWhere([Person::tableName() . '.id' => $personIds]);
        }

        return self::formatPersons($query->asArray()->all());
    }

    /***
     * @param $persons
     * @param bool $closeMode
     * @return array
     */
    public function formatPersons($persons, $closeMode = false)
    {

        $fields = self::getDataFields($closeMode);

        $return = [];
        foreach ($persons as $person) {

            $item = [];
            if (!$closeMode) {
                $item = $person;
            }

            if ($closeMode) {
                foreach ($fields as $field) {
                    $item[$field] = $person[$field] ?? null;
                }
                $item['person_id'] = $person['id'];
                $item['name'] = $person['fio'];
            }

            if ($item['designation_team_lead']) {
                $item['parent_id'] = $item['person_id'];
                $item['parent_name'] = $item['name'];
                $item['parent_designation'] = $item['designation'];
            }
            $item['statement_id'] = $this->id;
            $item['created_at'] = time();
            $item['updated_at'] = time();

            $return[] = $item;
        }

        return $return;
    }

    /**
     * @param bool $closeMode
     * @return array
     */
    public static function getDataFields($closeMode = false)
    {
        $return = [
            'person_id',
            'office',
            'name',
            'lunch_time',
            'start_date',
            'dismissal_date',
            'active',
            'parent_id',
            'parent_name',
            'parent_designation',
            'designation',
            'designation_id',
            'designation_team_lead',
            'designation_calc_work_time',
            'working_1',
            'working_2',
            'working_3',
            'working_4',
            'working_5',
            'working_6',
            'working_7',
            'call_center_user_logins',
            'call_center_user_ids',
            'call_center_oper_ids',
            'call_center_id_oper_id',
            'country_names',
            'salary_local',
            'salary_usd',
            'salary_hour_local',
            'salary_hour_usd',
            'salary_scheme',
            'statement_id',
            'created_at',
            'updated_at',
        ];
        if ($closeMode) {
            array_push($return,
                'count_normal_hours',
                'count_time',
                'count_calls',
                'count_extra_hours',
                'holiday_worked_time',
                'salary_hand_local_extra',
                'salary_hand_usd_extra',
                'salary_hand_local_holiday',
                'salary_hand_usd_holiday',
                'penalty_sum_local',
                'penalty_sum_usd',
                'bonus_sum_local',
                'bonus_sum_usd',
                'salary_hand_local',
                'salary_tax_local',
                'salary_hand_usd',
                'salary_tax_usd',
                'salary_sheet_file'
            );
        }
        return $return;
    }

    /**
     * @param $persons
     * @param bool $closeMode
     */
    public function savePersons($persons, $closeMode = false)
    {
        Yii::$app->db->createCommand()->batchInsert(
            MonthlyStatementData::tableName(),
            self::getDataFields($closeMode),
            $persons
        )->execute();
    }

    /**
     * @param $personId
     * @return bool|string
     */
    public static function getSalaryFilePath($personId = null)
    {
        $path = Yii::getAlias('@files') . DIRECTORY_SEPARATOR . 'salary';
        Utils::prepareDir($path);
        if ($personId) {
            $path .= DIRECTORY_SEPARATOR . $personId;
            Utils::prepareDir($path);
        }
        return $path;
    }
}
