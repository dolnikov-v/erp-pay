<?php

namespace app\modules\salary\models;

use app\modules\salary\models\query\MonthlyStatementDataQuery;
use app\components\db\ActiveRecordLogUpdateTime;
use Yii;

/**
 * This is the model class for table "salary_monthly_statement_data".
 *
 * @property integer $id
 * @property integer $statement_id
 * @property integer $person_id
 * @property string $name
 * @property string $lunch_time
 * @property string $start_date
 * @property string $dismissal_date
 * @property integer $active
 * @property integer $parent_id
 * @property string $parent_name
 * @property string $parent_designation
 * @property string $designation
 * @property integer $designation_id
 * @property integer $designation_team_lead
 * @property integer $designation_calc_work_time
 * @property string $working_1
 * @property string $working_2
 * @property string $working_3
 * @property string $working_4
 * @property string $working_5
 * @property string $working_6
 * @property string $working_7
 * @property string $call_center_user_logins
 * @property string $call_center_user_ids
 * @property string $call_center_oper_ids
 * @property string $call_center_id_oper_id
 * @property double $salary_local
 * @property double $salary_usd
 * @property string $salary_scheme
 * @property string $country_names
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $office
 * @property double $salary_hour_local
 * @property double $salary_hour_usd
 * @property string $count_normal_hours
 * @property string $count_time
 * @property string $count_calls
 * @property string $count_extra_hours
 * @property string $holiday_worked_time
 * @property double $salary_hand_local_extra
 * @property double $salary_hand_usd_extra
 * @property double $salary_hand_local_holiday
 * @property double $salary_hand_usd_holiday
 * @property double $penalty_sum_local
 * @property double $penalty_sum_usd
 * @property double $bonus_sum_local
 * @property double $bonus_sum_usd
 * @property double $salary_hand_local
 * @property double $salary_tax_local
 * @property double $salary_hand_usd
 * @property double $salary_tax_usd
 * @property integer $sent_at
 * @property string $salary_sheet_file
 *
 * @property MonthlyStatement $statement
 */
class MonthlyStatementData extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%salary_monthly_statement_data}}';
    }

    /**
     * @return MonthlyStatementDataQuery
     */
    public static function find()
    {
        return new MonthlyStatementDataQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'statement_id',
                    'person_id',
                    'active',
                    'parent_id',
                    'designation_id',
                    'designation_team_lead',
                    'designation_calc_work_time',
                    'count_calls',
                    'created_at',
                    'updated_at',
                    'sent_at',
                ],
                'integer'
            ],
            [
                [
                    'salary_local',
                    'salary_usd',
                    'salary_hour_local',
                    'salary_hour_usd',
                    'count_normal_hours',
                    'count_time',
                    'count_extra_hours',
                    'holiday_worked_time',
                    'salary_hand_local_extra',
                    'salary_hand_usd_extra',
                    'salary_hand_local_holiday',
                    'salary_hand_usd_holiday',
                    'penalty_sum_local',
                    'penalty_sum_usd',
                    'bonus_sum_local',
                    'bonus_sum_usd',
                    'salary_hand_local',
                    'salary_tax_local',
                    'salary_hand_usd',
                    'salary_tax_usd'
                ],
                'number'
            ],
            [
                [
                    'name',
                    'lunch_time',
                    'start_date',
                    'dismissal_date',
                    'parent_name',
                    'parent_designation',
                    'designation',
                    'working_1',
                    'working_2',
                    'working_3',
                    'working_4',
                    'working_5',
                    'working_6',
                    'working_7',
                    'call_center_user_logins',
                    'call_center_user_ids',
                    'call_center_oper_ids',
                    'call_center_id_oper_id',
                    'salary_scheme',
                    'country_names',
                    'office',
                    'salary_sheet_file',
                ],
                'string',
                'max' => 255
            ],
            [
                ['statement_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => MonthlyStatement::className(),
                'targetAttribute' => ['statement_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'statement_id' => Yii::t('common', 'График'),
            'person_id' => Yii::t('common', 'person.id'),
            'office' => Yii::t('common', 'Офис'),
            'name' => Yii::t('common', 'ФИО'),
            'lunch_time' => Yii::t('common', 'Обед'),
            'start_date' => Yii::t('common', 'Дата приема на работу'),
            'dismissal_date' => Yii::t('common', 'Дата увольнения'),
            'active' => Yii::t('common', 'Активность'),
            'parent_id' => Yii::t('common', 'person.parent_id'),
            'parent_name' => Yii::t('common', 'Руководитель'),
            'parent_designation' => 'Должность руководителя',
            'designation' => Yii::t('common', 'Должность'),
            'designation_id' => Yii::t('common', 'Должность'),
            'designation_team_lead' => Yii::t('common', 'Руководитель региона'),
            'designation_calc_work_time' => Yii::t('common', 'Считать почасовую ЗП'),
            'working_1' => Yii::t('common', 'Пн'),
            'working_2' => Yii::t('common', 'Вт'),
            'working_3' => Yii::t('common', 'Ср'),
            'working_4' => Yii::t('common', 'Чт'),
            'working_5' => Yii::t('common', 'Пт'),
            'working_6' => Yii::t('common', 'Сб'),
            'working_7' => Yii::t('common', 'Вс'),
            'call_center_user_logins' => Yii::t('common', 'Логин'),
            'call_center_user_ids' => Yii::t('common', 'call_center_user.id'),
            'call_center_oper_ids' => Yii::t('common', 'call_center_request.last_foreign_operator'),
            'call_center_id_oper_id' => Yii::t('common', 'call_center_request.call_center_id call_center_request.last_foreign_operator'),
            'salary_local' => Yii::t('common', 'Оклад за месяц в местной валюте'),
            'salary_usd' => Yii::t('common', 'Оклад за месяц в USD'),
            'salary_hour_local' => Yii::t('common', 'Оклад за час в местной валюте'),
            'salary_hour_usd' => Yii::t('common', 'Оклад за час в USD'),
            'salary_scheme' => Yii::t('common', 'Схема начисления з/п'),
            'country_names' => Yii::t('common', 'Направление'),
            'created_at' => Yii::t('common', 'Дата добавления'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
            'count_normal_hours' => Yii::t('common', 'Норма часов по КЗоТ'),
            'count_time' => Yii::t('common', 'Отработанных часов'),
            'count_calls' => Yii::t('common', 'Число звонков'),
            'count_extra_hours' => Yii::t('common', 'Переработка часов'),
            'holiday_worked_time' => Yii::t('common', 'Праздники часов'),
            'salary_hand_local_extra' => Yii::t('common', 'ЗП переработка'),
            'salary_hand_usd_extra' => Yii::t('common', 'ЗП переработка в USD'),
            'salary_hand_local_holiday' => Yii::t('common', 'ЗП праздники'),
            'salary_hand_usd_holiday' => Yii::t('common', 'ЗП праздники в USD'),
            'penalty_sum_local' => Yii::t('common', 'Штрафы'),
            'penalty_sum_usd' => Yii::t('common', 'Штрафы в USD'),
            'bonus_sum_local' => Yii::t('common', 'Бонусы'),
            'bonus_sum_usd' => Yii::t('common', 'Бонусы в USD'),
            'salary_hand_local' => Yii::t('common', 'ЗП в местной валюте "на руки"'),
            'salary_tax_local' => Yii::t('common', 'ЗП в местной валюте с учетом налогов'),
            'salary_hand_usd' => Yii::t('common', 'ЗП в USD "на руки"'),
            'salary_tax_usd' => Yii::t('common', 'ЗП в USD с учетом налогов'),
            'sent_at' => Yii::t('common', 'Дата отправки'),
            'salary_sheet_file' => Yii::t('common', 'Файл расчетки'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatement()
    {
        return $this->hasOne(MonthlyStatement::className(), ['id' => 'statement_id']);
    }
}
