<?php

namespace app\modules\salary\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\modules\catalog\models\ExternalSource;
use app\modules\catalog\models\ExternalSourceRole;
use app\modules\salary\models\query\DesignationExtensionRoleQuery;

/**
 * Class DesignationExtensionRole
 * @package app\modules\salary\models
 */
class DesignationExtensionRole extends ActiveRecordLogUpdateTime
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%designation_extension_role}}';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['designation_id', 'external_source_id', 'external_source_role_id'], 'required'],
            [['designation_id', 'external_source_id', 'external_source_role_id', 'created_at', 'updated_at'], 'integer'],
        ];
    }

    /**
     * @return DesignationExtensionRoleQuery
     */
    public static function find()
    {
        return new DesignationExtensionRoleQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDesignation()
    {
        return $this->hasOne(Designation::className(), ['designation_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExternalSource()
    {
        return $this->hasOne(ExternalSource::className(), ['id' => 'external_source_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExternalSourceRole()
    {
        return $this->hasOne(ExternalSourceRole::className(), ['id' => 'external_source_role_id']);
    }
}