<?php

namespace app\modules\salary\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\User;
use Yii;

/**
 * Class PersonLog
 * @package app\modules\salary\models
 *
 * @property integer $id
 * @property integer $person_id
 * @property integer $user_id
 * @property string $group_id
 * @property string $field
 * @property string $old
 * @property string $new
 *
 * @property Person $person
 * @property User $user
 */
class PersonLog extends ActiveRecordLogUpdateTime
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%salary_person_log}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecordLogUpdateTime::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['person_id', 'field'], 'required'],
            [['person_id', 'user_id', 'created_at'], 'integer'],
            [['field', 'old', 'new', 'group_id'], 'string', 'max' => 255],
            [
                ['person_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Person::className(),
                'targetAttribute' => ['person_id' => 'id']
            ],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'person_id' => Yii::t('common', 'Сотрудник'),
            'group_id' => Yii::t('common', 'Группа'),
            'field' => Yii::t('common', 'Поле'),
            'old' => Yii::t('common', 'Старое значение'),
            'new' => Yii::t('common', 'Новое значение'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'user_id' => Yii::t('common', 'Пользователь'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery | Person
     */
    public function getPerson()
    {
        return $this->hasOne(Person::className(), ['id' => 'person_id']);
    }

    /**
     * @return \yii\db\ActiveQuery | User
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
