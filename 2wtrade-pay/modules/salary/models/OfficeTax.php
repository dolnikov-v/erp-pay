<?php

namespace app\modules\salary\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\logs\LinkData;
use app\modules\salary\models\query\OfficeTaxQuery;
use Yii;

/**
 * This is the model class for table "salary_office_tax".
 *
 * @property integer $id
 * @property integer $office_id
 * @property double $sum_from
 * @property double $sum_to
 * @property string $type
 * @property string $tax_type
 * @property double $unpaid_amount
 * @property double $rate
 * @property double $amount
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Office $office
 */
class OfficeTax extends ActiveRecordLogUpdateTime
{
    protected $insertLog = true;
    protected $updateLog = true;
    protected $deleteLog = true;

    /**
     * @return array|null
     */
    protected function getLinkData(): ?array
    {
        return [
            new LinkData(['field' => 'office_id', 'value' => (int)$this->office_id]),
        ];
    }

    const TYPE_PERCENT = 'percent';
    const TYPE_FIXED = 'fixed';

    const TAX_TYPE_INCOME = 'Income';
    const TAX_TYPE_INSURANCE = 'Insurance';
    const TAX_TYPE_PENSION = 'Pension';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%salary_office_tax}}';
    }

    /**
     * @return OfficeTaxQuery
     */
    public static function find()
    {
        return new OfficeTaxQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['office_id', 'created_at', 'updated_at'], 'integer'],
            [['sum_from', 'sum_to', 'rate', 'amount', 'unpaid_amount'], 'number'],
            [['type', 'tax_type'], 'string', 'max' => 255],
            [
                ['office_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Office::className(),
                'targetAttribute' => ['office_id' => 'id']
            ],
            [
                'amount',
                'required',
                'when' => function ($model) {
                    return $model->type == self::TYPE_FIXED;
                },
                'whenClient' => "function (attribute, value) {
                return $('#officetax-type').val() == 'fixed';
            }"
            ],
            [
                'rate',
                'required',
                'when' => function ($model) {
                    return $model->type == self::TYPE_PERCENT;
                },
                'whenClient' => "function (attribute, value) {
                return $('#officetax-type').val() == 'percent';
            }"
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'office_id' => Yii::t('common', 'Офис'),
            'sum_from' => Yii::t('common', 'Сумма от'),
            'sum_to' => Yii::t('common', 'Сумма до'),
            'type' => Yii::t('common', 'Тип расчета'),
            'tax_type' => Yii::t('common', 'Тип налога'),
            'unpaid_amount' => Yii::t('common', 'Не облагаемая сумма'),
            'rate' => Yii::t('common', 'Ставка налога'),
            'amount' => Yii::t('common', 'Сумма налога'),
            'created_at' => Yii::t('common', 'Дата добавления'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOffice()
    {
        return $this->hasOne(Office::className(), ['id' => 'office_id']);
    }

    /**
     * @return array
     */
    public static function getTypes()
    {
        return [
            self::TYPE_PERCENT => Yii::t('common', 'Процентная ставка'),
            self::TYPE_FIXED => Yii::t('common', 'Фиксированная сумма'),
        ];
    }
    /**
     * @return array
     */
    public static function getTaxTypes()
    {
        return [
            self::TAX_TYPE_INCOME => Yii::t('common', 'Налог на доход'),
            self::TAX_TYPE_INSURANCE=> Yii::t('common', 'Страховой взнос'),
            self::TAX_TYPE_PENSION => Yii::t('common', 'Пенсионный взнос'),
        ];
    }


    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($this->type != self::TYPE_PERCENT) {
            $this->rate = null;
        }
        if ($this->type != self::TYPE_FIXED) {
            $this->amount = null;
        }
        return parent::beforeSave($insert);
    }
}
