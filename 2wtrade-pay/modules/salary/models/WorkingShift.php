<?php

namespace app\modules\salary\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\logs\LinkData;
use app\modules\crocotime\components\CrocotimeApi;
use app\modules\salary\models\query\WorkingShiftQuery;
use Yii;

/**
 * This is the model class for table "salary_working_shift".
 *
 * @property integer $id
 * @property integer $office_id
 * @property integer $crocotime_id
 * @property string $name
 * @property string $lunch_time
 * @property string $working_mon
 * @property string $working_tue
 * @property string $working_wed
 * @property string $working_thu
 * @property string $working_fri
 * @property string $working_sat
 * @property string $working_sun
 * @property integer $default
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Person[] $people
 * @property Person[] $peopleActive
 * @property Office $office
 */
class WorkingShift extends ActiveRecordLogUpdateTime
{
    protected $insertLog = true;
    protected $updateLog = true;
    protected $deleteLog = true;

    /**
     * @return array|null
     */
    protected function getLinkData(): ?array
    {
        return [
            new LinkData(['field' => 'office_id', 'value' => (int)$this->office_id]),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%salary_working_shift}}';
    }

    /**
     * @return WorkingShiftQuery
     */
    public static function find()
    {
        return new WorkingShiftQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'office_id'], 'required'],
            ['crocotime_id', 'integer'],
            [['office_id', 'created_at', 'updated_at', 'default'], 'integer'],
            [['default'], 'default', 'value' => 0],
            [['default'], 'number', 'min' => 0, 'max' => 1],
            [
                [
                    'name',
                    'lunch_time',
                    'working_mon',
                    'working_tue',
                    'working_wed',
                    'working_thu',
                    'working_fri',
                    'working_sat',
                    'working_sun',
                ],
                'string',
                'max' => 255,
            ],
            [
                [
                    'lunch_time',
                    'working_mon',
                    'working_tue',
                    'working_wed',
                    'working_thu',
                    'working_fri',
                    'working_sat',
                    'working_sun',
                ],
                'match',
                'pattern' => '/^\d{1,2}:\d{2}-\d{1,2}:\d{2}$/i',
                'message' => Yii::t('common', 'Значение "{attribute}" не верно, пример: 10:00-19:00'),
            ],
            [
                ['office_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Office::className(),
                'targetAttribute' => ['office_id' => 'id'],
            ],
            [
                ['default'],
                'unique',
                'when' => function ($model) {
                    return $model->default == 1;
                },
                'targetAttribute' => ['office_id', 'default'],
                'message' => Yii::t('common', 'Смена по умолчанию уже есть в данном физическом КЦ'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'office_id' => Yii::t('common', 'Офис'),
            'crocotime_id' => Yii::t('common', 'Смена в Crocotime'),
            'name' => Yii::t('common', 'Название'),
            'lunch_time' => Yii::t('common', 'Обед'),
            'working_mon' => Yii::t('common', 'Пн'),
            'working_tue' => Yii::t('common', 'Вт'),
            'working_wed' => Yii::t('common', 'Ср'),
            'working_thu' => Yii::t('common', 'Чт'),
            'working_fri' => Yii::t('common', 'Пт'),
            'working_sat' => Yii::t('common', 'Сб'),
            'working_sun' => Yii::t('common', 'Вс'),
            'default' => Yii::t('common', 'По умолчанию'),
            'created_at' => Yii::t('common', 'Дата добавления'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeople()
    {
        return $this->hasMany(Person::className(), ['working_shift_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeopleActive()
    {
        return $this->hasMany(Person::className(), ['working_shift_id' => 'id'])
            ->andOnCondition(['active' => Person::ACTIVE]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOffice()
    {
        return $this->hasOne(Office::className(), ['id' => 'office_id']);
    }

    public function beforeDelete()
    {
        if ($this->peopleActive) {
            $this->addError('id', Yii::t('common', 'Нельзя удалить рабочую смену, т.к. она есть у сотрудников'));
            return false;
        }

        /*if ($this->crocotime_id) {
            $crocotimeApi = new CrocotimeApi();
            if ( !$crocotimeApi->deleteSchedule( $this->crocotime_id ) ) {
                $this->addError('id', Yii::t('common', 'Не удалось удалить рабочую смену в Crocotime'));

                return false;
            };
        }*/

        return parent::beforeDelete();
    }

    public function beforeSave($insert)
    {
        /*if (!$this->office->isLinkedWithCrocotime()) {
            return parent::beforeSave($insert);
        }
        $crocotimeApi = new CrocotimeApi();
        if ($insert || !$this->crocotime_id) {
            if (!$scheduleId = $crocotimeApi->insertSchedule($this->name)) {
                $this->addError('crocotime_id', 'Can not insert shift to CrocoTime');

                return false;
            };

            $this->crocotime_id = $scheduleId;
        }

        if (!$crocotimeApi->updateSchedule($this)) {
            $this->addError('crocotime_id', "Can not update shift in CrocoTime, CrocoTime id $this->crocotime_id");

            return false;
        }*/

        return parent::beforeSave($insert);
    }

}
