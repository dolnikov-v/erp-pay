<?php
namespace app\modules\salary\assets;

use yii\web\AssetBundle;

/**
 * Class StaffingEditAsset
 * @package app\modules\salary\assets
 */
class StaffingEditAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/salary/staffing';

    public $js = [
        'edit.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
