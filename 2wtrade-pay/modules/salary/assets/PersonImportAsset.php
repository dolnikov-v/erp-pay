<?php
namespace app\modules\salary\assets;

use yii\web\AssetBundle;

/**
 * Class PersonImportAsset
 * @package app\modules\salary\assets
 */
class PersonImportAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/salary';

    public $css = [
        'person-import/import-list.css'
    ];

    public $js = [
        'person-import/import-list.js'
    ];

    public $depends = [
        'app\assets\SiteAsset',
    ];
}
