<?php
namespace app\modules\salary\assets;

use yii\web\AssetBundle;

/**
 * Class PenaltyIndexAsset
 * @package app\modules\salary\assets
 */
class PenaltyIndexAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/salary/penalty';

    public $js = [
        'index.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
