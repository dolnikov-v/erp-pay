<?php
namespace app\modules\salary\assets;

use yii\web\AssetBundle;

/**
 * Class WorkingShiftEditAsset
 * @package app\modules\salary\assets
 */
class WorkingShiftEditAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/salary/working-shift';

    public $js = [
        'edit.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
