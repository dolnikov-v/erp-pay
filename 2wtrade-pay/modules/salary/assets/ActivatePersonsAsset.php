<?php
namespace app\modules\salary\assets;

use yii\web\AssetBundle;

/**
 * Class ActivatePersonsAsset
 * @package app\modules\salary\assets
 */
class ActivatePersonsAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/salary/activate-persons';

    public $js = [
        'activate-persons.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}