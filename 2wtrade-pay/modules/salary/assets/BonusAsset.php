<?php
namespace app\modules\salary\assets;

use yii\web\AssetBundle;

/**
 * Class BonusAsset
 * @package app\modules\salary\assets
 */
class BonusAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/salary/bonus';

    public $js = [
        'bonus.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
