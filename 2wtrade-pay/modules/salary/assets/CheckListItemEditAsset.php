<?php
namespace app\modules\salary\assets;

use yii\web\AssetBundle;

/**
 * Class CheckListItemEditAsset
 * @package app\modules\salary\assets
 */
class CheckListItemEditAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/salary/check-list-item';

    public $js = [
        'edit.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];

}
