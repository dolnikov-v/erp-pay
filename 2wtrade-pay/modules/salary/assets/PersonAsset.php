<?php
namespace app\modules\salary\assets;

use yii\web\AssetBundle;

/**
 * Class PersonAsset
 * @package app\modules\salary\assets
 */
class PersonAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/salary/person';

    public $css = [
        'edit.css'
    ];

    public $js = [
        'edit.js'
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
