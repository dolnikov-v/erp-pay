<?php
namespace app\modules\salary\assets;

use yii\web\AssetBundle;

/**
 * Class SetPenaltyAsset
 * @package app\modules\salary\assets
 */
class SetPenaltyAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/salary/set-penalty';

    public $js = [
        'set-penalty.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}