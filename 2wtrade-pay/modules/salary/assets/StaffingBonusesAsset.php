<?php
namespace app\modules\salary\assets;

use yii\web\AssetBundle;

/**
 * Class StaffingBonusesAsset
 * @package app\modules\salary\assets
 */
class StaffingBonusesAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/salary/staffing';

    public $js = [
        'staffing-bonuses.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
