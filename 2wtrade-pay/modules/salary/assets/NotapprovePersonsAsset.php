<?php
namespace app\modules\salary\assets;

use yii\web\AssetBundle;

/**
 * Class NotapprovePersonsAsset
 * @package app\modules\salary\assets
 */
class NotapprovePersonsAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/salary/notapprove-persons';

    public $js = [
        'notapprove-persons.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}