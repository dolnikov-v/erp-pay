<?php
namespace app\modules\salary\assets;

use yii\web\AssetBundle;

/**
 * Class PenaltyTypeAsset
 * @package app\modules\salary\assets
 */
class OfficeTaxAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/salary/office-tax';

    public $js = [
        'edit.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
