<?php
namespace app\modules\salary\assets;

use yii\web\AssetBundle;

/**
 * Class StaffingIndexAsset
 * @package app\modules\salary\assets
 */
class StaffingIndexAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/salary/staffing';

    public $js = [
        'index.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
