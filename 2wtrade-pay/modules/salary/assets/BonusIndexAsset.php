<?php
namespace app\modules\salary\assets;

use yii\web\AssetBundle;

/**
 * Class BonusIndexAsset
 * @package app\modules\salary\assets
 */
class BonusIndexAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/salary/bonus';

    public $js = [
        'index.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
