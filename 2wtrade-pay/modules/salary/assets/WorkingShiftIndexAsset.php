<?php
namespace app\modules\salary\assets;

use yii\web\AssetBundle;

/**
 * Class WorkingShiftIndexAsset
 * @package app\modules\salary\assets
 */
class WorkingShiftIndexAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/salary/working-shift';

    public $js = [
        'index.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
