<?php
namespace app\modules\salary\assets;

use yii\web\AssetBundle;

/**
 * Class DeactivatePersonsAsset
 * @package app\modules\salary\assets
 */
class DeactivatePersonsAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/salary/deactivate-persons';

    public $js = [
        'deactivate-persons.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}