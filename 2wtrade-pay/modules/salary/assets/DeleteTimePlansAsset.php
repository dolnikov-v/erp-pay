<?php
namespace app\modules\salary\assets;

use yii\web\AssetBundle;

/**
 * Class DeleteTimePlansAsset
 * @package app\modules\salary\assets
 */
class DeleteTimePlansAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/salary/work-time-plan';

    public $js = [
        'delete-list.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}