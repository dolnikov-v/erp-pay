<?php
namespace app\modules\salary\assets;

use yii\web\AssetBundle;

/**
 * Class PersonLinkUserAsset
 * @package app\modules\salary\assets
 */
class PersonLinkUserAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/salary/person';

    public $js = [
        'link-user.js'
    ];

    public $depends = [
        'app\assets\SiteAsset',
    ];
}
