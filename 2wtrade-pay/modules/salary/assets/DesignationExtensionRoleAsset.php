<?php

namespace app\modules\salary\assets;

use yii\web\AssetBundle;

/**
 * Class DesignationExtensionRoleAsset
 * @package app\modules\salary\assets
 */
class DesignationExtensionRoleAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/salary/designation-extension-role';

    public $js = [
        'designation-extension-role.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}