<?php
namespace app\modules\salary\assets;

use yii\web\AssetBundle;

/**
 * Class SetRoleAsset
 * @package app\modules\salary\assets
 */
class SetRoleAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/salary/set-role';

    public $js = [
        'set-role.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}