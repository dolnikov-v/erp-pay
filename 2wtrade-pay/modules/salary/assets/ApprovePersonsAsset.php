<?php
namespace app\modules\salary\assets;

use yii\web\AssetBundle;

/**
 * Class ApprovePersonsAsset
 * @package app\modules\salary\assets
 */
class ApprovePersonsAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/salary/approve-persons';

    public $js = [
        'approve-persons.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}