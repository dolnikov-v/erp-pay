<?php
namespace app\modules\salary\assets;

use yii\web\AssetBundle;

/**
 * Class PenaltyAsset
 * @package app\modules\salary\assets
 */
class PenaltyAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/salary/penalty';

    public $js = [
        'penalty.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
