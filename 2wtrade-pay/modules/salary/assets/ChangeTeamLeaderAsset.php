<?php
namespace app\modules\salary\assets;

use yii\web\AssetBundle;

/**
 * Class ChangeTeamLeaderAsset
 * @package app\modules\salary\assets
 */
class ChangeTeamLeaderAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/salary/change-team-leader';

    public $js = [
        'change-team-leader.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
