<?php
namespace app\modules\salary\assets;

use yii\web\AssetBundle;

/**
 * Class PersonIndexAsset
 * @package app\modules\salary\assets
 */
class PersonIndexAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/salary/person';

    public $css = [
        'index.css',
    ];

    public $js = [
        'index.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
