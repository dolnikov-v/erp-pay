<?php
namespace app\modules\salary\assets;

use yii\web\AssetBundle;

/**
 * Class MonthlyStatementEditAsset
 * @package app\modules\salary\assets
 */
class MonthlyStatementEditAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/salary/monthly-statement';

    public $js = [
        'edit.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
