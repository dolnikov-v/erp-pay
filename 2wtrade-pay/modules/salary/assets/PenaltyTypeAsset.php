<?php
namespace app\modules\salary\assets;

use yii\web\AssetBundle;

/**
 * Class PenaltyTypeAsset
 * @package app\modules\salary\assets
 */
class PenaltyTypeAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/salary/penalty-type';

    public $js = [
        'type.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
