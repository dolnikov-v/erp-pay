<?php
namespace app\modules\salary\assets;

use yii\web\AssetBundle;

/**
 * Class SetWorkAsset
 * @package app\modules\salary\assets
 */
class SetWorkAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/salary/set-work';

    public $js = [
        'set-work.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}