<?php
namespace app\modules\salary\assets;

use yii\web\AssetBundle;

/**
 * Class DeleteBonusesAsset
 * @package app\modules\salary\assets
 */
class DeleteBonusesAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/salary/delete-bonuses';

    public $js = [
        'delete-bonuses.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}