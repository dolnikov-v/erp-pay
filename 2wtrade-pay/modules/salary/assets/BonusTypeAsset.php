<?php
namespace app\modules\salary\assets;

use yii\web\AssetBundle;

/**
 * Class BonusTypeAsset
 * @package app\modules\salary\assets
 */
class BonusTypeAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/salary/bonus-type';

    public $js = [
        'type.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
