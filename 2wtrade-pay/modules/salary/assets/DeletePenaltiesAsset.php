<?php
namespace app\modules\salary\assets;

use yii\web\AssetBundle;

/**
 * Class DeletePenaltiesAsset
 * @package app\modules\salary\assets
 */
class DeletePenaltiesAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/salary/delete-penalties';

    public $js = [
        'delete-penalties.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}