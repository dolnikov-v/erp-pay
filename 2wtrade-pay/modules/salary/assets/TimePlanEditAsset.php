<?php
namespace app\modules\salary\assets;

use yii\web\AssetBundle;

/**
 * Class TimePlanEditAsset
 * @package app\modules\salary\assets
 */
class TimePlanEditAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/salary/work-time-plan';

    public $js = [
        'edit.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}