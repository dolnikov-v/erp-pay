<?php
namespace app\modules\salary\assets;

use yii\web\AssetBundle;

/**
 * Class MonthlyStatementDataDeleteAsset
 * @package app\modules\salary\assets
 */
class MonthlyStatementDataDeleteAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/salary/monthly-statement';

    public $js = [
        'delete-persons.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}