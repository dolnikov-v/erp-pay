<?php
namespace app\modules\salary\assets;

use yii\web\AssetBundle;

/**
 * Class TimePlanIndexAsset
 * @package app\modules\salary\assets
 */
class TimePlanIndexAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/salary/work-time-plan';

    public $js = [
        'index.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
