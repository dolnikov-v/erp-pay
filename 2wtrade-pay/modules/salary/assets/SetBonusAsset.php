<?php
namespace app\modules\salary\assets;

use yii\web\AssetBundle;

/**
 * Class SetBonusAsset
 * @package app\modules\salary\assets
 */
class SetBonusAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/salary/set-bonus';

    public $js = [
        'set-bonus.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}