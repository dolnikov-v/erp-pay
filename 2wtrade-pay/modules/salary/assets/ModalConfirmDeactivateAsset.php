<?php
namespace app\modules\salary\assets;

use yii\web\AssetBundle;

/**
 * Class ModalConfirmDeactivateAsset
 * @package app\modules\salary\assets
 */
class ModalConfirmDeactivateAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/salary/modal-confirm-deactivate';

    public $js = [
        'modal-confirm-deactivate.js',
    ];

    /**
     * @var array
     */
    public $depends = [
        'app\assets\SiteAsset',
    ];

}