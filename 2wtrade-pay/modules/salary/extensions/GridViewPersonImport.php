<?php
namespace app\modules\salary\extensions;

use app\components\grid\ActionColumn;
use app\components\grid\CustomCheckboxColumn;
use app\components\grid\GridView;
use app\modules\salary\models\Person;
use app\modules\salary\models\PersonImport;
use app\modules\salary\models\PersonImportData;
use app\modules\callcenter\models\CallCenterUser;
use app\widgets\custom\Checkbox;
use app\widgets\Label;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class GridViewInProgress
 * @package app\salary\report\extensions
 */
class GridViewPersonImport extends GridView
{
    /**
     * @var Person[]
     */
    public $persons;

    /**
     * @var PersonImport
     */
    public $import;

    /**
     * @var array
     */
    public $countryCollection;

    /**
     * @var array
     */
    public $positionCollection;


    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if (!$this->import->isImported()) {
            $this->afterRow = function ($model) {
                $row = '';
                if ($model instanceof PersonImportData &&
                    (!$model->importErrors || !empty($model->importErrors['user']))
                ) {
                    $options = $this->rowOptions; // save options
                    $this->rowOptions = ['class' => 'info kv-group-footer'];

                    $personIds = [];
                    if ($users = $model->getUsers()) {
                        foreach ($users as $user) {
                            if ($user->person) {
                                $personIds[] = $user->person->id;
                            }
                            $row .= $this->renderTableRow($user, null, null);
                        }
                    }
                    if ($persons = $model->getPersons()) {
                        foreach ($persons as $person) {
                            if (in_array($person->id, $personIds)) {
                                continue;
                            }
                            $row .= $this->renderTableRow($person, null, null);
                        }
                    }
                    $this->rowOptions = $options; // load options
                    if ($row) {
                        $row .= Html::tag('tr', Html::tag('td', '', ['colspan' => count($this->columns)]));
                    }
                }
                return $row;
            };
        }
    }

    /**
     * @inheritdoc
     */
    public function initColumns()
    {
        $this->columns = [
            [
                'class' => CustomCheckboxColumn::className(),
                'content' => function ($model) {

                    $visible = ($model instanceof PersonImportData);
                    return $visible ? Checkbox::widget([
                        'name' => 'rows[]',
                        'value' => $model->id,
                        'style' => 'checkbox-inline',
                        'label' => true,
                    ]) : '';
                },
                'visible' => !$this->import->isImported(),
            ],
            [
                'attribute' => 'row_number',
                'label' => '#',
                'content' => function ($model) {
                    if ($model instanceof PersonImportData) {
                        return $model->row_number;
                    }
                    return '';
                },
            ],
            [
                'attribute' => 'id',
                'label' => Yii::t('common', 'ID'),
                'content' => function ($model) {
                    if ($model instanceof PersonImportData) {
                        return $model->user_id;
                    } elseif ($model instanceof CallCenterUser) {
                        return $model->user_id;
                    }
                    return '';
                },
                'visible' => $this->import->cell_login || $this->import->cell_id,
            ],
            [
                'attribute' => 'login',
                'label' => Yii::t('common', 'Логин'),
                'content' => function ($model) {
                    if ($model instanceof PersonImportData) {
                        return $model->user_login;
                    } elseif ($model instanceof CallCenterUser) {
                        return $model->user_login;
                    }
                    return '';
                },
                'visible' => $this->import->cell_login,
            ],
            [
                'attribute' => 'country',
                'label' => Yii::t('common', 'Страна'),
                'content' => function ($model) {
                    if ($model instanceof PersonImportData) {
                        return ArrayHelper::getValue($this->countryCollection, $model->getCountryId(), $model->country);
                    } elseif ($model instanceof CallCenterUser) {
                        return $model->callCenter->country->name;
                    }
                    return '';
                },
                'visible' => $this->import->cell_country
            ],
            [
                'attribute' => 'call_center',
                'label' => Yii::t('common', 'Колл-центр'),
                'content' => function ($model) {
                    if ($model instanceof CallCenterUser) {
                        return $model->callCenter->url;
                    }
                    return '';
                },
                'visible' => $this->import->cell_full_name,
            ],
            [
                'attribute' => 'full_name',
                'label' => Yii::t('common', 'Полное имя'),
                'content' => function ($model) {
                    if ($model instanceof PersonImportData) {
                        return $model->person_full_name;
                    } elseif ($model instanceof CallCenterUser) {
                        return $model->person ? $model->person->name : '';
                    } elseif ($model instanceof Person) {
                        return $model->name;
                    }
                    return '';
                },
                'visible' => $this->import->cell_full_name,
            ],
            [
                'attribute' => 'head',
                'label' => Yii::t('common', 'Руководитель'),
                'content' => function ($model) {
                    if ($model instanceof PersonImportData) {
                        if ($model->head_id) {
                            return Label::widget(['label' => $model->head_id, 'style' => Label::STYLE_DARK]) . ($model->head ? $model->head->name : '');
                        } elseif ($officeTeamLead = $model->getOfficeTeamLead()) {
                            return Label::widget(['label' => $officeTeamLead->id, 'style' => Label::STYLE_DARK]) . $officeTeamLead->name;
                        }
                        return $model->person_head;
                    } elseif ($model instanceof Person) {
                        return $model->parent ? $model->parent->name : '';
                    } elseif ($model instanceof CallCenterUser) {
                        return $model->person && $model->person->parent ? $model->person->parent->name : '';
                    }
                    return '';
                },
                'visible' => $this->import->cell_position
            ],
            [
                'attribute' => 'position',
                'label' => Yii::t('common', 'Должность'),
                'content' => function ($model) {
                    if ($model instanceof PersonImportData) {
                        return ArrayHelper::getValue($this->positionCollection, $model->getPositionId(), $model->person_position);
                    } elseif ($model instanceof CallCenterUser) {
                        return $model->person ? $model->person->designation->name : null;
                    } elseif ($model instanceof Person) {
                        return $model->designation->name;
                    }
                    return '';
                },
                'visible' => $this->import->cell_position
            ],
            [
                'attribute' => 'salary',
                'label' => Yii::t('common', 'Зарплата за месяц'),
                'content' => function ($model) {
                    if ($model instanceof PersonImportData) {
                        return $model->person_salary;
                    } elseif ($model instanceof CallCenterUser) {
                        return $model->person ? $model->person->salary_local : null;
                    } elseif ($model instanceof Person) {
                        return $model->salary_local;
                    }
                    return '';
                },
                'visible' => $this->import->cell_salary
            ],
            [
                'attribute' => 'salary_usd',
                'label' => Yii::t('common', 'Зарплата за месяц в долларах'),
                'content' => function ($model) {
                    if ($model instanceof PersonImportData) {
                        return $model->person_salary_usd;
                    } elseif ($model instanceof CallCenterUser) {
                        return $model->person ? $model->person->salary_usd : null;
                    } elseif ($model instanceof Person) {
                        return $model->salary_usd;
                    }
                    return '';
                },
                'visible' => $this->import->cell_salary_usd
            ],
            [
                'attribute' => 'salary_hourly',
                'label' => Yii::t('common', 'Зарплата за час'),
                'content' => function ($model) {
                    if ($model instanceof PersonImportData) {
                        return $model->person_salary_hourly;
                    } elseif ($model instanceof CallCenterUser) {
                        return $model->person ? $model->person->salary_hour_local : null;
                    } elseif ($model instanceof Person) {
                        return $model->salary_hour_local;
                    }
                    return '';
                },
                'visible' => $this->import->cell_salary_hourly
            ],
            [
                'attribute' => 'salary_usd',
                'label' => Yii::t('common', 'Зарплата за час в долларах'),
                'content' => function ($model) {
                    if ($model instanceof PersonImportData) {
                        return $model->person_salary_hourly_usd;
                    } elseif ($model instanceof CallCenterUser) {
                        return $model->person ? $model->person->salary_hour_usd : null;
                    } elseif ($model instanceof Person) {
                        return $model->salary_hour_usd;
                    }
                    return '';
                },
                'visible' => $this->import->cell_salary_hourly_usd
            ],
            [
                'attribute' => 'person_email',
                'label' => Yii::t('common', 'E-mail'),
                'content' => function ($model) {
                    if ($model instanceof PersonImportData) {
                        return $model->person_email;
                    } elseif ($model instanceof CallCenterUser) {
                        return $model->person ? $model->person->corporate_email : null;
                    } elseif ($model instanceof Person) {
                        return $model->corporate_email;
                    }
                    return '';
                },
                'visible' => $this->import->cell_email
            ],
            [
                'attribute' => 'person_skype',
                'label' => Yii::t('common', 'Skype'),
                'content' => function ($model) {
                    if ($model instanceof PersonImportData) {
                        return $model->person_skype;
                    } elseif ($model instanceof CallCenterUser) {
                        return $model->person ? $model->person->skype : null;
                    } elseif ($model instanceof Person) {
                        return $model->skype;
                    }
                    return '';
                },
                'visible' => $this->import->cell_skype
            ],
            [
                'attribute' => 'person_phone',
                'label' => Yii::t('common', 'Телефон'),
                'content' => function ($model) {
                    if ($model instanceof PersonImportData) {
                        return $model->person_phone;
                    } elseif ($model instanceof CallCenterUser) {
                        return $model->person ? $model->person->phone : null;
                    } elseif ($model instanceof Person) {
                        return $model->phone;
                    }
                    return '';
                },
                'visible' => $this->import->cell_phone
            ],
            [
                'attribute' => 'person_passport',
                'label' => Yii::t('common', 'Паспорт'),
                'content' => function ($model) {
                    if ($model instanceof PersonImportData) {
                        return $model->person_passport;
                    } elseif ($model instanceof CallCenterUser) {
                        return $model->person ? $model->person->passport_id_number : null;
                    } elseif ($model instanceof Person) {
                        return $model->passport_id_number;
                    }
                    return '';
                },
                'visible' => $this->import->cell_passport
            ],
            [
                'attribute' => 'person_employment_date',
                'label' => Yii::t('common', 'Дата трудоустройства'),
                'content' => function ($model) {
                    if ($model instanceof PersonImportData) {
                        return $model->person_employment_date;
                    } elseif ($model instanceof CallCenterUser) {
                        return $model->person ? $model->person->start_date : null;
                    } elseif ($model instanceof Person) {
                        return $model->start_date;
                    }
                    return '';
                },
                'visible' => $this->import->cell_employment_date
            ],
            [
                'attribute' => 'working_shift_id',
                'content' => function ($model) {
                    if ($model instanceof PersonImportData) {
                        return $model->workingShift ? $model->workingShift->name : null;
                    } elseif ($model instanceof CallCenterUser) {
                        return $model->person && $model->person->workingShift ? $model->person->workingShift->name : null;
                    } elseif ($model instanceof Person) {
                        return $model->workingShift ? $model->workingShift->name : null;
                    }
                    return '';
                },
            ],
            [
                'attribute' => 'person_termination_date',
                'label' => Yii::t('common', 'Дата увольнения'),
                'content' => function ($model) {
                    if ($model instanceof PersonImportData) {
                        return $model->person_termination_date;
                    } elseif ($model instanceof CallCenterUser) {
                        return $model->person ? $model->person->dismissal_date : null;
                    } elseif ($model instanceof Person) {
                        return $model->dismissal_date;
                    }
                    return '';
                },
                'visible' => $this->import->cell_termination_date
            ],
            [
                'attribute' => 'person_account_number',
                'label' => Yii::t('common', 'Номер счета'),
                'content' => function ($model) {
                    if ($model instanceof PersonImportData) {
                        return $model->person_account_number;
                    } elseif ($model instanceof CallCenterUser) {
                        return $model->person ? $model->person->account_number : null;
                    } elseif ($model instanceof Person) {
                        return $model->account_number;
                    }
                    return '';
                },
                'visible' => $this->import->cell_account_number
            ],
            [
                'attribute' => 'message',
                'label' => Yii::t('common', 'Сообщение'),
                'content' => function ($model) {
                    $content = '';
                    if ($model instanceof PersonImportData) {
                        if ($model->status == $model::STATUS_IGNORE) {
                            $content .= Label::widget([
                                'label' => Yii::t('common', 'Исключен из импорта'),
                                'style' => Label::STYLE_DARK,
                            ]);
                        } elseif ($model->status == $model::STATUS_PENDING) {

                            if ($model->importErrors) {

                                foreach ($model->importErrors as $error) {
                                    $content .= Label::widget([
                                        'label' => $error,
                                        'style' => Label::STYLE_DANGER,
                                    ]);
                                }
                            } else {

                                foreach ($model->importWarning as $warning) {
                                    $content .= Label::widget([
                                        'label' => $warning,
                                        'style' => Label::STYLE_WARNING,
                                    ]);
                                }

                                if (($model->import->cell_login || $this->import->cell_id) && !$model->hasUserIdentifier()) {
                                    $content .= Label::widget([
                                        'label' => Yii::t('common', 'Импорт без привязки к пользователю'),
                                        'style' => Label::STYLE_WARNING,
                                    ]);
                                }
                            }

                        } elseif ($model->status == $model::STATUS_IMPORT) {
                            $content .= Label::widget([
                                'label' => Yii::t('common', 'Импортирован'),
                                'style' => Label::STYLE_SUCCESS,
                            ]);
                        } elseif ($model->status == $model::STATUS_IMPORT_ONLY_PERSON) {
                            $content .= Label::widget([
                                'label' => Yii::t('common', 'Импортирован без привязки к пользователю'),
                                'style' => Label::STYLE_SUCCESS,
                            ]);
                        }
                    } elseif ($model instanceof Person) {
                        if (!$model->active) {
                            $content .= Label::widget([
                                'label' => ($model->dismissal_date) ? Yii::t('common', 'Сотрудник уволен') : Yii::t('common', 'Сотрудник неактивен'),
                                'style' => Label::STYLE_WARNING,
                            ]);
                        }
                    }
                    return $content;
                },
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/salary/person-import/record-edit', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return $model instanceof PersonImportData && Yii::$app->user->can('salary.personimport.recordedit') && !$model->import->isImported();
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Исключить из иморта'),
                        'url' => function ($model) {
                            return Url::toRoute(['/salary/person-import/record-exclude', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return $model instanceof PersonImportData && Yii::$app->user->can('salary.personimport.recordexclude') && !$model->import->isImported() && $model->status != $model::STATUS_IGNORE;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Включить в импорт'),
                        'url' => function ($model) {
                            return Url::toRoute(['/salary/person-import/record-include', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return $model instanceof PersonImportData && Yii::$app->user->can('salary.personimport.recordinclude') && !$model->import->isImported() && $model->status == $model::STATUS_IGNORE;
                        }
                    ],
                ],
                'visible' => !$this->import->isImported()
            ]
        ];

        parent::initColumns();
    }
}
