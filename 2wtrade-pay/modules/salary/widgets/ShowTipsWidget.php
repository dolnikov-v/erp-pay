<?php

namespace app\modules\salary\widgets;

use app\modules\salary\models\Designation;
use app\modules\salary\models\Office;

/**
 * Class ShowTipsWidget
 * @package app\modules\salary\widgets
 */
class ShowTipsWidget extends Sender
{

    /**
     * @var array
     */
    public $tips;

    /**
     * @var integer
     */
    public $sizeOfTips;

    /**
     * @return string
     */
    public function run()
    {
        $officeNames = Office::find()->active()->collection();
        $designationNames = Designation::find()->collection();

        return $this->render('show-tips/modal', [
            'tips' => $this->tips,
            'sizeOfTips' => $this->sizeOfTips,
            'officeNames' => $officeNames,
            'designationNames' => $designationNames,
        ]);
    }
}
