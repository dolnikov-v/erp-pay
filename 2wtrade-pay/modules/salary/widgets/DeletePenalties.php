<?php

namespace app\modules\salary\widgets;

/**
 * Class DeletePenalties
 * @package app\modules\salary\widgets
 */
class DeletePenalties extends Sender
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('delete-penalties/modal', [
            'url' => $this->url
        ]);
    }
}
