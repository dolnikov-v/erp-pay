<?php
namespace app\modules\salary\widgets;

use app\widgets\Modal;
use Yii;

/**
 * Class ModalConfirmWorkingShift
 * @package app\modules\salary\widgets
 */
class ModalConfirmWorkingShift extends Modal
{

    /**
     * @var string
     */
    public $url;

    /**
     * @var integer
     */
    public $minDate;

    /**
     * @return string
     */
    public function run()
    {

        if (!$this->minDate) {
            $this->minDate = strtotime("+1day");
        }

        return $this->render('modal-confirm-working-shift', [
            'id' => 'modal_confirm_working_shift',
            'size' => self::SIZE_MEDIUM,
            'color' => self::COLOR_SUCCESS,
            'title' => Yii::t('common', 'Подтверждение изменения рабочей смены'),
            'close' => true,
            'url' => $this->url,
            'minDate' => $this->minDate,
        ]);
    }
}
