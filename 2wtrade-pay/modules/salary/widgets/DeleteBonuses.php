<?php

namespace app\modules\salary\widgets;

/**
 * Class DeleteBonuses
 * @package app\modules\salary\widgets
 */
class DeleteBonuses extends Sender
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('delete-bonuses/modal', [
            'url' => $this->url
        ]);
    }
}
