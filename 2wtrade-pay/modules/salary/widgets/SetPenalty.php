<?php

namespace app\modules\salary\widgets;

use app\modules\salary\models\PenaltyType;

/**
 * Class SetPenalty
 * @package app\modules\salary\widgets
 */
class SetPenalty extends Sender
{

    public $officeId;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('set-penalty/modal', [
            'url' => $this->url,
            'types' => PenaltyType::find()->collection(),
            'officeId' => $this->officeId
        ]);
    }
}
