<?php
namespace app\modules\salary\widgets;

use app\modules\salary\models\Office;
use app\widgets\Nav;
use Yii;
use yii\helpers\Url;

/**
 * Class OfficeLinkNav
 * @package app\modules\salary\widgets
 */
class OfficeLinkNav extends Nav
{
    /**
     * @var Office;
     */
    public $office;


    public function init()
    {
        $actionId = Yii::$app->controller->action->id;

        $this->tabs = [
            [
                'label' => Yii::t('common', 'Просмотр'),
                'url' => Url::toRoute(['view', 'id' => $this->office->id]),
                'can' => Yii::$app->user->can('salary.office.view'),
                'active' => $actionId == 'view'
            ],
            [
                'label' => Yii::t('common', 'Редактировать'),
                'url' => Url::toRoute(['edit', 'id' => $this->office->id]),
                'can' => Yii::$app->user->can('salary.office.edit'),
                'active' => $actionId == 'edit'
            ],
            [
                'label' => Yii::t('common', 'Системные настройки'),
                'url' => Url::toRoute(['edit-system', 'id' => $this->office->id]),
                'can' => Yii::$app->user->can('salary.office.editsystem'),
                'active' => $actionId == 'edit-system'
            ],
            [
                'label' => Yii::t('common', 'Шаблон письма с ДР'),
                'url' => Url::toRoute(['edit-greeting', 'id' => $this->office->id]),
                'can' => Yii::$app->user->can('salary.office.editgreeting'),
                'active' => $actionId == 'edit-greeting'
            ],
            [
                'label' => Yii::t('common', 'Постоянные расходы'),
                'url' => Url::toRoute(['edit-expenses', 'id' => $this->office->id]),
                'can' => Yii::$app->user->can('salary.office.editexpenses'),
                'active' => $actionId == 'edit-expenses'
            ],
            [
                'label' => Yii::t('common', 'Налоговые ставки'),
                'url' => Url::toRoute(['edit-tax', 'id' => $this->office->id]),
                'can' => Yii::$app->user->can('salary.office.edittax'),
                'active' => $actionId == 'edit-tax'
            ],
            [
                'label' => Yii::t('common', 'Направления колл-центра'),
                'url' => Url::toRoute(['edit-direction', 'id' => $this->office->id]),
                'can' => $this->office->type === Office::TYPE_CALL_CENTER && Yii::$app->user->can('salary.office.editdirection'),
                'active' => $actionId == 'edit-direction'
            ],
            [
                'label' => Yii::t('common', 'Рабочие смены'),
                'url' => Url::toRoute(['edit-working', 'id' => $this->office->id]),
                'can' => Yii::$app->user->can('salary.office.editworking'),
                'active' => $actionId == 'edit-working'
            ],
            [
                'label' => Yii::t('common', 'Штатное расписание'),
                'url' => Url::toRoute(['edit-staffing', 'id' => $this->office->id]),
                'can' => Yii::$app->user->can('salary.office.editstaffing'),
                'active' => $actionId == 'edit-staffing'
            ],
        ];
    }

    /**
     * @return string
     */
    public function renderTabs()
    {
        return $this->render('office-link-nav', [
            'id' => $this->id,
            'typeNav' => $this->typeNav,
            'typeTabs' => $this->typeTabs,
            'tabs' => $this->tabs,
        ]);
    }

    /**
     * @return string
     */
    public function renderContent()
    {
        return "";
    }
}
