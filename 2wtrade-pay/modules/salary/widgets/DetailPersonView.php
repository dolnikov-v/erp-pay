<?php
namespace app\modules\salary\widgets;

use app\modules\callcenter\models\CallCenterUser;
use app\modules\report\components\ReportFormSalary;
use app\modules\salary\models\Person;
use Yii;
use yii\widgets\DetailView;

/**
 * Class DetailPersonView
 * @package app\modules\salary\widgets
 */
class DetailPersonView extends DetailView
{
    /**
     * @inheritdoc
     */
    public function init()
    {

        /** @var Person $model */
        $model = $this->model;

        $logins = [];
        $directions = [];
        foreach ($model->callCenterUsers as $login) {
            /** @var CallCenterUser $login */
            if (isset($login->callCenter->country->name)) {
                $directions[] = Yii::t('common', $login->callCenter->country->name);
            }
            $logins[] = $login->user_login;
        }

        $this->attributes[] = [
            'label' => Yii::t('common', 'ФИО'),
            'value' => $model->name
        ];
        $this->attributes[] = [
            'label' => Yii::t('common', 'Должность'),
            'value' => isset($model->designation->name) ? Yii::t('common', $model->designation->name) : ''
        ];
        $this->attributes[] = [
            'label' => Yii::t('common', 'Дата приема на работу'),
            'value' => $model->start_date,
            'format' => ['date'],
            'visible' => $model->start_date != ''
        ];
        $this->attributes[] = [
            'label' => Yii::t('common', 'Дата увольнения'),
            'value' => $model->dismissal_date,
            'format' => ['date'],
            'visible' => $model->dismissal_date != ''
        ];
        if (Yii::$app->user->can('view_salary_person_numbers')) {
            $this->attributes[] = [
                'label' => Yii::t('common', 'Номер счета'),
                'value' => $model->account_number,
            ];
            $this->attributes[] = [
                'label' => Yii::t('common', 'Название банка'),
                'value' => $model->bank_name,
            ];
            $this->attributes[] = [
                'label' => Yii::t('common', 'Адрес банка'),
                'value' => $model->bank_address,
            ];
            $this->attributes[] = [
                'label' => Yii::t('common', 'СВИФТ–код банка'),
                'value' => $model->bank_swift_code,
            ];
            $this->attributes[] = [
                'label' => Yii::t('common', 'Номер ID/Паспорта'),
                'value' => $model->passport_id_number,
            ];
        }
        $this->attributes[] = [
            'label' => Yii::t('common', 'Руководитель'),
            'value' => (!empty($model->parent_id && isset($model->parent->designation->name) && isset($model->parent->name)) ?
                Yii::t('common', $model->parent->designation->name) . ': ' . $model->parent->name : '-')
        ];
        $this->attributes[] = [
            'label' => Yii::t('common', 'Офис'),
            'value' => isset($model->office->name) ? Yii::t('common', $model->office->name) : '-'
        ];
        $this->attributes[] = [
            'label' => Yii::t('common', 'Направление'),
            'value' => $directions ? implode(', ', $directions) : '-'
        ];
        $this->attributes[] = [
            'label' => Yii::t('common', 'Логин'),
            'value' => $logins ? implode(', ', $logins) : '-'
        ];
        $this->attributes[] = [
            'label' => Yii::t('common', 'Оклад в местной валюте в месяц'),
            'format' => ['decimal', 2],
            'value' => $model->salary_local ?? 0,
            'visible' => $model->office->calc_salary_local && $model->salary_scheme == Person::SALARY_SCHEME_PER_MONTH
        ];
        $this->attributes[] = [
            'attribute' => 'salary_usd',
            'label' => Yii::t('common', 'Оклад в USD'),
            'format' => ['decimal', 2],
            'value' => $model->salary_usd ?? 0,
            'visible' => $model->office->calc_salary_usd  && $model->salary_scheme == Person::SALARY_SCHEME_PER_MONTH
        ];
        $this->attributes[] = [
            'attribute' => 'salary_hour_local',
            'label' => Yii::t('common', 'Оклад в местной валюте в час'),
            'format' => ['decimal', 2],
            'value' => $model->salary_hour_local ?? 0,
            'visible' => $model->office->calc_salary_local && $model->salary_scheme == Person::SALARY_SCHEME_HOURLY
        ];
        $this->attributes[] = [
            'attribute' => 'salary_hour_usd',
            'label' => Yii::t('common', 'Оклад в USD в час'),
            'format' => ['decimal', 2],
            'value' => $model->salary_hour_usd ?? 0,
            'visible' => $model->office->calc_salary_usd && $model->salary_scheme == Person::SALARY_SCHEME_HOURLY
        ];
        parent::init();
    }
}