<?php

namespace app\modules\salary\widgets;

/**
 * Class DeleteTimePlans
 * @package app\modules\salary\widgets
 */
class DeleteTimePlans extends Sender
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('delete-time-plans/modal', [
            'url' => $this->url
        ]);
    }
}
