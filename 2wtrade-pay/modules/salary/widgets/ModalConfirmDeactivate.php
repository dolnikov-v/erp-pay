<?php
namespace app\modules\salary\widgets;

use app\widgets\Modal;
use Yii;

/**
 * Class ModalConfirmDeactivate
 * @package app\modules\salary\widgets
 */
class ModalConfirmDeactivate extends Modal
{

    /**
     * @var string
     */
    public $url;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('modal-confirm-deactivate/modal', [
            'id' => 'modal_confirm_deactivate',
            'size' => self::SIZE_MEDIUM,
            'color' => self::COLOR_DANGER,
            'title' => Yii::t('common', 'Подтверждение увольнения'),
            'close' => true,
        ]);
    }
}
