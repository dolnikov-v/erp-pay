<?php

namespace app\modules\salary\widgets;

use app\modules\salary\models\Designation;

/**
 * Class SetRole
 * @package app\modules\salary\widgets
 */
class SetRole extends Sender
{
    public function getDesignations()
    {
        return Designation::find()->where(['active' => 1])->orderBy(['name' => SORT_ASC])->collection();
    }

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('set-role/modal', [
            'url' => $this->url,
            'designations' => $this->getDesignations()
        ]);
    }
}
