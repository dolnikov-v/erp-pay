<?php

namespace app\modules\salary\widgets;

/**
 * Class ApprovePersons
 * @package app\modules\salary\widgets
 */
class ApprovePersons extends Sender
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('approve-persons/modal', [
            'url' => $this->url
        ]);
    }
}
