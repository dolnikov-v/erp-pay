<?php

namespace app\modules\salary\widgets;

use app\modules\salary\models\BonusType;

/**
 * Class SetBonus
 * @package app\modules\salary\widgets
 */
class SetBonus extends Sender
{

    public $officeId;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('set-bonus/modal', [
            'url' => $this->url,
            'types' => BonusType::find()->byOfficeId($this->officeId)->collection(),
            'officeId' => $this->officeId
        ]);
    }
}
