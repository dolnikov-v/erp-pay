<?php

namespace app\modules\salary\widgets;

use kartik\grid\GridView;

/**
 * Class GridViewStaffing
 * @package app\modules\salary\widgets
 */
class GridViewStaffing extends GridView
{
    /**
     * @var array
     */
    public $tableOptions = [
        'class' => 'table table-report table-striped table-hover tl-fixed',
    ];

    /**
     * @var array|boolean
     */
    public $pageSummaryRowOptions = [
        'class' => 'warning',
    ];

    /**
     * @var string
     */
    public $layout = '{items}';

    /**
     * @var string
     */
    public $toolbar = '';
}
