<?php

namespace app\modules\salary\widgets;

/**
 * Class ShowJobDescriptionWidget
 * @package app\modules\salary\widgets
 */
class ShowJobDescriptionWidget extends Sender
{

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('show-jobs-description/modal', [
            'url' => $this->url
        ]);
    }
}
