<?php

namespace app\modules\salary\widgets;

/**
 * Class NotapprovePersons
 * @package app\modules\salary\widgets
 */
class NotapprovePersons extends Sender
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('notapprove-persons/modal', [
            'url' => $this->url
        ]);
    }
}
