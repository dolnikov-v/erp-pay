<?php

namespace app\modules\salary\widgets;

/**
 * Class DeactivatePersons
 * @package app\modules\salary\widgets
 */
class DeactivatePersons extends Sender
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('deactivate-persons/modal', [
            'url' => $this->url
        ]);
    }
}
