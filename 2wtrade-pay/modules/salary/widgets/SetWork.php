<?php

namespace app\modules\salary\widgets;

use app\modules\salary\models\WorkingShift;

/**
 * Class SetWork
 * @package app\modules\salary\widgets
 */
class SetWork extends Sender
{

    public $officeId;

    public function getWorkingShifts()
    {
        return WorkingShift::find()->byOfficeId($this->officeId)->collection();
    }

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('set-work/modal', [
            'url' => $this->url,
            'workingShifts' => $this->getWorkingShifts(),
            'officeId' => $this->officeId
        ]);
    }
}
