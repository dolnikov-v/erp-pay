<?php

namespace app\modules\salary\widgets;

use app\modules\salary\models\Person;

/**
 * Class ChangeTeamLeader
 * @package app\modules\salary\widgets
 */
class ChangeTeamLeader extends Sender
{
    public $officeId;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('change-team-leader/modal', [
            'url' => $this->url,
            'users' => Person::find()->isTeamLead()->bySystemUserCountries()->byOfficeId($this->officeId)->collection(),
            'officeId' => $this->officeId
        ]);
    }
}
