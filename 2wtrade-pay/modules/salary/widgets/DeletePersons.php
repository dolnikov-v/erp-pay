<?php

namespace app\modules\salary\widgets;

/**
 * Class DeletePersons
 * @package app\modules\salary\widgets
 */
class DeletePersons extends Sender
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('delete-persons/modal', [
            'url' => $this->url
        ]);
    }
}
