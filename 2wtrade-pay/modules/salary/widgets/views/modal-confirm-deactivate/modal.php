<?php
use app\widgets\DateTimePicker;
use app\widgets\InputText;
use app\components\widgets\ActiveForm;
use yii\helpers\Url;
use app\widgets\Button;
use app\widgets\ButtonLink;
use app\widgets\Submit;


/** @var string $id */
/** @var string $size */
/** @var string $color */
/** @var string $title */
/** @var string $close */
/** @var string $body */
/** @var string $footer */
?>

<div id="<?= $id ?>" class="modal fade <?= $color ?> in" tabindex="-1" role="dialog" aria-hidden="true">

    <?php $form = ActiveForm::begin([
        'enableClientValidation' => true,
        'action' => Url::toRoute('deactivate'),
        'method' => 'post',
        'options' => ['enctype' => 'multipart/form-data']
    ]); ?>

    <div class="modal-dialog <?= $size ?>">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" aria-label="Close" data-dismiss="modal" type="button">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title"><?= $title ?></h4>
            </div>

            <div class="modal-body text-center">
                <?= Yii::t('common', 'Вы уверены, что хотите уволить сотрудника? При наличии записи в Call, она будет заблокирована?') ?>
            </div>

            <div class="row">
                <div class="col-xs-4 text-right">
                    <?= Yii::t('common', 'Дата увольнения') ?>
                </div>
                <div class="col-xs-7">
                    <?= DateTimePicker::widget([
                        'value' => date("Y-m-d"),
                        'name' => 'person_dismissal_date',
                        'attributes' => [
                            'id' => 'person_dismissal_date'
                        ]
                    ]) ?>
                </div>
                <div class="col-xs-1"></div>
            </div>

            <div class="row mt15">
                <div class="col-xs-4 text-right">
                    <?= Yii::t('common', 'Причина увольнения') ?>
                </div>
                <div class="col-xs-7">
                    <?= InputText::widget([
                        'name' => 'person_dismissal_comment',
                        'attributes' => [
                            'id' => 'person_dismissal_comment'
                        ]
                    ]) ?>
                </div>
                <div class="col-xs-1"></div>
            </div>

            <div class="row mt15">
                <div class="col-xs-4 text-right">
                    <?= Yii::t('common', 'Документ (заявление)') ?>
                </div>
                <div class="col-xs-7">
                    <?= \app\widgets\InputGroupFile::widget([
                        'right' => false,
                        'input' => InputText::widget([
                            'type' => 'file',
                            'name' => 'person_dismissal_file',
                        ]),
                    ]) ?>
                </div>
                <div class="col-xs-1"></div>
            </div>

            <div class="modal-footer mt15 text-right">
                <?= Button::widget([
                    'label' => Yii::t('common', 'Отмена'),
                    'size' => Button::SIZE_SMALL,
                    'attributes' => [
                        'data-dismiss' => 'modal',
                    ],
                ]) ?>

                <?= Submit::widget([
                    'id' => 'modal_confirm_deactivate_link',
                    'label' => Yii::t('common', 'Уволить'),
                    'style' => ButtonLink::STYLE_DANGER,
                    'size' => Button::SIZE_SMALL,
                ]) ?>

            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
