<?php
use app\widgets\Modal;
/** @var string $url */
/** @var array $users */
?>

<?= Modal::widget([
    'id' => 'modal_deactivate_persons',
    'title' => Yii::t('common', 'Увольнение сотрудников'),
    'body' => $this->render('_modal-body', [
        'url' => $url
    ]),
]) ?>


