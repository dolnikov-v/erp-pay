<?php
use app\widgets\Modal;
/** @var string $url */
/** @var array $users */
?>

<?= Modal::widget([
    'id' => 'modal_delete_penalties',
    'title' => Yii::t('common', 'Удаление штрафов'),
    'body' => $this->render('_modal-body', [
        'url' => $url
    ]),
]) ?>


