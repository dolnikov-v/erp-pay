<?php
use app\widgets\Modal;

/** @var string $url */
/** @var array $types */
/** @var integer $officeId */

?>

<?= Modal::widget([
    'id' => 'modal_set_bonus',
    'title' => Yii::t('common', 'Назначить бонус'),
    'body' => $this->render('_modal-body', [
        'url' => $url,
        'types' => $types,
        'officeId' => $officeId
    ]),
]) ?>