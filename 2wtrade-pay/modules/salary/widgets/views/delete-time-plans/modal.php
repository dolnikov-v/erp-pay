<?php
use app\widgets\Modal;
/** @var string $url */
?>

<?= Modal::widget([
    'id' => 'modal_delete_list',
    'title' => Yii::t('common', 'Удаление планов'),
    'body' => $this->render('_modal-body', [
        'url' => $url
    ]),
]) ?>


