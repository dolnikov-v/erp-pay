<?php
use app\widgets\Button;
use app\widgets\ButtonLink;
use app\helpers\FontAwesome;

/** @var string $url */

?>

<div class="row-with-text-start url_holder" data-url="<?= $url ?>">
    <div id="job_description_text">

    </div>
    <div class="text-center">
        <?= ButtonLink::widget([
            'id' => 'job_description_file',
            'icon' => '<i class="fa '.FontAwesome::DOWNLOAD.'"></i>',
            'style' => Button::STYLE_SUCCESS,
            'label' => Yii::t('common', 'Скачать'),
            'url' => 'bla'
        ]) ?>
    </div>

    <div id="no_job_description" style="display: none">
        <?= Yii::t('common', 'Должностные инструкции еще не загружены') ?>
    </div>
</div>

<div class="padding-top-20">
    <div class="row-with-btn-start text-right">
        <?= Button::widget([
            'label' => Yii::t('common', 'Закрыть'),
            'size' => Button::SIZE_SMALL,
            'attributes' => [
                'data-dismiss' => 'modal',
            ],
        ]) ?>
    </div>
</div>

