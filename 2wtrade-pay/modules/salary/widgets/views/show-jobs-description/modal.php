<?php
use app\widgets\Modal;

/** @var string $url */

?>

<?= Modal::widget([
    'id' => 'modal_job_description',
    'title' => Yii::t('common', 'Должностные инструкции'),
    'body' => $this->render('_modal-body', [
        'url' => $url
    ]),
]) ?>


