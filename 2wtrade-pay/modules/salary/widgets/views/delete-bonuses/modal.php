<?php
use app\widgets\Modal;
/** @var string $url */
/** @var array $users */
?>

<?= Modal::widget([
    'id' => 'modal_delete_bonuses',
    'title' => Yii::t('common', 'Удаление бонусов'),
    'body' => $this->render('_modal-body', [
        'url' => $url
    ]),
]) ?>


