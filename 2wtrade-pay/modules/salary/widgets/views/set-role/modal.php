<?php
use app\widgets\Modal;

/** @var string $url */
/** @var array $designations */

?>

<?= Modal::widget([
    'id' => 'modal_set_role',
    'title' => Yii::t('common', 'Установить должность'),
    'body' => $this->render('_modal-body', [
        'url' => $url,
        'designations' => $designations
    ]),
]) ?>


