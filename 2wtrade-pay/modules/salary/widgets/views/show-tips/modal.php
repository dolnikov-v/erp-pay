<?php
use app\widgets\Modal;

/** @var array $tips */
/** @var integer $sizeOfTips */
/** @var array $officeNames */
/** @var array $designationNames */
?>

<?= Modal::widget([
    'id' => 'modal_show_tips',
    'title' => Yii::t('common', 'Рекомендации').' ('.$sizeOfTips.')',
    'body' => $this->render('_modal-body', [
        'tips' => $tips,
        'officeNames' => $officeNames,
        'designationNames' => $designationNames
    ]),
]) ?>


