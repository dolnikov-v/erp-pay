<?php
use app\widgets\Button;

/** @var array $tips */
/** @var array $officeNames */
/** @var array $designationNames */
/** @var integer $sizeOfTips */

?>

<div class="row-with-text-start">
    <div class="">
        <?php foreach ($tips as $officeId => $officeTips): ?>
            <b><?= isset($officeNames[$officeId]) ? Yii::t('common', $officeNames[$officeId]) : '-' ?></b>
            <ul>
                <?php foreach ($officeTips as $designationId => $designationTips): ?>
                    <li><?= isset($designationNames[$designationId]) ? Yii::t('common', $designationNames[$designationId]) . ':' : '-' ?>
                        <?php foreach ($designationTips as $tipKey => $tipText): ?>
                            <?php if (is_array($tipText)): ?>
                                <?= implode('<br>', $tipText) ?>
                            <?php else: ?>
                                <?= $tipText ?><br>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </li>
                <?php endforeach; ?>
            </ul>
        <?php endforeach; ?>
    </div>
</div>

<div class="padding-top-20">
    <div class="row-with-btn-start text-right">
        <?= Button::widget([
            'label' => Yii::t('common', 'Закрыть'),
            'size' => Button::SIZE_SMALL,
            'attributes' => [
                'data-dismiss' => 'modal',
            ],
        ]) ?>
    </div>
</div>

