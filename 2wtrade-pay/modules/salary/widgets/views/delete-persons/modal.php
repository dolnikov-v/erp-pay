<?php
use app\widgets\Modal;
/** @var string $url */
/** @var array $users */
?>

<?= Modal::widget([
    'id' => 'modal_delete_persons',
    'title' => Yii::t('common', 'Удаление сотрудников'),
    'body' => $this->render('_modal-body', [
        'url' => $url
    ]),
]) ?>


