<?php
use app\widgets\Modal;
/** @var string $url */
/** @var array $users */
/** @var integer $officeId */
?>

<?= Modal::widget([
    'id' => 'modal_change_team_leader',
    'title' => Yii::t('common', 'Смена руководителя'),
    'body' => $this->render('_modal-body', [
        'url' => $url,
        'users' => $users,
        'officeId' => $officeId
    ]),
]) ?>


