<?php
use app\widgets\Modal;
/** @var string $url */
/** @var array $users */
?>

<?= Modal::widget([
    'id' => 'modal_notapprove_persons',
    'title' => Yii::t('common', 'Убрать подтверждение у сотрудников'),
    'body' => $this->render('_modal-body', [
        'url' => $url
    ]),
]) ?>


