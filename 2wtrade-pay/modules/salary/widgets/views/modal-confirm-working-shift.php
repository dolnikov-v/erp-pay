<?php
use app\widgets\Button;
use app\widgets\ButtonLink;
use yii\helpers\Html;
use app\widgets\DateRangePicker;

/** @var string $id */
/** @var string $size */
/** @var string $color */
/** @var string $title */
/** @var string $close */
/** @var string $url */
/** @var integer $minDate */
?>

<div id="<?= $id ?>" class="modal fade <?= $color ?> in" tabindex="-1" role="dialog" aria-hidden="true"
     data-url="<?= $url ?>">
    <div class="modal-dialog <?= $size ?>">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" aria-label="Close" data-dismiss="modal" type="button">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title"><?= $title ?></h4>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <p><?= Yii::t('common', 'Вы действительно хотите изменить рабочую смену?') ?></p>
                </div>
                <div>
                    <p><?= Yii::t('common', 'Сотрудник') ?>: <span id="modal_confirm_working_shift_person"></span></p>
                    <p><?= Yii::t('common', 'Новая смена') ?>: <span id="modal_confirm_working_shift_name"></span></p>
                    <p><?= Yii::t('common', 'Сгенерировать рабочий план на даты') ?>:</p>
                </div>
                <div>
                    <?= DateRangePicker::widget([
                        'nameFrom' => 'working_shift_from',
                        'nameTo' => 'working_shift_to',
                        'hideRanges' => true,
                        'minDate' => date('Y-m-d', $minDate),
                        'valueFrom' => date('Y-m-d', strtotime("+1day")),
                        // TODO баг селектора дат при выставлении minDate в будущее, получается valueTo = valueFrom, а не то что ему задал
                        'valueTo' => date('Y-m-d', strtotime(date('Y-m-t', strtotime("next month")))),
                    ]) ?>
                </div>
                <br>
            </div>
            <div class="modal-footer text-right">
                <?= Button::widget([
                    'id' => 'modal_confirm_working_shift_cancel',
                    'label' => Yii::t('common', 'Отмена'),
                    'size' => Button::SIZE_SMALL,
                    'attributes' => [
                        'data-dismiss' => 'modal',
                    ],
                ]) ?>

                <?= ButtonLink::widget([
                    'id' => 'modal_confirm_working_shift_ok',
                    'label' => Yii::t('common', 'ОК'),
                    'url' => '#',
                    'style' => ButtonLink::STYLE_DANGER,
                    'size' => Button::SIZE_SMALL,
                ]) ?>
            </div>
        </div>
    </div>
</div>
