<?php
use app\components\widgets\ActiveForm;
use app\widgets\Button;
use app\widgets\ProgressBar;
use yii\helpers\Url;
use app\widgets\Select2;

/** @var string $url */
/** @var array $workingShifts */
/** @var integer $officeId */
?>

<?php $form = ActiveForm::begin([
    'enableClientValidation' => false,
    'action' => Url::toRoute('index'),
    'method' => 'post',
    'options' => [
        'data-sender-url' => $url,
    ]
]); ?>


<div class="row-with-text-start">
    <?php if ($officeId): ?>
        <div class="text-center">
            <?= Yii::t('common', 'Выберите рабочую смену') ?>
        </div>
        <div class="row help-block">
            <div class="col-xs-2"></div>
            <div class="col-xs-8">
                <?= Select2::widget([
                    'id' => 'set_work_select',
                    'items' => $workingShifts,
                    'length' => -1,
                    'autoEllipsis' => true,
                ]) ?>
            </div>
            <div class="col-xs-2"></div>
        </div>
    <?php else: ?>
        <div class="text-center">
            <?= Yii::t('common', 'Необходимо отфильтровать список сотрудников по Офису') ?>
        </div>
    <?php endif; ?>
</div>

<div class="row-with-text-stop" style="display: none;">
    <h5>
        <?= Yii::t('common', 'Установка рабочей смены') ?>
        <span class="pull-right sender-percent">0%</span>
    </h5>
    <?= ProgressBar::widget() ?>
</div>

<div class="padding-top-20">
    <div class="row-with-btn-start text-right">
        <?php if ($officeId): ?>
            <?= Button::widget([
                'id' => 'start_set_work',
                'label' => Yii::t('common', 'Установить'),
                'style' => Button::STYLE_SUCCESS,
                'size' => Button::SIZE_SMALL,
            ]) ?>
        <?php endif; ?>

        <?= Button::widget([
            'label' => Yii::t('common', 'Закрыть'),
            'size' => Button::SIZE_SMALL,
            'attributes' => [
                'data-dismiss' => 'modal',
            ],
        ]) ?>
    </div>

    <div class="row-with-btn-stop" style="display: none;">
        <?= Button::widget([
            'id' => 'stop_set_work',
            'label' => Yii::t('common', 'Остановить'),
            'style' => Button::STYLE_DANGER . ' btn-block',
            'size' => Button::SIZE_SMALL,
        ]) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
