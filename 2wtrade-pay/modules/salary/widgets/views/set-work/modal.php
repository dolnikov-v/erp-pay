<?php
use app\widgets\Modal;

/** @var string $url */
/** @var array $workingShifts */
/** @var integer $officeId */

?>

<?= Modal::widget([
    'id' => 'modal_set_work',
    'title' => Yii::t('common', 'Установить рабочую смену'),
    'body' => $this->render('_modal-body', [
        'url' => $url,
        'workingShifts' => $workingShifts,
        'officeId' => $officeId
    ]),
]) ?>