<?php
use app\components\widgets\ActiveForm;
use app\widgets\Button;
use app\widgets\ProgressBar;
use yii\helpers\Url;
use app\widgets\Select2;
use app\widgets\InputText;
use app\widgets\DateTimePicker;
use app\modules\salary\models\PenaltyType;


/** @var string $url */
/** @var array $types */
/** @var integer $officeId */
?>

<?php $form = ActiveForm::begin([
    'enableClientValidation' => false,
    'action' => Url::toRoute('index'),
    'method' => 'post',
    'options' => [
        'data-sender-url' => $url,
    ]
]); ?>


<div class="row-with-text-start">
    <?php if ($officeId): ?>
        <div class="text-center">
            <?= Yii::t('common', 'Выберите тип штрафа') ?>
        </div>
        <div class="row help-block">
            <div class="col-xs-2"></div>
            <div class="col-xs-8">
                <?= Select2::widget([
                    'id' => 'set_penalty_type',
                    'items' => $types,
                    'length' => -1,
                    'autoEllipsis' => true,
                    'prompt' => '-'
                ]) ?>
            </div>
            <div class="col-xs-2"></div>
        </div>

        <div class="row">
            <div class="col-xs-5 text-right">
                <?= Yii::t('common', 'Дата штрафа') ?>
            </div>
            <div class="col-xs-5">
                <?= DateTimePicker::widget([
                    'value' => date("Y-m-d"),
                    'attributes' => ['id' => 'set_penalty_date']
                ]) ?>
            </div>
            <div class="col-xs-2"></div>
        </div>

        <div class="row limit limit_<?= PenaltyType::PENALTY_TYPE_PERCENT ?> limit_<?= PenaltyType::PENALTY_TYPE_OTHER ?>" style="display: none">
            <div class="col-xs-5 text-right">
                <?= Yii::t('common', 'Процент штрафа') ?>
            </div>
            <div class="col-xs-5">
                <?= InputText::widget([
                    'name' => 'set_penalty_percent',
                    'attributes' => ['id' => 'set_penalty_percent']
                ]) ?>
            </div>
            <div class="col-xs-2"></div>
        </div>

        <div class="row limit limit_<?= PenaltyType::PENALTY_TYPE_SUM ?> limit_<?= PenaltyType::PENALTY_TYPE_OTHER ?>" style="display: none">
            <div class="col-xs-5 text-right">
                <?= Yii::t('common', 'Сумма штрафа в USD') ?>
            </div>
            <div class="col-xs-5">
                <?= InputText::widget([
                    'name' => 'set_penalty_sum_usd',
                    'attributes' => ['id' => 'set_penalty_sum_usd']
                ]) ?>
            </div>
            <div class="col-xs-2"></div>
        </div>

        <div class="row limit limit_<?= PenaltyType::PENALTY_TYPE_SUM ?> limit_<?= PenaltyType::PENALTY_TYPE_OTHER ?>" style="display: none">
            <div class="col-xs-5 text-right">
                <?= Yii::t('common', 'Сумма штрафа в местной валюте') ?>
            </div>
            <div class="col-xs-5">
                <?= InputText::widget([
                    'name' => 'set_penalty_sum_local',
                    'attributes' => ['id' => 'set_penalty_sum_local']
                ]) ?>
            </div>
            <div class="col-xs-2"></div>
        </div>

        <div class="row">
            <div class="col-xs-5 text-right">
                <?= Yii::t('common', 'Комментарий') ?>
            </div>
            <div class="col-xs-5">
                <?= InputText::widget([
                    'name' => 'set_penalty_comment',
                    'attributes' => ['id' => 'set_penalty_comment']
                ]) ?>
            </div>
            <div class="col-xs-2"></div>
        </div>
    <?php else: ?>
        <div class="text-center">
            <?= Yii::t('common', 'Необходимо отфильтровать список сотрудников по Офису') ?>
        </div>
    <?php endif; ?>
</div>

<div class="row-with-text-stop" style="display: none;">
    <h5>
        <?= Yii::t('common', 'Назначение штрафа') ?>
        <span class="pull-right sender-percent">0%</span>
    </h5>
    <?= ProgressBar::widget() ?>
</div>

<div class="padding-top-20">
    <div class="row-with-btn-start text-right">
        <?php if ($officeId): ?>
            <?= Button::widget([
                'id' => 'start_set_penalty',
                'label' => Yii::t('common', 'Установить'),
                'style' => Button::STYLE_SUCCESS,
                'size' => Button::SIZE_SMALL,
            ]) ?>
        <?php endif; ?>

        <?= Button::widget([
            'label' => Yii::t('common', 'Закрыть'),
            'size' => Button::SIZE_SMALL,
            'attributes' => [
                'data-dismiss' => 'modal',
            ],
        ]) ?>
    </div>

    <div class="row-with-btn-stop" style="display: none;">
        <?= Button::widget([
            'id' => 'stop_set_penalty',
            'label' => Yii::t('common', 'Остановить'),
            'style' => Button::STYLE_DANGER . ' btn-block',
            'size' => Button::SIZE_SMALL,
        ]) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
