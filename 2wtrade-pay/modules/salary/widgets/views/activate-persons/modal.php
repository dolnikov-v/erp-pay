<?php
use app\widgets\Modal;
/** @var string $url */
/** @var array $users */
?>

<?= Modal::widget([
    'id' => 'modal_activate_persons',
    'title' => Yii::t('common', 'Активировать сотрудников'),
    'body' => $this->render('_modal-body', [
        'url' => $url
    ]),
]) ?>


