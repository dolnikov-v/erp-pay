<?php

namespace app\modules\salary\widgets;

/**
 * Class ActivatePersons
 * @package app\modules\salary\widgets
 */
class ActivatePersons extends Sender
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('activate-persons/modal', [
            'url' => $this->url
        ]);
    }
}
