<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/** @var app\modules\salary\models\search\HolidaySearch $modelSearch */
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute('index'),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($modelSearch, 'date')->dateRangePicker('dateFrom', 'dateTo') ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'country_id')->select2List(\app\models\Country::find()->bySystemUserCountries()->active()->orderBy(['name' => SORT_ASC])->collection(), ['prompt' => '-']) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->submit(Yii::t('common', 'Применить')); ?>
            <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')); ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>