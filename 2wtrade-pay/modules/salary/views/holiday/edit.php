<?php
use app\widgets\Panel;
use yii\helpers\Url;
use app\widgets\Nav;

/** @var yii\web\View $this */
/** @var app\modules\salary\models\Holiday $model */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление государственного праздника') : Yii::t('common', 'Редактирование государственного праздника');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Зарплата'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Управление государственными праздниками'), 'url' => Url::toRoute('/salary/holiday/index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'nav' => new Nav([
        'tabs' => [
            [
                'label' => Yii::t('common', 'Бонус'),
                'content' => $this->render('_edit-form', [
                    'model' => $model,
                ]),
            ],
        ]
    ])
]) ?>

