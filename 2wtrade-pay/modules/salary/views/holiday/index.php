<?php
use app\components\grid\ActionColumn;
use app\components\grid\DateColumn;
use app\components\grid\GridView;
use app\helpers\DataProvider;
use app\widgets\assets\custom\ModalConfirmDeleteAsset;
use app\widgets\ButtonLink;
use app\widgets\custom\ModalConfirmDelete;
use app\widgets\Panel;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use app\widgets\Nav;


/** @var yii\web\View $this */
/** @var app\modules\salary\models\search\HolidaySearch $modelSearch */
/** @var yii\data\ActiveDataProvider $dataProvider */

ModalConfirmDeleteAsset::register($this);

$this->title = Yii::t('common', 'Государственные праздники');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Зарплата'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

?>


<?= Panel::widget([
    'nav' => new Nav([
        'tabs' => [
            [
                'label' => Yii::t('common', 'Фильтр поиска'),
                'content' => $this->render('_nav-tab-filter', [
                    'modelSearch' => $modelSearch
                ]),
            ],
        ]
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с государственными праздниками'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' =>
        (Yii::$app->user->can('salary.holiday.edit') ?
            '<div class="add-wrap">' . ButtonLink::widget([
                'label' => Yii::t('common', 'Добавить государственный праздник'),
                'url' => Url::toRoute('/salary/holiday/edit'),
                'style' => ButtonLink::STYLE_SUCCESS,
                'size' => ButtonLink::SIZE_SMALL,
            ]) . '</div>' : '') .
        GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                [
                    'label' => Yii::t('common', 'Страна'),
                    'content' => function ($model) {
                        return Yii::t('common', $model->country->name);
                    }
                ],
                [
                    'attribute' => 'date',
                    'headerOptions' => ['class' => 'text-center'],
                    'contentOptions' => ['class' => 'text-center'],
                    'content' => function ($model) {
                        return Yii::$app->formatter->asDate($model->date);
                    },
                ],
                [
                    'attribute' => 'name',
                ],
                [
                    'class' => DateColumn::className(),
                    'attribute' => 'updated_at',
                ],
                [
                    'class' => DateColumn::className(),
                    'attribute' => 'created_at',
                ],
                [
                    'class' => ActionColumn::className(),
                    'items' => [
                        [
                            'label' => Yii::t('common', 'Редактировать'),
                            'url' => function ($model) {
                                return Url::toRoute(['/salary/holiday/edit', 'id' => $model->id]);
                            },
                            'can' => function () {
                                return Yii::$app->user->can('salary.holiday.edit');
                            }
                        ],
                        [
                            'can' => function () {
                                return Yii::$app->user->can('salary.control.edit');
                            }
                        ],
                        [
                            'label' => Yii::t('common', 'Удалить'),
                            'url' => function () {
                                return '#';
                            },
                            'style' => 'confirm-delete-link',
                            'attributes' => function ($model) {
                                return [
                                    'data-href' => Url::toRoute(['/salary/holiday/delete', 'id' => $model->id]),
                                ];
                            },
                            'can' => function () {
                                return Yii::$app->user->can('salary.holiday.delete');
                            }
                        ],
                    ]
                ]
            ],
        ]),
    'footer' => (Yii::$app->user->can('salary.holiday.edit') ?
            ButtonLink::widget([
                'label' => Yii::t('common', 'Добавить государственный праздник'),
                'url' => Url::toRoute('/salary/holiday/edit'),
                'style' => ButtonLink::STYLE_SUCCESS,
                'size' => ButtonLink::SIZE_SMALL,
            ]) : '') . LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>

<?= ModalConfirmDelete::widget() ?>
