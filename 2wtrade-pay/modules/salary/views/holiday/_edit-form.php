<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/** @var app\modules\salary\models\Holiday $model */

?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['edit', 'id' => $model->id])]); ?>
<div class="row">
    <div class="col-lg-4">
        <div class="form-group">
            <?= $form->field($model, 'country_id')
                ->select2List(
                    \app\models\Country::find()
                        ->active()
                        ->bySystemUserCountries()
                        ->orderBy(['name' => SORT_ASC])
                        ->collection(),
                    ['prompt' => '-']) ?>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="form-group">
            <?= $form->field($model, 'date')->datePicker() ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="form-group">
            <?= $form->field($model, 'name')->textInput() ?>
        </div>
    </div>
</div>
<div class="row margin-top-20">
    <div class="col-lg-12">
        <h4><?= Yii::t('common', 'Шаблон письма поздравления с праздником') ?></h4>
        {*NAME*} - <?= Yii::t('common', 'ФИО') ?> <br>
        {*HOLIDAY*} - <?= Yii::t('common', 'название праздника') ?> <br>
        {*DATE*} - <?= Yii::t('common', 'дата') ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="form-group">
            <?= $form->field($model, 'greeting_card_template_subject')->textInput() ?>
        </div>
        <div class="form-group">
            <?= $form->field($model, 'greeting_card_template_message')->textarea(['rows' => 4]) ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить государственный праздник') : Yii::t('common', 'Сохранить государственный праздник')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
