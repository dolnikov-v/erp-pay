<?php

use app\components\widgets\ActiveForm;
use yii\helpers\Url;
use app\modules\salary\models\Office;
use yii\helpers\Html;
use app\widgets\InputGroupFile;
use app\widgets\InputText;

/** @var app\modules\salary\models\OfficeExpense $model */
/** @var integer $officeId */

$offices = Office::find()->collection();
$currencies = \app\models\Currency::find()->collection();
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute(['edit', 'id' => $model->id]),
    'options' => ['enctype' => 'multipart/form-data']
]); ?>
<div class="row">
    <div class="col-lg-3">
        <?= $form->field($model, 'type')->select2List(Office::getExpensesFieldsList()) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'expense')->textInput() ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'currency_id')->select2List($currencies, ['length' => 1]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-3">
        <?= $form->field($model, 'date_from')->datePicker() ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'date_to')->datePicker() ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-3">
        <div class="control-label"><label><?= $model->getAttributeLabel('filename') ?></label></div>
        <?= InputGroupFile::widget([
            'right' => false,
            'input' => InputText::widget([
                'type' => 'file',
                'name' => Html::getInputName($model, 'file'),
            ]),
        ]) ?>
    </div>
    <div class="col-lg-3">
        <div class="control-label"><label></label></div>
        <?php if ($model->filename): ?>
            <?= Html::a(Yii::t('common', 'Просмотр'), '/media/salary-expense/filename/' . $model->filename, ['target' => '_blank']) ?>
            <?php if (Yii::$app->user->can('salary.officeexpense.deletefile')) : ?>
                <?= Html::a(
                    Yii::t('common', 'Удалить'),
                    Url::toRoute(['/salary/office-expense/delete-file', 'id' => $model->id, 'photo']),
                    [
                        'class' => 'ml15'
                    ]
                );
                ?>
            <?php endif; ?>
        <?php endif; ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12"><?= $form->field($model, 'office_id')->hiddenInput()->label('') ?></div>
</div>

<div class="row">
    <div class="col-lg-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить расход') : Yii::t('common', 'Сохранить расход')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute([
            'office/edit-expenses/',
            'id' => $model->office_id
        ])) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
