<?php
use app\widgets\Panel;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var app\modules\salary\models\OfficeExpense $model */
/** @var integer $officeId */


$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление расхода') : Yii::t('common', 'Редактирование расхода');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Зарплата'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Управление офисами'), 'url' => Url::toRoute('/salary/office/index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];

?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Расход'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
        'officeId' => $officeId,
    ])
]) ?>