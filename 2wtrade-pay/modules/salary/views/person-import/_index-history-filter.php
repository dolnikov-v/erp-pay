<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/** @var app\modules\salary\models\search\PersonImportSearch $searchForm */
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute('index'),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($searchForm, 'created_at')->dateRangePicker('date_from', 'date_to') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->submit(Yii::t('common', 'Применить')); ?>
            <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')); ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>