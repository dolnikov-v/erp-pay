<?php

use app\widgets\Panel;
use app\widgets\Nav;
use app\assets\SiteAsset;

/** @var \yii\web\View $this */
/** @var app\modules\salary\models\PersonImport $model */
/** @var array $history */


$this->title = Yii::t('common', 'Импорт сотрудников');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Зарплата'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

SiteAsset::register($this);
?>

<?= Panel::widget([
    'nav' => new Nav([
        'tabs' => [
            [
                'label' => Yii::t('common', 'История загрузок'),
                'content' => $this->render('_index-history', $history),
            ],
            [
                'label' => Yii::t('common', 'Загрузка файла'),
                'content' => $this->render('_index-load-form', ['model' => $model]),
            ],
        ]
    ])
]) ?>
