<?php
use app\widgets\InputGroupFile;
use app\widgets\InputText;
use yii\helpers\Html;
use app\widgets\ButtonLink;
use app\components\widgets\ActiveForm;
use yii\helpers\Url;
use yii\web\View;

/** @var yii\web\View $this */
/** @var \app\modules\salary\models\PersonImport $model */

$this->registerJs("var I18n = {};", View::POS_HEAD);
$this->registerJs("I18n['Файл обработан'] = '" . Yii::t('common',
        'Файл обработан') . "';", View::POS_HEAD);

?>
<div id="person-import">

    <?php
    $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data',
        ],
        'method' => 'post',
    ]); ?>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'row_begin')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'cell_country')->checkTextInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'cell_login')->checkTextInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'cell_id')->checkTextInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'cell_full_name')->checkTextInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'cell_position')->checkTextInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'cell_head')->checkTextInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'cell_salary')->checkTextInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'cell_salary_usd')->checkTextInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'cell_salary_hourly')->checkTextInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'cell_salary_hourly_usd')->checkTextInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'cell_email')->checkTextInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'cell_skype')->checkTextInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'cell_phone')->checkTextInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'cell_account_number')->checkTextInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'cell_passport')->checkTextInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'cell_employment_date')->checkTextInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'cell_termination_date')->checkTextInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'cell_termination_reason')->checkTextInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?= InputGroupFile::widget([
                'right' => false,
                'input' => InputText::widget([
                    'type' => 'file',
                    'name' => Html::getInputName($model, 'loadFile'),
                    'id' => 'loadFile',
                ]),
            ]) ?>
        </div>
        <div class="col-md-8">
            <?= Html::submitButton(Yii::t('common', 'Отправить'), ['class' => 'btn btn-success']) ?>

            <?php if (!$model->isNewRecord) { ?>

            <?= Html::hiddenInput('changeImport', 1) ?>

            <?= ButtonLink::widget([
                'label' => Yii::t('common', 'Скачать файл импорта'),
                'url' => Url::to(['download-import-file', 'id' => $model->id])
            ]) ?>

            <?php } ?>

        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
