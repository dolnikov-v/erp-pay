<?php
use app\components\grid\GridView;
use app\modules\salary\models\PersonImport;
use app\components\grid\ActionColumn;
use yii\helpers\Html;
use yii\helpers\Url;
use app\widgets\Label;

/* @var \yii\data\ActiveDataProvider $dataProvider */
?>

<div class="table-responsive margin-top-20">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'id',
                'label' => '#',
            ],
            [
                'attribute' => 'original_file_name',
                'content' => function ($model) {
                    return Html::a($model->original_file_name, Url::to(['process', 'id' => $model->id]));
                }
            ],
            [
                'attribute' => 'office_id',
                'content' => function ($model) {
                    return $model->office ? $model->office->name : '';
                }
            ],
            [
                'attribute' => 'status',
                'content' => function ($model) {
                    /* @var \app\modules\salary\models\PersonImport $model */
                    return Label::widget(['label' => $model->getStatusLabel(), 'style' => $model->status ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT]);
                }
            ],
            [
                'attribute' => 'created_at',
                'content' => function ($model) {
                    return Yii::$app->formatter->asDatetime($model->created_at);
                }
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Просмотр'),
                        'url' => function ($model) {
                            /* @var $model PersonImport */
                            return Url::toRoute(['/salary/person-import/process', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('salary.personimport.process');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить'),
                        'url' => function () {
                            return '#';
                        },
                        'style' => 'confirm-delete-link',
                        'attributes' => function ($model) {
                            return [
                                'data-href' => Url::toRoute(['/salary/person-import/delete', 'id' => $model->id])
                            ];
                        },
                        'can' => function ($model) {
                            /* @var $model PersonImport */
                            return Yii::$app->user->can('salary.personimport.delete') && !$model->isImported();
                        }
                    ],
                ],
            ]
        ]
    ]) ?>
</div>


