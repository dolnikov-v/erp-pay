<?php

use app\modules\salary\extensions\GridViewPersonImport;
use app\widgets\ButtonLink;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var yii\data\ArrayDataProvider $dataProvider */
/** @var app\modules\salary\models\PersonImport $model */
/** @var array $countryCollection */
/** @var array $positionCollection */
/** @var array $fileTeamLeadList */
/** @var array $officeTeamLeadList */
/** @var array $workingShiftList */

?>

<?= GridViewPersonImport::widget([
    'dataProvider' => $dataProvider,
    'import' => $model,
    'positionCollection' => $positionCollection,
    'countryCollection' => $countryCollection,
    'tableOptions' => [
        'id' => 'person-import-grid',
//        'class' => 'table table-striped table-hover table-sortable tl-fixed',
    ],
]) ?>

<?php if (!$model->isImported()) { ?>

    <?= $this->render('_process-group-actions', [
        'model' => $model,
        'fileTeamLeadList' => $fileTeamLeadList,
        'officeTeamLeadList' => $officeTeamLeadList,
        'workingShiftList' => $workingShiftList,
    ]) ?>

    <?php if (Yii::$app->user->can('salary.personimport.import')) { ?>

        <?= ButtonLink::widget([
            'url' => Url::to(['import', 'id' => $model->id]),
            'label' => Yii::t('common', 'Импортировать'),
            'style' => ButtonLink::STYLE_PRIMARY,
        ]) ?>

    <?php } ?>


<?php } ?>

