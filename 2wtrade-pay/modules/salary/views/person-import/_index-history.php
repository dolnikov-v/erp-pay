<?php
use app\widgets\LinkPager;
use app\widgets\Panel;
use app\widgets\assets\custom\ModalConfirmDeleteAsset;
use app\widgets\custom\ModalConfirmDelete;

/** @var app\modules\salary\models\search\PersonImportSearch $searchForm */
/** @var yii\data\ArrayDataProvider $dataProvider */

ModalConfirmDeleteAsset::register($this);

?>

<?= $this->render('_index-history-filter', ['searchForm' => $searchForm]) ?>

<?= Panel::widget([
    'withBody' => false,
    'content' => $this->render('_index-history-list', ['dataProvider' => $dataProvider]),
    'footer' => LinkPager::widget([
        'pagination' => $dataProvider->getPagination(),
        'counterPosition' => LinkPager::COUNTER_POSITION_RIGHT
    ]),
]) ?>

<?= ModalConfirmDelete::widget() ?>
