<?php
use yii\helpers\Url;
use app\widgets\Dropdown;
use yii\helpers\Html;
use app\widgets\Button;
use app\widgets\Modal;
use app\widgets\Select2;

/** @var yii\web\View $this */
/** @var \app\modules\salary\models\PersonImport $model */
/** @var array $fileTeamLeadList */
/** @var array $officeTeamLeadList */
/** @var array $workingShiftList */

$links = [];
if (Yii::$app->user->can('salary.personimport.recordgroupinclude')) {
    $links[] = [
        'label' => Yii::t('common', 'Включить в импорт'),
        'url' => Url::to(['record-group-include', 'id' => $model->id]),
    ];

}
if (Yii::$app->user->can('salary.personimport.recordgroupexclude')) {
    $links[] = [
        'label' => Yii::t('common', 'Исключить из импорта'),
        'url' => Url::to(['record-group-exclude', 'id' => $model->id]),
    ];
}

if (Yii::$app->user->can('salary.personimport.recordgroupapprovedhead')) {

    if ($fileTeamLeadList) {

        $links[] = [
            'label' => Yii::t('common', 'Назначить руководителя из файла'),
            'url' => '#group-set-team-lead-from-file-modal',
        ];

        echo Modal::widget([
            'id' => 'group-set-team-lead-from-file-modal',
            'size' => Modal::SIZE_LARGE,
            'title' => Yii::t('common', 'Назначить руководителя из файла'),
            'body' => Select2::widget(['name' => 'head', 'items' => $fileTeamLeadList, 'prompt' => '-']),
            'footer' => Html::tag('div',
                Button::widget([
                    'label' => Yii::t('common', 'Назначить'),
                    'size' => Button::SIZE_SMALL,
                    'style' => Button::STYLE_PRIMARY,
                    'attributes' => [
                        'data-url' => Url::to(['record-group-approved-head', 'id' => $model->id, 'type' => 'file']),
                    ]
                ]) .
                Button::widget([
                    'label' => Yii::t('common', 'Закрыть'),
                    'size' => Button::SIZE_SMALL,
                    'attributes' => [
                        'data-dismiss' => 'modal',
                    ],
                ]),
                ['class' => 'text-right']
            ),
        ]);
    }

    if ($officeTeamLeadList) {

        $links[] = [
            'label' => Yii::t('common', 'Назначить руководителя из офиса'),
            'url' => '#group-set-team-lead-from-office-modal',
        ];

        echo Modal::widget([
            'id' => 'group-set-team-lead-from-office-modal',
            'size' => Modal::SIZE_LARGE,
            'title' => Yii::t('common', 'Назначить руководителя из офиса'),
            'body' => Select2::widget(['name' => 'head', 'items' => $officeTeamLeadList, 'prompt' => '-']),
            'footer' => Html::tag('div',
                Button::widget([
                    'label' => Yii::t('common', 'Назначить'),
                    'size' => Button::SIZE_SMALL,
                    'style' => Button::STYLE_PRIMARY,
                    'attributes' => [
                        'data-url' => Url::to(['record-group-approved-head', 'id' => $model->id, 'type' => 'office']),
                    ]
                ]) .
                Button::widget([
                    'label' => Yii::t('common', 'Закрыть'),
                    'size' => Button::SIZE_SMALL,
                    'attributes' => [
                        'data-dismiss' => 'modal',
                    ],
                ]),
                ['class' => 'text-right']
            ),
        ]);
    }
}

if (Yii::$app->user->can('salary.personimport.recordgroupsetworkinfshift')) {

    $links[] = [
        'label' => Yii::t('common', 'Назначить рабочую смену'),
        'url' => '#group-set-working-shift-modal',
    ];

    echo Modal::widget([
        'id' => 'group-set-working-shift-modal',
        'size' => Modal::SIZE_LARGE,
        'title' => Yii::t('common', 'Назначить рабочую смену'),
        'body' => Select2::widget(['name' => 'workingShift', 'items' => $workingShiftList, 'prompt' => Yii::t('common', '(По умолчанию)')]),
        'footer' => Html::tag('div',
            Button::widget([
                'label' => Yii::t('common', 'Назначить'),
                'size' => Button::SIZE_SMALL,
                'style' => Button::STYLE_PRIMARY,
                'attributes' => [
                    'data-url' => Url::to(['record-group-set-working-shift', 'id' => $model->id]),
                ]
            ]) .
            Button::widget([
                'label' => Yii::t('common', 'Закрыть'),
                'size' => Button::SIZE_SMALL,
                'attributes' => [
                    'data-dismiss' => 'modal',
                ],
            ]),
            ['class' => 'text-right']
        ),
    ]);
}

if ($links) {
    $dropdown = Dropdown::widget([
        'links' => $links,
        'label' => 'Групповые действия',
        'dropup' => true,
    ]);

    echo Html::tag('div', $dropdown, [
        'id' => 'person-import-actions',
        'class' => 'pull-left padding-right-10',
    ]);
}