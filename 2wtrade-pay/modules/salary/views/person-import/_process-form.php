<?php
use yii\helpers\Html;
use app\widgets\Select2;
use app\components\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\modules\salary\models\PersonImport $model */
/** @var array $dataPositionList */
/** @var array $positionCollection */
/** @var array $officeCollection */
/** @var array $dataCountryList */
/** @var array $countryCollection */

$onlyView = $model->isImported();
?>

<?php $form = ActiveForm::begin(['method' => 'post']); ?>

<div class="row">
    <div class="col-md-4">
        <?= $form->field($model, 'office_id')->select2List($officeCollection, [
            'disabled' => $onlyView,
        ]) ?>
    </div>
</div>
<div class="row">

    <div class="col-md-6">
        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th><?= Yii::t('common', 'Должность в файле') ?></th>
                <th class="text-center"><?= Yii::t('common', 'Должность в системе') ?></th>
            </tr>
            </thead>
            <tbody>

            <?php if ($map = $model->getPositionMap()) { ?>
                <?php foreach ($map as $key => $value) { ?>
                    <tr>
                        <td style="vertical-align: middle"><?= Yii::t('common', $key) ?></td>
                        <td class="text-center">
                            <?= Select2::widget([
                                'id' => 'positions_' . $key,
                                'name' => 'positions[' . $key . ']',
                                'items' => $positionCollection,
                                'prompt' => '-',
                                'value' => $value,
                                'disabled' => $onlyView,
                            ]) ?>
                        </td>
                    </tr>
                <?php } ?>
            <?php } else { ?>
                <tr>
                    <td class="text-center" colspan="2"><?= Yii::t('common', 'Должностей в файле не найдено') ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>

    <div class="col-md-6">

        <table class="table table-hover">
            <thead>
            <tr>
                <th><?= Yii::t('common', 'Страна в файле') ?></th>
                <th class="text-center"><?= Yii::t('common', 'Страна в системе') ?></th>
            </tr>
            </thead>
            <tbody>

            <?php if ($map = $model->getCountryMap()) { ?>
                <?php foreach ($map as $key => $value) { ?>
                    <tr>
                        <td style="vertical-align: middle"><?= Yii::t('common', $key) ?></td>
                        <td class="text-center">
                            <?= Select2::widget([
                                'id' => 'countries_' . $key,
                                'name' => 'countries[' . $key . ']',
                                'items' => $countryCollection,
                                'prompt' => '-',
                                'value' => $value,
                                'disabled' => $onlyView,
                            ]) ?>
                        </td>
                    </tr>
                <?php } ?>
            <?php } else { ?>
                <tr>
                    <td class="text-center" colspan="2"><?= Yii::t('common', 'Стран в файле не найдено') ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>

    </div>

</div>
<?php if (!$onlyView) { ?>
<div class="row">
    <div class="col-md-8">
        <?= Html::submitButton(Yii::t('common', 'Сохранить'), ['class' => 'btn btn-primary']) ?>
    </div>
</div>
<?php } ?>
<?php ActiveForm::end(); ?>
