<?php

use app\widgets\Panel;
use app\widgets\Nav;
use app\modules\salary\assets\PersonImportAsset;
use yii\helpers\Url;

/** @var yii\data\ArrayDataProvider $dataProvider */
/** @var app\modules\salary\models\PersonImport $model */
/** @var array $positionCollection */
/** @var array $officeCollection */
/** @var array $countryCollection */
/** @var array $fileTeamLeadList */
/** @var array $officeTeamLeadList */
/** @var array $workingShiftList */

$this->title = Yii::t('common', 'Файл импорта ' . $model->id);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Зарплата'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Импорт сотрудников'), 'url' => Url::to(['index'])];
$this->params['breadcrumbs'][] = ['label' => $this->title];

PersonImportAsset::register($this);
?>

<?= Panel::widget([
    'nav' => new Nav([
        'tabs' => [
            [
                'label' => Yii::t('common', 'Настройки импорта'),
                'content' => $this->render('_process-form', [
                    'model' => $model,
                    'countryCollection' => $countryCollection,
                    'positionCollection' => $positionCollection,
                    'officeCollection' => $officeCollection,
                ])
            ],
            [
                'label' => Yii::t('common', 'Импорт записей'),
                'content' => $this->render('_process-import', [
                    'model' => $model,
                    'dataProvider' => $dataProvider,
                    'countryCollection' => $countryCollection,
                    'positionCollection' => $positionCollection,
                    'officeCollection' => $officeCollection,
                    'officeTeamLeadList' => $officeTeamLeadList,
                    'fileTeamLeadList' => $fileTeamLeadList,
                    'workingShiftList' => $workingShiftList,
                ]),
            ],
            [
                'label' => Yii::t('common', 'Изменение импорта'),
                'content' => $this->render('_index-load-form', [
                    'model' => $model,
                    'dataProvider' => $dataProvider,
                    'countryCollection' => $countryCollection,
                    'positionCollection' => $positionCollection,
                    'officeCollection' => $officeCollection,
                    'officeTeamLeadList' => $officeTeamLeadList,
                    'fileTeamLeadList' => $fileTeamLeadList,
                ]),
            ],
        ]
    ])
]) ?>