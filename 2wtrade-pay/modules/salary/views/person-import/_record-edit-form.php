<?php
use yii\helpers\Html;
use app\components\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var \app\modules\salary\models\PersonImportData $model */
/** @var array $headList */
/** @var array $workingShiftList */

$fields = $model->import->getColumnMapRef();
unset($fields['cell_id']);

?>
<div id="person-import">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">

        <?php if ($model->import->cell_login || $model->import->cell_id) { ?>

            <div class="col-md-3">
                <?= $form->field($model, 'user_id')->textInput() ?>
            </div>

        <?php } ?>

        <?php foreach ($fields as $prop => $field) { ?>

            <?php if ($model->import->$prop) { ?>

                <div class="col-md-3">
                    <?= $form->field($model, $field)->textInput() ?>
                </div>

                <?php if ($prop == 'cell_head') { ?>

                    <div class="col-md-3">
                        <?= $form->field($model, 'head_id')->select2List($headList, ['prompt' => '-']) ?>
                    </div>

                <?php } ?>

            <?php } ?>

        <?php } ?>

        <div class="col-md-3">
            <?= $form->field($model, 'working_shift_id')->select2List($workingShiftList, ['prompt' => Yii::t('common', '(По умолчанию)')]) ?>
        </div>

    </div>
    <div class="row">
        <div class="col-md-8">
            <?= Html::submitButton(Yii::t('common', 'Сохранить'), ['class' => 'btn btn-success']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
