<?php
use app\widgets\Panel;

/** @var \yii\web\View $this */
/** @var app\modules\salary\models\PersonImportData $model */
/** @var array $headList */
/** @var array $workingShiftList */

$this->title = Yii::t('common', 'Редартирование записи {row_number}', ['row_number' => $model->row_number]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Зарплата'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Запись для импорта'),
    'content' => $this->render('_record-edit-form', [
        'model' => $model,
        'headList' => $headList,
        'workingShiftList' => $workingShiftList,
    ])
]) ?>