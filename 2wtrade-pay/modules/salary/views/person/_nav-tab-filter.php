<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;
use app\modules\salary\models\Designation;
use app\modules\salary\models\Person;
use app\modules\salary\models\Office;

/** @var app\modules\salary\models\search\PersonSearch $modelSearch */

$parentPersons = [$modelSearch::NO_TEAMLEAD => Yii::t('common', 'Без активного Тимлида')] + Person::getPatentsCollection();
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute('index'),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>
    <div class="row">
        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'name')->textarea() ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'user_login')->textarea() ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'user_id')->textarea() ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'activeSearch')->select2List($modelSearch->types_active) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'designation_id')->select2List(Designation::find()->collection(), ['prompt' => '-', 'length' => 1]) ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'office_id')->select2List(Office::find()->bySystemUserCountries()->collection(), ['prompt' => '-', 'length' => 1]) ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'parent_id')->select2List($parentPersons, ['prompt' => '-', 'length' => 1]) ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'approvedSearch')->select2List($modelSearch->types_approved) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->submit(Yii::t('common', 'Применить')); ?>
            <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')); ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>