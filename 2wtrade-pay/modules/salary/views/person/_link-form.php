<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;
use app\modules\salary\assets\PersonLinkUserAsset;

/** @var app\modules\salary\models\Person $model */
/** @var app\modules\callcenter\models\search\CallCenterUserSearch $callCenterUserSearch */
/** @var array $callCenters */
/** @var array $callCenterUsers */

PersonLinkUserAsset::register($this);
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['edit', 'id' => $model->id])]); ?>
<div class="row">
    <div class="col-lg-3 user_callcenter_holder">
        <?= $form->field($callCenterUserSearch, 'callcenter_id')->select2List($callCenters, ['length' => false]) ?>
    </div>
    <div class="col-lg-3 user_login_holder">
        <?= $form->field($callCenterUserSearch, 'user_login')->select2ListMultiple($callCenterUsers, ['prompt' => '-']) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= $form->submit(Yii::t('common', 'Сохранить')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')) ?>
        <div style="display: none">
            <?= $form->field($callCenterUserSearch, 'person_id')->hiddenInput(['value' => $model->id]) ?>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
