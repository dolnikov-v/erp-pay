<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use app\modules\salary\models\Office;
use app\widgets\InputGroupFile;
use app\widgets\InputText;
use app\models\Language;

/** @var app\modules\salary\models\Person $model */
/** @var array $salarySchemes */
/** @var array $parentUsers */
/** @var array $personLanguageIds */
/** @var bool $isView */

$offices = Office::find()->bySystemUserCountries()->collection();
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute([$isView ? 'view' : 'edit', 'id' => $model->id]),
    'options' => ['enctype' => 'multipart/form-data']
]); ?>
<div class="row">
    <div class="col-lg-3">
        <?= $form->field($model, 'office_id')->select2List($offices, [
            'prompt' => '-',
            'disabled' => $isView
        ]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'passport_id_number')->textInput([
            'disabled' => $isView
        ]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'corporate_email')->textInput([
            'disabled' => $isView
        ]) ?>
    </div>
    <div class="col-lg-3 no-margin-custom-checkbox">
        <?= $form->field($model, 'active')->checkboxCustom([
            'value' => 1,
            'checked' => $model->active,
            'uncheckedValue' => 0,
            'disabled' => $isView
        ]) ?>
        <?php if (Yii::$app->user->can('salary.person.approve')): ?>
            <?= $form->field($model, 'approved')->checkboxCustom([
                'value' => 1,
                'checked' => $model->approved,
                'uncheckedValue' => 0,
                'disabled' => $isView,
            ]) ?>
        <?php endif; ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-3">
        <?= $form->field($model, 'name')->textInput([
            'disabled' => $isView
        ]) ?>
    </div>
    <div class="col-lg-3 parent_holder">
        <?= $form->field($model, 'parent_id')->select2List($parentUsers, [
            'prompt' => '-',
            'disabled' => $isView,
            'length' => 1
        ]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'phone')->textInput([
            'disabled' => $isView
        ]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'skype')->textInput([
            'disabled' => $isView
        ]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-3">
        <?= $form->field($model, 'gender')->select2List($model::getGenders(), [
            'disabled' => $isView
        ]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'birth_date')->datePicker([
            'disabled' => $isView
        ]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'start_date')->datePicker([
            'disabled' => $isView
        ]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'dismissal_date')->datePicker([
            'disabled' => $isView
        ]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-3">
        <?= $form->field($model, 'insurance_coefficient')->textInput([
            'disabled' => $isView
        ]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'pension_contribution')->textInput([
            'disabled' => $isView
        ]) ?>
        <div class="small"><?=Yii::t('common', 'Eсли указан 0, то будет браться из настроек офиса, если там будет указан')?></div>
        <br>
    </div>
    <div class="col-lg-3">
        <?php if ($model->dismissal_file): ?>
            <div class="control-label"><label><?= $model->getAttributeLabel('dismissal_file') ?></label></div>
            <div class="user-photo">
                <?php if (Yii::$app->user->can('media.salaryperson.dismissal')) : ?>
                    <?= Html::a(Yii::t('common', 'Просмотр'), '/media/salary-person/dismissal-file/' . $model->dismissal_file, ['target' => '_blank']) ?>
                <?php endif; ?>
            </div>
        <?php endif; ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'dismissal_comment')->textInput([
            'disabled' => $isView
        ]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-3">
        <?= $form->field($model, 'designation_id')->select2List(\app\modules\salary\models\Designation::find()
            ->where(['active' => 1])
            ->orderBy(['name' => SORT_ASC])
            ->collection(), [
            'prompt' => Yii::t('common', 'Выберите должность'),
            'disabled' => $isView,
            'length' => 1
        ]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'salary_scheme')->select2List($salarySchemes, [
            'disabled' => $isView
        ]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'salary_usd')->textInput([
            'disabled' => $isView
        ]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'salary_local')->textInput([
            'disabled' => $isView
        ]) ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-3">
        <?= $form->field($model, 'working_shift_id')->select2List(\app\modules\salary\models\WorkingShift::find()
            ->byOfficeId($model->office_id)
            ->collection(), [
            'prompt' => Yii::t('common', 'Выберите рабочую смену'),
            'disabled' => $isView
        ]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'account_number')->textInput([
            'disabled' => $isView
        ]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'salary_hour_usd')->textInput([
            'disabled' => $isView
        ]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'salary_hour_local')->textInput([
            'disabled' => $isView
        ]) ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-3">
        <?= $form->field($model, 'bank_name')->textInput([
            'disabled' => $isView
        ]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'bank_address')->textInput([
            'disabled' => $isView
        ]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'bank_swift_code')->textInput([
            'disabled' => $isView
        ]) ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-6">
        <?= $form->field($model, 'address')->textInput([
            'disabled' => $isView
        ]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'languages')->select2ListMultiple(Language::find()
            ->collection(), [
            'value' => $personLanguageIds,
            'disabled' => $isView
        ]); ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'experience')->textInput([
            'disabled' => $isView
        ]) ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-6">
        <div class="control-label"><label><?= $model->getAttributeLabel('photo') ?></label></div>
        <?php if (Yii::$app->user->can('salary.person.edit.photo') && !$isView) : ?>
            <?= InputGroupFile::widget([
                'right' => false,
                'input' => InputText::widget([
                    'type' => 'file',
                    'name' => Html::getInputName($model, 'imageFile'),
                ]),
            ]) ?>
        <?php endif; ?>
        <br>
        <?php if ($model->photo): ?>
            <div class="user-photo">
                <?php if (Yii::$app->user->can('media.salaryperson.photo')) : ?>
                    <?= Html::a(Yii::t('common', 'Просмотр'), '/media/salary-person/photo/' . $model->photo, ['target' => '_blank']) ?>
                <?php endif; ?>
                <?php if (Yii::$app->user->can('salary.person.deletephoto')) : ?>
                    <?= Html::a(
                        Yii::t('common', 'Удалить'),
                        Url::toRoute(['/salary/person/delete-photo', 'id' => $model->id, 'photo'])
                    );
                    ?>
                <?php endif; ?>
            </div>
        <?php endif; ?>
    </div>
    <div class="col-lg-6">
        <div class="control-label"><label><?= $model->getAttributeLabel('photo_agreement') ?></label></div>
        <?php if (Yii::$app->user->can('salary.person.edit.photoagreement') && !$isView) : ?>
            <?= InputGroupFile::widget([
                'right' => false,
                'input' => InputText::widget([
                    'type' => 'file',
                    'name' => Html::getInputName($model, 'imageFileAgreement'),
                ]),
            ]) ?>
        <?php endif; ?>
        <br>
        <?php if ($model->photo_agreement): ?>
            <div class="user-photo">
                <?php if (Yii::$app->user->can('media.salaryperson.photoagreement')) : ?>
                    <?= Html::a(Yii::t('common', 'Просмотр'), '/media/salary-person/photo-agreement/' . $model->photo_agreement, ['target' => '_blank']) ?>
                <?php endif; ?>
                <?php if (Yii::$app->user->can('salary.person.deletephotoagreement')) : ?>
                    <?= Html::a(
                        Yii::t('common', 'Удалить'),
                        Url::toRoute(['/salary/person/delete-photo-agreement', 'id' => $model->id])
                    );
                    ?>
                <?php endif; ?>
            </div>
        <?php endif; ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?php if (!$isView): ?>
            <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить сотрудника') : Yii::t('common', 'Сохранить сотрудника')) ?>
        <?php endif; ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
