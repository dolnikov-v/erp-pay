<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/** @var app\modules\salary\models\Person $model */
/** @var app\modules\salary\models\PersonLink $personLink */
/** @var array $linkTypes */
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['edit', 'id' => $model->id])]); ?>
<div class="row">
    <div class="col-lg-3">
        <?= $form->field($personLink, 'type')->select2List($linkTypes) ?>
    </div>
    <div class="col-lg-3 user_login_holder">
        <?= $form->field($personLink, 'foreign_id')->textInput() ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= $form->submit(Yii::t('common', 'Сохранить')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')) ?>
        <div style="display: none">
            <?= $form->field($personLink, 'person_id')->hiddenInput(['value' => $model->id]) ?>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
