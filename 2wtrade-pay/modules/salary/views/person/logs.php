<?php
use app\widgets\Panel;
use yii\helpers\Url;
use app\modules\salary\widgets\PanelGroupPersonLogs;

/** @var \app\modules\salary\models\PersonLog[] $logs */
/** @var \app\modules\salary\models\Person $model */
/** @var \app\models\logs\TableLog[] $childLog */

$this->title = Yii::t('common', 'История изменения сотрудника {name}', ['name' => $model->name]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Зарплата'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Сотрудники'), 'url' => Url::toRoute('index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<div class="panel panel panel-bordered ">
    <ul class="panel nav nav-tabs" role="tablist">
        <li class="active"><a href="#tabChange" aria-controls="tabChange" role="tab" data-toggle="tab">История изменения</a></li>
        <li><a href="#tabChild" aria-controls="tabChild" role="tab" data-toggle="tab">Сотрудники КЦ</a></li>
    </ul>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="tabChange">
            <?= Panel::widget([
                'title' => Yii::t('common', 'История изменения'),
                'border' => false,
                'content' => PanelGroupPersonLogs::widget([
                    'logs' => $logs,
                ]),
            ]) ?>
        </div>
        <div role="tabpanel" class="tab-pane" id="tabChild">
            <?= \app\widgets\Log::widget([
                'data' => $childLog,
                'showLinks' => false,
                'showData' => true,
                'titleContent' => 'Пользователь № {id}',
            ]); ?>
        </div>
    </div>
</div>
