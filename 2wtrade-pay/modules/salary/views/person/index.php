<?php
use app\components\grid\ActionColumn;
use kartik\grid\GridView;
use app\helpers\DataProvider;
use app\helpers\FontAwesome;
use app\widgets\assets\custom\ModalConfirmDeleteAsset;
use app\widgets\ButtonLink;
use app\widgets\custom\ModalConfirmDelete;
use app\widgets\Label;
use app\widgets\Panel;
use yii\helpers\Url;
use app\widgets\LinkPager;
use yii\web\View;
use app\widgets\Nav;

use app\modules\salary\models\Person;
use app\modules\salary\models\Holiday;
use app\modules\salary\assets\PersonIndexAsset;
use yii\helpers\Html;
use app\components\grid\CustomCheckboxColumn;
use app\widgets\custom\Checkbox;

use app\modules\salary\widgets\ChangeTeamLeader as ChangeTeamLeaderWidget;
use app\modules\salary\widgets\DeactivatePersons as DeactivatePersonsWidget;  // todo вообще это оставим?
use app\modules\salary\widgets\ActivatePersons as ActivatePersonsWidget;
use app\modules\salary\widgets\NotapprovePersons as NotapprovePersonsWidget;
use app\modules\salary\widgets\ApprovePersons as ApprovePersonsWidget;
use app\modules\salary\widgets\SetRole as SetRoleWidget;
use app\modules\salary\widgets\SetWork as SetWorkWidget;
use app\modules\salary\widgets\SetBonus as SetBonusWidget;
use app\modules\salary\widgets\SetPenalty as SetPenaltyWidget;
use app\modules\salary\widgets\ModalConfirmDeactivate;
use app\modules\salary\models\BonusType;
use app\modules\salary\models\PenaltyType;

use app\modules\salary\assets\ChangeTeamLeaderAsset;
use app\modules\salary\assets\DeactivatePersonsAsset;
use app\modules\salary\assets\ActivatePersonsAsset;
use app\modules\salary\assets\NotapprovePersonsAsset;
use app\modules\salary\assets\ApprovePersonsAsset;
use app\modules\salary\assets\SetRoleAsset;
use app\modules\salary\assets\SetWorkAsset;
use app\modules\salary\assets\SetBonusAsset;
use app\modules\salary\assets\SetPenaltyAsset;
use app\modules\salary\assets\ModalConfirmDeactivateAsset;

/** @var yii\web\View $this */
/** @var app\modules\salary\models\search\PersonSearch $modelSearch */
/** @var yii\data\ActiveDataProvider $dataProvider */

ModalConfirmDeleteAsset::register($this);

$this->title = Yii::t('common', 'Сотрудники');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Зарплата'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

PersonIndexAsset::register($this);

ChangeTeamLeaderAsset::register($this);
DeactivatePersonsAsset::register($this);
ActivatePersonsAsset::register($this);
NotapprovePersonsAsset::register($this);
ApprovePersonsAsset::register($this);
SetRoleAsset::register($this);
SetWorkAsset::register($this);
SetBonusAsset::register($this);
SetPenaltyAsset::register($this);
ModalConfirmDeactivateAsset::register($this);

$this->registerJs("var I18n = {};", View::POS_HEAD);
$this->registerJs("I18n['Необходимо выбрать сотрудников.'] = '" . Yii::t('common', 'Необходимо выбрать сотрудников.') . "';", View::POS_HEAD);
$this->registerJs("I18n['Ничего не сделали.'] = '" . Yii::t('common', 'Ничего не сделали.') . "';", View::POS_HEAD);
$this->registerJs("I18n['Нет прав для выполнения данной операции'] = '" . Yii::t('common', 'Нет прав для выполнения данной операции') . "';", View::POS_HEAD);
$this->registerJs("I18n['Не удалось сменить руководителя'] = '" . Yii::t('common', 'Не удалось сменить руководителя') . "';", View::POS_HEAD);
$this->registerJs("I18n['Операция выполнена успешно'] = '" . Yii::t('common', 'Операция выполнена успешно') . "';", View::POS_HEAD);
$this->registerJs("I18n['Не удалось деактивировать сотрудников'] = '" . Yii::t('common', 'Не удалось деактивировать сотрудников') . "';", View::POS_HEAD);
$this->registerJs("I18n['Не удалось активировать сотрудников'] = '" . Yii::t('common', 'Не удалось активировать сотрудников') . "';", View::POS_HEAD);
$this->registerJs("I18n['Не удалось убрать подтверждение у сотрудников'] = '" . Yii::t('common', 'Не удалось убрать подтверждение у сотрудников') . "';", View::POS_HEAD);
$this->registerJs("I18n['Не удалось подтвердить сотрудников'] = '" . Yii::t('common', 'Не удалось подтвердить сотрудников') . "';", View::POS_HEAD);
$this->registerJs("I18n['Не удалось сменить руководителя, т.к. у сотрудников разные КЦ'] = '" . Yii::t('common', 'Не удалось сменить руководителя, т.к. у сотрудников разные КЦ') . "';", View::POS_HEAD);
$this->registerJs("I18n['Не удалось установить должность'] = '" . Yii::t('common', 'Не удалось установить должность') . "';", View::POS_HEAD);
$this->registerJs("I18n['Не удалось установить рабочую смену'] = '" . Yii::t('common', 'Не удалось установить рабочую смену') . "';", View::POS_HEAD);
$this->registerJs("I18n['Не удалось назначить бонус'] = '" . Yii::t('common', 'Не удалось назначить бонус') . "';", View::POS_HEAD);
$this->registerJs("I18n['Не удалось назначить штраф'] = '" . Yii::t('common', 'Не удалось назначить штраф') . "';", View::POS_HEAD);
$this->registerJs("I18n['Да'] = '" . Yii::t('common', 'Да') . "';", View::POS_HEAD);
$this->registerJs("I18n['Нет'] = '" . Yii::t('common', 'Нет') . "';", View::POS_HEAD);
$this->registerJs("I18n['Введите дату увольнения'] = '" . Yii::t('common', 'Введите дату увольнения') . "';", View::POS_HEAD);
$this->registerJs("I18n['Введите причину увольнения'] = '" . Yii::t('common', 'Введите причину увольнения') . "';", View::POS_HEAD);

if (Yii::$app->user->can('salary.person.setbonus')) {

    $bonusTypes = BonusType::find()->byOfficeId($modelSearch->office_id)->active()->all();

    $this->registerJs("var bonusTypes = {};", View::POS_HEAD);
    foreach ($bonusTypes as $type) {
        $this->registerJs("bonusTypes[" . $type->id . "] = {type: '" . $type->type . "', sum: '" . $type->sum . "', sum_local: '" . $type->sum_local . "', percent: '" . $type->percent . "'};", View::POS_HEAD);
    }
}

if (Yii::$app->user->can('salary.person.setpenalty')) {

    $penaltyTypes = PenaltyType::find()->active()->all();

    $this->registerJs("var penaltyTypes = {};", View::POS_HEAD);
    foreach ($penaltyTypes as $type) {
        $this->registerJs("penaltyTypes[" . $type->id . "] = {type: '" . $type->type . "', sum: '" . $type->sum . "', percent: '" . $type->percent . "'};", View::POS_HEAD);
    }
}


$callCenterUserRoles = \app\modules\callcenter\models\CallCenterUser::getRoles();
?>


<?= Panel::widget([
    'nav' => new Nav([
        'tabs' => [
            [
                'label' => Yii::t('common', 'Фильтр поиска'),
                'content' => $this->render('_nav-tab-filter', [
                    'modelSearch' => $modelSearch
                ]),
            ],
            [
                'label' => Yii::t('common', 'Групповые операции'),
                'content' => $this->render('_nav-tab-operations'),
            ],
        ]
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с сотрудниками'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' =>
        '<div class="add-wrap">' .
        (Yii::$app->user->can('salary.person.edit') ?
            ButtonLink::widget([
                'label' => Yii::t('common', 'Добавить сотрудника'),
                'url' => Url::toRoute('/salary/person/edit'),
                'style' => ButtonLink::STYLE_SUCCESS,
                'size' => ButtonLink::SIZE_SMALL,
            ]) : '') .
        (Yii::$app->user->can('salary.person.export') ?
            ButtonLink::widget([
                "id" => "btn_export_person",
                "url" => Url::toRoute(['person/export'] + Yii::$app->request->queryParams),
                "style" => ButtonLink::STYLE_INFO . " width-200 pull-right ml15",
                "size" => ButtonLink::SIZE_SMALL,
                "label" => Yii::t("common", "Экспорт сотрудников"),
            ]) : '') .
        (Yii::$app->user->can('salary.personimport.index') ?
            ButtonLink::widget([
                "id" => "btn_export_person",
                "url" => Url::toRoute(['person-import/index'] + Yii::$app->request->queryParams),
                "style" => ButtonLink::STYLE_INFO . " width-200 pull-right ml15",
                "size" => ButtonLink::SIZE_SMALL,
                "label" => Yii::t("common", "Импорт сотрудников"),
            ]) : '') .
        '</div>' .
        GridView::widget([
            'dataProvider' => $dataProvider,
            'layout' => "{items}",
            'tableOptions' => [
                'id' => 'persons_content',
            ],
            'beforeHeader' => [
                [
                    'columns' => [
                        ['content' => '', 'options' => ['rowspan' => 2]],
                        ['content' => Yii::t('common', 'Офис'), 'options' => ['rowspan' => 2]],
                        ['content' => Yii::t('common', 'Направление'), 'options' => ['rowspan' => 2]],
                        ['content' => Yii::t('common', 'Должность'), 'options' => ['rowspan' => 2]],
                        ['content' => Yii::t('common', 'ФИО'), 'options' => ['rowspan' => 2]],
                        ['content' => Yii::t('common', 'Руководитель'), 'options' => ['rowspan' => 2]],
                        [
                            'content' => Yii::t('common', 'Логин в системе / роль'),
                            'options' => ['colspan' => 2, 'class' => 'text-center']
                        ],
                        ['content' => Yii::t('common', 'Статус сотрудника'), 'options' => ['rowspan' => 2]],
                        ['content' => Yii::t('common', 'График работы'), 'options' => ['rowspan' => 2]],
                        ['content' => Yii::t('common', 'Дата приема на работу'), 'options' => ['rowspan' => 2]],
                        [
                            'content' => Yii::t('common', 'Контакты'),
                            'options' => ['colspan' => 2, 'class' => 'text-center']
                        ],
                        ['content' => Yii::t('common', 'Учет времени в Crocotime'), 'options' => ['rowspan' => 2]],
                        [
                            'content' => Yii::t('common', 'Оклад'),
                            'options' => ['colspan' => 2, 'class' => 'text-center']
                        ],
                        ['content' => Yii::t('common', 'Опыт работы'), 'options' => ['rowspan' => 2]],
                        ['content' => '', 'options' => ['rowspan' => 2]],
                    ],
                    'options' => ['class' => 'skip-export']
                ]
            ],
            'rowOptions' => function ($model) {
                $class = 'tr-vertical-align-middle' . ($model->active ? '' : ' text-muted');
                if ($model->active && $model->activityWarning !== false) {
                    $class .= ' custom-error';
                }
                return [
                    'class' => $class,
                ];
            },
            'columns' => [
                [
                    'class' => CustomCheckboxColumn::className(),
                    'headerOptions' => [
                        'style' => 'display: none;',
                    ],
                    'content' => function ($model) {
                        return Checkbox::widget([
                            'name' => 'id[]',
                            'value' => $model->id,
                            'style' => 'checkbox-inline',
                            'label' => true,
                        ]);
                    },
                ],
                [
                    'attribute' => 'office_id',
                    'headerOptions' => [
                        'style' => 'display: none;',
                    ],
                    'content' => function ($model) {
                        return $model->office_id ? Yii::t('common', $model->office->name) : '-';
                    },
                ],
                [
                    'attribute' => 'CallCenter',
                    'headerOptions' => [
                        'style' => 'display: none;',
                    ],
                    'label' => Yii::t('common', 'Направление'),
                    'content' => function ($model) {
                        if ($model->callCenterUsers) {
                            $r = [];
                            foreach ($model->callCenterUsers as $callCenterUser) {
                                if (isset($callCenterUser->callCenter->country->name)) {
                                    $r[$callCenterUser->callCenter->country->name] = Yii::t('common', $callCenterUser->callCenter->country->name);
                                }
                            }
                            return implode(", ", $r);
                        }
                        return '';
                    },
                ],
                [
                    'attribute' => 'designation_id',
                    'headerOptions' => [
                        'style' => 'display: none;',
                    ],
                    'contentOptions' => ['class' => 'designation'],
                    'content' => function ($model) {
                        if ($model->designation_id && isset($model->designation->name)) {
                            $text = Yii::t('common', $model->designation->name);
                            return (Yii::$app->user->can('salary.designation.edit')) ? Html::a($text, Url::toRoute([
                                'designation/edit/',
                                'id' => $model->designation_id
                            ]), ['target' => '_blank',]) : $text;
                        }
                        return '-';
                    },
                ],
                [
                    'attribute' => 'name',
                    'headerOptions' => [
                        'style' => 'display: none;',
                    ],
                    'content' => function ($model) {
                        return (Yii::$app->user->can('salary.person.view')) ? Html::a($model->name, Url::toRoute([
                            'person/view/',
                            'id' => $model->id
                        ]), ['target' => '_blank',]) : $model->name;
                    },
                ],
                [
                    'attribute' => 'parent_id',
                    'headerOptions' => [
                        'style' => 'display: none;',
                    ],
                    'contentOptions' => ['class' => 'parent'],
                    'content' => function ($model) {
                        return $model->parent_id ? $model->parent->name : '-';
                    },
                ],
                [
                    'label' => Yii::t('common', 'ERP'),
                    'content' => function ($model) {
                        if ($model->userERP) {
                            $role = $model->userERP->rolesNames ?? '';
                            $text = $model->userERP->username ?? '';

                            $return = (Yii::$app->user->can('access.userview')) ?
                                Html::a($text, Url::toRoute(['/access/user/view/', 'id' => $model->userERP->id]), [
                                    'target' => '_blank',
                                    'title' => $role
                                ]) :
                                Html::tag('span', $text, [
                                    'title' => $role
                                ]);

                            $label = '';
                            if (isset($model->userERP->lastact_at)) {
                                $daysAFK = (time() - $model->userERP->lastact_at) / (60 * 60 * 24);
                                if ($daysAFK > Person::DATE_WARNING_LIMIT) {
                                    // нет активности более 6х дней
                                    $label = Yii::t('common', 'не активен {days} дней', ['days' => floor($daysAFK)]);
                                }
                            } else {
                                $label = Yii::t('common', 'не работал');
                            }

                            if ($label) {
                                $return .= '<br>' . Label::widget([
                                        'label' => $label,
                                        'style' => Label::STYLE_WARNING,
                                    ]);
                            }
                            return $return;

                        }
                        return '';
                    }
                ],
                [
                    'attribute' => 'CallCenterUsers',
                    'label' => Yii::t('common', 'CRM'),
                    'content' => function ($model) use ($callCenterUserRoles) {

                        if ($model->callCenterUsers) {
                            $r = [];
                            foreach ($model->callCenterUsers as $callCenterUser) {
                                $role = $callCenterUserRoles[$callCenterUser->user_role] ?? '';
                                $text = $callCenterUser->user_login;

                                $noActivity = '';
                                if ($model->active) {
                                    $label = '';
                                    if ($callCenterUser->pingInfo) {
                                        $date = new DateTime($callCenterUser->pingInfo);
                                        $now = new DateTime();
                                        $days = $date->diff($now)->format("%d");
                                        if ($days > Person::DATE_WARNING_LIMIT) {
                                            // нет активности более 6х дней
                                            $label = Yii::t('common', 'не активен {days} дней', ['days' => $days]);
                                        }
                                    } else {
                                        $label = Yii::t('common', 'не работал');
                                    }

                                    if ($label != '') {
                                        $noActivity = '<br>' . Label::widget([
                                                'label' => $label,
                                                'style' => Label::STYLE_WARNING,
                                            ]);
                                    }
                                }

                                $r[$text] =
                                    (Yii::$app->user->can('callcenter.user.edit')) ?
                                        Html::a($text, Url::to([
                                            '/call-center/user/edit',
                                            'force_country_slug' => $callCenterUser->callCenter->country->slug ?? '',
                                            'id' => $callCenterUser->id
                                        ]), [
                                            'target' => '_blank',
                                            'title' => $role
                                        ]) :
                                        Html::tag('span', $text, [
                                            'title' => $role
                                        ]);

                                $r[$text] .= $noActivity;
                            }
                            return implode("<br>", $r);
                        }
                        return '';
                    },
                ],
                [
                    'attribute' => 'active',
                    'label' => Yii::t('common', 'Статус сотрудника'),
                    'headerOptions' => [
                        'style' => 'display: none;',
                    ],
                    'contentOptions' => ['class' => 'active-status'],
                    'content' => function ($model) {
                        if ($model->active) {
                            if ($model->approved) {
                                return Yii::t('common', 'Активен');
                            } else {
                                return Yii::t('common', 'Не подтвержден');
                            }
                        } else {
                            return Yii::t('common', 'Уволен');
                        }
                    },
                    'enableSorting' => false,
                ],
                [
                    'attribute' => 'working_shift_id',
                    'label' => Yii::t('common', 'График работы'),
                    'headerOptions' => [
                        'style' => 'display: none;',
                    ],
                    'contentOptions' => ['class' => 'working_shift'],
                    'content' => function ($model) {
                        /** @var $model Person */
                        if ($model->working_shift_id && $model->workingShift) {

                            $today = '';
                            if ($model->active) {

                                $prop = 'working_' . strtolower(date('D'));
                                $today = $model->workingShift->$prop ?? '';

                                $holiday = false;
                                if (isset($model->office->country_id)) {
                                    $holiday = Holiday::find()
                                        ->byDate(date('Y-m-d'))
                                        ->byCountryId($model->office->country_id)
                                        ->exists();
                                }

                                if ($today != '' && !$holiday) {
                                    $today = ' ' . Label::widget([
                                            'label' => $today,
                                            'style' => $model->isWorkingOnTime($today) ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                                            'title' => Yii::t('common', 'Сегодня')
                                        ]);
                                }
                            }

                            $link = (Yii::$app->user->can('salary.workingshift.edit')) ?
                                Html::a($model->workingShift->name, Url::toRoute([
                                    '/salary/working-shift/edit/' . $model->workingShift->id,
                                ]), [
                                    'target' => '_blank',
                                    'title' => Yii::t('common', 'Подробнее')
                                ]) : Yii::t('common', $model->workingShift->name);

                            return $link . $today;
                        }
                        return '';
                    },
                    'enableSorting' => false,
                ],
                [
                    'attribute' => 'start_date',
                    'headerOptions' => [
                        'style' => 'display: none;',
                    ],
                    'content' => function ($model) {
                        return Yii::$app->formatter->asDate($model->start_date);
                    },
                    'enableSorting' => false,
                ],
                [
                    'attribute' => 'skype',
                    'enableSorting' => false,
                    'label' => Html::tag('span', '', [
                        'class' => FontAwesome::SKYPE
                    ]),
                    'encodeLabel' => false,
                    'headerOptions' => ['class' => 'text-center h2', 'style' => 'color: #12A5F4'],
                    'contentOptions' => ['class' => 'text-center'],
                    'content' => function ($model) {
                        return Label::widget([
                            'label' => $model->skype ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                            'style' => $model->skype ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                        ]);
                    },
                ],
                [
                    'attribute' => 'corporate_email',
                    'enableSorting' => false,
                    'label' => Html::tag('span', '', [
                        'class' => FontAwesome::AT
                    ]),
                    'encodeLabel' => false,
                    'contentOptions' => ['class' => 'text-center'],
                    'headerOptions' => ['class' => 'text-center h2'],
                    'content' => function ($model) {
                        return Label::widget([
                            'label' => $model->corporate_email ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                            'style' => $model->corporate_email ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                        ]);
                    },
                ],
                [
                    'headerOptions' => [
                        'style' => 'display: none;',
                    ],
                    'contentOptions' => ['class' => 'text-center'],
                    'content' => function ($model) {
                        return Label::widget([
                            'label' => $model->personLinkCrocoTime ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                            'style' => $model->personLinkCrocoTime ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                        ]);
                    },
                ],
                [
                    'attribute' => 'salary_usd',
                    'label' => 'USD',
                    'contentOptions' => ['class' => 'text-right'],
                ],
                [
                    'attribute' => 'salary_local',
                    'label' => Yii::t('common', 'Местная валюта'),
                    'contentOptions' => ['class' => 'text-right'],
                ],
                [
                    'attribute' => 'experience',
                    'headerOptions' => [
                        'style' => 'display: none;',
                    ],
                ],
                [
                    'class' => ActionColumn::className(),
                    'visible' =>
                        Yii::$app->user->can('salary.person.view') ||
                        Yii::$app->user->can('salary.person.edit') ||
                        Yii::$app->user->can('salary.person.activate') ||
                        Yii::$app->user->can('salary.person.deactivate') ||
                        Yii::$app->user->can('salary.person.delete') ||
                        Yii::$app->user->can('salary.person.approve') ||
                        Yii::$app->user->can('salary.person.notapprove') ||
                        Yii::$app->user->can('salary.person.logs')
                    ,
                    'headerOptions' => [
                        'style' => 'display: none;',
                    ],
                    'items' => [
                        [
                            'label' => Yii::t('common', 'Просмотр'),
                            'url' => function ($model) {
                                return Url::toRoute(['view', 'id' => $model->id]);
                            },
                            'can' => function () {
                                return Yii::$app->user->can('salary.person.view');
                            }
                        ],
                        [
                            'label' => Yii::t('common', 'Открыть карточку'),
                            'url' => function ($model) {
                                return Url::toRoute(['details', 'id' => $model->id]);
                            },
                            'can' => function () {
                                return Yii::$app->user->can('salary.person.details');
                            }
                        ],
                        [
                            'label' => Yii::t('common', 'Редактировать'),
                            'url' => function ($model) {
                                return Url::toRoute(['edit', 'id' => $model->id]);
                            },
                            'can' => function () {
                                return Yii::$app->user->can('salary.person.edit');
                            }
                        ],
                        [
                            'label' => Yii::t('common', 'Подтвердить'),
                            'url' => function ($model) {
                                return Url::toRoute(['approve', 'id' => $model->id]);
                            },
                            'can' => function ($model) {
                                return Yii::$app->user->can('salary.person.approve') && !$model->approved;
                            }
                        ],
                        [
                            'label' => Yii::t('common', 'Убрать подтверждение'),
                            'url' => function ($model) {
                                return Url::toRoute(['notapprove', 'id' => $model->id]);
                            },
                            'can' => function ($model) {
                                return Yii::$app->user->can('salary.person.notapprove') && $model->approved;
                            }
                        ],
                        [
                            'label' => Yii::t('common', 'Активировать'),
                            'url' => function ($model) {
                                return Url::toRoute(['activate', 'id' => $model->id]);
                            },
                            'can' => function ($model) {
                                return Yii::$app->user->can('salary.person.activate') && !$model->active;
                            }
                        ],
                        [
                            'label' => Yii::t('common', 'Уволить'),
                            'url' => function () {
                                return '#';
                            },
                            'style' => 'confirm-deactivate-link',
                            'attributes' => function ($model) {
                                return [
                                    'data-href' => Url::toRoute(['deactivate', 'id' => $model->id]),
                                ];
                            },
                            'can' => function ($model) {
                                return Yii::$app->user->can('salary.person.deactivate') && $model->active;
                            }
                        ],
                        [
                            'label' => Yii::t('common', 'Логи'),
                            'url' => function ($model) {
                                return Url::toRoute(['logs', 'id' => $model->id]);
                            },
                            'can' => function () {
                                return Yii::$app->user->can('salary.person.logs');
                            }
                        ],
                        [
                            'label' => Yii::t('common', 'Удалить'),
                            'url' => function () {
                                return '#';
                            },
                            'style' => 'confirm-delete-link',
                            'attributes' => function ($model) {
                                return [
                                    'data-href' => Url::toRoute(['delete', 'id' => $model->id]),
                                ];
                            },
                            'can' => function () {
                                return Yii::$app->user->can('salary.person.delete');
                            }
                        ],
                    ]
                ]
            ],
        ]),
    'footer' => (Yii::$app->user->can('salary.person.edit') ?
            ButtonLink::widget([
                'label' => Yii::t('common', 'Добавить сотрудника'),
                'url' => Url::toRoute('/salary/person/edit'),
                'style' => ButtonLink::STYLE_SUCCESS,
                'size' => ButtonLink::SIZE_SMALL,
            ]) : '') . LinkPager::widget([
            'pagination' => $dataProvider->getPagination(),
            'counterPosition' => LinkPager::COUNTER_POSITION_RIGHT
        ]),
]) ?>

<?php if (Yii::$app->user->can('salary.person.changeteamleader')): ?>
    <?= ChangeTeamLeaderWidget::widget([
        'url' => Url::toRoute('change-team-leader'),
        'officeId' => $modelSearch->office_id
    ]); ?>
<?php endif; ?>
<?php if (Yii::$app->user->can('salary.person.deactivatepersons')): ?>
    <?= DeactivatePersonsWidget::widget([
        'url' => Url::toRoute('deactivate-persons'),
    ]); ?>
<?php endif; ?>
<?php if (Yii::$app->user->can('salary.person.activatepersons')): ?>
    <?= ActivatePersonsWidget::widget([
        'url' => Url::toRoute('activate-persons'),
    ]); ?>
<?php endif; ?>
<?php if (Yii::$app->user->can('salary.person.notapprovepersons')): ?>
    <?= NotapprovePersonsWidget::widget([
        'url' => Url::toRoute('notapprove-persons'),
    ]); ?>
<?php endif; ?>
<?php if (Yii::$app->user->can('salary.person.approvepersons')): ?>
    <?= ApprovePersonsWidget::widget([
        'url' => Url::toRoute('approve-persons'),
    ]); ?>
<?php endif; ?>
<?php if (Yii::$app->user->can('salary.person.setrole')): ?>
    <?= SetRoleWidget::widget([
        'url' => Url::toRoute('set-role'),
    ]); ?>
<?php endif; ?>
<?php if (Yii::$app->user->can('salary.person.setwork')): ?>
    <?= SetWorkWidget::widget([
        'url' => Url::toRoute('set-work'),
        'officeId' => $modelSearch->office_id
    ]); ?>
<?php endif; ?>
<?php if (Yii::$app->user->can('salary.person.setbonus')): ?>
    <?= SetBonusWidget::widget([
        'url' => Url::toRoute('set-bonus'),
        'officeId' => $modelSearch->office_id
    ]); ?>
<?php endif; ?>
<?php if (Yii::$app->user->can('salary.person.setpenalty')): ?>
    <?= SetPenaltyWidget::widget([
        'url' => Url::toRoute('set-penalty'),
        'officeId' => $modelSearch->office_id
    ]); ?>
<?php endif; ?>
<?= ModalConfirmDelete::widget() ?>
<?php if (Yii::$app->user->can('salary.person.deactivate')): ?>
    <?= ModalConfirmDeactivate::widget() ?>
<?php endif; ?>
