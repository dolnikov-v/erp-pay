<?php
use app\widgets\Panel;
use yii\helpers\Url;
use app\modules\salary\assets\PersonAsset;
use app\helpers\DataProvider;
use app\components\grid\GridView;
use app\widgets\Label;
use app\widgets\LinkPager;
use app\components\grid\DateColumn;
use app\modules\salary\models\PersonLink;

/** @var yii\web\View $this */
/** @var app\modules\salary\models\Person $model */
/** @var array $salarySchemes */
/** @var array $parentUsers */
/** @var yii\data\ActiveDataProvider $userDataProvider */
/** @var yii\data\ActiveDataProvider $linkDataProvider */
/** @var array $personLanguageIds */

$this->title = Yii::t('common', 'Просмотр сотрудника {name}', ['name' => $model->name]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Зарплата'), 'url' => '#'];
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('common', 'Управление сотрудниками'),
    'url' => Url::toRoute('index')
];
$this->params['breadcrumbs'][] = ['label' => $this->title];

PersonAsset::register($this);

$personLinkTypes = PersonLink::getTypeCollection();
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Сотрудник'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
        'salarySchemes' => $salarySchemes,
        'parentUsers' => $parentUsers,
        'personLanguageIds' => $personLanguageIds,
        'isView' => true,
    ])
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Привязанные пользователи колл-центра'),
    'actions' => DataProvider::renderSummary($userDataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $userDataProvider,
        'columns' => [
            [
                'attribute' => 'callcenter_id',
                'label' => Yii::t('common', 'Направление'),
                'content' => function ($user) {
                    return $user->callcenter_id ? Yii::t('common', $user->callCenter->country->name) : '-';
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'user_id',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'user_login',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'active',
                'headerOptions' => ['class' => 'text-center width-150'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($user) {
                    return Label::widget([
                        'label' => $user->active ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $user->active ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'created_at',
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'updated_at',
            ]
        ],
    ]),
    'footer' => LinkPager::widget(['pagination' => $userDataProvider->getPagination()]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Аккаунты внешних систем'),
    'actions' => DataProvider::renderSummary($linkDataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $linkDataProvider,
        'columns' => [
            [
                'attribute' => 'type',
                'enableSorting' => false,
                'content' => function ($link) use ($personLinkTypes) {
                    return $personLinkTypes[$link->type] ?? '-';
                },
            ],
            [
                'attribute' => 'foreign_id',
                'enableSorting' => false,
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'created_at',
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'updated_at',
            ]
        ],
    ]),
    'footer' => LinkPager::widget(['pagination' => $linkDataProvider->getPagination()]),
]) ?>