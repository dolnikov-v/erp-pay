<?php
use app\assets\vendor\BootstrapLaddaAsset;
use app\components\widgets\ActiveForm;
use app\widgets\ButtonProgress;
use yii\helpers\Url;

BootstrapLaddaAsset::register($this);
?>

<?php $form = ActiveForm::begin([
    'enableClientValidation' => false,
    'action' => Url::toRoute('index'),
    'method' => 'get',
]); ?>

<div class="row">
    <div class="col-lg-3">
        <?php if (Yii::$app->user->can('salary.person.changeteamleader')): ?>
            <div class="margin-bottom-5">
                <?= ButtonProgress::widget([
                    'id' => 'btn_change_team_leader',
                    'style' => ButtonProgress::STYLE_WARNING . ' width-300',
                    'label' => Yii::t('common', 'Сменить руководителя'),
                ]) ?>
            </div>
        <?php endif; ?>

        <?php if (Yii::$app->user->can('salary.person.setrole')): ?>
            <div class="margin-bottom-5">
                <?= ButtonProgress::widget([
                    'id' => 'btn_set_role',
                    'style' => ButtonProgress::STYLE_WARNING . ' width-300',
                    'label' => Yii::t('common', 'Сменить должность'),
                ]) ?>
            </div>
        <?php endif; ?>

        <?php if (Yii::$app->user->can('salary.person.setwork')): ?>
            <div class="margin-bottom-5">
                <?= ButtonProgress::widget([
                    'id' => 'btn_set_work',
                    'style' => ButtonProgress::STYLE_WARNING . ' width-300',
                    'label' => Yii::t('common', 'Задать рабочую смену'),
                ]) ?>
            </div>
        <?php endif; ?>
    </div>

    <div class="col-lg-3">
        <?php if (Yii::$app->user->can('salary.person.setbonus')): ?>
            <div class="margin-bottom-5">
                <?= ButtonProgress::widget([
                    'id' => 'btn_set_bonus',
                    'style' => ButtonProgress::STYLE_SUCCESS . ' width-300',
                    'label' => Yii::t('common', 'Назначение бонусов'),
                ]) ?>
            </div>
        <?php endif; ?>

        <?php if (Yii::$app->user->can('salary.person.setpenalty')): ?>
            <div class="margin-bottom-5">
                <?= ButtonProgress::widget([
                    'id' => 'btn_set_penalty',
                    'style' => ButtonProgress::STYLE_DANGER . ' width-300',
                    'label' => Yii::t('common', 'Назначение штрафов'),
                ]) ?>
            </div>
        <?php endif; ?>
    </div>

    <div class="col-lg-3">
        <?php if (Yii::$app->user->can('salary.person.activatepersons')): ?>
            <div class="margin-bottom-5">
                <?= ButtonProgress::widget([
                    'id' => 'btn_activate_persons',
                    'style' => ButtonProgress::STYLE_SUCCESS . ' width-300',
                    'label' => Yii::t('common', 'Активация сотрудников'),
                ]) ?>
            </div>
        <?php endif; ?>

        <?php if (Yii::$app->user->can('salary.person.deactivatepersons')): ?>
            <div class="margin-bottom-5">
                <?= ButtonProgress::widget([
                    'id' => 'btn_deactivate_persons',
                    'style' => ButtonProgress::STYLE_DANGER . ' width-300',
                    'label' => Yii::t('common', 'Увольнение сотрудников'),
                ]) ?>
            </div>
        <?php endif; ?>
    </div>

    <div class="col-lg-3">
        <?php if (Yii::$app->user->can('salary.person.approvepersons')): ?>
            <div class="margin-bottom-5">
                <?= ButtonProgress::widget([
                    'id' => 'btn_approve_persons',
                    'style' => ButtonProgress::STYLE_SUCCESS . ' width-300',
                    'label' => Yii::t('common', 'Подтверждение сотрудников'),
                ]) ?>
            </div>
        <?php endif; ?>

        <?php if (Yii::$app->user->can('salary.person.notapprovepersons')): ?>
            <div class="margin-bottom-5">
                <?= ButtonProgress::widget([
                    'id' => 'btn_notapprove_persons',
                    'style' => ButtonProgress::STYLE_DANGER . ' width-300',
                    'label' => Yii::t('common', 'Убрать подтверждение сотрудников'),
                ]) ?>
            </div>
        <?php endif; ?>
    </div>
</div>
