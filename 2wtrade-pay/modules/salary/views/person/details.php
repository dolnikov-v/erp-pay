<?php
use app\widgets\Panel;
use yii\helpers\Url;
use app\helpers\DataProvider;
use app\components\grid\GridView;
use app\widgets\Label;
use app\components\grid\ActionColumn;
use app\widgets\LinkPager;


/** @var yii\web\View $this */
/** @var app\modules\salary\models\Person $model */
/** @var yii\data\ActiveDataProvider $monthlyStatementDataProvider */

$this->title = Yii::t('common', 'Карточка сотрудника {name}', ['name' => $model->name]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Зарплата'), 'url' => '#'];
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('common', 'Управление сотрудниками'),
    'url' => Url::toRoute('index')
];
$this->params['breadcrumbs'][] = ['label' => $this->title];


?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Сотрудник'),
    'content' => $this->render('_details', [
        'model' => $model,
    ])
]) ?>

<?php if (Yii::$app->user->can('media.salaryperson.sheetview') || Yii::$app->user->can('media.salaryperson.sheetdownload')) : ?>
    <?= Panel::widget([
        'title' => Yii::t('common', 'Расчетные листы'),
        'actions' => DataProvider::renderSummary($monthlyStatementDataProvider),
        'withBody' => false,
        'content' => GridView::widget([
            'dataProvider' => $monthlyStatementDataProvider,
            'columns' => [
                [
                    'attribute' => 'date_from',
                    'label' => Yii::t('common', 'Период'),
                    'content' => function ($data) {
                        /** @var \app\modules\salary\models\MonthlyStatementData $data */
                        return Yii::$app->formatter->asDate($data->statement->date_from) . ' - ' . Yii::$app->formatter->asDate($data->statement->date_to);
                    },
                    'enableSorting' => false,
                ],
                [
                    'class' => ActionColumn::className(),
                    'items' => [
                        [
                            'label' => Yii::t('common', 'Посмотреть'),
                            'url' => function ($data) {
                                /** @var \app\modules\salary\models\MonthlyStatementData $data */
                                return Url::to('/media/salary-person/sheet-view/' . $data->salary_sheet_file);
                            },
                            'attributes' => function () {
                                return [
                                    'target' => '_blank'
                                ];
                            },
                            'can' => function () {
                                return Yii::$app->user->can('media.salaryperson.sheetview');
                            }
                        ],
                        [
                            'label' => Yii::t('common', 'Скачать'),
                            'url' => function ($data) {
                                /** @var \app\modules\salary\models\MonthlyStatementData $data */
                                return Url::to('/media/salary-person/sheet-download/' . $data->salary_sheet_file);
                            },
                            'can' => function () {
                                return Yii::$app->user->can('media.salaryperson.sheetdownload');
                            }
                        ],
                    ]
                ]
            ],
        ]),
        'footer' => LinkPager::widget(['pagination' => $monthlyStatementDataProvider->getPagination()]),
    ]) ?>
<?php endif; ?>