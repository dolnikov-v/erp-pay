<?php
use app\modules\salary\widgets\DetailPersonView;

/** @var app\modules\salary\models\Person $model */

?>
<?= DetailPersonView::widget([
    'model' => $model
]); ?>
