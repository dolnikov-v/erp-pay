<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;
use app\modules\salary\assets\BonusTypeAsset;

/** @var array $types */
/** @var array $currencies */
/** @var app\modules\salary\models\BonusType $model */

BonusTypeAsset::register($this);

$offices = \app\modules\salary\models\Office::find()->collection();
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['edit', 'id' => $model->id])]); ?>
<div class="row">
    <div class="col-lg-4">
        <?= $form->field($model, 'office_id')->select2List($offices, ['prompt' => Yii::t('common', 'Любой')]) ?>
    </div>
    <div class="col-lg-6"></div>
    <div class="col-lg-2">
        <?= $form->field($model, 'is_dismissal_pay')->checkboxCustom([
            'value' => 1,
            'checked' => $model->is_dismissal_pay,
        ]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-10">
        <div class="form-group">
            <?= $form->field($model, 'name')->textInput() ?>
        </div>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'active')->checkboxCustom([
            'value' => 1,
            'checked' => $model->active,
        ]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-10">
        <div class="form-group">
            <?= $form->field($model, 'type')->select2List($types) ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-4
 limit
 limit_<?= $model::BONUS_TYPE_PERCENT ?>
 limit_<?= $model::BONUS_TYPE_PERCENT_CALC_SALARY ?>
 limit_<?= $model::BONUS_TYPE_PERCENT_WORK_HOURS ?>
 limit_<?= $model::BONUS_TYPE_13_SALARY ?>"
        <?= (!in_array($model->type, [
            $model::BONUS_TYPE_PERCENT,
            $model::BONUS_TYPE_PERCENT_CALC_SALARY,
            $model::BONUS_TYPE_PERCENT_WORK_HOURS,
            $model::BONUS_TYPE_13_SALARY
        ]) ? 'style="display: none"' : '') ?>>
        <div class="form-group">
            <?= $form->field($model, 'percent')->textInput() ?>
        </div>
    </div>
    <div class="col-lg-8 limit
    limit_<?= $model::BONUS_TYPE_SUM ?>
    limit_<?= $model::BONUS_TYPE_SUM_WORK_HOURS ?>
    limit_<?= $model::BONUS_TYPE_13_SALARY ?>"
        <?= (!in_array($model->type, [
            $model::BONUS_TYPE_SUM,
            $model::BONUS_TYPE_SUM_WORK_HOURS,
            $model::BONUS_TYPE_13_SALARY,
        ]) ? 'style="display: none"' : '') ?>>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <?= $form->field($model, 'sum')->textInput() ?>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <?= $form->field($model, 'sum_local')->textInput() ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить тип бонуса') : Yii::t('common', 'Сохранить тип бонуса')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
