<?php
use app\widgets\Panel;
use yii\helpers\Url;
use app\widgets\Nav;

/** @var yii\web\View $this */
/** @var array $types */
/** @var array $currencies */
/** @var app\modules\salary\models\PenaltyType $model */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление типа штрафа') : Yii::t('common', 'Редактирование типа штрафа');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Зарплата'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Управление типами штрафов'), 'url' => Url::toRoute('/salary/penalty-type/index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'nav' => new Nav([
        'tabs' => [
            [
                'label' => Yii::t('common', 'Тип штрафа'),
                'content' => $this->render('_edit-form', [
                    'model' => $model,
                    'types' => $types,
                    'currencies' => $currencies
                ]),
            ],
        ]
    ])
])
?>

