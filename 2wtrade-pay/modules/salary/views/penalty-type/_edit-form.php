<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;
use app\modules\salary\assets\PenaltyTypeAsset;

/** @var array $types */
/** @var array $currencies */
/** @var app\modules\salary\models\PenaltyType $model */

PenaltyTypeAsset::register($this);
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['edit', 'id' => $model->id])]); ?>
<div class="row">
    <div class="col-lg-10">
        <div class="form-group">
            <?= $form->field($model, 'name')->textInput() ?>
        </div>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'active')->checkboxCustom([
            'value' => 1,
            'checked' => $model->active,
        ]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-4">
        <div class="form-group">
            <?= $form->field($model, 'type')->select2List($types) ?>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="form-group">
            <?= $form->field($model, 'trigger')->textInput(['disabled' => !Yii::$app->user->can('salary.penaltytype.changetrigger')]) ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-4 limit limit_<?= $model::PENALTY_TYPE_PERCENT ?>" <?= (($model->type != $model::PENALTY_TYPE_PERCENT) ? 'style="display: none"' : '') ?>>
        <div class="form-group">
            <?= $form->field($model, 'percent')->textInput() ?>
        </div>
    </div>
    <div class="col-lg-4 limit limit_<?= $model::PENALTY_TYPE_SUM ?>" <?= (($model->type != $model::PENALTY_TYPE_SUM) ? 'style="display: none"' : '') ?>>
        <div class="form-group">
            <?= $form->field($model, 'sum')->textInput() ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить тип штрафа') : Yii::t('common', 'Сохранить тип штрафа')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
