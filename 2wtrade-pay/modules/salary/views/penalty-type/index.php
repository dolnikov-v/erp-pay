<?php
use app\components\grid\ActionColumn;
use app\components\grid\DateColumn;
use app\components\grid\GridView;
use app\helpers\DataProvider;
use app\widgets\assets\custom\ModalConfirmDeleteAsset;
use app\widgets\ButtonLink;
use app\widgets\custom\ModalConfirmDelete;
use app\widgets\Panel;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use app\modules\salary\models\PenaltyType;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

ModalConfirmDeleteAsset::register($this);

$this->title = Yii::t('common', 'Типы штрафов');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Зарплата'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

$types = PenaltyType::getTypes();
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с типами штрафов'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function ($model) {
            return [
                'class' => 'tr-vertical-align-middle ' . ($model->active ? '' : 'text-muted'),
            ];
        },
        'columns' => [
            [
                'attribute' => 'name',
            ],
            [
                'attribute' => 'trigger',
            ],
            [
                'attribute' => 'type',
                'content' => function ($model) use ($types) {
                    return $types[$model->type];
                }
            ],
            [
                'attribute' => 'percent',
                'headerOptions' => ['class' => 'text-right'],
                'contentOptions' => ['class' => 'text-right'],
            ],
            [
                'attribute' => 'sum',
                'headerOptions' => ['class' => 'text-right'],
                'contentOptions' => ['class' => 'text-right'],
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'updated_at',
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'created_at',
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/salary/penalty-type/edit', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('salary.penaltytype.edit');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Активировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/salary/penalty-type/activate', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('salary.penaltytype.activate') && !$model->active;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Деактивировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/salary/penalty-type/deactivate', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('salary.penaltytype.deactivate') && $model->active;
                        }
                    ],
                ]
            ]
        ],
    ]),
    'footer' => (Yii::$app->user->can('salary.penaltytype.edit') ?
            ButtonLink::widget([
                'label' => Yii::t('common', 'Добавить тип штрафа'),
                'url' => Url::toRoute('/salary/penalty-type/edit'),
                'style' => ButtonLink::STYLE_SUCCESS,
                'size' => ButtonLink::SIZE_SMALL,
            ]) : '') . LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>

<?= ModalConfirmDelete::widget() ?>
