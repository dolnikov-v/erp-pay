<?php

use app\components\widgets\ActiveForm;
use app\modules\salary\models\Office;
use yii\helpers\Url;

/** @var Office $model */

$sort = [];
foreach (Office::expensesFields() as $expense) {
    $sort[$expense] = $model->getAttributeLabel($expense);
}
asort($sort);
$expenseFields = array_keys($sort);

$currencies = \app\models\Currency::find()->collection();
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['edit-expenses', 'id' => $model->id])]); ?>
    <div class="row">
        <?php foreach ($expenseFields as $field): ?>
            <div class="col-lg-4">
                <?= $form->field($model, $field)->textInput() ?>
            </div>
            <div class="col-lg-2">
                <?= $form->field($model, $field . '_currency_id')->select2List($currencies, ['length' => 1]) ?>
            </div>
        <?php endforeach; ?>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <?= $form->submit(Yii::t('common', 'Сохранить')) ?>
            <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>