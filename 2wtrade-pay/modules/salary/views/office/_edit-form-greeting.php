<?php
use app\components\widgets\ActiveForm;
use app\modules\salary\models\Office;
use yii\helpers\Url;

/** @var Office $model */

?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['edit-greeting', 'id' => $model->id])]); ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="form-group">
                <?= $form->field($model, 'birthday_greeting_card_template_subject')->textInput() ?>
            </div>
            <div class="form-group">
                <?= $form->field($model, 'birthday_greeting_card_template_message')->textarea(['rows' => 4]) ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            {*NAME*} - <?= Yii::t('common', 'ФИО') ?> <br>
            {*DATE*} - <?= Yii::t('common', 'дата') ?> <br><br>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <?= $form->submit(Yii::t('common', 'Сохранить')) ?>
            <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>