<?php

use app\components\grid\ActionColumn;
use app\components\grid\DateColumn;
use app\components\grid\GridView;
use app\widgets\ButtonLink;
use app\widgets\Panel;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var app\modules\salary\models\Office $model */
/** @var yii\data\ActiveDataProvider $expenseDataProvider */

$this->title = Yii::t('common', 'Постоянные расходы офиса {name}', ['name' => $model->name]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Зарплата'), 'url' => '#'];
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('common', 'Управление офисами'),
    'url' => Url::toRoute('/salary/office/index')
];
$this->params['breadcrumbs'][] = ['label' => $this->title];

$expensesFieldsList = $model::getExpensesFieldsList();
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Постоянные расходы'),
    'nav' => new \app\modules\salary\widgets\OfficeLinkNav([
        'office' => $model
    ]),
    'content' => $this->render('_edit-form-expenses', [
        'model' => $model,
        'expenseDataProvider' => $expenseDataProvider,
    ])
]); ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Периодические расходы'),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $expenseDataProvider,
        'columns' => [
            [
                'attribute' => 'type',
                'enableSorting' => false,
                'content' => function ($model) use ($expensesFieldsList) {
                    return $expensesFieldsList[$model->type] ?? '-';
                }
            ],
            [
                'attribute' => 'expense',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'currency_id',
                'enableSorting' => false,
                'content' => function ($model) {
                    /** @var \app\modules\salary\models\OfficeExpense $model */
                    return $model->currency->name ?? '-';
                }
            ],
            [
                'attribute' => 'filename',
                'enableSorting' => false,
                'content' => function ($model) {
                    /** @var \app\modules\salary\models\OfficeExpense $model */
                    return Html::a(Yii::t('common', 'Просмотр'), '/media/salary-expense/filename/' . $model->filename, ['target' => '_blank']);
                }
            ],
            [
                'class' => DateColumn::className(),
                'formatType' => 'Date',
                'attribute' => 'date_from',
                'enableSorting' => false,
            ],
            [
                'class' => DateColumn::className(),
                'formatType' => 'Date',
                'attribute' => 'date_to',
                'enableSorting' => false,
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'created_at',
                'enableSorting' => false,
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'updated_at',
                'enableSorting' => false,
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/salary/office-expense/edit', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('salary.officeexpense.edit');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить'),
                        'url' => function ($work) {
                            return Url::toRoute([
                                '/salary/office-expense/delete',
                                'id' => $work->id,
                                'office_id' => $work->office_id
                            ]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('salary.officeexpense.delete');
                        }
                    ],
                ]
            ]
        ],
    ]),
    'footer' => (Yii::$app->user->can('salary.officeexpense.edit') ?
        ButtonLink::widget([
            'label' => Yii::t('common', 'Добавить расход'),
            'url' => Url::toRoute(['/salary/office-expense/edit', 'office_id' => $model->id]),
            'style' => ButtonLink::STYLE_SUCCESS,
            'size' => ButtonLink::SIZE_SMALL,
        ])
        : '')
]) ?>
