<?php
use app\components\widgets\ActiveForm;
use app\modules\salary\models\Office;
use yii\helpers\Url;

/** @var Office $model */

?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['edit-system', 'id' => $model->id])]); ?>
<div class="row">
    <div class="col-lg-4">
        <div class="form-group">
            <?= $form->field($model, 'norm_hours_per_workplace')->textInput() ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-4">
        <div class="form-group">
            <?= $form->field($model, 'jobs_number')->textInput() ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-4">
        <?= $form->field($model, 'technical_equipment_mikrotik')->checkboxCustom([
            'value' => 1,
            'checked' => $model->technical_equipment_mikrotik,
            'uncheckedValue' => 0
        ]) ?>
        <?= $form->field($model, 'technical_equipment_video')->checkboxCustom([
            'value' => 1,
            'checked' => $model->technical_equipment_video,
            'uncheckedValue' => 0
        ]) ?>
        <?= $form->field($model, 'technical_equipment_internet')->checkboxCustom([
            'value' => 1,
            'checked' => $model->technical_equipment_internet,
            'uncheckedValue' => 0
        ]) ?>
        <br>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить офис') : Yii::t('common', 'Сохранить офис')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
