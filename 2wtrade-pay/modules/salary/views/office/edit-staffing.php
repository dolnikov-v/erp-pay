<?php
use app\widgets\Panel;
use yii\helpers\Url;

use app\components\grid\ActionColumn;
use app\components\grid\GridView;
use app\helpers\DataProvider;
use app\widgets\ButtonLink;
use yii\widgets\LinkPager;

/** @var yii\web\View $this */
/** @var app\modules\salary\models\Office $model */
/** @var yii\data\ActiveDataProvider $staffingDataProvider */

$this->title = Yii::t('common', 'Штатное расписание офиса {name}', ['name' => $model->name]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Зарплата'), 'url' => '#'];
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('common', 'Управление офисами'),
    'url' => Url::toRoute('/salary/office/index')
];
$this->params['breadcrumbs'][] = ['label' => $this->title];

?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Штатное расписание'),
    'nav' => new \app\modules\salary\widgets\OfficeLinkNav([
        'office' => $model
    ]),
    'actions' => DataProvider::renderSummary($staffingDataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $staffingDataProvider,
        'columns' => [
            [
                'attribute' => 'designation_id',
                'content' => function ($model) {
                    return $model->designation_id && isset($model->designation->name) ? Yii::t('common', $model->designation->name) : '-';
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'max_persons',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'enableSorting' => false,
            ],
            [
                'attribute' => 'min_jobs',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'enableSorting' => false,
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/salary/staffing/edit', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('salary.staffing.edit');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить'),
                        'url' => function ($work) {
                            return Url::toRoute([
                                '/salary/staffing/delete',
                                'id' => $work->id,
                                'office_id' => $work->office_id
                            ]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('salary.staffing.delete');
                        }
                    ],
                ]
            ]
        ],
    ]),
    'footer' => (Yii::$app->user->can('salary.staffing.edit') ?
            ButtonLink::widget([
                'label' => Yii::t('common', 'Добавить должность в штатное расписание'),
                'url' => Url::toRoute(['/salary/staffing/edit', 'office_id' => $model->id]),
                'style' => ButtonLink::STYLE_SUCCESS,
                'size' => ButtonLink::SIZE_SMALL,
            ])
            : '') . LinkPager::widget(['pagination' => $staffingDataProvider->getPagination()]),
]) ?>
