<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\widgets\Label;
use app\widgets\Panel;

use app\components\grid\ActionColumn;
use app\components\grid\GridView;
use app\helpers\DataProvider;
use app\widgets\ButtonLink;
use yii\widgets\LinkPager;

use app\components\grid\DateColumn;


/** @var yii\web\View $this */
/** @var app\modules\salary\models\Office $model */
/** @var yii\data\ActiveDataProvider $directionDataProvider */

$this->title = Yii::t('common', 'Направления офиса {name}', ['name' => $model->name]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Зарплата'), 'url' => '#'];
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('common', 'Управление офисами'),
    'url' => Url::toRoute('/salary/office/index')
];
$this->params['breadcrumbs'][] = ['label' => $this->title];

?>
<?= Panel::widget([
    'title' => Yii::t('common', 'Направления колл-центра'),
    'nav' => new \app\modules\salary\widgets\OfficeLinkNav([
        'office' => $model
    ]),
    'actions' => DataProvider::renderSummary($directionDataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $directionDataProvider,
        'columns' => [
            [
                'attribute' => 'name',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'country_id',
                'content' => function ($direction) {
                    return $direction->country_id ? Yii::t('common', $direction->country->name) : '-';
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'weight',
                'content' => function ($direction) {
                    return $direction->weight;
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'url',
                'content' => function ($direction) {
                    return $direction->url ? Html::a($direction->url, $direction->url, [
                        'target' => '_blank',
                    ]) : '';
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'active',
                'headerOptions' => ['class' => 'text-center width-150'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($direction) {
                    return Label::widget([
                        'label' => $direction->active ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $direction->active ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'created_at',
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($direction) use ($model) {
                            return Url::toRoute([
                                '/call-center/control/edit',
                                'id' => $direction->id,
                                'office_id' => $model->id
                            ]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('callcenter.control.edit');
                        }
                    ],
                ]
            ]
        ],
    ]),
    'footer' => (Yii::$app->user->can('callcenter.control.edit') ?
            ButtonLink::widget([
                'label' => Yii::t('common', 'Добавить новое направление'),
                'url' => Url::toRoute(['/call-center/control/edit', 'office_id' => $model->id]),
                'style' => ButtonLink::STYLE_SUCCESS,
                'size' => ButtonLink::SIZE_SMALL,
            ])
            : '') . LinkPager::widget(['pagination' => $directionDataProvider->getPagination()]),
]) ?>