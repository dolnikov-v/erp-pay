<?php
use yii\helpers\Url;
use app\widgets\Panel;

use app\components\grid\ActionColumn;
use app\components\grid\GridView;
use app\helpers\DataProvider;
use app\widgets\ButtonLink;
use yii\widgets\LinkPager;

use app\components\grid\DateColumn;

use app\modules\salary\models\OfficeTax;


/** @var yii\web\View $this */
/** @var app\modules\salary\models\Office $model */
/** @var yii\data\ActiveDataProvider $taxDataProvider */

$this->title = Yii::t('common', 'Налоговые ставки {name}', ['name' => $model->name]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Зарплата'), 'url' => '#'];
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('common', 'Управление офисами'),
    'url' => Url::toRoute('/salary/office/index')
];
$this->params['breadcrumbs'][] = ['label' => $this->title];

$types = OfficeTax::getTypes();
$taxTypes = OfficeTax::getTaxTypes();

?>
<?= Panel::widget([
    'title' => Yii::t('common', 'Налоговые ставки'),
    'nav' => new \app\modules\salary\widgets\OfficeLinkNav([
        'office' => $model
    ]),
    'actions' => DataProvider::renderSummary($taxDataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $taxDataProvider,
        'columns' => [
            [
                'attribute' => 'tax_type',
                'content' => function ($model) use ($taxTypes) {
                    return $taxTypes[$model->tax_type];
                }
            ],
            [
                'attribute' => 'type',
                'content' => function ($model) use ($types) {
                    return $types[$model->type];
                }
            ],
            [
                'attribute' => 'unpaid_amount',
            ],
            [
                'attribute' => 'sum_from',
            ],
            [
                'attribute' => 'sum_to',
            ],
            [
                'attribute' => 'rate',
            ],
            [
                'attribute' => 'amount',
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'created_at',
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'updated_at',
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/salary/office-tax/edit', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('salary.officetax.edit');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить'),
                        'url' => function ($work) {
                            return Url::toRoute([
                                '/salary/office-tax/delete',
                                'id' => $work->id,
                                'office_id' => $work->office_id
                            ]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('salary.officetax.delete');
                        }
                    ],
                ]
            ]
        ],
    ]),
    'footer' => (Yii::$app->user->can('salary.officetax.edit') ?
            ButtonLink::widget([
                'label' => Yii::t('common', 'Добавить налоговую ставку'),
                'url' => Url::toRoute(['/salary/office-tax/edit', 'office_id' => $model->id]),
                'style' => ButtonLink::STYLE_SUCCESS,
                'size' => ButtonLink::SIZE_SMALL,
            ])
            : '') . LinkPager::widget(['pagination' => $taxDataProvider->getPagination()]),
]) ?>