<?php
use app\widgets\Panel;
use yii\helpers\Url;

use app\widgets\assets\TimeRangePickerAsset;

/** @var yii\web\View $this */
/** @var app\modules\salary\models\Office $model */

$this->title = Yii::t('common', 'Шаблон письма офиса {name}', ['name' => $model->name]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Зарплата'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Управление офисами'), 'url' => Url::toRoute('/salary/office/index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];

?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Шаблон письма поздравления с Днем Рождения'),
    'nav' => new \app\modules\salary\widgets\OfficeLinkNav([
        'office' => $model
    ]),
    'content' => $this->render('_edit-form-greeting', [
        'model' => $model,
    ])
]); ?>
