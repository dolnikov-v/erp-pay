<?php
use app\components\grid\ActionColumn;
use app\components\grid\DateColumn;
use app\components\grid\GridView;
use app\helpers\DataProvider;
use app\widgets\assets\custom\ModalConfirmDeleteAsset;
use app\widgets\ButtonLink;
use app\widgets\custom\ModalConfirmDelete;
use app\widgets\Label;
use app\widgets\Panel;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use app\modules\salary\models\Office;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

ModalConfirmDeleteAsset::register($this);

$this->title = Yii::t('common', 'Офисы');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Зарплата'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

$officeTypes = Office::getTypes();
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с офисами'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function ($model) {
            return [
                'class' => 'tr-vertical-align-middle ' . ($model->active ? '' : 'text-muted'),
            ];
        },
        'columns' => [
            [
                'attribute' => 'name',
            ],
            [
                'attribute' => 'country_id',
                'content' => function ($model) {
                    return ($model->country instanceof \app\models\Country ? Yii::t('common', $model->country->name) : '');
                },
                'enableSorting' => false,
            ],
            [
                'label' => Yii::t('common', 'Направления'),
                'content' => function ($model) {
                    $return = [];
                    foreach ($model->callCenters as $callCenter) {
                        $return[] = $callCenter->country->name;
                    }
                    return implode(", ", $return);
                },
                'enableSorting' => false
            ],
            [
                'attribute' => 'type',
                'content' => function ($model) use ($officeTypes) {
                    return $officeTypes[$model->type] ?? '-';
                },
            ],
            [
                'attribute' => 'active',
                'headerOptions' => ['class' => 'text-center width-150'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return Label::widget([
                        'label' => $model->active ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->active ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'created_at',
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'updated_at',
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Просмотр'),
                        'url' => function ($model) {
                            return Url::toRoute(['view', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('salary.office.view');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['edit', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('salary.office.edit');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Системные настройки'),
                        'url' => function ($model) {
                            return Url::toRoute(['edit-system', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('salary.office.editsystem');
                        }
                    ],
                    [
                        'can' => function () {
                            return Yii::$app->user->can('salary.office.edit');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Шаблон письма поздравления с Днем Рождения'),
                        'url' => function ($model) {
                            return Url::toRoute(['edit-greeting', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('salary.office.editgreeting');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Постоянные расходы'),
                        'url' => function ($model) {
                            return Url::toRoute(['edit-expenses', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('salary.office.editexpenses');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Налоговые ставки'),
                        'url' => function ($model) {
                            return Url::toRoute(['edit-tax', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('salary.office.edittax');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Направления колл-центра'),
                        'url' => function ($model) {
                            return Url::toRoute(['edit-direction', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return $model->type === $model::TYPE_CALL_CENTER && Yii::$app->user->can('salary.office.editdirection');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Рабочие смены'),
                        'url' => function ($model) {
                            return Url::toRoute(['edit-working', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('salary.office.editworking');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Штатное расписание'),
                        'url' => function ($model) {
                            return Url::toRoute(['edit-staffing', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('salary.office.editstaffing');
                        }
                    ],
                    [
                        'can' => function () {
                            return Yii::$app->user->can('salary.office.activate') || Yii::$app->user->can('salary.office.deactivate');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Активировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['activate', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('salary.office.activate') && !$model->active;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Деактивировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['deactivate', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('salary.office.deactivate') && $model->active;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Лог изменений'),
                        'url' => function ($model) {
                            return Url::toRoute(['/salary/office/log', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('salary.office.log');
                        }
                    ]
                ]
            ]
        ],
    ]),
    'footer' => (Yii::$app->user->can('salary.office.edit') ?
            ButtonLink::widget([
                'label' => Yii::t('common', 'Добавить офис'),
                'url' => Url::toRoute('/salary/office/edit'),
                'style' => ButtonLink::STYLE_SUCCESS,
                'size' => ButtonLink::SIZE_SMALL,
            ]) : '') . LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>

<?= ModalConfirmDelete::widget() ?>
