<?php
use app\widgets\Panel;
use yii\helpers\Url;

use app\widgets\assets\TimeRangePickerAsset;

/** @var yii\web\View $this */
/** @var app\modules\salary\models\Office $model */

$this->title = Yii::t('common', 'Системные настройки офиса {name}', ['name' => $model->name]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Зарплата'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Управление офисами'), 'url' => Url::toRoute('/salary/office/index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];

TimeRangePickerAsset::register($this);
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Системные настройки'),
    'nav' => new \app\modules\salary\widgets\OfficeLinkNav([
        'office' => $model
    ]),
    'content' => $this->render('_edit-form-system', [
        'model' => $model
    ])
]); ?>