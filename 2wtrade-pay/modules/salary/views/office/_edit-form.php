<?php
use app\components\widgets\ActiveForm;
use app\modules\salary\models\Office;
use app\widgets\Select2;
use app\widgets\TimeRangePicker;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Currency;

/** @var Office $model */
/** @var array $countries */
/** @var bool $isView */

?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute([$isView ? 'view' : 'edit', 'id' => $model->id])]); ?>
<div class="row">
    <div class="col-lg-4">
        <?= $form->field($model, 'name')->textInput([
            'disabled' => $isView
        ]) ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'country_id')->select2List($countries, [
            'prompt' => Yii::t('common', 'Выберите страну'),
            'disabled' => $isView
        ]) ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'type')->select2List($model::getTypes(), [
            'disabled' => $isView
        ]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-8">
        <?= $form->field($model, 'address')->textInput([
            'disabled' => $isView
        ]) ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'currency_id')->select2List(Currency::find()->collection(), [
            'prompt' => '— по стране —',
            'disabled' => $isView
        ]); ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-4">
        <?= $form->field($model, 'count_fixed_norm')->checkboxCustom([
            'value' => 1,
            'checked' => $model->count_fixed_norm,
            'uncheckedValue' => 0,
            'disabled' => $isView
        ]) ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'calc_salary_local')->checkboxCustom([
            'value' => 1,
            'checked' => $model->calc_salary_local,
            'uncheckedValue' => 0,
            'disabled' => $isView
        ]) ?>
        <?= $form->field($model, 'calc_salary_usd')->checkboxCustom([
            'value' => 1,
            'checked' => $model->calc_salary_usd,
            'uncheckedValue' => 0,
            'disabled' => $isView
        ]) ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'active')->checkboxCustom([
            'value' => 1,
            'checked' => $model->active,
            'uncheckedValue' => 0,
            'disabled' => $isView
        ]) ?>
        <?php if (Yii::$app->user->can('office_edit_hide_staffing_salary')): ?>
            <?= $form->field($model, 'hide_staffing_salary')->checkboxCustom([
                'value' => 1,
                'checked' => $model->hide_staffing_salary,
                'uncheckedValue' => 0,
                'disabled' => $isView
            ]) ?>
        <?php endif; ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-4">
        <?= $form->field($model, 'working_hours_per_week')->textInput([
            'disabled' => $isView
        ]) ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'holiday_hours')->textInput([
            'disabled' => $isView
        ]) ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'auto_penalty')->checkboxCustom([
            'value' => 1,
            'checked' => $model->auto_penalty,
            'uncheckedValue' => 0,
            'disabled' => $isView
        ]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-4">
        <?= $form->field($model, 'min_salary_local')->textInput([
            'disabled' => $isView
        ]) ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'min_salary_usd')->textInput([
            'disabled' => $isView
        ]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-4">
        <?= $form->field($model, 'holidays_coefficient')->textInput([
            'disabled' => $isView
        ]) ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'extra_days_coefficient')->textInput([
            'disabled' => $isView
        ]) ?>
    </div>
</div>
<?php if (Yii::$app->user->can('salary.office.edit.worktime')): ?>
    <div class="row">
        <div class="col-lg-3">
            <div class="form-group">
                <label><?= Yii::t('common', 'Время работы (GMT+0)') ?></label>
                <?= TimeRangePicker::widget([
                    'valueFrom' => $model->workTime['from'] ?? null,
                    'valueTo' => $model->workTime['to'] ?? null,
                    'nameFrom' => Html::getInputName($model, 'work_time') . "[from]",
                    'nameTo' => Html::getInputName($model, 'work_time') . "[to]",
                    'disabled' => $isView
                ]) ?>
            </div>
        </div>
        <div class="col-lg-5">
            <div class="form-group">
                <label><?= Yii::t('common', 'Рабочие дни') ?></label>
                <?= Select2::widget([
                    'name' => Html::getInputName($model, 'work_time') . '[days][]',
                    'id' => Html::getInputId($model, 'work_time') . '-days',
                    'value' => $model->workTime['days'] ?? null,
                    'multiple' => 'multiple',
                    'items' => Office::getWeekDays(),
                    'disabled' => $isView
                ]) ?>
            </div>
        </div>
    </div>
<?php endif; ?>

<div class="row">
    <div class="col-lg-12">
        <?php if (!$isView): ?>
            <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить офис') : Yii::t('common', 'Сохранить офис')) ?>
        <?php endif; ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
