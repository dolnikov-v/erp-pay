<?php
use app\widgets\Panel;
use yii\helpers\Url;
use app\widgets\Label;

use app\components\grid\ActionColumn;
use app\components\grid\GridView;
use app\helpers\DataProvider;
use app\widgets\ButtonLink;
use yii\widgets\LinkPager;

/** @var yii\web\View $this */
/** @var app\modules\salary\models\Office $model */
/** @var yii\data\ActiveDataProvider $workDataProvider */

$this->title = Yii::t('common', 'Рабочие смены офиса {name}', ['name' => $model->name]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Зарплата'), 'url' => '#'];
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('common', 'Управление офисами'),
    'url' => Url::toRoute('/salary/office/index')
];
$this->params['breadcrumbs'][] = ['label' => $this->title];

?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Рабочие смены'),
    'nav' => new \app\modules\salary\widgets\OfficeLinkNav([
        'office' => $model
    ]),
    'actions' => DataProvider::renderSummary($workDataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $workDataProvider,
        'columns' => [
            [
                'attribute' => 'name',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'lunch_time',
                'enableSorting' => false,
            ],
            [
                'label' => Yii::t('common', 'Рабочие дни'),
                'content' => function ($model) {
                    $r = [];
                    if ($model->working_mon) $r[] = Yii::t('common', 'Пн') . ' ' . $model->working_mon;
                    if ($model->working_tue) $r[] = Yii::t('common', 'Вт') . ' ' . $model->working_tue;
                    if ($model->working_wed) $r[] = Yii::t('common', 'Ср') . ' ' . $model->working_wed;
                    if ($model->working_thu) $r[] = Yii::t('common', 'Чт') . ' ' . $model->working_thu;
                    if ($model->working_fri) $r[] = Yii::t('common', 'Пт') . ' ' . $model->working_fri;
                    if ($model->working_sat) $r[] = Yii::t('common', 'Сб') . ' ' . $model->working_sat;
                    if ($model->working_sun) $r[] = Yii::t('common', 'Вс') . ' ' . $model->working_sun;
                    return implode('<br>', $r);
                },
            ],
            [
                'attribute' => 'default',
                'headerOptions' => ['class' => 'text-center width-150'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return Label::widget([
                        'label' => $model->default ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->default ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/salary/working-shift/edit', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('salary.workingshift.edit');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить'),
                        'url' => function ($work) {
                            return Url::toRoute(['/salary/working-shift/delete', 'id' => $work->id, 'office_id' => $work->office_id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('salary.workingshift.delete');
                        }
                    ],
                ]
            ]
        ],
    ]),
    'footer' => (Yii::$app->user->can('salary.workingshift.edit') ?
            ButtonLink::widget([
                'label' => Yii::t('common', 'Добавить рабочую смену'),
                'url' => Url::toRoute(['/salary/working-shift/edit', 'office_id' => $model->id]),
                'style' => ButtonLink::STYLE_SUCCESS,
                'size' => ButtonLink::SIZE_SMALL,
            ])
            : '') . LinkPager::widget(['pagination' => $workDataProvider->getPagination()]),
]) ?>