<?php

use app\widgets\Log;
use yii\helpers\Url;

/**
 * @var \app\models\logs\TableLog[] $changeLog
 */

$this->title = Yii::t('common', 'Лог изменений');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Офисы'), 'url' => Url::toRoute('/salary/office/index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<div class="panel panel panel-bordered ">
    <ul class="panel nav nav-tabs" role="tablist">
        <li class="active"><a href="#tabChange" aria-controls="tabChange" role="tab" data-toggle="tab">SalaryOffice</a></li>
        <li><a href="#tabOne" aria-controls="tabOne" role="tab" data-toggle="tab">SalaryOfficeTax</a></li>
        <li><a href="#tabTwo" aria-controls="tabTwo" role="tab" data-toggle="tab">SalaryStaffing</a></li>
        <li><a href="#tabThree" aria-controls="tabThree" role="tab" data-toggle="tab">SalaryWorkingShift</a></li>
    </ul>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="tabChange">
            <?= Log::widget([
                'title' => Yii::t('common', 'История изменения'),
                'data' => $changeLog,
            ]); ?>
        </div>
        <div role="tabpanel" class="tab-pane" id="tabOne">
            <?= Log::widget([
                'data' => $tax,
            ]); ?>
        </div>
        <div role="tabpanel" class="tab-pane" id="tabTwo">
            <?= Log::widget([
                'data' => $staffing,
            ]); ?>
        </div>
        <div role="tabpanel" class="tab-pane" id="tabThree">
            <?= Log::widget([
                'data' => $workingShift,
            ]); ?>
        </div>
    </div>
</div>
