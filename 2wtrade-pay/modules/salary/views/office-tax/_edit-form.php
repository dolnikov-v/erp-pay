<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;
use app\modules\salary\models\Office;

/** @var app\modules\salary\models\OfficeTax $model */
/** @var integer $officeId */

$offices = Office::find()->collection();
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['edit', 'id' => $model->id])]); ?>
<div class="row">
    <div class="col-lg-3">
        <?= $form->field($model, 'type')->select2List($model::getTypes()) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'tax_type')->select2List($model::getTaxTypes()) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'unpaid_amount')->textInput() ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-3">
        <?= $form->field($model, 'sum_from')->textInput() ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'sum_to')->textInput() ?>
    </div>
    <div class="col-lg-3 limit limit_<?= $model::TYPE_PERCENT?>" <?= (($model->type != $model::TYPE_PERCENT) ? 'style="display: none"' : '') ?>>
        <?= $form->field($model, 'rate')->textInput() ?>
    </div>
    <div class="col-lg-3 limit limit_<?= $model::TYPE_FIXED?>" <?= (($model->type != $model::TYPE_FIXED) ? 'style="display: none"' : '') ?>>
        <?= $form->field($model, 'amount')->textInput() ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'office_id')->hiddenInput()->label('') ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить налоговую ставку') : Yii::t('common', 'Сохранить налоговую ставку')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute(['office/edit-tax/', 'id' => $model->office_id])) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
