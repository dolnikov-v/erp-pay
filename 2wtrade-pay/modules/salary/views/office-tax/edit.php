<?php
use app\widgets\Panel;
use yii\helpers\Url;
use app\modules\salary\assets\OfficeTaxAsset;

/** @var yii\web\View $this */
/** @var app\modules\salary\models\OfficeTax $model */
/** @var integer $officeId */

OfficeTaxAsset::register($this);

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление налоговой ставки') : Yii::t('common', 'Редактирование налоговой ставки');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Зарплата'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Управление офисами'), 'url' => Url::toRoute('/salary/office/index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];

?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Налоговая ставка'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
        'officeId' => $officeId,
    ])
]) ?>