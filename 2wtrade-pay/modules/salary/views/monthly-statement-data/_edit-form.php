<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/** @var array $offices */
/** @var app\modules\salary\models\MonthlyStatementData $model */
/** @var app\modules\salary\models\MonthlyStatement $parent */
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['edit', 'id' => $model->id])]); ?>
<div class="row">
    <div class="col-lg-3">
        <?= $form->field($model, 'person_id')->textInput() ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'name')->textInput() ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'designation')->textInput() ?>
    </div>
    <div class="col-lg-3 no-margin-custom-checkbox">
        <?= $form->field($model, 'active')->checkboxCustom(['value' => 1,
            'checked' => $model->active,]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-3">
        <?= $form->field($model, 'salary_local')->textInput() ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'salary_usd')->textInput() ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'salary_hour_local')->textInput() ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'salary_hour_usd')->textInput() ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-3">
        <?= $form->field($model, 'start_date')->datePicker() ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'dismissal_date')->datePicker() ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-3">
        <?= $form->field($model, 'parent_id')->textInput() ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'parent_name')->textInput() ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'parent_designation')->textInput() ?>
    </div>
    <div class="col-lg-3 no-margin-custom-checkbox">
        <?= $form->field($model, 'designation_team_lead')->checkboxCustom(['value' => 1,
            'checked' => $model->designation_team_lead,]) ?>
        <?= $form->field($model, 'designation_calc_work_time')->checkboxCustom(['value' => 1,
            'checked' => $model->designation_calc_work_time,]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-9">
        <div class="control-label"><label><?= Yii::t('common', 'Рабочие дни') ?></label></div>
        <div class="row">
            <div class="col-sm-2">
                <?= $form->field($model, 'working_1')->checkTextInput() ?>
            </div>
            <div class="col-sm-2">
                <?= $form->field($model, 'working_2')->checkTextInput() ?>
            </div>
            <div class="col-sm-2">
                <?= $form->field($model, 'working_3')->checkTextInput() ?>
            </div>
            <div class="col-sm-2">
                <?= $form->field($model, 'working_4')->checkTextInput() ?>
            </div>
            <div class="col-sm-2">
                <?= $form->field($model, 'working_5')->checkTextInput() ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-2">
                <?= $form->field($model, 'working_6')->checkTextInput() ?>
            </div>
            <div class="col-sm-2">
                <?= $form->field($model, 'working_7')->checkTextInput() ?>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'lunch_time')->textInput() ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-3">
        <?= $form->field($model, 'country_names')->textInput() ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'call_center_user_logins')->textInput() ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-3">
        <?= $form->field($model, 'call_center_oper_ids')->textInput() ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'call_center_user_ids')->textInput() ?>
    </div>
    <div class="col-lg-6">
        <?= $form->field($model, 'call_center_id_oper_id')->textInput() ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить сотрудника в график') : Yii::t('common', 'Сохранить сотрудника в графике')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('monthly-statement/edit/' . $parent->id)) ?>
        <?= $form->field($model, 'statement_id')->hiddenInput(['value' => $parent->id])->label(false); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
