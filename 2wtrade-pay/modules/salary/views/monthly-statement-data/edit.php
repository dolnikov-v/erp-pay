<?php
use app\widgets\Panel;
use yii\helpers\Url;
use app\widgets\Nav;
use app\widgets\assets\custom\ModalConfirmDeleteAsset;

/** @var yii\web\View $this */
/** @var array $persons */
/** @var app\modules\salary\models\MonthlyStatementData $model */
/** @var app\modules\salary\models\MonthlyStatement $parent */

ModalConfirmDeleteAsset::register($this);

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление сотрудника') : Yii::t('common', 'Редактирование сотрудника');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Зарплата'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Управление графиками'), 'url' => Url::toRoute('/salary/monthly-statement/edit/' . $parent->id)];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'nav' => new Nav([
        'tabs' => [
            [
                'label' => Yii::t('common', 'Сотрудник'),
                'content' => $this->render($model->isNewRecord ? '_add-form' : '_edit-form', [
                    'model' => $model,
                    'parent' => $parent,
                    'persons' => $persons,
                ]),
            ],
        ]
    ])
]) ?>