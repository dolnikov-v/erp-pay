<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/** @var array $persons */
/** @var app\modules\salary\models\MonthlyStatementData $model */
/** @var app\modules\salary\models\MonthlyStatement $parent */
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['edit', 'id' => $model->id])]); ?>
<div class="row">
    <div class="col-lg-3">
        <?= $form->field($model, 'person_id', ['labelOptions' => ['label' => Yii::t('common', 'Сотрудники')]])->select2ListMultiple($persons, ['prompt' => '-']) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= $form->submit(Yii::t('common', 'Добавить сотрудника в график')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('monthly-statement/edit/' . $parent->id)) ?>
        <?= $form->field($model, 'statement_id')->hiddenInput(['value' => $parent->id])->label(false); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
