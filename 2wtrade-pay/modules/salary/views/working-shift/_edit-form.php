<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;
use app\modules\salary\models\Office;
use yii\helpers\Html;

/** @var app\modules\salary\models\WorkingShift $model */
/** @var array $salarySchemes */
/** @var array $parentUsers */
/** @var integer $officeId */
/** @var string $back */

$offices = Office::find()->bySystemUserCountries()->collection();
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute(['edit', 'id' => $model->id]),
    'options' => [
        'class' => !$model->isNewRecord ? 'working-shift-edit' : ''
    ]
]); ?>
<div class="row">
    <div class="col-lg-3">
        <?= $form->field($model, 'name')->textInput() ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'office_id')->select2List($offices, [
            'prompt' => '-',
            'disabled' => !$model->isNewRecord
        ]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'lunch_time')->textInput() ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'default')->checkboxCustom([
            'value' => 1,
            'checked' => $model->default,
        ]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-9">
        <div class="control-label"><label><?= Yii::t('common', 'Рабочие дни') ?></label></div>
        <div class="row">
            <div class="col-sm-2">
                <?= $form->field($model, 'working_mon')->checkTextInput() ?>
            </div>
            <div class="col-sm-2">
                <?= $form->field($model, 'working_tue')->checkTextInput() ?>
            </div>
            <div class="col-sm-2">
                <?= $form->field($model, 'working_wed')->checkTextInput() ?>
            </div>
            <div class="col-sm-2">
                <?= $form->field($model, 'working_thu')->checkTextInput() ?>
            </div>
            <div class="col-sm-2">
                <?= $form->field($model, 'working_fri')->checkTextInput() ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-2">
                <?= $form->field($model, 'working_sat')->checkTextInput() ?>
            </div>
            <div class="col-sm-2">
                <?= $form->field($model, 'working_sun')->checkTextInput() ?>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить рабочую смену') : Yii::t('common', 'Сохранить рабочую смену')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'),
            $back == 'working-shift' ?
                Url::to([
                    'working-shift/index',
                    'PersonSearch' => [
                        'office_id' => $model->office_id
                    ],
                    'activeTab' => 1
                ])
                :
                Url::toRoute(['office/edit-working/', 'id' => $model->office_id])) ?>
    </div>
</div>
<?= Html::hiddenInput('back', $back) ?>
<?= Html::hiddenInput('generateDateFrom', '') ?>
<?= Html::hiddenInput('generateDateTo', '') ?>
<?php ActiveForm::end(); ?>
