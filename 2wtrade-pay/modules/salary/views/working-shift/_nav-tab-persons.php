<?php

use app\components\grid\GridView;
use app\components\widgets\ActiveForm;
use app\helpers\DataProvider;
use app\modules\salary\models\Designation;
use app\widgets\LinkPager;
use app\widgets\Panel;
use yii\helpers\Url;
use app\widgets\custom\Checkbox;
use yii\web\View;


/** @var yii\data\ActiveDataProvider $dataPersonProvider */
/** @var app\modules\salary\models\search\PersonSearch $personSearch */
/** @var yii\data\ActiveDataProvider $dataWorkingShiftProvider */

$this->registerJs("var I18n = {};", View::POS_HEAD);
$this->registerJs("I18n['Ничего не сделали.'] = '" . Yii::t('common', 'Ничего не сделали.') . "';", View::POS_HEAD);
$this->registerJs("I18n['Операция выполнена успешно'] = '" . Yii::t('common', 'Операция выполнена успешно') . "';", View::POS_HEAD);
$this->registerJs("I18n['Не удалось установить рабочую смену'] = '" . Yii::t('common', 'Не удалось установить рабочую смену') . "';", View::POS_HEAD);
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute('index'),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>
<div class="row">
    <div class="col-lg-3">
        <?= $form->field($personSearch, 'office_id')->select2List($offices) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($personSearch, 'designation_id')->select2List(Designation::find()
            ->collection(), ['prompt' => '-']) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($personSearch, 'activeSearch')->select2List($personSearch->types_active) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($personSearch, 'approvedSearch')->select2List($personSearch->types_approved) ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')); ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>

<?php
$columns = [
    [
        'attribute' => 'CallCenter',
        'label' => Yii::t('common', 'Направление'),
        'content' => function ($model) {
            if ($model->callCenterUsers) {
                $r = [];
                foreach ($model->callCenterUsers as $callCenterUser) {
                    if (isset($callCenterUser->callCenter->country->name)) {
                        $r[$callCenterUser->callCenter->country->name] = Yii::t('common', $callCenterUser->callCenter->country->name);
                    }
                }
                return implode(", ", $r);
            }
            return '';
        },
    ],
    [
        'attribute' => 'name',
        'contentOptions' => ['class' => 'person'],
    ],
    [
        'attribute' => 'designation_id',
        'content' => function ($model) {
            return $model->designation_id && isset($model->designation->name) ? Yii::t('common', $model->designation->name) : '-';
        },
    ],
];

foreach ($dataWorkingShiftProvider->query->all() as $workingShift) {

    $columns[] = [
        'label' => $workingShift->name,
        'headerOptions' => ['class' => 'working_shift_' . $workingShift->id],
        'contentOptions' => ['class' => 'text-center td-custom-checkbox'],
        'content' => function ($model) use ($workingShift) {

            $attributes = [
                'class' => 'person_working_shift person' . $model->id,
                'data-id' => $model->id,
            ];
            if (!Yii::$app->user->can('salary.person.setwork')) {
                $attributes['disabled'] = 'disabled';
            }

            return Checkbox::widget([
                'value' => $workingShift->id,
                'style' => 'checkbox-inline',
                'label' => true,
                'checked' => $model->working_shift_id == $workingShift->id,
                'attributes' => $attributes
            ]);
        },
        'enableSorting' => false,
    ];
}

$columns[] = [
    'label' => Yii::t('common', 'Действия'),
]

?>
<?= Panel::widget([
    'title' => Yii::t('common', 'Сотрудники'),
    'actions' => DataProvider::renderSummary($dataPersonProvider),
    'withBody' => false,
    'content' =>
        GridView::widget([
            'dataProvider' => $dataPersonProvider,
            'tableOptions' => [
                'id' => 'persons_content',
                'data-url' => Url::toRoute('/salary/person/set-work')
            ],
            'rowOptions' => function ($model) {
                return [
                    'class' => 'tr-vertical-align-middle ' . ($model->active ? '' : 'text-muted'),
                ];
            },
            'columns' => $columns,
        ]),
    'footer' => LinkPager::widget([
        'pagination' => $dataPersonProvider->getPagination(),
        'counterPosition' => LinkPager::COUNTER_POSITION_RIGHT
    ]),
]) ?>

