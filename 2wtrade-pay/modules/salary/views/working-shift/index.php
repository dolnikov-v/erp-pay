<?php
use app\widgets\assets\custom\ModalConfirmDeleteAsset;
use app\widgets\Panel;
use app\modules\salary\models\Office;
use app\widgets\Nav;
use app\modules\salary\assets\WorkingShiftIndexAsset;
use app\modules\salary\widgets\ModalConfirmWorkingShift;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataPersonProvider */
/** @var yii\data\ActiveDataProvider $dataWorkingShiftProvider */
/** @var app\modules\salary\models\search\PersonSearch $personSearch */
/** @var integer $activeTab */
/** @var array $offices */


WorkingShiftIndexAsset::register($this);
ModalConfirmDeleteAsset::register($this);

$this->title = Yii::t('common', 'Рабочие смены');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Зарплата'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

$officeTypes = Office::getTypes();
?>

<?= Panel::widget([
    'nav' => new Nav([
        'tabs' => [
            [
                'label' => Yii::t('common', 'Смены КЦ'),
                'content' => $this->render('_nav-tab-persons', [
                    'personSearch' => $personSearch,
                    'dataPersonProvider' => $dataPersonProvider,
                    'dataWorkingShiftProvider' => $dataWorkingShiftProvider,
                    'offices' => $offices,
                ]),
            ],
            [
                'label' => Yii::t('common', 'Параметры смен'),
                'content' => $this->render('_nav-tab-params', [
                    'personSearch' => $personSearch,
                    'dataWorkingShiftProvider' => $dataWorkingShiftProvider,
                ]),
            ],
        ],
        'activeTab' => $activeTab
    ]),
]) ?>

<?php if (Yii::$app->user->can('salary.person.setwork')): ?>
    <?= ModalConfirmWorkingShift::widget() ?>
<?php endif; ?>