<?php
use app\widgets\Panel;
use yii\helpers\Url;

use app\modules\salary\assets\WorkingShiftEditAsset;
use app\modules\salary\widgets\ModalConfirmWorkingShift;

/** @var yii\web\View $this */
/** @var app\modules\salary\models\WorkingShift $model */
/** @var integer $officeId */
/** @var string $back */

WorkingShiftEditAsset::register($this);

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление рабочей смены') : Yii::t('common', 'Редактирование рабочей смены');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Зарплата'), 'url' => '#'];
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('common', 'Управление офисами'),
    'url' => Url::toRoute('/salary/office/index')
];
$this->params['breadcrumbs'][] = ['label' => $this->title];

?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Рабочая смена'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
        'officeId' => $officeId,
        'back' => $back,
    ])
]) ?>

<?= ModalConfirmWorkingShift::widget([
    'minDate' => strtotime("1day")
]) ?>