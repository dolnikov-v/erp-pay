<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;
use app\modules\salary\assets\BonusAsset;
use app\modules\salary\models\BonusType;
use yii\helpers\ArrayHelper;
use yii\web\View;

/** @var array $bonusTypes */
/** @var array $offices */
/** @var array $persons */
/** @var app\modules\salary\models\Bonus $model */

BonusAsset::register($this);

$this->registerJs("var bonusTypes = {};", View::POS_HEAD);
foreach ($bonusTypes as $type) {
    $this->registerJs("bonusTypes[" . $type->id . "] = {type: '" . $type->type . "', sum: '" . $type->sum . "', sum_local: '" . $type->sum_local . "', percent: '" . $type->percent . "'};", View::POS_HEAD);
}
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['edit', 'id' => $model->id])]); ?>
<div class="row">
    <div class="col-lg-4">
        <div class="form-group">
            <?= $form->field($model, 'office_id')->select2List($offices) ?>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="form-group person_holder">
            <?php if ($model->isNewRecord): ?>
                <?= $form->field($model, 'person_id')
                    ->select2ListMultiple($persons)
                    ->label(Yii::t('common', 'Выберите сотрудников')) ?>
            <?php else: ?>
                <?= $form->field($model, 'person_id')
                    ->select2List($persons, ['prompt' => Yii::t('common', 'Выберите сотрудника')]) ?>
            <?php endif; ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-4">
        <div class="form-group bonus_type_holder">
            <?= $form->field($model, 'bonus_type_id')
                ->select2List(
                    ArrayHelper::map($bonusTypes, 'id', 'name'), [
                    'prompt' => Yii::t('common', 'Выберите тип бонуса')
                ]) ?>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="form-group">
            <?= $form->field($model, 'date')->datePicker() ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-4 limit
 limit_<?= BonusType::BONUS_TYPE_PERCENT ?>
 limit_<?= BonusType::BONUS_TYPE_PERCENT_CALC_SALARY ?>
 limit_<?= BonusType::BONUS_TYPE_OTHER ?>
 limit_<?= BonusType::BONUS_TYPE_13_SALARY ?>
" <?= (!$model->bonus_type_id || (!in_array($model->bonusType->type, [
        BonusType::BONUS_TYPE_OTHER,
        BonusType::BONUS_TYPE_PERCENT,
        BonusType::BONUS_TYPE_13_SALARY,
        BonusType::BONUS_TYPE_PERCENT_CALC_SALARY
    ])) ? 'style="display: none"' : '') ?>>
        <div class="form-group">
            <?= $form->field($model, 'bonus_percent')->textInput() ?>
        </div>
    </div>

    <div class="col-lg-4 limit
    limit_<?= BonusType::BONUS_TYPE_SUM ?>
    limit_<?= BonusType::BONUS_TYPE_SUM_WORK_HOURS ?>
    limit_<?= BonusType::BONUS_TYPE_13_SALARY ?>
    limit_<?= BonusType::BONUS_TYPE_OTHER ?>"
        <?= ((!$model->bonus_type_id || (!in_array($model->bonusType->type, [
                BonusType::BONUS_TYPE_SUM,
                BonusType::BONUS_TYPE_SUM_WORK_HOURS,
                BonusType::BONUS_TYPE_13_SALARY,
                BonusType::BONUS_TYPE_OTHER
            ]))) ? 'style="display: none"' : '') ?>>
        <div class="form-group">
            <?= $form->field($model, 'bonus_sum_usd')->textInput() ?>
        </div>
    </div>

    <div class="col-lg-4 limit
        limit_<?= BonusType::BONUS_TYPE_SUM ?>
        limit_<?= BonusType::BONUS_TYPE_SUM_WORK_HOURS ?>
        limit_<?= BonusType::BONUS_TYPE_13_SALARY ?>
        limit_<?= BonusType::BONUS_TYPE_OTHER ?>"
        <?= ((!$model->bonus_type_id || (!in_array($model->bonusType->type, [
                BonusType::BONUS_TYPE_SUM,
                BonusType::BONUS_TYPE_SUM_WORK_HOURS,
                BonusType::BONUS_TYPE_13_SALARY,
                BonusType::BONUS_TYPE_13_SALARY,
            ]))) ? 'style="display: none"' : '') ?>>
        <div class="form-group">
            <?= $form->field($model, 'bonus_sum_local')->textInput() ?>
        </div>
    </div>

    <div class="col-lg-4 limit limit_<?= BonusType::BONUS_TYPE_HOUR ?>
 limit_<?= BonusType::BONUS_TYPE_OTHER ?>" <?= ((!$model->bonus_type_id || $model->bonusType->type != BonusType::BONUS_TYPE_HOUR) ? 'style="display: none"' : '') ?>>
        <div class="form-group">
            <?= $form->field($model, 'bonus_hour')->textInput() ?>
        </div>
    </div>

    <div class="col-lg-4 limit limit_<?= BonusType::BONUS_TYPE_PERCENT_WORK_HOURS ?>
 limit_<?= BonusType::BONUS_TYPE_OTHER ?>" <?= ((!$model->bonus_type_id || $model->bonusType->type != BonusType::BONUS_TYPE_PERCENT_WORK_HOURS) ? 'style="display: none"' : '') ?>>
        <div class="form-group">
            <?= $form->field($model, 'bonus_percent_hour')->textInput() ?>
        </div>
    </div>

</div>
<div class="row">
    <div class="col-lg-12">
        <div class="form-group">
            <?= $form->field($model, 'comment')->textInput() ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить бонус') : Yii::t('common', 'Сохранить бонус')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
