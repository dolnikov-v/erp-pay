<?php
use app\widgets\Panel;
use yii\helpers\Url;
use app\widgets\Nav;

/** @var yii\web\View $this */
/** @var array $bonusTypes */
/** @var array $offices */
/** @var array $persons */
/** @var app\modules\salary\models\Bonus $model */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление бонуса') : Yii::t('common', 'Редактирование бонуса');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Зарплата'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Управление бонусами'), 'url' => Url::toRoute('/salary/bonus/index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'nav' => new Nav([
        'tabs' => [
            [
                'label' => Yii::t('common', 'Бонус'),
                'content' => $this->render('_edit-form', [
                    'model' => $model,
                    'bonusTypes' => $bonusTypes,
                    'persons' => $persons,
                    'offices' => $offices,
                ]),
            ],
        ]
    ])
]) ?>

