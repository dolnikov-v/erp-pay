<?php
use app\components\grid\ActionColumn;
use app\components\grid\DateColumn;
use app\components\grid\GridView;
use app\helpers\DataProvider;
use app\widgets\assets\custom\ModalConfirmDeleteAsset;
use app\widgets\ButtonLink;
use app\widgets\custom\ModalConfirmDelete;
use app\widgets\Panel;
use yii\helpers\Url;
use app\widgets\LinkPager;
use yii\web\View;
use app\widgets\Nav;

use app\components\grid\CustomCheckboxColumn;
use app\widgets\custom\Checkbox;

use app\modules\salary\assets\BonusIndexAsset;
use app\modules\salary\widgets\DeleteBonuses as DeleteBonusesWidget;
use app\modules\salary\assets\DeleteBonusesAsset;

/** @var yii\web\View $this */
/** @var app\modules\salary\models\search\BonusSearch $modelSearch */
/** @var yii\data\ActiveDataProvider $dataProvider */

ModalConfirmDeleteAsset::register($this);

BonusIndexAsset::register($this);
DeleteBonusesAsset::register($this);

$this->registerJs("var I18n = {};", View::POS_HEAD);
$this->registerJs("I18n['Необходимо выбрать бонусы'] = '" . Yii::t('common', 'Необходимо выбрать бонусы') . "';", View::POS_HEAD);
$this->registerJs("I18n['Ничего не сделали.'] = '" . Yii::t('common', 'Ничего не сделали.') . "';", View::POS_HEAD);
$this->registerJs("I18n['Нет прав для выполнения данной операции'] = '" . Yii::t('common', 'Нет прав для выполнения данной операции') . "';", View::POS_HEAD);
$this->registerJs("I18n['Операция выполнена успешно'] = '" . Yii::t('common', 'Операция выполнена успешно') . "';", View::POS_HEAD);
$this->registerJs("I18n['Не удалось удалить бонусы'] = '" . Yii::t('common', 'Не удалось удалить бонусы') . "';", View::POS_HEAD);

$this->title = Yii::t('common', 'Бонусы');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Зарплата'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

?>


<?= Panel::widget([
    'nav' => new Nav([
        'tabs' => [
            [
                'label' => Yii::t('common', 'Фильтр поиска'),
                'content' => $this->render('_nav-tab-filter', [
                    'modelSearch' => $modelSearch
                ]),
            ],
            [
                'label' => Yii::t('common', 'Групповые операции'),
                'content' => $this->render('_nav-tab-operations'),
            ],
        ]
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с бонусами'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => (Yii::$app->user->can('salary.bonus.edit') ?
            '<div class="add-wrap">' . ButtonLink::widget([
                'label' => Yii::t('common', 'Добавить бонус'),
                'url' => Url::toRoute('/salary/bonus/edit'),
                'style' => ButtonLink::STYLE_SUCCESS,
                'size' => ButtonLink::SIZE_SMALL,
            ]) . '</div>' : '') . GridView::widget([
            'dataProvider' => $dataProvider,
            'tableOptions' => [
                'id' => 'bonuses_content',
            ],
            'columns' => [
                [
                    'class' => CustomCheckboxColumn::className(),
                    'content' => function ($model) {
                        return Checkbox::widget([
                            'name' => 'id[]',
                            'value' => $model->id,
                            'style' => 'checkbox-inline',
                            'label' => true,
                        ]);
                    },
                ],
                [
                    'label' => Yii::t('common', 'Офис'),
                    'content' => function ($model) {
                        return $model->person->office->name;
                    }
                ],
                [
                    'attribute' => 'person_id',
                    'content' => function ($model) {
                        return $model->person->name;
                    }
                ],
                [
                    'attribute' => 'bonus_type_id',
                    'content' => function ($model) {
                        if (!empty($model->bonus_type_id)) {
                            return $model->bonusType->name;
                        }
                        return '';
                    }
                ],
                [
                    'attribute' => 'date',
                    'headerOptions' => ['class' => 'text-center'],
                    'contentOptions' => ['class' => 'text-center'],
                    'content' => function ($model) {
                        return Yii::$app->formatter->asDate($model->date);
                    },
                ],
                [
                    'attribute' => 'bonus_percent',
                    'label' => Yii::t('common', 'Процент'),
                    'headerOptions' => ['class' => 'text-right'],
                    'contentOptions' => ['class' => 'text-right'],
                    'format' => ['decimal', 2],
                ],
                [
                    'attribute' => 'bonus_percent_hour',
                    'label' => Yii::t('common', 'Процент часа'),
                    'headerOptions' => ['class' => 'text-right'],
                    'contentOptions' => ['class' => 'text-right'],
                    'format' => ['decimal', 2],
                ],
                [
                    'attribute' => 'bonus_hour',
                    'label' => Yii::t('common', 'Час'),
                    'headerOptions' => ['class' => 'text-right'],
                    'contentOptions' => ['class' => 'text-right'],
                    'format' => ['decimal', 2],
                ],
                [
                    'attribute' => 'bonus_sum_local',
                    'label' => Yii::t('common', 'Сумма в местной валюте'),
                    'headerOptions' => ['class' => 'text-right'],
                    'contentOptions' => ['class' => 'text-right'],
                    'format' => ['decimal', 2],
                ],
                [
                    'attribute' => 'comment',
                ],
                [
                    'class' => DateColumn::className(),
                    'attribute' => 'updated_at',
                ],
                [
                    'class' => DateColumn::className(),
                    'attribute' => 'created_at',
                ],
                [
                    'class' => ActionColumn::className(),
                    'items' => [
                        [
                            'label' => Yii::t('common', 'Редактировать'),
                            'url' => function ($model) {
                                return Url::toRoute(['/salary/bonus/edit', 'id' => $model->id]);
                            },
                            'can' => function () {
                                return Yii::$app->user->can('salary.bonus.edit');
                            }
                        ],
                        [
                            'can' => function () {
                                return Yii::$app->user->can('salary.control.edit');
                            }
                        ],
                        [
                            'label' => Yii::t('common', 'Удалить'),
                            'url' => function () {
                                return '#';
                            },
                            'style' => 'confirm-delete-link',
                            'attributes' => function ($model) {
                                return [
                                    'data-href' => Url::toRoute(['/salary/bonus/delete', 'id' => $model->id]),
                                ];
                            },
                            'can' => function () {
                                return Yii::$app->user->can('salary.bonus.delete');
                            }
                        ],
                    ]
                ]
            ],
        ]),
    'footer' => (Yii::$app->user->can('salary.bonus.edit') ?
            ButtonLink::widget([
                'label' => Yii::t('common', 'Добавить бонус'),
                'url' => Url::toRoute('/salary/bonus/edit'),
                'style' => ButtonLink::STYLE_SUCCESS,
                'size' => ButtonLink::SIZE_SMALL,
            ]) : '') . LinkPager::widget([
            'pagination' => $dataProvider->getPagination(),
            'counterPosition' => LinkPager::COUNTER_POSITION_RIGHT
        ]),
]) ?>

<?= ModalConfirmDelete::widget() ?>

<?php if (Yii::$app->user->can('salary.bonus.deletebonuses')): ?>
    <?= DeleteBonusesWidget::widget([
        'url' => Url::toRoute('delete-bonuses'),
    ]); ?>
<?php endif; ?>