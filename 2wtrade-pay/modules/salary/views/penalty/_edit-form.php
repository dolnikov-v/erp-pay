<?php
use app\components\widgets\ActiveForm;
use app\modules\salary\assets\PenaltyAsset;
use app\modules\salary\models\PenaltyType;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/** @var array $penaltyTypes */
/** @var array $offices */
/** @var array $persons */
/** @var app\modules\salary\models\Penalty $model */

PenaltyAsset::register($this);

$this->registerJs("var penaltyTypes = {};", View::POS_HEAD);
foreach ($penaltyTypes as $type) {
    $this->registerJs("penaltyTypes[" . $type->id . "] = {type: '" . $type->type . "', sum: '" . $type->sum . "', percent: '" . $type->percent . "'};", View::POS_HEAD);
}
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['edit', 'id' => $model->id])]); ?>
<div class="row">
    <div class="col-lg-4">
        <div class="form-group office_holder <?php echo $model->order_id ? 'hidden' : '' ?>">
            <?= $form->field($model, 'office_id')->select2List($offices) ?>
        </div>
        <div class="form-group office_holder_clone <?php echo !$model->order_id ? 'hidden' : '' ?>">
            <?= $form->field($model, 'office_id', ['enableClientValidation' => false])->select2List($offices, ['name' => '', 'id' => Html::getInputId($model, 'office_id') . '_clone', 'disabled' => true]) ?>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="form-group person_holder <?php echo $model->order_id ? 'hidden' : '' ?>">
            <?php if ($model->isNewRecord): ?>
                <?= $form->field($model, 'person_id')->select2ListMultiple($persons)->label(Yii::t('common', 'Выберите сотрудников')) ?>
            <?php else: ?>
                <?= $form->field($model, 'person_id')->select2List($persons, ['prompt' => Yii::t('common', 'Выберите сотрудника')]) ?>
            <?php endif; ?>
        </div>
        <div class="form-group person_holder_clone <?php echo !$model->order_id ? 'hidden' : '' ?>">
            <?php if ($model->isNewRecord): ?>
                <?= $form->field($model, 'person_id', ['enableClientValidation' => false])->select2ListMultiple($persons, ['name' => '', 'id' => Html::getInputId($model, 'person_id') . '_clone', 'disabled' => true])->label(Yii::t('common', 'Выберите сотрудников')) ?>
            <?php else: ?>
                <?= $form->field($model, 'person_id', ['enableClientValidation' => false])->select2List($persons, ['prompt' => Yii::t('common', 'Выберите сотрудника'), 'name' => '', 'id' => Html::getInputId($model, 'person_id') . '_clone', 'disabled' => true]) ?>
            <?php endif; ?>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="form-group">
            <?= $form->field($model, 'order_id')->textInput(['placeholder' => Yii::t('common', 'Введите номер заказа')])->label(Yii::t('common', 'Поиск сотрудника по номеру заказа')) ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-4">
        <div class="form-group">
            <?= $form->field($model, 'penalty_type_id')
                ->select2List(
                    ArrayHelper::map($penaltyTypes, 'id', 'name'), [
                    'prompt' => Yii::t('common', 'Выберите тип штрафа')]) ?>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="form-group">
            <?= $form->field($model, 'date')->datePicker() ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-4 limit limit_<?= PenaltyType::PENALTY_TYPE_PERCENT ?> limit_<?= PenaltyType::PENALTY_TYPE_OTHER ?>" <?= (($model->penalty_type_id && $model->penaltyType->type != PenaltyType::PENALTY_TYPE_PERCENT) ? 'style="display: none"' : '') ?>>
        <div class="form-group">
            <?= $form->field($model, 'penalty_percent')->textInput() ?>
        </div>
    </div>
    <div class="col-lg-4 limit limit_<?= PenaltyType::PENALTY_TYPE_SUM ?> limit_<?= PenaltyType::PENALTY_TYPE_OTHER ?>" <?= (($model->penalty_type_id && $model->penaltyType->type != PenaltyType::PENALTY_TYPE_SUM) ? 'style="display: none"' : '') ?>>
        <div class="form-group">
            <?= $form->field($model, 'penalty_sum_usd')->textInput() ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="form-group">
            <?= $form->field($model, 'comment')->textInput() ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить штраф') : Yii::t('common', 'Сохранить штраф')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
