<?php
use app\components\grid\ActionColumn;
use app\components\grid\DateColumn;
use app\components\grid\GridView;
use app\helpers\DataProvider;
use app\modules\order\models\search\filters\NumberFilter;
use app\widgets\assets\custom\ModalConfirmDeleteAsset;
use app\widgets\ButtonLink;
use app\widgets\custom\ModalConfirmDelete;
use app\widgets\Panel;
use yii\helpers\Html;
use yii\helpers\Url;
use app\widgets\LinkPager;
use yii\web\View;
use app\widgets\Nav;

use app\components\grid\CustomCheckboxColumn;
use app\widgets\custom\Checkbox;

use app\modules\salary\assets\PenaltyIndexAsset;
use app\modules\salary\widgets\DeletePenalties as DeletePenaltiesWidget;
use app\modules\salary\assets\DeletePenaltiesAsset;

/** @var yii\web\View $this */
/** @var app\modules\salary\models\search\PenaltySearch $modelSearch */
/** @var yii\data\ActiveDataProvider $dataProvider */

ModalConfirmDeleteAsset::register($this);

PenaltyIndexAsset::register($this);
DeletePenaltiesAsset::register($this);

$this->registerJs("var I18n = {};", View::POS_HEAD);
$this->registerJs("I18n['Необходимо выбрать штрафы'] = '" . Yii::t('common', 'Необходимо выбрать штрафы') . "';", View::POS_HEAD);
$this->registerJs("I18n['Ничего не сделали.'] = '" . Yii::t('common', 'Ничего не сделали.') . "';", View::POS_HEAD);
$this->registerJs("I18n['Нет прав для выполнения данной операции'] = '" . Yii::t('common', 'Нет прав для выполнения данной операции') . "';", View::POS_HEAD);
$this->registerJs("I18n['Операция выполнена успешно'] = '" . Yii::t('common', 'Операция выполнена успешно') . "';", View::POS_HEAD);
$this->registerJs("I18n['Не удалось удалить штрафы'] = '" . Yii::t('common', 'Не удалось удалить штрафы') . "';", View::POS_HEAD);

$this->title = Yii::t('common', 'Штрафы');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Зарплата'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

?>


<?= Panel::widget([
    'nav' => new Nav([
        'tabs' => [
            [
                'label' => Yii::t('common', 'Фильтр поиска'),
                'content' => $this->render('_nav-tab-filter', [
                    'modelSearch' => $modelSearch
                ]),
            ],
            [
                'label' => Yii::t('common', 'Групповые операции'),
                'content' => $this->render('_nav-tab-operations'),
            ],
        ]
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с штрафами'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => [
            'id' => 'penalties_content',
        ],
        'columns' => [
            [
                'class' => CustomCheckboxColumn::className(),
                'content' => function ($model) {
                    return Checkbox::widget([
                        'name' => 'id[]',
                        'value' => $model->id,
                        'style' => 'checkbox-inline',
                        'label' => true,
                    ]);
                },
            ],
            [
                'label' => Yii::t('common', 'Офис'),
                'content' => function ($model) {
                    return $model->person->office->name;
                }
            ],
            [
                'attribute' => 'person_id',
                'content' => function ($model) {
                    return $model->person->name;
                }
            ],
            [
                'attribute' => 'order_id',
                'format' => 'raw',
                'content' => function ($model) {
                    if ($model->order_id) {
                        return Html::a($model->order_id, Url::toRoute([
                            '/order/index/index',
                            'NumberFilter' => [
                                [
                                    'entity' => NumberFilter::ENTITY_ID,
                                    'number' => $model->order_id,
                                ]
                            ],
                            'force_country_slug' => $model->order->country->slug,
                        ]));
                    } else {
                        return '—';
                    }
                }
            ],
            [
                'attribute' => 'penalty_type_id',
                'content' => function ($model) {
                    if (!empty($model->penalty_type_id)) {
                        return $model->penaltyType->name;
                    }
                    return '';
                }
            ],
            [
                'attribute' => 'date',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return Yii::$app->formatter->asDate($model->date);
                },
            ],
            [
                'attribute' => 'penalty_sum_usd',
                'headerOptions' => ['class' => 'text-right'],
                'contentOptions' => ['class' => 'text-right'],
                'format' => ['decimal', 2],
            ],
            [
                'attribute' => 'penalty_sum_local',
                'headerOptions' => ['class' => 'text-right'],
                'contentOptions' => ['class' => 'text-right'],
                'format' => ['decimal', 2],
            ],
            [
                'attribute' => 'comment',
            ],
            [
                'attribute' => 'created_by',
                'label' => Yii::t('common', 'Кто назначил штраф'),
                'content' => function ($model) {
                    return $model->created_by ? $model->createdBy->username : '-';
                },
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'updated_at',
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'created_at',
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/salary/penalty/edit', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('salary.penalty.edit');
                        }
                    ],
                    [
                        'can' => function () {
                            return Yii::$app->user->can('salary.control.edit');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить'),
                        'url' => function () {
                            return '#';
                        },
                        'style' => 'confirm-delete-link',
                        'attributes' => function ($model) {
                            return [
                                'data-href' => Url::toRoute(['/salary/penalty/delete', 'id' => $model->id]),
                            ];
                        },
                        'can' => function () {
                            return Yii::$app->user->can('salary.penalty.delete');
                        }
                    ],
                ]
            ]
        ],
    ]),
    'footer' => (Yii::$app->user->can('salary.penalty.edit') ?
            ButtonLink::widget([
                'label' => Yii::t('common', 'Добавить штраф'),
                'url' => Url::toRoute('/salary/penalty/edit'),
                'style' => ButtonLink::STYLE_SUCCESS,
                'size' => ButtonLink::SIZE_SMALL,
            ]) : '') . LinkPager::widget([
            'pagination' => $dataProvider->getPagination(),
            'counterPosition' => LinkPager::COUNTER_POSITION_RIGHT
        ]),
]) ?>

<?= ModalConfirmDelete::widget() ?>

<?php if (Yii::$app->user->can('salary.penalty.deletepenalties')): ?>
    <?= DeletePenaltiesWidget::widget([
        'url' => Url::toRoute('delete-penalties'),
    ]); ?>
<?php endif; ?>