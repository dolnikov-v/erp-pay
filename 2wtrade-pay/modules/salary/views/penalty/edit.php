<?php
use app\widgets\Panel;
use yii\helpers\Url;
use app\widgets\Nav;

/** @var yii\web\View $this */
/** @var array $penaltyTypes */
/** @var array $offices */
/** @var array $persons */
/** @var app\modules\salary\models\Penalty $model */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление штрафа') : Yii::t('common', 'Редактирование штрафа');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Зарплата'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Управление штрафами'), 'url' => Url::toRoute('/salary/penalty/index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'nav' => new Nav([
        'tabs' => [
            [
                'label' => Yii::t('common', 'Штраф'),
                'content' => $this->render('_edit-form', [
                    'model' => $model,
                    'penaltyTypes' => $penaltyTypes,
                    'persons' => $persons,
                    'offices' => $offices,
                ]),
            ],
        ]
    ])
]) ?>

