<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;
use app\modules\salary\models\Office;

/** @var app\modules\salary\models\search\PenaltySearch $modelSearch */
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute('index'),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>
    <div class="row">
        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'office_id')->select2List(Office::find()->bySystemUserCountries()->collection(), ['prompt' => '-']) ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'person_id')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->submit(Yii::t('common', 'Применить')); ?>
            <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')); ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>