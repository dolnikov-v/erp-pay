<?php
use app\widgets\Panel;
use yii\helpers\Url;
use app\widgets\Nav;
use app\components\grid\GridView;
use app\helpers\DataProvider;
use app\components\grid\ActionColumn;
use app\widgets\ButtonLink;
use app\widgets\assets\custom\ModalConfirmDeleteAsset;
use app\widgets\custom\ModalConfirmDelete;
use app\components\grid\DateColumn;
use app\components\grid\CustomCheckboxColumn;
use app\widgets\custom\Checkbox;
use yii\web\View;
use yii\helpers\Html;

use app\modules\salary\widgets\DeletePersons as DeletePersonsWidget;

use app\modules\salary\assets\MonthlyStatementEditAsset;
use app\modules\salary\assets\MonthlyStatementDataDeleteAsset;

/** @var yii\web\View $this */
/** @var app\modules\salary\models\MonthlyStatement $model */
/** @var app\modules\salary\models\search\MonthlyStatementDataSearch $modelDataSearch */
/** @var yii\data\ActiveDataProvider $dataProvider */

ModalConfirmDeleteAsset::register($this);
MonthlyStatementEditAsset::register($this);
MonthlyStatementDataDeleteAsset::register($this);

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление графика') : Yii::t('common', 'Редактирование графика');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Зарплата'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Управление графиками'), 'url' => Url::toRoute('/salary/monthly-statement/index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];

$this->registerJs("var I18n = {};", View::POS_HEAD);
$this->registerJs("I18n['Необходимо выбрать сотрудников.'] = '" . Yii::t('common', 'Необходимо выбрать сотрудников.') . "';", View::POS_HEAD);
$this->registerJs("I18n['Не удалось удалить сотрудников.'] = '" . Yii::t('common', 'Не удалось удалить сотрудников.') . "';", View::POS_HEAD);
$this->registerJs("I18n['Да'] = '" . Yii::t('common', 'Да') . "';", View::POS_HEAD);
$this->registerJs("I18n['Нет'] = '" . Yii::t('common', 'Нет') . "';", View::POS_HEAD);


$tabs = [
    [
        'label' => Yii::t('common', 'График'),
        'content' => $this->render('_edit-form', [
            'model' => $model,
        ]),
    ]
];

if ($model->id) {
    $tabs[] = [
        'label' => Yii::t('common', 'Фильтр поиска'),
        'content' => $this->render('_nav-tab-filter', [
            'model' => $model,
            'modelDataSearch' => $modelDataSearch
        ]),

    ];
    $tabs[] = [
        'label' => Yii::t('common', 'Групповые операции'),
        'content' => $this->render('_nav-tab-operations', [
            'model' => $model
        ]),
    ];
}

$alert = null;
if (isset($modelDataSearch) && ($names = $modelDataSearch->getNotFoundNames())) {
    $alert = '<b>' . Yii::t('common', 'Не удалось найти имена') . ':' . '</b><br>' . join('<br>', $names);
}
?>

<?= Panel::widget([
    'nav' => new Nav([
        'tabs' => $tabs,
        'activeTab' => $model->id &&
        ($modelDataSearch->name || $modelDataSearch->country_names || $modelDataSearch->parent_name || $modelDataSearch->designation || $modelDataSearch->call_center_user_logins)
            ? 1 : 0
    ])
]) ?>

<?php if ($model->id): ?>
    <?= Panel::widget([
        'title' => Yii::t('common', 'Данные графика'),
        'actions' => DataProvider::renderSummary($dataProvider),
        'withBody' => false,
        'alert' => $alert,
        'alertStyle' => Panel::ALERT_WARNING,
        'content' => '<div class="add-wrap">' .
            (Yii::$app->user->can('salary.monthlystatementdata.edit') ?
                ButtonLink::widget([
                    'label' => Yii::t('common', 'Добавить сотрудника в график'),
                    'url' => Url::toRoute(['/salary/monthly-statement-data/edit', 'statement_id' => $model->id]),
                    'style' => ButtonLink::STYLE_SUCCESS,
                    'size' => ButtonLink::SIZE_SMALL,
                ]) : '') .
            '</div>' . GridView::widget([
                'dataProvider' => $dataProvider,
                'tableOptions' => [
                    'id' => 'data_content',
                ],
                'columns' => [
                    [
                        'class' => CustomCheckboxColumn::className(),
                        'content' => function ($person) {
                            return Checkbox::widget([
                                'name' => 'data_id[]',
                                'value' => $person->id,
                                'style' => 'checkbox-inline',
                                'label' => true,
                            ]);
                        },
                    ],
                    [
                        'class' => 'yii\grid\SerialColumn',
                    ],
                    [
                        'attribute' => 'parent_name'
                    ],
                    [
                        'attribute' => 'country_names',
                    ],
                    [
                        'attribute' => 'name',
                    ],
                    [
                        'attribute' => 'designation',
                    ],
                    [
                        'attribute' => 'call_center_user_logins',
                    ],
                    [
                        'attribute' => 'salary_local',
                        'format' => ['decimal', 2],
                    ],
                    [
                        'class' => DateColumn::className(),
                        'attribute' => 'updated_at',
                    ],
                    [
                        'class' => DateColumn::className(),
                        'attribute' => 'created_at',
                    ],
                    [
                        'class' => ActionColumn::className(),
                        'items' => [
                            [
                                'label' => Yii::t('common', 'Редактировать'),
                                'url' => function ($person) use ($model) {
                                    return Url::toRoute(['/salary/monthly-statement-data/edit', 'id' => $person->id, 'statement_id' => $model->id]);
                                },
                                'can' => function () {
                                    return Yii::$app->user->can('salary.monthlystatementdata.edit');
                                }
                            ],
                            [
                                'label' => Yii::t('common', 'Удалить'),
                                'url' => function () {
                                    return '#';
                                },
                                'style' => 'confirm-delete-link',
                                'attributes' => function ($person) use ($model) {
                                    return [
                                        'data-href' => Url::toRoute(['/salary/monthly-statement-data/delete', 'id' => $person->id, 'statement_id' => $model->id]),
                                    ];
                                },
                                'can' => function () {
                                    return Yii::$app->user->can('salary.monthlystatementdata.delete');
                                }
                            ],
                        ]
                    ]
                ],
            ]),
        'footer' => (Yii::$app->user->can('salary.monthlystatementdata.edit') ?
            ButtonLink::widget([
                'label' => Yii::t('common', 'Добавить сотрудника в график'),
                'url' => Url::toRoute(['/salary/monthly-statement-data/edit', 'statement_id' => $model->id]),
                'style' => ButtonLink::STYLE_SUCCESS,
                'size' => ButtonLink::SIZE_SMALL,
            ])
            : ''),
    ]) ?>

    <?= ModalConfirmDelete::widget() ?>

    <?php if (Yii::$app->user->can('salary.monthlystatement.deletepersons')): ?>
        <?= DeletePersonsWidget::widget([
            'url' => Url::toRoute('delete-persons'),
        ]); ?>
        <?= Html::hiddenInput('statement_id', $model->id, ['id' => 'statement_id']) ?>
    <?php endif; ?>

<?php endif; ?>