<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;
use app\modules\salary\models\Office;

/** @var array $offices */
/** @var app\modules\salary\models\MonthlyStatement $model */

$offices = Office::find()->bySystemUserCountries()->byId($model->office_id)->collection();

?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['edit', 'id' => $model->id])]); ?>
<div class="row">
    <div class="col-lg-3">
        <?= $form->field($model, 'office_id')->select2List($offices, (($model->isNewRecord) ? ['prompt' => '-'] : [])) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'date_from')->datePicker() ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'date_to')->datePicker() ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'active')->checkboxCustom([
            'value' => 1,
            'checked' => $model->active,
            'uncheckedValue' => 0
        ]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить график') : Yii::t('common', 'Сохранить график')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
