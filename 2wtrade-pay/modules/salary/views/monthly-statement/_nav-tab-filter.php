<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/** @var app\modules\salary\models\search\MonthlyStatementDataSearch $modelDataSearch */
/** @var app\modules\salary\models\MonthlyStatement $model */
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute('monthly-statement/edit/' . $model->id),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>
    <div class="row">
        <div class="col-lg-3">
            <?= $form->field($modelDataSearch, 'name')->textarea() ?>
        </div>
        <div class="col-lg-2">
            <?= $form->field($modelDataSearch, 'parent_name')->textInput() ?>
        </div>
        <div class="col-lg-2">
            <?= $form->field($modelDataSearch, 'country_names')->textInput() ?>
        </div>
        <div class="col-lg-2">
            <?= $form->field($modelDataSearch, 'designation')->textInput() ?>
        </div>
        <div class="col-lg-2">
            <?= $form->field($modelDataSearch, 'call_center_user_logins')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3">
            <?= $form->field($modelDataSearch, 'use_strict_name')->checkboxCustom([
                'checked' => $modelDataSearch->use_strict_name,
            ]) ?>
            <br>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->submit(Yii::t('common', 'Применить')); ?>
            <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('monthly-statement/edit/' . $model->id)); ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>