<?php
use app\components\grid\ActionColumn;
use app\components\grid\DateColumn;
use app\components\grid\GridView;
use app\helpers\DataProvider;
use app\widgets\assets\custom\ModalConfirmDeleteAsset;
use app\widgets\ButtonLink;
use app\widgets\custom\ModalConfirmDelete;
use app\widgets\Panel;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use app\widgets\Label;

/** @var yii\web\View $this */
/** @var app\modules\salary\models\MonthlyStatement $model */
/** @var yii\data\ActiveDataProvider $dataProvider */

ModalConfirmDeleteAsset::register($this);

$this->title = Yii::t('common', 'Графики');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Зарплата'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с графиками'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function ($model) {
            return [
                'class' => 'tr-vertical-align-middle ' . ($model->active ? '' : 'text-muted'),
            ];
        },
        'columns' => [
            [
                'attribute' => 'office_id',
                'content' => function ($model) {
                    return $model->office_id ? $model->office->name : '-';
                },
            ],
            [
                'attribute' => 'date_from',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return Yii::$app->formatter->asDate($model->date_from);
                },
            ],
            [
                'attribute' => 'date_to',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return Yii::$app->formatter->asDate($model->date_to);
                },
            ],
            [
                'attribute' => 'active',
                'headerOptions' => ['class' => 'text-center width-150'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return Label::widget([
                        'label' => $model->active ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->active ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'closed_at',
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'updated_at',
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'created_at',
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Просмотреть расчет'),
                        'url' => function ($model) {
                            return Url::toRoute([
                                '/report/salary/index',
                                's' => [
                                    'office' => [$model->office_id],
                                    'from' => Yii::$app->formatter->asDate($model->date_from),
                                    'to' => Yii::$app->formatter->asDate($model->date_to),
                                    'monthlyStatement' => $model->id
                                ]
                            ]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('report.salary.index');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/salary/monthly-statement/edit', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('salary.monthlystatement.edit') && !$model->closed_at;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Активировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/salary/monthly-statement/activate', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('salary.monthlystatement.activate') && !$model->active && !$model->closed_at;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Деактивировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/salary/monthly-statement/deactivate', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('salary.monthlystatement.deactivate') && $model->active && !$model->closed_at;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить'),
                        'url' => function () {
                            return '#';
                        },
                        'style' => 'confirm-delete-link',
                        'attributes' => function ($model) {
                            return [
                                'data-href' => Url::toRoute(['/salary/monthly-statement/delete', 'id' => $model->id]),
                            ];
                        },
                        'can' => function ($model) {
                            return $model->closed_at ? Yii::$app->user->can('salary.monthlystatement.deleteclosed') : Yii::$app->user->can('salary.monthlystatement.delete');
                        }
                    ],
                ]
            ]
        ],
    ]),
    'footer' => (Yii::$app->user->can('salary.monthlystatement.edit') ?
            ButtonLink::widget([
                'label' => Yii::t('common', 'Добавить график'),
                'url' => Url::toRoute('/salary/monthly-statement/edit'),
                'style' => ButtonLink::STYLE_SUCCESS,
                'size' => ButtonLink::SIZE_SMALL,
            ]) : '') . LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>

<?= ModalConfirmDelete::widget() ?>
