<?php
use app\modules\salary\models\StaffingBonus;
use app\modules\salary\assets\StaffingBonusesAsset;

/** @var app\modules\salary\models\Staffing $model */
/** @var app\modules\salary\models\StaffingBonus[] $staffingBonuses */
/** @var app\components\widgets\ActiveForm $form */

StaffingBonusesAsset::register($this);
?>

<div class="col-lg-12">
    <h4><?= Yii::t('common', 'Ежемесячные бонусы') ?></h4>
</div>


<div class="col-lg-12 margin-bottom-20 group-bonuses-group">
    <?php if (empty($staffingBonuses)): ?>
        <?= $this->render('_edit-form-bonuses', [
            'form' => $form,
            'staffing' => $model,
            'model' => new StaffingBonus(),
        ]) ?>
    <?php else: ?>
        <?php foreach ($staffingBonuses as $key => $staffingBonus): ?>
            <?= $this->render('_edit-form-bonuses', [
                'form' => $form,
                'staffing' => $model,
                'model' => $staffingBonus,
            ]) ?>
        <?php endforeach; ?>
    <?php endif; ?>
</div>

