<?php
use app\widgets\Panel;
use yii\helpers\Url;
use app\modules\salary\assets\StaffingEditAsset;

/** @var yii\web\View $this */
/** @var app\modules\salary\models\Staffing $model */
/** @var integer $officeId */
/** @var string $back */
/** @var array $offices */
/** @var array $designations */
/** @var array $staffings*/
/** @var array $bonusTypes */
/** @var array $staffingBonuses */

StaffingEditAsset::register($this);

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление должности в штатное расписание') : Yii::t('common', 'Редактирование должности в штатном расписании');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Зарплата'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Управление офисами'), 'url' => Url::toRoute('index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];

?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Должность'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
        'officeId' => $officeId,
        'back' => $back,
        'offices' => $offices,
        'designations' => $designations,
        'staffings' => $staffings,
        'bonusTypes' => $bonusTypes,
        'staffingBonuses' => $staffingBonuses,
    ])
]) ?>