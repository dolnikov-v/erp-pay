<?php
use app\helpers\WbIcon;
use app\widgets\Button;
use yii\helpers\Html;
use app\modules\salary\models\BonusType;
use app\modules\salary\models\StaffingBonus;

/** @var yii\web\View $this */
/** @var app\components\widgets\ActiveForm $form */
/** @var app\modules\salary\models\Staffing $staffing */
/** @var app\modules\salary\models\StaffingBonus $model */

$bonusTypes = BonusType::find()->byOfficeId($staffing->office_id)->collection();
$days = StaffingBonus::getDays();

?>

<div class="row row-staffing-bonuses">

    <?= Html::hiddenInput(Html::getInputName($model, 'id') . '[]', $model->id) ?>

    <div class="col-lg-4">
        <?= $form->field($model, 'bonus_type_id')->select2List($bonusTypes,
            [
                'prompt' => '-',
                'id' => false,
                'name' => Html::getInputName($model, 'bonus_type_id') . '[]',
            ]) ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'day')->select2List($days,
            [
                'prompt' => '-',
                'id' => false,
                'name' => Html::getInputName($model, 'day') . '[]',
            ]) ?>
    </div>

    <div class="col-lg-2">
        <div class="form-group">
            <div class="control-label">
                <label>&nbsp;</label>
            </div>
            <?= Button::widget([
                'type' => 'button',
                'icon' => WbIcon::MINUS,
                'style' => Button::STYLE_DANGER . ' btn-staffing-bonuses btn-staffing-bonuses-minus',
            ]) ?>

            <?= Button::widget([
                'type' => 'button',
                'icon' => WbIcon::PLUS,
                'style' => Button::STYLE_SUCCESS . ' btn-staffing-bonuses btn-staffing-bonuses-plus',
            ]) ?>

        </div>
    </div>

</div>


