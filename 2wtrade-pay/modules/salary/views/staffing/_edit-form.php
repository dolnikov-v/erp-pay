<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use app\modules\salary\models\Staffing;

/** @var app\modules\salary\models\Staffing $model */
/** @var integer $officeId */
/** @var string $back */
/** @var array $offices */
/** @var array $designations */
/** @var array $staffings*/
/** @var array $bonusTypes */
/** @var array $staffingBonuses */

?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute(['edit', 'id' => $model->id]),
    'options' => [
        'enctype' => 'multipart/form-data',
        'data-id' => $model->id
    ]
]); ?>
<div class="row">
    <div class="col-lg-3">
        <?= $form->field($model, 'designation_id')->select2List($designations, [
            'prompt' => Yii::t('common', 'Выберите должность'),
        ]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'office_id')->select2List($offices, ['prompt' => '-']) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'max_persons')->textInput() ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'min_jobs')->textInput() ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-3">
        <?= $form->field($model, 'payment_type')->select2List(Staffing::getPaymentTypes()) ?>
    </div>
</div>
<div class="row payment_type_fixed " <?php if ($model->payment_type != Staffing::PAYMENT_TYPE_FIXED): ?> style="display: none" <?php endif; ?>>
    <div class="col-lg-3">
        <?= $form->field($model, 'salary_usd')->textInput() ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'salary_local')->textInput() ?>
    </div>
</div>
<div class="row payment_type_percent " <?php if ($model->payment_type != Staffing::PAYMENT_TYPE_PERCENT): ?> style="display: none" <?php endif; ?>>
    <div class="col-lg-3">
        <?= $form->field($model, 'percent_base')->textInput() ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'base_staffing_id')->select2List($staffings, [
            'prompt' => Yii::t('common', '-'),
        ]) ?>
    </div>
</div>

<div class="row margin-top-40">
    <?= $this->render('_edit-bonuses', [
        'model' => $model,
        'staffingBonuses' => $staffingBonuses,
        'form' => $form,
    ]) ?>
</div>
<div class="row">
    <div class="col-lg-6">
        <?= $form->field($model, 'comment')->textInput() ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить должность') : Yii::t('common', 'Сохранить должность')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'),
            $back == 'staffing' ?
                Url::to([
                    'staffing/index',
                    'StaffingSearch' => [
                        'office_id' => $model->office_id
                    ]
                ])
                :
                Url::toRoute(['office/edit-staffing/', 'id' => $model->office_id])) ?>
    </div>
</div>
<?= Html::hiddenInput('back', $back) ?>
<?php ActiveForm::end(); ?>
