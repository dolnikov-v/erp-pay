<?php
use app\helpers\DataProvider;
use app\modules\salary\assets\StaffingIndexAsset;
use app\widgets\Panel;
use app\widgets\Nav;
use yii\helpers\Url;
use yii\helpers\Html;
use app\widgets\custom\DropdownActions;
use app\widgets\ButtonLink;
use yii\web\View;

use app\modules\salary\widgets\GridViewStaffing;
use app\modules\salary\widgets\ShowTipsWidget;
use app\modules\salary\widgets\ShowJobDescriptionWidget;
use app\modules\salary\models\Staffing;

/** @var yii\web\View $this */
/** @var app\modules\salary\models\search\StaffingSearch $modelSearch */
/** @var yii\data\ActiveDataProvider $dataProvider */
/** @var array $tips */
/** @var integer $sizeOfTips */
/** @var array $offices */
/** @var array $officesCollection */
/** @var array $typesCollection */
/** @var string $alert */

StaffingIndexAsset::register($this);

$this->registerJs("var officesCol = {};", View::POS_HEAD);
foreach ($officesCollection as $type => $list) {
    $this->registerJs("officesCol['" . $type . "'] = " . json_encode($list) . ";", View::POS_HEAD);
}
$this->registerJs("offices = " . json_encode($offices) . ";", View::POS_HEAD);


$this->title = Yii::t('common', 'Штатное расписание');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Зарплата'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

$paymentTypes = Staffing::getPaymentTypes();

?>

<?= Panel::widget([
    'nav' => new Nav([
        'tabs' => [
            [
                'label' => Yii::t('common', 'Фильтр поиска'),
                'content' => $this->render('_nav-tab-filter', [
                    'modelSearch' => $modelSearch,
                    'offices' => $offices,
                    'officesCollection' => $officesCollection,
                    'typesCollection' => $typesCollection,
                ]),
            ],
        ]
    ]),
]) ?>

<?php

$columns = [
    [
        'attribute' => 'office_id',
        'label' => Yii::t('common', 'Офис'),
        'content' => function ($model) {
            return $model['office_id'] ? Yii::t('common', $model['office_name']) : '-';
        },
        'enableSorting' => false,
        'group' => true,
        'pageSummary' => Yii::t('common', 'Итого'),
    ],
    [
        'attribute' => 'designation_id',
        'label' => Yii::t('common', 'Должность'),
        'content' => function ($model) {
            $designation = $model['designation_id'] && isset($model['designation_name']) ? Yii::t('common', $model['designation_name']) : '-';
            if (Yii::$app->user->can('salary.staffing.edit') && $model['staffing_id']) {
                $editLink = Url::toRoute([
                    '/salary/staffing/edit',
                    'id' => $model['staffing_id'],
                    'back' => 'staffing'
                ]);
            }
            if (Yii::$app->user->can('salary.staffing.getjobdescription') && $model['designation_id']) {
                $designation = Html::a($designation, '#', [
                    'class' => 'jobdescription',
                    'data-id' => $model['designation_id']
                ]);
            }

            return $designation;
        },
        'contentOptions' => function ($model) use ($tips) {
            if (isset($tips[$model['office_id']][$model['designation_id']]['staffing_id'])) {
                return [
                    'class' => 'custom-error',
                    'title' => $tips[$model['office_id']][$model['designation_id']]['staffing_id']
                ];
            }
            return [];
        },
        'enableSorting' => false,
    ],
    [
        'label' => Yii::t('common', 'Количество штатных едениц'),
        'headerOptions' => ['class' => 'text-center'],
        'hAlign' => 'center',
        'pageSummary' => true,
        'enableSorting' => false,
        'format' => 'integer',
        'value' => function ($model) {
            return $model['fact'];
        },
        'contentOptions' => function ($model) use ($tips) {
            if (isset($tips[$model['office_id']][$model['designation_id']]['fact'])) {
                return [
                    'class' => 'custom-error',
                    'title' => $tips[$model['office_id']][$model['designation_id']]['fact']
                ];
            }
            return [];
        },
        'content' => function ($model) {
            $people = $model['fact'];
            if (Yii::$app->user->can('salary.person.index')) {
                $people = Html::a($people,
                    Url::to([
                        '/salary/person/index',
                        'PersonSearch' => [
                            'designation_id' => $model['designation_id'],
                            'office_id' => $model['office_id'],
                            'activeSearch' => 1,
                        ],
                    ]), [
                        'target' => '_blank',
                    ]);
            }
            return $people;
        },
    ],
    [
        'attribute' => 'norm',
        'label' => Yii::t('common', 'Норма штатных едениц'),
        'headerOptions' => ['class' => 'text-center'],
        'hAlign' => 'center',
        'pageSummary' => true,
        'enableSorting' => false,
        'format' => 'integer',
    ],
    [
        'attribute' => 'payment_type',
        'label' => Yii::t('common', 'Тип оплаты'),
        'headerOptions' => ['class' => 'text-center'],
        'hAlign' => 'center',
        'pageSummary' => false,
        'enableSorting' => false,
        'content' => function ($model) use ($paymentTypes) {
            $return = '';
            if ($model['payment_type'] == Staffing::PAYMENT_TYPE_PERCENT) {
                $return .= Yii::$app->formatter->asDecimal($model['percent_base']) . ' ';
            }
            $return .= ($paymentTypes[$model['payment_type']] ?? '-');
            return $return;
        }
    ]
];
if (Yii::$app->user->can('view_staffing_salary')) {
    $columns[] = [
        'attribute' => 'salaryusd',
        'label' => Yii::t('common', 'Оклад в USD'),
        'headerOptions' => ['class' => 'text-center'],
        'hAlign' => 'right',
        'enableSorting' => false,
        'pageSummary' => true,
        'format' => ['decimal', 2],
        'value' => function ($model) {
            // Данные в колонках "Оклад" (МСК офис) доступны только для Ген директора
            if ($model['salaryusd'] && $model['hide_staffing_salary'] && !Yii::$app->user->can('view_staffing_salary_moscow')) {
                $model['salaryusd'] = 0;
            }
            return $model['salaryusd'];
        },
        'contentOptions' => function ($model) use ($tips) {
            if (isset($tips[$model['office_id']][$model['designation_id']]['salaryusd'])) {
                return [
                    'class' => 'custom-error',
                    'title' => Yii::t('common', 'сотрудников с несоответствующим окладом ({count})', [
                        'count' => sizeof($tips[$model['office_id']][$model['designation_id']]['salaryusd'])
                    ])
                ];
            }
            return [];
        },
    ];
    $columns[] = [
        'attribute' => 'salarylocal',
        'label' => Yii::t('common', 'Оклад в местной валюте'),
        'headerOptions' => ['class' => 'text-center'],
        'hAlign' => 'right',
        'enableSorting' => false,
        'pageSummary' => false,
        'format' => ['decimal', 2],
        'value' => function ($model) {
            // Данные в колонках "Оклад" (МСК офис) доступны только для Ген директора
            if ($model['salaryusd'] && $model['hide_staffing_salary'] && !Yii::$app->user->can('view_staffing_salary_moscow')) {
                $model['salarylocal'] = 0;
            }
            return $model['salarylocal'];
        },
        'contentOptions' => function ($model) use ($tips) {
            if (isset($tips[$model['office_id']][$model['designation_id']]['salarylocal'])) {
                return [
                    'class' => 'custom-error',
                    'title' => Yii::t('common', 'сотрудников с несоответствующим окладом ({count})', [
                        'count' => sizeof($tips[$model['office_id']][$model['designation_id']]['salarylocal'])
                    ])
                ];
            }
            return [];
        },
    ];
}
$columns[] = [
    'attribute' => 'comment',
    'label' => Yii::t('common', 'Комментарий'),
    'enableSorting' => false,
];
$columns[] = [
    'hAlign' => 'right',
    'content' => function ($model) {
        $items = [];
        if (Yii::$app->user->can('salary.person.index')) {
            $items[] = [
                'label' => Yii::t('common', 'Список сотрудников'),
                'url' => Url::to([
                    '/salary/person/index',
                    'PersonSearch' => [
                        'designation_id' => $model['designation_id'],
                        'office_id' => $model['office_id'],
                        'activeSearch' => 1,
                    ],
                ])
            ];
        }
        if (Yii::$app->user->can('salary.staffing.edit') && $model['staffing_id']) {
            $items[] = [
                'label' => Yii::t('common', 'Редактировать'),
                'url' => Url::toRoute([
                    '/salary/staffing/edit',
                    'id' => $model['staffing_id'],
                    'back' => 'staffing'
                ])
            ];
        }
        if (Yii::$app->user->can('salary.staffing.delete') && $model['staffing_id']) {
            $items[] = [
                'label' => Yii::t('common', 'Удалить'),
                'url' => Url::toRoute([
                    '/salary/staffing/delete',
                    'id' => $model['staffing_id'],
                    'office_id' => $model['office_id'],
                    'back' => 'staffing'
                ]),
            ];
        }
        return DropdownActions::widget([
            'links' => $items
        ]);
    }
];
?>
<?= Panel::widget([
    'title' => Yii::t('common', 'Штатное расписание'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'alert' => $alert,
    'alertStyle' => Panel::ALERT_WARNING,
    'content' =>
        '<div class="add-wrap">' .
        (Yii::$app->user->can('salary.staffing.edit') ?
            ButtonLink::widget([
                'label' => Yii::t('common', 'Добавить должность'),
                'url' => Url::toRoute([
                    '/salary/staffing/edit',
                    'office_id' => $modelSearch->office_id,
                    'back' => 'staffing'
                ]),
                'style' => ButtonLink::STYLE_SUCCESS,
                'size' => ButtonLink::SIZE_SMALL,
            ]) : '') .
        Html::a(Yii::t('common', 'Рекомендации') . ' (' . $sizeOfTips . ')', '#', [
            'class' => 'pull-right',
            'id' => 'btn_show_tips'
        ]) .
        '</div>' .
        GridViewStaffing::widget([
            'dataProvider' => $dataProvider,
            'showPageSummary' => true,
            'bordered' => false,
            'striped' => false,
            'responsive' => false,
            'columns' => $columns,
        ]),
    'footer' => (Yii::$app->user->can('salary.staffing.edit') ?
        ButtonLink::widget([
            'label' => Yii::t('common', 'Добавить должность'),
            'url' => Url::toRoute([
                'edit',
                'office_id' => $modelSearch->office_id,
                'back' => 'staffing'
            ]),
            'style' => ButtonLink::STYLE_SUCCESS,
            'size' => ButtonLink::SIZE_SMALL,
        ]) : ''),
]) ?>

<?= ShowTipsWidget::widget([
    'tips' => $tips,
    'sizeOfTips' => $sizeOfTips
]); ?>

<?php if (Yii::$app->user->can('media.salarydesignation.jobdescription')): ?>
    <?= ShowJobDescriptionWidget::widget(['url' => 'get-job-description']); ?>
<?php endif; ?>
