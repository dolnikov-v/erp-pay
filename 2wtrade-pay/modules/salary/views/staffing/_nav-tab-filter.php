<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;
use app\modules\salary\models\Office;

/** @var app\modules\salary\models\search\StaffingSearch $modelSearch */
/** @var array $offices */
/** @var array $officesCollection */
/** @var array $typesCollection */

?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute('index'),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>
    <div class="row">
        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'type')
                ->select2List($typesCollection, ['prompt' => '-'])
                ->label(Yii::t('common', 'Тип офиса')) ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'office_id')
                ->select2List($modelSearch->type ? ($officesCollection[$modelSearch->type] ?? $offices) : $offices, [
                    'prompt' => '-',
                    'length' => false
                ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->submit(Yii::t('common', 'Применить')); ?>
            <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')); ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>