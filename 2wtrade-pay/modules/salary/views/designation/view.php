<?php

use app\components\widgets\ActiveForm;
use app\widgets\Panel;
use yii\helpers\Url;
use app\widgets\Nav;

/** @var yii\web\View $this */
/** @var app\modules\salary\models\Designation $model */
/** @var array $external_source */
/** @var array $designationExtensionRole */

$this->title = Yii::t('common', 'Просмотр должности');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Зарплата'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Управление должностями'), 'url' => Url::toRoute('/salary/designation/index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute(['edit', 'id' => $model->id]),
    'options' => ['enctype' => 'multipart/form-data']
]); ?>

<?php
$tabs[] = [
    'label' => Yii::t('common', 'Должность'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
        'form' => $form,
        'isView' => true
    ]),
];

foreach ($external_source as $source) {
    $tabs[] = [
        'label' => Yii::t('common', $source->name),
        'content' => $this->render('_external-source-role', [
            'external_source_role' => $source->externalSourceRole,
            'model' => $model,
            'designationExtensionRole' => $designationExtensionRole,
            'form' => $form,
            'isView' => true
        ]),
    ];
}
?>



<?= Panel::widget([
    'nav' => new Nav([
        'tabs' => $tabs
    ])
])
?>

<?php ActiveForm::end(); ?>
