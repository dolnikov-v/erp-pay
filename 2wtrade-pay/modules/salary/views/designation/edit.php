<?php

use app\components\widgets\ActiveForm;
use app\widgets\Panel;
use yii\helpers\Url;
use app\widgets\Nav;

/** @var yii\web\View $this */
/** @var app\modules\salary\models\Designation $model */
/** @var array $external_source */
/** @var array $designationExtensionRole */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление должности') : Yii::t('common', 'Редактирование должности');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Зарплата'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Управление должностями'), 'url' => Url::toRoute('/salary/designation/index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute(['edit', 'id' => $model->id]),
    'options' => ['enctype' => 'multipart/form-data']
]); ?>

<?php
$tabs[] = [
    'label' => Yii::t('common', 'Должность'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
        'form' => $form,
        'isView' => false
    ]),
];

foreach ($external_source as $source) {
    $tabs[] = [
        'label' => Yii::t('common', $source->name),
        'content' => $this->render('_external-source-role', [
            'external_source_role' => $source->externalSourceRole,
            'model' => $model,
            'designationExtensionRole' => $designationExtensionRole,
            'form' => $form,
            'isView' => false
        ]),
    ];
}
?>



<?= Panel::widget([
    'nav' => new Nav([
        'tabs' => $tabs
    ])
])
?>

<?php ActiveForm::end(); ?>
