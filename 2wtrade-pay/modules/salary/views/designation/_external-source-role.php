<?php

use app\widgets\custom\Checkbox;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use app\modules\salary\assets\DesignationExtensionRoleAsset;

/** @var array $external_source_role */
/** @var \app\modules\salary\models\Designation $model */
/** @var array $designationExtensionRole */
/** @var boolean $isView */

DesignationExtensionRoleAsset::register($this);
?>

<div class="row external-roles">

    <?php if (empty($external_source_role)) : ?>

        <div class="col-lg-12 alert alert-warning">
            <?=yii::t('common', 'Для данного источника роли не были получены')?>
        </div>

    <?php else: ?>

        <?php foreach ($external_source_role as $role): ?>

            <div class="col-lg-12">
                <?= Checkbox::widget([
                    'name' => 'externalRoles[' . $role->externalSource->id . '][' . $role->id . ']',
                    'label' => Html::tag('span', Yii::t('common', $role->description)),
                    'uncheckedValue' => 0,
                    'value' => 1,
                    'checked' => $model->isNewRecord ? 0 : in_array($role->id, $designationExtensionRole),
                    'disabled' => $isView,
                    'attributes' => [
                        'data-external_source_id' => $role->externalSource->id,
                        'data-external_source_name' => $role->externalSource->name,
                        'data-external_source_role_id' => $role->id,
                        'data-external_source_role_name' => $role->name,
                        'data-designation_id' => $model->isNewRecord ? null : $model->id
                    ]
                ]) ?>
            </div>

        <?php endforeach; ?>

    <?php endif; ?>

    <?php if ($model->isNewRecord) : ?>

        <div class="row">
            <div class="col-lg-12"><br></div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить должность') : Yii::t('common', 'Сохранить должность')) ?>
                <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')) ?>
            </div>
        </div>

    <?php endif;?>

</div>