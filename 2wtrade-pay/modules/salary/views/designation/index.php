<?php
use app\components\grid\ActionColumn;
use app\components\grid\DateColumn;
use app\components\grid\GridView;
use app\helpers\DataProvider;
use app\widgets\assets\custom\ModalConfirmDeleteAsset;
use app\widgets\ButtonLink;
use app\widgets\custom\ModalConfirmDelete;
use app\widgets\Panel;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use app\widgets\Label;
use yii\helpers\Html;
use app\widgets\Nav;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */
/** @var app\modules\salary\models\search\DesignationSearch $modelSearch */

ModalConfirmDeleteAsset::register($this);

$this->title = Yii::t('common', 'Должности');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Зарплата'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

?>

<?= Panel::widget([
    'nav' => new Nav([
        'tabs' => [
            [
                'label' => Yii::t('common', 'Фильтр поиска'),
                'content' => $this->render('_nav-tab-filter', [
                    'modelSearch' => $modelSearch
                ]),
            ]
        ]
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с должностями'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function ($model) {
            return [
                'class' => 'tr-vertical-align-middle ' . ($model->active ? '' : 'text-muted'),
            ];
        },
        'columns' => [
            [
                'attribute' => 'name',
            ],
            [
                'attribute' => 'people',
                'label' => Yii::t('common', 'Число сотрудников'),
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    $text = sizeof($model->people);
                    if (Yii::$app->user->can('salary.person.index')) {
                        $text = Html::a($text,
                            Url::to([
                                '/salary/person/index',
                                'PersonSearch' => [
                                    'designation_id' => $model->id
                                ],
                            ]), [
                                'target' => '_blank',
                            ]);
                    }
                    return $text;
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'supervisor',
                'headerOptions' => ['class' => 'text-center width-150'],
                'contentOptions' => ['class' => 'active-status text-center'],
                'content' => function ($model) {
                    return Label::widget([
                        'label' => $model->supervisor ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->supervisor ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'team_lead',
                'headerOptions' => ['class' => 'text-center width-150'],
                'contentOptions' => ['class' => 'active-status text-center'],
                'content' => function ($model) {
                    return Label::widget([
                        'label' => $model->team_lead ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->team_lead ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'calc_work_time',
                'label' => Yii::t('common', 'ЗП по часам'),
                'headerOptions' => ['class' => 'text-center width-150'],
                'contentOptions' => ['class' => 'active-status text-center'],
                'content' => function ($model) {
                    return Label::widget([
                        'label' => $model->calc_work_time ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->calc_work_time ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'active',
                'headerOptions' => ['class' => 'text-center width-150'],
                'contentOptions' => ['class' => 'active-status text-center'],
                'content' => function ($model) {
                    return Label::widget([
                        'label' => $model->active ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->active ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'updated_at',
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'created_at',
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Просмотреть'),
                        'url' => function ($model) {
                            return Url::toRoute(['/salary/designation/view', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('salary.designation.view');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/salary/designation/edit', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('salary.designation.edit');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Активировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/salary/designation/activate', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('salary.designation.activate') && !$model->active;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Деактивировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/salary/designation/deactivate', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('salary.designation.deactivate') && $model->active;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить'),
                        'url' => function ($model) {
                            return Url::toRoute(['/salary/designation/delete', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('salary.designation.delete');
                        }
                    ],
                ]
            ]
        ],
    ]),
    'footer' => (Yii::$app->user->can('salary.designation.edit') ?
            ButtonLink::widget([
                'label' => Yii::t('common', 'Добавить должность'),
                'url' => Url::toRoute('/salary/designation/edit'),
                'style' => ButtonLink::STYLE_SUCCESS,
                'size' => ButtonLink::SIZE_SMALL,
            ]) : '') . LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>

<?= ModalConfirmDelete::widget() ?>