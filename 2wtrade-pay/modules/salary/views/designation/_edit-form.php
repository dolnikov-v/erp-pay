<?php

use yii\helpers\Url;
use yii\helpers\Html;
use app\widgets\InputGroupFile;
use app\widgets\InputText;
use dosamigos\ckeditor\CKEditor;

/** @var app\modules\salary\models\Designation $model */
/** @var \app\components\widgets\ActiveForm $form */
/** @var boolean $isView */

?>

<div class="row">
    <div class="col-lg-6">
        <div class="form-group">
            <?= $form->field($model, 'name')->textInput(['disabled' => $isView]) ?>
        </div>
    </div>
    <div class="col-lg-6">
        <?= $form->field($model, 'active')->checkboxCustom([
            'value' => 1,
            'checked' => $model->active,
            'uncheckedValue' => 0,
            'disabled' => $isView
        ]) ?>
        <?= $form->field($model, 'calc_work_time')->checkboxCustom([
            'value' => 1,
            'checked' => $model->calc_work_time,
            'uncheckedValue' => 0,
            'disabled' => $isView
        ]) ?>
        <?= $form->field($model, 'team_lead')->checkboxCustom([
            'value' => 1,
            'checked' => $model->team_lead,
            'uncheckedValue' => 0,
            'disabled' => $isView
        ]) ?>
        <?= $form->field($model, 'supervisor')->checkboxCustom([
            'value' => 1,
            'checked' => $model->supervisor,
            'uncheckedValue' => 0,
            'disabled' => $isView
        ]) ?>
        <?= $form->field($model, 'senior_operator')->checkboxCustom([
            'value' => 1,
            'checked' => $model->senior_operator,
            'uncheckedValue' => 0,
            'disabled' => $isView
        ]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        <div class="form-group">
            <?= $form->field($model, 'comment')->textInput(['disabled' => $isView]) ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <?php if (!$isView): ?>
            <div class="form-group">
                <?= $form->field($model, 'job_description_text')->widget(CKEditor::className(), [
                    'options' => ['rows' => 6],
                    'preset' => 'basic'
                ]); ?>
            </div>
        <?php else: ?>
            <div class="control-label"><label><?= $model->getAttributeLabel('job_description_text') ?></label></div>
            <div>
                <?= $model->job_description_text ?>
            </div>
        <?php endif; ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        <?php if (!$isView || $model->job_description_file): ?>
            <div class="control-label"><label><?= $model->getAttributeLabel('job_description_file') ?></label></div>
        <?php endif; ?>
        <?php if (!$isView && Yii::$app->user->can('salary.designation.deletejobdescriptionfile')) : ?>
            <?= InputGroupFile::widget([
                'right' => false,
                'input' => InputText::widget([
                    'type' => 'file',
                    'name' => Html::getInputName($model, 'fileJobDescription'),
                ]),
            ]) ?>
        <?php endif; ?>
    </div>
    <?php if ($model->job_description_file): ?>
        <div class="col-lg-6">
            <?php if (Yii::$app->user->can('media.salarydesignation.jobdescription')) : ?>
                <?= Html::a(Yii::t('common', 'Скачать'), '/media/salary-designation/job-description/' . $model->job_description_file, ['target' => '_blank']) ?>
            <?php endif; ?>
            <?php if (!$isView && Yii::$app->user->can('salary.designation.deletejobdescriptionfile')) : ?>&nbsp;&nbsp;
                <?= Html::a(
                    Yii::t('common', 'Удалить'),
                    Url::toRoute(['delete-job-description-file', 'id' => $model->id])
                );
                ?>
            <?php endif; ?>
        </div>
    <?php endif; ?>
</div>

<div class="row">
    <div class="col-lg-12"><br></div>
</div>

<div class="row">
    <div class="col-lg-12">
        <?php if (!$isView): ?>
            <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить должность') : Yii::t('common', 'Сохранить должность')) ?>
        <?php endif; ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')) ?>
    </div>
</div>