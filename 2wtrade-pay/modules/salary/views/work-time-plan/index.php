<?php
use app\components\grid\ActionColumn;
use app\components\grid\GridView;
use app\helpers\DataProvider;
use app\widgets\assets\custom\ModalConfirmDeleteAsset;
use app\widgets\ButtonLink;
use app\widgets\custom\ModalConfirmDelete;
use app\widgets\Panel;
use yii\helpers\Url;
use app\widgets\LinkPager;
use app\components\grid\CustomCheckboxColumn;
use app\widgets\custom\Checkbox;
use yii\web\View;
use app\widgets\Nav;

use app\modules\salary\assets\TimePlanIndexAsset;

use app\modules\salary\widgets\DeleteTimePlans as DeleteTimePlansWidget;
use app\modules\salary\assets\DeleteTimePlansAsset;

/** @var yii\web\View $this */
/** @var app\modules\salary\models\search\WorkTimePlanSearch $modelSearch */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = Yii::t('common', 'Рабочий план');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Зарплата'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

$this->registerJs("var I18n = {};", View::POS_HEAD);
$this->registerJs("I18n['Необходимо выбрать планы.'] = '" . Yii::t('common', 'Необходимо выбрать планы.') . "';", View::POS_HEAD);

DeleteTimePlansAsset::register($this);
TimePlanIndexAsset::register($this);
ModalConfirmDeleteAsset::register($this);

?>


<?= Panel::widget([
    'nav' => new Nav([
        'tabs' => [
            [
                'label' => Yii::t('common', 'Фильтр поиска'),
                'content' => $this->render('_nav-tab-filter', [
                    'modelSearch' => $modelSearch
                ]),
            ],
            [
                'label' => Yii::t('common', 'Групповые операции'),
                'content' => $this->render('_nav-tab-operations'),
            ],
        ]
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с рабочими планами'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => [
            'id' => 'work-time-plan_content',
        ],
        'columns' => [
            [
                'class' => CustomCheckboxColumn::className(),
                'content' => function ($model) {
                    return Checkbox::widget([
                        'name' => 'id[]',
                        'value' => $model->id,
                        'style' => 'checkbox-inline',
                        'label' => true,
                    ]);
                },
            ],
            [
                'attribute' => 'person_id',
                'content' => function ($model) {
                    return $model->person->name;
                }
            ],
            [
                'label' => Yii::t('common', 'Рабочая смена'),
                'attribute' => 'working_shift_id',
                'content' => function ($model) {
                    return $model->working_shift_id && $model->workingShift ? Yii::t('common', $model->workingShift->name) : '-';
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'date',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return Yii::$app->formatter->asDate($model->date);
                },
            ],
            [
                'label' => '',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return Yii::$app->formatter->asDate($model->date, 'php:D');
                },
            ],
            [
                'attribute' => 'work_hours',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'format' => ['decimal', 2],
            ],
            [
                'attribute' => 'start_at',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return date('H:i', $model->start_at);
                },
            ],
            [
                'attribute' => 'end_at',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return date('H:i', $model->end_at);
                },
            ],
            [
                'attribute' => 'lunch_time',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/salary/work-time-plan/edit', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('salary.worktimeplan.edit');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить'),
                        'url' => function () {
                            return '#';
                        },
                        'style' => 'confirm-delete-link',
                        'attributes' => function ($model) {
                            return [
                                'data-href' => Url::toRoute(['/salary/work-time-plan/delete', 'id' => $model->id]),
                            ];
                        },
                        'can' => function () {
                            return Yii::$app->user->can('salary.worktimeplan.delete');
                        }
                    ],
                ]
            ]
        ],
    ]),
    'footer' =>
        (Yii::$app->user->can('salary.worktimeplan.generate') ?
            ButtonLink::widget([
                'label' => Yii::t('common', 'Сгенерировать план'),
                'url' => Url::toRoute('/salary/work-time-plan/generate'),
                'style' => ButtonLink::STYLE_SUCCESS,
                'size' => ButtonLink::SIZE_SMALL,
            ])
            : '') .
        (Yii::$app->user->can('salary.worktimeplan.edit') ?
            ButtonLink::widget([
                'label' => Yii::t('common', 'Добавить план на день'),
                'url' => Url::toRoute('/salary/work-time-plan/edit'),
                'style' => ButtonLink::STYLE_SUCCESS,
                'size' => ButtonLink::SIZE_SMALL,
            ])
            : '') .
        LinkPager::widget([
            'pagination' => $dataProvider->getPagination(),
            'counterPosition' => LinkPager::COUNTER_POSITION_RIGHT
        ]),
]) ?>

<?= ModalConfirmDelete::widget() ?>

<?php if (Yii::$app->user->can('salary.worktimeplan.deletelist')): ?>
    <?= DeleteTimePlansWidget::widget([
        'url' => Url::toRoute('delete-list'),
    ]); ?>
<?php endif; ?>