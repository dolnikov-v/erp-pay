<?php
use app\widgets\Panel;
use yii\helpers\Url;
use app\widgets\Nav;

use app\modules\salary\assets\TimePlanEditAsset;

/** @var yii\web\View $this */
/** @var array $persons */
/** @var app\modules\salary\models\WorkTimePlan $model */

TimePlanEditAsset::register($this);

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление плана') : Yii::t('common', 'Редактирование плана');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Зарплата'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Управление планами'), 'url' => Url::toRoute('/salary/work-time-plan/index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'nav' => new Nav([
        'tabs' => [
            [
                'label' => Yii::t('common', 'План'),
                'content' => $this->render('_edit-form', [
                    'model' => $model,
                    'persons' => $persons,
                ]),
            ],
        ]
    ])
]) ?>

