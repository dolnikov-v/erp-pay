<?php
use app\widgets\Panel;
use yii\helpers\Url;
use app\widgets\Nav;

/** @var yii\web\View $this */
/** @var app\modules\salary\models\WorkTimePlan $model */

$this->title = Yii::t('common', 'Генерация плана');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Зарплата'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Управление планами'), 'url' => Url::toRoute('/salary/work-time-plan/index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'nav' => new Nav([
        'tabs' => [
            [
                'label' => Yii::t('common', 'План'),
                'content' => $this->render('_generate-form', [
                    'model' => $model,
                ]),
            ],
        ]
    ])
]) ?>

