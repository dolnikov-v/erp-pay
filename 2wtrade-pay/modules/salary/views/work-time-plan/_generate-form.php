<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;
use app\modules\salary\models\Office;
use app\modules\salary\models\Person;

/** @var app\modules\salary\models\WorkTimePlanGenerate $model */

$offices = Office::find()->bySystemUserCountries()->collection();
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['generate'])]); ?>
<div class="row">
    <div class="col-lg-3">
        <div class="form-group">
            <?= $form->field($model, 'office_id')
                ->select2List($offices, ['prompt' => Yii::t('common', 'Для всех')]) ?>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group">
            <?= $form->field($model, 'person_id')
                ->select2ListMultiple(
                    Person::find()
                        ->bySystemUserCountries()
                        ->orderBy(['name' => SORT_ASC])
                        ->collection(),
                    ['prompt' => '-']) ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-3">
        <div class="form-group">
            <?= $form->field($model, 'dateFrom')->datePicker() ?>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group">
            <?= $form->field($model, 'dateTo')->datePicker() ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= $form->submit(Yii::t('common', 'Сгенерировать план')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
