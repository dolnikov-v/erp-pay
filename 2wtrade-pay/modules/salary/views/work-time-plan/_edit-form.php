<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;
use \app\modules\salary\models\WorkingShift;

/** @var array $workTimePlanTypes */
/** @var array $persons */
/** @var app\modules\salary\models\WorkTimePlan $model */
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['edit', 'id' => $model->id])]); ?>
<div class="row">
    <div class="col-lg-4">
        <div class="form-group person_holder">
            <?= $form->field($model, 'person_id')
                ->select2List($persons, [
                    'prompt' => Yii::t('common', 'Выберите сотрудника'),
                    'length' => false
                ]) ?>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="form-group person_holder">
            <?= $form->field($model, 'working_shift_id')
                ->select2List(WorkingShift::find()
                    ->byOfficeId($model->person->office_id ?? null)
                    ->collection(), [
                    'prompt' => Yii::t('common', 'Выберите рабочую смену'),
                ]) ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-3">
        <div class="form-group">
            <?= $form->field($model, 'date')->datePicker() ?>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group">
            <?= $form->field($model, 'start_at')->textInput([
                'readonly' => true
            ]) ?>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group">
            <?= $form->field($model, 'end_at')->textInput([
                'readonly' => true
            ]) ?>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group">
            <?= $form->field($model, 'lunch_time')->textInput([
                'readonly' => true
            ]) ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить план') : Yii::t('common', 'Сохранить план')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
