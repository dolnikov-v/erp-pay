<?php
use app\assets\vendor\BootstrapLaddaAsset;
use app\components\widgets\ActiveForm;
use app\widgets\ButtonProgress;
use yii\helpers\Url;

BootstrapLaddaAsset::register($this);
?>

<?php $form = ActiveForm::begin([
    'enableClientValidation' => false,
    'action' => Url::toRoute('index'),
    'method' => 'get',
]); ?>

<div class="row">
    <div class="col-lg-4">
        <?php if (Yii::$app->user->can('salary.worktimeplan.deletelist')): ?>
            <div class="margin-bottom-5">
                <?= ButtonProgress::widget([
                    'id' => 'btn_delete_list',
                    'style' => ButtonProgress::STYLE_DANGER . ' width-300',
                    'label' => Yii::t('common', 'Удалить'),
                ]) ?>
            </div>
        <?php endif; ?>
    </div>
</div>
