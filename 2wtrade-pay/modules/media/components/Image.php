<?php
namespace app\modules\media\components;

use app\helpers\Utils;
use app\modules\media\exceptions\ImageException;
use app\modules\media\models\Media;
use Yii;
use yii\base\Component;

/**
 * Class Image
 */
class Image extends Component
{
    const FORMAT_JPG = 'jpg';
    const TYPE_CROP_USER_AVATAR = 'UserAvatar';
    const TYPE_CROP_PRODUCT_THUMB = 'ProductThumbnail';

    const DEFAULT_URL_USER_AVATAR = '/media/placeholder/avatar-profile.png';

    /**
     * @var integer
     */
    public static $defaultJpegCompressionQuality = 90;

    /**
     * @var array
     */
    protected static $normalizedExtensions = [
        'png' => 'png',
        'gif' => 'gif',
        'jpg' => 'jpg',
        'jpeg' => 'jpg',
    ];

    /**
     * @var resource
     */
    protected $image;

    /**
     * @var integer
     */
    protected $height;

    /**
     * @var integer
     */
    protected $width;

    /**
     * @var string
     */
    protected $ext;

    /**
     * @return string
     */
    public static function getPathThumbnail()
    {
        return DIRECTORY_SEPARATOR . 'media' . DIRECTORY_SEPARATOR . 'thumbnail';
    }

    /**
     * @return string
     */
    public static function getPathPlaceholder()
    {
        return DIRECTORY_SEPARATOR . 'media' . DIRECTORY_SEPARATOR . 'placeholder';
    }

    /**
     * @param $type
     * @return bool|mixed
     */
    public static function getPlaceholderUrlByType($type)
    {
        $placeholders = [
            self::TYPE_CROP_USER_AVATAR => self::DEFAULT_URL_USER_AVATAR,
        ];

        if (array_key_exists($type, $placeholders)) {
            $response = $placeholders[$type];
        } else {
            $response = false;
        }

        return $response;
    }

    /**
     * @param $type
     * @return bool|mixed
     */
    public static function getPlaceholderPathByType($type)
    {
        $placeholders = [
            self::TYPE_CROP_USER_AVATAR => Yii::getAlias('@webroot') . self::getPathPlaceholder() . DIRECTORY_SEPARATOR . 'avatar-profile.png',
        ];

        if (array_key_exists($type, $placeholders)) {
            $response = $placeholders[$type];
        } else {
            $response = false;
        }

        return $response;
    }

    /**
     * @param string $fileName
     * @return string
     * @throws ImageException
     */
    public static function getExtension($fileName)
    {
        if (!preg_match('/\.([a-z0-9]+)$/i', $fileName, $match)) {
            throw new ImageException(Yii::t('common', 'Отсутствует расширение у файла.'));
        }

        $ext = strtolower($match[1]);

        if (!isset(self::$normalizedExtensions[$ext])) {
            throw new ImageException('Неверный формат изображения.');
        }

        return self::$normalizedExtensions[$ext];
    }

    /**
     * @param $imageFile
     * @return $this
     * @throws ImageException
     */
    public function load($imageFile)
    {
        if (!file_exists($imageFile)) {
            throw new ImageException(Yii::t('common', 'Изображение не найдено.'));
        }

        $ext = self::getExtension($imageFile);

        if ($ext == 'jpg') {
            $this->image = imagecreatefromjpeg($imageFile);
        } else if ($ext == 'png') {
            $this->image = @imagecreatefrompng($imageFile);
        } else if ($ext == 'gif') {
            $this->image = @imagecreatefromgif($imageFile);
        } else {
            throw new ImageException(Yii::t('common', 'Неподдерживаемое раширение файла.'));
        }

        if (!$this->image) {
            throw new ImageException(Yii::t('common', 'Не удалось загрузить изображение.'));
        }

        $this->width = imagesx($this->image);
        $this->height = imagesy($this->image);
        $this->ext = $ext;

        return $this;
    }

    /**
     * @param boolean $withHeader
     * @param boolean $destroy
     * @param string $filename
     * @throws ImageException
     */
    public function output($withHeader = true, $destroy = true, $filename = null)
    {
        if (!$this->image) {
            throw new ImageException(Yii::t('common', 'Необходимо инициализировать изображение.'));
        }

        Media::preparePathUpload($filename, true);

        if ($withHeader && !$filename) {
            if ($this->ext == 'jpg') {
                header('Content-type: image/jpg');
            } elseif ($this->ext == 'png') {
                header('Content-type: image/png');
            } elseif ($this->ext == 'gif') {
                header('Content-type: image/gif');
            } else {
                throw new ImageException(Yii::t('common', 'Неподдерживаемое раширение файла.'));
            }
        }

        if ($this->ext == 'jpg') {
            imagejpeg($this->image, $filename ? Media::getPathThumbnail($filename) : null);
        } elseif ($this->ext == 'png') {
            imagepng($this->image, $filename ? Media::getPathThumbnail($filename) : null);
        } elseif ($this->ext == 'gif') {
            imagegif($this->image, $filename ? Media::getPathThumbnail($filename) : null);
        } else {
            throw new ImageException(Yii::t('common', 'Неподдерживаемое раширение файла.'));
        }

        if ($destroy) {
            $this->destroy();
        }
    }

    /**
     * Destroy
     */
    public function destroy()
    {
        if ($this->image) {
            @imagedestroy($this->image);
        }

        $this->image = null;
        $this->width = null;
        $this->height = null;
    }

    /**
     * @param $x0
     * @param $y0
     * @param $w
     * @param $h
     * @param null $newWidth
     * @param null $newHeight
     * @return string
     */
    public function crop($x0, $y0, $w, $h, $newWidth = null, $newHeight = null)
    {
        $newWidth = $newWidth ? $newWidth : $w;
        $newHeight = $newHeight ? $newHeight : $h;

        $newImage = imagecreatetruecolor($newWidth, $newHeight);
        imagecopyresampled($newImage, $this->image, 0, 0, $x0, $y0, $newWidth, $newHeight, $w, $h);
        $this->image = $newImage;

        $uniqId = Utils::uid();
        $this->ext = self::FORMAT_JPG;
        $fileName = $uniqId . "." . $this->ext;

        $this->output(false, true, $fileName);

        return $fileName;
    }
}
