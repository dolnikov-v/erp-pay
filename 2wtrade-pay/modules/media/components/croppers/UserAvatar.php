<?php
namespace app\modules\media\components\croppers;

use app\models\User;
use app\modules\media\components\Image;
use app\modules\media\models\Media;
use Yii;

/**
 * Class UserAvatar
 * @package app\modules\media\components\croppers
 */
class UserAvatar extends Cropper
{
    /** @var int */
    protected $resizeWidth = 130;

    /** @var int */
    protected $resizeHeight = 130;

    public function init()
    {
        parent::init();
    }

    /**
     * @return string
     */
    public function link()
    {
        /** @var User $user */
        $user = User::find()
            ->where(['id' => Yii::$app->user->id])
            ->one();

        if ($user->photo_id != $this->model->id) {
            $oldMedia = Media::findOne([$user->photo_id]);
            if ($oldMedia) {
                $oldMedia->link_id = null;
                $oldMedia->save();
            }

            $user->photo_id = $this->model->id;
            $user->media->link_id = Yii::$app->user->id;

            $user->save();
            $user->media->save();
        }

        return [
            'fileName' => $user->media->filename,
            'pathFile' => $user->media->getUrlThumbnail()
        ];
    }
}
