<?php
namespace app\modules\media\components\croppers;

use yii\base\InvalidParamException;
use Yii;

/**
 * Class CropperFactory
 * @package app\modules\media\components\croppers
 */
abstract class CropperFactory
{
    /**
     * @param $type
     * @param array $config
     * @return mixed
     */
    public static function build($type, $config = [])
    {
        $path = "\\app\\modules\\media\\components\\croppers\\" . $type;

        if (class_exists($path)) {
            return new $path($config);
        } else {
            throw new InvalidParamException(Yii::t('common', 'Неизвестный тип Cropper.'));
        }
    }
}
