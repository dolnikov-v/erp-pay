<?php
namespace app\modules\media\components\croppers;

use app\helpers\Utils;
use app\modules\media\components\Image;
use app\modules\media\exceptions\CropperException;
use app\modules\media\exceptions\ImageException;
use app\modules\media\models\Media;
use app\modules\media\models\Uploader;
use Yii;
use yii\base\BaseObject;

abstract class Cropper extends BaseObject
{
    /** @var Media */
    public $model;

    /** @var string */
    public $currentFileName;

    /** @var array */
    public $cropData;

    /** @var  integer */
    public $linkId;

    /** @var  integer */
    public $cropType;

    /** @var integer */
    protected $resizeWidth;

    /** @var integer */
    protected $resizeHeight;

    /** @var integer */
    private $x0;

    /** @var integer */
    private $y0;

    /** @var integer */
    private $width;

    /** @var integer */
    private $height;

    /** @var  string */
    private $ext;

    public function init()
    {
        parent::init();

        $this->x0 = $this->cropData['x'];
        $this->y0 = $this->cropData['y'];
        $this->width = $this->cropData['width'];
        $this->height = $this->cropData['height'];

        $fileName = explode('.', $this->currentFileName);
        $this->ext = array_pop($fileName);

        $model = Media::find()
            ->where(['filename' => $this->currentFileName])
            ->one();

        if (!$model) {
            $placeholder = Image::getPlaceholderPathByType($this->cropType);

            if ($placeholder) {
                $uniqName = Utils::uid() . "." . $this->ext;
                Uploader::preparePathUpload($uniqName, true);

                if (copy($placeholder, Uploader::getPathFile($uniqName))) {
                    $model = new Media();
                    $model->filename = $uniqName;
                } else {
                    throw new CropperException(Yii::t('common', 'Файл не сохранен.'));
                }
            }
        }

        $this->model = $model;
    }

    /**
     * @return array
     */
    public function save()
    {
        $response = [
            'status' => 'fail',
            'answer' => ''
        ];

        $dataCropRound = [];

        foreach ($this->cropData as $key => $item) {
            $dataCropRound += [$key => (int)$item];
        }

        $pathFile = Uploader::getPathFile($this->model->filename);
        $this->model->crop_file = (new Image())
            ->load($pathFile)
            ->crop($this->x0, $this->y0, $this->width, $this->height, $this->resizeWidth, $this->resizeHeight);

        $this->model->link_type = $this->cropType;
        $this->model->type = Media::TYPE_IMAGE;
        $this->model->crop_data = json_encode($dataCropRound, JSON_UNESCAPED_SLASHES);

        if ($this->model->save()) {
            $response['status'] = 'success';
            $response['answer'] = $this->link();
        }

        return $response;
    }

    /**
     * @return mixed
     */
    abstract function link();
}
