<?php
namespace app\modules\media\models\query;

use app\components\db\ActiveQuery;
use app\modules\media\models\Media;

/**
 * Class MediaQuery
 * @package app\models\query
 *
 * @method Media one($db = null)
 * @method Media[] all($db = null)
 */
class MediaQuery extends ActiveQuery
{

}
