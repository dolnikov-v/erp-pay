<?php
namespace app\modules\media\models;

use app\helpers\Utils;
use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Class Uploader
 * @package app\media\models
 */
class Uploader extends Model
{
    /**
     * @var UploadedFile
     */
    public $file;

    public function rules()
    {
        return [
            [['file'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg, jpeg'],
        ];
    }

    /**
     * @param $fileName
     * @param bool $prepareDir
     * @return string
     */
    public static function preparePathUpload($fileName, $prepareDir = false)
    {
        $subDir = substr($fileName, 0, 2);

        $dir = Yii::getAlias('@media') . DIRECTORY_SEPARATOR . 'original' . DIRECTORY_SEPARATOR . $subDir . DIRECTORY_SEPARATOR;

        if ($prepareDir) {
            Utils::prepareDir($dir);
        }

        return $dir;
    }

    /**
     * @param $fileName
     * @return string
     */
    public static function getPathFile($fileName)
    {
        $path = self::preparePathUpload($fileName);

        return $path . $fileName;
    }

    /**
     * @return string
     */
    public function upload()
    {
        if ($this->validate()) {
            $uniqId = Utils::uid();
            $file = $uniqId . "." . $this->file->extension;
            $path = self::preparePathUpload($file, true);

            $this->file->saveAs($path . $file);

            return $file;
        }
    }

}
