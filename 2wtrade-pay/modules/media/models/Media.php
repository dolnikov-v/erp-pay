<?php
namespace app\modules\media\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\helpers\Utils;
use app\models\User;
use app\modules\media\components\Image;
use app\modules\media\models\query\MediaQuery;
use Yii;
use yii\helpers\Url;

/**
 * Class Media
 * @package app\models
 * @property integer $id
 * @property string $filename
 * @property string $type
 * @property string $link_id
 * @property string $link_type
 * @property string $crop_file
 * @property string $crop_data
 * @property integer $created_at
 * @property integer $updated_at
 * @property User $user
 */
class Media extends ActiveRecordLogUpdateTime
{
    const TYPE_IMAGE = 'image';
    const TYPE_VIDEO = 'video';
    const TYPE_AUDIO = 'audio';

    /**
     * @return array
     */
    public function getTypeCollection()
    {
        return [
            self::TYPE_IMAGE => Yii::t('common', 'Изображение'),
            self::TYPE_AUDIO => Yii::t('common', 'Аудио'),
            self::TYPE_VIDEO => Yii::t('common', 'Видео'),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%media}}';
    }

    /**
     * @return MediaQuery
     */
    public static function find()
    {
        return new MediaQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['type', 'in', 'range' => array_keys(self::getTypeCollection())],
            [['link_id'], 'integer'],
            [['filename', 'type', 'link_type', 'crop_file', 'crop_data'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'filename' => Yii::t('common', 'Ссылка'),
            'type' => Yii::t('common', 'Дата создания'),
            'link_id' => Yii::t('common', 'Id связи'),
            'link_type' => Yii::t('common', 'Тип Media'),
            'crop_file' => Yii::t('common', 'Файл превью'),
            'crop_data' => Yii::t('common', 'Данные превью'),
        ];
    }

    /**
     * @return string
     */
    public function getUrlOriginal()
    {
        return Url::to('/media/index/original/' . $this->filename);
    }

    /**
     * @return string
     */
    public function getUrlThumbnail()
    {
        return Url::to('/media/thumbnail/' . substr($this->crop_file, 0, 2) . DIRECTORY_SEPARATOR . $this->crop_file);
    }

    /**
     * @param $fileName
     * @return string
     */
    public static function getPathThumbnail($fileName)
    {
        $path = self::preparePathUpload($fileName);

        return $path . $fileName;
    }

    /**
     * @param $fileName
     * @param bool $prepareDir
     * @return string
     */
    public static function preparePathUpload($fileName, $prepareDir = false)
    {
        $subDir = substr($fileName, 0, 2);

        $dir = Yii::getAlias('@webroot') . DIRECTORY_SEPARATOR . 'media' . DIRECTORY_SEPARATOR . 'thumbnail' . DIRECTORY_SEPARATOR . $subDir . DIRECTORY_SEPARATOR;

        if ($prepareDir) {
            Utils::prepareDir($dir);
        }

        return $dir;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['photo_id' => 'id']);
    }

}
