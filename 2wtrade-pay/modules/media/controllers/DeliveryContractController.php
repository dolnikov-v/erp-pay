<?php

namespace app\modules\media\controllers;

use app\components\web\Controller;
use app\modules\delivery\models\DeliveryContractFile;
use app\modules\media\components\Image;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use app\models\User;

/**
 * Class DeliveryContractController
 * @package app\media\controllers
 */
class DeliveryContractController extends Controller
{
    /**
     * @param integer $id
     * @throws NotFoundHttpException
     */
    public function actionFile($id)
    {
        $media = false;
        if (!User::isAllowCountry()) {
            throw new ForbiddenHttpException(Yii::t('common', 'Доступ запрещен.'));
        }

        if (Yii::$app->user->can('media.deliverycontract.file')) {
            $media = DeliveryContractFile::findOne($id);
        }

        if (!$media) {
            throw new NotFoundHttpException(Yii::t('common', 'Не найдена запись с таким ид.'));
        }

        $contract = $media->deliveryContract;

        if (!isset($contract->delivery_id) || !User::isAllowDelivery($contract->delivery_id)) {
            throw new ForbiddenHttpException(Yii::t('common', 'Доступ запрещен.'));
        }

        $filePath = $contract->getUploadPath(true) . DIRECTORY_SEPARATOR . $media->system_filename;

        Yii::$app->response->sendFile($filePath, $media->file_name);
    }
}