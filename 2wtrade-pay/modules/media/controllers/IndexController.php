<?php
namespace app\modules\media\controllers;

use app\components\filters\AjaxFilter;
use app\components\web\Controller;
use app\modules\media\components\croppers\CropperFactory;
use app\modules\media\components\croppers\UserAvatar;
use app\modules\media\components\Image;
use app\modules\media\models\Media;
use app\modules\media\models\Uploader;
use Yii;
use yii\base\Exception;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * Class IndexController
 * @package app\controllers
 */
class IndexController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => [
                    'upload',
                    'save-crop-data'
                ],
            ]
        ];
    }

    /**
     * @return string
     */
    public function actionUpload()
    {
        $response = [];

        if (Yii::$app->request->isPost) {
            $model = new Uploader();
            $model->file = UploadedFile::getInstanceByName('mediaFile');
            $response = $model->upload();
        }

        if (!empty($response)) {
            $media = new Media();
            $media->filename = $response;
            $media->type = Media::TYPE_IMAGE;
            if ($media->save()) {
                return [
                    'pathFile' => $media->getUrlOriginal(),
                    'file' => $media->filename
                ];
            }
        }
    }

    /**
     * @param $fileName
     * @throws NotFoundHttpException
     */
    public function actionOriginal($fileName)
    {
        $media = Media::find()->where(['filename' => $fileName])->one();

        if (!$media) {
            throw new NotFoundHttpException(Yii::t('common', 'Не найдена запись с таким именем.'));
        }

        $filePath = Uploader::getPathFile($fileName);

        return (new Image())->load($filePath)->output();
    }

    /**
     * @return array
     * @throws Exception
     */
    public function actionSaveCropData()
    {
        $cropData = Yii::$app->request->post('cropData');
        $cropType = Yii::$app->request->post('cropType');
        $linkId = Yii::$app->request->post('linkId');
        $currentFileName = Yii::$app->request->post('currentFileName');

        $configCropper = [
            'linkId' => $linkId,
            'currentFileName' => $currentFileName,
            'cropData' => $cropData,
            'cropType' => $cropType,
        ];

        /** @var UserAvatar $cropper */
        $cropper = CropperFactory::build($cropType, $configCropper);

        return $cropper->save();
    }
}
