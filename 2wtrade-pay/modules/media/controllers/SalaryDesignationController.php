<?php
namespace app\modules\media\controllers;

use app\components\web\Controller;
use app\modules\salary\models\Designation;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * Class SalaryDesignationController
 * @package app\controllers
 */
class SalaryDesignationController extends Controller
{
    /**
     * @param string $fileName
     * @throws NotFoundHttpException
     */
    public function actionJobDescription($fileName)
    {
        $media = false;
        if (Yii::$app->user->can('media.salarydesignation.jobdescription')) {
            $media = Designation::find()->where(['job_description_file' => $fileName])->one();
        }

        if (!$media) {
            throw new NotFoundHttpException(Yii::t('common', 'Не найдена запись с таким именем.'));
        }

        $filePath = Designation::getPathFile($fileName);

        Yii::$app->response->sendFile($filePath);
    }
}