<?php

namespace app\modules\media\controllers;

use app\components\web\Controller;
use app\modules\media\components\Image;
use app\modules\salary\models\OfficeExpense;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * Class SalaryExpenseController
 * @package app\controllers
 */
class SalaryExpenseController extends Controller
{
    /**
     * @param string $fileName
     * @throws NotFoundHttpException
     */
    public function actionFilename($fileName)
    {
        self::getModel('filename', $fileName);
        self::outputFile(OfficeExpense::getPathFile($fileName));
    }

    /**
     * @param $attribute
     * @param $fileName
     * @return array|null|\yii\db\ActiveRecord
     * @throws NotFoundHttpException
     */
    private function getModel($attribute, $fileName)
    {
        $model = null;

        if (Yii::$app->user->can('media.salaryexpense.filename')) {
            $model = OfficeExpense::find()->where([$attribute => $fileName])->one();
        }
        if (!$model || !$model->hasAttribute($attribute) || $model->$attribute == '') {
            throw new NotFoundHttpException(Yii::t('common', 'Не найдена запись с таким именем.'));
        }

        return $model;
    }

    /**
     * @param $filePath
     */
    private function outputFile($filePath)
    {
        try {
            (new Image())->load($filePath)->output();
        } catch (\Exception $e) {
            Yii::$app->response->sendFile($filePath);
        }
    }
}