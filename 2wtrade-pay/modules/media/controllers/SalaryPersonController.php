<?php
namespace app\modules\media\controllers;

use app\components\web\Controller;
use app\modules\salary\models\MonthlyStatement;
use app\modules\salary\models\MonthlyStatementData;
use app\modules\salary\models\Person;
use app\modules\media\components\Image;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * Class SalaryPersonController
 * @package app\controllers
 */
class SalaryPersonController extends Controller
{
    /**
     * @param string $fileName
     * @throws NotFoundHttpException
     */
    public function actionPhoto($fileName)
    {
        self::getModel('photo', $fileName);
        self::outputFile(Person::getPathFile($fileName));
    }

    /**
     * @param string $fileName
     * @throws NotFoundHttpException
     */
    public function actionPhotoAgreement($fileName)
    {
        self::getModel('photo_agreement', $fileName);
        self::outputFile(Person::getPathFile($fileName));
    }

    /**
     * @param string $fileName
     * @throws NotFoundHttpException
     */
    public function actionDismissalFile($fileName)
    {
        self::getModel('dismissal_file', $fileName);
        self::outputFile(Person::getPathFile($fileName));
    }

    /**
     * @param string $fileName
     * @throws NotFoundHttpException
     */
    public function actionSheetView($fileName)
    {
        $data = MonthlyStatementData::find()->byFile($fileName)->one();
        if ($data) {
            $filePath = MonthlyStatement::getSalaryFilePath($data->person_id) . DIRECTORY_SEPARATOR . $fileName;
            Yii::$app->response->sendFile($filePath, $fileName, ['inline' => true]);
        } else {
            throw new NotFoundHttpException(Yii::t('common', 'Не найдена запись с таким именем.'));
        }
    }

    /**
     * @param string $fileName
     * @throws NotFoundHttpException
     */
    public function actionSheetDownload($fileName)
    {
        $data = MonthlyStatementData::find()->byFile($fileName)->one();
        if ($data) {
            $filePath = MonthlyStatement::getSalaryFilePath($data->person_id) . DIRECTORY_SEPARATOR . $fileName;
            Yii::$app->response->sendFile($filePath);
        } else {
            throw new NotFoundHttpException(Yii::t('common', 'Не найдена запись с таким именем.'));
        }
    }

    /**
     * @param $attribute
     * @param $fileName
     * @return Person
     * @throws NotFoundHttpException
     */
    private function getModel($attribute, $fileName)
    {
        $person = null;

        if (Yii::$app->user->can('media.salaryperson.' . str_replace('_', '', $attribute))) {
            $person = Person::find()->where([$attribute => $fileName])->one();
        }
        if (!$person || !$person->hasAttribute($attribute) || $person->$attribute == '') {
            throw new NotFoundHttpException(Yii::t('common', 'Не найдена запись с таким именем.'));
        }

        return $person;
    }

    /**
     * @param $filePath
     */
    private function outputFile($filePath)
    {
        try {
            (new Image())->load($filePath)->output();
        } catch (\Exception $e) {
            Yii::$app->response->sendFile($filePath);
        }
    }
}