<?php
namespace app\modules\media\controllers;

use app\components\filters\AjaxFilter;
use app\components\web\Controller;
use app\modules\callcenter\models\CallCenterUser;
use app\modules\media\components\croppers\CropperFactory;
use app\modules\media\components\croppers\UserAvatar;
use app\modules\media\components\Image;
use app\modules\media\models\Media;
use app\modules\media\models\Uploader;
use Yii;
use yii\base\Exception;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;


/**
 * Class CallCenterUserController
 * @package app\controllers
 */
class CallCenterUserController extends Controller
{
    /**
     * @throws NotFoundHttpException
     */
    public function actionIndex($fileName)
    {
        $media = CallCenterUser::find()->where(['photo' => $fileName])->one();

        if (!$media) {
            throw new NotFoundHttpException(Yii::t('common', 'Не найдена запись с таким именем.'));
        }

        $filePath = CallCenterUser::getPathFile($fileName);

        return (new Image())->load($filePath)->output();
    }
}
