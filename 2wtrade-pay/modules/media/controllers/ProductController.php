<?php
namespace app\modules\media\controllers;

use app\components\web\Controller;
use app\models\Product;
use app\models\ProductPhoto;
use app\modules\media\components\Image;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * Class ProductController
 * @package app\controllers
 */
class ProductController extends Controller
{
    /**
     * @param string $fileName
     * @throws NotFoundHttpException
     */
    public function actionIndex($fileName)
    {
        $media = ProductPhoto::find()->where(['image' => $fileName])->one();

        if (!$media) {
            $media = Product::find()->where(['image' => $fileName])->one();
            if (!$media) {
                throw new NotFoundHttpException(Yii::t('common', 'Не найдена запись с таким именем.'));
            }
        }

        $filePath = Product::getPathFile($fileName);

        return (new Image())->load($filePath)->output();
    }
}
