<?php
namespace app\modules\storage\assets;

use yii\web\AssetBundle;

/**
 * Class StorageBalanceAsset
 * @package app\modules\report\assets
 */
class StorageBalanceAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/storage';

    public $css = [
        'balance.css'
    ];

    public $js = [
        'balance.js'
    ];

    public $depends = [
        'app\assets\SiteAsset',
    ];
}