<?php
namespace app\modules\storage\controllers;

use app\components\web\Controller;
use app\modules\storage\models\StoragePartMoves;
use app\modules\storage\models\search\StoragePartMovesSearch;
use yii\base\Exception;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use Yii;
use yii\helpers\Url;
use app\components\Notifier;

/**
 * Class MovesController
 * @package app\modules\controllers\storage
 */
class MovesController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearchMoves = new StoragePartMovesSearch();
        $dataProviderMoves = $modelSearchMoves->search();

        return $this->render('index', [
            'dataProvider' => $dataProviderMoves,
        ]);
    }

    /**
     * @param integer $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     * @throws ForbiddenHttpException
     */
    public function actionCancelMove($id)
    {
        $partMove = $this->findPartMove($id);
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $storagePartFrom = $partMove->storagePartFrom;
            $storagePartFrom->balance += $partMove->quantity;
            $storagePartFrom->active = 1;

            if ($storagePartFrom->save() && $partMove->delete()) {
                Yii::$app->notifier->addNotification(Yii::t('common', 'Отмена перемещения прошла успешно.'), 'success');
                $transaction->commit();
            } else {
                Yii::$app->notifier->addNotificationsByModel($storagePartFrom);
                Yii::$app->notifier->addNotificationsByModel($partMove);
                $transaction->rollBack();
            }
        } catch (Exception $e) {
            Yii::$app->notifier->addNotification($e->getMessage(), Notifier::TYPE_DANGER);
            $transaction->rollBack();
        }
        return $this->redirect(Url::toRoute(['index']));
    }

    /**
     * @param integer $id
     * @return StoragePartMoves
     * @throws NotFoundHttpException
     * @throws ForbiddenHttpException
     */
    private function findPartMove($id)
    {
        $model = StoragePartMoves::findOne($id);

        if (!$model) {
            throw new NotFoundHttpException(Yii::t('common', 'Партия на перемещение не найдена.'));
        }

        if ($model->storage_part_to != null) {
            throw new ForbiddenHttpException(Yii::t('common', 'Партия уже была перемещена'));
        }

        return $model;
    }
}
