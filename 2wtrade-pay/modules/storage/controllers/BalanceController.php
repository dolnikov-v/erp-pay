<?php
namespace app\modules\storage\controllers;

use app\components\filters\AjaxFilter;
use app\components\web\Controller;
use app\models\logs\TableLog;
use app\modules\storage\models\search\StorageProductSearch;
use app\modules\storage\models\StorageDocument;
use app\modules\storage\models\StorageProduct;
use Yii;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

/**
 * Class BalanceController
 * @package app\modules\controllers\storage
 */
class BalanceController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => [
                    'change',
                    'delete',
                ],
            ]
        ];
    }


    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new StorageProductSearch();

        $params = Yii::$app->request->queryParams;

        $dataProvider = $modelSearch->search($params, $groupByProduct = true);

        return $this->render('index', [
            'modelSearch' => $modelSearch,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param int $id
     * @return string
     */
    public function actionLog(int $id)
    {
        $change = TableLog::find()->byTable(StorageProduct::clearTableName())->byID($id)->allSorted();
        return $this->render('log', [
            'changeLog' => $change,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionUnlimit($id)
    {
        $model = $this->getModel($id);

        $model->unlimit = 1;

        if ($model->save(true, ['unlimit'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Товар склада сделан безлимитным.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionLimit($id)
    {
        $model = $this->getModel($id);

        $model->unlimit = 0;

        if ($model->save(true, ['unlimit'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Товар склада сделан лимитным.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param integer $id
     * @return StorageProduct
     * @throws NotFoundHttpException
     */
    private function getModel($id)
    {
        $model = StorageProduct::findOne($id);

        if (!$model) {
            throw new NotFoundHttpException(Yii::t('common', 'Товар склада не найден.'));
        }

        return $model;
    }

    /**
     * @throws NotFoundHttpException
     * @return array
     */
    public function actionChange()
    {
        $id = Yii::$app->request->post('id');
        $val = Yii::$app->request->post('val');

        $model = $this->getModel($id);

        $storageDocument = new StorageDocument();
        if ($val > 0) {
            $storageDocument->type = StorageDocument::TYPE_CORRECTION_POSITIVE;
        } else {
            $storageDocument->type = StorageDocument::TYPE_CORRECTION_NEGATIVE;
        }
        $storageDocument->user_id = Yii::$app->user->id;
        $storageDocument->quantity = abs($val);
        $storageDocument->product_id = $model->product_id;
        $storageDocument->storage_id_to = $model->storage_id;

        $response['status'] = 'fail';

        if ($storageDocument->save(true)) {
            $model->balance = $model->balance + $val;
            if ($model->save(true)) {
                $response['balance'] = $model->balance;
                $response['updated_at'] = Yii::$app->formatter->asDatetime($model->updated_at);
                $response['status'] = 'success';
                $response['message'] = Yii::t('common', 'Операция выполнена успешно');
            } else {
                $response['message'] = $model->getFirstErrorAsString();
            }
        } else {
            $response['message'] = $storageDocument->getFirstErrorAsString();
        }
        return $response;
    }

    /**
     * @param int $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionDelete(int $id)
    {
        $model = $this->getModel($id);

        $response['status'] = 'fail';
        if ($model->balance == 0) {
            $storageDocument = new StorageDocument();
            $storageDocument->scenario = StorageDocument::SCENARIO_DELETE;
            $storageDocument->type = StorageDocument::TYPE_DELETE;
            $storageDocument->user_id = Yii::$app->user->id;
            $storageDocument->quantity = 0;
            $storageDocument->product_id = $model->product_id;
            $storageDocument->storage_id_to = $model->storage_id;
            if ($storageDocument->save(true)) {
                try {
                    if ($model->delete()) {
                        $response['status'] = 'success';
                        $response['message'] = Yii::t('common', 'Операция выполнена успешно');
                    } else {
                        $response['message'] = $model->getFirstErrorAsString();
                    }
                } catch (\Throwable $e) {
                    $response['message'] = $e->getMessage();
                }
            } else {
                $response['message'] = $storageDocument->getFirstErrorAsString();
            }
        } else {
            $response['message'] = Yii::t('common', 'Баланс {balance}. Удалить можно только записи с нулевым балансом.', ['balance' => $model->balance]);
        }
        return $response;
    }
}
