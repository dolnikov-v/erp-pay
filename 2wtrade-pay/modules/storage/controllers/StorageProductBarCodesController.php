<?php
namespace app\modules\storage\controllers;

use app\components\web\Controller;
use app\models\Product;
use app\modules\storage\models\Storage;
use app\modules\storage\models\StorageDocument;
use app\modules\bar\models\Bar;
use Yii;
use yii\web\BadRequestHttpException;
use yii\helpers\Html;

/**
 * Class StorageProductBarCodesController
 * @package app\modules\controllers\storage
 */
class StorageProductBarCodesController extends Controller
{
    /**
     * @throws \yii\web\HttpException
     * @return array|string
     */
    public function actionGetSelectOptionsBarList()
    {
        $storageId = (int)Yii::$app->request->post('storage_id');
        $productId = (int)Yii::$app->request->post('product_id');

        $storage = Storage::findOne($storageId);
        if (!$storage) {
            throw new BadRequestHttpException(Yii::t('common', 'Обращение к несуществующему складу.'));
        }

        $product = Product::findOne($productId);
        if (!$product) {
            throw new BadRequestHttpException(Yii::t('common', 'Обращение к несуществующему товару.'));
        }

        if ($storage->use_bar_code) {
            $subQuery = Bar::find()
                ->select([
                    'id' => Bar::tableName() . '.id',
                ])
                ->where([Bar::tableName() . '.bar_list_id' => StorageDocument::tableName() . '.bar_list_id'])
                ->andWhere(['!=', Bar::tableName() . '.status', Bar::CREATED])
                ->limit(1);

            $storageDocumentsUseBarListsArray = StorageDocument::find()
                ->where(['storage_id_to' => $storageId])
                ->andWhere(['product_id' => $productId])
                ->andWhere(['is not', 'bar_list_id', null])
                ->andWhere(['not exists', $subQuery])
                ->asArray()
                ->all();
            $resultArray = null;
            if (count($storageDocumentsUseBarListsArray) > 0) {
                foreach ($storageDocumentsUseBarListsArray as $storageDocumentsUseBarListsOne) {
                    $resultArray[$storageDocumentsUseBarListsOne['bar_list_id']] = $storageDocumentsUseBarListsOne['quantity'];
                }
                return Html::checkboxList('StorageDocument[barListArray]', null, $resultArray, ['class' => 'checkbox']);
            }
        }

        return [
            'error' => true
        ];
    }
}