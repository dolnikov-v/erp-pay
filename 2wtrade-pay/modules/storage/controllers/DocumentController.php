<?php
namespace app\modules\storage\controllers;

use app\components\web\Controller;
use app\modules\storage\models\search\StorageDocumentSearch;
use Yii;

/**
 * Class DocumentController
 * @package app\modules\controllers\storage
 */
class DocumentController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new StorageDocumentSearch();

        $params = Yii::$app->request->queryParams;

        $dataProvider = $modelSearch->search($params);

        return $this->render('index', [
            'modelSearch' => $modelSearch,
            'dataProvider' => $dataProvider,
        ]);
    }
}