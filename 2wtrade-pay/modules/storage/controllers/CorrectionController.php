<?php
namespace app\modules\storage\controllers;

use app\components\web\Controller;
use app\modules\storage\models\StorageDocument;
use app\modules\storage\models\StorageProduct;
use Yii;
use yii\helpers\Url;

/**
 * Class CorrectionController
 * @package app\modules\controllers\storage
 */
class CorrectionController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $model = new StorageDocument();
        $model->setScenario(StorageDocument::SCENARIO_CORRECTION);
        $model->type = StorageDocument::TYPE_CORRECTION_POSITIVE;
        $model->user_id = Yii::$app->user->id;

        $params = Yii::$app->request->post();
        if ($model->load($params)) {

            if (isset($params['StorageDocument']['isNegative']) && $params['StorageDocument']['isNegative'] === '1') {
                $model->isNegative = true;
                $model->type = StorageDocument::TYPE_CORRECTION_NEGATIVE;
            }

            $transaction = Yii::$app->db->beginTransaction();
            if ($model->save()) {
                try {
                    $quantity = $model->quantity;
                    if ($model->type == StorageDocument::TYPE_CORRECTION_NEGATIVE){
                        $quantity = -$model->quantity;
                    }
                    StorageProduct::addProductOnStorage($model->product_id, $model->storage_id_to, $quantity, $model->shelf_life);
                    $transaction->commit();
                    Yii::$app->notifier->addNotification(Yii::t('common', 'Документ на корректировку успешно добавлен.'), 'success');
                } catch (\Exception $e) {
                    $transaction->rollBack();
                    Yii::$app->notifier->addNotification($e->getMessage(), 'danger');
                }

                return $this->redirect(Url::toRoute('index'));
            } else {
                $transaction->rollBack();
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }

        return $this->render('index', [
            'model' => $model
        ]);
    }
}