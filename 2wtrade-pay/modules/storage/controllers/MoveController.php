<?php
namespace app\modules\storage\controllers;

use app\components\web\Controller;
use app\modules\storage\models\StorageDocument;
use app\modules\storage\models\StorageProduct;
use app\modules\storage\models\Storage;
use app\modules\bar\models\BarList;
use app\modules\bar\models\Bar;
use Yii;
use yii\helpers\Url;

/**
 * Class MoveController
 * @package app\modules\controllers\storage
 */
class MoveController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $model = new StorageDocument();
        $model->setScenario(StorageDocument::SCENARIO_MOVE);
        $model->type = StorageDocument::TYPE_MOVEMENT;
        $model->user_id = Yii::$app->user->id;

        if ($model->load(Yii::$app->request->post())) {
            if (count($model->barListArray) > 0) { // по списку файлов штрих-кодов
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    foreach ($model->barListArray as $barListItem) {
                        $barList = BarList::findOne(['id' => $barListItem]);
                        if ($barList !== null) {
                            StorageDocument::createNewDocument(
                                StorageDocument::TYPE_MOVEMENT,
                                $model->product_id,
                                $barList->quantity,
                                $model->storage_id_from,
                                $model->storage_id_to,
                                $barList->id,
                                null,
                                null,
                                $model->comment,
                                StorageDocument::SCENARIO_MOVE,
                                $model->shelf_life
                            );
                            StorageProduct::addProductOnStorage($model->product_id, $model->storage_id_to, $barList->quantity, $model->shelf_life);
                            Bar::updateAll(['storage_id' => $model->storage_id_to],['bar_list_id' => $barList->id]);
                        } else {
                            throw new \Exception(Yii::t('common', 'Лист штрих-кодов не найден.'));
                        }
                    }
                    $transaction->commit();
                    Yii::$app->notifier->addNotification(Yii::t('common', 'Документ на перемещение успешно добавлен.'), 'success');
                } catch (\Exception $e) {
                    $transaction->rollBack();
                    Yii::$app->notifier->addNotification($e->getMessage(), 'danger');
                }
            } else { // перемещение без файла штрих-кодов
                $transaction = Yii::$app->db->beginTransaction();
                if ($model->save()) {
                    try {
                        StorageProduct::addProductOnStorage($model->product_id, $model->storage_id_to, $model->quantity, $model->shelf_life);
                        $transaction->commit();
                        Yii::$app->notifier->addNotification(Yii::t('common', 'Документ на перемещение успешно добавлен.'), 'success');
                        return $this->redirect(Url::toRoute('index'));
                    } catch (\Exception $e) {
                        $transaction->rollBack();
                        Yii::$app->notifier->addNotification($e->getMessage(), 'danger');
                    }
                } else {
                    Yii::$app->notifier->addNotificationsByModel($model);
                }
            }
        }

        return $this->render('index', [
            'model' => $model,
            'storagesFrom' => Storage::find()
                ->byCountryId(Yii::$app->user->country->id)
                ->collection(),
        ]);
    }
}