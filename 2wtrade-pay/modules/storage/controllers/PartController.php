<?php

namespace app\modules\storage\controllers;

use app\components\web\Controller;
use app\modules\storage\models\search\StoragePartSearch;
use app\modules\storage\models\StoragePart;
use app\modules\storage\models\Storage;
use app\modules\storage\models\StoragePartMoves;
use app\modules\storage\models\search\StoragePartMovesSearch;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\base\InvalidParamException;
use yii\helpers\Url;
use yii\helpers\Html;
use Yii;
use app\models\Currency;
use app\components\Currency as CurrencyComponent;
use app\components\Notifier;


/**
 * Class PartController
 * @package app\modules\controllers\storage
 */
class PartController extends Controller
{
    /**
     * @return string
     * @throws InvalidParamException
     */
    public function actionIndex()
    {
        try {
            $modelSearch = new StoragePartSearch();
            $modelSearch->withExpected = true;
            $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

            $modelSearchMoves = new StoragePartMovesSearch();
            $dataProviderMoves = $modelSearchMoves->search($modelSearch->storage_id);

            $storageName = null;
            if (isset($modelSearch->storage)) {
                $storageName = $modelSearch->storage->name;
            }

            return $this->render('index', [
                'modelSearch' => $modelSearch,
                'dataProvider' => $dataProvider,
                'dataProviderMoves' => $dataProviderMoves,
                'storageName' => $storageName,
            ]);
        } catch (InvalidParamException $e) {
            Yii::$app->notifier->addNotification($e->getMessage());
            return $this->redirect(['control/index']);
        }
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     * @throws ForbiddenHttpException
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->findModel($id);

            if ($model->balance != $model->quantity) {
                throw new ForbiddenHttpException(Yii::t('common', 'Запрещено редактировать партию с изменившимся балансом.'));
            }

            $storageItem['name'] = null;
        } else {
            $model = new StoragePart();

            try {
                $storage = Yii::$app->request->get('StoragePart');
                $storage = $storage['storage_id'];
                if (!isset($storage)) {
                    $storage = Yii::$app->request->post('StoragePart');
                    $storage = $storage['storage_id'];
                    if (!isset($storage)) {
                        throw new NotFoundHttpException(Yii::t('common', 'Склад не найден.'));
                    }
                }
                $storageItem = Storage::find()
                    ->select(['id' => 'id', 'name' => 'name'])
                    ->where(['id' => $storage])
                    ->asArray()
                    ->one();
                $model->storage_id = $storageItem['id'];

                if ($model->storage_id == null) {
                    throw new NotFoundHttpException(Yii::t('common', 'Склад не найден.'));
                }
            } catch (Exception $e) {
                throw new NotFoundHttpException(Yii::t('common', 'Склад не найден.'));
            }
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->balance = $model->quantity;
            $usd = Currency::findOne(['char_code' => CurrencyComponent::DEFAULT_CURRENCY]);
            $model->currency_id = $usd->id;
            $model->expected = ArrayHelper::getValue(Yii::$app->request->post('StoragePart'), 'expected');
            $model->expected_at = ($model->expected && $model->expected_at) ? strtotime($model->expected_at) : null;

            if ($model->save()) {
                return $this->redirect(Url::toRoute(['index', Html::getInputName(new StoragePartSearch(), 'storage_id') => $model->storage_id]));
            }
        }

        return $this->render('edit', [
            'model' => $model,
            'products' => StoragePart::getCollectionProducts(),
            'storageName' => $storageItem['name'],
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws ForbiddenHttpException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if ($model->delete()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Складская партия успешно удалена.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute(['index', Html::getInputName(new StoragePartSearch(), 'storage_id') => $model->storage_id]));
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     * @throws ForbiddenHttpException
     */
    public function actionMove($id = null)
    {
        $partMove = new StoragePartMoves();
        if ($id) {
            $model = $this->findModel($id);

            if ($model->expected || $model->unlimit) {
                throw new ForbiddenHttpException(Yii::t('common', 'Запрешено для запланированной/безлимитной партии.'));
            }

            $partMove->storage_part_from = $model->id;
            $partMove->product_id = $model->product_id;
            $partMove->user_from = Yii::$app->user->id;
            $partMove->type = StoragePartMoves::MOVE;
        }

        if ($partMove->load(Yii::$app->request->post())) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                if ($partMove->save()) {
                    $partFrom = $partMove->storagePartFrom;
                    $partFrom->balance -= $partMove->quantity;
                    if ($partFrom->balance == 0) {
                        $partFrom->active = 0;
                    }
                    if ($partFrom->save()) {
                        Yii::$app->notifier->addNotification(Yii::t('common', 'Запрос на перемещение из партии добавлен.'), 'success');
                        $transaction->commit();
                        return $this->redirect(Url::toRoute(['index', Html::getInputName(new StoragePartSearch(), 'storage_id') => $partMove->storagePartFrom->storage_id]));
                    } else {
                        Yii::$app->notifier->addNotificationsByModel($partFrom);
                        $transaction->rollBack();
                    }
                } else {
                    Yii::$app->notifier->addNotificationsByModel($partMove);
                    $transaction->rollBack();
                }
            } catch (Exception $e) {
                Yii::$app->notifier->addNotification($e->getMessage(), Notifier::TYPE_DANGER);
                $transaction->rollBack();
            }
        }

        return $this->render('move', [
            'model' => $partMove,
            'storages' => StoragePart::getCollectionStoragesAllCountries(),
        ]);
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     * @throws ForbiddenHttpException
     */
    public function actionRemove($id = null)
    {
        $partMove = new StoragePartMoves();
        if ($id) {
            $model = $this->findModel($id);

            if ($model->expected || $model->unlimit) {
                throw new ForbiddenHttpException(Yii::t('common', 'Запрешено для запланированной/безлимитной партии.'));
            }

            $partMove->storage_part_from = $model->id;
            $partMove->product_id = $model->product_id;
            $partMove->user_from = Yii::$app->user->id;
            $partMove->type = StoragePartMoves::REMOVE;
        }

        if ($partMove->load(Yii::$app->request->post())) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                if ($partMove->save()) {
                    $partFrom = $partMove->storagePartFrom;
                    $partFrom->balance -= $partMove->quantity;
                    if ($partFrom->balance == 0) {
                        $partFrom->active = 0;
                    }
                    if ($partFrom->save()) {
                        Yii::$app->notifier->addNotification(Yii::t('common', 'Списание из партии прошло успешно.'), 'success');
                        $transaction->commit();
                        return $this->redirect(Url::toRoute(['index', Html::getInputName(new StoragePartSearch(), 'storage_id') => $partMove->storagePartFrom->storage_id]));
                    } else {
                        Yii::$app->notifier->addNotificationsByModel($partFrom);
                        $transaction->rollBack();
                    }
                } else {
                    Yii::$app->notifier->addNotificationsByModel($partMove);
                    $transaction->rollBack();
                }
            } catch (Exception $e) {
                Yii::$app->notifier->addNotification($e->getMessage(), Notifier::TYPE_DANGER);
                $transaction->rollBack();
            }
        }

        return $this->render('remove', [
            'model' => $partMove,
        ]);
    }

    /**
     * @param integer $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     * @throws ForbiddenHttpException
     */
    public function actionConfirmMove($id)
    {
        $partMove = $this->findPartMove($id);
        $partMove->setScenario(StoragePartMoves::SCENARIO_IGNORE_VALIDATE_QUANTITY);
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $storagePartFrom = $partMove->storagePartFrom;
            $model = new StoragePart();
            $model->price_purchase = $storagePartFrom->price_purchase;
            $model->logistics_delivery_storage = $storagePartFrom->logistics_delivery_storage;
            $model->logistics_customs = $storagePartFrom->logistics_customs;
            $model->logistics_certification = $storagePartFrom->logistics_certification;
            $model->logistics_other = $storagePartFrom->logistics_other;
            $model->number = StoragePart::getMaxNumber($partMove->storage_to);
            $model->quantity = $partMove->quantity;
            $model->product_id = $partMove->product_id;
            $model->storage_id = $partMove->storage_to;
            $model->balance = $partMove->quantity;
            $model->active = 1;
            $usd = Currency::findOne(['char_code' => CurrencyComponent::DEFAULT_CURRENCY]);
            $model->currency_id = $usd->id;

            if ($model->save()) {
                $partMove->storage_part_to = $model->id;
                $partMove->user_to = Yii::$app->user->id;
                if ($partMove->save()) {
                    Yii::$app->notifier->addNotification(Yii::t('common', 'Добавление товара на склад прошло успешно.'), 'success');
                    $transaction->commit();
                } else {
                    Yii::$app->notifier->addNotificationsByModel($partMove);
                    $transaction->rollBack();
                }
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
                $transaction->rollBack();
            }
            return $this->redirect(Url::toRoute(['index', Html::getInputName(new StoragePartSearch(), 'storage_id') => $model->storage_id]));
            
        } catch (Exception $e) {
            Yii::$app->notifier->addNotification($e->getMessage(), Notifier::TYPE_DANGER);
            $transaction->rollBack();
        }
    }

    /**
     * @param integer $id
     * @return StoragePart
     * @throws NotFoundHttpException
     */
    private function findModel($id)
    {
        $model = StoragePart::findOne($id);

        if (!$model) {
            throw new NotFoundHttpException(Yii::t('common', 'Партия не найдена.'));
        }

        return $model;
    }

    /**
     * @param integer $id
     * @return StoragePartMoves
     * @throws NotFoundHttpException
     * @throws ForbiddenHttpException
     */
    private function findPartMove($id)
    {
        $model = StoragePartMoves::findOne($id);

        if (!$model) {
            throw new NotFoundHttpException(Yii::t('common', 'Партия на перемещение не найдена.'));
        }

        if ($model->storage_part_to != null) {
            throw new ForbiddenHttpException(Yii::t('common', 'Партия уже была перемещена'));
        }

        return $model;
    }
}
