<?php
namespace app\modules\storage\controllers;

use app\components\web\Controller;
use app\models\Country;
use app\modules\storage\models\Storage;
use Yii;
use yii\web\BadRequestHttpException;

/**
 * Class CountryStorageController
 * @package app\modules\controllers\storage
 */
class CountryStorageController extends Controller
{
    /**
     * @throws \yii\web\HttpException
     * @return string|array
     */
    public function actionGetSelectOptionsStorage()
    {
        $countryId = (int)Yii::$app->request->post('country_id');

        $country = Country::findOne($countryId);

        if (!$country) {
            throw new BadRequestHttpException(Yii::t('common', 'Обращение к несуществущей стране.'));
        }

        $resultString = '<option value="">—</option>';

        $storages = Storage::find()
            ->byCountryId($countryId)
            ->orderBy('name')
            ->collection();

        if (count($storages) > 0) {
            foreach ($storages as $key => $value) {
                $resultString .= '<option value="' . $key . '">' . $value . '</option>';
            }
            return $resultString;
        }

        return [
            'error' => true
        ];
    }
}