<?php
namespace app\modules\storage\controllers;

use app\models\logs\TableLog;
use app\components\web\Controller;
use app\modules\callcenter\models\CallCenter;
use app\modules\delivery\models\Delivery;
use app\modules\storage\models\search\StorageSearch;
use app\modules\storage\models\Storage;
use Yii;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

/**
 * Class ControlController
 * @package app\modules\controllers\storage
 */
class ControlController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new StorageSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView(int $id)
    {
        $changeLog = TableLog::find()->byTable(Storage::clearTableName())->byID($id)->allSorted();

        return $this->render('view', [
            'changeLog' => $changeLog,
        ]);
    }

    /**
     * @param null $id
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new Storage();
        }

        if ($model->load(Yii::$app->request->post())) {
            $isNewRecord = $model->isNewRecord;

            $model->country_id = Yii::$app->user->country->id;

            if ($model->save()) {
                Yii::$app->notifier->addNotification($isNewRecord ? Yii::t('common', 'Склад успешно добавлен.') : Yii::t('common', 'Склад успешно сохранен.'), 'success');

                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }

        return $this->render('edit', [
            'model' => $model,
            'callCenters' => CallCenter::find()
                ->byCountryId(Yii::$app->user->country->id)
                ->collection(),
            'deliveries' => Delivery::find()
                ->byCountryId(Yii::$app->user->country->id)
                ->collection(),
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionDefault($id)
    {
        $model = $this->getModel($id);
        Storage::updateAll(['default' => 0], ['country_id' => $model->country_id]);

        $model->default = 1;

        if ($model->save(true, ['default'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Склад по умолчанию успешно выбран.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param integer $id
     * @return Storage
     * @throws NotFoundHttpException
     */
    private function getModel($id)
    {
        $model = Storage::findOne($id);

        if (!$model) {
            throw new NotFoundHttpException(Yii::t('common', 'Склад не найден.'));
        }

        return $model;
    }
}
