<?php
namespace app\modules\storage\controllers;

use app\components\web\Controller;
use app\modules\storage\models\Storage;
use app\modules\storage\models\StorageProduct;
use Yii;
use yii\web\BadRequestHttpException;

/**
 * Class StorageProductController
 * @package app\modules\controllers\storage
 */
class StorageProductController extends Controller
{
    /**
     * @throws \yii\web\HttpException
     * @return string|array
     */
    public function actionGetSelectOptionsProduct()
    {
        $storageId = (int)Yii::$app->request->post('storage_id');

        $storage = Storage::findOne($storageId);

        if (!$storage) {
            throw new BadRequestHttpException(Yii::t('common', 'Обращение к несуществующему складу.'));
        }

        $resultString = '<option value="">—</option>';

        $products = StorageProduct::find()
            ->joinWith(['product'])
            ->byStorageId($storageId)
            ->orderBy('name')
            ->groupBy('product_id')
            ->all();

        if (count($products) > 0) {
            foreach ($products as $product) {
                $resultString .= '<option value="' . $product->product->id . '">' . $product->product->name . '</option>';
            }
            return $resultString;
        }

        return [
            'error' => true
        ];
    }

    /**
     * @throws \yii\web\HttpException
     * @return string|array
     */
    public function actionGetSelectOptionsShelfLife()
    {
        $storageId = (int)Yii::$app->request->post('storage_id');

        $storage = Storage::findOne($storageId);

        if (!$storage) {
            throw new BadRequestHttpException(Yii::t('common', 'Обращение к несуществующему складу.'));
        }

        $productId = (int)Yii::$app->request->post('product_id');

        if (!$productId) {
            throw new BadRequestHttpException(Yii::t('common', 'Продукт не выбран.'));
        }

        $resultString = '';

        $products = StorageProduct::find()
            ->byStorageId($storageId)
            ->byProductId($productId)
            ->orderBy('shelf_life')
            ->groupBy('shelf_life')
            ->all();

        if (count($products) > 0) {
            foreach ($products as $product) {
                if ($product->shelf_life) {
                    $resultString .= '<option value="' . $product->shelf_life . '">' . $product->shelf_life . '</option>';
                } else {
                    $resultString .= '<option value="">—</option>';
                }
            }
            return $resultString;
        }

        return [
            'error' => true
        ];
    }
}