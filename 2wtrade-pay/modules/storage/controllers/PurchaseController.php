<?php

namespace app\modules\storage\controllers;

use app\components\web\Controller;
use app\models\logs\TableLog;
use app\modules\storage\models\search\StorageProductPurchaseSearch;
use app\modules\storage\models\StorageProductPurchase;
use app\models\Product;
use Yii;
use yii\web\NotFoundHttpException;
use yii\helpers\Url;

/**
 * Class PurchaseController
 * @package app\modules\controllers\storage
 */
class PurchaseController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new StorageProductPurchaseSearch();

        $params = Yii::$app->request->queryParams;

        $dataProvider = $modelSearch->search($params, $groupByProduct = true);

        return $this->render('index', [
            'modelSearch' => $modelSearch,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param int $id
     * @return string
     */
    public function actionLog(int $id)
    {
        $change = TableLog::find()->byTable(StorageProductPurchase::clearTableName())->byID($id)->allSorted();
        return $this->render('log', [
            'changeLog' => $change,
        ]);
    }

    /**
     * @param null $id
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new StorageProductPurchase();
        }

        if ($model->load(Yii::$app->request->post())) {
            $isNewRecord = $model->isNewRecord;

            $model->last_purchase_date = Yii::$app->formatter->asTimestamp($model->last_purchase_date);
            $model->country_id = Yii::$app->user->country->id;

            if ($model->save()) {
                Yii::$app->notifier->addNotification($isNewRecord ? Yii::t('common', 'Закупка успешно добавлена.') : Yii::t('common', 'Закупка успешно сохранена.'), 'success');

                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }

        return $this->render('edit', [
            'model' => $model,
            'products' => Product::find()->collection(),
        ]);
    }

    /**
     * @param integer $id
     * @return StorageProductPurchase
     * @throws NotFoundHttpException
     */
    private function getModel($id)
    {
        $model = StorageProductPurchase::findOne($id);

        if (!$model) {
            throw new NotFoundHttpException(Yii::t('common', 'Закупка не найдена.'));
        }

        return $model;
    }
}