<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;
use app\modules\storage\models\Storage;
use app\models\Product;

/** @var app\modules\storage\models\StorageDocument $model */
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['index', 'id' => $model->id])]); ?>
    <div class="row">
        <div class="col-lg-3">
            <div class="form-group">
                <?= $form->field($model, 'storage_id_to')->select2List(Storage::getCollectionStorages(), ['length' => false]);
                ?>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <?= $form->field($model, 'product_id')->select2List(Product::getCollectionProducts(), ['length' => false]);
                ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-2">
            <div class="form-group">
                <?= $form->field($model, 'quantity')->textInput() ?>
            </div>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'shelf_life')->datePicker([
                    'showTime' => false,
                    'minDate' => date('Y-m-d'),
                    'format' => 'YYYY-MM-DD'
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="form-group">
                <?= $form->field($model, 'comment')->textarea() ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <?= $form->submit(Yii::t('common', 'Добавить документ на добавление товара')) ?>
            <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>