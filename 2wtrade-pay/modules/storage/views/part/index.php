<?php
use app\components\grid\ActionColumn;
use app\components\grid\DateColumn;
use app\components\grid\GridView;
use app\helpers\DataProvider;
use app\modules\storage\models\StoragePart;
use app\widgets\assets\custom\ModalConfirmDeleteAsset;
use app\widgets\ButtonLink;
use app\widgets\custom\ModalConfirmDelete;
use app\widgets\Panel;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use app\modules\storage\models\StoragePartMoves;
use app\widgets\Label;

/** @var \yii\web\View $this */
/** @var \app\modules\storage\models\search\StoragePartSearch $modelSearch */
/** @var \app\modules\storage\models\search\StoragePartMovesSearch $modelSearchMoves */
/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var \yii\data\ActiveDataProvider $dataProviderMoves */
/** @var string $storageName */

ModalConfirmDeleteAsset::register($this);

$this->title = Yii::t('common', 'Складские партии на складе {name}', ['name' => $storageName]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Склады'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'collapse' => true,
    'content' => $this->render('_index-filter', [
        'modelSearch' => $modelSearch,
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Запросы на добавление на склад'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProviderMoves,
        'columns' => [
            [
                'attribute' => 'quantity',
                'label' => Yii::t('common', 'Количество'),
                'enableSorting' => false,
            ],
            [
                'attribute' => 'product.name',
                'label' => Yii::t('common', 'Товар'),
                'enableSorting' => false,
            ],
            [
                'attribute' => 'created_at',
                'label' => Yii::t('common', 'Дата создания'),
                'enableSorting' => false,
                'class' => DateColumn::className(),
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Принять на склад'),
                        'url' => function ($model) {
                            return Url::toRoute(['/storage/part/confirm-move', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('storage.part.confirmmove');
                        },
                    ],
                ]
            ]
        ],
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с партиями'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'number',
                'label' => '#',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'storage.name',
                'label' => Yii::t('common', 'Склад'),
                'enableSorting' => false,
            ],
            [
                'attribute' => 'product.name',
                'label' => Yii::t('common', 'Товар'),
                'enableSorting' => false,
            ],
            [
                'attribute' => 'balance',
                'label' => Yii::t('common', 'Баланс'),
                'enableSorting' => false,
            ],
            [
                'attribute' => 'quantity',
                'label' => Yii::t('common', 'Количество'),
                'enableSorting' => false,
            ],
            [
                'attribute' => 'price_purchase',
                'label' => Yii::t('common', 'Цена закупки'),
                'enableSorting' => false,
            ],
            [
                'attribute' => 'logistics_delivery_storage',
                'label' => Yii::t('common', 'Логистика: доставка и склад'),
                'enableSorting' => false,
            ],
            [
                'attribute' => 'logistics_customs',
                'label' => Yii::t('common', 'Логистика: таможня'),
                'enableSorting' => false,
            ],
            [
                'attribute' => 'logistics_certification',
                'label' => Yii::t('common', 'Логистика: сертификация'),
                'enableSorting' => false,
            ],
            [
                'attribute' => 'logistics_other',
                'label' => Yii::t('common', 'Логистика: прочее'),
                'enableSorting' => false,
            ],
            [
                'attribute' => 'created_at',
                'label' => Yii::t('common', 'Дата создания'),
                'enableSorting' => false,
                'class' => DateColumn::className(),
            ],
            [
                'label' => Yii::t('common', 'Запрос на перемещение'),
                'headerOptions' => ['class' => 'width-150 text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    $value = StoragePartMoves::getPartMove($model->id);
                    return Label::widget([
                        'label' => $value ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $value ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'label' => Yii::t('common', 'Запланированная'),
                'headerOptions' => ['class' => 'width-150 text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return Label::widget([
                        'label' => $model->expected ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->expected ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/storage/part/edit', 'id' => $model->id, Html::getInputName(new StoragePart(), 'storage_id') => $model->storage_id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('storage.part.edit') && ($model->quantity == $model->balance);
                        },
                    ],
                    [
                        'label' => Yii::t('common', 'Переместить из партии'),
                        'url' => function ($model) {
                            return Url::toRoute(['/storage/part/move', 'id' => $model->id, Html::getInputName(new StoragePart(), 'storage_id') => $model->storage_id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('storage.part.move') && ($model->balance > 0) && (!$model->expected) && (!$model->unlimit);
                        },
                    ],
                    [
                        'label' => Yii::t('common', 'Списать из партии'),
                        'url' => function ($model) {
                            return Url::toRoute(['/storage/part/remove', 'id' => $model->id, Html::getInputName(new StoragePart(), 'storage_id') => $model->storage_id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('storage.part.remove') && ($model->balance > 0) && (!$model->expected) && (!$model->unlimit);
                        },
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить'),
                        'url' => function () {
                            return '#';
                        },
                        'style' => 'confirm-delete-link',
                        'attributes' => function ($model) {
                            return [
                                'data-href' => Url::toRoute(['/storage/part/delete', 'id' => $model->id]),
                            ];
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('storage.part.delete') && ($model->quantity == $model->balance);
                        },
                    ],
                ]
            ]
        ],
    ]),
    'footer' => (Yii::$app->user->can('storage.part.edit') ?
            ButtonLink::widget([
                'label' => Yii::t('common', 'Добавить партию'),
                'url' => Url::toRoute(['/storage/part/edit', Html::getInputName(new StoragePart(), 'storage_id') => $modelSearch->storage_id]),
                'style' => ButtonLink::STYLE_SUCCESS,
                'size' => ButtonLink::SIZE_SMALL,
            ]) : '') . LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>

<?= ModalConfirmDelete::widget() ?>
