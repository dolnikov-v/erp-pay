<?php
use app\components\widgets\ActiveForm;
use app\modules\storage\models\search\StoragePartSearch;
use yii\helpers\Url;
use yii\helpers\Html;

/** @var \app\modules\storage\models\StoragePartMoves $model */
/** @var \app\components\widgets\ActiveForm $form */
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute(['remove', 'id' => $model->id]),
]); ?>
<div class="row">
    <div class="col-lg-12">
        <div class="form-group">
            <?= $form->field($model, 'quantity')->textInput() ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="form-group">
            <?= $form->field($model, 'comment')->textarea() ?>
        </div>
    </div>
</div>
<div class="row hidden">
    <div class="col-lg-12">
        <div class="form-group">
            <?= $form->field($model, 'storage_part_from')->hiddenInput() ?>
        </div>
    </div>
</div>
<div class="row hidden">
    <div class="col-lg-12">
        <div class="form-group">
            <?= $form->field($model, 'product_id')->hiddenInput() ?>
        </div>
    </div>
</div>
<div class="row hidden">
    <div class="col-lg-12">
        <div class="form-group">
            <?= $form->field($model, 'type')->hiddenInput() ?>
        </div>
    </div>
</div>
<div class="row hidden">
    <div class="col-lg-12">
        <div class="form-group">
            <?= $form->field($model, 'user_from')->hiddenInput() ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= $form->submit(Yii::t('common', 'Списать из партии')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute(['index', Html::getInputName(new StoragePartSearch(), 'storage_id') => $model->storagePartFrom->storage_id])) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
