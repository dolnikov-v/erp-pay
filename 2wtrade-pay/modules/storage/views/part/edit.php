<?php
use app\widgets\Panel;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var app\modules\storage\models\StoragePart $model */
/** @var string $storageName */
/** @var array $products */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление складской партии') : Yii::t('common', 'Редактирование складской партии');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Склады'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Управление складами'), 'url' => Url::toRoute('/storage/control/index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Складская партия (валюта USD)'),
    'alert' => $model->isNewRecord ? Yii::t('common', 'Складская партия будет добавлена и привязана к активному складу - {storage}.', [
        'storage' => $storageName,
    ]) : '',
    'content' => $this->render('_edit-form', [
        'model' => $model,
        'products' => $products,
    ])
]) ?>
