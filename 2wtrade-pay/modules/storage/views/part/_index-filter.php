<?php
use app\components\widgets\ActiveForm;
use app\modules\storage\models\StoragePart;
use yii\helpers\Url;
use yii\helpers\Html;

/** @var \app\modules\storage\models\search\StoragePartSearch $modelSearch */
/** @var app\components\widgets\ActiveForm $form */
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute('index'),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>
    <div class="row">
        <div class="col-lg-4">
            <div class="form-group">
                <?= $form->field($modelSearch, 'product_id')->select2List(StoragePart::getCollectionProductsOnStorages(), [
                    'prompt' => '—',
                ]) ?>
            </div>
        </div>
        <div class="hidden">
            <div class="form-group">
                <?= $form->field($modelSearch, 'storage_id')->hiddenInput(['value' => $modelSearch->storage_id]) ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->submit(Yii::t('common', 'Применить')); ?>
            <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute(['index', Html::getInputName($modelSearch, 'storage_id') => $modelSearch->storage_id])); ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>
