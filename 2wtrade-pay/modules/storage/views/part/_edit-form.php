<?php
use app\components\widgets\ActiveForm;
use app\modules\storage\models\search\StoragePartSearch;
use yii\helpers\Url;
use yii\helpers\Html;

/** @var \app\modules\storage\models\StoragePart $model */
/** @var \app\components\widgets\ActiveForm $form */
/** @var array $products */
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute(['edit', 'id' => $model->id]),
]); ?>
<div class="row">
    <div class="col-lg-12">
        <div class="form-group">
            <?= $form->field($model, 'number')->textInput() ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="form-group">
            <?= $form->field($model, 'product_id')->select2List($products) ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="form-group">
            <?= $form->field($model, 'quantity')->textInput() ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="form-group">
            <?= $form->field($model, 'price_purchase')->textInput() ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="form-group">
            <?= $form->field($model, 'logistics_delivery_storage')->textInput() ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="form-group">
            <?= $form->field($model, 'logistics_customs')->textInput() ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="form-group">
            <?= $form->field($model, 'logistics_certification')->textInput() ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="form-group">
            <?= $form->field($model, 'logistics_other')->textInput() ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="form-group">
            <?= $form->field($model, 'expected_at')->datePicker() ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="form-group">
            <?= $form->field($model, 'shelf_life')->datePicker([
                'showTime' => false,
                'minDate' => date('Y-m-d'),
                'format' => 'YYYY-MM-DD',
                'value' => Yii::$app->formatter->asDate($model->shelf_life, 'yyyy-MM-dd'),
                'type' => 'month',
            ]) ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="form-group">
            <?= $form->field($model, 'expected')->checkboxCustom([
                'value' => 1,
                'checked' => $model->expected
            ]) ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="form-group">
            <?= $form->field($model, 'unlimit')->checkboxCustom([
                'value' => 1,
                'checked' => $model->unlimit
            ]) ?>
        </div>
    </div>
</div>
<div class="row hidden">
    <div class="col-lg-12">
        <div class="form-group">
            <?= $form->field($model, 'storage_id')->hiddenInput() ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить партию') : Yii::t('common', 'Сохранить партию')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute(['index', Html::getInputName(new StoragePartSearch(), 'storage_id') => $model->storage_id])) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
