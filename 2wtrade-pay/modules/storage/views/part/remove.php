<?php
use app\widgets\Panel;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var app\modules\storage\models\StoragePartMoves $model */

$this->title = Yii::t('common', 'Списание из складской партии');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Склады'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Управление складами'), 'url' => Url::toRoute('/storage/control/index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Максимально для списания') . ': ' . $model->storagePartFrom->balance . ' ' . $model->storagePartFrom->product->name. ' (' . $model->storagePartFrom->storage->name . ')',
    'content' => $this->render('_remove-form', [
        'model' => $model,
    ])
]) ?>
