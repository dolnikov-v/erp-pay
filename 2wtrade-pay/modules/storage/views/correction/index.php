<?php
use app\widgets\Panel;

/** @var yii\web\View $this */
/** @var app\modules\storage\models\StorageDocument $model */

$this->title = Yii::t('common', 'Добавление документа на корректировку');
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Документ на корректировку'),
    'alert' => Yii::t('common', 'Только для складов, не использующих сканер штрих-кодов'),
    'content' => $this->render('_index-form', [
        'model' => $model,
    ])
]) ?>