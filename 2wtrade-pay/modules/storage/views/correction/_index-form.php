<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/** @var app\modules\storage\models\StorageDocument $model */
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['index', 'id' => $model->id])]); ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="form-group">
                <?= $form->field($model, 'storage_id_to', [
                    'labelOptions' => ['label' => false]
                ])->storageProduct('storage_id_to', 'product_id', $model->storage_id_to, $model->product_id, true, 'shelf_life');
                ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-2">
            <div class="form-group">
                <?= $form->field($model, 'quantity')->textInput() ?>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="form-group">
                <?= $form->field($model, 'isNegative')->checkboxCustom([
                    'value' => 1,
                    'checked' => $model->isNegative
                ]) ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="form-group">
                <?= $form->field($model, 'comment')->textarea() ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <?= $form->submit(Yii::t('common', 'Добавить документ на корректировку')) ?>
            <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>