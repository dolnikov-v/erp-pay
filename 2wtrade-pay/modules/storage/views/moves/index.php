<?php
use app\components\grid\ActionColumn;
use app\components\grid\DateColumn;
use app\components\grid\GridView;
use app\helpers\DataProvider;
use app\widgets\Panel;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \yii\data\ActiveDataProvider $dataProvider */


$this->title = Yii::t('common', 'Запросы на перемещение из партий');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Склады'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Запросы на перемещение из партий'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'quantity',
                'label' => Yii::t('common', 'Количество'),
                'enableSorting' => false,
            ],
            [
                'attribute' => 'product.name',
                'label' => Yii::t('common', 'Товар'),
                'enableSorting' => false,
            ],
            [
                'attribute' => 'created_at',
                'label' => Yii::t('common', 'Дата создания'),
                'enableSorting' => false,
                'class' => DateColumn::className(),
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Отменить'),
                        'url' => function ($model) {
                            return Url::toRoute(['/storage/moves/cancel-move', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('storage.moves.cancelmove');
                        },
                    ],
                ]
            ]
        ],
    ]),
]) ?>
