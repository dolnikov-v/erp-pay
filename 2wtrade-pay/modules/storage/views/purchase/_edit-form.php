<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/** @var app\modules\storage\models\StorageProductPurchase $model */
/** @var array $products */
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['edit', 'id' => $model->id])]); ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="form-group">
                <?= $form->field($model, 'product_id')->select2List($products, [
                    'prompt' => '—',
                    'length' => false
                ]) ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="form-group">
                <?= $form->field($model, 'plan_lead_day')->textInput() ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="form-group">
                <?= $form->field($model, 'limit_days')->textInput() ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="form-group">
                <?= $form->field($model, 'last_purchase_count')->textInput() ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-2">
            <div class="form-group">
                <?= $form->field($model, 'last_purchase_date')->datePicker() ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить закупку') : Yii::t('common', 'Сохранить закупку')) ?>
            <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>
