<?php

use app\components\grid\GridView;
use app\helpers\DataProvider;
use app\widgets\Panel;
use yii\widgets\LinkPager;
use app\widgets\ButtonLink;
use yii\helpers\Url;
use app\components\grid\DateColumn;
use app\components\grid\ActionColumn;
use app\modules\storage\models\StorageProductPurchase;
use app\widgets\assets\StoragePurchaseAsset;

/** @var \yii\web\View $this */
/** @var \app\modules\storage\models\search\StorageProductPurchaseSearch $modelSearch */
/** @var \yii\data\ArrayDataProvider $dataProvider */

StoragePurchaseAsset::register($this);

$this->title = Yii::t('common', 'Закупки');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Склады'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с закупками'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function ($model) {
            return StorageProductPurchase::check($model);
        },
        'columns' => [
            [
                'attribute' => 'product_name',
                'label' => Yii::t('common', 'Товар'),
                'contentOptions' => ['style' => 'vertical-align:middle;'],
            ],
            [
                'attribute' => 'plan_lead_day',
                'label' => Yii::t('common', 'Планируемое количество лидов в день'),
                'enableSorting' => false,
                'contentOptions' => ['style' => 'vertical-align:middle;'],
            ],
            [
                'attribute' => 'orders',
                'label' => Yii::t('common', 'Фактическое количество лидов в день'),
                'enableSorting' => false,
                'contentOptions' => ['style' => 'vertical-align:middle;'],
            ],
            [
                'attribute' => 'shelf_life',
                'label' => Yii::t('common', 'Срок годности'),
                'enableSorting' => false,
                'value' => function ($model) {
                    $shelf_life = explode(',', $model['shelf_life']);
                    foreach ($shelf_life as &$item) {
                        if (strlen($item) > 3) {
                            $item = Yii::$app->formatter->asDate($item, 'dd.MM.yyyy');
                        }
                    }
                    sort($shelf_life, SORT_NUMERIC);
                    return implode('<hr>', $shelf_life);
                },
                'format' => 'raw',
                'contentOptions' => ['style' => 'text-align:center; vertical-align:middle;'],
            ],
            [
                'attribute' => 'balance',
                'label' => Yii::t('common', 'Баланс'),
                'value' => function ($model) {
                    return str_replace(',', '<hr>', $model['balance']);
                },
                'format' => 'raw',
                'contentOptions' => ['style' => 'vertical-align:middle;'],
            ],
            [
                'value' => function ($model) {
                    if (!empty(floatval($model['days_by_plan_lead_day']))) {
                        return $model['days_by_plan_lead_day'];
                    }
                    return null;
                },
                'label' => Yii::t('common', 'Планируемый остаток в днях'),
                'enableSorting' => false,
                'contentOptions' => ['style' => 'vertical-align:middle;'],
            ],
            [
                'value' => function ($model) {
                    if (!empty(floatval($model['days_by_orders']))) {
                        return $model['days_by_orders'];
                    }
                    return null;
                },
                'label' => Yii::t('common', 'Фактический остаток в днях'),
                'enableSorting' => false,
                'contentOptions' => ['style' => 'vertical-align:middle;'],
            ],
            [
                'attribute' => 'limit_days',
                'label' => Yii::t('common', 'Порог в днях'),
                'enableSorting' => false,
                'contentOptions' => ['style' => 'vertical-align:middle;'],
            ],
            [
                'attribute' => 'last_purchase_count',
                'label' => Yii::t('common', 'Количество заказанного товара'),
                'enableSorting' => false,
                'contentOptions' => ['style' => 'vertical-align:middle;'],
            ],
            [
                'attribute' => 'last_purchase_date',
                'label' => Yii::t('common', 'Дата заказа товара'),
                'enableSorting' => false,
                'class' => DateColumn::className(),
                'value' => function ($model) {
                    if (empty($model['last_purchase_date'])) {
                        return null;
                    }
                    return $model['last_purchase_date'];
                },
                'contentOptions' => ['style' => 'vertical-align:middle;'],
            ],
            [
                'attribute' => 'created_at',
                'label' => Yii::t('common', 'Дата создания'),
                'enableSorting' => false,
                'class' => DateColumn::className(),
                'contentOptions' => ['style' => 'vertical-align:middle;'],
            ],
            [
                'attribute' => 'updated_at',
                'label' => Yii::t('common', 'Дата изменения'),
                'enableSorting' => false,
                'class' => DateColumn::className(),
                'contentOptions' => ['style' => 'vertical-align:middle;'],
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/storage/purchase/edit', 'id' => $model['id']]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('storage.purchase.edit');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Просмотр изменений'),
                        'url' => function ($model) {
                            return Url::toRoute(['/storage/purchase/log', 'id' => $model['id']]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('storage.purchase.log');
                        }
                    ],
                ],
                'contentOptions' => ['style' => 'vertical-align:middle;'],
            ]
        ],
    ]),
    'footer' => (Yii::$app->user->can('storage.purchase.edit') ?
            ButtonLink::widget([
                'label' => Yii::t('common', 'Добавить закупку'),
                'url' => Url::toRoute('/storage/purchase/edit'),
                'style' => ButtonLink::STYLE_SUCCESS,
                'size' => ButtonLink::SIZE_SMALL,
            ]) : '') . LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>
