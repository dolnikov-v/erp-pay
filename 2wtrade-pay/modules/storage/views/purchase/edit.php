<?php
use app\widgets\Panel;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var app\modules\storage\models\StorageProductPurchase $model */
/** @var array $products */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление закупки') : Yii::t('common', 'Редактирование закупки');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Склады'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Закупки'), 'url' => Url::toRoute('/storage/control/index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Закупка'),
    'alert' => $model->isNewRecord ? Yii::t('common', 'Закупка будет привязана к активной стране - {country}.', [
        'country' => Yii::$app->user->country->name,
    ]) : '',
    'content' => $this->render('_edit-form', [
        'model' => $model,
        'products' => $products,
    ])
]) ?>
