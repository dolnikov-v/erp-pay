<?php

use app\widgets\Log;
use yii\helpers\Url;

/**
 * @var \app\models\logs\TableLog[] $changeLog
 */

$this->title = Yii::t('common', 'Лог изменений');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Таблица с остатками'), 'url' => Url::toRoute(['index'])];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Log::widget([
    'title' => Yii::t('common', 'История изменения'),
    'data' => $changeLog,
]); ?>