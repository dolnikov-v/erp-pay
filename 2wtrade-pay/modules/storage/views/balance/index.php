<?php
use app\components\grid\GridView;
use app\helpers\DataProvider;
use app\widgets\Panel;
use yii\widgets\LinkPager;
use app\widgets\Label;
use yii\helpers\Url;
use app\components\grid\ActionColumn;
use app\components\grid\DateColumn;
use app\widgets\InputText;
use app\modules\storage\assets\StorageBalanceAsset;
use app\widgets\custom\ModalConfirmDelete;
use app\widgets\assets\custom\ModalConfirmDeleteAsset;

/** @var \yii\web\View $this */
/** @var \app\modules\storage\models\search\StorageProductSearch $modelSearch */
/** @var \yii\data\ArrayDataProvider $dataProvider */

$this->title = Yii::t('common', 'Складские остатки');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Склады'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

StorageBalanceAsset::register($this);
ModalConfirmDeleteAsset::register($this);
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'collapse' => true,
    'content' => $this->render('_index-filter', [
        'modelSearch' => $modelSearch,
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с остатками'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'storage.name',
                'label' => Yii::t('common', 'Склад'),
            ],
            [
                'attribute' => 'storage.delivery.name',
                'label' => Yii::t('common', 'Служба доставки'),
            ],
            [
                'attribute' => 'product.name',
                'label' => Yii::t('common', 'Товар'),
            ],
            [
                'attribute' => 'balance_sub',
                'label' => Yii::t('common', 'Списание'),
                'enableSorting' => false,
                'content' => function ($model) {
                    return InputText::widget([
                        'value' => '',
                        'size' => 'balance_sub',
                        'attributes' => [
                            'data-id' => $model->id,
                        ]
                    ]);
                },
                'visible' => Yii::$app->user->can('storage.balance.change')
            ],
            [
                'attribute' => 'balance',
                'label' => Yii::t('common', 'Баланс'),
                'contentOptions' => ['class' => 'current-balance'],
            ],
            [
                'attribute' => 'balance_add',
                'label' => Yii::t('common', 'Пополнение'),
                'enableSorting' => false,
                'content' => function ($model) {
                    return InputText::widget([
                        'value' => '',
                        'size' => 'balance_add',
                        'attributes' => [
                            'data-id' => $model->id,
                        ]
                    ]);
                },
                'visible' => Yii::$app->user->can('storage.balance.change')
            ],
            [
                'attribute' => 'unlimit',
                'headerOptions' => ['class' => 'width-150 text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return Label::widget([
                        'label' => $model->unlimit ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->unlimit ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'shelf_life',
                'value' => function ($model) {
                    return Yii::$app->formatter->asDate($model->shelf_life, 'dd.MM.yyyy');
                }
            ],
            [
                'attribute' => 'reserve',
            ],
            [
                'attribute' => 'updated_at',
                'label' => Yii::t('common', 'Дата изменения'),
                'class' => DateColumn::className(),
                'contentOptions' => ['class' => 'text-center updated_at'],
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Сделать безлимитным'),
                        'url' => function ($model) {
                            return Url::toRoute(['/storage/balance/unlimit', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('storage.balance.unlimit') && !$model->unlimit;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Сделать лимитным'),
                        'url' => function ($model) {
                            return Url::toRoute(['/storage/balance/limit', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('storage.balance.limit') && $model->unlimit;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Просмотр изменений'),
                        'url' => function ($model) {
                            return Url::toRoute(['/storage/balance/log', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('storage.balance.log');
                        }
                    ],
                    [
                        'can' => function ($model) {
                            return Yii::$app->user->can('storage.balance.delete');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить'),
                        'url' => function () {
                            return '#';
                        },
                        'style' => 'confirm-delete-link',
                        'attributes' => function ($data) {
                            return [
                                'data-href' => Url::toRoute(['delete', 'id' => $data['id']]),
                            ];
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('storage.balance.delete');
                        }
                    ],
                ],
            ],
        ],
    ]),
    'footer' => LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>

<?= ModalConfirmDelete::widget() ?>