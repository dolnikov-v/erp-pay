<?php
use app\components\widgets\ActiveForm;
use app\modules\storage\models\Storage;
use app\modules\storage\models\StorageProduct;
use app\widgets\assets\Select2Asset;
use yii\helpers\Url;

/** @var \app\modules\storage\models\search\StorageProductSearch $modelSearch */

Select2Asset::register($this);
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute('index'),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>
    <div class="row">
        <div class="col-lg-3">
            <div class="form-group">
                <?= $form->field($modelSearch, 'storage_id')->select2List(Storage::getCollectionStorages(), [
                    'prompt' => '—',
                    'length' => false
                ]) ?>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <?= $form->field($modelSearch, 'delivery_id')->select2List(Storage::getCollectionDeliveries(), [
                    'prompt' => '—',
                    'length' => false
                ])->label(Yii::t('common', 'Служба доставки')) ?>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <?= $form->field($modelSearch, 'product_id')->select2List(StorageProduct::getCollectionProducts(), [
                    'prompt' => '—',
                    'length' => false
                ]) ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->submit(Yii::t('common', 'Применить')); ?>
            <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')); ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>