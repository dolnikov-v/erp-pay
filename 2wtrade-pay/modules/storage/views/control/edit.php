<?php
use app\widgets\Panel;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var app\modules\storage\models\Storage $model */
/** @var array $callCenters */
/** @var array $deliveries */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление склада') : Yii::t('common', 'Редактирование склада');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Склады'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Управление складами'), 'url' => Url::toRoute('/storage/control/index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Склад'),
    'alert' => $model->isNewRecord ? Yii::t('common', 'Склад будет добавлен и привязан к активной стране - {country}.', [
        'country' => Yii::$app->user->country->name,
    ]) : '',
    'content' => $this->render('_edit-form', [
        'model' => $model,
        'callCenters' => $callCenters,
        'deliveries' => $deliveries,
    ])
]) ?>
