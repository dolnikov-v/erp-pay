<?php
use app\components\grid\ActionColumn;
use app\components\grid\DateColumn;
use app\components\grid\GridView;
use app\helpers\DataProvider;
use app\widgets\ButtonLink;
use app\widgets\Panel;
use app\widgets\Label;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = Yii::t('common', 'Управление');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Склады'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица со складами'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'name',
            [
                'attribute' => 'type',
                'content' => function ($model) {
                    /** @var \app\modules\storage\models\Storage $model */
                    $types = $model::getTypes();
                    return $model->type ? Label::widget([
                        'label' => Html::encode($types[$model->type] ?? ''),
                    ]) : '';
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'call_center_id',
                'content' => function ($model) {
                    return $model->callCenter ? Label::widget([
                        'label' => Html::encode($model->callCenter->name),
                    ]) : '';
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'delivery_id',
                'content' => function ($model) {
                    return $model->delivery ? Label::widget([
                        'label' => Html::encode($model->delivery->name),
                    ]) : '';
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'space',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'default',
                'headerOptions' => ['class' => 'width-150 text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return Label::widget([
                        'label' => $model->default ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->default ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'created_at',
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/storage/control/edit', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('storage.control.edit');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'По умолчанию'),
                        'url' => function ($model) {
                            return Url::toRoute(['/storage/control/default', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('storage.control.default') && !$model->default;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Лог изменений'),
                        'url' => function ($model) {
                            return Url::toRoute(['/storage/control/view', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('storage.control.view') && !$model->default;
                        }
                    ],
                ]
            ]
        ],
    ]),
    'footer' => (Yii::$app->user->can('storage.control.edit') ?
            ButtonLink::widget([
                'label' => Yii::t('common', 'Добавить склад'),
                'url' => Url::toRoute('/storage/control/edit'),
                'style' => ButtonLink::STYLE_SUCCESS,
                'size' => ButtonLink::SIZE_SMALL,
            ]) : '') . LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>
