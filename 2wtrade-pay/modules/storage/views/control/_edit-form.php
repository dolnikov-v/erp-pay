<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/** @var app\modules\storage\models\Storage $model */
/** @var array $callCenters */
/** @var array $deliveries */
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['edit', 'id' => $model->id])]); ?>
    <div class="row">
        <div class="col-lg-8">
            <div class="form-group">
                <?= $form->field($model, 'name')->textInput() ?>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                <?= $form->field($model, 'type')->select2List($model::getTypes(), [
                    'prompt' => '—',
                ]) ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="form-group">
                <?= $form->field($model, 'call_center_id')->select2List($callCenters, [
                    'prompt' => '—',
                ]) ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="form-group">
                <?= $form->field($model, 'delivery_id')->select2List($deliveries, [
                    'prompt' => '—',
                ]) ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="form-group">
                <?= $form->field($model, 'address')->textarea() ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="form-group">
                <?= $form->field($model, 'sender_name')->textarea() ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="form-group">
                <?= $form->field($model, 'sender_city')->textarea() ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="form-group">
                <?= $form->field($model, 'sender_zip')->textarea() ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="form-group">
                <?= $form->field($model, 'sender_phone')->textarea() ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="form-group">
                <?= $form->field($model, 'space')->textInput() ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4">
            <div class="form-group">
                <?= $form->field($model, 'use_bar_code')->checkboxCustom([
                    'value' => 1,
                    'checked' => $model->use_bar_code
                ]) ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="form-group">
                <?= $form->field($model, 'comment')->textarea() ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить склад') : Yii::t('common', 'Сохранить склад')) ?>
            <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>