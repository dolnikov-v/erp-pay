<?php

use app\widgets\Log;
use yii\helpers\Url;

/**
 * @var \app\models\logs\TableLog[] $changeLog
 */

$this->title = Yii::t('common', 'Лог изменений');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Склады'), 'url' => Url::toRoute('/storage/control/index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Log::widget([
    'data' => $changeLog,
]); ?>