<?php
use app\components\widgets\ActiveForm;
use app\modules\storage\models\StoragePart;
use app\modules\storage\models\StoragePartHistory;
use yii\helpers\Url;

/** @var \app\modules\storage\models\search\StoragePartHistorySearch $modelSearch */
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute('index'),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>

<div class="row">
    <div class="col-lg-5">
        <div class="form-group">
            <?= $form->field($modelSearch, 'storageId')->select2List(StoragePart::getCollectionStorages(), [
                'prompt' => '—',
                'length' => false
            ]) ?>
        </div>
    </div>
    <div class="col-lg-5">
        <div class="form-group">
            <?= $form->field($modelSearch, 'productId')->select2List(StoragePart::getCollectionProducts(), [
                'prompt' => '—',
                'length' => false
            ]) ?>
        </div>
    </div>
    <div class="col-lg-2">
        <div class="form-group">
            <?= $form->field($modelSearch, 'type')->select2List(StoragePartHistory::getCollectionTypes(), [
                'prompt' => '—',
                'length' => -1,
            ]) ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')); ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')); ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
