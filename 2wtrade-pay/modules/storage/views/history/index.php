<?php
use app\components\grid\DateColumn;
use app\components\grid\GridView;
use app\helpers\DataProvider;
use app\modules\storage\models\StoragePartHistory;
use app\widgets\Label;
use app\widgets\Panel;
use yii\widgets\LinkPager;

/** @var \yii\web\View $this */
/** @var \app\modules\storage\models\search\StoragePartHistorySearch $modelSearch */
/** @var \yii\data\ActiveDataProvider $dataProvider */

$this->title = Yii::t('common', 'История движений');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Склады'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'История движений')];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'collapse' => true,
    'content' => $this->render('_index-filter', [
        'modelSearch' => $modelSearch,
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с историей'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'storagePart.storage.name',
                'label' => Yii::t('common', 'Склад'),
            ],
            [
                'attribute' => 'storagePart.product.name',
                'label' => Yii::t('common', 'Товар'),
            ],
            [
                'attribute' => 'amount',
                'label' => Yii::t('common', 'Количество'),
                'headerOptions' => ['class' => 'width-150 text-center'],
                'contentOptions' => ['class' => 'text-center'],
            ],
            [
                'label' => Yii::t('common', 'Тип'),
                'headerOptions' => ['class' => 'width-150 text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return Label::widget([
                        'style' => $model->type == StoragePartHistory::TYPE_PLUS ? Label::STYLE_SUCCESS : Label::STYLE_DANGER,
                        'label' => StoragePartHistory::getCollectionTypes()[$model->type],
                    ]);
                },
            ],
            [
                'attribute' => 'order_id',
                'label' => Yii::t('common', 'Заказ'),
                'headerOptions' => ['class' => 'width-150 text-center'],
                'contentOptions' => ['class' => 'text-center'],
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'created_at',
                'label' => Yii::t('common', 'Дата создания'),
            ],
        ],
    ]),
    'footer' => LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>
