<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/** @var app\modules\storage\models\StorageDocument $model */
/** @var array $storagesFrom */
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['index', 'id' => $model->id])]); ?>
    <div class="row">
        <?= Yii::t('common', 'Пункт отправления') ?>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="form-group">
                <?= $form->field($model, 'storage_id_from', [
                    'labelOptions' => ['label' => false]
                ])->storageProductBarCodes('storage_id_from', 'product_id', 'quantity', 'barListArray', $model->storage_id_to, $model->product_id, 'shelf_life');
                ?>
            </div>
        </div>
    </div>
    <div class="row">
        <?= Yii::t('common', 'Пункт назначения') ?>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <div class="form-group">
                <?= $form->field($model, 'countryTo', [
                    'labelOptions' => ['label' => false]
                ])->countryStorage('countryTo', 'storage_id_to', $model->countryTo, $model->storage_id_to);
                ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="form-group">
                <?= $form->field($model, 'comment')->textarea() ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <?= $form->submit(Yii::t('common', 'Добавить документ на перемещение')) ?>
            <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>