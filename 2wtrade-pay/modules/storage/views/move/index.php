<?php
use app\widgets\Panel;

/** @var yii\web\View $this */
/** @var app\modules\storage\models\StorageDocument $model */
/** @var array $storagesFrom */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление документа на перемещение') : Yii::t('common', 'Редактирование документа на перемещение');
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Документ на перемещение'),
    'content' => $this->render('_index-form', [
        'model' => $model,
        'storagesFrom' => $storagesFrom
    ])
]) ?>