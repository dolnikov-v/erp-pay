<?php
use app\components\widgets\ActiveForm;
use app\modules\storage\models\StorageProduct;
use app\widgets\assets\Select2Asset;
use yii\helpers\Url;

/** @var \app\modules\storage\models\search\StorageDocumentSearch $modelSearch */

Select2Asset::register($this);
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute('index'),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>
    <div class="row">
        <div class="col-lg-6">
            <div class="form-group">
                <?= $form->field($modelSearch, 'created_at')->dateRangePicker('dateFrom', 'dateTo') ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <div class="form-group">
                <?= Yii::t('common', 'Пункт отправления') ?>
                <?= $form->field($modelSearch, 'countryFrom', [
                    'labelOptions' => ['label' => false]
                ])->countryStorage('countryFrom', 'storageFrom', $modelSearch->countryFrom, $modelSearch->storageFrom);
                ?>
            </div>
        </div>
        <div class="col-lg-6">
            <?= Yii::t('common', 'Пункт назначения') ?>
            <div class="form-group">
                <?= $form->field($modelSearch, 'countryTo', [
                    'labelOptions' => ['label' => false]
                ])->countryStorage('countryTo', 'storageTo', $modelSearch->countryTo, $modelSearch->storageTo);
                ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3">
            <?= Yii::t('common', 'Товар') ?>
            <div class="form-group">
                <?= $form->field($modelSearch, 'product_id')->select2List(StorageProduct::getCollectionProducts(), [
                    'prompt' => '—',
                    'length' => false
                ]) ?>
            </div>
        </div>
        <div class="col-lg-3">
            <?= Yii::t('common', 'Заказ') ?>
            <div class="form-group">
                <?= $form->field($modelSearch, 'order_id')->textInput() ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->submit(Yii::t('common', 'Применить')); ?>
            <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')); ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>