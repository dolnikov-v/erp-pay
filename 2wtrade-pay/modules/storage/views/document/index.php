<?php
use app\components\grid\GridView;
use app\components\grid\DateColumn;
use app\helpers\DataProvider;
use app\widgets\Panel;
use yii\widgets\LinkPager;

/** @var \yii\web\View $this */
/** @var \app\modules\storage\models\search\StorageDocumentSearch $modelSearch */
/** @var \yii\data\ArrayDataProvider $dataProvider */

$this->title = Yii::t('common', 'Движения');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Склады'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'collapse' => true,
    'content' => $this->render('_index-filter', [
        'modelSearch' => $modelSearch,
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с движениями'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'id',
            ],
            [
                'attribute' => 'user.username',
            ],
            [
                'attribute' => 'type',
            ],
            [
                'attribute' => 'product.name',
                'label' => Yii::t('common', 'Товар'),
            ],
            [
                'attribute' => 'quantity',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'storageFrom.name',
                'label' => Yii::t('common', 'Склад отправления'),
            ],
            [
                'attribute' => 'storageTo.name',
                'label' => Yii::t('common', 'Склад назначения'),
            ],
            [
                'attribute' => 'shelf_life',
                'value' => function ($model) {
                    return Yii::$app->formatter->asDate($model->shelf_life, 'dd.MM.yyyy');
                }
            ],
            [
                'attribute' => 'order_id',
                'enableSorting' => false,
            ],
            /*[
                'label' => Yii::t('common', 'Форма на печать'),
                'headerOptions' => ['class' => 'text-center width-100'],
                'contentOptions' => ['class' => 'text-center td-with-icon'],
                'content' => function ($model) {
                    $icon = Html::tag('i', '', ['class' => 'font-size-16 fa ' . FontAwesome::FILE_TEXT]);

                    return Html::a($icon, '#', [
                        'class' => 'build-storage-document text-info',
                        'data-list-id' => $model->id,
                        'data-count-orders' => $model['quantity'],
                    ]);
                },
                'enableSorting' => false,
            ],*/
            [
                'attribute' => 'comment',
                'enableSorting' => false,
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'created_at',
                'enableSorting' => false,
            ],
        ],
    ]),
    'footer' => LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>