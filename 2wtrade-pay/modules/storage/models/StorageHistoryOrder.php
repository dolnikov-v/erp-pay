<?php
namespace app\modules\storage\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\modules\storage\models\query\StorageHistoryOrderQuery;
use app\modules\delivery\models\DeliveryRequest;

/**
 * Class StorageHistoryOrder
 * @property integer $id
 * @property string $type
 * @property integer $product_id
 * @property integer $quantity
 * @property integer $order_id
 * @property integer $country_id
 * @property integer $cron_launched_at
 *
 * @property DeliveryRequest $deliveryRequest
 */
class StorageHistoryOrder extends ActiveRecordLogUpdateTime
{
    const TYPE_PLUS = 'plus';
    const TYPE_MINUS = 'minus';

    public static $types = [
        self::TYPE_PLUS,
        self::TYPE_MINUS,
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%storage_history_order}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'product_id', 'quantity', 'order_id', 'country_id'], 'required'],
            [['product_id', 'quantity', 'order_id', 'country_id', 'cron_launched_at'], 'integer'],
            [['type'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'product_id' => 'Product ID',
            'quantity' => 'Quantity',
            'order_id' => 'Order ID',
            'country_id' => 'Country ID',
            'cron_launched_at' => 'cron_launched_at',
        ];
    }

    /**
     * @inheritdoc
     * @return StorageHistoryOrderQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new StorageHistoryOrderQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveryRequest()
    {
        return $this->hasOne(DeliveryRequest::className(), ['order_id' => 'order_id']);
    }
}
