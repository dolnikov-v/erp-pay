<?php
namespace app\modules\storage\models\query;

use app\components\db\ActiveQuery;
use app\modules\storage\models\Storage;

/**
 * Class StorageQuery
 * @package app\modules\storage\models\query
 * @method Storage one($db = null)
 * @method Storage[] all($db = null)
 */
class StorageQuery extends ActiveQuery
{
    /**
     * @param integer $countryId
     * @return $this
     */
    public function byCountryId($countryId)
    {
        return $this->andWhere([Storage::tableName() . '.country_id' => $countryId]);
    }

    /**
     * @return $this
     */
    public function isRealStorage()
    {
        return $this->andWhere(['delivery_id' => null]);
    }

    /**
     * @return $this
     */
    public function isDefault()
    {
        return $this->andWhere(['default' => 1]);
    }

    /**
     * @param bool $noUse
     * @return $this
     */
    public function noUseBar($noUse)
    {
        if ($noUse) {
            return $this->andWhere(['use_bar_code' => 0]);
        }
        return $this;
    }

    /**
     * @return $this
     */
    public function useBar()
    {
        return $this->andWhere(['use_bar_code' => 1]);
    }
}
