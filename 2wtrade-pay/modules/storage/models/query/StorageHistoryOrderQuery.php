<?php
namespace app\modules\storage\models\query;

use app\components\db\ActiveQuery;
use app\modules\storage\models\StorageHistoryOrder;

/**
 * Class StorageHistoryOrderQuery
 * @package app\modules\storage\models\query
 */
class StorageHistoryOrderQuery extends ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return StorageHistoryOrder[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return StorageHistoryOrder|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}