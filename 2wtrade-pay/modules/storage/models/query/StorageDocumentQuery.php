<?php
namespace app\modules\storage\models\query;

use app\components\db\ActiveQuery;
use app\modules\storage\models\StorageDocument;

/**
 * Class StorageDocumentQuery
 * @package app\modules\storage\models\query
 */
class StorageDocumentQuery extends ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return StorageDocument[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return StorageDocument|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}