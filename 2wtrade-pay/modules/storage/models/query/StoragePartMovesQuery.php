<?php
namespace app\modules\storage\models\query;

use app\components\db\ActiveQuery;
use app\modules\storage\models\StoragePartMoves;

/**
 * Class StoragePartMovesQuery
 * @package app\modules\storage\models\query
 */
class StoragePartMovesQuery extends ActiveQuery
{
    /**
     * @inheritdoc
     * @return StoragePartMoves[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return StoragePartMoves|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}