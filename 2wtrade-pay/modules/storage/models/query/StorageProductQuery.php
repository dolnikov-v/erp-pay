<?php

namespace app\modules\storage\models\query;

use app\components\db\ActiveQuery;
use app\modules\storage\models\StorageProduct;

/**
 * Class StorageProductQuery
 * @package app\modules\storage\models\query
 *
 * @method StorageProduct one($db = null)
 * @method StorageProduct[] all($db = null)
 */
class StorageProductQuery extends ActiveQuery
{
    /**
     * @param integer $storageId
     * @return $this
     */
    public function byStorageId($storageId)
    {
        return $this->andWhere(['storage_id' => $storageId]);
    }

    /**
     * @param integer $productId
     * @return $this
     */
    public function byProductId($productId)
    {
        return $this->andWhere(['product_id' => $productId]);
    }
}