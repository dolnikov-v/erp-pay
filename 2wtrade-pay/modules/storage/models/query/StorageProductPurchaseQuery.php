<?php

namespace app\modules\storage\models\query;

use app\components\db\ActiveQuery;
use app\modules\storage\models\StorageProductPurchase;

/**
 * This is the ActiveQuery class for [[StorageProductPurchase]].
 *
 * @see StorageProductPurchase
 */
class StorageProductPurchaseQuery extends ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return StorageProductPurchase[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return StorageProductPurchase|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}