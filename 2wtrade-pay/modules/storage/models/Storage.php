<?php
namespace app\modules\storage\models;

use app\components\db\abstracts\ConnectionInterface;
use app\components\db\ActiveRecordLogUpdateTime;
use app\components\db\models\DirtyData;
use app\models\Country;
use app\models\PartnerCountry;
use app\modules\callcenter\models\CallCenter;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryStorage;
use app\modules\storage\models\query\StorageQuery;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class Storage
 * @package app\modules\storage\models
 * @property integer $id
 * @property integer $country_id
 * @property integer $call_center_id
 * @property string $name
 * @property string $sender_name
 * @property string $sender_zip
 * @property string $sender_city
 * @property string $sender_phone
 * @property string $address
 * @property string $comment
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $delivery_id
 * @property integer $default
 * @property integer $use_bar_code
 * @property string $space
 * @property string $type
 *
 * @property Country $country
 * @property PartnerCountry $partnerCountry
 * @property CallCenter $callCenter
 * @property Delivery $delivery
 * @property DeliveryStorage[] $deliveryStorage
 * @property Delivery[] $deliveries
 */
class Storage extends ActiveRecordLogUpdateTime
{
    protected $insertLog = true;
    protected $updateLog = true;

    private static $collectionStorages;

    private static $collectionDeliveries;

    const TYPE_OUR = 1;
    const TYPE_INTERIM = 2;
    const TYPE_DELIVERY = 3;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%storage}}';
    }

    /**
     * @return StorageQuery
     */
    public static function find()
    {
        return new StorageQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'name'], 'required'],
            [
                'country_id',
                'exist',
                'targetClass' => '\app\models\Country',
                'targetAttribute' => 'id',
                'message' => Yii::t('common', 'Неверное значение страны.'),
            ],
            [
                'call_center_id',
                'exist',
                'targetClass' => '\app\modules\callcenter\models\CallCenter',
                'targetAttribute' => 'id',
                'filter' => [
                    'country_id' => Yii::$app->user->country->id,
                ],
                'message' => Yii::t('common', 'Неверное значение колл-центра.'),
            ],
            [
                'delivery_id',
                'exist',
                'targetClass' => '\app\modules\delivery\models\Delivery',
                'targetAttribute' => 'id',
                'filter' => [
                    'country_id' => Yii::$app->user->country->id,
                ],
                'message' => Yii::t('common', 'Неверное значение службы доставки.'),
            ],
            [['name', 'sender_name', 'address', 'comment'], 'filter', 'filter' => 'trim'],
            [['name', 'sender_name', 'sender_city'], 'string', 'max' => 100],
            [['sender_zip', 'sender_phone'], 'string', 'max' => 20],
            [['address','comment'], 'string', 'max' => 1000],
            [['default', 'use_bar_code', 'type'], 'integer'],
            [['space'], 'number', 'min' => 0],
            ['type', 'in', 'range' => array_keys(self::getTypes())],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'country_id' => Yii::t('common', 'Страна'),
            'call_center_id' => Yii::t('common', 'Колл-центр'),
            'name' => Yii::t('common', 'Название'),
            'sender_name' => Yii::t('common', 'Наименование отправителя'),
            'sender_city' => Yii::t('common', 'Город отправки'),
            'sender_zip' => Yii::t('common', 'Индекс отправки'),
            'sender_phone' => Yii::t('common', 'Контактный телефон отправителя'),
            'address' => Yii::t('common', 'Адрес'),
            'comment' => Yii::t('common', 'Комментарий'),
            'created_at' => Yii::t('common', 'Дата добавления'),
            'delivery_id' => Yii::t('common', 'Служба доставки'),
            'default' => Yii::t('common', 'По умолчанию'),
            'use_bar_code' => Yii::t('common', 'Использует сканер штрих-кодов'),
            'space' => Yii::t('common', 'Площадь'),
            'type' => Yii::t('common', 'Тип'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartnerCountry()
    {
        return $this->hasOne(PartnerCountry::className(), ['country_id' => 'country_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCallCenter()
    {
        return $this->hasOne(CallCenter::className(), ['id' => 'call_center_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDelivery()
    {
        return $this->hasOne(Delivery::className(), ['id' => 'delivery_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveryStorage()
    {
        return $this->hasMany(DeliveryStorage::className(), ['storage_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveries()
    {
        return $this->hasMany(Delivery::className(), ['id' => 'delivery_id'])->via('deliveryStorage');
    }

    /**
     * @return array
     */
    public static function getCollectionStorages()
    {
        if (self::$collectionStorages === null) {
            self::$collectionStorages = self::find()->orderBy('name')->byCountryId(Yii::$app->user->country->id)->collection();
        }
        return self::$collectionStorages;
    }

    /**
     * @return array
     */
    public static function getCollectionDeliveries()
    {
        if (self::$collectionDeliveries === null) {
            self::$collectionDeliveries = ArrayHelper::map(self::find()
                ->joinWith('delivery')
                ->orderBy(Delivery::tableName() . '.name')
                ->byCountryId(Yii::$app->user->country->id)
                ->groupBy(Delivery::tableName() . '.id')
                ->all(), 'delivery.id', 'delivery.name');
        }
        return self::$collectionDeliveries;
    }

    /**
     * “Обменный” алфавитно-цифровой код склада
     * @return string
     */
    public function getExchangeCode()
    {
        $a = ((($this->id & 992) >> 5) < 26) ? ((($this->id & 992) >> 5) + 65) : ((($this->id & 31744) >> 10) + 22);
        $b = (($this->id & 31) < 26) ? (($this->id & 31) + 65) : (($this->id & 31) + 22);
        return chr($a) . chr($b);
    }

    /**
     * @return array
     */
    public static function getTypes()
    {
        return [
            self::TYPE_OUR => Yii::t('common', 'Наш склад'),
            self::TYPE_INTERIM => Yii::t('common', 'Промежуточный'),
            self::TYPE_DELIVERY => Yii::t('common', 'Склад КС'),
        ];
    }

    /**
     * @return bool
     */
    public function isTypeOur()
    {
        return $this->type == self::TYPE_OUR;
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ((static::getDb() instanceof ConnectionInterface) && Country::find()->where(['id' => $this->country_id, 'sink1c' => true])->exists()) {
            $changedAttributes['id'] = $this->id;
            $changedAttributes['country_id'] = $this->country_id;
            static::getDb()->addBufferChange(new DirtyData([
                'id' => static::clearTableName(),
                'action' => $insert ? DirtyData::ACTION_INSERT : DirtyData::ACTION_UPDATE,
                'data' => $changedAttributes
            ]));
        }

        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        $data = ['id' => $this->id, 'country_id' => $this->country_id];
        if (static::getDb() instanceof ConnectionInterface && Country::find()->where(['id' => $this->country_id, 'sink1c' => true])->exists()) {
            static::getDb()->addBufferChange(new DirtyData([
                'id' => static::clearTableName(),
                'action' => DirtyData::ACTION_DELETE,
                'data' => $data
            ]));
        }
        return parent::afterDelete();
    }
}
