<?php
namespace app\modules\storage\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\modules\order\models\Order;
use app\models\Country;
use app\models\Product;
use app\modules\delivery\models\DeliveryRequest;

/**
 * Class StorageProductUnaccounted
 * @property integer $id
 * @property integer $amount
 * @property integer $product_id
 * @property integer $storage_id
 * @property integer $order_id
 * @property integer $country_id
 * @property integer $cron_launched_at
 *
 * @property Country $country
 * @property Order $order
 * @property Product $product
 * @property Storage $storage
 */
class StorageProductUnaccounted extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%storage_product_unaccounted}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['amount', 'product_id', 'storage_id', 'order_id', 'country_id', 'cron_launched_at'], 'integer'],
            [['product_id', 'country_id'], 'required'],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_id' => 'id']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['storage_id'], 'exist', 'skipOnError' => true, 'targetClass' => Storage::className(), 'targetAttribute' => ['storage_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'amount' => 'Amount',
            'product_id' => 'Product ID',
            'storage_id' => 'Storage ID',
            'order_id' => 'Order ID',
            'country_id' => 'Country ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStorage()
    {
        return $this->hasOne(Storage::className(), ['id' => 'storage_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveryRequest()
    {
        return $this->hasOne(DeliveryRequest::className(), ['order_id' => 'order_id']);
    }
}
