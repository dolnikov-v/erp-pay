<?php
namespace app\modules\storage\models\search;

use app\components\db\ActiveQuery;
use app\modules\storage\models\StorageDocument;
use Yii;
use yii\base\InvalidParamException;
use yii\data\ActiveDataProvider;

/**
 * Class StorageDocumentSearch
 * @package app\modules\storage\models\search
 */
class StorageDocumentSearch extends StorageDocument
{
    public $dateFrom;
    public $dateTo;
    public $countryFrom;
    public $storageFrom;
    public $storageTo;
    public $withExpected = false;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dateFrom', 'dateTo'], 'safe'],
            [['product_id', 'storageFrom', 'storageTo', 'order_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'storageId' => Yii::t('common', 'Склад'),
            'product_id' => Yii::t('common', 'Товар'),
            'order_id' => Yii::t('common', 'Заказ'),
        ];
    }

    /**
     * @param array $params
     * @param bool $withPagination
     * @return ActiveDataProvider
     * @throws InvalidParamException
     */
    public function search($params = [], $withPagination = true)
    {
        $country = Yii::$app->user->getCountry();

        /** @var ActiveQuery $query */
        $query = StorageDocument::find()
            ->joinWith(['storageFrom s_from', 'storageTo s_to', 'product'])
            ->where(
                [
                    'or',
                    ['s_from.country_id' => $country->id],
                    ['s_to.country_id' => $country->id]
                ]
            );

        if ($this->load($params)) {
            if ($this->validate()) {
                $query = $query->andFilterWhere([StorageDocument::tableName() . '.storage_id_from' => $this->storageFrom]);
                $query = $query->andFilterWhere([StorageDocument::tableName() . '.storage_id_to' => $this->storageTo]);
                $query = $query->andFilterWhere([StorageDocument::tableName() . '.product_id' => $this->product_id]);
                $query = $query->andFilterWhere([StorageDocument::tableName() . '.order_id' => $this->order_id]);

                if ($this->dateFrom != null) {
                    $timeFrom = Yii::$app->formatter->asTimestamp($this->dateFrom);
                    if ($timeFrom) {
                        $query = $query->andWhere(['>=', StorageDocument::tableName() . '.created_at', $timeFrom]);
                    }
                }

                if ($this->dateTo != null) {
                    $timeTo = Yii::$app->formatter->asTimestamp($this->dateTo);

                    if ($timeTo) {
                        $query = $query->andWhere(['<=', StorageDocument::tableName() . '.created_at', $timeTo + 86399]);
                    }
                }
            } else {
                throw new InvalidParamException(Yii::t('common', 'Неверные параметры запроса.'));
            }
        }

        $query = $query->orderBy(StorageDocument::tableName() . '.updated_at DESC');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize' => ($withPagination == true ? 20 : 0)]
        ]);

        return $dataProvider;
    }
}