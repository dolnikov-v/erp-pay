<?php
namespace app\modules\storage\models\search;

use app\components\db\ActiveQuery;
use app\models\Product;
use app\modules\storage\models\Storage;
use app\modules\storage\models\StoragePart;
use app\modules\storage\models\StoragePartMoves;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use Yii;

/**
 * Class StoragePartSearch
 * @package app\modules\storage\models\search
 */
class StoragePartMovesSearch extends StoragePartMoves
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['product_id', 'in', 'range' => array_keys(StoragePart::getCollectionProducts())],
        ];
    }

    /**
     * @param integer|null $storage_id
     * @param bool $withPagination
     * @return ActiveDataProvider
     * @throws NotFoundHttpException
     */
    public function search($storage_id = null, $withPagination = true)
    {
        /** @var ActiveQuery $query */
        if ($storage_id === null) {
            $query = StoragePartMoves::find()
                ->joinWith(['storagePartFrom.storage', 'product'])
                ->where([Storage::tableName() . '.country_id' => Yii::$app->user->getCountry()->id])
                ->andWhere([StoragePartMoves::tableName() . '.storage_part_to' => null]);
        } else {
            $query = StoragePartMoves::find()
                ->joinWith(['product'])
                ->where([StoragePartMoves::tableName() . '.storage_to' => $storage_id])
                ->andWhere([StoragePartMoves::tableName() . '.storage_part_to' => null]);
        }

        $query->orderBy(Product::tableName() . '.name');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize' => ($withPagination == true ? 20 : 0)]
        ]);

        return $dataProvider;
    }
}
