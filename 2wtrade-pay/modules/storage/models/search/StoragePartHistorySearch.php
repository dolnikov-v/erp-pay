<?php
namespace app\modules\storage\models\search;

use app\modules\storage\models\StoragePart;
use app\modules\storage\models\StoragePartHistory;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * Class StoragePartHistorySearch
 * @package app\modules\storage\models\search
 */
class StoragePartHistorySearch extends StoragePartHistory
{
    public $storageId;
    public $productId;
    public $type;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['storageId', 'in', 'range' => array_keys(StoragePart::getCollectionStorages())],
            ['productId', 'in', 'range' => array_keys(StoragePart::getCollectionProducts())],
            ['type', 'in', 'range' => self::$types],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'storageId' => Yii::t('common', 'Склад'),
            'productId' => Yii::t('common', 'Товар'),
            'type' => Yii::t('common', 'Тип'),
        ];
    }

    /**
     * @param array $params
     * @param bool $withPagination
     * @return ActiveDataProvider
     */
    public function search($params = [], $withPagination = true)
    {
        $query = StoragePartHistory::find()
            ->joinWith(['storagePart', 'storagePart.product', 'storagePart.storage'])
            ->where(['storage.country_id' => Yii::$app->user->getCountry()->id]);

        if ($this->load($params)) {
            if ($this->validate()) {
                $query->andFilterWhere(['storage.id' => $this->storageId]);
                $query->andFilterWhere(['product.id' => $this->productId]);
                $query->andFilterWhere(['type' => $this->type]);
            }
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize' => ($withPagination == true ? 20 : 0)]
        ]);

        return $dataProvider;
    }
}
