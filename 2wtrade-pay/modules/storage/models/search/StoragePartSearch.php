<?php

namespace app\modules\storage\models\search;

use app\components\db\ActiveQuery;
use app\models\Product;
use app\modules\storage\models\Storage;
use app\modules\storage\models\StoragePart;
use Yii;
use yii\base\InvalidParamException;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;

/**
 * Class StoragePartSearch
 * @package app\modules\storage\models\search
 */
class StoragePartSearch extends StoragePart
{
    public $dateFrom;
    public $dateTo;
    public $withExpected = false;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['storage_id', 'in', 'range' => array_keys(StoragePart::getCollectionStorages())],
            ['product_id', 'in', 'range' => array_keys(StoragePart::getCollectionProducts())],
            [['dateFrom', 'dateTo'], 'safe'],
        ];
    }

    /**
     * @param array $params
     * @param bool $withPagination
     * @param bool $groupByProduct
     * @return ActiveDataProvider
     * @throws NotFoundHttpException
     */
    public function search($params = [], $withPagination = true, $groupByProduct = false)
    {
        $country = Yii::$app->user->getCountry();

        /** @var ActiveQuery $query */
        $query = StoragePart::find()
            ->joinWith(['storage', 'product'])
            ->where([Storage::tableName() . '.country_id' => $country->id])
            ->andWhere([StoragePart::tableName() . '.active' => 1]);

        if ($groupByProduct) {
            $query = $query->groupBy([StoragePart::tableName() . '.product_id']);
        } else {
            $query = $query->groupBy([StoragePart::tableName() . '.id']);
        }

        $query = $query->orderBy(Storage::tableName() . '.name, ' . Product::tableName() . '.name, ' . StoragePart::tableName() . '.number');

        if ($this->load($params)) {
            if ($this->validate()) {
                $query = $query->andFilterWhere(['storage_part.storage_id' => $this->storage_id]);
                $query = $query->andFilterWhere(['storage_part.product_id' => $this->product_id]);

                if ($this->dateFrom != null) {
                    $timeFrom = Yii::$app->formatter->asTimestamp($this->dateFrom);
                    if ($timeFrom) {
                        $query = $query->andWhere(['>=', 'storage_part.created_at', $timeFrom]);
                    }
                }

                if ($this->dateTo != null) {
                    $timeTo = Yii::$app->formatter->asTimestamp($this->dateTo);

                    if ($timeTo) {
                        $query = $query->andWhere(['<=', 'storage_part.created_at', $timeTo + 86399]);
                    }
                }
            } else {
                throw new InvalidParamException(Yii::t('common', 'Неверные параметры запроса.'));
            }
        }

        if ($this->withExpected === false) {
            $query = $query->andWhere(['storage_part.expected' => null]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize' => ($withPagination == true ? 20 : 0)]
        ]);

        return $dataProvider;
    }
}
