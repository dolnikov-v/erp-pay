<?php
namespace app\modules\storage\models\search;

use app\modules\storage\models\Storage;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * Class StorageSearch
 * @package app\modules\storage\models\search
 */
class StorageSearch extends Storage
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @param array $params
     * @param bool $withPagination
     * @return ActiveDataProvider
     */
    public function search($params = [], $withPagination = true)
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['created_at'] = SORT_ASC;
        }

        $query = Storage::find()
            ->joinWith(['callCenter', 'delivery'])
            ->where([Storage::tableName() . '.country_id' => Yii::$app->user->getCountry()->id])
            ->orderBy($sort);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $withPagination == true ? 20 : 0,
            ]
        ]);

        return $dataProvider;
    }
}
