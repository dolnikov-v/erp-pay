<?php
namespace app\modules\storage\models\search;

use app\components\db\ActiveQuery;
use app\modules\storage\models\Storage;
use app\modules\storage\models\StorageProduct;
use Yii;
use yii\base\InvalidParamException;
use yii\data\ActiveDataProvider;

/**
 * Class StorageProductSearch
 * @package app\modules\storage\models\search
 */
class StorageProductSearch extends StorageProduct
{

    public $delivery_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['storage_id', 'product_id', 'delivery_id'], 'integer'],
        ];
    }

    /**
     * @param array $params
     * @param bool $withPagination
     * @return ActiveDataProvider
     * @throws InvalidParamException
     */
    public function search($params = [], $withPagination = true)
    {
        /** @var ActiveQuery $query */
        $query = StorageProduct::find()
            ->joinWith(['storage', 'product'])
            ->where([Storage::tableName() . '.country_id' => Yii::$app->user->country->id]);

        if ($this->load($params)) {
            if ($this->validate()) {
                if ($this->storage_id) {
                    $query = $query->andFilterWhere([StorageProduct::tableName() . '.storage_id' => $this->storage_id]);
                }
                if ($this->product_id) {
                    $query = $query->andFilterWhere([StorageProduct::tableName() . '.product_id' => $this->product_id]);
                }
                if ($this->delivery_id) {
                    $query = $query->andFilterWhere([Storage::tableName() . '.delivery_id' => $this->delivery_id]);
                }
            } else {
                throw new InvalidParamException(Yii::t('common', 'Неверные параметры запроса.'));
            }
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize' => ($withPagination == true ? 20 : 0)]
        ]);

        return $dataProvider;
    }
}