<?php

namespace app\modules\storage\models\search;

use app\models\Product;
use app\modules\storage\models\StorageProductPurchase;
use app\modules\storage\models\StoragePart;
use app\modules\storage\models\Storage;
use app\modules\order\models\Order;
use app\modules\order\models\OrderProduct;
use Yii;
use yii\data\ArrayDataProvider;
use yii\db\Expression;

/**
 * Class StorageProductPurchaseSearch
 * @package app\modules\storage\models\search
 */
class StorageProductPurchaseSearch extends StorageProductPurchase
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @param array $params
     * @param bool $withPagination
     * @return ArrayDataProvider
     */
    public function search($params = [], $withPagination = true)
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort[StorageProductPurchase::tableName() . '.created_at'] = SORT_ASC;
        }

        $query = StorageProductPurchase::find()
            ->joinWith(['product'])
            ->select([
                'id' => StorageProductPurchase::tableName() . '.id',
                'product_name' => Product::tableName() . '.name',
                'product_id' => StorageProductPurchase::tableName() . '.product_id',
                'plan_lead_day' => StorageProductPurchase::tableName() . '.plan_lead_day',
                'limit_days' => StorageProductPurchase::tableName() . '.limit_days',
                'limit_count' => StorageProductPurchase::tableName() . '.limit_count',
                'last_purchase_count' => StorageProductPurchase::tableName() . '.last_purchase_count',
                'last_purchase_date' => StorageProductPurchase::tableName() . '.last_purchase_date',
                'created_at' => StorageProductPurchase::tableName() . '.created_at',
                'updated_at' => StorageProductPurchase::tableName() . '.updated_at',
                'shelf_life' => 'sl_by_product.shelf_life'
            ])
            ->where([StorageProductPurchase::tableName() . '.country_id' => Yii::$app->user->getCountry()->id])
            ->orderBy($sort)
            ->groupBy('product_id');

        $query->leftJoin([
            'sl_by_product' => StoragePart::find()
                ->select(new Expression('group_concat(DISTINCT IFNULL(shelf_life, \'—\')) AS shelf_life, product_id'))
                ->joinWith(['storage'])
                ->where([Storage::tableName() . '.country_id' => Yii::$app->user->getCountry()->id])
                ->andWhere([StoragePart::tableName() . '.active' => 1])
                ->orderBy('shelf_life')
                ->groupBy(['product_id'])
        ], 'sl_by_product.product_id = ' . StorageProductPurchase::tableName() . '.product_id');

        $query->addSelect([
            'balance' =>  StoragePart::find()->select('group_concat(balance)')->from([StoragePart::find()
                ->select(['sum(' . StoragePart::tableName() . '.balance) balance, product_id'])
                ->joinWith(['storage'])
                ->where([Storage::tableName() . '.country_id' => Yii::$app->user->getCountry()->id])
                ->andWhere([StoragePart::tableName() . '.active' => 1])
                ->orderBy('shelf_life')
                ->groupBy(['shelf_life', 'product_id'])
        ])->andWhere(['product_id' => new Expression(StorageProductPurchase::tableName() . '.product_id')])]);

        $query->addSelect([
            'orders' => Order::find()
                ->select(['COUNT(DISTINCT ' . Order::tableName() . '.`id`) / 3'])
                ->joinWith(['orderProducts'])
                ->where([Order::tableName() . '.country_id' => Yii::$app->user->getCountry()->id])
                ->andWhere([OrderProduct::tableName() . '.product_id' => new Expression(StorageProductPurchase::tableName() . '.product_id')])
                ->andWhere(['>=', Order::tableName() . '.created_at', time() - 3 * 86400])
                ->andWhere(['is', Order::tableName() . '.duplicate_order_id', null])
        ]);

        $data = $query->asArray()->all();
        foreach ($data as $key => $item) {
            $data[$key]['days_by_plan_lead_day'] = $this->calulateDays($item, $item['plan_lead_day']);
            $data[$key]['days_by_orders'] = $this->calulateDays($item, $item['orders']);
        }
        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'pagination' => [
                'pageSize' => $withPagination == true ? 20 : 0,
            ]
        ]);

        return $dataProvider;
    }

    /**
     * Подсчет остатка в днях с учетом рока годности
     * @param array $model
     * @param float $plan_lead_day
     * @return float|int|null
     */
    private function calulateDays($model, float $plan_lead_day)
    {
        $days = [];
        if ($plan_lead_day > 0 && !empty($model) && is_array($model)) {
            $shelf_life = explode(',', $model['shelf_life']);
            sort($shelf_life, SORT_NUMERIC);
            $balance = explode(',', $model['balance']);
            $day_back = 0;
            foreach ($shelf_life as $key => $item) {
                $day_plan = round($balance[$key] / $plan_lead_day, 1);
                if (strlen($item) > 3) {
                    $day_dist = round((strtotime($item) - strtotime(date('Y-m-d'))) / 86400, 1) - $day_back;
                    if ($day_plan > $day_dist) {
                        $days[] = $day_dist;
                        $day_back += $day_dist;
                    } else {
                        $days[] = $day_plan;
                        $day_back += $day_plan;
                    }
                } else {
                    $days[] = $day_plan;
                }
            }
        }
        return array_sum($days) ?? null;
    }
}