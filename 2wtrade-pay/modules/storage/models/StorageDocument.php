<?php
namespace app\modules\storage\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\Product;
use app\models\User;
use app\modules\storage\models\query\StorageDocumentQuery;
use Yii;
use Zend\Validator\Date;

/**
 * Class StorageDocument
 * @property integer $id
 * @property string $type
 * @property integer $product_id
 * @property integer $user_id
 * @property integer $quantity
 * @property integer $storage_id_from
 * @property integer $storage_id_to
 * @property integer $order_id
 * @property integer $bar_list_id
 * @property string $bar_code
 * @property string $comment
 * @property integer $created_at
 * @property integer $updated_at
 * @property Date $shelf_life
 */
class StorageDocument extends ActiveRecordLogUpdateTime
{
    const TYPE_RECEIPT = 'ПОСТУПЛЕНИЕ';
    const TYPE_MOVEMENT = 'ПЕРЕМЕЩЕНИЕ';
    const TYPE_WRITEOFF_ORDER = 'СПИСАНИЕ_ЗАКАЗ';
    const TYPE_RETURN_ORDER = 'ВОЗВРАТ_ЗАКАЗ';
    const TYPE_CORRECTION_POSITIVE = 'КОРРЕКТИРОВКА_ПОЛОЖИТЕЛЬНАЯ';
    const TYPE_CORRECTION_NEGATIVE = 'КОРРЕКТИРОВКА_ОТРИЦАТЕЛЬНАЯ';
    const TYPE_DELETE = 'УДАЛЕНИЕ';

    const SCENARIO_CORRECTION = 'correction';
    const SCENARIO_MOVE = 'move';
    const SCENARIO_RECEIPT = 'receipt';
    const SCENARIO_ORDER = 'order';
    const SCENARIO_DELETE = 'delete_record';

    public $countryTo;

    /**
     * @var integer
     */
    public $isNegative;

    /**
     * @var array
     */
    public $barListArray;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%storage_document}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'product_id'], 'required'],
            ['storage_id_to', 'required', 'on' =>
                [
                    self::SCENARIO_CORRECTION,
                    self::SCENARIO_MOVE,
                    self::SCENARIO_RECEIPT,
                ],
            ],
            ['storage_id_from', 'required', 'on' =>
                [
                    self::SCENARIO_MOVE,
                    self::SCENARIO_ORDER
                ],
            ],
            ['quantity', 'required', 'on' =>
                [
                    self::SCENARIO_CORRECTION,
                    self::SCENARIO_RECEIPT
                ],
            ],
            ['bar_list_id', 'required', 'on' =>
                [
                    self::SCENARIO_RECEIPT
                ],
            ],
            [['bar_code', 'order_id'], 'required', 'on' =>
                [
                    self::SCENARIO_ORDER
                ],
            ],
            [['product_id', 'user_id', 'storage_id_from', 'storage_id_to', 'order_id', 'bar_list_id', 'created_at', 'updated_at'], 'integer'],
            ['quantity', 'integer', 'min' => 1, 'except' => [self::SCENARIO_DELETE]],
            [['comment'], 'string'],
            [['shelf_life'], 'date', 'format' => 'Y-m-d'],
            [['type'], 'string', 'max' => 50],
            [['bar_code'], 'string', 'max' => 64],
            [['created_at', 'updated_at', 'barListArray'], 'safe'],
            ['quantity', 'validateQuantityCorrection', 'on' =>
                [
                    self::SCENARIO_CORRECTION
                ],
            ],
            ['quantity', 'validateQuantityMove', 'on' =>
                [
                    self::SCENARIO_MOVE
                ],
            ],
            ['barListArray', 'validatebarListArrayMove', 'on' =>
                [
                    self::SCENARIO_MOVE
                ],
            ],
        ];
    }

    /**
     * @param string $attribute
     * @param array $params
     */
    public function validateQuantityCorrection($attribute, $params)
    {
        if ($this->isNegative) {
            $storageProduct = StorageProduct::find()->byStorageId($this->storage_id_to)->byProductId($this->product_id)->one();
            if ($storageProduct === null) {
                $this->addError($attribute, Yii::t('common', 'Не может быть списано больше товаров, чем есть на складе.'));
            } else {
                if ($storageProduct->balance < $this->quantity) {
                    $this->addError($attribute, Yii::t('common', 'Не может быть списано больше товаров, чем есть на складе.'));
                }
            }
        }
    }

    /**
     * @param string $attribute
     * @param array $params
     */
    public function validateQuantityMove($attribute, $params)
    {
        if (count($this->barListArray) == 0) {
            $storageProduct = StorageProduct::find()->byStorageId($this->storage_id_from)->byProductId($this->product_id)->one();
            if ($storageProduct === null) {
                $this->addError($attribute, Yii::t('common', 'Не может быть списано больше товаров, чем есть на складе.'));
            } else {
                if ($storageProduct->balance < $this->quantity) {
                    $this->addError($attribute, Yii::t('common', 'Не может быть списано больше товаров, чем есть на складе.'));
                }
            }
        }
    }

    /**
     * @param string $attribute
     * @param array $params
     */
    public function validatebarListArrayMove($attribute, $params)
    {
        if (count($this->barListArray) == 0 && $this->quantity == null) {
            $this->addError($attribute, Yii::t('common', 'Не указано количество товара.'));
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'type' => Yii::t('common', 'Тип'),
            'product_id' => Yii::t('common', 'Товар'),
            'user_id' => Yii::t('common', 'Пользователь'),
            'quantity' => Yii::t('common', 'Количество'),
            'storage_id_from' => Yii::t('common', 'Склад отправления'),
            'storage_id_to' => Yii::t('common', 'Склад назначения'),
            'order_id' => Yii::t('common', 'Заказ'),
            'bar_list_id' => Yii::t('common', 'Файл штрих-кодов'),
            'bar_code' => Yii::t('common', 'Штрих-код'),
            'comment' => Yii::t('common', 'Комментарий'),
            'created_at' => Yii::t('common', 'Дата добавления'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
            'isNegative' => Yii::t('common', 'Списать'),
            'shelf_life' => Yii::t('common', 'Срок годности'),
        ];
    }

    /**
     * @inheritdoc
     * @return StorageDocumentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new StorageDocumentQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStorageFrom()
    {
        return $this->hasOne(Storage::className(), ['id' => 'storage_id_from']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStorageTo()
    {
        return $this->hasOne(Storage::className(), ['id' => 'storage_id_to']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @param string $paramTypeDocument
     * @param integer $paramProduct
     * @param integer $paramQuantity
     * @param integer $paramStorageIdFrom
     * @param integer $paramStorageIdTo
     * @param integer $paramBarListId
     * @param string $paramBarCode
     * @param integer $paramOrderId
     * @param string $paramComment
     * @param string $paramScenario
     * @param Date $paramShelfLife
     * @return integer
     * @throws \Exception
     */
    public static function createNewDocument($paramTypeDocument, $paramProduct, $paramQuantity, $paramStorageIdFrom = null, $paramStorageIdTo = null, $paramBarListId = null, $paramBarCode = null, $paramOrderId = null, $paramComment = null, $paramScenario = null, $paramShelfLife = null)
    {
        $document = new self();
        if ($paramScenario != null) {
            $document->setScenario($paramScenario);
        }
        $document->type = $paramTypeDocument;
        $document->user_id = isset(Yii::$app->user->id) ? Yii::$app->user->id : null;
        $document->product_id = $paramProduct;
        $document->quantity = $paramQuantity;
        $document->storage_id_from = $paramStorageIdFrom;
        $document->storage_id_to = $paramStorageIdTo;
        $document->bar_list_id = $paramBarListId;
        $document->bar_code = $paramBarCode;
        $document->order_id = $paramOrderId;
        $document->comment = $paramComment;
        $document->shelf_life = $paramShelfLife;
        if (!$document->save()) {
            throw new \Exception(Yii::t('common', 'Не удалось создать документ {error}.', ['error' => $document->getFirstErrorAsString()]));
        }
        return $document->id;
    }
}