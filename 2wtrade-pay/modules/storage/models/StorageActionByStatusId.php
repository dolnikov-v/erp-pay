<?php

namespace app\modules\storage\models;

use app\components\db\ActiveRecord;
use app\modules\order\models\OrderStatus;

/**
 * Class StorageActionByStatusId
 *
 * @package app\modules\storage\models
 * @property int $status_id
 * @property int $action_id
 *
 * @property OrderStatus $status
 */
class StorageActionByStatusId extends ActiveRecord
{
    const ACTION_STORAGE_PART_MINUS = 0; // Списание со склада
    const ACTION_STORAGE_PART_PLUS = 1; // Возврат на склад

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'storage_action_by_status_id';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['status_id', 'action_id'], 'required'],
            [['status_id', 'action_id'], 'integer']
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(OrderStatus::className(), ['id' => 'status_id']);
    }
}