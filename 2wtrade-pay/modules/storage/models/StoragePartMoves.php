<?php
namespace app\modules\storage\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\User;
use app\models\Product;
use app\modules\storage\models\query\StoragePartMovesQuery;
use Yii;

/**
 * Class StoragePartMoves
 * @property integer $id
 * @property string $type
 * @property integer $quantity
 * @property integer $product_id
 * @property integer $storage_part_from
 * @property integer $storage_part_to
 * @property integer $user_from
 * @property integer $user_to
 * @property integer $storage_to
 * @property string $comment
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Storage $storageTo
 * @property Product $product
 * @property StoragePart $storagePartFrom
 * @property StoragePart $storagePartTo
 * @property User $userFrom
 * @property User $userTo
 */
class StoragePartMoves extends ActiveRecordLogUpdateTime
{
    const MOVE = 'move';
    const REMOVE = 'remove';
    const SCENARIO_IGNORE_VALIDATE_QUANTITY = 'ignore_validate_quantity';

    /**
     * @var integer
     */
    public $country;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%storage_part_moves}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'comment'], 'string'],
            [['quantity', 'product_id', 'storage_part_from'], 'required'],
            [['product_id', 'storage_part_from', 'storage_part_to', 'user_from', 'user_to', 'storage_to', 'created_at', 'updated_at'], 'integer'],
            [['quantity'], 'validateQuantity', 'skipOnEmpty' => false, 'except' => self::SCENARIO_IGNORE_VALIDATE_QUANTITY],
            [['storage_to'], 'validateStorage', 'skipOnEmpty' => false],
            [['quantity'], 'integer', 'min' => 1],
        ];
    }

    /**
     * @param string $attribute
     * @param array $params
     */
    public function validateQuantity($attribute, $params)
    {
        if ($this->quantity > $this->storagePartFrom->balance) {
            $this->addError($attribute, Yii::t('common', 'Не может быть перемещено/списано больше товаров, чем есть в исходной партии.'));
        }
    }

    /**
     * @param string $attribute
     * @param array $params
     */
    public function validateStorage($attribute, $params)
    {
        if ($this->type == self::MOVE && $this->storage_to == null) {
            $this->addError($attribute, Yii::t('common', 'При перемещении склад получения - обязательное поле'));
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', '#'),
            'type' => Yii::t('common', 'Тип'),
            'quantity' => Yii::t('common', 'Количество товаров для перемещения/списания'),
            'product_id' => Yii::t('common', 'Товар'),
            'storage_part_from' => Yii::t('common', 'Партия из'),
            'storage_part_to' => Yii::t('common', 'Партия в'),
            'comment' => Yii::t('common', 'Комментарий'),
            'user_from' => Yii::t('common', 'Отправитель'),
            'user_to' => Yii::t('common', 'Получатель'),
            'storage_to' => Yii::t('common', 'Склад получения'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStorageTo()
    {
        return $this->hasOne(Storage::className(), ['id' => 'storage_to']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStoragePartFrom()
    {
        return $this->hasOne(StoragePart::className(), ['id' => 'storage_part_from']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStoragePartTo()
    {
        return $this->hasOne(StoragePart::className(), ['id' => 'storage_part_to']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserFrom()
    {
        return $this->hasOne(User::className(), ['id' => 'user_from']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserTo()
    {
        return $this->hasOne(User::className(), ['id' => 'user_to']);
    }

    /**
     * @inheritdoc
     * @return StoragePartMovesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new StoragePartMovesQuery(get_called_class());
    }

    /**
     * @param integer $storage_part_id
     * @return integer
     */
    public static function getPartMove($storage_part_id)
    {
        $isHave = self::find()
            ->where(['storage_part_from' => $storage_part_id])
            ->andWhere(['storage_part_to' => null])
            ->limit(1)->asArray()->one();
        return count($isHave);
    }
}
