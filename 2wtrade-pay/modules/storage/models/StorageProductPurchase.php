<?php

namespace app\modules\storage\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\Product;
use app\models\Country;
use app\modules\storage\models\query\StorageProductPurchaseQuery;
use Yii;

/**
 * Class StorageProductPurchase
 * @package app\modules\storage\models
 * @property integer $id
 * @property integer $country_id
 * @property integer $product_id
 * @property string $plan_lead_day
 * @property integer $limit_days
 * @property string $limit_count
 * @property string $last_purchase_count
 * @property integer $last_purchase_date
 * @property integer $created_at
 * @property integer $updated_at
 */
class StorageProductPurchase extends ActiveRecordLogUpdateTime
{
    protected $insertLog = true;
    protected $updateLog = true;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%storage_product_purchase}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'product_id'], 'required'],
            [['country_id', 'product_id', 'limit_days', 'last_purchase_date', 'created_at', 'updated_at'], 'integer'],
            [['plan_lead_day', 'last_purchase_count', 'limit_count'], 'number'],
            [['product_id'], 'validateProduct'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @param string $attribute
     * @param array $params
     */
    public function validateProduct($attribute, $params)
    {
        if ($this->isNewRecord) {
            $countProducts = self::find()
                ->where([self::tableName() . '.country_id' => Yii::$app->user->getCountry()->id])
                ->andWhere([self::tableName() . '.product_id' => $this->product_id])
                ->limit(1)->count();
        } else {
            $countProducts = self::find()
                ->where([self::tableName() . '.country_id' => Yii::$app->user->getCountry()->id])
                ->andWhere(['!=', self::tableName() . '.id', $this->id])
                ->andWhere([self::tableName() . '.product_id' => $this->product_id])
                ->limit(1)->count();
        }

        if ($countProducts > 0) {
            $this->addError($attribute, Yii::t('common', 'Такой продукт уже есть'));
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'country_id' => Yii::t('common', 'Страна'),
            'product_id' => Yii::t('common', 'Товар'),
            'plan_lead_day' => Yii::t('common', 'Планируемое количество лидов в день'),
            'limit_days' => Yii::t('common', 'Порог в днях'),
            'limit_count' => Yii::t('common', 'Порог в количестве заказов'),
            'last_purchase_count' => Yii::t('common', 'Количество заказанного товара'),
            'last_purchase_date' => Yii::t('common', 'Дата заказа товара'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
        ];
    }

    /**
     * @inheritdoc
     * @return StorageProductPurchaseQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new StorageProductPurchaseQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @param array $row
     * @return array
     */
    public static function check($row)
    {
        if ($row['balance'] <= $row['limit_count']) {
            return [
                'class' => 'text-danger'
            ];
        }


        $fact = 0;
        try {
            $fact = $row['balance'] / $row['orders'];
        } catch (\Exception $e) {
        }
        if ($fact <= $row['limit_days']) {
            return [
                'class' => 'text-danger'
            ];
        }

        $plan = 0;
        try {
            $plan = $row['balance'] / $row['plan_lead_day'];
        } catch (\Exception $e) {
        }
        if ($plan <= $row['limit_days']) {
            return [
                'class' => 'text-warning'
            ];
        }

        return [];
    }
}