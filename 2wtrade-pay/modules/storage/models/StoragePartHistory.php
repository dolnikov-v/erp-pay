<?php
namespace app\modules\storage\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\modules\order\models\Order;
use Yii;

/**
 * Class StoragePartHistory
 * @package app\modules\storage\models
 * @property integer $id
 * @property integer $storage_part_id
 * @property string $type
 * @property integer $amount
 * @property string $order_id
 * @property integer $created_at
 * @property StoragePart $storagePart
 * @property Order $order
 */
class StoragePartHistory extends ActiveRecordLogUpdateTime
{
    const TYPE_PLUS = 'plus';
    const TYPE_MINUS = 'minus';

    public static $types = [
        self::TYPE_PLUS,
        self::TYPE_MINUS,
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%storage_part_history}}';
    }

    /**
     * @return array
     */
    public static function getCollectionTypes()
    {
        return [
            self::TYPE_PLUS => Yii::t('common', 'Поступление'),
            self::TYPE_MINUS => Yii::t('common', 'Списание'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['storage_part_id', 'type', 'amount'], 'required'],
            [
                'storage_part_id',
                'exist',
                'targetClass' => '\app\modules\storage\models\StoragePart',
                'targetAttribute' => 'id',
                'message' => Yii::t('common', 'Неверное значение складской партии.'),
            ],
            ['type', 'in', 'range' => self::$types],

            ['amount', 'number', 'min' => 0, 'on' => 'insert_storage_part', 'tooSmall' => 'Количество должно быть положительным.'],
            ['amount', 'number', 'min' => 1, 'on' => 'update_storage_part', 'tooSmall' => 'Количество должно быть больше 0.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'storage_part_id' => Yii::t('common', 'Складская партия'),
            'type' => Yii::t('common', 'Тип'),
            'amount' => Yii::t('common', 'Количество'),
            'order_id' => Yii::t('common', 'Заказ'),
            'created_at' => Yii::t('common', 'Дата добавления'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecordLogUpdateTime::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStoragePart()
    {
        return $this->hasOne(StoragePart::className(), ['id' => 'storage_part_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }
}
