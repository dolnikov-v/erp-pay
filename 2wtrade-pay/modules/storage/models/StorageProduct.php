<?php
namespace app\modules\storage\models;

use Yii;
use app\models\Product;
use app\components\db\ActiveRecordLogUpdateTime;
use app\modules\storage\models\query\StorageProductQuery;
use Zend\Validator\Date;

/**
 * Class StorageProduct
 * @property integer $id
 * @property integer $storage_id
 * @property integer $product_id
 * @property integer $balance
 * @property integer $unlimit
 * @property integer $created_at
 * @property Storage $storage
 * @property Product $product
 * @property integer $updated_at
 * @property integer $reserve
 * @property Date $shelf_life
 */
class StorageProduct extends ActiveRecordLogUpdateTime
{
    protected $updateLog = false;
    private static $collectionProducts;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%storage_product}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['storage_id', 'product_id'], 'required'],
            [['storage_id', 'product_id', 'unlimit', 'created_at', 'updated_at', 'reserve'], 'integer'],
            ['balance', 'integer', 'min' => 0],
            [['created_at', 'updated_at'], 'safe'],
            [['shelf_life'], 'date', 'format' => 'Y-m-d'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'storage_id' => Yii::t('common', 'Склад'),
            'product_id' => Yii::t('common', 'Товар'),
            'balance' => Yii::t('common', 'Баланс'),
            'unlimit' => Yii::t('common', 'Безлимитный продукт'),
            'created_at' => Yii::t('common', 'Дата добавления'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
            'shelf_life' => Yii::t('common', 'Срок годности'),
            'reserve' => Yii::t('common', 'Резерв'),
        ];
    }

    /**
     * @inheritdoc
     * @return StorageProductQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new StorageProductQuery(get_called_class());
    }

    /**
     * @param integer $paramProduct
     * @param integer $paramStorage
     * @param integer $paramQuantity
     * @param Date $paramShelfLife
     * @throws \Exception
     */
    public static function addProductOnStorage($paramProduct, $paramStorage, $paramQuantity, $paramShelfLife = null)
    {
        $product = self::find()
            ->where(['storage_id' => $paramStorage])
            ->andWhere(['product_id' => $paramProduct])
            ->andWhere(['shelf_life' => $paramShelfLife])
            ->one();
        if ($product === null) { // создадим новый товар на складе
            $product = new self();
            $product->product_id = $paramProduct;
            $product->storage_id = $paramStorage;
            $product->balance = $paramQuantity;
            $product->shelf_life = $paramShelfLife;
        } else { // добавим к балансу товара на складе
            $product->balance += $paramQuantity;
        }
        if (!$product->save()) {
            throw new \Exception(Yii::t('common', 'Не удалось добавить товар на склад {error}.', ['error' => $product->getFirstErrorAsString()]));
        }
    }

    /**
     * @return array
     */
    public static function getCollectionProducts()
    {
        if (self::$collectionProducts === null) {
            self::$collectionProducts = Product::find()->collection();
        }
        return self::$collectionProducts;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStorage()
    {
        return $this->hasOne(Storage::className(), ['id' => 'storage_id']);
    }
}