<?php

namespace app\modules\storage\models;

use Yii;
use app\models\Country;
use app\models\Product;
use yii\helpers\ArrayHelper;
use app\components\db\ActiveRecordLogUpdateTime;

/**
 * Class StoragePart
 * @property integer $id
 * @property string $price_purchase
 * @property string $logistics_delivery_storage
 * @property string $logistics_customs
 * @property string $logistics_certification
 * @property string $logistics_other
 * @property integer $number
 * @property integer $quantity
 * @property integer $product_id
 * @property integer $storage_id
 * @property integer $currency_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $active
 * @property integer $balance
 * @property integer $expected
 * @property array $existsNumbers
 * @property StoragePartHistory $storagePartHistory
 * @property Country $country
 * @property integer $expected_at
 * @property integer $unlimit
 *
 * @property Product $product
 * @property Storage $storage
 */
class StoragePart extends ActiveRecordLogUpdateTime
{
    private static $collectionProducts;
    private static $collectionProductsOnStorages;
    private static $collectionStorages;
    private static $collectionStoragesAllCountries;

    public $oldBalance;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%storage_part}}';
    }

    /**
     * @return array
     */
    public static function getCollectionStorages()
    {
        if (self::$collectionStorages === null) {
            self::$collectionStorages = Storage::find()
                ->where(['country_id' => Yii::$app->user->country->id])
                ->collection();
        }
        return self::$collectionStorages;
    }

    /**
     * @return array
     */
    public static function getCollectionStoragesAllCountries()
    {
        if (self::$collectionStoragesAllCountries === null) {
            self::$collectionStoragesAllCountries = Storage::find()
                ->where(['!=', 'country_id', Yii::$app->user->country->id])
                ->collection();
        }
        return self::$collectionStoragesAllCountries;
    }

    /**
     * @return array
     */
    public static function getCollectionProducts()
    {
        if (self::$collectionProducts === null) {
            self::$collectionProducts = Product::find()->collection();
        }
        return self::$collectionProducts;
    }

    /**
     * @return array
     */
    public static function getCollectionProductsOnStorages()
    {
        if (self::$collectionProductsOnStorages === null) {
            $storages = array_keys(self::getCollectionStorages());

            if ($storages) {
                $products = self::find()
                    ->distinct('product_id')
                    ->joinWith('product')
                    ->where(['in', 'storage_id', $storages])
                    ->all();

                self::$collectionProductsOnStorages = ArrayHelper::map($products, 'product_id', 'product.name');
            } else {
                self::$collectionProductsOnStorages = [];
            }
        }

        return self::$collectionProductsOnStorages;
    }

    /**
     * @return array
     */
    public function getExistsNumbers()
    {
        $numbers = self::find()
            ->select(['number' => 'number'])
            ->where(['storage_id' => $this->storage_id]);
        if (isset($this->number)) {
            $numbers = $numbers->andWhere(['!=', 'number', $this->number]);
        }
        $numbers = $numbers->asArray()->all();
        $numbersFinished = [];
        foreach ($numbers as $item) {
            $numbersFinished[] = $item['number'];
        }
        return $numbersFinished;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['number', 'quantity', 'price_purchase', 'product_id', 'storage_id'], 'required'],
            ['expected_at', 'required', 'when' => function($model) {return $model->expected;}, 'enableClientValidation' => false],
            [['price_purchase', 'logistics_delivery_storage', 'logistics_customs', 'logistics_certification', 'logistics_other', 'expected', 'expected_at'], 'number'],
            [['product_id', 'storage_id', 'unlimit'], 'integer'],
            [['number', 'quantity'], 'integer', 'min' => 1],
            [['shelf_life'], 'date', 'format' => 'Y-m-d'],
            ['number', 'in', 'range' => $this->getExistsNumbers(), 'not' => true, 'message' => Yii::t('common', 'Партия с таким номером для такого склада уже существует.')],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'price_purchase' => Yii::t('common', 'Цена закупки'),
            'logistics_delivery_storage' => Yii::t('common', 'Логистика: доставка и склад'),
            'logistics_customs' => Yii::t('common', 'Логистика: таможня'),
            'logistics_certification' => Yii::t('common', 'Логистика: сертификация'),
            'logistics_other' => Yii::t('common', 'Логистика: прочее'),
            'number' => Yii::t('common', 'Номер'),
            'quantity' => Yii::t('common', 'Количество'),
            'product_id' => Yii::t('common', 'Товар'),
            'storage_id' => Yii::t('common', 'Склад'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
            'active' => Yii::t('common', 'Активный'),
            'balance' => Yii::t('common', 'Баланс'),
            'expected' => Yii::t('common', 'Запланированная'),
            'expected_at' => Yii::t('common', 'Запланированная дата'),
            'unlimit' => Yii::t('common', 'Безлимитная партия'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStorage()
    {
        return $this->hasOne(Storage::className(), ['id' => 'storage_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStoragePartHistory()
    {
        return $this->hasMany(StoragePartHistory::className(), ['storage_part_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id'])
            ->via('storage');

    }

    /**
     * @param integer $storage_id
     * @return integer
     */
    public static function getMaxNumber($storage_id)
    {
        $number = self::find()
            ->select(['number' => 'max(number)'])
            ->where(['storage_id' => $storage_id])->asArray()->one();
        if (isset($number)) {
            return $number['number'] + 1;
        }
        return 1;
    }
}
