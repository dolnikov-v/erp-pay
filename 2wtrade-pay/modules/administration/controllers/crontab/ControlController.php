<?php
namespace app\modules\administration\controllers\crontab;

use app\components\filters\AjaxFilter;
use app\components\web\Controller;
use app\modules\administration\models\CrontabTask;
use app\modules\administration\models\search\CrontabTaskSearch;
use Yii;
use yii\web\BadRequestHttpException;

/**
 * Class ControlController
 * @package app\modules\administration\controllers
 */
class ControlController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => ['set-active'],
            ]
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new CrontabTaskSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return array
     * @throws BadRequestHttpException
     */
    public function actionSetActive()
    {
        $taskId = (int)Yii::$app->request->post('task_id');

        /** @var CrontabTask $task */
        $task = CrontabTask::findOne($taskId);

        if (!$task) {
            throw new BadRequestHttpException(Yii::t('common', 'Обращение к несуществущей задаче.'));
        }

        $task->active = Yii::$app->request->post('value') == 'on' ? 1 : 0;

        if (!$task->save(true, ['active'])) {
            throw new BadRequestHttpException(Yii::t('common', 'Не удалось выполнить запрос.'));
        }

        return [
            'success' => true,
        ];
    }
}
