<?php
namespace app\modules\administration\controllers\crontab;

use app\components\web\Controller;
use app\models\Country;
use app\modules\administration\components\crontab\Logger;
use app\modules\administration\models\CrontabTask;
use app\modules\administration\models\CrontabTaskLog;
use app\modules\administration\models\CrontabTaskLogAnswer;
use app\modules\administration\models\search\CrontabTaskLogAnswerSearch;
use app\modules\administration\models\search\CrontabTaskLogSearch;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * Class LogsController
 * @package app\modules\administration\controllers
 */
class LogsController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new CrontabTaskLogSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'modelSearch' => $modelSearch,
            'dataProvider' => $dataProvider,
            'tasks' => CrontabTask::find()->collection(),
        ]);
    }

    /**
     * @param $id
     * @return string
     */
    public function actionAnswers($id)
    {
        $model = $this->findModel($id);

        $modelSearch = new CrontabTaskLogAnswerSearch();

        $queryParams = Yii::$app->request->queryParams;
        $queryParams['CrontabTaskLogAnswerSearch']['log_id'] = $id;

        $dataProvider = $modelSearch->search($queryParams);

        $countries = Country::find()->all();

        return $this->render('answers', [
            'model' => $model,
            'modelSearch' => $modelSearch,
            'dataProvider' => $dataProvider,
            'countries' => $countries
        ]);
    }

    /**
     * @param $id
     * @return string
     */
    public function actionAnswer($id)
    {
        $model = $this->findModelAnswer($id);

        return $this->render('answer', [
            'model' => $model,
            'data' => json_decode($model->data, true),
            'answer' => json_decode(Logger::read($model->file_answer), true),
        ]);
    }

    /**
     * @param $id
     * @return CrontabTaskLog
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        /** @var CrontabTaskLog $model */
        $model = CrontabTaskLog::find()
            ->joinWith('task')
            ->where([CrontabTaskLog::tableName() . '.id' => $id])
            ->one();

        if (!$model) {
            throw new NotFoundHttpException(Yii::t('common', 'Лог не найден.'));
        }

        return $model;
    }

    /**
     * @param $id
     * @return CrontabTaskLogAnswer
     * @throws NotFoundHttpException
     */
    protected function findModelAnswer($id)
    {
        /** @var CrontabTaskLogAnswer $model */
        $model = CrontabTaskLogAnswer::find()
            ->joinWith([
                'log',
                'log.task',
            ])
            ->where([CrontabTaskLogAnswer::tableName() . '.id' => $id])
            ->one();

        if (!$model) {
            throw new NotFoundHttpException(Yii::t('common', 'Ответ лога не найден.'));
        }

        return $model;
    }
}
