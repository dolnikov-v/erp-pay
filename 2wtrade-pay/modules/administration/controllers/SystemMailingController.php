<?php
namespace app\modules\administration\controllers;

use app\components\web\Controller;
use app\models\User;
use Yii;
use yii\helpers\Url;

/**
 * Class SystemController
 * @package app\modules\administration\controllers
 */
class SystemMailingController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        if (isset(Yii::$app->request->queryParams['Message']) && !empty(Yii::$app->request->queryParams['Message'])) {
            if ($this->sendMassMail()) {
                Yii::$app->notifier->addNotification(Yii::t('common', 'Сообщение успешно разослано'), 'success');
            }
            else {
                Yii::$app->notifier->addNotification(Yii::t('common', 'Ошибка отправки сообщения!'), 'danger');
            }
        }

        return $this->render('index');
    }

    /**
     * @return bool
     */
    public function sendMassMail()
    {
        // Получаем список всех активных пользователей
        $allUser = User::find()
            ->where(["=", User::tableName() .".status", 0])
            ->asArray()
            ->all();

        $mailUsers = [];

        foreach ($allUser as $user) {
            if (!empty($user['email']) && !is_null($user['email'])) {
                $mailUsers[] = $user['email'];
            }
        }

        $res = true;                        // флаг успешной рассылки
        if (!empty($mailUsers)) {
            foreach ($mailUsers as $recipient) {
                $sent = Yii::$app->mailer->compose()
                    ->setFrom('admin@2wtrade-pay.com')
                    ->setTo($recipient)
                    ->setSubject('Внимание! Общее уведомление.')
                    ->setTextBody(Yii::$app->request->queryParams['Message']['message'])
                    ->setHtmlBody('<b>' .Yii::$app->request->queryParams['Message']['message'] .'</b>')
                    ->send();
                if (!$sent) {
                    $res = false;
                }
            }
        }
        else {
            $res = false;
        }

        return $res;
    }

}
