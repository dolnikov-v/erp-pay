<?php

namespace app\modules\administration\controllers;

use app\components\web\Controller;
use app\models\User;
use app\modules\access\jobs\AccessFlushPasswordsJob;
use Yii;
use yii\helpers\Url;

/**
 * Class SystemController
 * @package app\modules\administration\controllers
 */
class SystemController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @return mixed
     */
    public function actionFlushCache()
    {
        Yii::$app->cache->flush();

        Yii::$app->notifier->addNotification(Yii::t('common', 'Cache успешно очищен.'), 'success');

        return $this->redirect(Url::toRoute(['index']));
    }

    /**
     * @return \yii\web\Response
     */
    public function actionClearAssets()
    {
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Пользователи Windows не могут чистить Assets.'));
        } else {
            foreach (glob(Yii::$app->assetManager->basePath . DIRECTORY_SEPARATOR . '*') as $asset) {
                if (is_link($asset)) {
                    unlink($asset);
                } elseif (is_dir($asset)) {
                    $this->deleteDir($asset);
                } else {
                    unlink($asset);
                }
            }

            Yii::$app->notifier->addNotification(Yii::t('common', 'Assets успешно очищены.'), 'success');
        }

        return $this->redirect(Url::toRoute(['index']));
    }

    /**
     * @return mixed
     */
    public function actionDropAllPass()
    {
        $currentUser = Yii::$app->user->id;

        $users = User::find()->all();

        foreach ($users as $user) {
            if (!$user->isSuperadmin && $user->id != $currentUser && $user->email) {
                Yii::$app->queue->push(new AccessFlushPasswordsJob(['userId' => $user->id]));
            }
        }

        Yii::$app->notifier->addNotification(Yii::t('common', 'Все пароли успешно сброшены'), 'success');

        return $this->redirect(Url::toRoute(['index']));
    }

    /**
     * @param $directory
     * @return bool
     */
    private function deleteDir($directory)
    {
        $iterator = new \RecursiveDirectoryIterator($directory, \RecursiveDirectoryIterator::SKIP_DOTS);
        $files = new \RecursiveIteratorIterator($iterator, \RecursiveIteratorIterator::CHILD_FIRST);

        foreach ($files as $file) {
            if ($file->isDir()) {
                rmdir($file->getRealPath());
            } else {
                unlink($file->getRealPath());
            }
        }

        return rmdir($directory);
    }

}
