<?php
use app\components\widgets\ActiveForm;
use app\modules\ticket\models\Message;
?>


<?php
$model = new Message();

$form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    'enableClientValidation' => false,
]);
?>

<div class="row">
    <div class="col-lg-12">
        <?=$form->field($model,'message')->textarea(['placeholder'=>'Введите текст сообщения...'])->label(Yii::t('common','Сообщение')); ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Разослать всем')) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

