<?php
use app\widgets\Panel;

/** @var \yii\web\View $this */

$this->title = Yii::t('common', 'Почтовые оповещения');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Администрирование'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Почтовые оповещения')];
?>

<div class="row">
    <div class="col-lg-12">
        <?= Panel::widget([
            'title' => Yii::t('common', 'Массовая рассылка оповещения'),
            'content' => $this->render('_index-content'),
        ]); ?>
    </div>
</div>
