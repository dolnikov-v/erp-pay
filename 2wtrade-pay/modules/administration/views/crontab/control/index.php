<?php
use app\components\grid\DateColumn;
use app\components\grid\GridView;
use app\helpers\DataProvider;
use app\modules\administration\assets\crontab\ControlAsset;
use app\modules\administration\models\CrontabTaskLog;
use app\widgets\Label;
use app\widgets\Panel;
use app\widgets\Switchery;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

ControlAsset::register($this);

$this->title = Yii::t('common', 'Управление');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Администрирование'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Crontab'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с задачами'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function ($model) {
            return [
                'class' => 'tr-vertical-align-middle ' . ($model->active ? '' : 'text-muted'),
            ];
        },
        'columns' => [
            [
                'attribute' => 'description',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'lastLog.status',
                'headerOptions' => ['class' => 'text-center width-150'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return (isset($model->lastLog[0])) ? Label::widget([
                        'label' => CrontabTaskLog::getStatusesCollection()[$model->lastLog[0]->status],
                        'style' => $model->lastLog[0]->getStyle(),
                    ]) : '';
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'lastLog.created_at',
                'content' => function ($model) {
                    return (isset($model->lastLog[0])) ? Yii::$app->formatter->asDatetime($model->lastLog[0]->created_at) : '';
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'active',
                'headerOptions' => ['class' => 'text-center width-150'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return Switchery::widget([
                        'checked' => $model->active ? true : false,
                        'attributes' => [
                            'data-task-id' => $model->id,
                            'data-action' => 'crontab-active-switchery',
                        ],
                    ]);
                },
                'enableSorting' => false,
            ],
        ],
    ]),
]) ?>
