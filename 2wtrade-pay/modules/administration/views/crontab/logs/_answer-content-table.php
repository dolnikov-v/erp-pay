<?php
use yii\helpers\Html;
use yii\helpers\VarDumper;

/** @var yii\web\View $this */
/** @var array $messages */
?>

<table class="table table-striped table-hover table-sortable tl-fixed">
    <thead>
    <tr>
        <th><?= Yii::t('common', 'Переменная') ?></th>
        <th><?= Yii::t('common', 'Значение') ?></th>
    </tr>
    </thead>
    <tbody>
    <?php if ($messages && is_array($messages)): ?>
        <?php foreach ($messages as $key => $item): ?>
            <tr>
                <td><?= Html::encode($key) ?></td>
                <td>
                    <?php if (is_array($item)) : ?>
                        <pre><?= VarDumper::dumpAsString($item) ?></pre>
                    <?php else: ?>
                        <?php $array = @json_decode($item, true); ?>
                        <?php if ($array): ?>
                            <pre><?= VarDumper::dumpAsString($array) ?></pre>
                        <?php else: ?>
                            <?= $item ?>
                        <?php endif; ?>
                    <?php endif; ?>
                </td>
            </tr>
        <?php endforeach; ?>
    <?php else: ?>
        <tr>
            <td class="text-center" colspan="2"><?= Yii::t('common', 'Данные отсутствуют') ?></td>
        </tr>
    <?php endif; ?>
    </tbody>
</table>
