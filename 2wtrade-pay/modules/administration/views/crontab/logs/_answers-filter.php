<?php
use app\components\widgets\ActiveForm;
use app\modules\administration\models\CrontabTaskLogAnswer;
use yii\helpers\Url;
use app\widgets\assets\DatePickerAsset;
use yii\helpers\ArrayHelper;

/** @var yii\web\View $this */
/** @var app\models\Country[] $countries */
/** @var app\modules\administration\models\CrontabTaskLog $model */
/** @var app\modules\administration\models\search\CrontabTaskLogAnswerSearch $modelSearch */

DatePickerAsset::register($this);
?>

<?php
/** @var ActiveForm $form */
$form = ActiveForm::begin([
    'action' => ['answers', 'id' => $model->id],
    'method' => 'get',
    'enableClientValidation' => false,
]);
?>

<div class="row">
    <div class="col-lg-6">
        <?= $form->field($modelSearch, 'created_at')->dateRangePicker('dateFrom', 'dateTo') ?>
    </div>
    <div class="col-lg-6">
        <?= $form->field($modelSearch, 'status')->select2List(CrontabTaskLogAnswer::getStatusesCollection(), [
            'prompt' => '—',
        ]) ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-3">
        <?= $form->field($modelSearch, 'orderId')->textInput(['placeholder' => 'Номер заказа'])->label('Номер заказа') ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($modelSearch, 'countryCode')->select2List(ArrayHelper::map($countries, 'char_code', 'name'), [
            'prompt' => '—',
        ]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($modelSearch, 'searchEveryWhere')->checkbox() ?>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <?= $form->submit(Yii::t('common', 'Применить')) ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute(['answers', 'id' => $model->id])) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
