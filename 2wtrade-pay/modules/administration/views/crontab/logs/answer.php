<?php
use app\widgets\ButtonLink;
use app\widgets\Panel;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \app\modules\administration\models\CrontabTaskLogAnswer $model */
/** @var array $data */
/** @var array $answer */

$this->title = Yii::t('common', 'Детальный просмотр');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Администрирование'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Crontab'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Логи'), 'url' => Url::toRoute('index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Детальный просмотр'),
    'content' => $this->render('_answer-content', [
        'model' => $model,
        'data' => $data,
        'answer' => $answer,
    ]),
    'footer' => ButtonLink::widget([
        'label' => Yii::t('common', 'Вернуться назад'),
        'url' => Url::toRoute(['answers', 'id' => $model->log_id]),
        'style' => ButtonLink::STYLE_DEFAULT,
        'size' => ButtonLink::SIZE_SMALL,
    ])
]) ?>
