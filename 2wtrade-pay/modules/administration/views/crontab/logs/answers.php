<?php
use app\components\grid\ActionColumn;
use app\components\grid\DateColumn;
use app\components\grid\GridView;
use app\helpers\DataProvider;
use app\modules\administration\components\crontab\Logger;
use app\modules\administration\models\CrontabTaskLogAnswer;
use app\widgets\Label;
use app\widgets\Panel;
use yii\helpers\Url;
use app\widgets\LinkPager;

/** @var yii\web\View $this */
/** @var app\models\Country[] $countries */
/** @var app\modules\administration\models\CrontabTaskLog $model */
/** @var app\modules\administration\models\search\CrontabTaskLogAnswerSearch $modelSearch */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = Yii::t('common', $model->task->description);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Администрирование'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Crontab'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Логи'), 'url' => Url::toRoute('index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'content' => $this->render('_answers-filter', [
        'model' => $model,
        'modelSearch' => $modelSearch,
        'countries' => $countries
    ]),
    'collapse' => true,
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с ответами'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => [
            'class' => 'table table-striped table-hover table-sortable tl-fixed',
        ],
        'columns' => [
            [
                'content' => function ($model) {
                    return Logger::readPart($model->file_answer);
                },
                'contentOptions' => ['class' => 'ellipsis'],
                'enableSorting' => false,
            ],
            [
                'attribute' => 'status',
                'headerOptions' => ['class' => 'text-center width-150'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return Label::widget([
                        'label' => CrontabTaskLogAnswer::getStatusesCollection()[$model->status],
                        'style' => $model->status == CrontabTaskLogAnswer::STATUS_SUCCESS ? Label::STYLE_SUCCESS : Label::STYLE_DANGER,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'created_at',
                'enableSorting' => false,
            ],
            'actions' => [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Детальный просмотр'),
                        'url' => function ($model) {
                            return Url::toRoute(['answer', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('administration.crontab.logs.answer');
                        },
                    ],
                ],
            ],
        ],
    ]),
    'footer' => LinkPager::widget(['pagination' => $dataProvider->getPagination(),
                                   'counterPosition' => LinkPager::COUNTER_POSITION_RIGHT,]),
]) ?>
