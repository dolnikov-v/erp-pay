<?php
use yii\helpers\Html;

/** @var \yii\web\View $this */
/** @var \app\modules\administration\models\CrontabTaskLogAnswer $model */
/** @var array $data */
/** @var array $answer */
?>

<div class="row">
    <div class="col-lg-12 margin-bottom-20">
        <h4 class="example-title"><?= Yii::t('common', 'URL') ?></h4>
        <div><?= Html::input('text', '', $model->url, ['class' => 'form-control', 'readonly' => 'readonly']) ?></div>
    </div>
    <div class="col-lg-6">
        <h4 class="example-title"><?= Yii::t('common', 'Данные') ?></h4>
        <?= $this->render('_answer-content-table', ['messages' => $data]) ?>
    </div>
    <div class="col-lg-6">
        <h4 class="example-title"><?= Yii::t('common', 'Ответ') ?></h4>
        <?= $this->render('_answer-content-table', ['messages' => $answer]) ?>
    </div>
</div>
