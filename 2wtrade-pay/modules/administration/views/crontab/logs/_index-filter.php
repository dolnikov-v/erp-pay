<?php
use app\components\widgets\ActiveForm;
use app\modules\administration\models\CrontabTaskLog;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var app\modules\administration\models\search\CrontabTaskLogSearch $modelSearch */
/** @var array $tasks */
?>

<?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>

<div class="row">
    <div class="col-lg-8">
        <?= $form->field($modelSearch, 'task_id')->select2List($tasks, [
            'prompt' => '—',
            'length' => false
        ]) ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($modelSearch, 'status')->select2List(CrontabTaskLog::getStatusesCollection(), [
            'prompt' => '—',
        ]) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($modelSearch, 'orderId')->textInput(['placeholder' => 'Номер заказа'])->label('Номера заказа (через запятую)') ?>
    </div>
    <div class="col-lg-6">
        <?= $form->field($modelSearch, 'created_at')->dateRangePicker('dateFrom', 'dateTo') ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($modelSearch, 'searchText')->textInput(['placeholder' => 'Текст для поиска', 'disabled' => 'true'])->label('Поиск по тексту') ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')) ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
