<?php
use app\components\grid\ActionColumn;
use app\components\grid\DateColumn;
use app\components\grid\GridView;
use app\helpers\DataProvider;
use app\modules\administration\models\CrontabTaskLog;
use app\widgets\Label;
use app\widgets\Panel;
use yii\helpers\Url;
use app\widgets\LinkPager;

/** @var yii\web\View $this */
/** @var app\modules\administration\models\search\CrontabTaskLogSearch $modelSearch */
/** @var yii\data\ActiveDataProvider $dataProvider */
/** @var array $tasks */

$this->title = Yii::t('common', 'Логи');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Администрирование'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Crontab'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'content' => $this->render('_index-filter', [
        'modelSearch' => $modelSearch,
        'tasks' => $tasks,
    ]),
    'collapse' => true,
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с логами'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'label' => Yii::t('common', 'Задача'),
                'attribute' => 'task.description',
                'enableSorting' => false,
            ],
            [
                'label' => Yii::t('common', 'Ответов'),
                'headerOptions' => ['class' => 'text-center width-100'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return count($model->answers);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'status',
                'headerOptions' => ['class' => 'text-center width-150'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return Label::widget([
                        'label' => CrontabTaskLog::getStatusesCollection()[$model->status],
                        'style' => $model->getStyle(),
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'created_at',
                'enableSorting' => false,
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'updated_at',
                'enableSorting' => false,
            ],
            'actions' => [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Просмотреть ответы'),
                        'url' => function ($data) {
                            $orderId = '';
                            if (isset(yii::$app->request->queryParams['CrontabTaskLogSearch']['orderId'])) $orderId = yii::$app->request->queryParams['CrontabTaskLogSearch']['orderId'];
                            return Url::toRoute(['answers', 'id' => $data->id, 'orderId' => $orderId]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('administration.crontab.logs.answers');
                        },
                    ],
                ],
            ],
        ],
    ]),
    'footer' => LinkPager::widget(['pagination' => $dataProvider->getPagination(),
                                   'counterPosition' => LinkPager::COUNTER_POSITION_RIGHT,]),
]) ?>
