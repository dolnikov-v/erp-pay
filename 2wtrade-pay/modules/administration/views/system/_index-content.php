<?php
use app\widgets\ButtonLink;
use yii\helpers\Url;
?>

<?= ButtonLink::widget([
    'url' => Url::toRoute(['flush-cache']),
    'icon' => '<i class="icon wb-memory"></i>',
    'label' => Yii::t('common', 'Очистить Cache'),
]) ?>

<?= ButtonLink::widget([
    'url' => Url::toRoute(['clear-assets']),
    'icon' => '<i class="icon wb-trash"></i>',
    'label' => Yii::t('common', 'Очистить Assets'),
]) ?>

<?php
    if (Yii::$app->user->isSuperadmin) {
        echo ButtonLink::widget([
            'url' => Url::toRoute(['drop-all-pass']),
            'icon' => '<i class="icon wb-lock"></i>',
            'label' => Yii::t('common', 'Сбросить пароли всем пользователям'),
            'style' => ButtonLink::STYLE_WARNING,
        ]);
    }
?>
