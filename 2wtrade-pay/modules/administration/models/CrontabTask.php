<?php

namespace app\modules\administration\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\modules\administration\models\query\CrontabTaskQuery;
use Yii;

/**
 * Class CrontabTask
 * @package app\modules\administration\models
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $active
 * @property integer $created_at
 * @property integer $updated_at
 * @property \yii\db\ActiveQuery $lastLog
 * @property CrontabTaskLog $LastLog
 */
class CrontabTask extends ActiveRecordLogUpdateTime
{
    const TASK_CLEAN_USER_ACTION_LOG = 'clean_user_action_log';
    const TASK_CLEAN_API_LOG = 'clean_api_log';
    const TASK_CLEAN_CRONTAB_TASK_LOG = 'clean_crontab_task_log';
    const TASK_CHECK_CRONTAB_LOG = 'check_crontab_log';
    const TASK_UPDATE_CURRENCIES_RATES = 'update_currencies_rates';
    const TASK_MAILER_SEND_NOTIFICATIONS = 'mailer_send_notifications';
    const TASK_DELIVERY_SEND_ORDERS = 'delivery_send_orders';
    const TASK_DELIVERY_GET_ORDERS_INFO = 'delivery_get_orders_info';
    const TASK_DELIVERY_MONITOR_DELIVERED = 'monitor_delivered_anomalies';
    const TASK_DELIVERY_SEND_LOGISTIC_LISTS = 'delivery_send_logistic_lists';
    const TASK_DELIVERY_AUTO_INVOICE_SENDER = 'auto_invoice_sender';  // генерация и отправка инвойсов по параметрам (КС->инвойсирование)
    const TASK_CALL_CENTER_CREATE_REQUESTS = 'call_center_create_requests';
    const TASK_CALL_CENTER_SEND_REQUESTS = 'call_center_send_requests';
    const TASK_CALL_CENTER_GET_STATUSES = 'call_center_get_statuses';
    const TASK_CALL_CENTER_GET_NEW_ORDERS = 'call_center_get_new_orders';
    const TASK_CALL_CENTER_RESEND_REQUESTS = 'call_center_resend_requests';
    const TASK_REPORT_INSERT_DEBTS = 'report_insert_debts';
    const TASK_REPORT_ORDER_DAILY = 'report_order_daily';
    const TASK_REPORT_ORDER_TWO_DAYS_ERROR = 'report_order_two_days_error';
    const TASK_DELIVERY_NOT_DELIVERED_ORDERS = 'not_delivered_orders';
    const TASK_DELIVERY_REPORT_ANOMALIES = "delivery_anomalies_daily_notifications";
    const TASK_CLEAN_MEDIA = 'clean_media';
    const TASK_DELIVERY_REPORT_PARSE_REPORTS = 'delivery_report_parse_reports';
    const TASK_DELIVERY_REPORT_NOTIFICATION_DELAY = 'delivery_report_notification_delay';
    const TASK_REPORT_PRODUCT_ON_STORAGE = 'product_on_storage';
    const TASK_REPORT_COMPARE_WITH_ADCOMBO = 'report_compare_with_adcombo';
    const TASK_CLEAN_NOTIFICATIONS = 'clean_notifications';
    const TASK_SEND_STATS_NEWRELIC = 'send_stats_newrelic';
    const TASK_REGISTER_STATS_LOCAL = 'register_stats_local';
    const TASK_REPORT_NO_PRODUCTS_FOR_ORDER = 'no_products_for_order';
    const TASK_REPORT_DELIVERY_REJECTED = 'report_rejected_by_delivery';
    const TASK_MAILER_SEND_DELIVERY_PAYMENT_REMINDERS = "mailer_send_delivery_payment_reminders";
    const TASK_REPORT_COMPARE_WITH_CALL_CENTER = 'report_compare_with_call_center';
    const TASK_REPORT_STALE_CC_APPOVED = 'report_stale_cc_approved';
    const TASK_REPORT_STALE_DELIVERY_PENDING = 'report_stale_delivery_pending';
    const TASK_ORDER_NEW_STATUS_STALE = 'report_stale_new_order';
    const TASK_DELIVERY_GENERATE_LISTS = 'delivery_generate_lists';
    const TASK_DELIVERY_GENERATE_CORRECTION_LISTS = 'delivery_generate_correction_lists';
    const TASK_REPORT_NO_TRACK_SENT_ORDERS = 'report_no_track_sent_orders';
    const TASK_CALL_CENTER_SEND_CHECK_ORDER = 'send_check_order';
    const TASK_REPORT_ORDER_DAILY_FLUCTUATION = 'report_order_dayly_fluctuation';
    const TASK_SMS_NOTIFIER_SEND_NOTIFICATIONS = 'sms_notifier_send_notifications';   // Отправка SMS нотификаций пользователям
    const TASK_REPORT_CALCULATE_DELIVERY_DEBTS = 'report_calculate_delivery_debts';
    const TASK_REPORT_IMPORT_OLD_DELIVERY_DEBTS = 'report_import_old_delivery_debts';
    const TASK_REPORT_ORDERS_UNSENT = 'report_orders_unsent';           //  уведомления о неотправленных заказах находящизся в колонках 1, 2, 6, 19, 31
    const TASK_IMPORT_1C_COSTS = 'import_1c_costs';
    const TASK_SEND_TO_1C_CHANGES = 'send_to_1c_changes';
    const TASK_DELIVERY_GET_ORDERS_INFO_FOR_LISTS = 'delivery_get_orders_info_for_lists';
    const TASK_REPORT_STATUS_IN_1_2 = 'report_orders_status_in_1_2';    // Скопление в колонках 1 и 2 в сумме по всем странам
    const TASK_REPORT_STATUS_IN_31 = 'report_orders_status_in_31';      // Скопление заказов в 31й колонке
    const TASK_REPORT_ORDERS_FROZEN = 'report_orders_frozen';      // Скопление заказов в 1,2,6,9,12,14,15,16,19,27,31,32 колонке
    const TASK_REPORT_ORDERS_STILL_FROZEN = 'report_orders_still_frozen';      // Скопление заказов в 1,2,6,9,12,14,15,16,19,27,31,32 колонке
    const TASK_REPORT_NO_ANSWER_FROM_CC = 'report_no_answer_from_cc';   // Не получали больше часа статусов из КЦ по всем странам
    const TASK_CACHE_WIDGETS = 'cache_widgets';                         // Кеширование данных для виджетов
    const TASK_DELIVERY_CHECKING_DELIVERY_PERIOD = 'delivery_checking_delivery_period';
    const TASK_GENERATE_EXPORT_FILES = 'generate_export_files';         // Генерация заказанных файлов экспорта
    const TASK_CALL_CENTER_GET_USERS = 'get_call_center_users';         // Получение списка пользователей Колл Центра
    const TASK_SEND_ORDER_CLARIFICATION = 'send_order_clarification';   // Отправка на уточнение в КС писем с выгрузкой заказов в файл
    const TASK_CC_GET_CHECKING_ORDER_INFO = 'cc_get_checking_order_info'; // Получение инфы о заявках в КЦ из очередей для проверки
    const TASK_TAKE_SMS_POLL_HISTORY = 'take_sms_poll_history'; // Получение инфы о заявках в КЦ из очередей для проверки
    const TASK_ORDER_NOTIFIER_SEND_NOTIFICATIONS = 'order_notifier_send_notifications';
    const TASK_ORDER_NOTIFIER_CHECK_NOTIFICATIONS = 'order_notifier_check_notifications';
    const TASK_GET_ANSWERS_FOR_SMS_POLL_HISTORY = 'get_answers_for_sms_poll_history'; // Получение ответов на смс опросы
    const TASK_LEAD_LOG_SUCCESS_ABSENT_10MINUTS = 'lead_log_success_absent_10minuts'; //NPAY1294
    const TASK_GET_ANSWERS_FOR_NOTIFICATION = 'get_answers_for_notification'; //Получение смс, email ответов на уведомления
    const TASK_PACKAGER_SEND_ORDERS_TO_PACKAGING = 'packager_send_orders_to_packaging'; // Отправка заказов на упаковку
    const TASK_REPORT_RAPID_INCREASE_ORDERS_IN_AUTOTRASH = 'report_rapid_increase_orders_in_autotrash'; // Уведомление о быстром увеличении автотрешей
    const TASK_SMS_BUYOUTS_NOTIFICATION = 'sms_buyouts_notification';  // В 7 утра генерим sms о выкупах за 15 дней
    const TASK_SKYPE_BUYOUTS_COORDINATION = 'skype_buyouts_coordination';  // Сообщение (В 7 утра генерим sms о выкупах за 15 дней) в чат Координация
    const TASK_BUYOUTS_BY_COUNTRY = 'buyouts_by_country';  // Выкуп и средний чек п странам в скайп за 7/15/30/45 дней
    const TASK_DELIVERY_SEND_ORDERS_TO_UPDATE_QUEUE = 'delivery_send_orders_to_update_queue';
    const TASK_CC_GET_OPERS_ACTIVITY_DATA = 'get_opers_activity_data';  // Получение отработанных часов из КЦ по пользователям
    const TASK_CC_GET_OPERS_NUMBER_OF_CALLS = 'get_opers_number_of_calls';  // Получение кол-ва звонков из КЦ по пользователям
    const TASK_ACTUALIZE_OPERS_DIRECTIONS = 'actualize_opers_directions';  // Актуализация направлений у оперов
    const TASK_STORAGE_PRODUCT_PURCHASE = 'storage_product_purchase'; // отправка оповещений по закупкам товара в разделе Склады
    const TASK_CC_SEND_CHECK_DELIVERY = 'send_check_delivery'; // отправлять по 10 заказов раз в сутки в статусе 15 в очередь check_delivery
    const TASK_DELIVERY_CALCULATE_CHARGES = 'delivery_calculate_charges'; // пересчитывает расходы на доставку заказа
    const TASK_VOIP_EXPENSE = 'voip_expense'; // пересчитывает расходы на доставку заказа
    const TASK_SYNC_EMPLOYEES = 'sync_employees'; // синхронизирует список сотрудников в crocotime и в пее
    const TASK_SYNC_OFFICES = 'sync_offices'; // синхронизирует отделы в crocotime и CallCenterOffice в пее
    const TASK_WORK_TIME = 'work_time'; // забирает рабочее время crocotime
    const TASK_CALL_CENTER_SEND_REQUESTS_TO_QUEUE = 'cc_send_requests_to_queue'; // Отправка заявок на обзвон через очередь Амазона
    const TASK_DELIVERY_GENERATE_EXPRESS_LISTS = 'delivery_generate_express_lists'; // Генерация и отправка экспресс листов
    const TASK_DELIVERY_GENERATE_PRETENSION_LISTS = 'delivery_generate_pretension_lists'; // Генерация и отправка листов претензий
    const TASK_DELIVERY_SAVE_TRANSFERRED_ORDERS = 'delivery_save_transferred_orders'; // Попытка сохранения заказов, которые не удалось сохранить при отправке в КС
    const TASK_CALL_CENTER_TRY_TO_SAVE_SENT_LEADS = 'cc_try_to_save_sent_leads'; // Попытка сохранения заказов, которые не удалось сохранить при отправке в КЦ
    const TASK_CALL_CENTER_NOT_APPROVE_NOTIFICATION = 'cc_not_approve_notification'; // Уведомление о том, что за последнее время снизился апрув
    const TASK_CALL_CENTER_HOLD_LEADS = 'cc_hold_leads'; // Уведомление о том, что за последние сутки % холдов превысил 50%
    const TASK_CALL_CENTER_NO_APPROVE_LAST_HOUR = 'cc_no_approve_last_hour'; // Уведомление о том, что за последний час не было апрувов
    const TASK_CALL_CENTER_CHECK_DAY_APPROVE = 'cc_check_day_approve'; // Уведомление о том, сколько процент аппрува с начала дня
    const TASK_CALL_CENTER_DAILY_REPORT = 'cc_daily_report'; // Уведомление отчет работы кц
    const TASK_CALL_CENTER_EFFICIENCY_OPERATORS = 'cc_efficiency_operators'; // Уведомление эффективности операторов
    const TASK_ADCOMBO_NOT_GET_STATUSES = 'adcombo_not_get_statuses'; // Уведомление о том, что за последнее время адкомбо не забирает статусы
    const TASK_MONITOR_ORDER_STATUSES = 'monitor_order_statuses'; // Мониторинг статусов заказов на скопление больше определенного времени и создание задач на исправление ситуации в Jira
    const TASK_REPEAT_MONITOR_ORDER_STATUSES = 'repeat_monitor_order_statuses'; // Повторный мониторинг статусов заказов на скопление больше определенного времени и создание задач на исправление ситуации в Jira
    const TASK_MAKE_GREETING_CARDS = 'make_greeting_cards'; // Проверяем и генерим поздравления с днем рождения и праздниками для сотрудников
    const TASK_SEND_GREETING_CARDS = 'send_greeting_cards'; // Отправляем поздравления сотрудникам на почту
    const TASK_FREE_STORAGE_HISTORY_ORDER = 'free_storage_history_order'; // распределение товаров по складам (без штрих-кодов)
    const TASK_MAKE_AUTO_PENALTY = 'make_auto_penalty'; // автоматические штрафы сотрудникам
    const TASK_SEND_SALARY_SHEET = 'send_salary_sheet'; // отправить расчетки сотрудникам зп проекта
    const TASK_GET_TELEGRAM_CHATS = 'get_telegram_chats'; // Telegram получить список чатов (пользователей) бота
    const TASK_SEND_TELEGRAM_NOTIFICATIONS = 'send_telegram_notifications'; // Telegram отправка нотификаций пользователям
    const TASK_SEND_HOLDS_NOTIFICATIONS = 'send_holds_notifications'; // Отправка уведомлений о холдах (утром и вечером)
    const TASK_AUTO_CHECK_LIST = 'auto_check_list'; // Автоматическая проверка пунктов чек листа
    const TASK_ORDERS_IN_HOLD_STATUS = 'orders_in_hold_status'; // Заказы в статусах холдов с тенденциями и разбивкой по командам и странам
    const TASK_ORDERS_IN_READY_TO_SEND_STATUS = 'orders_in_ready_to_send_status'; // Заказы в статусах готовых к оправке в КС с тенденциями
    const TASK_CALCULATED_PARAM = 'calculated_param'; // Вычисляемые параметры (пока только для отчетов)
    const TASK_REPORT_GENERATE_INVOICES = 'report_generate_invoices'; // Генерация инвойсов
    const TASK_CALL_CENTER_GET_USER_ORDER = 'get_call_center_user_order'; // Получение звонков по заказам операторов Колл Центра
    const TASK_CALL_CENTER_GET_PRODUCT_CALL = 'get_call_center_product_call'; // Получение звонков по продуктам операторов Колл Центра (по дням)
    const TASK_REPORT_ADCOMBO_STATUS = 'adcombo_status';
    const TASK_JIRA_MONITORING_DELIVERING = 'monitoring_delivering';
    const TASK_SOURCE_PSEUDO_APPROVE = 'source_pseudo_approve';
    const TASK_SOURCE_TRASH_HOLDS = 'source_trash_holds';
    const TASK_AUTO_GENERATE_WORK_PLANE = 'auto_generate_work_plan'; //https://2wtrade-tasks.atlassian.net/browse/ERP-561  автогенерация рабочего плана автоматом 25 числа каждого месяца на следующий месяц
    const TASK_ADCOMBO_PARSER = 'adcombo_parser';
    const TASK_ADCOMBO_PARSE_APPROVE_PENALTIES = 'adcombo_parse_approve_penalties';
    const TASK_REPORT_STATUS_IN_12_9_37_38 = 'report_orders_status_in_12_9_37_38';    // Скопление в колонке 12, 9, 37, 38 c группировкой по странам КС
    const TASK_REPORT_CALCULATE_DELIVERY_DEBTS_OLD = 'report_calculate_delivery_debts_old';
    const TASK_MAKE_MONTHLY_BONUS = 'make_monthly_bonus'; // ежемесячные бонусы в ЗП проект
    const TRIGGER_ORDER_LOGISTIC_CHECK_SENDING_LIST = 'order_logistic_check_sending_list'; // Проверка часовой задержки отправки листов
    const TASK_CALL_CENTER_REQUEST_ACTUALITY = 'check_call_center_request_actuality'; // предупреждение о задержке обновления статусов
    const TASK_CALL_CENTER_CHECKING_STATUS = 'call_center_checking_status'; // Запрос статусов обзвона в КЦ
    const TASK_REQUESTS_ERROR_SUMMARY = 'requests_error_summary'; // ошибки заявок в КЦ и КС
    const TASK_PROFIT_STAT_TO_SKYPE = 'profit_stat_to_skype'; // сумарная инфо по прибыли в скайп
    const TASK_SEND_SKYPE_NOTIFICATIONS = 'send_skype_notifications'; // отправка уведомлений в Skype
    const TASK_DEBTS_NOTIFICATION_FOR_THREE_MONTH = 'debts_notification_three_month'; // нотификация ДЗ за 3 месяца
    const TASK_REPORT_CALCULATE_COMPANY_STANDARDS = 'report_calculate_company_standards';
    const TASK_REPORT_SAVE_DELIVERY_DEBTS_DAILY = 'report_save_delivery_debts_daily';
    const TASK_MARKETPLACE_CREATE_ORDERS = 'marketplace_create_orders';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%crontab_task}}';
    }

    /**
     * @return CrontabTaskQuery
     */
    public static function find()
    {
        return new CrontabTaskQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'required'],
            ['name', 'unique'],
            ['active', 'default', 'value' => 1],
            ['active', 'number', 'min' => 0, 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'name' => Yii::t('common', 'Название'),
            'description' => Yii::t('common', 'Описание'),
            'active' => Yii::t('common', 'Вкл/Выкл'),
            'created_at' => Yii::t('common', 'Дата добавления'),
            'lastLog.status' => Yii::t('common', 'Последний статус'),
            'lastLog.created_at' => Yii::t('common', 'Последний вызов'),
            'lastLog.updated_at' => Yii::t('common', 'Последнее завершение'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastLog()
    {
        return $this->hasMany(CrontabTaskLog::className(), ['task_id' => 'id'])
            ->orderBy(['id' => SORT_DESC])
            ->limit(1);
    }
}
