<?php
namespace app\modules\administration\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\modules\administration\models\query\CrontabTaskLogQuery;
use app\widgets\Label;
use Yii;

/**
 * Class CrontabTaskLog
 * @package app\modules\administration\models
 * @property integer $id
 * @property integer $task_id
 * @property string $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property CrontabTask $task
 * @property CrontabTaskLogAnswer[] $answers
 */
class CrontabTaskLog extends ActiveRecordLogUpdateTime
{
    const STATUS_IN_PROGRESS = 'in_progress';
    const STATUS_DONE = 'done';
    const STATUS_ERROR = 'error';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%crontab_task_log}}';
    }

    /**
     * @return CrontabTaskLogQuery
     */
    public static function find()
    {
        return new CrontabTaskLogQuery(get_called_class());
    }

    /**
     * @return array
     */
    public static function getStatusesCollection()
    {
        return [
            self::STATUS_IN_PROGRESS => Yii::t('common', 'Выполняется'),
            self::STATUS_DONE => Yii::t('common', 'Завершена'),
            self::STATUS_ERROR => Yii::t('common', 'Ошибка'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['task_id', 'required'],
            ['status', 'default', 'value' => self::STATUS_IN_PROGRESS],
            ['status', 'in', 'range' => array_keys(self::getStatusesCollection())],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'task_id' => Yii::t('common', 'Задача'),
            'status' => Yii::t('common', 'Статус'),
            'created_at' => Yii::t('common', 'Дата начала'),
            'updated_at' => Yii::t('common', 'Дата завершения'),
            'task' => Yii::t('common', 'Задача'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(CrontabTask::className(), ['id' => 'task_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnswers()
    {
        return $this->hasMany(CrontabTaskLogAnswer::className(), ['log_id' => 'id']);
    }

    /**
     * @return string
     */
    public function getStyle()
    {
        switch ($this->status) {
            case CrontabTaskLog::STATUS_DONE:
                return Label::STYLE_SUCCESS;
            case CrontabTaskLog::STATUS_IN_PROGRESS:
                return Label::STYLE_WARNING;
            case CrontabTaskLog::STATUS_ERROR:
                return Label::STYLE_DANGER;
        }
        return null;
    }
}
