<?php
namespace app\modules\administration\models\query;

use app\components\db\ActiveQuery;

/**
 * Class CrontabTaskLogSubanswerQuery
 * @package app\modules\administration\models\query
 */
class CrontabTaskLogSubanswerQuery extends ActiveQuery
{
    /**
     * @param integer $answerId
     * @return $this
     */
    public function byAnswerId($answerId)
    {
        return $this->andWhere(['answer_id' => $answerId]);
    }
}
