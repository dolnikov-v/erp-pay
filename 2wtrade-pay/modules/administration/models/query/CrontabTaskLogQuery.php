<?php
namespace app\modules\administration\models\query;

use app\components\db\ActiveQuery;
use app\modules\administration\models\CrontabTaskLog;

/**
 * Class CrontabTaskLogQuery
 * @package app\modules\administration\models\query
 * @method CrontabTaskLog one($db = null)
 * @method CrontabTaskLog[] all($db = null)
 */
class CrontabTaskLogQuery extends ActiveQuery
{
    /**
     * @param integer $taskId
     * @return $this
     */
    public function byTaskId($taskId)
    {
        return $this->andWhere(['task_id' => $taskId]);
    }
}
