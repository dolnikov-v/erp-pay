<?php
namespace app\modules\administration\models\query;

use app\components\db\ActiveQuery;

/**
 * Class CrontabTaskLogAnswerQuery
 * @package app\modules\administration\models\query
 */
class CrontabTaskLogAnswerQuery extends ActiveQuery
{
    /**
     * @param integer $logId
     * @return $this
     */
    public function byLogId($logId)
    {
        return $this->andWhere(['log_id' => $logId]);
    }
}
