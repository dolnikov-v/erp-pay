<?php
namespace app\modules\administration\models\query;

use app\components\db\ActiveQuery;

/**
 * Class CrontabTaskQuery
 * @package app\modules\administration\models\query
 */
class CrontabTaskQuery extends ActiveQuery
{
    /**
     * @var string
     */
    protected $collectionValue = 'description';

    /**
     * @param string $name
     * @return $this
     */
    public function byName($name)
    {
        return $this->andWhere(['name' => $name]);
    }

    /**
     * @return $this
     */
    public function active()
    {
        return $this->andWhere(['active' => 1]);
    }

    /**
     * @return $this
     */
    public function notActive()
    {
        return $this->andWhere(['active' => 0]);
    }
}
