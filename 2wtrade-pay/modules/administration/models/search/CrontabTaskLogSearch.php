<?php
namespace app\modules\administration\models\search;

use app\modules\administration\models\CrontabTaskLog;
use app\modules\administration\models\CrontabTaskLogAnswer;
use app\modules\administration\components\crontab\Logger;
use phpDocumentor\Reflection\Types\Array_;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * Class CrontabTaskLogSearch
 * @package app\modules\administration\models\search
 */
class CrontabTaskLogSearch extends CrontabTaskLog
{
    public $orderId;
    public $dateFrom;
    public $dateTo;
    public $searchText;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['task_id', 'integer'],
            ['status', 'in', 'range' => array_keys(self::getStatusesCollection())],
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];
        $answer_query = null;

        if (empty($params['sort'])) {
            $sort['id'] = SORT_DESC;
        }

        if (!empty($params['CrontabTaskLogSearch']['orderId'])) {
            $this->orderId = $params['CrontabTaskLogSearch']['orderId'];
        }
        if (!empty($params['CrontabTaskLogSearch']['dateFrom'])) {
            $this->dateFrom = $params['CrontabTaskLogSearch']['dateFrom'];
        }
        if (!empty($params['CrontabTaskLogSearch']['dateTo'])) {
            $this->dateTo = $params['CrontabTaskLogSearch']['dateTo'];
        }
        if (!empty($params['CrontabTaskLogSearch']['searchText'])) {
            $this->searchText = $params['CrontabTaskLogSearch']['searchText'];
        }

        if ($this->orderId || $this->dateFrom || $this->dateTo || $this->searchText) {  // раз есть хоть один запрос к ответам - строим очередь
            $answer_query = CrontabTaskLogAnswer::find()->orderBy($sort);
        }

        $query = CrontabTaskLog::find()
            ->with('answers')
            ->joinWith('task')
            ->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['task_id' => $this->task_id]);      // фильтр по типу задачи
        $query->andFilterWhere(['status' => $this->status]);        // фильтр по статусу задачи

        // Фильтр по номеру заказа
        if (isset($this->orderId) && trim($this->orderId) != '') {
            $inputArray = $this->getIdsArray($this->orderId);
            $resArray = Array();
            foreach ($inputArray as $item) {                                // Перебираем все введенные индексы
                $tempQuery = $answer_query;
                $temp = $tempQuery->where(['like', CrontabTaskLogAnswer::tableName() . '.data', $item])->all();
                $temp = $this->getFilterArray($temp);
                for ($i = 0; $i < count($temp); $i++) {
                    array_push($resArray, $temp[$i]);
                }
            }

            $query->andFilterWhere(["IN", CrontabTaskLog::tableName() . ".id", $resArray]);
        }

        // Фильтр по дате (ответов)
        if (!is_null($this->dateFrom) && !is_null($this->dateTo))
        {
            $answer_query->andFilterWhere(['>=', CrontabTaskLogAnswer::tableName() . '.created_at', Yii::$app->formatter->asTimestamp($this->dateFrom)]);
            $answer_query->andFilterWhere(['<=', CrontabTaskLogAnswer::tableName() . '.created_at', Yii::$app->formatter->asTimestamp($this->dateTo) + 86399]);
            $temp = $answer_query->all();
            $resArray = $this->getFilterArray($temp);
            $query->andFilterWhere(["IN", CrontabTaskLog::tableName() . ".id", $resArray]);
        }

        // Фильтрация по тексту
        if (isset($this->searchText) && trim($this->searchText !='')) {
        /*===========убрано до решения о целесообразности============*/
        }

        return $dataProvider;
    }

    /**
     * @param array $inArray
     * @return array $resArray
     */
    public function getFilterArray($inArray = null) {
        if (!empty($inArray)) {
            $resArray = Array();
            for ($i = 0; $i < count($inArray); $i++) {
                $resArray[] = intval($inArray[$i]['log_id']);
            }
            return $resArray;
        }

        return null;
    }

    /**
     * @param array $inArray
     * @return array $resArray
     */
    public function getIdsArray($inArray = null) {
        if (!empty($inArray)) {
            $tmp = preg_replace('/[^0-9,]/', '', $inArray);
            if (!empty($tmp)) {
                return $resArray = explode(',', $tmp);
            }
        }

        return null;
    }
}
