<?php
namespace app\modules\administration\models\search;

use app\modules\administration\models\CrontabTask;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * Class CrontabTaskSearch
 * @package app\modules\administration\models\search
 */
class CrontabTaskSearch extends CrontabTask
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $query = CrontabTask::find()
            ->orderBy([
                'active' => SORT_DESC,
                'description' => SORT_ASC,
            ]);

        $config = [
            'query' => $query,
            'pagination' => [
                'pageSize' => 0,
            ],
        ];

        $dataProvider = new ActiveDataProvider($config);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        return $dataProvider;
    }
}
