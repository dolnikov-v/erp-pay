<?php
namespace app\modules\administration\models\search;

use app\components\Notifier;
use app\models\Country;
use app\modules\administration\models\CrontabTask;
use app\modules\administration\models\CrontabTaskLogAnswer;
use app\modules\callcenter\models\CallCenter;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Query;

/**
 * Class CrontabTaskLogAnswerSearch
 * @package app\modules\administration\models\search
 */
class CrontabTaskLogAnswerSearch extends CrontabTaskLogAnswer
{
    public $dateFrom;
    public $dateTo;
    public $orderId;
    public $countryCode;
    public $searchEveryWhere;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['log_id'], 'integer'],
            ['status', 'in', 'range' => array_keys(self::getStatusesCollection())],
            [['dateFrom', 'dateTo', 'orderId', 'countryCode'], 'safe'],
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];
        if (empty($params['sort'])) {
            $sort['id'] = SORT_DESC;
        }

        /** @var Query $query */
        $query = CrontabTaskLogAnswer::find()
            ->joinWith('log')
            ->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if (!(isset($params['CrontabTaskLogAnswerSearch']['searchEveryWhere']) && $params['CrontabTaskLogAnswerSearch']['searchEveryWhere'])) {
            $query->andFilterWhere(['log_id' => $this->log_id]);
        }

        // Проверяем включать ли автофильтр ответов по номеру заказа
        if (isset($params['orderId']) && trim($params['orderId']) != '') {
            $this->orderId = $params['orderId'];
        }

        $query->andFilterWhere([CrontabTaskLogAnswer::tableName() . '.status' => $this->status]);

        if ($this->dateFrom && $dateFrom = Yii::$app->formatter->asTimestamp($this->dateFrom)) {
            $query->andFilterWhere(['>=', CrontabTaskLogAnswer::tableName() . '.created_at', $dateFrom]);
        }

        if ($this->dateTo && $dateTo = Yii::$app->formatter->asTimestamp($this->dateTo)) {
            $query->andFilterWhere(['<=', CrontabTaskLogAnswer::tableName() . '.created_at', $dateTo + 86399]);
        }

        if (isset($this->orderId) && trim($this->orderId) != '') {
            $inputArray = $this->getIdsArray($this->orderId);
            $resArray = Array();
            foreach ($inputArray as $item) {                                // Перебираем все переданные индексы ордеров
                $tempQuery = $query;
                $temp = $tempQuery->where(['like', CrontabTaskLogAnswer::tableName() . '.data', $item])->all();
                $temp = $this->getFilterArray($temp);
                for ($i = 0; $i < count($temp); $i++) {
                    array_push($resArray, $temp[$i]);
                }
            }
            $query->andFilterWhere(["IN", CrontabTaskLogAnswer::tableName() . '.log_id', $resArray]);
        }

        if ($this->countryCode) {
            switch ($this->log->task->name) {
                case CrontabTask::TASK_CALL_CENTER_SEND_REQUESTS:
                case CrontabTask::TASK_CALL_CENTER_GET_NEW_ORDERS:
                    $conditionCallCenters = $this->getSearchCallCentersCountry($this->countryCode);
                    $query->andFilterWhere($conditionCallCenters);

                    break;
                default:
                    Yii::$app->notifier->addNotification(Yii::t('common', 'Невозможно выполнить поиск по стране в этом.'), Notifier::TYPE_DANGER);
            }
        }

        return $dataProvider;
    }

    /**
     * @param $charCode
     * @return \app\modules\callcenter\models\CallCenter[]
     */
    protected function getSearchCallCentersCountry($charCode)
    {
        $callCenters = CallCenter::find()
            ->joinWith(['country'])
            ->where([Country::tableName() . '.char_code' => $charCode])
            ->asArray()
            ->all();

        $response = ['or'];

        foreach ($callCenters as $callCenter) {
            $response[] = ['like', CrontabTaskLogAnswer::tableName() . '.url', $callCenter['url']];
        }

        return $response;
    }


    /**
     * @param array $inArray
     * @return array $resArray
     */
    public function getFilterArray($inArray = null) {
        if (!empty($inArray)) {
            $resArray = Array();
            for ($i = 0; $i < count($inArray); $i++) {
                $resArray[] = intval($inArray[$i]['log_id']);
            }
            return $resArray;
        }

        return null;
    }

    /**
     * @param array $inArray
     * @return array $resArray
     */
    public function getIdsArray($inArray = null) {
        if (!empty($inArray)) {
            $tmp = preg_replace('/[^0-9,]/', '', $inArray);
            if (!empty($tmp)) {
                return $resArray = explode(',', $tmp);
            }
        }

        return null;
    }

}
