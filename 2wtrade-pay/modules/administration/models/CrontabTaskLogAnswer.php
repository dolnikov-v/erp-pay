<?php
namespace app\modules\administration\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\modules\administration\components\crontab\answer\Subanswer;
use app\modules\administration\components\crontab\answer\SubanswerReader;
use app\modules\administration\components\crontab\Logger;
use app\modules\administration\models\query\CrontabTaskLogAnswerQuery;
use Yii;
use yii\base\InvalidCallException;

/**
 * Class CrontabTaskLogAnswer
 * @package app\modules\administration\models
 * @property integer $id
 * @property integer $log_id
 * @property string $url
 * @property string $data
 * @property string $file_answer
 * @property string $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property CrontabTaskLog $log
 * @property Subanswer[] $subanswers
 */
class CrontabTaskLogAnswer extends ActiveRecordLogUpdateTime
{
    const STATUS_FAIL = 'fail';
    const STATUS_SUCCESS = 'success';

    /**
     * @var string
     */
    public $answer;

    /**
     * @var Subanswer[]
     */
    private $subanswerValues;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%crontab_task_log_answer}}';
    }

    /**
     * @return CrontabTaskLogAnswerQuery
     */
    public static function find()
    {
        return new CrontabTaskLogAnswerQuery(get_called_class());
    }

    /**
     * @return array
     */
    public static function getStatusesCollection()
    {
        return [
            self::STATUS_FAIL => Yii::t('common', 'С ошибкой'),
            self::STATUS_SUCCESS => Yii::t('common', 'Успешно'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['log_id', 'required'],
            [['url', 'data', 'answer', 'file_answer'], 'string'],
            ['status', 'default', 'value' => self::STATUS_FAIL],
            ['status', 'in', 'range' => array_keys(self::getStatusesCollection())],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'log_id' => Yii::t('common', 'Лог'),
            'url' => Yii::t('common', 'Адрес'),
            'data' => Yii::t('common', 'Данные'),
            'answer' => Yii::t('common', 'Ответ'),
            'file_answer' => Yii::t('common', 'Файл с ответом'),
            'status' => Yii::t('common', 'Статус'),
            'created_at' => Yii::t('common', 'Дата добавления'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
            'log' => Yii::t('common', 'Лог'),
            'orderId' => Yii::t('common', 'Номер заказа'),
            'countryCode' => Yii::t('common', 'Страна'),
            'searchEveryWhere' => Yii::t('common', 'Искать во всех логах'),
        ];
    }

    /**
     * @param boolean $insert
     * @return boolean
     * @throws InvalidCallException
     */
    public function beforeSave($insert)
    {
        if ($this->file_answer) {
            Logger::delete($this->file_answer);
            $this->file_answer = null;
        }

        if ($this->answer) {
            $this->file_answer = Logger::write($this->answer);
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLog()
    {
        return $this->hasOne(CrontabTaskLog::className(), ['id' => 'log_id']);
    }

    /**
     * @return Subanswer[]
     */
    public function getSubanswers()
    {
        if (is_null($this->subanswerValues)) {
            $reader = new SubanswerReader(['answer' => $this]);
            $this->subanswerValues = $reader->getSubanswers();
        }

        return $this->subanswerValues;
    }
}
