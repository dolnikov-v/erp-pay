<?php
namespace app\modules\administration\components\crontab;

use app\helpers\Utils;
use yii\base\Component;
use Yii;

/**
 * Class Logger
 * @package app\modules\administration\components\crontab
 */
class Logger extends Component
{
    /**
     * Папка для сохранения логов
     */
    const FOLDER = 'crontab';

    /**
     * Формат сохранения логов
     */
    const FORMAT = '.json';

    /**
     * @param string $data
     * @return string
     */
    public static function write($data)
    {
        $fileName = Utils::uid() . self::FORMAT;
        $filePath = self::getFilePath($fileName, true);

        file_put_contents($filePath, $data);

        return $fileName;
    }

    /**
     * @param string $fileName
     * @param null|integer $length
     * @return string
     */
    public static function read($fileName, $length = null)
    {
        $filePath = self::getFilePath($fileName, false);

        $content = "";

        if (file_exists($filePath) && !is_dir($filePath)) {
            if (is_null($length)) {
                $content = file_get_contents($filePath);
            } else {
                $content = file_get_contents($filePath, null, null, 0, $length);
            }
        }

        return $content;
    }

    /**
     * @param string $fileName
     * @param integer $length
     * @return string
     */
    public static function readPart($fileName, $length = 255)
    {
        return self::read($fileName, $length);
    }

    /**
     * @param string $fileName
     */
    public static function delete($fileName)
    {
        if ($fileName) {
            $filePath = self::getFilePath($fileName);

            if (file_exists($filePath)) {
                unlink($filePath);
            }
        }
    }

    /**
     * @param string $fileName
     * @param boolean $prepareDir
     * @return string
     */
    public static function getFilePath($fileName, $prepareDir = false)
    {
        $dir = self::getDir($fileName, $prepareDir);

        return $dir . DIRECTORY_SEPARATOR . $fileName;
    }

    /**
     * @param string $fileName
     * @param boolean $prepareDir
     * @return string
     */
    protected static function getDir($fileName, $prepareDir = false)
    {
        $subDir = substr($fileName, 0, 2);

        $dir = Yii::getAlias('@logs') . DIRECTORY_SEPARATOR . self::FOLDER . DIRECTORY_SEPARATOR . $subDir;

        if ($prepareDir) {
            Utils::prepareDir($dir);
        }

        return $dir;
    }
}
