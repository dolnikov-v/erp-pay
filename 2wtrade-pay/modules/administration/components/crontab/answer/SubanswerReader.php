<?php
namespace app\modules\administration\components\crontab\answer;

use app\modules\administration\components\crontab\Logger;
use app\modules\administration\models\CrontabTask;
use app\modules\administration\models\CrontabTaskLogAnswer;
use yii\base\Component;

/**
 * Class SubanswerReader
 * @package app\modules\administration\components\crontab\answer
 */
class SubanswerReader extends Component
{
    /**
     * @var CrontabTaskLogAnswer
     */
    public $answer;

    /**
     * @var array
     */
    protected $answerData;

    /**
     * @return Subanswer[]
     */
    public function getSubanswers()
    {
        $values = [];

        $this->answerData = json_decode(Logger::read($this->answer->file_answer), true);

        if ($this->answerData) {
            switch ($this->answer->log->task->name) {
                case CrontabTask::TASK_CALL_CENTER_GET_STATUSES:
                    $values = $this->getSubanswersCallCenterGetStatuses();
                    break;
            }
        }

        return $values;
    }

    /**
     * @return Subanswer[]
     */
    protected function getSubanswersCallCenterGetStatuses()
    {
        $values = [];

        $this->answerData = empty($this->answerData['answer']) ? [] : json_decode($this->answerData['answer'], true);

        if (!empty($this->answerData['response'])) {
            foreach ($this->answerData['response'] as $orderId => $orderData) {
                $subanswer = new Subanswer();
                $subanswer->data = json_encode($orderData);
                $subanswer->status = Subanswer::STATUS_SUCCESS;

                $values[] = $subanswer;
            }
        }

        return $values;
    }
}
