<?php
namespace app\modules\administration\components\crontab\answer;

use yii\base\Component;
use Yii;

/**
 * Class Subanswer
 * @package app\modules\administration\components\crontab\answer
 */
class Subanswer extends Component
{
    const STATUS_FAIL = 'fail';
    const STATUS_SUCCESS = 'success';

    public $data;
    public $status;

    /**
     * @return array
     */
    public static function getStatusesCollection()
    {
        return [
            self::STATUS_FAIL => Yii::t('common', 'С ошибкой'),
            self::STATUS_SUCCESS => Yii::t('common', 'Успешно'),
        ];
    }

}
