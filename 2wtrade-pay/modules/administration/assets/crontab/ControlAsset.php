<?php
namespace app\modules\administration\assets\crontab;

use yii\web\AssetBundle;

/**
 * Class ControlAsset
 * @package app\modules\administration\assets\crontab
 */
class ControlAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/administration/crontab';

    public $js = [
        'control.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
