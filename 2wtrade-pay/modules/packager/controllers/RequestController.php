<?php

namespace app\modules\packager\controllers;

use app\components\filters\AjaxFilter;
use app\components\Notifier;
use app\components\web\Controller;
use app\models\UserDelivery;
use app\modules\delivery\components\api\TransmitterAdaptee;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\models\Order;
use app\modules\order\models\OrderLogisticList;
use app\modules\order\models\OrderStatus;
use app\modules\packager\components\exporter\ExporterRecordCsv;
use app\modules\packager\components\exporter\ExporterRecordExcel;
use app\modules\packager\models\OrderPackaging;
use app\modules\packager\models\search\OrderApproveSearch;
use app\modules\packager\models\search\OrderPackagingSearch;
use app\modules\storage\models\StorageProduct;
use app\widgets\LinkPager;
use iio\libmergepdf\Merger;
use kartik\mpdf\Pdf;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class RequestController extends Controller
{

    private $deliveries;

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => [
                    'refusal-completing',
                    'confirm-completing',
                    'cancel-completing',
                    'cancel-shipment',
                    'set-shipped',
                    'set-return',
                    'set-approve',
                    'change-delivery',
                    'send-in-delivery',
                ],
            ]
        ];
    }

    private function getDeliveries()
    {
        if (!empty($this->deliveries)) {
            return $this->deliveries;
        }

        $query = Delivery::find()
            ->active()
            ->byCountryId(Yii::$app->user->country->id)
            ->orderBy(['sort_order' => SORT_ASC]);

        if (Yii::$app->user->isImplant) {
            $query->leftJoin(UserDelivery::tableName(), Delivery::tableName() . '.id = ' . UserDelivery::tableName() . '.delivery_id');
            $query->andWhere([UserDelivery::tableName() . '.user_id' => Yii::$app->user->id]);
            $query->groupBy([Delivery::tableName() . '.id']);
        }

        $this->deliveries = $query->asArray()->all();

        return $this->deliveries;
    }

    /**
     * @return string|Response
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Writer_Exception
     * @throws \yii\base\ExitException
     */
    public function actionIndex()
    {
        $deliveriesCollection = [];
        if ($this->getDeliveries()) {
            $deliveriesCollection = ArrayHelper::map($this->getDeliveries(), 'id', 'name');
        }

        $modelSearch = new OrderPackagingSearch();
        if ($this->getDeliveries()) {
            $modelSearch->delivery_id = key($deliveriesCollection);
        }

        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);
        LinkPager::setPageSize($dataProvider->pagination);

        if (!$modelSearch->delivery_id) {
            $modelSearch->delivery_id = key($deliveriesCollection);
        }

        if ($export = Yii::$app->request->get('export')) {
            if ($dataProvider->getCount()) {
                if ($export == 'excel') {
                    (new ExporterRecordExcel(['dataProvider' => $dataProvider]))->sendFile();
                } elseif ($export == 'csv') {
                    (new ExporterRecordCsv(['dataProvider' => $dataProvider]))->sendFile();
                } else {
                    Yii::$app->notifier->addNotification(Yii::t('common', 'Неизвестный формат'), 'danger');
                }
            } else {
                Yii::$app->notifier->addNotification(Yii::t('common', 'Нет данных для экспорта'), 'danger');
            }
            $params = Yii::$app->request->get();
            unset($params['export']);
            return $this->redirect(['index'] + $params);
        }

        return $this->render('index', [
            'modelSearch' => $modelSearch,
            'dataProvider' => $dataProvider,
            'deliveriesCollection' => $deliveriesCollection,
        ]);
    }


    /**
     * @return string|Response
     */
    public function actionApprove()
    {
        $deliveriesCollection = [];
        if ($this->getDeliveries()) {
            $deliveriesCollection = ArrayHelper::map($this->getDeliveries(), 'id', 'name');
        }

        $orderSearch = new OrderApproveSearch();

        $dataProvider = $orderSearch->search(Yii::$app->request->queryParams);

        LinkPager::setPageSize($dataProvider->pagination);

        return $this->render('approve', [
            'modelSearch' => $orderSearch,
            'dataProvider' => $dataProvider,
            'deliveriesCollection' => $deliveriesCollection,
        ]);
    }

    /**
     * @return string|Response
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     */
    public function actionLabelPrinting()
    {
        $id = Yii::$app->request->post('id');
        $delivery_id = Yii::$app->request->post('delivery_id');
        $date_from = Yii::$app->request->post('date_from');
        $date_to = Yii::$app->request->post('date_to');
        $limit = Yii::$app->request->post('limit');
        $print_invoice = Yii::$app->request->post('print_invoice');

        $query = OrderPackaging::find();

        if ($this->getDeliveries()) {
            $query->andWhere([OrderPackaging::tableName() . '.delivery_id' => ArrayHelper::getColumn($this->getDeliveries(), 'id')]);
        }

        if ($delivery_id) {
            $query->andWhere([OrderPackaging::tableName() . '.delivery_id' => $delivery_id]);
        }

        if ($date_from && $dateCreateFrom = Yii::$app->formatter->asTimestamp($date_from)) {
            $query->andWhere(['>=', OrderPackaging::tableName() . '.created_at', $dateCreateFrom]);
        }

        if ($date_to && $dateCreateTo = Yii::$app->formatter->asTimestamp($date_to)) {
            $query->andWhere(['<=', OrderPackaging::tableName() . '.created_at', $dateCreateTo]);
        }

        if ($id) {
            $query->andWhere([OrderPackaging::tableName() . '.id' => $id]);
        } else {
            $query->andWhere([OrderPackaging::tableName() . '.status' => OrderPackaging::STATUS_PENDING]);
        }

        if ($limit) {
            $query->limit($limit);
        }

        $models = $query->all();

        $transaction = OrderPackaging::getDb()->beginTransaction();
        try {
            $merger = new Merger();
            $iterator = [];

            if (!$models) {
                throw new \Exception(Yii::t('common', 'Нет заявок для печати'));
            }

            foreach ($models as $packaging) {

                $labelFilePath = (new TransmitterAdaptee($packaging->delivery))->generateLabelByOrder($packaging->order);

                if ($labelFilePath === false) {
                    throw new \Exception(Yii::t('common', 'Служба доставки {delivery} не поддерживает создание этикеток', ['delivery' => $packaging->delivery->name]));
                } else {
                    $labelConvertedFilePath = self::convertToPdf($labelFilePath);
                    if ($labelConvertedFilePath) {
                        $iterator[] = $labelConvertedFilePath;
                    } else {
                        throw new \Exception(Yii::t('common', 'Не сохранен для заказа {order}', ['order' => $packaging->order_id]));
                    }
                }

                if ($print_invoice) {
                    $invoiceFilePath = $packaging->generateInvoices();
                    if ($invoiceFilePath) {
                        $iterator[] = $invoiceFilePath;
                    }
                }
            }
            $merger->addIterator($iterator);

            $mergerContent = $merger->merge();

            if ($mergerContent) {

                foreach ($models as $packaging) {
                    if ($packaging->status == OrderPackaging::STATUS_PENDING) {
                        if ($packaging->order->canChangeStatusTo(OrderStatus::STATUS_LOG_SET)) {
                            $packaging->order->status_id = OrderStatus::STATUS_LOG_SET;
                            if (!$packaging->order->save(true, ['status_id'])) {
                                throw new \Exception($packaging->order->getFirstErrorAsString());
                            }
                        }
                        $packaging->status = OrderPackaging::STATUS_COMPLETING;
                        if (!$packaging->save(true, ['status'])) {
                            throw new \Exception($packaging->getFirstErrorAsString());
                        }
                    }
                }

                $transaction->commit();

                $mergerFile = 'labels_(' . sizeof($models) . ').pdf';
                return Yii::$app->response->sendContentAsFile($mergerContent, $mergerFile, [
                    'mimeType' => 'application/pdf',
                    'inline' => true
                ]);
            } else {
                $transaction->rollBack();
                throw new \Exception(Yii::t('common', 'Не удалось объединить файлы'));
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw new NotFoundHttpException(Yii::t('common', 'Ошибка создания файла.') . ' ' . $e->getMessage());
        }
    }

    /**
     * @throws NotFoundHttpException
     */
    public function actionGetManifest()
    {
        $id = Yii::$app->request->post('id');
        $delivery_id = Yii::$app->request->post('delivery_id');
        $date_from = Yii::$app->request->post('date_from');
        $date_to = Yii::$app->request->post('date_to');

        $query = OrderPackaging::find();

        if ($this->getDeliveries()) {
            $query->andWhere([OrderPackaging::tableName() . '.delivery_id' => ArrayHelper::getColumn($this->getDeliveries(), 'id')]);
        }

        $delivery = null;
        if ($delivery_id) {
            $delivery = Delivery::findOne(['id' => $delivery_id]);
            $query->andWhere([OrderPackaging::tableName() . '.delivery_id' => $delivery_id]);
        }

        if ($date_from && $dateCreateFrom = Yii::$app->formatter->asTimestamp($date_from)) {
            $query->andWhere(['>=', OrderPackaging::tableName() . '.created_at', $dateCreateFrom]);
        }

        if ($date_to && $dateCreateTo = Yii::$app->formatter->asTimestamp($date_to)) {
            $query->andWhere(['<=', OrderPackaging::tableName() . '.created_at', $dateCreateTo]);
        }

        if ($id) {
            $query->andWhere([OrderPackaging::tableName() . '.id' => $id]);
        }
        $query->andWhere([OrderPackaging::tableName() . '.status' => OrderPackaging::STATUS_AWAITING_SHIPMENT]);

        $models = $query->all();

        try {

            if (!$models) {
                throw new \Exception(Yii::t('common', 'Нет подходящих заявок'));
            }

            if (!($delivery instanceof Delivery)) {
                throw new \Exception(Yii::t('common', 'Служба доставки не выбрана'));
            }

            $ordersId = [];
            $dates = [];
            foreach ($models as $packaging) {
                $dates[] = max($packaging->order->deliveryRequest->second_sent_at ?? 0, $packaging->order->deliveryRequest->sent_at ?? 0, $packaging->order->deliveryRequest->created_at ?? 0, $packaging->order->created_at);
                $ordersId[] = $packaging->order_id;
            }

            if (!$dates) {
                $dates[] = time();
            }

            // временно ограничил по 10, все равно там игнорируется
            $manifestFilePath = (new TransmitterAdaptee($delivery))->generateManifestByOrders(array_slice($ordersId, 0, 10), [
                'date_from' => min($dates),
                'date_to' => max($dates)
            ]);


            if ($manifestFilePath === false) {
                throw new \Exception(Yii::t('common', 'Служба доставки {delivery} не поддерживает получение манифеста', ['delivery' => $delivery->name]));
            }

            return Yii::$app->response->sendFile($manifestFilePath, 'Manifest.pdf', [
                'mimeType' => 'application/pdf',
                'inline' => true
            ]);


        } catch (\Exception $e) {
            throw new NotFoundHttpException(Yii::t('common', 'Ошибка создания файла.') . ' ' . $e->getMessage());
        }
    }


    /**
     * @return Response
     * @throws \yii\db\Exception
     */
    public function actionListGeneration()
    {
        $id = Yii::$app->request->post('id');
        $delivery_id = Yii::$app->request->post('delivery_id');
        $date_from = Yii::$app->request->post('date_from');
        $date_to = Yii::$app->request->post('date_to');

        $query = OrderPackaging::find();

        if ($this->getDeliveries()) {
            $query->andWhere([OrderPackaging::tableName() . '.delivery_id' => ArrayHelper::getColumn($this->getDeliveries(), 'id')]);
        }

        if ($delivery_id) {
            $query->andWhere([OrderPackaging::tableName() . '.delivery_id' => $delivery_id]);
        }

        if ($date_from && $dateCreateFrom = Yii::$app->formatter->asTimestamp($date_from)) {
            $query->andWhere(['>=', OrderPackaging::tableName() . '.created_at', $dateCreateFrom]);
        }

        if ($date_to && $dateCreateTo = Yii::$app->formatter->asTimestamp($date_to)) {
            $query->andWhere(['<=', OrderPackaging::tableName() . '.created_at', $dateCreateTo]);
        }

        $query->andWhere([OrderPackaging::tableName() . '.id' => $id]);
        $query->andWhere([OrderPackaging::tableName() . '.status' => OrderPackaging::STATUS_COMPLETED]);

        $modelSearch = new OrderPackagingSearch();

        $params = [
            'status' => OrderPackaging::STATUS_COMPLETED,
            'delivery_id' => $delivery_id,
        ];
        if ($date_from) {
            $params['date_from'] = $date_from;
        }
        if ($date_to) {
            $params['date_to'] = $date_to;
        }
        $backUrl = Url::toRoute([
            'index',
            $modelSearch->formName() => $params
        ]);

        $models = $query->all();

        if ($models) {
            $delivery = Delivery::find()
                ->where(['id' => $delivery_id])
                ->byCountryId(Yii::$app->user->country->id)
                ->active()
                ->one();

            if ($delivery) {
                $commit = false;
                $successIds = [];
                $successModels = [];

                $transaction = OrderLogisticList::getDb()->beginTransaction();

                $list = new OrderLogisticList();
                $list->country_id = Yii::$app->user->country->id;
                $list->delivery_id = $delivery->id;
                $list->user_id = Yii::$app->user->id;

                foreach ($models as $model) {
                    if ($model->order->status_id != OrderStatus::STATUS_LOG_DEFERRED &&
                        $model->order->canChangeStatusTo(OrderStatus::STATUS_LOG_GENERATED)) {
                        $model->order->status_id = OrderStatus::STATUS_LOG_GENERATED;

                        $model->order->route = Yii::$app->controller->route;

                        if (!Delivery::createRequestInList($delivery_id, $model->order)) {
                            $transaction->rollBack();
                            Yii::$app->notifier->addNotification(Yii::t('common', 'Ошибка при создании заявок для листа.'));
                            return $this->redirect($backUrl);
                        }

                        if ($model->order->save(true, ['status_id'])) {
                            $successIds[] = $model->order->id;
                            $successModels[] = $model->id;

                            $model->status = OrderPackaging::STATUS_AWAITING_SHIPMENT;
                            if (!$model->save()) {
                                $transaction->rollBack();
                                Yii::$app->notifier->addNotification($model->getFirstErrorAsString());
                                return $this->redirect($backUrl);
                            }
                        }
                    }
                }

                if ($successIds) {
                    $todayStart = Yii::$app->formatter->asTimestamp(date('Y-m-d 00:00:00'));
                    $todayEnd = $todayStart + 86399;

                    $todayCount = OrderLogisticList::find()
                        ->where(['country_id' => Yii::$app->user->country->id])
                        ->andWhere(['delivery_id' => $delivery->id])
                        ->andWhere(['>=', 'created_at', $todayStart])
                        ->andWhere(['<=', 'created_at', $todayEnd])
                        ->count();

                    $list->number = $todayCount + 1;
                    $list->ids = implode(',', $successIds);

                    if ($list->save()) {

                        OrderPackaging::updateAll([
                            'list_id' => $list->id
                        ], [
                            'id' => $successModels
                        ]);

                        $commit = true;
                        Yii::$app->notifier->addNotification(
                            Yii::t('common', 'Лист успешно сгенерирован. Заявки перешли в статус Ожидает отгрузки'),
                            'success'
                        );
                    } else {
                        Yii::$app->notifier->addNotification(Yii::t('common', 'Не удалось сгенерировать лист.') . ' ' . $list->getFirstErrorAsString());
                    }
                } else {
                    Yii::$app->notifier->addNotification(Yii::t('common', 'Необходимо указать корректные заказы.'));
                }

                if ($commit) {
                    $transaction->commit();

                    $errors = count($id) - count($successIds);

                    if ($errors) {
                        Yii::$app->notifier->addNotification(
                            Yii::t(
                                'common',
                                '{n, plural, one{# заказ} few{# заказа} many{# заказов} other{# заказов}} не удалось добавить в сгенерированный лист.',
                                [
                                    'n' => $errors,
                                ]
                            )
                        );
                    }
                } else {
                    $transaction->rollBack();
                }
            } else {
                Yii::$app->notifier->addNotification(
                    Yii::t('common', 'Необходимо указать корректную службу доставки.'),
                    Notifier::TYPE_DANGER
                );
            }
        }
        return $this->redirect($backUrl);
    }

    /**
     * @return array
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     */
    public function actionRefusalCompleting()
    {
        $id = Yii::$app->request->post('id');
        $reason = Yii::$app->request->post('reason');
        $model = $this->findModel($id);

        if (trim($reason) == '') {
            return [
                'status' => 'fail',
                'message' => Yii::t('common', 'Введите причину отказа')
            ];
        }

        if ($model->status == OrderPackaging::STATUS_PENDING ||
            $model->status == OrderPackaging::STATUS_COMPLETING
        ) {

            $transaction = OrderPackaging::getDb()->beginTransaction();
            try {
                if ($model->order->canChangeStatusTo(OrderStatus::STATUS_LOGISTICS_REJECTED)) {
                    $model->order->status_id = OrderStatus::STATUS_LOGISTICS_REJECTED;
                    if (!$model->order->save(true, ['status_id'])) {
                        $transaction->rollBack();
                        return [
                            'status' => 'fail',
                            'message' => Yii::t('common', $model->order->getFirstErrorAsString())
                        ];
                    }
                } else {
                    $transaction->rollBack();
                    return [
                        'status' => 'fail',
                        'message' => Yii::t('common', 'Некорректный статус заказа для операции')
                    ];
                }

                $model->order->deliveryRequest->status = DeliveryRequest::STATUS_ERROR;
                $model->order->deliveryRequest->api_error = $reason;
                if (!$model->order->deliveryRequest->save(true, ['status', 'api_error'])) {
                    $transaction->rollBack();
                    return [
                        'status' => 'fail',
                        'message' => Yii::t('common', $model->order->deliveryRequest->getFirstErrorAsString())
                    ];
                }

                $model->status = OrderPackaging::STATUS_ERROR;
                $model->reason = $reason;
                if (!$model->save()) {
                    $transaction->rollBack();
                    return [
                        'status' => 'fail',
                        'message' => Yii::t('common', $model->getFirstErrorAsString())
                    ];
                }

                $transaction->commit();
                return [
                    'status' => 'success',
                    'message' => Yii::t('common', 'Заявка перешла в статус Ошибка')
                ];
            } catch (\Exception $e) {
                $transaction->rollBack();
                return [
                    'status' => 'success',
                    'message' => Yii::t('common', $e->getMessage())
                ];
            }
        } else {
            return [
                'status' => 'fail',
                'message' => Yii::t('common', 'Некорректный статус заявки для операции')
            ];
        }
    }

    /**
     * @return array
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     */
    public function actionConfirmCompleting()
    {
        $id = Yii::$app->request->post('id');
        $model = $this->findModel($id);

        if ($model->status == OrderPackaging::STATUS_COMPLETING) {

            $transaction = OrderPackaging::getDb()->beginTransaction();
            try {
                if ($model->order->canChangeStatusTo(OrderStatus::STATUS_LOG_PASTED)) {
                    $model->order->status_id = OrderStatus::STATUS_LOG_PASTED;
                    if (!$model->order->save(true, ['status_id'])) {
                        $transaction->rollBack();
                        return [
                            'status' => 'fail',
                            'message' => Yii::t('common', $model->order->getFirstErrorAsString())
                        ];
                    }
                } else {
                    $transaction->rollBack();
                    return [
                        'status' => 'fail',
                        'message' => Yii::t('common', 'Некорректный статус заказа для операции')
                    ];
                }

                $model->order->deliveryRequest->status = DeliveryRequest::STATUS_COMPLETED;
                if (!$model->order->deliveryRequest->save(true, ['status'])) {
                    $transaction->rollBack();
                    return [
                        'status' => 'fail',
                        'message' => Yii::t('common', $model->order->deliveryRequest->getFirstErrorAsString())
                    ];
                }

                foreach ($model->order->orderProducts as $orderProduct) {
                    if ($orderProduct->storage_id) {
                        $storageItem = StorageProduct::find()
                            ->where([
                                StorageProduct::tableName() . '.storage_id' => $orderProduct->storage_id,
                                StorageProduct::tableName() . '.product_id' => $orderProduct->product_id,
                            ])
                            ->limit(1)
                            ->one();

                        if (!$storageItem) {
                            $storageItem = new StorageProduct([
                                'storage_id' => $orderProduct->storage_id,
                                'product_id' => $orderProduct->product_id,
                                'reserve' => 0,
                            ]);
                        }

                        if (!$storageItem->reserve) {
                            $storageItem->reserve = 0;
                        }
                        $storageItem->reserve += $orderProduct->quantity;
                        if (!$storageItem->save()) {
                            $transaction->rollBack();
                            return [
                                'status' => 'fail',
                                'message' => Yii::t('common', $storageItem->getFirstErrorAsString())
                            ];
                        }
                    }
                }

                $model->status = OrderPackaging::STATUS_COMPLETED;
                if (!$model->save()) {
                    $transaction->rollBack();
                    return [
                        'status' => 'fail',
                        'message' => Yii::t('common', $model->getFirstErrorAsString())
                    ];
                }

                $transaction->commit();
                return [
                    'status' => 'success',
                    'message' => Yii::t('common', 'Заявка перешла в статус Укомплектовано')
                ];
            } catch (\Exception $e) {
                $transaction->rollBack();
                return [
                    'status' => 'success',
                    'message' => Yii::t('common', $e->getMessage())
                ];
            }
        } else {
            return [
                'status' => 'fail',
                'message' => Yii::t('common', 'Некорректный статус заявки для операции')
            ];
        }
    }

    /**
     * @return array
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     */
    public function actionCancelCompleting()
    {
        $id = Yii::$app->request->post('id');
        $model = $this->findModel($id);

        if ($model->status == OrderPackaging::STATUS_COMPLETING ||
            $model->status == OrderPackaging::STATUS_COMPLETED
        ) {

            $transaction = OrderPackaging::getDb()->beginTransaction();
            try {
                if ($model->order->canChangeStatusTo(OrderStatus::STATUS_LOG_ACCEPTED)) {
                    $model->order->status_id = OrderStatus::STATUS_LOG_ACCEPTED;
                    if (!$model->order->save(true, ['status_id'])) {
                        $transaction->rollBack();
                        return [
                            'status' => 'fail',
                            'message' => Yii::t('common', $model->order->getFirstErrorAsString())
                        ];
                    }
                } else {
                    $transaction->rollBack();
                    return [
                        'status' => 'fail',
                        'message' => Yii::t('common', 'Некорректный статус заказа для операции')
                    ];
                }

                if ($model->order->deliveryRequest->status == DeliveryRequest::STATUS_COMPLETED) {
                    $model->order->deliveryRequest->status = DeliveryRequest::STATUS_COMPLETION;
                    if (!$model->order->deliveryRequest->save(true, ['status'])) {
                        $transaction->rollBack();
                        return [
                            'status' => 'fail',
                            'message' => Yii::t('common', $model->order->deliveryRequest->getFirstErrorAsString())
                        ];
                    }
                }

                $model->status = OrderPackaging::STATUS_PENDING;
                if (!$model->save()) {
                    $transaction->rollBack();
                    return [
                        'status' => 'fail',
                        'message' => Yii::t('common', $model->getFirstErrorAsString())
                    ];
                }

                $transaction->commit();
                return [
                    'status' => 'success',
                    'message' => Yii::t('common', 'Заявка перешла в статус Ожидает')
                ];
            } catch (\Exception $e) {
                $transaction->rollBack();
                return [
                    'status' => 'success',
                    'message' => Yii::t('common', $e->getMessage())
                ];
            }
        } else {
            return [
                'status' => 'fail',
                'message' => Yii::t('common', 'Некорректный статус заявки для операции')
            ];
        }
    }

    /**
     * @return array
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     */
    public function actionCancelShipment()
    {
        $id = Yii::$app->request->post('id');
        $model = $this->findModel($id);

        if ($model->status == OrderPackaging::STATUS_AWAITING_SHIPMENT) {

            $transaction = OrderPackaging::getDb()->beginTransaction();
            try {
                if ($model->order->canChangeStatusTo(OrderStatus::STATUS_LOG_PASTED)) {
                    if ($model->list_id) {
                        $list = OrderLogisticList::findOne($model->list_id);
                        if ($list) {
                            $tmp = explode(',', $list->ids);
                            if ($tmp) {
                                $arr = array_combine($tmp, $tmp);
                                if (isset($arr[$model->order_id])) {
                                    unset($arr[$model->order_id]);
                                    $list->ids = implode(',', $arr);
                                    $list->orders_hash = md5($list->ids);
                                    $list->user_id = Yii::$app->user->id;
                                    if (!$list->save()) {
                                        $transaction->rollBack();
                                        return [
                                            'status' => 'fail',
                                            'message' => Yii::t('common', $list->getFirstErrorAsString())
                                        ];
                                    }
                                }
                            }
                        }
                    }

                    $model->order->status_id = OrderStatus::STATUS_LOG_PASTED;
                    if (!$model->order->save(true, ['status_id'])) {
                        $transaction->rollBack();
                        return [
                            'status' => 'fail',
                            'message' => Yii::t('common', $model->order->getFirstErrorAsString())
                        ];
                    }
                } else {
                    $transaction->rollBack();
                    return [
                        'status' => 'fail',
                        'message' => Yii::t('common', 'Некорректный статус заказа для операции')
                    ];
                }

                if ($model->order->deliveryRequest->status == DeliveryRequest::STATUS_COMPLETION) {
                    $model->order->deliveryRequest->status = DeliveryRequest::STATUS_COMPLETED;
                    if (!$model->order->deliveryRequest->save(true, ['status'])) {
                        $transaction->rollBack();
                        return [
                            'status' => 'fail',
                            'message' => Yii::t('common', $model->order->deliveryRequest->getFirstErrorAsString())
                        ];
                    }
                }

                $model->status = OrderPackaging::STATUS_COMPLETED;
                if (!$model->save()) {
                    $transaction->rollBack();
                    return [
                        'status' => 'fail',
                        'message' => Yii::t('common', $model->getFirstErrorAsString())
                    ];
                }

                $transaction->commit();
                return [
                    'status' => 'success',
                    'message' => Yii::t('common', 'Заявка перешла в статус Укомплектовано')
                ];
            } catch (\Exception $e) {
                $transaction->rollBack();
                return [
                    'status' => 'success',
                    'message' => Yii::t('common', $e->getMessage())
                ];
            }
        } else {
            return [
                'status' => 'fail',
                'message' => Yii::t('common', 'Некорректный статус заявки для операции')
            ];
        }
    }

    /**
     * @return array
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     */
    public function actionSetShipped()
    {
        $id = Yii::$app->request->post('id');
        $model = $this->findModel($id);

        if ($model->status == OrderPackaging::STATUS_AWAITING_SHIPMENT) {

            $transaction = OrderPackaging::getDb()->beginTransaction();
            try {
                if ($model->order->canChangeStatusTo(OrderStatus::STATUS_DELIVERY_ACCEPTED)) {
                    $model->order->status_id = OrderStatus::STATUS_DELIVERY_ACCEPTED;
                    if (!$model->order->save(true, ['status_id'])) {
                        $transaction->rollBack();
                        return [
                            'status' => 'fail',
                            'message' => Yii::t('common', $model->order->getFirstErrorAsString())
                        ];
                    }
                } else {
                    $transaction->rollBack();
                    return [
                        'status' => 'fail',
                        'message' => Yii::t('common', 'Некорректный статус заказа для операции')
                    ];
                }

                foreach ($model->order->orderProducts as $orderProduct) {
                    if ($orderProduct->storage_id) {
                        $storageItem = StorageProduct::find()
                            ->where([
                                StorageProduct::tableName() . '.storage_id' => $orderProduct->storage_id,
                                StorageProduct::tableName() . '.product_id' => $orderProduct->product_id,
                            ])
                            ->limit(1)
                            ->one();

                        if ($storageItem) {
                            if (!$storageItem->reserve) {
                                $storageItem->reserve = 0;
                            }
                            $storageItem->reserve -= $orderProduct->quantity;
                            if (!$storageItem->save()) {
                                $transaction->rollBack();
                                return [
                                    'status' => 'fail',
                                    'message' => Yii::t('common', $storageItem->getFirstErrorAsString())
                                ];
                            }
                        }
                    }
                }

                if ($model->order->deliveryRequest->status == DeliveryRequest::STATUS_COMPLETION) {
                    $model->order->deliveryRequest->status = DeliveryRequest::STATUS_IN_PROGRESS;
                    if (!$model->order->deliveryRequest->save(true, ['status'])) {
                        $transaction->rollBack();
                        return [
                            'status' => 'fail',
                            'message' => Yii::t('common', $model->order->deliveryRequest->getFirstErrorAsString())
                        ];
                    }
                }

                $model->status = OrderPackaging::STATUS_DONE;
                if (!$model->save()) {
                    $transaction->rollBack();
                    return [
                        'status' => 'fail',
                        'message' => Yii::t('common', $model->getFirstErrorAsString())
                    ];
                }

                $transaction->commit();
                return [
                    'status' => 'success',
                    'message' => Yii::t('common', 'Заявка перешла в статус Отгружено')
                ];
            } catch (\Exception $e) {
                $transaction->rollBack();
                return [
                    'status' => 'success',
                    'message' => Yii::t('common', $e->getMessage())
                ];
            }
        } else {
            return [
                'status' => 'fail',
                'message' => Yii::t('common', 'Некорректный статус заявки для операции')
            ];
        }
    }

    /**
     * @return array
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     */
    public function actionSetReturn()
    {
        $id = Yii::$app->request->post('id');
        $model = $this->findModel($id);

        if ($model->status == OrderPackaging::STATUS_DONE) {

            $transaction = OrderPackaging::getDb()->beginTransaction();
            try {
                if ($model->order->canChangeStatusTo(OrderStatus::STATUS_LOGISTICS_ACCEPTED)) {
                    $model->order->status_id = OrderStatus::STATUS_LOGISTICS_ACCEPTED;
                    if (!$model->order->save(true, ['status_id'])) {
                        $transaction->rollBack();
                        return [
                            'status' => 'fail',
                            'message' => Yii::t('common', $model->order->getFirstErrorAsString())
                        ];
                    }
                } else {
                    $transaction->rollBack();
                    return [
                        'status' => 'fail',
                        'message' => Yii::t('common', 'Некорректный статус заказа для операции')
                    ];
                }

                $model->order->deliveryRequest->status = DeliveryRequest::STATUS_RETURN;
                if (!$model->order->deliveryRequest->save(true, ['status'])) {
                    $transaction->rollBack();
                    return [
                        'status' => 'fail',
                        'message' => Yii::t('common', $model->order->deliveryRequest->getFirstErrorAsString())
                    ];
                }

                $model->status = OrderPackaging::STATUS_RETURN;
                if (!$model->save()) {
                    $transaction->rollBack();
                    return [
                        'status' => 'fail',
                        'message' => Yii::t('common', $model->getFirstErrorAsString())
                    ];
                }

                $transaction->commit();
                return [
                    'status' => 'success',
                    'message' => Yii::t('common', 'Заявка перешла в статус Возврат')
                ];
            } catch (\Exception $e) {
                $transaction->rollBack();
                return [
                    'status' => 'success',
                    'message' => Yii::t('common', $e->getMessage())
                ];
            }
        } else {
            return [
                'status' => 'fail',
                'message' => Yii::t('common', 'Некорректный статус заявки для операции')
            ];
        }
    }

    /**
     * @return array
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function actionSetApprove()
    {
        $id = Yii::$app->request->post('id');
        $model = $this->findModel($id);

        if ($model->status == OrderPackaging::STATUS_ERROR) {

            $transaction = OrderPackaging::getDb()->beginTransaction();
            try {

                if (in_array($model->order->status_id, [
                    OrderStatus::STATUS_LOGISTICS_REJECTED
                ])) {

                    $model->status = OrderPackaging::STATUS_DELETED;
                    if (!$model->save()) {
                        $transaction->rollBack();
                        return [
                            'status' => 'fail',
                            'message' => Yii::t('common', $model->getFirstErrorAsString())
                        ];
                    }

                    $model->order->status_id = OrderStatus::STATUS_CC_APPROVED;
                    $model->order->route = Yii::$app->controller->route;

                    if ($model->order->save(true, ['status_id', 'route'])) {
                        if ($model->deliveryRequest instanceof DeliveryRequest) {
                            $model->deliveryRequest->delete();
                        }
                    } else {
                        $transaction->rollBack();
                        return [
                            'status' => 'fail',
                            'message' => $model->order->getFirstErrorAsString()
                        ];
                    }

                } else {
                    $transaction->rollBack();
                    return [
                        'status' => 'fail',
                        'message' => Yii::t('common',
                            'Не удалось вернуть в Одобрено заказ {orderId} со статусом "{status}"', [
                                'orderId' => $model->order->id,
                                'status' => $model->order->status->name,
                            ]
                        )
                    ];
                }

                $transaction->commit();
                return [
                    'status' => 'success',
                    'message' => Yii::t('common', 'Заказ вернулся в Одобрено, заявка на упаковку удалена')
                ];
            } catch (\Exception $e) {
                $transaction->rollBack();
                return [
                    'status' => 'success',
                    'message' => Yii::t('common', $e->getMessage())
                ];
            }
        } else {
            return [
                'status' => 'fail',
                'message' => Yii::t('common', 'Некорректный статус заявки для операции')
            ];
        }
    }


    /**
     * @return array
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function actionChangeDelivery()
    {
        $id = Yii::$app->request->post('id');
        $deliveryId = Yii::$app->request->post('delivery');
        $model = $this->findModel($id);

        $deliveriesCollection = ArrayHelper::map($this->getDeliveries(), 'id', 'name');
        $delivery = Delivery::findOne(['id' => $deliveryId]);
        if (!$deliveryId || !isset($deliveriesCollection[$deliveryId]) || !$delivery) {
            throw new NotFoundHttpException(Yii::t('common', 'Служба доставки не найдена.'));
        }

        if ($model->status == OrderPackaging::STATUS_ERROR) {

            $transaction = OrderPackaging::getDb()->beginTransaction();
            try {
                $hasContractRequisiteTariff = $delivery->hasContractRequisiteTariff();
                if (!$hasContractRequisiteTariff['status']) {
                    $transaction->rollBack();
                    return [
                        'status' => 'fail',
                        'message' => Yii::t('common', 'Заказы не могут быть отправлены в {delivery}. Заполните данные контракта.', [
                                    'delivery' => $delivery->name
                                ]
                            ) . ' ' . $hasContractRequisiteTariff['status']
                    ];
                }

                if ($delivery->hasDebts()) {
                    $transaction->rollBack();
                    return [
                        'status' => 'fail',
                        'message' => Yii::t('common', 'Заказы не могут быть отправлены в {delivery}. Есть ее не погашенная ДЗ более {days} дней.', [
                            'delivery' => $delivery->name,
                            'days' => $delivery->not_send_if_has_debts_days
                        ])
                    ];
                }

                $countOldOrdersInProcess = $delivery->countOldOrdersInProcess();

                if ($countOldOrdersInProcess) {
                    return [
                        'status' => 'fail',
                        'message' => Yii::t('common', 'Заказы не могут быть отправлены в {delivery}. {orders} заказов в процессе более {days} дней.', [
                            'delivery' => $delivery->name,
                            'orders' => $countOldOrdersInProcess,
                            'days' => $delivery->not_send_if_in_process_days
                        ])
                    ];
                }

                if (!$delivery->canDeliveryOrder($model->order)) {
                    return [
                        'status' => 'fail',
                        'message' => Yii::t('common', 'Заказ не может быть доставлен данной службой доставки.')
                    ];
                }

                if (in_array($model->order->status_id, [
                    OrderStatus::STATUS_CC_APPROVED,
                    OrderStatus::STATUS_DELIVERY_REJECTED
                ])) {

                    $model->status = OrderPackaging::STATUS_DELETED;
                    if (!$model->save()) {
                        $transaction->rollBack();
                        return [
                            'status' => 'fail',
                            'message' => Yii::t('common', $model->getFirstErrorAsString())
                        ];
                    }

                    if ($delivery->workflow) {
                        $priorityNextStatuses = $delivery->workflow->getNextStatuses(OrderStatus::STATUS_CC_APPROVED);
                    } else {
                        $priorityNextStatuses = $model->order->getNextStatuses(OrderStatus::STATUS_CC_APPROVED);
                    }

                    if ($delivery->auto_sending &&
                        !in_array(OrderStatus::STATUS_LOG_ACCEPTED, $priorityNextStatuses) &&
                        in_array(OrderStatus::STATUS_DELIVERY_PENDING, $priorityNextStatuses)
                    ) {
                        $model->order->pending_delivery_id = $deliveryId;
                        $model->order->status_id = OrderStatus::STATUS_CC_APPROVED;
                        $model->order->route = Yii::$app->controller->route;
                        $model->order->commentForLog = $model->reason;
                        $model->order->deliveryRequest->delete();

                        $result = Delivery::createRequest($delivery, $model->order);
                        if ($result['status'] != 'success') {
                            $transaction->rollBack();
                            return [
                                'status' => 'fail',
                                'message' => $result['message']
                            ];
                        }
                    } else {
                        $model->order->pending_delivery_id = $deliveryId;
                        $model->order->status_id = OrderStatus::STATUS_CC_APPROVED;
                        $model->order->route = Yii::$app->controller->route;
                        $model->order->commentForLog = $model->reason;

                        if ($model->order->save(true, ['pending_delivery_id', 'status_id', 'route'])) {
                            $model->order->deliveryRequest->delete();
                        } else {
                            return [
                                'status' => 'fail',
                                'message' => $model->order->getFirstErrorAsString()
                            ];
                        }
                    }
                } else {
                    $transaction->rollBack();
                    return [
                        'status' => 'fail',
                        'message' => Yii::t('common',
                            'Смена доставки у заказа {orderId} со статусом "{status}" запрещена.', [
                                'orderId' => $model->order->id,
                                'status' => $model->order->status->name,
                            ]
                        )
                    ];
                }

                $transaction->commit();
                return [
                    'status' => 'success',
                    'message' => Yii::t('common', 'У заказа сменилась доставка, заявка на упаковку удалена')
                ];
            } catch (\Exception $e) {
                $transaction->rollBack();
                return [
                    'status' => 'success',
                    'message' => Yii::t('common', $e->getMessage())
                ];
            }
        } else {
            return [
                'status' => 'fail',
                'message' => Yii::t('common', 'Некорректный статус заявки для операции')
            ];
        }
    }

    /**
     * @return array
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \yii\web\HttpException
     */
    public function actionSendInDelivery()
    {
        $id = Yii::$app->request->post('id');
        $deliveryId = Yii::$app->request->post('delivery');
        $model = $this->findApproveOrder($id);

        $deliveriesCollection = ArrayHelper::map($this->getDeliveries(), 'id', 'name');
        $delivery = Delivery::findOne(['id' => $deliveryId]);
        if (!$deliveryId || !isset($deliveriesCollection[$deliveryId]) || !$delivery) {
            throw new NotFoundHttpException(Yii::t('common', 'Служба доставки не найдена.'));
        }

        if (!empty($model->created_at) && $model->created_at < (time() - 3600 * 24 * 7) && !Yii::$app->user->can("old_orders_access")) {
            throw new ForbiddenHttpException(Yii::t('common',
                "У вас недостаточно прав для просмотра таких старых заказов"));
        }

        $deliveryIdPost = Yii::$app->request->post('delivery');
        $delivery = Delivery::findOne($deliveryIdPost);
        if (!$delivery->canSendOrder()) {
            $result['notifyWarning'] = true;
            $result['message'] = Yii::t('common', 'Апи курьерской службы не предназначено для отправки заказов');
            return $result;
        }

        $hasContractRequisiteTariff = $delivery->hasContractRequisiteTariff();

        if (!$hasContractRequisiteTariff['status']) {
            $result['notifyWarning'] = true;
            $result['message'] = Yii::t('common', 'Заказы не могут быть отправлены в {delivery}. Заполните данные контракта.', ['delivery' => $delivery->name]) . ' ' . $hasContractRequisiteTariff['status'];
            return $result;
        }

        if ($delivery->hasDebts()) {
            $result['notifyWarning'] = true;
            $result['message'] = Yii::t('common', 'Заказы не могут быть отправлены в {delivery}. Есть ее не погашенная ДЗ более {days} дней.', [
                'delivery' => $delivery->name,
                'days' => $delivery->not_send_if_has_debts_days
            ]);
            return $result;
        }

        $countOldOrdersInProcess = $delivery->countOldOrdersInProcess();
        if ($countOldOrdersInProcess) {
            $result['notifyWarning'] = true;
            $result['message'] = Yii::t('common', 'Заказы не могут быть отправлены в {delivery}. {orders} заказов в процессе более {days} дней.', [
                'delivery' => $delivery->name,
                'orders' => $countOldOrdersInProcess,
                'days' => $delivery->not_send_if_in_process_days
            ]);
            return $result;
        }

        $deliveryId = $model->pending_delivery_id ? $model->pending_delivery_id : $deliveryIdPost;

        if (!$model->deliveryRequest) {
            if ($deliveryId != $deliveryIdPost) {
                $result['notifyWarning'] = true;

                $result['message'] = Yii::t(
                    'common',
                    'Заказу #{id} была назначена другая курьерская служба. Обратитесь к разработчику.',
                    [
                        'id' => $model->id,
                    ]
                );
            } else {
                $result['notifyWarning'] = false;
                $result = Delivery::createRequest($deliveryId, $model);
                if ($result['status'] == 'success') {
                    $result['message'] = Yii::t('common', 'Заказу назначена служба доставки');
                }
            }
        } else {
            $result['status'] = 'fail';
            $result['message'] = Yii::t(
                'common',
                'Заявка уже существует. Требуется повторная отправка заявки в службу доставки.'
            );
        }
        return $result;
    }


    /**
     * @param integer $id
     * @return OrderPackaging
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        $model = OrderPackaging::find()
            ->where([OrderPackaging::tableName() . '.id' => $id])
            ->andWhere(['<>', 'status', OrderPackaging::STATUS_DELETED])
            ->one();

        if ($model == null) {
            throw new NotFoundHttpException(Yii::t('common', 'Заявка на упаковку не найдена.'));
        }

        return $model;
    }

    /**
     * @param int|null $id
     * @return Order
     * @throws NotFoundHttpException
     * @throws \yii\web\HttpException
     */
    protected function findApproveOrder(?int $id)
    {
        if (!$id) {
            throw new NotFoundHttpException(Yii::t('common', 'Заказ не задан.'));
        }
        $query = Order::find()
            ->joinWith([
                'callCenterRequest',
                'deliveryRequest',
            ])
            ->where([
                Order::tableName() . '.id' => $id,
                'country_id' => Yii::$app->user->getCountry()->id
            ]);

        $query->andWhere([Order::tableName() . '.status_id' => OrderStatus::STATUS_CC_APPROVED]);

        //если пользователь службы доставки (роль implant), то только привязанные ему доставки
        if (Yii::$app->user->getIsImplant()) {
            $delivery_ids = Yii::$app->user->getDeliveriesIds();
            $query->andFilterWhere([
                'or',
                [
                    'and',
                    DeliveryRequest::tableName() . '.delivery_id is not null',
                    [DeliveryRequest::tableName() . ".delivery_id" => $delivery_ids]
                ],
                [
                    'and',
                    DeliveryRequest::tableName() . '.delivery_id is null',
                    [Order::tableName() . ".pending_delivery_id" => $delivery_ids]
                ]
            ]);
        }
        $model = $query->one();

        if (!$model) {
            throw new NotFoundHttpException(Yii::t('common', 'Заказ не найден.'));
        }

        return $model;
    }


    /**
     * @param $filePath
     * @return bool|string
     * @throws \yii\base\InvalidConfigException
     */
    protected function convertToPdf($filePath)
    {
        $info = pathinfo($filePath);
        if (strtolower($info['extension']) == 'pdf') {
            return $filePath;
        } else {
            $pdfFileName = $info['dirname'] . DIRECTORY_SEPARATOR . $info['filename'] . '.pdf';
            if (is_file($pdfFileName)) {
                return $pdfFileName;
            } else {

                $imageData = base64_encode(file_get_contents($filePath));
                $src = 'data: ' . mime_content_type($filePath) . ';base64,' . $imageData;
                $content = '<img src="' . $src . '">';

                $document = new Pdf([
                    'mode' => Pdf::MODE_UTF8,
                    'options' => [
                        'autoScriptToLang' => true,
                        'autoLangToFont' => true,
                        'ignore_invalid_utf8' => true,
                    ],
                    'content' => $content,
                    'filename' => $pdfFileName,
                    'destination' => Pdf::DEST_FILE,
                ]);
                $document->render();
                return $pdfFileName;
            }
        }
    }
}