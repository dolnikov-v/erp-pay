<?php

namespace app\modules\packager\controllers;

use app\modules\packager\models\search\PackagerSearch;
use Yii;
use app\components\web\Controller;
use app\modules\packager\models\Packager;
use yii\helpers\Url;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;

class ControlController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new PackagerSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws HttpException
     * @throws NotFoundHttpException
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->findModel($id);
        } else {
            $model = new Packager();
        }

        if ($model->load(Yii::$app->request->post())) {
            $isNewRecord = $model->isNewRecord;
            if ($isNewRecord) {
                $model->country_id = Yii::$app->user->country->id;
            } elseif($model->country_id != Yii::$app->user->country->id) {
                throw new NotFoundHttpException(Yii::t('common', 'Служба упаковки находится в другой стране.'));
            }

            $model->setEmailList($model->emailList);
            if ($model->save()) {
                Yii::$app->notifier->addNotification($isNewRecord ? Yii::t('common', 'Служба упаковки успешно добавлена.') : Yii::t('common', 'Служба упаковки успешно сохранена.'), 'success');
                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }

        return $this->render('edit', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionActivate($id)
    {
        $model = $this->findModel($id);

        $model->active = 1;

        if ($model->save(true, ['active'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Служба упаковки успешно активирована.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionDeactivate($id)
    {
        $model = $this->findModel($id);

        $model->active = 0;

        if ($model->save(true, ['active'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Служба упаковки успешно деактивирована.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if ($model->delete()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Служба упаковки успешно удалена.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionActivateAutoSending($id)
    {
        $model = $this->findModel($id);

        if (!$model->email) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Не заполнено поле "E-mail"'));
            return $this->redirect(Url::toRoute('index'));
        }

        $model->auto_sending = 1;

        if ($model->save(true, ['auto_sending'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', '«Автоматическая отправка» успешно разрешена.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionDeactivateAutoSending($id)
    {
        $model = $this->findModel($id);

        $model->auto_sending = 0;

        if ($model->save(true, ['auto_sending'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', '«Автоматическая отправка» успешно запрещена.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param integer $id
     * @return Packager
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        $model = Packager::find()
            ->where([Packager::tableName() . '.id' => $id])
            ->byCountryId(Yii::$app->user->country->id)
            ->one();

        if ($model == null) {
            throw new NotFoundHttpException(Yii::t('common', 'Упаковщик не найден.'));
        }

        return $model;
    }
}