<?php

namespace app\modules\packager\assets;

use yii\web\AssetBundle;

/**
 * Class LabelPrintingAsset
 * @package app\modules\packager\assets
 */
class LabelPrintingAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/packager';

    public $js = [
        'label-printing.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}