<?php

namespace app\modules\packager\assets;

use yii\web\AssetBundle;

/**
 * Class CancelCompletingAsset
 * @package app\modules\packager\assets
 */
class CancelCompletingAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/packager';

    public $js = [
        'cancel-completing.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}