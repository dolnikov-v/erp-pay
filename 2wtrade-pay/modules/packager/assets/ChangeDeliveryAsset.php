<?php

namespace app\modules\packager\assets;

use yii\web\AssetBundle;

/**
 * Class ChangeDeliveryAsset
 * @package app\modules\packager\assets
 */
class ChangeDeliveryAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/packager';

    public $js = [
        'change-delivery.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}