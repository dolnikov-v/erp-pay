<?php

namespace app\modules\packager\assets;

use yii\web\AssetBundle;

/**
 * Class SetApproveAsset
 * @package app\modules\packager\assets
 */
class SetApproveAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/packager';

    public $js = [
        'set-approve.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}