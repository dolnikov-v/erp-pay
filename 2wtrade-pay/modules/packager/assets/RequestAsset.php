<?php
namespace app\modules\packager\assets;

use yii\web\AssetBundle;

/**
 * Class RequestAsset
 * @package app\modules\packager\assets
 */
class RequestAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/packager';

    public $js = [
        'request.js',
    ];

    public $css = [
        'request.css'
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
        'app\assets\SiteAsset',
    ];
}
