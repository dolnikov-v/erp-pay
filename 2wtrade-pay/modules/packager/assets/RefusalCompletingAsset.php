<?php

namespace app\modules\packager\assets;

use yii\web\AssetBundle;

/**
 * Class RefusalCompletingAsset
 * @package app\modules\packager\assets
 */
class RefusalCompletingAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/packager';

    public $js = [
        'refusal-completing.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}