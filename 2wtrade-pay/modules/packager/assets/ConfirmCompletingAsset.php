<?php

namespace app\modules\packager\assets;

use yii\web\AssetBundle;

/**
 * Class ConfirmCompletingAsset
 * @package app\modules\packager\assets
 */
class ConfirmCompletingAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/packager';

    public $js = [
        'confirm-completing.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}