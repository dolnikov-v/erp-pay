<?php

namespace app\modules\packager\assets;

use yii\web\AssetBundle;

/**
 * Class GetManifestAsset
 * @package app\modules\packager\assets
 */
class GetManifestAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/packager';

    public $js = [
        'get-manifest.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}