<?php

namespace app\modules\packager\assets;

use yii\web\AssetBundle;

/**
 * Class SetShippedAsset
 * @package app\modules\packager\assets
 */
class SetShippedAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/packager';

    public $js = [
        'set-shipped.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}