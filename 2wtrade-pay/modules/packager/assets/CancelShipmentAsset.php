<?php

namespace app\modules\packager\assets;

use yii\web\AssetBundle;

/**
 * Class CancelShipmentAsset
 * @package app\modules\packager\assets
 */
class CancelShipmentAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/packager';

    public $js = [
        'cancel-shipment.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}