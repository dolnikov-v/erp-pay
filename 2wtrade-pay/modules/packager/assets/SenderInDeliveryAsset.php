<?php

namespace app\modules\packager\assets;

use yii\web\AssetBundle;

/**
 * Class SenderInDeliveryAsset
 * @package app\modules\packager\assets
 */
class SenderInDeliveryAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/packager';

    public $js = [
        'sender-in-delivery.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}