<?php

namespace app\modules\packager\assets;

use yii\web\AssetBundle;

/**
 * Class EmailListAsset
 * @package app\modules\packager\assets
 */
class EmailListAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/packager/email-list';

    public $js = [
        'email-list.js',
    ];

    public $css = [
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
