<?php

namespace app\modules\packager\assets;

use yii\web\AssetBundle;

/**
 * Class ListGenerationAsset
 * @package app\modules\packager\assets
 */
class ListGenerationAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/packager';

    public $js = [
        'list-generation.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}