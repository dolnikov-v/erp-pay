<?php

namespace app\modules\packager\assets;

use yii\web\AssetBundle;

/**
 * Class SetReturnAsset
 * @package app\modules\packager\assets
 */
class SetReturnAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/packager';

    public $js = [
        'set-return.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}