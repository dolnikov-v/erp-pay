<?php

namespace app\modules\packager\components\exporter;

use PHPExcel;
use PHPExcel_Writer_Excel2007;
use Yii;

/**
 * Class ExporterRecordExcel
 * @package app\modules\packager\components\exporter
 */
class ExporterRecordExcel extends ExporterRecord
{
    protected static $type = self::TYPE_EXCEL;

    /**
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Writer_Exception
     * @throws \yii\base\ExitException
     */
    public function sendFile()
    {
        $excel = new PHPExcel();
        $excel->setActiveSheetIndex(0);

        $sheet = $excel->getActiveSheet();
        $sheet->setTitle(Yii::t('common', 'Заказы'));

        $iHead = 0;
        foreach ($this->getFields() as $label) {
            $excel->getActiveSheet()->getColumnDimension($this->printAlphabet($iHead))
                ->setAutoSize(true);
            $sheet->setCellValue($this->printAlphabet($iHead) . 1, $label);
            $iHead++;
        }

        // Заполнение данными
        $offset = 0;
        $limit = 100;

        while ($rows = $this->getRequestData($offset, $limit)) {
            foreach ($rows as $key => $row) {
                $i = 0;
                foreach ($this->prepareRow($row) as $value) {
                    $sheet->setCellValue($this->printAlphabet($i) . ($offset + $key + 2), $value);
                    $i++;
                }
            }

            $offset += $limit;
        }

        $filename = "Export_DeliveryReport_" . date("Y-m-d_H-i-s") . "_" . Yii::t('common',
                Yii::$app->user->country->name) . '_2wtrade.xlsx';

        header('Content-Type:xlsx:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition:attachment;filename="' . $filename . '"');

        $objWriter = new PHPExcel_Writer_Excel2007($excel);
        $objWriter->save('php://output');

        Yii::$app->end();
    }
}
