<?php

namespace app\modules\packager\components\exporter;

use PHPExcel;
use PHPExcel_Writer_CSV;
use Yii;

/**
 * Class ExporterCsv
 * @package app\modules\packager\components\exporter
 */
class ExporterRecordCsv extends ExporterRecord
{
    protected static $type = self::TYPE_CSV;

    /**
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Writer_Exception
     * @throws \yii\base\ExitException
     */
    public function sendFile()
    {
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);

        $sheet = $objPHPExcel->getActiveSheet();

        $iHead = 0;

        // Установка заголовка
        foreach ($this->getFields() as $name) {
            $sheet->setCellValue($this->printAlphabet($iHead) . 1, $name);
            $iHead++;
        }

        $offset = 0;
        $limit = 100;

        while ($records = $this->getRequestData($offset, $limit)) {
            foreach ($records as $key => $record) {
                $i = 0;
                foreach ($this->prepareRow($record) as $value) {
                    $sheet->setCellValue($this->printAlphabet($i) . ($offset + $key + 2), $value);
                    $i++;
                }
            }

            $offset += $limit;
        }

        $filename = "Export_DeliveryReport_". date("Y-m-d_H-i-s"). "_". Yii::t('common', Yii::$app->user->country->name) . '_2wtrade.csv';

        header('Content-Type: text/csv; charset=UTF-8');
        header('Content-Disposition:attachment;filename="' . $filename . '"');

        $objWriter = new PHPExcel_Writer_CSV($objPHPExcel);
        $objWriter->setDelimiter(',');
        $objWriter->setUseBOM(true);

        $objWriter->setLineEnding("\r\n");
        $objWriter->save('php://output');
        Yii::$app->end();
    }
}
