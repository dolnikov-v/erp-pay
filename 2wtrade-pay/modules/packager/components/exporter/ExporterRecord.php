<?php

namespace app\modules\packager\components\exporter;

use app\modules\packager\models\OrderPackaging;
use Yii;
use yii\base\BaseObject;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;

/**
 * Class ExporterRecord
 * @package app\modules\packager\components\exporter
 */
abstract class ExporterRecord extends BaseObject
{
    const TYPE_EXCEL = 'excel';
    const TYPE_CSV = 'csv';

    /** @var ActiveDataProvider */
    public $dataProvider;

    /**
     * @var string
     */
    protected static $type;

    abstract public function sendFile();

    /**
     * @return string
     */
    public static function getExporterUrl()
    {
        $queryParams = Yii::$app->request->getQueryParams();
        $queryParams['export'] = static::$type;
        $url = array_merge([''], $queryParams);

        return Url::to($url);
    }

    /**
     * @param int $offset
     * @param int $limit
     * @return OrderPackaging[]
     */
    public function getRequestData($offset = null, $limit = null)
    {
        /** @var \yii\db\ActiveQuery $query */
        $query = $this->dataProvider->query;

        $query->orderBy(OrderPackaging::tableName() . '.id');
        $query->offset($offset);
        $query->limit($limit);

        return $query->all();
    }

    /**
     * @param $key
     * @return string
     */
    protected function printAlphabet($key)
    {
        $array = [];
        $range = range(65, 90);

        foreach ($range as $letter) {
            $array[] = chr($letter);
        }

        $str = "";
        do {
            $index = $key % 25;
            $str = $array[$index] . $str;
            $key /= 25;
            $key = intval($key) - 1;
        } while ($key >= 0);

        return $str;
    }

    /**
     * @param $dataCell
     * @return mixed|string
     */
    protected function prepareMapping($dataCell)
    {
        if ($dataCell) {
            return $dataCell;
        }
        return "";
    }

    /**
     * @return array
     */
    protected function getFields()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'deliveryRequest.tracking' => 'Tracking',
            'packager_id' => 'Packager service',
            'status' => 'Status',
            'updated_at' => 'Date update',
            'customer_full_name' => 'Customer full name',
            'product_list' => 'Products'
        ];
    }

    /**
     * @param OrderPackaging $orderPackaging
     * @return array
     */
    protected function prepareRow($orderPackaging)
    {
        $products = '';
        if ($orderPackaging->order->orderProducts) {
            $products = [];
            foreach ($orderPackaging->order->orderProducts as $product) {
                $products[] = $product->product->name . ' - ' . $product->quantity;
            }
            $products = join(PHP_EOL, $products);
        }

        return [
            'id' => $orderPackaging->id,
            'order_id' => $orderPackaging->order_id,
            'deliveryRequest.tracking' => $orderPackaging->deliveryRequest->tracking,
            'packager_id' => $orderPackaging->packager->name,
            'status' => $orderPackaging->getStatusLabel(),
            'updated_at' => Yii::$app->formatter->asDatetime($orderPackaging->updated_at),
            'customer_full_name' => $orderPackaging->order->customer_full_name,
            'product_list' => $products,
        ];
    }
}
