<?php

namespace app\modules\packager\models;

use app\helpers\Utils;
use app\models\Country;
use app\modules\packager\models\query\PackagerQuery;
use Yii;
use app\components\db\ActiveRecordLogUpdateTime;
use app\models\User;
use PhpOffice\PhpWord\Shared\ZipArchive;

/**
 * Class Packager
 * @package app\modules\packager\models
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property integer $auto_sending
 * @property integer $active
 * @property integer $country_id
 * @property Country $country
 * @property integer $created_at
 * @property integer $updated_at
 */
class Packager extends ActiveRecordLogUpdateTime
{
    /**
     * @var array
     */
    public $emailList = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%packager}}';
    }

    /**
     * @return PackagerQuery
     */
    public static function find()
    {
        return new PackagerQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['name', 'email'], 'string', 'max' => 255],
            [['auto_sending', 'country_id', 'created_at', 'updated_at', 'active'], 'integer'],
            [['name', 'country_id'], 'required'],
            [['auto_sending', 'active'], 'default', 'value' => 0],
            [['auto_sending', 'active'], 'in', 'range' => [0, 1]],
            [['country_id'], 'in', 'range' => array_keys(User::getAllowCountries())],
            ['emailList', 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function getEmailList()
    {
        $emails = [];
        if (preg_match_all('/[,]*([^,\s]+@[a-z0-9.]+)/i', $this->email, $match) && isset($match[1])) {
            foreach ($match[1] as $item) {
                $emails[] = $item;
            }
        }
        return $emails;
    }

    /**
     * @param $list
     */
    public function setEmailList($list)
    {
        if (!is_array($list)) {
            $list = [$list];
        }

        $buffer = [];
        foreach ($list as $email) {
            if (preg_match_all('/[,]*([^,\s]+@[a-z0-9.]+)/i', $email, $match) && isset($match[1])) {
                foreach ($match[1] as $item) {
                    $buffer[] = $item;
                }
            }
        }

        $this->email = implode(',', $buffer);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'name' => Yii::t('common', 'Название'),
            'email' => Yii::t('common', 'E-mail'),
            'country_id' => Yii::t('common', 'Страна'),
            'active' => Yii::t('common', 'Доступность'),
            'auto_sending' => Yii::t('common', 'Автоматическая отправка'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата обновления'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @param array $invoices
     * @param string|null $fileName
     * @param string|null $filePath path/to/directory
     * @return string
     * @throws \yii\web\ForbiddenHttpException
     */
    public function package(array $invoices, string $fileName = null, string $filePath = null)
    {
        if (!$filePath) {
            $filePath = Yii::getAlias('@downloads') . DIRECTORY_SEPARATOR . 'temp';
        }
        if (!file_exists($filePath)) {
            Utils::prepareDir($filePath, 0777);
        }
        if (!$fileName) {
            do {
                $fileName = Utils::uid();
                $fileName .= '.zip';
            } while (file_exists($filePath . DIRECTORY_SEPARATOR . $fileName));
        } else {
            $fileName .= '.zip';
            if (file_exists($filePath . DIRECTORY_SEPARATOR . $fileName)) {
                unlink($filePath . DIRECTORY_SEPARATOR . $fileName);
            }
        }

        $zip = new ZipArchive();
        if ($zip->open($filePath . DIRECTORY_SEPARATOR . $fileName, ZipArchive::CREATE) !== true) {
            throw new \Exception(Yii::t('common', 'Невозможно открыть "{dir}."', ['dir' => $filePath . DIRECTORY_SEPARATOR . $fileName]));
        }
        try {
            foreach ($invoices as $invoice) {
                $zip->addFile($invoice[0], $invoice[1]);
            }
            $zip->close();
        } catch (\Exception $e) {
            throw new \Exception(Yii::t('common', 'Не удалось создать архив.'));
        }

        return $filePath . DIRECTORY_SEPARATOR . $fileName;
    }
}