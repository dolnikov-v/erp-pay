<?php

namespace app\modules\packager\models;

use app\helpers\Utils;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\components\InvoiceBuilder;
use app\modules\order\models\OrderLogisticList;
use app\modules\packager\models\query\OrderPackagingQuery;
use Yii;
use app\components\db\ActiveRecordLogUpdateTime;
use app\modules\order\models\Order;
use yii\helpers\ArrayHelper;
use kartik\mpdf\Pdf;

/**
 * Class OrderPackaging
 * @package app\modules\packager\models
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $delivery_id
 * @property integer $packager_id
 * @property Order $order
 * @property Delivery $delivery
 * @property DeliveryRequest $deliveryRequest
 * @property Packager $packager
 * @property string $status
 * @property string $reason
 * @property integer $list_id
 * @property string $method
 * @property string $invoice
 * @property integer $created_at
 * @property integer $updated_at
 *
 */
class OrderPackaging extends ActiveRecordLogUpdateTime
{
    const STATUS_PENDING = 0;           // Ожидает
    const STATUS_DONE = 1;              // Отгружено
    const STATUS_ERROR = 2;             // Ошибка
    const STATUS_COMPLETING = 3;        // Комплектуется
    const STATUS_COMPLETED = 4;         // Укомплектовано
    const STATUS_AWAITING_SHIPMENT = 5; // Ожидает отгрузки
    const STATUS_RETURN = 6;            // Возврат
    const STATUS_DELETED = 9;           // Удалено : скрытый статус

    const METHOD_MANUAL = 0;
    const METHOD_EMAIL = 1;

    /**
     * @var string
     */
    protected static $logoBase64;
    /**
     * @var string
     */
    protected static $folderFiles = 'packager-invoices';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_packaging}}';
    }

    /**
     * @return OrderPackagingQuery
     */
    public static function find()
    {
        return new OrderPackagingQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['order_id', 'delivery_id', 'packager_id', 'status', 'method', 'created_at', 'updated_at'], 'integer'],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
            [['delivery_id'], 'exist', 'skipOnError' => true, 'targetClass' => Delivery::className(), 'targetAttribute' => ['delivery_id' => 'id']],
            [['packager_id'], 'exist', 'skipOnError' => true, 'targetClass' => Packager::className(), 'targetAttribute' => ['packager_id' => 'id']],
            [['invoice'], 'string', 'max' => 100],
            [['reason'], 'string', 'max' => 255],
            [['list_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrderLogisticList::className(), 'targetAttribute' => ['list_id' => 'id']],
        ];
    }

    /**
     * @return array
     */
    public static function getStatuses()
    {
        return [
            static::STATUS_PENDING => Yii::t('common', 'Ожидает'),
            static::STATUS_COMPLETING => Yii::t('common', 'Комплектуется'),
            static::STATUS_COMPLETED => Yii::t('common', 'Укомплектовано'),
            static::STATUS_AWAITING_SHIPMENT => Yii::t('common', 'Ожидает отгрузки'),
            static::STATUS_DONE => Yii::t('common', 'Отгружено'),
            static::STATUS_RETURN => Yii::t('common', 'Возврат'),
            static::STATUS_ERROR => Yii::t('common', 'Ошибка'),
        ];
    }

    /**
     * @return array
     */
    public static function getMethods()
    {
        return [
            static::METHOD_MANUAL => Yii::t('common', 'Вручную'),
            static::METHOD_EMAIL => Yii::t('common', 'E-mail'),
        ];
    }

    /**
     * @return mixed
     */
    public function getStatusLabel()
    {
        return ArrayHelper::getValue(self::getStatuses(), $this->status);
    }

    /**
     * @return mixed
     */
    public function getMethodLabel()
    {
        return ArrayHelper::getValue(self::getMethods(), $this->method);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'order_id' => Yii::t('common', 'Номер заказа'),
            'delivery_id' => Yii::t('common', 'Служба доставки'),
            'packager_id' => Yii::t('common', 'Служба упаковки'),
            'status' => Yii::t('common', 'Статус'),
            'method' => Yii::t('common', 'Способ выгрузки'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата обновления'),
            'invoice' => Yii::t('common', 'Накладная'),
            'reason' => Yii::t('common', 'Причина'),
            'list_id' => Yii::t('common', 'Лист'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDelivery()
    {
        return $this->hasOne(Delivery::className(), ['id' => 'delivery_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveryRequest()
    {
        return $this->hasOne(DeliveryRequest::className(), ['order_id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPackager()
    {
        return $this->hasOne(Packager::className(), ['id' => 'packager_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getList()
    {
        return $this->hasOne(OrderLogisticList::className(), ['id' => 'list_id']);
    }


    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function generateInvoices($rewrite = false)
    {
        $build = false;
        if (!$this->invoice || !file_exists($this->getDownloadPath($this->invoice) . DIRECTORY_SEPARATOR . $this->invoice) || $rewrite) {
            $build = true;
        }
        if ($build) {
            $fileName = $this->order_id . '.pdf';
            $filePath = $this->getDownloadPath($fileName, true) . DIRECTORY_SEPARATOR . $fileName;
            $this->invoice = $fileName;
            $templateFile = $this->getTemplateFilePath();
            $viewData['data'] = $this->getViewData();
            $content = Yii::$app->controller->renderFile($templateFile, $viewData);

            $document = new Pdf([
                'mode' => Pdf::MODE_UTF8,
                'options' => [
                    'autoScriptToLang' => true,
                    'autoLangToFont' => true,
                    'ignore_invalid_utf8' => true,
                    'tabSpaces' => 4
                ],
                'marginLeft' => 11,
                'marginRight' => 11,
                'marginTop' => 8,
                'marginBottom' => 8,
                'content' => $content,
                'filename' => $filePath,
                'destination' => Pdf::DEST_FILE,
                'cssFile' => Yii::getAlias('@templates') . DIRECTORY_SEPARATOR . self::$folderFiles . DIRECTORY_SEPARATOR . 'pdf-style.css',
            ]);
            $document->render();

            $this->save(false, ['invoice']);
        }
        return $this->getDownloadPath($this->invoice) . DIRECTORY_SEPARATOR . $this->invoice;
    }

    /**
     * @param string $fileName
     * @param bool $prepareDir
     * @return string
     * @throws \yii\web\ForbiddenHttpException
     */
    public function getDownloadPath($fileName, $prepareDir = false)
    {
        $subDir = substr($fileName, 0, 3);
        $dir = Yii::getAlias('@downloads') . DIRECTORY_SEPARATOR . self::$folderFiles . DIRECTORY_SEPARATOR . $this->packager_id . '-' . $this->delivery_id . DIRECTORY_SEPARATOR . $subDir;
        if ($prepareDir) {
            Utils::prepareDir($dir, 0777);
        }
        return $dir;
    }

    /**
     * @return string
     */
    private function getTemplateFilePath()
    {
        $filePath = Yii::getAlias('@templates') . DIRECTORY_SEPARATOR . InvoiceBuilder::INVOICE_TEMPLATE_PDF_1;
        $courierFile = Yii::getAlias(
                '@templates'
            ) . DIRECTORY_SEPARATOR . $this->packager->country->char_code . DIRECTORY_SEPARATOR . $this->delivery->char_code . DIRECTORY_SEPARATOR . InvoiceBuilder::INVOICE_TEMPLATE_PDF_1;
        if (file_exists($courierFile)) {
            $filePath = $courierFile;
        } else {
            $countryFile = Yii::getAlias(
                    '@templates'
                ) . DIRECTORY_SEPARATOR . $this->packager->country->char_code . DIRECTORY_SEPARATOR . InvoiceBuilder::INVOICE_TEMPLATE_PDF_1;
            if (file_exists($countryFile)) {
                $filePath = $countryFile;
            }
        }
        return $filePath;
    }

    /**
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    private function getViewData()
    {
        $values = $this->getValues();
        if (is_null(self::$logoBase64)) {
            $logoPath = Yii::getAlias('@templates') . DIRECTORY_SEPARATOR . 'invoice-files' . DIRECTORY_SEPARATOR . 'pdf-logo.png';
            $logoContent = file_get_contents($logoPath);
            self::$logoBase64 = 'data:image/png;base64,' . base64_encode($logoContent);;
        }
        $values['logoBase64'] = self::$logoBase64;
        return $values;
    }

    /**
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    private function getValues()
    {
        if (!$this->packager->country->currency) {
            throw new \Exception(Yii::t('common', 'Отсутствует валюта у страны.'), 400);
        }
        $values = [];
        $order = $this->order;
        $values['InvPrint'] = $this->delivery->char_code . '/' . $this->order_id;
        $amount = 0;
        $amountQuantity = 0;
        $index = 1;
        $amountTotal = 0;
        foreach ($order->orderProducts as $orderProduct) {
            $amount += $orderProduct->price;
            $amountQuantity += $orderProduct->quantity;
            $amountTotal += $orderProduct->price * $orderProduct->quantity;

            $tax = round(($orderProduct->price / 1.05) * 5) / 100;

            $values['P_name' . $index] = $orderProduct->product->name;
            $values['P_q' . $index] = $orderProduct->quantity;
            $values['P_pr' . $index] = $orderProduct->price - $tax;
            $values['T' . $index] = $tax;
            $values['Tt' . $index] = $orderProduct->price * $orderProduct->quantity;

            $index++;
        }

        for ($i = $index; $i <= 5; $i++) {
            $values['P_name' . $i] = ' ';
            $values['P_q' . $i] = ' ';
            $values['P_pr' . $i] = ' ';
            $values['T' . $i] = ' ';
            $values['Tt' . $i] = ' ';
        }

        $dateTime = new \DateTime('now', new \DateTimeZone($this->packager->country->timezone->timezone_id));
        $values['CC'] = $this->packager->country->currency->char_code;
        $values['Sum'] = $order->delivery + $order->price_total;
        $values['InvoiceDateOne'] = $dateTime->setTimestamp($this->created_at)->format('Y-m-d');
        $values['Pid'] = $order->id;
        $values['CallId'] = $order->callCenterRequest->id;
        $values['InvoiceDateTwo'] = $dateTime->setTimestamp($this->created_at)->format('Y-m-d');
        $values['Buyer'] = $order->customer_full_name;
        $values['Address'] = htmlspecialchars($order->customer_address);
        $values['Phone'] = $order->customer_phone;
        $values['P_qS'] = $amountQuantity;
        $values['TS'] = $amountTotal;

        if (!is_numeric($order->delivery_time_from)) {
            $values['Delivery_time_from'] = '';
        } else {
            $values['Delivery_time_from'] = $dateTime->setTimestamp($order->delivery_time_from)->format('Y-m-d');
        }

        $values['CC_comment'] = ($order->getCallCenterRequest()->exists() ? $order->callCenterRequest->comment : '');

        $numberFormatter = new \NumberFormatter("en", \NumberFormatter::SPELLOUT);
        $values['Sum_word'] = ucwords($numberFormatter->format($values['Sum']));

        return $values;
    }
}