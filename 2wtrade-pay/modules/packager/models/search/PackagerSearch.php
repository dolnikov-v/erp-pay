<?php
namespace app\modules\packager\models\search;

use Yii;
use app\modules\packager\models\Packager;
use yii\data\ActiveDataProvider;

/**
 * Class PackagerSearch
 * @package app\modules\packager\models\search
 */
class PackagerSearch extends Packager
{
    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['id'] = SORT_DESC;
        }

        $query = Packager::find()
            ->byCountryId(Yii::$app->user->country->id)
            ->orderBy($sort);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }
}
