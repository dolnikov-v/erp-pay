<?php
namespace app\modules\packager\models\search;

use app\modules\order\models\Order;
use Yii;
use app\modules\packager\models\OrderPackaging;
use yii\data\ActiveDataProvider;

/**
 * Class OrderPackagingSearch
 * @package app\modules\packager\models\search
 */
class OrderPackagingSearch extends OrderPackaging
{
    /**
     * @var string
     */
    public $date_from;

    /**
     * @var string
     */
    public $date_to;

    /**
     * @var integer | array
     */
    public $order_id;

    /**
     * @var integer | array
     */
    public $status = OrderPackaging::STATUS_PENDING;

    /**
     * @var integer
     */
    public $delivery_id;


    public function rules()
    {
        return [
            [['date_from', 'date_to', 'order_id'], 'string'],
            [['status', 'delivery_id'], 'integer',]
        ];
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['id'] = SORT_DESC;
        }

        $query = OrderPackaging::find()
            ->joinWith(['order', 'deliveryRequest'])
            ->where([Order::tableName() . '.country_id' => Yii::$app->user->country->id])
            ->orderBy($sort);

        if ($this->load($params)) {

            if ($this->date_from && $dateFrom = Yii::$app->formatter->asTimestamp($this->date_from)) {
                $query->andWhere(['>=', OrderPackaging::tableName() . '.created_at', $dateFrom]);
            }

            if ($this->date_to && $dateTo = Yii::$app->formatter->asTimestamp($this->date_to . ' 23:59:59')) {
                $query->andWhere(['<=', OrderPackaging::tableName() . '.created_at', $dateTo]);
            }

            if ($this->order_id) {
                $query->andWhere([OrderPackaging::tableName() . '.order_id' => array_map('trim', explode(',', $this->order_id))]);
            } else {
                $this->order_id = null;
            }
        }

        $this->status = intval($this->status);

        $query->andWhere([OrderPackaging::tableName() . '.status' => $this->status]);

        if ($this->delivery_id) {
            $query->andWhere([OrderPackaging::tableName() . '.delivery_id' => $this->delivery_id]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }

    /**
     * @return string
     */
    public function formName()
    {
        return 's';
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'date' => Yii::t('common', 'Дата создания'),
        ]);
    }
}
