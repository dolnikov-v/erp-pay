<?php

namespace app\modules\packager\models\search;

use app\modules\callcenter\models\CallCenterRequest;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use app\modules\report\components\filters\DateFilter;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * Class OrderApproveSearch
 * @package app\modules\packager\models\search
 */
class OrderApproveSearch extends Order
{
    /**
     * @var string
     */
    public $date_from;

    /**
     * @var string
     */
    public $date_to;

    /**
     * @var integer | array
     */
    public $order_id;

    /**
     * @var integer
     */
    public $delivery_id;

    /**
     * @var DateFilter
     */
    protected $dateFilter;

    public function rules()
    {
        return [
            [['date_from', 'date_to', 'order_id'], 'string'],
            [['status', 'delivery_id'], 'integer']
        ];
    }

    /**
     * @return DateFilter
     */
    public function getDateFilter()
    {
        if (is_null($this->dateFilter)) {
            $this->dateFilter = new DateFilter();
            $this->dateFilter->from = Yii::$app->formatter->asDate(time());
            $this->dateFilter->to = Yii::$app->formatter->asDate(time());
            $this->dateFilter->types = [
                DateFilter::TYPE_CREATED_AT => Yii::t('common', 'Дата создания'),
                DateFilter::TYPE_UPDATED_AT => Yii::t('common', 'Дата обновления'),
                DateFilter::TYPE_CALL_CENTER_SENT_AT => Yii::t('common', 'Дата отправки (колл-центр)'),
                DateFilter::TYPE_CALL_CENTER_APPROVED_AT => Yii::t('common', 'Дата одобрения (колл-центр)'),
                DateFilter::TYPE_DELIVERY_TIME_FROM => Yii::t('common', 'Дата предполагаемой доставки'),
                DateFilter::TYPE_TS_SPAWN => Yii::t('common', 'Дата создания у партнера (лид)'),
            ];
            $this->dateFilter->type = DateFilter::TYPE_CALL_CENTER_APPROVED_AT;
        }

        return $this->dateFilter;
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['delivery_time_from'] = SORT_ASC;
            $sort['id'] = SORT_DESC;
        }

        if (empty($params[$this->formName()]['from'])) {
            $params[$this->formName()]['from'] = Yii::$app->formatter->asDate(time());
        }

        if (empty($params[$this->formName()]['to'])) {
            $params[$this->formName()]['to'] = Yii::$app->formatter->asDate(time());
        }

        $query = Order::find()
            ->joinWith(['callCenterRequest'])
            ->where([Order::tableName() . '.country_id' => Yii::$app->user->country->id])
            ->andWhere([Order::tableName() . '.status_id' => OrderStatus::STATUS_CC_APPROVED])
            ->orderBy($sort);

        if ($this->load($params)) {

            $this->getDateFilter()->apply($query, $params);

            if ($this->order_id) {
                $query->andWhere([Order::tableName() . '.id' => array_map('trim', explode(',', $this->order_id))]);
            } else {
                $this->order_id = null;
            }
        }

        if ($this->delivery_id) {
            $query->andWhere([Order::tableName() . '.pending_delivery_id' => $this->delivery_id]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }

    /**
     * @return string
     */
    public function formName()
    {
        return 's';
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'order_id' => Yii::t('common', 'Номер заказа'),
            'delivery_id' => Yii::t('common', 'Служба доставки'),
        ]);
    }
}
