<?php
namespace app\modules\packager\models\query;

use app\components\db\ActiveQuery;
use app\modules\packager\models\Packager;

/**
 * Class PackagerQuery
 * @package app\modules\packager\models\query
 *
 * @method Packager one($db = null)
 * @method Packager[] all($db = null)
 */
class PackagerQuery extends ActiveQuery
{
    /**
     * @param integer $countryId
     * @return $this
     */
    public function byCountryId($countryId)
    {
        return $this->andWhere([Packager::tableName() . '.country_id' => $countryId]);
    }

    /**
     * @return $this
     */
    public function active()
    {
        return $this->andWhere(['active' => 1]);
    }

}
