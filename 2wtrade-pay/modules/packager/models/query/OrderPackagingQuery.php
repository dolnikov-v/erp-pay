<?php
namespace app\modules\packager\models\query;

use app\components\db\ActiveQuery;
use app\modules\packager\models\OrderPackaging;

/**
 * Class PackagerQuery
 * @package app\modules\packager\models\query
 *
 * @method OrderPackaging one($db = null)
 * @method OrderPackaging[] all($db = null)
 */
class OrderPackagingQuery extends ActiveQuery
{

}
