<?php

namespace app\modules\packager\widgets;

use app\modules\packager\models\search\OrderPackagingSearch;
use yii\base\Widget;

/**
 * Class ListGeneration
 * @package app\modules\packager\widgets
 */
class ListGeneration extends Widget
{
    /**
     * @var string
     */
    public $url;

    /**
     * @var OrderPackagingSearch
     */
    public $modelSearch;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('list-generation/modal', [
            'url' => $this->url,
            'modelSearch' => $this->modelSearch,
        ]);
    }
}
