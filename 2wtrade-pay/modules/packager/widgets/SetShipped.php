<?php

namespace app\modules\packager\widgets;

use yii\base\Widget;

/**
 * Class SetShipped
 * @package app\modules\packager\widgets
 */
class SetShipped extends Widget
{
    /**
     * @var string
     */
    public $url;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('set-shipped/modal', [
            'url' => $this->url
        ]);
    }
}
