<?php

namespace app\modules\packager\widgets;

use yii\base\Widget;

/**
 * Class ChangeDelivery
 * @package app\modules\packager\widgets
 */
class ChangeDelivery extends Widget
{
    /**
     * @var string
     */
    public $url;

    /**
     * @var array
     */
    public $deliveries;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('change-delivery/modal', [
            'url' => $this->url,
            'deliveries' => $this->deliveries
        ]);
    }
}
