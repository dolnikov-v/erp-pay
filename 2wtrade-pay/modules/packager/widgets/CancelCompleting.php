<?php

namespace app\modules\packager\widgets;

use yii\base\Widget;

/**
 * Class CancelCompleting
 * @package app\modules\packager\widgets
 */
class CancelCompleting extends Widget
{
    /**
     * @var string
     */
    public $url;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('cancel-completing/modal', [
            'url' => $this->url
        ]);
    }
}
