<?php

namespace app\modules\packager\widgets;

use yii\base\Widget;

/**
 * Class ConfirmCompleting
 * @package app\modules\packager\widgets
 */
class ConfirmCompleting extends Widget
{
    /**
     * @var string
     */
    public $url;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('confirm-completing/modal', [
            'url' => $this->url
        ]);
    }
}
