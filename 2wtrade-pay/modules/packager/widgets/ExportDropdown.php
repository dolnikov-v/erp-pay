<?php
namespace app\modules\packager\widgets;

use yii\base\Widget;

/**
 * Class ExportDropdown
 * @package app\modules\packager\widgets
 */
class ExportDropdown extends Widget
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('export-dropdown');
    }
}
