<?php

namespace app\modules\packager\widgets;


use app\widgets\Widget;

/**
 * Class EmailList
 * @package app\modules\packager\widgets
 */
class EmailList extends Widget
{
    /**
     * @var string[]
     */
    public $emailList;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('email-list/index', ['emailList' => $this->emailList]);
    }
}
