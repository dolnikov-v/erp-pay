<?php

namespace app\modules\packager\widgets;

use yii\base\Widget;

/**
 * Class CancelShipment
 * @package app\modules\packager\widgets
 */
class CancelShipment extends Widget
{
    /**
     * @var string
     */
    public $url;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('cancel-shipment/modal', [
            'url' => $this->url
        ]);
    }
}
