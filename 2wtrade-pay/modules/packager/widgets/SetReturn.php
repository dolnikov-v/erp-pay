<?php

namespace app\modules\packager\widgets;

use yii\base\Widget;

/**
 * Class SetReturn
 * @package app\modules\packager\widgets
 */
class SetReturn extends Widget
{
    /**
     * @var string
     */
    public $url;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('set-return/modal', [
            'url' => $this->url
        ]);
    }
}
