<?php

namespace app\modules\packager\widgets;

use app\modules\packager\models\search\OrderPackagingSearch;
use yii\base\Widget;

/**
 * Class GetManifest
 * @package app\modules\packager\widgets
 */
class GetManifest extends Widget
{
    /**
     * @var string
     */
    public $url;

    /**
     * @var OrderPackagingSearch
     */
    public $modelSearch;

    /**
     * @var integer
     */
    public $totalCount;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('get-manifest/modal', [
            'url' => $this->url,
            'modelSearch' => $this->modelSearch,
            'totalCount' => $this->totalCount,
        ]);
    }
}
