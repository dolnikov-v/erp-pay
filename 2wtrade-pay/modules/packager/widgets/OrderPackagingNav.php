<?php

namespace app\modules\packager\widgets;

use app\modules\packager\models\OrderPackaging;
use app\modules\packager\models\search\OrderApproveSearch;
use app\modules\packager\models\search\OrderPackagingSearch;
use app\widgets\Nav;
use Yii;
use yii\helpers\Url;

/**
 * Class OrderPackagingNav
 * @package app\modules\packager\widgets
 */
class OrderPackagingNav extends Nav
{

    /**
     * @var OrderPackagingSearch|OrderApproveSearch
     */
    public $modelSearch;

    public function init()
    {
        $actionId = Yii::$app->controller->action->id;

        $this->tabs = [];

        $this->tabs[] = [
            'label' => Yii::t('common', 'Одобрено'),
            'url' => Url::toRoute(['approve', $this->modelSearch->formName() => [
                'date_from' => $this->modelSearch->date_from,
                'date_to' => $this->modelSearch->date_to,
            ]]),
            'can' => Yii::$app->user->can('packager.request.approve'),
            'active' => $actionId == 'approve'
        ];

        foreach (OrderPackaging::getStatuses() as $statusId => $statusName) {
            $this->tabs[] = [
                'label' => $statusName,
                'url' => Url::toRoute(['index', $this->modelSearch->formName() => [
                    'status' => $statusId,
                    'date_from' => $this->modelSearch->date_from,
                    'date_create_to' => $this->modelSearch->date_to,
                    'delivery_id' => $this->modelSearch->delivery_id,
                    'order_id' => $this->modelSearch->order_id,
                ]]),
                'can' => Yii::$app->user->can('packager.request.index'),
                'active' => $actionId == 'index' && $this->modelSearch->status == $statusId
            ];
        }
    }

    /**
     * @return string
     */
    public function renderTabs()
    {
        return $this->render('order-packaging-nav', [
            'id' => $this->id,
            'typeNav' => $this->typeNav,
            'typeTabs' => $this->typeTabs,
            'tabs' => $this->tabs,
        ]);
    }

    /**
     * @return string
     */
    public function renderContent()
    {
        return "";
    }
}
