<?php

namespace app\modules\packager\widgets;

use yii\base\Widget;

/**
 * Class RefusalCompleting
 * @package app\modules\packager\widgets
 */
class RefusalCompleting extends Widget
{
    /**
     * @var string
     */
    public $url;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('refusal-completing/modal', [
            'url' => $this->url
        ]);
    }
}
