<?php

namespace app\modules\packager\widgets;

use app\modules\packager\models\search\OrderPackagingSearch;
use yii\base\Widget;

/**
 * Class LabelPrinting
 * @package app\modules\packager\widgets
 */
class LabelPrinting extends Widget
{
    /**
     * @var string
     */
    public $url;

    /**
     * @var OrderPackagingSearch
     */
    public $modelSearch;

    /**
     * @var integer
     */
    public $totalCount;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('label-printing/modal', [
            'url' => $this->url,
            'modelSearch' => $this->modelSearch,
            'totalCount' => $this->totalCount,
        ]);
    }
}
