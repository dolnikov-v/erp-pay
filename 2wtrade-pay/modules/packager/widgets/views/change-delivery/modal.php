<?php

use app\widgets\Modal;

/** @var string $url */
/** @var array $deliveries */
?>

<?= Modal::widget([
    'id' => 'modal_change_delivery',
    'title' => Yii::t('common', 'Сменить доставку'),
    'body' => $this->render('_modal-body', [
        'url' => $url,
        'deliveries' => $deliveries
    ]),
]) ?>


