<?php
use app\widgets\Modal;
/** @var string $url */
/** @var \app\modules\packager\models\search\OrderPackagingSearch $modelSearch */
?>

<?= Modal::widget([
    'id' => 'modal_list_generation',
    'title' => Yii::t('common', 'Генерация листа'),
    'body' => $this->render('_modal-body', [
        'url' => $url,
        'modelSearch' => $modelSearch
    ]),
]) ?>


