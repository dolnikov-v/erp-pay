<?php

use app\components\widgets\ActiveForm;
use app\widgets\Button;
use app\widgets\ProgressBar;
use app\widgets\InputText;
use yii\helpers\Html;


/** @var string $url */
/** @var \app\modules\packager\models\search\OrderPackagingSearch $modelSearch */
/** @var integer $totalCount */
?>

<?php $form = ActiveForm::begin([
    'enableClientValidation' => false,
    'action' => $url,
    'method' => 'post',
    'options' => [
        'data-sender-url' => $url,
        'target' => '_blank',
    ]
]); ?>
<?= Html::hiddenInput('delivery_id', $modelSearch->delivery_id) ?>
<?= Html::hiddenInput('date_from', $modelSearch->date_from) ?>
<?= Html::hiddenInput('date_to', $modelSearch->date_to) ?>


<div class="margin-bottom-20">
    <p><?= Yii::t('common', 'Получить манифест по API службы доставки') ?></p>
</div>

<div class="row-with-text-stop" style="display: none;">
    <h5>
        <?= Yii::t('common', 'Получить манифест') ?>
        <span class="pull-right sender-percent">0%</span>
    </h5>
    <?= ProgressBar::widget() ?>
</div>

<div class="padding-top-20">
    <div class="row-with-btn-start text-right">
        <?= Button::widget([
            'id' => 'start_get_manifest',
            'label' => Yii::t('common', 'Применить'),
            'style' => Button::STYLE_SUCCESS,
            'size' => Button::SIZE_SMALL,
        ]) ?>

        <?= Button::widget([
            'label' => Yii::t('common', 'Закрыть'),
            'size' => Button::SIZE_SMALL,
            'attributes' => [
                'data-dismiss' => 'modal',
            ],
        ]) ?>
    </div>

    <div class="row-with-btn-stop" style="display: none;">
        <?= Button::widget([
            'id' => 'stop_get_manifest',
            'label' => Yii::t('common', 'Остановить'),
            'style' => Button::STYLE_DANGER . ' btn-block',
            'size' => Button::SIZE_SMALL,
        ]) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
