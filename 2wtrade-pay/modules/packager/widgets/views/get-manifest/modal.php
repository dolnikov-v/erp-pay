<?php
use app\widgets\Modal;
/** @var string $url */
/** @var \app\modules\packager\models\search\OrderPackagingSearch $modelSearch */
/** @var integer $totalCount */
?>

<?= Modal::widget([
    'id' => 'modal_get_manifest',
    'title' => Yii::t('common', 'Манифест'),
    'body' => $this->render('_modal-body', [
        'url' => $url,
        'modelSearch' => $modelSearch,
        'totalCount' => $totalCount,
    ]),
]) ?>


