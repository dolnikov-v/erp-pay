<?php

use app\widgets\Modal;

/** @var string $url */
/** @var array $deliveries */
?>

<?= Modal::widget([
    'id' => 'modal_sender_in_delivery',
    'title' => Yii::t('common', 'Назначить доставку'),
    'body' => $this->render('_modal-body', [
        'url' => $url,
        'deliveries' => $deliveries
    ]),
]) ?>


