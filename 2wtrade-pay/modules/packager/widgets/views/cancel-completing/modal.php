<?php
use app\widgets\Modal;
/** @var string $url */
?>

<?= Modal::widget([
    'id' => 'modal_cancel_completing',
    'title' => Yii::t('common', 'Отмена комплектовки'),
    'body' => $this->render('_modal-body', [
        'url' => $url
    ]),
]) ?>


