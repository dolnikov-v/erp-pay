<?php

use app\components\widgets\ActiveForm;
use app\widgets\Button;
use app\widgets\ProgressBar;
use app\widgets\InputText;
use yii\helpers\Html;
use app\widgets\custom\Checkbox;

/** @var string $url */
/** @var \app\modules\packager\models\search\OrderPackagingSearch $modelSearch */
/** @var integer $totalCount */
?>

<?php $form = ActiveForm::begin([
    'enableClientValidation' => false,
    'action' => $url,
    'method' => 'post',
    'options' => [
        'data-sender-url' => $url,
        'target' => '_blank',
    ]
]); ?>
<?= Html::hiddenInput('delivery_id', $modelSearch->delivery_id) ?>
<?= Html::hiddenInput('date_from', $modelSearch->date_from) ?>
<?= Html::hiddenInput('date_to', $modelSearch->date_to) ?>

<div class="margin-bottom-20">
    <p><?= Yii::t('common', 'После печати заявки перейдут в статус Комплектуется') ?></p>
</div>

<div class="text-center" id="all_label_printing_warning" style="display: none">
    <h3><?= Yii::t('common', 'Распечатать {number} этикеток, вы уверены?', ['number' => $totalCount]) ?></h3>
</div>

<div class="row-with-text-start">
    <div class="row">
        <div class="col-xs-5 text-right">
            <?= Yii::t('common', 'Количество заявок') ?>
        </div>
        <div class="col-xs-5">
            <?= InputText::widget([
                'name' => 'limit',
            ]) ?>
        </div>
        <div class="col-xs-2"></div>
    </div>

    <div class="row text-center">
        <div class="col-xs-2"></div>
        <div class="col-xs-8">
            <?= Checkbox::widget([
                'label' => Yii::t('common', 'Печатать накладные'),
                'name' => 'print_invoice',
                'value' => 1
            ]) ?>
        </div>
        <div class="col-xs-2"></div>
    </div>
</div>

<div class="row-with-text-stop" style="display: none;">
    <h5>
        <?= Yii::t('common', 'Печать этикеток и накладных') ?>
        <span class="pull-right sender-percent">0%</span>
    </h5>
    <?= ProgressBar::widget() ?>
</div>

<div class="padding-top-20">
    <div class="row-with-btn-start text-right">
        <?= Button::widget([
            'id' => 'start_label_printing',
            'label' => Yii::t('common', 'Применить'),
            'style' => Button::STYLE_SUCCESS,
            'size' => Button::SIZE_SMALL,
        ]) ?>

        <?= Button::widget([
            'label' => Yii::t('common', 'Закрыть'),
            'size' => Button::SIZE_SMALL,
            'attributes' => [
                'data-dismiss' => 'modal',
            ],
        ]) ?>
    </div>

    <div class="row-with-btn-stop" style="display: none;">
        <?= Button::widget([
            'id' => 'stop_label_printing',
            'label' => Yii::t('common', 'Остановить'),
            'style' => Button::STYLE_DANGER . ' btn-block',
            'size' => Button::SIZE_SMALL,
        ]) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
