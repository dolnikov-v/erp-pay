<?php
use app\widgets\Modal;
/** @var string $url */
/** @var \app\modules\packager\models\search\OrderPackagingSearch $modelSearch */
/** @var integer $totalCount */
?>

<?= Modal::widget([
    'id' => 'modal_label_printing',
    'title' => Yii::t('common', 'Печать этикеток и накладных'),
    'body' => $this->render('_modal-body', [
        'url' => $url,
        'modelSearch' => $modelSearch,
        'totalCount' => $totalCount,
    ]),
]) ?>


