<?php
use app\widgets\Modal;
/** @var string $url */
?>

<?= Modal::widget([
    'id' => 'modal_confirm_completing',
    'title' => Yii::t('common', 'Подтвердить комплектовку'),
    'body' => $this->render('_modal-body', [
        'url' => $url
    ]),
]) ?>


