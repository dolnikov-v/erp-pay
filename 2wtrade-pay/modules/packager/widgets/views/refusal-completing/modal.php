<?php
use app\widgets\Modal;
/** @var string $url */
?>

<?= Modal::widget([
    'id' => 'modal_refusal_completing',
    'title' => Yii::t('common', 'Отказ от комплектовки'),
    'body' => $this->render('_modal-body', [
        'url' => $url
    ]),
]) ?>


