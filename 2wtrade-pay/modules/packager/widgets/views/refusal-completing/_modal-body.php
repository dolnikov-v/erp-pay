<?php

use app\components\widgets\ActiveForm;
use app\widgets\Button;
use app\widgets\ProgressBar;
use yii\helpers\Url;
use yii\helpers\Html;

/** @var string $url */
?>

<?php $form = ActiveForm::begin([
    'enableClientValidation' => false,
    'action' => Url::toRoute('index'),
    'method' => 'post',
    'options' => [
        'data-sender-url' => $url,
    ]
]); ?>

<div class="row-with-text-start">
    <div class="text-center">
        <?= Yii::t('common', 'Действие с выбранными заявками') ?>
    </div>
</div>

<div class="row">
    <div class="col-xs-2"></div>
    <div class="col-xs-8">
        <?= Html::textarea('reason', '', [
            'id' => 'reason',
            'placeholder' => Yii::t('common', 'Причина отказа'),
            'style' => 'width: 100%;'
        ]) ?>
    </div>
    <div class="col-xs-2"></div>
</div>

<div class="row-with-text-stop" style="display: none;">
    <h5>
        <?= Yii::t('common', 'Отказ от комплектовки') ?>
        <span class="pull-right sender-percent">0%</span>
    </h5>
    <?= ProgressBar::widget() ?>
</div>

<div class="padding-top-20">
    <div class="row-with-btn-start text-right">
        <?= Button::widget([
            'id' => 'start_refusal_completing',
            'label' => Yii::t('common', 'Применить'),
            'style' => Button::STYLE_SUCCESS,
            'size' => Button::SIZE_SMALL,
        ]) ?>

        <?= Button::widget([
            'label' => Yii::t('common', 'Закрыть'),
            'size' => Button::SIZE_SMALL,
            'attributes' => [
                'data-dismiss' => 'modal',
            ],
        ]) ?>
    </div>

    <div class="row-with-btn-stop" style="display: none;">
        <?= Button::widget([
            'id' => 'stop_refusal_completing',
            'label' => Yii::t('common', 'Остановить'),
            'style' => Button::STYLE_DANGER . ' btn-block',
            'size' => Button::SIZE_SMALL,
        ]) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
