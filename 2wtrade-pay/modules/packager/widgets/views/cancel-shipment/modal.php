<?php
use app\widgets\Modal;
/** @var string $url */
?>

<?= Modal::widget([
    'id' => 'modal_cancel_shipment',
    'title' => Yii::t('common', 'Отмена отгрузки'),
    'body' => $this->render('_modal-body', [
        'url' => $url
    ]),
]) ?>


