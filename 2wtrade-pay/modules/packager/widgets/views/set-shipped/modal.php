<?php
use app\widgets\Modal;
/** @var string $url */
?>

<?= Modal::widget([
    'id' => 'modal_set_shipped',
    'title' => Yii::t('common', 'Отгружено'),
    'body' => $this->render('_modal-body', [
        'url' => $url
    ]),
]) ?>


