<?php
use app\widgets\Modal;
/** @var string $url */
?>

<?= Modal::widget([
    'id' => 'modal_set_approve',
    'title' => Yii::t('common', 'Вернуть в Одобрено'),
    'body' => $this->render('_modal-body', [
        'url' => $url
    ]),
]) ?>


