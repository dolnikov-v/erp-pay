<?php
use app\widgets\Modal;
/** @var string $url */
?>

<?= Modal::widget([
    'id' => 'modal_set_return',
    'title' => Yii::t('common', 'Возврат'),
    'body' => $this->render('_modal-body', [
        'url' => $url
    ]),
]) ?>


