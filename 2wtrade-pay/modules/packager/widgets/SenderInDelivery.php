<?php

namespace app\modules\packager\widgets;

use yii\base\Widget;

/**
 * Class SenderInDelivery
 * @package app\modules\packager\widgets
 */
class SenderInDelivery extends Widget
{
    /**
     * @var string
     */
    public $url;

    /**
     * @var array
     */
    public $deliveries;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('sender-in-delivery/modal', [
            'url' => $this->url,
            'deliveries' => $this->deliveries
        ]);


    }
}
