<?php

namespace app\modules\packager\widgets;

use yii\base\Widget;

/**
 * Class SetApprove
 * @package app\modules\packager\widgets
 */
class SetApprove extends Widget
{
    /**
     * @var string
     */
    public $url;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('set-approve/modal', [
            'url' => $this->url
        ]);
    }
}
