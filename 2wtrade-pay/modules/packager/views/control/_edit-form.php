<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;
use app\modules\packager\widgets\EmailList;
use app\modules\packager\assets\EmailListAsset;

/** @var \app\modules\packager\models\Packager $model */

EmailListAsset::register($this);
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute(['edit', 'id' => $model->id]),
    'enableClientValidation' => true
]); ?>

<div class="row">
    <div class="col-lg-4">
        <?= $form->field($model, 'name')->textInput() ?>
    </div>
</div>

<div class="row margin-bottom-5">
    <div class="col-lg-12">
        <?= Yii::t('common', 'Список e-mail для отправки этикеток') ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <?= EmailList::widget(['emailList' => $model->getEmailList()]) ?>
    </div>
</div>



<div class="row margin-top-20">
    <div class="col-lg-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить службу упаковки') : Yii::t('common',
            'Сохранить службу упаковки')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
