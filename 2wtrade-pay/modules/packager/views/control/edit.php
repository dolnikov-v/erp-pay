<?php
use app\widgets\Panel;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \app\modules\packager\models\Packager $model */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление службы упаковки') : Yii::t('common', 'Редактирование службы упаковки');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Службы упаковки'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Управление'), 'url' => Url::toRoute('/packager/control/index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Служба упаковки'),
    'alert' => $model->isNewRecord ? Yii::t('common', 'Служба упаковки будет добавлена и привязана к активной стране - {country}.', [
        'country' => Yii::$app->user->country->name,
    ]) : '',
    'content' => $this->render('_edit-form', [
        'model' => $model,
    ])
]) ?>