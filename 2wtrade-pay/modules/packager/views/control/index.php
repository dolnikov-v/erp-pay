<?php
use app\components\grid\ActionColumn;
use app\components\grid\DateColumn;
use app\helpers\DataProvider;
use app\widgets\assets\custom\ModalConfirmDeleteAsset;
use app\widgets\ButtonLink;
use app\widgets\custom\ModalConfirmDelete;
use app\widgets\Label;
use app\widgets\Panel;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use app\components\grid\SortableGridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

ModalConfirmDeleteAsset::register($this);

$this->title = Yii::t('common', 'Управление');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Службы упаковки'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица со службами упаковки'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => SortableGridView::widget([
        'summary' => false,
        'dataProvider' => $dataProvider,
        'rowOptions' => function ($model) {
            return [
                'class' => 'tr-vertical-align-middle ' . ($model->active ? '' : 'text-muted'),
            ];
        },
        'columns' => [
            [
                'attribute' => 'id',
                'headerOptions' => ['class' => 'width-50 text-center'],
                'content' => function ($model) {
                    return $model->id;
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'name',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'active',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return Label::widget([
                        'label' => $model->active ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->active ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'email',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'auto_sending',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return Label::widget([
                        'label' => $model->auto_sending ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->auto_sending ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'created_at',
                'enableSorting' => false,
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/packager/control/edit', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('packager.control.edit');
                        }
                    ],

                    [
                        'label' => Yii::t('common', 'Активировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/packager/control/activate', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('packager.control.activate') && !$model->active;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Деактивировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/packager/control/deactivate', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('packager.control.deactivate') && $model->active;
                        }
                    ],

                    [
                        'label' => Yii::t('common', 'Разрешить автоматическую отправку'),
                        'url' => function ($model) {
                            return Url::toRoute(['/packager/control/activate-auto-sending', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('packager.control.activateautosending') && !$model->auto_sending;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Запретить автоматическую отправку'),
                        'url' => function ($model) {
                            return Url::toRoute(['/packager/control/deactivate-auto-sending', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('packager.control.deactivateautosending') && $model->auto_sending;
                        }
                    ],

                    [
                        'can' => function ($model) {
                            return (Yii::$app->user->can('packager.control.edit')
                                    || (Yii::$app->user->can('packager.control.activate') && !$model->active)
                                    || (Yii::$app->user->can('packager.control.deactivate') && $model->active)
                                    || (Yii::$app->user->can('packager.control.activateautosending') && !$model->auto_sending)
                                    || (Yii::$app->user->can('packager.control.deactivateautosending') && $model->auto_sending)
                                )
                                || Yii::$app->user->can('packager.control.delete');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить'),
                        'url' => function () {
                            return '#';
                        },
                        'style' => 'confirm-delete-link',
                        'attributes' => function ($model) {
                            return [
                                'data-href' => Url::toRoute(['/packager/control/delete', 'id' => $model->id]),
                            ];
                        },
                        'can' => function () {
                            return Yii::$app->user->can('packager.control.delete');
                        }
                    ],
                ]
            ]
        ],
    ]),
    'footer' => (Yii::$app->user->can('packager.control.edit') ?
            ButtonLink::widget([
                'label' => Yii::t('common', 'Добавить службу упаковки'),
                'url' => Url::toRoute('/packager/control/edit'),
                'style' => ButtonLink::STYLE_SUCCESS,
                'size' => ButtonLink::SIZE_SMALL,
            ]) : '') . LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>

<?= ModalConfirmDelete::widget() ?>
