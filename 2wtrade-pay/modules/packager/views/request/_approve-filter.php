<?php
use app\components\widgets\ActiveForm;
use app\modules\packager\models\OrderPackaging;
use app\modules\packager\widgets\ExportDropdown;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var app\modules\packager\models\search\OrderPackagingSearch|\app\modules\packager\models\search\OrderApproveSearch $modelSearch */
/** @var array $deliveriesCollection */
?>

<?php $form = ActiveForm::begin([
    'action' => ['approve'],
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>

<?= $modelSearch->getDateFilter()->restore($form) ?>
<div class="row">
    <div class="col-lg-4">
        <div class="form-group">
            <?= $form->field($modelSearch, 'delivery_id')->select2List($deliveriesCollection, ['prompt' => '—']) ?>
        </div>
    </div>

    <div class="col-lg-4">
        <div class="form-group">
            <?= $form->field($modelSearch, 'order_id')->textInput()->label(Yii::t('common', 'Номер заказа')) ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <?= $form->submit(Yii::t('common', 'Применить')) ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('approve')) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>