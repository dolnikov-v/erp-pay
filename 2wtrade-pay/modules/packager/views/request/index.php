<?php

use app\components\grid\ActionColumn;
use app\components\grid\CustomCheckboxColumn;
use app\components\grid\DateColumn;
use app\components\grid\GridView;
use app\helpers\DataProvider;
use app\modules\packager\assets\CancelCompletingAsset;
use app\modules\packager\assets\CancelShipmentAsset;
use app\modules\packager\assets\ConfirmCompletingAsset;
use app\modules\packager\assets\LabelPrintingAsset;
use app\modules\packager\assets\ListGenerationAsset;
use app\modules\packager\assets\RefusalCompletingAsset;
use app\modules\packager\assets\RequestAsset;
use app\modules\packager\assets\SetReturnAsset;
use app\modules\packager\assets\SetShippedAsset;
use app\modules\packager\assets\ChangeDeliveryAsset;
use app\modules\packager\assets\SetApproveAsset;
use app\modules\packager\assets\GetManifestAsset;
use app\modules\packager\models\OrderPackaging;
use app\modules\packager\widgets\CancelCompleting as CancelCompletingWidget;
use app\modules\packager\widgets\CancelShipment as CancelShipmentWidget;
use app\modules\packager\widgets\ConfirmCompleting as ConfirmCompletingWidget;
use app\modules\packager\widgets\LabelPrinting as LabelPrintingWidget;
use app\modules\packager\widgets\ListGeneration as ListGenerationWidget;
use app\modules\packager\widgets\RefusalCompleting as RefusalCompletingWidget;
use app\modules\packager\widgets\SetReturn as SetReturnWidget;
use app\modules\packager\widgets\SetShipped as SetShippedWidget;
use app\modules\packager\widgets\ChangeDelivery as ChangeDeliveryWidget;
use app\modules\packager\widgets\SetApprove as SetApproveWidget;
use app\modules\packager\widgets\GetManifest as GetManifestWidget;
use app\widgets\custom\Checkbox;
use app\widgets\Label;
use app\widgets\LinkPager;
use app\widgets\Panel;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;


/** @var yii\web\View $this */
/** @var \app\modules\packager\models\search\OrderPackagingSearch $modelSearch */
/** @var yii\data\ActiveDataProvider $dataProvider */
/** @var array $deliveriesCollection */

RequestAsset::register($this);
LabelPrintingAsset::register($this);
ConfirmCompletingAsset::register($this);
CancelCompletingAsset::register($this);
RefusalCompletingAsset::register($this);
ListGenerationAsset::register($this);
CancelShipmentAsset::register($this);
SetShippedAsset::register($this);
SetReturnAsset::register($this);
ChangeDeliveryAsset::register($this);
SetApproveAsset::register($this);
GetManifestAsset::register($this);

$this->registerJs("var I18n = {};", View::POS_HEAD);
$this->registerJs("I18n['Да'] = '" . Yii::t('common', 'Да') . "';", View::POS_HEAD);
$this->registerJs("I18n['Не удалось выполнить операцию'] = '" . Yii::t('common', 'Не удалось выполнить операцию') . "';", View::POS_HEAD);
$this->registerJs("I18n['Операция выполнена успешно'] = '" . Yii::t('common', 'Операция выполнена успешно') . "';", View::POS_HEAD);
$this->registerJs("I18n['Необходимо выбрать заявки'] = '" . Yii::t('common', 'Необходимо выбрать заявки') . "';", View::POS_HEAD);
$this->registerJs("I18n['Введите причину отказа'] = '" . Yii::t('common', 'Введите причину отказа') . "';", View::POS_HEAD);

$this->title = Yii::t('common', 'Заявки');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Службы упаковки'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'nav' => new \app\modules\packager\widgets\OrderPackagingNav([
        'modelSearch' => $modelSearch,
    ]),
    'id' => 'panel1',
    'content' => $this->render('_index-filter', [
        'modelSearch' => $modelSearch,
        'deliveriesCollection' => $deliveriesCollection,
    ]),
]);
?>

<?= $this->render('_nav-tab-operations', [
    'modelSearch' => $modelSearch,
    'deliveriesCollection' => $deliveriesCollection,
]);
?>

<?= Panel::widget([
    'id' => 'panel3',
    'title' => Yii::t('common', 'Таблица с заявками'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => [
            'id' => 'packager-request-grid',
            'class' => 'table table-striped table-hover table-sortable',
        ],
        'columns' => [
            [
                'class' => CustomCheckboxColumn::className(),
                'content' => function ($model) {
                    return Checkbox::widget([
                        'name' => 'id[]',
                        'value' => $model->id,
                        'style' => 'checkbox-inline',
                        'label' => true,
                    ]);
                },
            ],
            [
                'attribute' => 'id',
                'label' => '#',
                'headerOptions' => ['class' => 'th-custom-record-id text-center'],
                'contentOptions' => ['class' => 'td-custom-record-id text-center'],
            ],
            [
                'attribute' => 'order_id',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    if (Yii::$app->user->can('order.index.view')) {
                        $text = Html::a($model->order_id, Url::toRoute([
                            '/order/index/view',
                            'id' => $model->order_id,
                        ]), [
                            'target' => '_blank',
                        ]);
                    } else {
                        $text = $model->order_id;
                    }
                    return $text;
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'order_customer_address',
                'label' => Yii::t('common', 'Адрес'),
                'content' => function ($model) {
                    /** @var OrderPackaging $model */
                    $tmp = [];
                    if ($model->order->customer_zip) $tmp[] = $model->order->customer_zip;
                    if ($model->order->customer_city) $tmp[] = $model->order->customer_city;
                    if ($model->order->customer_province) $tmp[] = $model->order->customer_province;
                    if ($model->order->customer_district) $tmp[] = $model->order->customer_district;
                    if ($model->order->customer_subdistrict) $tmp[] = $model->order->customer_subdistrict;
                    if ($model->order->customer_address) $tmp[] = $model->order->customer_address;
                    if ($model->order->customer_address_additional) $tmp[] = $model->order->customer_address_additional;
                    return implode('<br />', $tmp);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'order.callCenterRequest.comment',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'order.deliveryRequest.tracking',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'order_products',
                'label' => Yii::t('common', 'Товары'),
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    /** @var OrderPackaging $model */
                    $tmp = [];
                    foreach ($model->order->orderProducts as $orderProduct) {
                        $tmp[] = Label::widget([
                            'label' => $orderProduct->product->name . ' - ' . $orderProduct->quantity,
                            'style' => Label::STYLE_INFO,
                        ]);
                    }
                    return implode('<br />', $tmp);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'order.price_total',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'enableSorting' => false,
            ],
            [
                'attribute' => 'order.delivery',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'enableSorting' => false,
            ],
            [
                'attribute' => 'delivery.name',
                'label' => Yii::t('common', 'Служба доставки'),
                'enableSorting' => false,
            ],
            [
                'attribute' => 'order.delivery_time_from',
                'class' => DateColumn::className(),
                'content' => function ($model) {
                    /** @var OrderPackaging $model */
                    return $model->order->delivery_time_from ? $model->order->delivery_time_from : '—';
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'order.callCenterRequest.approved_at',
                'class' => DateColumn::className(),
                'content' => function ($model) {
                    /** @var OrderPackaging $model */
                    if ($model->order->callCenterRequest) {
                        return $model->order->callCenterRequest->approved_at ? $model->order->callCenterRequest->approved_at : '—';
                    } else {
                        return '—';
                    }
                },
            ],
            [
                'attribute' => 'order_customer_full_name',
                'label' => Yii::t('common', 'ФИО покупателя'),
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    /** @var OrderPackaging $model */
                    return $model->order ? $model->order->customer_full_name : '';
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'order.customer_phone',
                'content' => function ($model) {
                    $tmp = [];
                    if ($model->order->customer_phone) $tmp[$model->order->customer_phone] = $model->order->customer_phone;
                    if ($model->order->customer_mobile) $tmp[$model->order->customer_mobile] = $model->order->customer_mobile;
                    return implode('<br />', $tmp);
                },
                'enableSorting' => false,
            ],
            'actions' => [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/order/index/edit', 'id' => $model->order_id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('order.index.edit');
                        },
                        'attributes' => function () {
                            return [
                                'target' => '_blank',
                            ];
                        },
                    ],
                ]
            ],
        ],
    ]),
    'footer' => LinkPager::widget([
        'pagination' => $dataProvider->getPagination(),
        'counterPosition' => LinkPager::COUNTER_POSITION_RIGHT,
    ]),
]) ?>

<?php if (Yii::$app->user->can('packager.request.labelprinting')): ?>
    <?= LabelPrintingWidget::widget([
        'url' => Url::toRoute('label-printing'),
        'modelSearch' => $modelSearch,
        'totalCount' => $dataProvider->totalCount
    ]); ?>
<?php endif; ?>

<?php if (Yii::$app->user->can('packager.request.confirmcompleting')): ?>
    <?= ConfirmCompletingWidget::widget([
        'url' => Url::toRoute('confirm-completing'),
    ]); ?>
<?php endif; ?>

<?php if (Yii::$app->user->can('packager.request.cancelcompleting')): ?>
    <?= CancelCompletingWidget::widget([
        'url' => Url::toRoute('cancel-completing'),
    ]); ?>
<?php endif; ?>

<?php if (Yii::$app->user->can('packager.request.refusalcompleting')): ?>
    <?= RefusalCompletingWidget::widget([
        'url' => Url::toRoute('refusal-completing'),
    ]); ?>
<?php endif; ?>

<?php if (Yii::$app->user->can('packager.request.listgeneration')): ?>
    <?= ListGenerationWidget::widget([
        'url' => Url::toRoute('list-generation'),
        'modelSearch' => $modelSearch
    ]); ?>
<?php endif; ?>

<?php if (Yii::$app->user->can('packager.request.cancelshipment')): ?>
    <?= CancelShipmentWidget::widget([
        'url' => Url::toRoute('cancel-shipment'),
    ]); ?>
<?php endif; ?>

<?php if (Yii::$app->user->can('packager.request.setshipped')): ?>
    <?= SetShippedWidget::widget([
        'url' => Url::toRoute('set-shipped'),
    ]); ?>
<?php endif; ?>

<?php if (Yii::$app->user->can('packager.request.setreturn')): ?>
    <?= SetReturnWidget::widget([
        'url' => Url::toRoute('set-return'),
    ]); ?>
<?php endif; ?>

<?php if (Yii::$app->user->can('packager.request.changedelivery')): ?>
    <?= ChangeDeliveryWidget::widget([
        'url' => Url::toRoute('change-delivery'),
        'deliveries' => $deliveriesCollection
    ]); ?>
<?php endif; ?>

<?php if (Yii::$app->user->can('packager.request.setapprove')): ?>
    <?= SetApproveWidget::widget([
        'url' => Url::toRoute('set-approve'),
    ]); ?>
<?php endif; ?>

<?php if (Yii::$app->user->can('packager.request.getmanifest')): ?>
    <?= GetManifestWidget::widget([
        'url' => Url::toRoute('get-manifest'),
        'modelSearch' => $modelSearch,
        'totalCount' => $dataProvider->totalCount
    ]); ?>
<?php endif; ?>
