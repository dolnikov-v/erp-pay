<?php

use app\components\grid\ActionColumn;
use app\components\grid\CustomCheckboxColumn;
use app\components\grid\DateColumn;
use app\components\grid\GridView;
use app\helpers\DataProvider;
use app\modules\order\models\Order;
use app\modules\packager\assets\SenderInDeliveryAsset;
use app\modules\packager\assets\RequestAsset;
use app\modules\packager\widgets\SenderInDelivery as SenderInDeliveryWidget;
use app\widgets\custom\Checkbox;
use app\widgets\Label;
use app\widgets\LinkPager;
use app\widgets\Panel;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;


/** @var yii\web\View $this */
/** @var \app\modules\packager\models\search\OrderPackagingSearch $modelSearch */
/** @var yii\data\ActiveDataProvider $dataProvider */
/** @var array $deliveriesCollection */

RequestAsset::register($this);
SenderInDeliveryAsset::register($this);

$this->registerJs("var I18n = {};", View::POS_HEAD);
$this->registerJs("I18n['Да'] = '" . Yii::t('common', 'Да') . "';", View::POS_HEAD);
$this->registerJs("I18n['Не удалось выполнить операцию'] = '" . Yii::t('common', 'Не удалось выполнить операцию') . "';", View::POS_HEAD);
$this->registerJs("I18n['Операция выполнена успешно'] = '" . Yii::t('common', 'Операция выполнена успешно') . "';", View::POS_HEAD);
$this->registerJs("I18n['Необходимо выбрать заявки'] = '" . Yii::t('common', 'Необходимо выбрать заявки') . "';", View::POS_HEAD);

$this->title = Yii::t('common', 'Заявки');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Службы упаковки'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'nav' => new \app\modules\packager\widgets\OrderPackagingNav([
        'modelSearch' => $modelSearch,
    ]),
    'id' => 'panel1',
    'content' => $this->render('_approve-filter', [
        'modelSearch' => $modelSearch,
        'deliveriesCollection' => $deliveriesCollection,
    ]),
]);
?>

<?= $this->render('_nav-tab-operations', [
    'modelSearch' => $modelSearch,
    'deliveriesCollection' => $deliveriesCollection,
]);
?>

<?= Panel::widget([
    'id' => 'panel3',
    'title' => Yii::t('common', 'Одобренные заказы'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => [
            'id' => 'packager-request-grid',
            'class' => 'table table-striped table-hover table-sortable',
        ],
        'columns' => [
            [
                'class' => CustomCheckboxColumn::className(),
                'content' => function ($model) {
                    return Checkbox::widget([
                        'name' => 'id[]',
                        'value' => $model->id,
                        'style' => 'checkbox-inline',
                        'label' => true,
                    ]);
                },
            ],
            [
                'attribute' => 'id',
                'label' => '#',
                'headerOptions' => ['class' => 'th-custom-record-id text-center'],
                'contentOptions' => ['class' => 'td-custom-record-id text-center'],
                'content' => function ($model) {
                    if (Yii::$app->user->can('order.index.view')) {
                        $text = Html::a($model->id, Url::toRoute([
                            '/order/index/view',
                            'id' => $model->id,
                        ]), [
                            'target' => '_blank',
                        ]);
                    } else {
                        $text = $model->id;
                    }
                    return $text;
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'order_customer_address',
                'label' => Yii::t('common', 'Адрес'),
                'content' => function ($model) {
                    /** @var Order $model */
                    $tmp = [];
                    if ($model->customer_zip) $tmp[] = $model->customer_zip;
                    if ($model->customer_city) $tmp[] = $model->customer_city;
                    if ($model->customer_province) $tmp[] = $model->customer_province;
                    if ($model->customer_district) $tmp[] = $model->customer_district;
                    if ($model->customer_subdistrict) $tmp[] = $model->customer_subdistrict;
                    if ($model->customer_address) $tmp[] = $model->customer_address;
                    if ($model->customer_address_additional) $tmp[] = $model->customer_address_additional;
                    return implode('<br />', $tmp);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'callCenterRequest.comment',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'order_products',
                'label' => Yii::t('common', 'Товары'),
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    /** @var Order $model */
                    $tmp = [];
                    foreach ($model->orderProducts as $orderProduct) {
                        $tmp[] = Label::widget([
                            'label' => $orderProduct->product->name . ' - ' . $orderProduct->quantity,
                            'style' => Label::STYLE_INFO,
                        ]);
                    }
                    return implode('<br />', $tmp);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'price_total',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'enableSorting' => false,
            ],
            [
                'attribute' => 'delivery',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'enableSorting' => false,
            ],
            [
                'attribute' => 'pending_delivery_id',
                'label' => Yii::t('common', 'Служба доставки'),
                'content' => function ($model) use ($deliveriesCollection) {
                    if ($model->deliveryRequest && $model->deliveryRequest->delivery_id) {
                        return array_key_exists($model->deliveryRequest->delivery_id,
                            $deliveriesCollection) ? $deliveriesCollection[$model->deliveryRequest->delivery_id] : '—';
                    } elseif ($model->pending_delivery_id) {
                        return array_key_exists($model->pending_delivery_id,
                            $deliveriesCollection) ? ($deliveriesCollection[$model->pending_delivery_id]) . '*' : '—';
                    } else {
                        return '—';
                    }
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'delivery_time_from',
                'class' => DateColumn::className(),
                'content' => function ($model) {
                    /** @var Order $model */
                    return $model->delivery_time_from ? $model->delivery_time_from : '—';
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'callCenterRequest.approved_at',
                'class' => DateColumn::className(),
                'content' => function ($model) {
                    /** @var Order $model */
                    if ($model->callCenterRequest) {
                        return $model->callCenterRequest->approved_at ? $model->callCenterRequest->approved_at : '—';
                    } else {
                        return '—';
                    }
                },
            ],
            [
                'attribute' => 'customer_full_name',
                'label' => Yii::t('common', 'ФИО покупателя'),
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'enableSorting' => false,
            ],
            [
                'attribute' => 'customer_phone',
                'content' => function ($model) {
                    $tmp = [];
                    if ($model->customer_phone)  {
                        $tmp[$model->customer_phone] = $model->customer_phone;
                    }
                    if ($model->customer_mobile) {
                        $tmp[$model->customer_mobile] = $model->customer_mobile;
                    }
                    return implode('<br />', $tmp);
                },
                'enableSorting' => false,
            ],
            'actions' => [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/order/index/edit', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('order.index.edit');
                        },
                        'attributes' => function () {
                            return [
                                'target' => '_blank',
                            ];
                        },
                    ],
                ]
            ],
        ],
    ]),
    'footer' => LinkPager::widget([
        'pagination' => $dataProvider->getPagination(),
        'counterPosition' => LinkPager::COUNTER_POSITION_RIGHT,
    ]),
]) ?>

<?php if (Yii::$app->user->can('packager.request.sendindelivery')): ?>
    <?= SenderInDeliveryWidget::widget([
        'url' => Url::toRoute('send-in-delivery'),
        'deliveries' => $deliveriesCollection
    ]); ?>
<?php endif; ?>