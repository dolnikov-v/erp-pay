<?php

use app\assets\vendor\BootstrapLaddaAsset;
use app\modules\packager\models\OrderPackaging;
use app\widgets\ButtonProgress;

/** @var \app\modules\packager\models\search\OrderPackagingSearch|\app\modules\packager\models\search\OrderApproveSearch $modelSearch */

BootstrapLaddaAsset::register($this);
?>

<div class="row margin-bottom-30">

    <?php if ($modelSearch instanceof \app\modules\packager\models\search\OrderApproveSearch) : ?>
        <?php if (Yii::$app->user->can('packager.request.sendindelivery')): ?>
            <div class="col-lg-3">
                <?= ButtonProgress::widget([
                    'id' => 'btn_sender_in_delivery',
                    'style' => ButtonProgress::STYLE_INFO . ' width-300',
                    'label' => Yii::t('common', 'Назначить службу доставки'),
                ]) ?>
            </div>
        <?php endif; ?>
    <?php endif; ?>

    <?php if ($modelSearch->status === OrderPackaging::STATUS_PENDING) : ?>
        <?php if (Yii::$app->user->can('packager.request.labelprinting')): ?>
            <div class="col-lg-3">
                <?= ButtonProgress::widget([
                    'id' => 'btn_label_printing',
                    'style' => ButtonProgress::STYLE_INFO . ' width-300',
                    'label' => Yii::t('common', 'Печать этикеток и накладных'),
                ]) ?>
            </div>
        <?php endif; ?>
    <?php endif; ?>

    <?php if ($modelSearch->status === OrderPackaging::STATUS_COMPLETING) : ?>
        <?php if (Yii::$app->user->can('packager.request.confirmcompleting')): ?>
            <div class="col-lg-3">
                <?= ButtonProgress::widget([
                    'id' => 'btn_confirm_completing',
                    'style' => ButtonProgress::STYLE_SUCCESS . ' width-300',
                    'label' => Yii::t('common', 'Подтвердить комплектовку'),
                ]) ?>
            </div>
        <?php endif; ?>
    <?php endif; ?>

    <?php if (
        $modelSearch->status === OrderPackaging::STATUS_COMPLETING ||
        $modelSearch->status === OrderPackaging::STATUS_COMPLETED
    ) : ?>
        <?php if (Yii::$app->user->can('packager.request.cancelcompleting')): ?>
            <div class="col-lg-3">
                <?= ButtonProgress::widget([
                    'id' => 'btn_cancel_completing',
                    'style' => ButtonProgress::STYLE_DANGER . ' width-300',
                    'label' => Yii::t('common', 'Отмена комплектовки'),
                ]) ?>
            </div>
        <?php endif; ?>
    <?php endif; ?>

    <?php if (
        $modelSearch->status === OrderPackaging::STATUS_PENDING ||
        $modelSearch->status === OrderPackaging::STATUS_COMPLETING
    ) : ?>
        <?php if (Yii::$app->user->can('packager.request.refusalcompleting')): ?>
            <div class="col-lg-3">
                <?= ButtonProgress::widget([
                    'id' => 'btn_refusal_completing',
                    'style' => ButtonProgress::STYLE_DANGER . ' width-300',
                    'label' => Yii::t('common', 'Отказ от комплектовки'),
                ]) ?>
            </div>
        <?php endif; ?>
    <?php endif; ?>

    <?php if ($modelSearch->status === OrderPackaging::STATUS_COMPLETED) : ?>
        <?php if (Yii::$app->user->can('packager.request.listgeneration')): ?>
            <div class="col-lg-3">
                <?= ButtonProgress::widget([
                    'id' => 'btn_list_generation',
                    'style' => ButtonProgress::STYLE_INFO . ' width-300',
                    'label' => Yii::t('common', 'Генерация листа'),
                ]) ?>
            </div>
        <?php endif; ?>
    <?php endif; ?>

    <?php if ($modelSearch->status === OrderPackaging::STATUS_AWAITING_SHIPMENT) : ?>
        <?php if (Yii::$app->user->can('packager.request.cancelshipment')): ?>
            <div class="col-lg-3">
                <?= ButtonProgress::widget([
                    'id' => 'btn_cancel_shipment',
                    'style' => ButtonProgress::STYLE_DANGER . ' width-300',
                    'label' => Yii::t('common', 'Отмена отгрузки'),
                ]) ?>
            </div>
        <?php endif; ?>
    <?php endif; ?>

    <?php if ($modelSearch->status === OrderPackaging::STATUS_AWAITING_SHIPMENT && $modelSearch->delivery_id && $modelSearch->delivery->hasApiGenerateManifestByOrders()) : ?>
        <?php if (Yii::$app->user->can('packager.request.getmanifest')): ?>
            <div class="col-lg-3">
                <?= ButtonProgress::widget([
                    'id' => 'btn_get_manifest',
                    'style' => ButtonProgress::STYLE_INFO . ' width-300',
                    'label' => Yii::t('common', 'Получить манифест'),
                ]) ?>
            </div>
        <?php endif; ?>
    <?php endif; ?>

    <?php if ($modelSearch->status === OrderPackaging::STATUS_AWAITING_SHIPMENT) : ?>
        <?php if (Yii::$app->user->can('packager.request.setshipped')): ?>
            <div class="col-lg-3">
                <?= ButtonProgress::widget([
                    'id' => 'btn_set_shipped',
                    'style' => ButtonProgress::STYLE_SUCCESS . ' width-300',
                    'label' => Yii::t('common', 'Отгружено'),
                ]) ?>
            </div>
        <?php endif; ?>
    <?php endif; ?>

    <?php if ($modelSearch->status === OrderPackaging::STATUS_DONE) : ?>
        <?php if (Yii::$app->user->can('packager.request.setreturn')): ?>
            <div class="col-lg-3">
                <?= ButtonProgress::widget([
                    'id' => 'btn_set_return',
                    'style' => ButtonProgress::STYLE_DANGER . ' width-300',
                    'label' => Yii::t('common', 'Возврат'),
                ]) ?>
            </div>
        <?php endif; ?>
    <?php endif; ?>

    <?php if ($modelSearch->status === OrderPackaging::STATUS_ERROR) : ?>
        <?php if (Yii::$app->user->can('packager.request.changedelivery')): ?>
            <div class="col-lg-3">
                <?= ButtonProgress::widget([
                    'id' => 'btn_change_delivery',
                    'style' => ButtonProgress::STYLE_DARK . ' width-300',
                    'label' => Yii::t('common', 'Сменить доставку'),
                ]) ?>
            </div>
        <?php endif; ?>

        <?php if (Yii::$app->user->can('packager.request.setapprove')): ?>
            <div class="col-lg-3">
                <?= ButtonProgress::widget([
                    'id' => 'btn_set_approve',
                    'style' => ButtonProgress::STYLE_DARK . ' width-300',
                    'label' => Yii::t('common', 'Вернуть в Одобрено'),
                ]) ?>
            </div>
        <?php endif; ?>
    <?php endif; ?>
</div>
