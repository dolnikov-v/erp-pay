<?php
use app\components\widgets\ActiveForm;
use app\modules\packager\models\OrderPackaging;
use app\modules\packager\widgets\ExportDropdown;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var app\modules\packager\models\search\OrderPackagingSearch $modelSearch */
/** @var array $deliveriesCollection */
?>

<?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>

<div class="row">
    <div class="col-lg-4">
        <div class="form-group">
            <?= $form->field($modelSearch, 'date')->dateRangePicker('date_from', 'date_to') ?>
        </div>
    </div>

    <div class="col-lg-4">
        <div class="form-group">
            <?= $form->field($modelSearch, 'delivery_id')->select2List($deliveriesCollection) ?>
        </div>
    </div>

</div>
<div class="row">
    <div class="col-lg-4">
        <div class="form-group">
            <?= $form->field($modelSearch, 'status')->select2List(OrderPackaging::getStatuses()) ?>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="form-group">
            <?= $form->field($modelSearch, 'order_id')->textInput() ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <?= $form->submit(Yii::t('common', 'Применить')) ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')) ?>
    </div>

    <div class="pull-right">
        <?= ExportDropdown::widget() ?>
    </div>
</div>

<?php ActiveForm::end(); ?>