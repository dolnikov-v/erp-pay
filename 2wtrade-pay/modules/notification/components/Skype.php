<?php

namespace app\modules\notification\components;

use yii\base\Component;
use yii\httpclient\Client;

class Skype extends Component
{
    const SKYPE_ERP646_CUSTOM_CHATS = [
        'Колл-центр Тайланд' => '19:be536cac0d2442448af4a44f516e25ab@thread.skype',
        'Колл-Центр Чили' => '19:fe00972ce63148e1b39e5625de53c6d4@thread.skype',
        'Колл-центр Индонезия B' => '19:389a86d1a6634ea383ed0f53971f254a@thread.skype',
        'Колл-центр Колумбия' => '19:833d72e6a7ab45b8afceb299b9472b2b@thread.skype',
        'Колл-центр Гватемала' => '19:4f2dcb1cd21546d9bba6e472feb07396@thread.skype',
        'Колл-центр Пакистан B' => '19:e69f2549575d4ee090ae144f65e57546@thread.skype',
    ];

    const SKYPE_OFFICE_CC_CHATS = [
        'Колл-центр Малайзия' => '19:ec12e32bcc6b41ffb0693e504f85626e@thread.skype',
        'Колл-центр Китай A' => '19:ab0a58b697864f459bbddf4c0bf1ef2e@thread.skype',
        'Колл-центр Китай B' => '19:296d0bde437e42278ad216e81a7b7672@thread.skype',
        'Колл-центр Гватемала' => '19:4f2dcb1cd21546d9bba6e472feb07396@thread.skype',
        'Колл-центр Тайланд' => '19:be536cac0d2442448af4a44f516e25ab@thread.skype',
        'Колл-центр Филиппины' => '19:6b7037ac2a9a49aeb612a1738095f05a@thread.skype',
        'Колл-центр Индонезия A' => '19:fe00972ce63148e1b39e5625de53c6d4@thread.skype',
        'Колл-центр Индонезия B' => '19:389a86d1a6634ea383ed0f53971f254a@thread.skype',
        'Колл-центр Тунис' => '19:923962adada14e1c87d4e2b9907c8d2f@thread.skype',
        'Колл-центр Колумбия' => '19:833d72e6a7ab45b8afceb299b9472b2b@thread.skype',
        'Колл-центр Пакистан B' => '19:e69f2549575d4ee090ae144f65e57546@thread.skype',
        'Колл-центр Москва' => '19:b771fb01a75e4137a4fc38570a5f3ccf@thread.skype',
        'Колл-центр Египет' => '19:d810d3853a1245b2819ecb74e51cd7ec@thread.skype',
        //'Мексика' => '19:d0245c885a0845ef8273c618da2be674@thread.skype',
    ];

    const SKYPE_CHAT_COUNTRIES = [
        // Юго -Восточная Азия:
        'Вьетнам' => '19:c6425358ce6c46388f1b6719d2ef22c2@thread.skype',
        'Камбоджа' => '19:c6425358ce6c46388f1b6719d2ef22c2@thread.skype',
        'Лаос' => '19:c6425358ce6c46388f1b6719d2ef22c2@thread.skype',
        'Тайланд 2' => '19:c6425358ce6c46388f1b6719d2ef22c2@thread.skype',
        'Малайзия 1' => '19:c6425358ce6c46388f1b6719d2ef22c2@thread.skype',
        'Бруней' => '19:c6425358ce6c46388f1b6719d2ef22c2@thread.skype',
        'Индонезия 1' => '19:c6425358ce6c46388f1b6719d2ef22c2@thread.skype',
        'Филиппины' => '19:c6425358ce6c46388f1b6719d2ef22c2@thread.skype',
        'Сингапур' => '19:c6425358ce6c46388f1b6719d2ef22c2@thread.skype',
        // Южная Азия:
        'Бангладеш' => '19:2c7025addbcb4f1d8f985825d667b987@thread.skype',
        'Индия' => '19:2c7025addbcb4f1d8f985825d667b987@thread.skype',
        'Непал' => '19:2c7025addbcb4f1d8f985825d667b987@thread.skype',
        'Пакистан' => '19:2c7025addbcb4f1d8f985825d667b987@thread.skype',
        'Шри-Ланка' => '19:2c7025addbcb4f1d8f985825d667b987@thread.skype',
        //Восточная Азия:
        'Китай' => '19:4f5482923b3142198840aa728b5ed7a1@thread.skype',
        'Тайвань' => '19:4f5482923b3142198840aa728b5ed7a1@thread.skype',
        'Гонконг' => '19:4f5482923b3142198840aa728b5ed7a1@thread.skype',
        'Монголия' => '19:4f5482923b3142198840aa728b5ed7a1@thread.skype',
        'Япония' => '19:4f5482923b3142198840aa728b5ed7a1@thread.skype',
        // Юго-Западная Азия:
        'Саудовская Аравия' => '19:70cddce915f7458088b063014bd07d08@thread.skype',
        'Кувейт' => '19:70cddce915f7458088b063014bd07d08@thread.skype',
        'Катар' => '19:70cddce915f7458088b063014bd07d08@thread.skype',
        'Бахрейн' => '19:70cddce915f7458088b063014bd07d08@thread.skype',
        'Оман' => '19:70cddce915f7458088b063014bd07d08@thread.skype',
        'ОАЭ' => '19:70cddce915f7458088b063014bd07d08@thread.skype',
        'Ливан' => '19:70cddce915f7458088b063014bd07d08@thread.skype',
        'Иордания' => '19:70cddce915f7458088b063014bd07d08@thread.skype',
        // Северная Африка:
        'Тунис' => '19:eac7c10d6ef74c1d91ff76edb65815ff@thread.skype',
        'Морокко' => '19:eac7c10d6ef74c1d91ff76edb65815ff@thread.skype',
        'Алжир' => '19:eac7c10d6ef74c1d91ff76edb65815ff@thread.skype',
        'Египет' => '19:eac7c10d6ef74c1d91ff76edb65815ff@thread.skype',
        // Южная Африка:
        'ЮАР' => '19:71164c47e7074e2b8b5dfb8844b6ed96@thread.skype',
        'Ботсвана' => '19:71164c47e7074e2b8b5dfb8844b6ed96@thread.skype',
        'Экваториальная Гвинея' => '19:71164c47e7074e2b8b5dfb8844b6ed96@thread.skype',
        'Сейшелы' => '19:71164c47e7074e2b8b5dfb8844b6ed96@thread.skype',
        'Маврикий' => '19:71164c47e7074e2b8b5dfb8844b6ed96@thread.skype',
        'Габон' => '19:71164c47e7074e2b8b5dfb8844b6ed96@thread.skype',
        //Центральная и Восточная Африка :
        //'Габон' => '',
        //'Экваториальная Гвинея' => '',
        //'Уганда' => '',
        //Западная Африка :
        //'Гана' => '',
        //'Нигерия' => '',
        //Латинская америка:
        'Колумбия' => '19:c5467287d3b8491da075b0a455767217@thread.skype',
        'Гватемала' => '19:0cd0ed1ce3b54b829065c4e0b6bda324@thread.skype',
        'Чили' => '19:c5467287d3b8491da075b0a455767217@thread.skype',
        'Перу' => '19:c5467287d3b8491da075b0a455767217@thread.skype',
        'Мексика' => '19:c5467287d3b8491da075b0a455767217@thread.skype',
    ];

    const ADCOMBO_CHAT_COUNTRIES = [
        // Юго -Восточная Азия:  19:93967e6fa42540dea449fc260ce25f0e@thread.skype
        'Вьетнам' => '19:93967e6fa42540dea449fc260ce25f0e@thread.skype',
        'Камбоджа' => '19:93967e6fa42540dea449fc260ce25f0e@thread.skype',
        'Лаос' => '19:93967e6fa42540dea449fc260ce25f0e@thread.skype',
        'Тайланд 2' => '19:93967e6fa42540dea449fc260ce25f0e@thread.skype',
        'Малайзия 1' => '19:93967e6fa42540dea449fc260ce25f0e@thread.skype',
        'Бруней' => '19:93967e6fa42540dea449fc260ce25f0e@thread.skype',
        'Индонезия 1' => '19:93967e6fa42540dea449fc260ce25f0e@thread.skype',
        'Сингапур' => '19:93967e6fa42540dea449fc260ce25f0e@thread.skype',
        'Филиппины' => '19:93967e6fa42540dea449fc260ce25f0e@thread.skype',
        // Южная Азия:  19:0554d94c42584454bbdce37a6c79dfac@thread.skype
        'Бангладеш' => '19:0554d94c42584454bbdce37a6c79dfac@thread.skype',
        'Индия' => '19:0554d94c42584454bbdce37a6c79dfac@thread.skype',
        'Непал' => '19:0554d94c42584454bbdce37a6c79dfac@thread.skype',
        'Пакистан' => '19:0554d94c42584454bbdce37a6c79dfac@thread.skype',
        'Шри-Ланка' => '19:93967e6fa42540dea449fc260ce25f0e@thread.skype',
        // Восточная Азия :  19:aa8f29f48d994d3f9d458ff7c1382505@thread.skype
        'Китай' => '19:aa8f29f48d994d3f9d458ff7c1382505@thread.skype',
        'Тайвань' => '19:aa8f29f48d994d3f9d458ff7c1382505@thread.skype',
        'Гонконг' => '19:aa8f29f48d994d3f9d458ff7c1382505@thread.skype',
        'Монголия' => '19:aa8f29f48d994d3f9d458ff7c1382505@thread.skype',
        'Япония' => '19:aa8f29f48d994d3f9d458ff7c1382505@thread.skype',
        // Юго-Западная Азия: 19:18e89ec80c1d45c7b66ce6b326cf512a@thread.skype
        'Саудовская Аравия' => '19:18e89ec80c1d45c7b66ce6b326cf512a@thread.skype',
        'Кувейт' => '19:18e89ec80c1d45c7b66ce6b326cf512a@thread.skype',
        'Катар' => '19:18e89ec80c1d45c7b66ce6b326cf512a@thread.skype',
        'Бахрейн' => '19:18e89ec80c1d45c7b66ce6b326cf512a@thread.skype',
        'Оман' => '19:18e89ec80c1d45c7b66ce6b326cf512a@thread.skype',
        'ОАЭ' => '19:18e89ec80c1d45c7b66ce6b326cf512a@thread.skype',
        'Ливан' => '19:18e89ec80c1d45c7b66ce6b326cf512a@thread.skype',
        'Иордания' => '19:18e89ec80c1d45c7b66ce6b326cf512a@thread.skype',
        // Северная Африка : 19:15b9fafea0f04fa1b1a6d7d6d4f6a1f0@thread.skype
        'Тунис' => '19:15b9fafea0f04fa1b1a6d7d6d4f6a1f0@thread.skype',
        'Морокко' => '19:15b9fafea0f04fa1b1a6d7d6d4f6a1f0@thread.skype',
        'Алжир' => '19:15b9fafea0f04fa1b1a6d7d6d4f6a1f0@thread.skype',
        'Египет' => '19:15b9fafea0f04fa1b1a6d7d6d4f6a1f0@thread.skype',
        // Южная Африка:
        // 'ЮАР' => '',
        // 'Ботсвана' => '',
        // 'Экваториальная Гвинея' => '',
        // 'Сейшелы' => '',
        // 'Маврикий' => '',
        //'Габон' => '',
        // Центральная и Восточная Африка :
        //'Габон' => '',
        //'Уганда' => '',
        // Западная Африка :
        //'Гана'  => '',
        //'Нигерия' => '',
        // Латинская америка: 19:1704da3cd4db4330a17c738eb0cf1706@thread.skype
        'Колумбия' => '19:1704da3cd4db4330a17c738eb0cf1706@thread.skype',
        'Гватемала' => '19:fee4bacfc4194c3abb7cd108f4005240@thread.skype',
        'Чили' => '19:1704da3cd4db4330a17c738eb0cf1706@thread.skype',
        'Перу' => '19:1704da3cd4db4330a17c738eb0cf1706@thread.skype',
        'Мексика' => '19:1704da3cd4db4330a17c738eb0cf1706@thread.skype',
    ];


    /**
     * Норма лидов, пока так, потом перенести
     */
    const LEAD_PLAN = [
        'Мексика' => 2000,
        'Колумбия' => 2000,
        'Чили' => 3000,
        'Перу' => 3500,
        'Гватемала' => 500,
        'Тайланд 2' => 1300,
        'Индонезия 1' => 1300,
        'Малайзия 1' => 200,
        'Камбоджа' => 450,
        'Филиппины' => 50,
    ];


    /**
     * Норма апрува в процентах, пока так, потом перенести
     */
    const APPROVE_PLAN_PERCENT = [
        'Мексика' => 30,
        'Колумбия' => 30,
        'Чили' => 30,
        'Перу' => 30,
        'Гватемала' => 0,
        'Тайланд 2' => 50,
        'Индонезия 1' => 30,
        'Малайзия 1' => 30,
        'Камбоджа' => 25,
        'Филиппины' => 25,
    ];

    const UNANSWERED_CALL_PLAN_PERCENT = 30;
    const RECALL_PLAN_PERCENT = 15;
    const REJECTED_PLAN_PERCENT = 15;
    const TRASH_PLAN_PERCENT = 9;
    const DUPLICATE_PLAN_PERCENT = 1;
    const HOLD_PLAN_PERCENT = 45;
    const ASR_PLAN_PERCENT = 40;

    const SKYPE_URL = 'https://bot.mobile-expansion.com/skype.php';

    const TEST_CHAT_ID = '19:ec8aafdd402442c48dee0316707c8378@thread.skype';

    const COORDINATION_CHAT_ID = '19:cc69096c224743c29d61a941c20f70ee@thread.skype';

    /**
     * @param string $text
     * @param string|array $chatId
     *
     * @return bool|array
     */
    public static function sendSkypeMessage($text, $chatId)
    {
        $return = [];

        if (is_array($chatId)) {
            $chats = $chatId;
        }
        else {
            $chats[] = $chatId;
        }

        foreach ($chats as $chat) {
            $data = [
                'chat_id' => $chat,
                'text' => $text,
            ];

            $client = new Client();
            $client->setTransport('yii\httpclient\CurlTransport');
            $response = $client->createRequest()
                ->setUrl(self::SKYPE_URL)
                ->setMethod('post')
                ->setOptions(['sslVerifyPeer' => false])
                ->setData($data)
                ->setFormat(Client::FORMAT_JSON)
                ->send();
            $return[] = $response->isOk;
        }

        return $return;
    }
}