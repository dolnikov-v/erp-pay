<?php

namespace app\modules\notification\components;

use app\modules\callcenter\models\CallCenterWorkTime;
use app\modules\order\models\Lead;
use app\modules\salary\models\Designation;
use app\modules\salary\models\Office;
use app\modules\salary\models\Person;
use Yii;
use yii\base\Component;
use app\models\Country;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;


/**
 * Class CallCenterDailyReport
 * @package app\modules\notification\components
 *
 * @property void $todayLeads
 * @property array $officesByEndOfWorkDay
 */
class CallCenterDailyReport extends Component
{

    /**
     * @param integer $officeId
     * @return integer
     */
    public static function getOperatorsOnline($officeId)
    {
        return CallCenterWorkTime::find()
            ->joinWith(['callCenterUser', 'callCenterUser.person', 'callCenterUser.person.designation'])
            ->where([CallCenterWorkTime::tableName() . '.date' => date('Y-m-d')])
            ->andWhere(['>', CallCenterWorkTime::tableName() . '.time', 0])
            ->andWhere([Person::tableName() . '.office_id' => $officeId])
            ->andWhere([Designation::tableName() . '.calc_work_time' => 1])
            ->groupBy(['person_id'])
            ->count();
    }


    /**
     * @return array
     */
    public static function getData()
    {
        $dateTime = new \DateTime('today midnight', new \DateTimeZone('Europe/Moscow'));
        $moscowDateStart = $dateTime->getTimestamp();

        $data = Order::find()
            ->joinWith(['country', 'lead', 'orderProducts'])
            ->select([
                'all' => 'COUNT(`order`.id)',
                'approved' => 'COUNT(`order`.status_id IN(' . join(',', OrderStatus::getApproveList()) . ') OR NULL)',
                'hold' => 'COUNT(`order`.status_id IN(' . join(',', OrderStatus::getHoldStatusList()) . ') OR NULL)',
                'trash' => 'COUNT(`order`.status_id IN(' . join(',', OrderStatus::getCCRejectedStatusList()) . ') OR NULL)',
                'products' => 'SUM(`order_product`.quantity)',
                'country_id' => Order::tableName() . '.country_id',
                'country_name' => Country::tableName() . '.name_en',
            ])
            ->where(['>=', Lead::tableName() . '.ts_spawn', $moscowDateStart])
            ->andWhere([Country::tableName() . '.active' => 1])
            ->andWhere([Country::tableName() . '.is_partner' => 0])
            ->andWhere([Country::tableName() . '.is_stop_list' => 0])
            ->groupBy(['country_id'])
            ->asArray()
            ->all();

        return ArrayHelper::index($data, 'country_name');
    }

    /**
     * @param array $officeData
     * @param Office $office
     * @param integer $operatorsOnline
     * @param null|string $testMode
     *
     * @return string
     */
    public static function prepareText($officeData, $office, $operatorsOnline, $testMode)
    {

        $br = PHP_EOL;

        $dateTimeFrom = new \DateTime('today midnight', new \DateTimeZone('Europe/Moscow'));
        $dateTimeNow = new \DateTime('now', new \DateTimeZone('Europe/Moscow'));

        $workTimeFrom = $office->workTime['from'] ?? 0;
        $workTimeTo = $office->workTime['to'] ?? 0;
        $text= '';
        $timeOffset = isset($office->country->timezone->time_offset) ? ($office->country->timezone->time_offset) : 0;

        $skypeTextBegin =
            Yii::t('common', 'Отчет работы', [], 'en-US') . ' ' . Yii::t('common', $office->name, [], 'en-US') . ': ' . $br .
            Yii::t('common', 'Данные за период', [], 'en-US') . ' ' . $dateTimeFrom->format('d.m.Y') . ' 00:00-' . $dateTimeNow->format('H:i') . ' (GMT+3)' . $br .
            Yii::t('common', 'Сегодня режим работы КЦ', [], 'en-US') . ': ' .
            Yii::$app->formatter->asTime($workTimeFrom + $timeOffset, 'php:H:i') . '-' .
            Yii::$app->formatter->asTime($workTimeTo + $timeOffset, 'php:H:i') . ' (GMT' . ($timeOffset < 0 ? '-' : '+') . +$timeOffset / 3600 . ')' . $br .
            Yii::t('common', 'Операторов онлайн', [], 'en-US') . ': ' . $operatorsOnline . $br . $br .
            Yii::t('common', 'Новых лидов сегодня по направлениям:', [], 'en-US') . $br;


        $totalHold = 0;
        foreach ($office->callCenters as $callCenter) {
            if (isset($officeData[$callCenter->country->name_en])) {

                $data = $officeData[$callCenter->country->name_en];

                // чтобы не отправлять дубли в пределах Офиса, т.к старый и новый КЦ
                unset($officeData[$callCenter->country->name_en]);

                $text .= Yii::t('common', $callCenter->country->name_en, [], 'en-US') . ' - ';
                $approvePercent = $holdPercent = $trashPercent = 0;
                $all = $data['all'];
                $approve = $data['approved'] ?? 0;
                $hold = $data['hold'] ?? 0;
                $totalHold += $hold;
                $trash = $data['trash'] ?? 0;
                $products = $data['products'] ?? 0;
                if (intval($all) > 0) {
                    $approvePercent = round($approve / $all * 100, 2);
                    $holdPercent = round($hold / $all * 100, 2);
                    $trashPercent = round($trash / $all * 100, 2);
                }

                $allNorma = Skype::LEAD_PLAN[$callCenter->country->name] ?? 0;
                $approvePercentNorma = Skype::APPROVE_PLAN_PERCENT[$callCenter->country->name] ?? 0;

                $line = Yii::t('common', '{all} (норма {allNorma}) из них апрув {approve} ({approvePercent}%) при норме {approvePercentNorma}%, холд {hold} ({holdPercent}%), треш {trash} ({trashPercent}%)', [
                    'all' => $all,
                    'allNorma' => $allNorma,
                    'approve' => $approve,
                    'approvePercent' => $approvePercent,
                    'approvePercentNorma' => $approvePercentNorma,
                    'hold' => $hold,
                    'holdPercent' => $holdPercent,
                    'trash' => $trash,
                    'trashPercent' => $trashPercent,
                ], 'en-US');

                $text .= $line . $br;


                $line = Yii::t('common', 'Общее кол-во товаров: {products};', [
                    'products' => $products,
                ], 'en-US');

                $text .= $line . $br;
            }
        }

        $text .= $br;

        $line = Yii::t('common', 'Всего по направлениям в холде: {totalHold};', [
            'totalHold' => $totalHold,
        ], 'en-US');

        $text .= $line . $br;

        $skypeText = nl2br ($skypeTextBegin . $text);

        if ($testMode) {
            Skype::sendSkypeMessage($skypeText, Skype::TEST_CHAT_ID);
            Console::stdout($skypeText);
            Console::stdout('Testing complete...');
            echo PHP_EOL;
            die();
        }

        if (array_key_exists($office->country->name, Skype::ADCOMBO_CHAT_COUNTRIES)) {
            Skype::sendSkypeMessage($skypeText, Skype::ADCOMBO_CHAT_COUNTRIES[$office->country->name]);
        }

        return $text;
    }
}