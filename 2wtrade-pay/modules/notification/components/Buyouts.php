<?php

namespace app\modules\notification\components;

use app\models\Country;
use app\models\CurrencyRate;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryApiClass;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\deliveryreport\models\DeliveryReport;
use app\modules\finance\components\ReportFormCountryCurrentBalance;
use app\modules\finance\models\ReportExpensesCountry;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use Yii;
use yii\base\Component;
use yii\helpers\ArrayHelper;

/**
 * Class Buyouts
 * @package app\modules\notification\components
 *
 * @property void $todayLeads
 * @property array $officesByEndOfWorkDay
 */
class Buyouts extends Component
{
    const NORM_AVG_CHECK = 65;

    const NORM_PAID = 100;

    const NORM_BUYOUT_BY_DAY = [
        7 => 40,
        15 => 60,
        30 => 75,
        45 => 90,
    ];

    /**
     * @param array $checkDates
     *
     * @return array
     * @throws \yii\db\Exception
     */
    public static function getData($checkDates)
    {
        $endDay = strtotime(end($checkDates) . ' 23:59:59');
        $startDay = strtotime(reset($checkDates) . ' 00:00:00');

        $statusApprovedList = OrderStatus::getApproveList();

        $rawDataArray = self::getRawData($statusApprovedList, $startDay, $endDay);
        $dataArray = ArrayHelper::index($rawDataArray, 'country_name', 'date');
        $buyOutData = self::formBuyoutData($checkDates, $dataArray, $startDay);

        return $buyOutData;
    }

    /**
     * @param array $statusApprovedList
     * @param int $startDay
     * @param int $endDay
     *
     * @return Order[]
     */
    protected static function getRawData($statusApprovedList, $startDay, $endDay)
    {
        return Order::find()
            ->leftJoin(Country::tableName(), Order::tableName() . '.country_id=' . Country::tableName() . '.id')
            //->leftJoin(CallCenterRequest::tableName(), CallCenterRequest::tableName() . '.order_id=' . Order::tableName() . '.id')
            ->leftJoin(CurrencyRate::tableName(), Country::tableName() . '.currency_id = ' . CurrencyRate::tableName() . '.currency_id')
            ->leftJoin(DeliveryRequest::tableName(), DeliveryRequest::tableName() . '.order_id=' . Order::tableName() . '.id')
            //->leftJoin(Delivery::tableName(), DeliveryRequest::tableName() . '.delivery_id=' . Delivery::tableName() . '.id')
            //->leftJoin(DeliveryApiClass::tableName(), Delivery::tableName() . '.api_class_id=' . DeliveryApiClass::tableName() . '.id')
            ->select([
                'country_id' => Order::tableName() . '.country_id',
                'country_name' => Country::tableName() . '.name',
                'date' => 'DATE_FORMAT(FROM_UNIXTIME(COALESCE(' . DeliveryRequest::tableName() . '.second_sent_at,' . DeliveryRequest::tableName() . '.sent_at, ' . DeliveryRequest::tableName() . '.created_at)), "%Y-%m-%d")',
                'vykupleno' => 'COUNT(`order`.status_id IN(' . join(',', OrderStatus::getBuyoutList()) . ') OR NULL)',
                'in_process' => 'COUNT(`order`.status_id IN(' . join(',', OrderStatus::getProcessList()) . ') OR NULL)',
                'not_buyout' => 'COUNT(`order`.status_id IN(' . join(',', OrderStatus::getNotBuyoutList()) . ') OR NULL)',
                'paid' => 'COUNT(`order`.status_id IN(' . OrderStatus::STATUS_FINANCE_MONEY_RECEIVED . ') OR NULL)',
                'vsego' => 'COUNT(`order`.id)',
                'sr_check' => 'AVG(CASE WHEN `order`.status_id IN (' . join(',', OrderStatus::getBuyoutList()) . ') THEN ((`order`.price_total + `order`.delivery)/ `currency_rate`.rate ) ELSE NULL END)',
                'cena_za_lidy' => 'SUM(`order`.income)',
            ])
            ->andWhere([
                '>=',
                'COALESCE(' . DeliveryRequest::tableName() . '.second_sent_at,' . DeliveryRequest::tableName() . '.sent_at, ' . DeliveryRequest::tableName() . '.created_at)',
                $startDay,
            ])
            ->andWhere([
                '<=',
                'COALESCE(' . DeliveryRequest::tableName() . '.second_sent_at,' . DeliveryRequest::tableName() . '.sent_at, ' . DeliveryRequest::tableName() . '.created_at)',
                $endDay,
            ])
            ->andWhere([Order::tableName() . '.status_id' => $statusApprovedList])
            ->groupBy(['country_id', 'date'])
            ->orderBy(['date' => SORT_DESC])
            ->asArray()
            ->all();
    }

    /**
     * @param int $day
     *
     * @return false|int
     */
    public static function convertDayToDate($day)
    {
        return strtotime("-$day days");
    }

    /**
     * формируем текст по выкупам для команд
     *
     * @param $team
     * @param $teamCountries
     * @param $data
     *
     * @param array $checkDates
     * @param string $br
     * @param boolean $showProvider
     *
     * @return string
     */
    public static function prepareText($team, $teamCountries, $data, $checkDates, $br = PHP_EOL, $showProvider = false)
    {
        $days = array_keys($checkDates);
        $sevenDaysAgo = $checkDates[7];
        $summary = "На " . date('d.m.Y') . $br
                   . 'По заказам отправленным ' . implode('/', $days) . ' дней назад' . $br . $br;

        $hasAnyTeam = false;
        foreach ($teamCountries as $countryName) {

            $buyoutText = "Выкуп: ";
            $averageCheckText = "Ср чек: ";
            $all = 'Отправлено: ';
            $inProcess = 'В процессе: ';
            $notBuyout = 'Невыкупы: ';
            $lastApiUpdate = 'никогда';
            $lastReportUpdate = 'никогда';
            $paid = $data[$countryName]['paid'] ?? 0;

            if (isset($data[$countryName]['last_api_update'])) {
                $lastApiUpdate = date('d.m.y', $data[$countryName]['last_api_update']);
            }

            if (isset($data[$countryName]['last_report_update'])) {
                $lastReportUpdate = date('d.m.y', $data[$countryName]['last_report_update']);
            }

            $hasAnyAll = false;
            foreach ($data[$countryName]['checkDates'] as $checkDate => $dataByDate) {
                if ($checkDate == $sevenDaysAgo) {
                    $buyoutText .= $dataByDate['buyout'] . "% / ";
                    $averageCheckText .= $dataByDate['sr_check'] . " (норма $" . self::NORM_AVG_CHECK . ") / ";
                } else {
                    $buyoutText .= $dataByDate['buyout'] . '% / ';
                    $averageCheckText .= '$' . $dataByDate['sr_check'] . ' / ';
                }

                $all .= $dataByDate['vsego'] . ' / ';
                if ($dataByDate['vsego']) {
                    $hasAnyAll = true;
                }
                $inProcess .= $dataByDate['in_process'] . '% / ';
                $notBuyout .= $dataByDate['not_buyout'] . '% / ';
            }

            if ($showProvider
                && $reportCountry = ReportExpensesCountry::find()->joinWith([Country::clearTableName()])->where([Country::tableName().'.name' => $countryName])->one())
            {
                $months = [
                    'Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
                    'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь',
                ];
                $profitText = 'Баланс:' . $br;
                $reportForm = new ReportFormCountryCurrentBalance();
                for ($i = 0; $i < 3; $i++) {
                    $month = date('m', strtotime(' - ' . $i . ' MONTH'));
                    $profits = $reportForm->getProfit([
                        'countries' => [$reportCountry],
                        'month' => $month,
                        'year' => date('Y', strtotime(' - ' . $i . ' MONTH')),
                    ]);
                    $profitText .= ArrayHelper::getValue($months, intval($month) - 1) . ': ' . round($profits[0]['profit'] ?? 0, 2) . ' $' . $br;
                }
            }

            if ($hasAnyAll) {
                $summary .= "В стране $countryName:" . $br;
                $hasAnyTeam = true;
                $textBodyTelegram = trim($averageCheckText, '/ ') . $br
                    . trim($buyoutText, '/ ') . $br
                    . trim($all, '/ ') . ' заказов' . $br
                    . trim($inProcess, '/ ') . $br
                    . trim($notBuyout, '/ ') . $br . $br
                    . (($showProvider && !empty($profitText)) ? $profitText . $br . $br : '')
                    . 'По заказам отправленным 45 дней назад и выкупленным, ' . $br
                    . 'деньги получены за ' . $paid . "% заказов (норма " . self::NORM_PAID . "%)" . $br
                    . 'Дата последнего обновления информации из отчетов: ' . $lastReportUpdate . $br
                    . 'Дата полного обновления информации по АПИ: ' . $lastApiUpdate . $br . $br;
                $summary .= $textBodyTelegram;
            }
        }

        $text = '';
        if ($hasAnyTeam) {
            $text = $br . 'По региону ' . $team['name'] . $br;
            $text .= $summary . $br;
        }
        return $text;
    }

    /**
     * @param array $checkDates
     *
     * @return array
     */
    private static function prepareBuyoutDataScheme($checkDates)
    {
        $buyOutData = [];
        $countries = Country::find()->select('name')->active()->notStopped()->asArray()->column();
        foreach ($countries as $country) {
            foreach ($checkDates as $checkDate) {
                $buyOutData[$country]['checkDates'][$checkDate]['buyout'] = '0';
                $buyOutData[$country]['checkDates'][$checkDate]['sr_check'] = '0';
                $buyOutData[$country]['checkDates'][$checkDate]['in_process'] = '0';
                $buyOutData[$country]['checkDates'][$checkDate]['not_buyout'] = '0';
                $buyOutData[$country]['checkDates'][$checkDate]['vsego'] = '0';
            }
        }

        return $buyOutData;
    }

    /**
     * @param int $startDay
     *
     * @return array
     */
    public static function getPaidOrders($startDay)
    {
        return Order::find()
            ->joinWith(['country'])
            ->leftJoin(DeliveryRequest::tableName(), DeliveryRequest::tableName() . '.order_id=' . Order::tableName() . '.id')
            ->select([
                'buyoutOrders' => 'COUNT(' . Order::tableName() . '.id)',
                'paidOrders' => 'COUNT(`order`.status_id IN(' . OrderStatus::STATUS_FINANCE_MONEY_RECEIVED . ') OR NULL)',
                'country_name' => Country::tableName() . '.name',
                'country_id' => Order::tableName() . '.country_id',
            ])
            ->andWhere([Order::tableName() . '.status_id' => OrderStatus::getBuyoutList()])
            ->andWhere([Country::tableName() . '.active' => 1])
            ->andWhere([Country::tableName() . '.is_stop_list' => 0])
            ->andWhere([
                '<=',
                'COALESCE(' . DeliveryRequest::tableName() . '.second_sent_at,' . DeliveryRequest::tableName() . '.sent_at, ' . DeliveryRequest::tableName() . '.created_at)',
                $startDay,
            ])
            ->groupBy(['country_id'])
            ->asArray()
            ->all();
    }

    /**
     * @param array $checkDates
     * @param array $dataArray
     * @param int $startDay
     *
     * @return array
     * @throws \yii\db\Exception
     */
    public static function formBuyoutData($checkDates, $dataArray, $startDay)
    {

        $buyOutData = self::prepareBuyoutDataScheme($checkDates);
        $paidOrdersData = ArrayHelper::index(self::getPaidOrders($startDay), 'country_name');
        $lastUpdateData = self::lastUpdate();

        foreach ($checkDates as $checkDate) {
            if (!isset($dataArray[$checkDate])) {
                continue;
            }
            foreach ($dataArray[$checkDate] as $country => $buyoutDataByCountry) {
                $buyout = 0;
                $inProcess = 0;
                $notBuyout = 0;
                $paid = 0;
                $all = $buyoutDataByCountry['vsego'] ?? 0;

                if (intval($all) > 0) {
                    $buyout = round($buyoutDataByCountry['vykupleno'] / $all * 100, 2);
                    $inProcess = round($buyoutDataByCountry['in_process'] / $all * 100, 2);
                    $notBuyout = round($buyoutDataByCountry['not_buyout'] / $all * 100, 2);
                }
                $buyOutData[$country]['checkDates'][$checkDate]['buyout'] = $buyout;
                $buyOutData[$country]['checkDates'][$checkDate]['sr_check'] = round($buyoutDataByCountry['sr_check'] ?? '0', 2);
                $buyOutData[$country]['checkDates'][$checkDate]['in_process'] = $inProcess;
                $buyOutData[$country]['checkDates'][$checkDate]['not_buyout'] = $notBuyout;
                $buyOutData[$country]['checkDates'][$checkDate]['vsego'] = $all;
                if (isset($paidOrdersData[$country]['buyoutOrders']) && intval($paidOrdersData[$country]['buyoutOrders']) > 0) {
                    $paid = round($paidOrdersData[$country]['paidOrders'] / $paidOrdersData[$country]['buyoutOrders'], 2);
                }

                $buyOutData[$country]['paid'] = $paid;
                if (isset($lastUpdateData[$country]['last_api_update'])) {
                    $buyOutData[$country]['last_api_update'] = $lastUpdateData[$country]['last_api_update'];
                }
                if (isset($lastUpdateData[$country]['last_report_update'])) {
                    $buyOutData[$country]['last_report_update'] = $lastUpdateData[$country]['last_report_update'];
                }

            }
        }

        return $buyOutData;
    }

    /**
     * @return array
     * @throws \yii\db\Exception
     */
    public static function lastUpdate()
    {
        $lastUpdateQuery = Order::find()
            ->joinWith(['country', 'deliveryRequest.delivery.apiClass'])
            ->select([
                'delivery_id' => Delivery::tableName() . '.id',
                'last_api_update' => "CASE WHEN (" . DeliveryApiClass::tableName() . ".can_tracking = 1 and " . Delivery::tableName() . ".can_tracking = 1) THEN MIN(" . DeliveryRequest::tableName() . '.cron_launched_at) ELSE NULL END',
                'last_report_update' => 'MIN(COALESCE(' . DeliveryRequest::tableName() . '.sent_at, ' . DeliveryRequest::tableName() . '.created_at))',
                'country_name' => Country::tableName() . '.name',
                'country_id' => Order::tableName() . '.country_id',
            ])
            ->where([Country::tableName() . '.active' => 1])
            ->andWhere([Country::tableName() . '.is_stop_list' => 0])
            ->andWhere([
                DeliveryRequest::tableName() . '.status' => [
                    DeliveryRequest::STATUS_IN_PROGRESS,
                    DeliveryRequest::STATUS_IN_LIST,
                ],
            ])
            ->andWhere([Delivery::tableName() . '.active' => 1,])
            ->groupBy(['delivery_id'])
            ->asArray()
            ->createCommand()->queryAll();
        $lastUpdate = ArrayHelper::index($lastUpdateQuery, 'delivery_id', 'country_name');
        $lastUpdateResult = self::findMinReportDate($lastUpdate);

        $lastUpdateReportQuery = DeliveryReport::find()
            ->joinWith(['country', 'delivery'])
            ->where([
                Delivery::tableName() . '.active' => 1,
                Country::tableName() . '.active' => 1,
            ])
            ->select([
                'delivery_id' => Delivery::tableName() . '.id',
                'country_id' => Country::tableName() . '.id',
                'country_name' => Country::tableName() . '.name',
                'last_report_update' => "MAX(COALESCE(" . DeliveryReport::tableName() . '.period_to, ' . DeliveryReport::tableName() . '.created_at))',
            ])
            ->groupBy(['delivery_id'])
            ->createCommand()->queryAll();
        $lastUpdateReport = ArrayHelper::index($lastUpdateReportQuery, 'delivery_id', 'country_name');
        $lastUpdateReportResult = self::findMinReportDate($lastUpdateReport);
        //$lastUpdateResult = [];
        foreach ($lastUpdateReportResult as $country => $datesByReport) {
            if (isset($lastUpdateResult[$country]['last_report_update'])) {
                $lastUpdateResult[$country]['last_report_update'] = $datesByReport['last_report_update'];
            }
        }

        return $lastUpdateResult;
    }

    /**
     * @param array $lastUpdate
     *
     * @return array
     */
    private static function findMinReportDate($lastUpdate)
    {
        $result = [];
        foreach ($lastUpdate as $country => $countryData) {
            $lasApiUpdateMin = $lasApiReportMin = 9999999999;
            foreach ($countryData as $deliveryData) {
                if (isset($deliveryData['last_api_update'])
                    && ($deliveryData['last_api_update'] < $lasApiUpdateMin)
                ) {
                    $lasApiUpdateMin = $deliveryData['last_api_update'];
                }
                if (isset($deliveryData['last_report_update'])
                    && ($deliveryData['last_report_update'] < $lasApiReportMin)
                ) {
                    $lasApiReportMin = $deliveryData['last_report_update'];
                }
            }
            $lasApiUpdateMin = ($lasApiUpdateMin != 9999999999) ? $lasApiUpdateMin : null;
            $lasApiReportMin = ($lasApiReportMin != 9999999999) ? $lasApiReportMin : null;

            $result[$country]['last_api_update'] = $lasApiUpdateMin;
            $result[$country]['last_report_update'] = $lasApiReportMin;
        }

        return $result;
    }

    /**
     * @param int $buyout
     *
     * @param int $day
     *
     * @return string
     */
    protected static function checkBuyoutNorm($buyout, $day)
    {
        $norm = self::NORM_BUYOUT_BY_DAY[$day];
        if ((int)$buyout < $norm) {
            return $buyout . "% (norm " . $norm . "%)";
        }

        return $buyout . "% (norm " . $norm . "%)";
    }

    /**
     * @param $avgCheck
     *
     * @return string
     */
    protected static function checkAvgCheckNorm($avgCheck)
    {
        $norm = self::NORM_AVG_CHECK;
        if ((int)$avgCheck < self::NORM_AVG_CHECK) {
            return "$" . $avgCheck . " (norm $" . $norm . ")";
        }

        return "$" . $avgCheck . " (norm $" . $norm . ")";
    }

    /**
     * @param $paid
     *
     * @return string
     */
    protected static function checkPaidNorm($paid)
    {
        $norm = self::NORM_PAID;
        if ((int)$paid < $norm) {
            return $paid . "% orders (norm " . $norm . "%)";
        }

        return $paid . "% orders (norm " . $norm . "%)";
    }

    /**
     * @param array $days
     *
     * @return array
     */
    public static function getCheckDates($days)
    {
        $checkDates = [];
        foreach ($days as $day) {
            $checkDates[$day] = Yii::$app->formatter->asDate(Buyouts::convertDayToDate($day), 'php:Y-m-d');
        }

        return $checkDates;
    }
}