<?php

namespace app\modules\notification\components;

use app\modules\callcenter\models\CallCenterWorkTime;
use app\modules\order\models\Lead;
use app\modules\salary\models\Designation;
use app\modules\salary\models\Office;
use app\modules\salary\models\Person;
use Yii;
use yii\base\Component;
use app\models\Country;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;


/**
 * Class CallCenterEfficiencyOperators
 * @package app\modules\notification\components
 *
 * @property void $todayLeads
 * @property array $officesByEndOfWorkDay
 */
class CallCenterEfficiencyOperators extends Component
{

    /**
     * @param Office $office
     * @return float|int
     */
    private static function getEmployedJobs($office)
    {
        $todayHours = $office->getTodayWorkHoursOperators();
        $normHours = $office->getNormHoursPerWorkplace();

        $employedJobs = 0;
        if ($normHours > 0) {
            $employedJobs = round($todayHours / $normHours, 1);
        }

        return $employedJobs;
    }

    /**
     * @param Office $office
     * @return string
     */
    private static function getOccupiedWorkplaces($office)
    {
        $employedJobs = self::getEmployedJobs($office);
        $jobsNumber = $office->getJobsNumber();

        $employedJobsPercent = 0;
        if ($jobsNumber > 0) {
            $employedJobsPercent = round($employedJobs * 100 / $jobsNumber);
        }

        return $employedJobs . ' (' . $employedJobsPercent . '%)';
    }

    /**
     * @param Office $office
     * @return string
     */
    private static function getOverallEfficiencyWorkplaces($office)
    {
        $employedJobs = self::getEmployedJobs($office);

        $efficiency = 0;
        $jobsNumber = $office->getJobsNumber();
        if ($jobsNumber > 0) {
            $efficiency = round($employedJobs / $jobsNumber * 100);
        }
        return $efficiency . '%';
    }

    /**
     * @param Office $office
     * @return string
     */
    private static function getHoursWorkedPerWorkplace($office)
    {
        $todayHours = $office->getTodayWorkHoursOperators();
        $hoursPerWorkPlace = $percent = 0;
        $jobsNumber = $office->getJobsNumber();
        if ($jobsNumber > 0) {
            $hoursPerWorkPlace = round($todayHours / $jobsNumber, 1);
        }
        $normHours = $office->getNormHoursPerWorkplace();
        if ($normHours) {
            $percent = round($hoursPerWorkPlace * 100 / $normHours);
        }

        return $hoursPerWorkPlace . ' (' . $percent . '%)';
    }

    /**
     * @param Office $office
     * @param null|string $testMode
     *
     * @return string
     */
    public static function prepareText($office, $testMode)
    {

        $br = '<br />';

        $dateTimeNow = new \DateTime('now', new \DateTimeZone('Europe/Moscow'));
        $data = AdcomboStatus::getData();
        $avgApproveData = $data['avgApproveData'];
        $avgApproveCost = $avgApproveData[$office->country->char_code] ?? 0;
        $asrArray = $data['asr'];
        $asrByCountry = $asrArray[$office->country->char_code] ?? 0;
        $acdArray = $data['acd'];
        $acdByCountry = $acdArray[$office->country->char_code] ?? 0;

        $text = '<b>' . $office->country->name_en . '</b> Call Center on ' . $dateTimeNow->format('d.m.Y H:i') . ' GMT+3' . $br;
        $text .= 'Working hours: ' . $office->getWorkingHours() . $br;
        $text .= 'Total workplaces of operators: ' . $office->getJobsNumber() . $br;
        $text .= 'Occupied workplaces: ' . self::getOccupiedWorkplaces($office) . $br;

        //https://2wtrade-tasks.atlassian.net/browse/ERP-646
        $text .= 'Average approve cost: ' . $avgApproveCost . $br;
        $text .= 'ASR: ' . $asrByCountry . $br;
        $text .= 'ACD: ' . $acdByCountry . $br;

        //https://2wtrade-tasks.atlassian.net/browse/ERP-646
        //$text .= 'Overall efficiency of workplaces: ' . self::getOverallEfficiencyWorkplaces($office) . $br;
        //$text .= 'Hours worked per workplace: ' . self::getHoursWorkedPerWorkplace($office) . ', the norm is ' . $office->getNormHoursPerWorkplace() . ' hours';

        if ($testMode) {
            $response = Skype::sendSkypeMessage($text, Skype::TEST_CHAT_ID);
            Console::stdout($text);
            Console::stdout('Testing complete...');
            echo PHP_EOL;
            print_r($response);
            die();
        }

        if (array_key_exists($office->name, Skype::SKYPE_ERP646_CUSTOM_CHATS)) {
            Skype::sendSkypeMessage($text, Skype::SKYPE_ERP646_CUSTOM_CHATS[$office->name]);
        }

        return $text;
    }
}