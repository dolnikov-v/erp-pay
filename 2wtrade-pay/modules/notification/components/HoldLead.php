<?php

namespace app\modules\notification\components;

use app\modules\callcenter\models\CallCenterRequest;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use app\modules\salary\models\Office;
use yii\base\Component;
use yii\helpers\ArrayHelper;


/**
 * Class HoldLead
 * @package app\modules\notification\components
 *
 * @property void $todayLeads
 * @property array $officesByEndOfWorkDay
 */
class HoldLead extends Component
{
    /**
     * @return Office[]
     */
    public static function getOfficesByEndOfWorkDay()
    {
        date_default_timezone_set('UTC');
        $startDay = strtotime("midnight", time());
        $currentTime = time() - $startDay;
        $halfHourAgo = $currentTime - 30 * 60;
        if ($halfHourAgo < 0) {
            $halfHourAgo += 86400;
        }
        $offices = Office::find()->where(['is not', 'work_time', null])->callCenter()->active()->all();
        $officesRecentlyFinishedWorkDay = [];
        foreach ($offices as $office) {
            if (!$office->workTime
                || $office->workTime['from'] < 0
                || !self::inTimeRange($halfHourAgo, $currentTime, $office->workTime['to'])
            ) {
                continue;
            }
            $officesRecentlyFinishedWorkDay[] = $office;
        }

        return $officesRecentlyFinishedWorkDay;
    }

    /**
     * @param Office $office
     *
     * @return array
     */
    public static function getBuyoutData($office)
    {
        $yesterday = time() - 86400;
        $endTime = strtotime(date('H:i:s', $office->workTime['to']));
        $officeCallCenters = ArrayHelper::getColumn($office->callCenters, 'id');
        $query = Order::find()
            ->joinWith(['callCenterRequest.callCenter.offices'])
            ->where(['>', Order::tableName() . '.created_at', $yesterday])
            ->andWhere(['<', Order::tableName() . '.created_at', $endTime - 30 * 60])
            ->andWhere([CallCenterRequest::tableName() . '.call_center_id' => $officeCallCenters]);
        $allOrders = $query->count();
        $holdOrders = $query->andWhere([Order::tableName() . '.status_id' => OrderStatus::STATUS_CC_POST_CALL])
            ->count();

        $holdPerCent = $allOrders ? round($holdOrders / $allOrders * 100, 2) : 0;

        return [
            'all' => $allOrders,
            'holdPerCent' => $holdPerCent,
        ];
    }

    /**
     * Находится ли время $time в секундах в диапазоне заданного времени $start - $end
     *
     * @param int $start
     * @param int $end
     * @param int $time
     *
     * @return bool
     */
    public static function inTimeRange($start, $end, $time)
    {
        if ($start > $end) {
            $end = strtotime('+1 day', $end);
            echo $end . PHP_EOL;
        }

        return (($time >= $start) && ($time <= $end)) || (($time + 86400 >= $start) && ($time + 86400 <= $end));
    }
}