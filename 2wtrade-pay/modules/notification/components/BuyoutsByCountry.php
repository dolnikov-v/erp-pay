<?php

namespace app\modules\notification\components;

use app\models\Country;
use app\models\Currency;
use app\models\CurrencyRate;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\order\models\Order;
use app\modules\order\models\OrderLog;
use app\modules\order\models\OrderStatus;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;

/**
 * Class BuyoutsByCountry
 * @package app\modules\notification\components
 *
 * @property void $todayLeads
 * @property array $officesByEndOfWorkDay
 */
class BuyoutsByCountry extends Buyouts
{
    const BR = "<br />";
    const DAYS_AGO_45 = 45;
    const DAYS_AGO_7 = 7;

    /**
     * @param array $checkDates
     *
     * @return array
     */
    public static function getData($checkDates)
    {
        $max =  max(array_values($checkDates));
        $min = min(array_values($checkDates));

        $startDay = strtotime($min . ' 00:00:00');
        $endDay = strtotime($max . ' 23:59:59');

        $statusApprovedList = OrderStatus::getApproveList();

        $rawDataArray = self::getRawData($statusApprovedList, $startDay, $endDay);
        $rawDataArrayJapan = self::getRawDataJapan($startDay, $endDay);
        $dataArray = ArrayHelper::index(array_merge($rawDataArray, $rawDataArrayJapan), 'country_id', 'date');

        $buyOutData = self::formBuyoutData($checkDates, $dataArray, $startDay);

        return $buyOutData;
    }

    /**
     * @param array $statusApprovedList
     * @param int $startDay
     * @param int $endDay
     *
     * @return Order[]
     */
    protected static function getRawData($statusApprovedList, $startDay, $endDay)
    {
        $usdCurrency = Currency::getUSD();

        return Order::find()
            ->leftJoin(Country::tableName(), Order::tableName() . '.country_id=' . Country::tableName() . '.id')
            ->leftJoin(CurrencyRate::tableName(), Country::tableName() . '.currency_id = ' . CurrencyRate::tableName() . '.currency_id')
            ->leftJoin(CallCenterRequest::tableName(), CallCenterRequest::tableName() . '.order_id=' . Order::tableName() . '.id')
            ->select([
                'country_id' => Order::tableName() . '.country_id',
                'country_name' => Country::tableName() . '.name',
                'date' => 'DATE_FORMAT(FROM_UNIXTIME(' . CallCenterRequest::tableName() . '.approved_at), "%Y-%m-%d")',
                'vykupleno' => 'COUNT(`order`.status_id IN(' . join(',', OrderStatus::getBuyoutList()) . ') OR NULL)',
                'in_process' => 'COUNT(`order`.status_id IN(' . join(',', OrderStatus::getProcessList()) . ') OR NULL)',
                'vsego' => 'COUNT(`order`.id)',
                'sr_check' => 'AVG(CASE WHEN `order`.status_id IN (' . join(',', OrderStatus::getBuyoutList()) . ') THEN convertToCurrencyByCountry(' . Order::tableName() . '.price_total + ' . Order::tableName() . '.delivery, ' . Order::tableName() . '.price_currency,' . $usdCurrency->id . ') ELSE NULL END)',
                'paid' => 'COUNT(`order`.status_id IN(' . OrderStatus::STATUS_FINANCE_MONEY_RECEIVED . ') OR NULL)',
            ])
            ->andWhere([
                '>=',
                CallCenterRequest::tableName() . '.approved_at',
                min($startDay, $endDay),
            ])
            ->andWhere([
                '<=',
                CallCenterRequest::tableName() . '.approved_at',
                max($startDay, $endDay),
            ])
            ->andWhere([Country::tableName() . '.is_stop_list' => 0])
            ->andWhere([Country::tableName() . '.active' => 1])
            ->andWhere(['<>', Country::tableName() . '.char_code', 'JP'])
            ->groupBy(['country_id', 'date'])
            ->orderBy(['date' => SORT_DESC])
            ->asArray()
            ->all();
    }

    /**
     * Костыль для Японии для получения данных, там нет CallCenterRequest смотрим OrderLog
     * @param int $startDay
     * @param int $endDay
     *
     * @return Order[]
     */
    protected static function getRawDataJapan($startDay, $endDay)
    {
        $usdCurrency = Currency::getUSD();

        $subQuery = OrderLog::find()
            ->select([
                'order_id' => OrderLog::tableName() . '.order_id',
                'created_at' => OrderLog::tableName() . '.created_at',
            ])
            ->where([
                'field' => 'status_id',
                'new' => OrderStatus::STATUS_CC_APPROVED,
            ]);

        return Order::find()
            ->leftJoin(Country::tableName(), Order::tableName() . '.country_id=' . Country::tableName() . '.id')
            ->leftJoin(CurrencyRate::tableName(), Country::tableName() . '.currency_id = ' . CurrencyRate::tableName() . '.currency_id')
            ->leftJoin(['t' => $subQuery], 't.order_id = ' . Order::tableName() . '.id')
            ->select([
                'country_id' => Order::tableName() . '.country_id',
                'country_name' => Country::tableName() . '.name',
                'date' => 'DATE_FORMAT(FROM_UNIXTIME(t.created_at), "%Y-%m-%d")',
                'vykupleno' => 'COUNT(`order`.status_id IN(' . join(',', OrderStatus::getBuyoutList()) . ') OR NULL)',
                'in_process' => 'COUNT(`order`.status_id IN(' . join(',', OrderStatus::getProcessList()) . ') OR NULL)',
                'vsego' => 'COUNT(`order`.id)',
                'sr_check' => 'AVG(CASE WHEN `order`.status_id IN (' . join(',', OrderStatus::getBuyoutList()) . ') THEN convertToCurrencyByCountry(' . Order::tableName() . '.price_total + ' . Order::tableName() . '.delivery, ' . Order::tableName() . '.price_currency,' . $usdCurrency->id . ') ELSE NULL END)',
                'paid' => 'COUNT(`order`.status_id IN(' . OrderStatus::STATUS_FINANCE_MONEY_RECEIVED . ') OR NULL)',
            ])
            ->andWhere([
                '>=',
                't.created_at',
                min($startDay, $endDay),
            ])
            ->andWhere([
                '<=',
                't.created_at',
                max($startDay, $endDay),
            ])
            ->andWhere([Country::tableName() . '.is_stop_list' => 0])
            ->andWhere([Country::tableName() . '.active' => 1])
            ->andWhere([Country::tableName() . '.char_code' => 'JP'])
            ->groupBy(['country_id', 'date'])
            ->orderBy(['date' => SORT_DESC])
            ->asArray()
            ->all();
    }


    /**
     * @param array $countryData
     * @param array $country
     * @param array $checkDates
     * @param null|string $testMode
     *
     * @return string
     */
    public static function sendMessageByCountry($countryData, $country, $checkDates, $testMode = null)
    {
        $paid = $countryData['paid'] ?? 0;
        $text = "<b>" . $country['name_en'] . "</b>" . self::BR;
        $text .= "Date " . date('d.m.Y') . self::BR;
        $text .= "By orders approved <b>45 days ago</b> and buyout, " . self::BR;
        $text .= "money received for " . self::checkPaidNorm($paid). self::BR;

        if (isset($checkDates[self::DAYS_AGO_45])) {
            $checkDate = $checkDates[self::DAYS_AGO_45];
            $inProcess = $countryData['checkDates'][$checkDate]['in_process'] ?? 0;
            $text .=  "in process: $inProcess% (norm 0%); ".self::BR;
        }

        $text .= self::BR;
        $text .= "By orders approved:" .self::BR;
        foreach ($checkDates as $day => $checkDate) {
            if ($day == self::DAYS_AGO_7) {
                continue;
            }
            $buyout = $countryData['checkDates'][$checkDate]['buyout'] ?? 0;
            $avgCheck = $countryData['checkDates'][$checkDate]['sr_check'] ?? 0;
            $text .= "<b>" . $day . " days ago</b>, buyout: " . self::BR . self::checkBuyoutNorm($buyout, $day) . ";" . self::BR;
            $text .= "average buyout check: " . self::BR . self::checkAvgCheckNorm($avgCheck) . ";" . self::BR . self::BR;
        }
        $chatId = $testMode ? Skype::TEST_CHAT_ID : (Skype::SKYPE_CHAT_COUNTRIES[$country['name']] ?? 0);

        if ($chatId) {
            Skype::sendSkypeMessage($text, $chatId);
        }
        if ($testMode) {
            Console::stdout($text);
            Console::stdout('Testing complete...');
            echo PHP_EOL;
            die();
        }

        return $text;
    }

    /**
     * @param array $checkDates
     *
     * @return array
     */
    private static function prepareBuyoutDataScheme($checkDates)
    {
        $buyOutData = [];
        $countries = Country::find()->select('id')->active()->notStopped()->asArray()->column();
        foreach ($countries as $country) {
            foreach ($checkDates as $checkDate) {
                $buyOutData[$country]['checkDates'][$checkDate]['buyout'] = '0';
                $buyOutData[$country]['checkDates'][$checkDate]['sr_check'] = '0';
                $buyOutData[$country]['checkDates'][$checkDate]['vsego'] = '0';
                $buyOutData[$country]['checkDates'][$checkDate]['in_process'] = '0';
            }
        }

        return $buyOutData;
    }

    /**
     * @param array $checkDates
     * @param array $dataArray
     * @param int $startDay
     *
     * @return array
     */
    public static function formBuyoutData($checkDates, $dataArray, $startDay)
    {
        $buyOutData = self::prepareBuyoutDataScheme($checkDates);
        $paidOrdersData = ArrayHelper::index(self::getPaidOrders($startDay), 'country_id');

        foreach ($checkDates as $checkDate) {
            if (!isset($dataArray[$checkDate])) {
                continue;
            }
            foreach ($dataArray[$checkDate] as $country => $buyoutDataByCountry) {
                $buyout = 0;
                $process = 0;
                $all = $buyoutDataByCountry['vsego'] ?? 0;

                if (intval($all) > 0) {
                    $buyout = round($buyoutDataByCountry['vykupleno'] / $all * 100, 2);
                    $process = round($buyoutDataByCountry['in_process'] / $all * 100, 2);
                }
                $buyOutData[$country]['checkDates'][$checkDate]['buyout'] = $buyout;
                $buyOutData[$country]['checkDates'][$checkDate]['sr_check'] = round($buyoutDataByCountry['sr_check'] ?? '0', 2);
                $buyOutData[$country]['checkDates'][$checkDate]['vsego'] = $all;
                $buyOutData[$country]['checkDates'][$checkDate]['in_process'] = $process;
            }
        }

        foreach ($buyOutData as $country => $data) {
            $paid = 0;
            if (isset($paidOrdersData[$country]['buyoutOrders']) && intval($paidOrdersData[$country]['buyoutOrders']) > 0) {
                $paid = round($paidOrdersData[$country]['paidOrders'] / $paidOrdersData[$country]['buyoutOrders'] * 100);
            }
            $buyOutData[$country]['paid'] = $paid;
        }

        return $buyOutData;
    }
}