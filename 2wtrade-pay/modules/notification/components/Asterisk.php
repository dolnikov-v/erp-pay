<?php
namespace app\modules\notification\components;

use yii\base\Component;
use app\models\Country;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;
use Yii;

/**
 * Class ASR
 * @package app\modules\notification\components
 *
 */
class Asterisk extends Component
{
    /**
     * @param Country[] $countries
     * @param \DateTime $dateTime
     * @return array
     */
    public static function getAsrData(&$countries, $dateTime)
    {
        $countriesCodes = ArrayHelper::getColumn($countries, 'char_code');

        $asrArray = [];
        try {
            $asrArray = Yii::$app->asterisk_db->createCommand(
                'SELECT `asr` as asr, upper(`country_code`) as country_code FROM asr 
                        where time >="' . $dateTime->format('Y-m-d') . ' 00:00:00" 
                        and time <="' . $dateTime->format('Y-m-d') . ' 23:59:59"
                        and country_code in ( "' . join('", "', $countriesCodes) . '")'
            )->queryAll();
            $asrArray = ArrayHelper::map($asrArray, 'country_code', 'asr');
        } catch (\Exception $e) {
            Console::stdout('Exception: ' . $e->getMessage());
        }
        return $asrArray;
    }

    /**
     * @param $countries
     * @param $dateTime
     * @return array
     */
    public static function getAcdData(&$countries, $dateTime)
    {
        $countriesCodes = ArrayHelper::getColumn($countries, 'char_code');

        $acdArray = [];
        try {
            $acdArray = Yii::$app->asterisk_db->createCommand(
                'SELECT round(avg((total_answ_duration/60)/answeredcall), 2) as acd, upper(`country_code`) as country_code FROM asr 
                        where time >="' . $dateTime->format('Y-m-d') . ' 00:00:00" 
                        and time <="' . $dateTime->format('Y-m-d') . ' 23:59:59"
                        and country_code in ( "' . join('", "', $countriesCodes) . '") group by asr.country_code'
            )->queryAll();
            $acdArray = ArrayHelper::map($acdArray, 'country_code', 'acd');
        } catch (\Exception $e) {
            Console::stdout('Exception: ' . $e->getMessage());
        }
        return $acdArray;
    }
}