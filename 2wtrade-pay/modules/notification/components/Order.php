<?php

namespace app\modules\notification\components;

use app\models\Country;
use app\modules\order\models\Order as OrderModel;
use app\modules\order\models\OrderStatus;
use Yii;
use yii\base\Component;
use yii\db\Expression;

/**
 * Class Order
 * @package app\modules\notification\components
 */
class Order extends Component
{
    /**
     * Средняя стоимость апрува - кусок запроса из Васиного функционала "Текущий баланс"
     * @param $countries
     * @param $dateStart
     * @return OrderModel[]
     */
    public static function getAverageApproveCost($countries, $dateStart)
    {
        $query = OrderModel::find()
            ->select([
                'leadavgprice' => new Expression('round(' . OrderModel::tableName() . '.income, 2)'),
                'country_code' => new Expression('upper(country.char_code)')
            ])
            ->joinWith(['country'])
            ->where(['is', OrderModel::tableName() . '.duplicate_order_id', null])
            ->andWhere(['>=', OrderModel::tableName() . '.created_at', yii::$app->formatter->asTimestamp($dateStart)])
            ->andWhere(['<=', OrderModel::tableName() . '.created_at', yii::$app->formatter->asTimestamp($dateStart) + 86400])
            ->andWhere([OrderModel::tableName() . '.status_id' => OrderStatus::getApproveList()])
            ->andWhere([OrderModel::tableName() . '.country_id' => $countries])
            ->groupBy([OrderModel::tableName() . '.country_id']);

        return $query->asArray()->all();
    }
}