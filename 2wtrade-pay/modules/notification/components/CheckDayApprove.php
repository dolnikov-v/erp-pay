<?php

namespace app\modules\notification\components;

use app\modules\callcenter\models\CallCenter;
use app\modules\callcenter\models\CallCenterUserOrder;
use app\modules\finance\models\ReportExpensesItem;
use app\modules\order\models\Lead;
use app\modules\report\extensions\Query;
use app\modules\salary\models\Office;
use Yii;
use yii\base\Component;
use app\models\Country;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;


/**
 * Class CheckDayApprove
 * @package app\modules\notification\components
 *
 * @property void $todayLeads
 * @property array $officesByEndOfWorkDay
 */
class CheckDayApprove extends Component
{

    private static $officeData;

    private static $penaltyData;

    private static $holdData;

    /**
     * @param array $officeCallCenterIds
     *
     * @return array
     */
    private static function getData($officeCallCenterIds)
    {
        $dateTime = new \DateTime('today midnight', new \DateTimeZone('Europe/Moscow'));
        $moscowDateStart = $dateTime->getTimestamp();

        $approves = Order::find()
            ->joinWith(['callCenterRequest', 'country'])
            ->select([
                'approve' => 'COUNT(`order`.status_id IN(' . join(',', OrderStatus::getApproveList()) . ') OR NULL)',
                'all' => 'COUNT(`order`.id)',
                'country_id' => Order::tableName() . '.country_id',
                'country_name' => Country::tableName() . '.name',
            ])
            ->where(['>', Order::tableName() . '.created_at', $moscowDateStart])
            ->andWhere([CallCenterRequest::tableName() . '.call_center_id' => $officeCallCenterIds])
            ->groupBy(['country_id'])
            ->asArray()
            ->all();

        return ArrayHelper::index($approves, 'country_name');
    }

    /**
     * @param array $officeCallCenterIds
     *
     * @return array
     */
    private static function getHoldData($officeCallCenterIds)
    {
        $queryHolds = new Query();
        return $queryHolds->select([
                "hold" => "count(*)",
                "minimal_hold_date" => "FROM_UNIXTIME(MIN(s.created_at))",
                'country_name' => Country::tableName() . ".name",
                "call_depth" => "sum(total_order_calls) / sum(total_rows)"
            ])
            ->from([
                "s" => Order::find()
                    ->joinWith(['callCenterRequest'], true, "JOIN")
                    ->leftJoin(
                        CallCenterUserOrder::tableName(),
                        CallCenterUserOrder::tableName() . ".order_id = ". Order::tableName() . ".id"
                    )
                    ->select([
                        "id" => "`order`.id",
                        "country_id" => "`order`.`country_id`",
                        "created_at" => "`order`.`created_at`",
                        "total_order_calls" => "sum( COALESCE(number_of_calls, 0) )",
                        "total_rows" => "COUNT(call_center_user_order.id)"
                    ])->andWhere([
                        CallCenterRequest::tableName() . '.call_center_id' => $officeCallCenterIds,
                        Order::tableName() . ".status_id" => OrderStatus::getHoldStatusList()
                    ])
                    ->groupBy(['`order`.id'])
            ])
            ->leftJoin("country", "`country_id` = `country`.`id`")
            ->groupBy(["`country`.`id`"])
            ->indexBy('country_name')
            ->all();
    }

    private static function getPenaltyData($callCenterIds) {
        $dateTime = new \DateTime('today midnight', new \DateTimeZone('Europe/Moscow'));
        $moscowDateStart = $dateTime->getTimestamp();

        $penaltyData = new Query();
        $data = $penaltyData->select([
                "countryName" => Country::tableName().".name",
                "approved" => "COUNT(CASE WHEN ".Order::tableName().".status_id in (".join(",", OrderStatus::getApproveList()).") THEN 1 ELSE NULL END)",
                "all" => "COUNT(*)",
                "avgLeadPrice" => "AVG(".Lead::tableName().".total_price)",
            ])->from(
                Order::tableName()
            )->innerJoin(
                CallCenterRequest::tableName(),
                CallCenterRequest::tableName().".order_id = ".Order::tableName().".id"
            )->innerJoin(
                Lead::tableName(),
                Lead::tableName().".order_id = ".Order::tableName().".id"
            )->innerJoin(
                CallCenter::tableName(),
                CallCenter::tableName().".id = ".CallCenterRequest::tableName().".call_center_id"
            )->innerJoin(
                Country::tableName(),
                Country::tableName().".id = ".CallCenter::tableName().".country_id"
            )->where([
                '>', Order::tableName() . '.created_at', $moscowDateStart
            ])->andWhere([
                CallCenterRequest::tableName().".call_center_id" => $callCenterIds
            ])->groupBy([Country::tableName().".name"])
            ->indexBy("countryName")
            ->all();

        $adcomboApprove = ReportExpensesItem::find()->byCode(ReportExpensesItem::ADCOMBO_GTD_APPROVE_PERCENT)->one();
        if ($adcomboApprove) {
            array_walk($data, function ($item) use ($adcomboApprove) {
                $data["adcomboapprove"] = $adcomboApprove->value;
            });
        }

        return $data;
    }

    /**
     * @param Office $office
     * @param null|string $testMode
     *
     * @return string
     */
    public static function buildMessageForOffice($office, $testMode)
    {
        $skypeBr = '<br/>';
        $textBr = PHP_EOL;

        $officeCallCenterIds = ArrayHelper::getColumn($office->callCenters, 'id');
        self::$penaltyData = self::getPenaltyData($officeCallCenterIds);
        self::$officeData = self::getData($officeCallCenterIds);
        self::$holdData = self::getHoldData($officeCallCenterIds);

        $dateTimeFrom = new \DateTime('today midnight', new \DateTimeZone('Europe/Moscow'));
        $dateTimeNow = new \DateTime('now', new \DateTimeZone('Europe/Moscow'));

        $workTimeFrom = $office->workTime['from'] ?? 0;
        $workTimeTo = $office->workTime['to'] ?? 0;

        $textByCountrySkype = $textByCountryNotification = '';
        $skypeTextBegin = '<b>'.$office->country->name_en . '</b> Call Center ('
                          . Yii::$app->formatter->asTime($workTimeFrom, 'php:H:i') . '-'
                          . Yii::$app->formatter->asTime($workTimeTo, 'php:H:i') . ' GMT+0),' . $skypeBr
                          . 'Data for the period ' . $dateTimeFrom->format('d.m.Y H:i') . '-' . $dateTimeNow->format('H:i') . ' (GMT+3)' . $skypeBr
                          . 'New leads today in the directions:' . $skypeBr . $skypeBr;

        foreach (self::getCallCenterCountries($officeCallCenterIds) as $country) {

            $lines = self::buildCountryMessageLines($country);

            $textByCountrySkype .= implode($skypeBr, $lines) . $skypeBr . $skypeBr;
            $textByCountryNotification .= implode($textBr, $lines) . $textBr . $textBr;

        }
        $skypeText = $skypeTextBegin . $textByCountrySkype;

        if ($testMode) {
            Skype::sendSkypeMessage($skypeText, Skype::TEST_CHAT_ID);
            Console::stdout($skypeText);
            Console::stdout('Testing complete...');
            echo PHP_EOL;
            die();
        }

        if (array_key_exists($office->name, Skype::SKYPE_OFFICE_CC_CHATS)) {
            Skype::sendSkypeMessage($skypeText, Skype::SKYPE_OFFICE_CC_CHATS[$office->name]);
        }

        return strip_tags($textByCountryNotification);
    }

    private static function getCallCenterCountries($callCenterIds) {
        return CallCenter::find()
            ->joinWith(["country"], true, "JOIN")
            ->select([
                Country::tableName() . ".id",
                Country::tableName() . ".name"
            ])
            ->where([
                CallCenter::tableName() . '.id' => $callCenterIds
            ])
            ->groupBy([
                Country::tableName() . ".id"
            ])
            ->indexBy("id")
            ->all();
    }

    private static function buildCountryMessageLines($country) : array {
        $approveData = self::$officeData[$country->name] ?? [];
        $holdData = self::$holdData[$country->name] ?? [];
        $penaltyData = self::$penaltyData[$country->name] ?? [];

        $approvePercent = 0;
        $all = $approveData['all'] ?? 0;
        $approve = $approveData['approve'] ?? 0;

        if (intval($all) > 0) {
            $approvePercent = round($approveData['approve'] / $approveData['all'] * 100, 2);
        }

        $allNorma = Skype::LEAD_PLAN[$country->name] ?? 0;
        $approvePercentNorma = Skype::APPROVE_PLAN_PERCENT[$country->name] ?? 0;

        $callDepth = $holdData['call_depth'] ? round($holdData['call_depth'], 2) : null;

        $penaltyPrice = $penaltyData ? self::calculatePenalty($penaltyData) : null;

        $lines = [];
        $lines[] = '<b>'. Yii::t('common', $country->name, [], 'en-US') . '</b>  - ' . $all . ' (norm ' . $allNorma . ')';
        $lines[] = 'penalty is ' . ($penaltyData ? $penaltyPrice : "--no data--");
        $lines[] = 'approved ' . $approve . ' (' . $approvePercent . '%) at the norm ' . $approvePercentNorma . '%';
        $lines[] = 'total in hold - ' . ($holdData['hold'] ?? 0);
        $lines[] = 'minimum hold date - ' . ($holdData['minimal_hold_date'] ?? "--no data--");
        $lines[] = 'call depth - ' . ($callDepth ?? "--no data--");

        return $lines;
    }

    private static function calculatePenalty($penaltyData) {
        $penaltyLeadCount = (int) ($penaltyData["all"] * $penaltyData["adcomboapprove"] - $penaltyData["approved"]);
        return $penaltyLeadCount * $penaltyData["avgLeadPrice"];
    }
}