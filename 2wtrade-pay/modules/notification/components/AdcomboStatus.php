<?php

namespace app\modules\notification\components;

use app\modules\notification\components\Order as OrderComponent;
use app\modules\order\models\Lead;
use app\modules\report\extensions\Query;
use yii\base\Component;
use app\models\Country;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;


/**
 * Class AdcomboStatus
 * @package app\modules\notification\components
 *
 * @property void $todayLeads
 * @property array $officesByEndOfWorkDay
 */
class AdcomboStatus extends Component
{

    /**
     * @param null $countryId
     * @return array
     */
    public static function getData($countryId = null)
    {
        $dateTime = new \DateTime('yesterday midnight', new \DateTimeZone('Europe/Moscow'));
        $moscowDateStart = $dateTime->getTimestamp();
        $countryQuery = Country::find()
            ->where([
                'country.active' => 1,
                'country.is_partner' => 0,
                'country.is_stop_list' => 0
            ])
            ->indexBy("id");

        if ($countryId) {
            $countryQuery->andWhere(['id' => $countryId]);
        }
        $countries = $countryQuery->asArray()->all();

        $countriesIds = ArrayHelper::getColumn($countries, 'id');
        $asrArray = Asterisk::getAsrData($countries, $dateTime);
        $acdArray = Asterisk::getAcdData($countries, $dateTime);
        $avgApproveData = OrderComponent::getAverageApproveCost($countriesIds, $dateTime);

        $orderStatisticsSubQuery = Order::find()
            ->innerJoinWith(["lead"])
            ->where(['>=', Lead::tableName() . '.ts_spawn', $moscowDateStart])
            ->andWhere(['<', Lead::tableName() . '.ts_spawn', $moscowDateStart + 86400])
            ->andWhere([Order::tableName() . '.country_id' => $countriesIds]);
        $countryStatisticsQuery = new Query();
        return [
            'data' => $countryStatisticsQuery->select([
                'country_id' => '`subquery`.country_id',
                'all' => 'COUNT(`subquery`.id)',
                'approved' => 'COUNT(`subquery`.status_id IN(' . join(',', OrderStatus::getApproveList()) . ') OR NULL)',
                'unanswered' => 'COUNT(`subquery`.status_id = ' . OrderStatus::STATUS_CC_FAIL_CALL . ' OR NULL)',
                'recall' => 'COUNT(`subquery`.status_id =' . OrderStatus::STATUS_CC_RECALL . ' OR NULL)',
                'rejected' => 'COUNT(`subquery`.status_id IN(' . OrderStatus::STATUS_CC_REJECTED . ') OR NULL)',
                'trash' => 'COUNT(`subquery`.status_id IN(' . OrderStatus::STATUS_CC_TRASH . ') OR NULL)',
                'duplicate' => 'COUNT(`subquery`.status_id IN(' . OrderStatus::STATUS_CC_DOUBLE . ') OR NULL)',
                'in_hold' => 'COUNT(`subquery`.status_id IN(' . implode(',', OrderStatus::getHoldStatusList()) . ') OR NULL)',
            ])
                ->from([
                    'subquery' => $orderStatisticsSubQuery
                ])
                ->groupBy(["`subquery`.country_id"])
                ->indexBy("country_id")
                ->all(),
            'asr' => $asrArray,
            'acd' => $acdArray,
            'avgApproveData' => ArrayHelper::map($avgApproveData, 'country_code', 'leadavgprice'),
            'countries' => $countries,
        ];
    }

    /**
     * @param array $countryData
     * @param array $country
     * @param array $asrArray
     * @param null|string $testMode
     */
    public static function sendMessageByCountry($countryData, $country, $asrArray, $testMode = null)
    {
        if ($testMode) {
            Console::stdout(print_r($country['name']) . PHP_EOL . print_r($countryData) . PHP_EOL);
        }
        $moscowTime = new \DateTime('yesterday', new \DateTimeZone('Europe/Moscow'));
        $moscowDate = $moscowTime->format('d.m.Y');
        $approved = $countryData['approved'] ?? 0;
        $unanswered = $countryData['unanswered'] ?? 0;
        $recall = $countryData['recall'] ?? 0;
        $rejected = $countryData['rejected'] ?? 0;
        $trash = $countryData['trash'] ?? 0;
        $duplicate = $countryData['duplicate'] ?? 0;
        $hold = $countryData['in_hold'] ?? 0;
        $asr = $asrArray[$country['char_code']] ?? 0;
        $all = $countryData['all'] ?? 0;
        $approvedPerCent = $unansweredPerCent = $recallPerCent = $rejectedPerCent = $trashPerCent = $duplicatePerCent = $holdPerCent = 0;
        if ($all) {
            $approvedPerCent = round((int)$approved / (int)$all * 100, 2);
            $unansweredPerCent = round((int)$unanswered / (int)$all * 100, 2);
            $recallPerCent = round((int)$recall / (int)$all * 100, 2);
            $rejectedPerCent = round((int)$rejected / (int)$all * 100, 2);
            $trashPerCent = round((int)$trash / (int)$all * 100, 2);
            $duplicatePerCent = round((int)$duplicate / (int)$all * 100, 2);
            $holdPerCent = round((int)$hold / (int)$all * 100, 2);
        }

        $text = "<b>" . $country['name_en'] . '</b> ' . $moscowDate . ' GMT+3' . '<br/>';
        $text .= "New leads: " . $all . self::getNorm($all, Skype::LEAD_PLAN[$country['name']] ?? 0) . "<br/>";

        $text .= "approved: " . $approved . " (" . $approvedPerCent . "%" . self::getNorm($approvedPerCent, Skype::APPROVE_PLAN_PERCENT[$country['name']] ?? 0, "%)") . "<br/>";
        $text .= "unanswered call: " . $unanswered . " (" . $unansweredPerCent . "%" . self::getNormBadPercent($unansweredPerCent, Skype::UNANSWERED_CALL_PLAN_PERCENT) . "<br/>";
        $text .= "recall: " . $recall . " (" . $recallPerCent . "%" . self::getNormBadPercent($recallPerCent, Skype::RECALL_PLAN_PERCENT) . "<br/>";
        $text .= "rejected: " . $rejected . " (" . $rejectedPerCent . "%" . self::getNormBadPercent($rejectedPerCent, Skype::REJECTED_PLAN_PERCENT) . "<br/>";
        $text .= "trash: " . $trash . " (" . $trashPerCent . "%" . self::getNormBadPercent($trashPerCent, Skype::TRASH_PLAN_PERCENT) . "<br/>";
        $text .= "duplicate orders: " . $duplicate . " (" . $duplicatePerCent . "%" . self::getNormBadPercent($duplicatePerCent, Skype::DUPLICATE_PLAN_PERCENT) . "<br/>";
        $text .= "total in hold: " . $hold . " (" . $holdPerCent . "%" . self::getNormBadPercent($holdPerCent, Skype::HOLD_PLAN_PERCENT) . "<br/>";
        $text .= "ASR: " . $asr . "%" . self::getNorm($asr, Skype::ASR_PLAN_PERCENT, "%") . "<br/>";

        $chatId = $testMode ? Skype::TEST_CHAT_ID : Skype::ADCOMBO_CHAT_COUNTRIES[$country['name']];

        if ($testMode) {
            Console::stdout($text).PHP_EOL;
            Console::stdout('Testing complete...');
            //return;
        }

        Skype::sendSkypeMessage($text, $chatId);
    }

    /**
     * @param $value
     * @param $norm
     * @param string $sfx
     * @return string
     */
    private static function getNorm($value, $norm, $sfx = '')
    {
        return " / " . $norm . $sfx . " " . self::getArrow($value, $norm);
    }

    /**
     * @param $value
     * @param $norm
     * @return string
     */
    private static function getNormBadPercent($value, $norm)
    {
        return " / " . $norm . "%) " . self::getArrow($norm, $value);
    }

    /**
     * @param $value
     * @param $norm
     * @return string
     */
    private static function getArrow($value, $norm)
    {
        if ($value > $norm) {
            return "(yes)";
        }
        return "";
    }
}