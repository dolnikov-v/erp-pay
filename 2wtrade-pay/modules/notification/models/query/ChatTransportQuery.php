<?php
namespace app\modules\notification\models\query;

use app\modules\notification\models\ChatTransport;
use app\components\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[ChatTransport]].
 *
 * @see ChatTransport
 */
class ChatTransportQuery extends ActiveQuery
{
    /**
     * @inheritdoc
     * @return ChatTransport[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ChatTransport|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}