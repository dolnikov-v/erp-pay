<?php

namespace app\modules\notification\models\query;

use app\modules\notification\models\ChatGroup;
use app\components\db\ActiveQuery;
use yii\helpers\ArrayHelper;

/**
 * Class ChatGroupQuery
 * @package app\models\query
 * @method ChatGroup one($db = null)
 * @method ChatGroup[] all($db = null)
 */
class ChatGroupQuery extends ActiveQuery
{
    /**
     * @param null $language
     * @return array
     */
    public function collection($language = null)
    {
        return ArrayHelper::map($this->all(), $this->collectionKey, $this->collectionValue);
    }
}
