<?php

namespace app\modules\notification\models;

use Yii;
use yii\db\ActiveRecord;
use app\modules\notification\models\query\ChatGroupQuery;

/**
 * Class ChatGroup
 * @package app\models
 *
 * @property Chat[] $chats
 */
class ChatGroup extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%chat_group}}';
    }

    /**
     * @return ChatGroupQuery
     */
    public static function find()
    {
        return new ChatGroupQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'required'],
            ['name', 'unique'],
            ['name', 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('common', 'Название группы'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChats()
    {
        return $this->hasMany(Chat::className(), ['id' => 'chat_id'])
            ->viaTable('chat_link_group', ['group_id' => 'id']);
    }
}