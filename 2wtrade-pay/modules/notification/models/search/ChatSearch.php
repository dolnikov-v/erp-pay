<?php

namespace app\modules\notification\models\search;

use app\modules\notification\models\ChatTransport;
use app\modules\notification\models\Chat;
use yii\data\ActiveDataProvider;
use Yii;

/**
 * Class ChatSearch
 * @package app\modules\nitification\models\search
 */
class ChatSearch extends ChatTransport
{
    /*
     * @var $name string
     */
    public $name;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['chat_number', 'name', 'messenger'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['name'] = Yii::t('common', 'Название чата');
        return $labels;
    }

    /**
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['id'] = SORT_ASC;
        }

        $query = Chat::find()
            ->joinWith(['chatTransport'])
            ->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        if (!$this->load($params) || !$this->validate()) {
            return $dataProvider;
        }

        if ($this->chat_number) {
            $query->andFilterWhere(['like', ChatTransport::tableName() . '.chat_number', '%' . $this->chat_number . '%']);
        }
        $query->andFilterWhere([ChatTransport::tableName() . '.messenger' => $this->messenger]);
        if ($this->name) {
            $query->andFilterWhere(['like', Chat::tableName() . '.name', '%' . $this->name . '%']);
        }
        $query->groupBy([Chat::tableName() . '.id']);

        return $dataProvider;
    }
}
