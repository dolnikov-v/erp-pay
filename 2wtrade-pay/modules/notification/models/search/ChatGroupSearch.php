<?php

namespace app\modules\notification\models\search;

use app\modules\notification\models\ChatGroup;
use yii\data\ActiveDataProvider;

/**
 * Class ChatGroupSearch
 * @package app\models\search
 */
class ChatGroupSearch extends ChatGroup
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['id'] = SORT_ASC;
        }

        $query = ChatGroup::find()
            ->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        if (!$this->load($params) || !$this->validate()) {
            return $dataProvider;
        }

        return $dataProvider;
    }
}
