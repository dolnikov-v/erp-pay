<?php

namespace app\modules\notification\models;

use Yii;
use yii\db\ActiveRecord;
use app\models\Country;
use yii\helpers\ArrayHelper;

/**
 * Class Chat
 * @package app\models
 *
 * @property integer $id
 * @property string $name
 * @property integer $active
 * @property Chat[] $groups
 * @property array $groupsList
 * @property Country[] $countries
 * @property array $countriesIds
 * @property ChatTransport[] $chatTransport
 */
class Chat extends ActiveRecord
{

    const ICONS = [
        'skype' => [
            'class' =>  'skype',
            'style' => 'color: #12A5F4'
        ], /*'telegram' => 'fa-telegram',*/];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%chat}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'required'],
            ['name', 'string', 'max' => 255],
            [['active'], 'boolean'],
            [['groupsList', 'countriesIds'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'chat_id' => Yii::t('common', 'Номер чата'),
            'messenger' => Yii::t('common', 'Мессенджер'),
            'name' => Yii::t('common', 'Имя'),
            'active' => Yii::t('common', 'Доступность'),
            'groupsList' => Yii::t('common', 'Группы чатов'),
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($this->groupsList) {

            $this->unlinkAll('groups', true);
            foreach ($this->groupsList as $groupId) {
                $group = ChatGroup::findOne($groupId);
                $this->link('groups', $group);
            }
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountries()
    {
        return $this->hasMany(Country::className(), ['id' => 'country_id'])
            ->viaTable('chat_country', ['chat_id' => 'id']);
    }

    /**
     * @return array
     */
    public function getCountriesIds()
    {
        if (!$this->countries) {
            return [];
        }

        return $countries = ArrayHelper::getColumn($this->countries, 'id');
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroups()
    {
        return $this->hasMany(ChatGroup::className(), ['id' => 'group_id'])
            ->viaTable('chat_link_group', ['chat_id' => 'id']);
    }

    /**
     * @return array
     */
    public function getGroupsList()
    {
        $groups = ArrayHelper::index($this->groups, 'id');

        return $groups = ArrayHelper::getColumn($groups, 'name');
    }

    /**
     * @param array $groupsList
     */
    public function setGroupsList($groupsList)
    {
        $this->groupsList = $groupsList;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChatTransport()
    {
        return $this->hasMany(ChatTransport::className(), ['chat_id' => 'id']);
    }

    /**
     * сохранение ChatTransport, выбранных для $this
     * @param array $data
     * @return bool
     * @throws \Exception
     */
    public function saveTransport($data)
    {
        if (is_array($data['chat_number'])) {
            ChatTransport::deleteAll(['chat_id' => $this->id]);
            foreach ($data['chat_number'] as $key => $item) {
                if (!$item) {
                    continue;
                }
                try {
                    $transport = new ChatTransport();
                    $transport->chat_id = $this->id;
                    $transport->chat_number = $item;
                    $transport->messenger = $data['messenger'][$key] ?? null;
                    if (!$transport->save()) {
                        throw new \Exception(json_encode($transport->getFirstErrors()));
                    }
                } catch (\Exception $e) {
                    throw new \Exception($e->getMessage());
                }
            }
        }
        return true;
    }
}