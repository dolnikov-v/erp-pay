<?php

namespace app\modules\notification\models;
use yii\db\ActiveRecord;
use app\modules\notification\models\query\ChatTransportQuery;
use Yii;

/**
 * This is the model class for table "chat_transport".
 *
 * @property integer $id
 * @property integer $chat_id
 * @property string $chat_number
 * @property string $messenger
 *
 * @property Chat $chat
 */
class ChatTransport extends ActiveRecord
{
    const MESSENGERS = ['skype' => 'Skype', 'telegram' => 'Telegram'];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%chat_transport}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['chat_id', 'chat_number', 'messenger'], 'required'],
            [['chat_id'], 'integer'],
            [['chat_number'], 'string', 'max' => 256],
            [['messenger'], 'string', 'max' => 32],
            ['messenger', 'in', 'range' => array_keys(self::MESSENGERS)],
            //[['chat_id'], 'exist', 'skipOnError' => true, 'targetClass' => Chat::className(), 'targetAttribute' => ['chat_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'chat_id' => Yii::t('common', 'Ссылка на чат'),
            'chat_number' => Yii::t('common', 'Номер чата'),
            'messenger' => Yii::t('common', 'Транспорт'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChat()
    {
        return $this->hasOne(Chat::className(), ['id' => 'chat_id']);
    }

    /**
     * @inheritdoc
     * @return ChatTransportQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ChatTransportQuery(get_called_class());
    }
}
