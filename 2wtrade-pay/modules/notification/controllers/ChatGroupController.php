<?php

namespace app\modules\notification\controllers;

use Yii;
use yii\helpers\Url;
use yii\web\HttpException;
use app\components\web\Controller;
use app\modules\notification\models\ChatGroup;
use app\modules\notification\models\search\ChatGroupSearch;

/**
 * Class ChatGroupController
 * @package app\controllers\notification
 */
class ChatGroupController extends Controller
{

    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new ChatGroupSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param null $id
     *
     * @return string|\yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new ChatGroup();
        }

        if ($model->load(Yii::$app->request->post())) {
            $isNewRecord = $model->isNewRecord;

            if ($model->save()) {
                Yii::$app->notifier
                    ->addNotification($isNewRecord
                        ? Yii::t('common', 'Группа успешно добавлена.')
                        : Yii::t('common', 'Группа успешно отредактирована.'),
                        'success');

                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }

        return $this->render('edit', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     * @throws HttpException
     * @throws \Exception
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $model = $this->getModel($id);

        if ($model->delete()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Чат удален.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param integer $id
     *
     * @return ChatGroup
     * @throws HttpException
     */
    private function getModel($id)
    {
        $model = ChatGroup::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Группа не найдена.'));
        }

        return $model;
    }
}