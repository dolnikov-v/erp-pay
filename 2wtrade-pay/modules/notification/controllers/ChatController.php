<?php

namespace app\modules\notification\controllers;

use app\modules\notification\models\ChatGroup;
use Yii;
use yii\helpers\Url;
use app\models\Country;
use app\components\web\Controller;
use yii\web\HttpException;
use app\modules\notification\models\Chat;
use app\modules\notification\models\ChatTransport;
use app\modules\notification\models\search\ChatSearch;
use yii\web\BadRequestHttpException;
use app\components\filters\AjaxFilter;

/**
 * Class ChatController
 * @package app\controllers\notification
 */
class ChatController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => ['set-country'],
            ]
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new ChatSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'modelSearch' => $modelSearch,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param null $id
     *
     * @return string|\yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionEdit($id = null)
    {
        $transports = null;
        if ($id) {
            $model = $this->getModel($id);
            $transports = $model->chatTransport;
        } else {
            $model = new Chat();
        }

        if ($model->load(Yii::$app->request->post())) {
            $transaction = Yii::$app->db->beginTransaction();
            $isNewRecord = $model->isNewRecord;

            if ($model->save()) {
                try {
                    $model->saveTransport(Yii::$app->request->post('ChatTransport'));
                    Yii::$app->notifier
                        ->addNotification($isNewRecord
                            ? Yii::t('common', 'Чат успешно добавлен.')
                            : Yii::t('common', 'Чат успешно отредактирован.'),
                            'success');
                    $transaction->commit();
                    return $this->redirect(Url::toRoute('index'));
                } catch (\Exception $e) {
                    Yii::$app->notifier->addNotification($e->getMessage(), 'danger');
                }
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
            $transaction->rollBack();
        }

        $groups = ChatGroup::find()->collection();
        $countries = Country::find()->active()->notStopped();
        $controller = '/' . Yii::$app->controller->module->id . '/' . Yii::$app->controller->id . '/';

        return $this->render('edit', [
            'model' => $model,
            'groups' => $groups,
            'countries' => $countries->collection(),
            'controller' => $controller,
            'countryList' => $countries->all(),
            'countriesByChat' => $model->countriesIds,
            'transports' => $transports ?? [new ChatTransport()],
        ]);
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionActivate($id)
    {
        $model = $this->getModel($id);

        $model->active = 1;

        if ($model->save()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Чат активирован.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionDeactivate($id)
    {
        $model = $this->getModel($id);

        $model->active = 0;

        if ($model->save()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Чат деактивирован.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     * @throws HttpException
     * @throws \Exception
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $model = $this->getModel($id);

        if ($model->delete()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Чат удален.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * Ajax установка страны
     * @throws \yii\web\HttpException
     * @todo Требует рефакторинга
     */
    public function actionSetCountry()
    {
        $chatId = (int)Yii::$app->request->post('model_id');
        $countryId = (int)Yii::$app->request->post('country_id');

        $chat = Chat::find()->where(['id' => $chatId])->one();
        $country = Country::find()->where(['id' => $countryId])->one();

        if (!$chat || !$country) {
            throw new BadRequestHttpException(Yii::t('common', 'Обращение к несуществущей стране или чату.'));
        }

        $value = Yii::$app->request->post('value');

        switch ($value) {
            case 'on':
                $chat->link('countries', $country);
                break;
            case 'off':
                $chat->unlink('countries', $country, true);
                break;
        }

        return [
            'success' => true,
        ];
    }

    /**
     * @param integer $id
     *
     * @return Chat
     * @throws HttpException
     */
    private function getModel($id)
    {
        $model = Chat::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Чат не найден.'));
        }

        return $model;
    }
}