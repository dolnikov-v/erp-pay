<?php

use app\widgets\Panel;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var app\modules\notification\models\ChatGroup $model */


$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление чата') : Yii::t('common', 'Редактирование группы чатов');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Чаты'), 'url' => Url::toRoute('/notification/chat/index')];
$this->params['breadcrumbs'][] = ['label' => $model->isNewRecord ? Yii::t('common', 'Добавление чата') : Yii::t('common', 'Редактирование группы чатов')];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Группа чатов'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
    ]),
]); ?>
