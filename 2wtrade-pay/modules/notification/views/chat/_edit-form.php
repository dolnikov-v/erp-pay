<?php

use app\components\widgets\ActiveForm;
use app\modules\notification\models\ChatTransport;
use yii\helpers\Url;

/** @var app\modules\notification\models\Chat $model */
/** @var array $groups */
/** @var array $countries */
/** @var ChatTransport[] $transports */
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute(['edit', 'id' => $model->id]),
]); ?>
<div class="row">
    <div class="col-lg-4">
        <?= $form->field($model, 'name')->textInput() ?>
    </div>
    <div class="col-lg-8">
        <div class="form-group">
            <?= $form->field($model, 'groupsList')->select2ListMultiple($groups, [
                'value' => $model->isNewRecord ? null : array_keys($model->groupsList),
            ]) ?>
        </div>
    </div>
</div>
<div class="group-transport-group">
    <?php foreach ($transports as $key => $transport): ?>
        <?= $this->render('_edit-form-transport', [
            'form' => $form,
            'model' => $transport,
        ]) ?>
    <?php endforeach; ?>
    <?= $this->render('_edit-form-transport', [
        'form' => $form,
        'model' => new ChatTransport(),
    ]) ?>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить чат') : Yii::t('common', 'Сохранить чат')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
