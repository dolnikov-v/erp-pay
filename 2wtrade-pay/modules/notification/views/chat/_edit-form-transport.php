<?php
use app\helpers\WbIcon;
use app\widgets\Button;
use yii\helpers\Html;
use app\modules\notification\models\ChatTransport;

/** @var yii\web\View $this */
/** @var app\components\widgets\ActiveForm $form */
/** @var app\modules\notification\models\ChatTransport $model */
/** @var $users array */
?>

<div class="row row-transport">
    <div class="col-lg-4 chat_number">
        <?= $form->field($model, 'chat_number')
            ->textInput([
                'name' => Html::getInputName($model, 'chat_number') . '[]',
                'value' => $model->chat_number,
                'id' => false,
            ]) ?>
    </div>
    <div class="col-lg-4 messengers">
        <?= $form->field($model, 'messenger')->select2List(ChatTransport::MESSENGERS, [
            'prompt' => '—',
            'value' => $model->messenger,
            'id' => false,
            'name' => Html::getInputName($model, 'messenger') . '[]',
        ]) ?>
    </div>

    <div class="col-lg-2">
        <div class="form-group">
            <div class="control-label">
                <label>&nbsp;</label>
            </div>
            <?= Button::widget([
                'type' => 'button',
                'icon' => WbIcon::MINUS,
                'style' => Button::STYLE_DANGER . ' btn-transport btn-transport-minus',
            ]) ?>

            <?= Button::widget([
                'type' => 'button',
                'icon' => WbIcon::PLUS,
                'style' => Button::STYLE_SUCCESS . ' btn-transport btn-transport-plus',
            ]) ?>

        </div>
    </div>

</div>


