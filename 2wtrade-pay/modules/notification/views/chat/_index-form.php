<?php
use app\components\widgets\ActiveForm;
use app\modules\notification\models\ChatTransport;
use yii\helpers\Url;

/** @var app\modules\notification\models\search\ChatSearch $modelSearch */
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute('index'),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>
<div class="row">
    <div class="col-lg-3">
        <?= $form->field($modelSearch, 'chat_number')->textInput() ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($modelSearch, 'messenger')->select2List(ChatTransport::MESSENGERS, ['prompt' => '—']) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($modelSearch, 'name')->textInput() ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')); ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
