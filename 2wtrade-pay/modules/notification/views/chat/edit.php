<?php

use app\widgets\Panel;
use yii\helpers\Url;
use app\modules\catalog\widgets\Country;
use app\modules\notification\assets\TransportAsset;

/** @var \yii\web\View $this */
/** @var app\modules\notification\models\Chat $model */
/** @var array $groups */
/** @var array $countries */
/** @var string $controller */
/** @var array $countriesByChat */
/** @var app\models\Country[] $countryList */
/** @var app\modules\notification\models\ChatTransport[] $transports */

TransportAsset::register($this);

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление чата') : Yii::t('common', 'Редактирование чата');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Чаты'), 'url' => Url::toRoute('/notification/chat/index')];
$this->params['breadcrumbs'][] = ['label' => $model->isNewRecord ? Yii::t('common', 'Добавление чата') : Yii::t('common', 'Редактирование чата')];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Чат'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
        'groups' => $groups,
        'countries' => $countries,
        'transports' => $transports,
    ]),
]); ?>

<?php if (!$model->isNewRecord): ?>
    <?= Panel::widget([
        'title' => Yii::t('common', 'Список стран'),
        'collapse' => true,
        'withBody' => false,
        'content' => Country::widget([
            'model' => $model,
            'controller' => $controller,
            'countryList' => $countryList,
            'countriesByModel' => $countriesByChat,
        ]),
    ]) ?>
<?php endif; ?>
