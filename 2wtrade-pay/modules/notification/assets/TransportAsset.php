<?php
namespace app\modules\notification\assets;

use yii\web\AssetBundle;

/**
 * Class TransportAsset
 * @package app\modules\reportoperational\assets
 */
class TransportAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/notification';

    public $js = [
        'transport.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}