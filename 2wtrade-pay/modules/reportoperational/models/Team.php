<?php

namespace app\modules\reportoperational\models;

use app\modules\reportoperational\models\query\TeamQuery;
use Yii;
use app\models\Country;
use app\models\User;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%report_operational_settings_team}}".
 *
 * @property integer $id
 * @property string $name
 * @property integer $leader_id         // равно user.id
 * @property integer $leader_name
 * @property integer $created_at
 * @property integer $updated_at
 */
class Team extends ActiveRecord
{
    /** @var \app\models\Country[] */
    public $countries;

    /** @var array */
    public $country_ids;

    /**
     * @inheritdoc
     */
    public function init()
    {
        if (isset(Yii::$app->user->id)) {
            $allowCountries = array_keys(User::getAllowCountries());
            $this->countries = Country::find()
                ->where(['IN', Country::tableName() . '.id', $allowCountries])
                ->andWhere(['active' => 1])
                ->orderBy(['name' => SORT_ASC]);
        }
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%report_operational_settings_team}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'country_ids'], 'required'],
            [['leader_id', 'created_at', 'updated_at'], 'integer'],
            [['name', 'leader_name'], 'string', 'max' => 255],
            [['country_ids'], 'safe'],
            [['country_ids'], 'validateCountry'],
        ];
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateCountry($attribute, $params)
    {
        $foundCountry = null;
        if ($this->isNewRecord) {
            $foundCountry = TeamCountry::find()
                ->where(['country_id' => $this->country_ids])
                ->limit(1)->one();
        } else {
            $foundCountry = TeamCountry::find()
                ->where(['!=', 'team_id', $this->id])
                ->andWhere(['country_id' => $this->country_ids])
                ->limit(1)->one();
        }
        if ($foundCountry !== null) {
            $this->addError($attribute, Yii::t('common', 'Одна или несколько стран уже есть у региона {command}!', ['command' => $foundCountry->team->name]));
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Индекс'),
            'name' => Yii::t('common', 'Наименование'),
            'leader_id' => Yii::t('common', 'Ответственный'),
            'leader_name' => Yii::t('common', 'ФИО ответственного'),
            'created_at' => Yii::t('common', 'Создано'),
            'updated_at' => Yii::t('common', 'Обновлено'),
            'country_ids' => Yii::t('common', 'Страна'),
        ];
    }

    /**
     * @inheritdoc
     * @return TeamQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TeamQuery(get_called_class());
    }

    /**
     * возвращает команды
     * @return Team[]|array
     */
    public static function getAllTeams()
    {
        return self::find()
            ->asArray()
            ->all();
    }
}
