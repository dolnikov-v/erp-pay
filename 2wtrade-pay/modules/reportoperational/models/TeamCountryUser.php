<?php

namespace app\modules\reportoperational\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\Country;
use app\models\User;
use app\modules\reportoperational\models\query\TeamCountryUserQuery;
use Yii;

/**
 * This is the model class for table "report_operational_settings_team_country_user".
 *
 * @property integer $user_id
 * @property integer $team_id
 * @property integer $country_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Country $country
 * @property Team $team
 * @property User $user
 */
class TeamCountryUser extends ActiveRecordLogUpdateTime
{

    /** @var \app\models\Country[] */
    public $countries;

    /** @var array */
    public $country_ids;

    /**
     * @param integer $teamId
     */
    function __construct($teamId = null)
    {
        if ($teamId) {
            $this->team_id = $teamId;
        }
        parent::__construct();
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        if ($this->team_id) {
            $teamCountries = TeamCountry::find()
                ->where(['team_id' => $this->team_id])
                ->all();
            if (is_array($teamCountries)) {
                $countries = null;
                foreach ($teamCountries as $teamCountry) {
                    $countries[] = $teamCountry->country_id;
                }
                $this->countries = Country::find()
                    ->where(['IN', Country::tableName() . '.id', $countries])
                    ->orderBy(['name' => SORT_ASC]);
            }
        }
        parent::init();
    }

    /**
     * @param integer $teamId
     * @param array $teamCountries
     * @return TeamCountryUser[].
     */
    public static function prepareCountries($teamId, $teamCountries)
    {
        $users = self::find()->byTeam($teamId)->groupBy(['user_id'])->all();
        if (is_array($users)) {
            foreach ($users as $key => $value) {
                $dataCountries = self::find()
                    ->where(['user_id' => $users[$key]->user_id])
                    ->andWhere(['team_id' => $users[$key]->team_id])
                    ->all();
                if (is_array($dataCountries)) {
                    foreach ($dataCountries as $dataCountry) {
                        $users[$key]->country_ids[] = $dataCountry->country_id;
                    }
                }
                $users[$key]->countries = Country::find()
                    ->where(['IN', Country::tableName() . '.id', $teamCountries])
                    ->orderBy(['name' => SORT_ASC]);
            }
        }
        return $users;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%report_operational_settings_team_country_user}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['user_id', 'team_id', 'country_id', 'created_at', 'updated_at'], 'required'],
            [['user_id', 'team_id', 'country_id', 'created_at', 'updated_at'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('common', 'Пользователь'),
            'team_id' => Yii::t('common', 'Регион'),
            'country_id' => Yii::t('common', 'Страна'),
            'created_at' => Yii::t('common', 'Создано'),
            'updated_at' => Yii::t('common', 'Обновлено'),
            'country_ids' => Yii::t('common', 'Страны'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeam()
    {
        return $this->hasOne(Team::className(), ['id' => 'team_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return TeamCountryUserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TeamCountryUserQuery(get_called_class());
    }

    /**
     * @param array $post
     * @throws \Throwable
     * @return boolean
     */
    public static function saveFromPost($post)
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            if (!$post) {
                return true;
            }

            $teamId = null;
            if (isset($post['team_id'])) {
                $teamId = $post['team_id'];
            } else {
                return true;
            }

            if (!isset($post['user_id'])) {
                return true;
            }

            if (!is_array($post['user_id'])) {
                return true;
            }

            self::deleteAll(['team_id' => $teamId]);
            $arrayForInsert = null;
            $now = time();
            foreach ($post['user_id'] as $userId) {
                if ($userId == 0 || !is_array($post['country_ids_' . $userId])) {
                    continue;
                }
                foreach ($post['country_ids_' . $userId] as $countryOne) {
                    $arrayForInsert[] = [$userId, $teamId, $countryOne, $now, $now];
                }
            }

            if (is_array($arrayForInsert)) {
                Yii::$app->db->createCommand()->batchInsert(
                    self::tableName(),
                    [
                        'user_id',
                        'team_id',
                        'country_id',
                        'created_at',
                        'updated_at'
                    ],
                    $arrayForInsert
                )->execute();
            }
            $transaction->commit();
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
        return true;
    }
}
