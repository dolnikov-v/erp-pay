<?php

namespace app\modules\reportoperational\models;

use app\models\CalculatedParam;
use app\models\Country;
use app\modules\checklist\models\CheckList;
use app\modules\checklist\models\CheckListItem;
use app\modules\reportoperational\models\query\TeamCountryQuery;
use app\components\db\ActiveRecordLogUpdateTime;
use yii\db\Expression;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "report_operational_settings_team_country".
 *
 * @property integer $team_id
 * @property integer $country_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Country $country
 * @property Team $team
 */
class TeamCountry extends ActiveRecordLogUpdateTime
{
    /**
     * @var array
     */
    public static $currencies;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%report_operational_settings_team_country}}';
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if (!is_array(self::$currencies)) {
            $currency = Country::find()
                ->joinWith(['currencyRate']);

            self::$currencies = ArrayHelper::map($currency->all(), 'id', 'currencyRate.rate');
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['team_id', 'country_id', 'created_at', 'updated_at'], 'required'],
            [['team_id', 'country_id', 'created_at', 'updated_at'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'team_id' => 'Team ID',
            'country_id' => 'Country ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeam()
    {
        return $this->hasOne(Team::className(), ['id' => 'team_id']);
    }

    /**
     * @inheritdoc
     * @return TeamCountryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TeamCountryQuery(get_called_class());
    }

    /**
     * данные для виджета Efficiency (Эффективность команд)
     * @return array
     */
    public static function getEfficiencyData()
    {
        // данные за последнюю дату, которая есть в БД
        $maxDateArray = CheckList::find()->select(['max_date' => 'MAX(date)'])->asArray()->one();
        $maxDate = $maxDateArray['max_date'];
        $result = self::find()
            ->joinWith(['team', 'country'])
            ->select([
                'team_name' => Team::tableName() . '.name',
                'team_id' => self::tableName() . '.team_id',
                'leader_id' => Team::tableName() . '.leader_id',
                'country_name' => Country::tableName() . '.name',
                'country_id' => self::tableName() . '.country_id',
                'efficiency_check_all' => CheckListItem::find()
                    ->select([new Expression('count(1)')])
                    ->where(['type' => CheckListItem::TYPE_PAY])
                    ->andWhere(['active' => 1]),
                'efficiency_check_ok' => CheckList::find()
                    ->select([new Expression('count(1)')])
                    ->where(['country_id' => new Expression(self::tableName() . '.country_id')])
                    ->andWhere(['team_id' => new Expression(self::tableName() . '.team_id')])
                    ->andWhere(['date' => $maxDate])
                    ->andWhere(['status' => CheckList::STATUS_OK]),
                'percent_approve' => CalculatedParam::find()
                    ->select(['AVG(value * 100)'])
                    ->where(['name' => CalculatedParam::PERCENT_APPROVED])
                    ->andWhere(['country_id' => new Expression(self::tableName() . '.country_id')]),
                'avg_check_approve' => CalculatedParam::find()
                    ->select(['AVG(value)'])
                    ->where(['name' => CalculatedParam::AVG_CHECK_APPROVED])
                    ->andWhere(['country_id' => new Expression(self::tableName() . '.country_id')]),
            ])
            ->orderBy('team_id, country_name')
            ->asArray()->all();

        $teamData = null;
        if (is_array($result)) {
            foreach ($result as $item) {
                if (isset($teamData[$item['team_id']])) {
                    $teamData[$item['team_id']]['efficiency_all'] += $item['efficiency_check_all'];
                    $teamData[$item['team_id']]['efficiency_ok'] += $item['efficiency_check_ok'];
                } else {
                    $teamData[$item['team_id']] = [
                        'name' => $item['team_name'],
                        'leader_id' => $item['leader_id'],
                        'efficiency_all' => ($item['efficiency_check_all'] ? $item['efficiency_check_all'] : 0),
                        'efficiency_ok' => ($item['efficiency_check_ok'] ? $item['efficiency_check_ok'] : 0),
                    ];
                }
            }
        } else {
            $result = [];
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $result,
        ]);

        return [
            'dataProvider' => $dataProvider,
            'teamData' => $teamData
        ];
    }

    /**
     * возвращаем список стран команды за исключением запрещенных
     * @param $teamId
     * @return TeamCountry[]|array
     */
    public static function getTeamCountriesList($teamId) {

        return self::find()
            ->joinWith('country')
            ->select(['country_id'])
            ->where(['!=', Country::tableName() . '.is_stop_list', 1])
            ->andWhere(['team_id' => $teamId])
            ->column();
    }

    /**
     * возвращаем список стран команды по именам за исключением запрещенных
     * @param $teamId
     * @return TeamCountry[]|array
     */
    public static function getTeamCountriesNamesList($teamId) {

        return self::find()
            ->joinWith('country')
            ->select(['name'])
            ->where(['!=', Country::tableName() . '.is_stop_list', 1])
            ->andWhere(['=', Country::tableName() . '.active', 1])
            ->andWhere(['team_id' => $teamId])
            ->column();
    }
}
