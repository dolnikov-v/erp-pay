<?php

namespace app\modules\reportoperational\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%report_operational_settings_countries}}".
 *
 * @property integer $id
 * @property integer $country_id
 * @property string $char_code
 * @property string $country_slug
 * @property string $country_name
 * @property integer $status
 * @property integer $sms_status
 * @property integer $partner_buyout_percent
 * @property integer $team
 * @property integer $leader_id
 * @property string $leader_name
 * @property integer $created_at
 * @property integer $updated_at
 */
class Countries extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%report_operational_settings_countries}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'char_code', 'country_slug', 'country_name', 'created_at', 'updated_at'], 'required'],
            [['country_id', 'status', 'sms_status', 'partner_buyout_percent', 'team', 'leader_id', 'created_at', 'updated_at'], 'integer'],
            [['char_code', 'country_slug'], 'string', 'max' => 10],
            [['country_name'], 'string', 'max' => 32],
            [['leader_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Индекс'),
            'country_id' => Yii::t('common', 'Индекс страны'),
            'char_code' => Yii::t('common', 'Код страны'),
            'country_slug' => Yii::t('common', 'Абр. страны'),
            'country_name' => Yii::t('common', 'Наименование страны'),
            'status' => Yii::t('common', 'Статус'),
            'sms_status' => Yii::t('common', 'SMS'),
            'partner_buyout_percent' => Yii::t('common', '% выкупа партнера'),
            'team' => Yii::t('common', 'Регион'),
            'leader_id' => Yii::t('common', 'Ответственный'),
            'leader_name' => Yii::t('common', 'ФИО ответственного'),
            'created_at' => Yii::t('common', 'Создано'),
            'updated_at' => Yii::t('common', 'Обновлено'),
        ];
    }

    /**
     * @inheritdoc
     * @return \app\modules\reportoperational\models\query\CountriesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\modules\reportoperational\models\query\CountriesQuery(get_called_class());
    }


    /**
     * возвращаем список запрещенных стран
     * @return array
     */
    public static function getRejectedCountries() {

        return ArrayHelper::getColumn(self::find()
            ->select(['country_id'])
            ->where(['=', Countries::tableName() . '.sms_status', 0])
            ->asArray()
            ->all(), 'country_id');
    }


    /**
     * возвращаем список стран команды за исключением запрещенных
     * @param $teamId
     * @return Countries[]|array
     */
    public static function getTeamCountriesList($teamId) {

        return ArrayHelper::getColumn(self::find()
            ->select(['country_id'])
            ->where(['!=', Countries::tableName() . '.sms_status', 0])
            ->andWhere([Countries::tableName() .'.team' => $teamId])
            ->all(), 'country_id');
    }
}
