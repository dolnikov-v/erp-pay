<?php

namespace app\modules\reportoperational\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\reportoperational\models\Countries;

/**
 * CountriesSearch represents the model behind the search form about `app\modules\reportoperational\models\Countries`.
 */
class CountriesSearch extends Countries
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'country_id', 'status', 'sms_status', 'partner_buyout_percent', 'team', 'leader_id', 'created_at', 'updated_at'], 'integer'],
            [['char_code', 'country_slug', 'country_name', 'leader_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Countries::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'country_id' => $this->country_id,
            'status' => $this->status,
            'sms_status' => $this->sms_status,
            'partner_buyout_percent' => $this->partner_buyout_percent,
            'team' => $this->team,
            'leader_id' => $this->leader_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'char_code', $this->char_code])
            ->andFilterWhere(['like', 'country_slug', $this->country_slug])
            ->andFilterWhere(['like', 'country_name', $this->country_name])
            ->andFilterWhere(['like', 'leader_name', $this->leader_name]);

        return $dataProvider;
    }
}
