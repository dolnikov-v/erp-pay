<?php

namespace app\modules\reportoperational\models\query;

/**
 * This is the ActiveQuery class for [[\app\modules\reportoperational\models\Team]].
 *
 * @see \app\modules\reportoperational\models\Team
 */
class TeamQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \app\modules\reportoperational\models\Team[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\modules\reportoperational\models\Team|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
