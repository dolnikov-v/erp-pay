<?php

namespace app\modules\reportoperational\models\query;

use app\modules\reportoperational\models\TeamCountry;
use app\components\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[TeamCountry]].
 *
 * @see ReportOperationalSettingsTeamCountry
 */
class TeamCountryQuery extends ActiveQuery
{
    /**
     * @inheritdoc
     * @return TeamCountry[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TeamCountry|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}