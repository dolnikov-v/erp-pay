<?php

namespace app\modules\reportoperational\models\query;

/**
 * This is the ActiveQuery class for [[\app\modules\reportoperational\models\Countries]].
 *
 * @see \app\modules\reportoperational\models\Countries
 */
class CountriesQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \app\modules\reportoperational\models\Countries[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\modules\reportoperational\models\Countries|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
