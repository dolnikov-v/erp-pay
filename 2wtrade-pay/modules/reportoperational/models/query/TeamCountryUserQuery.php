<?php

namespace app\modules\reportoperational\models\query;

use app\modules\reportoperational\models\TeamCountryUser;

/**
 * This is the ActiveQuery class for [[TeamCountryUser]].
 *
 * @see TeamCountryUser
 */
class TeamCountryUserQuery extends \yii\db\ActiveQuery
{
    /**
     * @param $code
     * @return $this
     */
    public function byTeam($team_id)
    {
        return $this->andWhere(['team_id' => $team_id]);
    }

    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TeamCountryUser|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
