<?php
namespace app\modules\reportoperational\assets;

use yii\web\AssetBundle;

/**
 * Class TeamUserAsset
 * @package app\modules\reportoperational\assets
 */
class TeamUserAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/reportoperational';

    public $js = [
        'team-user.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}