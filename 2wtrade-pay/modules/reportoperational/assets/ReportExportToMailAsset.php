<?php
namespace app\modules\reportoperational\assets;

use yii\web\AssetBundle;

/**
 * Class ReportExportToMailAsset
 * @package app\modules\reportoperational\assets
 */
class ReportExportToMailAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/report/export-report-to-mail';

    public $js = [
        'export-report-to-mail.js',
    ];

    public $depends = [
        'app\assets\SiteAsset',
    ];
}