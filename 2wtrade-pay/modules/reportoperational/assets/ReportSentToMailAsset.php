<?php

namespace app\modules\reportoperational\assets;

use yii\web\AssetBundle;

/**
 * Class ReportSentToMailAsset
 * @package app\modules\reportoperational\assets
 */
class ReportSentToMailAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/report';

    public $css = [
        'report-sent-to-mail.css'
    ];

    public $js = [
        'report-sent-to-mail.js'
    ];

    public $depends = [
        'app\assets\SiteAsset',
    ];
}
