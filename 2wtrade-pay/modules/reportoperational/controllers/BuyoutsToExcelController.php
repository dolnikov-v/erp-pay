<?php
namespace app\modules\reportoperational\controllers;

use app\components\web\Controller;
use app\helpers\Utils;
use app\modules\reportoperational\components\ReportFormBuyoutsToExcel;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class BuyoutsToExcelController
 * @package app\modules\reportoperational\controllers
 */
class BuyoutsToExcelController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        date_default_timezone_set("UTC");

        $reportForm = new ReportFormBuyoutsToExcel();
        $dataProvider = $reportForm->apply(Yii::$app->request->queryParams);
        $dateFilter = $reportForm->getDateFilter();

        $first = '';
        if ($dateFilter->from == $dateFilter->to) {
            $first = $dateFilter->from;
        }
        else {
            $first = ' ' .Yii::t('common', 'период') .'&nbsp;' .$dateFilter->from .' - ' .$dateFilter->to;
        }

        $firstInterval = Yii::t('common', 'Выкуп за') .' ' .$first .'&nbsp;&nbsp;&nbsp;' .Yii::t('common', 'Тип') .': "' .$dateFilter->types[$dateFilter->type] .'" от ' .Yii::$app->formatter->asDate(time());

        return $this->render('index', [
            'reportForm' => $reportForm,
            'firstInterval' => $firstInterval,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Экспортируем отчет в Excel
     */
    public function actionExportToExcel()
    {
        date_default_timezone_set("UTC");

        $reportForm = new ReportFormBuyoutsToExcel();
        $dataProvider = $reportForm->apply(Yii::$app->request->queryParams);
        $dateFilter = $reportForm->getDateFilter();

        foreach ($dataProvider->allModels as &$modelItem) {
            $modelItem['day'] = Yii::$app->formatter->asDate($modelItem['day']);
        }

        $excel = new \PHPExcel();
        $sheet = $excel->getSheet(0);
        $sheet->setTitle(Yii::t('common', 'Выкуп по странам'));

        $row = 1;
        $sheet->setCellValueByColumnAndRow(0, $row, 'data_timestamp');
        $sheet->setCellValueByColumnAndRow(1, $row, Yii::t('common', 'Тип'));
        $sheet->setCellValueByColumnAndRow(2, $row, Yii::t('common', 'Дата'));
        $sheet->setCellValueByColumnAndRow(3, $row, Yii::t('common', 'Страна'));
        $sheet->setCellValueByColumnAndRow(4, $row, Yii::t('common', '% Выкупа'));
        $sheet->setCellValueByColumnAndRow(5, $row, Yii::t('common', 'Выкуплено'));
        $sheet->setCellValueByColumnAndRow(6, $row, Yii::t('common', 'Не выкуплено'));
        $sheet->setCellValueByColumnAndRow(7, $row, Yii::t('common', 'В процессе'));
        $sheet->setCellValueByColumnAndRow(8, $row, Yii::t('common', 'Всего'));
        $sheet->setCellValueByColumnAndRow(9, $row, Yii::t('common', 'Ср. чек выкупа($)'));
        $sheet->getStyleByColumnAndRow(0, $row, 9, $row)->getFont()->setBold(true);
        $sheet->getStyleByColumnAndRow(0, 2, 0, 1000)->getNumberFormat()->setFormatCode('dd/mm/yyyy');
        $sheet->getStyleByColumnAndRow(2, 2, 2, 1000)->getNumberFormat()->setFormatCode('dd/mm/yyyy');
        $sheet->getStyleByColumnAndRow(2, 2, 2, 1000)->getAlignment()->setHorizontal('right');
        $row++;

        foreach ($dataProvider->allModels as $items) {
            $datatime = Yii::$app->formatter->asDatetime(time());
            $typeDate = $reportForm->groupByCollection[$dateFilter->type];

            $sheet->setCellValueByColumnAndRow(0, $row, $datatime);
            $sheet->setCellValueByColumnAndRow(1, $row, $typeDate);
            $sheet->setCellValueByColumnAndRow(2, $row, $items['day']);
            $sheet->setCellValueByColumnAndRow(3, $row, $items['country_name']);
            $sheet->setCellValueByColumnAndRow(4, $row, round($items['vikup_percent'], 2));
            $sheet->setCellValueByColumnAndRow(5, $row, $items['vykupleno']);
            $sheet->setCellValueByColumnAndRow(6, $row, $items['nevykupleno']);
            $sheet->setCellValueByColumnAndRow(7, $row, $items['vprocesse']);
            $sheet->setCellValueByColumnAndRow(8, $row, $items['vsego']);
            $sheet->setCellValueByColumnAndRow(9, $row, round($items['sr_vykup'], 2));

            $row++;
        }

        $dir = Yii::getAlias("@downloads") .DIRECTORY_SEPARATOR ."excels" .DIRECTORY_SEPARATOR ."operationalreports" .DIRECTORY_SEPARATOR ."buyouts";
        Utils::prepareDir($dir);
        $filename = $dir .DIRECTORY_SEPARATOR .date('Y-m-d', time()) .".xlsx";

        $objWriter = \PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        $objWriter->save($filename);

        Yii::$app->response->sendFile($filename);
    }
}
