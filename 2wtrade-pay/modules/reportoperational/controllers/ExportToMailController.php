<?php
namespace app\modules\reportoperational\controllers;

use app\components\web\Controller;
use app\components\filters\AjaxFilter;
use app\helpers\Utils;
use kartik\mpdf\Pdf;

use Yii;


/**
 * Class ExportToMailController
 * @package app\modules\reportoperational\controllers
 */
class ExportToMailController extends Controller
{

    private $report;
    private $content;


    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => [
                    'sent',
                ],
            ]
        ];
    }

    /**
     * @return string
     */
    public function actionSent()
    {
        $request = Yii::$app->request;
        $this->report = $request->post('report');
        $this->content = $request->post('export_content');

        $mailsStr = $request->post('mails', Yii::t('common', 'No email found'));
        $mailsBeforeValidate = explode('×', $mailsStr);
        $mails = [];

        foreach ($mailsBeforeValidate as $item) {
            if (mb_strpos($item, '@')) {
                $mails[] = $item;
            }
        }

        $result = $this->sender($mails);

        if (empty($result)) {
            return 'success';
        }

        return $result;
    }

    private function sender($mails) {

        $errorList = [];

        foreach ($mails as $recipient) {
            $sent = Yii::$app->mailer->compose()
                ->setFrom('no-reply@2wtrade-pay.com')
                ->setTo($recipient)
                ->setSubject($this->report .'_' .Yii::$app->formatter->asDate(time()))
                ->setHtmlBody($this->content)
                ->send();
            if (!$sent) {
                $errorList[] = $recipient;
            }
        }

        return $errorList;
    }

}
