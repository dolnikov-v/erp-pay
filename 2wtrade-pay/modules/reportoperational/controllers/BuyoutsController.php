<?php
namespace app\modules\reportoperational\controllers;

use app\components\web\Controller;
use app\modules\reportoperational\components\ReportFormBuyouts;
use Yii;

/**
 * Class BuyoutsController
 * @package app\modules\reportoperational\controllers
 */
class BuyoutsController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        date_default_timezone_set("UTC");

        $reportForm = new ReportFormBuyouts();
        $dataProvider = $reportForm->apply(Yii::$app->request->queryParams);

        $dateFilter = $reportForm->getDateTwoIntervalsFilter();
        $first = '';
        if ($dateFilter->from_first == $dateFilter->to_first) {
            $first = $dateFilter->from_first;
        }
        else {
            $first = ' ' .Yii::t('common', 'период') .'&nbsp;' .$dateFilter->from_first .' - ' .$dateFilter->to_first;
        }

        $second = '';
        if ($dateFilter->from_second == $dateFilter->to_second) {
            $second = $dateFilter->from_second;
        }
        else {
            $second = ' ' .Yii::t('common', 'период') .'&nbsp;' .$dateFilter->from_second .' - ' .$dateFilter->to_second;
        }

        $caption = Yii::t('common', 'Сравнение выкупов за') .' '
            .$first .' и'
            .$second .' ('
            .Yii::t('common', 'Тип') .': "' .$dateFilter->types[$dateFilter->type]
            .'" от ' .Yii::$app->formatter->asDate(time())
            .')';


        return $this->render('index', [
            'reportForm' => $reportForm,
            'caption' => $caption,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Экспортируем отчет в Excel
     */
    public function actionExportToExcel()
    {
        date_default_timezone_set("UTC");

        $reportForm = new ReportFormBuyouts();
        $dataProvider = $reportForm->apply(Yii::$app->request->queryParams);

        $dateFilter = $reportForm->getDateTwoIntervalsFilter();

        $excel = new \PHPExcel();
        $sheet = $excel->getSheet(0);
        $sheet->setTitle(Yii::t('common', 'Выкуп по странам'));

        $row = 1;
        $sheet->setCellValueByColumnAndRow(0, $row, 'data_timestamp');
        $sheet->setCellValueByColumnAndRow(1, $row, Yii::t('common', 'Тип'));
        $sheet->setCellValueByColumnAndRow(2, $row, Yii::t('common', 'Дата'));
        $sheet->setCellValueByColumnAndRow(3, $row, Yii::t('common', 'Регион'));
        $sheet->setCellValueByColumnAndRow(4, $row, Yii::t('common', 'Страна'));
        $sheet->setCellValueByColumnAndRow(5, $row, Yii::t('common', '% Выкупа'));
        $sheet->setCellValueByColumnAndRow(6, $row, Yii::t('common', 'Выкуплено'));
        $sheet->setCellValueByColumnAndRow(7, $row, Yii::t('common', 'Не выкуплено'));
        $sheet->setCellValueByColumnAndRow(8, $row, Yii::t('common', 'В процессе'));
        $sheet->setCellValueByColumnAndRow(9, $row, Yii::t('common', 'Всего'));
        $sheet->setCellValueByColumnAndRow(10, $row, Yii::t('common', 'Ср. чек выкупа($)'));
        $sheet->getStyleByColumnAndRow(0, $row, 10, $row)->getFont()->setBold(true);
        $sheet->getStyleByColumnAndRow(0, 2, 0, 1000)->getNumberFormat()->setFormatCode('dd/mm/yyyy');
        $sheet->getStyleByColumnAndRow(2, 2, 2, 1000)->getNumberFormat()->setFormatCode('dd/mm/yyyy');
        $sheet->getStyleByColumnAndRow(2, 2, 2, 1000)->getAlignment()->setHorizontal('right');
        $row++;

        foreach ($dataProvider->allModels as $items) {
            $datatime = Yii::$app->formatter->asDatetime(time());
            $typeDate = $reportForm->groupByCollection[$dateFilter->type];

            $sheet->setCellValueByColumnAndRow(0, $row, $datatime);
            $sheet->setCellValueByColumnAndRow(1, $row, $typeDate);
            $sheet->setCellValueByColumnAndRow(2, $row, Yii::$app->formatter->asDate($items['day']));
            $sheet->setCellValueByColumnAndRow(3, $row, $items['team']);
            $sheet->setCellValueByColumnAndRow(4, $row, $items['country_name']);
            $sheet->setCellValueByColumnAndRow(5, $row, round($items['vikup_percent_1'], 2) .' | ' .round($items['vikup_percent_2'], 2));
            $sheet->setCellValueByColumnAndRow(6, $row, $items['vykupleno_1'] .' | ' .$items['vykupleno_2']);
            $sheet->setCellValueByColumnAndRow(7, $row, $items['nevykupleno_1'] .' | ' .$items['nevykupleno_2']);
            $sheet->setCellValueByColumnAndRow(8, $row, $items['vprocesse_1'] .' | ' .$items['vprocesse_2']);
            $sheet->setCellValueByColumnAndRow(9, $row, $items['vsego_1'] .' | ' .$items['vsego_2']);
            $sheet->setCellValueByColumnAndRow(10, $row, round($items['sr_vykup_1'], 2) .' | ' .round($items['sr_vykup_2'], 2));

            $row++;
        }

        $dir = Yii::getAlias("@downloads") .DIRECTORY_SEPARATOR ."excels" .DIRECTORY_SEPARATOR ."operationalreports";
        $filename = $dir .DIRECTORY_SEPARATOR .date('Y-m-d', time()) .'_' .Yii::t('common', 'operational_buyouts') .".xlsx";

        $objWriter = \PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        $objWriter->save($filename);

        Yii::$app->response->sendFile($filename);
    }
}
