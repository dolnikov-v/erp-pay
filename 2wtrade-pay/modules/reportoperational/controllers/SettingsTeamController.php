<?php

namespace app\modules\reportoperational\controllers;

use app\components\Notifier;
use app\components\web\Controller;
use Yii;
use app\modules\reportoperational\models\Team;
use app\modules\reportoperational\models\TeamCountry;
use app\modules\reportoperational\models\TeamCountryUser;
use app\modules\reportoperational\models\search\TeamSearch;
use app\models\User;
use yii\web\NotFoundHttpException;

/**
 * SettingsTeamController implements the CRUD actions for Team model.
 */
class SettingsTeamController extends Controller
{

    /**
     * Lists all Team models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TeamSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Team model.
     * If creation is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Team();
        $model->created_at = time();
        $model->updated_at = $model->created_at;

        $leaders = $this->getTeamLeadersArray();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $teamCountries = $model->country_ids;
            $this->insertTeamCountries($teamCountries, $model);
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
            return $this->render('create', [
                'model' => $model,
                'leaders' => $leaders,
            ]);
        }
    }

    /**
     * Updates an existing Team model.
     * If update is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->updated_at = time();
        $teamCountries = TeamCountry::find()
            ->where(['team_id' => $model->id])
            ->all();
        if (is_array($teamCountries)) {
            foreach ($teamCountries as $teamCountry) {
                $model->country_ids[] = $teamCountry->country_id;
            }
        }

        $leaders = $this->getTeamLeadersArray();
        $teamCountries = $model->country_ids;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $teamCountries = $model->country_ids;
            $this->insertTeamCountries($teamCountries, $model);
            Yii::$app->notifier->addNotification(Yii::t('common', 'Настройки региона успешно обновлены'), Notifier::TYPE_SUCCESS);
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }
        $teamUsers = TeamCountryUser::prepareCountries($model->id, $teamCountries);
        return $this->render('update', [
            'model' => $model,
            'leaders' => $leaders,
            'users' => User::find()->collection(),
            'modelTeamUsers' => $teamUsers
        ]);
    }

    /**
     * Обновление состава команды
     * @return mixed
     */
    public function actionEditUsers()
    {
        if (!TeamCountryUser::saveFromPost(Yii::$app->request->post('TeamCountryUser'))) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Ошибки на вкладке Регион'));
        } else {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Состав региона успешно обновлен.'), Notifier::TYPE_SUCCESS);
        }
        return $this->redirect(array_merge(Yii::$app->request->queryParams, ['update']));
    }

    /**
     * Deletes an existing Team model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        TeamCountry::deleteAll(['team_id' => $id]);
        TeamCountryUser::deleteAll(['team_id' => $id]);

        return $this->redirect(['index']);
    }

    /**
     * Finds the Team model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Team the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Team::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * возращаем список тер.мен-ов
     * @return mixed
     */
    private function getTeamLeadersArray()
    {

        $countryCuratorsIds = Yii::$app->authManager->getUserIdsByRole('country.curator');

        $users = User::find()
            ->where(['=', User::tableName() . '.status', User::STATUS_DEFAULT])
            ->andWhere(['IN', User::tableName() . '.id', $countryCuratorsIds])
            ->orderBy(['username' => SORT_ASC])
            ->all();

        $res[0] = '-';
        foreach ($users as $user) {
            $res[$user->id] = $user->username;
        }

        return $res;
    }

    /**
     * @param array $teamCountries
     * @param Team $model
     */
    private function insertTeamCountries($teamCountries, $model)
    {
        if (is_array($teamCountries)) {
            TeamCountry::deleteAll(['team_id' => $model->id]);
            $arrayForInsert = null;
            $now = time();
            foreach ($teamCountries as $teamCountry) {
                $arrayForInsert[] = [$model->id, $teamCountry, $now, $now];
            }
            if (is_array($arrayForInsert)) {
                Yii::$app->db->createCommand()->batchInsert(
                    TeamCountry::tableName(),
                    [
                        'team_id',
                        'country_id',
                        'created_at',
                        'updated_at'
                    ],
                    $arrayForInsert
                )->execute();
            }
        }
    }
}
