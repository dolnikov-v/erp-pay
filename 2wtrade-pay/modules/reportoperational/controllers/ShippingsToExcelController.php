<?php
namespace app\modules\reportoperational\controllers;

use app\components\web\Controller;
use app\helpers\Utils;
use yii\helpers\ArrayHelper;
use app\modules\reportoperational\components\ReportFormShippingsToExcel;
use Yii;

/**
 * Class ShippingsToExcelController
 * @package app\modules\reportoperational\controllers
 */
class ShippingsToExcelController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        date_default_timezone_set("UTC");

        $reportForm = new ReportFormShippingsToExcel();
        $dataProvider = $reportForm->apply(Yii::$app->request->queryParams);
        $dateFilter = $reportForm->getDateFilter();

        $first = '';
        if ($dateFilter->from == $dateFilter->to) {
            $first = $dateFilter->from;
        }
        else {
            $first = ' ' .Yii::t('common', 'период') .'&nbsp;' .$dateFilter->from .' - ' .$dateFilter->to;
        }
        $caption = Yii::t('common', 'Отгрузки за') .' ' .$first .'&nbsp;&nbsp;&nbsp;' .Yii::t('common', 'Тип') .': "' .$dateFilter->types[$dateFilter->type] .'" от ' .Yii::$app->formatter->asDate(time());

        return $this->render('index', [
            'reportForm' => $reportForm,
            'dataProvider' => $dataProvider,
            'caption' => $caption,
        ]);
    }

    /**
     * Экспортируем отчет в Excel
     */
    public function actionExportToExcel()
    {
        date_default_timezone_set("UTC");

        $reportForm = new ReportFormShippingsToExcel();
        $dataProvider = $reportForm->apply(Yii::$app->request->queryParams);
        $dateFilter = $reportForm->getDateFilter();

        foreach ($dataProvider->allModels as &$modelItem) {
            $modelItem['day'] = Yii::$app->formatter->asDate($modelItem['day']);
        }

        ArrayHelper::multisort($dataProvider->allModels, ['day'], [SORT_DESC]);

        $excel = new \PHPExcel();
        $sheet = $excel->getSheet(0);
        $sheet->setTitle(Yii::t('common', 'Отгрузки'));

        $row = 1;
        $sheet->setCellValueByColumnAndRow(0, $row, 'data_timestamp');
        $sheet->setCellValueByColumnAndRow(1, $row, Yii::t('common', 'Тип'));
        $sheet->setCellValueByColumnAndRow(2, $row, Yii::t('common', 'Дата'));
        $sheet->setCellValueByColumnAndRow(3, $row, Yii::t('common', 'Регион'));
        $sheet->setCellValueByColumnAndRow(4, $row, Yii::t('common', 'Страна'));
        $sheet->setCellValueByColumnAndRow(5, $row, Yii::t('common', '6. Одобрен'));
        $sheet->setCellValueByColumnAndRow(6, $row, Yii::t('common', '9. Логистика (лист сгенерирован, ожидаются этикетки)'));
        $sheet->setCellValueByColumnAndRow(7, $row, Yii::t('common', '12. Курьерка (принято)'));
        $sheet->setCellValueByColumnAndRow(8, $row, Yii::t('common', '13. Курьерка (покинуло склад)'));
        $sheet->setCellValueByColumnAndRow(9, $row, Yii::t('common', '15. Курьерка (выкуп)'));
        $sheet->setCellValueByColumnAndRow(10, $row, Yii::t('common', '16. Курьерка (отказ)'));
        $sheet->setCellValueByColumnAndRow(11, $row, Yii::t('common', '19. Курьерка (отклонено)'));
        $sheet->setCellValueByColumnAndRow(12, $row, Yii::t('common', '31. Курьерка (отправлен на доставку)'));
        $sheet->setCellValueByColumnAndRow(13, $row, Yii::t('common', '32. Логистика (отложено)'));
        $sheet->setCellValueByColumnAndRow(14, $row, Yii::t('common', 'Всего'));
        $sheet->getStyleByColumnAndRow(0, $row, 14, $row)->getFont()->setBold(true);
        $sheet->getStyleByColumnAndRow(0, 2, 0, 1000)->getNumberFormat()->setFormatCode('dd/mm/yyyy');
        $sheet->getStyleByColumnAndRow(2, 2, 2, 1000)->getNumberFormat()->setFormatCode('dd/mm/yyyy');
        $sheet->getStyleByColumnAndRow(2, 2, 2, 1000)->getAlignment()->setHorizontal('right');
        $row++;

        foreach ($dataProvider->allModels as $items) {
            $datatime = Yii::$app->formatter->asDatetime(time());
            $typeDate = $reportForm->groupByCollection[$dateFilter->type];

            $sheet->setCellValueByColumnAndRow(0, $row, $datatime);
            $sheet->setCellValueByColumnAndRow(1, $row, $typeDate);
            $sheet->setCellValueByColumnAndRow(2, $row, Yii::$app->formatter->asDate($items['day']));
            $sheet->setCellValueByColumnAndRow(3, $row, $items['team_name']);
            $sheet->setCellValueByColumnAndRow(4, $row, $items['country_name']);
            $sheet->setCellValueByColumnAndRow(5, $row, $items['6']);
            $sheet->setCellValueByColumnAndRow(6, $row, $items['9']);
            $sheet->setCellValueByColumnAndRow(7, $row, $items['12']);
            $sheet->setCellValueByColumnAndRow(8, $row, $items['13']);
            $sheet->setCellValueByColumnAndRow(9, $row, $items['15']);
            $sheet->setCellValueByColumnAndRow(10, $row, $items['16']);
            $sheet->setCellValueByColumnAndRow(11, $row, $items['19']);
            $sheet->setCellValueByColumnAndRow(12, $row, $items['31']);
            $sheet->setCellValueByColumnAndRow(13, $row, $items['32']);
            $sheet->setCellValueByColumnAndRow(14, $row, $items['vsego']);
            $row++;
        }

        $dir = Yii::getAlias("@downloads") .DIRECTORY_SEPARATOR ."excels" .DIRECTORY_SEPARATOR ."operationalreports" .DIRECTORY_SEPARATOR ."shippings";
        Utils::prepareDir($dir);
        $filename = $dir .DIRECTORY_SEPARATOR .date('Y-m-d', time()) .".xlsx";

        $objWriter = \PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        $objWriter->save($filename);

        Yii::$app->response->sendFile($filename);
    }
}
