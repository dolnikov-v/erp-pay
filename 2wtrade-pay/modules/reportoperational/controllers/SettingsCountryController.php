<?php

namespace app\modules\reportoperational\controllers;

use app\components\web\Controller;
use app\modules\reportoperational\models\Team;
use Yii;
use app\modules\reportoperational\models\Countries;
use app\components\Notifier;
use app\models\Country;
use app\models\User;
use yii\data\ArrayDataProvider;


/**
 * SettingsCountryController implements the CRUD actions for Countries model.
 */
class SettingsCountryController extends Controller
{

    /**
     * Lists all Countries.
     * @return mixed
     */
    public function actionIndex()
    {

        $allModels = Country::find()
            ->leftJoin(Countries::tableName(), Countries::tableName() .'.country_id=' .Country::tableName() .'.id')
            ->leftJoin(Team::tableName(), Team::tableName() .'.id=' .Countries::tableName() .'.team')
            ->select([
                'id' => Country::tableName() .'.id',
                'name' => Country::tableName() .'.name',
                'country_active' => Country::tableName() .'.active',
                'use_into_report' => Countries::tableName() .'.status',
                'use_into_sms' => Countries::tableName() .'.sms_status',
                'team_name' => Team::tableName() .'.name',
                'leader_name' => Countries::tableName() .'.leader_name',
            ])
            ->orderBy(['name' => SORT_ASC])
            ->asArray()
            ->all();

        $dataProvider = new ArrayDataProvider([
            'allModels' => $allModels,
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Updates an existing Countries model.
     * If update is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $teamsList = Team::find()->all();

        $leaders = $this->getTeamLeadersArray();

        $teams[0] = Yii::t('common', '-');
        foreach ($teamsList as $team) {
            $teams[$team->id] = $team->name;
        }

        // Если модель найдена - обновляем
        if ($model !== null) {
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                if (is_null($_POST['Countries']['status'])) {
                    $model->status = 0;
                    $model->save();
                }
                if (is_null($_POST['Countries']['sms_status'])) {
                    $model->sms_status = 0;
                    $model->save();
                }
                return $this->redirect(['index', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                    'teams' => $teams,
                    'leaders' => $leaders,
                ]);
            }
        }
        else {
            $country = Country::findOne($id);

            $model = new Countries();
            $model->country_id = $country->id;
            $model->char_code = $country->char_code;
            $model->country_slug = $country->slug;
            $model->country_name = $country->name;
            $model->created_at = time();
            $model->updated_at = $model->created_at;
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['index', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                    'teams' => $teams,
                    'leaders' => $leaders,
                ]);
            }
        }
    }

    /**
     * Deletes an existing Countries model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if ($model !== null) {
            $model->delete();
            Yii::$app->notifier->addNotification('Настройки страны успешно удалены', Notifier::TYPE_SUCCESS);
        }
        else {
            Yii::$app->notifier->addNotification('Не удалось удалить настройки страны', Notifier::TYPE_DANGER);
        }

        return $this->redirect(['index']);
    }

    /**
     * Activate an existing Countries model.
     * @param integer $id
     * @return mixed
     */
    public function actionActivate($id)
    {
        $model = $this->findModel($id);

        // Если модель найдена - обновляем
        if ($model !== null) {
            $model->status = true;
            $model->sms_status = true;
            $model->updated_at = time();
            if ($model->save()) {
                Yii::$app->notifier->addNotification('Страна включена в расчет отчетов и отправку SMS', Notifier::TYPE_SUCCESS);
            }
            else {
                Yii::$app->notifier->addNotification('Ошибка при сохранении новых настроек страны', Notifier::TYPE_DANGER);
            }
        }
        // иначе создаем
        else {
            $country = Country::findOne($id);

            $model = new Countries();
            $model->country_id = $country->id;
            $model->char_code = $country->char_code;
            $model->country_slug = $country->slug;
            $model->country_name = $country->name;
            $model->status = true;
            $model->sms_status = true;
            $model->created_at = time();
            $model->updated_at = $model->created_at;
            if ($model->save()) {
                Yii::$app->notifier->addNotification('Страна включена в расчет отчетов и отправку SMS', Notifier::TYPE_SUCCESS);
            }
            else {
                Yii::$app->notifier->addNotification('Ошибка при сохранении новых настроек страны', Notifier::TYPE_DANGER);
            }
        }


        return $this->redirect(['index']);
    }

    /**
     * Deactivate an existing Countries model.
     * @param integer $id
     * @return mixed
     */
    public function actionDeactivate($id)
    {
        $model = $this->findModel($id);

        // Если модель найдена - обновляем
        if ($model !== null) {
            $model->status = 0;
            $model->sms_status = 0;
            $model->updated_at = time();
            if ($model->save()) {
                Yii::$app->notifier->addNotification('Страна исключена из расчетов отчетов и отправки SMS', Notifier::TYPE_SUCCESS);
            }
            else {
                Yii::$app->notifier->addNotification('Ошибка при сохранении новых настроек страны', Notifier::TYPE_DANGER);
            }
        }
        // иначе создаем
        else {
            $country = Country::findOne($id);

            $model = new Countries();
            $model->country_id = $country->id;
            $model->char_code = $country->char_code;
            $model->country_slug = $country->slug;
            $model->country_name = $country->name;
            $model->status = 0;
            $model->sms_status = 0;
            $model->created_at = time();
            $model->updated_at = $model->created_at;
            if ($model->save()) {
                Yii::$app->notifier->addNotification('Страна исключена из расчетов отчетов и отправки SMS', Notifier::TYPE_SUCCESS);
            }
            else {
                Yii::$app->notifier->addNotification('Ошибка при сохранении новых настроек страны', Notifier::TYPE_DANGER);
            }
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Countries model based on its primary key value.
     * @param integer $id
     * @return Countries the loaded model or null
     */
    protected function findModel($id)
    {
        $model = Countries::find()
            ->where(['=', Countries::tableName() .'.country_id', $id])
            ->one();

        if ($model !== null) {
            return $model;
        } else {
            return null;
        }
    }

    /**
     * возращаем список тер.мен-ов
     * @return mixed
     */
    private function getTeamLeadersArray() {

        $countryCuratorsIds = Yii::$app->authManager->getUserIdsByRole('country.curator');

        $users = User::find()
            ->where(['=', User::tableName() .'.status', User::STATUS_DEFAULT])
            ->andWhere(['IN', User::tableName() .'.id', $countryCuratorsIds])
            ->all();

        $res[0] = '-';
        foreach ($users as $user) {
            $res[$user->id] = $user->username;
        }

        return $res;
    }

}
