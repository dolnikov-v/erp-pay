<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\modules\reportoperational\models\Countries */
/* @var $teams array */
/* @var $leaders array */

$this->title = Yii::t('common', 'Изменить настройки');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Настройки стран'), 'url' => Url::toRoute('index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>
<div class="countries-update">

    <h1><?= Html::encode($this->title .Yii::t('common', ' для страны ' .$model['country_name'] )) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'teams' => $teams,
        'leaders' => $leaders,
    ]) ?>

</div>
