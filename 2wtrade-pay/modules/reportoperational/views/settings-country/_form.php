<?php

use yii\helpers\Html;
use app\components\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\reportoperational\models\Countries */
/* @var $teams array */
/* @var $leaders array */
?>

<div class="panel panel-bordered ">
    <div class="panel-body">
        <div class="countries-form">

            <?php $form = ActiveForm::begin([
                    'method' => 'POST',
            ]); ?>

            <div class="row">

                <div class="col-lg-2">
                    <?= $form->field($model, 'team')->select2List($teams) ?>
                </div>

                <div class="col-lg-2">
                    <?= $form->field($model, 'partner_buyout_percent')->textInput() ?>
                </div>

                <div class="col-lg-3">
                    <?= $form->field($model, 'leader_id')->select2List($leaders) ?>
                </div>

                <div class="col-lg-5">
                    <?= $form->field($model, 'leader_name')->textInput() ?>
                </div>

            </div>

            <div class="row">
                <div class="col-lg-6">
                    <?= $form->field($model, 'status')->checkboxCustom([
                        'label' => Yii::t('common', 'Использовать страну при расчете отчетов'),
                        'value' => 1,
                        'checked' => $model->status,
                        'uncheckedValue' => 0,
                    ]);
                    ?>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <?= $form->field($model, 'sms_status')->checkboxCustom([
                        'label' => Yii::t('common', 'Использовать страну при формировании SMS'),
                        'value' => 1,
                        'checked' => $model->sms_status,
                        'uncheckedValue' => 0,
                    ]);
                    ?>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('common', 'Добавить') : Yii::t('common', 'Изменить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    <?= Html::a(Yii::t('common', 'Назад'), ['index'], ['class' => 'btn btn-default']); ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
