<?php

use app\components\grid\ActionColumn;
use app\components\grid\GridView;
use app\components\grid\IdColumn;
use app\helpers\DataProvider;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use app\widgets\Label;
use app\widgets\Panel;
use app\widgets\assets\custom\ModalConfirmDeleteAsset;
use app\widgets\custom\ModalConfirmDelete;


/* @var $this yii\web\View */
/* @var $searchModel app\modules\reportoperational\models\search\CountriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

ModalConfirmDeleteAsset::register($this);

$this->title = Yii::t('common', 'Настройки стран');
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Страны'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => [
            'class' => 'table table-striped table-hover table-sortable tl-fixed',
        ],
        'columns' => [
            [
                'class' => IdColumn::className(),
            ],
            [
                'label' => Yii::t('common', 'Страна'),
                'attribute' => 'name',
                'enableSorting' => false,
            ],
            [
                'label' => Yii::t('common', 'Основной статус'),
                'format' => 'raw',
                'value' => function($model) {
                    $label = Yii::t('common', 'Отключена');
                    $style = Label::STYLE_DEFAULT;
                    if ($model['country_active'] == 1) {
                        $label = Yii::t('common', 'Активна');
                        $style = Label::STYLE_SUCCESS;
                    }
                    return Label::widget([
                        'label' => $label,
                        'style' => $style,
                    ]);
                }
            ],
            [
                'label' => Yii::t('common', 'Использовать для отчетов'),
                'format' => 'raw',
                'value' => function($model) {
                    $label = Yii::t('common', 'Отключено');
                    $style = Label::STYLE_DEFAULT;
                    if ($model['use_into_report'] == 1 || is_null($model['use_into_report'])) {
                        $label = Yii::t('common', 'Включено');
                        $style = Label::STYLE_SUCCESS;
                    }
                    return Label::widget([
                        'label' => $label,
                        'style' => $style,
                    ]);
                }
            ],
            [
                'label' => Yii::t('common', 'Использовать для SMS'),
                'format' => 'raw',
                'value' => function($model) {
                    $label = Yii::t('common', 'Отключено');
                    $style = Label::STYLE_DEFAULT;
                    if ($model['use_into_sms'] == 1 || is_null($model['use_into_sms'])) {
                        $label = Yii::t('common', 'Включено');
                        $style = Label::STYLE_SUCCESS;
                    }
                    return Label::widget([
                        'label' => $label,
                        'style' => $style,
                    ]);
                }
            ],
            [
                'label' => Yii::t('common', 'Регион'),
                'attribute' => 'team_name',
            ],
            [
                'label' => Yii::t('common', 'Ответственный'),
                'attribute' => 'leader_name',
                'enableSorting' => false,
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Активировать все'),
                        'url' => function ($model) {
                            return Url::toRoute(['activate', 'id' => $model['id']]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('reportoperational.settingscountry.activate') && (!$model['use_into_report'] || !$model['use_into_sms']);
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Деактивировать все'),
                        'url' => function ($model) {
                            return Url::toRoute(['deactivate', 'id' => $model['id']]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('reportoperational.settingscountry.deactivate') && ($model['use_into_report'] == null || $model['use_into_sms'] == null || $model['use_into_report'] || $model['use_into_sms']);
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['update', 'id' => $model['id']]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('reportoperational.settingscountry.update');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить'),
                        'url' => function () {
                            return '#';
                        },
                        'style' => 'confirm-delete-link',
                        'attributes' => function ($data) {
                            return [
                                'data-href' => Url::toRoute(['delete', 'id' => $data['id']]),
                            ];
                        },
                        'can' => function () {
                            return Yii::$app->user->can('reportoperational.settingscountry.delete');
                        },
                    ],
                ],
            ],

        ],
    ]),
    'footer' => LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>

<?= ModalConfirmDelete::widget() ?>