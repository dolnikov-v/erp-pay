<?php
use app\modules\report\assets\ReportAsset;
use app\modules\reportoperational\widgets\ReportFilterBuyoutsToExcel;
use app\widgets\Panel;


/** @var \yii\web\View $this */
/** @var \app\modules\reportoperational\components\ReportFormBuyouts $reportForm */
/** @var \yii\data\ArrayDataProvider $dataProvider */
/** @var string $firstInterval */


$this->title = Yii::t('common', 'Таблица для операционного отчета по выкупам (страны)');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Отчеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ReportAsset::register($this);
?>

<?= ReportFilterBuyoutsToExcel::widget([
    'reportForm' => $reportForm,
]) ?>

<?= Panel::widget([
//    'title' => Yii::t('common', 'Операционный отчет по выкупам'),
    'border' => false,
    'withBody' => false,
    'content' => $this->render('_index-content', [
        'reportForm' => $reportForm,
        'firstInterval' => $firstInterval,
        'dataProvider' => $dataProvider,
    ]),
]) ?>
