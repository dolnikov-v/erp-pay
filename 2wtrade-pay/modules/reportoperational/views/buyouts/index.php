<?php
use app\modules\report\assets\ReportAsset;
use app\modules\reportoperational\assets\ReportSentToMailAsset;
use app\modules\reportoperational\assets\ReportExportToMailAsset;
use app\modules\reportoperational\widgets\ReportFilterBuyouts;
use app\widgets\Panel;


/** @var \yii\web\View $this */
/** @var \app\modules\reportoperational\components\ReportFormBuyouts $reportForm */
/** @var \yii\data\ArrayDataProvider $dataProvider */
/** @var string $caption */


$this->title = Yii::t('common', 'Операционный отчет по выкупам');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Отчеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ReportAsset::register($this);
ReportSentToMailAsset::register($this);
ReportExportToMailAsset::register($this);
?>

<?= ReportFilterBuyouts::widget([
    'reportForm' => $reportForm,
]) ?>

<?= Panel::widget([
//    'title' => Yii::t('common', 'Операционный отчет по выкупам'),
    'border' => false,
    'withBody' => false,
    'content' => $this->render('_index-content', [
        'reportForm' => $reportForm,
        'caption' => $caption,
        'dataProvider' => $dataProvider,
    ]),
]) ?>
