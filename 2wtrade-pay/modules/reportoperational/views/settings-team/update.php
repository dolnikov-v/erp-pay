<?php

use yii\helpers\Url;
use app\widgets\Panel;
use app\widgets\Nav;

/* @var $this yii\web\View */
/* @var $model app\modules\reportoperational\models\Team */
/* @var $modelTeamUsers app\modules\reportoperational\models\TeamCountryUser[] */
/* @var array $leaders */
/* @var $users array */

$this->title = Yii::t('common', 'Редактировать регион');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Регионы'), 'url' => Url::toRoute(['index'])];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Редактирование')];
?>

<div class="team-update">
    <?= Panel::widget([
        'border' => false,
        'alert' => Yii::t('common', 'Страны состава региона добавляются во вкладке Регион по нажатию кнопки Изменить'),
        'nav' => new Nav([
            'tabs' => [
                [
                    'label' => Yii::t('common', 'Регион'),
                    'content' => $this->render('_form', [
                        'model' => $model,
                        'leaders' => $leaders,
                    ]),
                ],
                [
                    'label' => Yii::t('common', 'Состав региона'),
                    'content' => $this->render('_form-users', [
                        'modelTeam' => $model,
                        'users' => $users,
                        'modelTeamUsers' => $modelTeamUsers,
                    ]),
                ],
            ]
        ]),
    ]) ?>
</div>
