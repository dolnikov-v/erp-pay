<?php

use yii\helpers\Url;
use app\widgets\Panel;

/* @var $this yii\web\View */
/* @var $model app\modules\reportoperational\models\Team */
/* @var $modelTeamUsers app\modules\reportoperational\models\TeamCountryUser[] */
/* @var array $leaders */

$this->title = Yii::t('common', 'Добавить Регион');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Регионы'), 'url' => Url::toRoute(['index'])];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => $this->title,
    'content' => $this->render('_form', [
        'model' => $model,
        'leaders' => $leaders,
    ])
]) ?>
