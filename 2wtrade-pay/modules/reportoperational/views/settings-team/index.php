<?php

use app\components\grid\ActionColumn;
use app\components\grid\DateColumn;
use app\components\grid\GridView;
use app\components\grid\IdColumn;
use app\helpers\DataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use app\widgets\Panel;
use app\widgets\assets\custom\ModalConfirmDeleteAsset;
use app\widgets\custom\ModalConfirmDelete;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\reportoperational\models\search\TeamSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

ModalConfirmDeleteAsset::register($this);

$this->title = Yii::t('common', 'Регионы');
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с регионами'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => [
            'class' => 'table table-striped table-hover table-sortable tl-fixed',
        ],
        'columns' => [
            [
                'class' => IdColumn::className(),
            ],
            [
                'label' => Yii::t('common', 'Наименование региона'),
                'attribute' => 'name',
                'enableSorting' => false,
            ],
            [
                'label' => Yii::t('common', 'Ответственный'),
                'attribute' => 'leader_name',
                'enableSorting' => false,
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'created_at',
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'updated_at',
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/reportoperational/settings-team/update', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('reportoperational.settingsteam.update');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить'),
                        'url' => function () {
                            return '#';
                        },
                        'style' => 'confirm-delete-link',
                        'attributes' => function ($data) {
                            return [
                                'data-href' => Url::toRoute(['delete', 'id' => $data->id]),
                            ];
                        },
                        'can' => function () {
                            return Yii::$app->user->can('reportoperational.settingsteam.delete');
                        },
                    ],
                ],
            ],

        ],
    ]),
    'footer' => Html::a(Yii::t('common', 'Добавить регион'), ['create'], ['class' => 'btn btn-success']) . LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>

<?= ModalConfirmDelete::widget() ?>