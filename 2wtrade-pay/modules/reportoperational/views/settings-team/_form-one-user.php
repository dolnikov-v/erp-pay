<?php
use app\helpers\WbIcon;
use app\widgets\Button;
use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\components\widgets\ActiveForm $form */
/** @var app\modules\reportoperational\models\TeamCountryUser $model */
/** @var $users array */
?>

<div class="row row-team-users">
    <div class="col-lg-3 user">
        <?= $form->field($model, 'user_id')->select2List($users, [
            'prompt' => '-',
            'id' => false,
            'name' => Html::getInputName($model, 'user_id') . '[]',
            'length' => false,
        ]) ?>
    </div>
    <div class="col-lg-6 countries">
        <?= $form->field($model, 'country_ids')->select2ListMultiple($model->countries->collection(), [
            'value' => $model->country_ids,
            'id' => false,
            'name' => Html::getInputName($model, 'country_ids_' . ((isset($model->user_id)) ? $model->user_id : '')),
            //. '[]',
        ]); ?>
    </div>

    <div class="col-lg-2">
        <div class="form-group">
            <div class="control-label">
                <label>&nbsp;</label>
            </div>
            <?= Button::widget([
                'type' => 'button',
                'icon' => WbIcon::MINUS,
                'style' => Button::STYLE_DANGER . ' btn-team-users btn-team-users-minus',
            ]) ?>

            <?= Button::widget([
                'type' => 'button',
                'icon' => WbIcon::PLUS,
                'style' => Button::STYLE_SUCCESS . ' btn-team-users btn-team-users-plus',
            ]) ?>

        </div>
    </div>

</div>


