<?php
use app\modules\reportoperational\assets\TeamUserAsset;
use app\modules\reportoperational\models\TeamCountryUser;
use app\components\widgets\ActiveForm;
use yii\helpers\Html;

/** @var app\modules\reportoperational\models\TeamCountryUser[] $modelTeamUsers */
/** @var app\modules\reportoperational\models\Team $modelTeam */
/** @var $users array */

TeamUserAsset::register($this);
?>
<div class="team-form">
    <?php $form = ActiveForm::begin([
        'action' => array_merge(Yii::$app->request->queryParams, ['edit-users']),
    ]); ?>

    <?= Html::hiddenInput(Html::getInputName(new TeamCountryUser(), 'team_id'), $modelTeam->id) ?>

    <div class="group-users-team-group">
        <?php if (!is_array($modelTeam->country_ids)): ?>
            <p>
                <?php echo Yii::t('common', 'Добавьте страны на вкладке Регион'); ?>
                <br/><br/>
            </p>
        <?php else: ?>
            <?php if (!empty($modelTeamUsers)): ?>
                <?php foreach ($modelTeamUsers as $key => $modelTeamUser): ?>
                    <?= $this->render('_form-one-user', [
                        'form' => $form,
                        'model' => $modelTeamUser,
                        'users' => $users,
                    ]) ?>
                <?php endforeach; ?>
            <?php endif; ?>
            <?= $this->render('_form-one-user', [
                'form' => $form,
                'model' => new TeamCountryUser($modelTeam->id),
                'users' => $users,
            ]) ?>
        <?php endif; ?>
    </div>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('common', 'Сохранить'), ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('common', 'Назад'), ['index'], ['class' => 'btn btn-default']); ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
