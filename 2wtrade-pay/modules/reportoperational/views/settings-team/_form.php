<?php

use yii\helpers\Html;
use app\components\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\modules\reportoperational\models\Team */
/* @var $leaders array */
?>

<div class="team-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-lg-3">
            <?= $form->field($model, 'name')->textInput() ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($model, 'leader_id')->select2List($leaders, [
                'length' => false,
            ]) ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'leader_name')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <?= $form->field($model, 'country_ids')
                ->select2ListMultiple($model->countries->collection(), ['value' => $model->country_ids]); ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('common', 'Добавить') : Yii::t('common', 'Изменить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('common', 'Назад'), ['index'], ['class' => 'btn btn-default']); ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
