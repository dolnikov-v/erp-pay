<?php
use app\modules\report\assets\ReportAsset;
use app\modules\reportoperational\assets\ReportSentToMailAsset;
use app\modules\reportoperational\assets\ReportExportToMailAsset;
use app\modules\reportoperational\widgets\ReportFilterProducts;
use app\widgets\Panel;


/** @var \yii\web\View $this */
/** @var \app\modules\reportoperational\components\ReportFormProducts $reportForm */
/** @var \yii\data\ArrayDataProvider $dataProvider1 */
/** @var \yii\data\ArrayDataProvider $dataProvider2 */
/** @var string $firstInterval */
/** @var string $secondInterval */

$this->title = Yii::t('common', 'Операционный отчет по продуктам');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Отчеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ReportAsset::register($this);
ReportSentToMailAsset::register($this);
ReportExportToMailAsset::register($this);
?>

<?= ReportFilterProducts::widget([
    'reportForm' => $reportForm,
]) ?>

<?= Panel::widget([
//    'title' => Yii::t('common', 'Ежедневный операционный отчет по продуктам'),
    'border' => false,
    'withBody' => false,
    'content' => $this->render('_index-content', [
        'reportForm' => $reportForm,
        'firstInterval' => $firstInterval,
        'dataProvider1' => $dataProvider1,
        'secondInterval' => $secondInterval,
        'dataProvider2' => $dataProvider2,
    ]),
]) ?>
