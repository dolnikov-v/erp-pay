<?php
use app\modules\reportoperational\extensions\GridViewProducts;
use app\modules\report\widgets\ExportReportToMail;
use app\widgets\ButtonProgress;
use app\widgets\ButtonLink;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \yii\data\ArrayDataProvider $dataProvider1 */
/** @var \yii\data\ArrayDataProvider $dataProvider2 */
/** @var string $firstInterval */
/** @var string $secondInterval */
/** @var \app\modules\reportoperational\components\ReportFormBuyouts $reportForm */
?>
<br>
<br>

    <?php echo ExportReportToMail::widget([
            'url' => Url::toRoute('export-to-mail/sent'),
        ]);
    ?>

    <div id="report_export_to_excel" style="margin-right: 320px; position: absolute; right: 40px; top: 15px;">
        <?php echo ButtonLink ::widget([
            "id" => "btn_export_report_to_excel",
            "url" => Url::toRoute(['products/export-to-excel'] + Yii::$app->request->queryParams),
            "style" => ButtonLink::STYLE_DEFAULT . " width-200",
            "size" => ButtonLink::SIZE_SMALL,
            "label" => Yii::t("common", "Экспорт отчета в Excel"),
        ]); ?>
    </div>

    <div id="report_export_to_mail"
         data-url=" <?php echo Url::toRoute('export-to-mail/sent') ?>"
         data-controller="<?php echo str_replace('-', '_', Yii::$app->controller->id) ?>">
        <?php echo ButtonProgress::widget([
            "id" => "btn_send_report_to_mail",
            "style" => ButtonProgress::STYLE_DEFAULT . " width-200",
            "size" => ButtonProgress::SIZE_SMALL,
            "label" => Yii::t("common", "Отправить отчет на Email"),
        ]); ?>
    </div>


    <div id="report_export_to_print" style="margin-right: 10px; position: absolute; right: 30px; top: 15px;">
        <?php echo ButtonProgress::widget([
            "id" => "btn_send_report_to_print",
            "style" => ButtonProgress::STYLE_DEFAULT . " width-100",
            "size" => ButtonProgress::SIZE_SMALL,
            "label" => Yii::t("common", "Распечатать"),
        ]); ?>
    </div>

    <div id="ready_to_export">
        <div class="table-responsive">
            <?=GridViewProducts::widget([
                'id' => 'report_product_first',
                'caption' => $firstInterval,
                'captionOptions' => ['style' => 'text-align: left; font-size: 125%; margin-left: 20px;'],
                'dataProvider' => $dataProvider1,
                'reportForm' => $reportForm,
            ]);?>
        </div>
        <div class="row">
            <br>
            <br>
        </div>
        <div class="table-responsive">
            <?=GridViewProducts::widget([
                'id' => 'report_product_second',
                'caption' => $secondInterval,
                'captionOptions' => ['style' => 'text-align: left; font-size: 125%; margin-left: 20px;'],
                'dataProvider' => $dataProvider2,
                'reportForm' => $reportForm,
            ]);?>
        </div>
    </div>

<?php

$this->registerJs('
    var gridview_id = "#report_product_first";
    var columns = [1, 2];

    var column_data = [];
        column_start = [];
        rowspan = [];

    for (var i = 0; i < columns.length; i++) {
        column = columns[i];
        column_data[column] = "";
        column_start[column] = null;
        rowspan[column] = 1;
    }

    var row = 1;
    $(gridview_id+" table > tbody  > tr").each(function() {
        var col = 1;
        $(this).find("td").each(function(){
            for (var i = 0; i < columns.length; i++) {
                if(col==columns[i]){
                    if(column_data[columns[i]] == $(this).html()){
                        $(this).remove();
                        rowspan[columns[i]]++;
                        $(column_start[columns[i]]).attr("rowspan",rowspan[columns[i]]);
                    }
                    else{
                        column_data[columns[i]] = $(this).html();
                        rowspan[columns[i]] = 1;
                        column_start[columns[i]] = $(this);
                    }
                }
            }
            col++;
        })
        row++;
    });
');

$this->registerJs('
    var gridview_id = "#report_product_second";
    var columns = [1, 2];

    var column_data = [];
        column_start = [];
        rowspan = [];

    for (var i = 0; i < columns.length; i++) {
        column = columns[i];
        column_data[column] = "";
        column_start[column] = null;
        rowspan[column] = 1;
    }

    var row = 1;
    $(gridview_id+" table > tbody  > tr").each(function() {
        var col = 1;
        $(this).find("td").each(function(){
            for (var i = 0; i < columns.length; i++) {
                if(col==columns[i]){
                    if(column_data[columns[i]] == $(this).html()){
                        $(this).remove();
                        rowspan[columns[i]]++;
                        $(column_start[columns[i]]).attr("rowspan",rowspan[columns[i]]);
                    }
                    else{
                        column_data[columns[i]] = $(this).html();
                        rowspan[columns[i]] = 1;
                        column_start[columns[i]] = $(this);
                    }
                }
            }
            col++;
        })
        row++;
    });
');

$this->registerJs('
    $("#btn_send_report_to_print").on("click", function(){
        if (confirm("Вы действительно хотите распечатать отчет?")) {
            var content = $("#ready_to_export");
            var exception = $(".rc-handle-container");
            content.find(exception).remove();
            var table = content.find("table");
            $.each(table, function(){
                this.setAttribute("border", "1");
                this.setAttribute("cellspacing", "0");
                this.setAttribute("bordercolor", "black");
                this.setAttribute("style", "font-size: 40%;");
            });
            var svg = content.find("svg");
            $.each(svg, function(){
                this.setAttribute("height", "5");
            });
            var rect = content.find("rect");
            $.each(rect, function(){
                this.setAttribute("height", "5");
            });
            var td = content.find("td");
            $.each(td, function(){
                this.setAttribute("valign", "center");
            });
            var content = content.html();
            var newWin = window.open("", "printWindow", "Toolbar=0,Location=0,Directories=0,Status=0,Menubar=0,Scrollbars=0,Resizable=0");
            newWin.document.open();
            newWin.document.write(content);
            newWin.document.close();
            location.reload();
            newWin.print();
        };
    });
');
?>