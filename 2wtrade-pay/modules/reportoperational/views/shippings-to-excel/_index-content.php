<?php
use app\modules\reportoperational\extensions\GridViewShippings;
use app\widgets\ButtonProgress;
use app\widgets\ButtonLink;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */
/** @var \app\modules\reportoperational\components\ReportFormShippings $reportForm */
/** @var string $caption */
?>
<br>
<br>


    <div id="report_export_to_excel" style="position: absolute; right: 20px; top: 15px;">
        <?php echo ButtonLink ::widget([
            "id" => "btn_export_report_to_excel",
            "url" => Url::toRoute(['shippings-to-excel/export-to-excel'] + Yii::$app->request->queryParams),
            "style" => ButtonLink::STYLE_DEFAULT . " width-200",
            "size" => ButtonLink::SIZE_SMALL,
            "label" => Yii::t("common", "Экспорт отчета в Excel"),
        ]); ?>
    </div>

<div id="ready_to_export">
    <div class="table-responsive">
        <?=GridViewShippings::widget([
            'id' => 'report_shippings',
            'caption' => $caption,
            'captionOptions' => ['style' => 'text-align: left; font-size: 150%; margin-left: 20px;'],
            'dataProvider' => $dataProvider,
            'reportForm' => $reportForm,
    ]);?>
    </div>
</div>


<?php

$this->registerJs('
    var gridview_id = "#report_shippings";
    var columns = [1];

    var column_data = [];
        column_start = [];
        rowspan = [];

    for (var i = 0; i < columns.length; i++) {
        column = columns[i];
        column_data[column] = "";
        column_start[column] = null;
        rowspan[column] = 1;
    }

    var row = 1;
    $(gridview_id+" table > tbody  > tr").each(function() {
        var col = 1;
        $(this).find("td").each(function(){
            for (var i = 0; i < columns.length; i++) {
                if(col==columns[i]){
                    if(column_data[columns[i]] == $(this).html()){
                        $(this).remove();
                        rowspan[columns[i]]++;
                        $(column_start[columns[i]]).attr("rowspan",rowspan[columns[i]]);
                    }
                    else{
                        column_data[columns[i]] = $(this).html();
                        rowspan[columns[i]] = 1;
                        column_start[columns[i]] = $(this);
                    }
                }
            }
            col++;
        })
        row++;
    });
');
?>