<?php
use app\modules\report\assets\ReportAsset;
use app\modules\reportoperational\widgets\ReportFilterShippingsToExcel;
use app\widgets\Panel;

/** @var \yii\web\View $this */
/** @var \app\modules\reportoperational\components\ReportFormShippings $reportForm */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */
/** @var string $caption */

$this->title = Yii::t('common', 'Таблица для операционного отчета по отгрузкам');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Отчеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ReportAsset::register($this);
?>

<?= ReportFilterShippingsToExcel::widget([
    'reportForm' => $reportForm,
]) ?>

<?= Panel::widget([
//    'title' => Yii::t('common', 'Операционный отчет по отгрузкам'),
    'border' => false,
    'withBody' => false,
    'content' => $this->render('_index-content', [
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
        'caption' => $caption,
    ]),
]) ?>
