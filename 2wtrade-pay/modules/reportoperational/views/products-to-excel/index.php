<?php
use app\modules\report\assets\ReportAsset;
use app\modules\reportoperational\widgets\ReportFilterProductsToExcel;
use app\widgets\Panel;


/** @var \yii\web\View $this */
/** @var \app\modules\reportoperational\components\ReportFormProductsToExcel $reportForm */
/** @var \yii\data\ArrayDataProvider $dataProvider */
/** @var string $firstInterval */

$this->title = Yii::t('common', 'Таблица для операционного отчета по выкупам (продукты)');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Отчеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ReportAsset::register($this);
?>

<?= ReportFilterProductsToExcel::widget([
    'reportForm' => $reportForm,
]) ?>

<?= Panel::widget([
//    'title' => Yii::t('common', 'Ежедневный операционный отчет по продуктам'),
    'border' => false,
    'withBody' => false,
    'content' => $this->render('_index-content', [
        'reportForm' => $reportForm,
        'firstInterval' => $firstInterval,
        'dataProvider' => $dataProvider,
    ]),
]) ?>
