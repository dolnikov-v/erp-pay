<?php
use app\modules\reportoperational\extensions\GridViewProductsToExcel;
use app\modules\report\widgets\ExportReportToMail;
use app\widgets\ButtonLink;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \yii\data\ArrayDataProvider $dataProvider */
/** @var string $firstInterval */
/** @var \app\modules\reportoperational\components\ReportFormProductsToExcel $reportForm */
?>
<br>
<br>

    <?php echo ExportReportToMail::widget([
            'url' => Url::toRoute('export-to-mail/sent'),
        ]);
    ?>

    <div id="report_export_to_excel" style="position: absolute; right: 20px; top: 15px;">
        <?php echo ButtonLink ::widget([
            "id" => "btn_export_report_to_excel",
            "url" => Url::toRoute(['products-to-excel/export-to-excel'] + Yii::$app->request->queryParams),
            "style" => ButtonLink::STYLE_DEFAULT . " width-200",
            "size" => ButtonLink::SIZE_SMALL,
            "label" => Yii::t("common", "Экспорт отчета в Excel"),
        ]); ?>
    </div>

    <div id="ready_to_export">
        <div class="table-responsive">
            <?=GridViewProductsToExcel::widget([
                'id' => 'report_product',
                'caption' => $firstInterval,
                'captionOptions' => ['style' => 'text-align: left; font-size: 125%; margin-left: 20px;'],
                'dataProvider' => $dataProvider,
                'reportForm' => $reportForm,
            ]);?>
        </div>
    </div>
