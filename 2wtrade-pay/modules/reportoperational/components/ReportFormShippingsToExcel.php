<?php
namespace app\modules\reportoperational\components;

use app\models\Country;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use app\modules\report\components\ReportForm;
use app\modules\report\extensions\Query;
use app\modules\reportoperational\models\Countries;
use app\modules\report\components\filters\DateFilter;
use app\modules\reportoperational\models\Team;
use yii\data\ArrayDataProvider;


/**
 * Class ReportFormShippingsToExcel
 * @package app\modules\reportoperational\components
 */
class ReportFormShippingsToExcel extends ReportForm
{
    /**
     * @var DateFilter
     */
    protected $dateFilter;

    /**
     * @var array Маппинг статусов
     */
    protected $mapStatuses = [];

    /*
     * "Галочка" конвертировать в доллар всегда отмечена
     * @var string
     */
    public $dollar = 'on';


    /**
     * @param array $params
     * @return ArrayDataProvider
     */
    public function apply($params)
    {
        date_default_timezone_set("UTC");

        $this->load($params);

        $this->query = new Query();
        $this->query->from([Order::tableName()]);

        $this->buildSelect()->applyFilters($params);
        $this->query->groupBy(['team_name', 'country_name']);

        $data = $this->query->all();

        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
        ]);

        $dataProvider->pagination->pageSize = $dataProvider->pagination->totalCount;

        return $dataProvider;
    }

    /**
     * @inheritdoc
     */
    protected function buildSelect()
    {
        $countries = Countries::find()
            ->where(['=', Countries::tableName() .'.status', 0])
            ->all();
        $rejectedCountries = [];
        foreach ($countries as $country) {
            $rejectedCountries[] = $country->country_id;
        }

        $this->query->select([
            'day' => $this->getDateFilter()->getFilteredAttribute(),
            'team_name' => Team::tableName() .'.name',
            'country_name' => Country::tableName() . '.name',
        ]);

        $this->query->leftJoin(CallCenterRequest::tableName(), CallCenterRequest::tableName() . '.order_id=' . Order::tableName() . '.id');
        $this->query->leftJoin(Country::tableName(), Order::tableName() . '.country_id=' . Country::tableName() . '.id');
        $this->query->leftJoin(Countries::tableName(), Countries::tableName() . '.country_id=' . Order::tableName() . '.country_id');
        $this->query->leftJoin(Team::tableName(), Team::tableName() . '.id=' . Countries::tableName() . '.team');
        $this->query->andWhere([Order::tableName() . '.duplicate_order_id' => null]);
        $this->query->andWhere(['NOT IN', Order::tableName() .'.country_id', $rejectedCountries]);  // ограничитель по включенным в справочнике отчетам

        $this->buildSelectMapStatuses();

        return $this;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $content = '';
        return $content;
    }

    /**
     * @return array
     */
    public function getMapStatuses()
    {
        if (empty($this->mapStatuses)) {
            $this->mapStatuses = [
                OrderStatus::STATUS_CC_APPROVED => [
                    OrderStatus::STATUS_CC_APPROVED,
                ],
                OrderStatus::STATUS_LOG_GENERATED => [
                    OrderStatus::STATUS_LOG_GENERATED,
                ],
                OrderStatus::STATUS_DELIVERY_ACCEPTED => [
                    OrderStatus::STATUS_DELIVERY_ACCEPTED,
                ],
                OrderStatus::STATUS_DELIVERY_LEFT_WAREHOUSE => [
                    OrderStatus::STATUS_DELIVERY_LEFT_WAREHOUSE,
                ],
                OrderStatus::STATUS_DELIVERY_BUYOUT => [
                    OrderStatus::STATUS_DELIVERY_BUYOUT,
                ],
                OrderStatus::STATUS_DELIVERY_DENIAL => [
                    OrderStatus::STATUS_DELIVERY_DENIAL,
                ],
                OrderStatus::STATUS_DELIVERY_REJECTED => [
                    OrderStatus::STATUS_DELIVERY_REJECTED,
                ],
                OrderStatus::STATUS_DELIVERY_PENDING => [
                    OrderStatus::STATUS_DELIVERY_PENDING,
                ],
                OrderStatus::STATUS_LOG_DEFERRED => [
                    OrderStatus::STATUS_LOG_DEFERRED,
                ],
            ];
        }

        return $this->mapStatuses;
    }

    /**
     * @param array $params
     * @return bool
     */
    public function applyFilters($params)
    {
        $this->getDateFilter()->apply($this->query, $params);

        return true;
    }

    /**
     * @return DateFilter
     */
    public function getDateFilter()
    {
        if (is_null($this->dateFilter)) {
            $this->dateFilter = new DateFilter();
            $this->dateFilter->type = DateFilter::TYPE_CALL_CENTER_APPROVED_AT;
        }

        return $this->dateFilter;
    }

}
