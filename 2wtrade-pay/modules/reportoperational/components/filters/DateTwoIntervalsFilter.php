<?php
namespace app\modules\reportoperational\components\filters;

use app\modules\report\components\filters\Filter;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\models\Order;
use app\modules\callcenter\models\CallCenterCheckRequest;
use app\modules\reportoperational\widgets\filters\DateTwoIntervalsFilter as DateTwoIntervalsFilterWidget;
use Yii;
use yii\base\InvalidParamException;

/**
 * Class DateTwoIntervalsFilter
 * @package app\modules\report\components\filters
 */
class DateTwoIntervalsFilter extends Filter
{
    const TYPE_IGNORE = '';
    const TYPE_CREATED_AT = 'created_at';
    const TYPE_UPDATED_AT = 'updated_at';
    const TYPE_CALL_CENTER_SENT_AT = 'c_sent_at';
    const TYPE_CALL_CENTER_APPROVED_AT = 'c_approved_at';
    const TYPE_DELIVERY_CREATED_AT = 'd_created_at';
    const TYPE_DELIVERY_SENT_AT = 'd_sent_at';
    const TYPE_DELIVERY_FIRST_SENT_AT = 'd_first_sent_at';
    const TYPE_DELIVERY_TIME_FROM = 'delivery_time_from';
    const TYPE_DELIVERY_RETURNED_AT = 'd_returned_at';
    const TYPE_DELIVERY_APPROVED_AT = 'd_approved_at';
    const TYPE_DELIVERY_ACCEPTED_AT = 'd_accepted_at';
    const TYPE_DELIVERY_PAID_AT = 'd_paid_at';
    const TYPE_CHECK_SENT_AT = 'check_sent_at';
    const TYPE_CHECK_DONE_AT = 'check_done_at';

    public $from_first;
    public $to_first;
    public $from_second;
    public $to_second;

    public $type = self::TYPE_CREATED_AT;
    public $types = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->from_first = Yii::$app->formatter->asDate(time());
        $this->to_first = Yii::$app->formatter->asDate(time());
        $this->from_second = Yii::$app->formatter->asDate(time());
        $this->to_second = Yii::$app->formatter->asDate(time());

        if (Yii::$app->user->language->locale == 'es-ES') {                         // исключение для испанского представления даты
            $this->from_first = str_replace('/', '.', $this->from_first);
            $this->to_first = str_replace('/', '.', $this->to_first);
            $this->from_second = str_replace('/', '.', $this->from_second);
            $this->to_second = str_replace('/', '.', $this->to_second);
        }

        $this->types = [
            self::TYPE_IGNORE => Yii::t('common', 'Игнорировать'),
            self::TYPE_CREATED_AT => Yii::t('common', 'Дата создания'),
            self::TYPE_UPDATED_AT => Yii::t('common', 'Дата обновления'),
            self::TYPE_CALL_CENTER_SENT_AT => Yii::t('common', 'Дата отправки (колл-центр)'),
            self::TYPE_CALL_CENTER_APPROVED_AT => Yii::t('common', 'Дата одобрения (колл-центр)'),
            self::TYPE_DELIVERY_CREATED_AT => Yii::t('common', 'Дата создания (курьерка)'),
            self::TYPE_DELIVERY_SENT_AT => Yii::t('common', 'Дата отправки (курьерка)'),
            self::TYPE_DELIVERY_FIRST_SENT_AT => Yii::t('common', 'Дата первой отправки (курьерка)'),
            self::TYPE_DELIVERY_TIME_FROM => Yii::t('common', 'Дата доставки (курьерка)'),
            self::TYPE_DELIVERY_RETURNED_AT => Yii::t('common', 'Дата возврата (курьерка)'),
            self::TYPE_DELIVERY_APPROVED_AT => Yii::t('common', 'Дата утверждения (курьерка)'),
            self::TYPE_DELIVERY_ACCEPTED_AT => Yii::t('common', 'Дата принятия (курьерка)'),
            self::TYPE_DELIVERY_PAID_AT => Yii::t('common', 'Дата оплаты (курьерка)'),
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['type', 'in', 'range' => array_keys($this->types)],
            [['from_first', 'to_first', 'from_second', 'to_second'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'date_first' => Yii::t('common', 'Дата первого интервала'),
            'date_second' => Yii::t('common', 'Дата второго интервала'),
            'type' => Yii::t('common', 'Тип'),
        ];
    }

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function applyFirst($query, $params)
    {
        $this->load($params);

        if (Yii::$app->user->language->locale == 'es-ES') {                         // исключение для испанского представления даты
            $this->from_first = str_replace('/', '.', $this->from_first);
            $this->to_first = str_replace('/', '.', $this->to_first);
        }

        if ($this->validate()) {
            if ($this->type) {
                if ($this->from_first && $dateFrom = Yii::$app->formatter->asTimestamp($this->from_first)) {
                    $query->andFilterWhere(['>=', $this->getFilteredAttribute(), $dateFrom]);
                }
                if ($this->to_first && $dateTo = Yii::$app->formatter->asTimestamp($this->to_first)) {
                    $query->andFilterWhere(['<=', $this->getFilteredAttribute(), $dateTo + 86399]);
                }
            }
        }
    }

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {

    }

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function applySecond($query, $params)
    {
        $this->load($params);

        if (Yii::$app->user->language->locale == 'es-ES') {                         // исключение для испанского представления даты
            $this->from_second = str_replace('/', '.', $this->from_second);
            $this->to_second = str_replace('/', '.', $this->to_second);
        }

        if ($this->validate()) {
            if ($this->type) {
                if ($this->from_second && $dateFrom = Yii::$app->formatter->asTimestamp($this->from_second)) {
                    $query->andFilterWhere(['>=', $this->getFilteredAttribute(), $dateFrom]);
                }
                if ($this->to_second && $dateTo = Yii::$app->formatter->asTimestamp($this->to_second)) {
                    $query->andFilterWhere(['<=', $this->getFilteredAttribute(), $dateTo + 86399]);
                }
            }
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return DateTwoIntervalsFilterWidget::widget([
            'form' => $form,
            'model' => $this,
        ]);
    }

    /**
     * @return string
     */
    public function getFilteredAttribute()
    {
        $attributes = [
            self::TYPE_CREATED_AT => Order::tableName() . '.created_at',
            self::TYPE_UPDATED_AT => Order::tableName() . '.updated_at',
            self::TYPE_CALL_CENTER_SENT_AT => CallCenterRequest::tableName() . '.sent_at',
            self::TYPE_CALL_CENTER_APPROVED_AT => CallCenterRequest::tableName() . '.approved_at',
            self::TYPE_DELIVERY_CREATED_AT => DeliveryRequest::tableName() . '.created_at',
            self::TYPE_DELIVERY_SENT_AT => 'COALESCE(' . DeliveryRequest::tableName() . '.second_sent_at,' . DeliveryRequest::tableName() . '.sent_at)',
            self::TYPE_DELIVERY_FIRST_SENT_AT => DeliveryRequest::tableName() . '.sent_at',
            self::TYPE_DELIVERY_TIME_FROM => Order::tableName() . '.delivery_time_from',
            self::TYPE_DELIVERY_RETURNED_AT => DeliveryRequest::tableName() . '.returned_at',
            self::TYPE_DELIVERY_APPROVED_AT => DeliveryRequest::tableName() . '.approved_at',
            self::TYPE_DELIVERY_ACCEPTED_AT => DeliveryRequest::tableName() . '.accepted_at',
            self::TYPE_DELIVERY_PAID_AT => DeliveryRequest::tableName() . '.paid_at',
            self::TYPE_CHECK_DONE_AT => CallCenterCheckRequest::tableName() . '.done_at',
            self::TYPE_CHECK_SENT_AT => CallCenterCheckRequest::tableName() . '.sent_at',
        ];

        if (!array_key_exists($this->type, $attributes)) {
            throw new InvalidParamException(Yii::t('common', 'Неизвестный тип в условии.'));
        }

        return $attributes[$this->type];
    }
}
