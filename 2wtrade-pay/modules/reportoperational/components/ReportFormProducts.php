<?php
namespace app\modules\reportoperational\components;

use app\models\Country;
use app\models\CurrencyRate;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use app\models\Product;
use app\modules\order\models\OrderProduct;
use app\modules\report\components\ReportForm;
use app\modules\report\extensions\Query;
use app\modules\reportoperational\models\Countries;
use app\modules\reportoperational\components\filters\DateTwoIntervalsFilter;
use app\modules\reportoperational\components\filters\ProductFilter;
use Yii;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;


/**
 * Class ReportFormProducts
 * @package app\modules\reportoperational\components
 */
class ReportFormProducts extends ReportForm
{
    /**
     * @var DateTwoIntervalsFilter
     */
    protected $dateTwoIntervalsFilter;

    /**
     * @var ProductFilter
     */
    protected $productFilter;

    /**
     * @var array Маппинг статусов
     */
    protected $mapStatuses = [];

    /*
     * "Галочка" конвертировать в доллар всегда отмечена
     * @var string
     */
    public $dollar = 'on';

    /**
     * Для группировки результатов по группам процентов для дат первого интервала
     * @var array
     */
    private $resultFirstArray = [];

    /**
     * Для группировки результатов по группам процентов для дат второго интервала
     * @var array
     */
    private $resultSecondArray = [];

    /**
     * список id заказов
     * @var array
     */
    private $ids = [];


    /**
     * @param array $params
     * @return array
     */
    public function apply($params)
    {
        date_default_timezone_set("UTC");

        if (empty($params)) {
            $dataProvider1 = new ArrayDataProvider([
                'allModels' => [],
            ]);

            $dataProvider2 = new ArrayDataProvider([
                'allModels' => [],
            ]);

            return [$dataProvider1, $dataProvider2];
        }

        $this->load($params);

        $this->getDateTwoIntervalsFilter();

        // Получаем расчет для первого интервала
        $this->query = new Query();
        $this->query->from([Order::tableName()])
            ->select(['order_id' => Order::tableName() .'.id'])
            ->leftJoin(CallCenterRequest::tableName(), CallCenterRequest::tableName() . '.order_id=' . Order::tableName() . '.id')
            ->distinct();
        $this->dateTwoIntervalsFilter->applyFirst($this->query, $params);

        $idsByDate = $this->query->all();
        $this->ids = ArrayHelper::getColumn($idsByDate, 'order_id');
        unset($idsByDate);

        if (!empty($params['s']['product'])) {
            $this->query = new Query();
            $this->query->from([Order::tableName()])
                ->select(['order_id' => Order::tableName() .'.id'])
                ->distinct();
            $this->getProductFilter()->apply($this->query, $params);
            $this->query->andWhere(['IN', Order::tableName() .'.id', $this->ids]);

            $idsByProduct = $this->query->all();
            $this->ids = ArrayHelper::getColumn($idsByProduct, 'order_id');
            unset($idsByProduct);
        }

        $this->buildSelect();
        $firstQuery = clone($this->query);

        // Получаем расчет для второго интервала
        $this->query = new Query();
        $this->query->from([Order::tableName()])
            ->select(['order_id' => Order::tableName() .'.id'])
            ->leftJoin(CallCenterRequest::tableName(), CallCenterRequest::tableName() . '.order_id=' . Order::tableName() . '.id')
            ->distinct();
        $this->dateTwoIntervalsFilter->applySecond($this->query, $params);

        $idsByDate = $this->query->all();
        $this->ids = ArrayHelper::getColumn($idsByDate, 'order_id');
        unset($idsByDate);

        if (!empty($params['s']['product'])) {
            $this->query = new Query();
            $this->query->from([Order::tableName()])
                ->select(['order_id' => Order::tableName() .'.id'])
                ->distinct();
            $this->getProductFilter()->apply($this->query, $params);
            $this->query->andWhere(['IN', Order::tableName() .'.id', $this->ids]);

            $idsByProduct = $this->query->all();
            $this->ids = ArrayHelper::getColumn($idsByProduct, 'order_id');
            unset($idsByProduct);
        }

        $this->buildSelect();
        $secondQuery = clone($this->query);

        $this->dateTwoIntervalsFilter->applySecond($secondQuery, $params);

        $this->resultFirstArray = $firstQuery->all();
        $this->resultSecondArray = $secondQuery->all();
        $this->buildResultModels();

        $dataProvider1 = new ArrayDataProvider([
            'allModels' => $this->resultFirstArray,
        ]);

        $dataProvider2 = new ArrayDataProvider([
            'allModels' => $this->resultSecondArray,
        ]);

        return [$dataProvider1, $dataProvider2];
    }

    /**
     * @inheritdoc
     */
    protected function buildSelect()
    {
        $countries = Countries::find()
            ->where(['=', Countries::tableName() .'.status', 0])
            ->all();

        $rejectedCountries = ArrayHelper::getColumn($countries, 'country_id');

        $vikupPercent = '((COUNT(`order`.status_id IN(' .join(',', OrderStatus::getBuyoutList()) .') OR NULL) / ((COUNT(`order`.status_id IN(' .join(',', OrderStatus::getBuyoutList()) .') OR NULL) + COUNT(`order`.status_id IN(' .join(',', OrderStatus::getNotBuyoutList()) .') OR NULL) + COUNT(`order`.status_id IN(' .join(',', OrderStatus::getProcessDeliveryList()) .') OR NULL)))) * 100)';

        $subQuery = new Query();
        $subQuery->from([OrderProduct::tableName()])
            ->leftJoin(Product::tableName(), Product::tableName() .'.id=' .OrderProduct::tableName() .'.product_id')
            ->select([
                'order_id' => OrderProduct::tableName() .'.order_id',
                'product_id' => OrderProduct::tableName() .'.product_id',
                'product_name' => Product::tableName() .'.name'
            ])
            ->groupBy(['order_id', 'product_id']);

        if (!empty($params['s']['product'])) {
            $subQuery->where(['product_id' => $params['s']['product']]);
        }

        $this->query = new Query();
        $this->query->from([Order::tableName()])
            ->leftJoin(['q' => $subQuery], 'q.order_id=' .Order::tableName() .'.id')
            ->leftJoin(Country::tableName(), Order::tableName() . '.country_id=' . Country::tableName() . '.id')
            ->leftJoin(CallCenterRequest::tableName(), CallCenterRequest::tableName() . '.order_id=' . Order::tableName() . '.id')
            ->leftJoin(DeliveryRequest::tableName(), DeliveryRequest::tableName() . '.order_id=' . Order::tableName() . '.id')
            ->leftJoin(Countries::tableName(), Countries::tableName() . '.country_id=' . Order::tableName() . '.country_id')
            ->select([
                'day' => 'FROM_UNIXTIME((' . $this->getDateFilter()->getFilteredAttribute() . '), "%Y-%m-%d")',
                'country_name' => Country::tableName() . '.name',
                'province' => Order::tableName() .'.customer_province',
                'city' => Order::tableName() .'.customer_city',
                'product_name' => 'q.product_name',
                'count' => 'COUNT(q.product_name)',
                'vikup_percent' => $vikupPercent,
                'partner_percent' => Countries::tableName() .'.partner_buyout_percent',
            ])
            ->where(['IN', Order::tableName() .'.id', $this->ids])
            ->andWhere(['NOT IN', Order::tableName() .'.country_id', $rejectedCountries])   // ограничитель по включенным в справочнике отчетам
            ->andWhere([Order::tableName() . '.duplicate_order_id' => null]);

        $toDollar = '';
        if ($this->dollar) {
            $toDollar = '/ ' . CurrencyRate::tableName() . '.rate';
            $this->query->leftJoin(CurrencyRate::tableName(),
                Country::tableName() . '.currency_id = ' . CurrencyRate::tableName() . '.currency_id');
        }

        $caseBuyOut = $this->query->buildCaseCondition(
            '(' . Order::tableName() . '.price_total ' . $toDollar . ' + ' . Order::tableName() . '.delivery ' . $toDollar . ' )',
            Order::tableName() . '.status_id',
            OrderStatus::getBuyoutList()
        );

        $this->query->addAvgCondition($caseBuyOut, Yii::t('common', 'Ср. выкуп, $'));

        $this->buildSelectMapStatuses();

        $this->query->groupBy(['country_name', 'product_name']);

        return $this;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $content = '';
        return $content;
    }

    /**
     * @return array
     */
    public function getMapStatuses()
    {
        if (empty($this->mapStatuses)) {
            $this->mapStatuses = [
                'vykupleno' => OrderStatus::getBuyoutList(),
                'vprocesse' => OrderStatus::getProcessDeliveryList(),
                'nevykupleno' => OrderStatus::getNotBuyoutList(),
            ];
        }

        return $this->mapStatuses;
    }

    /**
     * @param array $params
     * @return bool
     */
    public function applyFilters($params)
    {
        $this->getProductFilter()->apply($this->query, $params);

        return true;
    }

    /**
     * @return DateTwoIntervalsFilter
     */
    public function getDateTwoIntervalsFilter()
    {
        if (is_null($this->dateTwoIntervalsFilter)) {
            $this->dateTwoIntervalsFilter = new DateTwoIntervalsFilter();
            $this->dateTwoIntervalsFilter->type = DateTwoIntervalsFilter::TYPE_CALL_CENTER_APPROVED_AT;
            $this->dateTwoIntervalsFilter->from_first = Yii::$app->formatter->asDate(time() - 86400*15);
            $this->dateTwoIntervalsFilter->to_first = Yii::$app->formatter->asDate(time() - 86400*15);
            $this->dateTwoIntervalsFilter->from_second = Yii::$app->formatter->asDate(time() - 86400*16);
            $this->dateTwoIntervalsFilter->to_second = Yii::$app->formatter->asDate(time() - 86400*16);
        }

        return $this->dateTwoIntervalsFilter;
    }

    /**
     * @return ProductFilter
     */
    public function getProductFilter()
    {
        if (is_null($this->productFilter)) {
            $this->productFilter = new ProductFilter();
        }
        return $this->productFilter;
    }


    /**
     * Строим результирующие модели с группировкой по группам процентов выкупа
     */
    private function buildResultModels() {

        foreach ($this->resultFirstArray as &$item) {
            if (floatval($item['vikup_percent']) < 20) {
                $item['percent_group'] = Yii::t('common', 'до 20%');
            }
            if (floatval($item['vikup_percent']) >= 20) {
                $item['percent_group'] = Yii::t('common', 'от 20%');
            }
            if (floatval($item['vikup_percent']) >= 50) {
                $item['percent_group'] = Yii::t('common', 'от 50%');
            }
            if (floatval($item['vikup_percent']) >= 70) {
                $item['percent_group'] = Yii::t('common', 'от 70%');
            }
        }
        ArrayHelper::multisort($this->resultFirstArray, ['vikup_percent'], [SORT_DESC]);

        foreach ($this->resultSecondArray as &$item) {
            if (floatval($item['vikup_percent']) < 20) {
                $item['percent_group'] = Yii::t('common', 'до 20%');
            }
            if (floatval($item['vikup_percent']) >= 20) {
                $item['percent_group'] = Yii::t('common', 'от 20%');
            }
            if (floatval($item['vikup_percent']) >= 50) {
                $item['percent_group'] = Yii::t('common', 'от 50%');
            }
            if (floatval($item['vikup_percent']) >= 70) {
                $item['percent_group'] = Yii::t('common', 'от 70%');
            }
        }
        ArrayHelper::multisort($this->resultSecondArray, ['vikup_percent'], [SORT_DESC]);
    }

}
