<?php
namespace app\modules\reportoperational\components;

use app\models\Country;
use app\models\CurrencyRate;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use app\models\Product;
use app\modules\order\models\OrderProduct;
use app\modules\report\components\ReportForm;
use app\modules\report\extensions\Query;
use app\modules\reportoperational\models\Countries;
use app\modules\report\components\filters\DateFilter;
use app\modules\reportoperational\components\filters\ProductFilter;
use Yii;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;


/**
 * Class ReportFormProductsToExcel
 * @package app\modules\reportoperational\components
 */
class ReportFormProductsToExcel extends ReportForm
{
    /**
     * @var DateFilter
     */
    protected $dateFilter;

    /**
     * @var ProductFilter
     */
    protected $productFilter;

    /**
     * @var array Маппинг статусов
     */
    protected $mapStatuses = [];

    /*
     * "Галочка" конвертировать в доллар всегда отмечена
     * @var string
     */
    public $dollar = 'on';

    /**
     * Для группировки результатов по группам процентов
     * @var array
     */
    private $resultArray = [];

    /**
     * список id заказов
     * @var array
     */
    private $ids = [];


    /**
     * @param array $params
     * @return ArrayDataProvider
     */
    public function apply($params)
    {
        date_default_timezone_set("UTC");

        if (empty($params)) {
            $dataProvider = new ArrayDataProvider([
                'allModels' => [],
            ]);

            return $dataProvider;
        }

        $this->load($params);

        $this->query = new Query();
        $this->query->from([Order::tableName()])
            ->select(['order_id' => Order::tableName() .'.id'])
            ->leftJoin(CallCenterRequest::tableName(), CallCenterRequest::tableName() . '.order_id=' . Order::tableName() . '.id')
            ->distinct();
        $this->getDateFilter()->apply($this->query, $params);

        $idsByDate = $this->query->all();
        $this->ids = ArrayHelper::getColumn($idsByDate, 'order_id');
        unset($idsByDate);

        if (!empty($params['s']['product'])) {
            $this->query = new Query();
            $this->query->from([Order::tableName()])
                ->select(['order_id' => Order::tableName() .'.id'])
                ->distinct();
            $this->getProductFilter()->apply($this->query, $params);
            $this->query->andWhere(['IN', Order::tableName() .'.id', $this->ids]);

            $idsByProduct = $this->query->all();
            $this->ids = ArrayHelper::getColumn($idsByProduct, 'order_id');
            unset($idsByProduct);
        }

        $this->buildSelect();

        // Получаем результирующую модель
        $this->resultArray = $this->query->all();

        ArrayHelper::multisort($this->resultArray, ['day', 'vikup_percent'], [SORT_DESC, SORT_DESC]);

        $dataProvider = new ArrayDataProvider([
            'allModels' => $this->resultArray,
        ]);

        $dataProvider->pagination->pageSize = $dataProvider->pagination->totalCount;

        return $dataProvider;
    }

    /**
     * @inheritdoc
     */
    protected function buildSelect()
    {
        $countries = Countries::find()
            ->where(['=', Countries::tableName() .'.status', 0])
            ->all();

        $rejectedCountries = ArrayHelper::getColumn($countries, 'country_id');

        $vikupPercent = '((COUNT(`order`.status_id IN(' .join(',', OrderStatus::getBuyoutList()) .') OR NULL) / ((COUNT(`order`.status_id IN(' .join(',', OrderStatus::getBuyoutList()) .') OR NULL) + COUNT(`order`.status_id IN(' .join(',', OrderStatus::getNotBuyoutList()) .') OR NULL) + COUNT(`order`.status_id IN(' .join(',', OrderStatus::getProcessDeliveryList()) .') OR NULL)))) * 100)';

        $subQuery = new Query();
        $subQuery->from([OrderProduct::tableName()])
            ->leftJoin(Product::tableName(), Product::tableName() .'.id=' .OrderProduct::tableName() .'.product_id')
            ->select([
                'order_id' => OrderProduct::tableName() .'.order_id',
                'product_id' => OrderProduct::tableName() .'.product_id',
                'product_name' => Product::tableName() .'.name'
            ])
            ->groupBy(['order_id', 'product_id']);

        if (!empty($params['s']['product'])) {
            $subQuery->where(['product_id' => $params['s']['product']]);
        }

        $this->query = new Query();
        $this->query->from([Order::tableName()])
            ->leftJoin(['q' => $subQuery], 'q.order_id=' .Order::tableName() .'.id')
            ->leftJoin(Country::tableName(), Order::tableName() . '.country_id=' . Country::tableName() . '.id')
            ->leftJoin(CallCenterRequest::tableName(), CallCenterRequest::tableName() . '.order_id=' . Order::tableName() . '.id')
            ->leftJoin(DeliveryRequest::tableName(), DeliveryRequest::tableName() . '.order_id=' . Order::tableName() . '.id')
            ->leftJoin(Countries::tableName(), Countries::tableName() . '.country_id=' . Order::tableName() . '.country_id')
            ->select([
                'day' => 'FROM_UNIXTIME((' . $this->getDateFilter()->getFilteredAttribute() . '), "%Y-%m-%d")',
                'country_name' => Country::tableName() . '.name',
                'province' => Order::tableName() .'.customer_province',
                'city' => Order::tableName() .'.customer_city',
                'product_name' => 'q.product_name',
                'count' => 'COUNT(q.product_name)',
                'vikup_percent' => $vikupPercent,
                'partner_percent' => Countries::tableName() .'.partner_buyout_percent',
            ])
            ->where(['IN', Order::tableName() .'.id', $this->ids])
            ->andWhere(['NOT IN', Order::tableName() .'.country_id', $rejectedCountries])   // ограничитель по включенным в справочнике отчетам
            ->andWhere([Order::tableName() . '.duplicate_order_id' => null]);

        $toDollar = '';
        if ($this->dollar) {
            $toDollar = '/ ' . CurrencyRate::tableName() . '.rate';
            $this->query->leftJoin(CurrencyRate::tableName(),
                Country::tableName() . '.currency_id = ' . CurrencyRate::tableName() . '.currency_id');
        }

        $caseBuyOut = $this->query->buildCaseCondition(
            '(' . Order::tableName() . '.price_total ' . $toDollar . ' + ' . Order::tableName() . '.delivery ' . $toDollar . ' )',
            Order::tableName() . '.status_id',
            OrderStatus::getBuyoutList()
        );

        $this->query->addAvgCondition($caseBuyOut, Yii::t('common', 'Ср. выкуп, $'));
        $this->buildSelectMapStatuses();
        $this->query->groupBy(['day', 'country_name', 'province', 'city', 'q.product_name']);

        return $this;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $content = '';
        return $content;
    }

    /**
     * @return array
     */
    public function getMapStatuses()
    {
        if (empty($this->mapStatuses)) {
            $this->mapStatuses = [
                'vykupleno' => OrderStatus::getBuyoutList(),
                'vprocesse' => OrderStatus::getProcessDeliveryList(),
                'nevykupleno' => OrderStatus::getNotBuyoutList(),
            ];
        }

        return $this->mapStatuses;
    }

    /**
     * @param array $params
     * @return bool
     */
    public function applyFilters($params)
    {
        $this->getDateFilter()->apply($this->query, $params);
        $this->getProductFilter()->apply($this->query, $params);

        return true;
    }

    /**
     * @return DateFilter
     */
    public function getDateFilter()
    {
        if (is_null($this->dateFilter)) {
            $this->dateFilter = new DateFilter();
            $this->dateFilter->type = DateFilter::TYPE_CALL_CENTER_APPROVED_AT;
        }

        return $this->dateFilter;
    }

    /**
     * @return ProductFilter
     */
    public function getProductFilter()
    {
        if (is_null($this->productFilter)) {
            $this->productFilter = new ProductFilter();
        }
        return $this->productFilter;
    }

}
