<?php

namespace app\modules\reportoperational\components;

use app\models\Country;
use app\models\CurrencyRate;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use app\modules\report\components\ReportForm;
use app\modules\report\extensions\Query;
use app\modules\reportoperational\models\Countries;
use app\modules\report\components\filters\DateFilter;
use Yii;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;


/**
 * Class ReportFormBuyoutsToExcel
 * @package app\modules\reportoperational\components
 */
class ReportFormBuyoutsToExcel extends ReportForm
{
    /**
     * @var DateFilter
     */
    protected $dateFilter;

    /**
     * @var array Маппинг статусов
     */
    protected $mapStatuses = [];

    /*
     * "Галочка" конвертировать в доллар всегда отмечена
     * @var string
     */
    public $dollar = 'on';

    /**
     * Для группировки результатов по группам процентов
     * @var array
     */
    private $resultArray = [];


    /**
     * @param array $params
     * @return ArrayDataProvider
     */
    public function apply($params)
    {
        date_default_timezone_set("UTC");

        $this->load($params);
        $this->query = new Query();
        $this->query->from([Order::tableName()]);
        $this->query->groupBy(['day', 'country_name']);

        $this->buildSelect()->applyFilters($params);

        $this->resultArray = $this->query->all();

        ArrayHelper::multisort($this->resultArray, ['day', 'vikup_percent'], [SORT_DESC, SORT_DESC]);

        $dataProvider = new ArrayDataProvider([
            'allModels' => $this->resultArray,
        ]);

        $dataProvider->pagination->pageSize = $dataProvider->pagination->totalCount;

        return $dataProvider;
    }

    /**
     * @inheritdoc
     */
    protected function buildSelect()
    {
        $countries = Countries::find()
            ->where(['=', Countries::tableName() .'.status', 0])
            ->all();
        $rejectedCountries = [];
        foreach ($countries as $country) {
            $rejectedCountries[] = $country->country_id;
        }

        $vikupPercent = '((COUNT(`order`.status_id IN(' .join(',', OrderStatus::getBuyoutList()) .') OR NULL) / ((COUNT(`order`.status_id IN(' .join(',', OrderStatus::getBuyoutList()) .') OR NULL) + COUNT(`order`.status_id IN(' .join(',', OrderStatus::getNotBuyoutList()) .') OR NULL) + COUNT(`order`.status_id IN(' .join(',', OrderStatus::getProcessDeliveryList()) .') OR NULL)))) * 100)';

        $this->query->select([
            'day' => 'FROM_UNIXTIME((' . $this->getDateFilter()->getFilteredAttribute() . '), "%Y-%m-%d")',
            'country_name' => Country::tableName() . '.name',
            'vikup_percent' => $vikupPercent,
            'partner_percent' => Countries::tableName() .'.partner_buyout_percent',
        ]);

        $this->query->leftJoin(Country::tableName(), Order::tableName() . '.country_id=' . Country::tableName() . '.id');
        $this->query->leftJoin(CallCenterRequest::tableName(), CallCenterRequest::tableName() . '.order_id=' . Order::tableName() . '.id');
        $this->query->leftJoin(Countries::tableName(), Countries::tableName() . '.country_id=' . Order::tableName() . '.country_id');
        $this->query->andWhere(['NOT IN', Order::tableName() .'.country_id', $rejectedCountries]);  // ограничитель по включенным в справочнике отчетам
        $this->query->andWhere([Order::tableName() . '.duplicate_order_id' => null]);

        $toDollar = '';
        if ($this->dollar) {
            $toDollar = '/ ' . CurrencyRate::tableName() . '.rate';
            $this->query->leftJoin(CurrencyRate::tableName(),
                Country::tableName() . '.currency_id = ' . CurrencyRate::tableName() . '.currency_id');
        }

        $caseBuyOut = $this->query->buildCaseCondition(
            '(' . Order::tableName() . '.price_total ' . $toDollar . ' + ' . Order::tableName() . '.delivery ' . $toDollar . ' )',
            Order::tableName() . '.status_id',
            OrderStatus::getBuyoutList()
        );

        $this->query->addAvgCondition($caseBuyOut, Yii::t('common', 'Ср. выкуп, $'));

        $this->buildSelectMapStatuses();

        return $this;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $content = '';
        return $content;
    }

    /**
     * @return array
     */
    public function getMapStatuses()
    {
        if (empty($this->mapStatuses)) {
            $this->mapStatuses = [
                'vykupleno' => OrderStatus::getBuyoutList(),
                'vprocesse' => OrderStatus::getProcessList(),
                'nevykupleno' => OrderStatus::getNotBuyoutList(),
            ];
        }

        return $this->mapStatuses;
    }

    /**
     * @param array $params
     * @return bool
     */
    public function applyFilters($params)
    {
        $this->getDateFilter()->apply($this->query, $params);

        return true;
    }

    /**
     * @return DateFilter
     */
    public function getDateFilter()
    {
        if (is_null($this->dateFilter)) {
            $this->dateFilter = new DateFilter();
            $this->dateFilter->type = DateFilter::TYPE_CALL_CENTER_APPROVED_AT;
        }

        return $this->dateFilter;
    }

}
