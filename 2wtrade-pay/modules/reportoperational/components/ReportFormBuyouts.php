<?php

namespace app\modules\reportoperational\components;

use app\models\Country;
use app\models\CurrencyRate;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use app\modules\report\components\ReportForm;
use app\modules\report\extensions\Query;
use app\modules\reportoperational\components\filters\DateTwoIntervalsFilter;
use app\modules\reportoperational\models\Team;
use app\modules\reportoperational\models\TeamCountry;
use Yii;
use yii\data\ArrayDataProvider;


/**
 * Class ReportFormBuyouts
 * @package app\modules\reportoperational\components
 */
class ReportFormBuyouts extends ReportForm
{
    /**
     * @var DateTwoIntervalsFilter
     */
    protected $dateTwoIntervalsFilter;

    /**
     * @var array Маппинг статусов
     */
    protected $mapStatuses = [];

    /*
     * "Галочка" конвертировать в доллар всегда отмечена
     * @var string
     */
    public $dollar = 'on';

    /**
     * Для группировки результатов по группам процентов для дат первого интервала
     * @var array
     */
    private $resultFirstArray = [];

    /**
     * Для группировки результатов по группам процентов для дат второго интервала
     * @var array
     */
    private $resultSecondArray = [];


    /**
     * @param array $params
     * @return ArrayDataProvider
     */
    public function apply($params)
    {

        $this->load($params);
        $this->query = new Query();
        $this->query->from([Order::tableName()]);
        $this->query->groupBy(['team', 'country_name']);
        $this->buildSelect();

        $firstQuery = clone($this->query);
        $secondQuery = clone($this->query);

        $this->getDateTwoIntervalsFilter();

        $this->dateTwoIntervalsFilter->applyFirst($firstQuery, $params);
        $this->dateTwoIntervalsFilter->applySecond($secondQuery, $params);

        $this->resultFirstArray = $firstQuery->all();
        $this->resultSecondArray = $secondQuery->all();
        $model = $this->buildResultModels();

        $dataProvider = new ArrayDataProvider([
            'allModels' => $model,
        ]);

        $dataProvider->pagination->pageSize = $dataProvider->pagination->totalCount;

        return $dataProvider;
    }

    /**
     * @inheritdoc
     */
    protected function buildSelect()
    {
        $vikupPercent = '((COUNT(`order`.status_id IN(' .join(',', OrderStatus::getBuyoutList()) .') OR NULL) 
        / ((COUNT(`order`.status_id IN(' .join(',', OrderStatus::getBuyoutList()) .') OR NULL) 
        + COUNT(`order`.status_id IN(' .join(',', OrderStatus::getNotBuyoutList()) .') OR NULL) 
        + COUNT(`order`.status_id IN(' .join(',', OrderStatus::getProcessDeliveryList()) .') OR NULL)))) * 100)';

        $this->query->select([
            'country_name' => Country::tableName() . '.name',
            'vikup_percent' => $vikupPercent,
            'team' => Team::tableName() .'.name',
        ]);

        $this->query->leftJoin(Country::tableName(), Order::tableName() . '.country_id=' . Country::tableName() . '.id');
        $this->query->leftJoin(CallCenterRequest::tableName(), CallCenterRequest::tableName() . '.order_id=' . Order::tableName() . '.id');
        $this->query->leftJoin(TeamCountry::tableName(), TeamCountry::tableName() . '.country_id=' . Order::tableName() . '.country_id');
        $this->query->leftJoin(Team::tableName(), TeamCountry::tableName() . '.team_id=' . Team::tableName() . '.id');
        $this->query->andWhere(['!=', Country::tableName() . '.is_stop_list', 1]);  // ограничитель по включенным в справочнике отчетам
        $this->query->andWhere([Order::tableName() . '.duplicate_order_id' => null]);

        $toDollar = '';
        if ($this->dollar) {
            $toDollar = '/ ' . CurrencyRate::tableName() . '.rate';
            $this->query->leftJoin(CurrencyRate::tableName(),
                Country::tableName() . '.currency_id = ' . CurrencyRate::tableName() . '.currency_id');
        }

        $caseBuyOut = $this->query->buildCaseCondition(
            '(' . Order::tableName() . '.price_total ' . $toDollar . ' + ' . Order::tableName() . '.delivery ' . $toDollar . ' )',
            Order::tableName() . '.status_id',
            OrderStatus::getBuyoutList()
        );

        $this->query->addAvgCondition($caseBuyOut, Yii::t('common', 'Ср. выкуп, $'));

        $this->buildSelectMapStatuses();

        return $this;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $content = '';
        return $content;
    }

    /**
     * @return array
     */
    public function getMapStatuses()
    {
        if (empty($this->mapStatuses)) {
            $this->mapStatuses = [
                'vykupleno' => OrderStatus::getBuyoutList(),
                'vprocesse' => OrderStatus::getProcessList(),
                'nevykupleno' => OrderStatus::getNotBuyoutList(),
            ];
        }

        return $this->mapStatuses;
    }

    /**
     * @return DateTwoIntervalsFilter
     */
    public function getDateTwoIntervalsFilter()
    {
        if (is_null($this->dateTwoIntervalsFilter)) {
            $this->dateTwoIntervalsFilter = new DateTwoIntervalsFilter();
            $this->dateTwoIntervalsFilter->type = DateTwoIntervalsFilter::TYPE_CALL_CENTER_APPROVED_AT;
            $this->dateTwoIntervalsFilter->from_first = Yii::$app->formatter->asDate(time() - 86400*15);
            $this->dateTwoIntervalsFilter->to_first = Yii::$app->formatter->asDate(time() - 86400*15);
            $this->dateTwoIntervalsFilter->from_second = Yii::$app->formatter->asDate(time() - 86400*16);
            $this->dateTwoIntervalsFilter->to_second = Yii::$app->formatter->asDate(time() - 86400*16);
        }

        return $this->dateTwoIntervalsFilter;
    }

    /**
     * Строим результирующую модель с группировкой по командам и процентам выкупа в команде
     * @return array
     */
    private function buildResultModels() {
        //ArrayHelper::multisort($this->resultFirstArray, ['team', 'vikup_percent'], [SORT_DESC, SORT_DESC]);
        //ArrayHelper::multisort($this->resultSecondArray, ['team', 'vikup_percent'], [SORT_DESC, SORT_DESC]);

        $model = [];
        foreach ($this->resultFirstArray as $firstItem) {
            foreach ($this->resultSecondArray as $secondItem) {
                if ($firstItem['team'] == $secondItem['team']) {
                    if ($firstItem['country_name'] == $secondItem['country_name']) {
                        $model[] = [
                            'team' => $firstItem['team'],
                            'country_name' => $firstItem['country_name'],
                            'vikup_percent_1' => $firstItem['vikup_percent'],
                            'vikup_percent_2' => $secondItem['vikup_percent'],
                            'vykupleno_1' => $firstItem['vykupleno'],
                            'vykupleno_2' => $secondItem['vykupleno'],
                            'nevykupleno_1' => $firstItem['nevykupleno'],
                            'nevykupleno_2' => $secondItem['nevykupleno'],
                            'vprocesse_1' => $firstItem['vprocesse'],
                            'vprocesse_2' => $secondItem['vprocesse'],
                            'vsego_1' => $firstItem['vsego'],
                            'vsego_2' => $secondItem['vsego'],
                            'sr_vykup_1' => $firstItem['sr_vykup'],
                            'sr_vykup_2' => $secondItem['sr_vykup'],
                            'hold_1' => $firstItem['hold'],
                            'hold_2' => $secondItem['hold'],
                            'reject_1' => $firstItem['reject'],
                            'reject_2' => $secondItem['reject'],
                        ];
                    }
                }
            }
        }

        return $model;
    }

}
