<?php
namespace app\modules\reportoperational\extensions;

use app\modules\report\extensions\GridView;
use app\modules\reportoperational\components\ReportFormProducts;
use app\modules\order\models\OrderStatus;
use app\modules\reportoperational\widgets\DataCellChart;
use Yii;

/**
 * Class GridViewProducts
 * @package app\modules\report\extensions
 */
class GridViewProducts extends GridView
{
    /**
     * @var ReportFormProducts
     */
    public $reportForm;

    /**
     * @inheritdoc
     */
    public function initColumns()
    {

        $this->showPageSummary = false;
        $this->export = false;

        $ids = [];
        $mapStatuses = $this->reportForm->getMapStatuses();
        $orderStatuses = OrderStatus::find()->collection();
        $innerStatuses= [];

        foreach ($mapStatuses as $name => $statuses) {
            $ids = array_merge($ids, $statuses);
        }

        foreach ($ids as $id) {
            $innerStatuses[$id] = $id . '. ' . $orderStatuses[$id];
        }

        $vikupStatuses = [];
        foreach (OrderStatus::getBuyoutList() as $list) {
            $vikupStatuses[] = $innerStatuses[$list];
        }

        $nevikupStatuses = [];
        foreach (OrderStatus::getNotBuyoutList() as $list) {
            $nevikupStatuses[] = $innerStatuses[$list];
        }

        $inprocessStatuses = [];
        foreach (OrderStatus::getProcessList() as $list) {
            $inprocessStatuses[] = $innerStatuses[$list];
        }

        $this->columns[] = [
            'label' => Yii::t('common', 'Выкуп'),
            'contentOptions' => ['style' => 'text-align: center;'],
            'attribute' => 'percent_group',
        ];

        $this->columns[] = [
            'label' => Yii::t('common', 'Страна'),
            'attribute' => 'country_name',
        ];

        $this->columns[] = [
            'label' => Yii::t('common', '% Норма'),
            'contentOptions' => ['style' => 'text-align: left;'],
            'format' => 'raw',
            'value' => function($model) {
                return DataCellChart::widget([
                    'valueCurrent' => 70,
                    'valueTotal' => 100,
                    'chartWidth' => 70,
                    'showInPercent' => true,
                ]);
            }
        ];

        $this->columns[] = [
            'label' => Yii::t('common', '% Партнеры'),
            'contentOptions' => ['style' => 'text-align: left;'],
            'format' => 'raw',
            'value' => function($model) {
                return DataCellChart::widget([
                    'valueCurrent' => $model['partner_percent'],
                    'valueTotal' => 100,
                    'chartWidth' => 70,
                    'showInPercent' => true,
                ]);
            }
        ];

        $this->columns[] = [
            'label' => Yii::t('common', 'Продукт'),
            'attribute' => 'product_name',
        ];

        $this->columns[] = [
            'label' => Yii::t('common', '% Выкупа'),
            'contentOptions' => ['style' => 'text-align: left;'],
            'format' => 'raw',
            'value' => function($model) {
                return DataCellChart::widget([
                    'valueCurrent' => $model['vikup_percent'],
                    'valueTotal' => 100,
                    'chartWidth' => 50,
                    'showInPercent' => true,
                ]);
            }
        ];

        $this->columns[] = [
            'label' => Yii::t('common', 'Выкуплено'),
            'contentOptions' => ['style' => 'text-align: right;'],
            'format' => 'raw',
            'headerOptions' => [
                'data-container' => 'body',
                'data-toggle' => 'tooltip',
                'title' => Yii::t('common', 'Статусы: {statuses}', ['statuses' => implode(', ', $vikupStatuses)]),
            ],
            'value' => function($model) {
                return DataCellChart::widget([
                    'valueCurrent' => $model['vykupleno'],
                    'valueTotal' => $model['vsego'],
                    'chartWidth' => 70,
                ]);
            }
        ];

        $this->columns[] = [
            'label' => Yii::t('common', 'Не выкуплено'),
            'contentOptions' => ['style' => 'text-align: left;'],
            'format' => 'raw',
            'headerOptions' => [
                'data-container' => 'body',
                'data-toggle' => 'tooltip',
                'title' => Yii::t('common', 'Статусы: {statuses}', ['statuses' => implode(', ', $nevikupStatuses)]),
            ],
            'value' => function($model) {
                return DataCellChart::widget([
                    'valueCurrent' => $model['nevykupleno'],
                    'valueTotal' => $model['vsego'],
                    'chartWidth' => 70,
                ]);
            }
        ];

        $this->columns[] = [
            'label' => Yii::t('common', 'В процессе'),
            'contentOptions' => ['style' => 'text-align: left;'],
            'format' => 'raw',
            'headerOptions' => [
                'data-container' => 'body',
                'data-toggle' => 'tooltip',
                'title' => Yii::t('common', 'Статусы: {statuses}', ['statuses' => implode(', ', $inprocessStatuses)]),
            ],
            'value' => function($model) {
                return DataCellChart::widget([
                    'valueCurrent' => $model['vprocesse'],
                    'valueTotal' => $model['vsego'],
                    'chartWidth' => 70,
                ]);
            }
        ];

        $this->columns[] = [
            'label' => Yii::t('common', 'Всего'),
            'contentOptions' => ['style' => 'text-align: center;'],
            'attribute' => 'vsego',
        ];

        $this->columns[] = [
            'label' => Yii::t('common', 'Ср. чек выкупа($)'),
            'contentOptions' => ['style' => 'text-align: center;'],
            'value' => function($model) {
                return number_format(round($model['sr_vykup'], 2), 2);
            }
        ];

        $this->columns[] = [
            'label' => Yii::t('common', ''),
            'headerOptions' => ['style' => 'width: 0px;'],
            'contentOptions' => ['style' => 'width: 0px;'],
            'value' => function() {
                return '.';
            },
            'group' => true,
            'groupFooter' => function ($model) {
                return [
                    'mergeColumns' => [[0, 1]],                     // columns to merge in summary
                    'content' => [                                  // content to show in each summary cell
                        0 => Yii::t('common', 'Итого'),
                        2 => DataCellChart::widget([
                            'valueCurrent' => 70,
                            'valueTotal' => 100,
                            'chartWidth' => 70,
                            'showInPercent' => true,
                        ]),
                        3 => DataCellChart::widget([
                            'valueCurrent' => 70,
                            'valueTotal' => 100,
                            'chartWidth' => 70,
                            'showInPercent' => true,
                        ]),
                        5 => DataCellChart::widget([
                            'valueCurrent' => ($this->subTotalCalculate('vykupleno') / $this->subTotalCalculate('vsego')) * 100,
                            'valueTotal' => 100,
                            'chartWidth' => 70,
                            'showInPercent' => true,
                        ]),
                        6 => DataCellChart::widget([
                            'valueCurrent' => $this->subTotalCalculate('vykupleno'),
                            'valueTotal' => $this->subTotalCalculate('vsego'),
                            'chartWidth' => 70,
                        ]),
                        7 => DataCellChart::widget([
                            'valueCurrent' => $this->subTotalCalculate('nevykupleno'),
                            'valueTotal' => $this->subTotalCalculate('vsego'),
                            'chartWidth' => 70,
                        ]),
                        8 => DataCellChart::widget([
                            'valueCurrent' => $this->subTotalCalculate('vprocesse'),
                            'valueTotal' => $this->subTotalCalculate('vsego'),
                            'chartWidth' => 70,
                        ]),
                        9 => $this->subTotalCalculate('vsego'),
                        10 => GridView::F_AVG,
                    ],
                    'contentFormats' => [      // content reformatting for each summary cell
                        10 => ['format' => 'number', 'decimals' => 2],
                    ],
                    'contentOptions' => [      // content html attributes for each summary cell
                        0 => ['style' => 'font-variant:small-caps'],
                        2 => ['style' => 'text-align:center; vertical-align: middle;'],
                        3 => ['style' => 'text-align:center; vertical-align: middle;'],
                        5 => ['style' => 'text-align:center; vertical-align: middle;'],
                        6 => ['style' => 'text-align:center; vertical-align: middle;'],
                        7 => ['style' => 'text-align:center; vertical-align: middle;'],
                        8 => ['style' => 'text-align:center; vertical-align: middle;'],
                        9 => ['style' => 'text-align:center; vertical-align: middle;'],
                        10 => ['style' => 'text-align:center; vertical-align: middle;'],
                    ],
                    // html attributes for group summary row
                    'options' => ['style' => 'background-color: #e4eaec;']
                ];
            },
        ];

        parent::initColumns();

    }

    /**
     * @param $fieldName
     * @return float|int
     */
    private function subTotalCalculate($fieldName)
    {
        $subTotal = 0;

        $models = $this->dataProvider->getModels();
        if ($models) {
            foreach ($models as $item) {
                $subTotal += $item[$fieldName];
            }
        }

        return $subTotal;
    }

}
