<?php
namespace app\modules\reportoperational\extensions;

use app\modules\report\extensions\GridView;
use app\modules\reportoperational\components\ReportFormShippings;
use app\modules\order\models\OrderStatus;
use Yii;

/**
 * Class GridViewShippings
 * @package app\modules\reportoperational\extensions
 */
class GridViewShippings extends GridView
{
    /**
     * @var ReportFormShippings
     */
    public $reportForm;

    /**
     * @inheritdoc
     */
    public function initColumns()
    {

        $this->showPageSummary = false;
        $this->export = false;

        $keys = [];
        if (!empty($this->dataProvider->allModels)) {
            $keys = array_keys($this->dataProvider->allModels[0]);
            unset($keys[count($keys) - 1]);
            unset($keys[0]);
            unset($keys[1]);
            $keys = array_values($keys);
        }

        $statusesArray = OrderStatus::find()->collection();

        $statuses = [];
        foreach ($statusesArray as $id => $status) {
            foreach ($keys as $key => $value) {
                if (intval($id) == intval($value)) {
                    $statuses[$id] = $id . '. ' . $status;
                    break;
                }
            }
        }

        $this->columns[] = [
            'label' => Yii::t('common', 'Регион'),
            'format' => 'raw',
            'contentOptions' => ['style' => 'text-align: center; font-weight: bold; text-transform: uppercase;'],
            'value' => function ($model) {
                return $model['team_name'] .'<span style = "display: none;">' . $model['team_name'] . '</span>';
            },
        ];

        $this->columns[] = [
            'label' => Yii::t('common', 'Страна'),
            'attribute' => 'country_name',
        ];

        foreach ($keys as $key) {
            $this->columns[] = [
                'label' => $statuses[$key],
                'format' => 'raw',
                'contentOptions' => ['style' => 'text-align: center;'],
                'attribute' => $key,
            ];
        }

        $this->columns[] = [
            'label' => Yii::t('common', 'Всего'),
            'format' => 'raw',
            'contentOptions' => ['style' => 'text-align: center;'],
            'attribute' => 'vsego',
        ];

        parent::initColumns();

    }
}
