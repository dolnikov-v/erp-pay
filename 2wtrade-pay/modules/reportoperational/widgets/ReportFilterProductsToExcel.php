<?php

namespace app\modules\reportoperational\widgets;

use app\modules\report\widgets\ReportFilter;
use app\modules\reportoperational\components\ReportFormProductsToExcel;

/**
 * Class ReportFilterProducts
 * @package app\modules\report\widgets
 */
class ReportFilterProductsToExcel extends ReportFilter
{
    /** @var ReportFormProductsToExcel */
    public $reportForm;

    public function apply()
    {
        return $this->render('report-filter-products-to-excel/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}
