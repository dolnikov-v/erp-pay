<?php

namespace app\modules\reportoperational\widgets;

use app\modules\report\widgets\ReportFilter;
use app\modules\reportoperational\components\ReportFormBuyouts;

/**
 * Class ReportFilterBuyouts
 * @package app\modules\reportoperational\widgets
 */
class ReportFilterBuyouts extends ReportFilter
{
    /** @var ReportFormBuyouts */
    public $reportForm;

    public function apply()
    {
        return $this->render('report-filter-buyouts/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}
