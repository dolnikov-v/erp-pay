<?php

namespace app\modules\reportoperational\widgets;

use app\modules\report\widgets\ReportFilter;
use app\modules\reportoperational\components\ReportFormProducts;

/**
 * Class ReportFilterProducts
 * @package app\modules\report\widgets
 */
class ReportFilterProducts extends ReportFilter
{
    /** @var ReportFormProducts */
    public $reportForm;

    public function apply()
    {
        return $this->render('report-filter-products/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}
