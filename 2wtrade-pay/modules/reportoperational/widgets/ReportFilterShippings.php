<?php
namespace app\modules\reportoperational\widgets;

use app\modules\report\widgets\ReportFilter;
use app\modules\reportoperational\components\ReportFormShippings;

/**
 * Class ReportFilterShippings
 * @package app\modules\report\widgets
 */
class ReportFilterShippings extends ReportFilter
{
    /** @var ReportFormShippings */
    public $reportForm;

    public function apply()
    {
        return $this->render('report-filter-shippings/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}
