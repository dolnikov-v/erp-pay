<?php
/** @var float $valueCurrent */
/** @var float $valueTotal */
/** @var float $chartWidth */
/** @var string $chartColor */
/** @var boolean $showInPercent */
?>

<?php
    $widthRect = 0;
    if ($valueTotal != 0) {
        $widthRect = (number_format(round($valueCurrent / $valueTotal * 100, 2), 2) * $chartWidth / 100);
    }
?>

<svg id="svgitem" width="<?= $widthRect .'%' ?>" height="10" style="display: block; float: left;">
    <rect style="display: block; float: left;" fill="<?='#' .$chartColor ?>" height="10" width="100%"></rect>
</svg>
<span style="display: block; float: left; margin-left: 5px; margin-top: -4px;">
    <?php
    if ($showInPercent) {
        echo (number_format(round($valueCurrent, 2), 2) .'%');
    }
    else {
        echo $valueCurrent;
    }
    ?>
</span>




