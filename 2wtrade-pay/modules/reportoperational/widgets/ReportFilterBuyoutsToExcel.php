<?php

namespace app\modules\reportoperational\widgets;

use app\modules\report\widgets\ReportFilter;
use app\modules\reportoperational\components\ReportFormBuyoutsToExcel;

/**
 * Class ReportFilterBuyoutsToExcel
 * @package app\modules\reportoperational\widgets
 */
class ReportFilterBuyoutsToExcel extends ReportFilter
{
    /** @var ReportFormBuyoutsToExcel */
    public $reportForm;

    public function apply()
    {
        return $this->render('report-filter-buyouts-to-excel/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}
