<?php
namespace app\modules\reportoperational\widgets;

use app\modules\report\widgets\ReportFilter;
use app\modules\reportoperational\components\ReportFormShippingsToExcel;

/**
 * Class ReportFilterShippingsToExcel
 * @package app\modules\reportoperational\widgets
 */
class ReportFilterShippingsToExcel extends ReportFilter
{
    /** @var ReportFormShippingsToExcel */
    public $reportForm;

    public function apply()
    {
        return $this->render('report-filter-shippings-to-excel/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}
