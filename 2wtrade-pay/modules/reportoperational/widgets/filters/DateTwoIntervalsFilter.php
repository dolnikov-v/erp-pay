<?php
namespace app\modules\reportoperational\widgets\filters;

use yii\base\Widget;

/**
 * Class DateTwoIntervalsFilter
 * @package app\modules\reportoperational\widgets\filters
 */
class DateTwoIntervalsFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var \app\modules\reportoperational\components\filters\DateTwoIntervalsFilter */
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('date-two-intervals-filter/filter', [
            'form' => $this->form,
            'model' => $this->model,
        ]);
    }
}
