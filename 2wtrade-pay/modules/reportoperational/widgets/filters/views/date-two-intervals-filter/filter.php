<?php
use app\widgets\assets\DatePickerAsset;

/** @var app\components\widgets\ActiveForm $form */
/** @var app\modules\reportoperational\components\filters\DateTwoIntervalsFilter $model */

DatePickerAsset::register($this);
?>


<div class="col-lg-6">
    <?= $form->field($model, 'date_first')->dateRangePicker('from_first', 'to_first') ?>
</div>
<div class="col-lg-3">
    <?= $form->field($model, 'type')->select2List($model->types) ?>
</div>

<div class="col-lg-6">
    <?= $form->field($model, 'date_second')->dateRangePicker('from_second', 'to_second') ?>
</div>
