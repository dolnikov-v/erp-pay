<?php
namespace app\modules\reportoperational\widgets;

use app\Widgets\Widget;

/**
 * Class Label
 * @package app\widgets
 */
class DataCellChart extends Widget
{
    /**
     * @var float
     */
    public $valueCurrent = 0;

    /**
     * @var float
     */
    public $valueTotal = 100;

    /**
     * @var bool
     */
    public $showInPercent = false;

    /**
     * максимальная ширина графика от ширины ячейки в %
     * @var float
     */
    public $chartWidth = 50;

    /**
     * цвет графика в hex
     * @var string
     */
    public $charColor = '74A0C6';

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('data-cell-chart', [
            'valueCurrent' => $this->valueCurrent,
            'valueTotal' => $this->valueTotal,
            'chartWidth' => $this->chartWidth,
            'chartColor' => $this->charColor,
            'showInPercent' => $this->showInPercent,
        ]);
    }
}
