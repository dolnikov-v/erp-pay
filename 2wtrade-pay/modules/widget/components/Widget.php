<?php

namespace app\modules\widget\components;

use app\modules\widget\models\WidgetUser;
use yii\base\InvalidParamException;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * Class Widget
 * @package app\modules\widget\components
 */
class Widget
{
    /**
     * @return array
     */
    public static function getClasses()
    {
        return [
            'buy_out_for_countries' => 'reports\BuyOutForCountries',
            'buy_out_goods' => 'reports\BuyOutGoods',
            'buyout_by_product_country_delivery' => 'buyoutbyproductcountrydelivery\BuyOutByProductCountryDelivery',
            'buyout_delivery' => 'buyoutdelivery\BuyOutDelivery',
            'call_center_rating' => 'reports\CallCenterApprove',
            'connected_delivery' => 'reports\ConnectedDelivery',
            'delivery_debts' => 'reports\DeliveryDebts',
            'delivery_terms' => 'deliveryterms\DeliveryTerms',
            'diff_of_checks' => 'diffofchecks\DiffOfChecks',
            'global_top_goods' => 'reports\GlobalTopGoods',
            'operator_trash_lead' => 'reports\OperatorTrashLead',
            'in_stock_balance' => 'reports\InStockBalance',
            'operators_efficiency' => 'reports\OperatorsEfficiency',
            'payback_advertising' => 'reports\PaybackAdvertising',
            'products_certificates' => 'certificates\Certificates',
            'terms_of_shipping' => 'termsofshipping\TermsOfShipping',
            'top20cities' => 'top20cities\Top20Cities',
            'top20products' => 'top20products\Top20Products',
            'top_countries' => 'reports\TopCountries',
            'top_сс_managers' => 'reports\TopCallCenterManagers',
            'top_deliveries' => 'reports\TopDeliveries',
            'top_goods' => 'reports\TopGoods',
            'range_products_from_countries' => 'rangeproductsfromcountries\RangeProductsFromCountries',
            'top_country_curators' => 'topcountrycurators\TopCountryCurators',
            'processing_period' => 'processingperiod\ProcessingPeriod',
            'old_orders_country_curators' => 'oldorderscountrycurators\OldOrdersCountryCurators',
            'average_bill_of_approves_chart' => 'charts\AverageBillOfApproves',
            'percent_of_approves_chart' => 'charts\PercentOfApproves',
            'leads_chart' => 'charts\Leads',
            'buyout' => 'charts\BuyOut',
            'trash' => 'reports\Trash',
            'leads_by_week_chart' => 'charts\LeadsByWeek',
            'bad_approve' => 'reports\BadApprove',
            'large_balances' => 'reports\LargeBalancesByCountries',
        ];
    }

    /**
     * @param $code
     * @return string
     */
    public static function getClass($code)
    {
        $class = ArrayHelper::getValue(static::getClasses(), $code);
        return ($class) ? 'app\modules\widget\widgets\\' . $class : '';
    }

    /**
     * @param WidgetUser
     * @param array
     * @return \app\modules\widget\widgets\CacheableWidget | null
     */

    public static function createWidgetObj($widget, $config = [])
    {
        $class = static::getClass($widget->type->code);
        if ($class && class_exists($class)) {
            /** @var \yii\base\Widget $widgetClass */

            $config['model'] = $widget;
            return new $class($config);
        }

        return null;
    }

    /**
     * @param WidgetUser
     * @param array
     * @return string
     */

    public static function widget($widget, $config = [])
    {
        $content = '';

        $class = static::getClass($widget->type->code);
        if ($class && class_exists($class)) {
            /** @var \yii\base\Widget $class */

            $config['model'] = $widget;
            $content = $class::widget($config);
        }
        return $content;
    }

    /**
     * Получить данные виджета для его кеширования
     *
     * @param WidgetUser $widget
     * @param integer $countryId
     * @param array $countryIds
     * @param integer $timeOffset
     * @return array
     */

    public static function getData($widget, $countryId = null, $countryIds = null, $timeOffset = null)
    {
        $data = [];

        $class = static::getClass($widget->type->code);
        if ($class && class_exists($class)) {
            /** @var \yii\base\Widget $widgetClass */

            $widgetObject = new $class;

            if (method_exists($widgetObject, 'runData')) {
                $widgetObject->model = $widget;
                $widgetObject->countryId = $countryId;
                $widgetObject->countryIds = $countryIds;
                $widgetObject->timeOffset = $timeOffset;

                $data = $widgetObject->runData();
            } else {
                throw new InvalidParamException(Yii::t('common', 'class method does not exists ' . $class . '::runData'));
            }

        }
        return $data;
    }

    /**
     * @param $widget
     * @param $config array
     * @return null|string
     */

    public static function getRefreshData($widget, $config = [])
    {
        $data = null;

        if ($widgetObject = static::createWidgetObj($widget, $config)) {

            if ($widgetObject->refreshDataType == 'string') {
                $data = static::widget($widget, $config);
            } elseif ($widgetObject->refreshDataType == 'array') {
                $data = $widgetObject->getRefreshData();
                $data['scenario'] = $widgetObject->refreshScenario;
            }
        }
        return $data;
    }

}

