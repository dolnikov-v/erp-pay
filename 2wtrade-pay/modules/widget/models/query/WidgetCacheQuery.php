<?php
namespace app\modules\widget\models\query;

use app\modules\widget\models\WidgetCache;
use app\components\db\ActiveQuery;

/**
 * Class WidgetCacheQuery
 * @package app\modules\order\models\query
 *
 * @method WidgetCache one($db = null)
 * @method WidgetCache[] all($db = null)
 */
class WidgetCacheQuery extends ActiveQuery
{
    /**
     * @param integer $countryId
     * @return $this
     */
    public function byCountry($countryId)
    {
        return $this->andWhere(['country_id' => $countryId]);
    }

    /**
     * @param integer $widgetUserId
     * @return $this
     */
    public function byWidgetUser($widgetUserId)
    {
        return $this->andWhere(['widgetuser_id' => $widgetUserId]);
    }
}
