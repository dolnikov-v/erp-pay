<?php

namespace app\modules\widget\models;

use app\components\db\ActiveRecordLogUpdateTime;

/**
 * Class WidgetRole
 * @package app\modules\widget\models
 *
 * @property integer $id
 * @property integer $type_id
 * @property string $role
 */
class WidgetRole extends ActiveRecordLogUpdateTime
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%widget_role}}';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['type_id'], 'integer'],
            [['role'], 'string'],
        ];
    }


}
