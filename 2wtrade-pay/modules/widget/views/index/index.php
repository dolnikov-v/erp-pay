<?php

use app\modules\widget\widgets\GridStack;

/** @var \yii\web\View $this */
/** @var \app\modules\widget\models\WidgetUser[] $widgets */
/** @var \app\modules\widget\models\WidgetType[] $widgetTypes */

?>

<?= GridStack::widget([
    'widgets' => $widgets,
    'widgetTypes' => $widgetTypes,
]); ?>

