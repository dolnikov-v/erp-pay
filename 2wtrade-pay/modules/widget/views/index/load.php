<?php

use app\modules\widget\components\Widget;

/** @var \app\modules\widget\models\WidgetUser $widget */
?>

<?= Widget::widget($widget); ?>
