<?php
namespace app\modules\widget\assets;

use yii\web\AssetBundle;

/**
 * Class WidgetRoleSwitcheryAsset
 * @package app\modules\access\assets
 */
class WidgetRoleSwitcheryAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/widget/widget-role-switchery';

    public $js = [
        'widget-role-switchery.js',
    ];

    public $depends = [
        'app\assets\vendor\SwitcheryAsset',
    ];
}
