<?php
namespace app\modules\widget\assets;

use yii\web\AssetBundle;

/**
 * Class CountrySwitcheryAsset
 * @package app\modules\access\assets
 */
class GridStackAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/widget/grid-stack';

    public $js = [
        'panel.js',
    ];

    public $css = [
        'panel.css',
    ];

    public $depends = [
        'app\assets\SiteAsset',
    ];
}
