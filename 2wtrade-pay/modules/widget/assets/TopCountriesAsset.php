<?php
namespace app\modules\widget\assets;

use yii\web\AssetBundle;

/**
 * Class TopCountriesAsset
 * @package app\modules\access\assets
 */
class TopCountriesAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/widget';

    public $js = [
        'top-countries.js',
    ];

    public $depends = [
        'app\assets\SiteAsset',
    ];
}