<?php
namespace app\modules\widget\assets;

use yii\web\AssetBundle;

/**
 * Class CountrySwitcheryAsset
 * @package app\modules\access\assets
 */
class RoleSwitcheryAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/widget/role-switchery';

    public $js = [
        'role-switchery.js',
    ];

    public $depends = [
        'app\assets\vendor\SwitcheryAsset',
    ];
}
