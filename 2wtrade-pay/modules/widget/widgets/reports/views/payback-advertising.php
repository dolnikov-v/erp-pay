<?php

use app\modules\widget\extensions\GridView;

/**
 * @var \yii\data\ArrayDataProvider $dataProvider
 */

?>

<?= GridView::widget([
    'id' => 'widget-user-' . $model->id . '-grid',
    'exportFilename' => Yii::t('common', $model->type->name),
    'dataProvider' => $dataProvider,
    'rowOptions' => function ($model, $key, $index) {

        $class = 'tr-vertical-align-middle text-center';
        $class .= ($model['profit'] < 0) ? ' danger' : ' success';

        return [
            'key' => $key,
            'index' => $index,
            'class' => $class
        ];
    },
    'columns' => [
        [
            'header' => Yii::t('common', 'Страна'),
            'headerOptions' => ['style' => 'text-align:center'],
            'attribute' => 'country_name',
            'value' => function ($model) {
                return Yii::t('common', $model['country_name']);
            },
        ],
        [
            'header' => Yii::t('common', 'Прибыль ($)'),
            'headerOptions' => ['style' => 'text-align:center'],
            'attribute' => 'profit',
            'value' => function ($model) {
                return number_format($model['profit'], 2, '.', ' ');
            },
        ],
    ]
]) ?>
