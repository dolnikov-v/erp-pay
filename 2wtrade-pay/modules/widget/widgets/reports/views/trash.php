<?php

use app\modules\widget\extensions\GridView;

/**
 * @var \yii\data\ArrayDataProvider $dataProvider
 * @var \app\modules\widget\models\WidgetUser $model
 */

?>

<div id="widget-trash" data-id="<?=$model->id; ?>">

    <?= GridView::widget([
        'id' => 'widget-user-' . $model->id . '-grid',
        'exportFilename' => Yii::t('common', $model->type->name),
        'dataProvider' => $dataProvider,
        'showPageSummary' => true,
        'columns' => [
            [
                'label' => Yii::t('common', 'Страна'),
                'headerOptions' => ['style' => 'text-align:center'],
                'attribute' => 'country_name',
                'pageSummary' => Yii::t('common', 'Среднее'),
                'pageSummaryOptions' => ['class' => 'text-warning'],
            ],
            [
                'label' => Yii::t('common', 'Треш'),
                'headerOptions' => ['style' => 'text-align:center;'],
                'contentOptions' => ['style' => 'text-align:center;'],
                'attribute' => 'trash_percent',
                'format' => ['percent', 2],
                'pageSummary' => true,
                'pageSummaryFunc' => GridView::F_AVG,
                'pageSummaryOptions' => ['class' => 'text-warning', 'style' => 'text-align:center;'],
            ]
        ]
    ]); ?>

</div>
<?= '<br><span style="font-size: 75%;">' .Yii::t("common", "*Данные берутся за последние 31 день") .'</span>' ?>