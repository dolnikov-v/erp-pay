<?php

use app\modules\widget\extensions\GridView;

/**
 * @var \yii\data\ArrayDataProvider $dataProvider
 */
?>

<?= GridView::widget([
    'id' => 'widget-user-' . $model->id . '-grid',
    'exportFilename' => Yii::t('common', $model->type->name),
    'dataProvider' => $dataProvider,
    'rowOptions' => function ($model, $key, $index) {
/*
        $class = 'tr-vertical-align-middle text-center ';
        $class .= ($model['percent'] < $percent) ? ' danger' : ' success';

        return [
            'key' => $key,
            'index' => $index,
            'class' => $class
        ];
*/
    },
    'columns' => [
        [
            'header' => Yii::t('common', 'Страна'),
            'headerOptions' => ['style' => 'text-align:center'],
            'attribute' => 'country',
        ],
        [
            'header' => Yii::t('common', 'Товар'),
            'headerOptions' => ['style' => 'text-align:center'],
            'contentOptions' => ['style' => 'text-align:center'],
            'attribute' => 'product',
        ],
        [
            'header' => Yii::t('common', 'Лидов в день'),
            'headerOptions' => ['style' => 'text-align:center'],
            'contentOptions' => ['style' => 'text-align:center'],
            'attribute' => 'leads',
        ],
        [
            'header' => Yii::t('common', 'Остаток, мес.'),
            'headerOptions' => ['style' => 'text-align:center'],
            'contentOptions' => ['style' => 'text-align:center'],
            'attribute' => 'lefttime',
/*
            'value' => function ($model) {
                return number_format($model['percent'], 2, '.', '') . '%';
            },
*/
        ],
    ],
]); ?>
