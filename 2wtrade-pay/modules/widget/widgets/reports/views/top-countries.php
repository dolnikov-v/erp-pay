<?php

use app\modules\widget\extensions\GridView;
use app\modules\widget\assets\TopCountriesAsset;

/**
 * @var \yii\data\ArrayDataProvider $dataProvider
 * @var \app\modules\widget\models\WidgetUser $model
 */

TopCountriesAsset::register($this);
?>

<div id="widget-top-countries" data-id="<?=$model->id; ?>">

    <?= GridView::widget([
        'id' => 'widget-user-' . $model->id . '-grid',
        'exportFilename' => Yii::t('common', $model->type->name),
        'dataProvider' => $dataProvider,
        'showPageSummary' => true,
        'rowOptions' => function ($model) {

            $class = 'tr-vertical-align-middle text-center';

            if ($model['offset'] < 0) {
                $class .= ' danger';
            } elseif ($model['offset'] > 0) {
                $class .= ' success';
            }

            return [
                'class' => $class
            ];
        },
        'columns' => [
            [
                'label' => Yii::t('common', 'Страна'),
                'headerOptions' => ['style' => 'text-align:center'],
                'attribute' => 'country_name',
                'value' => function ($model) {

                    $offset = '';

                    if ($model['offset'] < 0) {
                        $offset .= ' (' . $model['offset'] . ')';
                    } elseif ($model['offset'] > 0) {
                        $offset .= ' (+' . $model['offset'] . ')';
                    }

                    return  Yii::t('common', $model['country_name']) . $offset;
                }
            ],
            [
                'label' => Yii::t('common', 'Выручка ($)'),
                'headerOptions' => ['style' => 'text-align:center', 'class' => 'sorting'],
                'attribute' => 'profit',
                'pageSummary' => true,
                'pageSummaryFunc' => GridView::F_AVG,
                'pageSummaryOptions' => ['class' => 'text-warning', 'style' => 'text-align:center;'],
                'format' => ['decimal', 2]
            ],
            [
                'label' => Yii::t('common', 'Апрув'),
                'headerOptions' => ['style' => 'text-align:center', 'class' => 'sorting'],
                'attribute' => 'approve_percent',
                'format' => ['percent', 2],
                'pageSummary' => true,
                'pageSummaryFunc' => GridView::F_AVG,
                'pageSummaryOptions' => ['class' => 'text-warning', 'style' => 'text-align:center;'],
            ],
            [
                'label' => Yii::t('common', 'Выкуп'),
                'headerOptions' => ['style' => 'text-align:center', 'class' => 'sorting'],
                'attribute' => 'buyout_percent',
                'format' => ['percent', 2],
                'pageSummary' => true,
                'pageSummaryFunc' => GridView::F_AVG,
                'pageSummaryOptions' => ['class' => 'text-warning', 'style' => 'text-align:center;'],

            ],
            [
                'label' => Yii::t('common', 'Ср.чек ($)'),
                'headerOptions' => ['style' => 'text-align:center', 'class' => 'sorting'],
                'attribute' => 'sr_chk_cc',
                'pageSummary' => true,
                'pageSummaryFunc' => GridView::F_AVG,
                'pageSummaryOptions' => ['class' => 'text-warning', 'style' => 'text-align:center;'],
                'format' => ['decimal', 2]
            ],
        ],
    ]); ?>

</div>
<?= '<br><span style="font-size: 75%;">' .Yii::t("common", "*Данные берутся за последние 31 день") .'</span>' ?>