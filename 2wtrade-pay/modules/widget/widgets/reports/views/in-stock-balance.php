<?php

use app\modules\widget\extensions\GridView;

/**
 * @var \yii\data\ArrayDataProvider $dataProvider
 */
?>

<?= GridView::widget([
    'id' => 'widget-user-' . $model->id . '-grid',
    'exportFilename' => Yii::t('common', $model->type->name),
    'dataProvider' => $dataProvider,
    'rowOptions' => function ($model, $key, $index) {


        $class = 'tr-vertical-align-middle text-center';
        if ($model['days'] >= 28 || $model['days'] == -1) {
            $class .= ' success';
        } elseif ($model['days'] >= 14) {
            $class .= ' warning';
        } else {
            $class .= ' danger';
        }

        return [
            'key' => $key,
            'index' => $index,
            'class' => $class
        ];
    },
    'columns' => [
        [
            'header' => Yii::t('common', 'Товар'),
            'headerOptions' => ['style' => 'text-align:center'],
            'attribute' => 'product_name'
        ],
        [
            'header' => Yii::t('common', 'Остатки'),
            'headerOptions' => ['style' => 'text-align:center'],
            'attribute' => 'quantity',
        ],
    ],
]); ?>
