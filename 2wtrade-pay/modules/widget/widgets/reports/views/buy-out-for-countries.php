<?php

use app\modules\widget\extensions\GridView;

/**
 * @var \yii\data\ArrayDataProvider $dataProvider
 */
?>

<?= GridView::widget([
    'id' => 'widget-user-' . $model->id . '-grid',
    'exportFilename' => Yii::t('common', $model->type->name),
    'dataProvider' => $dataProvider,
    'rowOptions' => function ($model, $key, $index) {

        $countries = [
            'Мексика', 'Боливия', 'Парагвай', 'Уругвай',
            'Колумбия', 'Чили', 'Перу', 'Аргентина'
        ];
        $percent = in_array($model['country_name'], $countries) ? 50 : 65;

        $class = 'tr-vertical-align-middle text-center ';
        $class .= ($model['percent'] < $percent) ? ' danger' : ' success';

        return [
            'key' => $key,
            'index' => $index,
            'class' => $class
        ];
    },
    'columns' => [
        [
            'header' => Yii::t('common', 'Страна'),
            'headerOptions' => ['style' => 'text-align:center'],
            'attribute' => 'country_name',
            'value' => function ($model) {
                return Yii::t('common', $model['country_name']);
            },
        ],
        [
            'header' => Yii::t('common', 'Выкуп'),
            'headerOptions' => ['style' => 'text-align:center'],
            'attribute' => 'count',
            'value' => function ($model) {
                return number_format($model['count'], 0, '.', ' ');
            },
        ],
        [
            'header' => Yii::t('common', 'Процент'),
            'headerOptions' => ['style' => 'text-align:center'],
            'attribute' => 'percent',
            'value' => function ($model) {
                return number_format($model['percent'], 2, '.', '') . '%';
            },
        ],
    ],
]); ?>
