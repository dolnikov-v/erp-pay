<?php

use app\modules\widget\extensions\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var yii\data\ArrayDataProvider $dataProvider
 * @var app\modules\widget\models\WidgetUser $model
 * @var double $commonSum
 */
?>

<?= GridView::widget([
    'id' => 'widget-user-' . $model->id . '-grid',
    'exportFilename' => Yii::t('common', $model->type->name),
    'dataProvider' => $dataProvider,
    'showPageSummary' => true,
    'tableOptions' => ['class' => 'table table-striped'],
    'rowOptions' => ['class' => 'tr-vertical-align-middle'],
    'columns' => [
        [
            'label' => Yii::t('common', 'Страна'),
            'attribute' => 'country_name',
            'group' => true,
            'hAlign' => 'center',
            'vAlign' => 'middle',
        ],
        [
            'label' => Yii::t('common', 'Выкуплено заказов на сумму, {currency}', ['currency' => 'USD']),
            'hAlign' => 'right',
            'format' => 'raw',
            'value' => function ($model) {
                return Html::a(number_format($model['sum_total'], 2),
                    Url::to([
                        '/report/debts/index',
                        's' => [
                            'country_ids' => [$model['country_id']]
                        ]
                    ]));
            },
            'pageSummary' => number_format($commonSum, 2) . " USD",
        ],
    ],
]) ?>
