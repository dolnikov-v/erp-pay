<?php

use app\modules\widget\extensions\GridView;
/**
 * @var \yii\data\ArrayDataProvider $dataProvider
 */
?>

<?= GridView::widget([
    'id' => 'widget-user-' . $model->id . '-grid',
    'exportFilename' => Yii::t('common', $model->type->name),
    'dataProvider' => $dataProvider,
    'rowOptions' => [
        'class' => 'tr-vertical-align-middle text-center'
    ],
    'columns' => [
        [
            'header' => Yii::t('common', 'Страна') . ' / ' . Yii::t('common', 'товар'),
            'headerOptions' => ['style' => 'text-align:center'],
            'attribute' => 'country_name',
            'value' => function ($model) {
                return Yii::t('common', $model['country_name'])  . ' / ' . Yii::t('common', $model['product_name']) ;
            },
        ],
        [
            'header' => Yii::t('common', 'Апрув'),
            'headerOptions' => ['style' => 'text-align:center'],
            'attribute' => 'percent',
            'value' => function ($model) {
                return $model['approve'] . ' (' . number_format($model['percent'], 2, '.', '') . '%)';
            },
        ],
    ],
]); ?>
<small>* <?= Yii::t('common', 'Данные для виджета берутся за последние {days} дней', ['days' => 7]); ?></small>