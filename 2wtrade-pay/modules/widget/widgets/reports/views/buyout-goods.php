<?php

use app\widgets\Select2;
use app\modules\widget\assets\BuyoutGoodsAsset;

/**
 * @var app\modules\widget\models\WidgetType $model
 * @var app\models\Product[] $products
 * @var \yii\data\ArrayDataProvider[] $dataProviders
 * @var \yii\data\ArrayDataProvider[] $allDataProviders
 */

$items = ['all' => Yii::t('common', 'Все')] + $products;

$dataProviders['all'] = $allDataProviders;
?>

<div class="widget-buyout-goods">

    <?= Select2::widget([
        'items' => $items,
        'length' => -1,
    ]); ?>

<?php foreach ($items as $id => $name) { ?>

    <div class="widget-buyout-goods-item <?= ($id == 'all') ? 'visible' : 'hidden'; ?>" data-id="<?=$id; ?>">

        <?= $this->render('buyout-goods-item', [
            'model' => $model,
            'dataProvider' => $dataProviders[$id],
            'productName' => $name,
            'productId' => $id,
        ]); ?>

    </div>

<?php } ?>

</div>