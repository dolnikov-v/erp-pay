<?php

use app\modules\widget\extensions\GridView;

/**
 * @var \yii\data\ArrayDataProvider $dataProvider
 */
?>

<?= GridView::widget([
    'id' => 'widget-user-' . $model->id . '-grid',
    'exportFilename' => Yii::t('common', $model->type->name),
    'dataProvider' => $dataProvider,
    'rowOptions' => [
        'class' => 'text-center'
    ],
    'columns' => [
        [
            'header' => Yii::t('common', 'Курьерка'),
            'headerOptions' => ['style' => 'text-align:center'],
            'attribute' => 'delivery_name'
        ],
        [
            'header' => Yii::t('common', 'Доход ($)'),
            'headerOptions' => ['style' => 'text-align:center'],
            'attribute' => 'amount',
            'value' => function ($model) {
                return number_format($model['amount'], 0, '.', ' ');
            },
        ],
    ],
]); ?>
