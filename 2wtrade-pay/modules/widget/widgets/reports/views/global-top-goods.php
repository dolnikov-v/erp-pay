<?php

use app\modules\widget\extensions\GridView;
use yii\helpers\ArrayHelper;

/**
 * @var \yii\data\ArrayDataProvider $dataProvider
 */

$dateFrom = date('Y-m-d', strtotime('- 31 day'));
$dateTo = date('Y-m-d');

$totalAmount = array_sum(ArrayHelper::getColumn($dataProvider->allModels, 'amount'))

?>

<?= GridView::widget([
    'id' => 'widget-user-' . $model->id . '-grid',
    'exportFilename' => Yii::t('common', $model->type->name),
    'dataProvider' => $dataProvider,
    'emptyText' => Yii::t('common', 'Нет выкупов за последние 31 день'),
    'rowOptions' => [
        'class' => 'text-center'
    ],
    'columns' => [
        [
            'header' => Yii::t('common', 'Товар'),
            'headerOptions' => ['style' => 'text-align:center'],
            'attribute' => 'product_name',
        ],
        [
            'header' => Yii::t('common', 'Страна'),
            'headerOptions' => ['style' => 'text-align:center'],
            'attribute' => 'country_name',
            'value' => function ($model) {
                return Yii::t('common', $model['country_name']);
            },
        ],
        [
            'header' => Yii::t('common', 'Доход ($)'),
            'headerOptions' => ['style' => 'text-align:center'],
            'attribute' => 'amount',
            'value' => function ($model) {
                return number_format($model['amount'], 0, '.', ' ');
            },
            'footer' => number_format($totalAmount, 0, '.', ' '),
        ],
    ],
]); ?>
