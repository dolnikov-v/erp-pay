<?php

use app\modules\widget\extensions\GridView;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

/**
 * @var \yii\data\ArrayDataProvider $dataProvider
 * @var \app\modules\widget\models\WidgetUser $model
 */

$dateFrom = date('Y-m-d', strtotime('- 31 day'));
$dateTo = date('Y-m-d');

$totalAmount = array_sum(ArrayHelper::getColumn($dataProvider->allModels, 'amount'))

?>

<?= GridView::widget([
    'id' => 'widget-user-' . $model->id . '-grid',
    'exportFilename' => Yii::t('common', $model->type->name),
    'dataProvider' => $dataProvider,
    'emptyText' => Yii::t('common', 'Нет выкупов за последние 31 день'),
    'rowOptions' => [
        'class' => 'text-center'
    ],
    'showFooter' => (count($dataProvider->allModels) > 0),
    'footerRowOptions' => [
        'class' => 'text-center report-summary',
        'style' => ' background-color: #b4edd3 !important; color: #333333; font-weight: 400;'
    ],
    'columns' => [
        [
            'header' => Yii::t('common', 'Товар'),
            'headerOptions' => ['style' => 'text-align:center'],
            'attribute' => 'product_name',
            'format' => 'raw',
            'value' => function ($model) use ($dateFrom, $dateTo) {

                $urlParams = [
                    '/report/product/index',
                    's[from]' => $dateFrom,
                    's[to]' => $dateTo,
                    's[dollar]' => 'on',
                    's[product]' => $model['product_id'],
                    's[type]' => 'created_at',
                    's[groupBy]' => 'created_at',
                    's[orderStatus]' => [15, 18],
                ];

                return Html::a($model['product_name'], $urlParams, ['target' => '_blank']);
            }
        ],
        [
            'header' => Yii::t('common', 'Выручка ($)'),
            'headerOptions' => ['style' => 'text-align:center'],
            'attribute' => 'amount',
            'value' => function ($model) {
                return number_format($model['amount'], 0, '.', ' ');
            },
            'footer' => number_format($totalAmount, 0, '.', ' '),
        ],
    ],
]); ?>
<?= '<br><span style="font-size: 75%;">' .Yii::t("common", "*Данные берутся за последние 31 день по выбранной стране") .'</span>' ?>