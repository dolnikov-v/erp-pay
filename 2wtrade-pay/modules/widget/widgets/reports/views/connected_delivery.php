<?php

use app\modules\widget\extensions\GridView;

/**
 * @var \yii\data\ArrayDataProvider $dataProvider
 */
?>

<?= GridView::widget([
    'id' => 'widget-user-' . $model->id . '-grid',
    'exportFilename' => Yii::t('common', $model->type->name),
    'dataProvider' => $dataProvider,
    'rowOptions' => function ($model, $key, $index) {

        $class = 'tr-vertical-align-middle text-center  ';
        $class .= ($model['count'] < 4) ? ' danger' : ' success';

        return [
            'key' => $key,
            'index' => $index,
            'class' => $class
        ];
    },
    'columns' => [
        [
            'header' => Yii::t('common', 'Страна'),
            'headerOptions' => ['style' => 'text-align:center'],
            'attribute' => 'country_name',
            'value' => function ($model) {
                return Yii::t('common', $model['country_name']);
            },
        ],
        [
            'header' => Yii::t('common', 'Курьерок'),
            'headerOptions' => ['style' => 'text-align:center'],
            'attribute' => 'count',
        ],
    ],
]); ?>
<?= '<br><span style="font-size: 75%;">*Курьерская служба попадает в виджет, если за последние 7 дней ей был отгружен хотя бы один заказ</span>' ?>
