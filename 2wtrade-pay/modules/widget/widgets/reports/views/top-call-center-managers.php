<?php

use app\modules\widget\extensions\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * @var \yii\data\ArrayDataProvider $dataProvider
 */

$totalAmount = array_sum(ArrayHelper::getColumn($dataProvider->allModels, 'amount'))

?>

<?= GridView::widget([
    'id' => 'widget-user-' . $model->id . '-grid',
    'exportFilename' => Yii::t('common', $model->type->name),
    'dataProvider' => $dataProvider,
    'rowOptions' => [
        'class' => 'text-center'
    ],
    'columns' => [
        [
            'header' => Yii::t('common', 'Менеджер'),
            'headerOptions' => ['style' => 'text-align:center'],
            'attribute' => 'manager',
            'format' => 'raw',
            'content' => function ($model) {
                    return Html::a($model['manager'], [
                        '/report/approve-by-country/index',
                        's[user_ids]' => $model['manager_id'],
                        's[from]' => date('d.m.Y',time() - 90*60*60*24),
                        's[to]' => date('d.m.Y',time()),
                        's[country_ids]' => explode(',',$model['country_ids']),
                    ], [
                        'target' => '_blank',
                    ]);
            },
        ],
        [
            'header' => Yii::t('common', 'Доход ($)'),
            'headerOptions' => ['style' => 'text-align:center'],
            'attribute' => 'income',
            'value' => function ($model) {
                return round($model['income']);
            },
        ],
        [
            'header' => Yii::t('common', 'Подтвержденные заказы (%)'),
            'headerOptions' => ['style' => 'text-align:center'],
            'value' => function ($model) {
                return round($model['approvedOrders']*100/$model['allOrders'],2) . "%";
            },
        ],
    ],
]); ?>
