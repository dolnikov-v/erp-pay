<?php

use app\modules\widget\extensions\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var \yii\data\ArrayDataProvider $dataProvider
 */

$dateFrom = date('Y-m-d', strtotime('- 31 day'));
$dateTo = date('Y-m-d');
?>

<?= GridView::widget([
    'id' => 'widget-user-' . $model->id . '-grid',
    'exportFilename' => Yii::t('common', $model->type->name),
    'dataProvider' => $dataProvider,
    'rowOptions' => [
        'class' => 'text-center'
    ],
    'columns' => [
        [
            'header' => Yii::t('common', 'Страна / Оператор'),
            'headerOptions' => ['style' => 'text-align:center'],
            'attribute' => 'country_operator',
            'value' => function ($model) {
                return Yii::t('common', $model['country_name']) . ' / ' . $model['operator'];
            },
        ],
        [
            'header' => Yii::t('common', 'Потери ($)'),
            'headerOptions' => ['style' => 'text-align:center'],
            'attribute' => 'trash_amount',
            'value' => function ($model) {
                return number_format($model['trash_amount'], 2, '.', ' ');
            },
        ],
        [
            'header' => Yii::t('common', '% Треша'),
            'headerOptions' => ['style' => 'text-align:center'],
            'attribute' => 'trash_percent',
            'format' => 'raw',
            'value' => function ($model) use ($dateFrom, $dateTo) {

                $percent = $model['trash_count'] / $model['lead_count'] * 100;
                $text = number_format($percent, 0, '.', ' ') . '%';

                $url = Url::to([
                    '/order/index',
                    'DateFilter[dateType]' => 'c_sent_at',
                    'DateFilter[dateFrom]' => $dateFrom,
                    'DateFilter[dateTo]' => $dateTo,
                    'StatusFilter[not][]' => 0,
                    'StatusFilter[status]' => 25,
                    'TextFilter[entity]' => 'last_foreign_operator',
                    'TextFilter[text]' => $model['operator'],
                ]);
                $url = $model['country_slug'] . substr($url, 3);

                return Html::a($text, $url, ['target' => '_blank']);
            },
        ],
    ],
]); ?>
