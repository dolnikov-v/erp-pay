<?php

use app\modules\widget\extensions\GridView;

/**
 * @var \yii\data\ArrayDataProvider $dataProvider
 * @var \app\modules\widget\models\WidgetUser $model
 * @var string $productName
 * @var integer $productId
 */

?>

<?= GridView::widget([
    'id' => 'widget-user-' . $model->id . '-' . $productId . '-grid',
    'exportFilename' => Yii::t('common', $model->type->name),
    'exportMoveDropDown' => ($productId == 'all'),
    'dataProvider' => $dataProvider,
    'rowOptions' => [
        'class' => 'text-center'
    ],
    'columns' => [
        [
            'header' => Yii::t('common', 'Дата'),
            'headerOptions' => ['style' => 'text-align:center'],
            'attribute' => 'date'
        ],
        [
            'header' => Yii::t('common', 'Выкуп'),
            'headerOptions' => ['style' => 'text-align:center'],
            'attribute' => 'percent',
            'value' => function ($model) {
                return number_format($model['percent'], 2, '.', '') . '%';
            },
        ],
    ]
]); ?>
