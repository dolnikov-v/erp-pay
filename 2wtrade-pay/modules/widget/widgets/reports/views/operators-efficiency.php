<?php

use app\modules\widget\extensions\GridView;

/**
 * @var \yii\data\ArrayDataProvider $dataProvider
 * @var integer $approvePlan
 */

?>

<?= GridView::widget([
    'id' => 'widget-user-' . $model->id . '-grid',
    'exportFilename' => Yii::t('common', $model->type->name),
    'dataProvider' => $dataProvider,
    'rowOptions' => function ($model, $key, $index) use ($approvePlan) {

        $class = 'tr-vertical-align-middle text-center';
        if ($model['workdays'] * $approvePlan <= $model['approve_count'] &&
            $model['approve_count'] / $model['lead_count'] > 0.45
        ) {
            $class .= ' success';
        } else {
            $class .= ' danger';
        }

        return [
            'key' => $key,
            'index' => $index,
            'class' => $class
        ];
    },
    'columns' => [
        [
            'header' => Yii::t('common', 'Оператор'),
            'headerOptions' => ['style' => 'text-align:center'],
            'attribute' => 'operator',
        ],
        [
            'header' => Yii::t('common', 'Выручка ($)'),
            'headerOptions' => ['style' => 'text-align:center'],
            'attribute' => 'profit',
            'value' => function ($model) {
                return number_format($model['profit'], 2, '.', ' ');
            },
        ],
        [
            'header' => Yii::t('common', 'Ср. чек ($)'),
            'headerOptions' => ['style' => 'text-align:center'],
            'attribute' => 'bill_avg',
            'value' => function ($model) {
                return number_format($model['bill_avg'], 2, '.', ' ');
            },
        ],
        [
            'header' => Yii::t('common', 'Апрув'),
            'headerOptions' => ['style' => 'text-align:center'],
            'attribute' => 'approve_percent',
            'value' => function ($model) {

                $value = '-';

                if ($model['lead_count']) {
                    $percent = $model['approve_count'] / $model['lead_count'] * 100;
                    $value = number_format($percent, 2, '.', ' ') . '%' ;
                }

                return $value;
            },
        ],
        [
            'header' => Yii::t('common', 'Кол-во апрувов'),
            'headerOptions' => ['style' => 'text-align:center'],
            'attribute' => 'approve_count',
        ],
        [
            'header' => Yii::t('common', 'Выкуп'),
            'headerOptions' => ['style' => 'text-align:center'],
            'attribute' => 'buyout_percent',
            'value' => function ($model) {

                $value = '-';

                if ($model['lead_count']) {
                    $percent = $model['buyout_count'] / $model['lead_count'] * 100;
                    $value = number_format($percent, 2, '.', ' ') . '%';
                }

                return $value;
            },
        ],
        [
            'header' => Yii::t('common', 'Кол-во лидов'),
            'headerOptions' => ['style' => 'text-align:center'],
            'attribute' => 'lead_count',
        ],
    ]
]) ?>
<?= '<br><span style="font-size: 75%;">' .Yii::t("common", "*Данные берутся за последние 31 день по выбранной стране") .'</span>' ?>
