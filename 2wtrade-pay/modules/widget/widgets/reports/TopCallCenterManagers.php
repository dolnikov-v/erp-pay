<?php
namespace app\modules\widget\widgets\reports;

use app\models\UserCountry;
use app\modules\widget\models\WidgetCache;
use app\modules\widget\widgets\CacheableWidget;
use yii\data\ArrayDataProvider;
use app\models\CurrencyRate;
use app\modules\order\models\Order;
use app\models\User;
use app\models\AuthAssignment;
use app\modules\widget\models\WidgetUser;
use Yii;
use app\modules\order\models\OrderStatus;

class TopCallCenterManagers extends CacheableWidget
{

    /**
     * @var WidgetUser
     */
    public $model;

    /**
     * @var bool
     */
    public $export = true;

    private function findAll()
    {
        $approved = join(', ', OrderStatus::getApproveList());
        $buyOut = join(', ', OrderStatus::getBuyoutList());

        $totalPrice = Order::tableName() . '.price_total / ' . CurrencyRate::tableName() . '.rate';
        $allOrders = Order::tableName() . '.id';
        $orderStatus = Order::tableName() . ".status_id";
        $approvedOrders = Order::tableName() . ".status_id IN ($approved)";

        $query = Order::find()
            ->joinWith(['country.userCountry', 'country.userCountry.user', 'country.userCountry.user.roles', 'country.currencyRate'], false)
            ->select([
                'manager' => User::tableName() . '.username',
                'manager_id' => User::tableName() . '.id',
                'country_ids' => 'GROUP_CONCAT(DISTINCT' . UserCountry::tableName() . '.country_id)',
                'role' => 'GROUP_CONCAT(' . AuthAssignment::tableName() . '.item_name)',
                'allOrders' => "COUNT($allOrders)",
                'approvedOrders' => "COUNT($approvedOrders OR NULL)",
                'income' => "SUM(CASE WHEN {$orderStatus} IN ({$buyOut}) THEN {$totalPrice} ELSE NULL END)",
            ])
            ->where([AuthAssignment::tableName() . '.item_name' => 'callcenter.manager'])
            ->andWhere(["<>", User::tableName() . '.username', 'smykov'])
            ->andWhere([">=", Order::tableName() . '.created_at', time() - self::PERIOD*60*60*24])
            ->andWhere(['NOT IN', Order::tableName() .'.source_id', User::getRejectedSources($this->model->user_id)])
            ->groupBy('manager_id')
            ->orderBy(['income' => SORT_DESC])
            ->asArray();
        return $query->all();
    }

    /**
     * @return string
     */

    public function run()
    {

        $data = $this->data ?: WidgetCache::readCachedData($this->model->id, $this->model->type->by_country ? Yii::$app->user->country->id : null);
        if (!$data && $this->onlineLoad) {
            $data = $this->findAll();
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'pagination' => false,
        ]);

        return $this->render('top-call-center-managers', [
            'model' => $this->model,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function runData()
    {
        return $this->findAll();
    }
}