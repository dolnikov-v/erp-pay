<?php

namespace app\modules\widget\widgets\reports;


use app\models\User;
use app\modules\delivery\models\Delivery;
use app\modules\report\components\ReportFormDeliveryDebts;
use app\modules\report\models\ReportDeliveryDebts;
use app\modules\widget\models\WidgetUser;
use yii\base\Widget;
use yii\data\ArrayDataProvider;

/**
 * Class DeliveryDebts
 * @package app\modules\widget\widgets\reports
 */
class DeliveryDebts extends Widget
{
    /**
     * @var WidgetUser
     */
    public $model;

    /**
     * @var bool
     */
    public $export = true;

    /**
     * @return ReportDeliveryDebts[]
     */
    protected function getData()
    {
        $query = ReportDeliveryDebts::find()
            ->joinWith(['delivery'])
            ->with(['delivery.country'])
            ->where([Delivery::tableName() . '.country_id' => array_keys(User::getAllowCountries())]);
        return $query->all();
    }

    /**
     * @return string
     */
    public function run()
    {
        $records = $this->getData();

        $groupedRecords = [];
        $commonSum = 0;
        foreach ($records as $record) {
            if (!isset($groupedRecords[$record->delivery->country_id])) {
                $groupedRecords[$record->delivery->country_id] = [
                    'country_name' => $record->delivery->country->name,
                    'country_id' => $record->delivery->country_id,
                    'delivery_id' => $record->delivery_id,
                    'month' => date('Y-m-d'),
                    'sum_total' => 0,
                ];
            }
            $groupedRecords[$record->delivery->country_id]['sum_total'] += $record->decodedData['end_sum_dollars'] ?? 0;
            $commonSum += $record->decodedData['end_sum_dollars'] ?? 0;
        }

        $groupedRecords = ReportFormDeliveryDebts::sortRecords($groupedRecords, '-sum_total');
        $dataProvider = new ArrayDataProvider([
            'allModels' => $groupedRecords,
            'sort' => false,
            'pagination' => false
        ]);
        return $this->render('delivery-debts', [
            'model' => $this->model,
            'dataProvider' => $dataProvider,
            'commonSum' => $commonSum
        ]);
    }
}
