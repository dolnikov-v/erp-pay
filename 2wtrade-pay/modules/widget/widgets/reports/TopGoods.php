<?php

namespace app\modules\widget\widgets\reports;

use app\modules\callcenter\models\CallCenterRequest;
use app\modules\widget\models\WidgetCache;
use app\modules\widget\widgets\CacheableWidget;
use yii\data\ArrayDataProvider;
use app\models\User;
use app\models\Product;
use app\models\CurrencyRate;
use app\modules\order\models\Order;
use app\modules\order\models\OrderProduct;
use app\modules\widget\models\WidgetUser;
use app\modules\report\components\ReportFormFinance;
use Yii;

class TopGoods extends CacheableWidget
{
    /**
     * @var WidgetUser
     */
    public $model;

    /**
     * @var bool
     */
    public $export = true;

    private function findAll($params)
    {
        $mapStatuses = (new ReportFormFinance)->getMapStatuses();
        $statuses = $mapStatuses[Yii::t('common', 'Выкуплено')];

        $date = date('Y-m-d 00:00:00', strtotime('-' . $params['days'] . ' day'));
        $time = Yii::$app->formatter->asTimestamp($date);
        if (!is_null($this->timeOffset)) {
            $time += $this->timeOffset;
        }

        $totalPrice =
            OrderProduct::tableName() . '.price * ' .
            OrderProduct::tableName() . '.quantity / ' .
            CurrencyRate::tableName() . '.rate';

        $query = Order::find()
            ->joinWith(['orderProducts', 'orderProducts.product', 'callCenterRequest', 'country', 'country.currencyRate'], false)
            ->select([
                'product_id' => Product::tableName() . '.id',
                'product_name' => Product::tableName() . '.name',
                'amount' => 'sum(' . $totalPrice . ')',
            ])
            ->andWhere([Order::tableName() . '.country_id' => $params['country']])
            ->andWhere([Order::tableName() . '.status_id' => $statuses])
            ->andWhere(['>=', CallCenterRequest::tableName() . '.sent_at', $time])
            ->andWhere(['NOT IN', Order::tableName() .'.source_id', User::getRejectedSources($this->model->user_id)])
            ->groupBy(Product::tableName() . '.id')
            ->orderBy(['amount' => SORT_DESC])
            ->asArray();

        return $query->all();
    }

    /**
     * @return string
     */

    public function run()
    {
        $data = $this->data ?: WidgetCache::readCachedData($this->model->id, $this->model->type->by_country ? yii::$app->user->country->id : null);
        if (!$data && $this->onlineLoad) {
            $data = $this->findAll([
                'country' => Yii::$app->user->country->id,
                'days' => self::PERIOD,
            ]);
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'pagination' => false,
        ]);

        return $this->render('top-goods', [
            'model' => $this->model,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function runData()
    {
        return $this->findAll(['country' => $this->countryId, 'days' => self::PERIOD]);
    }
}