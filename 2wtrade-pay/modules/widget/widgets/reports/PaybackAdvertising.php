<?php
namespace app\modules\widget\widgets\reports;

use app\models\Country;
use app\models\CurrencyRate;
use app\modules\order\models\Order;
use app\modules\report\components\ReportFormFinance;
use app\modules\widget\models\WidgetCache;
use app\modules\widget\models\WidgetUser;
use app\modules\widget\widgets\CacheableWidget;
use Yii;
use yii\data\ArrayDataProvider;
use app\models\User;


/**
 * Class PaysForAdvertising
 * @package app\modules\widget\widgets\reports
 */
class PaybackAdvertising extends CacheableWidget
{
    /**
     * @var WidgetUser
     */
    public $model;

    /**
     * @var bool
     */
    public $export = true;

    /**
     * @var ReportFormFinance
     */

    private $form;

    public function init()
    {
        $this->form = new ReportFormFinance();

        parent::init();
    }


    private function findAll($params)
    {
        $mapStatuses = $this->form->getMapStatuses();
        $allStatuses = [];
        foreach ($mapStatuses as $name => $statuses) {
            $allStatuses = array_merge($allStatuses, $statuses);
        }

        $date = date('Y-m-d 00:00:00', strtotime('-' . $params['days'] . ' day'));
        $time = Yii::$app->formatter->asTimestamp($date);
        if (!is_null($this->timeOffset)) {
            $time += $this->timeOffset;
        }

        $allStatuses = array_unique($allStatuses);
        $statuses = $mapStatuses[Yii::t('common', 'Выкуплено')];

        $totalPrice = '(' .
            Order::tableName() . '.price_total + ' .
            Order::tableName() . '.delivery) / ' .
            CurrencyRate::tableName() . '.rate';

        $costs = '-(' . Order::tableName() . '.income / ' . CurrencyRate::tableName() . '.rate)';
        
        $condition = 'if(' . Order::tableName() . '.status_id in (' . join(',', $statuses) . '), '  . $totalPrice . ', ' . $costs . ')';

        $query = Order::find()
            ->select([
                'profit' => 'coalesce(sum(' . $condition . '), 0)',
                'country_name' => Country::tableName() . '.name',
            ])
            ->joinWith(['country', 'country.currencyRate'], false)
            ->where([
                Order::tableName() . '.country_id' => $params['countries'],
                Order::tableName() . '.status_id' => $allStatuses,
            ])
            ->andWhere(Order::tableName() . '.created_at >= :time', [':time' => $time])
            ->andWhere(['NOT IN', Order::tableName() .'.source_id', User::getRejectedSources($this->model->user_id)])
            ->groupBy(Country::tableName() . '.id')
            ->orderBy(['profit' => SORT_DESC])
            ->asArray();

        return $query->all();
    }

    /**
     * @return string
     */

    public function run()
    {
        //чтение данных из кеша
        $data = $this->data ?: WidgetCache::readCachedData($this->model->id, $this->model->type->by_country ? yii::$app->user->country->id : null);
        if (!$data && $this->onlineLoad) {
            $data = $this->findAll([
                'days' => self::PERIOD,
                'countries' => array_keys(User::getAllowCountries()),
            ]);
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'pagination' => false,
        ]);

        return $this->render('payback-advertising', [
            'model' => $this->model,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function runData()
    {
        $params = [
            'days' => self::PERIOD,
            'countries' => $this->countryIds
        ];
        return $this->findAll($params);
    }
}
