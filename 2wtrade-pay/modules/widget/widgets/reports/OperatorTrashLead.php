<?php

namespace app\modules\widget\widgets\reports;

use app\models\Country;
use app\models\CurrencyRate;
use app\models\User;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\order\models\Order;
use app\modules\report\components\ReportFormForeignOperator;
use app\modules\widget\models\WidgetCache;
use app\modules\widget\widgets\CacheableWidget;
use Yii;
use yii\data\ArrayDataProvider;
use app\modules\widget\models\WidgetUser;

class OperatorTrashLead extends CacheableWidget
{
    /**
     * @var WidgetUser
     */
    public $model;

    /**
     * @var bool
     */
    public $export = true;

    private function findAll($params)
    {

        $mapStatuses = (new ReportFormForeignOperator())->getMapStatuses();
        $trashStatuses = $mapStatuses[Yii::t('common', 'Треш')];
        $allStatuses = $mapStatuses[Yii::t('common', 'Всего')];

        $date = date('Y-m-d 00:00:00', strtotime('-' . $params['days'] . ' day'));
        $time = Yii::$app->formatter->asTimestamp($date);
        if (!is_null($this->timeOffset)) {
            $time += $this->timeOffset;
        }

        $price = Order::tableName() . '.price / ' .CurrencyRate::tableName(). '.rate';

        $all = Order::find()
            ->joinWith(['country', 'country.currencyRate', 'callCenterRequest'], false)
            ->select([
                'operator' => CallCenterRequest::tableName() . '.last_foreign_operator',
                'country_name' => Country::tableName() . '.name',
                'country_slug' => Country::tableName() . '.slug',
                'trash_amount' => 'sum(if(' . Order::tableName() . '.status_id in (' . join(',', $trashStatuses) . '), ' . $price . ', 0))',
                'trash_count' => 'count(' . Order::tableName() . '.status_id in (' . join(',', $trashStatuses) . ') or null)',
                'lead_count' => 'count(' . Order::tableName() . '.status_id in (' . join(',', $allStatuses) . ') or null)',
            ])
            ->where([
                Order::tableName() . '.status_id' => $allStatuses,
                Order::tableName() . '.country_id' => $params['countries'],
            ])
            ->andWhere(['>=', CallCenterRequest::tableName() . '.created_at', $time])
            ->andWhere(['>=', CallCenterRequest::tableName() . '.last_foreign_operator', 1])
            ->andWhere(['NOT IN', Order::tableName() .'.source_id', User::getRejectedSources($this->model->user_id)])
            ->groupBy([Order::tableName() . '.country_id', CallCenterRequest::tableName() . '.last_foreign_operator'])
            ->orderBy(['trash_amount' => SORT_DESC])
            ->asArray()
            ->all();

        return $all;
    }

    /**
     * @return string
     */

    public function run()
    {
        //чтение данных из кеша
        $data = $this->data ?: WidgetCache::readCachedData($this->model->id, $this->model->type->by_country ? yii::$app->user->country->id : null);
        if (!$data && $this->onlineLoad) {
            $data = $this->findAll([
                'countries' => array_keys(User::getAllowCountries()),
                'days' => self::PERIOD
            ]);
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'pagination' => false,
        ]);

        return $this->render('operator-trash-lead', [
            'model' => $this->model,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function runData() {
        return $this->findAll([
            'countries' => $this->countryIds,
            'days' => self::PERIOD
        ]);
    }
}