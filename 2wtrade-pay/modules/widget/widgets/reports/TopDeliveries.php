<?php

namespace app\modules\widget\widgets\reports;

use app\modules\delivery\models\Delivery;
use app\modules\widget\models\WidgetCache;
use app\modules\widget\widgets\CacheableWidget;
use yii\data\ArrayDataProvider;
use app\models\User;
use app\models\CurrencyRate;
use app\modules\order\models\Order;
use app\modules\widget\models\WidgetUser;
use app\modules\report\components\ReportFormFinance;
use Yii;

class TopDeliveries extends CacheableWidget
{
    /**
     * @var WidgetUser
     */
    public $model;

    /**
     * @var bool
     */
    public $export = true;

    private function findAll($params)
    {
        $mapStatuses = (new ReportFormFinance)->getMapStatuses();
        $statuses = $mapStatuses[Yii::t('common', 'Выкуплено')];

        $date = date('Y-m-d 00:00:00', strtotime('-' . $params['days'] . ' day'));
        $time = Yii::$app->formatter->asTimestamp($date);
        if (!is_null($this->timeOffset)) {
            $time += $this->timeOffset;
        }

        $totalPrice = '(' .
            Order::tableName() . '.price_total + ' .
            Order::tableName() . '.delivery) / ' .
            CurrencyRate::tableName() . '.rate';

        $query = Order::find()
            ->joinWith(['country', 'country.currencyRate', 'deliveryRequest', 'deliveryRequest.delivery'], false)
            ->select([
                'delivery_name' => Delivery::tableName() . '.name',
                'amount' => 'sum(' . $totalPrice . ')',
            ])
            ->andWhere([Order::tableName() . '.country_id' => $params['country']])
            ->andWhere([Order::tableName() . '.status_id' => $statuses])
            ->andWhere(['>=', Order::tableName() . '.created_at', $time])
            ->andWhere(['NOT IN', Order::tableName() .'.source_id', User::getRejectedSources($this->model->user_id)])
            ->groupBy(Delivery::tableName() . '.id')
            ->orderBy(['amount' => SORT_DESC])
            ->asArray();

        return $query->all();
    }

    /**
     * @return string
     */

    public function run()
    {
        $data = $this->data ?: WidgetCache::readCachedData($this->model->id, $this->model->type->by_country ? yii::$app->user->country->id : null);
        if (!$data && $this->onlineLoad) {
            $data = $this->findAll([
                'country' => Yii::$app->user->country->id,
                'days' => self::PERIOD,
            ]);
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'pagination' => false,
        ]);

        return $this->render('top-deliveries', [
            'model' => $this->model,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function runData()
    {
        return $this->findAll(['country' => $this->countryId, 'days' => self::PERIOD]);
    }
}