<?php

namespace app\modules\widget\widgets\reports;

use app\models\User;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\order\models\Order;
use app\modules\report\components\ReportFormFinance;
use app\modules\widget\models\WidgetCache;
use app\modules\widget\models\WidgetUser;
use app\modules\widget\widgets\CacheableWidget;
use Yii;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use app\models\Country;

/**
 * Class BuyOutForCountries
 * @package app\modules\widget\widgets\reports
 */
class BuyOutForCountries extends CacheableWidget
{
    /**
     * @var WidgetUser
     */
    public $model;

    /**
     * @var bool
     */
    public $export = true;

    /**
     * @param $params
     * @return array;
     */

    private function getData($params)
    {
        $mapStatuses = (new ReportFormFinance)->getMapStatuses();

        $allStatuses = [];
        foreach ($mapStatuses as $name => $statuses) {
            $allStatuses = array_merge($allStatuses, $statuses);
        }

        $allStatuses = array_unique($allStatuses);
        $statuses = $mapStatuses[Yii::t('common', 'Выкуплено')];

        $date = date('Y-m-d 00:00:00', strtotime('-' . $params['days'] . ' day'));

        $time = Yii::$app->formatter->asTimestamp($date);

        if (!is_null($this->timeOffset)) {
            $time += $this->timeOffset;
        }

        $query = Order::find()
            ->joinWith(['country', 'country.currencyRate', 'callCenterRequest'], false)
            ->select([
                'country_id' => Country::tableName() . '.id',
                'count' => 'count(1)',
            ])
            ->andWhere([Order::tableName() . '.country_id' => $params['countries']])
            ->andWhere(['>=', CallCenterRequest::tableName() . '.sent_at', $time])
            ->andWhere(['NOT IN', Order::tableName() .'.source_id', User::getRejectedSources($this->model->user_id)])
            ->groupBy(Country::tableName() . '.id');

        $queryAll = clone $query;

        $buyouts = $query
            ->andWhere([Order::tableName() . '.status_id' => $statuses])
            ->asArray()
            ->indexBy('country_id')
            ->all();

        $all = $queryAll
            ->addSelect(['country_name' => Country::tableName() . '.name'])
            ->andWhere([Order::tableName() . '.status_id' => $allStatuses])
            ->asArray()
            ->indexBy('country_id')
            ->all();

        $data = [];
        foreach ($all as $key => $order) {

            if (!$order['count']) {
                continue;
            }

            $count = 0;
            if ($buyout = ArrayHelper::getValue($buyouts, $key)) {
                $count = $buyout['count'];
            }

            $data[] = [
                'country_name' => $order['country_name'],
                'count' => $count,
                'percent' => $count / $order['count']  * 100,
            ];
        }

        ArrayHelper::multisort($data, 'percent');

        return $data;
    }

    public function run()
    {

        //чтение данных из кеша
        $data = $this->data ?: WidgetCache::readCachedData($this->model->id, $this->model->type->by_country ? yii::$app->user->country->id : null);
        if (!$data && $this->onlineLoad) {
            $params = [
                'countries' => array_keys(User::getAllowCountries()),
                'days' => self::PERIOD,
            ];
            $data = $this->getData($params);
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'pagination' => false,
        ]);

        return $this->render('buy-out-for-countries', [
            'model' => $this->model,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function runData()
    {
        $params = [
            'countries' => $this->countryIds,
            'days' => self::PERIOD,
        ];
        return $this->getData($params);
    }
}
