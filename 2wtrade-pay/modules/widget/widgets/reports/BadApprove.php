<?php

namespace app\modules\widget\widgets\reports;

use app\models\Country;
use app\models\Product;
use app\models\User;
use app\modules\order\models\Order;
use app\modules\order\models\OrderProduct;
use app\modules\report\components\ReportFormCallCenter;
use app\modules\widget\models\WidgetCache;
use app\modules\widget\models\WidgetUser;
use app\modules\widget\widgets\CacheableWidget;
use Yii;
use yii\data\ArrayDataProvider;

/**
 * Class BadApprove
 * @package app\modules\widget\widgets\reports
 */
class BadApprove extends CacheableWidget
{
    const PERIOD = 7;

    /**
     * @var WidgetUser
     */
    public $model;

    /**
     * @var bool
     */
    public $export = true;

    private function findAll($params)
    {
        $mapStatuses = (new ReportFormCallCenter)->getMapStatuses();
        $allStatuses = [];
        foreach ($mapStatuses as $name => $statuses) {
            $allStatuses = array_merge($allStatuses, $statuses);
        }

        $allStatuses = array_unique($allStatuses);
        $statuses = $mapStatuses[Yii::t('common', 'Подтверждено')];

        $condition = [
            'if(' . Order::tableName() . '.status_id in (' . join(',', $statuses) . '), 1, 0)',
            'if(' . Order::tableName() . '.status_id in (' . join(',', $allStatuses) . '), 1, 0)'
        ];

        $date = date('Y-m-d 00:00:00', strtotime('-' . $params['days'] . ' day'));
        $time = Yii::$app->formatter->asTimestamp($date);

        if (!is_null($this->timeOffset)) {
            $time += $this->timeOffset;
        }

        $subQuery = Order::find()
            ->select([
                'order_id' => Order::tableName() . '.id',
                'product_id' => OrderProduct::tableName() . '.product_id'
            ])
            ->joinWith(['orderProducts', 'callCenterRequest'], false)
            ->andWhere([Order::tableName() . '.status_id' => $allStatuses])
            ->andWhere([Order::tableName() . '.country_id' => $params['countries']])
            ->andWhere(['>=', Order::tableName() . '.created_at', $time])
            ->andWhere(['NOT IN', Order::tableName() .'.source_id', User::getRejectedSources($this->model->user_id)])
            ->distinct();

        $data = Order::find()
            ->select([
                'country_name' => Country::tableName() . '.name',
                'product_name' => Product::tableName() . '.name',
                'approve' => 'sum(' . $condition[0] . ')',
                'percent' => 'sum(' . $condition[0] . ') / sum(' . $condition[1] . ') * 100',
            ])
            ->joinWith(['country', 'callCenterRequest'], false)
            ->innerJoin(['t' => $subQuery], 't.order_id = ' . Order::tableName() . '.id')
            ->leftJoin(Product::tableName(), 't.product_id = ' . Product::tableName() . '.id')
            ->andWhere([Order::tableName() . '.country_id' => $params['countries']])
            ->andWhere([Order::tableName() . '.status_id' => $allStatuses])
            ->andWhere(['>=', Order::tableName() . '.created_at', $time])
            ->andWhere(['NOT IN', Order::tableName() .'.source_id', User::getRejectedSources($this->model->user_id)])
            ->andHaving(['>=', 'approve', 10])
            ->groupBy([Country::tableName() . '.id', Product::tableName() . '.id'])
            ->orderBy(['percent' => SORT_ASC])
            ->limit(20)
            ->asArray()
            ->all();

        return $data;
    }

    /**
     * @return string
     */
    public function run()
    {
        //чтение данных из кеша
        $data = $this->data ?: WidgetCache::readCachedData($this->model->id, $this->model->type->by_country ? yii::$app->user->country->id : null);
        if (!$data && $this->onlineLoad) {
            $params = [
                'days' => self::PERIOD,
                'countries' => array_keys(User::getAllowCountries()),
            ];
            $data = $this->findAll($params);
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'pagination' => false,
        ]);

        return $this->render('bad-approve', [
            'dataProvider' => $dataProvider,
            'model' => $this->model,
        ]);
    }

    public function runData()
    {
        return $this->findAll(['days' => self::PERIOD, 'countries' => $this->countryIds]);
    }
}
