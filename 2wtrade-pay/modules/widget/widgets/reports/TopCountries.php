<?php

namespace app\modules\widget\widgets\reports;

use app\models\User;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\report\components\ReportFormCallCenter;
use app\modules\report\components\ReportFormFinance;
use app\modules\widget\models\WidgetCache;
use app\modules\widget\widgets\CacheableWidget;
use yii\data\ArrayDataProvider;
use app\models\Country;
use app\models\CurrencyRate;
use app\modules\order\models\Order;
use app\modules\widget\models\WidgetUser;
use Yii;
use yii\helpers\ArrayHelper;
use yii\db\Expression;
use app\modules\order\models\OrderStatus;

class TopCountries extends CacheableWidget
{
    /**
     * @var WidgetUser
     */
    public $model;

    /**
     * @param array $params
     * @return array
     */

    /**
     * @var bool
     */
    public $export = true;

    private function getData($params)
    {
        $mapStatuses = (new ReportFormFinance())->getMapStatuses();
        $buyoutStatuses = $mapStatuses[Yii::t('common', 'Выкуплено')];
        $financeStatuses = [];
        foreach ($mapStatuses as $name => $statuses) {
            $financeStatuses = array_merge($financeStatuses, $statuses);
        }

        $mapStatuses = (new ReportFormCallCenter())->getMapStatuses();
        $approveStatuses = $mapStatuses[Yii::t('common', 'Подтверждено')];
        $allStatuses = [];
        foreach ($mapStatuses as $name => $statuses) {
            $allStatuses = array_merge($allStatuses, $statuses);
        }

        $date = date('Y-m-d 00:00:00', strtotime('-' . $params['days'] . ' day'));
        $time = Yii::$app->formatter->asTimestamp($date);

        if (!is_null($this->timeOffset)) {
            $time += $this->timeOffset;
        }

        $totalPrice = '(' . Order::tableName() . '.price_total + ' . Order::tableName() . '.delivery) / ' . CurrencyRate::tableName() . '.rate';

        $data = Order::find()
            ->joinWith(['country', 'country.currencyRate', 'callCenterRequest'], false)
            ->select([
                'country_id' => Country::tableName() . '.id',
                'country_name' => Country::tableName() . '.name',
                'profit' => 'sum(if(' . Order::tableName() . '.status_id in (' . join(',', $buyoutStatuses) . '), ' . $totalPrice . ', 0))',
                'approve_count' => 'count(' . Order::tableName() . '.status_id in (' . join(',', $approveStatuses) . ') or null)',
                'buyout_count' => 'count(' . Order::tableName() . '.status_id in (' . join(',', $buyoutStatuses) . ') or null)',
                'finance_count' => 'count(' . Order::tableName() . '.status_id in (' . join(',', $financeStatuses) . ') or null)',
                'lead_count' => 'count(' . Order::tableName() . '.status_id in (' . join(',', $allStatuses) . ') or null)',
                'sr_chk_cc' => new Expression('COALESCE(AVG(CASE WHEN ' . Order::tableName() . '.status_id IN (' . join(', ', OrderStatus::getApproveList()) . ') THEN (' . Order::tableName() . '.price_total / ' . CurrencyRate::tableName() . '.rate  + ' . Order::tableName() . '.delivery/ ' . CurrencyRate::tableName() . '.rate) ELSE NULL END), 0)'),
            ])
            ->andWhere([Order::tableName() . '.country_id' => $params['countries']])
            ->andWhere([Order::tableName() . '.status_id' => $allStatuses])
            ->andWhere(['>=', CallCenterRequest::tableName() . '.sent_at', $time])
            ->andWhere(['NOT IN', Order::tableName() .'.source_id', User::getRejectedSources($this->model->user_id)])
            ->groupBy(Order::tableName() . '.country_id')
            ->asArray()
            ->indexBy('country_id')
            ->all();

        foreach ($data as &$item) {
            $item['approve_percent'] = (!$item['lead_count']) ? 0 : $item['approve_count'] / $item['lead_count'];
            $item['buyout_percent'] = (!$item['finance_count']) ? 0 : $item['buyout_count'] / $item['finance_count'];
        }
        unset($item);

        return $data;
    }

    /**
     * @param @array $data
     * @param @array $prevData
     * @param @string $sort
     * @return array
     */

    public function calcOffset($data, $prevData, $sort)
    {
        $countries = array_keys($data);
        $prevCountries = array_keys($prevData);

        if ($diff = array_diff($prevCountries, $countries)) {

            foreach ($diff as $id) {

                $data[$id] = [
                    'country_id' => $prevData[$id]['country_id'],
                    'country_name' => $prevData[$id]['country_name'],
                    'profit' => 0,
                    'approve_percent' => 0,
                    'buyout_percent' => 0,
                    'sr_chk_cc' => 0,
                ];
            }
        }

        ArrayHelper::multisort($data, [$sort, 'country_name'], [SORT_DESC, SORT_ASC]);
        ArrayHelper::multisort($prevData, [$sort, 'country_name'], [SORT_DESC, SORT_ASC]);

        $countries = array_unique($prevCountries + $countries);

        $offsets = array_fill_keys($countries, '');
        foreach ($prevData as $key => $item) {
            $offsets[$item['country_id']] += $key;
        }

        foreach ($data as $key => $item) {

            if (in_array($item['country_id'], $prevCountries)) {
                $offsets[$item['country_id']] -= $key;
            } else {
                $offsets[$item['country_id']] = $key;
            }
        }

        foreach ($data as &$item) {
            $item['offset'] = $offsets[$item['country_id']];
        }
        unset($item);

        return $data;
    }

    /**
     * @return string
     */

    public function run()
    {
        $data = [];
        $prevData = [];
        $cachedData = $this->data ?: WidgetCache::readCachedData($this->model->id, $this->model->type->by_country ? yii::$app->user->country->id : null);
        if ($cachedData) {
            $data = $cachedData['data'];
            $prevData = $cachedData['prevData'];
        }
        if ((!$data || !$prevData) && $this->onlineLoad) {
            $countries = array_keys(User::getAllowCountries());

            $data = $this->getData([
                'countries' => $countries,
                'days' => self::PERIOD,
            ]);

            $prevData = $this->getData([
                'countries' => $countries,
                'days' => self::PERIOD + 31,
            ]);

        }

        $sort = Yii::$app->request->get('sort', '-profit');
        if ($desc = substr($sort, 0, 1) == '-') {
            $sort = substr($sort, 1);
        }

        $data = $this->calcOffset($data, $prevData, $sort);
        ArrayHelper::multisort($data, [$sort, 'country_name'], [SORT_DESC, $desc ? SORT_ASC : SORT_DESC]);

        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'pagination' => false,
            'sort' => [
                'attributes' => [
                    'profit' => [
                        'default' => SORT_DESC,
                    ],
                    'approve_percent' => [
                        'default' => SORT_DESC
                    ],
                    'buyout_percent' => [
                        'default' => SORT_DESC
                    ],
                    'sr_chk_cc' => [
                        'default' => SORT_DESC
                    ],
                ],
            ],
        ]);

        return $this->render('top-countries', [
            'model' => $this->model,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function runData()
    {
        return [
            'data' => $this->getData(['countries' => $this->countryIds, 'days' => self::PERIOD]),
            'prevData' => $this->getData(['countries' => $this->countryIds, 'days' => self::PERIOD + 31])
        ];
    }
}