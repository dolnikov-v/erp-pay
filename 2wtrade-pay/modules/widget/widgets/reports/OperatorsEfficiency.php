<?php

namespace app\modules\widget\widgets\reports;

use app\helpers\i18n\Formatter;
use app\models\CurrencyRate;
use app\models\User;
use app\modules\report\components\ReportFormForeignOperator;
use app\modules\widget\models\WidgetCache;
use app\modules\widget\widgets\CacheableWidget;
use app\modules\widget\models\WidgetUser;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\order\models\Order;
use yii\data\ArrayDataProvider;
use Yii;

/**
 * Class OperatorsEfficiency
 * @package app\modules\widget\widgets\reports
 */

class OperatorsEfficiency extends CacheableWidget
{

    /**
     * @var WidgetUser
     */
    public $model;

    /**
     * @var bool
     */
    public $export = true;

    private function findAll($params = [])
    {
        $mapStatuses = (new ReportFormForeignOperator())->getMapStatuses();
        $buyoutStatuses = $mapStatuses[Yii::t('common', 'Выкуплено')];
        $approveStatuses = $mapStatuses[Yii::t('common', 'Подтверждено')];
        $allStatuses = $mapStatuses[Yii::t('common', 'Всего')];

        $date = date('Y-m-d 00:00:00', strtotime('-' . $params['days'] . ' day'));
        $time = Yii::$app->formatter->asTimestamp($date);
        if (!is_null($this->timeOffset)) {
            $time += $this->timeOffset;
        }

        $totalPrice = '(' . Order::tableName() . '.price_total + ' . Order::tableName() . '.delivery) / ' .CurrencyRate::tableName(). '.rate';

        $condition = [
            'if(' . Order::tableName() . '.status_id in (' . join(',', $buyoutStatuses) . '), 1, 0)',
            'if(' . Order::tableName() . '.status_id in (' . join(',', $approveStatuses) . '), 1, 0)',

            'if(' . Order::tableName() . '.status_id in (' . join(',', $allStatuses) . '), 1, 0)',

            'if(' . Order::tableName() . '.status_id in (' . join(',', $buyoutStatuses) . '), ' . $totalPrice . ', 0)',
            'if(' . Order::tableName() . '.status_id in (' . join(',', $allStatuses) . '), ' . $totalPrice . ', 0)',
        ];

        $dateCondition = 'DATE_FORMAT(FROM_UNIXTIME(' . CallCenterRequest::tableName() . '.created_at), "' . Formatter::getMysqlDateFormat() . '")';

        $all = Order::find()
            ->joinWith(['country', 'country.currencyRate', 'callCenterRequest'], false)
            ->select([
                'operator' => CallCenterRequest::tableName() . '.last_foreign_operator',
                'buyout_count' => 'sum(' . $condition[0] . ')',
                'approve_count' => 'sum(' . $condition[1] . ')',
                'lead_count' => 'sum(' . $condition[2] . ')',
                'profit' => 'sum(' . $condition[3] . ')',
                'bill_avg' => 'avg(' . $condition[4] . ')',
                'workdays' => 'count(distinct ' . $dateCondition . ')',
            ])
            ->where([
                Order::tableName() . '.status_id' => $allStatuses,
                Order::tableName() . '.country_id' => $params['country'],
            ])
            ->andWhere(['>=', CallCenterRequest::tableName() . '.created_at', $time])
            ->andWhere(['>=', CallCenterRequest::tableName() . '.last_foreign_operator', 1])
            ->andWhere(['NOT IN', Order::tableName() .'.source_id', User::getRejectedSources($this->model->user_id)])
            ->groupBy(CallCenterRequest::tableName() . '.last_foreign_operator')
            ->orderBy(['profit' => SORT_DESC])
            ->asArray()
            ->indexBy('operator')
            ->all();

        return $all;
    }

    /**
     * @return string
     */

    public function run()
    {
        //чтение данных из кеша
        $data = $this->data ?: WidgetCache::readCachedData($this->model->id, $this->model->type->by_country ? yii::$app->user->country->id : null);
        if (!$data && $this->onlineLoad) {
            $data = $this->findAll([
                'country' => Yii::$app->user->country->id,
                'days' => self::PERIOD
            ]);
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'pagination' => false,
        ]);

        return $this->render('operators-efficiency', [
            'model' => $this->model,
            'dataProvider' => $dataProvider,
            'approvePlan' => Yii::$app->user->country->cc_operator_daily_approve_plan,
        ]);
    }

    public function runData() {
        return $this->findAll([
            'country' => $this->countryId,
            'days' => self::PERIOD
        ]);
    }
}