<?php
/**
 * Created by PhpStorm.
 * User: 65465132
 * Date: 23.01.2017
 * Time: 18:34
 */

namespace app\modules\widget\widgets\reports;

use app\models\Country;
use app\models\User;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\widget\models\WidgetCache;
use app\modules\widget\widgets\CacheableWidget;
use yii\data\ArrayDataProvider;
use app\modules\widget\models\WidgetUser;
use Yii;

/**
 * Class ConnectedDelivery
 * @package app\modules\widget\widgets\reports
 */
class ConnectedDelivery extends CacheableWidget
{
    const PERIOD = 7;
    /**
     * @var WidgetUser
     */
    public $model;

    /**
     * @var bool
     */
    public $export = true;

    private function findAll($params)
    {
        $date = date('Y-m-d 00:00:00', strtotime('-' . $params['days'] . ' day'));
        $time = Yii::$app->formatter->asTimestamp($date);

        if (!is_null($this->timeOffset)) {
            $time += $this->timeOffset;
        }

        $query = DeliveryRequest::find()
            ->joinWith(['order.country'], false)
            ->select([
                'country_name' => Country::tableName() . '.name',
                'count' => 'count(DISTINCT' . DeliveryRequest::tableName() . '.delivery_id' . ')'
            ])
            ->andWhere([Country::tableName() . '.id' => $params['countries']])
            ->andWhere(['>=', DeliveryRequest::tableName() . '.sent_at', $time])
            ->groupBy([Country::tableName() . '.id'])
            ->orderBy(['count' => SORT_DESC])
            ->asArray();

        return $query->all();
    }

    /**
     * @return string
     */
    public function run()
    {
        //чтение данных из кеша
        $data = $this->data ?: WidgetCache::readCachedData($this->model->id, $this->model->type->by_country ? yii::$app->user->country->id : null);
        if (!$data && $this->onlineLoad) {
            $params = [
                'countries' => array_keys(User::getAllowCountries()),
                'days' => self::PERIOD,
            ];
            $data = $this->findAll($params);
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'pagination' => false,
        ]);

        return $this->render('connected_delivery', [
            'model' => $this->model,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function runData()
    {
        return $this->findAll(['countries' => $this->countryIds, 'days' => self::PERIOD]);
    }
}