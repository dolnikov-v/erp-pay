<?php

namespace app\modules\widget\widgets\reports;

use app\models\Product;
use app\models\User;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\order\models\Order;
use app\modules\order\models\OrderProduct;
use app\modules\report\components\ReportFormDelivery;
use app\modules\widget\models\WidgetCache;
use app\helpers\i18n\Formatter;
use app\modules\widget\widgets\CacheableWidget;
use Yii;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;

/**
 * Class BuyoutGoods
 * @package app\modules\widget\widgets\reports
 */
class BuyoutGoods extends CacheableWidget
{

    /**
     * @var bool
     */
    public $export = true;

    private function getData($params) {

        $mapStatuses = (new ReportFormDelivery())->getMapStatuses();

        $allStatuses = [];
        foreach ($mapStatuses as $name => $statuses) {
            $allStatuses = array_merge($allStatuses, $statuses);
        }

        $allStatuses = array_unique($allStatuses);
        $statuses = $mapStatuses[Yii::t('common', 'Выкуплено')];

        $date = date('Y-m-d 00:00:00', strtotime('-' . $params['days'] . ' day'));
        $time = Yii::$app->formatter->asTimestamp($date);

        if (!is_null($this->timeOffset)) {
            $time += $this->timeOffset;
        }

        $dateCondition = 'DATE_FORMAT(FROM_UNIXTIME(' . CallCenterRequest::tableName() . '.sent_at), "' . Formatter::getMysqlDateFormat() . '")';

        $subQuery = Order::find()
            ->select([
                'order_id' => Order::tableName() . '.id',
                'product_id' => OrderProduct::tableName() . '.product_id'
            ])
            ->joinWith(['orderProducts'], false)
            ->andWhere([Order::tableName() . '.status_id' => $allStatuses])
            ->andWhere([Order::tableName() . '.country_id' => $params['country']])
            ->andWhere(['NOT IN', Order::tableName() .'.source_id', User::getRejectedSources($this->model->user_id)])
            ->groupBy(['order_id', 'product_id']);

        $list = Order::find()
            ->select([
                'date' => $dateCondition,
                'product_id' => 't.product_id',
                'payback' => 'count(' . Order::tableName() . '.status_id in (' . join(',', $statuses) . ') or null )',
                'all' => 'count(' . Order::tableName() . '.status_id in (' . join(',', $allStatuses) . ') or null )',
            ])
            ->leftJoin(['t' => $subQuery], 't.order_id = ' . Order::tableName() . '.id')
            ->joinWith(['callCenterRequest'], false)
            ->andWhere([Order::tableName() . '.status_id' => $allStatuses])
            ->andWhere([Order::tableName() . '.country_id' => $params['country']])
            ->andWhere(['>=', CallCenterRequest::tableName() .'.sent_at', $time])
            ->andWhere(['NOT IN', Order::tableName() .'.source_id', User::getRejectedSources($this->model->user_id)])
            ->groupBy(['t.product_id', 'date'])
            ->orderBy(['date' => SORT_DESC])
            ->asArray()
            ->all();

        return $list;
    }

    public function run()
    {
        //чтение данных из кеша
        $data = $this->data ?: WidgetCache::readCachedData($this->model->id, $this->model->type->by_country ? yii::$app->user->country->id : null);
        if (!$data && $this->onlineLoad) {
            $params = [
                'days' => self::PERIOD,
                'country' => Yii::$app->user->country->id
            ];
            $data = $this->getData($params);
        }

        $dates = ArrayHelper::index($data, null, 'date');
        $tmp = [];

        foreach ($dates as $date => $items) {

            $payback = 0;
            $all = 0;

            foreach ($items as $item) {

                $payback += $item['payback'];
                $all += $item['all'];
            }

            if ($all) {

                $tmp[] = [
                    'date' => $date,
                    'percent' => $payback / $all * 100,
                ];
            }
        }
        $dates = $tmp;


        $products = ArrayHelper::index($data, null, 'product_id');
        foreach ($products as &$items) {

            foreach ($items as $key => &$item) {

                if ($item['all']) {

                    $item['percent'] = $item['payback'] / $item['all'] * 100;

                } else {
                    unset($items[$key]);
                }

            } unset($item);

        } unset($items);

        $dataProviders = [];
        foreach ($products as $id => $item) {

            $dataProviders[$id] = new ArrayDataProvider([
                'allModels' => $item,
                'sort' => [
                    'attributes' => ['date', 'percent'],
                ]
            ]);
        }

        $allDataProviders = new ArrayDataProvider([
            'allModels' => $dates,
            'pagination' => false,
        ]);

        $productList = Product::find()
            ->where(['id' => array_keys($products)])
            ->collection();

        return $this->render('buyout-goods', [
            'model' => $this->model,
            'products' => $productList,
            'dataProviders' => $dataProviders,
            'allDataProviders' => $allDataProviders,
        ]);
    }


    public function runData()
    {
        $countryId = $this->countryId;
        $params = [
            'days' => self::PERIOD,
            'country' => $countryId
        ];
        return $this->getData($params);
    }
}
