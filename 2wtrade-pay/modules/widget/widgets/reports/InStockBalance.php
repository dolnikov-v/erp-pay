<?php

namespace app\modules\widget\widgets\reports;

use app\models\User;
use app\models\Product;
use app\modules\order\models\Order;
use app\modules\order\models\OrderProduct;
use app\modules\report\components\ReportFormDelivery;
use app\modules\storage\models\Storage;
use app\modules\storage\models\StorageProduct;
use app\modules\widget\models\WidgetCache;
use app\modules\widget\models\WidgetUser;
use app\modules\widget\widgets\CacheableWidget;
use Yii;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;

class InStockBalance extends CacheableWidget
{
    const PERIOD = 7;

    /**
     * @var WidgetUser
     */
    public $model;

    /**
     * @var bool
     */
    public $export = true;

    private function getData($params)
    {
        $storageData = StorageProduct::find()
            ->joinWith(['storage', 'product'], false)
            ->select([
                'product_name' => Product::tableName() . '.name',
                'product_id' => StorageProduct::tableName() . '.product_id',
                'quantity' => 'sum(' . StorageProduct::tableName() . '.balance' . ')',
            ])
            ->where([
                Storage::tableName() . '.country_id' => $params['country'],
            ])
            ->groupBy(Product::tableName() . '.id')
            ->indexBy('product_id')
            ->asArray()
            ->all();


        $products = ArrayHelper::getColumn($storageData, 'product_id');

        $mapStatuses = (new ReportFormDelivery())->getMapStatuses();
        $processStatuses = $mapStatuses[Yii::t('common', 'В процессе')];
        $doneStatuses = $mapStatuses[Yii::t('common', 'Выкуплено')];
        $allStatuses = array_merge($processStatuses, $doneStatuses);

        $date = date('Y-m-d 00:00:00', strtotime('-' . $params['days'] . ' day'));
        $time = Yii::$app->formatter->asTimestamp($date);
        if (!is_null($this->timeOffset)) {
            $time += $this->timeOffset;
        }

        $condition = [
            'if(' . Order::tableName() . '.status_id in (' . join(',', $processStatuses) . '), ' . OrderProduct::tableName() . '.quantity, 0)',
            'if(' . Order::tableName() . '.status_id in (' . join(',', $doneStatuses) . '), ' . OrderProduct::tableName() . '.quantity, 0)',
        ];

        $deliveryData = Order::find()
            ->joinWith(['orderProducts'], false)
            ->select([
                'product_id' => OrderProduct::tableName() . '.product_id',
                'process_quantity' => 'sum(' . $condition[0] . ')',
                'done_quantity' => 'sum(' . $condition[1] . ')',
            ])
            ->where([
                Order::tableName() . '.country_id' => $params['country'],
                Order::tableName() . '.status_id' => $allStatuses,
                OrderProduct::tableName() . '.product_id' => $products,
            ])
            ->andWhere(['>=', Order::tableName() . '.created_at', $time])
            ->andWhere(['NOT IN', Order::tableName() .'.source_id', User::getRejectedSources($this->model->user_id)])
            ->groupBy(OrderProduct::tableName() . '.product_id')
            ->indexBy('product_id')
            ->asArray()
            ->all();


        $data = [];
        foreach ($storageData as $productId => $storage) {

            $days = -1;
            $quantity = $storage['quantity'];

            if ($delivery = ArrayHelper::getValue($deliveryData, $productId)) {

                if ($delivery['process_quantity'] + $delivery['done_quantity'] > 0) {

                    $quantity -= $delivery['process_quantity'];
                    $days = $quantity / ($delivery['process_quantity'] + $delivery['done_quantity']);
                }
            }

            $data[] = [
                'product_name' => $storage['product_name'],
                'quantity' => $quantity,
                'days' => $days,
            ];
        }

        ArrayHelper::multisort($data, ['quantity'], [SORT_DESC]);

        return $data;
    }

    /**
     * @return string
     */

    public function run()
    {
        //чтение данных из кеша
        $data = $this->data ?: WidgetCache::readCachedData($this->model->id, $this->model->type->by_country ? yii::$app->user->country->id : null);
        if (!$data && $this->onlineLoad) {
            $data = $this->getData([
                'country' => Yii::$app->user->country->id,
                'days' => self::PERIOD,
            ]);
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'pagination' => false,
        ]);

        return $this->render('in-stock-balance', [
            'model' => $this->model,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function runData() {
        return $this->getData([
            'country' => $this->countryId,
            'days' => self::PERIOD
        ]);
    }
}