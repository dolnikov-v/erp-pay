<?php

namespace app\modules\widget\widgets\reports;

use app\models\User;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\order\models\OrderStatus;
use app\modules\widget\models\WidgetCache;
use app\modules\widget\widgets\CacheableWidget;
use yii\data\ArrayDataProvider;
use app\models\Country;
use app\modules\order\models\Order;
use app\modules\widget\models\WidgetUser;
use Yii;
use yii\helpers\ArrayHelper;

class Trash extends CacheableWidget
{
    /**
     * @var WidgetUser
     */
    public $model;

    /**
     * @var bool
     */
    public $export = true;

    private function getData($params)
    {
        $date = date('Y-m-d 00:00:00', strtotime('-' . $params['days'] . ' day'));
        $time = Yii::$app->formatter->asTimestamp($date);
        if (!is_null($this->timeOffset)) {
            $time += $this->timeOffset;
        }

        $data = Order::find()
            ->joinWith(['country', 'callCenterRequest'], false)
            ->select([
                'country_id' => Country::tableName() . '.id',
                'country_name' => Country::tableName() . '.name',
                'trash_count' => 'count(' . Order::tableName() . '.status_id in (' . OrderStatus::STATUS_CC_TRASH . ', ' . OrderStatus::STATUS_AUTOTRASH . ', ' . OrderStatus::STATUS_AUTOTRASH_DOUBLE . ') or null)',
                'lead_count' => 'count(*)',
            ])
            ->andWhere([Order::tableName() . '.country_id' => $params['countries']])
            ->andWhere(['>=', CallCenterRequest::tableName() . '.sent_at', $time])
            ->andWhere(['NOT IN', Order::tableName() .'.source_id', User::getRejectedSources($this->model->user_id)])
            ->groupBy(Order::tableName() . '.country_id')
            ->asArray()
            ->indexBy('country_id')
            ->all();

        foreach ($data as &$item) {
            $item['trash_percent'] = (!$item['lead_count']) ? 0 : $item['trash_count'] / $item['lead_count'];
        }
        ArrayHelper::multisort($data, 'trash_percent', SORT_DESC);

        return $data;
    }

    /**
     * @return string
     */

    public function run()
    {
        $data = $this->data ?: WidgetCache::readCachedData($this->model->id, $this->model->type->by_country ? yii::$app->user->country->id : null);
        if (!$data && $this->onlineLoad) {
            $countries = array_keys(User::getAllowCountries());
            $data = $this->getData([
                'countries' => $countries,
                'days' => self::PERIOD,
            ]);
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'pagination' => false,
        ]);

        return $this->render('trash', [
            'model' => $this->model,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function runData()
    {
        return $this->getData([
            'countries' => $this->countryIds,
            'days' => self::PERIOD
        ]);
    }
}