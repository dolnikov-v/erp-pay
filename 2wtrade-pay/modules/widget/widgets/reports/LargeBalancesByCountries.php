<?php

namespace app\modules\widget\widgets\reports;

use app\models\User;
use app\models\Product;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\models\Order;
use app\modules\order\models\OrderProduct;
use app\modules\order\models\OrderStatus;
use app\modules\storage\models\Storage;
use app\modules\storage\models\StorageProduct;
use app\modules\widget\models\WidgetCache;
use app\modules\widget\models\WidgetUser;
use app\modules\widget\widgets\CacheableWidget;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use app\models\Country;
use yii;

/**
 * Class LargeBalancesByCountries
 * @package app\modules\widget\widgets\reports
 */
class LargeBalancesByCountries extends CacheableWidget
{
    /**
     * @var WidgetUser
     */
    public $model;

    /**
     * @var bool
     */
    public $export = true;

    private $balances = [];

    private $products = [];


    /**
     * @return array
     */
    public function getMapStatuses()
    {
        return [
            'buyout' => OrderStatus::getBuyoutList(),
        ];
    }


    /**
     * @param $params
     * @return array;
     */
    private function getData($params)
    {
        $statuses = $this->getMapStatuses();

        $startDay = strtotime('now 00:00:00') - (60*60*24*7);

        $this->balances = StorageProduct::find()
            ->select([
                'countryName' => Country::tableName() .'.name',
                'productId' => StorageProduct::tableName() . '.product_id',
                'productName' => Product::tableName() .'.name',
                'quantity' => 'sum(' . StorageProduct::tableName() . '.balance)',
            ])
            ->joinWith(['storage'], false)
            ->innerJoin(Country::tableName(), Country::tableName() . '.id = ' . Storage::tableName() . '.country_id')
            ->innerJoin(Product::tableName(), Product::tableName() . '.id = ' . StorageProduct::tableName() . '.product_id')
            ->where(['IN', Storage::tableName() .'.country_id', $params['countries']])
            ->groupBy('countryName, productId')
            ->asArray()
            ->all();

        $this->products = Order::find()
            ->select([
                'countryName' => Country::tableName() .'.name',
                'productId' => OrderProduct::tableName() . '.product_id',
                'productName' => Product::tableName() .'.name',
                'quantity' => 'sum(' . OrderProduct::tableName() . '.quantity)',
                'leads' => 'count(1)',
            ])
            ->joinWith(['orderProducts'], false)
            ->innerJoin(Country::tableName(), Country::tableName() . '.id = ' . Order::tableName() . '.country_id')
            ->innerJoin(DeliveryRequest::tableName(), Order::tableName() . '.id = ' . DeliveryRequest::tableName() . '.order_id')
            ->innerJoin(Product::tableName(), Product::tableName() . '.id = ' . OrderProduct::tableName() . '.product_id')
            ->where(['IN', Order::tableName() .'.country_id', $params['countries']])
            ->andWhere([Order::tableName() . '.status_id' => $statuses['buyout']])
            ->andWhere(['>=', DeliveryRequest::tableName() .'.approved_at', $startDay])
            ->andWhere(['NOT IN', Order::tableName() .'.source_id', User::getRejectedSources($this->model->user_id)])
            ->groupBy('countryName, productId')
            ->asArray()
            ->all();

        $allProducts = Product::find()
            ->asArray()
            ->all();

        $allCountries = Country::find()
            ->select(['id', 'name'])
            ->where(['IN', Country::tableName() .'.id', $params['countries']])
            ->asArray()
            ->all();

        $data = [];
        foreach ($allCountries as $everyCountry) {
            foreach ($allProducts as $everyProduct) {
                $result = $this->getLeftTime($everyCountry, $everyProduct);
                if ($result['lefttime'] != '-') {
                    $data[] = Array('country' => $everyCountry['name'], 'product' => $everyProduct['name'], 'leads' => $result['leads'], 'lefttime' => $result['lefttime']);
                }
            }
        }

        ArrayHelper::multisort($data, ['lefttime', 'country'], [SORT_DESC, SORT_ASC]);

        return $data;
    }

    public function run()
    {
        //чтение данных из кеша
        $data = $this->data ?: WidgetCache::readCachedData($this->model->id, $this->model->type->by_country ? yii::$app->user->country->id : null);
        if (!$data && $this->onlineLoad) {
            $params = [
                'countries' => array_keys(User::getAllowCountries()),
                'days' => self::PERIOD,
            ];
            $data = $this->getData($params);
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'pagination' => false,
        ]);

        return $this->render('large-balances-by-countries', [
            'model' => $this->model,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function runData()
    {
        $params = [
            'countries' => $this->countryIds,
            'days' => self::PERIOD,
        ];
        return $this->getData($params);
    }


    /**
     * @param $country
     * @param $product
     * @return array $value
     */
    private function getLeftTime($country, $product) {

        $value = [];
        $valueBalance = 0;
        $valueSells = 0;
        $leadsCount = 0;
        $stop = false;
        foreach ($this->balances as $balance) {
            if ($balance['countryName'] == $country['name'] && $balance['productId'] == $product['id']) {
                foreach ($this->products as $prod) {
                    if ($prod['countryName'] == $country['name'] && $prod['productId'] == $product['id']) {
                        if (is_numeric($prod['quantity']) && $prod['quantity'] > 0) {
                            $valueBalance = $balance['quantity'];
                            $valueSells = $prod['quantity'] / 7;
                            $leadsCount = round($prod['leads'] / 7 , 2);
                            $stop = true;
                            break;
                        }
                    }
                }
            }
            if ($stop) {
                break;
            }
        }

        if ($valueBalance <= 0) {
            $value['lefttime'] = '-';
        }
        else {
            if ($valueSells <= 0) {
                $value['lefttime'] = '∞';
            }
            else {
                $value['lefttime'] = round((($valueBalance / ($valueSells / 7)) / 31), 2);
            }
        }

        $value['leads'] = $leadsCount;

        return $value;
    }
}
