<?php
namespace app\modules\widget\widgets\oldorderscountrycurators\models\Query;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\Country;
use app\modules\order\models\Order;
use app\models\User;
use app\models\UserCountry;
use app\models\AuthAssignment;
use app\modules\order\models\OrderStatus;
use yii\db\Expression;
use Yii;


/**
 * Class class OldOrdersCountryCuratorsQuery extends ActiveRecord
 * @package app\modules\widget\widgets\oldorderscountrycurators
 */
class OldOrdersCountryCuratorsQuery extends ActiveRecordLogUpdateTime
{
    /**
     * Получить Username терменов
     * @param $ids []
     * @return array
     */
    public static function getUsernames($ids)
    {
        $query = User::find()
            ->where(['in', User::tableName() . '.id', $ids]);

        return $query->asArray()->all();
    }

    /**
     * Получить даты самых древних незакрытых заказов по странам
     * @param $params []
     * @return array
     */
    public static function getMinDateOldOrders($params)
    {
        $countries_ids = $params['countries_ids'];
        $user_id =  $params['user_id'];

        $query = Order::find()
            ->select([
                'order_id' => Order::tableName() . '.id',
                'country_id' => Country::tableName() . '.id',
                'force_country_slug' => Country::tableName() . '.slug',
                'country_name' => Country::tableName() . '.name',
                'days' => new Expression('round((UNIX_TIMESTAMP(CURDATE()) -  min(' . Order::tableName() . '.`created_at`)) / (3600 * 24))')
            ])
            ->innerJoin(Country::tableName(), [Country::tableName() . '.id' => new Expression(Order::tableName() . '.country_id')])
            ->andWhere(['NOT IN', Order::tableName() .'.source_id', User::getRejectedSources($user_id)])
            ->groupBy([Country::tableName() . '.id']);

        $query->andFilterWhere(['in', Order::tableName() . '.country_id', $countries_ids]);
        $query->andFilterWhere(['in', Order::tableName() . '.status_id', OrderStatus::getProcessList()]);

        return $query->asArray()->all();
    }

    /**
     * Получить список id тер.менеджеров по странам
     * @param $params []
     * @return array
     */
    public static function getCuratorsByCountries($params)
    {
        $countries_ids = $params['countries_ids'];

        $query = Country::find()
            ->select([
                'country_id' => Country::tableName() . '.id',
                'country_name' => Country::tableName() . '.name',
                'users_count' => new Expression('count(DISTINCT ' . User::tableName() . '.id)'),
                'users_ids' => new Expression('GROUP_CONCAT(DISTINCT ' . User::tableName() . '.id ORDER BY ' . User::tableName() . '.id ASC SEPARATOR \',\') ')
            ])
            ->leftJoin(UserCountry::tableName(), [UserCountry::tableName() . '.country_id' => new Expression(Country::tableName() . '.id')])
            ->leftJoin(User::tableName(), [User::tableName() . '.id' => new Expression(UserCountry::tableName() . '.user_id')])
            ->leftJoin(AuthAssignment::tableName(), [AuthAssignment::tableName() . '.user_id' => new Expression(User::tableName() . '.id')])
            ->where([AuthAssignment::tableName() . '.item_name' => 'country.curator'])
            ->andWhere([User::tableName() . '.status' => User::STATUS_DEFAULT])
            ->groupBy([Country::tableName() . '.id']);

        $query->andFilterWhere(['in', Country::tableName() . '.id', $countries_ids]);

        return $query->asArray()->all();
    }

}
