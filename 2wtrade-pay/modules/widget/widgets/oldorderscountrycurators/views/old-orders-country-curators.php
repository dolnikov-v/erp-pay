<?php

use app\modules\widget\extensions\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var \yii\data\ArrayDataProvider $dataProvider
 */

?>

<?= GridView::widget([
    'id' => 'widget-user-' . $model->id . '-grid',
    'exportFilename' => Yii::t('common', $model->type->name),
    'dataProvider' => $dataProvider,
    'tableOptions' => ['class' => 'table table-bordered'],
    'rowOptions' => function ($model, $key, $index, $column) {
        $class = 'tr-vertical-align-middle text-center  ';
        $bgcolor = 'white';

        if ($model['days'] > 0 && $model['days'] <= 45) {
            $bgcolor = '#DDFADE';
        } elseif ($model['days'] > 45) {
            $bgcolor = '#FFE7E7';
        }

        return ['style' => 'background-color:' . $bgcolor];
    },
    'showHeader' => true,
    'columns' => [
        [
            'header' => Yii::t('common', 'Территориальный менеджер'),
            'headerOptions' => ['style' => 'text-align:center;', 'class' => 'width-150'],
            'attribute' => 'username',
            'value' => function ($model) {
                return $model['username'];
            },
        ],
        [
            'header' => Yii::t('common', 'Страна'),
            'headerOptions' => ['style' => 'text-align:center;', 'class' => 'width-150'],
            'contentOptions' => ['style' => 'text-align:center;'],
            'attribute' => 'country_name',
            'value' => function ($model) {
                return Yii::t('common', $model['country_name']);
            }
        ],
        [
            'header' => Yii::t('common', 'Срок старых заказов (дней)'),
            'headerOptions' => ['style' => 'text-align:center;', 'class' => 'width-150'],
            'contentOptions' => ['style' => 'text-align:center;'],
            'attribute' => 'days',
            'format' => 'raw',
            'value' => function ($model){
                $url = Url::to([
                    'force_country_slug' => $model['force_country_slug'],
                    '/order/index/index',
                    'DateFilter[dateType]' => '',
                    'NumberFilter[not][]' => '',
                    'NumberFilter[entity][]' => 'id',
                    'NumberFilter[number][]' => $model['order_id']
                ]);

                $content = Html::a(round($model['days']), $url, ['target' => '_blank']);

                return $content;
            }
        ],
    ]
]); ?>
