<?php
namespace app\modules\widget\widgets\oldorderscountrycurators;

use app\modules\widget\models\WidgetCache;
use app\modules\widget\widgets\oldorderscountrycurators\models\Query\OldOrdersCountryCuratorsQuery;
use app\modules\widget\models\WidgetUser;
use app\models\User;
use app\modules\widget\widgets\CacheableWidget;
use yii\data\ArrayDataProvider;
use yii;
use yii\helpers\ArrayHelper;


/**
 * Class OldOrdersCountryCurators
 * @package app\modules\widget\widgets\oldorderscountrycurators
 */
class OldOrdersCountryCurators extends CacheableWidget
{
    /**
     * @var WidgetUser
     */
    public $model;

    /**
     * @var bool
     */
    public $export = true;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }

    /**
     * $@var []
     */
    public $users;

    /**
     * Получить имя пользователя по id
     * @param integer
     * @return string
     */
    public function getUserNameById($id)
    {
        foreach ($this->users as $k => $v) {
            if ($v['id'] == $id) {
                return $v['username'];
            }
        }
    }

    /**
     * Подготовка массива страна -> старый заказ
     * @param []
     * @return []
     */
    public function prepareCountriesData($minDateOldOrders)
    {
        $preparedData = [];

        foreach ($minDateOldOrders as $k => $v) {
            $preparedData[$v['country_id']] = $v;
        }

        return $preparedData;
    }

    /**
     *  Подготовка $arrayDataProvider
     * @param []
     * @params []
     * @return []
     */
    public function combineData($preparedCountriesData, $curatorsByCountries)
    {
        $userList = [];
        $userIds = [];
        foreach ($curatorsByCountries as $k => $country_data) {
            $ids = explode(',', $country_data['users_ids']);
            $userIds = array_merge($userIds, $ids);

            foreach ($ids as $l => $id) {
                if (isset($preparedCountriesData[$country_data['country_id']]['days'])) {
                    $userList[$id]['days'][] = $preparedCountriesData[$country_data['country_id']]['days'];
                    $userList[$id]['country_ids'][] = $country_data['country_id'];
                    $userList[$id]['order_ids'][] = $preparedCountriesData[$country_data['country_id']]['order_id'];
                    $userList[$id]['slugs'][] = $preparedCountriesData[$country_data['country_id']]['force_country_slug'];
                    $userList[$id]['country_names'][] = $preparedCountriesData[$country_data['country_id']]['country_name'];
                }
            }
        }

        if (!is_array($this->users)) {
            $this->users = OldOrdersCountryCuratorsQuery::getUsernames(array_unique($userIds));
        }

        //оставить только максимальные сроки (из всех стран выбрать самый дальний заказ)
        $combinedData = [];

        foreach ($userList as $k => $v) {
            $this->users[] = $k;

            $key_from_max_value = array_keys($userList[$k]['days'], max($userList[$k]['days']));

            $combinedData[] = [
                'user_id' => $k,
                'username' => $this->getUserNameById($k),
                'days' => max($userList[$k]['days']),
                'country_id' => $userList[$k]['country_ids'][$key_from_max_value[0]],
                'order_id' => $userList[$k]['order_ids'][$key_from_max_value[0]],
                'force_country_slug' => $userList[$k]['slugs'][$key_from_max_value[0]],
                'country_name' => $userList[$k]['country_names'][$key_from_max_value[0]]
            ];
        }

        ArrayHelper::multisort($combinedData, ['days'], SORT_DESC);

        return $combinedData;
    }

    /**
     * @return string
     */
    public function run()
    {
        $data = $this->data ?: WidgetCache::readCachedData($this->model->id, $this->model->type->by_country ? yii::$app->user->country->id : null);
        if (!$data && $this->onlineLoad) {
            $countries_ids = array_keys(User::getAllowCountries());
            $params = ['countries_ids' => $countries_ids, 'user_id' => $this->model->user_id];

            // список дат  самых древних не закрытых заказов по странам
            $minDateOldOrders = OldOrdersCountryCuratorsQuery::getMinDateOldOrders($params);

            // список стран с кураторами
            $curatorsByCountries = OldOrdersCountryCuratorsQuery::getCuratorsByCountries($params);

            // Подготовленный массив страна -> срок старого заказа
            $preparedCountriesData = $this->prepareCountriesData($minDateOldOrders);

            $data = $this->combineData($preparedCountriesData, $curatorsByCountries);
        }

        $ArrayDataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'pagination' => false,
        ]);

        return $this->render('old-orders-country-curators', [
            'dataProvider' => $ArrayDataProvider,
            'model' => $this->model
        ]);
    }

    public function runData()
    {
        $params = ['countries_ids' => $this->countryIds, 'user_id' => $this->model->user_id];

        // список дат  самых древних не закрытых заказов по странам
        $minDateOldOrders = OldOrdersCountryCuratorsQuery::getMinDateOldOrders($params);

        // список стран с кураторами
        $curatorsByCountries = OldOrdersCountryCuratorsQuery::getCuratorsByCountries($params);

        // Подготовленный массив страна -> срок старого заказа
        $preparedCountriesData = $this->prepareCountriesData($minDateOldOrders);

        return $this->combineData($preparedCountriesData, $curatorsByCountries);
    }
}