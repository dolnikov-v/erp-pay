<?php

namespace app\modules\widget\widgets;

use yii\base\Widget;

/**
 * Class WidgetRole
 * @package app\modules\widget\widgets
 */
class WidgetRole extends Widget
{
    /**
     * @var
     */
    public $model;

    /**
     * @var
     */
    public $widgets;

    /**
     * @var
     */
    public $widgetsByRole;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('widget-role', [
            'model' => $this->model,
            'widgets' => $this->widgets,
            'widgetsByRole' => $this->widgetsByRole
        ]);
    }
}
