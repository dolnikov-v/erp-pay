<?php
namespace app\modules\widget\widgets\buyoutdelivery\models\Query;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\Country;
use app\models\User;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\order\models\Order;
use yii\db\Expression;
use app\modules\order\models\OrderStatus;
use Yii;


/**
 * Class class BuyOutDeliveryQuery extends ActiveRecord
 * @package app\modules\widget\widgets\buyoutdelivery
 */
class BuyOutDeliveryQuery extends ActiveRecordLogUpdateTime
{
    /**
     * @var integer
     * Устанавливается из класса виджета app\modules\widget\widgets\buyoutdelivery\BuyOutDelivery;
     * при работе с методами данной модели
     */
    public static $period;

    /**
     * @param []
     * @return []
     * Получить  кол-во выкупов по курьеркам за определённый период, по определённой стране
     */
    public static function getAVGOrdersByDeliveryFromPeriod($params)
    {
        $country_id = $params['country_id'];
        $user_id = $params['user_id'];
        $query = Order::find()
            ->select([
                'country_id' => Delivery::tableName() . '.country_id',
                'delivery_id' => Delivery::tableName() . '.id',
                'delivery_name' => Delivery::tableName() . '.name',
                'all_orders' => new Expression('COUNT(' . Order::tableName() . '.status_id IN(' . join(', ', OrderStatus::getInDeliveryAndFinanceList()) . ') OR NULL)'),
                'approved_orders' => new Expression('COUNT(' . Order::tableName() . '.status_id IN(' . join(', ', OrderStatus::getBuyoutList()) . ') OR NULL)'),
                'avgOrders' => new Expression('ROUND(COUNT(' . Order::tableName() . '.status_id IN(' . join(', ', OrderStatus::getInDeliveryAndFinanceList()) . ') OR NULL)/' . self::$period . ')')
            ])
            ->leftJoin(CallCenterRequest::tableName(), [CallCenterRequest::tableName() . '.order_id' => new Expression(Order::tableName() . '.id')])
            ->leftJoin(DeliveryRequest::tableName(), [DeliveryRequest::tableName() . '.order_id' => new Expression(Order::tableName() . '.id')])
            ->leftJoin(Delivery::tableName(), ['`delivery`.`id`' => new Expression(DeliveryRequest::tableName() . '.delivery_id')])
            ->andWhere(['NOT IN', Order::tableName() .'.source_id', User::getRejectedSources($user_id)])
            ->groupBy(['`delivery`.`id`']);

        $query->andFilterWhere([Delivery::tableName() . '.country_id' => $country_id]);
        $query->andFilterWhere(['>=', DeliveryRequest::tableName() . '.sent_at', new Expression('UNIX_TIMESTAMP(CURDATE() - INTERVAL ' . self::$period . ' DAY)')]);

        return $query->asArray()->all();
    }

    /**
     * @param []
     * @return []
     * Получить кол-во выкупов по курьеркам, по стране, за определённый период
     */
    public static function getCountOrdersByDeliveryOnDate($params)
    {
        $country_id = $params['country_id'];
        $user_id = $params['user_id'];
        $query = Order::find()
            ->select([
                'sent_at' => new Expression("DATE_FORMAT(FROM_UNIXTIME(" . DeliveryRequest::tableName() . ".sent_at), '%Y/%m/%d')"),
                'country_id' => Delivery::tableName() . '.country_id',
                'delivery_id' => Delivery::tableName() . '.id',
                'delivery_name' => Delivery::tableName() . '.name',
                'all_orders' => new Expression('COUNT(' . Order::tableName() . '.status_id IN(' . join(', ', OrderStatus::getApproveList()) . ') OR NULL)'),
                'approved_orders' => new Expression('COUNT(' . Order::tableName() . '.status_id IN(' . join(', ', OrderStatus::getBuyoutList()) . ') OR NULL)'),
                //для стран лат. америки зелёный цвет от 50, для остальных от 65
                'country_char_code' => Country::tableName() . '.char_code',
                'greenPercent' => new Expression('case when '.Country::tableName() . '.char_code in ("CL","PL","AR","MX","CO","UY","PY","CR") then 50 else 65 end')
            ])
            ->leftJoin(CallCenterRequest::tableName(), [CallCenterRequest::tableName() . '.order_id' => new Expression(Order::tableName() . '.id')])
            ->leftJoin(DeliveryRequest::tableName(), [DeliveryRequest::tableName() . '.order_id' => new Expression(Order::tableName() . '.id')])
            ->leftJoin(Delivery::tableName(), ['`delivery`.`id`' => new Expression(DeliveryRequest::tableName() . '.delivery_id')])
            ->leftJoin(Country::tableName(), [Country::tableName() . '.id' => new Expression(Delivery::tableName() . '.country_id')])
            ->andWhere(['NOT IN', Order::tableName() .'.source_id', User::getRejectedSources($user_id)])
            ->groupBy(['sent_at', '`delivery`.`id`'])
            ->orderBy(['sent_at' => SORT_DESC]);

        $query->andFilterWhere([Delivery::tableName() . '.country_id' => $country_id]);
        $query->andFilterWhere(['>=', DeliveryRequest::tableName() . '.sent_at', new Expression('UNIX_TIMESTAMP(CURDATE() - INTERVAL ' . self::$period . ' DAY)')]);

        return $query->asArray()->all();
    }
}
