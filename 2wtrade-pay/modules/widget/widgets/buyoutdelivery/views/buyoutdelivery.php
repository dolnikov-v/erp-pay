<?php

use app\modules\widget\extensions\GridView;
use app\widgets\Label;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var \yii\data\ArrayDataProvider $ArrayDataProvider
 * @var app\modules\widget\widgets\buyoutdelivery\BuyOutDelivery $columnsNames
 * @var app\modules\widget\widgets\buyoutdelivery\BuyOutDelivery $period_dates
 * @var app\modules\widget\widgets\buyoutdelivery\BuyOutDelivery $avgOrders
 * @var app\modules\widget\widgets\buyoutdelivery\BuyOutDelivery $allOrders
 * @var app\modules\widget\widgets\buyoutdelivery\BuyOutDelivery $period
 */


/**
 * Динамическое формирование столбцов курьерок
 */
$columnsDelivery = [
    [
        'header' => Yii::t('common', 'Дата'),
        'headerOptions' => ['style' => 'text-align:center;', 'class' => 'width-120'],
        'attribute' => 'sent_at',
        'contentOptions' => ['style' => 'text-align:center'],
        'format' => 'raw'
    ]
];

if (is_array($columnsNames)) {

    foreach ($columnsNames as $k => $v) {
        $urlDelivery = Url::to([
            '/report/delivery/index',
            's[from]' => Yii::$app->formatter->asDate($period_dates[count($period_dates) - 1], 'yyyy/MM/dd'),
            's[to]' => Yii::$app->formatter->asDate($period_dates[0], 'yyyy/MM/dd'),
            's[type]' => 'd_sent_at',
            's[groupBy]' => 'd_sent_at',
            's[product]' => '',
            's[delivery]' => $k,
            's[partner]' => ''
        ]);

        $columnsDelivery[] = [
            'header' => Html::a(Html::tag('b', $v), $urlDelivery, ['target' => '_blank']),
            'headerOptions' => ['style' => 'text-align:center;'],
            'attribute' => $v,
            'format' => 'raw',
            'contentOptions' => function ($model, $index) use ($ArrayDataProvider, $v) {
                if (!isset($model[$v])) $model[$v] = ['approvedOrders' => 0, 'color' => ''];
                //последняя строка с средними значениями
                if ($index == sizeof($ArrayDataProvider->allModels) - 1) {
                    $color = '#F9F9F9';
                } else {
                    $color = $model[$v]['color'];
                }

                return ['style' => 'text-align:center; background-color:' . $color];
            },
            'value' => function ($model, $index) use ($v, $period_dates, $allOrders, $ArrayDataProvider, $period) {
                if ($index < sizeof($ArrayDataProvider->allModels) - 1) {
                    $percent = (isset($model[$v]['all_orders']) && $model[$v]['all_orders'] > 0) ? $model[$v]['approvedOrders'] * 100 / $model[$v]['all_orders'] : 0;
                    $url = Url::to([
                        '/report/delivery/index',
                        's[from]' => $model[$v]['sent_at'],
                        's[to]' => $model[$v]['sent_at'],
                        's[type]' => 'd_sent_at',
                        's[groupBy]' => 'd_sent_at',
                        's[product]' => '',
                        's[delivery]' => $model[$v]['delivery_id'],
                        's[partner]' => ''
                    ]);

                    $formated_percent = round($percent) . '%';
                    $content = Html::a($formated_percent, $url, ['target' => '_blank']);

                } else {
                    $url = Url::to([
                        '/report/delivery/index',
                        's[from]' => Yii::$app->formatter->asDate($period_dates[count($period_dates) - 1], 'yyyy/MM/dd'),
                        's[to]' => Yii::$app->formatter->asDate($period_dates[0], 'yyyy/MM/dd'),
                        's[type]' => 'd_sent_at',
                        's[groupBy]' => 'd_sent_at',
                        's[product]' => '',
                        's[delivery]' => $model[$v]['delivery_id'],
                        's[partner]' => ''
                    ]);

                    $content = ($allOrders[$v]['avg'] > 0) ? round($allOrders[$v]['avg'] / $period) . '%' : 0 . '%';
                }
                return $content;
            },
        ];
    }
} else {
    $columnsDelivery = [];
}
?>

<?= GridView::widget([
    'id' => 'widget-user-' . $model->id . '-grid',
    'exportFilename' => Yii::t('common', $model->type->name),
    'dataProvider' => $ArrayDataProvider,
    'tableOptions' => ['class' => 'table table-bordered'],
    'rowOptions' => function ($model, $key, $index, $column) {
        $class = 'tr-vertical-align-middle text-center  ';
    },
    'showOnEmpty' => false,
    'showHeader' => true,
    'showFooter' => false,
    'columns' => $columnsDelivery
]); ?>
