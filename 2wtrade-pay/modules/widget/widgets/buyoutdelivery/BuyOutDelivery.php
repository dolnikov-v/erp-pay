<?php
namespace app\modules\widget\widgets\buyoutdelivery;

use app\modules\widget\models\WidgetCache;
use app\modules\widget\widgets\buyoutdelivery\models\Query\BuyOutDeliveryQuery;
use app\modules\widget\models\WidgetUser;
use app\modules\widget\widgets\CacheableWidget;
use yii\data\ArrayDataProvider;
use Yii;
use yii\helpers\Url;
use yii\helpers\Html;


/**
 * Widget Выкупы по крьеркам
 * Class BuyOutDelivery
 * @package app\modules\widget\widgets\buyoutdelivery
 */
class BuyOutDelivery extends CacheableWidget
{
    /**
     * @var WidgetUser
     */
    public $model;

    /**
     * @var bool
     */
    public $export = true;

    /**
     * @var []
     * Массив дат за указанный период
     */
    public $period_dates;

    /**
     * @var []
     * Массив курьерка => кол-во выкупов за период
     */
    public $allOrders;

    /**
     * @var []
     * Массив курьерка => среднее  кол-во выкупов за период
     */
    public $avgOrders;
    /**
     * @var []
     * Массив delivery_id => delivery_name
     */
    public $deliveryNames;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @param [] $avgData - массив среднее кол-во выкупа по курьеркам по стране за период
     * @return array
     * запись в $this->calculateAvgData упрощённый массив среднее кол-во выкупа по курьеркам по стране за период
     * на выходе многомерный массив [delivery_id => [delivery_name, avgOrdersOnPeriod]]
     */
    function calculatedAvgData($avgData)
    {
        $deliverisData = [];
        foreach ($avgData as $k => $v) {

            $deliverisData[$v['delivery_id']] = [
                'delivery_name' => $v['delivery_name'],
                'country_id' => $v['country_id'],
                'avgCountOrders' => $v['avgOrders']
            ];

            $this->deliveryNames[$v['delivery_id']] = $v['delivery_name'];
        }

        return $deliverisData;
    }

    /**
     * @param [] $avgData - массив среднее кол-во выкупа по курьеркам по стране за период
     * @param [] $countData - массив выкупов по дням по курьеркам по стране за период
     * @return []
     *
     */
    public function combineWidgetData($avgData, $countData)
    {
        $calculatedAvgData = $this->calculatedAvgData($avgData);
        $dates = [];
        $deliveryData = [];

        foreach ($countData as $k => $v) {
            $this->period_dates[] = $v['sent_at'];
            $dates[$v['sent_at']][$v['delivery_name']] = $v;

            unset($v['sent_at']);
            $v['approved_orders'] = 0;
            $v['all_orders'] = 0;
            $deliveryData[$v['delivery_name']] = $v;
        }

        //в некоторые дни у некоторых курьерок не было выкупов - нужно их добавить в строку GridView
        $fullData = [];
        foreach ($dates as $date => $delivery_data) {
            $deliveris = array_keys($deliveryData);
            $listOutDeliveris = [];

            if (sizeof($deliveris) > sizeof($delivery_data)) {
                $outDeliveris = array_keys(array_diff_key($deliveryData, $delivery_data));

                foreach ($deliveryData as $k => $v) {
                    if (in_array($k, $outDeliveris)) {
                        $listOutDeliveris[$k] = $deliveryData[$k];
                    }
                }

                if (!empty($listOutDeliveris)) {
                    $fullData[$date] = $listOutDeliveris + $delivery_data;
                }
            } else {
                $fullData[$date] = $delivery_data;

            }
        }

        return $fullData;
    }

    /**
     * Формирование ArrayDataProvider
     * @param [] combineWidgetData()
     * @return []
     */
    public function prepareArrayDataProvider($dates)
    {
        $data = [];
        //средний выкуп по курьеркам за период
        $avgFooter = [];

        //Список курьерок
        $fullListDelivery = [];

        foreach ($dates as $k => $v) {

            $url = Url::to([
                '/report/delivery/index',
                's[from]' => Yii::$app->formatter->asDate($k, 'yyyy/MM/dd'),
                's[to]' => Yii::$app->formatter->asDate($k, 'yyyy/MM/dd'),
                's[type]' => 'd_sent_at',
                's[groupBy]' => 'd_sent_at',
                's[product]' => '',
                's[delivery]' => '',
                's[partner]' => ''
            ]);

            $line['sent_at'] = Html::a($k, $url, ['target' => '_blank']);

            foreach ($v as $delivery_name => $delivery_data) {

                if (!isset($fullListDelivery[$delivery_name])) {
                    $fullListDelivery[$delivery_name] = $delivery_data;
                }

                $line[$delivery_name]['approvedOrders'] = $delivery_data['approved_orders'];
                $line[$delivery_name]['delivery_id'] = $delivery_data['delivery_id'];
                $line[$delivery_name]['greenPercent'] = $delivery_data['greenPercent'];
                $line[$delivery_name]['all_orders'] = isset($delivery_data['all_orders']) ? $delivery_data['all_orders'] : 0;
                $line[$delivery_name]['sent_at'] = $k;

                if (!isset($this->allOrders[$delivery_name])) {
                    $this->allOrders[$delivery_name]['avg'] = 0;
                } else {
                    $delivery_data['all_orders'] = isset($delivery_data['all_orders']) ? $delivery_data['all_orders'] : 0;
                    $avg_day = ($delivery_data['all_orders'] > 0) ? $delivery_data['approved_orders'] * 100 / $delivery_data['all_orders'] : 0;
                    $this->allOrders[$delivery_name]['avg'] += $avg_day;
                }

                $this->allOrders[$delivery_name]['delivery_id'] = $delivery_data['delivery_id'];

            }

            $data[] = $line;
        }

        //footer gridview
        //if not empty result
        if (is_array($this->allOrders)) {
            foreach ($this->allOrders as $k => $v) {
                $avgFooter[$k]['approvedOrders'] = ($v['avg'] / self::PERIOD);
                $avgFooter[$k]['delivery_id'] = $v['delivery_id'];
                $this->avgOrders[$k] = ($v['avg'] / self::PERIOD);
            }

            $avgFooter = ['sent_at' => yii::t('common', 'Среднее количество выкупов')] + $avgFooter;
            $data[] = $avgFooter;
        }
        //Закрасить ячейки виджета
        $coloredData = $this->setColorCell($data);
        return $coloredData;
    }

    /**
     * Определение цвета ячейки виджета
     * @var [] подготовленный массив с выкупами по дням по курьеркам
     * @var [] массив с данными по среднему выкупу за период по курьеркам
     */
    public function setColorCell($data)
    {
        /**
         * @var [] массив с данными по среднему выкупу за период по курьеркам
         */
        $avgOrders = $this->avgOrders;
        $coloredData = [];

        foreach ($data as $k => $v) {

            foreach ($v as $delivery_name => $delivery_data) {

                if (in_array($delivery_name, array_keys($avgOrders))) {
                    //по дефолту все без цвета
                    $delivery_data['color'] = '';

                    $delivery_data['all_orders'] = isset($delivery_data['all_orders']) ? $delivery_data['all_orders'] : 0;
                    $percent = ($delivery_data['all_orders'] == 0) ? 0 : $delivery_data['approvedOrders'] * 100 / $delivery_data['all_orders'];

                    if (!isset($flag[$delivery_name])) {
                        $flag[$delivery_name] = false;
                    }

                    if ($percent > 0 && $percent >= $avgOrders[$delivery_name]) {
                        $flag[$delivery_name] = true;
                    }

                    if ($flag[$delivery_name] === true) {
                        $delivery_data['color'] = (($percent > 0 && $percent >= $avgOrders[$delivery_name])) ? '#DDFADE' : '#FFE7E7'; 
                    }

                    $coloredData[$k][$delivery_name] = $delivery_data;
                } else {
                    //добаввляю первый столбец с датой, который не учавствует в анализе
                    $coloredData[$k][$delivery_name] = $delivery_data;
                }
            }
        }

        return $coloredData;
    }

    /**
     * @return string
     */
    public function run()
    {

        /**
         * @var integer
         */
        $country_id = yii::$app->user->country->id;

        /**
         *  Установка период для модели
         */
        BuyOutDeliveryQuery::$period = self::PERIOD;

        $avgOrdersByDeliveryFromPeriod = [];
        $countOrdersByDeliveryFromPeriod = [];
        //чтение данных из кеша
        $data = $this->data ?: WidgetCache::readCachedData($this->model->id, $this->model->type->by_country ? yii::$app->user->country->id : null);
        if ($data) {
            $avgOrdersByDeliveryFromPeriod = $data["avg"];
            $countOrdersByDeliveryFromPeriod = $data["count"];
        }
        if ((!$avgOrdersByDeliveryFromPeriod || !$countOrdersByDeliveryFromPeriod) && $this->onlineLoad) {
            $avgOrdersByDeliveryFromPeriod = BuyOutDeliveryQuery::getAVGOrdersByDeliveryFromPeriod(['country_id' => $country_id, 'user_id' => $this->model->user_id]);
            $countOrdersByDeliveryFromPeriod = BuyOutDeliveryQuery::getCountOrdersByDeliveryOnDate(['country_id' => $country_id, 'user_id' => $this->model->user_id]);
        }
        $combineWidgetData = $this->combineWidgetData($avgOrdersByDeliveryFromPeriod, $countOrdersByDeliveryFromPeriod);

        $arrayDataProviderData = $this->prepareArrayDataProvider($combineWidgetData);

        $ArrayDataProvider = new ArrayDataProvider([
            'allModels' => $arrayDataProviderData,
            'pagination' => false,
        ]);

        return $this->render('buyoutdelivery', [
            'ArrayDataProvider' => $ArrayDataProvider,
            'model' => $this->model,
            'columnsNames' => $this->deliveryNames,
            'period_dates' => $this->period_dates,
            'avgOrders' => $this->avgOrders,
            'allOrders' => $this->allOrders,
            'period' => self::PERIOD
        ]);
    }

    public function runData()
    {
        $country_id = $this->countryId;

        BuyOutDeliveryQuery::$period = self::PERIOD;

        $avgOrdersByDeliveryFromPeriod = BuyOutDeliveryQuery::getAVGOrdersByDeliveryFromPeriod(['country_id' => $country_id, 'user_id' => $this->model->user_id]);

        $countOrdersByDeliveryFromPeriod = BuyOutDeliveryQuery::getCountOrdersByDeliveryOnDate(['country_id' => $country_id, 'user_id' => $this->model->user_id]);

        return [
            "avg" => $avgOrdersByDeliveryFromPeriod,
            "count" => $countOrdersByDeliveryFromPeriod
        ];
    }
}