<?php

use app\modules\widget\extensions\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var \yii\data\ArrayDataProvider $dataProvider
 * @var app\modules\widget\widgets\diffofchecks\DiffOfChecks $period
 */

?>

<?= GridView::widget([
    'id' => 'widget-user-' . $model->id . '-grid',
    'exportFilename' => Yii::t('common', $model->type->name),
    'dataProvider' => $dataProvider,
    'tableOptions' => ['class' => 'table table-bordered'],
    'rowOptions' => function ($model, $key, $index, $column) {
        $class = 'tr-vertical-align-middle';
        $bgcolor = 'white';

        return ['class' => $class, 'style' => 'background-color:' . $bgcolor];
    },
    'showHeader' => true,
    'columns' => [
        [
            'header' => Yii::t('common', 'Страна'),
            'headerOptions' => ['style' => 'text-align:center;', 'class' => 'width-150'],
            'attribute' => 'country_name',
            'value' => function ($model) {
                return Yii::t('common', $model['country_name']);
            },
        ],
        [
            'header' => Yii::t('common', 'Средний чек по КЦ ($)'),
            'headerOptions' => ['style' => 'text-align:center;', 'class' => 'width-150'],
            'contentOptions' => ['style' => 'text-align:center;'],
            'attribute' => 'sr_cek_kc',
            'format' => 'raw',
            'value' => function ($model) use ($period) {

                $url = Url::to([
                    'force_country_slug' => $model['force_country_slug'],
                    '/order/index/index',
                    'DateFilter[dateType]' => 'd_sent_at',
                    'DateFilter[dateFrom]' => Yii::$app->formatter->asDate(strtotime("now") - (3600 * 24 * $period), 'MM/dd/yyyy'),
                    'DateFilter[dateTo]' => Yii::$app->formatter->asDate(strtotime("now"), 'MM/dd/yyyy'),
                    'StatusFilter[not][]' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    'StatusFilter[status][]' => [6, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 27, 28, 29, 31],
                ]);

                $formated_percent = round($model['sr_cek_kc'], 1);
                $content = Html::a($formated_percent, $url, ['target' => '_blank']);

                return $content;

            }
        ],
        [
            'header' => Yii::t('common', 'Средний чек по выкупу ($)'),
            'headerOptions' => ['style' => 'text-align:center;', 'class' => 'width-150'],
            'contentOptions' => ['style' => 'text-align:center;'],
            'attribute' => 'vikup',
            'format' => 'raw',
            'value' => function ($model) use ($period) {

                $url = Url::to([
                    'force_country_slug' => $model['force_country_slug'],
                    '/order/index/index',
                    'DateFilter[dateType]' => 'd_sent_at',
                    'DateFilter[dateFrom]' => Yii::$app->formatter->asDate(strtotime("now") - (3600 * 24 * $period), 'MM/dd/yyyy'),
                    'DateFilter[dateTo]' => Yii::$app->formatter->asDate(strtotime("now"), 'MM/dd/yyyy'),
                    'StatusFilter[not][]' => [0, 0],
                    'StatusFilter[status][]' => [15, 18],
                ]);

                $formated_percent = round($model['vikup'], 1);
                $content = Html::a($formated_percent, $url, ['target' => '_blank']);

                return $content;
            }
        ],
        [
            'header' => Yii::t('common', 'Расхождение'),
            'headerOptions' => ['style' => 'text-align:center;', 'class' => 'width-150'],
            'contentOptions' => ['style' => 'text-align:center;'],
            'attribute' => 'abs',
            'value' => function ($model) {
                return round($model['abs'], 2) . '%';
            }
        ],
    ]
]); ?>

<small>* Данные для виджета берутся за последние 31 день</small>
