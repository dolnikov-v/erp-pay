<?php

namespace app\modules\widget\widgets\diffofchecks;

use app\modules\order\models\Order;
use app\models\Country;
use app\models\User;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryRequest;
use app\models\CurrencyRate;
use app\modules\widget\models\WidgetCache;
use app\modules\widget\models\WidgetUser;
use app\modules\widget\widgets\CacheableWidget;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use Yii;
use yii\db\Expression;
use app\modules\order\models\OrderStatus;

/**
 * Class DiffOfCheks
 * @package app\modules\widget\widgets\diffofchecks
 */
class DiffOfChecks extends CacheableWidget
{

    /**
     * @var WidgetUser
     */
    public $model;

    /**
     * @var bool
     */
    public $export = true;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @param $params []
     * @return []
     */
    private function findAll($params)
    {
        $date = date('Y-m-d 00:00:00', strtotime('-' . self::PERIOD . ' day'));
        //ровняемся под фин отчет
        $time = Yii::$app->formatter->asTimestamp($date);

        if (!is_null($this->timeOffset)) {
            $time += $this->timeOffset;
        }

        $query = Order::find()
            ->select([
                'force_country_slug' => Country::tableName() . '.slug',
                'country_id' => Country::tableName() . '.id',
                'country_name' => Country::tableName() . '.name',
                'sr_cek_kc' => new Expression('COALESCE(AVG(CASE WHEN ' . Order::tableName() . '.status_id IN (' . join(', ', OrderStatus::getApproveList()) . ') THEN (' . Order::tableName() . '.price_total / ' . CurrencyRate::tableName() . '.rate  + ' . Order::tableName() . '.delivery/ ' . CurrencyRate::tableName() . '.rate) ELSE NULL END), 0)'),
                'vikup' => new Expression('COALESCE(AVG(CASE WHEN ' . Order::tableName() . '.status_id IN (' . join(', ', OrderStatus::getBuyoutList()) . ') THEN (' . Order::tableName() . '.price_total / ' . CurrencyRate::tableName() . '.rate  + ' . Order::tableName() . '.delivery/ ' . CurrencyRate::tableName() . '.rate) ELSE NULL END), 0)')
            ])
            ->leftJoin(CallCenterRequest::tableName(), [CallCenterRequest::tableName() . '.order_id' => new Expression(Order::tableName() . '.id')])
            ->leftJoin(DeliveryRequest::tableName(), [DeliveryRequest::tableName() . '.order_id' => new Expression(Order::tableName() . '.id')])
            ->leftJoin(Delivery::tableName(), [Delivery::tableName() . '.id' => new Expression(DeliveryRequest::tableName() . '.delivery_id')])
            ->leftJoin(Country::tableName(), [Delivery::tableName() . '.country_id' => new Expression(Country::tableName() . '.id')])
            ->LeftJoin(CurrencyRate::tableName(), [Country::tableName() . '.currency_id' => new Expression(CurrencyRate::tableName() . '.currency_id')])
            ->where(['is not', Country::tableName() . '.id', null])
            ->andWhere(['>=', DeliveryRequest::tableName() . '.sent_at', $time])
            ->andWhere(['NOT IN', Order::tableName() .'.source_id', User::getRejectedSources($this->model->user_id)])
            ->groupBy([Country::tableName() . '.id']);
        $query->andFilterWhere(['in', Order::tableName() . '.status_id', OrderStatus::getAllList()]);

        return $query->asArray()->all();
    }

    /**
     * @param []
     * @return []
     */
    public function reSort($data)
    {
        $sortedData = [];
        foreach ($data as $k => $v) {
            if ($v['vikup'] > 0) {
                $sortedData[$k] = $v;
                $sortedData[$k]['abs'] = 100 * abs(1 - $sortedData[$k]['vikup'] / $sortedData[$k]['sr_cek_kc']);
            }
        }

        ArrayHelper::multisort($sortedData, ['abs'], SORT_DESC);

        return $sortedData;
    }

    /**
     * @return string
     */
    public function run()
    {
        //чтение данных из кеша
        $data = $this->data ?: WidgetCache::readCachedData($this->model->id, $this->model->type->by_country ? yii::$app->user->country->id : null);
        if (!$data && $this->onlineLoad) {
            $ArrayDataProvider = $this->findAll([]);
            $data = $this->reSort($ArrayDataProvider);
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'pagination' => false
        ]);


        return $this->render('diff-of-checks', [
            'dataProvider' => $dataProvider,
            'model' => $this->model,
            'period' => self::PERIOD
        ]);

    }

    public function runData() {
        $ArrayDataProvider = $this->findAll([]);
        return $this->reSort($ArrayDataProvider);
    }
}