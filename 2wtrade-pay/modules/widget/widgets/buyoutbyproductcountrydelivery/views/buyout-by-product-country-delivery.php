<?php

use app\modules\widget\extensions\GridView;

/**
 * @var \app\modules\widget\models\WidgetUser $model
 * @var \yii\data\ArrayDataProvider $dataProvider
 */

?>

<?= GridView::widget([
    'id' => 'widget-user-' . $model->id . '-grid',
    'exportFilename' => Yii::t('common', $model->type->name),
    'dataProvider' => $dataProvider,
    'tableOptions' => ['class' => 'table table-bordered'],
    'rowOptions' => function ($model, $key, $index, $column) {
        $class = 'tr-vertical-align-middle';
        $bgcolor = 'white';

        return ['class' => $class, 'style' => 'background-color:' . $bgcolor];
    },
    'showHeader' => true,
    'columns' => [
        [
            'attribute' => 'lowData',
            'group' => true,
            'groupedRow' => true,
            'contentOptions' => ['style'=>'text-align:center; font-weight: bold'],
            'value' => function ($model) {
                return ($model['lowData'] ? Yii::t('common', 'Мало данных') : '');
            }
        ],
        [
            'header' => Yii::t('common', 'Товар'),
            'headerOptions' => ['style' => 'text-align:center;', 'class' => 'width-150'],
            'attribute' => 'product_name'
        ],
        [
            'header' => Yii::t('common', 'Страна'),
            'headerOptions' => ['style' => 'text-align:center;', 'class' => 'width-150'],
            'attribute' => 'country_name'
        ],
        [
            'header' => Yii::t('common', 'Курьерка'),
            'headerOptions' => ['style' => 'text-align:center;', 'class' => 'width-150'],
            'attribute' => 'delivery_name',
        ],
        [
            'header' => Yii::t('common', 'Выкуп'),
            'headerOptions' => ['style' => 'text-align:center;', 'class' => 'width-150'],
            'contentOptions' => ['style' => 'text-align:center;'],
            'attribute' => 'percentApproved',
            'value' => function ($model) {
                return round($model['percentApproved'], 2) . '%';
            }
        ],
    ]
]); ?>