<?php

namespace app\modules\widget\widgets\buyoutbyproductcountrydelivery;

use app\modules\order\models\Order;
use app\models\Country;
use app\modules\order\models\OrderProduct;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\widget\models\WidgetCache;
use app\modules\widget\models\WidgetUser;
use app\models\Product;
use app\modules\widget\widgets\CacheableWidget;
use yii\data\ArrayDataProvider;
use app\models\User;
use yii;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use app\modules\order\models\OrderStatus;

/**
 * Class BuyOutByProductCountryDelivery
 * @package app\modules\widget\widgets\buyoutbyproductcountrydelivery
 */
class BuyOutByProductCountryDelivery extends CacheableWidget
{

    /**
     * @var WidgetUser
     */
    public $model;

    /**
     * @var bool
     */
    public $export = true;


    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @param $params []
     * @return []
     */
    private function findAll($params)
    {
        $countries_ids = $params['countries_ids'];

        $query = Order::find()
            ->select([
                'country_id' => Country::tableName() . '.id',
                'country_name' => Country::tableName() . '.name',
                'delivery_id' => Delivery::tableName() . '.id',
                'delivery_name' => Delivery::tableName() . '.name',
                'product_id' => Product::tableName() . '.id',
                'product_name' => Product::tableName() . '.name',
                'vsego_zakazov' => new Expression('COUNT( DISTINCT if (' . Order::tableName() . '.status_id IN (' . join(', ', OrderStatus::getApproveList()) . '), ' . Order::tableName() . '.id, NULL))'),
                'vykupleno' => new Expression('COUNT( DISTINCT if (' . Order::tableName() . '.status_id IN (' . join(', ', OrderStatus::getBuyoutList()) . '), ' . Order::tableName() . '.id, NULL))')
            ])
            ->leftJoin(OrderProduct::tableName(), [Order::tableName() . '.id' => new Expression(OrderProduct::tableName() . '.order_id')])
            ->leftJoin(Country::tableName(), [Country::tableName() . '.id' => new Expression(Order::tableName() . '.country_id')])
            ->leftJoin(Product::tableName(), [Product::tableName() . '.id' => new Expression(OrderProduct::tableName() . '.product_id')])
            ->leftJoin(DeliveryRequest::tableName(), [DeliveryRequest::tableName() . '.order_id' => new Expression(Order::tableName() . '.id')])
            ->leftJoin(Delivery::tableName(), [Delivery::tableName() . '.id' => new Expression(DeliveryRequest::tableName() . '.delivery_id')])
            ->where(['in', Order::tableName() . '.status_id', OrderStatus::getApproveList()])
            ->andWhere(['is not', Product::tableName() . '.name', NULL])
            ->andWhere(['is not', Delivery::tableName() . '.name', NULL])
            ->andWhere(['NOT IN', Order::tableName() .'.source_id', User::getRejectedSources($this->model->user_id)])
            ->groupBy([Product::tableName() . '.id', Country::tableName() . '.id', Delivery::tableName() . '.id']);

        $query->andFilterWhere(['in', Country::tableName() . '.id', $countries_ids]);
        $query->andFilterWhere(['>=', Order::tableName() . '.created_at', new Expression('UNIX_TIMESTAMP(CURDATE() - INTERVAL ' . self::PERIOD . ' DAY)')]);

        return $query->asArray()->all();
    }

    /**
     * @param []
     * @return []
     * Процент выкупа
     */
    public function getPercentApproved($data)
    {
        $tempData = [];
        foreach ($data as $k => $v) {
            if ($v['vykupleno'] > 0) {
                $percentApproved = $v['vykupleno'] * 100 / $v['vsego_zakazov'];
                $tempData[$k] = $v;
                $tempData[$k]['percentApproved'] = $percentApproved;
            }
        }
        ArrayHelper::multisort($tempData, ['percentApproved'], SORT_DESC);

        return $tempData;
    }

    /**
     * @return string
     */
    public function run()
    {
        //чтение данных из кеша
        $data = $this->data ?: WidgetCache::readCachedData($this->model->id, $this->model->type->by_country ? yii::$app->user->country->id : null);
        if (!$data && $this->onlineLoad) {
            $countries_ids = array_keys(User::getAllowCountries());
            $params = ['countries_ids' => $countries_ids];
            $data = $this->findAll($params);
        }

        $sortedArrayDataProvider = $this->getPercentApproved($data);

        // если 10 и более выкупов то нормально, иначе будет под заголовком "Мало данных"
        foreach ($sortedArrayDataProvider as &$line) {
            if ($line['vykupleno'] > 10) {
                $line['lowData'] = 0;
            } else {
                $line['lowData'] = 1;
            }
        }

        ArrayHelper::multisort($sortedArrayDataProvider, ['lowData', 'percentApproved'], [SORT_ASC, SORT_DESC]);

        $dataProvider = new ArrayDataProvider([
            'allModels' => $sortedArrayDataProvider,
            'pagination' => false,
        ]);

        return $this->render('buyout-by-product-country-delivery', [
            'dataProvider' => $dataProvider,
            'model' => $this->model,
            'period' => self::PERIOD
        ]);

    }

    public function runData()
    {
        return $this->findAll(['countries_ids' => $this->countryIds]);
    }
}