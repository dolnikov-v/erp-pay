<?php

namespace app\modules\widget\widgets\top20cities;

use app\modules\order\models\Order;
use app\models\Country;
use app\models\User;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryRequest;
use app\models\CurrencyRate;
use app\modules\widget\models\WidgetCache;
use app\modules\widget\models\WidgetUser;
use app\modules\widget\widgets\CacheableWidget;
use yii\data\ArrayDataProvider;
use yii;
use yii\db\Expression;
use app\modules\order\models\OrderStatus;

/**
 * Class Top20Cities
 * @package app\modules\widget\widgets\top20cities
 * Виджет ТОП 20 городов по order.status_id in(12,15,18)
 */
class Top20Cities extends CacheableWidget
{

    /**
     * @var WidgetUser
     */
    public $model;

    /**
     * @var bool
     */
    public $export = true;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @param $params []
     * @return []
     */
    private function findAll($params)
    {
        $country_id = $params['country_id'];

        $query = Order::find()
            ->select([
                'country_id' => Country::tableName() . '.id',
                'customer_city' => Order::tableName() . '.customer_city',
                'income' => new Expression('sum(' . Order::tableName() . '.price_total / ' . CurrencyRate::tableName() . '.rate + ' . Order::tableName() . '.delivery / ' . CurrencyRate::tableName() . '.rate)'),
            ])
            ->leftJoin(DeliveryRequest::tableName(), [DeliveryRequest::tableName() . '.order_id' => new Expression(Order::tableName() . '.id')])
            ->leftJoin(Delivery::tableName(), [Delivery::tableName() . '.id' => new Expression(DeliveryRequest::tableName() . '.delivery_id')])
            ->leftJoin(Country::tableName(), [Delivery::tableName() . '.country_id' => new Expression(Country::tableName() . '.id')])
            ->LeftJoin(CurrencyRate::tableName(), [Country::tableName() . '.currency_id' => new Expression(CurrencyRate::tableName() . '.currency_id')])
            ->where([Country::tableName() . '.id' => $country_id])
            ->andWhere(['NOT IN', Order::tableName() .'.source_id', User::getRejectedSources($this->model->user_id)])
            ->orderBy(['income' => SORT_DESC])
            ->groupBy([Order::tableName() . '.customer_city'])
            ->limit(20);

        $query->andFilterWhere(['in', Order::tableName() . '.status_id', [OrderStatus::STATUS_DELIVERY_ACCEPTED, OrderStatus::STATUS_DELIVERY_BUYOUT, OrderStatus::STATUS_FINANCE_MONEY_RECEIVED]]);
        $query->andFilterWhere(['<>', Order::tableName() . '.customer_city', '']);
        $query->andFilterWhere(['>=', Order::tableName() . '.created_at', new Expression('UNIX_TIMESTAMP(CURDATE() - INTERVAL ' . self::PERIOD . ' DAY)')]);

        return $query->asArray()->all();
    }

    /**
     * @return string
     */
    public function run()
    {

        $data = $this->data ?: WidgetCache::readCachedData($this->model->id, $this->model->type->by_country ? yii::$app->user->country->id : null);
        if (!$data && $this->onlineLoad) {
            $data = $this->findAll(['country_id' => Yii::$app->user->country->id]);
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'pagination' => false
        ]);


        return $this->render('top20cities', [
            'dataProvider' => $dataProvider,
            'model' => $this->model,
            'period' => self::PERIOD
        ]);

    }

    public function runData()
    {
        return $this->findAll(['country_id' => $this->countryId]);
    }

}