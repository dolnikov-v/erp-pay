<?php

use app\modules\widget\assets\WidgetRoleSwitcheryAsset;
use app\widgets\Switchery;

/** @var \app\models\User $model */
/** @var array $widgetsByRole */
/** @var array $widgets */

WidgetRoleSwitcheryAsset::register($this);
?>
<table class="table table-striped table-hover" data-role="<?= $model->name ?>">
    <?php if (!empty($widgets)) : ?>
        <thead>
        <tr>
            <th><?= Yii::t('common', 'Название') ?></th>
            <th class="text-center width-150"><?= Yii::t('common', 'Вкл/Выкл') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($widgets as $widget) : ?>
            <tr>
                <td><?= Yii::t('common', $widget->name) ?></td>
                <td class="text-center" data-widget="<?= $widget->id ?>">
                    <?= Switchery::widget([
                        'checked' => in_array($widget->id, $widgetsByRole),
                        'disabled' => false,
                        'attributes' => [
                            'data-widget' => 'widget-role-switchery',
                        ],
                    ]); ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    <?php else: ?>
        <tr>
            <td class="text-center"><?= Yii::t('common', 'Виджеты отсутствуют') ?></td>
        </tr>
    <?php endif ?>
</table>
