<?php

use app\modules\widget\extensions\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var \yii\data\ArrayDataProvider $dataProvider
 * @var app\modules\widget\widgets\deliveryterms\DeliveryTerms $period ;
 * @var app\modules\widget\widgets\deliveryterms\DeliveryTerms $filterPeriod ;
 */

?>

<?= GridView::widget([
    'id' => 'widget-user-' . $model->id . '-grid',
    'exportFilename' => Yii::t('common', $model->type->name),
    'dataProvider' => $dataProvider,
    'tableOptions' => ['class' => 'table table-bordered'],
    'rowOptions' => function ($model, $key, $index, $column) {
        $class = 'tr-vertical-align-middle';

        if ($model['hours'] > 48) {
            $bgcolor = '#FFE7E7';
        } elseif ($model['hours'] >= 0 && $model['hours'] <= 48) {
            $bgcolor = '#DDFADE';
        } //все отрицательные не красим, отрицательное число это не нормально.
        else {
            $bgcolor = 'white';
        }

        return ['class' => $class, 'style' => 'background-color:' . $bgcolor];
    },
    'showHeader' => true,
    'columns' => [
        [
            'header' => Yii::t('common', 'Страна/Курьерка'),
            'headerOptions' => ['style' => 'text-align:center;', 'class' => 'width-250'],
            'attribute' => 'fullname'
        ],
        [
            'header' => Yii::t('common', 'Средний срок доставки'),
            'headerOptions' => ['style' => 'text-align:center;', 'class' => 'width-150'],
            'contentOptions' => ['style' => 'text-align:center;'],
            'attribute' => 'hours',
            'format' => 'raw',
            'value' => function ($model) use ($period, $filterPeriod) {
                $url = Url::to([
                    'force_country_slug' => $model['force_country_slug'],
                    '/order/index/index',
                    'DateFilter[dateType]' => 'created_at',
                    'DateFilter[dateFrom]' => Yii::$app->formatter->asDate(strtotime("now") - (3600 * 24 * $filterPeriod), 'MM/dd/yyyy'),
                    'DateFilter[dateTo]' => Yii::$app->formatter->asDate(strtotime("now"), 'MM/dd/yyyy'),
                    'DeliveryFilter[not][]' => 0,
                    'DeliveryFilter[delivery][]' => $model['delivery_id'],
                    'DeliveryTermsFilter[not][]' => '',
                    'DeliveryTermsFilter[entity][]' => 3600,
                    'DeliveryTermsFilter[deliveryTerms][]' => $filterPeriod
                ]);

                $content = Html::a($model['hours'], $url, ['target' => '_blank']);

                return $content;
            }
        ],
    ]
]); ?>
<?= '<br><span style="font-size: 75%;">' .Yii::t("common", "*Данные берутся за последние 31 день по выбранной стране. Сроки указаны в часах.") .'</span>' ?>