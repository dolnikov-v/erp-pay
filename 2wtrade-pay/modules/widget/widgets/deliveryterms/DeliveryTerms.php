<?php

namespace app\modules\widget\widgets\deliveryterms;

use app\modules\order\models\Order;
use app\models\Country;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\widget\models\WidgetCache;
use app\modules\widget\models\WidgetUser;
use app\modules\widget\widgets\CacheableWidget;
use yii\data\ArrayDataProvider;
use app\models\User;
use yii\db\Expression;
use yii;

/**
 * Class TermsOfShipping
 * @package app\modules\widget\widgets\deliveryterms
 */
class DeliveryTerms extends CacheableWidget
{
    const FILTER_PERIOD = 64;

    /**
     * @var bool
     */
    public $export = true;

    /**
     * @var WidgetUser
     */
    public $model;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @param $params []
     * @return []
     */
    private function findAll($params)
    {
        $countries_ids = $params['countries_ids'];

        $query = Order::find()
            ->select([
                'country_id' => Country::tableName() . '.id',
                'force_country_slug' => Country::tableName() . '.slug',
                'country_name' => Country::tableName() . '.name',
                'delivery_id' => Delivery::tableName() . '.id',
                'delivery_name' => Delivery::tableName() . '.name',
                'fullname' => new Expression("concat(" . Country::tableName() . " .name, '/', " . Delivery::tableName() . ".name)"),
                'hours' => new Expression('round(avg(((' . DeliveryRequest::tableName() . '.approved_at - if(' . DeliveryRequest::tableName() . '.accepted_at > 0, ' . DeliveryRequest::tableName() . '.accepted_at, ' . DeliveryRequest::tableName() . '.sent_at))/3600)))')
            ])
            ->leftJoin(CallCenterRequest::tableName(), [CallCenterRequest::tableName() . '.order_id' => new Expression(Order::tableName() . '.id')])
            ->leftJoin(DeliveryRequest::tableName(), [DeliveryRequest::tableName() . '.order_id' => new Expression(Order::tableName() . '.id')])
            ->leftJoin(Delivery::tableName(), [Delivery::tableName() . '.id' => new Expression(DeliveryRequest::tableName() . '.delivery_id')])
            ->leftJoin(Country::tableName(), [Delivery::tableName() . '.country_id' => new Expression(Country::tableName() . '.id')])
            ->where(['>', DeliveryRequest::tableName() . '.approved_at', 0])
            ->andWhere(['NOT IN', Order::tableName() .'.source_id', User::getRejectedSources($this->model->user_id)])
            ->andFilterWhere(['or',
                ['>', DeliveryRequest::tableName() . '.sent_at', 0],
                ['>', DeliveryRequest::tableName() . '.accepted_at', 0]
            ])
            ->orderBy(['hours' => SORT_DESC])
            ->groupBy([Delivery::tableName() . '.id']);

        $query->andFilterWhere(['in', Delivery::tableName() . '.country_id', $countries_ids]);
        $query->andFilterWhere(['>=', Order::tableName() . '.created_at', new Expression('UNIX_TIMESTAMP(CURDATE() - INTERVAL ' . self::PERIOD . ' DAY)')]);

        return $query->asArray()->all();
    }

    /**
     * @return string
     */
    public function run()
    {
        //чтение данных из кеша
        $data = $this->data ?: WidgetCache::readCachedData($this->model->id, $this->model->type->by_country ? yii::$app->user->country->id : null);
        if (!$data && $this->onlineLoad) {
            $data = $this->findAll(['countries_ids' => array_keys(User::getAllowCountries())]);
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'pagination' => false,
        ]);


        return $this->render('delivery-terms', [
            'dataProvider' => $dataProvider,
            'model' => $this->model,
            'period' => self::PERIOD,
            'filterPeriod' => self::FILTER_PERIOD
        ]);

    }

    public function runData() {
        return $this->findAll(['countries_ids' => $this->countryIds]);
    }
}