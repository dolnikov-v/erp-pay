<?php

namespace app\modules\widget\widgets\top20products;

use app\modules\order\models\Order;
use app\models\Product;
use app\models\User;
use app\modules\order\models\OrderProduct;
use app\models\Country;
use app\models\CurrencyRate;
use app\modules\widget\models\WidgetCache;
use app\modules\widget\models\WidgetUser;
use app\modules\widget\widgets\CacheableWidget;
use yii\data\ArrayDataProvider;
use yii;
use yii\db\Expression;
use app\modules\order\models\OrderStatus;

/**
 * Class Top20Cities
 * @package app\modules\widget\widgets\top20products
 * Виджет ТОП товаров order.status_id in(15,18)
 */
class Top20Products extends CacheableWidget
{

    /**
     * @var WidgetUser
     */
    public $model;

    /**
     * @var bool
     */
    public $export = true;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @param $params []
     * @return []
     */
    private function findAll($params)
    {
        $country_id = $params['country_id'];

        $query = OrderProduct::find()
            ->select([
                'customer_city' => Order::tableName() . '.customer_city',
                'order_product_id' => OrderProduct::tableName() . '.product_id',
                'product_name' => Product::tableName() . '.name',
                'income' => new Expression('sum(' . OrderProduct::tableName() . '.quantity * ' . OrderProduct::tableName() . '.price / ' . CurrencyRate::tableName() . '.rate) '),
            ])
            ->innerJoin(Order::tableName(), [Order::tableName() . '.id' => new Expression(OrderProduct::tableName() . '.order_id')])
            ->innerJoin(Country::tableName(), [Country::tableName() . '.id' => new Expression(Order::tableName() . '.country_id')])
            ->innerJoin(Product::tableName(), [Product::tableName() . '.id' => new Expression(OrderProduct::tableName() . '.product_id')])
            ->LeftJoin(CurrencyRate::tableName(), [Country::tableName() . '.currency_id' => new Expression(CurrencyRate::tableName() . '.currency_id')])
            ->where([Country::tableName() . '.id' => $country_id])
            ->andWhere(['NOT IN', Order::tableName() .'.source_id', User::getRejectedSources($this->model->user_id)])
            ->orderBy(['income' => SORT_DESC])
            ->groupBy([OrderProduct::tableName() . '.product_id', Order::tableName() . '.customer_city'])
            ->limit(20);

        $query->andFilterWhere(['in', Order::tableName() . '.status_id', OrderStatus::getBuyoutList()]);
        $query->andFilterWhere(['<>', Order::tableName() . '.customer_city', '']);
        $query->andFilterWhere(['>=', Order::tableName() . '.created_at', new Expression('UNIX_TIMESTAMP(CURDATE() - INTERVAL ' . self::PERIOD . ' DAY)')]);

        return $query->asArray()->all();
    }

    /**
     * @return string
     */
    public function run()
    {

        $data = $this->data ?: WidgetCache::readCachedData($this->model->id, $this->model->type->by_country ? yii::$app->user->country->id : null);
        if (!$data && $this->onlineLoad) {
            $data = $this->findAll(['country_id' => Yii::$app->user->country->id]);
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'pagination' => false
        ]);


        return $this->render('top20products', [
            'dataProvider' => $dataProvider,
            'model' => $this->model,
            'period' => self::PERIOD
        ]);

    }

    public function runData()
    {
        return $this->findAll(['country_id' => $this->countryId]);
    }
}