<?php

use app\modules\widget\extensions\GridView;
use yii\helpers\Html;

/**
 * @var \yii\data\ArrayDataProvider $dataProvider
 */

?>

<?= GridView::widget([
    'id' => 'widget-user-' . $model->id . '-grid',
    'exportFilename' => Yii::t('common', $model->type->name),
    'dataProvider' => $dataProvider,
    'tableOptions' => ['class' => 'table table-bordered'],
    'rowOptions' => function ($model, $key, $index, $column) {
        $class = 'tr-vertical-align-middle';
        $bgcolor = 'white';

        return ['class' => $class, 'style' => 'background-color:' . $bgcolor];
    },
    'showHeader' => true,
    'columns' => [
        [
            'header' => Yii::t('common', '№'),
            'headerOptions' => ['style' => 'text-align:center;', 'class' => 'width-20'],
            'attribute' => 'country_name',
            'value' => function ($data, $row, $column) {
                return ++$row . '.';
            }
        ],
        [
            'header' => Yii::t('common', 'Город'),
            'headerOptions' => ['style' => 'text-align:center;', 'class' => 'width-300'],
            'contentOptions' => ['style' => 'text-align:left;'],
            'attribute' => 'customer_city'
        ],
        [
            'header' => Yii::t('common', 'Товар'),
            'headerOptions' => ['style' => 'text-align:center;', 'class' => 'width-300'],
            'contentOptions' => ['style' => 'text-align:left;'],
            'attribute' => 'product_name'
        ],
        [
            'header' => Yii::t('common', 'Доход ($)'),
            'headerOptions' => ['style' => 'text-align:center;', 'class' => 'width-150'],
            'contentOptions' => ['style' => 'text-align:center;'],
            'attribute' => 'income',
            'value' => function ($model) {
                return number_format(round($model['income'], 0), 0, ',', ' ');
            }
        ],
    ]
]); ?>
