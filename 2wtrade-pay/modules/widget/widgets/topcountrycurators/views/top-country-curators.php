<?php

use app\modules\widget\extensions\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var \yii\data\ArrayDataProvider $dataProvider
 */

?>

<?= GridView::widget([
    'id' => 'widget-user-' . $model->id . '-grid',
    'exportFilename' => Yii::t('common', $model->type->name),
    'dataProvider' => $dataProvider,
    'tableOptions' => ['class' => 'table table-bordered'],
    'rowOptions' => function ($model, $key, $index, $column) {
        $class = 'tr-vertical-align-middle text-center  ';
        $bgcolor = 'white';

        return ['style' => 'background-color:' . $bgcolor];
    },
    'showHeader' => true,
    'columns' => [
        [
            'header' => Yii::t('common', 'Территориальный менеджер'),
            'headerOptions' => ['style' => 'text-align:center;', 'class' => 'width-150'],
            'attribute' => 'username',
            'value' => function ($model){
                return $model['username'];
            },
        ],
        [
            'header' => Yii::t('common', 'Доход ($)'),
            'headerOptions' => ['style' => 'text-align:center;', 'class' => 'width-50'],
            'contentOptions' => ['style' => 'text-align:center;'],
            'attribute' => 'sum',
            'format' => 'raw',
            'value' => function ($model){
                return number_format(round($model['sum'], 0), 0, ',', ' ');
            },
        ],
    ]
]); ?>
