<?php
namespace app\modules\widget\widgets\topcountrycurators;

use app\modules\widget\models\WidgetCache;
use app\modules\widget\widgets\CacheableWidget;
use app\modules\widget\widgets\topcountrycurators\models\Query\TopCountryCuratorsQuery;
use app\modules\widget\models\WidgetUser;
use app\models\User;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii;


/**
 * Class TopCoutryCurators
 * @package app\modules\widget\widgets\topcountrycurators
 */
class TopCountryCurators extends CacheableWidget
{
    /**
     * @var WidgetUser
     */
    public $model;

    /**
     * @var bool
     */
    public $export = true;

    /**
     * @var []
     */
    public $idsCurators;

    /**
     * @var []
     */
    public $users;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @param $sumApprovedOrdersByCountries []  массив со странами по которым были доходы за период
     */
    public function getNotEmptyApprovedCountriesIds($sumApprovedOrdersByCountries)
    {
        $notemptyCountriesIds = [];

        foreach ($sumApprovedOrdersByCountries as $k => $country) {
            $notemptyCountriesIds[] = $country['country_id'];
        }

        return $notemptyCountriesIds;
    }

    /**
     * Задача метода исключить из списка терменов, страны без доходом, за указанный период
     * @param [] ids стран, по которым были доходы
     * @param [] список терменов по странам
     * @return []
     */
    public function clearemptyCountries($notemptyCountriesIds, $curatorsByCountries)
    {
        $clearListCuratorsByCountries = [];

        foreach ($curatorsByCountries as $k => $country) {
            if (in_array($country['country_id'], $notemptyCountriesIds)) {
                $clearListCuratorsByCountries[$country['country_id']] = $country;
            }
        }

        return $clearListCuratorsByCountries;
    }

    /**
     * @param [] - список терменов по странам у которых есть доход за период
     * @return [] ids терменов
     */
    public function getIdsCurators($clearListCuratorsByCountries)
    {
        $listIdsCurators = [];

        foreach ($clearListCuratorsByCountries as $k => $countries) {
            $stringIds = $countries['users_ids'];
            $arrayIds = explode(',', $stringIds);

            $listIdsCurators = array_merge($listIdsCurators, $arrayIds);
        }

        return array_unique($listIdsCurators);
    }

    /**
     * @param [] доходные страны с доходами
     * @return []  - доход отдельного термена от страны (по ТЗ - весь доход страны распределён равномерно между терменами)
     */
    public function getPieceFromImcome($clearListCuratorsByCountries, $sumApprovedOrdersByCountries)
    {
        //сбросить ключи
        sort($clearListCuratorsByCountries);
        sort($sumApprovedOrdersByCountries);

        $piecesOfIncome = [];

        foreach ($clearListCuratorsByCountries as $k => $v) {
            $piecesOfIncome[$v['country_id']] = $sumApprovedOrdersByCountries[$k]['approvedSum'] / $v['users_count'];
        }

        return $piecesOfIncome;
    }

    /**
     * Получить имя пользователя по id
     * @param integer
     * @return string
     */
    public function getUserNameById($id)
    {
        foreach ($this->users as $k => $v) {
            if ($v['id'] == $id) {
                return $v['username'];
            }
        }
    }

    /**
     * @param [] термены по странам
     * @param  [] доходы терменам по отдельной стране
     * @return [] список терменов с подсчитанными доходами
     */
    public function calculateIncome($clearListCuratorsByCountries, $piecesOfIncome)
    {
        if (!is_array($this->users)) {
            $this->users = TopCountryCuratorsQuery::getUsernames($this->idsCurators);
        }

        $calculatedIncome = [];

        foreach ($clearListCuratorsByCountries as $k => $v) {
            $listIds = explode(',', $v['users_ids']);

            foreach ($listIds as $id) {
                if (!isset($calculatedIncome[$id])) {
                    $calculatedIncome[$id] = 0;
                }

                $calculatedIncome[$id] += $piecesOfIncome[$v['country_id']];
            }
        }

        return $calculatedIncome;
    }

    /**
     * Подготовка данный для ArrayDataProvider
     * @param []
     * @return []
     */
    public function combineData($calculatedIncome)
    {
        $arrayDataProvider = [];

        foreach ($calculatedIncome as $k => $v) {
            $arrayDataProvider[] = [
                'user_id' => $k,
                'username' => $this->getUserNameById($k),
                'sum' => $v
            ];
        }

        ArrayHelper::multisort($arrayDataProvider, ['sum'], SORT_DESC);

        return $arrayDataProvider;
    }

    /**
     * @return string
     */
    public function run()
    {

        $data = $this->data ?: WidgetCache::readCachedData($this->model->id, $this->model->type->by_country ? yii::$app->user->country->id : null);
        if (!$data && $this->onlineLoad) {

            $countries_ids = array_keys(User::getAllowCountries());
            $params = ['countries_ids' => $countries_ids, 'user_id' => $this->model->user_id];

            // Установка периода для модели
            TopCountryCuratorsQuery::$period = self::PERIOD;

            // список стран с доходами за период
            $sumApprovedOrdersByCountries = TopCountryCuratorsQuery::getSumApprovedOrdersByCountries($params);

            // подготовленный массив со странами по которым были доходы за период
            $getNotEmptyApprovedCountriesIds = $this->getNotEmptyApprovedCountriesIds($sumApprovedOrdersByCountries);

            // список стран с кураторами
            $curatorsByCountries = TopCountryCuratorsQuery::getCuratorsByCountries($params);

            // список стран, с доходами за период, с терменами
            $clearListCuratorsByCountries = $this->clearemptyCountries($getNotEmptyApprovedCountriesIds, $curatorsByCountries);

            // список ID терменов
            $this->idsCurators = $this->getIdsCurators($clearListCuratorsByCountries);

            // доля одного термена от дохода страны
            $piecesOfIncome = $this->getPieceFromImcome($clearListCuratorsByCountries, $sumApprovedOrdersByCountries);

            // доходы терменов по странам
            $calculatedIncome = $this->calculateIncome($clearListCuratorsByCountries, $piecesOfIncome);

            $data = $this->combineData($calculatedIncome);
        }


        $ArrayDataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'pagination' => false,
        ]);

        return $this->render('top-country-curators', [
            'dataProvider' => $ArrayDataProvider,
            'model' => $this->model
        ]);
    }

    public function runData()
    {

        $params = ['countries_ids' => $this->countryIds, 'user_id' => $this->model->user_id];

        // Установка периода для модели
        TopCountryCuratorsQuery::$period = self::PERIOD;

        // список стран с доходами за период
        $sumApprovedOrdersByCountries = TopCountryCuratorsQuery::getSumApprovedOrdersByCountries($params);

        // подготовленный массив со странами по которым были доходы за период
        $getNotEmptyApprovedCountriesIds = $this->getNotEmptyApprovedCountriesIds($sumApprovedOrdersByCountries);

        // список стран с кураторами
        $curatorsByCountries = TopCountryCuratorsQuery::getCuratorsByCountries($params);

        // список стран, с доходами за период, с терменами
        $clearListCuratorsByCountries = $this->clearemptyCountries($getNotEmptyApprovedCountriesIds, $curatorsByCountries);

        // список ID терменов
        $this->idsCurators = $this->getIdsCurators($clearListCuratorsByCountries);

        // доля одного термена от дохода страны
        $piecesOfIncome = $this->getPieceFromImcome($clearListCuratorsByCountries, $sumApprovedOrdersByCountries);

        // доходы терменов по странам
        $calculatedIncome = $this->calculateIncome($clearListCuratorsByCountries, $piecesOfIncome);

        return $this->combineData($calculatedIncome);
    }
}