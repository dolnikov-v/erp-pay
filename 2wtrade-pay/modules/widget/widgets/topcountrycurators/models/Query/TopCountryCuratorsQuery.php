<?php
namespace app\modules\widget\widgets\topcountrycurators\models\Query;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\Country;
use app\modules\order\models\Order;
use app\models\CurrencyRate;
use app\models\User;
use app\models\UserCountry;
use app\models\AuthAssignment;
use app\modules\order\models\OrderStatus;
use yii\db\Expression;
use Yii;


/**
 * Class class TopCountryCuratorsQuery extends ActiveRecord
 * @package app\modules\widget\widgets\topcountrycurators
 */
class TopCountryCuratorsQuery extends ActiveRecordLogUpdateTime
{
    /**
     * @var integer
     */
    public static $period;

    /**
     * Получить Username терменов
     * @param $ids []
     * @return array
     */
    public static function getUsernames($ids)
    {
        $query = User::find()
            ->where(['in', User::tableName() . '.id', $ids]);

        return $query->asArray()->all();
    }

    /**
     * Получить сумму апрувнутых заказов по странам за определённый период
     * @param $params []
     * @return array
     */
    public static function getSumApprovedOrdersByCountries($params)
    {
        $countries_ids = $params['countries_ids'];
        $user_id = $params['user_id'];

        $query = Order::find()
            ->select([
                'country_id' => Country::tableName() . '.id',
                'country_name' => Country::tableName() . '.name',
                'approvedSum' => new Expression('sum((' . Order::tableName() . '.price_total  + ' . Order::tableName() . '.delivery) / ' . CurrencyRate::tableName() . '.`rate`)')
            ])
            ->innerJoin(Country::tableName(), [Country::tableName() . '.id' => new Expression(Order::tableName() . '.country_id')])
            ->innerJoin(CurrencyRate::tableName(), [CurrencyRate::tableName() . '.currency_id' => new Expression(Country::tableName() . '.currency_id')])
            ->andWhere(['NOT IN', Order::tableName() .'.source_id', User::getRejectedSources($user_id)])
            ->groupBy([Country::tableName() . '.id']);

        $query->andFilterWhere(['in', Order::tableName() . '.country_id', $countries_ids]);
        $query->andFilterWhere(['in', Order::tableName() . '.status_id', [OrderStatus::STATUS_DELIVERY_BUYOUT, OrderStatus::STATUS_FINANCE_MONEY_RECEIVED]]);
        $query->andFilterWhere(['>=', Order::tableName() . '.created_at', new Expression('UNIX_TIMESTAMP(CURDATE() - INTERVAL ' . self::$period . ' DAY)')]);

        return $query->asArray()->all();
    }

    /**
     * Получить список id тер.менеджеров по странам
     * @param $params []
     * @return array
     */
    public static function getCuratorsByCountries($params)
    {
        $countries_ids = $params['countries_ids'];

        $query = Country::find()
            ->select([
                'country_id' => Country::tableName() . '.id',
                'country_name' => Country::tableName() . '.name',
                'users_count' => new Expression('count(DISTINCT ' . User::tableName() . '.id)'),
                'users_ids' => new Expression('GROUP_CONCAT(DISTINCT ' . User::tableName() . '.id ORDER BY ' . User::tableName() . '.id ASC SEPARATOR \',\') ')
            ])
            ->leftJoin(UserCountry::tableName(), [UserCountry::tableName() . '.country_id' => new Expression(Country::tableName() . '.id')])
            ->leftJoin(User::tableName(), [User::tableName() . '.id' => new Expression(UserCountry::tableName() . '.user_id')])
            ->leftJoin(AuthAssignment::tableName(), [AuthAssignment::tableName() . '.user_id' => new Expression(User::tableName() . '.id')])
            ->where([AuthAssignment::tableName() . '.item_name' => 'country.curator'])
            ->andWhere([User::tableName() . '.status' => User::STATUS_DEFAULT])
            ->groupBy([Country::tableName() . '.id']);

        $query->andFilterWhere(['in', Country::tableName() . '.id', $countries_ids]);

        return $query->asArray()->all();
    }

}
