<?php

namespace app\modules\widget\widgets\charts;

use app\models\User;
use app\modules\order\models\Order;
use app\modules\widget\models\WidgetCache;
use app\modules\widget\models\WidgetUser;
use app\modules\widget\widgets\CacheableWidget;
use Yii;
use yii\helpers\ArrayHelper;
use app\modules\order\models\OrderStatus;

/**
 * Class LeadsByWeek
 * @package app\modules\widget\widgets\charts
 */
class LeadsByWeek extends CacheableWidget
{
    const PERIOD = 13 * 7; // 13 weeks ~ 90 days

    /**
     * @var string
     */
    public $refreshDataType = 'array';

    /**
     * @var string
     */

    public $refreshScenario = 'chart';

    /**
     * @var WidgetUser
     */
    public $model;

    /**
     * @return array;
     */

    private function getData()
    {
        $params = $this->getParams();

        $statuses = OrderStatus::getAllList();
        $leadCount = 'COUNT(`order`.status_id IN(' .implode(', ', $statuses) .') OR NULL)';
        $dateCondition = 'DATE_FORMAT(FROM_UNIXTIME(' . Order::tableName() . '.created_at), "%u.%Y")';

        $weekday = date('N');
        $date = date('Y-m-d 00:00:00', strtotime('-' . ($params['days'] + $weekday - 1) . ' days'));
        $time = Yii::$app->formatter->asTimestamp($date);
        if (!is_null($this->timeOffset)) {
            $time += $this->timeOffset;
        }
        $data = Order::find()
            ->select([
                'date' => $dateCondition,
                'leads' => $leadCount,
            ])
            ->where(['>=', Order::tableName() . '.created_at', $time])
            ->andWhere(["IS NOT", Order::tableName() .".foreign_id", NULL])
            ->andWhere(['IN', Order::tableName() . '.country_id', $params['countries']])
            ->andWhere(['NOT IN', Order::tableName() .'.source_id', User::getRejectedSources($this->model->user_id)])
            ->groupBy(['date'])
            ->orderBy([Order::tableName() . '.created_at' => SORT_ASC])
            ->asArray()
            ->all();

        $date = date('Y-m-d 00:00:00', strtotime('-' . ($weekday - 1) . ' days'));
        $time = Yii::$app->formatter->asTimestamp($date);
        if (!is_null($this->timeOffset)) {
            $time += $this->timeOffset;
        }
        $leads = Order::find()
            ->select([
                'forecast_leads' => $leadCount
            ])
            ->where(['>=', Order::tableName() .'.created_at', $time])
            ->andWhere(["IS NOT", Order::tableName() .".foreign_id", NULL])
            ->andWhere(['IN', Order::tableName() . '.country_id', $params['countries']])
            ->andWhere(['NOT IN', Order::tableName() .'.source_id', User::getRejectedSources($this->model->user_id)])
            ->asArray()
            ->one();

        return [
            'data' => $data,
            'forecast' => $leads ? ($leads['forecast_leads'] * 7 / $weekday) : 0,
        ];
    }

    /**
     * @param $value1
     * @param $value2
     * @return array
     */
    private function generatePoint($value1, $value2)
    {
        $diff = ($value1) ? ($value1 / $value2 * 100) - 100 : 0;
        if ($diff < 0) {
            $sign = '';
            $pColor = '#FF0000';
        } else {
            $sign = '+';
            $pColor = '#228B22';
        }

        return [
            'y' => intval($value1),
            'sign' => $sign,
            'percentage' => round($diff, 2),
            'pColor' => $pColor
        ];
    }

    /**
     * @param array $dates
     * @return array
     */
    private function formattedDates($dates)
    {
        foreach ($dates as &$date) {
            list($week, $year) = explode('.', $date);
            if ($week == 0) {
                $time = strtotime($year . '-01-01 00:00:00');
            } else {
                $timestamp = mktime(0, 0, 0, 1, 1, $year) + ($week * 7 * 86400);
                $time = $timestamp - 86400 * (date('N', $timestamp) - 1);
            }
            $date = Yii::$app->formatter->asDate($time);
        } unset($date);

        return $dates;
    }


    public function prepareData($data)
    {
        $options = [
            'title' => ['text' => ' '],
            'credits' => [
                'enabled' => false
            ],
        ];

        if (!empty($data) && !empty($data['data'])) {
            $series = [];

            $leads = ArrayHelper::getColumn($data['data'], 'leads');
            $dates = ArrayHelper::getColumn($data['data'], 'date');
            $last = count($leads) - 1;
            if ($last == 0) {
                $leadsData = $leads;
            } else {
                $dates = array_splice($dates, 1);

                $leadsData = [];
                for ($i = 1; $i <= $last; $i++) {
                    $leadsData[] = $this->generatePoint($leads[$i], $leads[$i - 1]);
                }

                $forecastData = ($last > 1) ? array_fill(0, $last, null) : [];
                $forecastData[$last - 2] = intval($leads[$last - 1]);
                $forecastData[$last - 1] = $this->generatePoint($data['forecast'], $leads[$last - 1]);

                $series[] = [
                    'showInLegend' => false,
                    'data' => $forecastData,
                    'name' => Yii::t('common', 'Прогноз лидов'),
                    'color' => '#FF0000',
                    'dashStyle' => 'shortdot',
                    'tooltip' => [
                        'pointFormat' => '<span>{series.name}</span>: <b>{point.y}</b> <span style="color:{point.pColor}">({point.sign}{point.percentage:.1f}%)</span><br/>'
                    ],
                ];
            }

            $series[] = [
                'showInLegend' => false,
                'data' => $leadsData,
                'name' => Yii::t('common', 'Лиды'),
                'color' => '#7CB5EC',
                'tooltip' => [
                    'pointFormat' => '<span>{series.name}</span>: <b>{point.y}</b> <span style="color:{point.pColor}"> ({point.sign}{point.percentage:.1f}%)</span><br/>'
                ],
            ];

            $options += [
                'xAxis' => [
                    'categories' => $this->formattedDates($dates),
                ],
                'yAxis' => [
                    'title' => ['text' => 'Лиды'],
                ],
                'series' => $series,
            ];
        }

        return ['options' => $options];
    }

    /**
     * @return array
     */
    public function getCachedData()
    {
        $data = $this->data ?: WidgetCache::readCachedData($this->model->id, $this->model->type->by_country ? Yii::$app->user->country->id : null);
        if (!$data && $this->onlineLoad) {
            $data = $this->getData();
        }

        return $data;
    }

    /**
     * @param bool $cache
     * @return array
     */
    public function getRefreshData($cache = true)
    {
        $data = ($cache) ? $this->getCachedData() : $this->getData();

        return $this->prepareData($data);
    }

    /**
     * @return array
     */
    protected function getParams()
    {
        return [
            'countries' => $this->countryIds ?: array_keys(User::getAllowCountries()),
            'days' => static::PERIOD,
        ];
    }

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('leads-by-week', [
            'model' => $this->model,
            'options' => $this->getRefreshData(),
        ]);
    }

    /**
     * @return array
     */
    public function runData()
    {
        return $this->getData();
    }
}