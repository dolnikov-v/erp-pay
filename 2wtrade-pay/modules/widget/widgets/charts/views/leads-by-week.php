<?php

use miloschuman\highcharts\Highcharts;

/**
 * @var array $options
 */
?>

<?= Highcharts::widget($options);
