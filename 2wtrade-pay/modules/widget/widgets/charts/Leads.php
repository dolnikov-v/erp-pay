<?php

namespace app\modules\widget\widgets\charts;

use app\helpers\i18n\Formatter;
use app\models\User;
use app\modules\order\models\Order;
use app\modules\widget\models\WidgetCache;
use app\modules\widget\models\WidgetUser;
use app\modules\widget\widgets\CacheableWidget;
use Yii;
use yii\helpers\ArrayHelper;
use app\modules\order\models\OrderStatus;

/**
 * Class Leads
 * @package app\modules\widget\widgets\charts
 */
class Leads extends CacheableWidget
{
    const PERIOD = 3;

    /**
     * @var string
     */
    public $refreshDataType = 'array';

    /**
     * @var string
     */

    public $refreshScenario = 'chart';

    /**
     * @var WidgetUser
     */
    public $model;

    /**
     * @return array;
     */

    private function getData()
    {
        $params = $this->getParams();

        $statuses = OrderStatus::getAllList();
        $leadCount = 'COUNT(`order`.status_id IN(' .implode(",", $statuses) .') OR NULL)';
        $dateCondition = 'DATE_FORMAT(FROM_UNIXTIME(' . Order::tableName() . '.created_at), "' . Formatter::getMysqlMonthFormat() . '")';

        $date = date('Y-m-01 00:00:00', strtotime('-' . $params['month'] . ' month'));
        $time = Yii::$app->formatter->asTimestamp($date);
        if (!is_null($this->timeOffset)) {
            $time += $this->timeOffset;
        }

        $data = Order::find()
            ->select([
                'date' => $dateCondition,
                'leads' => $leadCount,
            ])
            ->where(['>=', Order::tableName() . '.created_at', $time])
            ->andWhere(["IS NOT", Order::tableName() .".foreign_id", NULL])
            ->andWhere(['IN', Order::tableName() . '.country_id', $params['countries']])
            ->andWhere(['NOT IN', Order::tableName() .'.source_id', User::getRejectedSources($this->model->user_id)])
            ->groupBy(['date'])
            ->orderBy([Order::tableName() . '.created_at' => SORT_ASC])
            ->asArray()
            ->all();

        $leads = Order::find()
            ->select([
                'forecast_leads' => $leadCount
            ])
            ->where(['>=', Order::tableName() .'.created_at', (time() - 60*60*24*7)])
            ->andWhere(["IS NOT", Order::tableName() .".foreign_id", NULL])
            ->andWhere(['IN', Order::tableName() . '.country_id', $params['countries']])
            ->andWhere(['NOT IN', Order::tableName() .'.source_id', User::getRejectedSources($this->model->user_id)])
            ->asArray()
            ->all();

        $days = intval(date("t"));
        $forecast = intval((intval($leads[0]['forecast_leads']) / 7) * $days);

        return $data = Array('data' => $data, 'forecast' => $forecast);
    }

    public function prepareData($data)
    {
        if (!empty($data)) {
            $leadsData = $data['data'];
            $forecastData = $data['forecast'];

            $dates = ArrayHelper::getColumn($leadsData, 'date');
            $values = [];
            $count = 0;
            foreach ($leadsData as $item) {
                $diff = 0;
                $sign = '+';
                $pColor = '#228B22';
                if ($count != 0) {
                    $diff = ($item['leads'] / $leadsData[$count - 1]['leads'] * 100) - 100;
                    if ($diff < 0) {
                        $sign = '';
                        $pColor = '#FF0000';
                    }
                }
                $values[] = Array('y' => intval($item['leads']), 'sign' => $sign, 'percentage' => $diff, 'pColor' => $pColor); // формируем кастомный tooltip для лидов
                $count++;
            }
            if (count($dates) > 3) {
                unset($values[0]);
                unset($dates[0]);
                $values = array_values($values);
                $dates = array_values($dates);
            }

            $values_forecast[] = intval($values[0]['y']);
            $values_forecast[] = intval($values[1]['y']);
            $diff = 0;
            if ($values[1] != 0) {
                $diff = ($forecastData / $values[1]['y'] * 100) - 100;
            }
            $sign = '+';
            $pColor = '#228B22';
            if ($diff < 0) {
                $sign = '';
                $pColor = '#FF0000';
            }
            $values_forecast[] = Array('y' => $forecastData, 'sign' => $sign, 'percentage' => $diff, 'pColor' => $pColor); // формируем кастомный tooltip для прогноза

            $series = [
                [
                    'showInLegend' => false,
                    'data' => $values_forecast,
                    'name' => Yii::t('common', 'Прогноз лидов'),        // Наименование точки в tooltip
                    'color' => '#FF0000',
                    'dashStyle' => 'shortdot',
                    'tooltip' => [
                        'pointFormat' => '<span>{series.name}</span>: <b>{point.y}</b> <span style="color:{point.pColor}">({point.sign}{point.percentage:.1f}%)</span><br/>'
                    ],
                ],
                [
                    'showInLegend' => false,
                    'data' => $values,
                    'name' => Yii::t('common', 'Лиды'),                  // Наименование точки в tooltip
                    'color' => '#7CB5EC',
                    'tooltip' => [
                        'pointFormat' => '<span>{series.name}</span>: <b>{point.y}</b> <span style="color:{point.pColor}"> ({point.sign}{point.percentage:.1f}%)</span><br/>'
                    ],
                ],
            ];

            return [
                'options' => [
                    'title' => ['text' => ' '],
                    'credits' => [
                        'enabled' => false
                    ],
                    'xAxis' => [
                        'categories' => $dates,
                    ],
                    'yAxis' => [
                        'title' => ['text' => 'Лиды'],
                    ],
                    'series' => $series,
                ],
            ];
        }

        return [
            'options' => [
                'title' => ['text' => ' '],
                'credits' => [
                    'enabled' => false
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function getCachedData()
    {
        $data = $this->data ?: WidgetCache::readCachedData($this->model->id, $this->model->type->by_country ? Yii::$app->user->country->id : null);
        if (!$data && $this->onlineLoad) {
            $data = $this->getData();
        }

        return $data;
    }

    /**
     * @param bool $cache
     * @return array
     */
    public function getRefreshData($cache = true)
    {
        $data = ($cache) ? $this->getCachedData() : $this->getData();

        return $this->prepareData($data);
    }

    /**
     * @return array
     */
    protected function getParams()
    {
        return [
            'countries' => $this->countryIds ?: array_keys(User::getAllowCountries()),
            'month' => static::PERIOD,
        ];
    }

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('leads', [
            'model' => $this->model,
            'options' => $this->getRefreshData(),
        ]);
    }

    /**
     * @return array
     */
    public function runData()
    {
        return $this->getData();
    }
}