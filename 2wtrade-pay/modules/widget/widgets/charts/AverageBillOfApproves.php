<?php

namespace app\modules\widget\widgets\charts;

use app\helpers\i18n\Formatter;
use app\models\CurrencyRate;
use app\models\User;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\order\models\Order;
use app\modules\report\components\ReportFormCallCenter;
use app\modules\widget\models\WidgetCache;
use app\modules\widget\models\WidgetUser;
use app\modules\widget\widgets\CacheableWidget;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class AverageBillOfApproves
 * @package app\modules\widget\widgets\charts
 */
class AverageBillOfApproves extends CacheableWidget
{
    const PERIOD = 2;

    /**
     * @var string
     */
    public $refreshDataType = 'array';

    /**
     * @var string
     */

    public $refreshScenario = 'chart';

    /**
     * @var WidgetUser
     */
    public $model;

    /**
     * @return array;
     */

    private function getData()
    {
        $params = $this->getParams();

        $mapStatuses = (new ReportFormCallCenter())->getMapStatuses();
        $allStatuses = [];
        foreach ($mapStatuses as $name => $statuses) {
            $allStatuses = array_merge($allStatuses, $statuses);
        }
        $statuses = $mapStatuses[Yii::t('common', 'Подтверждено')];

        $totalPrice = '(' . Order::tableName() . '.price_total + ' . Order::tableName() . '.delivery) / ' .CurrencyRate::tableName(). '.rate';
        $avgCondition =  'avg(if(' . Order::tableName() . '.status_id in (' . join(',', $statuses) . '), ' . $totalPrice . ', NULL))';
        $dateCondition = 'DATE_FORMAT(FROM_UNIXTIME(' . CallCenterRequest::tableName() . '.approved_at), "' . Formatter::getMysqlMonthFormat() . '")';

        $date = date('Y-m-01 00:00:00', strtotime('-' . $params['month'] . ' month'));
        $time = Yii::$app->formatter->asTimestamp($date);
        if (!is_null($this->timeOffset)) {
            $time += $this->timeOffset;
        }

        $data = Order::find()
            ->joinWith(['country', 'country.currencyRate', 'callCenterRequest'], false)
            ->select([
                'date' => $dateCondition,
                'avg' => $avgCondition,
            ])
            ->andWhere([Order::tableName() . '.country_id' => $params['countries']])
            ->andWhere(['>=', CallCenterRequest::tableName() . '.approved_at', $time])
            ->andWhere(['NOT IN', Order::tableName() .'.source_id', User::getRejectedSources($this->model->user_id)])
            ->groupBy(['date'])
            ->orderBy([CallCenterRequest::tableName() . '.approved_at' => SORT_ASC])
            ->asArray()
            ->all();

        return $data;
    }

    public function prepareData($data)
    {
        $dates = ArrayHelper::getColumn($data, 'date');
        $values = [];
        foreach ($data as $item) {
            $values[] = round($item['avg'], 2);
        }
        $values = [
            [
                'showInLegend' => false,
                'data' => $values,
                'name' => Yii::t('common', 'Средний чек'),
            ],
        ];

        return [
            'options' => [
                'title' => ['text' => ' '],
                'credits' => [
                    'enabled'=> false
                ],
                'xAxis' => [
                    'categories' => $dates,
                ],
                'yAxis' => [
                    'title' => ['text' => Yii::t('common', 'Средний чек')],
                ],
                'series' => $values,
            ],
        ];
    }

    /**
     * @return array
     */
    public function getCachedData()
    {
        $data = $this->data ?: WidgetCache::readCachedData($this->model->id, $this->model->type->by_country ? Yii::$app->user->country->id : null);
        if (!$data && $this->onlineLoad) {
            $data = $this->getData();
        }

        return $data;
    }

    /**
     * @param bool $cache
     * @return array
     */
    public function getRefreshData($cache = true)
    {
        $data = ($cache) ? $this->getCachedData() : $this->getData();

        return $this->prepareData($data);
    }

    /**
     * @return array
     */
    protected function getParams()
    {
        return [
            'countries' => $this->countryIds ?: array_keys(User::getAllowCountries()),
            'month' => static::PERIOD,
        ];
    }

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('average-bill-of-approves', [
            'model' => $this->model,
            'options' => $this->getRefreshData(),
        ]);
    }

    /**
     * @return array
     */
    public function runData()
    {
        return $this->getData();
    }
}