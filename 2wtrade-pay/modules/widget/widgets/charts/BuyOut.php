<?php

namespace app\modules\widget\widgets\charts;

use app\models\User;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use app\modules\widget\models\WidgetCache;
use app\modules\widget\models\WidgetUser;
use app\modules\widget\widgets\CacheableWidget;
use app\modules\callcenter\models\CallCenterRequest;
use Yii;
use yii\db\Expression;

/**
 * Class BuyOut
 * @package app\modules\widget\widgets\игнщге
 */
class BuyOut extends CacheableWidget
{
    /**
     * 30 дней назад
     * @var  int timestamp
     */
    public $dayBefore30;

    /**
     * 15 дней назад
     * @var  int timestamp
     */
    public $dayBefore15;

    /**
     * первое число пред предпоследнего месяца
     * @var int timestamp
     */
    public $preemptiveStart;

    /**
     * последнее число пред предпоследнего месяца
     * @var int timestamp
     */
    public $preemptiveEnd;

    /**
     * 15 число предпоследнего месяца
     * @var int timestamp
     */
    public $previous15day;

    /**
     * последнее число предпоследнего месяца
     * @var int timestamp
     */
    public $previousEnd;

    /**
     * @var []
     */
    public $statuses;

    /**
     * @var []
     */
    public $params;

    /**
     * @var string
     */
    public $refreshDataType = 'array';

    /**
     * @var string
     */

    public $refreshScenario = 'chart';

    /**
     * @var WidgetUser
     */
    public $model;

    /**
     * @var []
     */
    public $mapStatuses;

    /**
     * @return array
     */
    public function getMapStatuses()
    {
        if (empty($this->mapStatuses)) {
            $this->mapStatuses = [
                Yii::t('common', 'Выкуплено') => OrderStatus::getBuyoutList(),
                Yii::t('common', 'Подтверждено') => OrderStatus::getApproveList(),
                Yii::t('common', 'Все') => OrderStatus::getAllList(),
            ];
        }

        return $this->mapStatuses;
    }

    public function getMonthInterval()
    {
        return [
            date("m.Y", mktime(0, 0, 0, date("n") - 2, 1, date("Y"))),
            date("m.Y", mktime(0, 0, 0, date("n") - 1, 1, date("Y"))),
            date('m.Y')
        ];
    }

    /**
     * Метод определения временных диапозонов
     */
    public function calculateDates()
    {
        $this->dayBefore30 = time() - 3600 * 24 * 30;
        $this->dayBefore15 = time() - 3600 * 24 * 15;
        $this->preemptiveStart = strtotime(date("Y-m-2", strtotime("-2 month")));
        $this->preemptiveEnd = strtotime(date("Y-m-t", strtotime("-2 month")));
    }


    /**
     * Счиатаем ВВВ - % выкупа согласно задаче в интервале -30 и -15 дней
     * @return []
     */
    public function getBuyOut30_15days_beforeData()
    {
        $query = Order::find()
            ->select([
                'buyout' => new Expression("count(" . Order::tableName() . ".status_id in (" . implode(',', $this->statuses[Yii::t('common', 'Выкуплено')]) . ") or NULL)"),
                'approve' => new Expression("count(" . Order::tableName() . ".status_id in (" . implode(',', $this->statuses[Yii::t('common', 'Подтверждено')]) . ") or NULL)"),
            ])
            ->leftJoin(CallCenterRequest::tableName(), CallCenterRequest::tableName() . '.order_id=' . Order::tableName() . '.id')
            ->where(['in', Order::tableName() . '.status_id', $this->statuses[Yii::t('common', 'Все')]])
            ->andWhere(['>=', CallCenterRequest::tableName() . '.sent_at', $this->dayBefore30])
            ->andWhere(['<=', CallCenterRequest::tableName() . '.sent_at', $this->dayBefore15])
            ->andWhere(['in', Order::tableName() . '.country_id', $this->params['countries']])
            ->andWhere(['NOT IN', Order::tableName() .'.source_id', User::getRejectedSources($this->model->user_id)])
            ->asArray()
            ->all();

        return $query;
    }

    /**
     * Выкуп за 3 месяца
     * @return []
     */
    public function getBuyOutMonthData()
    {
        $query = Order::find()
            ->select([
                'buyout' => new Expression("count(" . Order::tableName() . ".status_id in (" . implode(',', $this->statuses[Yii::t('common', 'Выкуплено')]) . ") or NULL)"),
                'approve' => new Expression("count(" . Order::tableName() . ".status_id in (" . implode(',', $this->statuses[Yii::t('common', 'Подтверждено')]) . ") or NULL)"),
                'all' => new Expression("count(" . Order::tableName() . ".status_id in (" . implode(',', $this->statuses[Yii::t('common', 'Все')]) . ") or NULL)"),
                'groupbyname' => "DATE_FORMAT(FROM_UNIXTIME(" . CallCenterRequest::tableName() . ".sent_at), '%m.%Y')",
                'sorted' => "DATE_FORMAT(FROM_UNIXTIME(" . CallCenterRequest::tableName() . ".sent_at), '%Y%m%d')"
            ])
            ->leftJoin(CallCenterRequest::tableName(), CallCenterRequest::tableName() . '.order_id=' . Order::tableName() . '.id')
            ->where(['in', Order::tableName() . '.status_id', $this->statuses[Yii::t('common', 'Все')]])
            ->andWhere(['>=', CallCenterRequest::tableName() . '.sent_at', $this->preemptiveStart])
            ->andWhere(['in', Order::tableName() . '.country_id', $this->params['countries']])
            ->andWhere(['NOT IN', Order::tableName() .'.source_id', User::getRejectedSources($this->model->user_id)])
            ->groupBy(['groupbyname'])
            ->orderBy(['sorted' => SORT_ASC])
            ->asArray()
            ->all();

        return $query;
    }

    /**
     * Расчёт данных для графика
     * @param null $params
     * @return array
     */
    public function getData($params = null)
    {
        //Задать диапазоны дат
        $this->calculateDates();
        //получить параметры
        $this->params = $this->getParams();
        //разобрать статусы
        $this->statuses = $this->getMapStatuses();

        //% выкупа за период -30 и -15 дней
        $getBuyOut30_15days_beforeData = $this->getBuyOut30_15days_beforeData();

        if ($getBuyOut30_15days_beforeData[0]['approve'] == 0) {
            $getBuyOut30_15days_before = 0;
        } else {
            $getBuyOut30_15days_before = $getBuyOut30_15days_beforeData[0]['buyout'] * 100 / $getBuyOut30_15days_beforeData[0]['approve'];
        }

        //выкуп и заказы за 3 месяца
        $data = $this->getBuyOutMonthData();

        //Прогноз за предпоследний месяц
        if (isset($data[1]) && $data[1] > 0) {
            $percentBuyOut = $data[1]['buyout'] * 100 / $data[1]['approve'];
            $forecastPreviosMonth = max($percentBuyOut, $getBuyOut30_15days_before);
        } else {
            $forecastPreviosMonth = $getBuyOut30_15days_before;
        }

        $buyout = [];

        foreach ($data as $k => $v) {
            //нам нужно только 3 месяца
            if ($k <= 2) {
                if ($v['approve'] == 0) {
                    $buyout[] = 0;
                } else {
                    $buyout[] = $v['buyout'] * 100 / $v['approve'];
                }
            }
        }

        $forecast = [];

        if (!empty($buyout)) {
            $forecast[] = $buyout[0];
            $forecast = array_merge($forecast, [$forecastPreviosMonth, $getBuyOut30_15days_before]);
        }

        $buyout = array_map('ceil', $buyout);
        $forecast = array_map('ceil', $forecast);

        return [
            'data' => $buyout,
            'forecast' => $forecast
        ];

    }

    public function prepareData($data)
    {
        if (empty($data['data'])) {
            $data = [
                'data' => [0, 0, 0],
                'forecast' => [0, 0, 0]
            ];
        }

        $series = [
            [
                'showInLegend' => false,
                'data' => $data['forecast'],
                'name' => Yii::t('common', 'Прогноз выкупа'),
                'color' => '#FF0000',
                'dashStyle' => 'shortdot',
                'tooltip' => [
                    'pointFormat' => '<span>{series.name}</span>: <b>{point.y}</b> <span style="color:{point.pColor}">{point.sign}%</span><br/>'
                ],
            ],
            [
                'showInLegend' => false,
                'data' => $data['data'],
                'name' => Yii::t('common', 'Выкуп'),
                'color' => '#7CB5EC',
                'tooltip' => [
                    'pointFormat' => '<span>{series.name}</span>: <b>{point.y}</b> <span style="color:{point.pColor}">{point.sign}%</span><br/>'
                ],
            ],
        ];

        return [
            'options' => [
                'title' => ['text' => ' '],
                'credits' => [
                    'enabled' => false
                ],
                'xAxis' => [
                    'categories' => $this->getMonthInterval(),
                ],
                'yAxis' => [
                    'title' => ['text' => 'Выкуп, %'],
                ],
                'series' => $series,
            ],
        ];
    }

    /**
     * @return array
     */
    public function getCachedData()
    {
        $data = $this->data ?: WidgetCache::readCachedData($this->model->id, $this->model->type->by_country ? Yii::$app->user->country->id : null);
        if (!$data && $this->onlineLoad) {
            $data = $this->getData();
        }

        return $data;
    }

    /**
     * @param bool $cache
     * @return array
     */
    public function getRefreshData($cache = true)
    {
        $data = ($cache) ? $this->getCachedData() : $this->getData();

        return $this->prepareData($data);
    }

    /**
     * @return array
     */
    protected function getParams()
    {
        return [
            'countries' => $this->countryIds ?: array_keys(User::getAllowCountries())
        ];
    }

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('percent-of-approves', [
            'model' => $this->model,
            'options' => $this->getRefreshData(),
        ]);
    }

    /**
     * @return array
     */
    public function runData()
    {
        return $this->getData();
    }
}