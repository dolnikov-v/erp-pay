<?php

namespace app\modules\widget\widgets\certificates;

use app\models\Certificate;
use app\models\search\CertificateSearch;
use app\modules\widget\models\WidgetCache;
use app\modules\widget\models\WidgetUser;
use app\modules\widget\widgets\CacheableWidget;
use yii\data\ArrayDataProvider;
use yii;
use yii\helpers\ArrayHelper;

/**
 * Class Certificates
 * @package app\modules\widget\widgets\Certificates
 */
class Certificates extends CacheableWidget
{
    /**
     * @var WidgetUser
     */
    public $model;

    /**
     * @var bool
     */
    public $export = true;

    /**
     * @var Certificate
     */
    private $form;

    /**
     * @var []
     * Объеденнённые данные по 2м запросам по сертификации товаров
     */
    public $products_status_certificate;

    /**
     * @var []
     * Подготовленные данные по группам сертификации
     */
    public $combineProductsList;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->form = new Certificate();

        parent::init();
    }

    /**
     * @param array $params
     * certificate - товаров с данными по сертификатам в таблице Certificate::tableName
     * order - поиз товаров в заказах без данных по сертификации
     */
    private function findAll($params)
    {
        $country_id = $params['country_id'];

        //Все товары с сертификатами по данной стране
        $certificed_products_object = CertificateSearch::getCertifiedProducts($country_id);
        $certificed_products = ArrayHelper::toArray($certificed_products_object);

        //Товары в заказах, не имеющие данных о сертификате
        $products_no_certificated_in_orders_object = CertificateSearch::getNoCertifiedProducts($country_id);
        $products_no_certificated_in_orders = ArrayHelper::toArray($products_no_certificated_in_orders_object);

        $this->products_status_certificate = array_merge($certificed_products, $products_no_certificated_in_orders);

        return $this;
    }


    /**
     * Метод постобработки списка товаров для разбора их по группам сертификации
     * собирает многомерный массив с 4мя ключами
     * 0 - не требуют сертификации
     * 1 - есть сертификаты (наши)
     * 2 - есть сертификаты (партнёрский)
     * 3 - нет сертификатов
     */
    public function combineListProducts()
    {
        $combineProductsList = [
            0 => [], 1 => [], 2 => [], 3 => []
        ];

        foreach ($this->products_status_certificate as $k => $product) {
            //товары у которых есть сведения о сертификатах в таблице Certificate::tableName()
            if (isset($product['is_partner'])) {
                if ($product['certificate_not_needed'] == 1) {
                    $combineProductsList[0]['products'][] = $product['name'];
                } else {
                    if ($product['is_partner'] == 0) {
                        $combineProductsList[1]['products'][] = $product['name'];
                    } else if ($product['is_partner'] == 1) {
                        $combineProductsList[2]['products'][] = $product['name'];
                    }
                }
            } else {
                //товары из Order::tableName() у которых нет сертификатов в таблице Certificate::tableName()
                $combineProductsList[3]['products'][] = $product['name'];
            }

            //если нет товаров по какому-либо пункту - строку не отображать
            foreach ($combineProductsList as $v) {
                if (empty($v['products'])) {
                    unset($combineProductsList[$k]);
                }
            }
        }

        //если нет товаров по какому-либо пункту - строку не отображать
        foreach ($combineProductsList as $k => $v) {
            if (empty($v['products'])) {
                unset($combineProductsList[$k]);
            }
        }

        $this->combineProductsList = $combineProductsList;
    }

    /**
     * @return []
     * метод собирает в данные, необходимого вида для виджета
     */
    public function combineArrayDataProvider()
    {
        $typeGroup = [
            0 => yii::t('common', 'Не требует сертификации'),
            1 => yii::t('common', 'Есть сертификаты (наши)'),
            2 => yii::t('common', 'Есть сертификаты (партнёрские)'),
            3 => yii::t('common', 'Нет сертификатов')

        ];

        $combinedData = [];

        foreach ($this->combineProductsList as $groupCertification => $products) {
            $listProducts = ArrayHelper::getValue($products, 'products');
            $combinedData[] = [
                'type' => $groupCertification,
                'group' => $typeGroup[$groupCertification],
                'products' => is_array($listProducts) ? implode(', ', $listProducts) : ''
            ];
        }

        return $combinedData;
    }

    /**
     * @return string
     */
    public function run()
    {

        $data = $this->data ?: WidgetCache::readCachedData($this->model->id, $this->model->type->by_country ? yii::$app->user->country->id : null);
        if (!$data && $this->onlineLoad) {
            $this->findAll(['country_id' => yii::$app->user->country->id])->combineListProducts();
            $data = $this->combineArrayDataProvider();
        }


        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'pagination' => false,
        ]);


        return $this->render('certificates', [
            'dataProvider' => $dataProvider,
            'model' => $this->model
        ]);
    }

    public function runData()
    {
        $params = [
            'country_id' => $this->countryId
        ];
        $this->findAll($params)
            ->combineListProducts();

        return $this->combineArrayDataProvider();

    }
}