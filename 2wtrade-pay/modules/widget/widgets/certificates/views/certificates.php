<?php

use app\modules\widget\extensions\GridView;
use app\widgets\Label;
use yii\helpers\Html;

/**
 * @var \yii\data\ArrayDataProvider $dataProvider
 */

?>

<?= GridView::widget([
    'id' => 'widget-user-' . $model->id . '-grid',
    'exportFilename' => Yii::t('common', $model->type->name),
    'dataProvider' => $dataProvider,
    'tableOptions' => ['class' => 'table table-bordered'],
    'rowOptions' => function ($model, $key, $index, $column) {
        $class = 'tr-vertical-align-middle text-center  ';
        switch ($model['type']) {
            case 0:
            case 1:
                $bgcolor = '#DDFADE';
                break;
            case 2:
                $bgcolor = '#FFFFDD';
                break;
            case 3:
                $bgcolor = '#FFE7E7';
                break;
            default :
                $bgcolor = 'white';
                break;
        }

        return ['style' => 'background-color:' . $bgcolor];
    },
    'showHeader' => true,
    'columns' => [
        [
            'header' => Yii::t('common', 'Статус сертификации'),
            'headerOptions' => ['style' => 'text-align:center; background-color: #F0F0F0', 'class' => 'width-250'],
            'attribute' => 'group',
            'contentOptions' => ['style' => 'background-color:#F9F9F9']
        ],
        [
            'header' => Yii::t('common', 'Список товаров'),
            'headerOptions' => ['style' => 'text-align:center; background-color: #F0F0F0'],
            'attribute' => 'products',
            'value' => function ($data) {
                return $data['products'];
            },
        ],
    ]
]); ?>