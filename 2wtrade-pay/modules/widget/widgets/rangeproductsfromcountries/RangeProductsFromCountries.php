<?php

namespace app\modules\widget\widgets\rangeproductsfromcountries;

use app\models\Product;
use app\models\Country;
use app\modules\storage\models\Storage;
use app\modules\storage\models\StorageProduct;
use app\modules\order\models\Order;
use app\modules\order\models\OrderProduct;
use app\modules\widget\models\WidgetCache;
use app\modules\widget\models\WidgetUser;
use app\modules\widget\widgets\CacheableWidget;
use yii\data\ArrayDataProvider;
use app\models\User;
use yii;
use yii\db\Expression;


/**
 * Class RangeProductsFromCountries
 * @package app\modules\widget\widgets\rangeproductsfromcountries
 * Виджет ассортимент товаров по странам
 */
class RangeProductsFromCountries extends CacheableWidget
{
    /**
     * @var WidgetUser
     */
    public $model;

    /**
     * @var bool
     */
    public $export = true;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @param $params []
     * @return []
     */
    private function findAll($params)
    {
        $countries_ids = $params['countries_ids'];

        $query = StorageProduct::find()
            ->select([
                'country_id' => Country::tableName() . '.id',
                'country_name' => Country::tableName() . '.name',
                'count' => new Expression('count(distinct ' . Product::tableName() . '.id)'),
                'products_names' => new Expression('GROUP_CONCAT(DISTINCT ' . Product::tableName() . '.`name` ORDER BY ' . Product::tableName() . '.`name` ASC SEPARATOR \', \')')
            ])
            ->leftJoin(Storage::tableName(), [Storage::tableName() . '.id' => new Expression(StorageProduct::tableName() . '.storage_id')])
            ->leftJoin(Country::tableName(), [Country::tableName() . '.id' => new Expression(Storage::tableName() . '.country_id')])
            ->leftJoin(Product::tableName(), [Product::tableName() . '.id' => new Expression(StorageProduct::tableName() . '.product_id')])
            ->leftJoin(OrderProduct::tableName(), [OrderProduct::tableName() . '.product_id' => new Expression(Product::tableName() . '.id')])
            ->leftJoin(Order::tableName(), [Order::tableName() . '.id' => new Expression(OrderProduct::tableName() . '.order_id')])
            ->where([Country::tableName() . '.id' => $countries_ids])
            ->andWhere(['>', Order::tableName() . '.status_id', 12])
            ->andWhere(['not in', Order::tableName() . '.status_id', [25,26]])
            ->andWhere(['>=', Order::tableName() . '.created_at', new Expression('UNIX_TIMESTAMP(CURDATE() - INTERVAL ' . self::PERIOD . ' DAY)')])
            ->andWhere(['NOT IN', Order::tableName() .'.source_id', User::getRejectedSources($this->model->user_id)])
            ->orderBy(['count' => SORT_DESC])
            ->groupBy([Storage::tableName() . '.country_id']);
        return $query->asArray()->all();
    }

    /**
     * @return string
     */
    public function run()
    {

        $data = $this->data ?: WidgetCache::readCachedData($this->model->id, $this->model->type->by_country ? yii::$app->user->country->id : null);
        if (!$data && $this->onlineLoad) {
            $data = $this->findAll(['countries_ids' => array_keys(User::getAllowCountries())]);
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'pagination' => false
        ]);


        return $this->render('range-products-from-countries', [
            'dataProvider' => $dataProvider,
            'model' => $this->model
        ]);

    }

    public function runData()
    {
        return $this->findAll(['countries_ids' => $this->countryIds]);
    }
}