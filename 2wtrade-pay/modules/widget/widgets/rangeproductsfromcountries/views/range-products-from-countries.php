<?php

use app\modules\widget\extensions\GridView;
use yii\helpers\Html;

/**
 * @var \yii\data\ArrayDataProvider $dataProvider
 */

?>

<?= GridView::widget([
    'id' => 'widget-user-' . $model->id . '-grid',
    'exportFilename' => Yii::t('common', $model->type->name),
    'dataProvider' => $dataProvider,
    'tableOptions' => ['class' => 'table table-bordered'],
    'rowOptions' => function ($model, $key, $index, $column) {
        $class = 'tr-vertical-align-middle';
        $bgcolor = 'white';

        return ['class' => $class, 'style' => 'background-color:' . $bgcolor];
    },
    'showHeader' => true,
    'columns' => [
        [
            'header' => Yii::t('common', '№'),
            'headerOptions' => ['style' => 'text-align:center;', 'class' => 'width-20'],
            'attribute' => 'country_name',
            'value' => function ($data, $row, $column) {
                return ++$row . '.';
            }
        ],
        [
            'header' => Yii::t('common', 'Страна'),
            'headerOptions' => ['style' => 'text-align:center;', 'class' => 'width-150'],
            'contentOptions' => ['style' => 'text-align:left;'],
            'attribute' => 'country_name',
            'value' => function ($model) {
                return Yii::t('common', $model['country_name']);
            },
        ],
        [
            'header' => Yii::t('common', 'Количество'),
            'headerOptions' => ['style' => 'text-align:center;', 'class' => 'width-50'],
            'contentOptions' => ['style' => 'text-align:center;'],
            'attribute' => 'count'
        ],
        [
            'header' => Yii::t('common', 'Товары'),
            'headerOptions' => ['style' => 'text-align:center;', 'class' => 'width-350'],
            'contentOptions' => ['style' => 'text-align:left;'],
            'attribute' => 'products_names'
        ],
    ]
]); ?>

<small>* Товары попадают в виджет, если за последние 31 день хотя бы один заказ с этим товаром был отправлен в курьерскую службу</small>
