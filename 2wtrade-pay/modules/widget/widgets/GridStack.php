<?php

namespace app\modules\widget\widgets;

use yii\base\Widget;

/**
 * Class GridStack
 * @package app\modules\widget\widgets
 */

class GridStack extends Widget
{

    /**
     * @var
     */
    public $widgets;

    /**
     * @var
     */
    public $widgetTypes;


    /**
     * @return string
     */
    public function run()
    {
        return $this->render('grid-stack', [
            'widgets' => $this->widgets,
            'widgetTypes' => $this->widgetTypes,
        ]);
    }

}