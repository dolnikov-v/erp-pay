<?php

use app\modules\widget\extensions\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var \yii\data\ArrayDataProvider $dataProvider
 */

?>

<?= GridView::widget([
    'id' => 'widget-user-' . $model->id . '-grid',
    'exportFilename' => Yii::t('common', $model->type->name),
    'dataProvider' => $dataProvider,
    'tableOptions' => ['class' => 'table table-bordered'],
    'rowOptions' => function ($model, $key, $index, $column) {
        $class = 'tr-vertical-align-middle text-center  ';

        if ($model['hours'] > 0 && $model['hours'] <= 24) {
            $bgcolor = '#DDFADE';
        } else if ($model['hours'] > 24) {
            $bgcolor = '#FFE7E7';
        } else {
            $bgcolor = 'white';
        }

        return ['style' => 'background-color:' . $bgcolor];
    },
    'showHeader' => true,
    'columns' => [
        [
            'header' => Yii::t('common', 'Страна/Курьерка'),
            'headerOptions' => ['style' => 'text-align:center;', 'class' => 'width-250'],
            'attribute' => 'fullname',
            'value' => function ($model) {
                return Yii::t('common', $model['country_name']).'/'.Yii::t('common', $model['delivery_name']);
            },
        ],
        [
            'header' => Yii::t('common', 'Средний срок отправки'),
            'headerOptions' => ['style' => 'text-align:center;', 'class' => 'width-150'],
            'contentOptions' => ['style' => 'text-align:center;'],
            'attribute' => 'hours',
            'format' => 'raw',
            'value' => function ($model) use ($period) {
                $url = Url::to([
                    'force_country_slug' => $model['force_country_slug'],
                    '/order/index/index',
                    'DateFilter[dateType]' => 'created_at',
                    'DateFilter[dateFrom]' => Yii::$app->formatter->asDate(strtotime("now") - (3600 * 24 * $period), 'MM/dd/yyyy'),
                    'DateFilter[dateTo]' => Yii::$app->formatter->asDate(strtotime("now"), 'MM/dd/yyyy'),
                    'DeliveryFilter[not][]' => 0,
                    'DeliveryFilter[delivery][]' => $model['delivery_id'],
                    'TermsOfShippingFilter[not][]' => '',
                    'TermsOfShippingFilter[entity][]' => 3600,
                    'TermsOfShippingFilter[termsOfShipping][]' => 24
                ]);

                $content = Html::a($model['hours'], $url, ['target' => '_blank']);

                return $content;
            },
        ],
    ]
]); ?>
<?= '<small>' .Yii::t("common", "*Данные берутся за последние 7 дней по выбранной стране. Сроки указаны в часах. Норма отправки в курьерскую службу - 24 часа") .'</small><p>&nbsp;</p>' ?>