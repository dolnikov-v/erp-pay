<?php

namespace app\modules\order\jobs;


use app\jobs\BaseJob;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\report\models\InvoiceOrder;

/**
 * Class OrderCalculateDeliveryChargesJob
 * @package app\modules\order\jobs
 */
class OrderCalculateDeliveryChargesJob extends BaseJob
{
    /**
     * @var int
     */
    public $orderId;

    /**
     * @param \yii\queue\Queue $queue
     * @return mixed|void
     */
    public function execute($queue)
    {
        $deliveryRequest = DeliveryRequest::find()->joinWith(['order.financePrediction.invoiceOrder'], false)->where([
            DeliveryRequest::tableName() . '.order_id' => $this->orderId
        ])->andWhere(['is', InvoiceOrder::tableName() . '.invoice_id', null])->one();

        if (!$deliveryRequest) {
            return;
        }

        $lockName = 'order.calculatedeliverychargesjob.' . $this->orderId;
        $locked = $this->mutex->acquire($lockName, 15);
        try {
            if ($chargesCalculator = $deliveryRequest->chargesCalculator) {
                $finance = $chargesCalculator->calculate($deliveryRequest->order);
                if (!$finance->save()) {
                    throw new \Exception("Can't save order finance prediction: {$finance->getFirstErrorAsString()}.");
                }
            }
        } catch (\Throwable $e) {
            $this->logAddError($e->getMessage(), ['order_id' => $this->orderId, 'job' => __CLASS__]);
        } finally {
            if ($locked) {
                $this->mutex->release($lockName);
            }
        }
    }
}