<?php

namespace app\modules\order\jobs;


use app\jobs\BaseJob;
use app\models\Currency;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\deliveryreport\models\DeliveryReport;
use app\modules\order\models\Order;
use app\modules\order\models\OrderFinanceBalance;
use app\modules\order\models\OrderFinanceFact;
use app\modules\order\models\OrderFinancePrediction;
use app\modules\order\models\OrderStatus;

/**
 * Class OrderCalculateFinanceBalanceJob
 * @package app\modules\order\jobs
 */
class OrderCalculateFinanceBalanceJob extends BaseJob
{
    /**
     * @var integer
     */
    public $deliveryRequestId;

    /**
     * @var DeliveryRequest
     */
    protected $deliveryRequest;

    /**
     * @var OrderFinanceBalance
     */
    protected $financeBalance;

    /**
     * @param \yii\queue\Queue $queue
     * @return mixed|void
     */
    public function execute($queue)
    {
        $lockName = 'order.calculatefinancebalance' . $this->deliveryRequestId;
        $locked = $this->mutex->acquire($lockName, 15);
        try {
            if (($financeBalance = $this->getFinanceBalance()) && $this->updateOrderFinanceBalanceByPrediction($financeBalance)) {
                $this->updateOrderFinanceBalanceByFacts($financeBalance);
                if (!$financeBalance->save()) {
                    throw new \Exception("Can't save balance model: {$financeBalance->getFirstErrorAsString()}");
                }
            }
        } catch (\Throwable $e) {
            $this->logAddError($e->getMessage());
        } finally {
            if ($locked) {
                $this->mutex->release($lockName);
            }
        }
    }

    /**
     * @param OrderFinanceBalance $financeBalance
     * @throws \Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    protected function updateOrderFinanceBalanceByFacts(&$financeBalance)
    {
        if ($orderFinanceFacts = $this->getFacts()) {
            foreach ($orderFinanceFacts as $fact) {
                foreach (OrderFinanceBalance::getCurrencyFields() as $field => $currencyField) {
                    if (!$fact->hasAttribute($field) || !$fact->getAttribute($field)) {
                        continue;
                    }
                    $value = $fact->getAttribute($field);
                    $factCurrencyId = $fact->getAttribute($currencyField) ?? $financeBalance->order->country->currency_id;
                    $balanceCurrencyId = $financeBalance->getAttribute($currencyField) ?? $financeBalance->order->country->currency_id;

                    // Если факты в другой валюте, то переводим их в валюту калькулятора
                    if ($factCurrencyId != $balanceCurrencyId) {
                        if (($rateField = (OrderFinanceFact::currencyRateRelationMap()[$currencyField] ?? null)) && $rate = $fact->getAttribute($rateField)) {
                            $value /= $rate;
                            $value = Currency::find()->convertValueToCurrency($value, Currency::find()
                                ->where(['char_code' => 'USD'])
                                ->select('id')
                                ->scalar(), $balanceCurrencyId, $fact->payment->paid_at ?? null);
                        } else {
                            $value = Currency::find()
                                ->convertValueToCurrency($value, $factCurrencyId, $balanceCurrencyId, $fact->payment->paid_at ?? null);
                        }
                    }

                    $financeBalance->setAttribute($field, $financeBalance->getAttribute($field) - $value);
                }
            }
        }
    }

    /**
     * @param OrderFinanceBalance $financeBalance
     * @return bool
     * @throws \Exception
     */
    protected function updateOrderFinanceBalanceByPrediction(&$financeBalance)
    {
        if (!$prediction = $this->getPrediction()) {
            return false;
        }
        foreach (array_merge(array_keys(OrderFinanceBalance::getCurrencyFields()), array_values(OrderFinanceBalance::getCurrencyFields())) as $field) {
            if ($prediction->hasAttribute($field) && $financeBalance->hasAttribute($field)) {
                $financeBalance->setAttribute($field, $prediction->getAttribute($field));
            }
        }
        return true;
    }

    /**
     * @return OrderFinanceFact[]|array|\yii\db\ActiveRecord[]
     * @throws \Exception
     */
    protected function getFacts()
    {
        return OrderFinanceFact::find()
            ->joinWith('deliveryReport', false)
            ->where([OrderFinanceFact::tableName() . '.order_id' => $this->getDeliveryRequest()->order_id])
            ->andWhere([
                'OR',
                [DeliveryReport::tableName() . '.delivery_id' => $this->getDeliveryRequest()->delivery_id],
                ['is', DeliveryReport::tableName() . '.delivery_id', null]
            ])->all();
    }

    /**
     * @return OrderFinancePrediction|array|null|\yii\db\ActiveRecord
     * @throws \Exception
     */
    protected function getPrediction()
    {
        return OrderFinancePrediction::find()->where(['order_id' => $this->getDeliveryRequest()->order_id])->one();
    }

    /**
     * @return OrderFinanceBalance|array|null|\yii\db\ActiveRecord
     * @throws \Exception
     * @throws \Throwable
     */
    protected function getFinanceBalance()
    {
        if (!$this->financeBalance) {
            $this->financeBalance = OrderFinanceBalance::find()
                ->where(['delivery_request_id' => $this->deliveryRequestId])
                ->one();

            // Если отсутствует подходящий deliveryRequest, то удаляем баланс
            if (!$this->getDeliveryRequest(false)) {
                if ($this->financeBalance) {
                    $this->financeBalance->delete();
                }
                $this->financeBalance = null;
            } elseif (!$this->financeBalance) {
                $this->financeBalance = new OrderFinanceBalance([
                    'delivery_request_id' => $this->getDeliveryRequest()->id,
                    'order_id' => $this->getDeliveryRequest()->order_id
                ]);
            }
        }
        return $this->financeBalance;
    }

    /**
     * @param bool $throwException
     * @return DeliveryRequest
     * @throws \Exception
     */
    protected function getDeliveryRequest($throwException = true)
    {
        if (!$this->deliveryRequest) {
            $this->deliveryRequest = DeliveryRequest::find()
                ->joinWith('order', false)
                ->where([
                    DeliveryRequest::tableName() . '.id' => $this->deliveryRequestId,
                    Order::tableName() . '.status_id' => array_merge(OrderStatus::getBuyoutList(), OrderStatus::getOnlyNotBuyoutInDeliveryList())
                ])
                ->one();
            if (!$this->deliveryRequest && $throwException) {
                throw new \Exception("Delivery request with id={$this->deliveryRequestId} not found.");
            }
        }
        return $this->deliveryRequest;
    }
}