<?php

namespace app\modules\order\jobs;


use app\jobs\BaseJob;
use app\modules\order\models\OrderNotification;
use app\modules\order\models\OrderNotificationRequest;

/**
 * Class OrderEmailNotificationJob
 * @package app\modules\order\jobs
 */
class OrderEmailNotificationJob extends BaseJob
{
    /**
     * @var integer
     */
    public $requestId;

    /**
     * Вызов джобы
     * @param \yii\queue\Queue $queue
     * @return mixed
     */
    public function execute($queue)
    {
        $request = OrderNotificationRequest::find()->where([
            'id' => $this->requestId,
            'email_status' => OrderNotificationRequest::EMAIL_STATUS_PENDING,
            'status' => OrderNotificationRequest::SMS_STATUS_READY
        ])->one();
        if (empty($request) || empty($request->email_to)) {
            return null;
        }

        $answer = null;
        try {
            $request->email_notification_text = $request->orderNotification->getFilledEmailText($request->order);
            if ($request->orderNotification->answerable == OrderNotification::IS_ANSWERABLE || $request->orderNotification->issue_collector_id) {
                $request->email_notification_text .= ' ' . $request->getFeedbackLink();
            }
            $request->email_to = $this->prepareEmail($request->email_to);
            $mailer = $this->mailer
                ->compose()
                ->setFrom(\Yii::$app->params['noReplyEmail'])
                ->setTo($request->email_to)
                ->setSubject('2wtrade: changed order status')
                ->setTextBody($request->email_notification_text);

            if ($replyEmail = $request->order->country->order_notification_reply_email) {
                $mailer->setReplyTo($replyEmail);
            }
            if ($mailer->send()) {
                $request->email_status = OrderNotificationRequest::EMAIL_STATUS_DONE;
            } else {
                $request->email_status = OrderNotificationRequest::EMAIL_STATUS_ERROR;
            }
            $transaction = OrderNotificationRequest::getDb()->beginTransaction();
            try {
                if (!$request->save()) {
                    throw new \Exception($request->getFirstErrorAsString());
                }
                $transaction->commit();
            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            }
        } catch (\Throwable $e) {
            $request->email_status = OrderNotificationRequest::SMS_STATUS_ERROR;
            $request->save(false, ['email_status']);
            $this->logAddError($e->getMessage(), ['request_id' => $this->requestId]);
        }

        return $answer;
    }

    /**
     * @param string $email
     * @return string
     */
    protected function prepareEmail(string $email)
    {
        return $email;
    }
}