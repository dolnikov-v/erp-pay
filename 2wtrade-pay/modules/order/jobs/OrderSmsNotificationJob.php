<?php

namespace app\modules\order\jobs;


use app\jobs\BaseJob;
use app\modules\order\models\OrderNotification;
use app\modules\order\models\OrderNotificationRequest;
use app\modules\smsnotification\abstracts\SmsNotificationInformationInterface;
use app\modules\smsnotification\abstracts\SmsServiceResponseInterface;
use yii\httpclient\Client;

/**
 * Class OrderSmsNotificationJob
 * @package app\modules\order\jobs
 */
class OrderSmsNotificationJob extends BaseJob
{
    const VALIDATE_PHONE_URI = '/v1/validator/phone';

    /**
     * @var integer
     */
    public $requestId;

    /**
     * Вызов джобы
     * @param \yii\queue\Queue $queue
     * @return mixed
     */
    public function execute($queue)
    {
        $request = OrderNotificationRequest::find()->where([
            'id' => $this->requestId,
            'sms_status' => OrderNotificationRequest::SMS_STATUS_PENDING,
            'status' => OrderNotificationRequest::SMS_STATUS_READY
        ])->one();
        if (empty($request) || empty($request->phone_to)) {
            return null;
        }

        $answer = null;
        try {
            $request->sms_notification_text = $request->orderNotification->getFilledSmsText($request->order);
            if ($request->orderNotification->answerable == OrderNotification::IS_ANSWERABLE || $request->orderNotification->issue_collector_id) {
                $request->sms_notification_text .= ' ' . $request->getFeedbackLink();
            }
            $request->phone_to = $this->validatePhoneNumber($request->phone_to,$request);
            $result = $this->smsService->sendSmsNotification($request->phone_to, null, $request->sms_notification_text, $request->order->country->sms_notifier_phone_from, ['char_code' => $request->order->country->char_code]);
            $answer = $result->getResponse();
            $request->sms_api_error = $result->getMessage();
            $request->sms_api_code = $result->getCode();
            if ($result->getStatus() != SmsServiceResponseInterface::STATUS_SUCCESS) {
                $request->sms_status = OrderNotificationRequest::SMS_STATUS_ERROR;
            } else {
                switch ($result->getSmsNotificationInformation()->getStatus()) {
                    case SmsNotificationInformationInterface::STATUS_DONE:
                        $request->sms_status = OrderNotificationRequest::SMS_STATUS_DONE;
                        break;
                    case SmsNotificationInformationInterface::STATUS_ERROR:
                        $request->sms_status = OrderNotificationRequest::SMS_STATUS_ERROR;
                        break;
                    default:
                        $request->sms_status = OrderNotificationRequest::SMS_STATUS_IN_PROGRESS;
                        break;
                }
                $request->sms_date_created = $result->getSmsNotificationInformation()->getCreatedAt() ?? time();
                $request->sms_foreign_id = $result->getSmsNotificationInformation()->getForeignId();
                $request->sms_foreign_status = $result->getSmsNotificationInformation()->getForeignStatus();
                $request->sms_price = $result->getSmsNotificationInformation()->getPrice();
                $request->sms_date_updated = $result->getSmsNotificationInformation()->getUpdatedAt();
                $request->sms_date_sent = $result->getSmsNotificationInformation()->getSentAt();
                $request->sms_uri = $result->getSmsNotificationInformation()->getUri();
            }
            $transaction = OrderNotificationRequest::getDb()->beginTransaction();
            try {
                if (!$request->save()) {
                    throw new \Exception($request->getFirstErrorAsString());
                }
                $transaction->commit();
            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            }
        } catch (\Throwable $e) {
            $request->sms_status = OrderNotificationRequest::SMS_STATUS_ERROR;
            $request->sms_api_error = $e->getMessage();
            $request->save(false, ['sms_api_error', 'sms_status']);
            $this->logAddError($e->getMessage(), ['request_id' => $this->requestId]);
        }

        return $answer;
    }

    /**
     * @param string $phone
     * @param OrderNotificationRequest $request
     * @return string
     * @throws \Exception
     * @throws \yii\httpclient\Exception
     */
    public function validatePhoneNumber(string $phone, $request): string
    {
        $client = new Client();
        $httpRequest = $client->createRequest()->setUrl(trim(\Yii::$app->params['api2wcallUrl'], '/') . '/' . trim(static::VALIDATE_PHONE_URI, '/?') . '?' . http_build_query(['char_code' => $request->order->country->char_code, 'phone' => $phone, 'token' => \Yii::$app->params['api2wcallToken']]));
        $httpRequest->setHeaders([
            'Authorization' => 'Basic ' . base64_encode(\Yii::$app->params['api2wcallIdentity'])
        ]);
        $response = $httpRequest->send();
        $content = json_decode($response->content, true);
        if (!$response->isOk) {
            throw new \Exception("Couldn't validate phone number. Service return {$response->getStatusCode()} code." . implode("\n", $content['Message'] ?? []));
        }
        if (!$content['success']) {
            throw new \Exception("Phone is not valid. Errors: " . implode("\n", $content['Message'] ?? []));
        }
        if (!isset($content['phone'])) {
            throw new \Exception("Field with phone not found in response from validation service");
        }
        return $content['phone'];
    }
}