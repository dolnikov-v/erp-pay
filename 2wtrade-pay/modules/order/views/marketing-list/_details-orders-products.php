<?php
use app\widgets\Label;

/** @var \app\modules\order\models\Order $model */
/** @var array $products */
?>

<?php foreach ($model->orderProducts as $orderProduct): ?>
    <div>
        <?= Label::widget([
            'label' => $products[$orderProduct->product_id] . ' - ' . $orderProduct->quantity,
            'style' => Label::STYLE_INFO,
        ]) ?>
    </div>
<?php endforeach; ?>

