<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var \app\modules\order\models\MarketingList $list */
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute(['details', 'id' => $list->id]),
    'enableClientValidation' => false,
]); ?>

<div class="row">
    <div class="col-lg-3">
        <?= $form->field($list, 'name')->textInput([
            'value' => $list->name,
            'readonly' => 'readonly',
        ])->label(Yii::t('common', 'Название')) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($list, 'countOldOrders')->textInput([
            'value' => $list->getCountOldOrders(),
            'readonly' => 'readonly',
        ])->label(Yii::t('common', 'Количество старых заказов')) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($list, 'created_at')->textInput([
            'value' => Yii::$app->formatter->asDatetime($list->created_at),
            'readonly' => 'readonly',
        ])->label(Yii::t('common', 'Дата создания')) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
