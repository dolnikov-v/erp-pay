<?php
use app\modules\order\assets\ListAsset;
use app\modules\order\widgets\marketing\ModalConfirmDeleteOrder;
use app\widgets\Panel;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var \app\modules\order\models\MarketingList $list */
/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var array $statuses */
/** @var array $products */

$this->title = Yii::t('common', 'Детальная информация');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Заказы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Список маркетинговых листов'), 'url' => Url::toRoute('index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ListAsset::register($this);
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Основная информация'),
    'content' => $this->render('_details-main', [
        'list' => $list,
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Список старых заказов'),
    'withBody' => false,
    'content' => $this->render('_details-orders', [
        'list' => $list,
        'dataProvider' => $dataProvider,
        'statuses' => $statuses,
        'products' => $products,
    ]),
]) ?>

<?= ModalConfirmDeleteOrder::widget() ?>
