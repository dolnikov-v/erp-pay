<?php
use app\components\grid\ActionColumn;
use app\components\grid\DateColumn;
use app\components\grid\GridView;
use app\helpers\DataProvider;
use app\modules\order\assets\MarketingListAsset;
use app\widgets\Panel;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\LinkPager;
use app\modules\order\widgets\marketing\ModalConfirmDeleteList;
use app\modules\order\models\MarketingList;
use app\modules\order\models\OrderStatus;

/** @var yii\web\View $this */
/** @var app\modules\order\models\search\MarketingListSearch $modelSearch */
/** @var yii\data\ActiveDataProvider $dataProvider */

MarketingListAsset::register($this);

$this->registerJs("var I18n = {};", View::POS_HEAD);

$this->title = Yii::t('common', 'Список маркетинговых листов');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Заказы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'content' => $this->render('_index-filter', [
        'modelSearch' => $modelSearch,
    ]),
    'collapse' => true,
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с маркетинговыми листами'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'id',
            ],
            [
                'attribute' => 'name',
            ],
            [
                'attribute' => 'countOldOrders',
                'label' => Yii::t('common', 'Старые заказы'),
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    if ($model->countOldOrders) {
                        if (Yii::$app->user->can('order.marketinglist.details'))
                            return Html::a($model->countOldOrders, Url::toRoute(['details', 'id' => $model->id]));
                        else
                            return $model->countOldOrders;
                    } else {
                        return "-";
                    }
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'countNewOrders',
                'label' => Yii::t('common', 'Новые заказы'),
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    if ($model->countNewOrders) {
                        return Html::a($model->countNewOrders, [
                            '/order/index/index',
                            'NumberFilter[entity]' => 'marketing',
                            'NumberFilter[number]' => $model->id,
                        ], [
                            'target' => '_blank',
                        ]);
                    } else {
                        return "-";
                    }
                },
                'enableSorting' => false,
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'created_at',
            ],
            [
                'label' => Yii::t('common', 'Обработано'),
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    if (!$model->countNewOrders || !$model->process) return "-";
                    $statuses = MarketingList::getMapStatuses();
                    return Html::a(
                            $model->process,
                            [
                                '/order/index/index',
                                'NumberFilter[entity]' => 'marketing',
                                'NumberFilter[number]' => $model->id,
                                'StatusFilter[not]' => [1, 1, 1],
                                'StatusFilter[status]' => $statuses[Yii::t('common', 'Не обработано')],
                            ],
                            [
                                'target' => '_blank',
                            ]
                        ) . " (" . Yii::$app->formatter->asDecimal($model->process * 100 / $model->countNewOrders, 2) . "%)";
                }
            ],
            [
                'label' => Yii::t('common', 'Подтверждено'),
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    if (!$model->countNewOrders || !$model->approve) return "-";
                    $statuses = MarketingList::getMapStatuses();
                    return Html::a(
                            $model->approve,
                            [
                                '/order/index/index',
                                'NumberFilter[entity]' => 'marketing',
                                'NumberFilter[number]' => $model->id,
                                'StatusFilter[status]' => $statuses[Yii::t('common', 'Подтверждено')]
                            ],
                            [
                                'target' => '_blank',
                            ]
                        ) . " (" . Yii::$app->formatter->asDecimal($model->approve * 100 / $model->countNewOrders, 2) . "%)";
                }
            ],
            [
                'label' => Yii::t('common', 'Выкуплено'),
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    if (!$model->countNewOrders || !$model->buyout) return "-";
                    $statuses = MarketingList::getMapStatuses();
                    return Html::a(
                            $model->buyout,
                            [
                                '/order/index/index',
                                'NumberFilter[entity]' => 'marketing',
                                'NumberFilter[number]' => $model->id,
                                'StatusFilter[status]' => $statuses[Yii::t('common', 'Выкуплено')]
                            ],
                            [
                                'target' => '_blank',
                            ]
                        ) . " (" . Yii::$app->formatter->asDecimal($model->buyout * 100 / $model->countNewOrders, 2) . "%)";
                }
            ],
            [
                'label' => Yii::t('common', 'Средний чек по выкупам, USD'),
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    if (!$model->buyout || !$model->avgbuy) return "-";
                    return Yii::$app->formatter->asDecimal($model->avgbuy, 2);
                }
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'sent_at',
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Отправить в КЦ на повторную продажу'),
                        'url' => function ($model) {
                            return Url::toRoute(['send-orders-to-delivery', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return (Yii::$app->user->can('order.marketinglist.sendorderstodelivery') && !$model->sent_at);
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить лист'),
                        'url' => function () {
                            return Url::toRoute(["#"]);
                        },
                        'style' => 'confirm-delete-list',
                        'attributes' => function ($model) {
                            return [
                                'data-href' => Url::toRoute([
                                    'delete-list',
                                    'list' => $model->id,
                                ]),
                            ];
                        },
                        'can' => function () {
                            return Yii::$app->user->can('order.marketinglist.deletelist');
                        },
                    ],
                    [
                        'label' => Yii::t('common', 'Детальная информация'),
                        'url' => function ($model) {
                            return Url::toRoute(['details', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('order.marketinglist.details');
                        },
                    ],
                ]
            ],

        ],
    ]),
    'footer' => LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>

<?= ModalConfirmDeleteList::widget() ?>
