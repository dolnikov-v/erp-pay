<?php

use app\widgets\Panel;
use app\helpers\DataProvider;
use app\components\grid\GridView;
use app\components\grid\IdColumn;
use yii\helpers\Url;
use yii\helpers\Html;
use app\widgets\Label;
use app\components\grid\DateColumn;
use app\components\grid\ActionColumn;
use app\widgets\LinkPager;
use app\widgets\ButtonLink;

/** @var \yii\web\View $this */
/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var \app\modules\order\models\search\OrderNotificationRequestSearch $searchModel */

$this->title = Yii::t('common', 'Заявки');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Уведомления'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'content' => $this->render('_index-filter', [
        'searchModel' => $searchModel,
    ]),
    'collapse' => true,
]) ?>

<div class="order-notification-calls">
    <?= Panel::widget([
        'title' => Yii::t('common', 'Таблица с заявками'),
        'withBody' => false,
        'actions' => DataProvider::renderSummary($dataProvider) .
            ButtonLink::widget([
                'id' => 'btn_export',
                'icon' => '<i class="fa fa-table"></i>',
                'style' => ButtonLink::STYLE_DEFAULT . ' pull-right',
                'url' => Url::toRoute(['notification-request/index'] + Yii::$app->request->queryParams + ['export' => 'excel']),
                'label' => Yii::t("common", "Экспорт в Excel"),
            ]),
        'content' =>
            GridView::widget([
                'dataProvider' => $dataProvider,
                'tableOptions' => [
                    'class' => 'table table-striped table-hover table-sortable tl-fixed',
                ],
                'columns' => [
                    [
                        'class' => IdColumn::className(),
                    ],
                    [
                        'attribute' => 'order_id',
                        'headerOptions' => ['class' => 'text-center', 'style' => 'width: 100px'],
                        'contentOptions' => ['class' => 'text-center'],
                        'content' => function ($model) {
                            return Html::a($model->order_id, Url::toRoute([
                                '/order/index/index',
                                'NumberFilter[not][]' => '',
                                'NumberFilter[entity][]' => 'id',
                                'NumberFilter[number][]' => $model->order_id,
                            ]), [
                                'target' => '_blank',
                            ]);
                        },
                        'enableSorting' => false,
                    ],
                    [
                        'attribute' => 'order_status_id',
                        'headerOptions' => [
                            'class' => 'text-center',
                        ],
                        'contentOptions' => [
                            'class' => 'text-center',
                        ],
                        'content' => function ($model) {
                            return Label::widget(['label' => $model->orderStatus->name]);
                        },
                        'enableSorting' => false,
                    ],
                    [
                        'attribute' => 'delivery_id',
                        'headerOptions' => ['class' => 'text-center'],
                        'contentOptions' => [
                            'class' => 'text-center',
                            'style' => 'word-wrap: break-word;'
                        ],
                        'content' => function ($model) {
                            return $model->delivery ? $model->delivery->name : null;
                        },
                        'enableSorting' => false,
                    ],
                    [
                        'attribute' => 'email_to',
                        'headerOptions' => ['class' => 'text-center'],
                        'contentOptions' => [
                            'class' => 'text-center',
                            'style' => 'word-wrap: break-word;'
                        ],
                        'enableSorting' => false,
                    ],
                    [
                        'attribute' => 'email_status',
                        'headerOptions' => ['class' => 'text-center'],
                        'contentOptions' => ['class' => 'text-center'],
                        'content' => function ($model) {
                            /** @var \app\modules\order\models\OrderNotificationRequest $model */
                            return Label::widget([
                                'label' => $model->getEmailStatusLabel(),
                                'style' => $model->getLabelStyleByEmailStatus(),
                            ]);
                        },
                        'enableSorting' => false,
                    ],
                    [
                        'attribute' => 'phone_to',
                        'headerOptions' => ['class' => 'text-center'],
                        'contentOptions' => ['class' => 'text-center'],
                        'enableSorting' => false,
                    ],
                    [
                        'attribute' => 'sms_status',
                        'headerOptions' => ['class' => 'text-center'],
                        'contentOptions' => ['class' => 'text-center'],
                        'content' => function ($model) {
                            /** @var \app\modules\order\models\OrderNotificationRequest $model */
                            return Label::widget([
                                'label' => $model->getSmsStatusLabel(),
                                'style' => $model->getLabelStyleBySmsStatus(),
                            ]);
                        },
                        'enableSorting' => false,
                    ],
                    [
                        'attribute' => 'order_notification_id',
                        'headerOptions' => ['class' => 'text-center'],
                        'contentOptions' => ['class' => 'text-center'],
                        'value' => 'orderNotification.name',
                        'enableSorting' => false,
                    ],
                    [
                        'class' => DateColumn::className(),
                        'attribute' => 'sms_date_sent',
                    ],
                    [
                        'class' => DateColumn::className(),
                        'attribute' => 'created_at',
                    ],
                    [
                        'class' => ActionColumn::className(),
                        'items' => [
                            [
                                'label' => Yii::t('common', 'История изменения'),
                                'url' => function ($model) {
                                    return Url::toRoute(['/order/notification-request/logs', 'id' => $model->id]);
                                },
                                'can' => function () {
                                    return Yii::$app->user->can('order.notificationrequest.logs');
                                }
                            ],
                        ],
                    ],
                ],
            ]),
        'footer' => LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
    ]) ?>
</div>
