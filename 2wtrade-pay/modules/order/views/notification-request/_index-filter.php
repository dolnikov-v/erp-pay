<?php
use app\components\widgets\ActiveForm;
use app\widgets\assets\Select2Asset;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var \app\modules\order\models\search\OrderNotificationRequestSearch $searchModel */
Select2Asset::register($this);
?>

<?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
]); ?>

<div class="row">
    <div class="col-md-3">
        <?= $form->field($searchModel, 'dateType')->select2List($searchModel->dateTypeCollection(), [
            'length' => -1,
        ]) ?>
    </div>
    <div class="col-lg-6">
        <?= $form->field($searchModel, 'created_at')->dateRangePicker('dateFrom', 'dateTo')->label(Yii::t('common', 'Дата')) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($searchModel, 'phone_to') ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-3">
        <?= $form->field($searchModel, 'order_notification_id')->select2List($searchModel->orderNotifications,
            [
                'prompt' => Yii::t('common', 'Все'),
                'length' => 1
            ]
        ) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($searchModel, 'sms_status')->select2List($searchModel::getSmsStatusLabels(),
            [
                'prompt' => Yii::t('common', 'Все'),
                'length' => 1
            ]
        ) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($searchModel, 'order_id') ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($searchModel, 'order_status_id')->select2List($searchModel->orderStatuses,
            [
                'prompt' => Yii::t('common', 'Все'),
                'length' => 1
            ]
        ) ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')) ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
