<?php
use app\components\grid\ActionColumn;
use app\components\grid\GridView;
use app\helpers\DataProvider;
use app\modules\order\models\OrderWorkflowStatus;
use app\widgets\assets\custom\ModalConfirmDeleteAsset;
use app\widgets\ButtonLink;
use app\widgets\custom\ModalConfirmDelete;
use app\widgets\Label;
use app\widgets\Panel;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/** @var yii\web\View $this */
/** @var app\modules\order\models\OrderWorkflow $model */
/** @var app\modules\order\models\search\OrderWorkflowSearch $modelSearch */
/** @var yii\data\ActiveDataProvider $dataProvider */

ModalConfirmDeleteAsset::register($this);

$this->title = Yii::t('common', 'Просмотр workflow');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Заказы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Workflows'), 'url' => Url::toRoute('/order/workflow/index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Workflow'),
    'content' => $this->render('_view-form', [
        'model' => $model,
    ])
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица со статусами'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'status_id',
                'content' => function ($model) {
                    return Label::widget([
                        'label' => Yii::t('common', $model->status->name),
                        'style' => $model->status->getLabelStyle(),
                        'outline' => true,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'parents',
                'content' => function ($model) {
                    $content = '';

                    /** @var OrderWorkflowStatus $model */
                    foreach ($model->getParentsAsStatuses() as $status) {
                        $content .= Label::widget([
                            'label' => Yii::t('common', $status->name),
                            'style' => $status->getLabelStyle(),
                            'outline' => true,
                        ]);
                    }

                    return $content;
                },
                'enableSorting' => false,
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/order/workflow/edit-status', 'id' => $model->id, 'workflow' => $model->workflow_id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('order.workflow.editstatus');
                        }
                    ],
                    [
                        'can' => function () {
                            return Yii::$app->user->can('order.workflow.editstatus') || Yii::$app->user->can('order.workflow.deletestatus');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить'),
                        'url' => function () {
                            return '#';
                        },
                        'style' => 'confirm-delete-link',
                        'attributes' => function ($model) {
                            return [
                                'data-href' => Url::toRoute(['/order/workflow/delete-status', 'id' => $model->id]),
                            ];
                        },
                        'can' => function () {
                            return Yii::$app->user->can('order.workflow.deletestatus');
                        }
                    ],
                ]
            ]
        ],
    ]),
    'footer' => (Yii::$app->user->can('order.workflow.editstatus') ?
            ButtonLink::widget([
                'url' => Url::toRoute(['/order/workflow/edit-status', 'workflow' => $model->id]),
                'label' => Yii::t('common', 'Добавить статус'),
                'style' => ButtonLink::STYLE_SUCCESS,
                'size' => ButtonLink::SIZE_SMALL,
            ]) : '') . LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Визуальное представление'),
    'content' => $this->render('_view-scheme', [
        'model' => $model,
    ])
]) ?>

<?= ModalConfirmDelete::widget() ?>
