<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var app\modules\order\models\OrderWorkflow $model */
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['view', 'id' => $model->id])]); ?>
<div class="row">
    <div class="col-lg-12">
        <div class="form-group">
            <?= $form->field($model, 'name')->textInput([
                'disabled' => 'disabled'
            ]) ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="form-group">
            <?= $form->field($model, 'comment')->textarea([
                'disabled' => 'disabled'
            ]) ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= $form->link(Yii::t('common', 'Вернуться назад'), Url::toRoute('index')) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
