<?php
use app\widgets\Panel;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var app\modules\order\models\OrderWorkflow $model */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление workflow') : Yii::t('common', 'Редактирование workflow');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Заказы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Workflows'), 'url' => Url::toRoute('/order/workflow/index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Workflow'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
    ])
]) ?>
