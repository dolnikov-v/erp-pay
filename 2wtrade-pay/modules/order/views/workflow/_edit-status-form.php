<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var app\modules\order\models\OrderWorkflowStatus $model */
/** @var app\modules\order\models\OrderWorkflow $modelWorkflow */
/** @var array $statuses */
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['edit-status', 'id' => $model->id, 'workflow' => $modelWorkflow->id])]); ?>
<div class="row">
    <div class="col-lg-12">
        <div class="form-group">
            <?= $form->field($model, 'workflow_id')->textInput([
                'value' => $modelWorkflow->name,
                'disabled' => 'disabled',
            ])->label(Yii::t('common', 'Workflow')) ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="form-group">
            <?= $form->field($model, 'status_id')->select2List($statuses, [
                'length' => 1,
            ]) ?>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="form-group">
            <?= $form->field($model, 'parents')->select2ListMultiple($statuses, [
                'value' => $model->getParents(),
            ]) ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить статус') : Yii::t('common', 'Сохранить статус')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute(['view', 'id' => $modelWorkflow->id])) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
