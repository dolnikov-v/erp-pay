<?php
use app\modules\order\widgets\WorkflowEditor;

/** @var app\modules\order\models\OrderWorkflow $model */

echo WorkflowEditor::widget([
    'id' => 'workflow_scheme',
    'model' => $model,
]);
