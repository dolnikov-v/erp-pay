<?php
use app\widgets\Panel;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var app\modules\order\models\OrderWorkflowStatus $model */
/** @var app\modules\order\models\OrderWorkflow $modelWorkflow */
/** @var array $statuses */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление статуса') : Yii::t('common', 'Редактирование статуса');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Заказы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Workflows'), 'url' => Url::toRoute('/order/workflow/index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Статус'),
    'content' => $this->render('_edit-status-form', [
        'model' => $model,
        'modelWorkflow' => $modelWorkflow,
        'statuses' => $statuses,
    ])
]) ?>
