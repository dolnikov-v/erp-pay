<?php

use kartik\grid\GridView;
use app\modules\order\models\OrderFinanceFact;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */
/** @var app\modules\order\widgets\ShowerFinanceFactColumns $showerColumns */
/** @var array $currencies */

$columns = [];
foreach (OrderFinanceFact::getReportColumns()['column'] as $column) {
    $columns[] = [
        'attribute' => $column,
        'label' => OrderFinanceFact::attributeLabels()[$column] ?? $column,
        'value' => function ($array) use ($currencies, $column) {
            return !empty($array[$column]) ? $array[$column] . ' ' . $currencies[$array[$column . '_currency_id']] : '—';
        },
        'contentOptions' => function ($array) use ($column) {
            return $array[$column] ? ['title' => 'Курс к USD ' . round($array[$column . '_currency_rate'], 2)] : [];
        },
        'hAlign' => 'center',
        'visible' => $showerColumns->isChosenColumn($column),
    ];
}
?>

<?= GridView::widget([
    'layout' => "{items}",
    'tableOptions' => ['style' => 'margin-bottom: 0;', 'id' => 'orders_content'],
    'dataProvider' => $dataProvider,
    'rowOptions' => ['class' => 'tr-vertical-align-middle'],
    'columns' => array_merge(
        [
            ['class' => 'kartik\grid\SerialColumn', 'width' => 0],
            [
                'attribute' => 'id',
                'label' => Yii::t('common', 'Номер заказа'),
                'group' => true,
                'vAlign' => 'middle',
                'content' => function ($array) {
                    return Html::a($array['id'], Url::toRoute('/order/index/view/' . $array['id']));
                }
            ],
            [
                'attribute' => 'name',
                'label' => Yii::t('common', 'Отчёт'),
                'visible' => $showerColumns->isChosenColumn('name'),
                'content' => function ($array) {
                    return $array['name'] ? Html::a($array['name'], Url::toRoute('/deliveryreport/report/view/' . $array['delivery_report_id'])) : 'расчётный';
                }
            ],
            [
                'attribute' => 'payment',
                'label' => Yii::t('common', 'Платёжка'),
                'vAlign' => 'middle',
                'visible' => $showerColumns->isChosenColumn('payment'),
            ],
        ],
        $columns
    )]) ?>
