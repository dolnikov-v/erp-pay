<?php

use app\modules\order\widgets\orderFilters\OrderFinanceFactFilters;
use app\modules\order\assets\OrderFiltersAsset;
use app\widgets\Panel;
use app\helpers\DataProvider;
use app\widgets\LinkPager;
use yii\helpers\Url;
use app\modules\order\assets\ShowerColumnsAsset;

/** @var yii\web\View $this */
/** @var app\modules\order\models\search\OrderSearch $modelSearch */
/** @var yii\data\ActiveDataProvider $dataProvider */
/** @var app\modules\order\widgets\ShowerFinanceFactColumns $showerColumns */
/** @var array $currencies */

OrderFiltersAsset::register($this);
ShowerColumnsAsset::register($this);

$this->title = Yii::t('common', 'Фактические расходы');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Заказы'), 'url' => Url::to('/order/index/index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= OrderFinanceFactFilters::widget([
    'modelSearch' => $modelSearch,
]); ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с заказами'),
    'actions' => DataProvider::renderSummary($dataProvider) . $showerColumns->renderIcon(),
    'withBody' => false,
    'content' => $this->render('_table', [
        'dataProvider' => $dataProvider,
        'showerColumns' => $showerColumns,
        'currencies' => $currencies,
    ]),
    'footer' => LinkPager::widget([
        'pagination' => $dataProvider->getPagination(),
        'counterPosition' => LinkPager::COUNTER_POSITION_RIGHT,
    ]),
]);
?>

<?= $showerColumns->renderModal(); ?>
