<?php
use app\components\widgets\ActiveForm;
use app\widgets\assets\Select2Asset;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var app\modules\order\models\search\OrderClarificationSearch $modelSearch */

Select2Asset::register($this);
?>

<?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
]); ?>

<div class="row">
    <div class="col-lg-6">
        <?= $form->field($modelSearch, 'created_at')->dateRangePicker('dateFrom', 'dateTo') ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($modelSearch, 'delivery_id')->select2List($modelSearch->deliveries,
            ['prompt' => Yii::t('common', 'Все')]
        ) ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')) ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
