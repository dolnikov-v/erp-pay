<?php
use app\components\grid\ActionColumn;
use app\components\grid\GridView;
use app\helpers\DataProvider;
use app\widgets\Panel;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use app\widgets\custom\ModalConfirmDelete;
use app\widgets\assets\custom\ModalConfirmDeleteAsset;

/** @var yii\web\View $this */
/** @var app\modules\order\models\search\OrderClarificationSearch $modelSearch */
/** @var yii\data\ActiveDataProvider $dataProvider */
/** @var array $orderStatuses */

ModalConfirmDeleteAsset::register($this);
?>
<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'content' => $this->render('_index-filter', [
        'modelSearch' => $modelSearch,
    ]),
    'collapse' => true,
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Список'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'id',
            ],
            [
                'attribute' => 'delivery_id',
                'content' => function ($model) {
                    return $model->delivery->name;
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'created_at',
                'headerOptions' => ['class' => 'width-150'],
                'label' => Yii::t('common', 'Дата создания'),
                'content' => function ($model) {
                    return Yii::$app->formatter->asDate($model->created_at);
                },
            ],
            [
                'attribute' => 'sent_at',
                'headerOptions' => ['class' => 'width-150'],
                'label' => Yii::t('common', 'Дата отправки'),
                'content' => function ($model) {
                    return Yii::$app->formatter->asDate($model->sent_at);
                },
            ],
            [
                'attribute' => 'order_buyout',
                'label' => Yii::t('common', 'Выкуплено'),
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) use ($orderStatuses) {
                    $content = '0';
                    if ($orders = $orderStatuses[$model->id]['buyout']) {
                        $url = [
                            '/order/index/index',
                            'NumberFilter[not][]' => '',
                            'NumberFilter[entity][]' => 'id',
                            'NumberFilter[number][]' => join(',', $orders),
                        ];
                        $content = Html::a(count($orders), $url, ['target' => '_blank']);
                    }

                    return $content;
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'order_in_process',
                'label' => Yii::t('common', 'В процессе'),
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) use ($orderStatuses) {
                    $content = '0';
                    if ($orders = $orderStatuses[$model->id]['in_process']) {
                        $url = [
                            '/order/index/index',
                            'NumberFilter[not][]' => '',
                            'NumberFilter[entity][]' => 'id',
                            'NumberFilter[number][]' => join(',', $orders),
                        ];
                        $content = Html::a(count($orders), $url, ['target' => '_blank']);
                    }

                    return $content;
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'order_not_buyout',
                'label' => Yii::t('common', 'Не выкуплено'),
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) use ($orderStatuses) {
                    $content = '0';
                    if ($orders = $orderStatuses[$model->id]['not_buyout']) {
                        $url = [
                            '/order/index/index',
                            'NumberFilter[not][]' => '',
                            'NumberFilter[entity][]' => 'id',
                            'NumberFilter[number][]' => join(',', $orders),
                        ];
                        $content = Html::a(count($orders), $url, ['target' => '_blank']);
                    }

                    return $content;
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'order_count',
                'label' => Yii::t('common', 'Всего'),
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    $content = '0';
                    if ($orders = explode(',', $model->order_ids)){
                        $url = [
                            '/order/index/index',
                            'NumberFilter[not][]' => '',
                            'NumberFilter[entity][]' => 'id',
                            'NumberFilter[number][]' => $model->order_ids,
                        ];
                        $content = Html::a(count($orders), $url, ['target' => '_blank']);
                    }

                    return $content;
                },
                'enableSorting' => false,
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Удалить'),
                        'url' => function () {
                            return '#';
                        },
                        'style' => 'confirm-delete-link',
                        'attributes' => function ($model) {
                            return [
                                'data-href' => Url::toRoute(['delete', 'id' => $model->id]),
                            ];
                        },
                        'can' => function () {
                            return Yii::$app->user->can('order.clarification.delete');
                        },
                    ],
                ]
            ],

        ],
    ]),
    'footer' => LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>

<?= ModalConfirmDelete::widget() ?>
