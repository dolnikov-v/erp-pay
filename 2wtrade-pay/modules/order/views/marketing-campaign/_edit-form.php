<?php
use app\components\widgets\ActiveForm;
use app\modules\order\assets\MarketingCampaignAsset;
use yii\helpers\Url;

/** @var app\modules\order\models\MarketingCampaign $model */

MarketingCampaignAsset::register($this);
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['edit', 'id' => $model->id])]); ?>
<div class="row">
    <div class="col-lg-6">
        <?= $form->field($model, 'name')->textInput() ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        <?= $form->field($model, 'limit_type')->select2List($model->getLimitTypesCollection()) ?>
    </div>
    <div class="col-lg-6 limit limit_days" <?= (($model->limit_type != $model::TYPE_LIMIT_DAYS )?'style="display: none"':'')?>>
        <?= $form->field($model, 'limit_use_days')->textInput() ?>
    </div>
    <div class="col-lg-6 limit limit_at" <?= (($model->limit_type != $model::TYPE_LIMIT_AT )?'style="display: none"':'')?>>
        <?= $form->field($model, 'limit_use_at')->datePicker(
            [
                'minDate' => (($model->id && $model->limit_use_at) ? date("Y-m-d", min($model->limit_use_at, time())) : date("Y-m-d"))
            ]
        );?>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        <?= $form->field($model, 'newcoupons')->textInput() ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить маркетинговую акцию') : Yii::t('common', 'Сохранить маркетинговую акцию')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
