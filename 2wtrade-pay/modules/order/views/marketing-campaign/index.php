<?php
use app\components\grid\ActionColumn;
use app\components\grid\DateColumn;
use app\components\grid\GridView;
use app\helpers\DataProvider;
use app\modules\order\assets\MarketingCampaignAsset;
use app\widgets\Panel;
use app\widgets\ButtonLink;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use app\widgets\LinkPager;
use app\modules\order\widgets\marketing\ModalConfirmDeleteCampaign;
use app\modules\order\models\MarketingCampaign;

/** @var yii\web\View $this */
/** @var app\modules\order\models\search\MarketingCampaignSearch $modelSearch */
/** @var yii\data\ActiveDataProvider $dataProvider */

MarketingCampaignAsset::register($this);

$this->registerJs("var I18n = {};", View::POS_HEAD);

$this->title = Yii::t('common', 'Список маркетинговых акций');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Заказы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'content' => $this->render('_index-filter', [
        'modelSearch' => $modelSearch,
    ]),
    'collapse' => true,
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с маркетинговыми акциями'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'id'
            ],
            [
                'attribute' => 'name'
            ],
            [
                'attribute' => 'limit_type',
                'content' => function ($model) {
                    $types = MarketingCampaign::getLimitTypesCollection();
                    return $types[$model->limit_type];
                }
            ],
            [
                'attribute' => 'limit_use_at',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    if (!$model->limit_use_at) return '-';
                    return Yii::$app->formatter->asDate($model->limit_use_at);
                }
            ],
            [
                'attribute' => 'limit_use_days',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center']
            ],
            [
                'attribute' => 'countCoupons',
                'label' => Yii::t('common', 'Купоны'),
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    if ($model->countCoupons) {
                        if (Yii::$app->user->can('order.marketingcampaign.details'))
                            return Html::a($model->countCoupons, Url::toRoute(['details', 'id' => $model->id]));
                        else
                            return $model->countCoupons;
                    } else {
                        return "-";
                    }
                },
                'enableSorting' => false,
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'created_at',
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Напечатать'),
                        'attributes' => function () {
                            return [
                                'target' => '_blank',
                            ];
                        },
                        'url' => function ($model) {
                            return Url::toRoute(['print', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return (Yii::$app->user->can('order.marketingcampaign.print'));
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['edit', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('order.marketingcampaign.edit');
                        },
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить акцию'),
                        'url' => function () {
                            return Url::toRoute(["#"]);
                        },
                        'style' => 'confirm-delete-campaign',
                        'attributes' => function ($model) {
                            return [
                                'data-href' => Url::toRoute([
                                    'delete-campaign',
                                    'campaign' => $model->id,
                                ]),
                            ];
                        },
                        'can' => function () {
                            return Yii::$app->user->can('order.marketingcampaign.deletecampaign');
                        },
                    ],
                    [
                        'label' => Yii::t('common', 'Детальная информация'),
                        'url' => function ($model) {
                            return Url::toRoute(['details', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('order.marketingcampaign.details');
                        },
                    ],
                ]
            ],

        ],
    ]),
    'footer' => (Yii::$app->user->can('order.marketingcampaign.edit') ?
            ButtonLink::widget([
                'url' => Url::toRoute('edit'),
                'label' => Yii::t('common', 'Добавить маркетинговую акцию'),
                'style' => ButtonLink::STYLE_SUCCESS,
                'size' => ButtonLink::SIZE_SMALL,
            ]) : '') . LinkPager::widget([
            'pagination' => $dataProvider->getPagination(),
            'counterPosition' => LinkPager::COUNTER_POSITION_RIGHT,
        ]),
]) ?>

<?= ModalConfirmDeleteCampaign::widget() ?>
