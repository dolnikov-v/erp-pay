<?php
use app\components\grid\ActionColumn;
use app\components\grid\GridView;
use app\components\grid\IdColumn;
use app\widgets\Label;
use yii\helpers\Url;
use app\widgets\LinkPager;

/** @var \app\modules\order\models\MarketingCampaign $list */
/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var array $statuses */
/** @var array $products */
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'label' => Yii::t('common', 'Купон'),
            'content' => function ($model) {
                return $model->coupon;
            },
            'enableSorting' => false,
        ],
        [
            'attribute' => 'printed_at',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'content' => function ($model) {
                if (!$model->printed_at) return '-';
                return Yii::$app->formatter->asDatetime($model->printed_at);
            }
        ],
        [
            'class' => ActionColumn::className(),
            'items' => [
                [
                    'label' => Yii::t('common', 'Удалить из списка'),
                    'url' => function () {
                        return '#';
                    },
                    'style' => 'confirm-delete-coupon',
                    'attributes' => function ($model) use ($list) {
                        return [
                            'data-href' => Url::toRoute(['delete-coupon',
                                'list' => $list->id,
                                'campaign' => $model->marketing_campaign_id,
                                'coupon' => $model->id
                            ]),
                        ];
                    },
                    'can' => function () use ($list) {
                        return Yii::$app->user->can('order.marketingcampaign.deletecoupon');
                    },
                ],
            ]
        ],
    ],
    'showFooter' => true
]) ?>
