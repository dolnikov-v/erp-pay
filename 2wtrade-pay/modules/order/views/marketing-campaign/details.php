<?php
use app\modules\order\assets\MarketingCampaignAsset;
use app\modules\order\widgets\marketing\ModalConfirmDeleteCoupon;
use app\widgets\Panel;
use yii\helpers\Url;
use app\widgets\LinkPager;

/** @var yii\web\View $this */
/** @var \app\modules\order\models\MarketingCampaign $list */
/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var array $statuses */
/** @var array $products */

$this->title = Yii::t('common', 'Детальная информация');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Заказы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Список маркетинговых акций'), 'url' => Url::toRoute('index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];

MarketingCampaignAsset::register($this);
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Основная информация'),
    'content' => $this->render('_details-main', [
        'list' => $list,
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Список купонов'),
    'withBody' => false,
    'content' => $this->render('_details-coupons', [
        'list' => $list,
        'dataProvider' => $dataProvider
    ]),
    'footer' => LinkPager::widget([
        'pagination' => $dataProvider->getPagination(),
        'counterPosition' => LinkPager::COUNTER_POSITION_RIGHT,
    ]),
]) ?>
<?= ModalConfirmDeleteCoupon::widget() ?>
