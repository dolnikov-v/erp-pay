<?php
use app\widgets\Panel;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var app\models\Product $model */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление маркетинговой акции') : Yii::t('common', 'Редактирование маркетинговой акции');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Заказы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Маркетинговые акции'), 'url' => Url::toRoute('/order/marketing-campaign/index')];
$this->params['breadcrumbs'][] = ['label' => $model->isNewRecord ? Yii::t('common', 'Добавление маркетинговой акции') : Yii::t('common', 'Редактирование маркетинговой акции')];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Маркетинговая акция'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
    ])
]); ?>
