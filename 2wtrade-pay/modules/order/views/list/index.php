<?php

use app\components\grid\ActionColumn;
use app\components\grid\DateColumn;
use app\components\grid\GridView;
use app\helpers\DataProvider;
use app\helpers\FontAwesome;
use app\modules\order\assets\InvoiceBuilderAsset;
use app\modules\order\assets\ListAsset;
use app\modules\order\models\OrderLogisticList;
use app\modules\order\models\OrderLogisticListRequest;
use app\modules\order\models\OrderLogisticListInvoice;
use app\modules\order\widgets\InvoiceBuilder;
use app\widgets\assets\media\MagnificPopupAsset;
use app\widgets\Panel;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\LinkPager;
use app\modules\order\widgets\ExcelBuilder;
use app\modules\order\assets\ExcelBuilderAsset;
use app\modules\order\widgets\logistic\ModalConfirmDeleteList;
use app\modules\order\widgets\logistic\ModalConfirmSentList;
use app\modules\order\widgets\logistic\ModalConfirmReceivedList;
use app\modules\order\assets\GenerateAndSendListAsset;
use app\modules\order\widgets\GenerateAndSendList;
use app\widgets\Label;

/** @var yii\web\View $this */
/** @var app\modules\order\models\search\OrderLogisticListSearch $modelSearch */
/** @var yii\data\ActiveDataProvider $dataProvider */

ListAsset::register($this);
MagnificPopupAsset::register($this);
ExcelBuilderAsset::register($this);
InvoiceBuilderAsset::register($this);
GenerateAndSendListAsset::register($this);

$this->registerJs("var I18n = {};", View::POS_HEAD);
$this->registerJs("I18n['Не удалось сгенерировать Excel-лист.'] = '" . Yii::t('common',
        'Не удалось сгенерировать Excel-лист.') . "';", View::POS_HEAD);
$this->registerJs("I18n['Не удалось сгенерировать накладные.'] = '" . Yii::t('common',
        'Не удалось сгенерировать накладные.') . "';", View::POS_HEAD);
$this->registerJs("I18n['Не удалось сгенерировать этикетки.'] = '" . Yii::t('common',
        'Не удалось сгенерировать этикетки.') . "';", View::POS_HEAD);
$this->registerJs("I18n['Не удалось отправить лист.'] = '" . Yii::t('common',
        'Не удалось отправить лист.') . "';", View::POS_HEAD);
$this->registerJs("I18n['Отправка листа завершена.'] = '" . Yii::t('common',
        'Отправка листа завершена.') . "';", View::POS_HEAD);

$this->title = Yii::t('common', 'Список листов');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Заказы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

$statuses = OrderLogisticList::getStatusesCollection();
$types = OrderLogisticList::getTypesCollection();
$requestStatuses = OrderLogisticListRequest::getStatusesCollection();
$requestStatusesColors = [
    OrderLogisticListRequest::STATUS_PENDING => Label::STYLE_PRIMARY,
    OrderLogisticListRequest::STATUS_SENT => Label::STYLE_INFO,
    OrderLogisticListRequest::STATUS_RECEIVED => Label::STYLE_SUCCESS,
    OrderLogisticListRequest::STATUS_ERROR => Label::STYLE_DANGER,
];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'content' => $this->render('_index-filter', [
        'modelSearch' => $modelSearch,
    ]),
    'collapse' => true,
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с листами'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'id',
            ],
            [
                'headerOptions' => ['class' => 'width-150'],
                'label' => Yii::t('common', 'Дата отгрузки'),
                'content' => function ($model) {
                    return Yii::$app->formatter->asDate($model->created_at);
                },
            ],
            [
                'attribute' => 'delivery_id',
                'content' => function ($model) {
                    return ($model->delivery) ? $model->delivery->name : '-';
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'number',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return str_pad($model->number, 2, '0', STR_PAD_LEFT);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'countOrders',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    if ($model->countOrders) {
                        return Html::a($model->countOrders, [
                            '/order/index/index',
                            'NumberFilter[not][]' => '',
                            'NumberFilter[entity][]' => 'list',
                            'NumberFilter[number][]' => $model->id,
                        ], [
                            'target' => '_blank',
                        ]);
                    } else {
                        return $model->countOrders;
                    }
                },
                'enableSorting' => false,
            ],
            [
                'label' => Yii::t('common', 'Накладная'),
                'headerOptions' => ['class' => 'text-center width-100'],
                'contentOptions' => ['class' => 'text-center td-with-icon'],
                'content' => function ($model) {
                    /** @var OrderLogisticList $model */
                    if ($model->isPretensionType()) {
                        return '—';
                    }
                    if ($model->countOrders) {
                        /** @var OrderLogisticList $model */
                        $icon = Html::tag('i', '', ['class' => 'font-size-16 fa ' . FontAwesome::FILE_TEXT]);

                        return Html::a($icon, '#', [
                            'class' => 'build-invoice-link text-info',
                            'data-list-id' => $model->id,
                            'data-count-orders' => $model->getCountOrders(),
                        ]);
                    } else {
                        return '—';
                    }
                },
                'enableSorting' => false,
                'visible' => Yii::$app->user->can('order.list.download'),
            ],
            [
                'attribute' => 'label',
                'headerOptions' => ['class' => 'text-center width-100'],
                'contentOptions' => ['class' => 'text-center td-with-icon'],
                'content' => function ($model) {
                    /** @var OrderLogisticList $model */
                    if ($model->isPretensionType()) {
                        return '—';
                    }
                    $icon = Html::tag('i', '', ['class' => 'font-size-16 fa ' . FontAwesome::FILE_IMAGE_O]);
                    $content = Html::a($icon, Url::toRoute(['download-label', 'id' => $model->id]), [
                        'class' => 'text-primary',
                    ]);
                    return ($model->delivery && $model->delivery->hasApiGenerateLabel()) ? $content : '';
                },
                'enableSorting' => false,
                'visible' => Yii::$app->user->can('order.list.download'),
            ],
            [
                'attribute' => 'ticket',
                'headerOptions' => ['class' => 'text-center width-100'],
                'contentOptions' => ['class' => 'text-center td-with-icon'],
                'content' => function ($model) {
                    $content = '';
                    if ($model->isPretensionType()) {
                        return $content;
                    }
                    if ($model->ticket) {
                        $icon = Html::tag('i', '', ['class' => 'font-size-16 fa ' . FontAwesome::FILE_IMAGE_O]);
                        $content = Html::a($icon, Url::toRoute(['download-ticket']) . '/' . $model->ticket, [
                            'class' => 'text-primary',
                            'data-plugin' => 'magnificPopup',
                        ]);
                    }

                    return $content;
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'barcodes',
                'label' => Yii::t('common', 'Штрих-коды'),
                'headerOptions' => ['class' => 'text-center width-100'],
                'contentOptions' => ['class' => 'text-center td-with-icon'],
                'content' => function ($model) {
                    $content = '';
                    if ($model->isPretensionType()) {
                        return $content;
                    }
                    if ($model->barcodes) {
                        $icon = Html::tag('i', '', ['class' => 'font-size-16 fa fa-file']);
                        $content = Html::a($icon, Url::toRoute(['download-barcodes']) . '/' . $model->id, [
                            'class' => 'text-primary',
                        ]);
                    }
                    return $content;
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'status',
                'content' => function ($model) use ($statuses) {
                    return Label::widget([
                        'label' => $statuses[$model->status] ?? '—',
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'type',
                'label' => Yii::t('common', 'Тип'),
                'content' => function ($model) use ($types) {
                    return Label::widget([
                        'label' => $types[$model->type] ?? '—',
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'label' => Yii::t('common', 'Заявки'),
                'content' => function ($model) use ($requestStatuses, $requestStatusesColors) {
                    $return = [];
                    foreach ($model->orderLogisticListRequests as $request) {
                        $return[] = Label::widget([
                            'title' => $requestStatuses[$request->status] ?? '—',
                            'label' => $request->email_to ?? '—',
                            'style' => $requestStatusesColors[$request->status] ?? Label::STYLE_DEFAULT
                        ]);
                    }
                    if ($return) {
                        return implode('<br>', $return);
                    } else {
                        return '—';
                    }
                },
                'enableSorting' => false,
            ],
            [
                'label' => Yii::t('common', 'Дата отправки'),
                'headerOptions' => ['class' => 'th-date text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    /** @var OrderLogisticList $model */
                    if ($model->lastDateSent) {
                        return Html::tag('span', Yii::$app->formatter->asDatetime($model->lastDateSent),
                            ['class' => 'text-success']);
                    } else {
                        return '—';
                    }
                },
                'enableSorting' => false,
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'created_at',
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Скачать лист'),
                        'style' => 'build-excel-link',
                        'attributes' => function ($model) {
                            /** @var OrderLogisticList $model */
                            return [
                                'data-list-id' => $model->id,
                                'data-count-orders' => $model->getCountOrders(),
                            ];
                        },
                        'url' => function () {
                            return Url::toRoute(["#"]);
                        },
                        'can' => function ($model) {
                            /** @var OrderLogisticList $model */
                            return Yii::$app->user->can('order.list.downloadexcel')
                                && $model->getCountOrders()
                                && $model->delivery
                                && !$model->delivery->hasApiGenerateList();
                        },
                    ],
                    [
                        'label' => Yii::t('common', 'Скачать лист'),
                        'url' => function ($model) {
                            return Url::toRoute(['download', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            /** @var OrderLogisticList $model */
                            return Yii::$app->user->can('order.list.download')
                                && $model->getCountOrders()
                                && $model->delivery
                                && $model->delivery->hasApiGenerateList();
                        },
                    ],
                    [
                        'label' => Yii::t('common', 'Скачать накладные'),
                        'style' => 'build-invoice-link',
                        'url' => function ($model) {
                            return Url::toRoute(['download-invoice', 'id' => $model->id]);
                        },
                        'attributes' => function ($model) {
                            return [
                                'data-list-id' => $model->id,
                                'data-count-orders' => $model->getCountOrders(),
                            ];
                        },
                        'can' => function ($model) {
                            return \Yii::$app->user->can('order.list.downloadinvoice') && $model->countOrders && $model->type == OrderLogisticList::TYPE_NORMAL;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Отправить лист'),
                        'style' => 'btn-generate-and-send-list',
                        'url' => function () {
                            return Url::toRoute(["#"]);
                        },
                        'attributes' => function ($model) {
                            /** @var OrderLogisticList $model */
                            return [
                                'data-list-id' => $model->id,
                                'data-skip-generating-list' => $model->delivery && !is_null($model->actualListExcel) && $model->delivery->hasApiGenerateList(),
                                'data-emails-count' => count($model->getEmailList()),
                                'data-emails-list' => implode(', ', $model->getEmailList()),
                                'data-has-api-generate-list' => $model->delivery && $model->delivery->hasApiGenerateList(),
                            ];
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('order.list.send') && $model->countOrders && $model->delivery && $model->getEmailList();
                        },
                    ],
                    [
                        'label' => Yii::t('common', 'Отправить заказы по АПИ'),
                        'url' => function ($model) {
                            return Url::toRoute(['send-orders-to-delivery', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            /** @var OrderLogisticList $model */
                            return Yii::$app->user->can('order.list.sendorderstodelivery') && $model->delivery && $model->delivery->canSendOrder() && $model->type == OrderLogisticList::TYPE_NORMAL;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Подтвердить отправку'),
                        'style' => 'confirm-sent-list',
                        'url' => function () {
                            return Url::toRoute(["#"]);
                        },
                        'attributes' => function ($model) {
                            return [
                                'data-href' => Url::toRoute([
                                    'confirm-sent',
                                    'list' => $model->id,
                                ]),
                            ];
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('order.list.confirmsent') && $model->countOrders && ($model->status == OrderLogisticList::STATUS_PENDING || $model->status == OrderLogisticList::STATUS_DOWNLOADED);
                        },
                    ],
                    [
                        'label' => Yii::t('common', 'Подтвердить принятие'),
                        'style' => 'confirm-received-list',
                        'url' => function () {
                            return Url::toRoute(["#"]);
                        },
                        'attributes' => function ($model) {
                            return [
                                'data-href' => Url::toRoute([
                                    'confirm-received',
                                    'list' => $model->id,
                                ]),
                            ];
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('order.list.confirmreceived') && $model->countOrders && $model->status == OrderLogisticList::STATUS_SENT;
                        },
                    ],
                    [
                        'label' => Yii::t('common', 'Логи'),
                        'url' => function ($model) {
                            return Url::toRoute(['/order/list/logs', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('order.list.logs');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить лист'),
                        'url' => function () {
                            return Url::toRoute(["#"]);
                        },
                        'style' => 'confirm-delete-list',
                        'attributes' => function ($model) {
                            return [
                                'data-href' => Url::toRoute([
                                    'delete-list',
                                    'list' => $model->id,
                                ]),
                                'data-text' => OrderLogisticList::deleteListWarningText($model),
                            ];
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('order.list.deletelist') && $model->status != OrderLogisticList::STATUS_BARCODING;
                        },
                    ],
                    [
                        'can' => function ($model) {
                            return
                                (
                                    (Yii::$app->user->can('order.list.download') && $model->countOrders)
                                    ||
                                    (Yii::$app->user->can('order.list.send') && $model->countOrders && empty($model->actualListExcel->sent_at))
                                ) && Yii::$app->user->can('order.list.details');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Детальная информация'),
                        'url' => function ($model) {
                            return Url::toRoute(['details', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('order.list.details');
                        },
                    ],
                ]
            ],

        ],
    ]),
    'footer' => LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>

<?= ExcelBuilder::widget() ?>

<?= InvoiceBuilder::widget([
    'model' => new OrderLogisticListInvoice([
        'template' => OrderLogisticListInvoice::TEMPLATE_TWO,
        'format' => OrderLogisticListInvoice::FORMAT_DOCS,
    ])
]) ?>

<?= ModalConfirmDeleteList::widget() ?>

<?= GenerateAndSendList::widget() ?>

<?= ModalConfirmSentList::widget() ?>

<?= ModalConfirmReceivedList::widget() ?>