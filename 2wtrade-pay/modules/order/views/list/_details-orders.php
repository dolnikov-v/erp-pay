<?php
use app\components\grid\ActionColumn;
use app\components\grid\GridView;
use app\components\grid\IdColumn;
use app\widgets\Label;
use yii\helpers\Url;

/** @var \app\modules\order\models\OrderLogisticList $list */
/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var array $statuses */
/** @var array $products */
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'class' => IdColumn::className(),
        ],
        [
            'label' => Yii::t('common', 'Статус'),
            'content' => function ($model) use ($statuses) {
                return Label::widget([
                    'label' => array_key_exists($model->status_id, $statuses) ? Yii::t('common', $statuses[$model->status_id]) : '—',
                ]);
            },
            'enableSorting' => false,
        ],
        [
            'label' => Yii::t('common', 'Полное имя'),
            'content' => function ($model) {
                return $model->customer_full_name;
            },
            'enableSorting' => false,
        ],
        [
            'label' => Yii::t('common', 'Адрес'),
            'content' => function ($model) {
                return $model->customer_address;
            },
            'enableSorting' => false,
        ],
        [
            'label' => Yii::t('common', 'Товары'),
            'content' => function ($model) use ($products) {
                return $this->render('_details-orders-products', [
                    'model' => $model,
                    'products' => $products,
                ]);
            },
            'enableSorting' => false,
        ],
        [
            'class' => ActionColumn::className(),
            'items' => [
                [
                    'label' => Yii::t('common', 'Удалить из списка'),
                    'url' => function () {
                        return '#';
                    },
                    'style' => 'confirm-delete-order',
                    'attributes' => function ($model) use ($list) {
                        return [
                            'data-href' => Url::toRoute(['delete-order',
                                'list' => $list->id,
                                'order' => $model->id,
                            ]),
                        ];
                    },
                    'can' => function () use ($list) {
                        return Yii::$app->user->can('order.list.deleteorder') && empty($list->actualListExcel->sent_at);
                    },
                ],
            ]
        ],
    ]
]) ?>
