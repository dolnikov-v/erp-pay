<?php
use app\widgets\Log;
use yii\helpers\Url;

/** @var \app\modules\order\models\OrderLogisticList $model */

/**
 * @var \app\models\logs\TableLog[] $change
 * @var \app\models\logs\TableLog[] $barcode
 * @var \app\models\logs\TableLog[] $excel
 * @var \app\models\logs\TableLog[] $invoice
 * @var \app\models\logs\TableLog[] $invoiceArchive
 * @var \app\models\logs\TableLog[] $request
 */

$this->title = Yii::t('common', 'История изменения листа {id}', ['id' => $model->id]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Заказы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Список листов'), 'url' => Url::toRoute('index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<div class="panel panel panel-bordered ">
    <ul class="panel nav nav-tabs" role="tablist">
        <li class="active"><a href="#tabChange" aria-controls="tabChange" role="tab" data-toggle="tab"><?=Yii::t('common', 'Основные')?></a></li>
        <li><a href="#tabOne" aria-controls="tabOne" role="tab" data-toggle="tab">Barcode</a></li>
        <li><a href="#tabTwo" aria-controls="tabTwo" role="tab" data-toggle="tab">Excel</a></li>
        <li><a href="#tabThree" aria-controls="tabThree" role="tab" data-toggle="tab">Invoice</a></li>
        <li><a href="#tabFour" aria-controls="tabFour" role="tab" data-toggle="tab">InvoiceArchive</a></li>
        <li><a href="#tabFive" aria-controls="tabFive" role="tab" data-toggle="tab">Request</a></li>
    </ul>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="tabChange">
            <?= Log::widget([
                'title' => Yii::t('common', 'История изменения'),
                'data' => $change,
            ]); ?>
        </div>
        <div role="tabpanel" class="tab-pane" id="tabOne">
            <?= Log::widget([
                'data' => $barcode,
            ]); ?>
        </div>
        <div role="tabpanel" class="tab-pane" id="tabTwo">
            <?= Log::widget([
                'data' => $excel,
            ]); ?>
        </div>
        <div role="tabpanel" class="tab-pane" id="tabThree">
            <?= Log::widget([
                'data' => $invoice,
            ]); ?>
        </div>
        <div role="tabpanel" class="tab-pane" id="tabFour">
            <?= Log::widget([
                'data' => $invoiceArchive,
            ]); ?>
        </div>
        <div role="tabpanel" class="tab-pane" id="tabFive">
            <?= Log::widget([
                'data' => $request,
            ]); ?>
        </div>
    </div>
</div>
