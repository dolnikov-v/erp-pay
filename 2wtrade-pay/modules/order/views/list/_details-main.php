<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var \app\modules\order\models\OrderLogisticList $list */
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute(['details', 'id' => $list->id]),
    'enableClientValidation' => false,
]); ?>

<div class="row">
    <div class="col-lg-3">
        <?= $form->field($list, 'created_at')->textInput([
            'value' => Yii::$app->formatter->asDate($list->created_at),
            'readonly' => 'readonly',
        ])->label(Yii::t('common', 'Дата отгрузки')) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($list, 'number')->textInput([
            'value' => str_pad($list->number, 2, '0', STR_PAD_LEFT),
            'readonly' => 'readonly',
        ])->label(Yii::t('common', 'Номер')) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($list, 'countOrders')->textInput([
            'value' => $list->getCountOrders(),
            'readonly' => 'readonly',
        ])->label(Yii::t('common', 'Количество заказов')) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($list, 'created_at')->textInput([
            'value' => Yii::$app->formatter->asDatetime($list->created_at),
            'readonly' => 'readonly',
        ])->label(Yii::t('common', 'Дата создания')) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
