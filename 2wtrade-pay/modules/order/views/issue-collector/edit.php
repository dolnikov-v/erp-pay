<?php

use app\widgets\Panel;

/** @var \yii\web\View $this */
/** @var \app\modules\order\models\IssueCollector $model */

$this->title = $model->isNewRecord ? Yii::t('common', 'Создание Issue collector') : Yii::t('common',
    'Редактирование Issue collector');

$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Issue collectors'), 'url' => 'index'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Список Issue collector'), 'url' => 'index'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

?>

<div class="create-issue-collector">
    <?= Panel::widget([
        'title' => Yii::t('common', 'Issue collector'),
        'content' => $this->render('_form', [
            'model' => $model
        ])
    ]); ?>
</div>
