<?php

use app\components\grid\ActionColumn;
use app\helpers\DataProvider;
use app\components\grid\GridView;
use app\widgets\assets\custom\ModalConfirmDeleteAsset;
use app\widgets\ButtonLink;
use app\widgets\custom\ModalConfirmDelete;
use app\widgets\LinkPager;
use app\widgets\Panel;
use yii\helpers\Url;

/** @var $this \yii\web\View */
/** @var $dataProvider \yii\data\ActiveDataProvider */

$this->title = Yii::t('common', 'Список Issue Collectors');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Issue Collectors'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ModalConfirmDeleteAsset::register($this);
?>
    <div class="issue-collector-index">
        <?= Panel::widget([
            'title' => Yii::t('common', 'Таблица с Issue Collectors'),
            'actions' => DataProvider::renderSummary($dataProvider),
            'withBody' => false,
            'content' => GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    [
                        'attribute' => 'id',
                        'contentOptions' => [
                        'style' => 'width:50px; word-wrap: break-word'
                        ],
                    ],
                    [
                        'attribute' => 'name',
                        'contentOptions' => [
                        ],
                    ],
                    [
                        'attribute' => 'jira_link',
                        'contentOptions' => [
                        'style' => 'max-width:450px; word-wrap: break-word'
                        ],
                    ],
                    [
                        'attribute' => 'form_link',
                        'contentOptions' => [
                        ],
                    ],
                    [
                        'class' => ActionColumn::className(),
                        'items' => [
                            [
                                'label' => Yii::t('common', 'Редактировать'),
                                'url' => function ($model) {
                                    return Url::toRoute(['edit', 'id' => $model->id]);
                                },
                                'can' => function () {
                                    return Yii::$app->user->can('order.issuecollector.edit');
                                }
                            ],
                            [
                                'label' => Yii::t('common', 'Удалить'),
                                'url' => function () {
                                    return '#';
                                },
                                'style' => 'confirm-delete-link',
                                'attributes' => function ($model) {
                                    return [
                                        'data-href' => Url::toRoute(['delete', 'id' => $model->id]),
                                    ];
                                },
                            'can' => function () {
                                return Yii::$app->user->can('order.issuecollector.edit');
                            }
                            ]
                        ]
                    ]
                ],
            ]),
            'footer' =>
                ButtonLink::widget([
                    'url' => Url::toRoute('edit'),
                    'label' => Yii::t('common', 'Добавить Issue Collector'),
                    'style' => ButtonLink::STYLE_SUCCESS,
                    'size' => ButtonLink::SIZE_SMALL,
                ]) . LinkPager::widget([
                    'pagination' => $dataProvider->getPagination(),
                    'counterPosition' => LinkPager::COUNTER_POSITION_RIGHT,
                ]),
        ]) ?>
    </div>

<?= ModalConfirmDelete::widget() ?>
