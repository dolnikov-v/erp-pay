<?php

use app\components\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \app\modules\order\models\IssueCollector $model */

?>

<?php $form = ActiveForm::begin([
    'method' => 'post',
]); ?>

<div class="row">
    <div class="col-lg-12">
        <?= $form->field($model, 'name')->textInput() ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <?= $form->field($model, 'jira_link')->textInput() ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('common', 'Добавить') : Yii::t('common', 'Сохранить'),
            ['class' => 'btn btn-success btn-sm']) ?>
        <?= Html::a(Yii::t('common', 'Отмена'), Url::toRoute('index'), [
            'class' => 'btn btn-default btn-sm '
        ]) ?>
    </div>
</div>

<?php ActiveForm::end() ?>
