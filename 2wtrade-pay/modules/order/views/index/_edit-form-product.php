<?php
use app\helpers\WbIcon;
use app\widgets\Button;
use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\components\widgets\ActiveForm $form */
/** @var app\modules\order\models\OrderProduct $model */
/** @var array $productList */
/** @var boolean $select2 */
/** @var boolean $isView */

$end = $model->isNewRecord ? '[]' : '[' . $model->id . ']';
?>

<div class="row-order-products-inputs">
    <?= Html::hiddenInput(Html::getInputName($model, 'id') . $end, $model->id) ?>

    <div class="col-sm-3">
        <?php if ($select2): ?>
            <?= $form->field($model, 'product_id')->select2List($productList, [
                'id' => false,
                'name' => Html::getInputName($model, 'product_id') . $end,
                'disabled' => $isView,
            ]) ?>
        <?php else: ?>
            <?= $form->field($model, 'product_id')->dropDownList($productList, [
                'id' => false,
                'name' => Html::getInputName($model, 'product_id') . $end,
                'data-minimum-results-for-search' => -1,
                'disabled' => $isView,
            ]) ?>
        <?php endif; ?>
    </div>
    <div class="col-sm-3">
        <?= $form->field($model, 'price')->textInput([
            'id' => false,
            'name' => Html::getInputName($model, 'price') . $end,
            'readonly' => $isView,
        ]) ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'quantity')->textInput([
            'id' => false,
            'name' => Html::getInputName($model, 'quantity') . $end,
            'readonly' => $isView,
        ]) ?>
    </div>
    <div class="col-sm-2">
        <div class="form-group">
            <div class="control-label">
                <label>&nbsp;</label>
            </div>
            <?php if (!$isView): ?>
                <?= Button::widget([
                    'type' => 'button',
                    'icon' => WbIcon::MINUS,
                    'style' => Button::STYLE_DANGER . ' btn-order-product btn-order-product-minus',
                ]) ?>
            <?php endif; ?>
        </div>
    </div>
</div>
