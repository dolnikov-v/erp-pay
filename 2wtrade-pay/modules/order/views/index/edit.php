<?php
use app\modules\order\assets\IndexAsset;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var app\modules\order\models\Order $model */
/** @var app\modules\order\models\OrderProduct[] $modelsOrderProduct */
/** @var array $productList */
/** @var array $orderStatusList */
/** @var \app\modules\callcenter\models\CallCenterRequest[] $modelCallCenterRequest */
/** @var array $callCenters */
/** @var array $deliveryServices */
/** @var \app\modules\delivery\models\DeliveryRequest $modelDeliveryRequest */

IndexAsset::register($this);

$this->title = Yii::t('common', $model->isNewRecord ? 'Создание заказа' : 'Изменение заказа');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Заказы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Список заказов'), 'url' => Url::toRoute('index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];

$options = [
    'model' => $model,
    'modelsOrderProduct' => $modelsOrderProduct,
    'productList' => $productList,
    'orderStatusList' => $orderStatusList,
    'callCenters' => $callCenters,
    'modelCallCenterRequest' => $modelCallCenterRequest,
    'deliveryServices' => $deliveryServices,
    'modelDeliveryRequest' => $modelDeliveryRequest,
    'isView' => false
];
?>

<?= $this->render('_info-about-orders', [
    'model' => $model,
    'options' => $options,
]); ?>


