<?php
/** @var array $adcomboResponse */

?>

<div class="row">
    <div class="col-lg-2">
        <h4><?= Yii::t('common', 'Время') ?></h4>
    </div>
    <div class="col-lg-6">
        <h4 class="text-center"><?= Yii::t('common', 'Input data') ?></h4>
    </div>
    <div class="col-lg-4">
        <h4><?= Yii::t('common', 'Output data') ?></h4>
    </div>
</div>

<hr>

<div class="row" style="word-wrap: break-word">
    <div class="col-lg-2">
        <div><?= Yii::$app->formatter->asDatetime($adcomboResponse['updated_at'], 'long'); ?></div>
    </div>
    <div class="col-lg-6">
        <div><?= $adcomboResponse['input_data'] ?></div>
    </div>
    <div class="col-lg-4">
        <div><?= $adcomboResponse['output_data'] ?></div>
    </div>
</div>
