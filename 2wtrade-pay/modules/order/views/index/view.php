<?php
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var app\modules\order\models\Order $model */
/** @var app\modules\order\models\OrderProduct[] $modelsOrderProduct */
/** @var array $productList */
/** @var array $orderStatusList */
/** @var \app\modules\order\models\OrderLog[] $logsGroup */
/** @var \app\modules\order\models\OrderLogProduct[] $logsProduct */
/** @var yii\data\ActiveDataProvider $breakpoints */
/** @var \app\modules\callcenter\models\CallCenterRequest[] $modelCallCenterRequest */
/** @var array $callCenters */
/** @var array $deliveryServices */
/** @var array $adcomboResponse */
/** @var array $callCenterResponse */
/** @var array $deliveryResponse */
/** @var \app\modules\delivery\models\DeliveryRequest $modelDeliveryRequest */

$this->title = Yii::t('common', 'Просмотр заказа');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Заказы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Список заказов'), 'url' => Url::toRoute('index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];

$options = [
    'model' => $model,
    'modelsOrderProduct' => $modelsOrderProduct,
    'productList' => $productList,
    'orderStatusList' => $orderStatusList,
    'callCenters' => $callCenters,
    'modelCallCenterRequest' => $modelCallCenterRequest,
    'deliveryServices' => $deliveryServices,
    'modelDeliveryRequest' => $modelDeliveryRequest,
    'isView' => true,
];
?>

<?= $this->render('_info-about-orders', [
    'model' => $model,
    'options' => $options,
    'logsGroup' => $logsGroup,
    'breakpoints' => $breakpoints,
    'logsProduct' => $logsProduct,
    'adcomboResponse' => $adcomboResponse,
    'callCenterResponse' => $callCenterResponse,
    'deliveryResponse' => $deliveryResponse,
]); ?>




