<?php
/** @var array $deliveryResponse */

?>
<h3 class="text-primary"><?= Yii::t('common', 'Отправка заказа') ?></h3>
<hr>
<div class="row">
    <div class="col-lg-4">
        <h4><?= Yii::t('common', 'Время') ?></h4>
    </div>
    <div class="col-lg-8">
        <h4><?= Yii::t('common', 'Текст') ?></h4>
    </div>
</div>
<div class="row"  style="word-wrap: break-word">
    <div class="col-lg-4">
        <div><?= Yii::$app->formatter->asDatetime($deliveryResponse['cs_send_response_at'], 'long'); ?></div>
    </div>
    <div class="col-lg-8">
        <div><?= htmlspecialchars($deliveryResponse['cs_send_response']) ?></div>
    </div>
</div>

<h3 class="text-primary"><?= Yii::t('common', 'Получение статусов') ?></h3>
<hr>

<div class="row">
    <div class="col-lg-4">
        <h4><?= Yii::t('common', 'Время') ?></h4>
    </div>
    <div class="col-lg-8">
        <h4><?= Yii::t('common', 'Текст') ?></h4>
    </div>
</div>
<div class="row"  style="word-wrap: break-word">
    <div class="col-lg-4">
        <div><?= Yii::$app->formatter->asDatetime($deliveryResponse['last_update_info_at'], 'long'); ?></div>
    </div>
    <div class="col-lg-8">
        <div><?= htmlspecialchars($deliveryResponse['last_update_info']) ?></div>
    </div>
</div>