<?php
use app\components\grid\DateColumn;
use app\components\grid\GridView;
use app\modules\order\models\OrderLogProduct;
use app\widgets\Label;
use app\widgets\Panel;

/** @var array $logsProduct */
?>

<?= Panel::widget([
    'border' => false,
    'content' => GridView::widget([
        'dataProvider' => $logsProduct,
        'columns' => [
            'product.name',
            [
                'attribute' => 'action',
                'enableSorting' => false,
                'content' => function ($model) {
                    $style = Label::STYLE_PRIMARY;
                    if ($model->action == OrderLogProduct::ACTION_CREATE) {
                        $style = Label::STYLE_SUCCESS;
                    } elseif ($model->action == OrderLogProduct::ACTION_DELETE) {
                        $style = Label::STYLE_DANGER;
                    }

                    return Label::widget([
                        'label' => array_key_exists($model->action, OrderLogProduct::getProductActions()) ? OrderLogProduct::getProductActions()[$model->action] : Yii::t('common', 'Неопределено'),
                        'style' => $style
                    ]);
                }
            ],
            [
                'attribute' => 'price',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'quantity',
                'enableSorting' => false,
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'created_at',
                'enableSorting' => false,
                'label' => Yii::t('common', 'Дата')
            ]
        ],
    ])
]) ?>
