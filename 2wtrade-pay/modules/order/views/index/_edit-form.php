<?php
use app\components\widgets\ActiveForm;
use app\modules\order\models\OrderProduct;
use yii\helpers\Url;
use app\widgets\ButtonProgress;
use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\modules\order\models\Order $model */
/** @var app\modules\order\models\OrderProduct[] $modelsOrderProduct */
/** @var array $productList */
/** @var array $orderStatusList */
/** @var boolean $isView */
/** @var \app\modules\callcenter\models\CallCenterRequest[] $modelCallCenterRequest */
/** @var array $callCenters */
/** @var array $deliveryServices */
/** @var \app\modules\delivery\models\DeliveryRequest $modelDeliveryRequest */
?>

<?php $form = ActiveForm::begin([
    'action' => $model->isNewRecord ? Url::toRoute('edit') : Url::toRoute(['edit', 'id' => $model->id]),
    'enableClientValidation' => false,
]); ?>

<div class="row">
    <?php if (Yii::$app->user->can('order.index.edit.main')) : ?>
    <?= $this->render('_edit-form-info-main', [
        'model' => $model,
        'form' => $form,
        'orderStatusList' => $orderStatusList,
        'isView' => $isView,
    ]) ?>
    <?php endif; ?>

    <?php if (Yii::$app->user->can('order.index.edit.customer')) : ?>
    <?= $this->render('_edit-form-info-customer', [
        'model' => $model,
        'form' => $form,
        'isView' => $isView,
    ]) ?>
    <?php endif; ?>

    <?php if (Yii::$app->user->can('order.index.edit.products')) : ?>
    <?= $this->render('_edit-form-info-products', [
        'model' => $model,
        'modelsOrderProduct' => $modelsOrderProduct,
        'form' => $form,
        'productList' => $productList,
        'isView' => $isView,
    ]) ?>
    <?php endif; ?>

    <?php if (Yii::$app->user->can('order.index.edit.cc')) : ?>
    <?= $this->render('_edit-form-info-call-center', [
        'modelCallCenterRequest' => $modelCallCenterRequest,
        'modelOrder' => $model,
        'form' => $form,
        'callCenters' => $callCenters,
        'isView' => $isView,
    ]) ?>
    <?php endif; ?>

    <?php if (Yii::$app->user->can('order.index.edit.delivery')) : ?>
    <?= $this->render('_edit-form-info-delivery', [
        'modelDeliveryRequest' => $modelDeliveryRequest,
        'model' => $model,
        'form' => $form,
        'deliveryServices' => $deliveryServices,
        'isView' => $isView,
    ]) ?>
    <?php endif; ?>
</div>

<div class="row">
    <div class="col-md-4">
        <?php if (!$isView) : ?>
            <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Создать заказ') : Yii::t('common', 'Изменить заказ')) ?>
        <?php endif; ?>
        <?= $form->link(Yii::t('common', $isView ? 'Вернуться назад' : 'Отмена'), Url::toRoute('index')); ?>
    </div>

    <div class="col-md-4">
        <?php if (!$isView && Yii::$app->user->can('order.index.view')): ?>
            <?= $form->link(Yii::t('common', 'Просмотр заказа'), Url::toRoute(['view', 'id' => $model->id])); ?>
        <?php endif; ?>
        <?php if ($isView && Yii::$app->user->can('order.index.edit')): ?>
            <?= $form->link(Yii::t('common', 'Изменение заказа'), Url::toRoute(['edit', 'id' => $model->id])); ?>
        <?php endif; ?>
    </div>

    <div class="col-md-4">
        <?php if (Yii::$app->user->can('order.index.sendindelivery')): ?>
            <div class="margin-bottom-5 text-right">
                <?= ButtonProgress::widget([
                    'id' => 'btn_sender_in_delivery',
                    'style' => ButtonProgress::STYLE_PRIMARY . ' width-400',
                    'label' => Yii::t('common', 'Передать в службу доставки'),
                ]) ?>
            </div>
        <?php endif; ?>

        <?php if (Yii::$app->user->can('order.index.resendindelivery')): ?>
            <div class="margin-bottom-5 text-right">
                <?= ButtonProgress::widget([
                    'id' => 'btn_resender_in_delivery',
                    'style' => ButtonProgress::STYLE_PRIMARY . ' width-400',
                    'label' => Yii::t('common', 'Повторно передать в службу доставки'),
                ]) ?>
            </div>
        <?php endif; ?>
    </div>

</div>

<?php if ($model->isNewRecord) { ?>

    <?=Html::hiddenInput('tmp_token', uniqid('tmp'));?>

<?php } ?>

<?php ActiveForm::end(); ?>

<div id="order_products_inputs" style="display: none;">
    <?= $this->render('_edit-form-product', [
        'form' => $form,
        'model' => new OrderProduct(),
        'productList' => $productList,
        'select2' => false,
        'isView' => $isView,
    ]) ?>
</div>
