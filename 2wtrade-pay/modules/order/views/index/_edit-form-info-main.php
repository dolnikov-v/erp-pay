<?php

use app\models\Currency;
use app\modules\order\models\OrderStatus;

/** @var app\modules\order\models\Order $model */
/** @var app\components\widgets\ActiveForm $form */
/** @var array $orderStatusList */
/** @var boolean $isView */
?>

<div class="col-lg-12">
    <h4><?= Yii::t('common', 'Основная информация') ?></h4>
</div>
<div class="col-lg-4">
    <?= $form->field($model, 'country_id')->textInput([
        'value' => Yii::t('common', Yii::$app->user->country->name),
        'name' => 'country_name',
        'readonly' => true,
    ]) ?>
</div>
<div class="col-lg-4">
    <?= $form->field($model, 'status_id')->select2List($orderStatusList, [
        'autoEllipsis' => true,
        'disabled' => $isView,
    ]) ?>
</div>
<div class="col-lg-4">
    <?php if ($isView): ?>
        <?= $form->field($model, 'landing_id')->textInput(['value' => $model->landing ? $model->landing->url : '', 'disabled' => true]) ?>
    <?php endif; ?>
</div>

<?php if (!$model->isNewRecord): ?>
    <div class="col-lg-4">
        <?= $form->field($model, 'source_id')->select2List(\app\models\Source::find()->collection(), [
            'prompt' => '—',
            'disabled' => $isView
        ]) ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'foreign_id')->textInput(['readonly' => $isView]) ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'id')->textInput([
            'readonly' => true,
        ]) ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'delivery_time_from')->datePicker([
                'disabled' => $isView || !in_array($model->status_id, [OrderStatus::STATUS_CC_APPROVED, OrderStatus::STATUS_DELIVERY_REJECTED]),
                'showTime' => true
        ]) ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'delivery')->textInput([
            'readonly' => $isView,
            'placeholder' => 'Delivery price'
        ]) ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'price_currency')->select2List(Currency::find()->collection(), [
            'disabled' => $isView,
        ]) ?>
    </div>

<?php endif; ?>