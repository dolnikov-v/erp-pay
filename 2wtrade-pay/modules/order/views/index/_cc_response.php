<?php
/** @var array $callCenterResponse */

?>

<h3 class="text-primary"><?= Yii::t('common', 'Отправка заказа') ?></h3>
<hr>
<div class="row">
    <div class="col-lg-4">
        <h4><?= Yii::t('common', 'Время') ?></h4>
    </div>
    <div class="col-lg-8">
        <h4><?= Yii::t('common', 'Текст') ?></h4>
    </div>
</div>
<div class="row"  style="word-wrap: break-word">
    <div class="col-lg-4">
        <div><?= Yii::$app->formatter->asDatetime($callCenterResponse['cc_send_response_at'], 'long'); ?></div>
    </div>
    <div class="col-lg-8">
        <?php if ($callCenterResponse['cc_send_request']): ?>
            <div><b><?=Yii::t('common', 'Запрос')?>:</b> <?= htmlspecialchars($callCenterResponse['cc_send_request']) ?></div>
        <?php endif; ?>
        <div><b><?=Yii::t('common', 'Ответ')?>:</b> <?= htmlspecialchars($callCenterResponse['cc_send_response']) ?></div>
    </div>
</div>

<h3 class="text-primary"><?= Yii::t('common', 'Получение статусов') ?></h3>
<hr>

<div class="row">
    <div class="col-lg-4">
        <h4><?= Yii::t('common', 'Время') ?></h4>
    </div>
    <div class="col-lg-8">
        <h4><?= Yii::t('common', 'Текст') ?></h4>
    </div>
</div>
<div class="row"  style="word-wrap: break-word">
    <div class="col-lg-4">
        <div><?= Yii::$app->formatter->asDatetime($callCenterResponse['cc_update_response_at'], 'long'); ?></div>
    </div>
    <div class="col-lg-8">
        <div><?= htmlspecialchars($callCenterResponse['cc_update_response']) ?></div>
    </div>
</div>
