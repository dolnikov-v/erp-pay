<?php
use app\helpers\WbIcon;
use app\modules\order\models\OrderProduct;
use app\widgets\Button;

/** @var app\modules\order\models\Order $model */
/** @var app\modules\order\models\OrderProduct[] $modelsOrderProduct */
/** @var app\components\widgets\ActiveForm $form */
/** @var array $productList */
/** @var boolean $isView */
?>

    <div class="col-lg-12 margin-top-20">
        <h4><?= Yii::t('common', 'Информация о товарах') ?></h4>
    </div>

    <div id="order_products_content">
        <?php if (empty($modelsOrderProduct)): ?>
            <?= $this->render('_edit-form-product', [
                'form' => $form,
                'model' => new OrderProduct(),
                'productList' => $productList,
                'select2' => true,
                'isView' => $isView,
            ]) ?>
        <?php else: ?>
            <?php foreach ($modelsOrderProduct as $key => $modelOrderProduct): ?>
                <?= $this->render('_edit-form-product', [
                    'form' => $form,
                    'model' => $modelOrderProduct,
                    'productList' => $productList,
                    'select2' => true,
                    'isView' => $isView,
                ]) ?>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
<?php if (!$isView): ?>
    <div class="col-lg-12 margin-bottom-20">
        <?= Button::widget([
            'type' => 'button',
            'icon' => WbIcon::PLUS,
            'style' => Button::STYLE_SUCCESS . ' btn-order-product btn-order-product-plus',
        ]) ?>
    </div>
<?php endif; ?>
