<?php
use app\components\grid\GridView;
use app\components\grid\CustomCheckboxColumn;
use app\widgets\custom\Checkbox;

/** @var app\modules\order\widgets\ShowerColumns $showerColumns */
/** @var yii\data\ActiveDataProvider $dataProvider */
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'tableOptions' => [
        'id' => 'orders_content',
    ],
    'columns' => [
        [
            'class' => CustomCheckboxColumn::className(),
            'content' => function ($model) {
                return Checkbox::widget([
                    'name' => 'id[]',
                    'value' => $model->id,
                    'style' => 'checkbox-inline',
                    'label' => true,
                ]);
            },
        ],
        [
            'attribute' => 'id',
            'enableSorting' => false,
            'label' => '#',
            'headerOptions' => ['class' => 'th-custom-record-id text-center'],
            'contentOptions' => ['class' => 'td-custom-record-id text-center'],
        ],
        [
            'attribute' => 'callCenterRequest.foreign_id',
            'enableSorting' => false,
            'visible' => true,
            'content' => function ($model) {
                if ($model->callCenterRequest) {
                    return $model->callCenterRequest->foreign_id ? $model->callCenterRequest->foreign_id : '—';
                } else {
                    return '—';
                }
            },
        ],
        [
            'label' => Yii::t('common', 'Трекер'),
            'enableSorting' => false,
            'visible' => true,
            'content' => function ($model) {
                if ($model->deliveryRequest && $model->deliveryRequest->tracking) {
                    return $model->deliveryRequest->tracking;
                } else {
                    return '—';
                }
            },
        ],
        [
            'attribute' => 'customer_full_name',
            'enableSorting' => false,
            'visible' => true,
        ],
        [
            'attribute' => 'customer_phone',
            'enableSorting' => false,
            'visible' => true,
        ]
    ]
]); ?>

