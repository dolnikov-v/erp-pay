<?php

use app\modules\callcenter\models\CallCenterCheckRequest;
use app\widgets\Label;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var \app\modules\order\models\Order $model */
?>

<?php foreach ($model->callCenterCheckRequests as $checkRequest): ?>
<?php switch ($checkRequest->status) {
        case CallCenterCheckRequest::STATUS_ERROR:
            $style = Label::STYLE_DANGER;
            break;
        case CallCenterCheckRequest::STATUS_PENDING:
            $style = Label::STYLE_PRIMARY;
            break;
        case CallCenterCheckRequest::STATUS_IN_PROGRESS:
            $style = Label::STYLE_INFO;
            break;
        default:
            $style = Label::STYLE_DEFAULT;
    }
    $label = Label::widget([
        'label' => CallCenterCheckRequest::getTypes()[$checkRequest->type],
        'style' => $style,
    ]);
?>
    <div>
        <?=
        Html::a($label, Url::toRoute([
            '/call-center/checking-request/index',
            'NumberFilter' => [
                'number' => $model->id,
            ],
            'CheckingTypeFilter' => [
                'type' => $checkRequest->type,
            ],
        ]), [
            'class' => 'link-label'
        ])
        ?>
    </div>
<?php endforeach; ?>