<?php
use app\helpers\DataProvider;
use app\modules\order\assets\IndexAsset;
use app\modules\order\assets\OrderFiltersAsset;
use app\modules\order\assets\ChangeStatusByLimitedUserAsset;
use app\assets\vendor\BootstrapLaddaAsset;
use app\modules\order\widgets\orderFilters\OrderFilters;
use app\widgets\LinkPager;
use app\widgets\ButtonProgress;
use app\modules\order\widgets\ChangeStatusByLimitedUser;
use yii\helpers\Url;
use app\widgets\Panel;
use yii\web\View;

/** @var yii\web\View $this */
/** @var app\modules\order\models\search\OrderSearch $modelSearch */
/** @var yii\data\ActiveDataProvider $dataProvider */
/** @var string $alert */
/** @var mixed $alertStyle */


IndexAsset::register($this);
OrderFiltersAsset::register($this);
ChangeStatusByLimitedUserAsset::register($this);
BootstrapLaddaAsset::register($this);

$this->registerJs("var I18n = {};", View::POS_HEAD);
$this->registerJs("I18n['Необходимо выбрать заказы.'] = '" . Yii::t('common', 'Необходимо выбрать заказы.') . "';",
    View::POS_HEAD);
$this->registerJs("I18n['Не удалось сменить статус.'] = '" . Yii::t('common', 'Не удалось сменить статус.') . "';",
    View::POS_HEAD);
$this->registerJs("I18n['Смена статусов завершена.'] = '" . Yii::t('common', 'Смена статусов успешно завершена.') . "';",
    View::POS_HEAD);

$this->title = Yii::t('common', 'Список заказов');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Заказы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= OrderFilters::widget([
    'modelSearch' => $modelSearch,
]);
?>

<?= Panel::widget([
        'title' => Yii::t('common', 'Групповые операции'),
        'content' => ButtonProgress::widget([
            'id' => 'btn_change_status_by_limited_user',
            'style' => ButtonProgress::STYLE_WARNING . ' width-200',
            'label' => Yii::t('common', 'Изменить статус заказов'),
        ])
    ]);
?>

<?= ChangeStatusByLimitedUser::widget([
        'url' => Url::toRoute('change-status-by-limited-user'),
    ]);
?>

<?= Panel::widget([
        'title' => Yii::t('common', 'Таблица с заказами'),
        'alert' => $alert,
        'alertStyle' => $alertStyle,
        'actions' => DataProvider::renderSummary($dataProvider),
        'withBody' => false,
        'content' => $this->render('_index-limited-content', [
            'dataProvider' => $dataProvider,
            'products' => $products,
        ]),
        'footer' => LinkPager::widget([
            'pagination' => $dataProvider->getPagination(),
            'counterPosition' => LinkPager::COUNTER_POSITION_RIGHT,
        ]),
    ]);
?>
