<?php

use app\components\grid\ActionColumn;
use app\components\grid\CustomCheckboxColumn;
use app\components\grid\DateColumn;
use app\components\grid\GridView;
use app\helpers\FontAwesome;
use app\models\Currency;
use app\modules\callcenter\models\CallCenterCheckRequest;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\models\Order;
use app\modules\order\models\OrderFinancePretension;
use app\modules\order\models\OrderLogisticList;
use app\modules\order\models\OrderLogisticListInvoice;
use app\modules\order\widgets\InvoicePrint;
use app\modules\order\widgets\ShowerColumns;
use app\widgets\custom\Checkbox;
use app\widgets\Label;
use yii\helpers\Html;
use yii\helpers\Url;


/** @var app\modules\order\widgets\ShowerColumns $showerColumns */
/** @var yii\data\ActiveDataProvider $dataProvider */
/** @var array $statuses */
/** @var array $products */
/** @var array $landings */
/** @var array $deliveries */
/** @var array $callCenters */

$usd = Currency::getUSD();
$currencies = Currency::find()->customCollection('id', 'char_code');
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'tableOptions' => [
        'id' => 'orders_content',
    ],
    'columns' => [
        [
            'class' => CustomCheckboxColumn::className(),
            'content' => function ($model) {
                return Checkbox::widget([
                    'name' => 'id[]',
                    'value' => $model->id,
                    'style' => 'checkbox-inline',
                    'label' => true,
                ]);
            },
        ],
        [
            'attribute' => 'id',
            'label' => '#',
            'headerOptions' => ['class' => 'th-custom-record-id text-center'],
            'contentOptions' => ['class' => 'td-custom-record-id text-center'],
        ],
        [
            'label' => Yii::t('common', 'Источник'),
            'attribute' => 'sourceModel.name',
            'enableSorting' => false,
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_SOURCE),
        ],
        [
            'attribute' => 'foreign_id',
            'enableSorting' => false,
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_FOREIGN_ID),
            'content' => function ($model) {
                return Html::tag('span', $model->foreign_id,
                    ['class' => $model->source_confirmed == 1 ? 'text-success' : ""]);
            },
        ],
        [
            'attribute' => 'callCenterRequest.foreign_id',
            'enableSorting' => false,
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_CALL_CENTER_FOREIGN_ID),
            'content' => function ($model) {
                if ($model->callCenterRequest) {
                    return $model->callCenterRequest->foreign_id ? $model->callCenterRequest->foreign_id : '—';
                } else {
                    return '—';
                }
            },
        ],
        [
            'label' => Yii::t('common', 'Лист'),
            'enableSorting' => false,
            'format' => 'html',
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_ORDER_LOGISTIC_LIST_ID),
            'content' => function ($model) {
                $list = OrderLogisticList::firstListOrder($model->id);
                if ($list) {
                    return Html::a(
                        $list,
                        [
                            '/order/list/details',
                            'id' => $list
                        ],
                        [
                            'target' => '_blank',
                        ]
                    );
                } else {
                    return '—';
                }
            },
        ],
        [
            'label' => Yii::t('common', 'Заявка в колл-центре'),
            'content' => function ($model) {
                if (Yii::$app->user->can('callcenter.request.index') && $model->callCenterRequest) {
                    $style = Label::STYLE_DANGER;

                    if ($model->callCenterRequest->status == CallCenterRequest::STATUS_PENDING) {
                        $style = Label::STYLE_PRIMARY;
                    } elseif ($model->callCenterRequest->status == CallCenterRequest::STATUS_IN_PROGRESS) {
                        $style = Label::STYLE_INFO;
                    } elseif ($model->callCenterRequest->status == CallCenterRequest::STATUS_DONE) {
                        $style = Label::STYLE_DEFAULT;
                    }

                    $label = Label::widget([
                        'label' => CallCenterRequest::getStatusesCollection()[$model->callCenterRequest->status],
                        'style' => $style,
                    ]);

                    return Html::a($label, Url::toRoute([
                        '/call-center/request/index',
                        'CallCenterRequestSearch' => [
                            'order_id' => $model->id,
                        ],
                    ]), [
                        'class' => 'link-label'
                    ]);
                } else {
                    return Label::widget([
                        'label' => Yii::t('common', 'Отсутствует'),
                        'style' => Label::STYLE_DEFAULT,
                    ]);
                }
            },
            'enableSorting' => false,
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_CALL_CENTER_ID),
        ],
        [
            'label' => Yii::t('common', 'Ошибка КЦ'),
            'enableSorting' => false,
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_CALL_CENTER_API_ERROR),
            'content' => function ($model) {
                if ($model->callCenterRequest && $model->callCenterRequest->api_error) {
                    return $model->callCenterRequest->api_error;
                } else {
                    return '—';
                }
            },
        ],
        [
            'label' => Yii::t('common', 'Колл-центр'),
            'enableSorting' => false,
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_CALL_CENTER_NAME),
            'content' => function ($model) use ($callCenters) {
                if ($model->callCenterRequest && $model->callCenterRequest->call_center_id) {
                    return array_key_exists($model->callCenterRequest->call_center_id,
                        $callCenters) ? $callCenters[$model->callCenterRequest->call_center_id] : '—';
                } else {
                    return '—';
                }
            },
        ],
        [
            'label' => Yii::t('common', 'Заявка в службе доставки'),
            'content' => function ($model) {
                if (Yii::$app->user->can('delivery.request.index') && $model->deliveryRequest) {
                    $style = Label::STYLE_DANGER;

                    if ($model->deliveryRequest->status == DeliveryRequest::STATUS_PENDING) {
                        $style = Label::STYLE_PRIMARY;
                    } elseif ($model->deliveryRequest->status == DeliveryRequest::STATUS_IN_PROGRESS) {
                        $style = Label::STYLE_INFO;
                    } elseif ($model->deliveryRequest->status == DeliveryRequest::STATUS_DONE) {
                        $style = Label::STYLE_DEFAULT;
                    }

                    $label = Label::widget([
                        'label' => DeliveryRequest::getStatusesCollection()[$model->deliveryRequest->status],
                        'style' => $style,
                    ]);

                    return Html::a($label, Url::toRoute([
                        '/delivery/request/index',
                        'DeliveryRequestSearch' => [
                            'order_id' => $model->id,
                        ],
                    ]), [
                        'class' => 'link-label'
                    ]);
                } else {
                    return Label::widget([
                        'label' => Yii::t('common', 'Отсутствует'),
                        'style' => Label::STYLE_DEFAULT,
                    ]);
                }
            },
            'enableSorting' => false,
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_DELIVERY_ID),
        ],
        [
            'label' => Yii::t('common', 'Ошибка курьерки'),
            'enableSorting' => false,
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_DELIVERY_API_ERROR),
            'content' => function ($model) {
                if ($model->deliveryRequest && $model->deliveryRequest->api_error) {
                    return htmlspecialchars($model->deliveryRequest->api_error);
                } else {
                    return '—';
                }
            },
        ],
        [
            'label' => Yii::t('common', 'Служба доставки'),
            'enableSorting' => false,
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_DELIVERY_NAME),
            'content' => function ($model) use ($deliveries) {
                if ($model->deliveryRequest && $model->deliveryRequest->delivery_id) {
                    return array_key_exists($model->deliveryRequest->delivery_id,
                        $deliveries) ? $deliveries[$model->deliveryRequest->delivery_id] : '—';
                } elseif ($model->pending_delivery_id) {
                    return array_key_exists($model->pending_delivery_id,
                        $deliveries) ? ($deliveries[$model->pending_delivery_id]) . '*' : '—';
                } else {
                    return '—';
                }
            },
        ],
        [
            'label' => Yii::t('common', 'Партнер КС'),
            'attribute' => 'deliveryRequest.deliveryPartner.name',
            'enableSorting' => false,
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_DELIVERY_PARTNER_NAME),
        ],
        [
            'label' => Yii::t('common', 'Трекер'),
            'enableSorting' => false,
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_DELIVERY_TRACKING),
            'content' => function ($model) {
                if ($model->deliveryRequest && $model->deliveryRequest->tracking) {
                    return $model->deliveryRequest->tracking;
                } else {
                    return '—';
                }
            },
        ],
        [
            'label' => Yii::t('common', 'Этикетка'),
            'enableSorting' => false,
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_DELIVERY_LABEL),
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'content' => function ($model) {
                if ($model->deliveryRequest && $model->deliveryRequest->delivery && $model->deliveryRequest->delivery->hasApiGenerateLabelByOrder()) {
                    $icon = Html::tag('i', '', ['class' => 'font-size-16 fa ' . FontAwesome::FILE_IMAGE_O]);
                    $content = Html::a($icon, Url::toRoute(['download-label', 'id' => $model->id]), [
                        'class' => 'text-primary',
                    ]);
                    return $content;
                }
                return '—';
            },
        ],
        [
            'label' => Yii::t('common', 'Трекер партнера КС'),
            'enableSorting' => false,
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_PARTNER_TRACKER),
            'content' => function ($model) {
                if ($model->deliveryRequest && $model->deliveryRequest->finance_tracking) {
                    return $model->deliveryRequest->finance_tracking;
                } else {
                    return '—';
                }
            },
        ],
        [
            'attribute' => 'status_id',
            'content' => function ($model) use ($statuses) {
                return Label::widget([
                    'label' => array_key_exists($model->status_id, $statuses) ? $statuses[$model->status_id] : '—',
                ]);
            },
            'enableSorting' => false,
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_STATUS_ID),
        ],
        [
            'attribute' => 'pretension',
            'label' => Yii::t('common', 'Претензия'),
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_PRETENSION),
            'content' => function ($model) {
                /**
                 * @var Order $model
                 */
                if ($model->orderFinancePretensions) {
                    $return = [];
                    foreach ($model->orderFinancePretensions as $pretension) {
                        $label = OrderFinancePretension::getTypeCollecion()[$pretension->type] ?? '';
                        if ($label) {
                            $return[] = \app\widgets\Label::widget([
                                'label' => $label,
                                'style' => Label::STYLE_PRIMARY,
                            ]);
                        }
                    }
                    return implode('<br />', $return);
                }
                return '';
            }
        ],
        [
            'attribute' => 'duplicate_order_id',
            'enableSorting' => false,
            'headerOptions' => ['class' => 'th-custom-record-id text-center'],
            'contentOptions' => ['class' => 'td-custom-record-id text-center'],
            'content' => function ($model) {
                $url = Html::a($model['duplicate_order_id'],
                    Url::toRoute(['view', 'id' => $model['duplicate_order_id']]),
                    ['target' => '_blank']);
                return $model['duplicate_order_id'] > 0 ? $url : '';
            },
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::DUPLICATE_ORDER_ID)
        ],
        [
            'attribute' => 'originalOrder.id',
            'enableSorting' => false,
            'headerOptions' => ['class' => 'th-custom-record-id text-center'],
            'contentOptions' => ['class' => 'td-custom-record-id text-center'],
            'content' => function ($model) {
                if ($model->originalOrder) {
                    $url = Html::a($model->originalOrder->id, Url::toRoute(['view', 'id' => $model->originalOrder->id]),
                        ['target' => '_blank']);
                    return $url;
                }
                return '';
            },
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::ORIGINAL_ORDER_ID)
        ],
        [
            'label' => Yii::t('common', 'Товары'),
            'content' => function ($model) use ($products) {
                return $this->render('_index-content-products', [
                    'model' => $model,
                    'products' => $products,
                ]);
            },
            'enableSorting' => false,
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_PRODUCTS),
        ],
        [
            'attribute' => 'customer_full_name',
            'enableSorting' => false,
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_CUSTOMER_FULL_NAME),
        ],
        [
            'attribute' => 'customer_address',
            'enableSorting' => false,
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_CUSTOMER_ADDRESS),
            'value' => function ($model) {
                if (Yii::$app->user->can('order.customer_address')) {
                    return $model->customer_address;
                }
                return null;
            },
        ],
        [
            'attribute' => 'customer_address_additional',
            'enableSorting' => false,
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_CUSTOMER_ADDRESS_ADDITIONAL),
            'value' => function ($model) {
                if (Yii::$app->user->can('order.customer_address_additional')) {
                    return $model->customer_address_additional;
                }
                return null;
            },
        ],
        [
            'attribute' => 'customer_zip',
            'enableSorting' => false,
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_CUSTOMER_ZIP),
            'value' => function ($model) {
                if (Yii::$app->user->can('order.customer_zip')) {
                    return $model->customer_zip;
                }
                return null;
            },
        ],
        [
            'attribute' => 'customer_city',
            'enableSorting' => false,
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_CUSTOMER_CITY),
            'value' => function ($model) {
                if (Yii::$app->user->can('order.customer_city')) {
                    return $model->customer_city;
                }
                return null;
            },
        ],
        [
            'attribute' => 'customer_province',
            'enableSorting' => false,
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_CUSTOMER_PROVINCE),
            'value' => function ($model) {
                if (Yii::$app->user->can('order.customer_province')) {
                    return $model->customer_province;
                }
                return null;
            },
        ],
        [
            'attribute' => 'customer_district',
            'enableSorting' => false,
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_CUSTOMER_DISTRICT),
            'value' => function ($model) {
                if (Yii::$app->user->can('order.customer_district')) {
                    return $model->customer_district;
                }
                return null;
            },
        ],
        [
            'attribute' => 'customer_city_code',
            'enableSorting' => false,
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_CUSTOMER_CITY_CODE),
            'value' => function ($model) {
                if (Yii::$app->user->can('order.customer_city_code')) {
                    return $model->customer_city_code;
                }
                return null;
            },
        ],
        [
            'attribute' => 'customer_subdistrict',
            'enableSorting' => false,
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_CUSTOMER_SUBDISTRICT),
            'value' => function ($model) {
                if (Yii::$app->user->can('order.customer_subdistrict')) {
                    return $model->customer_subdistrict;
                }
                return null;
            },
        ],
        [
            'label' => Yii::t('common', 'Улица и дом заказчика'),
            'enableSorting' => false,
            'content' => function ($model) {
                return $model->customer_street . ' ' . $model->customer_house_number;
            },
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_CUSTOMER_STREET_HOUSE),
            'value' => function ($model) {
                if (Yii::$app->user->can('order.customer_street_house')) {
                    return $model->customer_street_house;
                }
                return null;
            },
        ],
        [
            'attribute' => 'customer_phone',
            'enableSorting' => false,
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_CUSTOMER_PHONE),
            'value' => function ($model) {
                if (Yii::$app->user->can('order.customer_phone')) {
                    return $model->customer_phone;
                }
                return null;
            },
        ],
        [
            'attribute' => 'customer_mobile',
            'enableSorting' => false,
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_CUSTOMER_MOBILE),
            'value' => function ($model) {
                if (Yii::$app->user->can('order.customer_mobile')) {
                    return $model->customer_mobile;
                }
                return null;
            },
        ],
        [
            'attribute' => 'customer_email',
            'enableSorting' => false,
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_CUSTOMER_EMAIL),
            'value' => function ($model) {
                if (Yii::$app->user->can('order.customer_email')) {
                    return $model->customer_email;
                }
                return null;
            },
        ],
        [
            'attribute' => 'delivery_time_from',
            'class' => DateColumn::className(),
            'formatType' => 'Date',
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_DELIVERY_DATE),
        ],
        [
            'attribute' => 'comment',
            'enableSorting' => false,
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_COMMENT),
        ],
        [
            'attribute' => 'callCenterRequest.comment',
            'enableSorting' => false,
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_CALL_CENTER_COMMENT),
            'content' => function ($model) {
                if ($model->callCenterRequest) {
                    return $model->callCenterRequest->comment ? $model->callCenterRequest->comment : '—';
                } else {
                    return '—';
                }
            },
        ],
        [
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'attribute' => 'payment_type',
            'enableSorting' => false,
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_PAYMENT_TYPE)
        ],
        [
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'attribute' => 'payment_amount',
            'enableSorting' => false,
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_PAYMENT_AMOUNT)
        ],
        [
            'attribute' => 'callCenterRequest.sent_at',
            'class' => DateColumn::className(),
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_CALL_CENTER_SENT_AT),
            'content' => function ($model) {
                if ($model->callCenterRequest) {
                    return $model->callCenterRequest->sent_at ? $model->callCenterRequest->sent_at : '—';
                } else {
                    return '—';
                }
            },
        ],
        [
            'attribute' => 'callCenterRequest.approved_at',
            'class' => DateColumn::className(),
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_CALL_CENTER_APPROVED_AT),
            'content' => function ($model) {
                if ($model->callCenterRequest) {
                    return $model->callCenterRequest->approved_at ? $model->callCenterRequest->approved_at : '—';
                } else {
                    return '—';
                }
            },
        ],
        [
            'attribute' => 'price',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'enableSorting' => false,
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_PRICE),
        ],
        [
            'label' => Yii::t('common', 'Конечная цена'),
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'enableSorting' => false,
            'content' => function ($model) {
                return $model->price_total + $model->delivery;
            },
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_PRICE_TOTAL_WITH_DELIVERY),
        ],
        [
            'attribute' => 'delivery',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'enableSorting' => false,
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_DELIVERY),
        ],
        [
            'attribute' => 'price_currency',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'enableSorting' => false,
            'content' => function ($model) {
                return $model->price_currency ? $model->priceCurrency->char_code : '';
            },
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_PRICE_CURRENCY),
        ],
        [
            'label' => Yii::t('common', 'Экспресс'),
            'enableSorting' => false,
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'content' => function ($model) {
                return Label::widget([
                    'label' => $model->orderExpressDelivery ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                    'style' => $model->orderExpressDelivery ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                ]);
            },
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_EXPRESS_DELIVERY),
        ],
        [
            'attribute' => 'created_at',
            'class' => DateColumn::className(),
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_CREATED_AT),
        ],
        [
            'attribute' => 'updated_at',
            'class' => DateColumn::className(),
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_UPDATED_AT),
        ],
        [
            'attribute' => 'deliveryRequest.sent_at',
            'label' => Yii::t('common', 'Дата отправки (курьерка)'),
            'class' => DateColumn::className(),
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_DELIVERY_SENT_AT),
        ],
        [
            'attribute' => 'deliveryRequest.second_sent_at',
            'label' => Yii::t('common', 'Дата повторной отправки (курьерка)'),
            'class' => DateColumn::className(),
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_DELIVERY_SECOND_SENT_AT),
        ],
        [
            'attribute' => 'deliveryRequest.sent_clarification_at',
            'label' => Yii::t('common', 'Дата отправки на уточнение (курьерка)'),
            'class' => DateColumn::className(),
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_DELIVERY_SENT_CLARIFICATION_AT),
        ],
        [
            'attribute' => 'deliveryRequest.accepted_at',
            'label' => Yii::t('common', 'Дата принятия (курьерка)'),
            'class' => DateColumn::className(),
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_DELIVERY_ACCEPTED_AT),
        ],
        [
            'attribute' => 'deliveryRequest.approved_at',
            'label' => Yii::t('common', 'Дата апрува (курьерка)'),
            'class' => DateColumn::className(),
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_DELIVERY_APPROVED_AT),
        ],
        [
            'attribute' => 'deliveryRequest.paid_at',
            'label' => Yii::t('common', 'Дата оплаты (курьерка)'),
            'class' => DateColumn::className(),
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_DELIVERY_PAID_AT),
        ],
        [
            'label' => Yii::t('common', 'Причина недоставки'),
            'enableSorting' => false,
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_UNSHIPPING_REASON),
            'content' => function ($model) {
                /** @var Order $model */
                if ($model->deliveryRequest && $model->deliveryRequest->unBuyoutReasonMapping instanceof \app\modules\catalog\models\UnBuyoutReasonMapping) {
                    $return = Yii::t('common', $model->deliveryRequest->unBuyoutReasonMapping->reason->name);
                    if (!empty($model->deliveryRequest->unBuyoutReasonMapping->foreign_reason)) {
                        $return .= ': ' . $model->deliveryRequest->unBuyoutReasonMapping->foreign_reason;
                    }
                    return htmlspecialchars($return);
                } else {
                    return '—';
                }
            },
        ],
        [
            'label' => Yii::t('common', 'Лэндинг'),
            'enableSorting' => false,
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_SITE),
            'content' => function ($model) {
                return (isset($model->landing->url)) ? $model->landing->url : ' - ';
            },
        ],
        [
            //'label' => Yii::t('common', 'Улица и дом заказчика'),
            'attribute' => 'income',
            'enableSorting' => false,
            /*'content' => function ($model) {
                return $model->income;
            },*/
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_INCOME),
        ],
        [
            'label' => Yii::t('common', 'ID вебмастера'),
            'enableSorting' => false,
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_WEBMASTER_IDENTIFIER),
            'content' => function ($model) {
                return $model->webmaster_identifier;
            },
        ],
        [
            'label' => Yii::t('common', 'Дата проверки треша в КЦ'),
            'enableSorting' => false,
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_CALL_CENTER_ANALYSIS_TRASH_AT),
            'content' => function ($model) {
                if ($model->callCenterRequest) {
                    if ($model->callCenterRequest->analysis_trash_at == CallCenterRequest::NEED_ANALYSIS_TRASH) {
                        return Yii::t('common', 'В очереди');
                    }
                    if ($model->callCenterRequest->analysis_trash_at > 0) {
                        return Yii::$app->formatter->asDatetime($model->callCenterRequest->analysis_trash_at);
                    }
                }
                return '';
            },
        ],
        [
            'label' => Yii::t('common', 'Результат проверки треша в КЦ'),
            'enableSorting' => false,
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_CALL_CENTER_ANALYSIS_TRASH_COMMENT),
            'content' => function ($model) {
                if ($model->callCenterRequest) {
                    return $model->callCenterRequest->analysis_trash_comment;
                }
                return '';
            },
        ],
        [
            'attribute' => 'callCenterRequest.autocheck_address',
            'enableSorting' => false,
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_CALL_CENTER_AUTO_CHECK_ADDRESS),
            'content' => function ($model) {
                if ($model->callCenterRequest) {
                    return $model->callCenterRequest->autoCheckAddressLabel ?: '—';
                } else {
                    return '—';
                }
            },
        ],
        [
            'label' => Yii::t('common', 'Оператор'),
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'enableSorting' => false,
            'content' => function ($model) {
                return (isset($model->callCenterRequest) && $model->callCenterRequest->last_foreign_operator > 0) ? $model->callCenterRequest->last_foreign_operator : Yii::t('common',
                    'Нет данных');
            },
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_LAST_FOREIGN_OPERATOR),
        ],
        [
            'attribute' => 'financePrediction.price_storage',
            'label' => Yii::t('common', 'Стоимость хранения'),
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'format' => ['decimal', 2],
            'enableSorting' => false,
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_FINANCE_PREDICTION_STORAGE),
        ],
        [
            'attribute' => 'financePrediction.price_fulfilment',
            'label' => Yii::t('common', 'Стоимость обслуживания заказа'),
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'format' => ['decimal', 2],
            'enableSorting' => false,
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_FINANCE_PREDICTION_FULFILMENT),
        ],
        [
            'attribute' => 'financePrediction.price_packing',
            'label' => Yii::t('common', 'Стоимость упаковывания'),
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'format' => ['decimal', 2],
            'enableSorting' => false,
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_FINANCE_PREDICTION_PACKING),
        ],
        [
            'attribute' => 'financePrediction.price_package',
            'label' => Yii::t('common', 'Стоимость упаковки'),
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'format' => ['decimal', 2],
            'enableSorting' => false,
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_FINANCE_PREDICTION_PACKAGE),
        ],
        [
            'attribute' => 'financePrediction.price_address_correction',
            'label' => Yii::t('common', 'Стоимость корректировки адреса'),
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'format' => ['decimal', 2],
            'enableSorting' => false,
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_FINANCE_PREDICTION_ADDRESS_CORRECTION),
        ],
        [
            'attribute' => 'financePrediction.price_delivery',
            'label' => Yii::t('common', 'Стоимость доставки'),
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'format' => ['decimal', 2],
            'enableSorting' => false,
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_FINANCE_PREDICTION_DELIVERY),
        ],
        [
            'attribute' => 'financePrediction.price_redelivery',
            'label' => Yii::t('common', 'Стоимость передоставки'),
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'format' => ['decimal', 2],
            'enableSorting' => false,
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_FINANCE_PREDICTION_REDELIVERY),
        ],
        [
            'attribute' => 'financePrediction.price_delivery_back',
            'label' => Yii::t('common', 'Стоимость возвращения'),
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'format' => ['decimal', 2],
            'enableSorting' => false,
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_FINANCE_PREDICTION_DELIVERY_BACK),
        ],
        [
            'attribute' => 'financePrediction.price_delivery_return',
            'label' => Yii::t('common', 'Стоимость возврата'),
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'format' => ['decimal', 2],
            'enableSorting' => false,
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_FINANCE_PREDICTION_DELIVERY_RETURN),
        ],
        [
            'attribute' => 'financePrediction.price_cod_service',
            'label' => Yii::t('common', 'Плата за наложенный платеж'),
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'format' => ['decimal', 2],
            'enableSorting' => false,
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_FINANCE_PREDICTION_COD_SERVICE),
        ],
        [
            'attribute' => 'financePrediction.price_vat',
            'label' => Yii::t('common', 'НДС'),
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'format' => ['decimal', 2],
            'enableSorting' => false,
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_FINANCE_PREDICTION_VAT),
        ],
        [
            'attribute' => 'financeFact',
            'label' => Yii::t('common', 'Платежка'),
            'content' => function ($model) {
                $return = [];
                if ($model->financeFact) {
                    foreach ($model->financeFact as $fact) {
                        $return[] = $fact->payment->name;
                    }
                }
                return !empty($return) ? implode("<br/>", $return) : '—';
            },
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_PAYMENT_NAME),
        ],
        [
            'attribute' => 'financeFact',
            'label' => Yii::t('common', 'Фактический COD'),
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_FINANCE_FACT_PRICE_COD),
            'content' => function ($model) use ($usd, $currencies) {
                if ($model->financeFact) {
                    $different = false;
                    foreach ($model->financeFact as $key => $fact) {
                        if ($key > 0 && $model->financeFact[$key - 1]->price_cod_currency_id != $fact->price_cod_currency_id) {
                            $different = true;
                            break;
                        }
                    }
                    if ($different) {
                        $arrayByCurrency = null;
                        foreach ($model->financeFact as $key => $fact) {
                            if (!empty($fact->price_cod_currency_rate) && $fact->price_cod > 0) {
                                $rate = $fact->price_cod_currency_rate ?? Currency::find()->convertValueToCurrency(1, $fact->price_cod_currency_id ?? $usd->id, $usd->id);
                                $arrayByCurrency[] = !empty($rate) ? round($fact->price_cod, 2)
                                    . (!empty($currencies[$fact->price_cod_currency_id]) ? ' ' . $currencies[$fact->price_cod_currency_id] : '')
                                    . (!empty($rate) ? ' ' . Yii::t('common', 'курс: {rate}', ['rate' => $rate]) : '')
                                    : null;
                            }
                        }
                        $return = $arrayByCurrency ? implode("<br/>", $arrayByCurrency) : null;
                    } else {
                        $currencyID = null;
                        $sum = $sumInUsd = null;
                        foreach ($model->financeFact as $key => $fact) {
                            $currencyID = $fact->price_cod_currency_id;
                            $rate = $fact->price_cod_currency_rate ?? Currency::find()->convertValueToCurrency(1, $fact->price_cod_currency_id ?? $usd->id, $usd->id);
                            $sum += $fact->price_cod;
                            $sumInUsd += $fact->price_cod / $rate;
                        }
                        $rate = $sum && $sumInUsd ? round($sum / $sumInUsd, 4) : null;
                        $return = !empty($rate) ? round($sumInUsd * $rate, 2)
                            . (!empty($currencies[$currencyID]) ? ' ' . $currencies[$currencyID] : '')
                            . (!empty($rate) ? ' ' . Yii::t('common', 'курс: {rate}', ['rate' => $rate]) : '')
                            : null;
                    }
                }
                return $return ?? '—';
            },
        ],
        [
            'attribute' => 'financeFact',
            'label' => Yii::t('common', 'Фактическая стоимость хранения'),
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_FINANCE_FACT_PRICE_STORAGE),
            'content' => function ($model) use ($usd, $currencies) {
                if ($model->financeFact) {
                    $different = false;
                    foreach ($model->financeFact as $key => $fact) {
                        if ($key > 0 && $model->financeFact[$key - 1]->price_storage_currency_id != $fact->price_storage_currency_id) {
                            $different = true;
                            break;
                        }
                    }
                    if ($different) {
                        $arrayByCurrency = null;
                        foreach ($model->financeFact as $key => $fact) {
                            if (!empty($fact->price_storage_currency_rate) && $fact->price_storage > 0) {
                                $rate = $fact->price_storage_currency_rate ?? Currency::find()->convertValueToCurrency(1, $fact->price_storage_currency_id ?? $usd->id, $usd->id);
                                $arrayByCurrency[] = !empty($rate) ? round($fact->price_storage, 2)
                                    . (!empty($currencies[$fact->price_storage_currency_id]) ? ' ' . $currencies[$fact->price_storage_currency_id] : '')
                                    . (!empty($rate) ? ' ' . Yii::t('common', 'курс: {rate}', ['rate' => $rate]) : '')
                                    : null;
                            }
                        }
                        $return = $arrayByCurrency ? implode("<br/>", $arrayByCurrency) : null;
                    } else {
                        $currencyID = null;
                        $sum = $sumInUsd = null;
                        foreach ($model->financeFact as $key => $fact) {
                            $currencyID = $fact->price_storage_currency_id;
                            $rate = $fact->price_storage_currency_rate ?? Currency::find()->convertValueToCurrency(1, $fact->price_storage_currency_id ?? $usd->id, $usd->id);
                            $sum += $fact->price_storage;
                            $sumInUsd += $fact->price_storage / $rate;
                        }
                        $rate = $sum && $sumInUsd ? round($sum / $sumInUsd, 4) : null;
                        $return = !empty($rate) ? round($sumInUsd * $rate, 2)
                            . (!empty($currencies[$currencyID]) ? ' ' . $currencies[$currencyID] : '')
                            . (!empty($rate) ? ' ' . Yii::t('common', 'курс: {rate}', ['rate' => $rate]) : '')
                            : null;
                    }
                }
                return $return ?? '—';
            }
        ],
        [
            'attribute' => 'financeFact',
            'label' => Yii::t('common', 'Фактическая стоимость обслуживания заказа'),
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_FINANCE_FACT_FULFILMENT),
            'content' => function ($model) use ($usd, $currencies) {
                if ($model->financeFact) {
                    $different = false;
                    foreach ($model->financeFact as $key => $fact) {
                        if ($key > 0 && $model->financeFact[$key - 1]->price_fulfilment_currency_id != $fact->price_fulfilment_currency_id) {
                            $different = true;
                            break;
                        }
                    }
                    if ($different) {
                        $arrayByCurrency = null;
                        foreach ($model->financeFact as $key => $fact) {
                            if (!empty($fact->price_fulfilment_currency_rate) && $fact->price_fulfilment > 0) {
                                $rate = $fact->price_fulfilment_currency_rate ?? Currency::find()->convertValueToCurrency(1, $fact->price_fulfilment_currency_id ?? $usd->id, $usd->id);
                                $arrayByCurrency[] =  !empty($rate) ? round($fact->price_fulfilment, 2)
                                    . (!empty($currencies[$fact->price_fulfilment_currency_id]) ? ' ' . $currencies[$fact->price_fulfilment_currency_id] : '')
                                    . (!empty($rate) ? ' ' . Yii::t('common', 'курс: {rate}', ['rate' => $rate]) : '')
                                    : null;
                            }
                        }
                        $return = $arrayByCurrency ? implode("<br/>", $arrayByCurrency) : null;
                    } else {
                        $currencyID = null;
                        $sum = $sumInUsd = null;
                        foreach ($model->financeFact as $key => $fact) {
                            $currencyID = $fact->price_fulfilment_currency_id;
                            $rate = $fact->price_fulfilment_currency_rate ?? Currency::find()->convertValueToCurrency(1, $fact->price_fulfilment_currency_id ?? $usd->id, $usd->id);
                            $sum += $fact->price_fulfilment;
                            $sumInUsd += $fact->price_fulfilment / $rate;
                        }
                        $rate = $sum && $sumInUsd ? round($sum / $sumInUsd, 4) : null;
                        $return = !empty($rate) ? round($sumInUsd * $rate, 2)
                            . (!empty($currencies[$currencyID]) ? ' ' . $currencies[$currencyID] : '')
                            . (!empty($rate) ? ' ' . Yii::t('common', 'курс: {rate}', ['rate' => $rate]) : '')
                            : null;
                    }
                }
                return $return ?? '—';
			},
        ],
        [
            'attribute' => 'financeFact',
            'label' => Yii::t('common', 'Фактическая стоимость упаковывания'),
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_FINANCE_FACT_PACKING),
            'content' => function ($model) use ($usd, $currencies) {
                if ($model->financeFact) {
                    $different = false;
                    foreach ($model->financeFact as $key => $fact) {
                        if ($key > 0 && $model->financeFact[$key - 1]->price_packing_currency_id != $fact->price_packing_currency_id) {
                            $different = true;
                            break;
                        }
                    }
                    if ($different) {
                        $arrayByCurrency = null;
                        foreach ($model->financeFact as $key => $fact) {
                            if (!empty($fact->price_packing_currency_rate) && $fact->price_packing > 0) {
                                $rate = $fact->price_packing_currency_rate ?? Currency::find()->convertValueToCurrency(1, $fact->price_packing_currency_id ?? $usd->id, $usd->id);
                                $arrayByCurrency[] = !empty($rate) ? round($fact->price_packing, 2)
                                    . (!empty($currencies[$fact->price_packing_currency_id]) ? ' ' . $currencies[$fact->price_packing_currency_id] : '')
                                    . (!empty($rate) ? ' ' . Yii::t('common', 'курс: {rate}', ['rate' => $rate]) : '')
                                    : null;
                            }
                        }
                        $return = $arrayByCurrency ? implode("<br/>", $arrayByCurrency) : null;
                    } else {
                        $currencyID = null;
                        $sum = $sumInUsd = null;
                        foreach ($model->financeFact as $key => $fact) {
                            $currencyID = $fact->price_packing_currency_id;
                            $rate = $fact->price_packing_currency_rate ?? Currency::find()->convertValueToCurrency(1, $fact->price_packing_currency_id ?? $usd->id, $usd->id);
                            $sum += $fact->price_packing;
                            $sumInUsd += $fact->price_packing / $rate;
                        }
                        $rate = $sum && $sumInUsd ? round($sum / $sumInUsd, 4) : null;
                        $return = !empty($rate) ? round($sumInUsd * $rate, 2)
                            . (!empty($currencies[$currencyID]) ? ' ' . $currencies[$currencyID] : '')
                            . (!empty($rate) ? ' ' . Yii::t('common', 'курс: {rate}', ['rate' => $rate]) : '')
                            : null;
                    }
                }
                return $return ?? '—';
			},
        ],
        [
            'attribute' => 'financeFact',
            'label' => Yii::t('common', 'Фактическая стоимость упаковки'),
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_FINANCE_FACT_PACKAGE),
            'content' => function ($model) use ($usd, $currencies) {
                if ($model->financeFact) {
                    $different = false;
                    foreach ($model->financeFact as $key => $fact) {
                        if ($key > 0 && $model->financeFact[$key - 1]->price_package_currency_id != $fact->price_package_currency_id) {
                            $different = true;
                            break;
                        }
                    }
                    if ($different) {
                        $arrayByCurrency = null;
                        foreach ($model->financeFact as $key => $fact) {
                            if (!empty($fact->price_package_currency_rate) && $fact->price_package > 0) {
                                $rate = $fact->price_package_currency_rate ?? Currency::find()->convertValueToCurrency(1, $fact->price_package_currency_id ?? $usd->id, $usd->id);
                                $arrayByCurrency[] =  !empty($rate) ? round($fact->price_package, 2)
                                    . (!empty($currencies[$fact->price_package_currency_id]) ? ' ' . $currencies[$fact->price_package_currency_id] : '')
                                    . (!empty($rate) ? ' ' . Yii::t('common', 'курс: {rate}', ['rate' => $rate]) : '')
                                    : null;
                            }
                        }
                        $return = $arrayByCurrency ? implode("<br/>", $arrayByCurrency) : null;
                    } else {
                        $currencyID = null;
                        $sum = $sumInUsd = null;
                        foreach ($model->financeFact as $key => $fact) {
                            $currencyID = $fact->price_package_currency_id;
                            $rate = $fact->price_package_currency_rate ?? Currency::find()->convertValueToCurrency(1, $fact->price_package_currency_id ?? $usd->id, $usd->id);
                            $sum += $fact->price_package;
                            $sumInUsd += $fact->price_package / $rate;
                        }
                        $rate = $sum && $sumInUsd ? round($sum / $sumInUsd, 4) : null;
                        $return = !empty($rate) ? round($sumInUsd * $rate, 2)
                            . (!empty($currencies[$currencyID]) ? ' ' . $currencies[$currencyID] : '')
                            . (!empty($rate) ? ' ' . Yii::t('common', 'курс: {rate}', ['rate' => $rate]) : '')
                            : null;
                    }
                }
                return $return ?? '—';
			},
        ],
        [
            'attribute' => 'financeFact',
            'label' => Yii::t('common', 'Фактическая стоимость корректировки адреса'),
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_FINANCE_FACT_ADDRESS_CORRECTION),
            'content' => function ($model) use ($usd, $currencies) {
                if ($model->financeFact) {
                    $different = false;
                    foreach ($model->financeFact as $key => $fact) {
                        if ($key > 0 && $model->financeFact[$key - 1]->price_address_correction_currency_id != $fact->price_address_correction_currency_id) {
                            $different = true;
                            break;
                        }
                    }
                    if ($different) {
                        $arrayByCurrency = null;
                        foreach ($model->financeFact as $key => $fact) {
                            if (!empty($fact->price_address_correction_currency_rate) && $fact->price_address_correction > 0) {
                                $rate = $fact->price_address_correction_currency_rate ?? Currency::find()->convertValueToCurrency(1, $fact->price_address_correction_currency_id ?? $usd->id, $usd->id);
                                $arrayByCurrency[] =  !empty($rate) ? round($fact->price_address_correction, 2)
                                    . (!empty($currencies[$fact->price_address_correction_currency_id]) ? ' ' . $currencies[$fact->price_address_correction_currency_id] : '')
                                    . (!empty($rate) ? ' ' . Yii::t('common', 'курс: {rate}', ['rate' => $rate]) : '')
                                    : null;
                            }
                        }
                        $return = $arrayByCurrency ? implode("<br/>", $arrayByCurrency) : null;
                    } else {
                        $currencyID = null;
                        $sum = $sumInUsd = null;
                        foreach ($model->financeFact as $key => $fact) {
                            $currencyID = $fact->price_address_correction_currency_id;
                            $rate = $fact->price_address_correction_currency_rate ?? Currency::find()->convertValueToCurrency(1, $fact->price_address_correction_currency_id ?? $usd->id, $usd->id);
                            $sum += $fact->price_address_correction;
                            $sumInUsd += $fact->price_address_correction / $rate;
                        }
                        $rate = $sum && $sumInUsd ? round($sum / $sumInUsd, 4) : null;
                        $return = !empty($rate) ? round($sumInUsd * $rate, 2)
                            . (!empty($currencies[$currencyID]) ? ' ' . $currencies[$currencyID] : '')
                            . (!empty($rate) ? ' ' . Yii::t('common', 'курс: {rate}', ['rate' => $rate]) : '')
                            : null;
                    }
                }
                return $return ?? '—';
			},
        ],
        [
            'attribute' => 'financeFact',
            'label' => Yii::t('common', 'Фактическая стоимость доставки'),
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_FINANCE_FACT_DELIVERY),
            'content' => function ($model) use ($usd, $currencies) {
                if ($model->financeFact) {
                    $different = false;
                    foreach ($model->financeFact as $key => $fact) {
                        if ($key > 0 && $model->financeFact[$key - 1]->price_delivery_currency_id != $fact->price_delivery_currency_id) {
                            $different = true;
                            break;
                        }
                    }
                    if ($different) {
                        $arrayByCurrency = null;
                        foreach ($model->financeFact as $key => $fact) {
                            if (!empty($fact->price_delivery_currency_rate && $fact->price_delivery > 0)) {
                                $rate = $fact->price_delivery_currency_rate ?? Currency::find()->convertValueToCurrency(1, $fact->price_delivery_currency_id ?? $usd->id, $usd->id);
                                $arrayByCurrency[] =  !empty($rate) ? round($fact->price_delivery, 2)
                                    . (!empty($currencies[$fact->price_delivery_currency_id]) ? ' ' . $currencies[$fact->price_delivery_currency_id] : '')
                                    . (!empty($rate) ? ' ' . Yii::t('common', 'курс: {rate}', ['rate' => $rate]) : '')
                                    : null;
                            }
                        }
                        $return = $arrayByCurrency ? implode("<br/>", $arrayByCurrency) : null;
                    } else {
                        $currencyID = null;
                        $sum = $sumInUsd = null;
                        foreach ($model->financeFact as $key => $fact) {
                            $currencyID = $fact->price_delivery_currency_id;
                            $rate = $fact->price_delivery_currency_rate ?? Currency::find()->convertValueToCurrency(1, $fact->price_delivery_currency_id ?? $usd->id, $usd->id);
                            $sum += $fact->price_delivery;
                            $sumInUsd += $fact->price_delivery / $rate;
                        }
                        $rate = $sum && $sumInUsd ? round($sum / $sumInUsd, 4) : null;
                        $return = !empty($rate) ? round($sumInUsd * $rate, 2)
                            . (!empty($currencies[$currencyID]) ? ' ' . $currencies[$currencyID] : '')
                            . (!empty($rate) ? ' ' . Yii::t('common', 'курс: {rate}', ['rate' => $rate]) : '')
                            : null;
                    }
                }
                return $return ?? '—';
			},
        ],
        [
            'attribute' => 'financeFact',
            'label' => Yii::t('common', 'Фактическая стоимость передоставки'),
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_FINANCE_FACT_REDELIVERY),
            'content' => function ($model) use ($usd, $currencies) {
                if ($model->financeFact) {
                    $different = false;
                    foreach ($model->financeFact as $key => $fact) {
                        if ($key > 0 && $model->financeFact[$key - 1]->price_redelivery_currency_id != $fact->price_redelivery_currency_id) {
                            $different = true;
                            break;
                        }
                    }
                    if ($different) {
                        $arrayByCurrency = null;
                        foreach ($model->financeFact as $key => $fact) {
                            if (!empty($fact->price_redelivery_currency_rate) && $fact->price_redelivery > 0) {
                                $rate = $fact->price_redelivery_currency_rate ?? Currency::find()->convertValueToCurrency(1, $fact->price_redelivery_currency_id ?? $usd->id, $usd->id);
                                $arrayByCurrency[] =  !empty($rate) ? round($fact->price_redelivery, 2)
                                    . (!empty($currencies[$fact->price_redelivery_currency_id]) ? ' ' . $currencies[$fact->price_redelivery_currency_id] : '')
                                    . (!empty($rate) ? ' ' . Yii::t('common', 'курс: {rate}', ['rate' => $rate]) : '')
                                    : null;
                            }
                        }
                        $return = $arrayByCurrency ? implode("<br/>", $arrayByCurrency) : null;
                    } else {
                        $currencyID = null;
                        $sum = $sumInUsd = null;
                        foreach ($model->financeFact as $key => $fact) {
                            $currencyID = $fact->price_redelivery_currency_id;
                            $rate = $fact->price_redelivery_currency_rate ?? Currency::find()->convertValueToCurrency(1, $fact->price_redelivery_currency_id ?? $usd->id, $usd->id);
                            $sum += $fact->price_redelivery;
                            $sumInUsd += $fact->price_redelivery / $rate;
                        }
                        $rate = $sum && $sumInUsd ? round($sum / $sumInUsd, 4) : null;
                        $return = !empty($rate) ? round($sumInUsd * $rate, 2)
                            . (!empty($currencies[$currencyID]) ? ' ' . $currencies[$currencyID] : '')
                            . (!empty($rate) ? ' ' . Yii::t('common', 'курс: {rate}', ['rate' => $rate]) : '')
                            : null;
                    }
                }
                return $return ?? '—';
			},
        ],
        [
            'attribute' => 'financeFact',
            'label' => Yii::t('common', 'Фактическая стоимость возвращения'),
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_FINANCE_FACT_DELIVERY_BACK),
            'content' => function ($model) use ($usd, $currencies) {
                if ($model->financeFact) {
                    $different = false;
                    foreach ($model->financeFact as $key => $fact) {
                        if ($key > 0 && $model->financeFact[$key - 1]->price_delivery_back_currency_id != $fact->price_delivery_back_currency_id) {
                            $different = true;
                            break;
                        }
                    }
                    if ($different) {
                        $arrayByCurrency = null;
                        foreach ($model->financeFact as $key => $fact) {
                            if (!empty($fact->price_delivery_back_currency_rate) && $fact->price_delivery_back > 0) {
                                $rate = $fact->price_delivery_back_currency_rate ?? Currency::find()->convertValueToCurrency(1, $fact->price_delivery_back_currency_id ?? $usd->id, $usd->id);
                                $arrayByCurrency[] =  !empty($rate) ? round($fact->price_delivery_back, 2)
                                    . (!empty($currencies[$fact->price_delivery_back_currency_id]) ? ' ' . $currencies[$fact->price_delivery_back_currency_id] : '')
                                    . (!empty($rate) ? ' ' . Yii::t('common', 'курс: {rate}', ['rate' => $rate]) : '')
                                    : null;
                            }
                        }
                        $return = $arrayByCurrency ? implode("<br/>", $arrayByCurrency) : null;
                    } else {
                        $currencyID = null;
                        $sum = $sumInUsd = null;
                        foreach ($model->financeFact as $key => $fact) {
                            $currencyID = $fact->price_delivery_back_currency_id;
                            $rate = $fact->price_delivery_back_currency_rate ?? Currency::find()->convertValueToCurrency(1, $fact->price_delivery_back_currency_id ?? $usd->id, $usd->id);
                            $sum += $fact->price_delivery_back;
                            $sumInUsd += $fact->price_delivery_back / $rate;
                        }
                        $rate = $sum && $sumInUsd ? round($sum / $sumInUsd, 4) : null;
                        $return = !empty($rate) ? round($sumInUsd * $rate, 2)
                            . (!empty($currencies[$currencyID]) ? ' ' . $currencies[$currencyID] : '')
                            . (!empty($rate) ? ' ' . Yii::t('common', 'курс: {rate}', ['rate' => $rate]) : '')
                            : null;
                    }
                }
                return $return ?? '—';
			},
        ],
        [
            'attribute' => 'financeFact',
            'label' => Yii::t('common', 'Фактическая стоимость возврата'),
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_FINANCE_FACT_DELIVERY_RETURN),
            'content' => function ($model) use ($usd, $currencies) {
                if ($model->financeFact) {
                    $different = false;
                    foreach ($model->financeFact as $key => $fact) {
                        if ($key > 0 && $model->financeFact[$key - 1]->price_delivery_return_currency_id != $fact->price_delivery_return_currency_id) {
                            $different = true;
                            break;
                        }
                    }
                    if ($different) {
                        $arrayByCurrency = null;
                        foreach ($model->financeFact as $key => $fact) {
                            if (!empty($fact->price_delivery_return_currency_rate) && $fact->price_delivery_return > 0) {
                                $rate = $fact->price_delivery_return_currency_rate ?? Currency::find()->convertValueToCurrency(1, $fact->price_delivery_return_currency_id ?? $usd->id, $usd->id);
                                $arrayByCurrency[] = !empty($rate) ? round($fact->price_delivery_return, 2)
                                    . (!empty($currencies[$fact->price_delivery_return_currency_id]) ? ' ' . $currencies[$fact->price_delivery_return_currency_id] : '')
                                    . (!empty($rate) ? ' ' . Yii::t('common', 'курс: {rate}', ['rate' => $rate]) : '')
                                    : null;
                            }
                        }
                        $return = $arrayByCurrency ? implode("<br/>", $arrayByCurrency) : null;
                    } else {
                        $currencyID = null;
                        $sum = $sumInUsd = null;
                        foreach ($model->financeFact as $key => $fact) {
                            $currencyID = $fact->price_delivery_return_currency_id;
                            $rate = $fact->price_delivery_return_currency_rate ?? Currency::find()->convertValueToCurrency(1, $fact->price_delivery_return_currency_id ?? $usd->id, $usd->id);
                            $sum += $fact->price_delivery_return;
                            $sumInUsd += $fact->price_delivery_return / $rate;
                        }
                        $rate = $sum && $sumInUsd ? round($sum / $sumInUsd, 4) : null;
                        $return = !empty($rate) ? round($sumInUsd * $rate, 2)
                            . (!empty($currencies[$currencyID]) ? ' ' . $currencies[$currencyID] : '')
                            . (!empty($rate) ? ' ' . Yii::t('common', 'курс: {rate}', ['rate' => $rate]) : '')
                            : null;
                    }
                }
                return $return ?? '—';
			},
        ],
        [
            'attribute' => 'financeFact',
            'label' => Yii::t('common', 'Фактическая плата за наложенный платеж'),
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_FINANCE_FACT_COD_SERVICE),
            'content' => function ($model) use ($usd, $currencies) {
                if ($model->financeFact) {
                    $different = false;
                    foreach ($model->financeFact as $key => $fact) {
                        if ($key > 0 && $model->financeFact[$key - 1]->price_cod_service_currency_id != $fact->price_cod_service_currency_id) {
                            $different = true;
                            break;
                        }
                    }
                    if ($different) {
                        $arrayByCurrency = null;
                        foreach ($model->financeFact as $key => $fact) {
                            if (!empty($fact->price_cod_service_currency_rate) && $fact->price_cod_service > 0) {
                                $rate = $fact->price_cod_service_currency_rate ?? Currency::find()->convertValueToCurrency(1, $fact->price_cod_service_currency_id ?? $usd->id, $usd->id);
                                $arrayByCurrency[] =  !empty($rate) ? round($fact->price_cod_service, 2)
                                    . (!empty($currencies[$fact->price_cod_service_currency_id]) ? ' ' . $currencies[$fact->price_cod_service_currency_id] : '')
                                    . (!empty($rate) ? ' ' . Yii::t('common', 'курс: {rate}', ['rate' => $rate]) : '')
                                    : null;
                            }
                        }
                        $return = $arrayByCurrency ? implode("<br/>", $arrayByCurrency) : null;
                    } else {
                        $currencyID = null;
                        $sum = $sumInUsd = null;
                        foreach ($model->financeFact as $key => $fact) {
                            $currencyID = $fact->price_cod_service_currency_id;
                            $rate = $fact->price_cod_service_currency_rate ?? Currency::find()->convertValueToCurrency(1, $fact->price_cod_service_currency_id ?? $usd->id, $usd->id);
                            $sum += $fact->price_cod_service;
                            $sumInUsd += $fact->price_cod_service / $rate;
                        }
                        $rate = $sum && $sumInUsd ? round($sum / $sumInUsd, 4) : null;
                        $return = !empty($rate) ? round($sumInUsd * $rate, 2)
                            . (!empty($currencies[$currencyID]) ? ' ' . $currencies[$currencyID] : '')
                            . (!empty($rate) ? ' ' . Yii::t('common', 'курс: {rate}', ['rate' => $rate]) : '')
                            : null;
                    }
                }
                return $return ?? '—';
			},
        ],
        [
            'attribute' => 'financeFact',
            'label' => Yii::t('common', 'Фактический НДС'),
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_FINANCE_FACT_VAT),
            'content' => function ($model) use ($usd, $currencies) {
                if ($model->financeFact) {
                    $different = false;
                    foreach ($model->financeFact as $key => $fact) {
                        if ($key > 0 && $model->financeFact[$key - 1]->price_vat_currency_id != $fact->price_vat_currency_id) {
                            $different = true;
                            break;
                        }
                    }
                    if ($different) {
                        $arrayByCurrency = null;
                        foreach ($model->financeFact as $key => $fact) {
                            if (!empty($fact->price_vat_currency_rate) && $fact->price_vat > 0) {
                                $rate = $fact->price_vat_currency_rate ?? Currency::find()->convertValueToCurrency(1, $fact->price_vat_currency_id ?? $usd->id, $usd->id);
                                $arrayByCurrency[] = !empty($rate) ? round($fact->price_vat, 2)
                                    . (!empty($currencies[$fact->price_vat_currency_id]) ? ' ' . $currencies[$fact->price_vat_currency_id] : '')
                                    . (!empty($rate) ? ' ' . Yii::t('common', 'курс: {rate}', ['rate' => $rate]) : '')
                                    : null;
                            }
                        }
                        $return = $arrayByCurrency ? implode("<br/>", $arrayByCurrency) : null;
                    } else {
                        $currencyID = null;
                        $sum = $sumInUsd = null;
                        foreach ($model->financeFact as $key => $fact) {
                            $currencyID = $fact->price_vat_currency_id;
                            $rate = $fact->price_vat_currency_rate ?? Currency::find()->convertValueToCurrency(1, $fact->price_vat_currency_id ?? $usd->id, $usd->id);
                            $sum += $fact->price_vat;
                            $sumInUsd += $fact->price_vat / $rate;
                        }
                        $rate = $sum && $sumInUsd ? round($sum / $sumInUsd, 4) : null;
                        $return = !empty($rate) ? round($sumInUsd * $rate, 2)
                            . (!empty($currencies[$currencyID]) ? ' ' . $currencies[$currencyID] : '')
                            . (!empty($rate) ? ' ' . Yii::t('common', 'курс: {rate}', ['rate' => $rate]) : '')
                            : null;
                    }
                }
                return $return ?? '—';
			},
        ],
        [
            'label' => Yii::t('common', 'Заявки на проверку заказа'),
            'enableSorting' => false,
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_CHECK_REQUESTS),
            'content' => function ($model) {
                if ($model->callCenterCheckRequests) {
                    return $this->render('_index-content-check-requests', [
                        'model' => $model,
                    ]);
                }
                return '—';
            },
        ],
        [
            'label' => Yii::t('common', 'Дата создания у партнера'),
            'attribute' => 'ts_spawn',
            'enableSorting' => false,
            'class' => DateColumn::className(),
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_TS_SPAWN),
            'value' => function ($model) {
                return !empty($model->lead->ts_spawn) ? $model->lead->ts_spawn : null;
            },
        ],
        [
            'label' => Yii::t('common', 'Результат проверки в КЦ'),
            'attribute' => 'callCenter.foreign_information',
            'enableSorting' => false,
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_CALL_CENTER_CHECK_REQUEST_RESULT),
            'value' => function ($model) {
                $notDone = CallCenterCheckRequest::getSubStatusesForCheckUndelivery(true);
                $done = CallCenterCheckRequest::getSubStatusesForCheckUndelivery();

                $content = '';
                if ($model->lastCallCenterCheckRequest && !empty($model->lastCallCenterCheckRequest)) {
                    if ($model->lastCallCenterCheckRequest->status != CallCenterCheckRequest::STATUS_DONE) {
                        $content = $notDone[CallCenterCheckRequest::FOREIGN_SUB_STATUS_EMPTY];
                    } else {
                        $content = isset($done[$model->lastCallCenterCheckRequest->foreign_information])
                            ? $done[$model->lastCallCenterCheckRequest->foreign_information]
                            : CallCenterCheckRequest::getStatusByName($model->lastCallCenterCheckRequest->status);
                    }
                }

                return $content;
            },
        ],
        [
            'label' => Yii::t('common', 'Число звонков'),
            'attribute' => 'callCenterRequest.calls_count',
            'enableSorting' => false,
            'visible' => $showerColumns->isChosenColumn(ShowerColumns::COLUMN_CALLS_COUNT),
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'content' => function ($model) {
                /** @var Order $model */
                if ($model->callCenterRequest) {
                    return $model->callCenterRequest->callsCount;
                } else {
                    return '—';
                }
            },
        ],
        'actions' => [
            'class' => ActionColumn::className(),
            'items' => [
                [
                    'label' => Yii::t('common', 'Просмотреть'),
                    'url' => function ($data) {
                        return Url::toRoute(['view', 'id' => $data->id]);
                    },
                    'can' => function () {
                        return Yii::$app->user->can('order.index.view');
                    },
                ],
                [
                    'label' => Yii::t('common', 'Редактировать'),
                    'url' => function ($data) {
                        return Url::toRoute(['edit', 'id' => $data->id]);
                    },
                    'can' => function () {
                        return Yii::$app->user->can('order.index.edit');
                    },
                ],
                [
                    'label' => Yii::t('common', 'Заявки на проверку'),
                    'url' => function ($model) {
                        return Url::toRoute([
                            '/call-center/checking-request/index',
                            'NumberFilter' => ['number' => $model->id]
                        ]);
                    },
                    'can' => function () {
                        return Yii::$app->user->can('callcenter.checkingrequest.index');
                    }
                ],
                [
                    'label' => Yii::t('common', 'Распечатать накладную'),
                    'style' => 'print-invoice-anchor',
                    'data-order_id' => '',
                    'contentOptions' => ['data-order_id' => 'text-center'],
                    'attributes' => function ($model) {
                        /**
                         * @var $model Order
                         * @var $model ->orderLogisticListInvoice OrderLogisticListInvoice
                         */
                        $data = [];
                        $data['data-order_id'] = $model->id;

                        if ($model->orderLogisticListInvoice) {

                            $data['data-list_id'] = $model->orderLogisticListInvoice->list_id;
                        } else {
                            $data['data-list_id'] = false;
                        }
                        return $data;
                    },
                    'url' => function () {
                        return Url::toRoute(['#']);
                    },
                    'can' => function () {
                        return Yii::$app->user->can('order.index.printinvoice');
                    },
                ],
            ]
        ],
    ],
]); ?>

<?= InvoicePrint::widget([
    'model' => new OrderLogisticListInvoice([
        'template' => OrderLogisticListInvoice::TEMPLATE_TWO,
        'format' => OrderLogisticListInvoice::FORMAT_DOCS,
    ])
]) ?>

