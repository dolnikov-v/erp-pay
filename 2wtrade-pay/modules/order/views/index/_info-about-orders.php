<?php
use app\components\grid\DateColumn;
use app\components\grid\GridView;
use app\widgets\Nav;
use app\widgets\Panel;
use yii\helpers\Url;
use app\modules\order\assets\ResenderInDeliveryAsset;
use app\modules\order\assets\SenderInDeliveryAsset;
use app\modules\order\widgets\ResenderInDelivery;
use app\modules\order\widgets\SenderInDelivery;

/** @var yii\web\View $this */
/** @var app\modules\order\models\Order $model */
/** @var array $options */
/** @var array $adcomboResponse */
/** @var array $callCenterResponse */
/** @var array $deliveryResponse */
/** @var \app\modules\order\models\OrderLog[] $logsGroup */
/** @var \app\modules\order\models\OrderLogProduct[] $logsProduct */
/** @var yii\data\ActiveDataProvider $breakpoints */
/** @var \app\modules\delivery\models\DeliveryRequest $modelDeliveryRequest */

SenderInDeliveryAsset::register($this);
ResenderInDeliveryAsset::register($this);
?>

<?php if ($model->isNewRecord): ?>
    <?= Panel::widget([
        'content' => $this->render('_edit-form', $options),
        'fullScreen' => true,
    ]) ?>
<?php else: ?>
    <?php if ($options['isView'] == true): ?>
        <?= Panel::widget([
            'title' => Yii::t('common', 'Информация о заказе'),
            'border' => false,
            'nav' => new Nav([
                'tabs' => [
                    [
                        'label' => Yii::t('common', 'Общая информация'),
                        'content' => $this->render('_edit-form', $options),
                    ],
                    [
                        'label' => Yii::t('common', 'Расходы'),
                        'content' => $this->render('_panel-costs', $options),
                    ],
                    [
                        'label' => Yii::t('common', 'Изменения заказа'),
                        'content' => $this->render('_panel-logs', [
                            'logsGroup' => $logsGroup,
                        ]),
                        'visible' => Yii::$app->user->can('order.index.view.changelog'),
                    ],
                    [
                        'label' => Yii::t('common', 'Изменение статуса'),
                        'content' => GridView::widget([
                            'dataProvider' => $breakpoints,
                            'columns' => [
                                [
                                    'attribute' => 'id',
                                    'label' => '#',
                                    'headerOptions' => ['class' => 'width-50'],
                                    'enableSorting' => false,
                                ],
                                [
                                    'attribute' => 'oldStatus.name',
                                    'label' => Yii::t('common', 'Старый статус'),
                                    'enableSorting' => false,
                                ],
                                [
                                    'attribute' => 'newStatus.name',
                                    'label' => Yii::t('common', 'Новый статус'),
                                    'enableSorting' => false,
                                ],
                                [
                                    'attribute' => 'comment',
                                    'label' => Yii::t('common', 'Комментарий'),
                                    'enableSorting' => false,
                                ],
                                [
                                    'class' => DateColumn::className(),
                                    'attribute' => 'created_at',
                                    'enableSorting' => false,
                                    'label' => Yii::t('common', 'Дата')
                                ],
                            ],
                        ]),
                    ],
                    [
                        'label' => Yii::t('common', 'Изменения продуктов'),
                        'content' => $this->render('_panel-logs-product', [
                            'logsProduct' => $logsProduct,
                        ]),
                        'visible' => Yii::$app->user->can('order.index.view.productlog'),
                    ],
                    [
                        'label' => Yii::t('common', 'Ответы КЦ'),
                        'content' => $this->render('_cc_response', [
                            'callCenterResponse' => $callCenterResponse,
                        ]),
                        'visible' => Yii::$app->user->can('order.index.view.callcenterresponse'),
                    ],
                    [
                        'label' => Yii::t('common', 'Ответы службы доставки'),
                        'content' => $this->render('_delivery_response', [
                            'deliveryResponse' => $deliveryResponse,
                        ]),
                        'visible' => Yii::$app->user->can('order.index.view.deliveryresponse'),
                    ],
                    [
                        'label' => Yii::t('common', 'Lead data'),
                        'content' => $this->render('_adcombo_response', [
                            'adcomboResponse' => $adcomboResponse,
                        ]),
                        'visible' => Yii::$app->user->can('order.index.view.leaddata'),

                    ],
                    [
                        'label' => Yii::t('common', 'Изменения лида'),
                        'content' => \app\modules\order\widgets\PanelGroupLogs::widget(['logs' => $model->lead ? $model->lead->logs : []]),
                        'visible' => Yii::$app->user->can('order.index.view.leadlogs'),
                    ]
                    /*[
                        'label' => Yii::t('common', 'Ответ службы доставки при проверке статуса'),
                        'content' => $this->render('_delivery_response', [
                            'deliveryResponse' => $deliveryResponse,
                        ]),
                    ],*/
                ]
            ]),
        ]) ?>
    <?php else: ?>
        <?= Panel::widget([
            'border' => false,
            'content' => $this->render('_edit-form', $options),
        ]) ?>
    <?php endif; ?>
<?php endif; ?>

<?php if (Yii::$app->user->can('order.index.sendindelivery')): ?>
    <?=SenderInDelivery::widget([
        'url' => Url::toRoute('send-in-delivery'),
    ]);?>
<?php endif;?>

<?php if (Yii::$app->user->can('order.index.resendindelivery')): ?>
    <?=ResenderInDelivery::widget([
        'url' => Url::toRoute('resend-in-delivery'),
    ]);?>
<?php endif;?>
