<?php

use app\components\grid\GridView;
use app\models\Currency;
use app\modules\order\models\OrderFinanceFact;
use app\modules\order\models\OrderFinancePretension;
use yii\data\ArrayDataProvider;
use app\components\grid\DateColumn;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var app\modules\order\models\Order $model */

$currencyCollection = Currency::find()->customCollection('id', 'char_code');
?>

<div style="width: 100%; overflow-x: scroll; ">
    <h3 class="text-primary"><?= Yii::t('common', 'Расчетные расходы') ?></h3>
    <?php if ($model->financePrediction) : ?>
        <?= GridView::widget([
            'dataProvider' => New ArrayDataProvider(['allModels' => [$model->financePrediction]]),
            'columns' => [
                [
                    'attribute' => 'price_cod',
                    'content' => function ($data) use ($currencyCollection) {
                        if (is_null($data->price_cod)) {
                            return 0;
                        }
                        return $data->price_cod . ' ' . ($data->price_cod_currency_id ? $currencyCollection[$data->price_cod_currency_id] ?? '' : '');
                    }
                ],
                [
                    'attribute' => 'price_storage',
                    'content' => function ($data) use ($currencyCollection) {
                        if (is_null($data->price_storage)) {
                            return 0;
                        }
                        return $data->price_storage . ' ' . ($data->price_storage_currency_id ? $currencyCollection[$data->price_storage_currency_id] ?? '' : '');
                    }
                ],
                [
                    'attribute' => 'price_fulfilment',
                    'content' => function ($data) use ($currencyCollection) {
                        if (is_null($data->price_fulfilment)) {
                            return 0;
                        }
                        return $data->price_fulfilment . ' ' . ($data->price_fulfilment_currency_id ? $currencyCollection[$data->price_fulfilment_currency_id] ?? '' : '');
                    }
                ],
                [
                    'attribute' => 'price_packing',
                    'content' => function ($data) use ($currencyCollection) {
                        if (is_null($data->price_packing)) {
                            return 0;
                        }
                        return $data->price_packing . ' ' . ($data->price_packing_currency_id ? $currencyCollection[$data->price_packing_currency_id] ?? '' : '');
                    }
                ],
                [
                    'attribute' => 'price_package',
                    'content' => function ($data) use ($currencyCollection) {
                        if (is_null($data->price_package)) {
                            return 0;
                        }
                        return $data->price_package . ' ' . ($data->price_package_currency_id ? $currencyCollection[$data->price_package_currency_id] ?? '' : '');
                    }
                ],
                [
                    'attribute' => 'price_delivery',
                    'content' => function ($data) use ($currencyCollection) {
                        if (is_null($data->price_delivery)) {
                            return 0;
                        }
                        return $data->price_delivery . ' ' . ($data->price_delivery_currency_id ? $currencyCollection[$data->price_delivery_currency_id] ?? '' : '');
                    }
                ],
                [
                    'attribute' => 'price_redelivery',
                    'content' => function ($data) use ($currencyCollection) {
                        if (is_null($data->price_redelivery)) {
                            return 0;
                        }
                        return $data->price_redelivery . ' ' . ($data->price_redelivery_currency_id ? $currencyCollection[$data->price_redelivery_currency_id] ?? '' : '');
                    }
                ],
                [
                    'attribute' => 'price_delivery_back',
                    'content' => function ($data) use ($currencyCollection) {
                        if (is_null($data->price_delivery_back)) {
                            return 0;
                        }
                        return $data->price_delivery_back . ' ' . ($data->price_delivery_back_currency_id ? $currencyCollection[$data->price_delivery_back_currency_id] ?? '' : '');
                    }
                ],
                [
                    'attribute' => 'price_delivery_return',
                    'content' => function ($data) use ($currencyCollection) {
                        if (is_null($data->price_delivery_return)) {
                            return 0;
                        }
                        return $data->price_delivery_return . ' ' . ($data->price_delivery_return_currency_id ? $currencyCollection[$data->price_delivery_return_currency_id] ?? '' : '');
                    }
                ],
                [
                    'attribute' => 'price_address_correction',
                    'content' => function ($data) use ($currencyCollection) {
                        if (is_null($data->price_address_correction)) {
                            return 0;
                        }
                        return $data->price_address_correction . ' ' . ($data->price_address_correction_currency_id ? $currencyCollection[$data->price_address_correction_currency_id] ?? '' : '');
                    }
                ],
                [
                    'attribute' => 'price_cod_service',
                    'content' => function ($data) use ($currencyCollection) {
                        if (is_null($data->price_cod_service)) {
                            return 0;
                        }
                        return $data->price_cod_service . ' ' . ($data->price_cod_service_currency_id ? $currencyCollection[$data->price_cod_service_currency_id] ?? '' : '');
                    }
                ],
                [
                    'attribute' => 'price_vat',
                    'content' => function ($data) use ($currencyCollection) {
                        if (is_null($data->price_vat)) {
                            return 0;
                        }
                        return $data->price_vat . ' ' . ($data->price_vat_currency_id ? $currencyCollection[$data->price_vat_currency_id] ?? '' : '');
                    }
                ],
                [
                    'attribute' => 'additional_prices',
                    'content' => function ($data) {
                        if (is_null($data->additional_prices)) {
                            return 0;
                        }
                        return $data->additional_prices;
                    }
                ],
                [
                    'attribute' => 'created_at',
                    'class' => DateColumn::className(),
                ],
                [
                    'attribute' => 'updated_at',
                    'class' => DateColumn::className(),
                ],
            ]
        ]) ?>
    <?php endif; ?>

    <h3 class="text-primary"><?= Yii::t('common', 'Фактические расходы') ?></h3>
    <?php if ($model->financeFact) : ?>
        <?= GridView::widget([
            'dataProvider' => New ArrayDataProvider(['allModels' => $model->financeFact]),
            'columns' => [
                [
                    'attribute' => 'price_cod',
                    'content' => function ($data) use ($currencyCollection) {
                        if (is_null($data->price_cod)) {
                            return 0;
                        }
                        return $data->price_cod . ' ' . ($data->price_cod_currency_id ? $currencyCollection[$data->price_cod_currency_id] ?? '' : '') .
                            ($data->price_cod_currency_rate > 0 ? '<br>' . Yii::t('common', 'курс') . ': ' . $data->price_cod_currency_rate : '');
                    }
                ],
                [
                    'attribute' => 'price_storage',
                    'content' => function ($data) use ($currencyCollection) {
                        if (is_null($data->price_storage)) {
                            return 0;
                        }
                        return $data->price_storage . ' ' . ($data->price_storage_currency_id ? $currencyCollection[$data->price_storage_currency_id] ?? '' : '') .
                            ($data->price_storage_currency_rate > 0 ? '<br>' . Yii::t('common', 'курс') . ': ' . $data->price_storage_currency_rate : '');
                    }
                ],
                [
                    'attribute' => 'price_fulfilment',
                    'content' => function ($data) use ($currencyCollection) {
                        if (is_null($data->price_fulfilment)) {
                            return 0;
                        }
                        return $data->price_fulfilment . ' ' . ($data->price_fulfilment_currency_id ? $currencyCollection[$data->price_fulfilment_currency_id] ?? '' : '') .
                            ($data->price_fulfilment_currency_rate > 0 ? '<br>' . Yii::t('common', 'курс') . ': ' . $data->price_fulfilment_currency_rate : '');
                    }
                ],
                [
                    'attribute' => 'price_packing',
                    'content' => function ($data) use ($currencyCollection) {
                        if (is_null($data->price_packing)) {
                            return 0;
                        }
                        return $data->price_packing . ' ' . ($data->price_packing_currency_id ? $currencyCollection[$data->price_packing_currency_id] ?? '' : '') .
                            ($data->price_packing_currency_rate > 0 ? '<br>' . Yii::t('common', 'курс') . ': ' . $data->price_packing_currency_rate : '');
                    }
                ],
                [
                    'attribute' => 'price_package',
                    'content' => function ($data) use ($currencyCollection) {
                        if (is_null($data->price_package)) {
                            return 0;
                        }
                        return $data->price_package . ' ' . ($data->price_package_currency_id ? $currencyCollection[$data->price_package_currency_id] ?? '' : '') .
                            ($data->price_package_currency_rate > 0 ? '<br>' . Yii::t('common', 'курс') . ': ' . $data->price_package_currency_rate : '');
                    }
                ],
                [
                    'attribute' => 'price_delivery',
                    'content' => function ($data) use ($currencyCollection) {
                        if (is_null($data->price_delivery)) {
                            return 0;
                        }
                        return $data->price_delivery . ' ' . ($data->price_delivery_currency_id ? $currencyCollection[$data->price_delivery_currency_id] ?? '' : '') .
                            ($data->price_delivery_currency_rate > 0 ? '<br>' . Yii::t('common', 'курс') . ': ' . $data->price_delivery_currency_rate : '');
                    }
                ],
                [
                    'attribute' => 'price_redelivery',
                    'content' => function ($data) use ($currencyCollection) {
                        if (is_null($data->price_redelivery)) {
                            return 0;
                        }
                        return $data->price_redelivery . ' ' . ($data->price_redelivery_currency_id ? $currencyCollection[$data->price_redelivery_currency_id] ?? '' : '') .
                            ($data->price_redelivery_currency_rate > 0 ? '<br>' . Yii::t('common', 'курс') . ': ' . $data->price_redelivery_currency_rate : '');
                    }
                ],
                [
                    'attribute' => 'price_delivery_back',
                    'content' => function ($data) use ($currencyCollection) {
                        if (is_null($data->price_delivery_back)) {
                            return 0;
                        }
                        return $data->price_delivery_back . ' ' . ($data->price_delivery_back_currency_id ? $currencyCollection[$data->price_delivery_back_currency_id] ?? '' : '') .
                            ($data->price_delivery_back_currency_rate > 0 ? '<br>' . Yii::t('common', 'курс') . ': ' . $data->price_delivery_back_currency_rate : '');
                    }
                ],
                [
                    'attribute' => 'price_delivery_return',
                    'content' => function ($data) use ($currencyCollection) {
                        if (is_null($data->price_delivery_return)) {
                            return 0;
                        }
                        return $data->price_delivery_return . ' ' . ($data->price_delivery_return_currency_id ? $currencyCollection[$data->price_delivery_return_currency_id] ?? '' : '') .
                            ($data->price_delivery_return_currency_rate > 0 ? '<br>' . Yii::t('common', 'курс') . ': ' . $data->price_delivery_return_currency_rate : '');
                    }
                ],
                [
                    'attribute' => 'price_address_correction',
                    'content' => function ($data) use ($currencyCollection) {
                        if (is_null($data->price_address_correction)) {
                            return 0;
                        }
                        return $data->price_address_correction . ' ' . ($data->price_address_correction_currency_id ? $currencyCollection[$data->price_address_correction_currency_id] ?? '' : '') .
                            ($data->price_address_correction_currency_rate > 0 ? '<br>' . Yii::t('common', 'курс') . ': ' . $data->price_address_correction_currency_rate : '');
                    }
                ],
                [
                    'attribute' => 'price_cod_service',
                    'content' => function ($data) use ($currencyCollection) {
                        if (is_null($data->price_cod_service)) {
                            return 0;
                        }
                        return $data->price_cod_service . ' ' . ($data->price_cod_service_currency_id ? $currencyCollection[$data->price_cod_service_currency_id] ?? '' : '') .
                            ($data->price_cod_service_currency_rate > 0 ? '<br>' . Yii::t('common', 'курс') . ': ' . $data->price_cod_service_currency_rate : '');
                    }
                ],
                [
                    'attribute' => 'price_vat',
                    'content' => function ($data) use ($currencyCollection) {
                        if (is_null($data->price_vat)) {
                            return 0;
                        }
                        return $data->price_vat . ' ' . ($data->price_vat_currency_id ? $currencyCollection[$data->price_vat_currency_id] ?? '' : '') .
                            ($data->price_vat_currency_rate > 0 ? '<br>' . Yii::t('common', 'курс') . ': ' . $data->price_vat_currency_rate : '');
                    }
                ],
                [
                    'attribute' => 'additional_prices',
                    'content' => function ($data) {
                        if (is_null($data->additional_prices)) {
                            return 0;
                        }
                        return $data->additional_prices;
                    }
                ],
                [
                    'label' => Yii::t('common', 'Отчет'),
                    'format' => 'raw',
                    'value' => function ($model) {
                        /** @var OrderFinanceFact $model */
                        if ($model->deliveryReport) {
                            return \yii\bootstrap\Html::a($model->deliveryReport->name, \yii\helpers\Url::toRoute([
                                '/deliveryreport/report/view',
                                'id' => $model->delivery_report_id,
                                'FieldNumberFilter' => [
                                    'entity' => 'order_id',
                                    'number' => $model->order_id
                                ]
                            ]));
                        } else {
                            return '—';
                        }
                    }
                ],
                [
                    'attribute' => 'created_at',
                    'class' => DateColumn::className(),
                ],
                [
                    'attribute' => 'updated_at',
                    'class' => DateColumn::className(),
                ],
                [
                    'attribute' => 'pretension',
                    'value' => function ($data) {
                        /** @var \app\modules\order\models\Order $data ->order */
                        if ($data->order->orderFinancePretensions) {
                            $count = [];
                            foreach ($data->order->orderFinancePretensions as $pretension) {
                                $pretensions[$pretension->type] = OrderFinancePretension::getTypeCollecion()[$pretension->type] ?? '';
                                if (isset($count[$pretension->type])) {
                                    $count[$pretension->type]++;
                                } else {
                                    $count[$pretension->type] = 1;
                                }
                            }
                            foreach ($pretensions as $key => $val) {
                                $pretensions[$key] .= ' (' . $count[$key] . ')';
                            }
                        }

                        return empty($pretensions) ? '' : implode(',' . PHP_EOL, $pretensions);
                        //return ($data->pretension && isset(OrderFinancePretension::getTypeCollecion()[$data->pretension])) ? OrderFinancePretension::getTypeCollecion()[$data->pretension] : '';
                    }
                ],
                [
                    'attribute' => 'payment_id',
                    'content' => function ($data) {
                        return $data->payment_id ? Html::a($data->payment->name, Url::to('/report/delivery-payment/index?s[name]=' . $data->payment->name)) : '';
                    }
                ],
            ]
        ]) ?>
    <?php endif; ?>

    <h3 class="text-primary"><?= Yii::t('common', 'Баланс') ?></h3>

    <?php if ($model->orderFinanceBalances) : ?>
        <?= GridView::widget([
            'dataProvider' => New ArrayDataProvider(['allModels' => $model->orderFinanceBalances]),
            'columns' => [
                [
                    'attribute' => 'price_cod',
                    'content' => function ($data) use ($currencyCollection) {
                        /** @var \app\modules\order\models\OrderFinanceBalance $data */
                        if (is_null($data->price_cod)) {
                            return 0;
                        }
                        return $data->price_cod . ' ' . ($data->price_cod_currency_id ? $currencyCollection[$data->price_cod_currency_id] ?? '' : '');
                    }
                ],
                [
                    'attribute' => 'price_storage',
                    'content' => function ($data) use ($currencyCollection) {
                        /** @var \app\modules\order\models\OrderFinanceBalance $data */
                        if (is_null($data->price_storage)) {
                            return 0;
                        }
                        return $data->price_storage . ' ' . ($data->price_storage_currency_id ? $currencyCollection[$data->price_storage_currency_id] ?? '' : '');
                    }
                ],
                [
                    'attribute' => 'price_fulfilment',
                    'content' => function ($data) use ($currencyCollection) {
                        /** @var \app\modules\order\models\OrderFinanceBalance $data */
                        if (is_null($data->price_fulfilment)) {
                            return 0;
                        }
                        return $data->price_fulfilment . ' ' . ($data->price_fulfilment_currency_id ? $currencyCollection[$data->price_fulfilment_currency_id] ?? '' : '');
                    }
                ],
                [
                    'attribute' => 'price_packing',
                    'content' => function ($data) use ($currencyCollection) {
                        /** @var \app\modules\order\models\OrderFinanceBalance $data */
                        if (is_null($data->price_packing)) {
                            return 0;
                        }
                        return $data->price_packing . ' ' . ($data->price_packing_currency_id ? $currencyCollection[$data->price_packing_currency_id] ?? '' : '');
                    }
                ],
                [
                    'attribute' => 'price_package',
                    'content' => function ($data) use ($currencyCollection) {
                        /** @var \app\modules\order\models\OrderFinanceBalance $data */
                        if (is_null($data->price_package)) {
                            return 0;
                        }
                        return $data->price_package . ' ' . ($data->price_package_currency_id ? $currencyCollection[$data->price_package_currency_id] ?? '' : '');
                    }
                ],
                [
                    'attribute' => 'price_delivery',
                    'content' => function ($data) use ($currencyCollection) {
                        /** @var \app\modules\order\models\OrderFinanceBalance $data */
                        if (is_null($data->price_delivery)) {
                            return 0;
                        }
                        return $data->price_delivery . ' ' . ($data->price_delivery_currency_id ? $currencyCollection[$data->price_delivery_currency_id] ?? '' : '');
                    }
                ],
                [
                    'attribute' => 'price_redelivery',
                    'content' => function ($data) use ($currencyCollection) {
                        /** @var \app\modules\order\models\OrderFinanceBalance $data */
                        if (is_null($data->price_redelivery)) {
                            return 0;
                        }
                        return $data->price_redelivery . ' ' . ($data->price_redelivery_currency_id ? $currencyCollection[$data->price_redelivery_currency_id] ?? '' : '');
                    }
                ],
                [
                    'attribute' => 'price_delivery_back',
                    'content' => function ($data) use ($currencyCollection) {
                        /** @var \app\modules\order\models\OrderFinanceBalance $data */
                        if (is_null($data->price_delivery_back)) {
                            return 0;
                        }
                        return $data->price_delivery_back . ' ' . ($data->price_delivery_back_currency_id ? $currencyCollection[$data->price_delivery_back_currency_id] ?? '' : '');
                    }
                ],
                [
                    'attribute' => 'price_delivery_return',
                    'content' => function ($data) use ($currencyCollection) {
                        /** @var \app\modules\order\models\OrderFinanceBalance $data */
                        if (is_null($data->price_delivery_return)) {
                            return 0;
                        }
                        return $data->price_delivery_return . ' ' . ($data->price_delivery_return_currency_id ? $currencyCollection[$data->price_delivery_return_currency_id] ?? '' : '');
                    }
                ],
                [
                    'attribute' => 'price_address_correction',
                    'content' => function ($data) use ($currencyCollection) {
                        /** @var \app\modules\order\models\OrderFinanceBalance $data */
                        if (is_null($data->price_address_correction)) {
                            return 0;
                        }
                        return $data->price_address_correction . ' ' . ($data->price_address_correction_currency_id ? $currencyCollection[$data->price_address_correction_currency_id] ?? '' : '');
                    }
                ],
                [
                    'attribute' => 'price_cod_service',
                    'content' => function ($data) use ($currencyCollection) {
                        /** @var \app\modules\order\models\OrderFinanceBalance $data */
                        if (is_null($data->price_cod_service)) {
                            return 0;
                        }
                        return $data->price_cod_service . ' ' . ($data->price_cod_service_currency_id ? $currencyCollection[$data->price_cod_service_currency_id] ?? '' : '');
                    }
                ],
                [
                    'attribute' => 'price_vat',
                    'content' => function ($data) use ($currencyCollection) {
                        /** @var \app\modules\order\models\OrderFinanceBalance $data */
                        if (is_null($data->price_vat)) {
                            return 0;
                        }
                        return $data->price_vat . ' ' . ($data->price_vat_currency_id ? $currencyCollection[$data->price_vat_currency_id] ?? '' : '');
                    }
                ],
                [
                    'label' => Yii::t('common', 'Служба доставки'),
                    'content' => function ($data) use ($currencyCollection) {
                        /** @var \app\modules\order\models\OrderFinanceBalance $data */
                        if ($data->deliveryRequest) {
                            return $data->deliveryRequest->delivery->name;
                        } else {
                            return '';
                        }
                    }
                ],
                [
                    'attribute' => 'created_at',
                    'class' => DateColumn::className(),
                ],
                [
                    'attribute' => 'updated_at',
                    'class' => DateColumn::className(),
                ],
            ]
        ]) ?>
    <?php endif; ?>
</div>