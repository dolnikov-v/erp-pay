<?php

use app\helpers\DataProvider;
use app\modules\order\assets\ChangerDeliveryAsset;
use app\modules\order\assets\ChangerStatusAsset;
use app\modules\order\assets\GeneratorListAsset;
use app\modules\order\assets\GeneratorPretensionListAsset;
use app\modules\order\assets\DeleteOrdersFromListsAsset;
use app\modules\order\assets\GeneratorMarketingListAsset;
use app\modules\order\assets\IndexAsset;
use app\modules\order\assets\OrderFiltersAsset;
use app\modules\order\assets\ResenderInCallCenterAsset;
use app\modules\order\assets\ResenderInCallCenterByExcelAsset;
use app\modules\order\assets\ResenderInCallCenterNewQueueAsset;
use app\modules\order\assets\ResenderInCallCenterStillWaitingAsset;
use app\modules\order\assets\ResenderInDeliveryAsset;
use app\modules\order\assets\ClarificationInDeliveryAsset;
use app\modules\order\assets\SecondDeliveryAttemptAsset;
use app\modules\order\assets\ResendSeparateOrderAsset;
use app\modules\order\assets\SenderInCallCenterAsset;
use app\modules\order\assets\SenderInDeliveryAsset;
use app\modules\order\assets\SendCheckOrderAsset;
use app\modules\order\assets\SendPostSellAsset;
use app\modules\order\assets\SendInformationAsset;
use app\modules\order\assets\ShowerColumnsAsset;
use app\modules\order\assets\SendAnalysisTrashAsset;
use app\modules\order\assets\SendCheckAddressAsset;
use app\modules\order\widgets\ChangerDelivery;
use app\modules\order\widgets\ChangerStatus;
use app\modules\order\widgets\DeliveryRequestStatusChanger;
use app\modules\order\widgets\GeneratorList;
use app\modules\order\widgets\GeneratorPretensionList;
use app\modules\order\widgets\DeleteOrdersFromLists;
use app\modules\order\widgets\GeneratorMarketingList;
use app\modules\order\widgets\orderFilters\OrderFilters;
use app\modules\order\widgets\ResenderInCallCenter;
use app\modules\order\widgets\ResenderInCallCenterByExcel;
use app\modules\order\widgets\ResenderInCallCenterNewQueue;
use app\modules\order\widgets\ResenderInCallCenterStillWaiting;
use app\modules\order\widgets\ResenderInDelivery;
use app\modules\order\widgets\ClarificationInDelivery;
use app\modules\order\widgets\SecondDeliveryAttempt;
use app\modules\order\widgets\ResendSeparateOrder;
use app\modules\order\widgets\SendCheckOrder;
use app\modules\order\widgets\SenderInCallCenter;
use app\modules\order\widgets\SenderInDelivery;
use app\modules\order\widgets\SendAnalysisTrash;
use app\modules\order\widgets\SendCheckAddress;
use app\modules\order\widgets\SendInQueue;
use app\modules\order\widgets\SendPostSell;
use app\modules\order\widgets\SendInformation;
use app\modules\order\widgets\TransferToDistributor;
use app\widgets\assets\InputGroupFileAsset;
use app\modules\order\assets\SendSmsPollAsset;
use app\modules\order\widgets\SendSmsPoll;
use app\modules\order\assets\InvoicePrintAsset;
use app\widgets\ButtonLink;
use app\widgets\LinkPager;
use app\widgets\Panel;
use yii\helpers\Url;
use yii\web\View;
use app\modules\order\widgets\RetryGetStatusFromCallCenter;
use app\modules\order\assets\RetryGetStatusFromCallCenterAsset;
use app\modules\order\assets\ResendSeparateOrderIntoDeliveryAsset;
use app\modules\order\widgets\ResendSeparateOrderIntoDelivery;
use app\modules\order\widgets\ResenderInCallCenterReturnNoProd;
use app\modules\order\assets\ResenderInCallCenterReturnNoProdAsset;
use app\modules\order\assets\SendCheckUndeliveryOrderAsset;
use app\modules\order\widgets\SendCheckUndeliveryOrder;
use app\modules\order\assets\DeliveryRequestStatusChangerAsset;
use app\modules\order\assets\CallCenterRequestStatusChangerAsset;
use app\modules\order\widgets\CallCenterRequestStatusChanger;
use app\modules\order\assets\TransferToDistributorAsset;

/** @var yii\web\View $this */
/** @var app\modules\order\models\search\OrderSearch $modelSearch */
/** @var app\modules\order\widgets\ShowerColumns $showerColumns */
/** @var yii\data\ActiveDataProvider $dataProvider */
/** @var array $statuses */
/** @var array $products */
/** @var array $deliveries */
/** @var array $callCenters */
/** @var string $alert */
/** @var mixed $alertStyle */
/** @var array $distributorCountry */

IndexAsset::register($this);
OrderFiltersAsset::register($this);
ShowerColumnsAsset::register($this);

ChangerDeliveryAsset::register($this);
ResendSeparateOrderAsset::register($this);
SendCheckOrderAsset::register($this);
ChangerStatusAsset::register($this);
SenderInCallCenterAsset::register($this);
ResenderInCallCenterAsset::register($this);
ResenderInCallCenterByExcelAsset::register($this);
ResenderInCallCenterNewQueueAsset::register($this);
ResenderInCallCenterStillWaitingAsset::register($this);
ResenderInCallCenterReturnNoProdAsset::register($this);
GeneratorListAsset::register($this);
GeneratorPretensionListAsset::register($this);
DeleteOrdersFromListsAsset::register($this);
GeneratorMarketingListAsset::register($this);
SenderInDeliveryAsset::register($this);
ResenderInDeliveryAsset::register($this);
ClarificationInDeliveryAsset::register($this);
SecondDeliveryAttemptAsset::register($this);
RetryGetStatusFromCallCenterAsset::register($this);
SendSmsPollAsset::register($this);
SendAnalysisTrashAsset::register($this);
SendCheckAddressAsset::register($this);
DeliveryRequestStatusChangerAsset::register($this);
CallCenterRequestStatusChangerAsset::register($this);

ResendSeparateOrderIntoDeliveryAsset::register($this);
InvoicePrintAsset::register($this);
SendCheckUndeliveryOrderAsset::register($this);

InputGroupFileAsset::register($this);
TransferToDistributorAsset::register($this);
SendPostSellAsset::register($this);
SendInformationAsset::register($this);


$this->registerJs("var I18n = {};", View::POS_HEAD);
$this->registerJs("I18n['Необходимо выбрать заказы.'] = '" . Yii::t('common', 'Необходимо выбрать заказы.') . "';",
    View::POS_HEAD);
$this->registerJs("I18n['Не удалось сменить статус.'] = '" . Yii::t('common', 'Не удалось сменить статус.') . "';",
    View::POS_HEAD);
$this->registerJs("I18n['Создание копий и отправка в КЦ на обзвон завершено.'] = '" . Yii::t('common',
        'Создание копий и отправка в КЦ на обзвон завершено.') . "';", View::POS_HEAD);
$this->registerJs("I18n['Смена доставки завершена.'] = '" . Yii::t('common', 'Смена доставки завершена.') . "';",
    View::POS_HEAD);
$this->registerJs("I18n['Не удалось отправить заказ в колл-центр.'] = '" . Yii::t('common',
        'Не удалось отправить заказ в колл-центр.') . "';", View::POS_HEAD);
$this->registerJs("I18n['Отправка заказов в колл-центр завершена.'] = '" . Yii::t('common',
        'Отправка заказов в колл-центр завершена.') . "';", View::POS_HEAD);
$this->registerJs("I18n['Не удалось передать заказ в службу доставки.'] = '" . Yii::t('common',
        'Не удалось передать заказ в службу доставки.') . "';", View::POS_HEAD);
$this->registerJs("I18n['Передача заказов в службу доставки завершена.'] = '" . Yii::t('common',
        'Передача заказов в службу доставки завершена.') . "';", View::POS_HEAD);
$this->registerJs("I18n['Не удалось поставить заявку в очередь на обновление статуса.'] = '" . Yii::t('common',
        'Не удалось поставить заявку в очередь на обновление статуса.') . "';", View::POS_HEAD);
$this->registerJs("I18n['Поднятие заказов в очереди на обновление статусов из КЦ завершено.'] = '" . Yii::t('common',
        'Поднятие заказов в очереди на обновление статусов из КЦ завершено.') . "';", View::POS_HEAD);
$this->registerJs("I18n['Не удалось создать копию отправки и отправить в службу доставки.'] = '" . Yii::t('common',
        'Не удалось создать копию отправки и отправить в службу доставки.') . "';", View::POS_HEAD);
$this->registerJs("I18n['Создание копий и отправка в службу доставки завершено.'] = '" . Yii::t('common',
        'Создание копий и отправка в службу доставки завершено.') . "';", View::POS_HEAD);

$this->registerJs("I18n['Выберите получателей.'] = '" . Yii::t('common', 'Выберите получателей.') . "';",
    View::POS_HEAD);
$this->registerJs("I18n['Введите тему.'] = '" . Yii::t('common', 'Введите тему.') . "';", View::POS_HEAD);
$this->registerJs("I18n['Введите сообщение.'] = '" . Yii::t('common', 'Введите сообщение.') . "';", View::POS_HEAD);
$this->registerJs("I18n['Введите комментарий.'] = '" . Yii::t('common', 'Введите комментарий.') . "';", View::POS_HEAD);
$this->registerJs("I18n['Сформирована очередь отправки на уточнение.'] = '" . Yii::t('common',
        'Сформирована очередь отправки на уточнение.') . "';", View::POS_HEAD);
$this->registerJs("I18n['Не удалось изменить заказ.'] = '" . Yii::t('common', 'Не удалось изменить заказ.') . "';",
    View::POS_HEAD);
$this->registerJs("I18n['Изменение заказов завершено.'] = '" . Yii::t('common', 'Изменение заказов завершено.') . "';",
    View::POS_HEAD);

$this->registerJs("I18n['Передача заказов завершена.'] = '" . Yii::t('common', 'Передача заказов завершена.') . "';",
    View::POS_HEAD);
$this->registerJs("I18n['Передача заказов дистрибьютору завершена.'] = '" . Yii::t('common', 'Передача заказов дистрибьютору завершена.') . "';",
    View::POS_HEAD);
$this->registerJs("I18n['Не удалось отправить заказы дистребьютору'] = '" . Yii::t('common', 'Не удалось отправить заказы дистребьютору') . "';",
    View::POS_HEAD);

$this->title = Yii::t('common', 'Список заказов');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Заказы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= OrderFilters::widget([
    'modelSearch' => $modelSearch,
]); ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с заказами'),
    'alert' => $alert,
    'alertStyle' => $alertStyle,
    'actions' => DataProvider::renderSummary($dataProvider) . $showerColumns->renderIcon(),
    'withBody' => false,
    'content' => $this->render('_index-content', [
        'dataProvider' => $dataProvider,
        'showerColumns' => $showerColumns,
        'statuses' => $statuses,
        'products' => $products,
        'deliveries' => $deliveries,
        'callCenters' => $callCenters,
    ]),
    'footer' => (Yii::$app->user->can('order.index.edit') ?
            ButtonLink::widget([
                'url' => Url::toRoute('edit'),
                'label' => Yii::t('common', 'Добавить заказ'),
                'style' => ButtonLink::STYLE_SUCCESS,
                'size' => ButtonLink::SIZE_SMALL,
            ]) : '') . LinkPager::widget([
            'pagination' => $dataProvider->getPagination(),
            'counterPosition' => LinkPager::COUNTER_POSITION_RIGHT,
        ]),
]); ?>

<?= $showerColumns->renderModal(); ?>

<?php if (Yii::$app->user->can('order.index.changestatus')): ?>
    <?= ChangerStatus::widget([
        'url' => Url::toRoute('change-status'),
    ]); ?>
<?php endif; ?>

<?php if (Yii::$app->user->can('order.index.changedelivery')): ?>
    <?= ChangerDelivery::widget([
        'url' => Url::toRoute('change-delivery'),
    ]); ?>
<?php endif; ?>

<?php if (Yii::$app->user->can('order.index.resendseparateorder')): ?>
    <?= ResendSeparateOrder::widget([
        'url' => Url::toRoute('resend-separate-order'),
    ]); ?>
<?php endif; ?>
<?php if (Yii::$app->user->can('order.index.resendseparateorderintodelivery')): ?>
    <?= ResendSeparateOrderIntoDelivery::widget([
        'url' => Url::toRoute('resend-separate-order-into-delivery')
    ])
    ?>
<?php endif; ?>

<?php if (Yii::$app->user->can('order.index.sendcheckorder')): ?>
    <?= SendCheckOrder::widget([
        'url' => Url::toRoute('send-check-order'),
    ]); ?>
<?php endif; ?>

<?php if (Yii::$app->user->can('order.index.sendcheckundeliveryorder')): ?>
    <?= SendCheckUndeliveryOrder::widget([
        'url' => Url::toRoute('send-check-undelivery-order'),
    ]); ?>
<?php endif; ?>


<?php if (Yii::$app->user->can('order.index.createcallcenterrequest')): ?>
    <?= SenderInCallCenter::widget([
        'url' => Url::toRoute('create-call-center-request'),
    ]); ?>
<?php endif; ?>

<?php if (Yii::$app->user->can('order.index.resendincallcenter')): ?>
    <?= ResenderInCallCenter::widget([
        'url' => Url::toRoute('resend-in-call-center'),
    ]); ?>
<?php endif; ?>

<?php if (Yii::$app->user->can('order.index.resendincallcenter')): ?>
    <?= ResenderInCallCenterByExcel::widget([
        'url' => Url::toRoute('resend-in-call-center-by-excel'),
    ]); ?>
<?php endif; ?>

<?php if (Yii::$app->user->can('order.index.resendincallcenternewqueue')): ?>
    <?= ResenderInCallCenterNewQueue::widget([
        'url' => Url::toRoute('resend-in-call-center-new-queue'),
    ]); ?>
<?php endif; ?>

<?php if (Yii::$app->user->can('order.index.resendincallcenterstillwaiting')): ?>
    <?= ResenderInCallCenterStillWaiting::widget([
        'url' => Url::toRoute('resend-in-call-center-still-waiting'),
    ]); ?>
<?php endif; ?>

<?php if (Yii::$app->user->can('order.index.resendincallcenterreturnnoprod')): ?>
    <?= ResenderInCallCenterReturnNoProd::widget([
        'url' => Url::toRoute('resend-in-call-center-return-no-prod'),
    ]); ?>
<?php endif; ?>

<?php if (Yii::$app->user->can('order.index.resendincallcenterstillwaiting')
    || Yii::$app->user->can('order.index.resendincallcenterreturnnoprod')
    || Yii::$app->user->can('order.index.resendincallcenternewqueue')
    || Yii::$app->user->can('order.index.sendcheckorder')
    || Yii::$app->user->can('order.index.sendcheckundeliveryorder')
    || Yii::$app->user->can('order.index.sendanalysistrash')): ?>
    <?= SendInQueue::widget([
        'url' => Url::toRoute('send-in-queue'),
    ]); ?>
<?php endif; ?>

<?php if (Yii::$app->user->can('order.index.sendindelivery')): ?>
    <?= SenderInDelivery::widget([
        'url' => Url::toRoute('send-in-delivery'),
    ]); ?>
<?php endif; ?>

<?php if (Yii::$app->user->can('order.index.resendindelivery')): ?>
    <?= ResenderInDelivery::widget([
        'url' => Url::toRoute('resend-in-delivery'),
    ]); ?>
<?php endif; ?>

<?php if (Yii::$app->user->can('order.index.clarificationdelivery')): ?>
    <?= ClarificationInDelivery::widget([
        'url' => Url::toRoute('clarification-in-delivery'),
        'deliveryUrl' => Url::toRoute('get-delivery-email'),
    ]); ?>
<?php endif; ?>

<?php if (Yii::$app->user->can('order.index.seconddeliveryattempt')): ?>
    <?= SecondDeliveryAttempt::widget([
        'url' => Url::toRoute('second-delivery-attempt'),
    ]); ?>
<?php endif; ?>

<?php if (Yii::$app->user->can('order.index.generatelist')): ?>
    <?= GeneratorList::widget([
        'url' => Url::toRoute('generate-list'),
    ]); ?>
<?php endif; ?>

<?php if (Yii::$app->user->can('order.index.generatepretensionlist')): ?>
    <?= GeneratorPretensionList::widget([
        'url' => Url::toRoute('generate-pretension-list'),
    ]); ?>
<?php endif; ?>

<?php if (Yii::$app->user->can('order.index.deleteordersfromlists')): ?>
    <?= DeleteOrdersFromLists::widget([
        'url' => Url::toRoute('delete-orders-from-lists'),
    ]); ?>
<?php endif; ?>

<?php if (Yii::$app->user->can('order.index.generatemarketinglist')): ?>
    <?= GeneratorMarketingList::widget([
        'url' => Url::toRoute('generate-marketing-list'),
    ]); ?>
<?php endif; ?>

<?php if (Yii::$app->user->can('order.index.retrygetstatusfromcallcenter')): ?>
    <?= RetryGetStatusFromCallCenter::widget([
        'url' => Url::toRoute('retry-get-status-from-call-center'),
    ]); ?>
<?php endif; ?>


<?php if (Yii::$app->user->can('order.index.sendsmspoll')): ?>
    <?= SendSmsPoll::widget([
        'url' => Url::toRoute('send-sms-poll'),
    ]); ?>
<?php endif; ?>

<?php if (Yii::$app->user->can('order.index.sendanalysistrash')): ?>
    <?= SendAnalysisTrash::widget([
        'url' => Url::toRoute('send-analysis-trash'),
    ]); ?>
<?php endif; ?>

<?php if (Yii::$app->user->can('order.index.changedeliveryrequeststatus')): ?>
    <?= DeliveryRequestStatusChanger::widget([
        'url' => Url::toRoute('change-delivery-request-status'),
    ]) ?>
<?php endif; ?>

<?php if (Yii::$app->user->can('order.index.changecallcenterrequeststatus')): ?>
    <?= CallCenterRequestStatusChanger::widget([
        'url' => Url::toRoute('change-call-center-request-status'),
    ]) ?>
<?php endif; ?>

<?php if (Yii::$app->user->can('order.index.sendcheckaddress')): ?>
    <?= SendCheckAddress::widget([
        'url' => Url::toRoute('send-check-address'),
    ]); ?>
<?php endif; ?>

<?php if (Yii::$app->user->can('order.index.transfertodistributor')): ?>
    <?= TransferToDistributor::widget([
        'url' => Url::toRoute('transfer-to-distributor'),
        'distributorCountry' => $distributorCountry
    ]); ?>
<?php endif; ?>

<?php if (Yii::$app->user->can('order.index.sendpostsell')): ?>
    <?= SendPostSell::widget([
        'url' => Url::toRoute('send-post-sell')
    ]); ?>
<?php endif; ?>

<?php if (Yii::$app->user->can('order.index.sendinformation')): ?>
    <?= SendInformation::widget([
        'url' => Url::toRoute('send-information')
    ]); ?>
<?php endif; ?>
