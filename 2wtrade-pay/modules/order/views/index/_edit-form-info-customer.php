<?php

use yii\helpers\BaseHtml;

/** @var app\modules\order\models\Order $model */
/** @var app\components\widgets\ActiveForm $form */
/** @var bool $isView */
?>

<div class="col-lg-12 margin-top-20">
    <h4><?= Yii::t('common', 'Информация о покупателе') ?></h4>
</div>
<?php if (Yii::$app->user->can('order.customer_full_name')): ?>
    <div class="col-lg-12">
        <?= $form->field($model, 'customer_full_name')->textInput(['readonly' => $isView]) ?>
    </div>
<?php endif; ?>
<?php if (Yii::$app->user->can('order.customer_phone')): ?>
    <div class="col-lg-6">
        <?= $form->field($model, 'customer_phone')->textInput(['readonly' => $isView]) ?>
    </div>
<?php endif; ?>
<?php if (Yii::$app->user->can('order.customer_mobile')): ?>
    <div class="col-lg-6">
        <?= $form->field($model, 'customer_mobile')->textInput(['readonly' => $isView]) ?>
    </div>
<?php endif; ?>
<?php if (Yii::$app->user->can('order.customer_city')): ?>
    <div class="col-lg-6">
        <?= $form->field($model, 'customer_city')->textInput(['readonly' => $isView]) ?>
    </div>
<?php endif; ?>
<?php if (Yii::$app->user->can('order.customer_zip')): ?>
    <div class="col-lg-6">
        <?= $form->field($model, 'customer_zip')->textInput(['readonly' => $isView]) ?>
    </div>
<?php endif; ?>
<?php if (Yii::$app->user->can('order.customer_address')): ?>
    <div class="col-lg-6">
        <?= $form->field($model, 'customer_address')->textInput(['readonly' => $isView]) ?>
    </div>
<?php endif; ?>
<?php if (Yii::$app->user->can('order.customer_email')): ?>
    <div class="col-lg-6">
        <?= $form->field($model, 'customer_email')->textInput(['readonly' => $isView]) ?>
    </div>
<?php endif; ?>
<?php if (Yii::$app->user->can('order.customer_address_additional')): ?>
    <div class="col-lg-6">
        <?= $form->field($model, 'customer_address_additional')->textInput(['readonly' => $isView]) ?>
    </div>
<?php endif; ?>
<?php if (Yii::$app->user->can('order.customer_address_add')): ?>
    <div class="col-lg-6">
        <?= $form->field($model, 'customer_address_add')->textInput([
            'value' => BaseHtml::encode($model->customer_address_add),
            'readonly' => $isView
        ]) ?>
    </div>
<?php endif; ?>
<?php if (Yii::$app->user->can('order.customer_street_house')): ?>
    <div class="col-lg-6">
        <?= $form->field($model, 'customer_street')->textInput(['readonly' => $isView]) ?>
    </div>
    <div class="col-lg-6">
        <?= $form->field($model, 'customer_house_number')->textInput(['readonly' => $isView]) ?>
    </div>
<?php endif; ?>
<?php if (Yii::$app->user->can('order.customer_province')): ?>
    <div class="col-lg-6">
        <?= $form->field($model, 'customer_province')->textInput(['readonly' => $isView]) ?>
    </div>
<?php endif; ?>
<?php if (Yii::$app->user->can('order.customer_district')): ?>
    <div class="col-lg-6">
        <?= $form->field($model, 'customer_district')->textInput(['readonly' => $isView]) ?>
    </div>
<?php endif; ?>
<div class="col-lg-12">
    <?= $form->field($model, 'comment')->textarea(['readonly' => $isView]) ?>
</div>
