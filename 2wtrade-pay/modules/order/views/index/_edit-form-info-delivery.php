<?php
use app\modules\delivery\models\DeliveryRequest;

/** @var DeliveryRequest $modelDeliveryRequest */
/** @var \app\modules\order\models\Order $model */
/** @var app\components\widgets\ActiveForm $form */
/** @var array $deliveryServices */
/** @var boolean $isView */
?>
<div class="col-lg-12 margin-top-20">
    <h4><?= Yii::t('common', 'Служба доставки') ?></h4>
</div>
<?php if ($modelDeliveryRequest): ?>
    <?php $statuses = DeliveryRequest::getStatusesCollection(); ?>

    <div class="col-lg-3">
        <?= $form->field($modelDeliveryRequest, 'delivery_id')->select2List($deliveryServices, [
            'disabled' => $isView,
        ]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($modelDeliveryRequest, 'status')->textInput([
            'value' => $statuses[$modelDeliveryRequest->status],
            'name' => 'status_name',
            'readonly' => true,
        ]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'delivery')->textInput([
            'readonly' => $isView,
        ]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($modelDeliveryRequest, 'updated_at')->textInput([
            'value' => Yii::$app->formatter->asDatetime($modelDeliveryRequest->updated_at),
            'name' => 'updated_at_name',
            'readonly' => true,
        ])
        ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($modelDeliveryRequest, 'tracking')->textInput([
            'readonly' => $isView,
            'placeholder' => ''
        ]) ?>
    </div>
    <div class="col-lg-9">
        <?= $form->field($modelDeliveryRequest, 'api_error')->textInput([
            'readonly' => true,
            'placeholder' => '',
            'value' => htmlspecialchars($modelDeliveryRequest->api_error)
        ]) ?>
    </div>
<?php else: ?>
    <div class="col-lg-12 margin-bottom-20">
        <?= Yii::t('common', 'Информация о службе доставки отсутствует') ?>
    </div>
<?php endif; ?>
