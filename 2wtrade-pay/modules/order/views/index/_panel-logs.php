<?php

use app\widgets\Label;
use app\components\widgets\PanelList;
use app\components\grid\DateColumn;
use app\helpers\logs\Route;

/** @var array $logsGroup */
?>

<?= PanelList::widget([
    'data' => $logsGroup,
    'groupBy' => 'group_id',
    'id' => 'panel_tab_update',
    'titleTemplates' => ['revertNum', 'created_at'],
    'titleContent' => Label::widget(['label' => '{route}', 'style' => Label::STYLE_INFO]) . ' ' . '{user.username}',
    'content' => [
        'user.username',
        [
            'attribute' => 'route',
            'content' => function ($model) {

                $route = Route::getRouteGroup($model->route);

                $style = Label::STYLE_PRIMARY;
                if ($route == Route::GROUP_CALL_CENTER) {
                    $style = Label::STYLE_INFO;
                } elseif ($route == Route::GROUP_DELIVERY) {
                    $style = Label::STYLE_SUCCESS;
                }

                return Label::widget([
                    'label' => $route != "" ? Route::getGroupDescription($route) : Yii::t('common', 'Неопределено'),
                    'style' => $route != "" ? $style : Label::STYLE_DEFAULT
                ]);
            }
        ],
        'field',
        'old',
        'new',
        [
            'attribute' => 'comment',
            'value' => function ($model) {
                return $model->comment ?? '';
            }
        ],
        [
            'class' => DateColumn::className(),
            'attribute' => 'created_at',
            'enableSorting' => false,
            'label' => Yii::t('common', 'Дата')
        ]
    ]
]) ?>
