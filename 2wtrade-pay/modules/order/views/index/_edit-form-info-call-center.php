<?php

use app\modules\callcenter\models\CallCenterRequest;

/** @var CallCenterRequest $modelCallCenterRequest */
/** @var \app\modules\order\models\Order $modelOrder */
/** @var app\components\widgets\ActiveForm $form */
/** @var array $callCenters */
/** @var boolean $isView */
?>
<div class="col-lg-12 margin-top-20">
    <h4><?= Yii::t('common', 'Колл-центр') ?></h4>
</div>
<?php if ($modelCallCenterRequest): ?>
    <?php
    $statuses = CallCenterRequest::getStatusesCollection();
    ?>

    <div class="col-lg-6">
        <?= $form->field($modelCallCenterRequest, 'call_center_id')->select2List($callCenters, [
            'disabled' => $isView,
        ]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($modelCallCenterRequest, 'status')->select2List($statuses, [
            'disabled' => $isView
        ]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($modelCallCenterRequest, 'updated_at')->textInput([
            'value' => Yii::$app->formatter->asDatetime($modelCallCenterRequest->updated_at),
            'name' => 'updated_at_name',
            'readonly' => true,
        ])
        ?>
    </div>
    <div class="col-lg-6">
        <?= $form->field($modelOrder, 'call_center_type')->select2List($modelOrder::getTypesCallCenter(), [
            'disabled' => $isView
        ]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($modelOrder, 'call_center_again')->select2List($modelOrder::getBooleanOptions(), [
            'disabled' => true
        ]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($modelOrder, 'call_center_create')->select2List($modelOrder::getBooleanOptions(), [
            'disabled' => true
        ]) ?>
    </div>
    <div class="col-lg-6">
        <?= $form->field($modelCallCenterRequest, 'autocheck_address')
            ->select2List(CallCenterRequest::autoCheckAddressLabels(), [
                'disabled' => true
            ]) ?>
    </div>
    <div class="col-lg-12">
        <?= $form->field($modelCallCenterRequest, 'comment')->textarea(['disabled' => $isView]) ?>
    </div>
<?php else: ?>
    <div class="col-lg-12 margin-bottom-20">
        <?= Yii::t('common', 'Информация о колл-центре отсутствует') ?>
    </div>
<?php endif; ?>

