<?php

use app\components\widgets\ActiveForm;
use app\modules\order\models\IssueCollector;
use app\modules\order\widgets\CKEditor;
use app\widgets\custom\Checkbox;
use app\widgets\Select2;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var array $statusList */
/** @var array $deliveryList */
/** @var \yii\web\View $this */
/** @var \app\modules\order\models\OrderNotification $model */

?>

<?php $form = ActiveForm::begin([
    'method' => 'post',
]); ?>

<div class="row">
    <div class="col-lg-12">
        <?= $form->field($model, 'name')->textInput() ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <?= $form->field($model, 'text')->widget(CKEditor::className()) ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-6">
        <?= $form->field($model, 'sms_text')->widget(CKEditor::className()) ?>
    </div>
    <div class="col-lg-6">
        <?= $form->field($model, 'email_text')->widget(CKEditor::className()) ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <?= $form->field($model, 'statuses')->select2ListMultiple($statusList, [
            'value' => $model->getOrderStatusesAsArray()
        ]) ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <?= $form->field($model, 'deliveryList')->select2ListMultiple($deliveryList, [
            'value' => $model->getDeliveryAsArray()
        ]) ?>
    </div>
</div>

<div class="form-group clearfix">
    <?= Checkbox::widget([
        'id' => Html::getInputId($model, 'sms_active'),
        'name' => Html::getInputName($model, 'sms_active'),
        'value' => 1,
        'style' => 'checkbox-inline checkbox-lg pull-left cur-p',
        'label' => Yii::t('common', 'Отправлять по SMS'),
        'checked' => (boolean)$model->sms_active,
        'uncheckedValue' => 0,
    ]) ?>
</div>

<div class="form-group clearfix">
    <?= Checkbox::widget([
        'id' => Html::getInputId($model, 'email_active'),
        'name' => Html::getInputName($model, 'email_active'),
        'value' => 1,
        'style' => 'checkbox-inline checkbox-lg pull-left cur-p',
        'label' => Yii::t('common', 'Отправлять по E-mail'),
        'checked' => (boolean)$model->email_active,
        'uncheckedValue' => 0,
    ]) ?>
</div>

<div class="form-group clearfix">
    <?= Checkbox::widget([
        'id' => Html::getInputId($model, 'active'),
        'name' => Html::getInputName($model, 'active'),
        'value' => 1,
        'style' => 'checkbox-inline checkbox-lg pull-left cur-p',
        'label' => Yii::t('common', 'Активность'),
        'checked' => (boolean)$model->active,
        'uncheckedValue' => 0,
    ]) ?>
</div>

<div class="form-group clearfix">
    <?= Checkbox::widget([
        'id' => Html::getInputId($model, 'is_just_one_time'),
        'name' => Html::getInputName($model, 'is_just_one_time'),
        'value' => 1,
        'style' => 'checkbox-inline checkbox-lg pull-left cur-p',
        'label' => Yii::t('common', 'Отправлять 1 раз'),
        'checked' => (boolean)$model->is_just_one_time,
        'uncheckedValue' => 0,
    ]) ?>
</div>

<div class="form-group clearfix">
    <?= Checkbox::widget([
        'id' => Html::getInputId($model, 'answerable'),
        'name' => Html::getInputName($model, 'answerable'),
        'value' => 1,
        'style' => 'checkbox-inline checkbox-lg pull-left cur-p',
        'label' => Yii::t('common', 'Возможен ответ'),
        'checked' => (boolean)$model->answerable,
        'uncheckedValue' => 0,
    ]) ?>
</div>

<div class="row form-group clearfix">
    <div class="col-lg-4 margin-left-35">
        <label><?= Yii::t('common', 'Обратная связь через Jira issue Collector') ?></label>
    </div>
    <div class="col-lg-4">
        <?= Select2::widget([
            'name' => 'issue_collector_id',
            'items' => IssueCollector::find()->collection(),
            'value' => $model->issue_collector_id,
            'length' => -1,
            'autoEllipsis' => true,
            'prompt' => '-',
            'defaultValue' => null,
            //'placeholder' => 'Обратная связь через Jira issue Collector?',
        ]) ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('common', 'Добавить') : Yii::t('common', 'Сохранить'),
            ['class' => 'btn btn-success btn-sm']) ?>
        <?= Html::a(Yii::t('common', 'Отмена'), Url::toRoute('index'), [
            'class' => 'btn btn-default btn-sm '
        ]) ?>
    </div>
</div>

<?php ActiveForm::end() ?>
