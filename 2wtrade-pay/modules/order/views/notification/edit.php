<?php

use app\widgets\Panel;

/** @var array $statusList */
/** @var array $deliveryList */
/** @var \yii\web\View $this */
/** @var \app\modules\order\models\OrderNotification $model */

$this->title = $model->isNewRecord ? Yii::t('common', 'Создание уведомления') : Yii::t('common',
    'Редактирование уведомления');

$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Уведомления'), 'url' => 'index'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Список уведомлений'), 'url' => 'index'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

?>

<div class="create-order-notification">
    <?= Panel::widget([
        'title' => Yii::t('common', 'Уведомление'),
        'content' => $this->render('_form', [
            'model' => $model,
            'statusList' => $statusList,
            'deliveryList' => $deliveryList,
        ])
    ]); ?>
</div>
