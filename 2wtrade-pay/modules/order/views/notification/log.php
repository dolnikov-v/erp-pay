<?php

use app\widgets\Log;
use yii\helpers\Url;

/**
 * @var \app\models\logs\TableLog[] $changeLog
 */

$this->title = Yii::t('common', 'Лог изменений');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Уведомления'), 'url' => 'index'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Список уведомлений'), 'url' => 'index'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Log::widget([
    'data' => $changeLog,
]); ?>