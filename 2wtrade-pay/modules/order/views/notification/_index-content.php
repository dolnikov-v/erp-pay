<?php

use app\components\grid\ActionColumn;
use app\components\grid\GridView;
use app\widgets\Label;
use yii\helpers\Url;

/** @var $this \yii\web\View */
/** @var $dataProvider \yii\data\ActiveDataProvider */
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'class' => 'yii\grid\SerialColumn',
            'headerOptions' => ['class' => 'width-100 text-center'],
            'contentOptions' => ['class' => 'text-center'],
        ],
        [
            'attribute' => 'name',
        ],
        [
            'attribute' => 'all_text',
            'label' => Yii::t('common', 'Текст'),
            'content' => function ($model) {
                $content = '';
                /** @var \app\modules\order\models\OrderNotification $model */
                $smsText = $model->sms_text ?: $model->text;
                $smsEmail = $model->email_text ?: $model->text;
                if ($smsText == $smsEmail) {
                    $content = Label::widget(['label' => 'SMS | Email', 'style' => Label::STYLE_DARK]) . nl2br($smsText);
                } else {
                    if ($smsText) {
                        $content .= Label::widget(['label' => 'SMS', 'style' => Label::STYLE_DARK]) . nl2br($smsText . PHP_EOL . PHP_EOL);
                    }
                    if ($smsEmail) {
                        $content .= Label::widget(['label' => 'Email', 'style' => Label::STYLE_DARK]) . nl2br($smsEmail);
                    }
                }
                return $content;
            }
        ],
        [
            'attribute' => 'statuses',
            'label' => Yii::t('common', 'Статусы'),
            'content' => function ($model) {
                $content = '';

                /** @var \app\modules\order\models\OrderNotification $model */
                if ($statuses = $model->getOrderStatusesAsModels()) {
                    foreach ($statuses as $status) {
                        $content .= Label::widget([
                            'label' => Yii::t('common', $status->name),
                            'style' => $status->getLabelStyle(),
                            'outline' => true,
                        ]);
                    }
                }

                return $content;
            },
            'enableSorting' => false,
        ],
        [
            'attribute' => 'deliveries',
            'content' => function ($model) {
                $content = '';

                /** @var \app\modules\order\models\OrderNotification $model */
                if ($statuses = $model->getDeliveryAsModel()) {
                    foreach ($statuses as $status) {
                        $content .= Label::widget([
                            'label' => Yii::t('common', $status->name),
                            'style' => Label::STYLE_PRIMARY,
                            'outline' => true,
                        ]);
                    }
                }

                return $content;
            },
            'enableSorting' => false,
        ],
        [
            'attribute' => 'active',
            'content' => function ($model) {
                return Label::widget([
                    'label' => $model->active ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                    'style' => $model->active ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                ]);
            },
            'headerOptions' => ['class' => 'width-100 text-center'],
            'contentOptions' => ['class' => 'text-center'],
        ],
        [
            'attribute' => 'transport',
            'label' => Yii::t('common', 'Транспорт'),
            'content' => function ($model) {
                return
                    Label::widget([
                        'label' => Yii::t('common', 'sms'),
                        'style' => $model->sms_active ? Label::STYLE_PRIMARY : Label::STYLE_DEFAULT,
                    ])
                    . ' ' .
                    Label::widget([
                        'label' => Yii::t('common', 'email'),
                        'style' => $model->email_active ? Label::STYLE_PRIMARY : Label::STYLE_DEFAULT,
                    ]);
            },
            'headerOptions' => ['class' => 'width-100 text-center'],
            'contentOptions' => ['class' => 'text-center'],
        ],
        [
            'attribute' => 'is_just_one_time',
            'headerOptions' => ['class' => 'width-100 text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'content' => function ($data) {
                return Label::widget([
                    'label' => $data->is_just_one_time ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                    'style' => $data->is_just_one_time ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                ]);
            },
            'enableSorting' => false,
        ],
        [
            'attribute' => 'answerable',
            'headerOptions' => ['class' => 'width-100 text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'content' => function ($data) {
                return Label::widget([
                    'label' => $data->answerable ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                    'style' => $data->answerable ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                ]);
            },
            'enableSorting' => false,
        ],
        [
            'attribute' => 'issue_collector_id',
            'headerOptions' => ['class' => 'width-100 text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'content' => function ($model, $issueCollectorCollection) {
                /** @var \app\modules\order\models\OrderNotification $model */
                return $model->issueCollector->name??' - ';
            },
            'enableSorting' => false,
        ],
        [
            'class' => ActionColumn::className(),
            'items' => [
                [
                    'label' => Yii::t('common', 'Редактировать'),
                    'url' => function ($model) {
                        return Url::toRoute(['edit', 'id' => $model->id]);
                    },
                    'can' => function () {
                        return Yii::$app->user->can('order.notification.edit');
                    }
                ],
                [
                    'label' => Yii::t('common', 'Активировать'),
                    'url' => function ($model) {
                        return Url::toRoute(['activate', 'id' => $model->id]);
                    },
                    'can' => function ($model) {
                        return Yii::$app->user->can('order.notification.activate') && !$model->active;
                    }
                ],
                [
                    'label' => Yii::t('common', 'Деактивировать'),
                    'url' => function ($model) {
                        return Url::toRoute(['deactivate', 'id' => $model->id]);
                    },
                    'can' => function ($model) {
                        return Yii::$app->user->can('order.notification.deactivate') && $model->active;
                    }
                ],
                [
                    'label' => Yii::t('common', 'Логи'),
                    'url' => function ($model) {
                        return Url::toRoute(['log', 'id' => $model->id]);
                    },
                    'can' => function () {
                        return Yii::$app->user->can('order.notification.log');
                    }
                ],
                [
                    'can' => function () {
                        return Yii::$app->user->can('order.notification.delete');
                    }
                ],
                [
                    'label' => Yii::t('common', 'Удалить'),
                    'url' => function () {
                        return '#';
                    },
                    'style' => 'confirm-delete-link',
                    'attributes' => function ($model) {
                        return [
                            'data-href' => Url::toRoute(['delete', 'id' => $model->id]),
                        ];
                    },
                    'can' => function () {
                        return Yii::$app->user->can('order.notification.delete');
                    }
                ],
            ]
        ]
    ]
]); ?>
