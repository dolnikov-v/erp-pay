<?php

use app\helpers\DataProvider;
use app\widgets\assets\custom\ModalConfirmDeleteAsset;
use app\widgets\ButtonLink;
use app\widgets\custom\ModalConfirmDelete;
use app\widgets\LinkPager;
use app\widgets\Panel;
use yii\helpers\Url;

/** @var $this \yii\web\View */
/** @var $dataProvider \yii\data\ActiveDataProvider */


$this->title = Yii::t('common', 'Список уведомлений');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Уведомления'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ModalConfirmDeleteAsset::register($this);
?>

    <div class="order-notification-type-index">
        <?= Panel::widget([
            'title' => Yii::t('common', 'Таблица с уведомлениями'),
            'actions' => DataProvider::renderSummary($dataProvider),
            'withBody' => false,
            'content' => $this->render('_index-content', [
                'dataProvider' => $dataProvider,
            ]),
            'footer' =>
                ButtonLink::widget([
                    'url' => Url::toRoute('edit'),
                    'label' => Yii::t('common', 'Добавить уведомление'),
                    'style' => ButtonLink::STYLE_SUCCESS,
                    'size' => ButtonLink::SIZE_SMALL,
                ]) . LinkPager::widget([
                    'pagination' => $dataProvider->getPagination(),
                    'counterPosition' => LinkPager::COUNTER_POSITION_RIGHT,
                ]),
        ]) ?>
    </div>

<?= ModalConfirmDelete::widget() ?>
