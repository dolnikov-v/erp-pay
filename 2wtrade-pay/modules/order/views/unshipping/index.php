<?php

use app\widgets\Panel;

/** @var yii\web\View $this */
/** @var app\modules\delivery\models\Delivery $deliveriesList */

$this->title = Yii::t('common', 'Недоставленные заказы');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Заказы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Статусы')];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Загрузка Excel файла'),
    'content' => $this->render('_index-loader', [
        'deliveriesList' => $deliveriesList,
    ]),
    'collapse' => true,
]) ?>

