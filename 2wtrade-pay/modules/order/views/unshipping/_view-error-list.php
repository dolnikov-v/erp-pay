<?php

use app\components\grid\GridView;
use app\widgets\Panel;
use yii\helpers\Url;
use app\widgets\Label;
use yii\helpers\Html;


/** @var yii\web\View $this */
/** @var yii\data\ArrayDataProvider $dataProvider2 */

?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с заказами, в которых найдены ошибки'),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider2,
        'columns' => [
            [
                'label' => Yii::t('common', 'Номер заказа'),
                'attribute' => 'order_id',
                'format' => 'raw',
                'value' => function ($model) {
                    $route = [
                        '/order/index/index',
                        'DateFilter' => [
                            'dateType' => null,
                        ],
                        'NumberFilter' => [
                            'entity' => 'id',
                            'number' => $model['order_id'],
                        ]
                    ];

                    return Html::a($model['order_id'], Url::toRoute($route), ['target' => '_blank']);
                },
            ],
            [
                'label' => Yii::t('common', 'Ошибки'),
                'attribute' => 'error',
                'content' => function ($model) {
                    $str = '';
                    foreach ($model['error'] as $error) {
                        $str .= Label::widget([
                            'label' => $error,
                        ]);
                    }
                    return $str;
                },
            ],
        ],
    ]),
]) ?>
