<?php

use app\modules\delivery\models\DeliveryRequest;
use app\components\grid\GridView;
use app\widgets\Panel;
use app\widgets\Label;
use yii\helpers\Url;
use yii\helpers\Html;


/** @var yii\web\View $this */
/** @var app\modules\order\models\search\OrderStatusSearch $modelSearch */
/** @var yii\data\ArrayDataProvider $dataProvider1 */

?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с корректно обновленными заказами'),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider1,
        'columns' => [
            [
                'label' => Yii::t('common', 'Номер заказа'),
                'attribute' => 'id',
                'format' => 'raw',
                'value' => function ($model) {
                    $route = [
                        '/order/index/index',
                        'DateFilter' => [
                            'dateType' => null,
                        ],
                        'NumberFilter' => [
                            'entity' => 'id',
                            'number' => $model['id'],
                        ]
                    ];

                    return Html::a($model['id'], Url::toRoute($route), ['target' => '_blank']);
                },
            ],
            [
                'label' => Yii::t('common', 'Статус заказа'),
                'attribute' => 'status_name',
                'content' => function ($model) {
                    return Label::widget([
                        'label' => $model['status_name'],
                    ]);
                },
            ],
            [
                'label' => Yii::t('common', 'Причина недоставки'),
                'attribute' => 'reason',
                'content' => function ($model) {
                    return $model['reason'];
                },
            ],
        ],
    ]),

]) ?>
