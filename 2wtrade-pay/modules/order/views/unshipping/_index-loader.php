<?php

use yii\helpers\Url;
use app\widgets\InputText;
use app\widgets\InputGroupFile;
use app\widgets\Select2;
use app\components\widgets\ActiveForm;


/** @var yii\web\View $this */
/** @var app\modules\order\models\search\OrderStatusSearch $modelSearch */
/** @var app\modules\delivery\models\Delivery $deliveriesList */
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute('view'),
    'method' => 'post',
    'enableClientValidation' => false,
    'options' => [
        'enctype' => 'multipart/form-data',
    ]
]); ?>

<div class="row">
    <div class="row margin-top-10">
        <div class="col-xs-4">
            <div class="text-center">
                <?= Yii::t('common', 'Выберите курьерскую службу') ?>
            </div>
            <?= Select2::widget([
                'id' => 'delivery_selector',
                'name' => 'delivery_id',
                'items' => $deliveriesList,
                'autoEllipsis' => true,
                'length' => -1,
            ]) ?>
        </div>

        <div class="col-xs-4">
            <div class="text-center">
                <?= Yii::t('common', 'Выберите Excel-файл для формирования списка заказов') ?>
            </div>
            <?= InputGroupFile::widget([
                'right' => false,
                'input' => InputText::widget([
                    'type' => 'file',
                    'name' => 'OrdersExcelFile',
                ]),
            ]) ?>
        </div>

    </div>
</div>
<div class="row">
    <div class="col-md-12 text-center padding-top-20" style="margin-top: 20px;">
        <?= $form->submit(Yii::t('common', 'Загрузить')) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
