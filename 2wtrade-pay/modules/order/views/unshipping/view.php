<?php
use app\components\grid\GridView;
use app\helpers\DataProvider;
use app\widgets\Panel;
use app\widgets\Nav;
use yii\widgets\LinkPager;


/** @var yii\web\View $this */
/** @var yii\data\ArrayDataProvider $dataProvider1 */
/** @var yii\data\ArrayDataProvider $dataProvider2 */
/** @var app\modules\delivery\models\Delivery $deliveriesList */

$this->title = Yii::t('common', 'Недоставленные заказы');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Заказы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Статусы')];
?>

<?= Panel::widget([
    'nav' => new Nav([
        'tabs' => [
            [
                'label' => Yii::t('common', 'Корректные заказы'),
                'content' => $this->render('_view-correct-list', [
                    'dataProvider1' => $dataProvider1,
                ]),
            ],
            [
                'label' => Yii::t('common', 'Заказы с ошибками'),
                'content' => $this->render('_view-error-list', [
                    'dataProvider2' => $dataProvider2,
                ]),
            ],
        ]
    ]),
]) ?>


