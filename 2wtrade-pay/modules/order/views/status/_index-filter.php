<?php
use app\components\widgets\ActiveForm;
use app\modules\order\models\OrderStatus;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var app\modules\order\models\search\OrderStatusSearch $modelSearch */
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute('index'),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>

<div class="row">
    <div class="col-lg-6">
        <div class="form-group">
            <?= $form->field($modelSearch, 'name') ?>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group">
            <?= $form->field($modelSearch, 'type')->select2List(OrderStatus::getTypesCollection(), [
                'prompt' => '—',
                'length' => -1,
            ]) ?>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group">
            <?= $form->field($modelSearch, 'group')->select2List(OrderStatus::getGroupsCollection(), [
                'prompt' => '—',
                'length' => -1,
            ]) ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')) ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
