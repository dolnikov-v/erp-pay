<?php
use app\components\grid\GridView;
use app\components\grid\IdColumn;
use app\helpers\DataProvider;
use app\widgets\Label;
use app\widgets\Panel;
use yii\widgets\LinkPager;
use app\modules\order\models\OrderStatus;

/** @var yii\web\View $this */
/** @var app\modules\order\models\search\OrderStatusSearch $modelSearch */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = Yii::t('common', 'Список статусов');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Заказы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Статусы')];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'content' => $this->render('_index-filter', [
        'modelSearch' => $modelSearch,
    ]),
    'collapse' => true,
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица со статусами'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => IdColumn::className(),
            ],
            [
                'attribute' => 'name',
                'content' => function ($model) {
                    return Yii::t('common', $model->name);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'type',
                'headerOptions' => ['class' => 'text-center width-200'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    $style = Label::STYLE_SUCCESS;

                    if ($model->type == OrderStatus::TYPE_MIDDLE) {
                        $style = Label::STYLE_WARNING;
                    } elseif ($model->type == OrderStatus::TYPE_FINAL) {
                        $style = Label::STYLE_DANGER;
                    }

                    return empty(OrderStatus::getTypesCollection()[$model->type]) ? '' : Label::widget([
                        'label' => OrderStatus::getTypesCollection()[$model->type],
                        'style' => $style,
                    ]);
                },
            ],
            [
                'attribute' => 'group',
                'headerOptions' => ['class' => 'text-center width-150'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return empty(OrderStatus::getGroupsCollection()[$model->group]) ? '' : Label::widget([
                        'label' => OrderStatus::getGroupsCollection()[$model->group],
                        'style' => Label::STYLE_DEFAULT,
                    ]);
                },
            ],
        ],
    ]),
    'footer' => LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>
