<?php

namespace app\modules\order\abstracts;

/**
 * Interface OrderProductInterface
 * @package app\modules\order\abstracts
 *
 * @property int $product_id
 * @property float $price
 * @property integer $quantity
 */
interface OrderProductInterface
{
    /**
     * @return int
     */
    public function getProductId(): int;

    /**
     * @return float
     */
    public function getPrice(): float;

    /**
     * @return int
     */
    public function getQuantity(): int;
}