<?php

namespace app\modules\order\controllers;

use app\modules\order\models\OrderStatus;
use Yii;
use app\modules\order\models\OrderClarification;
use app\components\web\Controller;
use app\modules\order\models\search\OrderClarificationSearch;
use app\modules\order\models\Order;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class ClarificationController
 * @package app\modules\order\controllers
 */
class ClarificationController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new OrderClarificationSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->get());

        $orderStatuses = [];
        if ($clarifications = $dataProvider->getModels()) {
            /* @var $clarifications $clarifications[] */
            $orderMap = [];
            foreach ($clarifications as $clarification) {
                if ($clarification->order_ids && $orders = explode(',', $clarification->order_ids)) {
                    foreach ($orders as $id) {
                        $orderMap[$id][$clarification->id] = $clarification->id;
                    }
                }
            }

            if ($orderMap && $ids = array_keys($orderMap)) {

                foreach ($clarifications as $clarification) {
                    $orderStatuses[$clarification->id] = [
                        'buyout' => [],
                        'in_process' => [],
                        'not_buyout' => [],
                    ];
                }

                foreach (array_chunk($ids, 1000) as $ids) {
                    $orders = Order::findAll(['id' => $ids]);
                    foreach ($orders as $order) {
                        $status = null;
                        if (in_array($order->status_id, OrderStatus::getBuyoutList())) {
                            $status = 'buyout';
                        } elseif (in_array($order->status_id, OrderStatus::getProcessList())) {
                            $status = 'in_process';
                        } elseif (in_array($order->status_id, OrderStatus::getNotBuyoutList())) {
                            $status = 'not_buyout';
                        }

                        if ($status) {
                            foreach ($orderMap[$order->id] as $clarificationId) {
                                $orderStatuses[$clarificationId][$status][] = $order->id;
                            }
                        }
                    }
                    sleep(0.1);
                }
            }
        }

        return $this->render('index', [
            'modelSearch' => $modelSearch,
            'dataProvider' => $dataProvider,
            'orderStatuses' => $orderStatuses,
        ]);
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionDelete()
    {
        $id = Yii::$app->request->get('id');
        $model = $this->findModel($id);

        if ($model->delete()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Лист на уточнение статустов успешно удален.'), 'success');
        } else {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Не удалось удалить лист на уточнения заказов.'), 'danger');
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param integer $id
     * @return OrderClarification
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        $model = OrderClarification::find()
            ->where(['id' => $id])
            ->byCountryId(Yii::$app->user->country->id)
            ->one();

        if ($model == null) {
            throw new NotFoundHttpException(Yii::t('common', 'Лист на уточнение заказав не найден'));
        }

        return $model;
    }
}