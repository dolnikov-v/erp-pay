<?php
namespace app\modules\order\controllers;

use app\components\filters\AjaxFilter;
use app\components\web\Controller;
use app\modules\order\models\OrderStatus;
use app\modules\order\models\OrderWorkflow;
use app\modules\order\models\OrderWorkflowStatus;
use app\modules\order\models\search\OrderWorkflowSearch;
use app\modules\order\models\search\OrderWorkflowStatusSearch;
use Yii;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\HttpException;

/**
 * Class WorkflowController
 * @package app\modules\order\controllers
 */
class WorkflowController extends Controller
{

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => [
                    'save-workflow-scheme',
                ],
            ]
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new OrderWorkflowSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'modelSearch' => $modelSearch,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param null $id
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionView($id = null)
    {
        $model = $this->getModel($id);

        if ($model->scheme == "[]") {
            $model->scheme = OrderWorkflowStatus::createNewJsonScheme($id);
            $model->save();
        }

        $model->scheme = OrderWorkflowStatus::updateOldJsonScheme($id);
        $model->save();

        $modelSearch = new OrderWorkflowStatusSearch();

        $params['OrderWorkflowStatusSearch']['workflow_id'] = $model->id;

        $dataProvider = $modelSearch->search($params);

        return $this->render('view', [
            'model' => $model,
            'modelSearch' => $modelSearch,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param null $id
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new OrderWorkflow();
        }

        if ($model->load(Yii::$app->request->post())) {
            $isNewRecord = $model->isNewRecord;

            if ($model->save()) {
                Yii::$app->notifier->addNotification($isNewRecord ? Yii::t('common', 'Workflow успешно добавлен.') : Yii::t('common', 'Workflow успешно сохранен.'), 'success');

                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }

        $data['model'] = $model;

        return $this->render('edit', $data);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionActivate($id)
    {
        $model = $this->getModel($id);

        $model->active = 1;

        if ($model->save(true, ['active'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Workflow успешно активирован.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionDeactivate($id)
    {
        $model = $this->getModel($id);

        $model->active = 0;

        if ($model->save(true, ['active'])) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Workflow успешно деактивирован.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param null $id
     * @return string
     * @throws HttpException
     */
    public function actionEditStatus($id = null)
    {
        if ($id) {
            $model = $this->getModelStatus($id);
        } else {
            $model = new OrderWorkflowStatus();
        }

        $modelWorkflow = $this->getModel(Yii::$app->request->get('workflow'));

        if ($model->load(Yii::$app->request->post())) {
            $isNewRecord = $model->isNewRecord;

            $model->workflow_id = $modelWorkflow->id;

            if ($model->save()) {
                Yii::$app->notifier->addNotification($isNewRecord ? Yii::t('common', 'Статус успешно добавлен.') : Yii::t('common', 'Статус успешно сохранен.'), 'success');

                return $this->redirect(Url::toRoute(['view', 'id' => $model->workflow_id]));
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }

        return $this->render('edit-status', [
            'model' => $model,
            'modelWorkflow' => $modelWorkflow,
            'statuses' => OrderStatus::find()->collection(),
        ]);
    }

    /**
     * @param null $id
     * @return string
     * @throws HttpException
     */
    public function actionDeleteStatus($id)
    {
        $model = $this->getModelStatus($id);

        $workflow = $model->workflow_id;

        if ($model->delete()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Статус успешно удален.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(['view', 'id' => $workflow]);
    }

    /**
     * @return array
     * @throws HttpException
     */
    public function actionSaveWorkflowScheme()
    {
        $model = $this->getModel(Yii::$app->request->post('id'));

        $schema = json_decode(Yii::$app->request->post('value'), true);

        foreach ($schema['nodeDataArray'] as $key => $item) {
            unset($schema['nodeDataArray'][$key]['text']);
        }

        $model->scheme = json_encode($schema, JSON_UNESCAPED_SLASHES);

        if (!$model->save()) {
            throw new BadRequestHttpException(Yii::t('common', 'Не удалось сохранить.'));
        }

        return $schema;
    }

    /**
     * @param integer $id
     * @return OrderWorkflow
     * @throws HttpException
     */
    private function getModel($id)
    {
        $model = OrderWorkflow::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Workflow не найден.'));
        }

        return $model;
    }

    /**
     * @param integer $id
     * @return OrderWorkflowStatus
     * @throws HttpException
     */
    private function getModelStatus($id)
    {
        $model = OrderWorkflowStatus::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Статус не найден.'));
        }

        return $model;
    }
}
