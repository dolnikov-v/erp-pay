<?php
namespace app\modules\order\controllers;

use app\components\web\Controller;
use app\components\Notifier;
use app\modules\delivery\components\DeliveryHandler;
use app\modules\delivery\models\Delivery;
use app\modules\order\models\Order;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\models\OrderStatus;
use app\modules\report\extensions\Query;
use Yii;
use yii\data\ArrayDataProvider;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;

/**
 * Class UnshippingController
 * @package app\modules\order\controllers
 */
class UnshippingController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $deliveries = Delivery::find()
            ->where(['=', Delivery::tableName() .'.country_id', Yii::$app->user->country->id])
            ->asArray()
            ->all();
        $deliveriesList = [];
        foreach ($deliveries as $item) {
            $deliveriesList[$item['id']] = $item['name'];
        }

        return $this->render('index', [
            'deliveriesList' => $deliveriesList,
        ]);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionView()
    {
        $table = [];
        $deliveryId = 0;

        if (Yii::$app->request->post('delivery_id')) {
            $deliveryId = intval(Yii::$app->request->post('delivery_id'));
        }
        // Получаем файл
        try {
            $file = UploadedFile::getInstanceByName('OrdersExcelFile');
            $file_type = \PHPExcel_IOFactory::identify($file->tempName);
            $objReader = \PHPExcel_IOFactory::createReader($file_type);
            $objPHPExcel = $objReader->load($file->tempName);
            $table = $objPHPExcel->getActiveSheet()->toArray();
        } catch (\Exception $e) {
            Yii::$app->notifier->addNotification('Отсутствует excel-файл или не верный формат файла',
                Notifier::TYPE_DANGER);
            return $this->redirect('/order/unshipping/index');
        }

        if (!empty($table)) {

            $arr = [];
            foreach ($table as $row) {
                if (!is_numeric($row[0]) && trim($row[0]) != '') {
                    continue;
                }
                $arr['id'][] = $row[0];
                $arr['track'][] = $row[1];
                $arr['reason'][] = $row[2];
            }

            $unshippingPackages = DeliveryRequest::find()
                ->where(['IN', DeliveryRequest::tableName() . '.order_id', $arr['id']])
                ->orWhere(['IN', DeliveryRequest::tableName() . '.tracking', $arr['track']])
                ->distinct()
                ->all();

            // Проставляем отсутствующие индексы в первую колонку таблицы
            foreach ($table as &$row) {
                if (!is_numeric($row[0]) && trim($row[0]) != '') {
                    continue;
                }
                if (!is_numeric($row[0]) && !empty($row[1])) {
                    foreach ($unshippingPackages as $item) {
                        if ($item->tracking == $row[1]) {
                            $row[0] = $item->order_id;
                            break;
                        }
                    }
                }
            }

            // Формируем список id заказов с обнаруженными ошибками
            $ordersWithErrorsList = $this->getOrderErrorsList($unshippingPackages, $deliveryId);
            $errorOrdersIds = [];
            foreach ($ordersWithErrorsList as $item) {
                $errorOrdersIds[] = $item['order_id'];
            }

            // Формируем список id корректных заказов
            $correctOrderIds = [];
            foreach ($unshippingPackages as $item) {
                if (!in_array($item->order_id, $errorOrdersIds)) {
                    $correctOrderIds[] = $item->order_id;
                }
            }
            $correctOrderList = Order::find()
                ->where(['IN', Order::tableName() .'.id', $correctOrderIds])
                ->all();

            // Проставляем причину недоставки из Excel в корректные заказы
            $deliveryRequests = DeliveryRequest::find()
                ->where(['IN', DeliveryRequest::tableName() .'.order_id', $correctOrderIds])
                ->all();

            $tempTable = $table;
            unset($table);

            $hasError = false;
            $transaction = Yii::$app->db->beginTransaction();

            foreach ($deliveryRequests as $dr) {
                foreach ($tempTable as $tempRow) {
                    if (intval($tempRow[0]) == intval($dr->order_id)) {

                        $dr->unshipping_reason = substr(strtolower($tempRow[2]), 0, 30);

                        $dr->saveUnBuyoutReason(strtolower($tempRow[2]));

                        if (!$dr->save()) {
                            $transaction->rollBack();
                            $hasError = true;
                        }
                        break;
                    }
                }
                if ($hasError) {
                    break;
                }
            }

            if (!$hasError) {

                $transaction->commit();
                $query = new Query();
                $correctOrders = $query->select([
                    'id' => Order::tableName() .'.id',
                    'status_name' => OrderStatus::tableName() .'.name',
                    'reason' => DeliveryRequest::tableName() .'.unshipping_reason',
                ])
                    ->from(Order::tableName())
                    ->innerJoin(DeliveryRequest::tableName(), DeliveryRequest::tableName() .'.order_id=' .Order::tableName() .'.id')
                    ->innerJoin(OrderStatus::tableName(), OrderStatus::tableName() .'.id=' .Order::tableName() .'.status_id')
                    ->where(['IN', Order::tableName() .'.id', $correctOrderIds])
                    ->all();

                $dataProvider1 = new ArrayDataProvider([
                    'allModels' => $correctOrders,
                    'pagination' => false,
                ]);

                $dataProvider2 = new ArrayDataProvider([
                    'allModels' => $ordersWithErrorsList,
                    'pagination' => false,
                ]);

                return $this->render('view', [
                    'dataProvider1' => $dataProvider1,
                    'dataProvider2' => $dataProvider2,
                ]);
            }
        }
        else {
            Yii::$app->notifier->addNotification('Не удалось сформировать список заказов из excel-файла', Notifier::TYPE_DANGER);
            return $this->redirect('index');
        }

        Yii::$app->notifier->addNotification('Не удалось сохранить изменения в базу данных', Notifier::TYPE_DANGER);
        return $this->redirect('index');
    }


    /**
     * Делаем проверки полученных недоставленных ордеров, возвращаем массив ошибок
     * @param $unshippingPackages
     * @param $deliveryId
     * @return array $errors
     */
    private function getOrderErrorsList($unshippingPackages, $deliveryId) {

        $errors = [];

        // 1. Правильная ли курьерка
        foreach ($unshippingPackages as $item) {
            if ($item->delivery_id != $deliveryId ) {
                $errors[] = Array('order_id' => $item->order_id, 'error' => Yii::t('common', 'Заказу присвоена курьеская служба c id=' .$item->delivery_id .' вместо id=' .$deliveryId));
            }
        }

        // 2. Правильный ли статус
        $ids = [];
        foreach ($unshippingPackages as $item) {
            $ids[] = $item->order_id;
        }
        $selectedOrders = Order::find()
            ->where(['IN', Order::tableName() .'.id', $ids])
            ->orderBy(['id' => SORT_DESC])
            ->all();

        foreach ($selectedOrders as $order) {

            if ($order->status_id == OrderStatus::STATUS_DELIVERY_DENIAL ||
                $order->status_id == OrderStatus::STATUS_DELIVERY_RETURNED ||
                $order->status_id == OrderStatus::STATUS_DELIVERY_REFUND)
            {
                continue;
            }

            $errors[] = Array('order_id' => $order->id, 'error' => Yii::t('common', 'Не корректный статус заказа (' .$order->status_id .') вместо (' .OrderStatus::STATUS_DELIVERY_DENIAL .'/' .OrderStatus::STATUS_DELIVERY_RETURNED .'/' .OrderStatus::STATUS_DELIVERY_REFUND .')'));
            if (!$order->canChangeStatusTo(OrderStatus::STATUS_DELIVERY_DENIAL) ||
                !$order->canChangeStatusTo(OrderStatus::STATUS_DELIVERY_RETURNED) ||
                !$order->canChangeStatusTo(OrderStatus::STATUS_DELIVERY_REFUND))
            {
                $errors[] = Array('order_id' => $order->id, 'error' => Yii::t('common', 'Cтатус заказа (' .$order->status_id .') не может быть изменен на (' .OrderStatus::STATUS_DELIVERY_DENIAL .'/' .OrderStatus::STATUS_DELIVERY_RETURNED .'/' .OrderStatus::STATUS_DELIVERY_REFUND .')'));
            }
        }

        // Сводим ошибки в единый массив по order_id
        $id = [];
        $result = [];
        foreach ($errors as $key => $error) {
            if (!in_array($error['order_id'], $id)) {
                $id[] = $error['order_id'];
                $message = [];
                foreach ($errors as $item) {
                    if ($item['order_id'] == $error['order_id']) {
                        $message[] = $item['error'];
                    }
                }
                $result[] = Array('order_id' => $error['order_id'], 'error' => $message);
            }
        }

        return $result;
    }
}
