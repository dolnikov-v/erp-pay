<?php

namespace app\modules\order\controllers;

use app\components\filters\AjaxFilter;
use app\components\web\Controller;
use app\models\Currency;
use app\modules\order\components\exporter\Exporter;
use app\modules\order\components\exporter\ExporterFactory;
use app\modules\order\models\search\OrderFinanceFactSearch;
use app\modules\order\widgets\ShowerFinanceFactColumns;
use Yii;
use yii\web\Response;

class FinanceFactController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => [
                    'export',
                ]
            ]
        ];
    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function actionIndex()
    {
        $queryParams = Yii::$app->request->queryParams;
        if (Yii::$app->request->get('query-from-session') && Yii::$app->session->has('order-index-query-params')) {
            $queryParams = Yii::$app->session->get('order-index-query-params');
        }

        $modelSearch = new OrderFinanceFactSearch(['userId' => Yii::$app->user->id]);
        $dataProvider = $modelSearch->search($queryParams);
        $currencies = Currency::find()->customCollection('id', 'char_code');

        $showerColumns = new ShowerFinanceFactColumns();

        $data = [
            'modelSearch' => $modelSearch,
            'dataProvider' => $dataProvider,
            'currencies' => $currencies,
            'showerColumns' => $showerColumns,
        ];

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $columns = Yii::$app->request->post('columns');
            $choices = Yii::$app->request->post('choices');

            $showerColumns->toggleColumn($columns, $choices);

            return [
                'content' => $this->renderPartial('_table', $data),
            ];
        }

        return $this->render('index', $data);
    }

    /**
     * @return array
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionExport()
    {
        $response = [
            'status' => 'fail',
            'message' => '',
        ];

        if ($exporterType = Yii::$app->request->get('export')) {

            $queryParams = Yii::$app->request->queryParams;
            if (Yii::$app->request->get('query-from-session') && Yii::$app->session->has('order-index-query-params')) {
                $queryParams = Yii::$app->session->get('order-index-query-params');
            }

            $modelSearch = new OrderFinanceFactSearch(['userId' => Yii::$app->user->id]);
            $dataProvider = $modelSearch->search($queryParams);

            $showerColumns = new ShowerFinanceFactColumns();

            $exporterConfig = [
                'dataProvider' => $dataProvider,
                'showerColumns' => $showerColumns,
            ];

            if ($dataProvider->getTotalCount() >= 64999 && $exporterType == Exporter::TYPE_EXCEL) {
                $response['message'] = 'Превышено количество строк XLS файла (более 65 000). Используйте формат CSV.';
                return $response;
            }

            $exporter = ExporterFactory::build($exporterType, $exporterConfig);
            return $exporter->orderFile($queryParams);
        }

        return $response;
    }
}