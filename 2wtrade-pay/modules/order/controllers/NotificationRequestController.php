<?php

namespace app\modules\order\controllers;

use app\components\web\Controller;
use app\helpers\Utils;
use app\modules\order\models\OrderNotificationRequest;
use app\modules\order\models\OrderNotificationRequestLog;
use app\modules\order\models\search\OrderNotificationRequestSearch;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;

/**
 * Class NotificationRequestController
 * @package app\modules\order\controllers
 */
class NotificationRequestController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new OrderNotificationRequestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $export = Yii::$app->request->get('export');
        if ($export) {
            return $this->exportExcel($dataProvider, $searchModel);
        }

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * @param $dataProvider ActiveDataProvider
     * @param $searchModel OrderNotificationRequestSearch
     * @return bool
     */
    private function exportExcel($dataProvider, $searchModel)
    {
        $excel = new \PHPExcel();
        $sheetIndex = 0;
        $sheet = $excel->getSheet($sheetIndex);
        $sheet->setTitle(Yii::t('common', 'Таблица с заявками'));

        $row = 1;
        $i = 0;
        $sheet->setCellValueByColumnAndRow($i++, $row, Yii::t('common', '#'));
        $sheet->setCellValueByColumnAndRow($i++, $row, Yii::t('common', 'Номер заказа'));
        $sheet->setCellValueByColumnAndRow($i++, $row, Yii::t('common', 'Статус заказа'));
        $sheet->setCellValueByColumnAndRow($i++, $row, Yii::t('common', 'Доставка'));
        $sheet->setCellValueByColumnAndRow($i++, $row, Yii::t('common', 'E-mail получателя'));
        $sheet->setCellValueByColumnAndRow($i++, $row, Yii::t('common', 'Статус E-mail'));
        $sheet->setCellValueByColumnAndRow($i++, $row, Yii::t('common', 'Телефон получателя'));
        $sheet->setCellValueByColumnAndRow($i++, $row, Yii::t('common', 'Статус SMS'));
        $sheet->setCellValueByColumnAndRow($i++, $row, Yii::t('common', 'Уведомление'));
        $sheet->setCellValueByColumnAndRow($i++, $row, Yii::t('common', 'Дата отправки'));
        $sheet->setCellValueByColumnAndRow($i++, $row, Yii::t('common', 'Дата создания'));
        $row++;

        foreach ($dataProvider->query->all() as $item) {
            /** @var OrderNotificationRequest $item */
            $i = 0;
            $sheet->setCellValueByColumnAndRow($i++, $row, $item->id);
            $sheet->setCellValueByColumnAndRow($i++, $row, $item->order_id);
            $sheet->setCellValueByColumnAndRow($i++, $row, $item->orderStatus->name);
            $sheet->setCellValueByColumnAndRow($i++, $row, $item->delivery_id ? $item->delivery->name : '');
            $sheet->setCellValueByColumnAndRow($i++, $row, $item->email_to);
            $sheet->setCellValueByColumnAndRow($i++, $row, $item->getEmailStatusLabel());
            $sheet->setCellValueByColumnAndRow($i++, $row, $item->phone_to);
            $sheet->setCellValueByColumnAndRow($i++, $row, $item->getSmsStatusLabel());
            $sheet->setCellValueByColumnAndRow($i++, $row, $item->order_notification_id ? $item->orderNotification->name : '');
            $sheet->setCellValueByColumnAndRow($i++, $row, $item->sms_date_sent ? Yii::$app->formatter->asDatetime($item->sms_date_sent) : '');
            $sheet->setCellValueByColumnAndRow($i++, $row, $item->created_at ? Yii::$app->formatter->asDatetime($item->created_at) : '');
            $row++;
        }

        foreach (range('A', 'J') as $columnID) {
            $excel->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        $dir = Yii::getAlias("@runtime") . DIRECTORY_SEPARATOR . "notification-request";
        Utils::prepareDir($dir);

        $filename = $dir . DIRECTORY_SEPARATOR . 'notification-request' . '_' . date('Y-m-d', strtotime($searchModel->dateFrom)) . ' - ' . date('Y-m-d', strtotime($searchModel->dateTo)) . ".xlsx";

        $objWriter = \PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        $objWriter->save($filename);

        Yii::$app->response->sendFile($filename);

        return true;
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionLogs($id)
    {
        $model = $this->findModel($id);

        $logs = $this->findModelLogs($model->id);

        return $this->render('logs', [
            'logs' => $logs
        ]);
    }

    /**
     * @param $id
     * @return OrderNotificationRequest
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        $model = OrderNotificationRequest::findOne($id);

        if (!$model) {
            throw new NotFoundHttpException(Yii::t('common', 'Заявка не найдена.'));
        }

        return $model;
    }

    /**
     * @param $id
     * @return OrderNotificationRequestLog[]
     */
    protected function findModelLogs($id)
    {
        return OrderNotificationRequestLog::find()
            ->joinWith('user')
            ->where(['order_notification_request_id' => $id])
            ->all();
    }
}
