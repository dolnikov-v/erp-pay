<?php
namespace app\modules\order\controllers;

use app\components\Notifier;
use app\components\web\Controller;
use app\models\Product;
use app\modules\order\models\OrderProduct;
use app\modules\order\models\search\MarketingListSearch;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use app\modules\order\models\MarketingList;
use app\modules\order\models\MarketingListOrder;
use Yii;
use yii\base\Exception;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class MarketingListController
 * @package app\modules\order\controllers
 */
class MarketingListController extends Controller
{

    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new MarketingListSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render(
            'index',
            [
                'modelSearch' => $modelSearch,
                'dataProvider' => $dataProvider,
            ]
        );
    }

    /**
     * @param integer $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionDetails($id)
    {
        $list = $this->findModel($id);

        $dataProvider = new ActiveDataProvider();
        $dataProvider->models = $list->oldOrders;

        return $this->render(
            'details',
            [
                'list' => $list,
                'dataProvider' => $dataProvider,
                'statuses' => OrderStatus::find()->collection(),
                'products' => Product::find()->collection(),
            ]
        );
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionDeleteOrder()
    {

        $listId = Yii::$app->request->get('list');
        $orderId = Yii::$app->request->get('order');

        $marketingListOrder = MarketingListOrder::find()->where(["marketing_list_id" => $listId, "old_order_id" => $orderId])->one();
        if (!$marketingListOrder) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Заказ в списке не найден.'));
        } else {
            if (!$marketingListOrder->delete()) {
                Yii::$app->notifier->addNotification(Yii::t('common', 'Не удалось удалить.'));
            } else {
                Yii::$app->notifier->addNotificationsByModel($marketingListOrder);
            }
        }
        return $this->redirect(Url::toRoute(['details', 'id' => $listId]));
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionDeleteList()
    {

        $listId = Yii::$app->request->get('list');
        $list = $this->findModel($listId);

        if (!$list) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Маркетинговый лист не найден.'));
        }
        else {
            if ($list->delete()) {
                Yii::$app->notifier->addNotification(
                    Yii::t('common', 'Лист успешно удален из списка.'),
                    'success'
                );
            } else {
                Yii::$app->notifier->addNotificationsByModel($list);
            }
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param integer $id
     * @return MarketingList
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {

        $condition = [MarketingList::tableName() . '.id' => $id];

        $model = MarketingList::find()
            ->joinWith(
                [
                    'country'
                ]
            )
            ->where($condition)
            ->byCountryId(Yii::$app->user->country->id)
            ->one();

        if ($model == null) {
            throw new NotFoundHttpException(Yii::t('common', 'Лист не найден.'));
        }

        return $model;
    }

    /**
     * @param integer $id
     * @return Order
     * @throws NotFoundHttpException
     */
    protected function findModelOrder($id)
    {
        $model = Order::find()
            ->joinWith('country')
            ->where([Order::tableName() . '.id' => $id])
            ->byCountryId(Yii::$app->user->country->id)
            ->one();

        if ($model == null) {
            throw new NotFoundHttpException(Yii::t('common', 'Заказ не найден.'));
        }

        return $model;
    }


    /**
     * @param $id
     * @return Response
     */
    public function actionSendOrdersToDelivery($id)
    {
        $list = $this->findModel($id);
        if (!is_null($list)) {

            if ($list->sent_at) {
                Yii::$app->notifier->addNotification(Yii::t('common', 'Лист уже отправлен'));
            }
            else {
                $transaction = Yii::$app->db->beginTransaction();
                try {

                    foreach ($list->oldOrders as $order) {

                        $orderCopy = new Order;
                        $orderCopy->attributes = $order->attributes;
                        $orderCopy->foreign_id = null;
                        $orderCopy->income = null;
                        switch ($order->source_form) {
                            case $order::TYPE_FORM_SHORT:
                                $orderCopy->status_id = OrderStatus::STATUS_SOURCE_SHORT_FORM;
                                break;
                            case $order::TYPE_FORM_LONG:
                                $orderCopy->status_id = OrderStatus::STATUS_SOURCE_LONG_FORM;
                                break;
                            default:
                                $orderCopy->status_id = OrderStatus::STATUS_SOURCE_SHORT_FORM;
                                break;
                        }

                        $orderCopy->source_id = null;
                        $orderCopy->call_center_type = Order::TYPE_CC_ORDER_REPEAT;
                        if (!$orderCopy->save()) {
                            throw new Exception(Yii::t('common', 'Не удалось повторно отправить заказ #{order_id}.',
                                ['order_id' => $order->id]));
                        }

                        $marketingListOrder = MarketingListOrder::find()->where(['marketing_list_id' => $list->id, 'old_order_id' => $order->id])->one();
                        if (!$marketingListOrder) {
                            throw new Exception(Yii::t('common', 'Не удалось повторно отправить заказ #{order_id}.',
                                ['order_id' => $order->id]));
                        }
                        else {
                            $marketingListOrder->new_order_id = $orderCopy->id;
                            if (!$marketingListOrder->save()) {
                                throw new Exception(Yii::t('common', 'Не удалось повторно отправить заказ #{order_id}.',
                                    ['order_id' => $order->id]));
                            }
                        }

                        foreach ($order->getOrderProducts()->all() as $orderProduct) {
                            $orderProductCopy = new OrderProduct();
                            $orderProductCopy->setAttributes($orderProduct->attributes);
                            $orderProductCopy->order_id = $orderCopy->id;
                            if (!$orderProductCopy->save()) {
                                throw new Exception(Yii::t('common', 'Не удалось повторно отправить заказ #{order_id}.',
                                    ['order_id' => $order->id]));
                            }
                        }
                    }

                    $list->sent_at = time();
                    if (!$list->save()) {
                        throw new Exception(Yii::t('common', 'Не удалось повторно отправить заказы'));
                    }

                    $transaction->commit();
                    Yii::$app->notifier->addNotification(Yii::t('common',
                        "Заказы успешно поставлены в очередь на отправку в КС."), Notifier::TYPE_SUCCESS);
                } catch (Exception $e) {
                    $transaction->rollBack();
                    Yii::$app->notifier->addNotification($e->getMessage(), Notifier::TYPE_DANGER);
                }
            }
        } else {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Лист не найден.'));
        }

        return $this->redirect(Url::toRoute('index'));
    }

}
