<?php
namespace app\modules\order\controllers;

use app\components\web\Controller;
use app\modules\order\models\OrderStatus;
use app\modules\order\models\search\OrderStatusSearch;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * Class StatusController
 * @package app\modules\order\controllers
 */
class StatusController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new OrderStatusSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'modelSearch' => $modelSearch,
            'dataProvider' => $dataProvider,
        ]);
    }
}
