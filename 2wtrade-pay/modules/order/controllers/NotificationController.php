<?php

namespace app\modules\order\controllers;

use app\components\web\Controller;
use app\models\logs\TableLog;
use app\modules\delivery\models\Delivery;
use app\modules\order\models\IssueCollector;
use app\modules\order\models\OrderStatus;
use app\modules\order\models\search\OrderNotificationSearch;
use app\modules\order\models\OrderNotification;
use Yii;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

/**
 * Class NotificationController
 * @package app\modules\order\controllers
 */
class NotificationController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new OrderNotificationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param null|integer $id
     * @return string|\yii\web\Response
     */
    public function actionEdit($id = null)
    {
        if (is_null($id)) {
            $model = new OrderNotification();
        } else {
            $model = $this->getModel($id);
        }

        $post = Yii::$app->request->post();
        if ($model->load($post)) {
            $post['issue_collector_id']?$model->issue_collector_id = $post['issue_collector_id']:$model->issue_collector_id = null;
            if ($model->isNewRecord) {
                $model->country_id = Yii::$app->user->country->id;
            }

            $model->order_statuses = implode(',', $model->statuses);
            $model->deliveries = implode(',', $model->deliveryList);

            foreach (['text', 'sms_text', 'email_text'] as $attr) {
                $model->$attr = strtr($model->$attr, [
                    PHP_EOL => '',
                    '<br />' => PHP_EOL,
                    '&nbsp;' => ' ',
                ]);
                $model->$attr = strip_tags($model->$attr);
                $model->$attr = trim($model->$attr);
            }

            $isNewRecord = $model->isNewRecord;
            if ($model->save()) {
                Yii::$app->notifier->addNotification($isNewRecord ? Yii::t('common',
                    'Уведомление успешно добавлено.') : Yii::t('common', 'Уведомление успешно сохранено.'), 'success');

                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }

        $statusList = OrderStatus::find()->orderBy(['name' => SORT_ASC])->collection();
        $deliveryList = Delivery::find()->orderBy(['name' => SORT_ASC])->byCountryId(Yii::$app->user->country->id)->collection();

        return $this->render('edit', [
            'model' => $model,
            'statusList' => $statusList,
            'deliveryList' => $deliveryList,
        ]);
    }

    /**
     * @param integer $id
     * @return \yii\web\Response
     */
    public function actionActivate($id)
    {
        $model = $this->getModel($id);

        if ($model->active == 0) {
            $model->active = 1;
            if (!$model->save()) {
                Yii::$app->notifier->addNotificationsByModel($model);
            } else {
                Yii::$app->notifier->addNotification(Yii::t('common', 'Уведомление активировано.'), 'success');
            }
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param integer $id
     * @return \yii\web\Response
     */
    public function actionDeactivate($id)
    {
        $model = $this->getModel($id);

        if ($model->active == 1) {
            $model->active = 0;
            if (!$model->save()) {
                Yii::$app->notifier->addNotificationsByModel($model);
            } else {
                Yii::$app->notifier->addNotification(Yii::t('common', 'Уведомление отключено.'), 'success');
            }
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $model = $this->getModel($id);

        if ($model->delete()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Уведомление успешно удалено.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute(['index']));
    }

    /**
     * @param int $id
     * @return string
     */
    public function actionLog(int $id)
    {
        $changeLog = TableLog::find()->byTable(OrderNotification::clearTableName())->byID($id)->allSorted();

        return $this->render('log', [
            'changeLog' => $changeLog,
        ]);
    }

    /**
     * @param integer $id
     * @return OrderNotification
     * @throws NotFoundHttpException
     */
    private function getModel($id)
    {
        $model = OrderNotification::findOne($id);

        if (!$model) {
            throw new NotFoundHttpException(Yii::t('common', 'Уведомление не найдено.'));
        }

        return $model;
    }
}
