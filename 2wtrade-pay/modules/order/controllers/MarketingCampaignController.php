<?php
namespace app\modules\order\controllers;

use app\components\web\Controller;
use app\modules\order\models\MarketingCampaign;
use app\modules\order\models\MarketingCampaignCoupon;
use app\modules\order\models\search\MarketingCampaignSearch;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class MarketingCampaignController
 * @package app\modules\order\controllers
 */
class MarketingCampaignController extends Controller
{

    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new MarketingCampaignSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render(
            'index',
            [
                'modelSearch' => $modelSearch,
                'dataProvider' => $dataProvider,
            ]
        );
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->findModel($id);
        } else {
            $model = new MarketingCampaign();
            $model->loadDefaultValues();
        }

        if ($model->load(Yii::$app->request->post())) {
            $isNewRecord = $model->isNewRecord;

            if ($isNewRecord) {
                $model->country_id = Yii::$app->user->country->id;
                $model->user_id = Yii::$app->user->id;
            }

            if ($model->limit_type == MarketingCampaign::TYPE_LIMIT_AT) {
                $model->limit_use_at = Yii::$app->formatter->asTimestamp($model->limit_use_at);
                if (!$model->limit_use_at) $model->limit_use_at = null;
            } else {
                $model->limit_use_at = null;
            }

            if ($model->limit_type != MarketingCampaign::TYPE_LIMIT_DAYS) {
                $model->limit_use_days = null;
            }

            if ($model->save()) {

                //генерация / догенерация купонов
                $request = Yii::$app->request->post('MarketingCampaign');
                if (!empty($request['newcoupons']) && (int)$request['newcoupons'] > 0) {
                    for ($i = 0; $i < (int)$request['newcoupons']; $i++) {
                        $marketingCampaignCoupon = new MarketingCampaignCoupon();
                        $marketingCampaignCoupon->coupon = $marketingCampaignCoupon->generateUniqueRandomString("coupon");
                        $marketingCampaignCoupon->marketing_campaign_id = $model->id;
                        if (!$marketingCampaignCoupon->save()) {
                            Yii::$app->notifier->addNotificationsByModel($marketingCampaignCoupon);
                        }
                    }
                }

                Yii::$app->notifier->addNotification($isNewRecord ?
                    Yii::t('common', 'Маркетинговая акция успешно добавлена.') :
                    Yii::t('common', 'Маркетинговая акция успешно отредактирована.'), 'success');

                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }

        $data['model'] = $model;

        return $this->render('edit', $data);
    }

    /**
     * @param integer $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionDetails($id)
    {
        $campaign = $this->findModel($id);

        $dataProvider = new ActiveDataProvider();
        $dataProvider->query = MarketingCampaignCoupon::find()->byCampaignId($id);

        $dataProvider->pagination = [
            'pageSize' => 20
        ];

        return $this->render(
            'details',
            [
                'list' => $campaign,
                'dataProvider' => $dataProvider,
            ]
        );
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionDeleteCoupon()
    {

        $campaignId = Yii::$app->request->get('campaign');
        $couponId = Yii::$app->request->get('coupon');

        $MarketingCampaignCoupon = MarketingCampaignCoupon::find()->where(["marketing_campaign_id" => $campaignId, "id" => $couponId])->one();
        if (!$MarketingCampaignCoupon) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Купон не найден.'));
        } else {
            if (!$MarketingCampaignCoupon->delete()) {
                Yii::$app->notifier->addNotification(Yii::t('common', 'Не удалось удалить.'));
            } else {
                Yii::$app->notifier->addNotificationsByModel($MarketingCampaignCoupon);
            }
        }
        return $this->redirect(Url::toRoute(['details', 'id' => $campaignId]));
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionDeleteCampaign()
    {
        $campaignId = Yii::$app->request->get('campaign');
        $campaign = $this->findModel($campaignId);

        if (!$campaign) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Маркетинговая акция не найдена.'));
        } else {
            if ($campaign->delete()) {
                Yii::$app->notifier->addNotification(
                    Yii::t('common', 'Акция успешно удалена из списка.'),
                    'success'
                );
            } else {
                Yii::$app->notifier->addNotificationsByModel($campaign);
            }
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param integer $id
     * @return MarketingCampaign
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {

        $condition = [MarketingCampaign::tableName() . '.id' => $id];

        $model = MarketingCampaign::find()
            ->joinWith(
                [
                    'country'
                ]
            )
            ->where($condition)
            ->byCountryId(Yii::$app->user->country->id)
            ->one();

        if ($model == null) {
            throw new NotFoundHttpException(Yii::t('common', 'Акция не найдена.'));
        }

        return $model;
    }

    /**
     * @param $id
     * @return Response
     */
    public function actionPrint($id)
    {
        $campaign = $this->findModel($id);
        if (!is_null($campaign)) {
            foreach ($campaign->getCoupons() as $coupon) {
                echo $coupon->coupon."<br>";
                $coupon->printed_at = time();
                if (!$coupon->save()) {
                    Yii::$app->notifier->addNotificationsByModel($coupon);
                }
            }
            die();
        } else {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Акция не найдена.'));
        }

        return $this->redirect(Url::toRoute('index'));
    }

}
