<?php

namespace app\modules\order\controllers;

use app\components\filters\AjaxFilter;
use app\components\Notifier;
use app\components\web\Controller;
use app\models\Country;
use app\models\Landing;
use app\models\Product;
use app\models\Source;
use app\modules\api\components\filters\auth\HttpBearerAuth;
use app\modules\callcenter\components\api\Lead;
use app\modules\callcenter\jobs\SendCallCenterRequestJob;
use app\modules\callcenter\models\CallCenter;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\delivery\components\api\TransmitterAdaptee;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryContacts;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\media\components\Image;
use app\modules\order\components\exporter\Exporter;
use app\modules\order\components\exporter\ExporterFactory;
use app\modules\order\components\InvoiceBuilderFactory;
use app\modules\order\models\MarketingList;
use app\modules\order\models\MarketingListOrder;
use app\modules\order\models\Order;
use app\modules\callcenter\models\CallCenterCheckRequest;
use app\modules\order\models\OrderClarification;
use app\modules\order\models\OrderFinancePretension;
use app\modules\order\models\OrderLog;
use app\modules\order\models\OrderLogisticList;
use app\modules\order\models\OrderLogisticListInvoice;
use app\modules\order\models\OrderLogProduct;
use app\modules\order\models\OrderLogStatus;
use app\modules\order\models\OrderProduct;
use app\modules\order\models\OrderStatus;
use app\modules\order\models\search\OrderSearch;
use app\modules\order\models\SmsPollQuestions;
use app\modules\order\widgets\ChangerDelivery;
use app\modules\order\widgets\ChangerStatus;
use app\modules\order\widgets\ResendSeparateOrder;
use app\modules\order\widgets\ResendSeparateOrderIntoDelivery;
use app\modules\order\widgets\ShowerColumns;
use app\modules\report\controllers\SmsPollHistoryController;
use app\widgets\LinkPager;
use app\widgets\Panel;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * Class IndexController
 * @package app\modules\order\controllers
 */
class IndexController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => [
                    'export',
                    'change-status',
                    'change-delivery',
                    'resend-separate-order',
                    'create-call-center-request',
                    'resend-in-call-center',
                    'resend-in-call-center-new-queue',
                    'resend-in-call-center-still-waiting',
                    'resend-in-call-center-return-no-prod',
                    'send-in-delivery',
                    'send-check-order',
                    'send-check-address',
                    'send-check-undelivery-order',
                    'resend-in-delivery',
                    'retry-get-status-from-call-center',
                    'send-sms-poll',
                    'send-analysis-trash',
                    'resend-separate-order-into-delivery',
                    'get-delivery-email',
                    'clarification-in-delivery',
                    'second-delivery-attempt',
                    'change-delivery-request-status',
                    'change-call-center-request-status',
                    'change-status-by-limited-user',
                    'transfer-to-distributor',
                    'send-post-sell',
                    'send-information',
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $alert = null;
        $alertStyle = null;

        $queryParams = Yii::$app->request->queryParams;
        if (Yii::$app->request->get('query-from-session') && Yii::$app->session->has('order-index-query-params')) {
            $queryParams = Yii::$app->session->get('order-index-query-params');
        }

        if (Yii::$app->session->has('orderlist-is-loaded-from-excel')) {
            $queryParams = Yii::$app->session->get('orderlist-is-loaded-from-excel');
            $alert = Yii::t('common',
                'Внимание! Выводятся заказы только из последнего загруженного Excel-файла. Чтобы отключить этот режим нажмите кнопку "Отключить режим из Excel" в фильтре');
            $alertStyle = Panel::ALERT_WARNING;
        }

        $modelSearch = new OrderSearch(['userId' => Yii::$app->user->id]);
        $dataProvider = $modelSearch->search($queryParams);

        LinkPager::setPageSize($dataProvider->pagination);

        $showerColumns = new ShowerColumns();

        $products = Product::find()->collection();
        $deliveries = Delivery::find()->collection();
        $callCenters = CallCenter::find()->collection();
        $orderStatuses = OrderStatus::find()->collection();

        $distributorCountry = Country::find()
            ->joinWith('curatorUser')
            ->where([
                'active' => Country::ACTIVE,
                'is_distributor' => Country::IS_DISTRIBUTOR,
            ])
            ->andFilterWhere(['char_code' => yii::$app->user->country->char_code])
            ->orderBy(['name' => SORT_ASC])
            ->all();

        $data = [
            'modelSearch' => $modelSearch,
            'dataProvider' => $dataProvider,
            'showerColumns' => $showerColumns,
            'statuses' => $orderStatuses,
            'products' => $products,
            'deliveries' => $deliveries,
            'callCenters' => $callCenters,
            'check' => Order::getCheckCollection(),
            'alert' => $alert,
            'alertStyle' => $alertStyle,
            'distributorCountry' => $distributorCountry
        ];

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $columns = Yii::$app->request->post('columns');
            $choices = Yii::$app->request->post('choices');

            $showerColumns->toggleColumn($columns, $choices);

            if (Yii::$app->user->can('order.index.changestatusbylimiteduser') && !Yii::$app->user->isSuperadmin) {
                return [
                    'content' => $this->renderPartial('_index-limited-content', $data),
                ];
            } else {
                return [
                    'content' => $this->renderPartial('_index-content', $data),
                ];
            }

        }

        if (Yii::$app->user->can('order.index.changestatusbylimiteduser') && !Yii::$app->user->isSuperadmin) {
            return $this->render('index_limited', $data);
        }

        if (Yii::$app->user->isDeliveryManager) {
            return $this->render('index_limited', $data);
        } else {
            return $this->render('index', $data);
        }
    }


    /**
     * @return array
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionExport()
    {
        $response = [
            'status' => 'fail',
            'message' => '',
        ];

        if ($exporterType = Yii::$app->request->get('export')) {

            $queryParams = Yii::$app->request->queryParams;
            if (Yii::$app->request->get('query-from-session') && Yii::$app->session->has('order-index-query-params')) {
                $queryParams = Yii::$app->session->get('order-index-query-params');
            }

            if (Yii::$app->session->has('orderlist-is-loaded-from-excel')) {
                $queryParams = Yii::$app->session->get('orderlist-is-loaded-from-excel');
            }

            $modelSearch = new OrderSearch(['userId' => Yii::$app->user->id]);
            $dataProvider = $modelSearch->search($queryParams);

            $showerColumns = new ShowerColumns();

            $products = Product::find()->collection();
            $deliveries = Delivery::find()->collection();
            $callCenters = CallCenter::find()->collection();
            $orderStatuses = OrderStatus::find()->collection();

            $exporterConfig = [
                'dataProvider' => $dataProvider,
                'showerColumns' => $showerColumns,
                'products' => $products,
                'deliveries' => $deliveries,
                'callCenters' => $callCenters,
                'orderStatus' => $orderStatuses,
            ];

            if ($dataProvider->getTotalCount() >= 64999 && $exporterType == Exporter::TYPE_EXCEL) {
                $response['message'] = 'Превышено количество строк XLS файла (более 65 000). Используйте формат CSV.';
                return $response;
            }

            $exporter = ExporterFactory::build($exporterType, $exporterConfig);
            return $exporter->orderFile($queryParams);
        }

        return $response;
    }

    /**
     * @param null $id
     * @return string|Response
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionEdit($id = null)
    {
        $model = is_null($id) ? new Order() : $this->findModel($id);
        $modelsOrderProduct = is_null($id) ? [] : $this->findProducts($id);
        $modelCallCenterRequest = is_null($id) ? [] : $this->findCallCenter($id);
        $modelDeliveryRequest = is_null($id) ? [] : $this->findDeliveryRequest($id);

        if (!empty($model->created_at) && $model->created_at < (time() - 3600 * 24 * 7) && !Yii::$app->user->can("old_orders_access")) {
            throw new ForbiddenHttpException (Yii::t('common',
                "У вас недостаточно прав для просмотра таких старых заказов"));
        }

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());
            $isNewRecord = $model->isNewRecord;

            $tmpToken = Yii::$app->request->post('tmp_token');
            if (!ctype_alnum($tmpToken)) {
                $tmpToken = false;
            }

            if ($isNewRecord) {
                if (!$tmpToken || Yii::$app->session->get('new-order-tmp-token.' . $tmpToken)) {
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }

            if ($modelCallCenterRequest) {
                $modelCallCenterRequest->load(Yii::$app->request->post());
                $modelCallCenterRequest->save(true, ['call_center_id', 'status', 'updated_at', 'comment']);
            }

            if ($modelDeliveryRequest) {
                $modelDeliveryRequest->load(Yii::$app->request->post());
                $modelDeliveryRequest->save(true, ['delivery_id', 'updated_at']);
            }

            if ($isNewRecord) {
                $model->country_id = Yii::$app->user->country->id;
            } else {

                if ($model->delivery_time_from) {
                    $model->delivery_time_from = Yii::$app->formatter->asTimestampLocal($model->delivery_time_from);
                }
                if ($model->delivery_time_from != $model->oldAttributes['delivery_time_from']) {
                    $model->delivery_time_to = $model->delivery_time_from;
                }
            }

            $transaction = Yii::$app->db->beginTransaction();

            $model->route = Yii::$app->controller->route;

            if ($model->save()) {
                if ($model->saveProductsFromPost(Yii::$app->request->post('OrderProduct'), $modelsOrderProduct)) {
                    $transaction->commit();
                    Yii::$app->notifier->addNotification(
                        $isNewRecord ? Yii::t(
                            'common',
                            'Заказ успешно добавлен.'
                        ) : Yii::t('common', 'Заказ успешно изменен.'),
                        'success'
                    );

                    if ($isNewRecord) {
                        Yii::$app->session->set('new-order-tmp-token.' . $tmpToken, true);
                    }

                    return $this->redirect(['view', 'id' => $model->id]);
                } else {
                    Yii::$app->notifier->addNotificationsByModel($model);
                }
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }

            $model->isNewRecord = $isNewRecord;

            $transaction->rollBack();
        }

        $dataArray = [
            'modelsOrderProduct' => $modelsOrderProduct,
            'modelCallCenterRequest' => $modelCallCenterRequest,
            'modelDeliveryRequest' => $modelDeliveryRequest,
        ];

        $groupData = $this->getGroupData($model);

        $mergeData = array_merge($dataArray, $groupData);

        return $this->render('edit', $mergeData);
    }

    /**
     * @param null $id
     * @throws NotFoundHttpException
     */
    public function actionDownloadLabel($id)
    {
        $model = $this->findModel($id);

        if ($model->deliveryRequest && $model->deliveryRequest->delivery && $model->deliveryRequest->delivery->hasApiGenerateLabelByOrder()) {

            $labelFilePath = (new TransmitterAdaptee($model->deliveryRequest->delivery))->generateLabelByOrder($model);
            if ($labelFilePath !== false) {
                try {
                    (new Image())->load($labelFilePath)->output();
                } catch (\Exception $e) {
                    Yii::$app->response->sendFile($labelFilePath);
                }
            }
            else {
                throw new NotFoundHttpException(Yii::t('common', 'Этикетка не получена от {apiClass}', ['apiClass' => $model->deliveryRequest->delivery->apiClass->name]));
            }
        }
        else {
            throw new NotFoundHttpException(Yii::t('common', 'Служба доставки {delivery} не поддерживает создание этикеток', ['delivery' => $model->deliveryRequest->delivery->name]));
        }
    }

    /**
     * @param $id
     * @return Order
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        $query = Order::find()
            ->joinWith([
                'callCenterRequest',
                'deliveryRequest',
            ])
            ->where([
                Order::tableName() . '.id' => $id,
                'country_id' => Yii::$app->user->getCountry()->id
            ]);

        //если пользователь службы доставки (роль implant), то только привязанные ему доставки
        if (Yii::$app->user->getIsImplant()) {
            $delivery_ids = Yii::$app->user->getDeliveriesIds();
            $query->andFilterWhere([
                'or',
                [
                    'and',
                    DeliveryRequest::tableName() . '.delivery_id is not null',
                    [DeliveryRequest::tableName() . ".delivery_id" => $delivery_ids]
                ],
                [
                    'and',
                    DeliveryRequest::tableName() . '.delivery_id is null',
                    [Order::tableName() . ".pending_delivery_id" => $delivery_ids]
                ]
            ]);
        }
        $model = $query->one();

        if (!$model) {
            throw new NotFoundHttpException(Yii::t('common', 'Заказ не найден.'));
        }

        return $model;
    }

    /**
     * @param integer $id
     * @return OrderProduct[]
     */
    protected function findProducts($id)
    {
        return OrderProduct::find()->byOrderId($id)->all();
    }

    /**
     * @param $id
     * @return CallCenterRequest
     */
    protected function findCallCenter($id)
    {
        return CallCenterRequest::findOne(['order_id' => $id]);
    }

    /**
     * @param $id
     * @return DeliveryRequest
     */
    protected function findDeliveryRequest($id)
    {
        return DeliveryRequest::findOne(['order_id' => $id]);
    }

    /**
     * @param $id
     * @return Delivery
     */
    protected function findDelivery($id)
    {
        return Delivery::findOne(['id' => $id]);
    }

    /**
     * @param $id
     * @return SmsPollQuestions
     */
    protected function findSmsPollQuestions($id)
    {
        return SmsPollQuestions::findOne(['id' => $id]);
    }

    /**
     * @param Order $model
     * @return array
     */
    private function getGroupData($model)
    {
        $orderStatusList = [];

        $statuses = $model->getNextStatuses($model->isNewRecord ? null : $model->status_id, true);

        if ($statuses) {
            $orderStatusList = OrderStatus::find()
                ->where(['in', 'id', $statuses])
                ->collection();
        }

        $callCenters = CallCenter::find()
            ->byCountryId($model->country_id)
            ->collection();

        $deliveryServices = Delivery::find()
            ->byCountryId($model->country_id)
            ->collection();

        return [
            'model' => $model,
            'productList' => Product::find()->collection(),
            'orderStatusList' => $orderStatusList,
            'callCenters' => $callCenters,
            'deliveryServices' => $deliveryServices,
        ];
    }

    /**
     * @param $id
     * @return string
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);


        if (!empty($model->created_at) && $model->created_at < (time() - 3600 * 24 * 7) && !Yii::$app->user->can(
                "old_orders_access"
            )
        ) {
            throw new ForbiddenHttpException (Yii::t(
                'common',
                "У вас недостаточно прав для просмотра таких старых заказов"
            ));
        }

        $modelsOrderProduct = $this->findProducts($id);
        $modelCallCenterRequest = $this->findCallCenter($id);
        $modelDeliveryRequest = $this->findDeliveryRequest($id);

        $logsGroup = OrderLog::find()
            ->joinWith('user')
            ->where(['order_id' => $model->id])
            ->orderBy(['id' => SORT_DESC])
            ->all();


        $breakpointsQuery = OrderLogStatus::find()
            ->where(['order_id' => $model->id])
            ->with(['newStatus', 'oldStatus'])
            ->orderBy(['id' => SORT_DESC]);

        $breakpoints = new ActiveDataProvider(['query' => $breakpointsQuery]);

        $logsProductQuery = OrderLogProduct::find()
            ->joinWith('product')
            ->where(['order_id' => $model->id])
            ->orderBy(['id' => SORT_DESC]);

        $logsProduct = new ActiveDataProvider(['query' => $logsProductQuery]);
        //Last responses from call center
        if ($modelCallCenterRequest) {
            $callCenterResponse['cc_send_response'] = $modelCallCenterRequest->lastSendResponse ? $modelCallCenterRequest->lastSendResponse->response : null;
            $callCenterResponse['cc_send_request'] = $modelCallCenterRequest->lastSendResponse ? $modelCallCenterRequest->lastSendResponse->request : null;
            $callCenterResponse['cc_send_response_at'] = $modelCallCenterRequest->cc_send_response_at;
            $callCenterResponse['cc_update_response'] = $modelCallCenterRequest->lastUpdateResponse ? $modelCallCenterRequest->lastUpdateResponse->response : null;
            $callCenterResponse['cc_update_response_at'] = $modelCallCenterRequest->cc_update_response_at;
        } else {
            $callCenterResponse = false;
        }
        //Last responses from courier service
        if ($modelDeliveryRequest) {
            $deliveryResponse['cs_send_response'] = $modelDeliveryRequest->lastSendResponse ? $modelDeliveryRequest->lastSendResponse->response : null;
            $deliveryResponse['cs_send_response_at'] = $modelDeliveryRequest->cs_send_response_at;
            $deliveryResponse['last_update_info'] = $modelDeliveryRequest->lastUpdateResponse ? $modelDeliveryRequest->lastUpdateResponse->response : null;
            $deliveryResponse['last_update_info_at'] = $modelDeliveryRequest->last_update_info_at;
        } else {
            $deliveryResponse = false;
        }

        if ($modelApiLog = $model->apiLog) {
            $adcomboResponse['input_data'] = $modelApiLog->input_data;
            $adcomboResponse['output_data'] = $modelApiLog->output_data;
            $adcomboResponse['updated_at'] = $modelApiLog->created_at;
        } else {
            $adcomboResponse = false;
        }


        $dataArray = [
            'modelsOrderProduct' => $modelsOrderProduct,
            'modelCallCenterRequest' => $modelCallCenterRequest,
            'modelDeliveryRequest' => $modelDeliveryRequest,
            'logsGroup' => $logsGroup,
            'breakpoints' => $breakpoints,
            'logsProduct' => $logsProduct,
            'adcomboResponse' => $adcomboResponse,
            'callCenterResponse' => $callCenterResponse,
            'deliveryResponse' => $deliveryResponse,
        ];

        $groupData = $this->getGroupData($model);

        $mergeData = array_merge($dataArray, $groupData);

        return $this->render('view', $mergeData);
    }

    /**
     * @return array
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionChangeStatus()
    {
        $response = [
            'status' => 'fail',
            'message' => '',
        ];

        $id = Yii::$app->request->post('id');
        $status = Yii::$app->request->post('status');
        $comment = Yii::$app->request->post('comment');
        $ignore = !empty(Yii::$app->request->post('ignore'));

        $model = $this->findModel($id);

        if (!empty($model->created_at) && $model->created_at < (time() - 3600 * 24 * 7) && !Yii::$app->user->can("old_orders_access") && !$ignore) {
            throw new ForbiddenHttpException (Yii::t('common',
                "У вас недостаточно прав для изменения таких старых заказов"));
        }

        if ($ignore && empty($comment)) {
            $response['message'] = Yii::t('common', 'Необходимо указать причину изменения.');
            return $response;
        }

        $allowStatuses = ChangerStatus::getAllowStatuses();

        if ((array_key_exists($status, $allowStatuses) && $model->canChangeStatusTo($status)) || $ignore) {
            $model->status_id = $status;

            $model->route = Yii::$app->controller->route;
            $model->commentForLog = $comment;

            $transaction = Yii::$app->db->beginTransaction();
            try {
                if ($model->deliveryRequest && OrderStatus::isFinalDeliveryStatus($status) && !in_array($model->deliveryRequest->status,
                        [DeliveryRequest::STATUS_ERROR, DeliveryRequest::STATUS_ERROR_AFTER_DONE]) && !$ignore
                ) {
                    $model->deliveryRequest->status = DeliveryRequest::STATUS_DONE;
                    $model->deliveryRequest->route = Yii::$app->controller->route;
                    if (!$model->deliveryRequest->save(true, ['status'])) {
                        throw new \Exception(array_shift($model->deliveryRequest->getFirstErrors()));
                    }
                }

                if ($model->save(!$ignore, ['status_id'])) {
                    $response['status'] = 'success';
                } else {
                    throw new \Exception(array_shift($model->getFirstErrors()));
                }
                $transaction->commit();
            } catch (\Throwable $e) {
                $transaction->rollBack();
                $response['message'] = $e->getMessage();
            }
        } else {
            $response['message'] = Yii::t('common', 'Неразрешенное значение статуса.');
        }

        return $response;
    }

    /**
     * @return array
     * @throws ForbiddenHttpException
     * @throws \Exception
     */
    public function actionChangeDelivery()
    {
        $response = [
            'status' => 'fail',
            'message' => '',
        ];

        $id = Yii::$app->request->post('id');
        $delivery_id = Yii::$app->request->post('delivery');
        $delivery = $this->findDelivery($delivery_id);
        $comment = Yii::$app->request->post('comment');
        $ignore = !empty(Yii::$app->request->post('ignore'));

        $model = $this->findModel($id);

        if (!empty($model->created_at) && $model->created_at < (time() - 3600 * 24 * 7) && !Yii::$app->user->can("old_orders_access") && !$ignore) {
            throw new ForbiddenHttpException (Yii::t('common',
                "У вас недостаточно прав для просмотра таких старых заказов"));

        }

        if ($ignore && empty($comment)) {
            $response['message'] = Yii::t('common', 'Необходимо указать причину изменения.');
            return $response;
        }


        $hasContractRequisiteTariff = $delivery->hasContractRequisiteTariff();
        if (!$hasContractRequisiteTariff['status']) {
            $result['notifyWarning'] = true;
            $result['message'] = Yii::t('common', 'Заказы не могут быть отправлены в {delivery}. Заполните данные контракта.', ['delivery' => $delivery->name]) . ' ' . $hasContractRequisiteTariff['status'];
            return $result;
        }

        if ($delivery->hasDebts()) {
            $result['notifyWarning'] = true;
            $result['message'] = Yii::t('common', 'Заказы не могут быть отправлены в {delivery}. Есть ее не погашенная ДЗ более {days} дней.', [
                'delivery' => $delivery->name,
                'days' => $delivery->not_send_if_has_debts_days
            ]);
            return $result;
        }

        $countOldOrdersInProcess = $delivery->countOldOrdersInProcess();

        if ($countOldOrdersInProcess) {
            $result['notifyWarning'] = true;
            $result['message'] = Yii::t('common', 'Заказы не могут быть отправлены в {delivery}. {orders} заказов в процессе более {days} дней.', [
                'delivery' => $delivery->name,
                'orders' => $countOldOrdersInProcess,
                'days' => $delivery->not_send_if_in_process_days
            ]);
            return $result;
        }

        if (!$delivery->canDeliveryOrder($model)) {
            $response['message'] = Yii::t('common', 'Заказ не может быть доставлен данной службой доставки.');
            return $response;
        }

        $allowDeliveries = ChangerDelivery::getAllowDeliveries();
        $allowStatuses = ChangerDelivery::getAllowStatusArray();

        if (isset($allowDeliveries[$delivery_id])) {
            if (in_array($model->status_id, $allowStatuses)) {
                if ($delivery->workflow) {
                    $priorityNextStatuses = $delivery->workflow->getNextStatuses(OrderStatus::STATUS_CC_APPROVED);
                } else {
                    $priorityNextStatuses = $model->getNextStatuses(OrderStatus::STATUS_CC_APPROVED);
                }
                if ($delivery->auto_sending && !in_array(OrderStatus::STATUS_LOG_ACCEPTED,
                        $priorityNextStatuses) && in_array(OrderStatus::STATUS_DELIVERY_PENDING, $priorityNextStatuses)
                ) {
                    $transaction = Yii::$app->db->beginTransaction();
                    try {
                        $model->status_id = OrderStatus::STATUS_CC_APPROVED;
                        $model->pending_delivery_id = $delivery_id;
                        $model->route = Yii::$app->controller->route;
                        $model->commentForLog = $comment;
                        $deliveryRequestArray = DeliveryRequest::find()->where(['order_id' => $model->id])->all();
                        foreach ($deliveryRequestArray as $deliveryRequest) {
                            $deliveryRequest->delete();
                        }
                        $result = Delivery::createRequest($delivery, $model);
                        if ($result['status'] == 'success') {
                            $response['status'] = 'success';
                            $transaction->commit();
                        } else {
                            $transaction->rollBack();
                            $response['message'] = $result['message'];
                        }
                    } catch (\Exception $e) {
                        $transaction->rollBack();
                        throw $e;
                    }
                } else {
                    $model->pending_delivery_id = $delivery_id;
                    $model->status_id = OrderStatus::STATUS_CC_APPROVED;
                    $model->route = Yii::$app->controller->route;
                    $model->commentForLog = $comment;

                    if ($model->save(true, ['pending_delivery_id', 'status_id', 'route'])) {
                        $response['status'] = 'success';

                        $deliveryRequestArray = DeliveryRequest::find()->where(['order_id' => $model->id])->all();

                        foreach ($deliveryRequestArray as $deliveryRequest) {
                            $deliveryRequest->delete();
                        }
                    } else {
                        $response['message'] = array_shift($model->getFirstErrors());
                    }
                }
            } elseif ($ignore) {
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    if ($model->deliveryRequest) {
                        $model->deliveryRequest->delivery_id = $delivery_id;
                        $model->deliveryRequest->commentForLog = $comment;
                        $model->deliveryRequest->route = Yii::$app->controller->route;
                        if (!$model->deliveryRequest->save(true, ['delivery_id'])) {
                            throw new \Exception(Yii::t('common',
                                'Не удалось изменить службу доставки. Причина: {reason}',
                                ['reason' => $model->deliveryRequest->getFirstErrorAsString()]));
                        }
                    } else {
                        $model->pending_delivery_id = $delivery_id;
                        $model->route = Yii::$app->controller->route;
                        $model->commentForLog = $comment;
                        if (!$model->save(true, ['pending_delivery_id'])) {
                            throw new \Exception(Yii::t('common',
                                'Не удалось изменить службу доставки. Причина: {reason}',
                                ['reason' => $model->getFirstErrorAsString()]));
                        }
                    }
                    $response['status'] = 'success';
                    $transaction->commit();
                } catch (\Throwable $e) {
                    $transaction->rollBack();
                    $response['status'] = 'fail';
                    $response['message'] = $e->getMessage();
                }
            } else {
                $response['message'] = Yii::t('common',
                    'Смена доставки у заказа {orderId} со статусом "{status}" запрещена.', array(
                        'orderId' => $model->id,
                        'status' => $model->status->name,
                    )
                );
            }
        } else {
            $response['message'] = Yii::t('common', 'Неразрешенное значение доставки.');
        }

        return $response;
    }

    /** Смена статус заявке в курьерке. Пока доступна только суперадминам
     * @return array
     */
    public function actionChangeDeliveryRequestStatus()
    {
        $response = [
            'status' => 'fail',
            'message' => '',
        ];

        if (!Yii::$app->user->can('order.index.changedeliveryrequeststatus')) {
            $response['message'] = Yii::t('common', 'Изменение доступно только суперадмину.');
            return $response;
        }

        $id = Yii::$app->request->post('id');
        $status = Yii::$app->request->post('status');
        $comment = Yii::$app->request->post('comment');

        if (empty($comment)) {
            $response['message'] = Yii::t('common', 'Необходимо указать причину изменения.');
            return $response;
        }

        $model = $this->findModel($id);

        if ($model->deliveryRequest) {
            $model->deliveryRequest->route = Yii::$app->controller->route;
            $model->deliveryRequest->commentForLog = $comment;
            $model->deliveryRequest->status = $status;

            if (!$model->deliveryRequest->save(false, ['status'])) {
                $response['message'] = Yii::t('common', 'Не удалось сменить статус заявке. Причина: {reason}',
                    ['reason' => $model->deliveryRequest->getFirstErrorAsString()]);
            } else {
                $response['status'] = 'success';
            }
        } else {
            $response['message'] = Yii::t('common', 'Отсутствует заявка в службе доставки.');
        }

        return $response;
    }

    /** Смена статус заявке в КЦ. Пока доступна только суперадминам
     * @return array
     */
    public function actionChangeCallCenterRequestStatus()
    {
        $response = [
            'status' => 'fail',
            'message' => '',
        ];

        if (!Yii::$app->user->can('order.index.changecallcenterrequeststatus')) {
            $response['message'] = Yii::t('common', 'Изменение доступно только суперадмину.');
            return $response;
        }

        $id = Yii::$app->request->post('id');
        $status = Yii::$app->request->post('status');
        $comment = Yii::$app->request->post('comment');

        if (empty($comment)) {
            $response['message'] = Yii::t('common', 'Необходимо указать причину изменения.');
            return $response;
        }

        $model = $this->findModel($id);

        if ($model->callCenterRequest) {
            $model->callCenterRequest->route = Yii::$app->controller->route;
            $model->callCenterRequest->commentForLog = $comment;
            $model->callCenterRequest->status = $status;

            if (!$model->callCenterRequest->save(false, ['status'])) {
                $response['message'] = Yii::t('common', 'Не удалось сменить статус заявке. Причина: {reason}',
                    ['reason' => $model->callCenterRequest->getFirstErrorAsString()]);
            } else {
                $response['status'] = 'success';
            }
        } else {
            $response['message'] = Yii::t('common', 'Отсутствует заявка в КЦ.');
        }

        return $response;
    }

    /**
     * @return array
     */
    public function actionResendSeparateOrder()
    {
        $response = [
            'status' => 'fail',
            'message' => '',
        ];

        $id = Yii::$app->request->post('id');

        $model = $this->findModel($id);
        $allowStatuses = ResendSeparateOrder::getAllowStatusArray();

        if (in_array($model->status_id, $allowStatuses)) {

            if ($model->duplicate_order_id == null) {
                if ($orderCopy = $model->duplicate()) {

                    switch ($model->source_form) {
                        case Order::TYPE_FORM_SHORT:
                            $orderCopy->status_id = OrderStatus::STATUS_SOURCE_SHORT_FORM;
                            break;
                        case Order::TYPE_FORM_LONG:
                            $orderCopy->status_id = OrderStatus::STATUS_SOURCE_LONG_FORM;
                            break;
                        default:
                            $orderCopy->status_id = OrderStatus::STATUS_SOURCE_SHORT_FORM;
                            break;
                    }

                    $orderCopy->call_center_type = Order::TYPE_CC_NEW_RETURN;
                    if (!$orderCopy->save()) {
                        $response['status'] = 'fail';
                        $response['message'] = Yii::t('common', 'Для заказа №{orderId} не удалось создать копию: {error}', [
                            'orderId' => $model->id,
                            'error' => $orderCopy->getFirstErrorAsString()
                        ]);
                    } else {
                        Yii::$app->queueCallCenterSend->push(new SendCallCenterRequestJob(['orderId' => $orderCopy->id]));
                        $response['status'] = 'success';
                        $response['message'] = Yii::t('common', 'Для заказа №{orderId} успешно создана копия', [
                            'orderId' => $model->id
                        ]);
                    }

                } else {
                    $response['message'] = array_shift($model->getFirstErrors());
                }
            } else {

                $response['message'] = Yii::t('common', 'У заказа №{orderId} уже есть копия №{duplicateOrderId}.',

                    array(
                        'orderId' => $model->id,
                        'duplicateOrderId' => $model->duplicate_order_id,
                    )
                );
            }
        } else {
            $response['message'] = Yii::t('common',
                'Создание копии заказа №{orderId} со статусом "{status}" запрещено.', array(
                    'orderId' => $model->id,
                    'status' => $model->getStatus()->one()->name,
                )
            );
        }

        return $response;
    }

    /**
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionResendSeparateOrderIntoDelivery()
    {
        $response = [
            'status' => 'fail',
            'message' => '',
        ];

        $id = Yii::$app->request->post('id');
        $delivery_id = Yii::$app->request->post('delivery');
        //Игнорировать возраст заказа
        $orderAgeIgnore = (bool)Yii::$app->request->post('orderAgeIgnore');

        $delivery = $this->findDelivery($delivery_id);
        if (empty($delivery)) {
            $response['message'] = Yii::t('common', 'Отсутствует указанная служба доставки.');
            return $response;
        }
        $model = $this->findModel($id);

        if ($model->status_id == OrderStatus::STATUS_LOG_DEFERRED) {
            $result['notifyWarning'] = true;
            $result['message'] = Yii::t('common',
                'Нельзя отправить в курьерскую службу заказ в статусе "Логистика (отложено)"');
            return $result;
        }

        $allowStatuses = ResendSeparateOrderIntoDelivery::getAllowStatusArray();
        if (!in_array($model->status_id, $allowStatuses)) {
            $response['message'] = Yii::t('common',
                'Создание копии заказа №{orderId} со статусом "{status}" запрещено.', array(
                    'orderId' => $model->id,
                    'status' => $model->getStatus()->one()->name,
                )
            );
            return $response;
        }
        if (empty($model->callCenterRequest)) {
            $response['message'] = Yii::t('common', 'Отсутствует заявка в КЦ.');
            return $response;
        }
        // 1 месяц
        if (!$orderAgeIgnore && $model->callCenterRequest->approved_at < (time() - 3600 * 24 * 31)) {
            $response['message'] = Yii::t('common',
                "Невозможно скопировать заказ. Заказ является устаревшим.\nНеобходима \"Переотправка отдельным заказом(в КЦ)\".");
            return $response;
        }

        if (!empty($model->duplicate_order_id)) {
            $response['message'] = Yii::t('common', 'У заказа №{orderId} уже есть копия №{duplicateOrderId}.',

                array(
                    'orderId' => $model->id,
                    'duplicateOrderId' => $model->duplicate_order_id,
                )
            );
            return $response;
        }


        $hasContractRequisiteTariff = $model->deliveryRequest->delivery->hasContractRequisiteTariff();
        if (!$hasContractRequisiteTariff['status']) {
            $result['notifyWarning'] = true;
            $result['message'] = Yii::t('common', 'Заказы не могут быть отправлены в {delivery}. Заполните данные контракта.', ['delivery' => $model->deliveryRequest->delivery->name]) . ' ' . $hasContractRequisiteTariff['status'];
            return $result;
        }

        if ($model->deliveryRequest->delivery->hasDebts()) {
            $result['notifyWarning'] = true;
            $result['message'] = Yii::t('common', 'Заказы не могут быть отправлены в {delivery}. Есть ее не погашенная ДЗ более {days} дней.', [
                'delivery' => $model->deliveryRequest->delivery->name,
                'days' => $model->deliveryRequest->delivery->not_send_if_has_debts_days
            ]);
            return $result;
        }

        $countOldOrdersInProcess = $model->deliveryRequest->delivery->countOldOrdersInProcess();
        if ($countOldOrdersInProcess) {
            $result['notifyWarning'] = true;
            $result['message'] = Yii::t('common', 'Заказы не могут быть отправлены в {delivery}. {orders} заказов в процессе более {days} дней.', [
                'delivery' => $model->deliveryRequest->delivery->name,
                'orders' => $countOldOrdersInProcess,
                'days' => $model->deliveryRequest->delivery->not_send_if_in_process_days
            ]);
            return $result;
        }


        $transaction = Yii::$app->db->beginTransaction();

        try {
            if ($model->duplicateIntoAnotherDelivery($delivery)) {
                $response['status'] = 'success';

                $response['message'] = Yii::t('common', 'Для заказа №{orderId} успешно создана копия',
                    array('orderId' => $model->id));
                $transaction->commit();
            } else {
                $transaction->rollBack();
                $response['message'] = array_shift($model->getFirstErrors());
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            $response['message'] = $e->getMessage();
        }

        return $response;
    }

    /**
     * @return array
     * @throws \yii\web\ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionCreateCallCenterRequest()
    {
        $response = [
            'status' => 'success',
        ];

        $id = Yii::$app->request->post('id');
        $model = $this->findModel($id);

        if (!empty($model->created_at) && $model->created_at < (time() - 3600 * 24 * 7) && !Yii::$app->user->can("old_orders_access")) {
            throw new ForbiddenHttpException (Yii::t('common',
                "У вас недостаточно прав для просмотра таких старых заказов"));
        }
        Yii::$app->queueCallCenterSend->push(new SendCallCenterRequestJob(['orderId' => $model->id]));
        return $response;
    }

    /**
     * @return array
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionSendSmsPoll()
    {
        $smsPollQuestion_id = Yii::$app->request->post('sms_poll');
        $order_id = Yii::$app->request->post('id');
        $response = SmsPollHistoryController::sendSmsPollQuestions($order_id, $smsPollQuestion_id);

        return $response;
    }

    /**
     * @return array
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionResendInCallCenter()
    {
        $response = [
            'status' => 'fail',
        ];

        $id = Yii::$app->request->post('id');
        $model = $this->findModel($id);

        if (!empty($model->created_at) && $model->created_at < (time() - 3600 * 24 * 7) && !Yii::$app->user->can("old_orders_access")) {
            throw new ForbiddenHttpException (Yii::t('common',
                "У вас недостаточно прав для просмотра таких старых заказов"));
        }

        if ($model->status_id == OrderStatus::STATUS_SOURCE_SHORT_FORM || $model->status_id == OrderStatus::STATUS_SOURCE_LONG_FORM) {
            if ($model->callCenterRequest && $model->callCenterRequest->status == CallCenterRequest::STATUS_ERROR) {
                $model->callCenterRequest->status = CallCenterRequest::STATUS_PENDING;
                $model->callCenterRequest->save(false, ['status']);
                Yii::$app->queueCallCenterSend->push(new SendCallCenterRequestJob(['orderId' => $model->id]));
                $response['status'] = 'success';
            } else {
                $response['message'] = Yii::t('common', 'Неверный статус у заявки.');
            }
        } else {
            $response['message'] = Yii::t('common', 'Неверный статус у заказа.');
        }

        return $response;
    }


    /**
     * @return string
     */
    public function actionResendInCallCenterByExcel()
    {
        $columnNum = 0;
        if (Yii::$app->request->post('column_num')) {
            $columnNum = intval(Yii::$app->request->post('column_num'));
        }
        // Получаем файл с ордерами
        try {
            $file = UploadedFile::getInstanceByName('OrdersExcelFile');
            $file_type = \PHPExcel_IOFactory::identify($file->tempName);
            $objReader = \PHPExcel_IOFactory::createReader($file_type);
            $objPHPExcel = $objReader->load($file->tempName);
            $table = $objPHPExcel->getActiveSheet()->toArray();
        } catch (\Exception $e) {
            Yii::$app->notifier->addNotification('Отсутствует excel-файл или не верный формат файла',
                Notifier::TYPE_DANGER);
            return $this->redirect('/order/index/index');
        }

        $res = [];
        if (!empty($table)) {
            foreach ($table as $row) {
                $res[] = $row[$columnNum];
            }
            if (!empty($res)) {
                unset($res[0]);
            }

            $ids = implode(",", $res);

            $data['DateFilter']['dateType'] = '';
            $data['NumberFilter']['not'][] = '';
            $data['NumberFilter']['entity'][] = 'id';
            $data['NumberFilter']['number'][] = $ids;
            Yii::$app->session->set('orderlist-is-loaded-from-excel', $data);

            Yii::$app->notifier->addNotification('Список заказов из excel-файла успешно сформирован и выведен',
                Notifier::TYPE_SUCCESS);
            return $this->redirect('/order/index/index');
        }

        Yii::$app->notifier->addNotification('Не удалось сформировать список заказов из excel-файла',
            Notifier::TYPE_DANGER);
        return $this->redirect('/order/index/index');
    }


    /**
     * @return Response
     */
    public function actionExcelOrderListClear()
    {
        if (Yii::$app->session->has('orderlist-is-loaded-from-excel')) {
            Yii::$app->session->remove('orderlist-is-loaded-from-excel');
        }

        return $this->redirect('/order/index/index');
    }


    /**
     * @return array
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionResendInCallCenterNewQueue()
    {
        $model = $this->findModel(Yii::$app->request->post('id'));

        if (!empty($model->created_at) && $model->created_at < (time() - 3600 * 24 * 7) && !Yii::$app->user->can("old_orders_access")) {
            throw new ForbiddenHttpException (Yii::t('common',
                "У вас недостаточно прав для просмотра таких старых заказов"));
        }

        return Lead::resendCallCenterRequest($model);
    }

    /**
     * @return array
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionResendInCallCenterStillWaiting()
    {
        $response = [
            'status' => 'fail',
            'message' => Yii::t('common', 'Отсутствует доступная заявка в курьерскую службу.'),
        ];

        $model = $this->findModel(Yii::$app->request->post('id'));

        if (!empty($model->created_at) && $model->created_at < (time() - 3600 * 24 * 7) && !Yii::$app->user->can("old_orders_access")) {
            throw new ForbiddenHttpException (Yii::t('common',
                "У вас недостаточно прав для просмотра таких старых заказов"));
        }

        //если уже отправлялся
        if (CallCenterCheckRequest::find()->where([
            'order_id' => $model->id,
            'type' => CallCenterCheckRequest::TYPE_STILL_WAITING,
            'status' => [CallCenterCheckRequest::STATUS_PENDING, CallCenterCheckRequest::STATUS_IN_PROGRESS]
        ])->exists()
        ) {
            throw new ForbiddenHttpException (Yii::t('common', 'Заказ сейчас уже на обработке в очереди ' . CallCenterCheckRequest::TYPE_STILL_WAITING));
        }

        if (in_array($model->status_id, array_merge(OrderStatus::getBuyoutList(), OrderStatus::getNotBuyoutInProcessList(), OrderStatus::getOnlyNotBuyoutDoneList())) ||
            !in_array($model->status_id, array_merge(OrderStatus::getProcessDeliveryList(), [
                OrderStatus::STATUS_DELIVERY_REJECTED,
                OrderStatus::STATUS_DELIVERY_LOST,
            ]))
        ) {
            throw new ForbiddenHttpException (Yii::t('common', "Некорректный статус для данной операции"));
        }

        $success = true;
        $transaction = Yii::$app->db->beginTransaction();

        if ($model->canChangeStatusTo(OrderStatus::STATUS_CC_CHECKING)) {
            $model->status_id = OrderStatus::STATUS_CC_CHECKING;
            if (!$model->save(true, ['status_id'])) {
                throw new ForbiddenHttpException (Yii::t('common', "Не удалось сменить статус заказа"));
            }
        }

        $afterActionId = Yii::$app->request->post('after_action_id');

        $orderCheckHistory = new CallCenterCheckRequest;
        $orderCheckHistory->order_id = $model->id;
        $orderCheckHistory->user_id = Yii::$app->user->id;
        $orderCheckHistory->status_id = $model->status_id;
        $orderCheckHistory->type = CallCenterCheckRequest::TYPE_STILL_WAITING;
        $orderCheckHistory->status = CallCenterCheckRequest::STATUS_PENDING;
        $orderCheckHistory->call_center_id = $model->callCenterRequest->call_center_id;
        $orderCheckHistory->after_action_id = empty($afterActionId) ? null : $afterActionId;

        if (!$orderCheckHistory->save()) {
            $success = false;
            $response['message'] = Yii::t('common', 'Не удалось создать заявку');
        }

        if ($success) {
            $transaction->commit();
            $response['status'] = 'success';
        } else {
            $transaction->rollBack();
        }

        return $response;
    }

    /**
     * @return array
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionResendInCallCenterReturnNoProd()
    {
        $model = $this->findModel(Yii::$app->request->post('id'));

        if (!empty($model->created_at) && $model->created_at < (time() - 3600 * 24 * 7) && !Yii::$app->user->can("old_orders_access")) {
            throw new ForbiddenHttpException (Yii::t('common',
                "У вас недостаточно прав для просмотра таких старых заказов"));
        }

        return Lead::resendCallCenterRequestReturnNoProd($model);
    }

    /**
     * @return array
     * @throws ForbiddenHttpException
     */
    public function actionRetryGetStatusFromCallCenter()
    {
        $response = [
            'status' => 'fail',
        ];

        $id = Yii::$app->request->post('id');
        $model = $this->findModel($id);

        if (!empty($model->created_at) && $model->created_at < (time() - 3600 * 24 * 7) && !Yii::$app->user->can("old_orders_access")) {
            throw new ForbiddenHttpException (Yii::t('common',
                "У вас недостаточно прав для просмотра таких старых заказов"));
        }

        if (is_null($model->callCenterRequest)) {
            $response['message'] = Yii::t('common', 'Отсутствует заявка в КЦ.');
            return $response;
        }

        if ($model->callCenterRequest->status == CallCenterRequest::STATUS_PENDING) {
            $response['message'] = Yii::t('common', 'Неверный статус у заявки.');
            return $response;
        }

        if (empty($model->callCenterRequest->foreign_id)) {
            $response['message'] = Yii::t('common', 'Отсутствует номер заявки в КЦ.');
            return $response;
        }

        $availableStatuses = [
            OrderStatus::STATUS_CC_POST_CALL,
            OrderStatus::STATUS_CC_FAIL_CALL,
            OrderStatus::STATUS_CC_RECALL,
            OrderStatus::STATUS_CC_REJECTED,
            OrderStatus::STATUS_CC_TRASH,
        ];

        if (!in_array($model->status_id, $availableStatuses)) {
            $response['message'] = Yii::t('common', 'Неверный статус у заказа.');
            return $response;
        }

        $transaction = Yii::$app->db->beginTransaction();
        try {
            if (in_array($model->status_id, [OrderStatus::STATUS_CC_TRASH, OrderStatus::STATUS_CC_REJECTED])) {
                $model->status_id = OrderStatus::STATUS_CC_POST_CALL;
                if (!$model->save(false, ['status_id'])) {
                    throw new \Exception(Yii::t('common', 'Не удалось изменить статус заказа.'));
                }
            }

            $model->callCenterRequest->status = CallCenterRequest::STATUS_IN_PROGRESS;
            $model->callCenterRequest->foreign_status = 1;
            $model->callCenterRequest->foreign_substatus = null;
            $model->callCenterRequest->cron_launched_at = 0;
            $model->callCenterRequest->foreign_waiting = 0;
            if (!$model->callCenterRequest->save(true,
                ['status', 'cron_launched_at', 'foreign_status', 'foreign_substatus', 'foreign_waiting'])
            ) {
                throw new \Exception(Yii::t('common', 'Не удалось сохранить изменения заявки.'));
            }
            $response['status'] = 'success';
            $transaction->commit();
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $response['message'] = $e->getMessage();
            $response['status'] = 'fail';
        }

        return $response;
    }

    /**
     * @return Response
     */
    public function actionGenerateList()
    {
        $ids = Yii::$app->request->post('id');
        $delivery = Yii::$app->request->post('delivery');

        if ($ids && is_array($ids)) {
            $delivery = Delivery::find()
                ->where(['id' => $delivery])
                ->byCountryId(Yii::$app->user->country->id)
                ->active()
                ->one();

            if ($delivery) {
                $commit = false;
                $successIds = [];
                $successModels = [];

                $models = Order::find()
                    ->where(['in', Order::tableName() . '.id', $ids])
                    ->andWhere(['country_id' => Yii::$app->user->country->id])
                    ->andWhere([
                        "!=",
                        Order::tableName() . ".status_id",
                        OrderStatus::STATUS_LOG_DEFERRED
                    ])// исключаем "Логистика (отложено)"
                    ->all();

                $transaction = Yii::$app->db->beginTransaction();

                $list = new OrderLogisticList();
                $list->country_id = Yii::$app->user->country->id;
                $list->delivery_id = $delivery->id;
                $list->user_id = Yii::$app->user->id;

                $list->ticketFile = UploadedFile::getInstance($list, 'ticketFile');

                if ($list->upload()) {
                    foreach ($models as $model) {
                        if ($model->canChangeStatusTo(OrderStatus::STATUS_LOG_GENERATED)) {
                            $model->status_id = OrderStatus::STATUS_LOG_GENERATED;

                            $model->route = Yii::$app->controller->route;

                            $deliveryRequestModel = $this->findModel($model->id);
                            $deliveryIdPost = Yii::$app->request->post('delivery');

                            if (!Delivery::createRequestInList($deliveryIdPost, $deliveryRequestModel)) {

                                Yii::$app->notifier->addNotification(Yii::t('common',
                                    'Ошибка при создании заявок для листа.'));
                                $transaction->rollBack();
                                return $this->redirect('index');
                            }

                            if ($model->save(true, ['status_id'])) {
                                $successIds[] = $model->id;
                                $successModels[] = $model;
                            }
                        }
                    }

                    if ($successIds) {
                        $todayStart = Yii::$app->formatter->asTimestamp(date('Y-m-d 00:00:00'));
                        $todayEnd = $todayStart + 86399;

                        $todayCount = OrderLogisticList::find()
                            ->where(['country_id' => Yii::$app->user->country->id])
                            ->andWhere(['delivery_id' => $delivery->id])
                            ->andWhere(['>=', 'created_at', $todayStart])
                            ->andWhere(['<=', 'created_at', $todayEnd])
                            ->count();

                        $list->number = $todayCount + 1;
                        $list->ids = implode(',', $successIds);

                        if ($list->save()) {
                            $commit = true;
                            Yii::$app->notifier->addNotification(
                                Yii::t('common', 'Лист успешно сгенерирован.'),
                                'success'
                            );
                        } else {
                            Yii::$app->notifier->addNotification(Yii::t('common', 'Не удалось сгенерировать лист.'));
                        }
                    } else {
                        Yii::$app->notifier->addNotification(Yii::t('common', 'Необходимо указать корректные заказы.'));
                    }
                } else {
                    Yii::$app->notifier->addNotificationsByModel($list);
                }

                if ($commit) {
                    $transaction->commit();

                    $errors = count($ids) - count($successIds);

                    if ($errors) {
                        Yii::$app->notifier->addNotification(
                            Yii::t(
                                'common',
                                '{n, plural, one{# заказ} few{# заказа} many{# заказов} other{# заказов}} не удалось добавить в сгенерированный лист.',
                                [
                                    'n' => $errors,
                                ]
                            )
                        );
                    }
                } else {
                    $transaction->rollBack();
                }
            } else {
                Yii::$app->notifier->addNotification(
                    Yii::t('common', 'Необходимо указать корректную службу доставки.'),
                    Notifier::TYPE_DANGER
                );
            }
        }

        return $this->redirect('index');
    }

    /**
     * @return Response
     */
    public function actionGeneratePretensionList()
    {
        $ids = Yii::$app->request->post('id');
        $delivery = Yii::$app->request->post('delivery');
        $pretension = Yii::$app->request->post('pretension');

        if ($ids && is_array($ids) && array_key_exists($pretension, OrderLogisticList::getPretensionTypesCollection())) {
            $delivery = Delivery::find()
                ->where(['id' => $delivery])
                ->byCountryId(Yii::$app->user->country->id)
                ->active()
                ->one();

            if ($delivery) {

                $orderQuery = Order::find()
                    ->joinWith([
                        'callCenterRequest',
                        'deliveryRequest',
                    ])
                    ->where(['in', Order::tableName() . '.id', $ids])
                    ->andWhere(['country_id' => Yii::$app->user->country->id]);

                if (Yii::$app->user->getIsImplant()) {
                    $delivery_ids = Yii::$app->user->getDeliveriesIds();
                    $orderQuery->andFilterWhere([
                        'or',
                        [
                            'and',
                            DeliveryRequest::tableName() . '.delivery_id is not null',
                            [DeliveryRequest::tableName() . ".delivery_id" => $delivery_ids]
                        ],
                        [
                            'and',
                            DeliveryRequest::tableName() . '.delivery_id is null',
                            [Order::tableName() . ".pending_delivery_id" => $delivery_ids]
                        ]
                    ]);
                }

                $orderQuery->joinWith(['orderFinancePretensions']);
                $pretensionMapping = OrderLogisticList::getPretensionMapping();
                $orderQuery->andWhere(['in', OrderFinancePretension::tableName() . '.type', $pretensionMapping[$pretension]]);
                $orderQuery->andWhere([OrderFinancePretension::tableName() . '.status' => OrderFinancePretension::PRETENSION_STATUS_IN_PROGRESS]);

                $orders = $orderQuery->all();
                $successIds = ArrayHelper::getColumn($orders, 'id');

                $list = new OrderLogisticList();
                $list->country_id = Yii::$app->user->country->id;
                $list->delivery_id = $delivery->id;
                $list->user_id = Yii::$app->user->id;
                $list->type = $pretension;

                if ($successIds) {
                    $list->number = $list->getTodayCountByDelivery() + 1;
                    $list->ids = implode(',', $successIds);

                    if ($list->save()) {

                        Yii::$app->notifier->addNotification(
                            Yii::t('common', 'Лист претензии успешно сгенерирован. Для отправки перейдите в список листов'), 'success'
                        );

                        $errors = count($ids) - count($successIds);
                        if ($errors) {
                            Yii::$app->notifier->addNotification(
                                Yii::t(
                                    'common',
                                    '{n, plural, one{# заказ} few{# заказа} many{# заказов} other{# заказов}} не удалось добавить в сгенерированный лист.',
                                    [
                                        'n' => $errors,
                                    ]
                                )
                            );
                        }
                    } else {
                        Yii::$app->notifier->addNotification(Yii::t('common', 'Не удалось сгенерировать лист претензии.'));
                    }
                } else {
                    Yii::$app->notifier->addNotification(Yii::t('common', 'Необходимо указать корректные заказы.'));
                }

            } else {
                Yii::$app->notifier->addNotification(
                    Yii::t('common', 'Необходимо указать корректную службу доставки.'),
                    Notifier::TYPE_DANGER
                );
            }
        }
        return $this->redirect('index');
    }


    /**
     * @return array
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionSendInDelivery()
    {
        $model = $this->findModel(Yii::$app->request->post('id'));

        if (!empty($model->created_at) && $model->created_at < (time() - 3600 * 24 * 7) && !Yii::$app->user->can("old_orders_access")) {
            throw new ForbiddenHttpException (Yii::t('common',
                "У вас недостаточно прав для просмотра таких старых заказов"));
        }

        if ($model->status_id == OrderStatus::STATUS_LOG_DEFERRED) {
            $result['notifyWarning'] = true;
            $result['message'] = Yii::t('common',
                'Нельзя отправить в курьерскую службу заказ в статусе "Логистика (отложено)"');
            return $result;
        }

        $deliveryIdPost = Yii::$app->request->post('delivery');
        $delivery = Delivery::findOne($deliveryIdPost);
        if (!$delivery->canSendOrder()) {
            $result['notifyWarning'] = true;
            $result['message'] = Yii::t('common', 'Апи курьерской службы не предназначено для отправки заказов');
            return $result;
        }

        $hasContractRequisiteTariff = $delivery->hasContractRequisiteTariff();

        if (!$hasContractRequisiteTariff['status']) {
            $result['notifyWarning'] = true;
            $result['message'] = Yii::t('common', 'Заказы не могут быть отправлены в {delivery}. Заполните данные контракта.', ['delivery' => $delivery->name]) . ' ' . $hasContractRequisiteTariff['status'];
            return $result;
        }

        if ($delivery->hasDebts()) {
            $result['notifyWarning'] = true;
            $result['message'] = Yii::t('common', 'Заказы не могут быть отправлены в {delivery}. Есть ее не погашенная ДЗ более {days} дней.', [
                'delivery' => $delivery->name,
                'days' => $delivery->not_send_if_has_debts_days
            ]);
            return $result;
        }

        $countOldOrdersInProcess = $delivery->countOldOrdersInProcess();
        if ($countOldOrdersInProcess) {
            $result['notifyWarning'] = true;
            $result['message'] = Yii::t('common', 'Заказы не могут быть отправлены в {delivery}. {orders} заказов в процессе более {days} дней.', [
                'delivery' => $delivery->name,
                'orders' => $countOldOrdersInProcess,
                'days' => $delivery->not_send_if_in_process_days
            ]);
            return $result;
        }

        $deliveryId = $model->pending_delivery_id ? $model->pending_delivery_id : $deliveryIdPost;

        if (!$model->deliveryRequest) {
            if ($deliveryId != $deliveryIdPost) {
                $result['notifyWarning'] = true;

                $result['message'] = Yii::t(
                    'common',
                    'Заказу #{id} была назначена другая курьерская служба. Обратитесь к разработчику.',
                    [
                        'id' => $model->id,
                    ]
                );
            } else {
                $result['notifyWarning'] = false;
                $result = Delivery::createRequest($deliveryId, $model);
            }
        } else {
            $result['status'] = 'fail';
            $result['message'] = Yii::t(
                'common',
                'Заявка уже существует. Требуется повторная отправка заявки в службу доставки.'
            );
        }

        return $result;
    }

    /**
     * @return array
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionResendInDelivery()
    {
        $response = [
            'status' => 'fail',
            'message' => '',
        ];

        $id = Yii::$app->request->post('id');

        $model = $this->findModel($id);


        if (!empty($model->created_at) && $model->created_at < (time() - 3600 * 24 * 7) && !Yii::$app->user->can("old_orders_access")) {
            throw new ForbiddenHttpException (Yii::t('common',
                "У вас недостаточно прав для просмотра таких старых заказов"));
        }

        if ($model->status_id == OrderStatus::STATUS_LOG_DEFERRED) {
            $result['notifyWarning'] = true;
            $result['message'] = Yii::t('common',
                'Нельзя отправить в курьерскую службу заказ в статусе "Логистика (отложено)"');
            return $result;
        }

        if ($model->deliveryRequest && ($model->deliveryRequest->status == DeliveryRequest::STATUS_ERROR || $model->deliveryRequest->status == DeliveryRequest::STATUS_IN_LIST)) {
            $hasContractRequisiteTariff = $model->deliveryRequest->delivery->hasContractRequisiteTariff();
            if (!$hasContractRequisiteTariff['status']) {
                $result['notifyWarning'] = true;
                $result['message'] = Yii::t('common', 'Заказы не могут быть отправлены в {delivery}. Заполните данные контракта.', ['delivery' => $model->deliveryRequest->delivery->name]) . ' ' . $hasContractRequisiteTariff['status'];
                return $result;
            }

            if ($model->deliveryRequest->delivery->hasDebts()) {
                $result['notifyWarning'] = true;
                $result['message'] = Yii::t('common', 'Заказы не могут быть отправлены в {delivery}. Есть ее не погашенная ДЗ более {days} дней.', [
                    'delivery' => $model->deliveryRequest->delivery->name,
                    'days' => $model->deliveryRequest->delivery->not_send_if_has_debts_days
                ]);
                return $result;
            }

            $countOldOrdersInProcess = $model->deliveryRequest->delivery->countOldOrdersInProcess();
            if ($countOldOrdersInProcess) {
                $result['notifyWarning'] = true;
                $result['message'] = Yii::t('common', 'Заказы не могут быть отправлены в {delivery}. {orders} заказов в процессе более {days} дней.', [
                    'delivery' => $model->deliveryRequest->delivery->name,
                    'orders' => $countOldOrdersInProcess,
                    'days' => $model->deliveryRequest->delivery->not_send_if_in_process_days
                ]);
                return $result;
            }

            $success = true;
            $transaction = Yii::$app->db->beginTransaction();

            $model->status_id = OrderStatus::STATUS_DELIVERY_PENDING;
            $setDeliveryId = false;
            if ($model->deliveryRequest->delivery) {
                $contract = $model->deliveryRequest->delivery->getActiveContract();
                if ($contract) {
                    if (!empty($contract->max_delivery_period) && $model->delivery_time_from > strtotime("+{$contract->max_delivery_period}days")) {
                        $model->pending_delivery_id = $model->deliveryRequest->delivery_id;
                        $setDeliveryId = true;
                    }
                }
            }
            if (!$setDeliveryId) {
                $model->deliveryRequest->status = DeliveryRequest::STATUS_PENDING;
            }

            $model->route = Yii::$app->controller->route;

            if (!$model->save()) {
                $success = false;
                $response['message'] = Yii::t('common', 'Не удалось сменить статус заказу.');
            }

            if (!$model->deliveryRequest->save()) {
                $success = false;
                $response['message'] = Yii::t('common', 'Не удалось сменить статус заявке.');
            }

            if ($success) {
                $transaction->commit();
                $response['status'] = 'success';
            } else {
                $transaction->rollBack();
            }
        } else {
            $response['message'] = Yii::t('common', 'Отсутствует доступная заявка в курьерскую службу.');
        }

        return $response;
    }

    /**
     * @return array
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionSendCheckOrder()
    {
        $response = [
            'status' => 'fail',
            'message' => Yii::t('common', 'Отсутствует доступная заявка в курьерскую службу.'),
        ];


        $id = Yii::$app->request->post('id');

        //если уже отправлялся
        if (CallCenterCheckRequest::find()
            ->where(['order_id' => $id, 'type' => CallCenterCheckRequest::TYPE_RETURN])
            ->andWhere(['!=', 'status', CallCenterCheckRequest::STATUS_DONE])
            ->exists()
        ) {
            throw new ForbiddenHttpException (Yii::t('common', 'Заказ уже был отправлен на проверку'));
        }

        $model = $this->findModel($id);

        //Заказ должен быть аппрувнутым КЦ
        if ($model->status_id < OrderStatus::STATUS_CC_APPROVED
            || $model->status_id == OrderStatus::STATUS_CC_TRASH
            || $model->status_id == OrderStatus::STATUS_CC_DOUBLE
            || $model->status_id == OrderStatus::STATUS_CC_INPUT_QUEUE
        ) {
            throw new ForbiddenHttpException (Yii::t('common', "Некорректный статус для данной операции"));
        }

        //Проверка заявки в КЦ
        if (!$model->getCallCenterRequest()->exists()) {
            throw new ForbiddenHttpException (Yii::t('common', "Нет заявки в КЦ"));
        }

        $success = true;
        $transaction = Yii::$app->db->beginTransaction();

        $orderCheckHistory = new CallCenterCheckRequest;
        $orderCheckHistory->order_id = $model->id;
        $orderCheckHistory->user_id = Yii::$app->user->id;
        $orderCheckHistory->status_id = $model->status_id;
        $orderCheckHistory->type = CallCenterCheckRequest::TYPE_RETURN;
        $orderCheckHistory->status = CallCenterCheckRequest::STATUS_PENDING;
        $orderCheckHistory->call_center_id = $model->callCenterRequest->call_center_id;

        if (!$orderCheckHistory->save()) {
            $success = false;
            $response['message'] = Yii::t('common', 'Не удалось создать заявку');
        }

        if ($success) {
            $transaction->commit();
            $response['status'] = 'success';
        } else {
            $transaction->rollBack();
        }

        return $response;
    }

    /**
     * @return array
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     */
    public function actionSendCheckAddress()
    {
        $response = [
            'status' => 'fail',
            'message' => Yii::t('common', 'Отсутствует доступная заявка в курьерскую службу.'),
        ];

        $id = Yii::$app->request->post('id');

        //если уже отправлялся
        if (CallCenterCheckRequest::find()->where([
            'order_id' => $id,
            'type' => CallCenterCheckRequest::TYPE_CHECK_ADDRESS
        ])->andWhere(['!=', 'status', CallCenterCheckRequest::STATUS_DONE])->exists()
        ) {
            throw new ForbiddenHttpException (Yii::t('common', 'Заказ уже был отправлен на проверку'));
        }

        $model = $this->findModel($id);

        //Заказ должен быть в статусе 16,17,19,27  9,12,37,38
        if (!in_array($model->status_id, [
            OrderStatus::STATUS_DELIVERY_REJECTED,
            OrderStatus::STATUS_DELIVERY_DENIAL,
            OrderStatus::STATUS_DELIVERY_RETURNED,
            OrderStatus::STATUS_DELIVERY_REFUND,
            OrderStatus::STATUS_LOG_GENERATED,
            OrderStatus::STATUS_DELIVERY_ACCEPTED,
            OrderStatus::STATUS_LOG_SENT,
            OrderStatus::STATUS_LOG_RECEIVED,
        ])
        ) {
            throw new ForbiddenHttpException (Yii::t('common', "Некорректный статус для данной операции"));
        }

        //Проверка заявки в КЦ
        if (!$model->getCallCenterRequest()->exists()) {
            throw new ForbiddenHttpException (Yii::t('common', 'Нет заявки в КЦ'));
        }

        if (!$model->getDeliveryRequest()->exists()) {
            throw new ForbiddenHttpException (Yii::t('common', 'Нет заявки в КC'));
        }

        if (!$model->deliveryRequest->delivery->emailList) {
            throw new ForbiddenHttpException (Yii::t('common', 'Пустой список e-mail для автоматической отправки листов в КС {name}', ['name' => $model->deliveryRequest->delivery->name]));
        }

        $transaction = Yii::$app->db->beginTransaction();
        try {

            $orderCheckHistory = new CallCenterCheckRequest;
            $orderCheckHistory->status_id = $model->status_id;
            $orderCheckHistory->order_id = $model->id;

            //для заказов в 16,17,19,27 статусе меняем на 39
            if ($model->status_id == OrderStatus::STATUS_DELIVERY_DENIAL ||
                $model->status_id == OrderStatus::STATUS_DELIVERY_RETURNED ||
                $model->status_id == OrderStatus::STATUS_DELIVERY_REJECTED ||
                $model->status_id == OrderStatus::STATUS_DELIVERY_REFUND) {
                if ($model->canChangeStatusTo(OrderStatus::STATUS_CC_CHECKING)) {
                    $model->status_id = OrderStatus::STATUS_CC_CHECKING;
                    if (!$model->save(true, ['status_id'])) {
                        throw new ForbiddenHttpException (Yii::t('common', 'Не удалось сменить статус заказа'));
                    }
                }
            }

            if ($model->deliveryRequest->status != DeliveryRequest::STATUS_DONE) {
                $model->deliveryRequest->status = DeliveryRequest::STATUS_DONE;
                if (!$model->deliveryRequest->save(true, ['status'])) {
                    throw new ForbiddenHttpException (Yii::t('common', 'Не удалось сменить статус заявки в КС'));
                }
            }

            $orderCheckHistory->user_id = Yii::$app->user->id;
            $orderCheckHistory->type = CallCenterCheckRequest::TYPE_CHECK_ADDRESS;
            $orderCheckHistory->status = CallCenterCheckRequest::STATUS_PENDING;
            $orderCheckHistory->call_center_id = $model->callCenterRequest->call_center_id;

            if ($orderCheckHistory->save()) {
                $response['status'] = 'success';
                $transaction->commit();
            } else {
                $response['message'] = Yii::t('common', 'Не удалось создать заявку');
                $transaction->rollBack();
            }
        } catch (\Exception $e) {
            $response['message'] = $e->getMessage();
            $transaction->rollBack();
        }

        return $response;
    }

    /**
     * @return array
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     */
    public function actionSendCheckUndeliveryOrder()
    {
        $response = [
            'status' => 'fail',
            'message' => Yii::t('common', 'Отсутствует доступная заявка в курьерскую службу.'),
        ];


        $id = Yii::$app->request->post('id');
        /** @var string $no_repeat */
        $no_repeat = Yii::$app->request->post('no_repeat');

        if ($no_repeat == 'true') {
            //если уже отправлялся
            if (CallCenterCheckRequest::find()->where([
                'order_id' => $id,
                'type' => CallCenterCheckRequest::TYPE_CHECKUNDELIVERY,
            ])
                ->exists()
            ) {
                throw new ForbiddenHttpException (Yii::t('common', 'Заказ уже ранее отправлялся на проверку.'));
            }
        }

        $model = $this->findModel($id);

        if (!in_array($model->status_id, [
            // для вкладки Отказы в отчете:
            OrderStatus::STATUS_DELIVERY_DENIAL,
            OrderStatus::STATUS_DELIVERY_RETURNED,
            OrderStatus::STATUS_DELIVERY_REJECTED,
            OrderStatus::STATUS_LOGISTICS_REJECTED,
            OrderStatus::STATUS_DELIVERY_REFUND,
            OrderStatus::STATUS_LOGISTICS_ACCEPTED,
            OrderStatus::STATUS_DELIVERY_LOST,
            // для вкладки Холды в отчете:
            OrderStatus::STATUS_DELIVERY_ACCEPTED,
            OrderStatus::STATUS_DELIVERY_LEFT_WAREHOUSE,
            OrderStatus::STATUS_DELIVERY,
            OrderStatus::STATUS_DELIVERY_LEFT_OUR_WAREHOUSE,
            OrderStatus::STATUS_DELIVERY_REDELIVERY,
            OrderStatus::STATUS_DELIVERY_LEFT_THEIR_WAREHOUSE,
            OrderStatus::STATUS_DELIVERY_DELAYED,
            OrderStatus::STATUS_LOG_SENT,
            OrderStatus::STATUS_LOG_RECEIVED,
            // для вкладки Выкуп в отчете:
            OrderStatus::STATUS_DELIVERY_BUYOUT
        ])
        ) {
            throw new ForbiddenHttpException (Yii::t('common', "Некорректный статус для данной операции"));
        }

        //Проверка заявки в КЦ
        if (!$model->getCallCenterRequest()->exists()) {
            throw new ForbiddenHttpException (Yii::t('common', "Отсутствует заявка в КЦ"));
        }

        if ($model->sendToCheckUndelivery()) {
            $response['status'] = 'success';
        }
        else {
            $response['message'] = Yii::t('common', 'Не удалось создать заявку');
        }

        return $response;
    }


    /**
     * @return Response
     */
    public function actionGenerateMarketingList()
    {

        $ids = Yii::$app->request->post('id');
        $name = Yii::$app->request->post('name');

        //если имя листа не названо
        if (!$name) {
            $name = date("d.m.Y");
        }

        $nameCount = MarketingList::find()
            ->where(['country_id' => Yii::$app->user->country->id])
            ->andWhere(['name' => $name])
            ->count();

        if ($nameCount) {
            $name .= "(" . $nameCount . ")";
        }

        if ($ids && is_array($ids)) {

            $commit = false;
            $successIds = [];

            $models = Order::find()
                ->where(['in', 'id', $ids])
                ->andWhere(['country_id' => Yii::$app->user->country->id])
                ->all();

            $transaction = Yii::$app->db->beginTransaction();

            $list = new MarketingList();
            $list->country_id = Yii::$app->user->country->id;
            $list->user_id = Yii::$app->user->id;
            $list->name = $name;

            if ($list->save()) {

                foreach ($models as $model) {

                    $listOrder = new MarketingListOrder();
                    $listOrder->marketing_list_id = $list->id;
                    $listOrder->old_order_id = $model->id;

                    if ($listOrder->save()) {
                        $successIds[] = $model->id;
                    }
                }
            }

            if ($successIds) {
                $commit = true;
            } else {
                Yii::$app->notifier->addNotification(Yii::t('common', 'Необходимо указать корректные заказы.'));
            }

            if ($commit) {
                $transaction->commit();

                $errors = count($ids) - count($successIds);

                if ($errors) {
                    Yii::$app->notifier->addNotification(Yii::t('common',
                        '{n, plural, one{# заказ} few{# заказа} many{# заказов} other{# заказов}} не удалось добавить в сгенерированный лист.',
                        [
                            'n' => $errors,
                        ]));
                }
            } else {
                $transaction->rollBack();
            }
        }
        return $this->redirect('/order/marketing-list/index');
    }

    /**
     * @return array
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionSendAnalysisTrash()
    {

        $response = [
            'status' => 'fail',
            'message' => Yii::t('common', "Ничего не сделали")
        ];

        $id = Yii::$app->request->post('id');

        $model = $this->findModel($id);

        //нет прав
        if (!Yii::$app->user->can("order.index.sendanalysistrash")) {
            throw new ForbiddenHttpException (Yii::t('common', 'Нет прав для выполнения данной операции'));
        }

        //Заказ должен быть трешем КЦ
        if ($model->status_id != OrderStatus::STATUS_CC_TRASH) {
            throw new ForbiddenHttpException (Yii::t('common', "Некорректный статус для данной операции"));
        }

        //Проверка заявки в КЦ
        if (!$model->getCallCenterRequest()->exists()) {
            throw new ForbiddenHttpException (Yii::t('common', "Нет заявки в КЦ"));
        }

        if (CallCenterCheckRequest::find()->where([
            'order_id' => $id,
            'type' => CallCenterCheckRequest::TYPE_TRASH_CHECK
        ])->andWhere(['!=', 'status', CallCenterCheckRequest::STATUS_DONE])->exists()
        ) {
            throw new ForbiddenHttpException (Yii::t('common', 'Заказ уже был отправлен на проверку'));
        }

        $success = true;
        $transaction = Yii::$app->db->beginTransaction();

        $orderCheckHistory = new CallCenterCheckRequest();
        $orderCheckHistory->order_id = $model->id;
        $orderCheckHistory->user_id = Yii::$app->user->id;
        $orderCheckHistory->status_id = $model->status_id;
        $orderCheckHistory->type = CallCenterCheckRequest::TYPE_TRASH_CHECK;
        $orderCheckHistory->status = CallCenterCheckRequest::STATUS_PENDING;
        $orderCheckHistory->call_center_id = $model->callCenterRequest->call_center_id;

        if (!$orderCheckHistory->save()) {
            $success = false;
            $response['message'] = Yii::t('common', 'Не удалось создать заявку');
        }

        if ($success) {
            $transaction->commit();
            $response['status'] = 'success';
            $response['message'] = Yii::t('common', 'Операция выполнена успешно');
        } else {
            $transaction->rollBack();
        }

        return $response;
    }

    /**
     * Print only one order invoice,
     * but this invoice must be in generated list
     * @return array
     */
    public function actionPrintInvoice()
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
        }

        $OrderLogisticListInvoice = new OrderLogisticListInvoice();
        $OrderLogisticListInvoice->load(Yii::$app->request->post());

        if (!$OrderLogisticListInvoice->list_id) {
            $answer['message'] = Yii::t('common', 'Лист не найден.');
            return $answer;
        }

        $list = OrderLogisticList::findOne($OrderLogisticListInvoice->list_id);
        try {
            $builder = InvoiceBuilderFactory::build($list, $OrderLogisticListInvoice->template,
                $OrderLogisticListInvoice->format);
            $fileContents = $builder->printOrderInvoice($OrderLogisticListInvoice->order_id);
        } catch (\Exception $e) {
            $answer['message'] = $e->getMessage() . " " . print_r($e->getTrace());
            return $answer;
        }
        $answer['status'] = 'success';
        $download = base64_encode($fileContents);
        $answer['type'] = $OrderLogisticListInvoice->format;
        $answer['file'] = $download;
        return $answer;
    }


    /**
     * @return string
     */
    public function actionDeleteOrdersFromLists()
    {
        $error = false;
        $listIdx = [];

        $ids = Yii::$app->request->post('id');
        if (!empty($ids)) {
            $ordersQuery = Order::find()
                ->where(["IN", Order::tableName() . ".id", $ids])
                ->andWhere(["=", Order::tableName() . ".country_id", Yii::$app->user->getCountry()->id]);
            $orders = $ordersQuery->all();

            $listQuery = OrderLogisticList::find()
                ->where(["=", OrderLogisticList::tableName() . ".country_id", Yii::$app->user->getCountry()->id]);
            $lists = $listQuery->all();

            $transaction = Yii::$app->db->beginTransaction();

            foreach ($orders as $order) {
                if ($order->status_id == OrderStatus::STATUS_LOG_GENERATED) {       // Удалять можно только заказы со статусом = 9
                    foreach ($lists as $list) {
                        $arr = explode(',', $list->ids);
                        $idToDelete = null;
                        $counter = 0;
                        foreach ($arr as $item) {
                            if ($item == $order->id) {
                                $idToDelete = $item;
                                unset($arr[$counter]);
                                $counter--;
                                break;
                            }
                            $counter++;
                        }

                        if (!is_null($idToDelete)) {
                            // Удаляем номер заказа из листа
                            $list->ids = implode(',', $arr);
                            $list->orders_hash = md5($list->ids);
                            $list->user_id = Yii::$app->user->id;
                            $listIdx[] = $list->id;
                            $saved = $list->save();
                            if (!$saved) {
                                $error = true;
                                Yii::$app->notifier->addNotification(Yii::t('common',
                                    'Ошибка обновления листа №' . $list->id), Notifier::TYPE_DANGER);
                            }
                            // Обновляем заказ
                            if (!$error) {

                                $order->status_id = OrderStatus::STATUS_CC_APPROVED;            // Возвращаем 6-ой статус

                                $saved = $order->save();
                                if (!$saved) {
                                    $error = true;
                                    Yii::$app->notifier->addNotification(Yii::t('common',
                                        'Ошибка обновления заказа №' . $order->id), Notifier::TYPE_DANGER);
                                }
                            }
                        }

                        if ($error) {
                            break;
                        }
                    }
                }
                if ($error) {
                    break;
                }
            }

            if ($error) {
                $transaction->rollBack();
                Yii::$app->notifier->addNotification(Yii::t('common',
                    'Ошибка при удалении заказов из листов. Все изменения отменены'), Notifier::TYPE_DANGER);
            } else {
                $transaction->commit();
                $listStr = implode(',', $listIdx);
                Yii::$app->notifier->addNotification(Yii::t('common',
                    'Выбранные заказы успешно удалены из листов. Листы ' . $listStr . ' успешно перегенерированы. Не забудьте перегенерировать накладные и этикетки'),
                    Notifier::TYPE_SUCCESS);
            }
        } else {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Не было выбрано ни одного заказа'),
                Notifier::TYPE_DANGER);
        }

        return $this->redirect('/order/index/index');
    }


    /**
     * @return array
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionGetDeliveryEmail()
    {

        $response = [
            'status' => 'fail',
            'message' => Yii::t('common', 'Контакты не найдены')
        ];


        if (!isset(Yii::$app->user->identity->email) || empty(Yii::$app->user->identity->email)) {
            $response = [
                'status' => 'fail',
                'message' => Yii::t('common', 'Необходимо указать в настройках профиля адрес электронной почты')
            ];
            return $response;
        }

        $id = Yii::$app->request->post('id');
        if ($id) {
            $query = new Query();
            $deliveries = $query
                ->select([
                    'id' => 'DISTINCT(' . Delivery::tableName() . '.id)',
                    'name' => Delivery::tableName() . '.name'
                ])
                ->from(DeliveryRequest::tableName())
                ->leftJoin(Delivery::tableName(),
                    DeliveryRequest::tableName() . '.delivery_id=' . Delivery::tableName() . '.id')
                ->where(['order_id' => explode(',', $id)])
                ->orderBy([
                    Delivery::tableName() . '.name' => SORT_ASC
                ])
                ->all();

            $return = [];
            if ($deliveries) {
                foreach ($deliveries as $delivery) {
                    $deliveryContacts = DeliveryContacts::find()->select([
                        'id',
                        'email'
                    ])->byDeliveryId($delivery['id'])->asArray()->all();
                    $return[] = [
                        'id' => $delivery['id'],
                        'name' => $delivery['name'],
                        'contacts' => $deliveryContacts
                    ];
                }
            }

            if ($return) {
                $response = [
                    'status' => 'success',
                    'message' => '',
                    'result' => $return
                ];
            }
        }

        return $response;
    }

    /**
     * @return array
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionClarificationInDelivery()
    {
        $response = [
            'status' => 'fail',
            'message' => '',
        ];

        $id = Yii::$app->request->post('id');
        $subject = Yii::$app->request->post('subject');
        $body = Yii::$app->request->post('body');
        $emails = Yii::$app->request->post('emails');

        $statuses = [
            OrderStatus::STATUS_LOG_ACCEPTED,
            OrderStatus::STATUS_LOG_GENERATED,
            OrderStatus::STATUS_LOG_SET,
            OrderStatus::STATUS_LOG_PASTED,
            OrderStatus::STATUS_DELIVERY_ACCEPTED,
            OrderStatus::STATUS_DELIVERY_LEFT_WAREHOUSE,
            OrderStatus::STATUS_DELIVERY,
            OrderStatus::STATUS_DELIVERY_LEFT_OUR_WAREHOUSE,
            OrderStatus::STATUS_DELIVERY_REDELIVERY,
            OrderStatus::STATUS_DELIVERY_LEFT_THEIR_WAREHOUSE,
            OrderStatus::STATUS_DELIVERY_DELAYED,
            OrderStatus::STATUS_DELIVERY_PENDING,
            OrderStatus::STATUS_LOG_DEFERRED
        ];

        $orders = Order::find()
            ->joinWith('deliveryRequest')
            ->where(['order_id' => explode(',', $id)])
            ->all();

        $dataSave = [];
        foreach ($orders as $model) {

            if (!empty($model->created_at) && $model->created_at < (time() - 3600 * 24 * 7) && !Yii::$app->user->can("old_orders_access")) {
                continue;
            }

            if (!in_array($model->status_id, $statuses)) {
                continue;
            }

            if ($model->deliveryRequest) {
                $dataSave[$model->deliveryRequest->delivery_id]['order_id'][] = $model->id;
            }
        }

        $error = [];
        foreach ($dataSave as $deliveryId => $d) {
            $deliveryContacts = DeliveryContacts::find()->byDeliveryId($deliveryId)->andWhere(['id' => $emails])->all();
            $emailsArray = [];
            if ($deliveryContacts) {
                $emailsArray = ArrayHelper::getColumn($deliveryContacts, 'email');
            }
            if ($emailsArray && !empty($d['order_id'])) {
                $orderClarification = new OrderClarification();
                $orderClarification->user_id = Yii::$app->user->id;
                $orderClarification->country_id = Yii::$app->user->country->id;
                $orderClarification->delivery_id = $deliveryId;
                $orderClarification->subject = $subject;
                $orderClarification->message = $body;
                $orderClarification->order_ids = implode(',', $d['order_id']);
                $orderClarification->delivery_emails = implode(',', $emailsArray);

                $orderClarification->language = Yii::$app->user->language->locale;
                $orderClarification->date_format = Yii::$app->formatter->datetimeFormat;
                $orderClarification->type = Exporter::TYPE_CSV;

                $exporter = ExporterFactory::build(Exporter::TYPE_CSV, [
                    'showerColumns' => new ShowerColumns()
                ]);
                $orderClarification->columns = json_encode($exporter->getShowerColumns());

                if (!$orderClarification->save()) {
                    $error[] = $orderClarification->getFirstErrors();
                }
            }
        }
        if ($error) {
            $response = [
                'status' => 'fail',
                'message' => print_r($error, true)
            ];
        }
        if (!$error) {
            $response = [
                'status' => 'success',
                'message' => 'OK'
            ];
        }
        return $response;
    }


    /**
     * @return array
     * @throws ForbiddenHttpException
     * @throws \Exception
     */
    public function actionSecondDeliveryAttempt()
    {
        $response = [
            'status' => 'fail',
            'message' => '',
        ];

        $id = Yii::$app->request->post('id');
        $date = Yii::$app->request->post('date');

        $model = $this->findModel($id);

        if (!empty($model->created_at) && $model->created_at < (time() - 3600 * 24 * 7) && !Yii::$app->user->can("old_orders_access")) {
            throw new ForbiddenHttpException (Yii::t('common',
                "У вас недостаточно прав для просмотра таких старых заказов"));
        }

        if ($model->deliveryRequest) {

            if (!$model->deliveryRequest->second_sent_at) {

                $delivery = $this->findDelivery($model->deliveryRequest->delivery_id);

                if ($delivery) {

                    $transaction = Yii::$app->db->beginTransaction();
                    try {
                        $timeDiff = $model->delivery_time_to - $model->delivery_time_from;
                        $model->delivery_time_from = Yii::$app->formatter->asTimestamp($date);
                        $model->delivery_time_to = $model->delivery_time_from + $timeDiff;
                        if ($model->save(true, ['delivery_time_from', 'delivery_time_to'])) {
                            $model->deliveryRequest->second_sent_at = time();
                            $model->deliveryRequest->sent_clarification_at = null;
                            if ($model->deliveryRequest->save(true, ['second_sent_at', 'sent_clarification_at'])) {
                                $response['status'] = 'success';
                                $transaction->commit();
                            } else {
                                $response['message'] = array_values($model->deliveryRequest->getFirstErrors());
                                $transaction->rollBack();
                            }
                        } else {
                            $response['message'] = array_values($model->getFirstErrors());
                            $transaction->rollBack();
                        }
                    } catch (\Exception $e) {
                        $response['message'] = $e->getMessage();
                        $transaction->rollBack();
                    }

                } else {
                    $response['message'] = Yii::t('common', 'Отсутствует указанная служба доставки.');
                }
            } else {
                $response['message'] = Yii::t('common', 'Заказу уже делали 2 попытку доставки.');
            }
        } else {
            $response['message'] = Yii::t('common', 'Отсутствует доступная заявка в курьерскую службу.');
        }

        return $response;
    }

    /**
     * смена статуса заказа для лимитированного пользователя
     * @return array
     */
    public function actionChangeStatusByLimitedUser()
    {

        $response = [
            'status' => 'fail',
            'message' => '',
        ];

        $id = Yii::$app->request->post('id');
        $status = Yii::$app->request->post('status');
        $comment = 'status_was_changed_by_limited_user';

        $model = $this->findModel($id);

        $finalStatuses = OrderStatus::getAllFinalStatuses();
        $rejectedStatuses = ArrayHelper::map($finalStatuses, 'id', 'status_id');

        if (!array_key_exists($status, $rejectedStatuses) && $model->canChangeStatusTo($status)) {
            $model->status_id = $status;

            $model->route = Yii::$app->controller->route;
            $model->commentForLog = $comment;

            $transaction = Yii::$app->db->beginTransaction();
            try {
                if ($model->deliveryRequest && OrderStatus::isFinalDeliveryStatus($status) &&
                    !in_array($model->deliveryRequest->status, [
                        DeliveryRequest::STATUS_ERROR,
                        DeliveryRequest::STATUS_ERROR_AFTER_DONE
                    ])
                ) {
                    $model->deliveryRequest->status = DeliveryRequest::STATUS_DONE;
                    $model->deliveryRequest->route = Yii::$app->controller->route;
                    if (!$model->deliveryRequest->save(true, ['status'])) {
                        throw new \Exception(array_shift($model->deliveryRequest->getFirstErrors()));
                    }
                }

                if ($model->save(true, ['status_id'])) {
                    $response['status'] = 'success';
                } else {
                    throw new \Exception(array_shift($model->getFirstErrors()));
                }
                $transaction->commit();
            } catch (\Throwable $e) {
                $transaction->rollBack();
                $response['message'] = $e->getMessage();
            }
        } else {
            $response['message'] = Yii::t('common', 'Неразрешенное значение статуса.');
        }

        return $response;
    }

    /**
     * @return array
     */
    public function actionTransferToDistributor()
    {
        $id = Yii::$app->request->post('id');
        $country_id = Yii::$app->request->post('country_id');

        $model = $this->findModel($id);

        $country = Country::findOne(['id' => $country_id]);
        if (!$country) {
            return [
                'status' => 'fail',
                'message' => Yii::t('common', 'Страна #{id} не найден', ['id' => $country_id])
            ];
        }

        //заказ должен быть в статусах Выкуп или Деньги получены
        if (!in_array($model->status_id,
            [OrderStatus::STATUS_DELIVERY_BUYOUT, OrderStatus::STATUS_FINANCE_MONEY_RECEIVED])
        ) {
            return [
                'status' => 'fail',
                'message' => Yii::t('common', 'Ошибка передачи заказа дистрибьютору. Заказ должен быть в статусах "Выкуп" или "Деньги получены"')
            ];
        }

        //Страна заказа по чаркоду должна соответствовать стране дистрибьютора
        if ($model->country->char_code != $country->char_code) {
            return [
                'status' => 'fail',
                'message' => Yii::t('common', 'Ошибка передачи заказа дистрибьютору. Страна заказа не соответствует стране дистрибьютора')
            ];
        }

        //если у заказ источник уже 2wDistr
        if ($model->sourceModel->unique_system_name == Source::SOURCE_2WDISTR) {
            return [
                'status' => 'fail',
                'message' => Yii::t('common', 'Ошибка передачи заказа дистрибьютору. У заказа указан источник {source}', [
                    'source' => Source::SOURCE_2WDISTR
                ])
            ];
        }

        if ($model->duplicateOrder && $model->duplicateOrder->sourceModel->unique_system_name == Source::SOURCE_2WDISTR) {
            return [
                'status' => 'fail',
                'message' => Yii::t('common', 'Заказ уже был отправен дистрибьютору')
            ];
        }

        $result = [
            'status' => 'success'
        ];
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $url = 'http://2wtrade.com';
            $landing = Landing::find()->byUrl($url)->one();
            if (!$landing) {
                $landing = new Landing();
                $landing->url = $url;
                $landing->save();
            }

            $orderCopy = $model->duplicate(false, [
                'foreign_id' => (string)$model->id,
                'landing_id' => $landing->id,
                'country_id' => $country->id,
                'status_id' => OrderStatus::STATUS_SOURCE_SHORT_FORM,
                'source_id' => Source::find()->where(['unique_system_name' => Source::SOURCE_2WDISTR])->select('id')->scalar(),
            ], true);

            if ($orderCopy) {
                $transaction->commit();
            }
            $transaction->rollBack();
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $result = [
                'status' => 'fail',
                'message' => $e->getMessage()
            ];
        }
        return $result;
    }

    /**
     * @return array
     */
    public function actionSendPostSell()
    {
        $id = Yii::$app->request->post('id');

        $model = $this->findModel($id);

        $result = [
            'status' => 'success'
        ];

        if (!in_array(
            $model->status_id, [OrderStatus::STATUS_DELIVERY_BUYOUT, OrderStatus::STATUS_FINANCE_MONEY_RECEIVED])
        ) {
            return [
                'status' => 'fail',
                'message' => Yii::t('common', 'Ошибка передачи заказа {id} на постпродажу. Заказ должен быть в статусах "Выкуп" или "Деньги получены"', [
                    'id' => $id
                ])
            ];
        }

        if ($model->sourceModel->unique_system_name == Source::SOURCE_WTRADE_POST_SALE) {
            return [
                'status' => 'fail',
                'message' => Yii::t('common', 'Ошибка передачи заказа на постпродажу. У заказа указан источник {source}', [
                    'source' => Source::SOURCE_WTRADE_POST_SALE
                ])
            ];
        }

        if ($model->duplicateOrder && $model->duplicateOrder->sourceModel->unique_system_name == Source::SOURCE_WTRADE_POST_SALE) {
            return [
                'status' => 'fail',
                'message' => Yii::t('common', 'Заказ уже был отправен на постпродажу')
            ];
        }

        $transaction = Yii::$app->db->beginTransaction();
        try {
            $url = 'http://2wtrade.com';
            $landing = Landing::find()->byUrl($url)->one();
            if (!$landing) {
                $landing = new Landing();
                $landing->url = $url;
                $landing->save();
            }

            $orderCopy = $model->duplicate(false, [
                'foreign_id' => (string)$model->id,
                'landing_id' => $landing->id,
                'call_center_type' => Order::TYPE_CC_POST_SALE,
                'status_id' => OrderStatus::STATUS_SOURCE_SHORT_FORM,
                'source_id' => Source::find()->where(['unique_system_name' => Source::SOURCE_WTRADE_POST_SALE])->select('id')->scalar(),
            ], true);

            if ($orderCopy) {
                $transaction->commit();
                Yii::$app->queueCallCenterSend->push(new SendCallCenterRequestJob(['orderId' => $orderCopy->id]));
            }
            $transaction->rollBack();
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $result = [
                'status' => 'fail',
                'message' => $e->getMessage()
            ];
        }
        return $result;
    }

    /**
     * @return mixed
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     */
    public function actionSendInformation()
    {
        $id = Yii::$app->request->post('id');
        $comment = Yii::$app->request->post('comment');

        /** @var string $no_repeat */
        $no_repeat = Yii::$app->request->post('no_repeat');

        if ($no_repeat == 'true') {
            //если уже отправлялся
            if (CallCenterCheckRequest::find()->where([
                'order_id' => $id,
                'type' => CallCenterCheckRequest::TYPE_INFORMATION,
            ])
                ->exists()
            ) {
                throw new ForbiddenHttpException (Yii::t('common', 'Заказ уже ранее отправлялся в Information.'));
            }
        }

        $model = $this->findModel($id);

        $success = true;
        $transaction = Yii::$app->db->beginTransaction();

        $orderCheckHistory = new CallCenterCheckRequest;
        $orderCheckHistory->order_id = $model->id;
        $orderCheckHistory->user_id = Yii::$app->user->id;
        $orderCheckHistory->status_id = $model->status_id;
        $orderCheckHistory->type = CallCenterCheckRequest::TYPE_INFORMATION;
        $orderCheckHistory->status = CallCenterCheckRequest::STATUS_PENDING;
        $orderCheckHistory->call_center_id = $model->callCenterRequest->call_center_id;
        $orderCheckHistory->request_comment = $comment;

        if (!$orderCheckHistory->save()) {
            $success = false;
            $response['message'] = Yii::t('common', 'Не удалось создать заявку');
        }

        if ($success) {
            $transaction->commit();
            $response['status'] = 'success';
        } else {
            $transaction->rollBack();
        }

        return $response;
    }
}
