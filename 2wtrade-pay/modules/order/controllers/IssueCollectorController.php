<?php

namespace app\modules\order\controllers;

use app\components\web\Controller;
use app\modules\order\models\IssueCollector;
use app\modules\smsnotification\components\GoogleApi;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

/**
 * Class IssueCollectorController
 * @package app\modules\order\controllers
 */
class IssueCollectorController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        /** @var \yii\db\ActiveQuery */
        $query = IssueCollector::find();
        $query->where([IssueCollector::tableName() . '.country_id' => Yii::$app->user->country->id]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * @param null|integer $id
     * @return string|\yii\web\Response
     */
    public function actionEdit($id = null)
    {
        if (is_null($id)) {
            $model = new IssueCollector();
        } else {
            $model = $this->getModel($id);
        }

        $post = Yii::$app->request->post();
        if ($model->load($post)) {


            if ($model->isNewRecord) {
                $model->country_id = Yii::$app->user->country->id;
                $model->form_link = $this->createShortLink($model->jira_link);
            }
            else {
                if ($model->isAttributeChanged('jira_link')) {
                    $model->form_link = $this->createShortLink($model->jira_link);
                };
            }

            if ($model->save()) {
                Yii::$app->notifier->addNotification($model->isNewRecord ? Yii::t('common',
                    'Issue collector успешно добавлен.') : Yii::t('common', 'Issue collector успешно сохранен.'), 'success');

                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotificationsByModel($model);
            }
        }
        return $this->render('edit', [
            'model' => $model
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $model = $this->getModel($id);

        if ($model->delete()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'IssueCollector успешно удален.'), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($model);
        }

        return $this->redirect(Url::toRoute(['index']));
    }

    /**
     * @param integer $id
     * @return IssueCollector
     * @throws NotFoundHttpException
     */
    private function getModel($id)
    {
        $model = IssueCollector::findOne($id);

        if (!$model) {
            throw new NotFoundHttpException(Yii::t('common', 'Issue Collector не найден.'));
        }

        return $model;
    }

    /**-
     * @param $jira_link
     * @return null
     */
    private function createShortLink($jira_link)
    {
        $part = 'https://2wtrade-tasks.atlassian.net/';
        $jira_link = str_replace($part, '', $jira_link);
        $jira_link = urlencode($jira_link);
        $longUrl = "http://2wstore.com/?jira_link=$jira_link#jira_problem";
        $response = GoogleApi::getShortUrl(['longUrl' => $longUrl]);
        return $response['answer']->id ?? null;
    }
}
