<?php

namespace app\modules\order\controllers;

use app\components\filters\AjaxFilter;
use app\components\Notifier;
use app\components\web\Controller;
use app\models\logs\TableLog;
use app\models\Product;
use app\models\User;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\delivery\models\Delivery;
use app\modules\media\components\Image;
use app\modules\media\exceptions\ImageException;
use app\modules\order\components\ExcelBuilderFactory;
use app\modules\order\components\InvoiceBuilder;
use app\modules\order\models\Order;
use app\modules\order\models\OrderFinancePretension;
use app\modules\order\models\OrderLogisticList;
use app\modules\order\models\OrderLogisticListBarcode;
use app\modules\order\models\OrderLogisticListExcel;
use app\modules\order\models\OrderLogisticListInvoice;
use app\modules\order\models\OrderLogisticListInvoiceArchive;
use app\modules\order\models\OrderLogisticListLog;
use app\modules\order\models\OrderLogisticListRequest;
use app\modules\order\models\OrderStatus;
use app\modules\order\models\search\OrderLogisticListSearch;
use Yii;
use yii\base\Exception;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

/**
 * Class ListController
 * @package app\modules\order\controllers
 */
class ListController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => [
                    'build-invoice',
                    'build-excel',
                    'build-label',
                    'change-status',
                    'send',
                ],
            ]
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new OrderLogisticListSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render(
            'index',
            [
                'modelSearch' => $modelSearch,
                'dataProvider' => $dataProvider,
            ]
        );
    }

    /**
     * @param string $name
     * @throws ImageException
     * @throws NotFoundHttpException
     */
    public function actionDownloadTicket($name)
    {
        $filePath = OrderLogisticList::getDownloadPath(
                $name,
                OrderLogisticList::DOWNLOAD_TYPE_TICKETS
            ) . DIRECTORY_SEPARATOR . $name;

        if (file_exists($filePath)) {
            (new Image())->load($filePath)->output();
        } else {
            throw new NotFoundHttpException(Yii::t('common', 'Изображение не найдено.'));
        }
    }


    /**
     * @param integer $id
     * @throws ImageException
     * @throws NotFoundHttpException
     */
    public function actionDownloadBarcodes($id)
    {
        $list = $this->findModel($id);

        if ($list->barcodes) {
            $filePath = OrderLogisticListBarcode::getPathDir('grouped') . $list->barcodes;
            if (file_exists($filePath)) {

                Yii::$app->response->sendFile($filePath, $list->barcodes);
                Yii::$app->end();

            } else {
                throw new NotFoundHttpException(Yii::t('common', 'Файл не найден.'));
            }
        } else {
            throw new NotFoundHttpException(Yii::t('common', 'Файл не создан.'));
        }
    }

    /**
     * @param $id
     * @return Response
     * @throws ForbiddenHttpException
     * @throws Exception
     */
    public function actionDownloadLabel($id)
    {
        $list = $this->findModel($id);

        if (!empty($list->created_at) && $list->created_at < (time() - 3600 * 24 * 7) && !Yii::$app->user->can("old_orders_access")) {
            throw new ForbiddenHttpException (Yii::t('common',
                "У вас недостаточно прав для просмотра таких старых листов"));
        }


        try {
            $fileName = $list->delivery->apiTransmitter->generateLabel($list);
            $filePath = OrderLogisticList::getDownloadPath(
                    $fileName,
                    OrderLogisticList::DOWNLOAD_TYPE_LABELS,
                    false
                ) . DIRECTORY_SEPARATOR . $fileName;

            $error = null;
            if (!$fileName || !file_exists($filePath)) {
                $error = Yii::t('common', 'Не удалось сгенерировать файл');
            } elseif (!is_readable($filePath)) {
                $error = Yii::t('common', 'Файл недоступен для скачивания');
            }
            if ($error) {
                throw new Exception($error);
            }

            if ($list->delivery->workflow) {
                $statuses = $list->delivery->workflow->getNextStatuses(OrderStatus::STATUS_LOG_GENERATED);
            } else {
                $statuses = $list->country->getNextOrderStatuses(OrderStatus::STATUS_LOG_GENERATED);
            }
            if (($list->delivery->auto_sending || $list->delivery->our_api) && in_array(OrderStatus::STATUS_DELIVERY_PENDING,
                    $statuses)
            ) {
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    foreach ($list->orders as $order) {
                        if (!$order->deliveryRequest) {
                            throw new Exception(Yii::t('common', "Отсутствует заявка для заказа #{order_id}.",
                                ['order_id' => $order->id]));
                        }
                        $skipDeliveryRequestStatuses = [
                            DeliveryRequest::STATUS_PENDING,
                            DeliveryRequest::STATUS_IN_PROGRESS,
                            DeliveryRequest::STATUS_DONE,
                        ];
                        if (in_array($order->deliveryRequest->status, $skipDeliveryRequestStatuses)) {
                            continue;
                        }

                        if ($order->deliveryRequest->status != DeliveryRequest::STATUS_IN_LIST && $order->deliveryRequest->status != DeliveryRequest::STATUS_ERROR) {
                            throw new Exception(Yii::t('common', "Неправильный статус заявки для заказа #{order_id}.",
                                ['order_id' => $order->id]));
                        }


                        $order->status_id = OrderStatus::STATUS_DELIVERY_PENDING;
                        $setDeliveryId = false;
                        if ($order->deliveryRequest->delivery) {
                            $contract = $order->deliveryRequest->delivery->getActiveContract();
                            if ($contract) {
                                if (!empty($contract->max_delivery_period) && $order->delivery_time_from > strtotime("+{$contract->max_delivery_period}days")) {
                                    $order->pending_delivery_id = $order->deliveryRequest->delivery_id;
                                    $setDeliveryId = true;
                                }
                            }
                        }

                        if (!$setDeliveryId) {
                            $order->deliveryRequest->status = DeliveryRequest::STATUS_PENDING;
                        }

                        $order->route = Yii::$app->controller->route;

                        if (!$order->save()) {
                            throw new Exception(Yii::t('common', 'Не удалось сменить статус заказу #{order_id}.',
                                ['order_id' => $order->id]));
                        }

                        if (!$order->deliveryRequest->save()) {
                            throw new Exception(Yii::t('common', 'Не удалось сменить статус заявке #{order_id}.',
                                ['order_id' => $order->id]));
                        }
                    }
                    $transaction->commit();
                    Yii::$app->notifier->addNotification(Yii::t('common',
                        "Заказы успешно поставлены в очередь на отправку в КС."), Notifier::TYPE_SUCCESS);
                } catch (Exception $e) {
                    $transaction->rollBack();
                    Yii::$app->notifier->addNotification($e->getMessage(), Notifier::TYPE_DANGER);
                }
            }

            Yii::$app->response->sendFile($filePath, $fileName);

            Yii::$app->end();
        } catch (Exception $e) {
            Yii::$app->notifier->addNotification($e->getMessage(), Notifier::TYPE_DANGER);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return Response
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionDownload($id)
    {
        $list = $this->findModel($id);

        if (!empty($list->created_at) && $list->created_at < (time() - 3600 * 24 * 7) && !Yii::$app->user->can("old_orders_access")) {
            throw new ForbiddenHttpException (Yii::t('common',
                "У вас недостаточно прав для просмотра таких старых листов"));
        }

        try {

            if ($list->status == OrderLogisticList::STATUS_PENDING) {
                $list->status = OrderLogisticList::STATUS_DOWNLOADED;
                $list->save(true, ['status']);
            }

            $excelList = $list->delivery->apiTransmitter->downloadList($list);

            $fileName = $excelList->system_file_name;
            $filePath = OrderLogisticList::getDownloadPath(
                    $fileName,
                    OrderLogisticList::DOWNLOAD_TYPE_EXCELS,
                    false
                ) . DIRECTORY_SEPARATOR . $fileName;

            Yii::$app->response->sendFile($filePath, $excelList->delivery_file_name);

            Yii::$app->end();
        } catch (Exception $e) {
            Yii::$app->notifier->addNotification($e->getMessage(), Notifier::TYPE_DANGER);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionSend()
    {
        $answer = [
            'status' => 'fail',
            'message' => '',
        ];

        if ($list = $this->findModel(Yii::$app->request->post('list_id'))) {
            try {
                if (!empty($list->created_at) && $list->created_at < (time() - 3600 * 24 * 7) && !Yii::$app->user->can("old_orders_access")) {
                    throw new ForbiddenHttpException(Yii::t('common', "У вас недостаточно прав для просмотра таких старых листов"));
                }

                $emailList = $list->getEmailList();
                if (Yii::$app->request->post('sending')) {
                    $offset = Yii::$app->request->post('emailsOffset');
                    if ($offset < count($emailList) && ($emailTo = $emailList[$offset]) && !User::find()
                            ->where(['email' => $emailTo, 'status' => [User::STATUS_BLOCKED, User::STATUS_DELETED, User::STATUS_BLOCKED | User::STATUS_DELETED]])
                            ->exists()) {
                        $fileName = Yii::$app->request->post('filename');
                        if (!$fileName) {
                            $excelList = $list->actualListExcel;
                        } else {
                            /** @var OrderLogisticListExcel $excel */
                            $excelList = OrderLogisticListExcel::find()->byFilename($fileName)->one();
                        }
                        if (!$excelList) {
                            throw new NotFoundHttpException(Yii::t('common', 'Excel-лист не найден.'));
                        }

                        $emailTo = $emailList[$offset];
                        $orderLogisticListRequest = OrderLogisticListRequest::find()
                            ->byList($list->id)
                            ->byEmail($emailTo)
                            ->one();
                        if (!$orderLogisticListRequest) {
                            $orderLogisticListRequest = new OrderLogisticListRequest();
                            $orderLogisticListRequest->list_id = $list->id;
                            $orderLogisticListRequest->email_to = $emailTo;
                        }

                        try {
                            $message = $orderLogisticListRequest->buildEmailMessageForLogisticList($excelList);
                            if (!$message->send()) {
                                $orderLogisticListRequest->status = OrderLogisticListRequest::STATUS_ERROR;
                            } else {
                                $orderLogisticListRequest->status = OrderLogisticListRequest::STATUS_SENT;
                            }
                            if (!$orderLogisticListRequest->save()) {
                                throw new \Exception($orderLogisticListRequest->getFirstErrorAsString());
                            }
                        } catch (\Throwable $e) {
                            throw new \Exception("{$emailTo}: {$e->getMessage()}");
                        }

                        if ($orderLogisticListRequest->status == OrderLogisticListRequest::STATUS_ERROR) {
                            throw new \Exception(Yii::t('common', 'Не удалось отправить сообщение на адрес {email}.', ['email' => $emailTo]));
                        }

                        if ($offset < count($emailList) - 1) {
                            sleep(1);
                        } else {
                            $excelList->sent_at = time();
                            $excelList->waiting_for_send = 0;
                            $excelList->save(false, ['sent_at', 'waiting_for_send']);

                            if ($list->status != OrderLogisticList::STATUS_SENT && $list->status != OrderLogisticList::STATUS_RECEIVED) {

                                if ($list->status != OrderLogisticList::STATUS_BARCODING) {
                                    $list->status = OrderLogisticList::STATUS_SENT;
                                    $list->save(false, ['status']);
                                }

                                if (!$list->isPretensionType()) {
                                    foreach ($list->orders as $order) {
                                        if ($order->canChangeStatusTo(OrderStatus::STATUS_LOG_SENT)) {
                                            $order->status_id = OrderStatus::STATUS_LOG_SENT;
                                            $order->save(false, ['status_id']);
                                        }
                                    }
                                }

                                if ($list->isPretensionType()) {
                                    OrderFinancePretension::updateAll([
                                        'status' => OrderFinancePretension::PRETENSION_STATUS_SEND,
                                        'updated_at' => time()
                                    ], [
                                        'and',
                                        ['=', 'status', OrderFinancePretension::PRETENSION_STATUS_IN_PROGRESS],
                                        ['in', 'order_id', explode(',', $list->ids)]
                                    ]);
                                }
                            }
                        }
                    }
                    $answer['emailsCount'] = count($emailList);
                    $answer['status'] = 'success';
                } else {
                    if ($list->delivery->hasApiGenerateList()) {
                        $excelList = $list->delivery->apiTransmitter->downloadList($list);
                        $fileName = $excelList->system_file_name;
                        $answer['message'] = $fileName;
                        $answer['status'] = 'success';
                    } else {
                        $columns = Yii::$app->request->post('columns');
                        $format = Yii::$app->request->post('format');

                        $builder = ExcelBuilderFactory::build($list, $format, $columns);

                        $result = $builder->generate();

                        $answer['status'] = $result['status'];
                        $answer['message'] = $result['message'];
                    }
                    if ($list->delivery->auto_generating_invoice) {
                        $result = $list->buildInvoices();
                        if (!$result['status'] == InvoiceBuilder::STATUS_SUCCESS) {
                            throw new \Exception($result['message']);
                        }
                    }
                }
            } catch (\Throwable $e) {
                $answer['status'] = 'fail';
                $answer['message'] = $e->getMessage();
            }
        } else {
            $answer['message'] = Yii::t('common', 'Лист не найден.');
        }

        return $answer;
    }

    /**
     * @param string $name
     * @throws ImageException
     * @throws NotFoundHttpException
     */
    public function actionDownloadInvoice($name)
    {
        $filePath = OrderLogisticList::getDownloadPath(
                $name,
                OrderLogisticList::DOWNLOAD_TYPE_INVOICES
            ) . DIRECTORY_SEPARATOR . $name;

        if (file_exists($filePath)) {
            $archive = OrderLogisticListInvoiceArchive::find()->byArchive($name)->one();

            if ($archive) {
                Yii::$app->response->sendFile($filePath, $archive->archive_local);
            } else {
                throw new NotFoundHttpException(Yii::t('common', 'Список накладных не найден.'));
            }
        } else {
            throw new NotFoundHttpException(Yii::t('common', 'Список накладных не найден.'));
        }
    }

    /**
     * @return array
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionBuildExcel()
    {
        $answer = [
            'status' => 'fail',
            'message' => '',
        ];

        if ($list = $this->findModel(Yii::$app->request->post('list_id'))) {

            if (!empty($list->created_at) && $list->created_at < (time() - 3600 * 24 * 7) && !Yii::$app->user->can("old_orders_access")) {
                throw new ForbiddenHttpException (Yii::t('common',
                    "У вас недостаточно прав для просмотра таких старых листов"));
            }

            $columns = Yii::$app->request->post('columns');
            $format = Yii::$app->request->post('format');

            $builder = ExcelBuilderFactory::build($list, $format, $columns);

            $result = $builder->generate();

            $answer['status'] = $result['status'];
            $answer['message'] = $result['message'];

            if ($answer['status'] == 'success') {
                $answer['message'] = Url::toRoute(['download-excel']) . '/' . $answer['message'];
            }
        } else {
            $answer['message'] = Yii::t('common', 'Лист не найден.');
        }

        return $answer;
    }

    /**
     * @param string $name
     * @throws NotFoundHttpException
     */
    public function actionDownloadExcel($name)
    {

        $filePath = OrderLogisticList::getDownloadPath(
                $name,
                OrderLogisticList::DOWNLOAD_TYPE_EXCELS
            ) . DIRECTORY_SEPARATOR . $name;

        if (file_exists($filePath)) {
            /** @var OrderLogisticListExcel $excel */
            $excel = OrderLogisticListExcel::find()->byFilename($name)->one();

            if ($excel) {
                Yii::$app->response->sendFile($filePath, $excel->delivery_file_name);
            } else {
                throw new NotFoundHttpException(Yii::t('common', 'Excel-лист не найден.'));
            }
        } else {
            throw new NotFoundHttpException(Yii::t('common', 'Excel-лист не найден.'));
        }
    }

    /**
     * @return array
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionBuildInvoice()
    {
        $answer = [
            'status' => 'fail',
            'message' => '',
        ];


        if ($list = $this->findModel(Yii::$app->request->post('list_id'))) {

            if (!empty($list->created_at) && $list->created_at < (time() - 3600 * 24 * 7) && !Yii::$app->user->can("old_orders_access")) {
                throw new ForbiddenHttpException (Yii::t('common',
                    "У вас недостаточно прав для просмотра таких старых листов"));
            }

            if ($list->getCountOrders()) {
                if ($post = Yii::$app->request->post('OrderLogisticListInvoice')) {
                    $template = isset($post['template']) ? $post['template'] : OrderLogisticListInvoice::TEMPLATE_TWO;
                    $format = isset($post['format']) ? $post['format'] : OrderLogisticListInvoice::FORMAT_DOCS;

                    try {
                        $result = $list->buildInvoices($template, $format);
                    } catch (\Exception $e) {
                        $answer['message'] = $e->getMessage() . " " . $e->getTraceAsString();
                        return $answer;
                    }

                    $answer['status'] = $result['status'];
                    $answer['message'] = $result['message'];

                    if ($answer['status'] == 'success') {
                        $answer['message'] = Url::toRoute(['download-invoice']) . '/' . $answer['message'];
                    }
                } else {
                    $answer['message'] = Yii::t('common', 'Необходимо указать опции генерации.');
                }
            } else {
                $answer['message'] = Yii::t('common', 'Отсутствуют заказы в листе.');
            }
        } else {
            $answer['message'] = Yii::t('common', 'Лист не найден.');
        }

        return $answer;
    }

    /**
     * @param integer $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionDetails($id)
    {
        $list = $this->findModel($id);

        $dataProvider = new ActiveDataProvider();
        $dataProvider->models = $list->orders;

        return $this->render(
            'details',
            [
                'list' => $list,
                'dataProvider' => $dataProvider,
                'statuses' => OrderStatus::find()->collection(),
                'products' => Product::find()->collection(),
            ]
        );
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionDeleteOrder()
    {
        $listId = Yii::$app->request->get('list');
        $orderId = Yii::$app->request->get('order');

        $list = $this->findModel($listId);
        $order = $this->findModelOrder($orderId);

        if (!empty($list->actualListExcel->sent_at)) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Лист уже был отправлен.'), Notifier::TYPE_DANGER);

            return $this->redirect(Url::toRoute(['details', 'id' => $listId]));
        }

        $ids = explode(',', $list->ids);
        $index = array_search($orderId, $ids);

        if ($index === false) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Заказ в списке не найден.'));
        } else {
            $transaction = Yii::$app->db->beginTransaction();

            $commit = false;
            if ($list->type == OrderLogisticList::TYPE_NORMAL) {
                $order->status_id = OrderStatus::STATUS_CC_APPROVED;
            }

            if (!$list->type == OrderLogisticList::TYPE_NORMAL || ($list->type == OrderLogisticList::TYPE_NORMAL && $order->save(true, ['status_id']))) {
                unset($ids[$index]);
                $list->ids = implode(',', $ids);
                $list->label = null;

                if ($list->save(true, ['ids', 'orders_hash', 'label'])) {

                    if ($list->type == OrderLogisticList::TYPE_NORMAL && $deliveryRequestId = DeliveryRequest::findOne(['order_id' => $orderId])) {
                        DeliveryRequest::deleteAll(['order_id' => $orderId]);
                    }
                    OrderLogisticListInvoice::deleteAll(['order_id' => $orderId]);
                    OrderLogisticListInvoiceArchive::deleteAll(['list_id' => $listId]);
                    OrderLogisticListExcel::deleteAll(['list_id' => $listId]);


                    $commit = true;
                    Yii::$app->notifier->addNotification(
                        Yii::t('common', 'Заказ успешно удален из списка.'),
                        'success'
                    );
                } else {
                    Yii::$app->notifier->addNotificationsByModel($list);
                }
            } else {
                Yii::$app->notifier->addNotificationsByModel($order);
            }

            if ($commit) {
                $transaction->commit();
            } else {
                $transaction->rollBack();
            }
        }

        return $this->redirect(Url::toRoute(['details', 'id' => $listId]));
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function actionDeleteList()
    {
        $listId = Yii::$app->request->get('list');
        $list = $this->findModel($listId);

        $transaction = Yii::$app->db->beginTransaction();
        $error = false;
        try {
            if ($list->delete() && $list->status != OrderLogisticList::STATUS_BARCODING) {
                $ids = explode(',', $list->ids);
                if (!$list->isPretensionType()) {
                    $orders = Order::find()->where(['in', 'id', $ids])->all();
                    foreach ($orders as $order) {
                        $order->setScenario(Order::SCENARIO_PREVIOUS);
                        $order->status_id = OrderStatus::STATUS_CC_APPROVED;
                        if ($order->save(true, ['status_id'])) {
                            if ($order->getDeliveryRequest()->exists() && !$order->deliveryRequest->delete()) {
                                throw new Exception(Yii::t('common', 'Не удалось удалить заявку заказа.'));
                            }
                        } else {
                            $error = $order;
                            break;
                        }
                    }
                }
                if (!$error) {
                    Yii::$app->notifier->addNotification(
                        Yii::t('common', 'Лист успешно удален из списка.') .
                        (!$list->isPretensionType() ? Yii::t('common', 'Статусы заказов обновлены.') : '')
                        ,
                        'success'
                    );
                    $transaction->commit();
                } else {
                    Yii::$app->notifier->addNotification('Сбой обновления статуса в заказе № ' . $error->id . ' (' . $error->getFirstError('status_id') . ')',
                        NOTIFIER::TYPE_DANGER);
                    $transaction->rollBack();
                }
            } else {
                Yii::$app->notifier->addNotificationsByModel($list);
                $transaction->rollBack();
            }
        } catch (Exception $e) {
            $transaction->rollBack();
            Yii::$app->notifier->addNotification($e->getMessage());
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return Response
     */
    public function actionSendOrdersToDelivery($id)
    {
        $list = $this->findModel($id);
        if (!is_null($list)) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                foreach ($list->orders as $order) {
                    if (!$order->deliveryRequest) {
                        throw new Exception(Yii::t('common', "Отсутствует заявка для заказа #{order_id}.",
                            ['order_id' => $order->id]));
                    }
                    $skipDeliveryRequestStatuses = [
                        DeliveryRequest::STATUS_PENDING,
                        DeliveryRequest::STATUS_IN_PROGRESS,
                        DeliveryRequest::STATUS_DONE,
                    ];
                    if (in_array($order->deliveryRequest->status, $skipDeliveryRequestStatuses)) {
                        continue;
                    }

                    if ($order->deliveryRequest->status != DeliveryRequest::STATUS_IN_LIST && $order->deliveryRequest->status != DeliveryRequest::STATUS_ERROR) {
                        throw new Exception(Yii::t('common', "Неправильный статус заявки для заказ #{order_id}.",
                            ['order_id' => $order->id]));
                    }

                    $order->status_id = OrderStatus::STATUS_DELIVERY_PENDING;
                    $setDeliveryId = false;
                    if ($order->deliveryRequest->delivery) {
                        $contract = $order->deliveryRequest->delivery->getActiveContract();
                        if ($contract) {
                            if (!empty($contract->max_delivery_period) && $order->delivery_time_from > strtotime("+{$contract->max_delivery_period}days")) {
                                $order->pending_delivery_id = $order->deliveryRequest->delivery_id;
                                $setDeliveryId = true;
                            }
                        }
                    }

                    if (!$setDeliveryId) {
                        $order->deliveryRequest->status = DeliveryRequest::STATUS_PENDING;
                    }

                    $order->route = Yii::$app->controller->route;

                    if (!$order->save()) {
                        throw new Exception(Yii::t('common', 'Не удалось сменить статус заказу #{order_id}.',
                            ['order_id' => $order->id]));
                    }

                    if (!$order->deliveryRequest->save()) {
                        throw new Exception(Yii::t('common', 'Не удалось сменить статус заявке #{order_id}.',
                            ['order_id' => $order->id]));
                    }
                }
                $transaction->commit();
                Yii::$app->notifier->addNotification(Yii::t('common',
                    "Заказы успешно поставлены в очередь на отправку в КС."), Notifier::TYPE_SUCCESS);
            } catch (Exception $e) {
                $transaction->rollBack();
                Yii::$app->notifier->addNotification($e->getMessage(), Notifier::TYPE_DANGER);
            }

        } else {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Лист не найден.'));
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param integer $id
     * @return OrderLogisticList
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {

        $condition = [OrderLogisticList::tableName() . '.id' => $id];

        if (Yii::$app->user->getIsImplant()) {
            $delivery_ids = Yii::$app->user->getDeliveriesIds();
            $condition[Delivery::tableName() . ".id"] = $delivery_ids;
        }

        $model = OrderLogisticList::find()
            ->joinWith(
                [
                    'country',
                    'delivery',
                    'actualListExcel',
                ]
            )
            ->where($condition)
            ->byCountryId(Yii::$app->user->country->id)
            ->one();

        if ($model == null) {
            throw new NotFoundHttpException(Yii::t('common', 'Лист не найден.'));
        }

        return $model;
    }

    /**
     * @param integer $id
     * @return Order
     * @throws NotFoundHttpException
     */
    protected function findModelOrder($id)
    {
        $model = Order::find()
            ->joinWith('country')
            ->where([Order::tableName() . '.id' => $id])
            ->byCountryId(Yii::$app->user->country->id)
            ->one();

        if ($model == null) {
            throw new NotFoundHttpException(Yii::t('common', 'Заказ не найден.'));
        }

        return $model;
    }

    /**
     * @param integer $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionLogs(int $id)
    {
        $model = $this->findModel($id);

        $change = TableLog::find()->byTable(OrderLogisticList::clearTableName())->byID($id)->allSorted();
        $barcode = TableLog::find()
            ->byTable(OrderLogisticListBarcode::clearTableName())
            ->byLink('list_id', $id)
            ->allSorted();
        $excel = TableLog::find()
            ->byTable(OrderLogisticListExcel::clearTableName())
            ->byLink('list_id', $id)
            ->allSorted();
        $invoice = TableLog::find()
            ->byTable(OrderLogisticListInvoice::clearTableName())
            ->byLink('list_id', $id)
            ->allSorted();
        $invoiceArchive = TableLog::find()
            ->byTable(OrderLogisticListInvoiceArchive::clearTableName())
            ->byLink('list_id', $id)
            ->allSorted();
        $request = TableLog::find()
            ->byTable(OrderLogisticListRequest::clearTableName())
            ->byLink('list_id', $id)
            ->allSorted();

        return $this->render('logs', [
            'model' => $model,
            'change' => $change,
            'barcode' => $barcode,
            'excel' => $excel,
            'invoice' => $invoice,
            'invoiceArchive' => $invoiceArchive,
            'request' => $request,
        ]);
    }

    /**
     * @param $id
     * @return OrderLogisticListLog[]
     */
    protected function findModelLogs($id)
    {
        return OrderLogisticListLog::find()
            ->joinWith('user')
            ->where(['list_id' => $id])
            ->orderBy([
                'id' => SORT_DESC
            ])
            ->all();
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionConfirmSent()
    {
        $listId = Yii::$app->request->get('list');
        $list = $this->findModel($listId);
        try {
            $list->confirmSend();
            Yii::$app->notifier->addNotification(
                Yii::t('common', 'Статус листа обновлен. Статусы заказов обновлены.'),
                'success'
            );
        } catch (\Throwable $e) {
            Yii::$app->notifier->addNotification($e->getMessage());
        }
        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionConfirmReceived()
    {
        $listId = Yii::$app->request->get('list');
        $list = $this->findModel($listId);

        $transaction = Yii::$app->db->beginTransaction();
        $error = false;
        try {
            if ($list->status == OrderLogisticList::STATUS_SENT) {
                $list->status = OrderLogisticList::STATUS_RECEIVED;
                if ($list->save(true, ['status'])) {
                    $ids = explode(',', $list->ids);
                    $orders = Order::find()->where(['in', 'id', $ids])->all();
                    foreach ($orders as $order) {
                        if ($order->canChangeStatusTo(OrderStatus::STATUS_LOG_RECEIVED)) {
                            $order->status_id = OrderStatus::STATUS_LOG_RECEIVED;
                            if (!$order->save(true, ['status_id'])) {
                                $error = $order;
                                break;
                            }
                        }
                    }
                    if (!$error) {
                        Yii::$app->notifier->addNotification(
                            Yii::t('common', 'Статус листа обновлен. Статусы заказов обновлены.'),
                            'success'
                        );
                        $transaction->commit();
                    } else {
                        Yii::$app->notifier->addNotification('Сбой обновления статуса в заказе № ' . $error->id . ' (' . $error->getFirstError('status_id') . ')',
                            NOTIFIER::TYPE_DANGER);
                        $transaction->rollBack();
                    }
                } else {
                    Yii::$app->notifier->addNotificationsByModel($list);
                    $transaction->rollBack();
                }
            }
        } catch (Exception $e) {
            $transaction->rollBack();
            Yii::$app->notifier->addNotification($e->getMessage());
        }

        return $this->redirect(Url::toRoute('index'));
    }

}
