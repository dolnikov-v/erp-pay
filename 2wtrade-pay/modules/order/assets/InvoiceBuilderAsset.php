<?php
namespace app\modules\order\assets;

use yii\web\AssetBundle;

/**
 * Class InvoiceBuilderAsset
 * @package app\modules\order\assets
 */
class InvoiceBuilderAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/order/invoice-builder';

    public $css = [
        'invoice-builder.css',
    ];

    public $js = [
        'invoice-builder.js',
    ];

    public $depends = [
        'app\modules\order\assets\IndexAsset',
    ];
}
