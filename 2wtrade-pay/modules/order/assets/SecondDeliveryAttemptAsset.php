<?php
namespace app\modules\order\assets;

use yii\web\AssetBundle;

/**
 * Class ClarificationInDeliveryAsset
 * @package app\modules\order\assets
 */
class SecondDeliveryAttemptAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/order/second-delivery-attempt';

    public $js = [
        'second-delivery-attempt.js',
    ];

    public $depends = [
        'app\modules\order\assets\IndexAsset',
    ];
}
