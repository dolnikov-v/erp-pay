<?php
namespace app\modules\order\assets;

use yii\web\AssetBundle;

/**
 * Class ListAsset
 * @package app\modules\order\assets
 */
class MarketingCampaignAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/order/marketing-campaign';

    public $js = [
        'campaign.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
