<?php
namespace app\modules\order\assets;

use yii\web\AssetBundle;

/**
 * Class SendAnalysisTrashAsset
 * @package app\modules\order\assets
 */
class SendAnalysisTrashAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/order/send-analysis-trash';

    public $js = [
        'send-analysis-trash.js',
    ];

    public $depends = [
        'app\modules\order\assets\IndexAsset',
    ];
}