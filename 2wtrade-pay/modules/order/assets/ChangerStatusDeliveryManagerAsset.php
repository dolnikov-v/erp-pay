<?php
namespace app\modules\order\assets;

use yii\web\AssetBundle;

/**
 * Class ChangerStatusDeliveryManagerAsset
 * @package app\modules\order\assets
 */
class ChangerStatusDeliveryManagerAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/order/changer-status-delivery-manager';

    public $css = [
        'changer-status-delivery-manager.css',
    ];

    public $js = [
        'changer-status-delivery-manager.js',
    ];

    public $depends = [
        'app\modules\order\assets\IndexAsset',
    ];
}
