<?php

namespace app\modules\order\assets;


use yii\web\AssetBundle;

/**
 * Class CallCenterRequestStatusChanger
 * @package app\modules\order\assets
 */
class CallCenterRequestStatusChangerAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/order/call-center-request-status-changer';

    public $css = [
    ];

    public $js = [
        'call-center-request-status-changer.js',
    ];

    public $depends = [
        'app\modules\order\assets\IndexAsset',
    ];
}
