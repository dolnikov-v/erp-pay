<?php
namespace app\modules\order\assets;

use yii\web\AssetBundle;

/**
 * Class ResenderInCallCenterStillWaitingAsset
 * @package app\modules\order\assets
 */
class ResenderInCallCenterStillWaitingAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/order/resender-in-call-center-still-waiting';

    public $js = [
        'resender-in-call-center-still-waiting.js',
    ];

    public $depends = [
        'app\modules\order\assets\IndexAsset',
    ];
}