<?php
namespace app\modules\order\assets;

use yii\web\AssetBundle;

/**
 * Class GeneratorMarketingListAsset
 * @package app\modules\order\assets
 */
class GeneratorMarketingListAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/order/generator-marketing-list';

    public $css = [
        'generator-marketing-list.css',
    ];

    public $js = [
        'generator-marketing-list.js',
    ];

    public $depends = [
        'app\modules\order\assets\IndexAsset',
    ];
}
