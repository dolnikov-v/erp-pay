<?php
namespace app\modules\order\assets;

use yii\web\AssetBundle;

/**
 * Class SendCheckOrderAsset
 * @package app\modules\order\assets
 */
class SendCheckOrderAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/order/send-check-order';

    public $css = [
        'send-check-order.css',
    ];

    public $js = [
        'send-check-order.js',
    ];

    public $depends = [
        'app\modules\order\assets\IndexAsset',
    ];
}