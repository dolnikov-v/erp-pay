<?php
namespace app\modules\order\assets;

use yii\web\AssetBundle;

/**
 * Class ChangeStatusByLimitedUserAsset
 * @package app\modules\order\assets
 */
class ChangeStatusByLimitedUserAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/order/change-status-by-limited-user';

    public $css = [
        'change-status-by-limited-user.css',
    ];

    public $js = [
        'change-status-by-limited-user.js',
    ];

    public $depends = [
        'app\modules\order\assets\IndexAsset',
    ];
}
