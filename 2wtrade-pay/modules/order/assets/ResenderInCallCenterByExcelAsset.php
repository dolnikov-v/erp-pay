<?php
namespace app\modules\order\assets;

use yii\web\AssetBundle;

/**
 * Class ResenderInCallCenterAsset
 * @package app\modules\order\assets
 */
class ResenderInCallCenterByExcelAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/order/resender-in-call-center-by-excel';

    public $css = [
        'resender-in-call-center-by-excel.css',
    ];

    public $js = [
        'resender-in-call-center-by-excel.js',
    ];

    public $depends = [
        'app\modules\order\assets\IndexAsset',
    ];
}
