<?php
namespace app\modules\order\assets;

use yii\web\AssetBundle;

/**
 * Class SendCheckAddressAsset
 * @package app\modules\order\assets
 */
class SendCheckAddressAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/order/send-check-address';

    public $js = [
        'send-check-address.js',
    ];

    public $depends = [
        'app\modules\order\assets\IndexAsset',
    ];
}