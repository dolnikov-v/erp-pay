<?php

namespace app\modules\order\assets;


use yii\web\AssetBundle;

/**
 * Class ResendSeparateOrderIntoDeliveryAsset
 * @package app\modules\order\assets
 */
class ResendSeparateOrderIntoDeliveryAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/order/resend-separate-order-into-delivery';

    public $js = [
        'resend-separate-order-into-delivery.js',
    ];

    public $depends = [
        'app\modules\order\assets\IndexAsset',
    ];
}