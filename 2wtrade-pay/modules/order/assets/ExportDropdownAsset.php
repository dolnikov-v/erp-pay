<?php
namespace app\modules\order\assets;

use yii\web\AssetBundle;

/**
 * Class ExportDropdownAsset
 * @package app\modules\order\assets
 */
class ExportDropdownAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/order/export-dropdown';

    public $js = [
        'export-dropdown.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];

}
