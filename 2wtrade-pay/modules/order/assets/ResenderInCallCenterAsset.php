<?php
namespace app\modules\order\assets;

use yii\web\AssetBundle;

/**
 * Class ResenderInCallCenterAsset
 * @package app\modules\order\assets
 */
class ResenderInCallCenterAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/order/resender-in-call-center';

    public $css = [
        'resender-in-call-center.css',
    ];

    public $js = [
        'resender-in-call-center.js',
    ];

    public $depends = [
        'app\modules\order\assets\IndexAsset',
    ];
}
