<?php
namespace app\modules\order\assets;

use yii\web\AssetBundle;

/**
 * Class ListAsset
 * @package app\modules\order\assets
 */
class ListAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/order/list';

    public $css = [
        'list.css',
    ];

    public $js = [
        'list.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
