<?php
namespace app\modules\order\assets;

use yii\web\AssetBundle;

/**
 * Class DeleteOrdersFromListsAsset
 * @package app\modules\order\assets
 */
class DeleteOrdersFromListsAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/order/delete-orders-from-lists';

    public $css = [
        'delete-orders-from-lists.css',
    ];

    public $js = [
        'delete-orders-from-lists.js',
    ];

    public $depends = [
        'app\modules\order\assets\IndexAsset',
    ];
}
