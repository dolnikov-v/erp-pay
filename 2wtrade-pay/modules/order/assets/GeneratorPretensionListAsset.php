<?php
namespace app\modules\order\assets;

use yii\web\AssetBundle;

/**
 * Class GeneratorPretensionListAsset
 * @package app\modules\order\assets
 */
class GeneratorPretensionListAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/order/generator-pretension-list';

    public $js = [
        'generator-pretension-list.js',
    ];

    public $depends = [
        'app\modules\order\assets\IndexAsset',
    ];
}
