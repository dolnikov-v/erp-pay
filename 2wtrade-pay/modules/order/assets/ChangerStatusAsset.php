<?php
namespace app\modules\order\assets;

use yii\web\AssetBundle;

/**
 * Class ChangerStatusAsset
 * @package app\modules\order\assets
 */
class ChangerStatusAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/order/changer-status';

    public $css = [
        'changer-status.css',
    ];

    public $js = [
        'changer-status.js',
    ];

    public $depends = [
        'app\modules\order\assets\IndexAsset',
    ];
}
