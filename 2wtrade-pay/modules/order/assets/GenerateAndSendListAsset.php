<?php

namespace app\modules\order\assets;


use yii\web\AssetBundle;

/**
 * Class GenerateAndSendListAsset
 * @package app\modules\order\assets
 */
class GenerateAndSendListAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/order/generate-and-send-list';

    public $css = [
        'generate-and-send-list.css',
    ];

    public $js = [
        'generate-and-send-list.js',
    ];

    public $depends = [
        'app\modules\order\assets\IndexAsset',
    ];
}
