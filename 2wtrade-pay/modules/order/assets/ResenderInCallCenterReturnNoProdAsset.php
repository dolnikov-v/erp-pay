<?php
namespace app\modules\order\assets;

use yii\web\AssetBundle;

/**
 * Class ResenderInCallCenterReturnNoProdAsset
 * @package app\modules\order\assets
 */
class ResenderInCallCenterReturnNoProdAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/order/resender-in-call-center-return-no-prod';

    public $js = [
        'resender-in-call-center-return-no-prod.js',
    ];

    public $depends = [
        'app\modules\order\assets\IndexAsset',
    ];
}
