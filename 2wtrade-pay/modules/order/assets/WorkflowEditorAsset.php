<?php
namespace app\modules\order\assets;

use yii\web\AssetBundle;

/**
 * Class WorkflowEditorAsset
 * @package app\modules\order\assets
 */
class WorkflowEditorAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/order/workflow-editor';

    public $css = [
        'workflow-editor.css',
    ];

    public $js = [
        'workflow-editor.js',
    ];

    public $depends = [
        'app\assets\vendor\GoJsAsset',
    ];
}
