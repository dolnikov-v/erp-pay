<?php

namespace app\modules\order\assets;

use yii\web\AssetBundle;

class SendPostSellAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/order/send-post-sell';

    public $js = [
        'send-post-sell.js',
    ];

    public $depends = [
        'app\modules\order\assets\IndexAsset',
    ];
}