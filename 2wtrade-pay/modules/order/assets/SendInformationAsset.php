<?php

namespace app\modules\order\assets;

use yii\web\AssetBundle;

class SendInformationAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/order/send-information';

    public $js = [
        'send-information.js',
    ];

    public $depends = [
        'app\modules\order\assets\IndexAsset',
    ];
}