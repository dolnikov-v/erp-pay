<?php
namespace app\modules\order\assets;

use yii\web\AssetBundle;

/**
 * Class MarketingListAsset
 * @package app\modules\order\assets
 */
class MarketingListAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/order/marketing-list';

    public $css = [
        'list.css',
    ];

    public $js = [
        'list.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
