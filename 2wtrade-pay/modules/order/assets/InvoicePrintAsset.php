<?php
namespace app\modules\order\assets;

use yii\web\AssetBundle;

/**
 * Class InvoicePrintAsset
 * @package app\modules\order\assets
 */
class InvoicePrintAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/order/invoice-print';

    public $js = [
//        'invoice-print.js',
    'invoice-modal.js'
    ];

    public $depends = [
        'app\modules\order\assets\IndexAsset',
    ];
}
