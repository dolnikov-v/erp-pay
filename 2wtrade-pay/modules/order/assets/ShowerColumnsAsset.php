<?php
namespace app\modules\order\assets;

use yii\web\AssetBundle;

/**
 * Class ShowerColumnsAsset
 * @package app\modules\order\assets
 */
class ShowerColumnsAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/order/shower-columns';

    public $css = [
        'shower-columns.css',
    ];

    public $js = [
        'shower-columns.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
