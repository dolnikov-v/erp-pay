<?php
namespace app\modules\order\assets;

use yii\web\AssetBundle;

/**
 * Class SendSmsPollAsset
 * @package app\modules\order\assets
 */
class SendSmsPollAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/order/send-sms-poll';

    public $js = [
        'send-sms-poll.js',
    ];

    public $depends = [
        'app\modules\order\assets\IndexAsset',
    ];
}

