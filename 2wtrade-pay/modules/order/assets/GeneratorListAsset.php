<?php
namespace app\modules\order\assets;

use yii\web\AssetBundle;

/**
 * Class GeneratorListAsset
 * @package app\modules\order\assets
 */
class GeneratorListAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/order/generator-list';

    public $css = [
        'generator-list.css',
    ];

    public $js = [
        'generator-list.js',
    ];

    public $depends = [
        'app\modules\order\assets\IndexAsset',
    ];
}
