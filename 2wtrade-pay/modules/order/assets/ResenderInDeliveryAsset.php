<?php
namespace app\modules\order\assets;

use yii\web\AssetBundle;

/**
 * Class ResenderInDeliveryAsset
 * @package app\modules\order\assets
 */
class ResenderInDeliveryAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/order/resender-in-delivery';

    public $css = [
        'resender-in-delivery.css',
    ];

    public $js = [
        'resender-in-delivery.js',
    ];

    public $depends = [
        'app\modules\order\assets\IndexAsset',
    ];
}
