<?php
namespace app\modules\order\assets;

use yii\web\AssetBundle;

/**
 * Class OrderFiltersAsset
 * @package app\modules\order\assets
 */
class OrderFiltersAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/order/order-filters';

    public $css = [
        'order-filters.css',
    ];

    public $js = [
        'order-filters.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
