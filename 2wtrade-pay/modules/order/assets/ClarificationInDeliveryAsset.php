<?php
namespace app\modules\order\assets;

use yii\web\AssetBundle;

/**
 * Class ClarificationInDeliveryAsset
 * @package app\modules\order\assets
 */
class ClarificationInDeliveryAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/order/clarification-in-delivery';

    public $css = [
        'clarification-in-delivery.css',
    ];

    public $js = [
        'clarification-in-delivery.js',
    ];

    public $depends = [
        'app\modules\order\assets\IndexAsset',
    ];
}
