<?php

namespace app\modules\order\assets;


use yii\web\AssetBundle;

/**
 * Class DeliveryRequestStatusChangerAsset
 * @package app\modules\order\assets
 */
class DeliveryRequestStatusChangerAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/order/delivery-request-status-changer';

    public $css = [
    ];

    public $js = [
        'delivery-request-status-changer.js',
    ];

    public $depends = [
        'app\modules\order\assets\IndexAsset',
    ];
}
