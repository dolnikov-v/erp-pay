<?php
namespace app\modules\order\assets;

use yii\web\AssetBundle;

/**
 * Class SenderInDeliveryAsset
 * @package app\modules\order\assets
 */
class SenderInDeliveryAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/order/sender-in-delivery';

    public $css = [
        'sender-in-delivery.css',
    ];

    public $js = [
        'sender-in-delivery.js',
    ];

    public $depends = [
        'app\modules\order\assets\IndexAsset',
    ];
}
