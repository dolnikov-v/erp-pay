<?php

namespace app\modules\order\assets;

use yii\web\AssetBundle;

class TransferToDistributorAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/order/transfer-to-distributor';

    public $js = [
        'transfer-to-distributor.js',
    ];
}