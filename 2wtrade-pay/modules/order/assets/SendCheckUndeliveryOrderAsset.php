<?php

namespace app\modules\order\assets;


use yii\web\AssetBundle;

/**
 * Class SendCheckUndeliveryOrderAsset
 * @package app\modules\order\assets
 */
class SendCheckUndeliveryOrderAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/order/send-check-undelivery-order';

    public $css = [
        'send-check-undelivery-order.css',
    ];

    public $js = [
        'send-check-undelivery-order.js',
    ];

    public $depends = [
        'app\modules\order\assets\IndexAsset',
    ];
}