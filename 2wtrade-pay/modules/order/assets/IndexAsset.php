<?php
namespace app\modules\order\assets;

use yii\web\AssetBundle;

/**
 * Class IndexAsset
 * @package app\modules\order\assets
 */
class IndexAsset extends AssetBundle
{
    public $sourcePath = '@app/web/modules/order/index';

    public $css = [
        'index.css',
        'edit.css',
    ];

    public $js = [
        'index.js',
        'edit.js',
    ];

    public $depends = [
        'app\assets\vendor\JqueryAsset',
    ];
}
