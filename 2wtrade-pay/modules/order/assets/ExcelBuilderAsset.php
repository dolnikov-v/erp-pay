<?php
namespace app\modules\order\assets;

use yii\web\AssetBundle;

/**
 * Class ExcelBuilderAsset
 * @package app\modules\order\assets
 */
class ExcelBuilderAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/order/excel-builder';

    public $css = [
        'excel-builder.css',
    ];

    public $js = [
        'excel-builder.js',
    ];

    public $depends = [
        'app\modules\order\assets\IndexAsset',
    ];
}
