<?php
namespace app\modules\order\assets;

use yii\web\AssetBundle;

/**
 * Class SenderInCallCenterAsset
 * @package app\modules\order\assets
 */
class SenderInCallCenterAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/order/sender-in-call-center';

    public $css = [
        'sender-in-call-center.css',
    ];

    public $js = [
        'sender-in-call-center.js',
    ];

    public $depends = [
        'app\modules\order\assets\IndexAsset',
    ];
}
