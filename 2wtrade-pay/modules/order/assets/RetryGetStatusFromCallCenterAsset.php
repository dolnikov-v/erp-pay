<?php

namespace app\modules\order\assets;

use yii\web\AssetBundle;

/**
 * Class RetryGetStatusFromCallCenterAsset
 * @package app\modules\order\assets
 */
class RetryGetStatusFromCallCenterAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/order/retry-get-status-from-call-center';

    public $css = [
    ];

    public $js = [
        'retry-get-status-from-call-center.js',
    ];

    public $depends = [
        'app\modules\order\assets\IndexAsset',
    ];
}
