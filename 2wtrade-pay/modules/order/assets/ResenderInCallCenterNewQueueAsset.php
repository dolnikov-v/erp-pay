<?php
namespace app\modules\order\assets;

use yii\web\AssetBundle;

/**
 * Class ResenderInCallCenterNewQueueAsset
 * @package app\modules\order\assets
 */
class ResenderInCallCenterNewQueueAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/order/resender-in-call-center-new-queue';

    public $css = [
        'resender-in-call-center-new-queue.css',
    ];

    public $js = [
        'resender-in-call-center-new-queue.js',
    ];

    public $depends = [
        'app\modules\order\assets\IndexAsset',
    ];
}
