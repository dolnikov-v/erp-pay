<?php
namespace app\modules\order\assets;

use yii\web\AssetBundle;

/**
 * Class ChangerStatusAsset
 * @package app\modules\order\assets
 */
class ChangerDeliveryAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/order/changer-delivery';

    public $js = [
        'changer-delivery.js',
    ];

    public $depends = [
        'app\modules\order\assets\IndexAsset',
    ];
}
