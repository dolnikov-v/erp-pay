<?php
namespace app\modules\order\assets;

use yii\web\AssetBundle;

/**
 * Class ChangerStatusAsset
 * @package app\modules\order\assets
 */
class ResendSeparateOrderAsset extends AssetBundle
{
    public $sourcePath = '@app/web/widgets/order/resend-separate-order';

    public $js = [
        'resend-separate-order.js',
    ];

    public $depends = [
        'app\modules\order\assets\IndexAsset',
    ];
}
