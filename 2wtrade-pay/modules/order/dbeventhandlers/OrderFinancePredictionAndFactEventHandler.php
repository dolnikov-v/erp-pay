<?php

namespace app\modules\order\dbeventhandlers;


use app\components\dbevent\EventHandlerInterface;
use app\components\dbevent\EventObject;
use app\components\dbevent\EventWatcher;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\jobs\OrderCalculateFinanceBalanceJob;

/**
 * Class OrderFinancePredictionAndFactEventHandler
 * @package app\modules\order\dbeventhandlers
 */
class OrderFinancePredictionAndFactEventHandler implements EventHandlerInterface
{
    /**
     * @param EventObject $eventObject
     * @param EventWatcher $watcher
     */
    public function handle(EventObject $eventObject, EventWatcher $watcher)
    {
        $deliveryRequestId = DeliveryRequest::find()
            ->where(['order_id' => $eventObject->primaryKey])
            ->select('id')
            ->scalar();
        \Yii::$app->queueOrders->push(new OrderCalculateFinanceBalanceJob(['deliveryRequestId' => $deliveryRequestId]));
    }
}