<?php

namespace app\modules\order\models;

use app\components\db\ActiveRecordLogCreateTime;
use app\models\User;
use app\modules\order\models\query\OrderLogQuery;
use Yii;

/**
 * Class OrderLog
 *
 * @property integer $id
 * @property integer $order_id
 * @property string $user_id
 * @property string $route
 * @property string $group_id
 * @property string $field
 * @property string $old
 * @property string $new
 * @property string $comment
 * @property integer $created_at
 *
 * @property Order $order
 */
class OrderLog extends ActiveRecordLogCreateTime
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%order_log}}';
    }

    /**
     * @return OrderLogQuery
     */
    public static function find()
    {
        return new OrderLogQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['order_id', 'group_id', 'field'], 'required'],
            [['order_id', 'created_at', 'user_id'], 'integer'],
            [['field', 'old', 'new', 'route'], 'string', 'max' => 255],
            [['comment'], 'string', 'max' => 500],
            [
                ['order_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Order::className(),
                'targetAttribute' => ['order_id' => 'id']
            ],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'order_id' => Yii::t('common', 'Заказ'),
            'group_id' => Yii::t('common', 'Группа'),
            'field' => Yii::t('common', 'Поле'),
            'old' => Yii::t('common', 'Старое значение'),
            'new' => Yii::t('common', 'Новое значение'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'user_id' => Yii::t('common', 'Пользователь'),
            'route' => Yii::t('common', 'Действие'),
            'comment' => Yii::t('common', 'Комментарий'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
