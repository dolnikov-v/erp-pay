<?php
namespace app\modules\order\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\helpers\Utils;
use app\models\logs\LinkData;
use app\models\User;
use app\modules\order\models\query\OrderLogisticListExcelQuery;
use Yii;
use yii\web\ForbiddenHttpException;

/**
 * Class OrderLogisticListExcel
 * @package app\modules\order\models
 * @property integer $id
 * @property integer $list_id
 * @property string $orders_hash
 * @property string $system_file_name
 * @property string $delivery_file_name
 * @property integer $format
 * @property string $columns_hash
 * @property integer $user_id
 * @property integer $sent_user_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $sent_at
 * @property integer $waiting_for_send
 * @property OrderLogisticList $list
 * @property User $user
 */
class OrderLogisticListExcel extends ActiveRecordLogUpdateTime
{
    protected $insertLog = true;
    protected $updateLog = true;
    protected $deleteLog = true;

    /**
     * @return array|null
     */
    protected function getLinkData(): ?array
    {
        return [
            new LinkData(['field' => 'list_id', 'value' => (int)$this->list_id]),
        ];
    }

    const FORMAT_XLSX = 'xlsx';
    const FORMAT_CSV = 'csv';

    /**
     * @return array
     */
    public static function getFormatsCollection()
    {
        return [
            self::FORMAT_XLSX => Yii::t('common', 'XLSX'),
            self::FORMAT_CSV => Yii::t('common', 'CSV'),
        ];
    }

    /**
     * @param string $fileName
     * @param boolean $prepareDir
     * @return string
     * @throws ForbiddenHttpException
     */
    public static function getDownloadPath($fileName, $prepareDir = false)
    {
        $subDir = substr($fileName, 0, 2);

        $dir = Yii::getAlias('@downloads') . DIRECTORY_SEPARATOR . 'excels' . DIRECTORY_SEPARATOR . $subDir;

        if ($prepareDir) {
            Utils::prepareDir($dir, 0777);
        }

        return $dir;
    }

    /**
     * @param string $format
     * @return string
     */
    public static function getExtension($format)
    {
        if ($format == self::FORMAT_XLSX) {
            return 'xlsx';
        } else {
            return 'csv';
        }
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_logistic_list_excel}}';
    }

    /**
     * @return OrderLogisticListExcelQuery
     */
    public static function find()
    {
        return new OrderLogisticListExcelQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['list_id', 'orders_hash', 'system_file_name'], 'required'],
            ['format', 'in', 'range' => array_keys(self::getFormatsCollection())],
            [['list_id', 'user_id', 'sent_user_id', 'waiting_for_send'], 'integer'],
            [['orders_hash', 'columns_hash'], 'string', 'max' => 32],
            [['system_file_name', 'delivery_file_name'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'list_id' => Yii::t('common', 'Лист'),
            'orders_hash' => Yii::t('common', 'Хеш заказов'),
            'system_file_name' => Yii::t('common', 'Системное имя файла'),
            'delivery_file_name' => Yii::t('common', 'Имя файла'),
            'format' => Yii::t('common', 'Формат'),
            'columns_hash' => Yii::t('common', 'Хеш колонок'),
            'user_id' => Yii::t('common', 'Пользователь'),
            'sent_user_id' => Yii::t('common', 'Пользователь'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
            'sent_at' => Yii::t('common', 'Дата отправки'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getList()
    {
        return $this->hasOne(OrderLogisticList::className(), ['id' => 'list_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
