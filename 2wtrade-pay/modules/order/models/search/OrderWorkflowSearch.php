<?php
namespace app\modules\order\models\search;

use app\modules\order\models\OrderWorkflow;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * Class OrderWorkflowSearch
 * @package app\modules\order\models\search
 */
class OrderWorkflowSearch extends OrderWorkflow
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['name', 'string'],
        ];
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OrderWorkflow::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
