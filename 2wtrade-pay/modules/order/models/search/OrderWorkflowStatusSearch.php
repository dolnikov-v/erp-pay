<?php
namespace app\modules\order\models\search;

use app\modules\order\models\OrderWorkflowStatus;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * Class OrderWorkflowStatusSearch
 * @package app\modules\order\models\search
 */
class OrderWorkflowStatusSearch extends OrderWorkflowStatus
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['workflow_id', 'status_id'], 'integer'],
        ];
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OrderWorkflowStatus::find()->joinWith('status');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['workflow_id' => $this->workflow_id]);

        return $dataProvider;
    }
}
