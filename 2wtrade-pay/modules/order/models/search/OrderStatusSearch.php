<?php
namespace app\modules\order\models\search;

use app\modules\order\models\OrderStatus;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * Class OrderStatusSearch
 * @package app\modules\order\models\search
 */
class OrderStatusSearch extends OrderStatus
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['name', 'string'],
            ['group', 'in', 'range' => array_keys(OrderStatus::getGroupsCollection())],
            ['type', 'in', 'range' => array_keys(OrderStatus::getTypesCollection())],
        ];
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OrderStatus::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pagesize' => 0,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'name', $this->name]);
        $query->andFilterWhere(['type' => $this->type]);
        $query->andFilterWhere(['group' => $this->group]);

        return $dataProvider;
    }
}
