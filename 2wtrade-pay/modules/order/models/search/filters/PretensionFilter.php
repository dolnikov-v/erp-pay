<?php
namespace app\modules\order\models\search\filters;

use app\modules\order\models\Order;
use app\modules\order\models\OrderFinanceFact;
use app\modules\order\models\OrderFinancePretension;
use app\modules\order\widgets\orderFilters\filters\PretensionFilter as PretensionFilterWidget;
use Yii;

/**
 * Class PretensionFilter
 * @package app\modules\order\models\search\filters
 */
class PretensionFilter extends Filter
{
    public $not;

    /**
     * @var array
     */
    public $pretension;

    /**
     * @var array
     */
    public $pretensions = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->pretensions = OrderFinancePretension::getTypeCollecion();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['pretension', 'in', 'range' => array_keys($this->pretensions), 'allowArray' => true, 'skipOnEmpty' => false],
            ['not', 'in', 'range' => array(0, 1), 'allowArray' => true],
        ];
    }

    /**
     * @param \yii\db\ActiveQuery $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate()) {
            $this->prepare();

            $in = [];
            $notIn = [];

            foreach ($this->pretension as $index => $status) {
                if ($status) {
                    if (isset($this->not[$index]) && $this->not[$index] == 1) {
                        $notIn[] = $status;
                    } else {
                        $in[] = $status;
                    }
                }
            }

            if ($in || $notIn) {
                $query->joinWith(['orderFinancePretensions']);
            }

            if ($in) {
                $query->andFilterWhere(['in', OrderFinancePretension::tableName().'.type', $in]);
            }

            if ($notIn) {
                $query->andFilterWhere(['not in', OrderFinancePretension::tableName().'.type', $notIn]);
            }
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $output = '';

        $this->prepare();

        foreach ($this->pretension as $index => $pretension) {
            if ($pretension) {
                $model = new PretensionFilter(['pretension' => $pretension]);

                if (isset($this->not[$index]) && $this->not[$index] == 1) {
                    $model->not = 1;
                } else {
                    $model->not = 0;
                }

                $output .= PretensionFilterWidget::widget([
                    'form' => $form,
                    'model' => $model,
                    'visible' => $index >= 1 ? false : true,
                    'formGroupMargin' => count($this->pretension) > 1 ? false : true,
                ]);
            }
        }

        $output .= $this->completeRestore('И еще {count, number} {count, plural, one{претензия} few{претензия} other{претензий}}...', count($this->pretension) - 1, 'pretension');

        return $output;
    }

    /**
     * Prepare
     */
    private function prepare()
    {
        if (!is_array($this->pretension)) {
            $this->pretension = [$this->pretension];
        }

        if (!is_array($this->not)) {
            $this->not = [$this->not];
        }
    }
}
