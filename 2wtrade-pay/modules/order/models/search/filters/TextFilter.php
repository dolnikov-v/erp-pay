<?php
namespace app\modules\order\models\search\filters;

use app\modules\callcenter\models\CallCenterRequest;
use app\modules\order\models\Order;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\models\OrderFinance;
use app\modules\order\widgets\orderFilters\filters\TextFilter as TextFilterWidget;
use Yii;
use yii\base\InvalidParamException;


/**
 * Class TextFilter
 * @package app\modules\order\models\filters
 */
class TextFilter extends Filter
{
    const ENTITY_CUSTOMER_FULL_NAME = 'fio';
    const ENTITY_CUSTOMER_PHONE = 'phone';
    const ENTITY_CUSTOMER_EMAIL = 'email';
    const ENTITY_CUSTOMER_ADDRESS = 'address';
    const ENTITY_CUSTOMER_ADDRESS_ADDITIONAL = 'address_additional';
    const ENTITY_CUSTOMER_CITY = 'city';
    const ENTITY_CUSTOMER_DISTRICT = 'district';
    const ENTITY_CUSTOMER_PROVINCE = 'province';
    const ENTITY_CUSTOMER_ZIP = 'zip';
    const ENTITY_COMMENT = 'comment';
    const ENTITY_DELIVERY = 'delivery';
    const ENTITY_WEBMASTER = 'webmaster';
    const ENTITY_LAST_FOREIGN_OPERATOR = 'last_foreign_operator';
    const ENTITY_PAYMENT_NUMBER = 'payment_number';
    const ENTITY_CALL_CENTER_DELIVERY_ERROR = 'call_center_delivery_error';
    const ENTITY_CALL_CENTER_API_ERROR = 'call_center_api_error';
    const ENTITY_DELIVERY_API_ERROR = 'delivery_api_error';

    /**
     * @var array
     */
    public $not;

    /**
     * @var array
     */
    public $entity;

    /**
     * @var array
     */
    public $text;

    /**
     * @var array
     */
    public $entities = [];

    /**
     * @var \app\models\User
     */
    public $specifiedUser;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $limitEntities = false;
        if (($this->specifiedUser instanceof \app\models\User && $this->specifiedUser->getIsDeliveryManager()) ||
            (isset(Yii::$app->user) && Yii::$app->user->getIsDeliveryManager())
        ) {
            $limitEntities = true;
        }

        if ($limitEntities) {
            $this->entities = [
                self::ENTITY_CUSTOMER_FULL_NAME => Yii::t('common', 'Полное имя заказчика'),
                self::ENTITY_CUSTOMER_PHONE => Yii::t('common', 'Телефон заказчика'),
                self::ENTITY_DELIVERY => Yii::t('common', 'Трекер в службе доставки'),
            ];
        } else {
            $this->entities = [
                self::ENTITY_CUSTOMER_FULL_NAME => Yii::t('common', 'Полное имя заказчика'),
                self::ENTITY_CUSTOMER_PHONE => Yii::t('common', 'Телефон заказчика'),
                self::ENTITY_CUSTOMER_EMAIL => Yii::t('common', 'E-mail заказчика'),
                self::ENTITY_CUSTOMER_ADDRESS => Yii::t('common', 'Адрес заказчика'),
                self::ENTITY_CUSTOMER_ADDRESS_ADDITIONAL => Yii::t('common', 'Доп. адрес заказчика'),
                self::ENTITY_CUSTOMER_CITY => Yii::t('common', 'Город заказчика'),
                self::ENTITY_CUSTOMER_DISTRICT => Yii::t('common', 'Регион заказчика'),
                self::ENTITY_CUSTOMER_PROVINCE => Yii::t('common', 'Провинция заказчика'),
                self::ENTITY_CUSTOMER_ZIP => Yii::t('common', 'Zip код заказчика'),
                self::ENTITY_COMMENT => Yii::t('common', 'Комментарий к заказу'),
                self::ENTITY_DELIVERY => Yii::t('common', 'Трекер в службе доставки'),
                self::ENTITY_WEBMASTER => Yii::t('common', 'ID вебмастера'),
                self::ENTITY_LAST_FOREIGN_OPERATOR => Yii::t('common', 'Внешний оператор'),
                self::ENTITY_PAYMENT_NUMBER => Yii::t('common', 'Платежка'),
//            self::ENTITY_CALL_CENTER_DELIVERY_ERROR => Yii::t('common', 'Ошибки доставки'),
                self::ENTITY_CALL_CENTER_API_ERROR => Yii::t('common', 'Ошибки коллцентра'),
                self::ENTITY_DELIVERY_API_ERROR => Yii::t('common', 'Ошибка курьерки'),
            ];
        }
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['not', 'in', 'range' => array(0, 1), 'allowArray' => true],
            ['entity', 'in', 'range' => array_keys($this->entities), 'allowArray' => true],
            ['text', 'safe'],
        ];
    }

    /**
     * @param \yii\db\ActiveQuery $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate()) {
            $this->prepare();

            foreach ($this->entity as $index => $entity) {

                if (array_key_exists($entity, $this->entities) && array_key_exists($index, $this->text)) {
                    $operator = 'like';

                    if (isset($this->not[$index]) && $this->not[$index] == 1) {
                        $operator = 'not like';
                    }

                    $attribute = $this->getEntityAttribute($entity);
                    $this->applyEntityQueryJoin($query, $entity);

                    if ($this->text[$index] === '') {

                        if ($operator == 'like') {

                            $query->andWhere([$attribute => '']);

                        } elseif ($operator == 'not like') {

                            $query->andWhere(['<>', $attribute, '']);
                            $query->andWhere(['not', [$attribute => null]]);
                        }

                    } else {
                        //NPAY1286
                        if ($entity == self::ENTITY_DELIVERY) {
                            $searchText = explode(',', $this->text[$index]);
                            $searchText = array_map('trim', $searchText);

                            if (count($searchText) > 1) {
                                $operator = ($operator == 'like') ? 'in' : 'not in';
                                $this->text[$index] = $searchText;
                            }
                        }

                        $query->andFilterWhere([$operator, $attribute, $this->text[$index]]);
                    }
                }
            }
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     * @throws \Exception
     */
    public function restore($form)
    {
        $output = '';

        $this->prepare();

        foreach ($this->entity as $index => $entity) {
            if (array_key_exists($entity, $this->entities) && array_key_exists($index, $this->text)) {

                $model = new TextFilter([
                    'entity' => $this->entity[$index],
                    //NPAY 1286
                    'text' => is_array($this->text[$index]) ? implode(', ', $this->text[$index]) : $this->text[$index],
                ]);

                if (isset($this->not[$index]) && $this->not[$index] == 1) {
                    $model->not = 1;
                } else {
                    $model->not = 0;
                }

                $output .= TextFilterWidget::widget([
                    'form' => $form,
                    'model' => $model,
                    'visible' => $index >= 1 ? false : true,
                    'formGroupMargin' => count($this->entity) > 1 ? false : true,
                ]);
            }
        }

        $output .= $this->completeRestore('И еще {count, number} {count, plural, one{поиск} few{поиска} other{поисков}} по тексту...',
            count($this->entity) - 1, 'text');

        return $output;
    }

    /**
     * @param string $entity
     * @param \yii\db\ActiveQuery $query
     * @return string
     */
    protected function getEntityAttribute($entity)
    {
        $attribute = '';

        switch ($entity) {
            case self::ENTITY_CUSTOMER_FULL_NAME:
                $attribute = Order::tableName() . '.customer_full_name';
                break;
            case self::ENTITY_CUSTOMER_PHONE:
                $attribute = Order::tableName() . '.customer_phone';
                break;
            case self::ENTITY_CUSTOMER_EMAIL:
                $attribute = Order::tableName() . '.customer_email';
                break;
            case self::ENTITY_CUSTOMER_ADDRESS:
                $attribute = Order::tableName() . '.customer_address';
                break;
            case self::ENTITY_CUSTOMER_ADDRESS_ADDITIONAL:
                $attribute = Order::tableName() . '.customer_address_additional';
                break;
            case self::ENTITY_CUSTOMER_CITY:
                $attribute = Order::tableName() . '.customer_city';
                break;
            case self::ENTITY_CUSTOMER_DISTRICT:
                $attribute = Order::tableName() . '.customer_district';
                break;
            case self::ENTITY_CUSTOMER_PROVINCE:
                $attribute = Order::tableName() . '.customer_province';
                break;
            case self::ENTITY_CUSTOMER_ZIP:
                $attribute = Order::tableName() . '.customer_zip';
                break;
            case self::ENTITY_COMMENT:
                $attribute = Order::tableName() . '.comment';
                break;
            case self::ENTITY_DELIVERY:
                $attribute = DeliveryRequest::tableName() . '.tracking';
                break;
            case self::ENTITY_WEBMASTER:
                $attribute = Order::tableName() . '.webmaster_identifier';
                break;
            case self::ENTITY_LAST_FOREIGN_OPERATOR:
                $attribute = CallCenterRequest::tableName() . '.last_foreign_operator';
                break;
            case self::ENTITY_PAYMENT_NUMBER:
                $attribute = OrderFinance::tableName() . '.payment';
                break;
            case self::ENTITY_CALL_CENTER_DELIVERY_ERROR:
                $attribute = CallCenterRequest::tableName() . '.delivery_error';
                break;
            case self::ENTITY_CALL_CENTER_API_ERROR:
                $attribute = CallCenterRequest::tableName() . '.api_error';
                break;
            case self::ENTITY_DELIVERY_API_ERROR:
                $attribute = DeliveryRequest::tableName() . '.api_error';
                break;
        }

        if (empty($attribute)) {
            throw new InvalidParamException(Yii::t('common', 'Неизвестное поле для поиска.'));
        }

        return $attribute;
    }

    /**
     * @param \yii\db\ActiveQuery $query
     * @param string $entity
     */
    protected function applyEntityQueryJoin($query, $entity)
    {
        switch ($entity) {
            case self::ENTITY_CALL_CENTER_API_ERROR:
            case self::ENTITY_LAST_FOREIGN_OPERATOR:
            case self::ENTITY_CALL_CENTER_DELIVERY_ERROR:
                $query->joinWith('callCenterRequest');
                break;
            case self::ENTITY_DELIVERY_API_ERROR:
            case self::ENTITY_DELIVERY:
                $query->joinWith('deliveryRequest');
                break;
            case self::ENTITY_PAYMENT_NUMBER:
                $query->joinWith('finance');
                break;
        }
    }

    /**
     * Prepare
     */
    private function prepare()
    {
        if (!is_array($this->not)) {
            $this->not = [$this->not];
        }

        if (!is_array($this->entity)) {
            $this->entity = [$this->entity];
        }

        if (!is_array($this->text)) {
            $this->text = [$this->text];
        }
    }
}
