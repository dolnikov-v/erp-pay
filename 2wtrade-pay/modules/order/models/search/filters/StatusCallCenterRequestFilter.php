<?php
namespace app\modules\order\models\search\filters;

use app\modules\callcenter\models\CallCenterRequest;
use app\modules\order\widgets\orderFilters\filters\StatusCallCenterRequestFilter as StatusCallCenterRequestFilterWidget;

/**
 * Class StatusCallCenterRequestFilter
 * @package app\modules\order\models\search\filters
 */
class StatusCallCenterRequestFilter extends Filter
{
    public $not;

    /**
     * @var array
     */
    public $statusCallCenterRequest;

    /**
     * @var array
     */
    public $statusesCallCenterRequests = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->statusesCallCenterRequests = CallCenterRequest::getStatusesCollection();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [
                'statusCallCenterRequest',
                'in',
                'range' => array_keys($this->statusesCallCenterRequests),
                'allowArray' => true,
                'skipOnEmpty' => false
            ],
            ['not', 'in', 'range' => array(0, 1), 'allowArray' => true],
        ];
    }

    /**
     * @param \yii\db\ActiveQuery $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate()) {
            $this->prepare();
            $query->joinWith('callCenterRequest');

            $in = [];
            $notIn = [];

            foreach ($this->statusCallCenterRequest as $index => $status) {
                if ($status) {
                    if (isset($this->not[$index]) && $this->not[$index] == 1) {
                        $notIn[] = $status;
                    } else {
                        $in[] = $status;
                    }
                }
            }

            if ($in) {
                $query->andFilterWhere(['in', CallCenterRequest::tableName().'.status', $in]);
            }

            if ($notIn) {
                $query->andFilterWhere(['not in', CallCenterRequest::tableName().'.status', $notIn]);
            }
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     *
     * @return string
     * @throws \Exception
     */
    public function restore($form)
    {
        $output = '';

        $this->prepare();

        foreach ($this->statusCallCenterRequest as $index => $status) {
            if ($status) {
                $model = new StatusCallCenterRequestFilter(['statusCallCenterRequest' => $status]);

                if (isset($this->not[$index]) && $this->not[$index] == 1) {
                    $model->not = 1;
                } else {
                    $model->not = 0;
                }

                $output .= StatusCallCenterRequestFilterWidget::widget([
                    'form' => $form,
                    'model' => $model,
                    'visible' => $index >= 1 ? false : true,
                    'formGroupMargin' => count($this->statusCallCenterRequest) > 1 ? false : true,
                ]);
            }
        }

        $output .= $this->completeRestore('И еще {count, number} {count, plural, one{статус колл-центра} few{статуса колл-центра} other{статусов колл-центра}}...',
            count($this->statusCallCenterRequest) - 1, 'statusCallCenterRequest');

        return $output;
    }

    /**
     * Prepare
     */
    private function prepare()
    {
        if (!is_array($this->statusCallCenterRequest)) {
            $this->statusCallCenterRequest = [$this->statusCallCenterRequest];
        }

        if (!is_array($this->not)) {
            $this->not = [$this->not];
        }
    }
}
