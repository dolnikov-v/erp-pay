<?php
namespace app\modules\order\models\search\filters;

use app\modules\order\models\OrderFinanceBalance;
use app\modules\order\models\OrderFinanceFact;
use app\modules\order\widgets\orderFilters\filters\DebtsFilter as DebtsFilterWidget;
use Yii;

/**
 * Class DebtsFilter
 * @package app\modules\order\models\filters
 */
class DebtsFilter extends Filter
{
    const STATUS_COD_FACT = 'cod_fact';
    const STATUS_DELIVERY_FACT = 'delivery_fact';
    const STATUS_COD_BALANCE = 'cod_balance';
    const STATUS_DELIVERY_BALANCE = 'delivery_balance';

    /** @var array */
    public $not;

    /** @var array */
    public $debtsStatus;

    /** @var array */
    public $debtsStatuses = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->debtsStatuses = [
            static::STATUS_COD_FACT => Yii::t('common', 'Имеется оплата COD'),
            static::STATUS_DELIVERY_FACT => Yii::t('common', 'Имеется оплата услуг КС'),
            static::STATUS_COD_BALANCE => Yii::t('common', 'Имеется задолжность по COD'),
            static::STATUS_DELIVERY_BALANCE => Yii::t('common', 'Имеется задолжность по услугам КС'),
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['debtsStatus', 'in', 'range' => array_keys($this->debtsStatuses), 'allowArray' => true],
            ['not', 'in', 'range' => array(0, 1), 'allowArray' => true],
        ];
    }

    /**
     * @param \yii\db\ActiveQuery $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate()) {
            $this->prepare();

            $in = [];
            $notIn = [];

            foreach ($this->debtsStatus as $index => $status) {
                if ($status) {
                    switch ($status) {
                        case static::STATUS_COD_FACT:
                            $query->joinWith('financeFact');
                            $status = 'SELECT SUM(CASE WHEN ' . OrderFinanceFact::tableName() . '.price_cod THEN 1 ELSE 0 END)';
                            break;
                        case static::STATUS_DELIVERY_FACT:
                            $fields = [];
                            $query->joinWith('financeFact');
                            foreach (OrderFinanceFact::getPriceFields() as $columnName) {
                                if ($fields != OrderFinanceFact::COLUMN_PRICE_COD) {
                                    $fields[] = 'SUM(CASE WHEN ' . OrderFinanceFact::tableName() . '.' . $columnName . ' THEN 1 ELSE 0 END)';
                                }
                            }
                            if (count($fields) > 0) {
                                $status = 'SELECT ' . implode(' + ', $fields);
                            } else {
                                $status = null;
                            }
                            break;
                        case static::STATUS_COD_BALANCE:
                            $query->joinWith('orderFinanceBalances');
                            $status = 'SELECT SUM(CASE WHEN ' . OrderFinanceBalance::tableName() . '.price_cod THEN 1 ELSE 0 END)';
                            break;
                        case static::STATUS_DELIVERY_BALANCE:
                            $query->joinWith('orderFinanceBalances');
                            $fields = [];
                            foreach (OrderFinanceBalance::getPriceFields() as $columnName) {
                                if ($fields != OrderFinanceFact::COLUMN_PRICE_COD) {
                                    $fields[] = 'SUM(CASE WHEN ' . OrderFinanceBalance::tableName() . '.' . $columnName . ' THEN 1 ELSE 0 END)';
                                }
                            }
                            if (count($fields) > 0) {
                                $status = 'SELECT ' . implode(' + ', $fields);
                            } else {
                                $status = null;
                            }
                            break;
                        default:
                            $status = null;
                    }
                    if ($status) {
                        if (isset($this->not[$index]) && $this->not[$index] == 1) {
                            $notIn[] = $status;
                        } else {
                            $in[] = $status;
                        }
                    }
                }
            }

            if ($in) {
                $where = [];
                foreach ($in as $condition) {
                    $where[] = ['>', $condition, 0];
                }
                if (count($where) > 1) {
                    $query->andFilterWhere(array_merge(['OR'], $where));
                } else {
                    $query->andFilterWhere(array_shift($where));
                }
            }

            if ($notIn) {
                $where = [];
                foreach ($notIn as $condition) {
                    $where[] = ['=', $condition, 0];
                }
                if (count($where) > 1) {
                    $query->andFilterWhere(array_merge(['OR'], $where));
                } else {
                    $query->andFilterWhere(array_shift($where));
                }
            }
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     * @throws \Exception
     */
    public function restore($form)
    {
        $output = '';

        $this->prepare();

        foreach ($this->debtsStatus as $index => $status) {
            if ($status) {
                $model = new DebtsFilter(['debtsStatus' => $status]);

                if (isset($this->not[$index]) && $this->not[$index] == 1) {
                    $model->not = 1;
                } else {
                    $model->not = 0;
                }

                $output .= DebtsFilterWidget::widget([
                    'form' => $form,
                    'model' => $model,
                    'visible' => $index >= 1 ? false : true,
                    'formGroupMargin' => count($this->debtsStatus) > 1 ? false : true,
                ]);
            }
        }

        $output .= $this->completeRestore('И еще {count, number} {count, plural, one{статус} few{статуса} other{статусов}}...', count($this->debtsStatus) - 1, 'debts');

        return $output;
    }

    /**
     * Prepare
     */
    private function prepare()
    {
        if (!is_array($this->debtsStatus)) {
            $this->debtsStatus = [$this->debtsStatus];
        }

        if (!is_array($this->not)) {
            $this->not = [$this->not];
        }
    }
}
