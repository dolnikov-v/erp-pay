<?php

namespace app\modules\order\models\search\filters;

use app\modules\callcenter\models\CallCenterCheckRequest;
use app\modules\order\models\Order;
use app\modules\order\widgets\orderFilters\filters\CheckingTypeFilter as CheckingTypeWidget;
use yii\db\Query;

/**
 * Class CheckingTypeFilter
 * @package app\modules\order\models\search\filters
 */
class CheckingTypeFilter extends Filter
{
    public $not;

    /**
     * @var string|array
     */
    public $type;

    /**
     * @var string|array
     */
    public $informationType;

    /**
     * @var array
     */
    public $types = [];

    /** @var array */
    public $status;

    /** @var array */
    public $statuses = [];

    /**
     * @var array
     */
    public $informationTypes = [];

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['type', 'in', 'range' => array_keys($this->types), 'allowArray' => true, 'skipOnEmpty' => false],
            // Добавляем в доступный для выбора список статусов значение "0", ибо статус может быть не выбран, тогда ищем во всех записях
            ['status', 'in', 'range' => array_merge(array_keys($this->statuses), [0]), 'allowArray' => true],
            ['informationType', 'safe'],
            ['not', 'in', 'range' => array(0, 1), 'allowArray' => true],
        ];
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->types = CallCenterCheckRequest::getTypes();
        $this->statuses = CallCenterCheckRequest::getStatuses();
        $this->informationTypes = CallCenterCheckRequest::getInformationCollection(true);
    }

    /**
     * @param \yii\db\ActiveQuery $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate()) {
            $this->prepare();
            foreach ($this->type as $index => $type) {
                if (isset($this->not[$index]) && $this->not[$index] == 1) {

                    $condition = ['and'];
                    $condition[] = ['!=', CallCenterCheckRequest::tableName() . '.type', $type];

                    $status = $this->status[$index] ?? null;
                    if ($status) {
                        $condition[] = ['=', CallCenterCheckRequest::tableName() . '.status', $status];
                    }

                    if (isset($this->informationType[$index]) && !empty($this->informationType[$index])) {
                        $condition[] = [
                            '!=',
                            CallCenterCheckRequest::tableName() . '.foreign_information',
                            $this->informationType[$index]
                        ];
                    }
                    $query->andFilterWhere([
                        'or',
                        $condition,
                        ['is', CallCenterCheckRequest::tableName() . '.id', null]
                    ]);
                } else {
                    $query->andFilterWhere([CallCenterCheckRequest::tableName() . '.type' => $type]);
                    if (isset($this->informationType[$index]) && !empty($this->informationType[$index])) {
                        if ($this->informationType[$index] == CallCenterCheckRequest::FOREIGN_SUB_STATUS_EMPTY) {

                            $condition = 'is';
                            $comparedValueByType = null;

                            if ($type == CallCenterCheckRequest::TYPE_CHECKUNDELIVERY) {
                                $condition = 'not in';
                                $subStatusesDone = CallCenterCheckRequest::getInformationCollection(false);
                                $comparedValueByType = array_keys($subStatusesDone[$type]);
                            }

                            $query->andWhere([
                                $condition,
                                CallCenterCheckRequest::tableName() . '.foreign_information',
                                $comparedValueByType
                            ]);

                            if ($type != CallCenterCheckRequest::TYPE_CHECKUNDELIVERY) {
                                $query->andWhere([
                                    'not in',
                                    CallCenterCheckRequest::tableName() . '.status',
                                    [
                                        CallCenterCheckRequest::STATUS_ERROR,
                                        CallCenterCheckRequest::STATUS_DONE,
                                    ]

                                ]);
                            }
                        } else {
                            $query->andFilterWhere([
                                CallCenterCheckRequest::tableName() . '.foreign_information' => $this->informationType[$index]
                            ]);
                        }
                    }
                    $status = $this->status[$index] ?? null;
                    if ($status) {
                        $query->andWhere([
                            CallCenterCheckRequest::tableName() . '.status' => $status
                        ]);
                    }
                }
            }
            $query->leftJoin(CallCenterCheckRequest::tableName(),
                CallCenterCheckRequest::tableName() . '.order_id=' . Order::tableName() . '.id and ' .
                CallCenterCheckRequest::tableName() . '.id = (SELECT MAX(id) FROM ' . CallCenterCheckRequest::tableName() . ' WHERE order_id = ' . Order::tableName() . '.id)');
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $output = '';

        $this->prepare();

        foreach ($this->type as $index => $type) {
            if ($type) {

                $status = $this->status[$index] ?? null;

                $model = new CheckingTypeFilter(['type' => $type, 'status' => $status]);

                if (isset($this->not[$index]) && $this->not[$index] == 1) {
                    $model->not = 1;
                } else {
                    $model->not = 0;
                }

                if (isset($this->informationType[$index])) {
                    $model->informationType = $this->informationType[$index];
                } else {
                    $model->informationType = null;
                }

                $output .= CheckingTypeWidget::widget([
                    'form' => $form,
                    'model' => $model,
                    'visible' => $index >= 1 ? false : true,
                    'formGroupMargin' => count($this->type) > 1 ? false : true,
                ]);
            }
        }

        $output .= $this->completeRestore('И еще {count, number} {count, plural, one{тип} few{типа} other{типов}}...',
            count($this->type) - 1, 'checking-queues');

        return $output;
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'type' => \Yii::t('common', 'Тип очереди'),
        ];
    }

    /**
     * Prepare
     */
    private function prepare()
    {
        if (!is_array($this->type)) {
            $this->type = [$this->type];
        }

        if (!is_array($this->informationType)) {
            $this->informationType = [$this->informationType];
        }

        if (!is_array($this->status)) {
            $this->status = [$this->status];
        }

        if (!is_array($this->not)) {
            $this->not = [$this->not];
        }
    }
}
