<?php

namespace app\modules\order\models\search\filters;

use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryZipcodes;
use app\modules\order\models\Order;
use app\modules\order\widgets\orderFilters\filters\DeliveryZipGroupFilter as DeliveryZipGroupFilterWidget;
use Yii;
use yii\db\Expression;

class DeliveryZipGroupFilter extends Filter
{
    public $not;

    /**
     * @var integer
     */
    public $countryId;

    /**
     * @var array
     */
    public $list;

    /**
     * @var array
     */
    public $lists = [];

    public function init()
    {
        parent::init();

        if (is_null($this->countryId)) {
            $this->countryId = Yii::$app->user->getCountry()->id;
        }

        //todo тут принятие выбранной службы доставки както через ajax или из get
        // но не очевидно т.к. может быть выбрано несколько служб доставки и достаточно сложно
        // поэтому пока формируем список вместе со службой доставки в названии
        $this->lists = DeliveryZipcodes::find()
            ->select([
                DeliveryZipcodes::tableName() . '.id',
                'name' => 'CONCAT(' . Delivery::tableName() . '.name,": ",' . DeliveryZipcodes::tableName() . '.name)'
            ])
            ->leftJoin(Delivery::tableName(), DeliveryZipcodes::tableName() . '.delivery_id=' . Delivery::tableName() . ".id")
            ->where([Delivery::tableName() . '.country_id' => $this->countryId])
            ->collection();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['list', 'in', 'range' => array_keys($this->lists), 'allowArray' => true],
            ['not', 'in', 'range' => array(0, 1), 'allowArray' => true],
        ];
    }

    /**
     * @param \yii\db\ActiveQuery $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate()) {
            $this->prepare();


            foreach ($this->list as $index => $list) {

                if ($list) {
                    $not = '';
                    if (isset($this->not[$index]) && $this->not[$index] == 1) {
                        $not = 'NOT ';
                    }

                    $doSearch = false;
                    $deliveryZipcode = DeliveryZipcodes::findOne($list);
                    if ($deliveryZipcode) {
                        $doSearch = true;
                        if (strlen(trim($deliveryZipcode->zipcodes)) == 0) {
                            //выбранная группа с пустыми индексами - значит курьерка доставляет по всей стране
                            $doSearch = false;
                        }
                        $hasAnyMoreDeliveryZipcodes = DeliveryZipcodes::find()
                            ->byDeliveryId($deliveryZipcode->delivery_id)
                            ->andWhere(['<>', 'id', $deliveryZipcode->id])
                            ->andWhere(['is not', 'zipcodes', null])
                            ->count();
                        if ($hasAnyMoreDeliveryZipcodes) {
                            // есть ли еще группы зипкодов у этой курьерки не пустые
                            $doSearch = true;
                        }
                    }
                    if ($doSearch) {
                        $query->andWhere(new Expression($not . 'FIND_IN_SET(' . Order::tableName() . '.customer_zip,(select zipcodes from delivery_zipcodes where id=:delivery_zipcode' . $index . '))'))
                            ->addParams([':delivery_zipcode' . $index => $list]);
                    }
                }
            }
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $output = '';

        $this->prepare();

        foreach ($this->list as $index => $list) {
            if ($list) {
                $model = new DeliveryZipGroupFilter(['list' => $list]);

                if (isset($this->not[$index]) && $this->not[$index] == 1) {
                    $model->not = 1;
                } else {
                    $model->not = 0;
                }

                $output .= DeliveryZipGroupFilterWidget::widget([
                    'form' => $form,
                    'model' => $model,
                    'visible' => $index >= 1 ? false : true,
                    'formGroupMargin' => count($this->list) > 1 ? false : true,
                ]);
            }
        }

        $output .= $this->completeRestore('И еще {count, number} {count, plural, one{группа} few{группа} other{групп}}...', count($this->list) - 1, 'delivery-zipgroup');

        return $output;
    }

    /**
     * Prepare
     */
    private function prepare()
    {
        if (!is_array($this->list)) {
            $this->list = [$this->list];
        }

        if (!is_array($this->not)) {
            $this->not = [$this->not];
        }
    }
}
