<?php

namespace app\modules\order\models\search\filters;

use app\models\Product;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\models\Order;
use app\modules\order\models\MarketingListOrder;
use app\modules\order\models\OrderProduct;
use app\modules\order\models\OrderStatus;
use app\modules\order\widgets\orderFilters\filters\MarketingListFilter as MarketingListFilterWidget;
use app\modules\report\extensions\Query;
use Yii;
use yii\db\Expression;

class MarketingListFilter extends Filter
{
    const PRODUCT_ENDED = 'ended';
    const PRODUCT_ENDED_NOT_LIST = 'ended_notlist';

    /**
     * @var array
     */
    public $marketinglist;

    /**
     * @var array
     */
    public $marketinglists = [];

    public function init()
    {
        parent::init();

        $this->marketinglists = [
            self::PRODUCT_ENDED_NOT_LIST => Yii::t('common', 'У клиента закончились проданные товары и заказ не добавлен ни в один в маркетинговый лист'),
            self::PRODUCT_ENDED => Yii::t('common', 'У клиента закончились проданные товары'),
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['marketinglist', 'in', 'range' => array_keys($this->marketinglists), 'allowArray' => true]
        ];
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $output = '';

        $this->prepare();

        foreach ($this->marketinglist as $index => $marketinglist) {
            if ($marketinglist) {
                $model = new MarketingListFilter(['marketinglist' => $marketinglist]);

                $output .= MarketingListFilterWidget::widget([
                    'form' => $form,
                    'model' => $model,
                    'visible' => $index >= 1 ? false : true,
                    'formGroupMargin' => count($this->marketinglist) > 1 ? false : true,
                ]);
            }
        }

        $output .= $this->completeRestore('И еще {count, number} {count, plural, one{маркетинг} few{маркетинг} other{маркетинг}}...',
            count($this->marketinglist) - 1, 'marketing-list');

        return $output;
    }

    /**
     * @param \yii\db\ActiveQuery $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate()) {
            $this->prepare();

            foreach ($this->marketinglist as $index => $marketinglist) {
                if ($marketinglist) {

                    $daysSubQuery = new Query();
                    $daysSubQuery->select([
                            'order_id' => OrderProduct::tableName() . '.order_id',
                            'days' => 'sum(' .OrderProduct::tableName(). '.quantity) * ' .Product::tableName(). '.use_days',
                            'approved_at' => DeliveryRequest::tableName() . '.approved_at'
                        ])
                        ->from(OrderProduct::tableName())
                        ->leftJoin(DeliveryRequest::tableName(), OrderProduct::tableName() . '.order_id = ' . DeliveryRequest::tableName() . '.order_id')
                        ->leftJoin(Product::tableName(), OrderProduct::tableName() . '.product_id = ' . Product::tableName() . '.id')
                        ->where(DeliveryRequest::tableName() . '.approved_at > 0')
                        ->groupBy([
                            OrderProduct::tableName(). '.order_id',
                            OrderProduct::tableName() . '.product_id'])
                        ->having(
                            new Expression('(approved_at + ' . 86400 . ' * days) < ' . time())
                        );

                    $query->innerJoin(['used_days' => $daysSubQuery],
                        'used_days.order_id = ' . Order::tableName() . '.id');

                    $query->andWhere([Order::tableName() . '.status_id' => [OrderStatus::STATUS_DELIVERY_BUYOUT, OrderStatus::STATUS_FINANCE_MONEY_RECEIVED]]);

                    if ($marketinglist == self::PRODUCT_ENDED_NOT_LIST) {

                        $mlSubQuery = new Query();
                        $mlSubQuery ->select('old_order_id')
                            ->from(MarketingListOrder::tableName());

                        $query->leftJoin(['marketing_lists' => $mlSubQuery],
                            'marketing_lists.old_order_id = ' . Order::tableName() . '.id');
                        $query->andWhere(['marketing_lists.old_order_id' => null]);

                    }
                }
            }
        }
    }

    /**
     * Prepare
     */
    private function prepare()
    {
        if (!is_array($this->marketinglist)) {
            $this->marketinglist = [$this->marketinglist];
        }

        if (!is_array($this->not)) {
            $this->not = [$this->not];
        }
    }
}
