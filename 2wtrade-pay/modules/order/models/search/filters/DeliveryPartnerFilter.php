<?php

namespace app\modules\order\models\search\filters;


use app\modules\delivery\models\DeliveryPartner;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\models\Order;
use Yii;
use app\modules\order\widgets\orderFilters\filters\DeliveryPartnerFilter as DeliveryPartnerFilterWidget;

class DeliveryPartnerFilter extends Filter
{
    public $not;

    /**
     * @var array
     */
    public $partner;

    /**
     * @var array
     */
    public $partners = [];

    /**
     * @var integer
     */
    public $countryId;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if (is_null($this->countryId)) {
            $this->countryId = Yii::$app->user->getCountry()->id;
        }

        $this->partners = DeliveryPartner::find()
            ->byCountryId($this->countryId)
            ->orderBy([DeliveryPartner::tableName() . '.name' => SORT_ASC])
            ->collection();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['partner', 'in', 'range' => array_keys($this->partners), 'allowArray' => true, 'skipOnEmpty' => false],
            ['not', 'in', 'range' => array(0, 1), 'allowArray' => true],
        ];
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $output = '';

        $this->prepare();

        foreach ($this->partner as $index => $partner) {
            if ($partner) {
                $model = new DeliveryPartnerFilter(['partner' => $partner]);

                if (isset($this->not[$index]) && $this->not[$index] == 1) {
                    $model->not = 1;
                } else {
                    $model->not = 0;
                }

                $output .= DeliveryPartnerFilterWidget::widget([
                    'form' => $form,
                    'model' => $model,
                    'visible' => $index >= 1 ? false : true,
                    'formGroupMargin' => count($this->partner) > 1 ? false : true,
                ]);
            }
        }

        $output .= $this->completeRestore('И еще {count, number} {count, plural, one{партнер} few{партнера} other{партнеров}}...',
            count($this->partner) - 1, 'partner');

        return $output;
    }

    /**
     * @param \yii\db\ActiveQuery $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate()) {
            $this->prepare();
            $query->joinWith('deliveryRequest');

            foreach ($this->partner as $index => $partner) {
                $query->groupBy(Order::tableName() . '.id');

                if (isset($this->not[$index]) && $this->not[$index] == 1) {
                    $query->andFilterWhere(['!=', DeliveryRequest::tableName() . '.delivery_partner_id', $partner]);
                } else {
                    $query->andFilterWhere([DeliveryRequest::tableName() . '.delivery_partner_id' => $partner]);
                }
            }
        }
    }

    /**
     * Prepare
     */
    private function prepare()
    {
        if (!is_array($this->partner)) {
            $this->partner = [$this->partner];
        }

        if (!is_array($this->not)) {
            $this->not = [$this->not];
        }
    }
}
