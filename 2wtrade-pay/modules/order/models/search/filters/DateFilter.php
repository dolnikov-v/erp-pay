<?php

namespace app\modules\order\models\search\filters;

use app\modules\callcenter\models\CallCenter;
use app\modules\callcenter\models\CallCenterCheckRequest;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\models\Lead;
use app\modules\order\models\Order;
use app\models\Timezone;
use Yii;
use yii\base\InvalidParamException;
use yii\db\Expression;
use yii\db\Query;

/**
 * Class DateFilter
 * @package app\modules\order\models\filters
 *
 * @property string $filteredAttribute
 */
class DateFilter extends Filter
{
    const TYPE_CREATED_AT = 'created_at';
    const TYPE_UPDATED_AT = 'updated_at';
    const TYPE_CALL_CENTER_SENT_AT = 'c_sent_at';
    const TYPE_CALL_CENTER_UPDATED_AT = 'c_update_at';
    const TYPE_CALL_CENTER_APPROVED_AT = 'c_approved_at';
    const TYPE_DELIVERY_CREATED_AT = 'd_created_at';
    const TYPE_DELIVERY_SENT_AT = 'd_sent_at';
    const TYPE_DELIVERY_FIRST_SENT_AT = 'd_first_sent_at';
    const TYPE_DELIVERY_SENT_CLARIFICATION_AT = 'sent_clarification_at';
    const TYPE_DELIVERY_ACCEPTED_AT = 'd_accepted_at';
    const TYPE_DELIVERY_APPROVED_AT = 'd_approved_at';
    const TYPE_DELIVERY_RETURNED_AT = 'd_returned_at';
    const TYPE_DELIVERY_PAID_AT = 'd_paid_at';
    const TYPE_DELIVERY_TIME_FROM = 'delivery_time_from';
    const TYPE_ORDER_AND_DELIVERY_APPROVE = 'order_and_delivery_approve';
    const TYPE_CALL_CENTER_ANALYSIS_TRASH = 'analysis_trash_at';
    const TYPE_DELIVERY_SENT_AT_NOT_EMPTY = 'd_sent_at_not_empty';
    const TYPE_TS_SPAWN = 'ts_spawn';
    const TYPE_CHECK_SENT_AT = 'check_sent_at';

    public $dateType;
    public $dateFrom;
    public $dateTo;
    public $timezone = Timezone::DEFAULT_OFFSET;

    public $dateTypes = [];
    public $timezones = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->dateTypes = [
            '' => Yii::t('common', 'Игнорировать'),
            self::TYPE_CREATED_AT => Yii::t('common', 'Дата создания'),
            self::TYPE_UPDATED_AT => Yii::t('common', 'Дата обновления'),
            self::TYPE_CALL_CENTER_SENT_AT => Yii::t('common', 'Дата отправки (колл-центр)'),
            // когда заказ отправили в КЦ
            self::TYPE_CALL_CENTER_UPDATED_AT => Yii::t('common', 'Дата последнего обновления информации из КЦ'),
            //Дата последнего обновления информации из КЦ
            self::TYPE_CALL_CENTER_APPROVED_AT => Yii::t('common', 'Дата одобрения (колл-центр)'),
            // когда КЦ одобрил заказ
            self::TYPE_DELIVERY_CREATED_AT => Yii::t('common', 'Дата создания (курьерка)'),
            // когда создалась заявка в курьерке
            self::TYPE_DELIVERY_SENT_AT => Yii::t('common', 'Дата отправки (курьерка)'),
            // когда заказ отправили в курьерку в последний раз
            self::TYPE_DELIVERY_FIRST_SENT_AT => Yii::t('common', 'Дата первой отправки (курьерка)'),
            // когда заказ отправили в курьерку в первый раз
            self::TYPE_DELIVERY_SENT_CLARIFICATION_AT => Yii::t('common', 'Дата отправки на уточнение (курьерка)'),
            // когда отправили на уточнение просроченный заказ в курьерку
            self::TYPE_DELIVERY_ACCEPTED_AT => Yii::t('common', 'Дата принятия (курьерка)'),
            // когда курьерка приняла заказ, т.е. есть товар на складе и курьерка готова отправлять
            self::TYPE_DELIVERY_APPROVED_AT => Yii::t('common', 'Дата фактической доставки (курьерка)'),
            // когда получатель выкупил заказ
            self::TYPE_DELIVERY_RETURNED_AT => Yii::t('common', 'Дата возврата (курьерка)'),
            // когда курьерка вернула заказ
            self::TYPE_DELIVERY_PAID_AT => Yii::t('common', 'Дата оплаты (курьерка)'),
            // когда мы получили оплату
            self::TYPE_DELIVERY_TIME_FROM => Yii::t('common', 'Дата предполагаемой доставки'),
            // дата доставки
            self::TYPE_ORDER_AND_DELIVERY_APPROVE => Yii::t('common',
                'Дата утверждения(курьерка) или дата создания(пей)'),
            // когда КЦ проанализировал трэши
            self::TYPE_CALL_CENTER_ANALYSIS_TRASH => Yii::t('common', 'Дата проверки треша (колл-центр)'),
            self::TYPE_DELIVERY_SENT_AT_NOT_EMPTY => Yii::t('common', 'Дата передачи заказа в курьерку'),
            self::TYPE_TS_SPAWN => Yii::t('common', 'Дата создания у партнера (лид)'),
            self::TYPE_CHECK_SENT_AT => Yii::t('common', 'Дата отправки на обзвон'),
        ];

        $this->timezones = Timezone::find()->customCollection('time_offset', 'name');
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['dateType', 'in', 'range' => array_keys($this->dateTypes)],
            [['dateFrom', 'dateTo', 'timezone'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'dateType' => Yii::t('common', 'Тип даты'),
            'dateFrom' => Yii::t('common', 'Период'),
            'dateTo' => Yii::t('common', 'Период'),
            'timezone' => Yii::t('common', 'Временная зона'),
        ];
    }

    /**
     * @param \yii\db\ActiveQuery $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate()) {
            if ($this->dateType) {
                $this->applyEntityQueryJoin($query, $this->dateType);
                $dateFrom = strtotime('midnight', Yii::$app->formatter->asTimestamp($this->dateFrom)) - $this->timezone;
                $dateTo = strtotime('midnight', Yii::$app->formatter->asTimestamp($this->dateTo)) - $this->timezone;
                if ($this->dateType == self::TYPE_ORDER_AND_DELIVERY_APPROVE) {
                    if ($this->dateFrom && $dateFrom) {
                        $query->andWhere([
                            'or',
                            [
                                'and',
                                ['>=', DeliveryRequest::tableName() . '.approved_at', $dateFrom],
                                ['is not', DeliveryRequest::tableName() . '.approved_at', null],
                                ['>', DeliveryRequest::tableName() . '.approved_at', 0],
                                [
                                    'is not',
                                    'DATE_FORMAT(FROM_UNIXTIME(' . DeliveryRequest::tableName() . '.approved_at), "%m/%d/%Y")',
                                    null
                                ],
                            ],
                            [
                                'and',
                                [
                                    'or',
                                    ['is', DeliveryRequest::tableName() . '.approved_at', null],
                                    ['<=', DeliveryRequest::tableName() . '.approved_at', 0],
                                    [
                                        'is',
                                        'DATE_FORMAT(FROM_UNIXTIME(' . DeliveryRequest::tableName() . '.approved_at), "%m/%d/%Y")',
                                        null
                                    ],
                                ],
                                ['>=', Order::tableName() . '.created_at', $dateFrom],
                            ]
                        ]);
                    }

                    if ($this->dateTo && $dateTo) {
                        $query->andWhere([
                            'or',
                            [
                                'and',
                                ['<=', DeliveryRequest::tableName() . '.approved_at', $dateTo],
                                ['is not', DeliveryRequest::tableName() . '.approved_at', null],
                                ['>', DeliveryRequest::tableName() . '.approved_at', 0],
                                [
                                    'is not',
                                    'DATE_FORMAT(FROM_UNIXTIME(' . DeliveryRequest::tableName() . '.approved_at), "%m/%d/%Y")',
                                    null
                                ],
                            ],
                            [
                                'and',
                                [
                                    'or',
                                    ['is', DeliveryRequest::tableName() . '.approved_at', null],
                                    ['<=', DeliveryRequest::tableName() . '.approved_at', 0],
                                    [
                                        'is',
                                        'DATE_FORMAT(FROM_UNIXTIME(' . DeliveryRequest::tableName() . '.approved_at), "%m/%d/%Y")',
                                        null
                                    ],
                                ],
                                ['<=', Order::tableName() . '.created_at', $dateTo],
                            ]
                        ]);
                    }
                } else {
                    if ($this->dateFrom && $dateFrom) {
                        $query->andFilterWhere(['>=', $this->getFilteredAttribute(), $dateFrom]);
                    }
                    if ($this->dateTo && $dateTo) {
                        $query->andFilterWhere(['<=', $this->getFilteredAttribute(), $dateTo + 86399]);
                    }
                }
            }
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     */
    public function restore($form)
    {

    }

    /**
     * @return string
     */
    protected function getFilteredAttribute()
    {
        $attributes = [
            self::TYPE_CREATED_AT => Order::tableName() . '.created_at',
            self::TYPE_UPDATED_AT => Order::tableName() . '.updated_at',
            self::TYPE_CALL_CENTER_SENT_AT => CallCenterRequest::tableName() . '.sent_at',
            self::TYPE_CALL_CENTER_UPDATED_AT => $dateTypes = "COALESCE(" .
                CallCenterRequest::tableName() . ".cc_update_response_at," .
                CallCenterRequest::tableName() . ".cc_send_response_at," .
                CallCenterRequest::tableName() . ".sent_at," .
                CallCenterRequest::tableName() . ".created_at)",
            self::TYPE_CALL_CENTER_APPROVED_AT => CallCenterRequest::tableName() . '.approved_at',
            self::TYPE_DELIVERY_CREATED_AT => DeliveryRequest::tableName() . '.created_at',
            self::TYPE_DELIVERY_SENT_AT => 'COALESCE(' . DeliveryRequest::tableName() . '.second_sent_at,' . DeliveryRequest::tableName() . '.sent_at)',
            self::TYPE_DELIVERY_FIRST_SENT_AT => DeliveryRequest::tableName() . '.sent_at',
            self::TYPE_DELIVERY_SENT_CLARIFICATION_AT => DeliveryRequest::tableName() . '.sent_clarification_at',
            self::TYPE_DELIVERY_RETURNED_AT => DeliveryRequest::tableName() . '.returned_at',
            self::TYPE_DELIVERY_APPROVED_AT => DeliveryRequest::tableName() . '.approved_at',
            self::TYPE_DELIVERY_ACCEPTED_AT => DeliveryRequest::tableName() . '.accepted_at',
            self::TYPE_DELIVERY_PAID_AT => DeliveryRequest::tableName() . '.paid_at',
            self::TYPE_DELIVERY_TIME_FROM => Order::tableName() . '.delivery_time_from',
            self::TYPE_CALL_CENTER_ANALYSIS_TRASH => CallCenterRequest::tableName() . '.analysis_trash_at',
            self::TYPE_DELIVERY_SENT_AT_NOT_EMPTY => 'COALESCE(' . DeliveryRequest::tableName() . '.second_sent_at, ' . DeliveryRequest::tableName() . '.sent_at, ' . DeliveryRequest::tableName() . '.created_at)',
            self::TYPE_TS_SPAWN => Lead::tableName() . '.ts_spawn',
            self::TYPE_CHECK_SENT_AT => 'call_center_check_requests.sent_at',
        ];

        if (!array_key_exists($this->dateType, $attributes)) {
            throw new InvalidParamException(Yii::t('common', 'Неизвестный тип даты.'));
        }

        return $attributes[$this->dateType];
    }

    /**
     * @param \yii\db\ActiveQuery $query
     * @param string $entity
     */
    protected function applyEntityQueryJoin($query, $entity)
    {
        switch ($entity) {
            case self::TYPE_CALL_CENTER_ANALYSIS_TRASH:
            case self::TYPE_CALL_CENTER_APPROVED_AT:
            case self::TYPE_CALL_CENTER_SENT_AT:
            case self::TYPE_CALL_CENTER_UPDATED_AT:
                $query->joinWith('callCenterRequest');
                break;
            case self::TYPE_ORDER_AND_DELIVERY_APPROVE:
            case self::TYPE_DELIVERY_PAID_AT:
            case self::TYPE_DELIVERY_ACCEPTED_AT:
            case self::TYPE_DELIVERY_APPROVED_AT:
            case self::TYPE_DELIVERY_RETURNED_AT:
            case self::TYPE_DELIVERY_SENT_CLARIFICATION_AT:
            case self::TYPE_DELIVERY_SENT_AT:
            case self::TYPE_DELIVERY_FIRST_SENT_AT:
            case self::TYPE_DELIVERY_CREATED_AT:
            case self::TYPE_DELIVERY_SENT_AT_NOT_EMPTY:
                $query->joinWith('deliveryRequest');
                break;
            case self::TYPE_TS_SPAWN:
                $query->joinWith('lead');
                break;
            case self::TYPE_CHECK_SENT_AT:
                $query->joinWith('callCenterCheckRequests call_center_check_requests');
                break;
        }
    }
}
