<?php

namespace app\modules\order\models\search\filters;

use app\modules\callcenter\models\CallCenterRequest;
use app\modules\callcenter\models\CallCenterRequestCallData;
use app\modules\order\models\Order;
use app\modules\order\models\OrderLogisticList;
use app\modules\order\models\MarketingListOrder;
use Yii;
use app\modules\order\widgets\orderFilters\filters\NumberFilter as NumberFilterWidget;
use yii\base\InvalidParamException;
use yii\db\ActiveQuery;
use yii\db\Query;
use yii\helpers\ArrayHelper;


/**
 * Class NumberFilter
 * @package app\modules\order\models\filters
 */
class NumberFilter extends Filter
{
    const ENTITY_ID = 'id';
    const ENTITY_SOURCE = 'source';
    const ENTITY_CALL_CENTER = 'callcenter';
    const ENTITY_LIST = 'list';
    const ENTITY_MARKETING_LIST = 'marketing';
    const ENTITY_DUPLICATE_ORDER_ID = 'order.duplicate_order_id';
    const ENTITY_ORIGINAL_ORDER_ID = 'original.id';
    const ENTITY_LAST_FOREIGN_OPERATOR = 'last_foreign_operator';
    const ENTITY_CALLS_COUNT = 'calls_count';

    const CALLS_COUNT_ALIAS = 'ccrcd';
    const CALLS_COUNT_MAX = 9;

    /**
     * @var array
     */
    public $not;

    /**
     * @var array
     */
    public $entity;

    /**
     * @var array
     */
    public $number;

    /**
     * @var array
     */
    public $entities = [];

    /**
     * @var \app\models\User
     */
    public $specifiedUser;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $limitEntities = false;
        if (($this->specifiedUser instanceof \app\models\User && $this->specifiedUser->getIsDeliveryManager()) ||
            (isset(Yii::$app->user) && Yii::$app->user->getIsDeliveryManager())
        ) {
            $limitEntities = true;
        }

        if ($limitEntities) {
            $this->entities = [
                self::ENTITY_ID => Yii::t('common', 'Номер заказа'),
                self::ENTITY_CALL_CENTER => Yii::t('common', 'Номер в колл-центре'),
            ];
        } else {
            $this->entities = [
                self::ENTITY_ID => Yii::t('common', 'Номер заказа'),
                self::ENTITY_SOURCE => Yii::t('common', 'Номер в источнике'),
                self::ENTITY_CALL_CENTER => Yii::t('common', 'Номер в колл-центре'),
                self::ENTITY_LIST => Yii::t('common', 'Номер листа'),
                self::ENTITY_MARKETING_LIST => Yii::t('common', 'Номер маркетингового листа'),
                self::ENTITY_DUPLICATE_ORDER_ID => Yii::t('common', 'Номер дубликата заказа'),
                self::ENTITY_ORIGINAL_ORDER_ID => Yii::t('common', 'Номер оригигального заказа'),
                self::ENTITY_LAST_FOREIGN_OPERATOR => Yii::t('common', 'Внешний оператор'),
                self::ENTITY_CALLS_COUNT => Yii::t('common', 'Число звонков'),
            ];
        }
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['not', 'in', 'range' => array(0, 1), 'allowArray' => true],
            ['entity', 'in', 'range' => array_keys($this->entities), 'allowArray' => true],
            ['number', 'safe'],
        ];
    }

    /**
     * @param \yii\db\ActiveQuery $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate()) {
            $this->prepare();

            foreach ($this->entity as $index => $entity) {
                if (array_key_exists($entity, $this->entities) && array_key_exists($index, $this->number)) {
                    $operator = 'in';

                    if (isset($this->not[$index]) && $this->not[$index] == 1) {
                        $operator = 'not in';
                    }

                    $attribute = $this->getEntityAttribute($entity);
                    $this->applyEntityQueryJoin($query, $entity);

                    if ($entity == self::ENTITY_LIST) {
                        $lists = OrderLogisticList::findAll($this->number[$index]);
                        $ids = [];

                        foreach ($lists as $list) {
                            $ids = array_merge($ids, explode(',', $list->ids));
                        }

                        $query->andFilterWhere([$operator, $attribute, $ids]);
                    } elseif ($entity == self::ENTITY_MARKETING_LIST) {
                        //новые заказы полученные из маркетингового листа
                        $marketingListOrders = MarketingListOrder::find()
                            ->where(["marketing_list_id" => $this->number[$index]])
                            ->all();
                        $ids = ArrayHelper::getColumn($marketingListOrders, "new_order_id");
                        //нет заказов
                        if (!$ids) $ids[] = -1;
                        $query->andFilterWhere([$operator, $attribute, $ids]);
                    } else if ($entity == self::ENTITY_CALL_CENTER) {
                        $query->andFilterWhere([
                            'or',
                            [$operator, 'call_center_check_requests.foreign_id', $this->number[$index]],
                            [$operator, CallCenterRequest::tableName() . '.foreign_id', $this->number[$index]],
                        ]);
                    } else if ($entity == self::ENTITY_CALLS_COUNT) {


                        if (isset($this->number[$index][0]) && $this->number[$index][0] >= self::CALLS_COUNT_MAX) {
                            $this->number[$index][0] = self::CALLS_COUNT_MAX;
                            if ($operator == 'in') {
                                $operator = '>=';
                            } else {
                                $operator = '<';
                            }
                        }
                        $query->andFilterWhere([$operator, $attribute, $this->number[$index][0]]);

                    } else {
                        $query->andFilterWhere([$operator, $attribute, $this->number[$index]]);
                    }
                }
            }
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     * @throws \Exception
     */
    public function restore($form)
    {
        $output = '';

        $this->prepare();

        foreach ($this->entity as $index => $entity) {
            if (array_key_exists($entity, $this->entities) && array_key_exists($index, $this->number)) {
                if (empty($this->number[$index])) {
                    continue;
                }

                $model = new NumberFilter([
                    'entity' => $this->entity[$index],
                    'number' => implode(', ', $this->number[$index]),
                ]);

                if (isset($this->not[$index]) && $this->not[$index] == 1) {
                    $model->not = 1;
                } else {
                    $model->not = 0;
                }

                $output .= NumberFilterWidget::widget([
                    'form' => $form,
                    'model' => $model,
                    'visible' => $index >= 1 ? false : true,
                    'formGroupMargin' => count($this->entity) > 1 ? false : true,
                ]);
            }
        }

        $output .= $this->completeRestore('И еще {count, number} {count, plural, one{поиск} few{поиска} other{поисков}} по номеру...', count($this->entity) - 1, 'number');

        return $output;
    }

    /**
     * @param string $entity
     * @return string
     */
    protected function getEntityAttribute($entity)
    {
        $attribute = '';

        switch ($entity) {
            case self::ENTITY_ID:
            case self::ENTITY_LIST:
            case self::ENTITY_MARKETING_LIST:
                $attribute = Order::tableName() . '.id';
                break;
            case self::ENTITY_SOURCE:
                $attribute = Order::tableName() . '.foreign_id';
                break;
            case self::ENTITY_CALL_CENTER:
                $attribute = CallCenterRequest::tableName() . '.foreign_id';
                break;
            case self::ENTITY_DUPLICATE_ORDER_ID:
                $attribute = Order::tableName() . '.duplicate_order_id';
                break;
            case self::ENTITY_ORIGINAL_ORDER_ID:
                $attribute = 'original.id';
                break;
            case self::ENTITY_LAST_FOREIGN_OPERATOR:
                $attribute = CallCenterRequest::tableName() . '.last_foreign_operator';
                break;
            case self::ENTITY_CALLS_COUNT:
                $attribute = 'IFNULL(' . self::CALLS_COUNT_ALIAS . '.calls_count' . ', 0)';
                break;
        }

        if (empty($attribute)) {
            throw new InvalidParamException(Yii::t('common', 'Неизвестное поле для поиска.'));
        }

        return $attribute;
    }

    /**
     * @param \yii\db\ActiveQuery $query
     * @param string $entity
     */
    protected function applyEntityQueryJoin($query, $entity)
    {
        switch ($entity) {
            case self::ENTITY_CALL_CENTER:
                $query->joinWith('callCenterCheckRequests call_center_check_requests');
            case self::ENTITY_LAST_FOREIGN_OPERATOR:
                $query->joinWith('callCenterRequest');
                break;
            case self::ENTITY_CALLS_COUNT:
                $query->joinWith('callCenterRequest');

                if ($this->shouldJoin($query, self::CALLS_COUNT_ALIAS)) {
                    $subQuery = new Query();
                    $subQuery->select([
                        'calls_count' => 'COUNT(*)',
                        'call_center_request_id' => 'call_center_request_id',
                    ])
                        ->from(CallCenterRequestCallData::tableName())
                        ->leftJoin(CallCenterRequest::tableName(), CallCenterRequest::tableName() . '.id = ' . CallCenterRequestCallData::tableName() . '.call_center_request_id')
                        ->groupBy(
                            CallCenterRequestCallData::tableName() . '.call_center_request_id'
                        );
                    $query->leftJoin([self::CALLS_COUNT_ALIAS => $subQuery], CallCenterRequest::tableName() . '.id=' . self::CALLS_COUNT_ALIAS . '.call_center_request_id');
                }

                break;
            case self::ENTITY_ORIGINAL_ORDER_ID:
                $query->joinWith('originalOrder');
                break;
        }
    }

    /**
     * Prepare
     */
    private function prepare()
    {
        if (!is_array($this->not)) {
            $this->not = [$this->not];
        }

        if (!is_array($this->entity)) {
            $this->entity = [$this->entity];
        }

        if (!is_array($this->number)) {
            $this->number = [$this->number];
        }

        foreach ($this->number as $key => $number) {
            if (is_string($number)) {
                $number = explode(',', $number);
                $this->number[$key] = array_map('intval', $number);
            }
        }
    }

    /**
     * @param ActiveQuery $query
     * @param string $joinName
     * @return bool
     */
    private function shouldJoin(ActiveQuery $query, string $joinName): bool
    {
        if ($query->join) {
            $joins = ArrayHelper::getColumn($query->join, '1');
            foreach ($joins as $table) {
                if (isset($table[$joinName])) {
                    return false;
                }
            }
        }
        return true;
    }
}
