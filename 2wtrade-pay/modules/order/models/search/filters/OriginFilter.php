<?php
namespace app\modules\order\models\search\filters;

use app\models\Source;
use app\modules\order\models\Order;
use app\modules\order\widgets\orderFilters\filters\OriginFilter as OriginFilterWidget;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class OriginFilter
 * @package app\modules\order\models\search\filters
 */
class OriginFilter extends Filter
{
    public $not;

    /**
     * @var array
     */
    public $origin;

    /**
     * @var array
     */
    public $origins = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->origins = Source::find()->collection();
    }
    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['origin', 'in', 'range' => array_keys($this->origins), 'allowArray' => true, 'skipOnEmpty' => false],
            ['not', 'in', 'range' => array(0, 1), 'allowArray' => true],
        ];
    }

    /**
     * @param \yii\db\ActiveQuery $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate()) {
            $this->prepare();

            $conditions = [];
            foreach ($this->origin as $index => $origin) {
                if (isset($this->not[$index]) && $this->not[$index] == 1) {
                    $query->andWhere(['or', [Order::tableName() . '.source_id' => null], ['!=', Order::tableName() . '.source_id', $origin]]);
                } else {
                    $conditions[] = [Order::tableName() . '.source_id' => $origin];
                }
            }
            if (count($conditions) > 1) {
                array_unshift($conditions, 'or');
            } else if (!empty($conditions)) {
                $conditions = array_shift($conditions);
            }
            if (!empty($conditions)) {
                $query->andWhere($conditions);
            }
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $output = '';

        $this->prepare();

        foreach ($this->origin as $index => $origin) {
            if ($origin) {
                $model = new OriginFilter(['origin' => $origin]);

                if (isset($this->not[$index]) && $this->not[$index] == 1) {
                    $model->not = 1;
                } else {
                    $model->not = 0;
                }

                $output .= OriginFilterWidget::widget([
                    'form' => $form,
                    'model' => $model,
                    'visible' => $index >= 1 ? false : true,
                    'formGroupMargin' => count($this->origin) > 1 ? false : true,
                ]);
            }
        }

        $output .= $this->completeRestore('И еще {count, number} {count, plural, one{источник} few{источника} other{источников}}...', count($this->origin) - 1, 'origin');

        return $output;
    }

    /**
     * Prepare
     */
    private function prepare()
    {
        if (!is_array($this->origin)) {
            $this->origin = [$this->origin];
        }

        if (!is_array($this->not)) {
            $this->not = [$this->not];
        }
    }
}
