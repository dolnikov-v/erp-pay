<?php

namespace app\modules\order\models\search\filters;

use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\models\Order;
use app\modules\order\widgets\orderFilters\filters\DeliveryFilter as DeliveryFilterWidget;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class DeliveryFilter
 * @package app\modules\order\models\search\filters
 */
class DeliveryFilter extends Filter
{
    public $not;

    /**
     * @var array
     */
    public $delivery;

    /**
     * @var array
     */
    public $deliveries = [];

    /**
     * @var integer
     */
    public $countryId;

    /**
     * @var \app\models\User
     */
    public $specifiedUser;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if (is_null($this->countryId)) {
            $this->countryId = Yii::$app->user->getCountry()->id;
        }

        // роль имплант (пользователь курьерской службы)
        $deliveryIds = [];
        if ($this->specifiedUser instanceof \app\models\User) {
            if ($this->specifiedUser->getIsImplant()) {
                $deliveryIds = ArrayHelper::getColumn($this->specifiedUser->getDeliveries()->asArray()->all(), 'id');
            }
        } else {
            if (Yii::$app->user->getIsImplant()) {
                $deliveryIds = Yii::$app->user->getDeliveriesIds();
            }
        }

        $this->deliveries = Delivery::find()
            ->byCountryId($this->countryId)
            ->byId($deliveryIds)
            ->collection();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['delivery', 'in', 'range' => array_keys($this->deliveries), 'allowArray' => true, 'skipOnEmpty' => false],
            ['not', 'in', 'range' => array(0, 1), 'allowArray' => true],
        ];
    }

    /**
     * @param \yii\db\ActiveQuery $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate()) {
            $this->prepare();
            $query->joinWith('deliveryRequest');

            $condition = [];
            foreach ($this->delivery as $index => $delivery) {
                $query->groupBy(Order::tableName() . '.id');
                $delivery = (int)$delivery;

                if ($delivery) {
                    if (isset($this->not[$index]) && $this->not[$index] == 1) {
                        $condition[] = [
                            'or',
                            [
                                'and',
                                DeliveryRequest::tableName() . '.delivery_id is not null',
                                DeliveryRequest::tableName() . '.delivery_id != ' . $delivery
                            ],
                            [
                                'and',
                                DeliveryRequest::tableName() . '.delivery_id is null',
                                Order::tableName() . '.pending_delivery_id != ' . $delivery
                            ]
                        ];
                    } else {
                        $condition[] = [
                            'or',
                            [
                                'and',
                                DeliveryRequest::tableName() . '.delivery_id is not null',
                                DeliveryRequest::tableName() . '.delivery_id = ' . $delivery
                            ],
                            [
                                'and',
                                DeliveryRequest::tableName() . '.delivery_id is null',
                                Order::tableName() . '.pending_delivery_id = ' . $delivery
                            ]
                        ];
                    }
                }
            }
            if (count($condition) > 0) {
                if (count($condition) > 1) {
                    array_unshift($condition, 'or');
                } else {
                    $condition = $condition[0];
                }
                $query->andFilterWhere($condition);
            }
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $output = '';

        $this->prepare();

        foreach ($this->delivery as $index => $delivery) {
            if ($delivery) {
                $model = new DeliveryFilter(['delivery' => $delivery]);

                if (isset($this->not[$index]) && $this->not[$index] == 1) {
                    $model->not = 1;
                } else {
                    $model->not = 0;
                }

                $output .= DeliveryFilterWidget::widget([
                    'form' => $form,
                    'model' => $model,
                    'visible' => $index >= 1 ? false : true,
                    'formGroupMargin' => count($this->delivery) > 1 ? false : true,
                ]);
            }
        }

        $output .= $this->completeRestore('И еще {count, number} {count, plural, one{служба доставки} few{службы доставки} other{служб доставок}}...',
            count($this->delivery) - 1, 'delivery');

        return $output;
    }

    /**
     * Prepare
     */
    private function prepare()
    {
        if (!is_array($this->delivery)) {
            $this->delivery = [$this->delivery];
        }

        if (!is_array($this->not)) {
            $this->not = [$this->not];
        }
    }
}
