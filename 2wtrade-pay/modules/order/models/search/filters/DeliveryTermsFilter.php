<?php
namespace app\modules\order\models\search\filters;

use app\modules\delivery\models\DeliveryRequest;
use Yii;
use app\modules\order\widgets\orderFilters\filters\DeliveryTermsFilter as DeliveryTermsFilterWidget;
use yii\base\InvalidParamException;

/**
 * Class DeliveryTermsFilter
 * @package app\modules\order\models\filters
 */
class DeliveryTermsFilter extends Filter
{
    const ENTITY_HOURS = 3600;
    const ENTITY_DAYS = 86400;

    /**
     * @var array
     */
    public $not;

    /**
     * @var array
     */
    public $entity;

    /**
     * @var array
     */
    public $deliveryTermsFilter;

    /**
     * @var array
     */
    public $deliveryTerms;

    /**
     * @var array
     */
    public $entities = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->entities = [
            self::ENTITY_HOURS => Yii::t('common', 'Часов'),
            self::ENTITY_DAYS => Yii::t('common', 'Дней'),
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['not', 'in', 'range' => array(0, 1), 'allowArray' => true],
            ['entity', 'in', 'range' => array_keys($this->entities), 'allowArray' => true, 'skipOnEmpty' => false],
            ['deliveryTerms', 'safe'],
        ];
    }

    /**
     * @param \yii\db\ActiveQuery $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate()) {
            $this->prepare();
            $query->joinWith('deliveryRequest');

            foreach ($this->entity as $index => $entity) {
                if (array_key_exists($entity, $this->entities) && array_key_exists($index, $this->deliveryTerms)) {
                    $operator = '>';

                    if (isset($this->not[$index]) && $this->not[$index] == 1) {
                        $operator = '<';
                    }

                    $attribute = $this->getEntityAttribute($entity);
                    if (isset($this->deliveryTerms[$index][0])) {
                        $seconds = $entity * $this->deliveryTerms[$index][0];
                        $query->andFilterWhere([$operator, $attribute, $seconds]);
                    }
                }
            }
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     * @throws \Exception
     */
    public function restore($form)
    {
        $output = '';

        $this->prepare();

        foreach ($this->entity as $index => $entity) {
            if (array_key_exists($entity, $this->entities) && array_key_exists($index, $this->deliveryTerms)) {
                if (empty($this->deliveryTerms[$index])) {
                    continue;
                }

                $model = new DeliveryTermsFilter([
                    'entity' => $this->entity[$index],
                    'deliveryTerms' => implode(', ', $this->deliveryTerms[$index]),
                ]);

                if (isset($this->not[$index]) && $this->not[$index] == 1) {
                    $model->not = 1;
                } else {
                    $model->not = 0;
                }

                $output .= DeliveryTermsFilterWidget::widget([
                    'form' => $form,
                    'model' => $model,
                    'visible' => $index >= 1 ? false : true,
                    'formGroupMargin' => count($this->entity) > 1 ? false : true,
                ]);
            }
        }

        $output .= $this->completeRestore('И еще {count, number} {count, plural, one{поиск} few{поиска} other{поисков}} по номеру...',
            count($this->entity) - 1, 'deliveryTerms');

        return $output;
    }

    /**
     * @param string $entity
     * @return string
     */
    protected function getEntityAttribute($entity)
    {
        $attribute = '';

        switch ($entity) {
            case self::ENTITY_HOURS:
            case self::ENTITY_DAYS:
                $attribute = DeliveryRequest::tableName() . '.approved_at-' . DeliveryRequest::tableName() . '.accepted_at';
                break;
        }

        if (empty($attribute)) {
            throw new InvalidParamException(Yii::t('common', 'Неизвестное поле для поиска.'));
        }

        return $attribute;
    }

    /**
     * Prepare
     */
    private function prepare()
    {
        if (!is_array($this->not)) {
            $this->not = [$this->not];
        }

        if (!is_array($this->entity)) {
            $this->entity = [$this->entity];
        }

        if (!is_array($this->deliveryTerms)) {
            $this->deliveryTerms = [$this->deliveryTerms];
        }

        foreach ($this->deliveryTerms as $key => $deliveryTerms) {
            if (!empty($deliveryTerms) && is_string($deliveryTerms)) {
                $deliveryTerms = explode(',', $deliveryTerms);
                $this->deliveryTerms[$key] = array_map('intval', $deliveryTerms);
            }
        }


    }
}
