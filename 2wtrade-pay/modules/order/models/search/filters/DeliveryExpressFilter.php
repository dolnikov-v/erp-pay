<?php

namespace app\modules\order\models\search\filters;

use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\models\OrderExpressDelivery;
use app\modules\order\widgets\orderFilters\filters\DeliveryExpressFilter as DeliveryExpressFilterWidget;
use Yii;

class DeliveryExpressFilter extends Filter
{
    const YES = 1;

    public $not;

    /**
     * @var array
     */
    public $list;

    /**
     * @var array
     */
    public $lists = [];

    public function init()
    {
        parent::init();

        $this->lists = [
            self::YES => Yii::t('common', 'Да'),
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['list', 'in', 'range' => array_keys($this->lists), 'allowArray' => true],
            ['not', 'in', 'range' => array(0, 1), 'allowArray' => true],
        ];
    }

    /**
     * @param \yii\db\ActiveQuery $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate()) {
            $this->prepare();

            foreach ($this->list as $index => $list) {
                if ($list) {
                    $query->joinWith('orderExpressDelivery');
                    if (isset($this->not[$index]) && $this->not[$index] == 1) {
                        $query->andWhere([OrderExpressDelivery::tableName() . ".id" => null]);
                    } else {
                        $query->andWhere(['is not', OrderExpressDelivery::tableName() . ".id", null]);
                    }
                }
            }
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $output = '';

        $this->prepare();

        foreach ($this->list as $index => $list) {
            if ($list) {
                $model = new DeliveryExpressFilter(['list' => $list]);

                if (isset($this->not[$index]) && $this->not[$index] == 1) {
                    $model->not = 1;
                } else {
                    $model->not = 0;
                }

                $output .= DeliveryExpressFilterWidget::widget([
                    'form' => $form,
                    'model' => $model,
                    'visible' => $index >= 1 ? false : true,
                    'formGroupMargin' => count($this->list) > 1 ? false : true,
                ]);
            }
        }

        $output .= $this->completeRestore('И еще {count, number} {count, plural, one{экспресс} few{экспресс} other{экспресс}}...',
            count($this->list) - 1, 'list');

        return $output;
    }

    /**
     * Prepare
     */
    private function prepare()
    {
        if (!is_array($this->list)) {
            $this->list = [$this->list];
        }

        if (!is_array($this->not)) {
            $this->not = [$this->not];
        }
    }
}
