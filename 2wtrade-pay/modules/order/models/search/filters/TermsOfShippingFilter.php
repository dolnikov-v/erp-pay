<?php
namespace app\modules\order\models\search\filters;

use app\modules\access\widgets\Delivery;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\order\models\Order;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\models\OrderLogisticList;
use Yii;
use app\modules\order\widgets\orderFilters\filters\TermsOfShippingFilter as TermsOfShippingFilterWidget;
use app\modules\order\models\MarketingListOrder;
use yii\base\InvalidParamException;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * Class TermsOfShippingFilter
 * @package app\modules\order\models\filters
 */
class TermsOfShippingFilter extends Filter
{
    const ENTITY_HOURS = 3600;
    const ENTITY_DAYS = 86400;

    /**
     * @var array
     */
    public $not;

    /**
     * @var array
     */
    public $entity;

    /**
     * @var array
     */
    public $termsOfShippingFilter;

    /**
     * @var array
     */
    public $termsOfShipping;

    /**
     * @var array
     */
    public $entities = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->entities = [
            self::ENTITY_HOURS => Yii::t('common', 'Часов'),
            self::ENTITY_DAYS => Yii::t('common', 'Дней')
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['not', 'in', 'range' => array(0, 1), 'allowArray' => true],
            ['entity', 'in', 'range' => array_keys($this->entities), 'allowArray' => true, 'skipOnEmpty' => false],
            ['termsOfShipping', 'safe'],
        ];
    }

    /**
     * @param \yii\db\ActiveQuery $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate()) {
            $this->prepare();
            $query->joinWith('deliveryRequest');

            foreach ($this->entity as $index => $entity) {
                if (array_key_exists($entity, $this->entities) && array_key_exists($index, $this->termsOfShipping)) {
                    $operator = '>';

                    if (isset($this->not[$index]) && $this->not[$index] == 1) {
                        $operator = '<';
                    }

                    $attribute = $this->getEntityAttribute($entity);
                    $seconds = $entity * $this->termsOfShipping[$index][0];
                    $query->andFilterWhere([$operator, $attribute, $seconds]);
                }
            }
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     * @throws \Exception
     */
    public function restore($form)
    {
        $output = '';

        $this->prepare();

        foreach ($this->entity as $index => $entity) {
            if (array_key_exists($entity, $this->entities) && array_key_exists($index, $this->termsOfShipping)) {
                if (empty($this->termsOfShipping[$index])) {
                    continue;
                }

                $model = new TermsOfShippingFilter([
                    'entity' => $this->entity[$index],
                    'termsOfShipping' => implode(', ', $this->termsOfShipping[$index]),
                ]);

                if (isset($this->not[$index]) && $this->not[$index] == 1) {
                    $model->not = 1;
                } else {
                    $model->not = 0;
                }

                $output .= TermsOfShippingFilterWidget::widget([
                    'form' => $form,
                    'model' => $model,
                    'visible' => $index >= 1 ? false : true,
                    'formGroupMargin' => count($this->entity) > 1 ? false : true,
                ]);
            }
        }

        $output .= $this->completeRestore('И еще {count, number} {count, plural, one{поиск} few{поиска} other{поисков}} по номеру...', count($this->entity) - 1, 'termsOfShipping');

        return $output;
    }

    /**
     * @param string $entity
     * @return string
     */
    protected function getEntityAttribute($entity)
    {
        $attribute = '';

        switch ($entity) {
            case self::ENTITY_HOURS:
            case self::ENTITY_DAYS:
                $attribute = DeliveryRequest::tableName() . '.sent_at-' . Order::tableName() . '.created_at';
                break;
        }

        if (empty($attribute)) {
            throw new InvalidParamException(Yii::t('common', 'Неизвестное поле для поиска.'));
        }

        return $attribute;
    }


    /**
     * Prepare
     */
    private function prepare()
    {
        if (!is_array($this->not)) {
            $this->not = [$this->not];
        }

        if (!is_array($this->entity)) {
            $this->entity = [$this->entity];
        }

        if (!is_array($this->termsOfShipping)) {
            $this->termsOfShipping = [$this->termsOfShipping];
        }

        foreach ($this->termsOfShipping as $key => $termsOfShipping) {
            if (!empty($termsOfShipping) && is_string($termsOfShipping)) {
                $termsOfShipping = explode(',', $termsOfShipping);
                $this->termsOfShipping[$key] = array_map('intval', $termsOfShipping);
            }
        }


    }
}
