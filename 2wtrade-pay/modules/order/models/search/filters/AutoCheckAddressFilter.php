<?php

namespace app\modules\order\models\search\filters;

use app\modules\callcenter\models\CallCenterRequest;
use app\modules\order\widgets\orderFilters\filters\AutoCheckAddressFilter as AutoCheckAddressFilterWidget;

/**
 * Class AutoCheckAddressFilter
 * @package app\modules\order\models\search\filters
 */
class AutoCheckAddressFilter extends Filter
{
    public $not;

    /**
     * @var string | array
     */
    public $autoCheckAddress;

    /**
     * @var array
     */
    public $statuses = [];

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['autoCheckAddress', 'in', 'range' => array_keys($this->statuses), 'allowArray' => true, 'skipOnEmpty' => false],
            ['not', 'in', 'range' => array(0, 1), 'allowArray' => true],
        ];
    }

    /**
     *
     */
    public function init()
    {
        parent::init();
        $this->statuses = CallCenterRequest::autoCheckAddressLabels();
    }

    /**
     * @param \yii\db\ActiveQuery $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate()) {
            $this->prepare();
            $query->joinWith('callCenterRequest');

            $condition = ['or'];
            foreach ($this->autoCheckAddress as $index => $autoCheckAddress) {
                if (isset($this->not[$index]) && $this->not[$index] == 1) {
                    $query->andFilterWhere(['!=', CallCenterRequest::tableName() . '.autocheck_address', $autoCheckAddress]);
                } else {
                    $condition[] = [CallCenterRequest::tableName() . '.autocheck_address' => $autoCheckAddress];
                }
            }
            if ($condition <= 2) {
                array_shift($condition);
            }
            if (!empty($condition)) {
                $query->andFilterWhere($condition);
            }
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public
    function restore($form)
    {
        $output = '';

        $this->prepare();

        foreach ($this->autoCheckAddress as $index => $autoCheckAddress) {
            if ($autoCheckAddress) {
                $model = new AutoCheckAddressFilter(['autoCheckAddress' => $autoCheckAddress]);

                if (isset($this->not[$index]) && $this->not[$index] == 1) {
                    $model->not = 1;
                } else {
                    $model->not = 0;
                }

                $output .= AutoCheckAddressFilterWidget::widget([
                    'form' => $form,
                    'model' => $model,
                    'visible' => $index >= 1 ? false : true,
                    'formGroupMargin' => count($this->autoCheckAddress) > 1 ? false : true,
                ]);
            }
        }

        $output .= $this->completeRestore('И еще {count, number} {count, plural, one{статус} few{статуса} other{статусов}}...', count($this->autoCheckAddress) - 1, 'auto-check-address');

        return $output;
    }

    /**
     * Prepare
     */
    private
    function prepare()
    {
        if (!is_array($this->autoCheckAddress)) {
            $this->autoCheckAddress = [$this->autoCheckAddress];
        }

        if (!is_array($this->not)) {
            $this->not = [$this->not];
        }
    }
}