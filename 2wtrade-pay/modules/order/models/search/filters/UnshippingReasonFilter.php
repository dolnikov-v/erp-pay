<?php
namespace app\modules\order\models\search\filters;

use app\modules\catalog\models\UnBuyoutReasonMapping;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\delivery\models\DeliveryRequestUnBuyoutReason;
use app\modules\order\models\Order;
use app\modules\order\widgets\orderFilters\filters\UnshippingReasonFilter as UnshippingReasonFilterWidget;
use yii\helpers\ArrayHelper;

/**
 * Class UnshippingReasonFilter
 * @package app\modules\order\models\search\filters
 */
class UnshippingReasonFilter extends Filter
{
    public $not;

    /**
     * @var array
     */
    public $unshippingReason;

    /**
     * @var array
     */
    public $unshippingReasons = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->unshippingReasons = DeliveryRequest::getUnshippingReasons();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [
                'unshippingReason',
                'in',
                'range' => array_keys($this->unshippingReasons),
                'allowArray' => true,
                'skipOnEmpty' => false
            ],
            ['not', 'in', 'range' => array(0, 1), 'allowArray' => true],
        ];
    }

    /**
     * @param \yii\db\ActiveQuery $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate()) {

            $this->prepare();
            $query->joinWith('deliveryRequest');
            $query->joinWith('deliveryRequest.unBuyoutReasonMapping');

            foreach ($this->unshippingReason as $index => $reason) {
                $query->groupBy(Order::tableName() . '.id');

                $reasonMappingIds = ArrayHelper::getColumn(UnBuyoutReasonMapping::find()->where(['reason_id' => $reason])->all(), 'id');

                if (isset($this->not[$index]) && $this->not[$index] == 1) {
                    $query->andWhere(
                        ['or',
                            ['not in', DeliveryRequestUnBuyoutReason::tableName() . '.reason_mapping_id', $reasonMappingIds],
                            ['is', DeliveryRequestUnBuyoutReason::tableName() . '.reason_mapping_id', null]
                        ]
                    );
                } else {
                    if (!$reasonMappingIds) {
                        $reasonMappingIds = -1;
                    }
                    $query->andFilterWhere([DeliveryRequestUnBuyoutReason::tableName() . '.reason_mapping_id' => $reasonMappingIds]);
                }
            }
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $output = '';

        $this->prepare();

        foreach ($this->unshippingReason as $index => $reason) {
            if ($reason) {
                $model = new UnshippingReasonFilter(['unshippingReason' => $reason]);

                if (isset($this->not[$index]) && $this->not[$index] == 1) {
                    $model->not = 1;
                } else {
                    $model->not = 0;
                }

                $output .= UnshippingReasonFilterWidget::widget([
                    'form' => $form,
                    'model' => $model,
                    'visible' => $index >= 1 ? false : true,
                    'formGroupMargin' => count($this->unshippingReason) > 1 ? false : true,
                ]);
            }
        }

        $output .= $this->completeRestore('И еще {count, number} {count, plural, one{причина недоставки} few{причин недоставки} other{причин недоставки}}...',
            count($this->unshippingReason) - 1, 'unshippingReason');

        return $output;
    }

    /**
     * Prepare
     */
    private function prepare()
    {
        if (!is_array($this->unshippingReason)) {
            $this->unshippingReason = [$this->unshippingReason];
        }

        if (!is_array($this->not)) {
            $this->not = [$this->not];
        }
    }
}
