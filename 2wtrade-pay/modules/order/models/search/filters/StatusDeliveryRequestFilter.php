<?php
namespace app\modules\order\models\search\filters;

use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\models\Order;
use app\modules\order\widgets\orderFilters\filters\StatusDeliveryRequestFilter as StatusDeliveryRequestFilterWidget;

/**
 * Class StatusDeliveryRequestFilter
 * @package app\modules\order\models\search\filters
 */
class StatusDeliveryRequestFilter extends Filter
{
    public $not;

    /**
     * @var array
     */
    public $statusDeliveryRequest;

    /**
     * @var array
     */
    public $statusesDeliveryRequests = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->statusesDeliveryRequests = DeliveryRequest::getStatusesCollection();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [
                'statusDeliveryRequest',
                'in',
                'range' => array_keys($this->statusesDeliveryRequests),
                'allowArray' => true,
                'skipOnEmpty' => false
            ],
            ['not', 'in', 'range' => array(0, 1), 'allowArray' => true],
        ];
    }

    /**
     * @param \yii\db\ActiveQuery $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate()) {

            $this->prepare();
            $query->joinWith('deliveryRequest');

            foreach ($this->statusDeliveryRequest as $index => $status) {
                $query->groupBy(Order::tableName() . '.id');

                if (isset($this->not[$index]) && $this->not[$index] == 1) {
                    $query->andFilterWhere(['!=', DeliveryRequest::tableName() . '.status', $status]);
                } else {
                    $query->andFilterWhere([DeliveryRequest::tableName() . '.status' => $status]);
                }
            }
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $output = '';

        $this->prepare();

        foreach ($this->statusDeliveryRequest as $index => $status) {
            if ($status) {
                $model = new StatusDeliveryRequestFilter(['statusDeliveryRequest' => $status]);

                if (isset($this->not[$index]) && $this->not[$index] == 1) {
                    $model->not = 1;
                } else {
                    $model->not = 0;
                }

                $output .= StatusDeliveryRequestFilterWidget::widget([
                    'form' => $form,
                    'model' => $model,
                    'visible' => $index >= 1 ? false : true,
                    'formGroupMargin' => count($this->statusDeliveryRequest) > 1 ? false : true,
                ]);
            }
        }

        $output .= $this->completeRestore('И еще {count, number} {count, plural, one{статус службы доставки} few{статуса службы доставки} other{статусов службы доставки}}...',
            count($this->statusDeliveryRequest) - 1, 'statusDeliveryRequest');

        return $output;
    }

    /**
     * Prepare
     */
    private function prepare()
    {
        if (!is_array($this->statusDeliveryRequest)) {
            $this->statusDeliveryRequest = [$this->statusDeliveryRequest];
        }

        if (!is_array($this->not)) {
            $this->not = [$this->not];
        }
    }
}
