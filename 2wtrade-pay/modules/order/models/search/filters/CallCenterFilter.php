<?php
namespace app\modules\order\models\search\filters;

use app\modules\callcenter\models\CallCenter;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\order\models\Order;
use app\modules\order\widgets\orderFilters\filters\CallCenterFilter as CallCenterFilterWidget;
use Yii;

/**
 * Class CallCenterFilter
 * @package app\modules\order\models\search\filters
 */
class CallCenterFilter extends Filter
{
    public $not;

    /**
     * @var array
     */
    public $callCenter;

    /**
     * @var array
     */
    public $callCenters = [];

    /**
     * @var integer
     */
    public $countryId;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if (is_null($this->countryId)) {
            $this->countryId = Yii::$app->user->getCountry()->id;
        }
        $this->callCenters = CallCenter::find()
            ->byCountryId($this->countryId)
            ->active()
            ->collection();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['callCenter', 'in', 'range' => array_keys($this->callCenters), 'allowArray' => true, 'skipOnEmpty' => false],
            ['not', 'in', 'range' => array(0, 1), 'allowArray' => true],
        ];
    }

    /**
     * @param \yii\db\ActiveQuery $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate()) {
            $this->prepare();
            $query->joinWith('callCenterRequest');

            foreach ($this->callCenter as $index => $callCenter) {
                $query->groupBy(Order::tableName() . '.id');

                if (isset($this->not[$index]) && $this->not[$index] == 1) {
                    $query->andFilterWhere(['!=', CallCenterRequest::tableName() . '.call_center_id', $callCenter]);
                } else {
                    $query->andFilterWhere([CallCenterRequest::tableName() . '.call_center_id' => $callCenter]);
                }
            }
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $output = '';

        $this->prepare();

        foreach ($this->callCenter as $index => $callCenter) {
            if ($callCenter) {
                $model = new CallCenterFilter(['callCenter' => $callCenter]);

                if (isset($this->not[$index]) && $this->not[$index] == 1) {
                    $model->not = 1;
                } else {
                    $model->not = 0;
                }

                $output .= CallCenterFilterWidget::widget([
                    'form' => $form,
                    'model' => $model,
                    'visible' => $index >= 1 ? false : true,
                    'formGroupMargin' => count($this->callCenter) > 1 ? false : true,
                ]);
            }
        }

        $output .= $this->completeRestore('И еще {count, number} {count, plural, one{колл-центр} few{колл-центра} other{колл-центров}}...', count($this->callCenter) - 1, 'callCenter');

        return $output;
    }

    /**
     * Prepare
     */
    private function prepare()
    {
        if (!is_array($this->callCenter)) {
            $this->callCenter = [$this->callCenter];
        }

        if (!is_array($this->not)) {
            $this->not = [$this->not];
        }
    }
}
