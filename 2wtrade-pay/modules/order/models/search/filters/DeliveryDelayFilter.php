<?php

namespace app\modules\order\models\search\filters;

use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\models\Order;
use app\modules\order\widgets\orderFilters\filters\DeliveryDelayFilter as DeliveryDelayFilterWidget;
use Yii;

class DeliveryDelayFilter extends Filter
{
    const IS_DELAY = 1;

    public $not;

    /**
     * @var array
     */
    public $list;

    /**
     * @var array
     */
    public $lists = [];

    public function init()
    {
        parent::init();

        $this->lists = [
            self::IS_DELAY => Yii::t('common', 'Превышен срок доставки по контракту'),
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['list', 'in', 'range' => array_keys($this->lists), 'allowArray' => true],
            ['not', 'in', 'range' => array(0, 1), 'allowArray' => true],
        ];
    }

    /**
     * @param \yii\db\ActiveQuery $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate()) {
            $this->prepare();

            foreach ($this->list as $index => $list) {
                if ($list) {
                    $query->joinWith('deliveryRequest');
                    if (isset($this->not[$index]) && $this->not[$index] == 1) {
                        $isDelay = 0;
                    } else {
                        $isDelay = 1;
                    }
                    $query->andWhere([
                        'is_bad_delivery_request_sent_at(' . DeliveryRequest::tableName() . '.id, 
                            ' . Order::tableName() . '.customer_zip, 
                            ' . Order::tableName() . '.customer_city, 
                            ' . Order::tableName() . '.customer_province)' => $isDelay
                    ]);
                }
            }
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $output = '';

        $this->prepare();

        foreach ($this->list as $index => $list) {
            if ($list) {
                $model = new DeliveryDelayFilter(['list' => $list]);

                if (isset($this->not[$index]) && $this->not[$index] == 1) {
                    $model->not = 1;
                } else {
                    $model->not = 0;
                }

                $output .= DeliveryDelayFilterWidget::widget([
                    'form' => $form,
                    'model' => $model,
                    'visible' => $index >= 1 ? false : true,
                    'formGroupMargin' => count($this->list) > 1 ? false : true,
                ]);
            }
        }

        $output .= $this->completeRestore('И еще {count, number} {count, plural, one{задержка} few{задержка} other{задержка}}...',
            count($this->list) - 1, 'list');

        return $output;
    }

    /**
     * Prepare
     */
    private function prepare()
    {
        if (!is_array($this->list)) {
            $this->list = [$this->list];
        }

        if (!is_array($this->not)) {
            $this->not = [$this->not];
        }
    }
}
