<?php

namespace app\modules\order\models\search\filters;

use app\models\Product;
use app\modules\order\models\LeadProduct;
use app\modules\order\widgets\orderFilters\filters\LeadProductFilter as LeadProductFilterWidget;
use Yii;

/**
 * Class LeadProductFilter
 * @package app\modules\order\models\filters
 */
class LeadProductFilter extends Filter
{
    /** @var array */
    public $not;

    /** @var array */
    public $product;

    /** @var array */
    public $products = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->products = Product::getCollectionProducts();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['product', 'in', 'range' => array_keys($this->products), 'allowArray' => true, 'skipOnEmpty' => false],
            ['not', 'in', 'range' => array(0, 1), 'allowArray' => true],
        ];
    }

    /**
     * @param \yii\db\ActiveQuery $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate()) {
            $this->prepare();
            $query->joinWith('leadProducts');

            $in = [];
            $notIn = [];

            foreach ($this->product as $index => $product) {
                if ($product) {
                    if (isset($this->not[$index]) && $this->not[$index] == 1) {
                        $notIn[] = $product;
                    } else {
                        $in[] = $product;
                    }
                }
            }

            if ($in) {
                $query->andFilterWhere(['in', LeadProduct::tableName() . '.product_id', $in]);
            }

            if ($notIn) {
                $query->andFilterWhere(['not in', LeadProduct::tableName() . '.product_id', $notIn]);
            }
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     * @throws \Exception
     */
    public function restore($form)
    {
        $output = '';

        $this->prepare();

        foreach ($this->product as $index => $product) {
            if ($product) {
                $model = new LeadProductFilter(['product' => $product]);

                if (isset($this->not[$index]) && $this->not[$index] == 1) {
                    $model->not = 1;
                } else {
                    $model->not = 0;
                }

                $output .= LeadProductFilterWidget::widget([
                    'form' => $form,
                    'model' => $model,
                    'visible' => $index >= 1 ? false : true,
                    'formGroupMargin' => count($this->product) > 1 ? false : true,
                ]);
            }
        }

        $output .= $this->completeRestore('И еще {count, number} {count, plural, one{товар} few{товара} other{товаров}}...', count($this->product) - 1, 'lead-product');

        return $output;
    }

    /**
     * Prepare
     */
    private function prepare()
    {
        if (!is_array($this->product)) {
            $this->product = [$this->product];
        }

        if (!is_array($this->not)) {
            $this->not = [$this->not];
        }
    }
}
