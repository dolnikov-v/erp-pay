<?php
namespace app\modules\order\models\search;

use app\modules\delivery\models\Delivery;
use app\modules\order\models\OrderLogisticList;
use app\modules\order\models\Order;
use app\models\User;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * Class OrderLogisticListSearch
 * @package app\modules\order\models\search
 */
class OrderLogisticListSearch extends OrderLogisticList
{
    public
        $dateFrom,
        $dateTo,
        $delivery_id,
        $deliveries = [],
        $rejectedSource = [],
        $id;


    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['dateFrom', 'dateTo', 'delivery_id'], 'safe']
        ];
    }

    public function init()
    {
        $delivery_ids = [];
        $this->deliveries = Delivery::find()
            ->byCountryId(Yii::$app->user->getCountry()->id)
            ->byId($delivery_ids)
            ->collection();

        $this->rejectedSource = User::getRejectedSources(Yii::$app->user->id);

        parent::init();
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['id'] = SORT_DESC;
        }

        $condition = [OrderLogisticList::tableName() . '.country_id' => Yii::$app->user->country->id];


        if (Yii::$app->user->getIsImplant()) {
            $delivery_ids = Yii::$app->user->getDeliveriesIds();
            $condition[Delivery::tableName() . ".id"] = $delivery_ids;
        }
        $query = OrderLogisticList::find()
            ->joinWith([
                'delivery',
                'actualListExcel',
            ])
            ->with('delivery.apiClass')
            ->where($condition);

        if (!Yii::$app->user->can('old_orders_access')) {
            $query->andWhere([">", OrderLogisticList::tableName() . ".created_at", time () - 3600 * 24 * 7]);
        }

        $query->orderBy($sort);

        // Исключаем листы полностью состоящие из заказов созданных с запрещенных источников
        if (!empty($this->rejectedSource)) {
            $listToShow = [];
            $lists = $query->all();
            foreach ($lists as $list) {
                $ids = explode(',', $list->ids);
                $order = Order::find()
                    ->andWhere(['IN', Order::tableName() .'.id', $ids])
                    ->andWhere(['NOT IN', Order::tableName() .'.source_id', $this->rejectedSource])
                    ->one();
                if (isset($order->id) && is_numeric($order->id)) {
                    $listToShow[] = $list->id;
                }
            }
            $query->andWhere(['IN', OrderLogisticList::tableName() .'.id', $listToShow]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if ($this->dateFrom && $dateFrom = Yii::$app->formatter->asTimestampLocal($this->dateFrom)) {
            $query->andFilterWhere(['>=', OrderLogisticList::tableName() . '.created_at', $dateFrom]);
        }

        if ($this->dateTo && $dateTo = Yii::$app->formatter->asTimestampLocal($this->dateTo)) {
            $query->andFilterWhere(['<=', OrderLogisticList::tableName() . '.created_at', $dateTo + 86399]);
        }

        if ($this->delivery_id) {
            $query->andFilterWhere(['=', OrderLogisticList::tableName() . '.delivery_id',$this->delivery_id]);
        }

        if ($this->id) {
            $query->andFilterWhere(['=', OrderLogisticList::tableName() . '.id',$this->id]);
        }

        return $dataProvider;
    }
}
