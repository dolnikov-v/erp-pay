<?php

namespace app\modules\order\models\search;

use app\models\Currency;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\models\Order;
use app\modules\order\models\OrderFinancePrediction;
use app\modules\order\models\search\filters\DateFilter;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use app\modules\order\models\OrderFinanceFact;

/**
 * Class OrderFinanceFactSearch
 * @package app\modules\order\models\search
 */
class OrderFinanceFactSearch extends OrderSearch
{
    const DATA_TYPE_FACT = 'fact';
    const DATA_TYPE_PREDICTION = 'prediction';

    /**
     * @param $params
     * @return ActiveDataProvider
     * @throws \yii\base\InvalidConfigException
     */
    public function search($params)
    {
        $checkParams = $params;
        unset($checkParams['export']);

        if (empty($checkParams)) {
            $params['DateFilter']['dateType'] = DateFilter::TYPE_CREATED_AT;
            $params['DateFilter']['dateFrom'] = Yii::$app->formatter->asDate(time());
            $params['DateFilter']['dateTo'] = Yii::$app->formatter->asDate(time());
        }

        $join = [];

        $with = [
            'orderProducts',
            'orderFinancePretensions',
            'callCenterRequest',
            'deliveryRequest',
            'orderLogisticListInvoice',
            'originalOrder',
            'finance',
            'orderProducts.product',
            'landing',
            'orderExpressDelivery',
            'financePrediction',
            'callCenterCheckRequests',
        ];

        if (!empty($params['MarketingListFilter'])) {
            array_push($join, 'orderProducts.product');
        }

        $this->query = Order::find()
            ->joinWith($join)
            ->where([Order::tableName() . '.country_id' => $this->countryId]);


        $oldOrdersAccess = true;
        if ($this->specifiedUser instanceof \app\models\User) {
            $permissions = Yii::$app->authManager->getPermissionsByUser($this->specifiedUser->id);
            if (!isset($permissions['old_orders_access']) && !$this->specifiedUser->isSuperadmin) {
                $oldOrdersAccess = false;
            }
        } else {
            if (!Yii::$app->user->can('old_orders_access')) {
                $oldOrdersAccess = false;
            }
        }

        if (!$oldOrdersAccess) {
            $this->query->andWhere([">", Order::tableName() . ".created_at", time() - 3600 * 24 * 7]);
        }

        // роль имплант (пользователь курьерской службы)
        $deliveryServiceUserImplant = false;
        $deliveryIds = [];
        if ($this->specifiedUser instanceof \app\models\User) {
            if ($this->specifiedUser->getIsImplant()) {
                $deliveryServiceUserImplant = true;
                $deliveryIds = ArrayHelper::getColumn($this->specifiedUser->getDeliveries()->asArray()->all(), 'id');
            }
        } else {
            if (Yii::$app->user->getIsImplant()) {
                $deliveryServiceUserImplant = true;
                $deliveryIds = Yii::$app->user->getDeliveriesIds();
            }
        }

        if ($deliveryServiceUserImplant) {
            $this->query->joinWith('deliveryRequest');
            $this->query->andFilterWhere([
                'or',
                [
                    'and',
                    DeliveryRequest::tableName() . '.delivery_id is not null',
                    [DeliveryRequest::tableName() . ".delivery_id" => $deliveryIds]
                ],
                [
                    'and',
                    DeliveryRequest::tableName() . '.delivery_id is null',
                    [Order::tableName() . ".pending_delivery_id" => $deliveryIds]
                ]
            ]);
        }

        // ограничение по доступным источникам
        if ($this->specifiedUser instanceof \app\models\User) {
            $this->query->andWhere(['NOT IN', Order::tableName() . '.source_id', $this->specifiedUser->getRejectedSources($this->specifiedUser->id)]);
        } else {
            $this->query->andWhere(['NOT IN', Order::tableName() . '.source_id', Yii::$app->user->rejectedSources]);
        }

        $this->query->with($with);

        $this->applyFilters($params);

        $this->query->distinct();

        $sort['id'] = SORT_DESC;
        $sort['delivery_report_id'] = SORT_ASC;

        $ratesFact = $ratesPrediction = [];
        $usdID = Currency::getUSD()->id;
        foreach (OrderFinanceFact::getReportColumns()['column'] as $column) {
            $rate = $column . '_currency_rate';
            $ratesFact[] = "IF({$column}_currency_id is not null, IF({$rate} is null, 1 / convertToCurrencyByCountry(1, {$column}_currency_id, {$usdID}), {$rate}), 1) as {$rate}";
//            TODO: на данный момент берем курс на текущую дату, что не совсем корректно. Запомнить. Придумать лучшее решение.
//            Проблема с курсом в том, что фактов может быть несколько. Даты закрытия фактов разные, валюты фактов могут не совпадать с предикшенами...
            $ratesPrediction[] = "IF({$column}_currency_id is not null, 1 / convertToCurrencyByCountry(1, {$column}_currency_id, {$usdID}), 1) as {$rate}";
        }

        $queryPrediction = clone $this->query;
        $queryPrediction->select(new Expression('`order`.`id`, null as delivery_report_id, null as name, null as payment, ' . implode(',', array_merge(OrderFinanceFact::getReportColumns()['column'], OrderFinanceFact::getReportColumns()['currency'], $ratesPrediction))))->joinWith('financePrediction')
            ->andWhere(new Expression(OrderFinancePrediction::clearTableName() . '.order_id is not null'));
        $queryFact = clone $this->query;
        $queryFact->select(new Expression('`order`.`id`, delivery_report_id, `delivery_report`.name as name, `payment_order_delivery`.name as payment, ' . implode(',', array_merge(OrderFinanceFact::getReportColumns()['column'], OrderFinanceFact::getReportColumns()['currency'], $ratesFact))))->joinWith(['financeFact', 'financeFact.deliveryReport', 'financeFact.payment'])
            ->andWhere(new Expression(OrderFinanceFact::clearTableName() . '.order_id is not null'));

        if (!empty($params['dataType']) && $params['dataType'] == self::DATA_TYPE_PREDICTION) {
            $dataProvider = new ActiveDataProvider([
                'query' => $queryPrediction->orderBy($sort),
            ]);
        } elseif (!empty($params['dataType']) && $params['dataType'] == self::DATA_TYPE_FACT) {
            $dataProvider = new ActiveDataProvider([
                'query' => $queryFact->orderBy($sort),
            ]);
        } else {
            $dataProvider = new ActiveDataProvider([
                'query' => (new Query())->from(['table' => $queryPrediction->union($queryFact)])->orderBy($sort),
            ]);
        }

        return $dataProvider;
    }
}
