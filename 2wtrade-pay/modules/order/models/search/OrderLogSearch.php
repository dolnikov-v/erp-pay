<?php

namespace app\modules\api\models\search;

use app\modules\order\models\OrderLog;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * Class OrderLogSearch
 * @package app\modules\api\models\search
 */
class OrderLogSearch extends OrderLog
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['id', 'order_id', 'group_id', 'created_at'], 'integer'],
            [['field', 'old', 'new'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OrderLog::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'order_id' => $this->order_id,
            'group_id' => $this->group_id,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'field', $this->field])
            ->andFilterWhere(['like', 'old', $this->old])
            ->andFilterWhere(['like', 'new', $this->new]);

        return $dataProvider;
    }
}
