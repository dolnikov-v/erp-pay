<?php

namespace app\modules\order\models\search;

use app\modules\delivery\models\Delivery;
use app\modules\order\models\OrderClarification;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * Class OrderClarificationSearch
 * @package app\modules\order\models\search
 */
class OrderClarificationSearch extends OrderClarification
{
    /**
     * @var string
     */
    public $dateFrom;

    /**
     * @var string
     */
    public $dateTo;

    /**
     * @var string
     */
    public $delivery_id;

    /**
     * @var array
     */
    public $deliveries = [];

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['dateFrom', 'dateTo', 'delivery_id'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->deliveries = Delivery::find()
            ->byCountryId(Yii::$app->user->country->id)
            ->collection();

        parent::init();
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $sort['id'] = SORT_DESC;
        if (isset($params['sort'])) {
            $sort = $params['sort'];
            if (strpos($sort, "-") === 0) {
                $sort = substr($sort, 1);
            }
        }

        $query = OrderClarification::find()
            ->where(['country_id' => Yii::$app->user->country->id])
            ->orderBy($sort);

        $this->load($params);
        if ($this->validate()) {

            if ($this->dateFrom && $dateFrom = Yii::$app->formatter->asTimestamp($this->dateFrom)) {
                $query->andFilterWhere(['>=', OrderClarification::tableName() . '.created_at', $dateFrom]);
            }

            if ($this->dateTo && $dateTo = Yii::$app->formatter->asTimestamp($this->dateTo)) {
                $query->andFilterWhere(['<=', OrderClarification::tableName() . '.created_at', $dateTo + 86399]);
            }

            if ($this->delivery_id) {
                $query->andFilterWhere(['=', OrderClarification::tableName() . '.delivery_id',$this->delivery_id]);
            }
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }
}
