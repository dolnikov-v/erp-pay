<?php

namespace app\modules\order\models\search;

use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\models\Order;
use app\modules\order\models\search\filters\AutoCheckAddressFilter;
use app\modules\order\models\search\filters\CallCenterFilter;
use app\modules\order\models\search\filters\CheckingTypeFilter;
use app\modules\order\models\search\filters\DateFilter;
use app\modules\order\models\search\filters\DebtsFilter;
use app\modules\order\models\search\filters\DeliveryExpressFilter;
use app\modules\order\models\search\filters\DeliveryFilter;
use app\modules\order\models\search\filters\NoDoubleFilter;
use app\modules\order\models\search\filters\NumberFilter;
use app\modules\order\models\search\filters\OriginFilter;
use app\modules\order\models\search\filters\ProductFilter;
use app\modules\order\models\search\filters\LeadProductFilter;
use app\modules\order\models\search\filters\StatusCallCenterRequestFilter;
use app\modules\order\models\search\filters\StatusDeliveryRequestFilter;
use app\modules\order\models\search\filters\StatusFilter;
use app\modules\order\models\search\filters\TextFilter;
use Yii;
use yii\data\ActiveDataProvider;
use app\modules\order\models\search\filters\DeliveryPartnerFilter;
use app\modules\order\models\search\filters\TermsOfShippingFilter;
use app\modules\order\models\search\filters\DeliveryTermsFilter;
use app\modules\order\models\search\filters\MarketingListFilter;
use app\modules\order\models\search\filters\DeliveryClarificationFilter;
use app\modules\order\models\search\filters\DeliveryDelayFilter;
use app\modules\order\models\search\filters\DeliveryZipGroupFilter;
use app\modules\order\models\search\filters\UnshippingReasonFilter;
use app\modules\order\models\search\filters\PretensionFilter;
use yii\helpers\ArrayHelper;


/**
 * Class OrderSearch
 * @package app\modules\order\models\search
 */
class OrderSearch extends Order
{
    public $selectableFilters = [];

    /** @var \yii\db\ActiveQuery */
    protected $query;

    /** @var DateFilter */
    protected $dateFilter;

    /** @var StatusFilter */
    protected $statusFilter;

    /** @var ProductFilter */
    protected $productFilter;

    /** @var LeadProductFilter */
    protected $leadProductFilter;

    /** @var NumberFilter */
    protected $numberFilter;

    /** @var TextFilter */
    protected $textFilter;

    /** @var CallCenterFilter */
    protected $callCenterFilter;

    /** @var DeliveryFilter */
    protected $deliveryFilter;

    /** @var StatusCallCenterRequestFilter */
    protected $statusCallCenterRequestFilter;

    /** @var StatusDeliveryRequestFilter */
    protected $statusDeliveryRequestFilter;

    /** @var OriginFilter */
    protected $originFilter;

    /**
     * @var DeliveryPartnerFilter
     */
    protected $deliveryPartnerFilter;

    /** @var  TermsOfShippingFilter */
    protected $termsOfShippingFilter;

    /** @var DeliveryTermsFilter */
    protected $deliveryTermsFilter;

    /**
     * @var MarketingListFilter
     */
    protected $marketingListFilter;

    /**
     * @var DeliveryClarificationFilter
     */
    protected $deliveryClarificationFilter;

    /**
     * @var DeliveryDelayFilter
     */
    protected $deliveryDelayFilter;

    /**
     * @var DeliveryExpressFilter
     */
    protected $deliveryExpressFilter;

    /**
     * @var DeliveryZipGroupFilter
     */
    protected $deliveryZipGroupFilter;

    /**
     * @var CheckingTypeFilter
     */
    protected $checkingTypeFilter;

    /**
     * @var AutoCheckAddressFilter
     */
    protected $autoCheckAddressFilter;

    /**
     * @var UnshippingReasonFilter
     */
    protected $unshippingReasonFilter;

    /**
     * @var PretensionFilter
     */
    protected $pretensionFilter;

    /**
     * @var NoDoubleFilter
     */
    protected $noDoubleFilter;

    /**
     * @var integer
     */
    public $countryId;

    /**
     * @var integer
     */
    public $userId;

    /**
     * @var \app\models\User
     */
    public $specifiedUser;

    /**
     * @var DebtsFilter
     */
    public $debtsFilter;

    /**
     * @inheritdoc
     */
    public function init()
    {
        if (is_null($this->countryId)) {
            $this->countryId = Yii::$app->user->getCountry()->id;
        }

        if (!is_null($this->userId)) {
            $specifiedUser = \app\models\User::findOne($this->userId);
            if ($specifiedUser) {
                $this->specifiedUser = $specifiedUser;
            }
            $this->userId = null;
        }

        parent::init();

        if ($this->specifiedUser->getIsDeliveryManager()) {
            $this->selectableFilters = [
                'number' => Yii::t('common', 'Поиск по номеру'),
                'text' => Yii::t('common', 'Поиск по тексту'),
            ];
        } else {
            $this->selectableFilters = [
                'status' => Yii::t('common', 'Статус заказа'),
                'product' => Yii::t('common', 'Товар'),
                'lead-product' => Yii::t('common', 'Товар лида'),
                'number' => Yii::t('common', 'Поиск по номеру'),
                'text' => Yii::t('common', 'Поиск по тексту'),
                'call-center' => Yii::t('common', 'Поиск по колл-центру'),
                'delivery' => Yii::t('common', 'Поиск по службе доставки'),
                'delivery-clarification' => Yii::t('common', 'Отправка на уточнение в службу доставки'),
                'delivery-delay' => Yii::t('common', 'Задержка сроков доставки'),
                'delivery-express' => Yii::t('common', 'Экспресс'),
                'delivery-zipgroup' => Yii::t('common', 'Поиск по группе Zip кодов'),
                'status-call-center-request' => Yii::t('common', 'Статус заявки в колл-центре'),
                'status-delivery-request' => Yii::t('common', 'Статус заявки в службе доставки'),
                'origin' => Yii::t('common', 'Источник'),
                'delivery-partner' => Yii::t('common', 'Поиск по партнеру КС'),
                'terms-of-shipping' => Yii::t('common', 'Превышение сроков отправки'),
                'delivery-terms' => Yii::t('common', 'Превышение сроков доставки'),
                'marketing-list' => Yii::t('common', 'Маркетинг'),
                'checking-queues' => Yii::t('common', 'Поиск по типу проверяемой очереди'),
                'auto-check-address' => Yii::t('common', 'Статус автоматической проверки адреса'),
                'unshipping-reason' => Yii::t('common', 'Причины недоставки'),
                'pretension' => Yii::t('common', 'Претензия'),
                'debts' => Yii::t('common', 'Дебеторский статус'),
                'no-double' => Yii::t('common', 'Не показывать дубли'),
            ];
        }
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $checkParams = $params;
        unset($checkParams['export']);

        $sort['id'] = SORT_DESC;
        if (isset($params['sort'])) {
            $sort = $params['sort'];
            if (strpos($sort, "-") === 0) {
                $sort = substr($sort, 1);
            }
        }

        $join = [];

        $with = [
            'orderProducts',
            'orderFinancePretensions',
            'callCenterRequest',
            'deliveryRequest.deliveryPartner',
            'orderLogisticListInvoice',
            'originalOrder',
            'orderProducts.product',
            'landing',
            'orderExpressDelivery',
            'financePrediction',
            'callCenterCheckRequests',
            'lead',
            'callCenterRequest.callCenterRequestCallData',
        ];

        if (!empty($params['MarketingListFilter'])) {
            array_push($join, 'orderProducts.product');
        }

        $this->query = Order::find()
            ->joinWith($join)
            ->where([Order::tableName() . '.country_id' => $this->countryId]);


        $oldOrdersAccess = true;
        if ($this->specifiedUser instanceof \app\models\User) {
            $permissions = Yii::$app->authManager->getPermissionsByUser($this->specifiedUser->id);
            if (!isset($permissions['old_orders_access']) && !$this->specifiedUser->isSuperadmin) {
                $oldOrdersAccess = false;
            }
        } else {
            if (!Yii::$app->user->can('old_orders_access')) {
                $oldOrdersAccess = false;
            }
        }

        if (!$oldOrdersAccess) {
            $this->query->andWhere([">", Order::tableName() . ".created_at", time() - 3600 * 24 * 7]);
        }

        // роль имплант (пользователь курьерской службы)
        $deliveryServiceUserImplant = false;
        $deliveryIds = [];
        if ($this->specifiedUser instanceof \app\models\User) {
            if ($this->specifiedUser->getIsImplant()) {
                $deliveryServiceUserImplant = true;
                $deliveryIds = ArrayHelper::getColumn($this->specifiedUser->getDeliveries()->asArray()->all(), 'id');
            }
        } else {
            if (Yii::$app->user->getIsImplant()) {
                $deliveryServiceUserImplant = true;
                $deliveryIds = Yii::$app->user->getDeliveriesIds();
            }
        }

        if ($deliveryServiceUserImplant) {
            $this->query->joinWith('deliveryRequest');
            $this->query->andFilterWhere([
                'or',
                [
                    'and',
                    DeliveryRequest::tableName() . '.delivery_id is not null',
                    [DeliveryRequest::tableName() . ".delivery_id" => $deliveryIds]
                ],
                [
                    'and',
                    DeliveryRequest::tableName() . '.delivery_id is null',
                    [Order::tableName() . ".pending_delivery_id" => $deliveryIds]
                ]
            ]);
        }

        // ограничение по доступным источникам
        if ($this->specifiedUser instanceof \app\models\User) {
            $this->query->andWhere(['NOT IN', Order::tableName() . '.source_id', $this->specifiedUser->getRejectedSources($this->specifiedUser->id)]);
        } else {
            $this->query->andWhere(['NOT IN', Order::tableName() . '.source_id', Yii::$app->user->rejectedSources]);
        }

        $this->query->orderBy($sort);
        $this->query->with($with);

        $dataProvider = new ActiveDataProvider([
            'query' => $this->query,
        ]);

        $this->applyFilters($params);

        $this->query->distinct();

        return $dataProvider;
    }

    /**
     * @return DateFilter
     */
    public function getDateFilter()
    {
        if (is_null($this->dateFilter)) {
            $this->dateFilter = new DateFilter([
                'dateType' => DateFilter::TYPE_CREATED_AT,
                'dateFrom' => Yii::$app->formatter->asDate(time()),
                'dateTo' => Yii::$app->formatter->asDate(time())
            ]);
        }

        return $this->dateFilter;
    }

    /**
     * @return StatusFilter
     */
    public function getStatusFilter()
    {
        if (is_null($this->statusFilter)) {
            $this->statusFilter = new StatusFilter();
        }

        return $this->statusFilter;
    }

    /**
     * @return DebtsFilter
     */
    public function getDebtsFilter()
    {
        if (is_null($this->debtsFilter)) {
            $this->debtsFilter = new DebtsFilter();
        }

        return $this->debtsFilter;
    }

    /**
     * @return ProductFilter
     */
    public function getProductFilter()
    {
        if (is_null($this->productFilter)) {
            $this->productFilter = new ProductFilter();
        }

        return $this->productFilter;
    }

    /**
     * @return LeadProductFilter
     */
    public function getLeadProductFilter()
    {
        if (is_null($this->leadProductFilter)) {
            $this->leadProductFilter = new LeadProductFilter();
        }

        return $this->leadProductFilter;
    }

    /**
     * @return NumberFilter
     */
    public function getNumberFilter()
    {
        if (is_null($this->numberFilter)) {
            $this->numberFilter = new NumberFilter(['specifiedUser' => $this->specifiedUser]);
        }

        return $this->numberFilter;
    }

    /**
     * @return TextFilter
     */
    public function getTextFilter()
    {
        if (is_null($this->textFilter)) {
            $this->textFilter = new TextFilter(['specifiedUser' => $this->specifiedUser]);
        }

        return $this->textFilter;
    }

    /**
     * @return CallCenterFilter
     */
    public function getCallCenterFilter()
    {
        if (is_null($this->callCenterFilter)) {
            $this->callCenterFilter = new CallCenterFilter(['countryId' => $this->countryId]);
        }

        return $this->callCenterFilter;
    }

    /**
     * @return DeliveryFilter
     */
    public function getDeliveryFilter()
    {
        if (is_null($this->deliveryFilter)) {
            $this->deliveryFilter = new DeliveryFilter([
                'countryId' => $this->countryId,
                'specifiedUser' => $this->specifiedUser
            ]);
        }

        return $this->deliveryFilter;
    }

    /**
     * @return StatusCallCenterRequestFilter
     */
    public function getStatusCallCenterRequestFilter()
    {
        if (is_null($this->statusCallCenterRequestFilter)) {
            $this->statusCallCenterRequestFilter = new StatusCallCenterRequestFilter();
        }

        return $this->statusCallCenterRequestFilter;
    }

    /**
     * @return StatusDeliveryRequestFilter
     */
    public function getStatusDeliveryRequestFilter()
    {
        if (is_null($this->statusDeliveryRequestFilter)) {
            $this->statusDeliveryRequestFilter = new StatusDeliveryRequestFilter();
        }

        return $this->statusDeliveryRequestFilter;
    }

    /**
     * @return OriginFilter
     */
    public function getOriginFilter()
    {
        if (is_null($this->originFilter)) {
            $this->originFilter = new OriginFilter();
        }

        return $this->originFilter;
    }

    /**
     * @return DeliveryPartnerFilter
     */
    public function getDeliveryPartnerFilter()
    {
        if (is_null($this->deliveryPartnerFilter)) {
            $this->deliveryPartnerFilter = new DeliveryPartnerFilter(['countryId' => $this->countryId]);
        }

        return $this->deliveryPartnerFilter;
    }

    /**
     * @return termsOfShippingFilter
     */
    public function getTermsOfShippingFilter()
    {
        if (is_null($this->termsOfShippingFilter)) {
            $this->termsOfShippingFilter = new TermsOfShippingFilter();
        }

        return $this->termsOfShippingFilter;
    }

    /**
     * @return deliveryTermsFilter
     */
    public function getDeliveryTermsFilter()
    {
        if (is_null($this->deliveryTermsFilter)) {
            $this->deliveryTermsFilter = new DeliveryTermsFilter();
        }

        return $this->deliveryTermsFilter;
    }

    /*
     * @return MarketingListFilter
     */
    public function getMarketingListFilter()
    {
        if (is_null($this->marketingListFilter)) {
            $this->marketingListFilter = new MarketingListFilter();
        }

        return $this->marketingListFilter;
    }

    /*
     * @return DeliveryClarificationFilter
     */
    public function getDeliveryClarificationFilter()
    {
        if (is_null($this->deliveryClarificationFilter)) {
            $this->deliveryClarificationFilter = new DeliveryClarificationFilter();
        }

        return $this->deliveryClarificationFilter;
    }

    /*
     * @return DeliveryDelayFilter
     */
    public function getDeliveryDelayFilter()
    {
        if (is_null($this->deliveryDelayFilter)) {
            $this->deliveryDelayFilter = new DeliveryDelayFilter();
        }

        return $this->deliveryDelayFilter;
    }


    /*
     * @return DeliveryExpressFilter
     */
    public function getDeliveryExpressFilter()
    {
        if (is_null($this->deliveryExpressFilter)) {
            $this->deliveryExpressFilter = new DeliveryExpressFilter();
        }

        return $this->deliveryExpressFilter;
    }

    /*
     * @return DeliveryZipGroupFilter
     */
    public function getDeliveryZipGroupFilter()
    {
        if (is_null($this->deliveryZipGroupFilter)) {
            $this->deliveryZipGroupFilter = new DeliveryZipGroupFilter(['countryId' => $this->countryId]);
        }

        return $this->deliveryZipGroupFilter;
    }

    /**
     * @return CheckingTypeFilter
     */
    public function getCheckingTypeFilter()
    {
        if (is_null($this->checkingTypeFilter)) {
            $this->checkingTypeFilter = new CheckingTypeFilter();
        }

        return $this->checkingTypeFilter;
    }

    /**
     * @return AutoCheckAddressFilter
     */
    public function getAutoCheckAddressFilter()
    {
        if (is_null($this->autoCheckAddressFilter)) {
            $this->autoCheckAddressFilter = new AutoCheckAddressFilter();
        }

        return $this->autoCheckAddressFilter;
    }

    /**
     * @return UnshippingReasonFilter
     */
    public function getUnshippingReasonFilter()
    {
        if (is_null($this->unshippingReasonFilter)) {
            $this->unshippingReasonFilter = new UnshippingReasonFilter();
        }

        return $this->unshippingReasonFilter;
    }

    /**
     * @return PretensionFilter
     */
    public function getPretensionFilter()
    {
        if (is_null($this->pretensionFilter)) {
            $this->pretensionFilter = new PretensionFilter();
        }

        return $this->pretensionFilter;
    }

    /**
     * @return NoDoubleFilter
     */
    public function getNoDoubleFilter()
    {
        if (is_null($this->noDoubleFilter)) {
            $this->noDoubleFilter = new NoDoubleFilter();
        }

        return $this->noDoubleFilter;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     * @throws \Exception
     */
    public function restore($form)
    {
        $output = '';

        $output .= $this->statusFilter->restore($form);
        $output .= $this->productFilter->restore($form);
        $output .= $this->leadProductFilter->restore($form);
        $output .= $this->numberFilter->restore($form);
        $output .= $this->textFilter->restore($form);
        $output .= $this->deliveryFilter->restore($form);
        $output .= $this->callCenterFilter->restore($form);
        $output .= $this->statusCallCenterRequestFilter->restore($form);
        $output .= $this->statusDeliveryRequestFilter->restore($form);
        $output .= $this->originFilter->restore($form);
        $output .= $this->deliveryPartnerFilter->restore($form);
        $output .= $this->termsOfShippingFilter->restore($form);
        $output .= $this->deliveryTermsFilter->restore($form);
        $output .= $this->marketingListFilter->restore($form);
        $output .= $this->deliveryZipGroupFilter->restore($form);
        $output .= $this->deliveryClarificationFilter->restore($form);
        $output .= $this->deliveryDelayFilter->restore($form);
        $output .= $this->deliveryExpressFilter->restore($form);
        $output .= $this->getCheckingTypeFilter()->restore($form);
        $output .= $this->getAutoCheckAddressFilter()->restore($form);
        $output .= $this->getUnshippingReasonFilter()->restore($form);
        $output .= $this->getPretensionFilter()->restore($form);
        $output .= $this->getDebtsFilter()->restore($form);
        $output .= $this->getNoDoubleFilter()->restore($form);

        return $output;
    }

    /**
     * @param array $params
     */
    protected function applyFilters($params)
    {
        $this->getDateFilter()->apply($this->query, $params);
        $this->getStatusFilter()->apply($this->query, $params);
        $this->getProductFilter()->apply($this->query, $params);
        $this->getLeadProductFilter()->apply($this->query, $params);
        $this->getNumberFilter()->apply($this->query, $params);
        $this->getTextFilter()->apply($this->query, $params);
        $this->getCallCenterFilter()->apply($this->query, $params);
        $this->getDeliveryFilter()->apply($this->query, $params);
        $this->getStatusCallCenterRequestFilter()->apply($this->query, $params);
        $this->getStatusDeliveryRequestFilter()->apply($this->query, $params);
        $this->getOriginFilter()->apply($this->query, $params);
        $this->getDeliveryPartnerFilter()->apply($this->query, $params);
        $this->getTermsOfShippingFilter()->apply($this->query, $params);
        $this->getDeliveryTermsFilter()->apply($this->query, $params);
        $this->getMarketingListFilter()->apply($this->query, $params);
        $this->getDeliveryClarificationFilter()->apply($this->query, $params);
        $this->getDeliveryDelayFilter()->apply($this->query, $params);
        $this->getDeliveryExpressFilter()->apply($this->query, $params);
        $this->getDeliveryZipGroupFilter()->apply($this->query, $params);
        $this->getCheckingTypeFilter()->apply($this->query, $params);
        $this->getAutoCheckAddressFilter()->apply($this->query, $params);
        $this->getUnshippingReasonFilter()->apply($this->query, $params);
        $this->getPretensionFilter()->apply($this->query, $params);
        $this->getDebtsFilter()->apply($this->query, $params);
        $this->getNoDoubleFilter()->apply($this->query, $params);
    }
}
