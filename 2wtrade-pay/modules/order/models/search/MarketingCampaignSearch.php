<?php
namespace app\modules\order\models\search;

use app\modules\order\models\MarketingCampaign;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * Class MarketingListSearch
 * @package app\models\search
 */
class MarketingCampaignSearch extends MarketingCampaign
{
    public $dateFrom;
    public $dateTo;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['dateFrom', 'dateTo'], 'safe']
        ];
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['id'] = SORT_DESC;
        }

        $condition = [MarketingCampaign::tableName() . '.country_id' => Yii::$app->user->country->id];

        $query = MarketingCampaign::find()
            ->where($condition);

        $query->orderBy($sort);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if ($this->dateFrom && $dateFrom = Yii::$app->formatter->asTimestamp($this->dateFrom)) {
            $query->andFilterWhere(['>=', MarketingCampaign::tableName() . '.created_at', $dateFrom]);
        }

        if ($this->dateTo && $dateTo = Yii::$app->formatter->asTimestamp($this->dateTo)) {
            $query->andFilterWhere(['<=', MarketingCampaign::tableName() . '.created_at', $dateTo + 86399]);
        }

        return $dataProvider;
    }
}
