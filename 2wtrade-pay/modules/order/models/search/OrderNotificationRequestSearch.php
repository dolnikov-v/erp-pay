<?php

namespace app\modules\order\models\search;

use app\modules\order\models\OrderNotification;
use app\modules\order\models\OrderNotificationRequest;
use app\modules\order\models\OrderStatus;
use yii\data\ActiveDataProvider;
use Yii;

/**
 * Class OrderNotificationRequestSearch
 * @package app\modules\smsnotification\models\search
 */
class OrderNotificationRequestSearch extends OrderNotificationRequest
{
    const DATE_TYPE_SMS_SENT = 'sms_sent';
    const DATE_TYPE_CREATED_AT = 'created_at';

    /** @var \yii\db\ActiveQuery */
    private $query;

    /**
     * @var string
     */
    public $dateFrom;

    /**
     * @var string
     */
    public $dateTo;

    /**
     * @var array
     */
    public $orderStatuses;

    /**
     * @var array
     */
    public $orderNotifications;

    /**
     * @var string
     */
    public $dateType;


    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->orderStatuses = OrderStatus::find()->collection();
        $this->orderNotifications = OrderNotification::find()
            ->byCountryId(Yii::$app->user->getCountry()->id)
            ->orderBy([
                OrderNotification::tableName() . '.name' => SORT_ASC
            ])
            ->collection();

        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'order_status_id',
                    'order_notification_id',
                    'sms_date_created',
                    'sms_date_updated',
                    'sms_date_sent',
                    'sms_api_code'
                ],
                'integer'
            ],
            [['order_id', 'sms_status'], 'string'],
            [['sms_price'], 'number'],
            [['phone_to', 'sms_foreign_id', 'sms_foreign_status'], 'string', 'max' => 100],
            [['sms_api_error'], 'string', 'max' => 255],
            [['sms_uri'], 'string', 'max' => 1000],
            ['dateType', 'in', 'range' => array_keys($this->dateTypeCollection())],
            [['dateFrom', 'dateTo'], 'safe']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);

        $this->query = OrderNotificationRequest::find();

        $this->query->joinWith(['orderNotification']);
        $this->query->andWhere([OrderNotification::tableName() . '.country_id' => Yii::$app->user->country->id]);
        $this->query->orderBy([OrderNotificationRequest::tableName() . '.id' => SORT_DESC]);

        $dateColumn = null;
        switch ($this->dateType) {
            case self::DATE_TYPE_SMS_SENT:
                $dateColumn = OrderNotificationRequest::tableName() . '.sms_date_sent';
                break;
            case self::DATE_TYPE_CREATED_AT:
                $dateColumn = OrderNotificationRequest::tableName() . '.created_at';
        }
        if ($dateColumn) {
            if ($this->dateFrom && $dateFrom = Yii::$app->formatter->asTimestamp($this->dateFrom)) {
                $this->query->andFilterWhere(['>=', $dateColumn, $dateFrom]);
            }

            if ($this->dateTo && $dateTo = Yii::$app->formatter->asTimestamp($this->dateTo)) {
                $this->query->andFilterWhere([
                    '<=',
                    $dateColumn,
                    $dateTo + 86399
                ]);
            }
        }

        if ($this->phone_to) {
            $this->query->andFilterWhere([
                'like',
                OrderNotificationRequest::tableName() . '.phone_to',
                $this->phone_to
            ]);
        }

        if ($this->order_id) {
            $orderIds = explode(',', $this->order_id);
            $this->query->andFilterWhere([OrderNotificationRequest::tableName() . '.order_id' => $orderIds]);
        }

        if ($this->order_notification_id) {
            $this->query->andFilterWhere([OrderNotificationRequest::tableName() . '.order_notification_id' => $this->order_notification_id]);
        }

        if ($this->sms_status) {
            $this->query->andFilterWhere([OrderNotificationRequest::tableName() . '.sms_status' => $this->sms_status]);
        }

        if ($this->order_status_id) {
            $this->query->andFilterWhere([OrderNotificationRequest::tableName() . '.order_status_id' => $this->order_status_id]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $this->query,
        ]);

        if (!$this->validate()) {
            return $dataProvider;
        }

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function dateTypeCollection()
    {
        return [
            self::DATE_TYPE_CREATED_AT => Yii::t('common', 'Дата создания'),
            self::DATE_TYPE_SMS_SENT => Yii::t('common', 'Дата отправки (Служба SMS)'),
        ];
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'dateType' => Yii::t('common', 'Тип даты')
        ]);
    }
}
