<?php

namespace app\modules\order\models\search;

use app\modules\order\models\OrderNotification;
use yii\data\ActiveDataProvider;
use Yii;

/**
 * Class OrderNotificationSearch
 */
class OrderNotificationSearch extends OrderNotification
{
    /** @var \yii\db\ActiveQuery */
    private $query;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['active'], 'integer'],
            [['text'], 'string', 'max' => 500],
            [['name'], 'string', 'max' => 100],
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);

        $this->query = OrderNotification::find();
        $this->query->where([OrderNotification::tableName() . '.country_id' => Yii::$app->user->country->id]);
        $this->query->joinWith('issueCollector');

        $dataProvider = new ActiveDataProvider([
            'query' => $this->query,
        ]);

        if (!$this->validate()) {
            return $dataProvider;
        }

        return $dataProvider;
    }
}
