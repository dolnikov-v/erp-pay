<?php

namespace app\modules\api\models\search;

use app\modules\order\models\OrderLogStatus;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * Class OrderLogStatusSearch
 * @package app\modules\api\models\search
 */
class OrderLogStatusSearch extends OrderLogStatus
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['id', 'order_id', 'old', 'new', 'created_at'], 'integer'],
        ];
    }

    /**
     * @return array
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OrderLogStatus::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'order_id' => $this->order_id,
            'old' => $this->old,
            'new' => $this->new,
            'created_at' => $this->created_at,
        ]);

        return $dataProvider;
    }
}
