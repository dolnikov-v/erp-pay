<?php

namespace app\modules\order\models;

use app\helpers\Utils;
use app\models\logs\LinkData;
use Yii;
use app\components\db\ActiveRecord;

/**
 * This is the model class for table "order_logistic_list_barcode".
 *
 * @property integer $id
 * @property integer $list_id
 * @property integer $order_id
 * @property string $barcode
 * @property string $barcode_filename
 *
 * @property OrderLogisticList $list
 * @property Order $order
 */
class OrderLogisticListBarcode extends ActiveRecord
{
    protected $insertLog = true;
    protected $updateLog = true;
    protected $deleteLog = true;

    /**
     * @return array|null
     */
    protected function getLinkData(): ?array
    {
        return [
            new LinkData(['field' => 'list_id', 'value' => (int)$this->list_id]),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_logistic_list_barcode}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['list_id', 'order_id'], 'required'],
            [['list_id', 'order_id'], 'integer'],
            [['barcode'], 'string', 'max' => 100],
            [['barcode_filename'], 'string', 'max' => 255],
            [
                ['list_id', 'order_id'],
                'unique',
                'targetAttribute' => ['list_id', 'order_id']
            ],
            [
                ['list_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => OrderLogisticList::className(),
                'targetAttribute' => ['list_id' => 'id']
            ],
            [
                ['order_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Order::className(),
                'targetAttribute' => ['order_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'list_id' => Yii::t('common', 'Лист'),
            'order_id' => Yii::t('common', 'Заказ'),
            'barcode' => Yii::t('common', 'Штрих-код'),
            'barcode_filename' => Yii::t('common', 'Файл штрих-кода'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getList()
    {
        return $this->hasOne(OrderLogisticList::className(), ['id' => 'list_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @param string $sub single | grouped
     * @return string
     */
    public static function getPathDir($sub = 'single')
    {
        $dir = Yii::getAlias('@downloads') . DIRECTORY_SEPARATOR . 'barcodes' . DIRECTORY_SEPARATOR . $sub . DIRECTORY_SEPARATOR;
        Utils::prepareDir($dir);
        return $dir;
    }

    /**
     * @param integer $orderId
     * @return string
     */
    public static function getFileName($orderId)
    {
        return $orderId . '.png';
    }

    /**
     * @param $imageData
     * @return bool|string
     */
    public function saveFile($imageData)
    {
        if ($image = base64_decode($imageData)) {
            $dirName = self::getPathDir();
            $fileName = self::getFileName($this->order_id);
            if (file_put_contents($dirName . $fileName, $image)) {
                return $fileName;
            }
        }
        return false;
    }
}
