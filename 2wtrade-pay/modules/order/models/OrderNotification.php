<?php

namespace app\modules\order\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\modules\access\widgets\Country;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\models\query\OrderNotificationQuery;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * OrderNotification
 *
 * @property integer $id
 * @property string $name
 * @property string $text
 * @property string $sms_text
 * @property string $email_text
 * @property integer $active
 * @property integer $email_active
 * @property integer $sms_active
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $country_id
 * @property integer $is_just_one_time
 * @property string $order_statuses
 * @property string $deliveries
 * @property integer $answerable
 * @property integer $issue_collector_id
 * @property Country $country
 * @property array $tokens
 * @property bool|\app\modules\order\models\OrderStatus[] $orderStatusesAsModels
 * @property bool|array $orderStatusesAsArray
 * @property bool|array $deliveriesAsArray
 * @property IssueCollector $issueCollector
 * @property OrderNotificationRequest[] $orderNotifications
 */
class OrderNotification extends ActiveRecordLogUpdateTime
{
    protected $insertLog = true;
    protected $updateLog = true;
    protected $deleteLog = true;

    //Адрес сайта, на котором принимаются ответы
    const RESPONSE_LINK = 'http://2wstore.com/notification/id';

    const IS_ANSWERABLE = 1;
    const IS_NOT_ANSWERABLE = 0;

    public $statuses = [];
    public $deliveryList = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_notification}}';
    }

    /**
     * @return OrderNotificationQuery
     */
    public static function find()
    {
        return new OrderNotificationQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'country_id'], 'required'],
            [['sms_text', 'email_text'], 'requiredTextValidate', 'skipOnEmpty' => false, 'enableClientValidation' => false],
            [['active', 'sms_active', 'email_active', 'country_id'], 'integer'],
            ['statuses', 'each', 'rule' => ['in', 'range' => array_keys(OrderStatus::find()->collection())]],
            ['deliveryList', 'each', 'rule' => ['in', 'range' => array_keys(Delivery::find()->collection())]],
            [['text', 'sms_text'], 'string', 'max' => 500],
            [['email_text'], 'string'],
            [['name'], 'string', 'max' => 100],
            [['created_at', 'updated_at'], 'safe'],
            ['is_just_one_time', 'integer'],
            [['answerable', 'issue_collector_id'], 'integer'],
            [['answerable', 'is_just_one_time', 'active', 'sms_active', 'email_active'], 'default', 'value' => 0],
            [['issue_collector_id'], 'default', 'value' => null],
        ];
    }

    /**
     * @param string $attr
     */
    public function requiredTextValidate($attr)
    {
        $active = ($attr == 'sms_text') ? $this->sms_active : $this->email_active;
        if (empty($this->text) && $active && empty($this->$attr)) {
            $this->addError($attr, Yii::t('common', 'Необходимо заполнить поле "{attr}" или "{text}"', [
                'attr' => $this->getAttributeLabel($attr),
                'text' => $this->getAttributeLabel('text'),
            ]));
        }

    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'name' => Yii::t('common', 'Название'),
            'active' => Yii::t('common', 'Доступность'),
            'country_id' => Yii::t('common', 'Страна'),
            'order_statuses' => Yii::t('common', 'Статусы заказа'),
            'statuses' => Yii::t('common', 'Статусы заказа'),
            'deliveryList' => Yii::t('common', 'Служба доставки'),
            'deliveries' => Yii::t('common', 'Служба доставки'),
            'text' => Yii::t('common', 'Текст по умолчанию'),
            'sms_text' => Yii::t('common', 'Текст SMS'),
            'sms_active' => Yii::t('common', 'Доступность отправки по sms'),
            'email_text' => Yii::t('common', 'Текст E-mail'),
            'email_active' => Yii::t('common', 'Доступность отправки по e-mail'),
            'issue_collector_id' => Yii::t('common', 'Issue collector'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
            'is_just_one_time' => Yii::t('common', 'Отправить 1 раз'),
            'answerable' => Yii::t('common', 'Возможен ответ'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderNotifications()
    {
        return $this->hasMany(OrderNotificationRequest::className(), ['order_notification_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['country_id' => 'id']);
    }

    /**
     * @return array | bool
     */
    public function getOrderStatusesAsArray()
    {
        return $this->order_statuses ? explode(',', $this->order_statuses) : false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIssueCollector()
    {
        return $this->hasOne(IssueCollector::className(), ['id' => 'issue_collector_id']);
    }

    /**
     * @return OrderStatus[] | bool
     */
    public function getOrderStatusesAsModels()
    {
        if ($statuses = $this->getOrderStatusesAsArray()) {
            return OrderStatus::find()->where(['id' => $statuses])->all();
        }

        return false;
    }

    /**
     * @return array | bool
     */
    public function getDeliveryAsArray()
    {
        return $this->deliveries ? explode(',', $this->deliveries) : false;
    }

    /**
     * @return array | bool
     */
    public function getDeliveryAsModel()
    {
        if ($deliveries = $this->getDeliveryAsArray()) {
            return Delivery::find()->where(['id' => $deliveries])->all();
        }

        return false;
    }

    /**
     * @return array
     */
    public function getTokens()
    {
        $tokens = [];
        $attributes = [
            'id',
            'customer_full_name',
            'customer_phone',
            'customer_mobile',
            'customer_email',
            'customer_province',
            'customer_district',
            'customer_subdistrict',
            'customer_city',
            'customer_street',
            'customer_house_number',
            'customer_address',
            'customer_zip',
            'price_total',
            'income',
            'delivery',
            'delivery_time_to',
        ];
        $labels = (new Order)->attributeLabels();
        foreach ($attributes as $attr) {
            $tokens['order.' . $attr] = Yii::t('common', 'Заказ') . '.' . ArrayHelper::getValue($labels, $attr, $attr);
        }
        $tokens['order.delivery_time_from'] = Yii::t('common', 'Заказ') . '.' . Yii::t('common', 'Ожидаемая дата доставки');
        $tokens['order.price_with_delivery'] = Yii::t('common', 'Заказ') . '.' . Yii::t('common', 'Цена с учетом доставки');
        $tokens['order.products'] = Yii::t('common', 'Заказ') . '.' . Yii::t('common', 'Товары');

        $attributes = ['tracking'];
        $labels = (new DeliveryRequest)->attributeLabels();
        foreach ($attributes as $attr) {
            $tokens['delivery.' . $attr] = Yii::t('common', 'Доставка') . '.' . ArrayHelper::getValue($labels, $attr, $attr);
        }
        $tokens['delivery.name'] = Yii::t('common', 'Доставка') . '.' . Yii::t('common', 'Название');
        $tokens['delivery.url'] = Yii::t('common', 'Доставка') . '.' . Yii::t('common', 'Сайт');

        return $tokens;
    }

    /**
     * @param string $text
     * @param Order $order
     * @return string
     */
    public function fillText($text, $order)
    {
        $tokens = array_fill_keys(array_keys($this->getTokens()), '');

        foreach ($order->attributes as $name => $value) {
            if (in_array($name,
                    ['created_at', 'updated_at', 'delivery_time_from', 'delivery_time_to']) && !empty($value)
            ) {
                $date = new \DateTime('now', new \DateTimeZone($order->country->timezone->timezone_id));
                $date->setTimestamp($value);
                $value = $date->format('d-m-Y');
            }
            $tokens['order.' . $name] = $value;
        }

        if (!empty($order->deliveryRequest)) {
            foreach ($order->deliveryRequest->attributes as $name => $value) {
                if (in_array($name, [
                    'created_at',
                    'updated_at',
                    'cron_launched_at',
                    'sent_at',
                    'returned_at',
                    'approved_at',
                    'accepted_at',
                    'paid_at',
                    'done_at'
                ])) {
                    $date = new \DateTime('now', new \DateTimeZone($order->country->timezone->timezone_id));
                    $date->setTimestamp($value);
                    $value = $date->format('d-m-Y');
                }
                $tokens['delivery.' . $name] = $value;
            }
            $tokens['delivery.name'] = $order->deliveryRequest->delivery->name;
            $tokens['delivery.url'] = $order->deliveryRequest->delivery->url;
        } elseif ($order->pendingDelivery) {
            $tokens['delivery.name'] = $order->pendingDelivery->name;
            $tokens['delivery.url'] = $order->pendingDelivery->url;
        }

        $products = [];
        if (!empty($order->orderProducts)) {
            foreach ($order->orderProducts as $product) {
                if (!isset($products[$product->product->name])) {
                    $products[$product->product->name] = 0;
                }
                $products[$product->product->name] += $product->quantity;
            }
        }
        $tmp = [];
        foreach ($products as $name => $quantity) {
            $tmp[] = $name . ' - ' . $quantity;
        }
        $tokens['order.products'] = implode(' ,', $tmp);
        $tokens['order.price_with_delivery'] = ($order->price_total + $order->delivery) . ' ' . strtoupper($order->country->currency->char_code);

        $text = Yii::t('common', $text, $tokens);

        return $text;
    }

    /**
     * @param Order $order
     * @return string
     */
    public function getFilledSmsText($order)
    {
        $text = $this->sms_text ?: $this->text;
        return $this->fillText($text, $order);
    }

    /**
     * @param Order $order
     * @return string
     */
    public function getFilledEmailText($order)
    {
        $text = $this->email_text ?: $this->text;
        return $this->fillText($text, $order);
    }
}
