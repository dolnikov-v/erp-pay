<?php
namespace app\modules\order\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\Country;
use app\models\User;
use app\modules\order\components\exporter\Exporter;
use app\modules\order\models\query\OrderExportQuery;
use Yii;

/**
 * Class OrderExport
 * @property integer $id
 * @property integer $country_id
 * @property integer $user_id
 * @property string $type
 * @property string $query
 * @property string $columns
 * @property integer $lines
 * @property string $url
 * @property string $date_format
 * @property string $language
 * @property string $filename
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $generated_at
 * @property integer $sent_at
 * @property string $after_action
 * @property Country $country
 * @property User $user
 * @property $queue_num
 * @property $generated_time
 */
class OrderExport extends ActiveRecordLogUpdateTime
{
    const ACTION_SEND_CLARIFICATION = 'sendClarification';
    /**
     * @var integer порядковый номер в очереди
     */
    public $queue_num;

    /**
     * @var integer время генерации файла в минутах
     */
    public $generated_time;

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%order_export}}';
    }

    /**
     * @return OrderExportQuery
     */
    public static function find()
    {
        return new OrderExportQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['country_id', 'user_id'], 'required'],
            [['country_id', 'user_id', 'generated_at', 'sent_at', 'created_at', 'updated_at', 'lines'], 'integer'],
            ['type', 'in', 'range' => array_keys(self::getTypes())],
            [['query', 'url', 'columns'], 'string'],
            [['filename', 'date_format', 'language', 'after_action'], 'string', 'max' => 255],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'country_id' => Yii::t('common', 'Страна'),
            'user_id' => Yii::t('common', 'Польователь'),
            'type' => Yii::t('common', 'Тип экспорта'),
            'date_format' => Yii::t('common', 'Формат времени'),
            'language' => Yii::t('common', 'Язык'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
            'generated_at' => Yii::t('common', 'Дата генерации'),
            'sent_at' => Yii::t('common', 'Дата отправки'),
            'lines' => Yii::t('common', 'Число записей'),
            'query' => Yii::t('common', 'Запрос'),
            'url' => Yii::t('common', 'URL'),
            'columns' => Yii::t('common', 'Выбранные столбцы'),
            'queue_num' => Yii::t('common', 'В очереди'),
            'generated_time' => Yii::t('common', 'Время генерации, мин.')
        ];
    }

    /**
     * @return array
     */
    public static function getTypes()
    {
        return [
            Exporter::TYPE_CSV,
            Exporter::TYPE_EXCEL
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ($insert) {
            Yii::$app->queueExport->push(new \app\jobs\ExportOrder(['id' => $this->id]));
        }
        parent::afterSave($insert, $changedAttributes);
    }
}
