<?php

namespace app\modules\order\models;

use app\components\db\ActiveRecordLogCreateTime;
use app\models\Product;
use app\modules\order\models\query\OrderLogProductQuery;
use Yii;

/**
 * Class OrderLogProduct
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $product_id
 * @property string $action
 * @property string $price
 * @property string $quantity
 * @property string $created_at
 *
 * @property Product $product
 * @property Order $order
 */
class OrderLogProduct extends ActiveRecordLogCreateTime
{
    const ACTION_CREATE = 'create';
    const ACTION_UPDATE = 'update';
    const ACTION_DELETE = 'delete';

    /**
     * @return array
     */
    public static function getProductActions()
    {
        return [
            self::ACTION_CREATE => Yii::t('common', 'Создано'),
            self::ACTION_UPDATE => Yii::t('common', 'Обновлено'),
            self::ACTION_DELETE => Yii::t('common', 'Удалено'),
        ];
    }

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%order_log_product}}';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['order_id', 'product_id'], 'required'],
            [['order_id', 'product_id', 'price', 'quantity', 'created_at'], 'integer'],
            [['action'], 'in', 'range' => array_keys(self::getProductActions())],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'order_id' => Yii::t('common', 'Заказ'),
            'product_id' => Yii::t('common', 'Товар'),
            'action' => Yii::t('common', 'Действие'),
            'price' => Yii::t('common', 'Цена'),
            'quantity' => Yii::t('common', 'Количество'),
            'created_at' => Yii::t('common', 'Дата создания'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return OrderLogProductQuery
     */
    public static function find()
    {
        return new OrderLogProductQuery(get_called_class());
    }
}
?>
