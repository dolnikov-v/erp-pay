<?php

namespace app\modules\order\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\logs\LinkData;
use app\modules\order\models\query\OrderLogisticListRequestQuery;
use Yii;
use yii\mail\MessageInterface;
use yii\swiftmailer\Message;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "order_logistic_list_request".
 *
 * @property integer $id
 * @property integer $list_id
 * @property string $email_to
 * @property string $status
 * @property string $hash
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property OrderLogisticList $list
 */
class OrderLogisticListRequest extends ActiveRecordLogUpdateTime
{
    protected $updateLog = true;

    /**
     * @return array|null
     */
    protected function getLinkData(): ?array
    {
        return [
            new LinkData(['field' => 'list_id', 'value' => (int)$this->list_id]),
        ];
    }

    const STATUS_PENDING = 'pending';       // ожидает отправки
    const STATUS_SENT = 'sent';             // отправлен
    const STATUS_RECEIVED = 'received';     // получен
    const STATUS_ERROR = 'error';           // ошибка

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_logistic_list_request}}';
    }

    /**
     * @return OrderLogisticListRequestQuery
     */
    public static function find()
    {
        return new OrderLogisticListRequestQuery(get_called_class());
    }

    /**
     * @return array
     */
    public static function getStatusesCollection()
    {
        return [
            self::STATUS_PENDING => Yii::t('common', 'Ожидает отправки'),
            self::STATUS_SENT => Yii::t('common', 'Отправлен'),
            self::STATUS_RECEIVED => Yii::t('common', 'Получен'),
            self::STATUS_ERROR => Yii::t('common', 'Ошибка'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['list_id'], 'required'],
            [['list_id', 'created_at', 'updated_at'], 'integer'],
            [['email_to', 'status', 'hash'], 'string', 'max' => 255],
            [['list_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrderLogisticList::className(), 'targetAttribute' => ['list_id' => 'id']],
            ['status', 'default', 'value' => self::STATUS_PENDING],
            ['status', 'in', 'range' => array_keys(self::getStatusesCollection())],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'list_id' => Yii::t('common', 'Лист'),
            'email_to' => Yii::t('common', 'Email'),
            'status' => Yii::t('common', 'Статус'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        $this->hash = $this->getHash();
        return parent::beforeSave($insert);
    }

    /**
     * @return string
     */
    public function getHash()
    {
        return md5('list_id' . $this->list_id . 'email_to' . $this->email_to);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getList()
    {
        return $this->hasOne(OrderLogisticList::className(), ['id' => 'list_id']);
    }

    /**
     * ссылка, имеющая определенный токен, который привязан к заявке на отправку
     * @return string
     */
    public function makeLink()
    {
        return Yii::$app->params['domain'] . "/webhook/list/" . $this->getHash();
    }


    /**
     * @param OrderLogisticListExcel $logisticListExcel
     * @param bool $correction
     * @param int $part
     * @param int $totalParts
     * @return MessageInterface
     * @throws \yii\web\ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function buildEmailMessageForLogisticList(OrderLogisticListExcel $logisticListExcel, int $part = 1, int $totalParts = 1): MessageInterface
    {

        switch ($this->list->type) {
            case OrderLogisticList::TYPE_PRETENSION_STILL_WAITING:
                $message = $this->getMailer()->compose('@app/mail/pretension/clients-still-waiting/mail', [
                    'delivery' => $this->list->delivery,
                ]);
                break;

            case OrderLogisticList::TYPE_PRETENSION_INCORRECT_STATUSES:
                $message = $this->getMailer()->compose('@app/mail/pretension/incorrect-statuses/mail', [
                    'delivery' => $this->list->delivery,
                ]);
                break;

            case OrderLogisticList::TYPE_PRETENSION_CHANGED_STATUSES:
                $message = $this->getMailer()->compose('@app/mail/pretension/changed-statuses/mail', [
                    'delivery' => $this->list->delivery,
                ]);
                break;

            case OrderLogisticList::TYPE_NORMAL:
            case OrderLogisticList::TYPE_CORRECTION:
            default:
                $message = $this->getMailer()->compose('@app/mail/logistic-list/mail', [
                    'correction' => $this->list->type == OrderLogisticList::TYPE_CORRECTION,
                    'link' => $this->makeLink(),
                    'orderCount' => $this->list->getCountOrders()
                ]);
                break;
        }

        $message->setFrom(Yii::$app->params['infoMailAddress']);
        $message->setSubject($this->buildSubjectForEmailMessage($part, $totalParts));
        $filePath = OrderLogisticList::getDownloadPath(
                $logisticListExcel->system_file_name,
                OrderLogisticList::DOWNLOAD_TYPE_EXCELS
            ) . DIRECTORY_SEPARATOR . $logisticListExcel->system_file_name;
        $message->attach($filePath,
            [
                'fileName' => "2Wtrade_{$this->list->delivery->name}_" . $this->list->getCountOrders() . "_orders_" . ($totalParts > 1 ? 'Part ' . str_pad($part,
                            2, '0',
                            STR_PAD_LEFT) . ' - ' : '') . $logisticListExcel->delivery_file_name
            ]);
        if (!$this->list->isPretensionType() && $this->list->delivery->auto_generating_invoice == 1 && count($this->list->invoiceArchives) > 0) {
            try {
                $invoice = array_shift($this->list->invoiceArchives);
                $filePath = OrderLogisticList::getDownloadPath(
                        $invoice->archive,
                        OrderLogisticList::DOWNLOAD_TYPE_INVOICES
                    ) . DIRECTORY_SEPARATOR . $invoice->archive;
                $message->attach($filePath,
                    ['fileName' => $invoice->archive_local]);
            } catch (\Throwable $e) {
                throw new NotFoundHttpException('Не удалось взять файл invoiceArchives. Причина: ' . $e->getMessage());
            }
        }

        if (!$this->list->isPretensionType() && $this->list->delivery->auto_generating_barcode == 1 && $this->list->barcodes) {
            $filePath = OrderLogisticListBarcode::getPathDir('grouped') . $this->list->barcodes;
            if (file_exists($filePath)) {
                $message->attach($filePath, ['fileName' => $this->list->barcodes]);
            } else {
                throw new NotFoundHttpException('Не удалось найти файл с штрих-кодами.');
            }
        }

        $message->setTo($this->email_to);

        return $message;
    }

    /**
     * @param int $part
     * @param int $totalParts
     * @return string
     */
    protected function buildSubjectForEmailMessage(int $part = 1, int $totalParts = 1): string
    {
        switch ($this->list->type) {
            case OrderLogisticList::TYPE_PRETENSION_STILL_WAITING:
                $subject = "2WTrade: clients are still waiting";
                break;

            case OrderLogisticList::TYPE_PRETENSION_INCORRECT_STATUSES:
                $subject = "2WTrade: incorrect statuses";
                break;

            case OrderLogisticList::TYPE_PRETENSION_CHANGED_STATUSES:
                $subject = "2WTrade: changed statuses of orders";
                break;

            case OrderLogisticList::TYPE_CORRECTION:
                $subject = "2WTrade orders correction";
                break;

            case OrderLogisticList::TYPE_NORMAL:
            default:
                $subject = "2WTrade orders";
                break;
        }

        $subject .= " - " . date('Y.m.d',
                $this->list->created_at) . ($totalParts > 1 ? ' - part ' . str_pad($part, 2, '0',
                    STR_PAD_LEFT) . ' from ' . $totalParts : '') . " ({$this->list->delivery->name}) (Number of orders - " . $this->list->getCountOrders() . ")";

        return $subject;

    }
}
