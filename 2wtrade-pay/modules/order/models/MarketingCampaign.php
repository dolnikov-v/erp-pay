<?php

namespace app\modules\order\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\Country;
use app\models\User;
use app\modules\order\models\query\MarketingCampaignQuery;
use phpDocumentor\Reflection\Types\Self_;
use Yii;

/**
 * This is the model class for table "marketing_campaign".
 *
 * @property integer $id
 * @property integer $country_id
 * @property string $name
 * @property string $limit_type
 * @property integer $limit_use_days
 * @property integer $limit_use_at
 * @property integer $user_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Country $country
 * @property User $user
 * @property MarketingCampaignCoupon[] $marketingCampaignCoupons
 * @property Order[] $orders
 */
class MarketingCampaign extends ActiveRecordLogUpdateTime
{
    const TYPE_LIMIT_NO = 'no';
    const TYPE_LIMIT_AT = 'at';
    const TYPE_LIMIT_DAYS = 'days';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%marketing_campaign}}';
    }

    /**
     * @return array
     */
    public static function getLimitTypesCollection()
    {
        return [
            self::TYPE_LIMIT_NO => Yii::t('common', 'Бессрочные купоны'),
            self::TYPE_LIMIT_AT => Yii::t('common', 'Купоны активные до даты'),
            self::TYPE_LIMIT_DAYS => Yii::t('common', 'Купоны активные количество дней'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'user_id', 'limit_type', 'name'], 'required'],
            [['country_id', 'user_id', 'limit_use_days', 'limit_use_at', 'created_at', 'updated_at'], 'integer'],
            ['name', 'string', 'max' => 64],
            ['country_id', 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_id' => 'id']],
            ['user_id', 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            ['limit_type', 'default', 'value' => self::TYPE_LIMIT_NO],
            ['limit_type', 'in', 'range' => array_keys(self::getLimitTypesCollection())],
            ['limit_use_days', 'required', 'when' => function ($model) {
                return $model->limit_type == self::TYPE_LIMIT_DAYS;
            }, 'whenClient' => "function (attribute, value) {
                return $('#marketingcampaign-limit_type').val() == 'days';
            }"],
            ['limit_use_at', 'required', 'when' => function ($model) {
                return $model->limit_type == self::TYPE_LIMIT_AT;
            }, 'enableClientValidation' => false],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'country_id' => 'Country ID',
            'name' => Yii::t('common', 'Название'),
            'user_id' => Yii::t('common', 'Пользователь'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата обновления'),
            'limit_type' => Yii::t('common', 'Тип ограничений'),
            'limit_use_days' => Yii::t('common', 'Количество дней активности'),
            'limit_use_at' => Yii::t('common', 'Дата окончания действия'),
            'newcoupons' => Yii::t('common', 'Число купонов для генерации (догенерации)')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMarketingCampaignCoupons()
    {
        return $this->hasMany(MarketingCampaignCoupon::className(), ['marketing_campaign_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveRecord[]
     */
    public function getCoupons()
    {
        return MarketingCampaignCoupon::find()->where(['marketing_campaign_id' => $this->id])->all();
    }

    /**
     * @return integer
     */
    public function getCountCoupons()
    {
        return MarketingCampaignCoupon::find()->where(['marketing_campaign_id' => $this->id])->count();
    }

    public function getNewCoupons() {
        return '';
    }

    /**
     * @return MarketingCampaignQuery
     */
    public static function find()
    {
        return new MarketingCampaignQuery(get_called_class());
    }
}
