<?php
namespace app\modules\order\models\query;

use app\components\db\ActiveQuery;
use app\modules\order\models\MarketingCampaign;

/**
 * Class MarketingCampaignQuery
 * @package app\modules\order\models\query
 * @method MarketingCampaign one($db = null)
 * @method MarketingCampaign[] all($db = null)
 */
class MarketingCampaignQuery extends ActiveQuery
{
    /**
     * @param integer $countryId
     * @return $this
     */
    public function byCountryId($countryId)
    {
        return $this->andWhere([MarketingCampaign::tableName() . '.country_id' => $countryId]);
    }
}
