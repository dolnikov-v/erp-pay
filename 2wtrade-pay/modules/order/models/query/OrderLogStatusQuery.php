<?php
namespace app\modules\order\models\query;

use app\components\db\ActiveQuery;
use app\modules\order\models\OrderLogStatus;

/**
 * Class OrderLogStatusQuery
 * @package app\modules\order\models\query
 * @method OrderLogStatus one($db = null)
 * @method OrderLogStatus[] all($db = null)
 */
class OrderLogStatusQuery extends ActiveQuery
{

}
