<?php
namespace app\modules\order\models\query;

use app\components\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use app\modules\order\models\SmsPollQuestions;

/**
 * Class SmsPollQuestionsQuery
 * @package app\modules\order\models\query
 * @method SmsPollQuestions one($db = null)
 * @method SmsPollQuestionsQuery[] all($db = null)
 */
class SmsPollQuestionsQuery extends ActiveQuery
{
    /**
     * @var string
     */
    protected $collectionKey = 'id';

    /**
     * @var string
     */
    protected $collectionValue = 'question_text';

    /**
     * @param integer $countryId
     * @return $this
     */
    public function byCountryId($countryId)
    {
        return $this->andWhere([SmsPollQuestions::tableName() . '.country_id' => $countryId]);
    }

    /**
     * @return $this
     */
    public function active()
    {
        return $this->andWhere([SmsPollQuestions::tableName() . '.is_active' => 1]);
    }

    /**
     * @return $this
     */
    public function notActive()
    {
        return $this->andWhere([SmsPollQuestions::tableName() . '.is_active' => 0]);
    }

    /**
     * @param null $language
     * @return array
     */
    public function collection($language = null)
    {
        return ArrayHelper::map($this->all(), $this->collectionKey, $this->collectionValue);
    }

    /**
     * @param array $id
     * @return mixed
     */
    public function byId($id)
    {
        if (!$id) return $this;
        return $this->andWhere([SmsPollQuestions::tableName() . '.id' => $id]);
    }
}
