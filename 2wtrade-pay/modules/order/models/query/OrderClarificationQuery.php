<?php
namespace app\modules\order\models\query;

use app\components\db\ActiveQuery;
use app\modules\order\models\OrderClarification;

/**
 * Class OrderClarificationQuery
 * @package app\modules\order\models\query
 * @method OrderClarification one($db = null)
 * @method OrderClarification[] all($db = null)
 */
class OrderClarificationQuery extends ActiveQuery
{
    /**
     * @param integer $countryId
     * @return $this
     */
    public function byCountryId($countryId)
    {
        return $this->andWhere([OrderClarification::tableName() . '.country_id' => $countryId]);
    }

    public function byOrderId($orderId)
    {
        return $this->andWhere('instr(concat(",", order_ids, ","), ",' . $orderId . ',")>0');
    }
}
