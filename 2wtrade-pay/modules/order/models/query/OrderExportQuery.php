<?php
namespace app\modules\order\models\query;

use app\components\db\ActiveQuery;
use app\modules\order\models\OrderExport;

/**
 * Class OrderQuery
 * @package app\modules\order\models\query
 *
 * @method OrderExport one($db = null)
 * @method OrderExport[] all($db = null)
 */
class OrderExportQuery extends ActiveQuery
{
    /**
     * @param $id
     * @return $this
     */
    public function byId($id)
    {
        return $this->andWhere(['id' => $id]);
    }
}
