<?php
namespace app\modules\order\models\query;

use app\components\db\ActiveQuery;
use app\modules\order\models\OrderStatus;

/**
 * Class OrderStatusQuery
 * @package app\modules\order\models\query
 * @method OrderStatus one($db = null)
 * @method OrderStatus[] all($db = null)
 */
class OrderStatusQuery extends ActiveQuery
{

}
