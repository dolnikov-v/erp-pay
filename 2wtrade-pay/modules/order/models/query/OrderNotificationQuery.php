<?php

namespace app\modules\order\models\query;

use app\components\db\ActiveQuery;
use app\modules\order\models\OrderNotification;

/**
 * Class OrderNotificationQuery
 * @package app\modules\smsnotification\models\query
 */
class OrderNotificationQuery extends ActiveQuery
{
    /**
     * @return $this
     */
    public function active()
    {
        return $this->andWhere([OrderNotification::tableName() . '.active' => 1]);
    }

    /**
     * @param integer $countryId
     * @return $this
     */
    public function byCountryId($countryId)
    {
        return $this->andWhere([OrderNotification::tableName() . '.country_id' => $countryId]);
    }
}
