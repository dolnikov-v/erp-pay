<?php
namespace app\modules\order\models\query;

use app\components\db\ActiveQuery;
use app\modules\order\models\OrderLogisticListInvoiceArchive;

/**
 * Class OrderLogisticListInvoiceArchiveQuery
 * @package app\modules\order\models\query
 * @method OrderLogisticListInvoiceArchive one($db = null)
 * @method OrderLogisticListInvoiceArchive[] all($db = null)
 */
class OrderLogisticListInvoiceArchiveQuery extends ActiveQuery
{
    /**
     * @param integer $listId
     * @return $this
     */
    public function byListId($listId)
    {
        return $this->andWhere(['list_id' => $listId]);
    }

    /**
     * @param string $template
     * @return $this
     */
    public function byTemplate($template)
    {
        return $this->andWhere(['template' => $template]);
    }

    /**
     * @param string $format
     * @return $this
     */
    public function byFormat($format)
    {
        return $this->andWhere(['format' => $format]);
    }

    /**
     * @param string $archive
     * @return $this
     */
    public function byArchive($archive)
    {
        return $this->andWhere(['archive' => $archive]);
    }
}
