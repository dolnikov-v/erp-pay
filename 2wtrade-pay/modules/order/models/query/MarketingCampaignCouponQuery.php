<?php
namespace app\modules\order\models\query;

use app\components\db\ActiveQuery;
use app\modules\order\models\MarketingCampaignCoupon;

/**
 * Class MarketingCampaignCouponQuery
 * @package app\modules\order\models\query
 * @method MarketingCampaignCoupon one($db = null)
 * @method MarketingCampaignCoupon[] all($db = null)
 */
class MarketingCampaignCouponQuery extends ActiveQuery
{
    /**
     * @param integer $campaignId
     * @return $this
     */
    public function byCampaignId($campaignId)
    {
        return $this->andWhere([MarketingCampaignCoupon::tableName() . '.marketing_campaign_id' => $campaignId]);
    }
}
