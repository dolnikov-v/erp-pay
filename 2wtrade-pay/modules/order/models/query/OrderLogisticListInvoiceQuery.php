<?php
namespace app\modules\order\models\query;

use app\components\db\ActiveQuery;
use app\modules\order\models\OrderLogisticListInvoice;

/**
 * Class OrderLogisticListInvoiceQuery
 * @package app\modules\order\models\query
 * @method OrderLogisticListInvoice one($db = null)
 * @method OrderLogisticListInvoice[] all($db = null)
 */
class OrderLogisticListInvoiceQuery extends ActiveQuery
{
    /**
     * @param integer $listId
     * @return $this
     */
    public function byListId($listId)
    {
        return $this->andWhere(['list_id' => $listId]);
    }

    /**
     * @param string $template
     * @return $this
     */
    public function byTemplate($template)
    {
        return $this->andWhere(['template' => $template]);
    }

    /**
     * @param string $format
     * @return $this
     */
    public function byFormat($format)
    {
        return $this->andWhere(['format' => $format]);
    }
}
