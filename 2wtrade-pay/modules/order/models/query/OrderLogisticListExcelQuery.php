<?php
namespace app\modules\order\models\query;

use app\components\db\ActiveQuery;
use app\modules\order\models\OrderLogisticListExcel;

/**
 * Class OrderLogisticListExcelQuery
 * @package app\modules\order\models\query
 * @method OrderLogisticListExcel one($db = null)
 * @method OrderLogisticListExcel[] all($db = null)
 */
class OrderLogisticListExcelQuery extends ActiveQuery
{
    /**
     * @param integer $listId
     * @return $this
     */
    public function byListId($listId)
    {
        return $this->andWhere(['list_id' => $listId]);
    }

    /**
     * @param string $ordersHash
     * @return $this
     */
    public function byOrdersHash($ordersHash)
    {
        return $this->andWhere(['orders_hash' => $ordersHash]);
    }

    /**
     * @param $format
     * @return $this
     */
    public function byFormat($format)
    {
        return $this->andWhere(['format' => $format]);
    }

    /**
     * @param string $columnsHash
     * @return $this
     */
    public function byColumnsHash($columnsHash)
    {
        return $this->andWhere(['columns_hash' => $columnsHash]);
    }

    /**
     * @param $filename
     * @return $this
     */
    public function byFilename($filename)
    {
        return $this->andWhere(['system_file_name' => $filename]);
    }
}
