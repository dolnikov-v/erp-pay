<?php
namespace app\modules\order\models\query;

use app\components\db\ActiveQuery;
use app\modules\order\models\OrderLogProduct;

/**
 * Class OrderLogProductQuery
 * @package app\modules\order\models\query
 * @method OrderLogProduct one($db = null)
 * @method OrderLogProduct[] all($db = null)
 */
class OrderLogProductQuery extends ActiveQuery
{

}
