<?php
namespace app\modules\order\models\query;

use app\modules\order\models\OrderWorkflowStatus;
use yii\db\ActiveQuery;

/**
 * Class OrderWorkflowStatusQuery
 * @package app\modules\order\models\query
 * @method OrderWorkflowStatus one($db = null)
 * @method OrderWorkflowStatus[] all($db = null)
 */
class OrderWorkflowStatusQuery extends ActiveQuery
{

}
