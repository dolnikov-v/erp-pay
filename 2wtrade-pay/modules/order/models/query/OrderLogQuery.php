<?php
namespace app\modules\order\models\query;

use app\components\db\ActiveQuery;
use app\modules\order\models\OrderLog;

/**
 * Class OrderLogQuery
 * @package app\modules\order\models\query
 *
 * @method OrderLog one($db = null)
 * @method OrderLog[] all($db = null)
 */
class OrderLogQuery extends ActiveQuery
{

}
