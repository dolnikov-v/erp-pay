<?php

namespace app\modules\order\models\query;

use app\components\db\ActiveQuery;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use Yii;

/**
 * Class OrderQuery
 * @package app\modules\order\models\query
 *
 * @method Order one($db = null)
 * @method Order[] all($db = null)
 */
class OrderQuery extends ActiveQuery
{
    /**
     * @param string $language the language code (e.g. `en-US`, `en`). If this is null, the current
     * @return array
     */
    public function collection($language = null)
    {
        $collection = [];

        foreach ($this->all() as $status) {
            /** @var OrderStatus $status */
            $collection[$status->id] = Yii::t('common', $status->name, [], $language);
        }

        return $collection;
    }

    /**
     * @param $id
     * @return $this
     */
    public function byId($id)
    {
        return $this->andWhere([Order::tableName() . '.id' => $id]);
    }

    /**
     * @param array $foreignIds
     * @return $this
     */
    public function byIds($ids)
    {
        return $this->andWhere(['in', Order::tableName() . '.id', $ids]);
    }

    /**
     * @param $foreignId
     * @return $this
     */
    public function byForeignId($foreignId)
    {
        return $this->andWhere(['foreign_id' => $foreignId]);
    }

    /**
     * @param $sourceId
     * @return OrderQuery
     */
    public function bySourceId($sourceId)
    {
        return $this->andWhere([Order::tableName() . '.source_id' => $sourceId]);
    }

    /**
     * @param array $foreignIds
     * @return $this
     */
    public function byForeignIds($foreignIds)
    {
        return $this->andWhere(['in', 'foreign_id', $foreignIds]);
    }

    /**
     * @param integer $countryId
     * @return $this
     */
    public function byCountryId($countryId)
    {
        return $this->andWhere([Order::tableName() . '.country_id' => $countryId]);
    }

    /**
     * @param array $countryIds
     * @return $this
     */
    public function byCountryIds($countryIds)
    {
        return $this->andWhere(['in', 'country_id', $countryIds]);
    }

    /**
     * @param integer $statusId
     * @return $this
     */
    public function byStatusId($statusId)
    {
        return $this->andWhere([Order::tableName() . '.status_id' => $statusId]);
    }

    /**
     * @return $this
     */
    public function start()
    {
        return $this->andWhere([
            'id' => [
                OrderStatus::STATUS_SOURCE_LONG_FORM,
                OrderStatus::STATUS_SOURCE_SHORT_FORM,
                OrderStatus::STATUS_CC_INPUT_QUEUE,
            ]
        ]);
    }

    /**
     * @return $this
     */
    public function expressDelivery()
    {
        return $this->innerJoinWith('orderExpressDelivery');
    }
}
