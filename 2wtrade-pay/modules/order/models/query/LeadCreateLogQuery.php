<?php

namespace app\modules\order\models\query;

use yii\mongodb\ActiveQuery;

/**
 * Class LeadCreateLogQuery
 * @package app\modules\order\models\query
 */
class LeadCreateLogQuery extends ActiveQuery
{
    /**
     * @param int $id
     * @return LeadCreateLogQuery
     */
    public function byOrderID(int $id)
    {
        return $this->andWhere(['order_id' => $id]);
    }

    /**
     * @return LeadCreateLogQuery
     */
    public function last()
    {
        return $this->orderBy(['created_at' => SORT_DESC])->limit(1);
    }
}
