<?php
namespace app\modules\order\models\query;

use app\components\db\ActiveQuery;
use app\modules\order\models\OrderLogisticList;

/**
 * Class OrderLogisticListQuery
 * @package app\modules\order\models\query
 * @method OrderLogisticList one($db = null)
 * @method OrderLogisticList[] all($db = null)
 */
class OrderLogisticListQuery extends ActiveQuery
{
    /**
     * @param integer $countryId
     * @return $this
     */
    public function byCountryId($countryId)
    {
        return $this->andWhere([OrderLogisticList::tableName() . '.country_id' => $countryId]);
    }

    /**
     * @param string $ticket
     * @return $this
     */
    public function byTicket($ticket)
    {
        return $this->andWhere(['ticket' => $ticket]);
    }

    /**
     * @param string $invoice
     * @return $this
     */
    public function byInvoice($invoice)
    {
        return $this->andWhere(['invoice' => $invoice]);
    }

    /**
     * @param integer $orderId
     * @return $this
     */
    public function byOrderId($orderId)
    {
        return $this->andWhere('instr(concat(",", ids, ","), ",' . $orderId . ',")>0');
    }
}
