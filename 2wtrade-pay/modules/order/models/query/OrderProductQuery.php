<?php
namespace app\modules\order\models\query;

use yii\db\ActiveQuery;

/**
 * Class OrderProductQuery
 * @package app\modules\order\models\query
 */
class OrderProductQuery extends ActiveQuery
{
    /**
     * @return $this
     */
    public function selectTotal()
    {
        return $this->select('SUM({{price}} * {{quantity}}) as total')->groupBy('{{order_id}}');
    }

    /**
     * @param $orderId
     * @return $this
     */
    public function byOrderId($orderId)
    {
        return $this->andWhere(['order_id' => $orderId]);
    }

    /**
     * @param null $db
     * @return array|\app\modules\order\models\OrderProduct[]
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @param null $db
     * @return array|null|\app\modules\order\models\OrderProduct
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
