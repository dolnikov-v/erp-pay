<?php
namespace app\modules\order\models\query;

use app\components\db\ActiveQuery;
use app\modules\order\models\OrderWorkflow;

/**
 * Class OrderWorkflowQuery
 * @package app\modules\order\models\query
 * @method OrderWorkflow one($db = null)
 * @method OrderWorkflow[] all($db = null)
 */
class OrderWorkflowQuery extends ActiveQuery
{
    /**
     * @return $this
     */
    public function active()
    {
        return $this->andWhere(['active' => 1]);
    }

    /**
     * @return $this
     */
    public function notActive()
    {
        return $this->andWhere(['active' => 0]);
    }
}
