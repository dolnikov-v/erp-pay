<?php
namespace app\modules\order\models\query;

use app\modules\order\models\TempLogOrders;

/**
 * This is the ActiveQuery class for [[TempLogOrders]].
 *
 * @see TempLogOrders
 */
class TempLogOrdersQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return TempLogOrders[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TempLogOrders|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}