<?php
namespace app\modules\order\models\query;

use app\components\db\ActiveQuery;
use app\modules\order\models\OrderLogisticListRequest;

/**
 * Class OrderLogisticListRequestQuery
 * @package app\modules\order\models\query
 * @method OrderLogisticListRequest one($db = null)
 * @method OrderLogisticListRequest[] all($db = null)
 */
class OrderLogisticListRequestQuery extends ActiveQuery
{
    /**
     * @param integer $list
     * @return $this
     */
    public function byList($list)
    {
        return $this->andWhere(['list_id' => $list]);
    }

    /**
     * @param string $email
     * @return $this
     */
    public function byEmail($email)
    {
        return $this->andWhere(['email_to' => $email]);
    }

    /**
     * @param string $hash
     * @return $this
     */
    public function byHash($hash)
    {
        return $this->andWhere(['hash' => $hash]);
    }

    /**
     * @return $this
     */
    public function pending()
    {
        return $this->andWhere(['status' => [OrderLogisticListRequest::STATUS_PENDING, OrderLogisticListRequest::STATUS_ERROR]]);
    }

}
