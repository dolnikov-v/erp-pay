<?php
namespace app\modules\order\models\query;

use app\components\db\ActiveQuery;
use app\modules\order\models\MarketingList;

/**
 * Class MarketingListQuery
 * @package app\modules\order\models\query
 * @method MarketingList one($db = null)
 * @method MarketingList[] all($db = null)
 */
class MarketingListQuery extends ActiveQuery
{
    /**
     * @param integer $countryId
     * @return $this
     */
    public function byCountryId($countryId)
    {
        return $this->andWhere([MarketingList::tableName() . '.country_id' => $countryId]);
    }
}
