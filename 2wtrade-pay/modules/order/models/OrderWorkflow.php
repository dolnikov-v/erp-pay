<?php

namespace app\modules\order\models;

use app\modules\order\models\query\OrderWorkflowQuery;
use Yii;
use yii\db\ActiveRecord;

/**
 * Class OrderWorkflow
 * @property integer $id
 * @property string $name
 * @property string $scheme
 * @property string $comment
 * @property integer $active
 * @property integer $created_at
 * @property integer $updated_at
 * @property OrderWorkflowStatus[] $statuses
 */
class OrderWorkflow extends ActiveRecord
{
    /**
     * @var array
     */
    protected static $nextStatuses = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_workflow}}';
    }

    /**
     * @return query\OrderWorkflowQuery
     */
    public static function find()
    {
        return new OrderWorkflowQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['scheme'], 'string'],
            ['name', 'string', 'max' => 100],
            ['comment', 'string', 'max' => 1000],

            [['name', 'comment'], 'filter', 'filter' => 'trim'],

            ['active', 'default', 'value' => 0],
            ['active', 'number', 'min' => 0, 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'name' => Yii::t('common', 'Название'),
            'comment' => Yii::t('common', 'Комментарий'),
            'scheme' => Yii::t('common', 'Схема'),
            'active' => Yii::t('common', 'Доступность'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatuses()
    {
        return $this->hasMany(OrderWorkflowStatus::className(), ['workflow_id' => 'id']);
    }

    /**
     * @param integer $status
     * @return array
     */
    public function getNextStatuses($status)
    {
        $result = [];

        if (!array_key_exists($this->id, self::$nextStatuses)) {
            self::$nextStatuses[$this->id] = [];

            /** @var OrderWorkflowStatus[] $workflowStatuses */
            $workflowStatuses = $this->statuses;

            foreach ($workflowStatuses as $workflowStatus) {
                self::$nextStatuses[$this->id][$workflowStatus->status_id] = explode(',', $workflowStatus->parents);
            }
        }

        if (array_key_exists($status, self::$nextStatuses[$this->id])) {
            $result = self::$nextStatuses[$this->id][$status];
        }

        return $result;
    }

    /**
     * @param integer $from
     * @param integer $to
     * @return bool
     */
    public function canChangeStatus($from, $to)
    {
        return in_array($to, $this->getNextStatuses($from) ?? []);
    }
}
