<?php

namespace app\modules\order\models;

use Yii;
use yii\db\ActiveRecord;
use app\components\db\ActiveQuery;

/**
 * IssueCollector
 *
 * @property integer $id
 * @property string $name
 * @property string $jira_link
 * @property string $form_link
 * @property string $country_id
 */
class IssueCollector extends ActiveRecord
{
    public $isJira = null;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%issue_collector}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'jira_link', 'country_id'], 'required'],
            [['country_id'], 'integer'],
            [['name', 'jira_link'], 'string', 'max' => 500],
            [['jira_link'], 'validateLink'],
            [['form_link'], 'url','message' => 'Error. Can not create short link. Please try again later'],
            [['name'], 'string', 'max' => 256],
            [['name'], 'unique', 'targetAttribute' => ['name', 'country_id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'name' => Yii::t('common', 'Название'),
            'jira_link' => Yii::t('common', 'Ссылка Jira issue collector'),
            'form_link' => Yii::t('common', 'Короткая ссылка Jira issue collector'),
            'country_id' => Yii::t('common', 'Страна'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public static function find()
    {
        return new ActiveQuery(get_called_class());
    }

    /**
     * @param $attribute
     */
    public function validateLink($attribute)
    {
        if (!substr_count($this->$attribute, 'https://2wtrade-tasks.atlassian.net/')) {
            $this->addError($attribute, Yii::t('common', 'Не правильная ссылка {attribute}', [
                'attribute' => $this->getAttributeLabel($attribute)
            ]));
        };
    }
}
