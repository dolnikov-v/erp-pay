<?php
namespace app\modules\order\models;

use app\components\db\abstracts\ConnectionInterface;
use app\components\db\ActiveRecordLogUpdateTime;
use app\components\db\models\DirtyData;
use app\models\Country;
use app\models\Product;
use app\modules\order\abstracts\OrderProductInterface;
use app\modules\order\models\query\OrderProductQuery;
use app\modules\storage\models\Storage;
use Yii;

/**
 * Class OrderProduct
 * @property integer $id
 * @property integer $order_id
 * @property integer $product_id
 * @property double $price
 * @property integer $quantity
 * @property string $created_at
 * @property string $updated_at
 * @property integer $storage_part_id
 * @property integer $storage_id
 *
 * @property Order $order
 * @property Product $product
 * @property Storage $storage
 */
class OrderProduct extends ActiveRecordLogUpdateTime implements OrderProductInterface
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%order_product}}';
    }

    /**
     * @return query\OrderProductQuery
     */
    public static function find()
    {
        return new OrderProductQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['order_id', 'product_id', 'quantity'], 'required'],
            [['order_id', 'product_id', 'quantity', 'storage_part_id', 'storage_id'], 'integer'],
            [
                'product_id',
                'exist',
                'targetClass' => '\app\models\Product',
                'targetAttribute' => 'id',
                'message' => Yii::t('common', 'Продукт не найден'),
            ],
            ['price', 'double', 'min' => 0],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'order_id' => Yii::t('common', 'Заказ'),
            'product_id' => Yii::t('common', 'Продукт'),
            'price' => Yii::t('common', 'Цена'),
            'quantity' => Yii::t('common', 'Количество'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата обновления'),
            'storage_part_id' => Yii::t('common', 'Складская партия'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStorage()
    {
        return $this->hasOne(Storage::className(), ['id' => 'storage_id']);
    }

    /**
     * @param boolean $insert
     * @return boolean
     */
    public function beforeSave($insert)
    {
        if (empty($this->price)) {
            $this->price = 0;
        }

        $this->createGoodsLogs();

        return parent::beforeSave($insert);
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ((static::getDb() instanceof ConnectionInterface) && Country::find()->where(['id' => $this->order->country_id, 'sink1c' => true])->exists()) {
            $data = [];
            foreach ($changedAttributes as $key => $val) {
                if (($newVal = $this->getAttribute($key)) != $val) {
                    $data[$key] = $newVal;
                }
            }
            if (count($data) > 1 || empty($data['updated_at'])) {
                $data['id'] = $this->id;
                $data['order_id'] = $this->order_id;
                static::getDb()->addBufferChange(new DirtyData([
                    'id' => static::clearTableName(),
                    'action' => $insert ? DirtyData::ACTION_INSERT : DirtyData::ACTION_UPDATE,
                    'data' => $data
                ]));
            }
        }

        $result = OrderProduct::find()
            ->selectTotal()
            ->byOrderId($this->order_id)
            ->asArray()
            ->one();

        $total = $result['total'] ? $result['total'] : 0;

        if ($this->order->price_total != $total) {
            $this->order->price_total = $total;

            if ($this->order->price == 0) {
                $this->order->price = $total;
            }

            $this->order->save();
        }

        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        $data = ['id' => $this->id, 'order_id' => $this->order_id];
        if (static::getDb() instanceof ConnectionInterface && Country::find()->where(['id' => $this->order->country_id, 'sink1c' => true])->exists()) {
            static::getDb()->addBufferChange(new DirtyData([
                'id' => static::clearTableName(),
                'action' => DirtyData::ACTION_DELETE,
                'data' => $data
            ]));
        }

        $result = OrderProduct::find()
            ->selectTotal()
            ->byOrderId($this->order_id)
            ->asArray()
            ->one();

        $total = $result['total'] ? $result['total'] : 0;

        if ($this->order->price_total != $total) {
            $this->order->price_total = $total;

            if ($this->order->price == 0) {
                $this->order->price = $total;
            }

            $this->order->save();
        }

        $orderLogProduct = new OrderLogProduct();
        $orderLogProduct->product_id = $this->product_id;
        $orderLogProduct->order_id = $this->order_id;
        $orderLogProduct->action = OrderLogProduct::ACTION_DELETE;
        $orderLogProduct->save();

        parent::afterDelete();
    }

    /**
     * Логи продуктов при создании и обновлении
     */
    private function createGoodsLogs()
    {
        $model = new OrderLogProduct();
        $model->product_id = $this->product_id;
        $model->order_id = $this->order_id;

        if (!$this->isNewRecord) {
            $model->action = OrderLogProduct::ACTION_UPDATE;

            if (array_key_exists('price', $this->dirtyAttributes) || array_key_exists('quantity', $this->dirtyAttributes)) {
                $model->price = $this->price;
                $model->quantity = $this->quantity;
                $model->save();
            }
        } else {
            $model->action = OrderLogProduct::ACTION_CREATE;
            $model->price = $this->price;
            $model->quantity = $this->quantity;
            $model->save();
        }
    }

    /**
     * @return int
     */
    public function getProductId(): int {
        return $this->product_id;
    }

    /**
     * @return float
     */
    public function getPrice(): float {
        return $this->price;
    }

    /**
     * @return int
     */
    public function getQuantity(): int {
        return $this->quantity;
    }
}
