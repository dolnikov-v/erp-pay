<?php

namespace app\modules\order\models;

use app\components\db\abstracts\ConnectionInterface;
use app\components\db\ActiveRecordLogUpdateTime;
use app\components\db\models\DirtyData;
use app\models\Country;
use app\models\Currency;
use app\models\Landing;
use app\models\OrderCreationDate;
use app\models\Product;
use app\models\Source;
use app\modules\api\models\ApiLog;
use app\modules\bar\models\Bar;
use app\modules\callcenter\models\CallCenter;
use app\modules\callcenter\models\CallCenterCheckRequest;
use app\modules\callcenter\models\CallCenterFullRequest;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\catalog\models\Autotrash;
use app\modules\delivery\abstracts\BrokerResultInterface;
use app\modules\delivery\components\broker\BrokerRequest;
use app\modules\delivery\DeliveryModule;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\jobs\OrderCalculateDeliveryChargesJob;
use app\modules\order\models\query\OrderQuery;
use app\modules\packager\models\OrderPackaging;
use app\modules\report\models\InvoiceOrder;
use Yii;
use yii\behaviors\AttributeBehavior;
use yii\db\Exception;

/**
 * Class Order
 * @property integer $id
 * @property string $source
 * @property string $source_id
 * @property integer $duplicate_order_id
 * @property integer $foreign_id
 * @property integer $country_id
 * @property integer $landing_id
 * @property integer $status_id
 * @property string $customer_ip
 * @property string $customer_code
 * @property string $customer_full_name
 * @property string $customer_email
 * @property string $customer_phone
 * @property string $customer_mobile
 * @property string $customer_province
 * @property string $customer_district
 * @property string $customer_city
 * @property string $customer_city_code
 * @property string $customer_street
 * @property string $customer_house_number
 * @property string $customer_address
 * @property string $customer_address_additional
 * @property string $customer_address_add
 * @property string $customer_zip
 * @property string $longitude
 * @property string $latitude
 * @property string $comment
 * @property double $price
 * @property double $price_total
 * @property integer $price_currency
 * @property double $income
 * @property double $delivery
 * @property string $source_form
 * @property integer $source_confirmed
 * @property integer $pending_delivery_id
 * @property integer $call_center_create
 * @property integer $call_center_again
 * @property string $call_center_type
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $report_status_id
 * @property integer $delivery_time_from
 * @property integer $delivery_time_to
 * @property string $export_history
 * @property string $webmaster_identifier
 * @property string $customer_subdistrict
 * @property Country $country
 * @property Landing $landing
 * @property OrderStatus $status
 * @property OrderProduct[] $orderProducts
 * @property Currency $priceCurrency
 * @property LeadProduct[] $leadProducts
 * @property Product[] $products
 * @property CallCenterRequest $callCenterRequest
 * @property DeliveryRequest $deliveryRequest
 * @property OrderNotificationRequest[] $orderNotificationRequests
 * @property OrderFinanceBalance[] $orderFinanceBalances
 * @property Delivery $pendingDelivery
 * @property OrderFinance $finance
 * @property OrderFinancePrediction $financePrediction
 * @property OrderFinanceFact[] $financeFact
 * @property OrderFinanceFact[] $financeFactReal
 * @property OrderFinanceFact[] $financeFactPretensions
 * @property InvoiceOrder[] $invoiceOrders
 * @property OrderLogStatus[] $orderLogStatuses
 * @property OrderLogisticListInvoice[] $orderLogisticListInvoice
 * @property OrderLogisticListBarcode[] $orderLogisticListBarcodes
 * @property ApiLog $apiLog
 * @property CallCenterCheckRequest[] $checkRequests
 * @property Order $duplicateOrder
 * @property Order $originalOrder
 * @property OrderExpressDelivery $orderExpressDelivery
 * @property string $payment_type
 * @property \yii\db\ActiveQuery $callCenterFullRequest
 * @property double $payment_amount
 * @property integer|bool creationDate
 * @property OrderSendError[] $orderSendErrors
 * @property OrderSendError $deliverySendError
 * @property OrderSendError $callCenterSendError
 * @property OrderFinancePretension[] $orderFinancePretensions
 * @property Lead $lead
 * @property CallCenterCheckRequest[] $callCenterCheckRequests
 * @property Source $sourceModel
 */
class Order extends ActiveRecordLogUpdateTime
{
    const CHECK_PENDING = 'pending';
    const CHECK_DONE = 'done';

    const TYPE_CC_ORDER = 'order';
    const TYPE_CC_NEW_RETURN = 'new_return';
    const TYPE_CC_ORDER_REJECTED = 'order_rejected';
    const TYPE_CC_ORDER_RETURN = 'return';
    const TYPE_CC_ORDER_2WSTORE = '2wstore';
    const TYPE_CC_ORDER_TOPHOT = 'tophot';
    const TYPE_CC_ORDER_REPEAT = 'repeat';
    const TYPE_CC_ORDER_ANALYSIS_TRASH = 'trash_check';
    const TYPE_CC_ORDER_STILL_WAITING = 'still_waiting';
    const TYPE_CC_RETURN_NO_PROD = 'return_no_prod';
    const TYPE_CC_CHECK_UNDELIVERY = 'check_undelivery';
    const TYPE_CC_ORDER_REFUSED = 'order_refused';
    const TYPE_CC_OLD_CRM = 'old_crm';
    const TYPE_CC_POST_SALE = 'post_sale';
    const TYPE_CC_ORDER_MERCADOLIBRE = 'mercadolibre';

    const TYPE_FORM_SHORT = 'short';
    const TYPE_FORM_LONG = 'long';

    const SCENARIO_PREVIOUS = 'previous';

    const ORDER_PAYMENT_TYPE_COD = 'COD';
    const ORDER_PAYMENT_TYPE_PREPAID = 'Prepaid';

    const BLACK_LIST_RULES_36_STATUS_COUNT = 2;
    /**
     * NPAY-798 Дополнительный JOIN order для определения дубликата или оригинала заказа
     */
    public $original;

    public $original_order_id;

    /**
     * @var string Выполняемый экшен, при изменение заказа
     */
    public $route;

    /**
     * @var string Комментарий для лога
     */
    public $commentForLog;

    /**
     * @var bool
     */
    public $shouldValidateStatusId = true;

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%order}}';
    }

    /**
     * @return OrderQuery
     */
    public static function find()
    {
        return new OrderQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function behaviors()
    {

        return array_merge(parent::behaviors(),
            [
                [
                    'class' => AttributeBehavior::class,
                    'attributes' => [
                        static::EVENT_BEFORE_VALIDATE => 'customer_full_name'
                    ],
                    'value' => function () {
                        return empty($this->customer_full_name) ? $this->getOldAttribute('customer_full_name') : $this->customer_full_name;
                    }
                ],
            ]
        );
    }

    /**
     * @return array
     */
    public static function getCheckCollection()
    {
        return [
            self::CHECK_PENDING => Yii::t('common', 'В процессе'),
            self::CHECK_DONE => Yii::t('common', 'Завершен'),
        ];
    }


    /**
     * @return array
     */
    public static function getCallCenterTypesToCheckStatus()
    {
        return [
            self::TYPE_CC_ORDER,
            self::TYPE_CC_NEW_RETURN,
            self::TYPE_CC_ORDER_REJECTED,
            self::TYPE_CC_ORDER_REPEAT,
            self::TYPE_CC_ORDER_RETURN,
            self::TYPE_CC_RETURN_NO_PROD,
            self::TYPE_CC_ORDER_STILL_WAITING,
            self::TYPE_CC_ORDER_REFUSED,
        ];
    }


    /**
     * @return array
     */
    public static function getTypesCallCenter()
    {
        return [
            self::TYPE_CC_ORDER => Yii::t('common', 'Новый заказ'),
            self::TYPE_CC_NEW_RETURN => Yii::t('common', 'Повторный заказ'),
            self::TYPE_CC_ORDER_REJECTED => Yii::t('common', 'Предыдущий заказ отклонён'),
            self::TYPE_CC_ORDER_RETURN => Yii::t('common', 'Очередь 200'),
            self::TYPE_CC_ORDER_2WSTORE => Yii::t('common', 'Создан в 2wstore'),
            self::TYPE_CC_ORDER_TOPHOT => Yii::t('common', 'Создан в tophot'),
            self::TYPE_CC_ORDER_REPEAT => Yii::t('common', 'Постпродажа'),
            self::TYPE_CC_ORDER_STILL_WAITING => Yii::t('common', 'Очередь still_waiting'),
            self::TYPE_CC_RETURN_NO_PROD => Yii::t('common', 'Очередь return_no_prod'),
            self::TYPE_CC_CHECK_UNDELIVERY => Yii::t('common', 'Очередь check_undelivery'),
            self::TYPE_CC_ORDER_REFUSED => Yii::t('common', 'Очередь order_refused'),
            self::TYPE_CC_OLD_CRM => Yii::t('common', 'Очередь прозвона холдов из старого КЦ'),
            self::TYPE_CC_POST_SALE => Yii::t('common', 'Очередь постпродажи'),
            self::TYPE_CC_ORDER_MERCADOLIBRE => Yii::t('common', 'Создан в MercadoLibre'),
        ];
    }

    /**
     * @return array
     */
    public static function getTypesSourceForm()
    {
        return [
            self::TYPE_FORM_SHORT => Yii::t('common', 'Короткая форма'),
            self::TYPE_FORM_LONG => Yii::t('common', 'Длинная форма'),
        ];
    }

    /**
     * @return array
     */
    public static function getBooleanOptions()
    {
        return [
            0 => Yii::t('common', 'Нет'),
            1 => Yii::t('common', 'Да'),
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['status_id', 'country_id', 'customer_full_name', 'customer_phone'], 'required'],
            [
                [
                    'country_id',
                    'landing_id',
                    'status_id',
                    'report_status_id',
                    'duplicate_order_id',
                    'price_currency',
                    'source_id',
                ],
                'integer'
            ],
            ['foreign_id', 'string', 'max' => 32],
            ['foreign_id', 'default', 'value' => null],
            ['source_id', 'default', 'value' => null],
            [['source_id', 'foreign_id'], 'unique', 'targetAttribute' => ['source_id', 'foreign_id']],
            [['longitude', 'latitude', 'price', 'price_total', 'income', 'delivery'], 'double'],
            [
                [
                    'created_at',
                    'updated_at',
                    'delivery_time_from',
                    'delivery_time_to',
                    'webmaster_identifier'
                ],
                'safe'
            ],
            [
                [
                    'customer_ip',
                    'customer_code',
                    'customer_email',
                    'customer_phone',
                    'customer_mobile',
                    'customer_city',
                    'customer_street',
                    'customer_house_number',
                    'customer_zip',
                    'customer_district',
                ],
                'string',
                'max' => 100
            ],
            [
                [
                    'customer_province',
                ],
                'string',
                'max' => 150,
            ],
            [
                [
                    'customer_ip',
                    'customer_code',
                    'customer_email',
                    'customer_phone',
                    'customer_mobile',
                    'customer_province',
                    'customer_city',
                    'customer_street',
                    'customer_house_number',
                    'customer_zip',
                    'customer_district'
                ],
                'filter',
                'filter' => 'trim'
            ],
            [['customer_full_name', 'customer_subdistrict'], 'string', 'max' => 200],
            ['comment', 'validatorCutString', 'params' => ['max' => 1000]],
            ['comment', 'string'],

            ['payment_type', 'string', 'max' => 50],
            ['payment_amount', 'double'],

            ['customer_address_add', 'string', 'max' => 800],
            ['customer_address', 'string', 'max' => 500],
            ['customer_address_additional', 'string', 'max' => 256],

            [
                'status_id',
                'validateStatusId',
                'params' => ['previous' => self::SCENARIO_PREVIOUS],
                'on' => self::SCENARIO_PREVIOUS
            ],

            ['pending_delivery_id', 'integer'],
            [['call_center_create', 'call_center_again'], 'in', 'range' => array_keys(self::getBooleanOptions())],
            ['call_center_type', 'in', 'range' => array_keys(self::getTypesCallCenter())],
            ['source_form', 'in', 'range' => array_keys(self::getTypesSourceForm())],
            ['original_order_id', 'integer'],
            [
                ['price_currency'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Currency::className(),
                'targetAttribute' => ['price_currency' => 'id']
            ],
            [['source_id'], 'exist', 'targetClass' => Source::class, 'targetAttribute' => ['source_id' => 'id']],
        ];
    }

    /**
     * Обрезание строки при валидации, если она превышает максимальную длину
     * @param $attribute
     * @param $params
     */
    public function validatorCutString($attribute, $params)
    {
        if (isset($params['max']) && is_integer($params['max']) && is_string($this->{$attribute}) && mb_strlen($this->{$attribute}) > $params['max']) {
            $this->{$attribute} = mb_strcut($this->{$attribute}, 0, $params['max']);
        }
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'foreign_id' => Yii::t('common', 'Номер в источнике'),
            'duplicate_order_id' => Yii::t('common', 'Дубликат'),
            'country_id' => Yii::t('common', 'Страна'),
            'landing_id' => Yii::t('common', 'Лендинг'),
            'status_id' => Yii::t('common', 'Статус заказа'),
            'customer_ip' => Yii::t('common', 'IP'),
            'customer_code' => Yii::t('common', 'Номер'),
            'customer_full_name' => Yii::t('common', 'Полное имя'),
            'customer_phone' => Yii::t('common', 'Телефон'),
            'customer_mobile' => Yii::t('common', 'Мобильный'),
            'customer_email' => Yii::t('common', 'Email'),
            'customer_province' => Yii::t('common', 'Провинция'),
            'customer_district' => Yii::t('common', 'Район'),
            'customer_city' => Yii::t('common', 'Город'),
            'customer_street' => Yii::t('common', 'Улица'),
            'customer_house_number' => Yii::t('common', 'Номер дома'),
            'customer_address' => Yii::t('common', 'Адрес'),
            'customer_address_add' => Yii::t('common', 'Добавочный адрес'),
            'customer_address_additional' => Yii::t('common', 'Доп. адрес'),
            'customer_zip' => Yii::t('common', 'Почтовый индекс'),
            'longitude' => Yii::t('common', 'Долгота'),
            'latitude' => Yii::t('common', 'Широта'),
            'comment' => Yii::t('common', 'Комментарий'),
            'price' => Yii::t('common', 'Начальная цена'),
            'price_total' => Yii::t('common', 'Конечная цена'),
            'price_currency' => Yii::t('common', 'Валюта'),
            'income' => Yii::t('common', 'Цена лида'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
            'delivery_date' => Yii::t('common', 'Дата доставки'),
            'delivery_time_from' => Yii::t('common', 'Дата доставки'),
            'delivery_time_to' => Yii::t('common', 'Дата доставки до'),
            'delivery' => Yii::t('common', 'Цена доставки'),
            'pending_delivery_id' => Yii::t('common', 'Отложенная служба доставки'),
            'call_center_create' => Yii::t('common', 'Создано в колл-центре'),
            'call_center_again' => Yii::t('common', 'Повторная отправка'),
            'call_center_type' => Yii::t('common', 'Тип очереди'),
            'callCenterRequest.foreign_id' => Yii::t('common', 'Номер в колл-центре'),
            'customer_subdistrict' => Yii::t('common', 'Квартал заказчика'),
            'originalOrder.id' => Yii::t('common', 'Оригинал'),
            'payment_type' => Yii::t('common', 'Тип оплаты'),
            'payment_amount' => Yii::t('common', 'Предоплата'),
            'source_id' => Yii::t('common', 'Источник'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanding()
    {
        return $this->hasOne(Landing::className(), ['id' => 'landing_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(OrderStatus::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderProducts()
    {
        return $this->hasMany(OrderProduct::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['id' => 'product_id'])
            ->via('orderProducts');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLeadProducts()
    {
        return $this->hasMany(LeadProduct::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCallCenterRequest()
    {
        return $this->hasOne(CallCenterRequest::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCallCenterFullRequest()
    {
        return $this->hasOne(CallCenterFullRequest::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveryRequest()
    {
        return $this->hasOne(DeliveryRequest::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPendingDelivery()
    {
        return $this->hasOne(Delivery::className(), ['id' => 'pending_delivery_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinance()
    {
        return $this->hasOne(OrderFinance::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinancePrediction()
    {
        return $this->hasOne(OrderFinancePrediction::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceFact()
    {
        return $this->hasMany(OrderFinanceFact::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceFactReal()
    {
        return $this->hasMany(OrderFinanceFact::className(), ['order_id' => 'id'])->andWhere([
            'is',
            'pretension',
            null
        ]);
    }

    /**
     * Претензии перенесены в orderFinancePretensions
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceFactPretensions()
    {
        return $this->hasMany(OrderFinanceFact::className(), ['order_id' => 'id'])->andWhere([
            'is not',
            'pretension',
            null
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderLogStatuses()
    {
        return $this->hasMany(OrderLogStatus::className(), ['order_id' => 'id']);
    }

    /**
     * @return query\LeadCreateLogQuery
     */
    public function getApiLog()
    {
        return LeadCreateLog::find()->byOrderID($this->id)->last();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderNotificationRequests()
    {
        return $this->hasMany(OrderNotificationRequest::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderLogisticListInvoice()
    {
        return $this->hasOne(OrderLogisticListInvoice::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDuplicateOrder()
    {
        return $this->hasOne(Order::className(),
            ['id' => 'duplicate_order_id'])->from(['duplicate_order' => Order::tableName()]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOriginalOrder()
    {
        return $this->hasOne(Order::className(),
            ['duplicate_order_id' => 'id'])->from(['original' => Order::tableName()]);
    }

    /**
     * @return bool|integer
     */
    public function getCreationDate()
    {
        return $this->hasOne(OrderCreationDate::className(), ['order_id' => 'id'])
            ->select('foreign_create_at')
            ->scalar();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSourceModel()
    {
        return $this->hasOne(Source::class, ['id' => 'source_id']);
    }

    /**
     * @param string $attribute
     * @param array $params
     */
    public function validateStatusId($attribute, $params)
    {
        $collection = OrderStatus::find()->collection();

        if (!array_key_exists($this->status_id, $collection)) {
            $this->addError($attribute, Yii::t('common', 'Неверное значение статуса.'));
        }

        if (!$this->isNewRecord && $this->shouldValidateStatusId) {
            $oldStatusId = $this->getOldAttribute('status_id');

            if ($this->status_id != $oldStatusId) {
                if ($params['previous'] === self::SCENARIO_PREVIOUS) {
                    if (!$this->couldChangedStatusFrom($this->status_id, $oldStatusId)) {
                        $this->addError($attribute, Yii::t('common',
                            'Неразрешенное значение статуса (из ' . $oldStatusId . ' в ' . $this->status_id . ', обратный переход).'));
                    }
                } else {
                    if (!$this->canUpdateStatusTo($this->status_id, $oldStatusId)) {
                        $this->addError($attribute, Yii::t('common',
                            'Неразрешенное значение статуса (из ' . $oldStatusId . ' в ' . $this->status_id . ').'));
                    }
                }
            }
        }
    }

    /**
     * Creating copy for order with products
     * @param boolean $createCallCenterRequest
     * @param array $newOrderProperties
     * @param boolean $createLead
     * @return Order
     * @throws Exception
     */
    public function duplicate($createCallCenterRequest = false, $newOrderProperties = [], $createLead = false)
    {
        $orderCopy = new Order;

        $orderCopy->attributes = $this->attributes;
        $orderCopy->foreign_id = null;
        $orderCopy->source_id = null;
        $orderCopy->income = null;

        if ($newOrderProperties) {
            foreach ($newOrderProperties as $key => $value) {
                if ($orderCopy->hasProperty($key)) {
                    $orderCopy->$key = $value;
                }
            }
        }

        if (!$orderCopy->save()) {
            throw new Exception(Yii::t('common', 'Не удалось создать копию заказа: {error}', ['error' => $orderCopy->getFirstErrorAsString()]));
        }

        $this->duplicate_order_id = $orderCopy->id;
        if (!$this->save(true, ['duplicate_order_id'])) {
            throw new Exception(Yii::t('common', 'Не удалось сохранить заказ: {error}', ['error' => $this->getFirstErrorAsString()]));
        }

        $this->copyProductsForDuplicateOrder();

        if ($createCallCenterRequest) {
            $this->copyCallCenterRequestForDuplicateOrder();
        }

        if ($createLead) {
            if (!$orderCopy->lead) {
                $leadCopy = new Lead();
                if ($this->lead) {
                    $leadCopy->attributes = $this->lead->attributes;
                }
                $leadCopy->foreign_id = $this->id;
                $leadCopy->order_id = $orderCopy->id;
                $leadCopy->source_id = $orderCopy->source_id;
                $leadCopy->revenue = $this->income ?? 0;
                $leadCopy->country = $orderCopy->country->char_code;
                $leadCopy->site = $orderCopy->landing->url;
                $leadCopy->ip = $orderCopy->customer_ip;
                $leadCopy->name = $orderCopy->customer_full_name;
                $leadCopy->phone = $orderCopy->customer_phone;
                $leadCopy->second_phone = $orderCopy->customer_mobile;
                $leadCopy->city = $orderCopy->customer_city;
                $leadCopy->address = $orderCopy->customer_address;
                $leadCopy->postal_code = $orderCopy->customer_zip;
                $leadCopy->email = $orderCopy->customer_email;
                $leadCopy->comment = $orderCopy->comment;
                $leadCopy->price = $orderCopy->price;
                $leadCopy->total_price = $orderCopy->price_total;
                $leadCopy->price_currency = $orderCopy->price_currency;
                $leadCopy->delivery = $orderCopy->delivery;
                $leadCopy->web_id = $orderCopy->webmaster_identifier;
                $leadCopy->goods_id = $orderCopy->orderProducts[0]->product_id;
                $leadCopy->num = $orderCopy->orderProducts[0]->quantity;


                if ($leadCopy->validate()) {
                    $leadCopy->save(false);
                } else {
                    throw new Exception(Yii::t('common', 'Не удалось создать лид копии заказа: {error}', ['error' => $leadCopy->getFirstErrorAsString()]));
                }
            }
        }

        return $orderCopy;
    }

    /**
     * Creating products for duplicate order
     * @return Order
     * @throws Exception
     */
    protected function copyProductsForDuplicateOrder()
    {
        foreach ($this->getOrderProducts()->all() as $orderProduct) {
            $orderProductCopy = new OrderProduct;
            $orderProductCopy->setAttributes($orderProduct->attributes);
            $orderProductCopy->order_id = $this->duplicate_order_id;
            if (!$orderProductCopy->save()) {
                throw new Exception(Yii::t('common', 'Не удалось создать продукт в заказе: {error}', ['error' => $orderProductCopy->getFirstErrorAsString()]));
            }
        }

        foreach ($this->getLeadProducts()->all() as $leadProduct) {
            $leadProductCopy = new LeadProduct();
            $leadProductCopy->setAttributes($leadProduct->attributes);
            $leadProductCopy->order_id = $this->duplicate_order_id;
            if (!$leadProductCopy->save()) {
                throw new Exception(Yii::t('common', 'Не удалось создать продукт в заказе: {error}', ['error' => $leadProductCopy->getFirstErrorAsString()]));
            }
        }
        return $this;
    }

    /**
     * @return Order
     * @throws Exception
     */
    protected function copyCallCenterRequestForDuplicateOrder()
    {
        $callCenterRequest = new CallCenterRequest();
        $callCenterRequest->setAttributes($this->callCenterRequest->attributes);
        $callCenterRequest->order_id = $this->duplicate_order_id;
        if (!$callCenterRequest->save()) {
            throw new Exception(Yii::t('common', 'Не удалось создать заявку в КЦ: {error}', ['error' => $callCenterRequest->getFirstErrorAsString()]));
        }
        return $this;
    }

    /**
     * @param Delivery $delivery
     * @param bool $createDeliveryRequest
     * @return Order
     * @throws Exception
     */
    public function duplicateIntoAnotherDelivery($delivery, $createDeliveryRequest = true)
    {
        $orderCopy = new Order;

        $orderCopy->route = Yii::$app->controller->route;
        $orderCopy->attributes = $this->attributes;
        $orderCopy->foreign_id = null;
        $orderCopy->source_id = null;
        $orderCopy->income = null;
        $orderCopy->pending_delivery_id = $delivery->id;
        $orderCopy->status_id = OrderStatus::STATUS_CC_APPROVED;

        $orderCopy->save();

        $this->duplicate_order_id = $orderCopy->id;
        $this->save();

        $this->copyProductsForDuplicateOrder();
        $this->copyCallCenterRequestForDuplicateOrder();

        if ($delivery->workflow) {
            $allowStatuses = $delivery->workflow->getNextStatuses($orderCopy->status_id);
        } else {
            $allowStatuses = $orderCopy->getNextStatuses($orderCopy->status_id);
        }

        if (!in_array(OrderStatus::STATUS_LOG_GENERATED, $allowStatuses) && $createDeliveryRequest) {
            Delivery::createRequest($delivery, $orderCopy, false);
        }

        return $orderCopy;
    }

    /**
     * @param boolean $insert
     * @return boolean
     */
    public function beforeSave($insert)
    {
        $groupId = uniqid();

        $dirtyAttributes = $this->getDirtyAttributes();

        foreach ($dirtyAttributes as $key => $item) {
            if ($item != $this->getOldAttribute($key)) {
                $old = (string)$this->getOldAttribute($key);

                $log = new OrderLog();
                $log->order_id = $this->id;
                $log->group_id = $groupId;
                $log->field = $key;
                $log->old = mb_substr($old, 0, 250);
                $log->new = mb_substr($item, 0, 250);
                $log->user_id = isset(Yii::$app->user) ? Yii::$app->user->id : null;
                $log->route = $this->route ?: isset(Yii::$app->controller) ? Yii::$app->controller->getRoute() : null;
                $log->comment = $this->commentForLog;
                $log->save();

                if ($key == 'status_id') {
                    $breakpoint = new OrderLogStatus();
                    $breakpoint->order_id = $this->id;
                    $breakpoint->group_id = $groupId;
                    $breakpoint->old = $this->getOldAttribute($key);
                    $breakpoint->new = $item;
                    $breakpoint->comment = $this->commentForLog;
                    $breakpoint->save();

                    OrderNotificationRequest::createByOrder($this);
                }
            }
        }

        return parent::beforeSave($insert);
    }

    /**
     *  Отправка заказа в очередь на пересчет расходов на доставку
     */
    public function sendToCalculateDeliveryCharges()
    {
        $this->queueOrders->push(new OrderCalculateDeliveryChargesJob(['orderId' => $this->id]));
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        $changedAttributesNames = array_keys($changedAttributes);

        //Set "bought" status to order products bar codes
        if (
            ($this->status_id == OrderStatus::STATUS_DELIVERY_BUYOUT || $this->status_id == OrderStatus::STATUS_FINANCE_MONEY_RECEIVED)
            && $this->isAttributeChanged('status_id')
            && Bar::findOne(['order_id' => $this->id])
        ) {
            Bar::orderBuyOut($this->id);
        }

        if (in_array('status_id', $changedAttributesNames)) {
            if (in_array($this->status_id, OrderStatus::getInDeliveryStatusList())) {
                try {
                    $this->sendToCalculateDeliveryCharges();
                } catch (\Throwable $e) {
                    $logger = Yii::$app->get('processingLogger');
                    $cronlog = $logger->getLogger([
                        'order_id' => $this->id,
                        'type' => 'dont_send_to_calculate_delivery_charges',
                    ]);
                    $cronlog($e->getMessage());
                }
            }
            //https://2wtrade-tasks.atlassian.net/browse/ERP-540
            if ($this->status_id == OrderStatus::STATUS_CLIENT_REFUSED && $this->getCountByStatusOrder($this, OrderStatus::STATUS_CLIENT_REFUSED) >= Order::BLACK_LIST_RULES_36_STATUS_COUNT) {
                $this->checkBlackListRules($this);
            }
        }

//        ERP-875 --->
        if (!$insert && !empty($changedAttributes['status_id']) && in_array($this->status_id, [
                OrderStatus::STATUS_DELIVERY_BUYOUT,
                OrderStatus::STATUS_DELIVERY_DENIAL,
                OrderStatus::STATUS_DELIVERY_RETURNED,
                OrderStatus::STATUS_FINANCE_MONEY_RECEIVED,
                OrderStatus::STATUS_DELIVERY_REFUND
            ]) && $this->deliveryRequest instanceof DeliveryRequest && empty($this->deliveryRequest->approved_at)) {
            $this->deliveryRequest->approved_at = time();
            $this->deliveryRequest->save(true, ['approved_at']);
        }
//        <--- ERP-875

        if (!$insert && !empty($changedAttributes['status_id']) && in_array($this->status_id, [
                OrderStatus::STATUS_DELIVERY_DENIAL,
                OrderStatus::STATUS_DELIVERY_RETURNED,
            ])) {

            if ($this->wasInBuyoutStatus()) {
                $this->makeFinancePretension(OrderFinancePretension::PRETENSION_TYPE_STATUS);
            } elseif ($this->shouldSendCheckUndelivery()) {
                $this->sendToCheckUndelivery();
            }
        }

        if ($insert) {
            OrderNotificationRequest::createByOrder($this);
        }

        if ((static::getDb() instanceof ConnectionInterface) && Country::find()->where(['id' => $this->country_id, 'sink1c' => true])->exists()) {
            $data = [];
            foreach ($changedAttributes as $key => $val) {
                if (($newVal = $this->getAttribute($key)) != $val) {
                    $data[$key] = $newVal;
                }
            }
            if ($data) {
                $data['id'] = $this->id;
                $data['country_id'] = $this->country_id;
                static::getDb()->addBufferChange(new DirtyData([
                    'id' => static::clearTableName(),
                    'action' => $insert ? DirtyData::ACTION_INSERT : DirtyData::ACTION_UPDATE,
                    'data' => $data
                ]));
            }
        }

        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * Если заказ 2 раз получает статус - отказ клиента - создать правило для автотреша по номеру телефона и адреса
     * @param Order $order
     * @return array
     */
    public function checkBlackListRules($order)
    {
        $existsRulesAutoTrash = Autotrash::find()
            ->where([
                'template' => $order->customer_phone,
                'country_id' => $order->country->id,
                'is_active' => Autotrash::ACTIVE
            ])
            ->exists();

        if (!$existsRulesAutoTrash) {
            foreach ([Autotrash::FIELD_CUSTOMER_ADDRESS, Autotrash::FIELD_CUSTOMER_PHONE] as $field) {
                $model = new Autotrash();
                //многи заказы приходят с пустым адресом, это не повод создавать правило автотреша - будет беда!
                if ($field == Autotrash::FIELD_CUSTOMER_ADDRESS && !empty($order->customer_address)) {
                    $model->template = '#^' . mb_substr($order->customer_address, 0, 255, 'utf-8') . '#';
                    $model->filter = Autotrash::FILTER_REGEXP;
                } elseif ($field == Autotrash::FIELD_CUSTOMER_PHONE) {
                    $model->template = $order->customer_phone;
                    $model->filter = Autotrash::FILTER_PHONE;
                }

                if ($model->template) {
                    $model->field = $field;
                    $model->is_active = Autotrash::ACTIVE;
                    $model->country_id = $order->country->id;
                    $model->created_at = time();
                    //может где и пригодится
                    if (!$model->save()) {
                        $result = [
                            'success' => false,
                            'message' => json_encode($model->getErrors(), JSON_UNESCAPED_UNICODE)
                        ];
                    } else {
                        $result = [
                            'success' => true
                        ];
                    }
                }
            }

        }

        if (!isset($result)) {
            $result = [
                'success' => true
            ];
        }

        return $result;
    }

    /**
     * @param Order $order
     * @param integer $status
     * @return int
     */
    public function getCountByStatusOrder($order, $status)
    {
        return OrderLogStatus::find()->where([
            'order_id' => $order->id,
            'new' => $status
        ])->count();
    }

    /**
     * @param integer|null $status
     * @param boolean $withCurrentStatus
     * @param bool $is_delivery
     * @return array
     */
    public function getNextStatuses($status = null, $withCurrentStatus = false, $is_delivery = false)
    {
        $statuses = [];

        $allowOrderStatuses = $this->country ? $this->country->getAllowOrderStatuses() : Yii::$app->user->country->getAllowOrderStatuses();

        if (is_null($status)) {
            $startStatuses = [
                OrderStatus::STATUS_SOURCE_LONG_FORM,
                OrderStatus::STATUS_SOURCE_SHORT_FORM,
                OrderStatus::STATUS_CC_INPUT_QUEUE,
            ];

            foreach ($startStatuses as $status) {
                if (array_key_exists($status, $allowOrderStatuses)) {
                    $statuses[] = $status;
                }
            }
        } else {
            if (array_key_exists($status, $allowOrderStatuses)) {
                $statuses = $allowOrderStatuses[$status];
            }

            if ($withCurrentStatus) {
                $statuses[] = $status;
            }
        }

        //NPAY 896
        if ($is_delivery) {
            //если текущий статус получен из отчёта курьерки, то ручные Workflow не применяются
            if ($this->status_id != $this->report_status_id) {
                //список workflow_id
                $listManualWorkflow = [13, 16];

                $workflows = OrderWorkflow::find()
                    ->where(['in', 'id', $listManualWorkflow])
                    ->andWhere(['active' => 1])
                    ->asArray()
                    ->all();

                //собрать массив вида status_from => [statuses_to]
                $workflow_data = [];

                foreach ($workflows as $k => $workflow) {
                    $scheme = json_decode($workflow['scheme'], 1);

                    $linkDataArray = $scheme['linkDataArray'];

                    foreach ($linkDataArray as $j => $linkData) {
                        $workflow_data[$linkData['from']][] = $linkData['to'];
                    }
                }

                //применение Workflow
                foreach ($workflow_data as $statusFrom => $data) {
                    if ($status == $statusFrom) {
                        $statuses = array_merge($statuses, $data);
                    }
                }
            }
        }
        //END NPAY 896

        return $statuses;
    }

    /**
     * @param integer|null $status
     * @param boolean $withCurrentStatus
     * @return array
     */
    public function getPreviousStatuses($status, $withCurrentStatus = false)
    {
        $statuses = [];

        $allowOrderStatuses = $this->country ? $this->country->getAllowOrderStatuses() : Yii::$app->user->country->getAllowOrderStatuses();

        foreach ($allowOrderStatuses as $allowStatus => $allowStatuses) {
            if (array_search($status, $allowStatuses) !== false) {
                if (array_search($allowStatus, $statuses) === false) {
                    $statuses[] = $allowStatus;
                }
            }
        }
        if ($withCurrentStatus) {
            $statuses[] = $status;
        }
        return $statuses;
    }

    /**
     * @param integer $status
     * @param boolean $withCurrentStatus
     * @return array
     */
    public function getAllNextStatuses($status, $withCurrentStatus = false)
    {
        $statuses = [$status];

        $allowOrderStatuses = $this->country ? $this->country->getAllowOrderStatuses() : Yii::$app->user->country->getAllowOrderStatuses();

        for ($i = 0; $i < count($statuses); $i++) {
            $status = $statuses[$i];
            $buffer = [];
            if (array_key_exists($status, $allowOrderStatuses)) {
                $buffer = $allowOrderStatuses[$status];
            }
            foreach ($buffer as $item) {
                if (array_search($item, $statuses) === false) {
                    $statuses[] = $item;
                }
            }
        }

        if (!$withCurrentStatus) {
            unset($statuses[0]);
        }

        return $statuses;
    }

    /**
     * @param integer $status
     * @param boolean $withCurrentStatus
     * @return array
     */
    public function getAllPreviousStatuses($status, $withCurrentStatus = false)
    {
        $statuses = [$status];

        $allowOrderStatuses = $this->country ? $this->country->getAllowOrderStatuses() : Yii::$app->user->country->getAllowOrderStatuses();

        for ($i = 0; $i < count($statuses); $i++) {
            $status = $statuses[$i];
            foreach ($allowOrderStatuses as $allowStatus => $allowStatuses) {
                if (array_search($status, $allowStatuses) !== false) {
                    if (array_search($allowStatus, $statuses) === false) {
                        $statuses[] = $allowStatus;
                    }
                }
            }
        }

        if (!$withCurrentStatus) {
            unset($statuses[0]);
        }

        return $statuses;
    }

    /**
     * @param integer $statusTo
     * @param integer $statusFrom
     * @param bool $is_delivery -признак того, что происходит попытка смены статуса из курьерки
     * @return boolean
     */
    public function canChangeStatusTo($statusTo, $statusFrom = null, $is_delivery = false)
    {
        if (is_null($statusFrom)) {
            $statusFrom = $this->status_id;
        }

        $nextStatuses = $this->getNextStatuses($statusFrom, false, $is_delivery);

        return in_array($statusTo, $nextStatuses);
    }

    /**
     * @param integer $statusTo
     * @param integer $statusFrom
     * @return boolean
     */
    public function couldChangedStatusFrom($statusTo, $statusFrom = null)
    {
        if (is_null($statusFrom)) {
            $statusFrom = $this->status_id;
        }

        $nextStatuses = $this->getPreviousStatuses($statusFrom);

        return in_array($statusTo, $nextStatuses);
    }

    /**
     * @param integer $statusTo
     * @param integer $statusFrom
     * @return boolean
     */
    public function canUpdateStatusTo($statusTo, $statusFrom = null)
    {
        if (is_null($statusFrom)) {
            $statusFrom = $this->status_id;
        }

        $nextStatuses = $this->getAllNextStatuses($statusFrom);
        return in_array($statusTo, $nextStatuses);
    }

    /**
     * @param integer $status
     * @return boolean
     */
    public function couldUpdatedStatusFrom($status)
    {
        $previousStatuses = $this->getAllPreviousStatuses($this->status_id);
        return in_array($status, $previousStatuses);
    }

    /**
     * @param array $post
     * @param OrderProduct[] $currentOrderProducts
     * @return boolean
     */
    public function saveProductsFromPost($post, $currentOrderProducts)
    {
        $saved = $productsData = [];

        if (isset($post['id']) && is_array($post['id'])) {
            $ids = $post['id'];

            foreach ($ids as $index => $id) {
                $model = null;

                if ($id) {
                    foreach ($currentOrderProducts as $currentOrderProduct) {
                        if ($currentOrderProduct->id == $id) {
                            $model = $currentOrderProduct;
                            break;
                        }
                    }
                } else {
                    $model = new OrderProduct();
                    $model->order_id = $this->id;
                }

                if (is_null($model)) {
                    $this->addError('product', Yii::t('common', 'Не удалось определить товар.'));
                    break;
                } else {
                    $model->product_id = array_key_exists(
                        $index,
                        $post['product_id']
                    ) ? $post['product_id'][$index] : null;
                    $model->price = array_key_exists($index, $post['price']) ? $post['price'][$index] : null;
                    $model->quantity = array_key_exists($index, $post['quantity']) ? $post['quantity'][$index] : null;

                    if ($model->save()) {
                        $saved[] = $model->id;
                    } else {
                        $error = $model->getErrorsAsArray()[0];
                        $this->addError('product', $error);
                        break;
                    }
                }
            }
        }

        if ($saved) {
            foreach ($currentOrderProducts as $currentOrderProduct) {
                if (!in_array($currentOrderProduct->id, $saved)) {
                    $currentOrderProduct->delete();
                }
            }
        } else {
            $this->addError('product', Yii::t('common', 'Отсутствует информация о товарах.'));
        }

        return $this->hasErrors() ? false : true;
    }

    /**
     * Брокер. Определить службу доставки и стоимость доставки
     * @param OrderProduct[] $products заказанные товары
     * @param integer $packageId акция
     * @param float|null $shippingPrice полученная стоимость доставки
     * @return array|false
     */
    public function findDeliveryByBroker($products, $packageId = null, $shippingPrice = null)
    {
        $deliveryId = null;
        $deliveryPrice = null;
        if ($this->country->brokerage_enabled) {

            $brokerRequest = new BrokerRequest([
                'country_id' => $this->country_id,
                'package_id' => $packageId,
                'products' => $products,
                'customer_zip' => $this->customer_zip,
                'customer_city' => $this->customer_city,
                'customer_province' => $this->customer_province,
                'express' => $this->orderExpressDelivery ? true : false,
                'order_price' => $this->price_total,
                'shipping_price' => $shippingPrice
            ]);

            /** @var DeliveryModule $deliveryModule */
            $deliveryModule = Yii::$app->getModule('delivery');
            $brokerResults = $deliveryModule->broker->getAvailableDeliveries($brokerRequest);
            if ($brokerResults) {
                /** @var BrokerResultInterface $brokerResult */
                $brokerResult = array_shift($brokerResults);
                $deliveryId = $brokerResult->getDeliveryId();
                $deliveryPrice = $brokerResult->getDeliveryPrice();
            }
            return [
                'deliveryId' => $deliveryId,
                'deliveryPrice' => $deliveryPrice
            ];
        }
        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCallCenterCheckRequests()
    {
        return $this->hasMany(CallCenterCheckRequest::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastCallCenterCheckRequest()
    {
        return $this->hasOne(CallCenterCheckRequest::className(), ['order_id' => 'id'])
            ->orderBy(['id' => SORT_DESC])
            ->limit(1);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderExpressDelivery()
    {
        return $this->hasOne(OrderExpressDelivery::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderSendErrors()
    {
        return $this->hasMany(OrderSendError::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliverySendError()
    {
        return $this->hasOne(OrderSendError::className(), ['order_id' => 'id'])
            ->andOnCondition(['type' => OrderSendError::TYPE_DELIVERY_SEND]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCallCenterSendError()
    {
        return $this->hasOne(OrderSendError::className(), ['order_id' => 'id'])
            ->andOnCondition(['type' => OrderSendError::TYPE_CALL_CENTER_SEND]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLead()
    {
        return $this->hasOne(Lead::className(), ['order_id' => 'id']);
    }

    /**
     * @return int
     */
    public function getPreviousStatus()
    {
        return OrderLogStatus::find()
            ->select(['old'])
            ->where(['order_id' => $this->id])
            ->orderBy(['id' => SORT_DESC])
            ->limit(1)
            ->scalar();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriceCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'price_currency']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderFinancePretensions()
    {
        return $this->hasMany(OrderFinancePretension::className(), ['order_id' => 'id'])
            ->andOnCondition(OrderFinancePretension::tableName() . '.' . OrderFinancePretension::COLUMN_DISABLED . '!=1');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderFinanceBalances()
    {
        return $this->hasMany(OrderFinanceBalance::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceOrders()
    {
        return $this->hasMany(InvoiceOrder::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderLogisticListBarcodes()
    {
        return $this->hasMany(OrderLogisticListBarcode::className(), ['order_id' => 'id']);
    }

    /**
     * Должен ли быть переход в службу упаковки
     * @param Delivery $delivery
     * @return bool
     */
    public function shouldUsePackageService(Delivery $delivery)
    {
        if ($this->canChangeStatusTo(OrderStatus::STATUS_LOG_ACCEPTED)
            && $delivery->getOurStorage()
            && $delivery->packager_id) {
            return true;
        }
        return false;
    }

    /**
     * Переход в службу упаковки
     * @param bool $needValidate
     * @param int $deliveryId
     * @throws \Exception
     */
    public function initPackageService($needValidate = true, $deliveryId)
    {
        $shouldUse = $needValidate ? $this->shouldUsePackageService($this->deliveryRequest->delivery) : true;
        if ($shouldUse) {

            $this->status_id = OrderStatus::STATUS_LOG_ACCEPTED;
            if (!$this->save(true, ['status_id'])) {
                throw new \Exception($this->getFirstErrorAsString());
            }

            if (!$this->deliveryRequest) {
                $deliveryRequest = new DeliveryRequest();
                $deliveryRequest->delivery_id = $deliveryId;
                $deliveryRequest->order_id = $this->id;
                $deliveryRequest->status = DeliveryRequest::STATUS_COMPLETION;
                if (!$deliveryRequest->save()) {
                    throw new \Exception($deliveryRequest->getFirstErrorAsString());
                }
            } else {
                $this->deliveryRequest->status = DeliveryRequest::STATUS_COMPLETION;
                if (!$this->deliveryRequest->save(true, ['status'])) {
                    throw new \Exception($this->deliveryRequest->getFirstErrorAsString());
                }
                $deliveryRequest = $this->deliveryRequest;
            }

            $params = [
                'order_id' => $this->id,
                'delivery_id' => $deliveryRequest->delivery_id,
                'packager_id' => $deliveryRequest->delivery->packager_id,
            ];
            $orderPackaging = OrderPackaging::findOne($params) ?: new OrderPackaging($params);
            $orderPackaging->status = OrderPackaging::STATUS_PENDING;
            $orderPackaging->method = ($deliveryRequest->delivery->packager->auto_sending) ? OrderPackaging::METHOD_EMAIL : OrderPackaging::METHOD_MANUAL;
            if (!$orderPackaging->save()) {
                throw new \Exception($orderPackaging->getFirstErrorAsString());
            }
        }
    }


    /**
     * Был ли заказ в доставлено
     * @return bool
     */
    public function wasInBuyoutStatus()
    {
        return OrderLogStatus::find()
            ->where(['order_id' => $this->id])
            ->andWhere(['in', 'new', OrderStatus::getBuyoutList()])
            ->exists();
    }

    /**
     * Должен ли быть отправлен отказ на проверку в очередь check_undelivery
     * @return bool
     */
    public function shouldSendCheckUndelivery()
    {
        if (!CallCenterCheckRequest::find()->where([
                'order_id' => $this->id,
                'type' => CallCenterCheckRequest::TYPE_CHECKUNDELIVERY
            ])->exists() &&
            $this->deliveryRequest instanceof DeliveryRequest &&
            $this->deliveryRequest->delivery->send_to_check_undelivery) {
            return true;
        }
        return false;
    }


    /**
     * Отправка в очередь check_undelivery
     * @return bool
     */
    public function sendToCheckUndelivery()
    {
        $orderCheckHistory = new CallCenterCheckRequest();
        $orderCheckHistory->order_id = $this->id;
        if (isset(Yii::$app->user->id)) {
            $orderCheckHistory->user_id = Yii::$app->user->id;
        }
        $orderCheckHistory->status_id = $this->status_id;
        $orderCheckHistory->type = CallCenterCheckRequest::TYPE_CHECKUNDELIVERY;
        $orderCheckHistory->status = CallCenterCheckRequest::STATUS_PENDING;
        if ($this->getCallCenterRequest()->exists()) {
            $orderCheckHistory->call_center_id = $this->callCenterRequest->call_center_id;
        } else {
            $callCenter = CallCenter::findOne([
                'active' => 1,
                'is_amazon_query' => 1,
                'country_id' => $this->country_id,
            ]);
            if (!$callCenter) {
                return false;
            }
            $orderCheckHistory->call_center_id = $callCenter->id;
        }
        return $orderCheckHistory->save();
    }

    /**
     * @param int $orderFinancePretensionType
     * @return bool
     */
    public function makeFinancePretension($orderFinancePretensionType)
    {
        if (!OrderFinancePretension::find()->where(
            [
                'order_id' => $this->id,
                'type' => $orderFinancePretensionType,
            ]
        )->exists()) {
            $pretension = new OrderFinancePretension();
            $pretension->order_id = $this->id;
            $pretension->type = $orderFinancePretensionType;
            $pretension->status = OrderFinancePretension::PRETENSION_STATUS_IN_PROGRESS;
            return $pretension->save();
        }
        return true;
    }
}
