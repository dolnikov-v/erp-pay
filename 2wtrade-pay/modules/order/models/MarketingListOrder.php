<?php

namespace app\modules\order\models;

use app\components\db\ActiveRecordLogUpdateTime;
use Yii;

/**
 * This is the model class for table "marketing_list_order".
 *
 * @property integer $marketing_list_id
 * @property integer $old_order_id
 * @property integer $new_order_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property MarketingList $marketingList
 * @property Order $order
 */
class MarketingListOrder extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%marketing_list_order}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['marketing_list_id', 'old_order_id'], 'required'],
            [['marketing_list_id', 'old_order_id', 'new_order_id', 'created_at', 'updated_at'], 'integer'],
            [['marketing_list_id'], 'exist', 'skipOnError' => true, 'targetClass' => MarketingList::className(), 'targetAttribute' => ['marketing_list_id' => 'id']],
            [['old_order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['old_order_id' => 'id']],
            [['new_order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['new_order_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'marketing_list_id' => 'Маркетинговый лист',
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата обновления'),
            'old_order_id' => Yii::t('common', 'Старый заказ'),
            'new_order_id' => Yii::t('common', 'Новый заказ'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMarketingList()
    {
        return $this->hasOne(MarketingList::className(), ['id' => 'marketing_list_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'old_order_id']);
    }
}
