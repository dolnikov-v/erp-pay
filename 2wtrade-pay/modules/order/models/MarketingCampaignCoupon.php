<?php

namespace app\modules\order\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\modules\order\models\query\MarketingCampaignCouponQuery;
use Yii;

/**
 * This is the model class for table "marketing_campaign_coupon".
 *
 * @property integer $marketing_campaign_id
 * @property string $coupon
 * @property integer $printed_at
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property MarketingList $marketingList
 * @property Order $order
 */
class MarketingCampaignCoupon extends ActiveRecordLogUpdateTime
{
    const COUPON_LENGTH = 10;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%marketing_campaign_coupon}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['marketing_campaign_id', 'coupon'], 'required'],
            [['marketing_campaign_id', 'created_at', 'updated_at', 'printed_at'], 'integer'],
            ['coupon', 'unique'],
            ['coupon', 'string', 'max' => 10],
            [['marketing_campaign_id'], 'exist', 'skipOnError' => true, 'targetClass' => MarketingCampaign::className(), 'targetAttribute' => ['marketing_campaign_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'marketing_campaign_id' => 'Маркетинговая акция',
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата обновления'),
            'printed_at' => Yii::t('common', 'Дата печати'),
            'coupon' => Yii::t('common', 'Купон'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMarketingCampaign()
    {
        return $this->hasOne(MarketingCampaign::className(), ['id' => 'marketing_campaign_id']);
    }

    /**
     * @param string $attribute
     * @return string
     */
    public function generateUniqueRandomString($attribute)
    {
        $randomString = substr(preg_replace('/[^\dA-Z]/i', '', strtoupper(Yii::$app->getSecurity()->generateRandomString())), 0, self::COUPON_LENGTH);
        if (!$this->findOne([$attribute => $randomString])) {
            return $randomString;
        } else {
            return $this->generateUniqueRandomString($attribute);
        }
    }

    /**
     * @return MarketingCampaignCouponQuery
     */
    public static function find()
    {
        return new MarketingCampaignCouponQuery(get_called_class());
    }
}
