<?php

namespace app\modules\order\models;


use app\components\db\ActiveRecordLogUpdateTime;
use app\models\Currency;
use app\models\CurrencyRate;
use app\modules\order\jobs\OrderCalculateFinanceBalanceJob;
use app\modules\report\models\Invoice;
use app\modules\report\models\InvoiceOrder;
use Yii;

/**
 * Class OrderFinancePrediction
 *
 * @property integer $id Номер заказа
 * @property integer $order_id
 * @property double $price_cod COD
 * @property double $price_storage Стоимость хранения
 * @property double $price_fulfilment Стоимость обслуживания заказа
 * @property double $price_packing Стоимость упаковывания
 * @property double $price_package Стоимость упаковки
 * @property double $price_address_correction Стоимость корректировки адреса
 * @property double $price_delivery Стоимость доставки
 * @property double $price_redelivery Стоимость передоставки
 * @property double $price_delivery_back Стоимость возвращения
 * @property double $price_delivery_return Стоимость возврата
 * @property double $price_cod_service Плата за наложенный платеж
 * @property double $price_vat НДС
 * @property integer $price_cod_currency_id COD, валюта
 * @property integer $price_storage_currency_id Стоимость хранения, валюта
 * @property integer $price_fulfilment_currency_id Стоимость обслуживания заказа, валюта
 * @property integer $price_packing_currency_id Стоимость упаковывания, валюта
 * @property integer $price_package_currency_id Стоимость упаковки, валюта
 * @property integer $price_address_correction_currency_id Стоимость корректировки адреса, валюта
 * @property integer $price_delivery_currency_id Стоимость доставки, валюта
 * @property integer $price_redelivery_currency_id Стоимость передоставки, валюта
 * @property integer $price_delivery_back_currency_id Стоимость возвращения, валюта
 * @property integer $price_delivery_return_currency_id Стоимость возврата, валюта
 * @property integer $price_cod_service_currency_id Плата за наложенный платеж, валюта
 * @property integer $price_vat_currency_id НДС, валюта
 * @property string $additional_prices Дополнительные расходы
 * @property integer $created_at
 * @property integer $updated_at
 * @property double $vat_included
 *
 * @property Order $order
 * @property \app\modules\report\models\Invoice $invoice
 *
 * @package app\modules\order\models
 */
class OrderFinancePrediction extends OrderFinanceAbstract
{
    const COLUMN_VAT_INCLUDED = 'vat_included';


    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%order_finance_prediction}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id'], 'required'],
            [
                [
                    'order_id',
                    'created_at',
                    'updated_at',
                    'price_cod_currency_id',
                    'price_storage_currency_id',
                    'price_fulfilment_currency_id',
                    'price_packing_currency_id',
                    'price_package_currency_id',
                    'price_address_correction_currency_id',
                    'price_delivery_currency_id',
                    'price_redelivery_currency_id',
                    'price_delivery_back_currency_id',
                    'price_delivery_return_currency_id',
                    'price_cod_service_currency_id',
                    'price_vat_currency_id',
                ],
                'integer'
            ],
            [
                [
                    'price_cod',
                    'price_storage',
                    'price_packing',
                    'price_package',
                    'price_address_correction',
                    'price_delivery',
                    'price_redelivery',
                    'price_delivery_back',
                    'price_delivery_return',
                    'price_cod_service',
                    'price_vat',
                    'price_fulfilment',
                    'vat_included',
                ],
                'number'
            ],
            [
                ['order_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Order::className(),
                'targetAttribute' => ['order_id' => 'id']
            ],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'order_id' => Yii::t('common', 'Номер заказа'),
            'price_cod' => Yii::t('common', 'COD'),
            'price_storage' => Yii::t('common', 'Стоимость хранения'),
            'price_fulfilment' => Yii::t('common', 'Стоимость обслуживания заказа'),
            'price_packing' => Yii::t('common', 'Стоимость упаковывания'),
            'price_package' => Yii::t('common', 'Стоимость упаковки'),
            'price_delivery' => Yii::t('common', 'Стоимость доставки'),
            'price_redelivery' => Yii::t('common', 'Стоимость передоставки'),
            'price_delivery_back' => Yii::t('common', 'Стоимость возвращения'),
            'price_delivery_return' => Yii::t('common', 'Стоимость возврата'),
            'price_address_correction' => Yii::t('common', 'Стоимость корректировки адреса'),
            'price_cod_service' => Yii::t('common', 'Плата за наложенный платеж'),
            'price_vat' => Yii::t('common', 'НДС'),
            'price_cod_currency_id' => Yii::t('common', 'COD, валюта'),
            'price_storage_currency_id' => Yii::t('common', 'Стоимость хранения, валюта'),
            'price_fulfilment_currency_id' => Yii::t('common', 'Стоимость обслуживания заказа, валюта'),
            'price_packing_currency_id' => Yii::t('common', 'Стоимость упаковывания, валюта'),
            'price_package_currency_id' => Yii::t('common', 'Стоимость упаковки, валюта'),
            'price_delivery_currency_id' => Yii::t('common', 'Стоимость доставки, валюта'),
            'price_redelivery_currency_id' => Yii::t('common', 'Стоимость передоставки, валюта'),
            'price_delivery_back_currency_id' => Yii::t('common', 'Стоимость возвращения, валюта'),
            'price_delivery_return_currency_id' => Yii::t('common', 'Стоимость возврата, валюта'),
            'price_address_correction_currency_id' => Yii::t('common', 'Стоимость корректировки адреса, валюта'),
            'price_cod_service_currency_id' => Yii::t('common', 'Плата за наложенный платеж, валюта'),
            'price_vat_currency_id' => Yii::t('common', 'НДС, валюта'),
            'additional_prices' => Yii::t('common', 'Дополнительные расходы'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
            'vat_included' => Yii::t('common', 'НДС включен в стоимость'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceOrder()
    {
        return $this->hasOne(InvoiceOrder::className(), ['order_id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(Invoice::className(), ['id' => 'invoice_id'])->via('invoiceOrder');
    }

    /**
     * Список полей где хранятся расходы
     *
     * @return array
     */
    public static function getExpensesFields()
    {
        return [
            self::COLUMN_PRICE_STORAGE => 'price_storage_currency_id',
            self::COLUMN_PRICE_FULFILMENT => 'price_fulfilment_currency_id',
            self::COLUMN_PRICE_PACKING => 'price_packing_currency_id',
            self::COLUMN_PRICE_PACKAGE => 'price_package_currency_id',
            self::COLUMN_PRICE_ADDRESS_CORRECTION => 'price_address_correction_currency_id',
            self::COLUMN_PRICE_DELIVERY => 'price_delivery_currency_id',
            self::COLUMN_PRICE_REDELIVERY => 'price_redelivery_currency_id',
            self::COLUMN_PRICE_DELIVERY_BACK => 'price_delivery_back_currency_id',
            self::COLUMN_PRICE_DELIVERY_RETURN => 'price_delivery_return_currency_id',
            self::COLUMN_PRICE_COD_SERVICE => 'price_cod_service_currency_id',
            self::COLUMN_PRICE_VAT => 'price_vat_currency_id',
        ];
    }

    /**
     * @param boolean $insert
     * @return boolean
     */
    public function beforeSave($insert)
    {
        $groupId = uniqid();

        $dirtyAttributes = $this->getDirtyAttributes();

        foreach ($dirtyAttributes as $key => $item) {
            if ($item != $this->getOldAttribute($key) && $key != 'order_id') {
                $old = (string)$this->getOldAttribute($key);

                $log = new OrderLog();
                $log->order_id = $this->order_id;
                $log->group_id = $groupId;
                $log->field = 'order_finance_prediction.' . $key;
                $log->old = mb_substr($old, 0, 250);
                $log->new = mb_substr($item, 0, 250);
                $log->user_id = isset(Yii::$app->user) ? Yii::$app->user->id : null;
                $log->route = isset(Yii::$app->controller) ? Yii::$app->controller->getRoute() : null;
                $log->save();
            }
        }
        return parent::beforeSave($insert);
    }
}