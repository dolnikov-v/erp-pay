<?php
namespace app\modules\order\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\Country;
use app\models\User;
use app\modules\delivery\models\Delivery;
use app\modules\order\components\exporter\Exporter;
use app\modules\order\models\query\OrderClarificationQuery;
use Yii;

/**
 * Class OrderClarification
 * @property integer $id
 * @property integer $country_id
 * @property integer $user_id
 * @property string $order_ids
 * @property integer $delivery_id
 * @property string $delivery_emails
 * @property string $subject
 * @property string $message
 * @property string $type
 * @property string $columns
 * @property integer $lines
 * @property string $date_format
 * @property string $language
 * @property string $filename
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $generated_at
 * @property integer $sent_at
 * @property Country $country
 * @property User $user
 */
class OrderClarification extends ActiveRecordLogUpdateTime
{

    public $after_action = OrderExport::ACTION_SEND_CLARIFICATION;

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%order_clarification}}';
    }

    /**
     * @return OrderClarificationQuery
     */
    public static function find()
    {
        return new OrderClarificationQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['country_id', 'user_id'], 'required'],
            [['country_id', 'user_id', 'delivery_id', 'generated_at', 'sent_at', 'created_at', 'updated_at', 'lines'], 'integer'],
            ['type', 'in', 'range' => array_keys(self::getTypes())],
            [['order_ids', 'message', 'columns'], 'string'],
            [['delivery_emails', 'subject', 'filename', 'date_format', 'language'], 'string', 'max' => 255],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['delivery_id'], 'exist', 'skipOnError' => true, 'targetClass' => Delivery::className(), 'targetAttribute' => ['delivery_id' => 'id']],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'country_id' => Yii::t('common', 'Страна'),
            'user_id' => Yii::t('common', 'Польователь'),
            'order_ids' => Yii::t('common', 'Номера заказов'),
            'delivery_id' => Yii::t('common', 'Служба доставки'),
            'delivery_emails' => Yii::t('common', 'Список email получателей'),
            'subject' => Yii::t('common', 'Тема'),
            'message' => Yii::t('common', 'Сообщение'),
            'type' => Yii::t('common', 'Тип экспорта'),
            'date_format' => Yii::t('common', 'Формат времени'),
            'language' => Yii::t('common', 'Язык'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
            'generated_at' => Yii::t('common', 'Дата генерации'),
            'sent_at' => Yii::t('common', 'Дата отправки'),
            'lines' => Yii::t('common', 'Число записей'),
            'columns' => Yii::t('common', 'Выбранные столбцы'),
            'generated_time' => Yii::t('common', 'Время генерации, мин.')
        ];
    }

    /**
     * @return array
     */
    public static function getTypes()
    {
        return [
            Exporter::TYPE_CSV,
            Exporter::TYPE_EXCEL
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDelivery()
    {
        return $this->hasOne(Delivery::className(), ['id' => 'delivery_id']);
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ($insert) {
            Yii::$app->queueExport->push(new \app\jobs\SendOrderClarification(['id' => $this->id]));
        }
        parent::afterSave($insert, $changedAttributes);
    }
}
