<?php

namespace app\modules\order\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\Product;
use Yii;

/**
 * Class LeadProduct
 * @property integer $id
 * @property integer $order_id
 * @property integer $product_id
 * @property double $price
 * @property integer $quantity
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Order $order
 * @property Product $product
 */
class LeadProduct extends ActiveRecordLogUpdateTime
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%lead_product}}';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['order_id', 'product_id', 'quantity'], 'required'],
            [['order_id', 'product_id', 'quantity'], 'integer'],
            [
                'product_id',
                'exist',
                'targetClass' => '\app\models\Product',
                'targetAttribute' => 'id',
                'message' => Yii::t('common', 'Продукт не найден'),
            ],
            ['price', 'double', 'min' => 0],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @param boolean $insert
     * @return boolean
     */
    public function beforeSave($insert)
    {
        if (empty($this->price)) {
            $this->price = 0;
        }

        return parent::beforeSave($insert);
    }

}