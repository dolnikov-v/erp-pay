<?php

namespace app\modules\order\models;

use app\components\db\ActiveRecordLogCreateTime;
use app\modules\order\models\query\OrderLogStatusQuery;
use Yii;

/**
 * Class OrderLogStatus
 *
 * @property integer $id
 * @property integer $order_id
 * @property string $group_id
 * @property integer $old
 * @property integer $new
 * @property string $comment
 * @property integer $created_at
 *
 * @property OrderStatus $newStatus
 * @property OrderStatus $oldStatus
 * @property Order $order
 */
class OrderLogStatus extends ActiveRecordLogCreateTime
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%order_log_status}}';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['order_id', 'group_id', 'old', 'new'], 'required'],
            [['order_id', 'old', 'new', 'created_at'], 'integer'],
            [['comment'], 'string', 'max' => 500],
            [['new'], 'exist', 'skipOnError' => true, 'targetClass' => OrderStatus::className(), 'targetAttribute' => ['new' => 'id']],
            [['old'], 'exist', 'skipOnError' => true, 'targetClass' => OrderStatus::className(), 'targetAttribute' => ['old' => 'id']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'order_id' => Yii::t('common', 'Заказ'),
            'group_id' => Yii::t('common', 'Группа'),
            'old' => Yii::t('common', 'Старое значение'),
            'new' => Yii::t('common', 'Новое значение'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'comment' => Yii::t('common', 'Комментарий'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNewStatus()
    {
        return $this->hasOne(OrderStatus::className(), ['id' => 'new']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOldStatus()
    {
        return $this->hasOne(OrderStatus::className(), ['id' => 'old']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return OrderLogStatusQuery
     */
    public static function find()
    {
        return new OrderLogStatusQuery(get_called_class());
    }
}
