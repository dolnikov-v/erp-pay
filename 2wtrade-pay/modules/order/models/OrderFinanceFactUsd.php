<?php

namespace app\modules\order\models;

use app\modules\deliveryreport\models\DeliveryReport;
use app\modules\deliveryreport\models\DeliveryReportRecord;
use app\modules\deliveryreport\models\PaymentOrder;
use Yii;

/**
 * This is the model class for view "order_finance_fact_usd".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $payment_id
 * @property integer $pretension
 * @property double $price_cod
 * @property double $price_storage
 * @property double $price_fulfilment
 * @property double $price_packing
 * @property double $price_package
 * @property double $price_delivery
 * @property double $price_redelivery
 * @property double $price_delivery_back
 * @property double $price_delivery_return
 * @property double $price_address_correction
 * @property double $price_cod_service
 * @property double $price_vat
 * @property string $additional_prices
 * @property integer $delivery_report_id
 * @property integer $delivery_report_record_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property DeliveryReport $deliveryReport
 * @property DeliveryReportRecord $deliveryReportRecord
 * @property Order $order
 * @property PaymentOrder $payment
 */
class OrderFinanceFactUsd extends OrderFinanceAbstract
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_finance_fact_usd}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'order_id',
                    'payment_id',
                    'pretension',
                    'delivery_report_id',
                    'delivery_report_record_id',
                    'created_at',
                    'updated_at',
                ],
                'integer'
            ],
            [
                [
                    'price_cod',
                    'price_storage',
                    'price_fulfilment',
                    'price_packing',
                    'price_package',
                    'price_delivery',
                    'price_redelivery',
                    'price_delivery_back',
                    'price_delivery_return',
                    'price_address_correction',
                    'price_cod_service',
                    'price_vat',
                ],
                'number'
            ],
            [['additional_prices'], 'string'],
            [
                ['order_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Order::className(),
                'targetAttribute' => ['order_id' => 'id']
            ],
            [
                ['payment_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => PaymentOrder::className(),
                'targetAttribute' => ['payment_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'order_id' => Yii::t('common', 'Номер заказа'),
            'payment_id' => Yii::t('common', 'Платежка'),
            'pretension' => Yii::t('common', 'Претензия'),
            'price_cod' => Yii::t('common', 'COD'),
            'price_storage' => Yii::t('common', 'Стоимость хранения'),
            'price_fulfilment' => Yii::t('common', 'Стоимость обслуживания заказа'),
            'price_packing' => Yii::t('common', 'Стоимость упаковывания'),
            'price_package' => Yii::t('common', 'Стоимость упаковки'),
            'price_address_correction' => Yii::t('common', 'Стоимость корректировки адреса'),
            'price_delivery' => Yii::t('common', 'Стоимость доставки'),
            'price_redelivery' => Yii::t('common', 'Стоимость передоставки'),
            'price_delivery_back' => Yii::t('common', 'Стоимость возвращения'),
            'price_delivery_return' => Yii::t('common', 'Стоимость возврата'),
            'price_cod_service' => Yii::t('common', 'Плата за наложенный платеж'),
            'price_vat' => Yii::t('common', 'НДС'),
            'additional_prices' => Yii::t('common', 'Дополнительные расходы'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
            'delivery_report_id' => Yii::t('common', 'Номер отчета'),
            'delivery_report_record_id' => Yii::t('common', 'Номер строки отчета'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayment()
    {
        return $this->hasOne(PaymentOrder::className(), ['id' => 'payment_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveryReport()
    {
        return $this->hasOne(DeliveryReport::className(), ['id' => 'delivery_report_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveryReportRecord()
    {
        return $this->hasOne(DeliveryReportRecord::className(), ['id' => 'delivery_report_record_id']);
    }
}
