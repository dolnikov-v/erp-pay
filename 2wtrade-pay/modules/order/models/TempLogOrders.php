<?php
namespace app\modules\order\models;

use app\modules\order\models\query\TempLogOrdersQuery;

/**
 * This is the model class for table "temp_log_orders".
 *
 * @property integer $order_id
 * @property integer $begin
 * @property integer $end
 * @property string $block_file
 * @property integer $block_result
 * @property string $string_params_cron
 *
 * @property Order $order
 */
class TempLogOrders extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%temp_log_orders}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'begin', 'end'], 'required'],
            [['order_id', 'begin', 'end', 'block_result'], 'integer'],
            [['block_file', 'string_params_cron'], 'string', 'max' => 300],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'order_id' => 'Order ID',
            'begin' => 'Begin',
            'end' => 'End',
            'block_file' => 'Block File',
            'block_result' => 'Block Result',
            'string_params_cron' => 'String Params Cron',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @inheritdoc
     * @return TempLogOrdersQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TempLogOrdersQuery(get_called_class());
    }
}