<?php

namespace app\modules\order\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\helpers\Utils;
use app\jobs\CreateBarcodes;
use app\models\Country;
use app\models\User;
use app\modules\delivery\models\Delivery;
use app\modules\order\components\InvoiceBuilderFactory;
use app\modules\order\models\query\OrderLogisticListQuery;
use app\modules\salary\models\Person;
use kartik\mpdf\Pdf;
use Yii;
use yii\helpers\ArrayHelper;
use yii\validators\FileValidator;
use yii\web\ForbiddenHttpException;
use yii\web\UploadedFile;

/**
 * Class OrderLogisticList
 * @package app\modules\order\models
 * @property integer $id
 * @property integer $country_id
 * @property integer $delivery_id
 * @property integer $number
 * @property string $ids
 * @property string $orders_hash
 * @property string $ticket
 * @property string $label
 * @property integer $user_id
 * @property string $status
 * @property integer $type
 * @property string $barcodes
 * @property integer $created_at
 * @property integer $updated_at
 * @property Country $country
 * @property Delivery $delivery
 * @property User $user
 * @property Order[] $orders
 * @property OrderLogisticListExcel $actualListExcel
 * @property OrderLogisticListInvoiceArchive[] $invoiceArchives
 * @property OrderLogisticListLog[] $orderLogisticListLogs
 * @property OrderLogisticListRequest[] $orderLogisticListRequests
 * @property OrderLogisticListBarcode[] $orderLogisticListBarcodes
 * @property int $lastDateSent
 */
class OrderLogisticList extends ActiveRecordLogUpdateTime
{
    protected $insertLog = true;
    protected $updateLog = true;

    const DOWNLOAD_TYPE_TICKETS = 'tickets';
    const DOWNLOAD_TYPE_LABELS = 'labels';
    const DOWNLOAD_TYPE_EXCELS = 'excels';
    const DOWNLOAD_TYPE_INVOICES = 'invoices';

    const STATUS_BARCODING = 'barcoding';   // генерация штрих-кодов
    const STATUS_PENDING = 'pending';       // ожидает отправки
    const STATUS_DOWNLOADED = 'downloaded'; // скачан, только из ожидает отправки
    const STATUS_SENT = 'sent';             // отправлен
    const STATUS_RECEIVED = 'received';     // принят

    const TYPE_NORMAL = 0;
    const TYPE_CORRECTION = 1;
    const TYPE_PRETENSION_STILL_WAITING = 2;
    const TYPE_PRETENSION_INCORRECT_STATUSES = 3;
    const TYPE_PRETENSION_CHANGED_STATUSES = 4;

    /**
     * @var UploadedFile
     */
    public $ticketFile;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_logistic_list}}';
    }

    /**
     * @return OrderLogisticListQuery
     */
    public static function find()
    {
        return new OrderLogisticListQuery(get_called_class());
    }

    /**
     * @param string $fileName
     * @param string $type
     * @param boolean $prepareDir
     * @return string
     * @throws ForbiddenHttpException
     */
    public static function getDownloadPath($fileName, $type, $prepareDir = false)
    {
        $subDir = substr($fileName, 0, 2);

        $dir = Yii::getAlias('@downloads') . DIRECTORY_SEPARATOR . $type . DIRECTORY_SEPARATOR . $subDir;

        if ($prepareDir) {
            Utils::prepareDir($dir, 0777);
        }
        return $dir;
    }

    /**
     * @return null|string
     */
    public function getLabelFilePath()
    {
        if ($this->label) {
            return static::getDownloadPath($this->label, static::DOWNLOAD_TYPE_LABELS, false) . DIRECTORY_SEPARATOR . $this->label;
        }
        return null;
    }

    /**
     * @param string $fileName
     * @param boolean $prepareDir
     * @return string
     * @throws ForbiddenHttpException
     */
    public static function getUploadPath($fileName, $prepareDir = false)
    {
        $subDir = substr($fileName, 0, 2);

        $dir = Yii::getAlias('@uploads') . DIRECTORY_SEPARATOR . $subDir;

        if ($prepareDir) {
            Utils::prepareDir($dir);
        }

        return $dir;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'delivery_id', 'number', 'user_id'], 'required'],
            [['country_id', 'number', 'user_id', 'type'], 'integer'],
            [['ids', 'status', 'barcodes'], 'string'],
            [['ticket', 'label'], 'string', 'max' => 100],
            ['ticketFile', 'validateUpload'],
            ['status', 'default', 'value' => self::STATUS_PENDING],
            ['status', 'in', 'range' => array_keys(self::getStatusesCollection())],
            ['type', 'in', 'range' => array_keys(self::getTypesCollection())],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'country_id' => Yii::t('common', 'Страна'),
            'delivery_id' => Yii::t('common', 'Служба доставки'),
            'number' => Yii::t('common', 'Номер'),
            'ids' => Yii::t('common', 'Заказы'),
            'ticket' => Yii::t('common', 'Изображение'),
            'label' => Yii::t('common', 'Этикетка'),
            'user_id' => Yii::t('common', 'Пользователь'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
            'actualListExcel' => Yii::t('common', 'Лист'),
            'countOrders' => Yii::t('common', 'Количество заказов'),
            'status' => Yii::t('common', 'Статус'),
            'type' => Yii::t('common', 'Тип'),
            'barcodes' => Yii::t('common', 'Файл штрих-кодов'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        $this->orders_hash = $this->getOrdersHash();
        if ($insert && $this->delivery->auto_generating_barcode) {
            $this->status = self::STATUS_BARCODING;
        };
        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ($insert && $this->status == self::STATUS_BARCODING) {
            Yii::$app->queue->push(new CreateBarcodes(['orderLogisticListId' => $this->id]));
        };
        return parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDelivery()
    {
        return $this->hasOne(Delivery::className(), ['id' => 'delivery_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActualListExcel()
    {
        return $this->hasOne(
            OrderLogisticListExcel::className(),
            [
                'list_id' => 'id',
                'orders_hash' => 'orders_hash',
            ]
        )->orderBy([OrderLogisticListExcel::tableName() . '.created_at' => SORT_DESC]);
    }

    /**
     * @return false|null|string
     */
    public function getLastDateSent()
    {
        return $this->hasOne(OrderLogisticListExcel::className(), ['list_id' => 'id'])
            ->select('MAX(' . OrderLogisticListExcel::tableName() . '.sent_at)')
            ->scalar();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceArchives()
    {
        return $this->hasMany(OrderLogisticListInvoiceArchive::className(), ['list_id' => 'id']);
    }

    /**
     * @return Order[]
     */
    public function getOrders()
    {
        $orders = [];
        $ids = explode(',', $this->ids);

        if ($ids) {
            $orders = Order::find()
                ->joinWith(
                    [
                        'orderProducts',
                    ]
                )
                ->where(['in', Order::tableName() . '.id', $ids])
                ->byCountryId($this->country_id)
                ->orderBy(
                    [
                        Order::tableName() . '.id' => SORT_DESC,
                    ]
                )
                ->all();
        }

        return $orders;
    }

    /**
     * @return string
     */
    public function getOrdersHash()
    {
        $hash = null;

        if ($this->ids && $ids = explode(',', $this->ids)) {
            asort($ids);
            $hash = md5(implode(',', $ids));
        }

        return $hash;
    }

    /**
     * @return integer
     */
    public function getCountOrders()
    {
        $count = 0;

        if ($this->ids && $ids = explode(',', $this->ids)) {
            $count = count($ids);
        }

        return $count;
    }

    /**
     * @param string $attribute
     * @param array $params
     */
    public function validateUpload($attribute, $params)
    {
        if ($this->ticketFile) {
            $fileName = Utils::uid() . '.' . $this->ticketFile->extension;
            $filePath = self::getDownloadPath(
                    $fileName,
                    self::DOWNLOAD_TYPE_TICKETS,
                    true
                ) . DIRECTORY_SEPARATOR . $fileName;

            if (rename($this->ticketFile->tempName, $filePath)) {
                $this->ticket = $fileName;
            } else {
                $this->addError($attribute, Yii::t('common', 'Не удалось переместить файл.'));
            }
        }
    }

    /**
     * @return boolean
     */
    public function upload()
    {
        if (is_null($this->ticketFile)) {
            return true;
        }

        $validator = new FileValidator();
        $validator->attributes = ['ticketFile'];
        $validator->skipOnEmpty = true;
        $validator->extensions = ['png', 'jpg', 'jpeg'];

        $validator->validateAttribute($this, 'ticketFile');

        if (!$this->hasErrors('ticketFile')) {
            $fileName = Utils::uid() . '.' . $this->ticketFile->extension;
            $filePath = self::getUploadPath($fileName, true) . DIRECTORY_SEPARATOR . $fileName;

            if ($this->ticketFile->saveAs($filePath)) {
                $this->ticketFile->tempName = $filePath;

                return true;
            } else {
                $this->addError('ticketFile', Yii::t('common', 'Не удалось переместить файл.'));

                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * @param OrderLogisticList $list
     * @return string
     */
    public static function deleteListWarningText($list)
    {
        if ($list->isPretensionType()) {
            return '';
        }

        $text = Yii::t('common', 'Всем заказам из этого листа будет выставлен статус {status}.', [
            'status' => Yii::t('common', 'Колл-центр (одобрен в КЦ)'),
        ]);

        if (!empty($list->actualListExcel->sent_at)) {
            $text = Yii::t('common', 'Внимание! Лист уже был отправлен.') . ' ' . $text;
        }

        return $text;
    }

    /**
     * @param integer $order
     * @return integer
     */
    public static function firstListOrder($order)
    {
        $list = self::find()->where('instr(concat(",", ids, ","), ",' . $order . ',")>0')->limit(1)->one();
        return (isset($list)) ? $list->id : null;
    }

    /**
     * @return array
     */
    public static function getStatusesCollection()
    {
        return [
            self::STATUS_BARCODING => Yii::t('common', 'Генерация штрих-кодов'),
            self::STATUS_PENDING => Yii::t('common', 'Ожидает отправки'),
            self::STATUS_DOWNLOADED => Yii::t('common', 'Скачан'),
            self::STATUS_SENT => Yii::t('common', 'Отправлен'),
            self::STATUS_RECEIVED => Yii::t('common', 'Принят'),
        ];
    }

    /**
     * @return array
     */
    public static function getTypesCollection()
    {
        return array_merge([
            self::TYPE_NORMAL => Yii::t('common', 'Первичная отправка'),
            self::TYPE_CORRECTION => Yii::t('common', 'Корректировка'),
        ], self::getPretensionTypesCollection());
    }

    /**
     * @return array
     */
    public static function getPretensionTypesCollection()
    {
        return [
            self::TYPE_PRETENSION_STILL_WAITING => Yii::t('common', 'Претензия: клиенты еще ждут'),
            self::TYPE_PRETENSION_INCORRECT_STATUSES => Yii::t('common', 'Претензия: неправильный статус'),
            self::TYPE_PRETENSION_CHANGED_STATUSES => Yii::t('common', 'Претензия: смена статуса с Выкуп на Возврат'),
        ];
    }

    /**
     * @return array
     */
    public static function getPretensionMapping()
    {
        return [
            self::TYPE_PRETENSION_STILL_WAITING => OrderFinancePretension::PRETENSION_TYPE_STILL_WAITING,
            self::TYPE_PRETENSION_INCORRECT_STATUSES => OrderFinancePretension::PRETENSION_TYPE_FALSE_STATUS,
            self::TYPE_PRETENSION_CHANGED_STATUSES => OrderFinancePretension::PRETENSION_TYPE_STATUS
        ];
    }

    /**
     * @return string
     * @return \yii\db\ActiveQuery
     */
    public function getOrderLogisticListRequests()
    {
        return $this->hasMany(OrderLogisticListRequest::className(), ['list_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderLogisticListBarcodes()
    {
        return $this->hasMany(OrderLogisticListBarcode::className(), ['list_id' => 'id']);
    }


    /**
     * После того, как все заявки упали в статус получен, лист падает в статус "получен", а заказы в листе в статус 38.
     * @return boolean
     */
    public function onReceivedSaveStatus()
    {
        $orderLogisticListRequests = OrderLogisticListRequest::find()->byList($this->id)->all();
        if ($orderLogisticListRequests) {
            foreach ($orderLogisticListRequests as $orderLogisticListRequest) {
                if ($orderLogisticListRequest->status != OrderLogisticListRequest::STATUS_RECEIVED) {
                    return false;
                }
            }
        }

        $transaction = Yii::$app->db->beginTransaction();
        try {
            if ($this->ids) {
                foreach ($this->orders as $order) {
                    if ($order->canChangeStatusTo(OrderStatus::STATUS_LOG_RECEIVED)) {
                        $order->status_id = OrderStatus::STATUS_LOG_RECEIVED;
                        $order->save(true, ['status_id']);
                    }
                }
            }
            if ($this->status == OrderLogisticList::STATUS_SENT) {
                $this->status = OrderLogisticList::STATUS_RECEIVED;
                $this->save(true, ['status']);
            }
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollback();
        }
        return false;
    }

    /**
     * Проверка все ли скачаны штрих коды
     * @return bool
     */
    public function checkAllBarcodesReady()
    {
        if ($this->status != self::STATUS_BARCODING) {
            return false;
        }

        if (OrderLogisticListBarcode::find()
            ->where(['list_id' => $this->id])
            ->having('COUNT(id) = COUNT(barcode_filename) and COUNT(id) > 0')
            ->exists()) {
            return $this->makeBarcodesGroupedPdf();
        }

        return false;
    }

    /**
     * После того как получен последний штрихкод генерит PDF
     * @return bool
     */
    public function makeBarcodesGroupedPdf()
    {
        $templateFile = $filePath = Yii::getAlias('@templates') . DIRECTORY_SEPARATOR . 'barcode.php';
        $content = Yii::$app->controller->renderFile($templateFile, [
            'barcodes' => $this->orderLogisticListBarcodes
        ]);

        $this->barcodes = $this->id . '.pdf';
        $this->status = self::STATUS_PENDING;

        $document = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            'options' => [
                'autoScriptToLang' => true,
                'autoLangToFont' => true,
                'ignore_invalid_utf8' => true,
            ],
            'content' => $content,
            'filename' => OrderLogisticListBarcode::getPathDir('grouped') . $this->barcodes,
            'destination' => Pdf::DEST_FILE,
        ]);
        $document->render();

        if ($this->save(true, ['barcodes', 'status'])) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Генерация инвойсов для листа
     *
     * @param string $template
     * @param string $format
     * @return string
     */
    public function buildInvoices($template = OrderLogisticListInvoice::TEMPLATE_TWO, $format = OrderLogisticListInvoice::FORMAT_DOCS)
    {
        $builder = InvoiceBuilderFactory::build($this, $template, $format, $this->delivery->country);
        return $builder->generate();
    }

    /**
     * Подтверждение отправки листа
     * @param bool $changeOrderStatus
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function confirmSend(bool $changeOrderStatus = true)
    {
        if ($this->status == static::STATUS_PENDING || $this->status == static::STATUS_DOWNLOADED) {
            $transaction = static::getDb()->beginTransaction();
            try {
                $this->status = OrderLogisticList::STATUS_SENT;
                if ($this->actualListExcel && !$this->actualListExcel->sent_at) {
                    $this->actualListExcel->sent_at = time();
                    $this->actualListExcel->waiting_for_send = 0;
                    if (!$this->actualListExcel->save(false, ['sent_at', 'waiting_for_send'])) {
                        throw new \Exception($this->actualListExcel->getFirstErrorAsString());
                    }
                }
                if (!$this->save(true, ['status'])) {
                    throw new \Exception($this->getFirstErrorAsString());
                }
                if ($changeOrderStatus) {
                    foreach ($this->getOrders() as $order) {
                        if ($order->canChangeStatusTo(OrderStatus::STATUS_LOG_SENT)) {
                            $order->status_id = OrderStatus::STATUS_LOG_SENT;
                            if (!$order->save(true, ['status_id'])) {
                                throw new \Exception($order->getFirstErrorAsString());
                            }
                            $order->deliveryRequest->sent_at = $this->actualListExcel->sent_at;
                            if (!$order->deliveryRequest->save(true, ['sent_at'])) {
                                throw new \Exception($order->deliveryRequest->getFirstErrorAsString());
                            }
                        }
                    }
                }
                $transaction->commit();
            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }
        }
    }

    /**
     * @return int
     */
    public function getTodayCountByDelivery()
    {
        $todayStart = Yii::$app->formatter->asTimestamp(date('Y-m-d 00:00:00'));
        $todayEnd = $todayStart + 86399;

        $todayCount = OrderLogisticList::find()
            ->where(['delivery_id' => $this->delivery_id])
            ->andWhere(['>=', 'created_at', $todayStart])
            ->andWhere(['<=', 'created_at', $todayEnd])
            ->count();

        return $todayCount;
    }

    /**
     * @param bool $withDeliveryContracts
     * @return string[]
     */
    public function getEmailList(bool $withDeliveryContracts = true)
    {
        if ($this->isPretensionType()) {

            $emails = [];

            if ($withDeliveryContracts) {
                foreach ($this->delivery->deliveryContacts as $contact) {
                    if ($contact->email) {
                        $emails[] = $contact->email;
                    }
                }
            }

            if ($this->country->curator_user_id && $this->country->curatorUser->email) {
                $emails[] = $this->country->curatorUser->email;
            }

            $lawyer = Person::getMoscowLawyer();
            if ($lawyer && $lawyer->corporate_email) {
                $emails[] = $lawyer->corporate_email;
            }

            return $emails;

        } else {
            return $this->delivery->emailList;
        }
    }

    /**
     * @return bool
     */
    public function isPretensionType()
    {
        return array_key_exists($this->type, self::getPretensionTypesCollection());
    }
}
