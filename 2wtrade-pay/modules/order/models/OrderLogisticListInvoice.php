<?php
namespace app\modules\order\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\helpers\Utils;
use app\models\logs\LinkData;
use app\models\User;
use app\modules\order\models\query\OrderLogisticListInvoiceQuery;
use Yii;
use yii\web\ForbiddenHttpException;

/**
 * Class OrderLogisticListInvoice
 * @package app\modules\order\models
 * @property integer $id
 * @property integer $list_id
 * @property integer $order_id
 * @property string $template
 * @property string $format
 * @property string $invoice
 * @property string $invoice_local
 * @property integer $user_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property OrderLogisticList $list
 * @property Order $order
 * @property User $user
 */
class OrderLogisticListInvoice extends ActiveRecordLogUpdateTime
{
    protected $insertLog = true;
    protected $updateLog = true;
    protected $deleteLog = true;

    /**
     * @return array|null
     */
    protected function getLinkData(): ?array
    {
        return [
            new LinkData(['field' => 'list_id', 'value' => (int)$this->list_id]),
        ];
    }

    const TEMPLATE_ONE = 'one_per_page';
    const TEMPLATE_TWO = 'two_per_page';
    const TEMPLATE_ALL = 'all_per_page';

    const FORMAT_DOCS = 'docx';
    const FORMAT_PDF = 'pdf';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_logistic_list_invoice}}';
    }

    /**
     * @return OrderLogisticListInvoiceQuery
     */
    public static function find()
    {
        return new OrderLogisticListInvoiceQuery(get_called_class());
    }

    /**
     * @return array
     */
    public static function getTemplatesCollection()
    {
        return [
            self::TEMPLATE_ONE => Yii::t('common', '1 накладная на странице'),
            self::TEMPLATE_TWO => Yii::t('common', '2 накладых на странице'),
            self::TEMPLATE_ALL => Yii::t('common', 'Все накладные в одном документе'),
        ];
    }

    /**
     * @return array
     */
    public static function getFormatsCollection()
    {
        return [
            self::FORMAT_DOCS => Yii::t('common', 'DOCX'),
            self::FORMAT_PDF => Yii::t('common', 'PDF'),
        ];
    }

    /**
     * @param string $fileName
     * @param boolean $prepareDir
     * @return string
     * @throws ForbiddenHttpException
     */
    public static function getDownloadPath($fileName, $prepareDir = false)
    {
        $subDir = substr($fileName, 0, 2);

        $dir = Yii::getAlias('@downloads') . DIRECTORY_SEPARATOR . 'invoices-parts' . DIRECTORY_SEPARATOR . $subDir;

        if ($prepareDir) {
            Utils::prepareDir($dir, 0777);
        }

        return $dir;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['list_id', 'order_id', 'template', 'format', 'invoice', 'invoice_local', 'user_id'], 'required'],
            ['template', 'in', 'range' => array_keys(self::getTemplatesCollection())],
            ['format', 'in', 'range' => array_keys(self::getFormatsCollection())],
            [['list_id', 'order_id', 'user_id'], 'integer'],
            [['template', 'format', 'invoice', 'invoice_local'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'list_id' => Yii::t('common', 'Лист'),
            'order_id' => Yii::t('common', 'Заказ'),
            'template' => Yii::t('common', 'Шаблон'),
            'format' => Yii::t('common', 'Формат файлов'),
            'invoice' => Yii::t('common', 'Накладная'),
            'invoice_local' => Yii::t('common', 'Имя файла'),
            'user_id' => Yii::t('common', 'Пользователь'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getList()
    {
        return $this->hasOne(OrderLogisticList::className(), ['id' => 'list_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return string
     */
    public function getExtension()
    {
        return $this->format == self::FORMAT_DOCS ? 'docx' : 'pdf';
    }
}
