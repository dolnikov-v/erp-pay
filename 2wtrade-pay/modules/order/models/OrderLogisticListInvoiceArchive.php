<?php
namespace app\modules\order\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\helpers\Utils;
use app\models\logs\LinkData;
use app\models\User;
use app\modules\order\models\query\OrderLogisticListInvoiceArchiveQuery;
use app\modules\order\models\query\OrderLogisticListInvoiceQuery;
use Yii;
use yii\web\ForbiddenHttpException;

/**
 * Class OrderLogisticListInvoice
 * @package app\modules\order\models
 * @property integer $id
 * @property integer $list_id
 * @property string $template
 * @property string $format
 * @property string $archive
 * @property string $archive_local
 * @property integer $user_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property OrderLogisticList $list
 * @property User $user
 */
class OrderLogisticListInvoiceArchive extends ActiveRecordLogUpdateTime
{
    protected $insertLog = true;
    protected $updateLog = true;
    protected $deleteLog = true;

    /**
     * @return array|null
     */
    protected function getLinkData(): ?array
    {
        return [
            new LinkData(['field' => 'list_id', 'value' => (int)$this->list_id]),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_logistic_list_invoice_archive}}';
    }

    /**
     * @return OrderLogisticListInvoiceArchiveQuery
     */
    public static function find()
    {
        return new OrderLogisticListInvoiceArchiveQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['list_id', 'template', 'format', 'archive', 'archive_local', 'user_id'], 'required'],
            ['template', 'in', 'range' => array_keys(OrderLogisticListInvoice::getTemplatesCollection())],
            ['format', 'in', 'range' => array_keys(OrderLogisticListInvoice::getFormatsCollection())],
            [['list_id', 'user_id'], 'integer'],
            [['template', 'format', 'archive', 'archive_local'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'list_id' => Yii::t('common', 'Лист'),
            'template' => Yii::t('common', 'Шаблон'),
            'format' => Yii::t('common', 'Формат файлов'),
            'archive' => Yii::t('common', 'Архив'),
            'invoice_local' => Yii::t('common', 'Имя файла'),
            'user_id' => Yii::t('common', 'Пользователь'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getList()
    {
        return $this->hasOne(OrderLogisticList::className(), ['id' => 'list_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
