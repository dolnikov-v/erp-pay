<?php

namespace app\modules\order\models;

use app\models\Country;
use app\modules\order\models\query\OrderStatusQuery;
use app\widgets\Label;
use Yii;
use yii\db\ActiveRecord;

/**
 * Class OrderStatus
 *
 * @property integer $id
 * @property integer $sort
 * @property string $name
 * @property string $comment
 * @property string $group
 * @property string $labelStyle
 * @property string $type
 */
class OrderStatus extends ActiveRecord
{
    const STATUS_SOURCE_LONG_FORM = 1; //Источник (заказ создан с длинной формы)
    const STATUS_SOURCE_SHORT_FORM = 2; //Источник (заказ создан с короткой формы)
    const STATUS_CC_POST_CALL = 3; //Колл-центр (отправлен в КЦ на обзвон)
    const STATUS_CC_RECALL = 4; //Колл-центр (перезвон КЦ)
    const STATUS_CC_FAIL_CALL = 5; //Колл-центр (недозвон КЦ)
    const STATUS_CC_APPROVED = 6; // Одобрен (старое название статуса Колл-центр (одобрен в КЦ))
    const STATUS_CC_REJECTED = 7; //Колл-центр (отклонен в КЦ)
    const STATUS_LOG_ACCEPTED = 8; //Логистика (принято, ожидается генерация листа)
    const STATUS_LOG_GENERATED = 9; //Логистика (лист сгенерирован, ожидаются этикетки)
    const STATUS_LOG_SET = 10; //Логистика (этикетки созданы)
    const STATUS_LOG_PASTED = 11; //Логистика (этикетки наклеены)
    const STATUS_DELIVERY_ACCEPTED = 12; //Курьерка (принято)
    const STATUS_DELIVERY_LEFT_WAREHOUSE = 13; //Курьерка (покинуло склад)
    const STATUS_DELIVERY = 14; //Курьерка (ожидает вручения)
    const STATUS_DELIVERY_BUYOUT = 15; //Курьерка (выкуп)
    const STATUS_DELIVERY_DENIAL = 16; //Курьерка (отказ)
    const STATUS_DELIVERY_RETURNED = 17; //Курьерка (вернулось отправителю)
    const STATUS_FINANCE_MONEY_RECEIVED = 18; //Финансы (деньги получены)
    const STATUS_DELIVERY_REJECTED = 19; //Курьерка (отклонено)
    const STATUS_DELIVERY_LEFT_OUR_WAREHOUSE = 20; //Курьерка (покинуло наш склад когда-то)
    const STATUS_DELIVERY_REDELIVERY = 21; //Курьерка (отправлено на передоставку)
    const STATUS_DELIVERY_LEFT_THEIR_WAREHOUSE = 22; //Курьерка (покинуло их склад)
    const STATUS_LOGISTICS_REJECTED = 23; //Логистика (отклонено)
    const STATUS_DELIVERY_DELAYED = 24; //Курьерка (задерживается)
    const STATUS_CC_TRASH = 25; //Колл-центр (треш)
    const STATUS_CC_DOUBLE = 26; //Колл-центр (дубль)
    const STATUS_DELIVERY_REFUND = 27; //Курьерка (отправлено на возврат)
    const STATUS_LOGISTICS_ACCEPTED = 28; //Логистика (возврат принят)
    const STATUS_DELIVERY_LOST = 29; //Курьерка (потерян)
    const STATUS_CC_INPUT_QUEUE = 30; //Колл-центр (заказ создан в КЦ из входящей очереди)
    const STATUS_DELIVERY_PENDING = 31; //Курьерка (отправлен на доставку)
    const STATUS_LOG_DEFERRED = 32; // Логистика (отложено)
    const STATUS_AUTOTRASH = 33; //Автотреш вместо отправки в КЦ
    const STATUS_RECLAMATION = 34; //Рекламация
    const STATUS_AUTOTRASH_DOUBLE = 35; //Автотреш по дублям вместо отправки в КЦ
    const STATUS_CLIENT_REFUSED = 36; //Отказ (клиент)
    const STATUS_LOG_SENT = 37; //Курьерка (лист отправлен)
    const STATUS_LOG_RECEIVED = 38; //Курьерка (лист принят)
    const STATUS_CC_CHECKING = 39; //Колл-центр (на проверке)

    const TYPE_STARTING = 'starting';
    const TYPE_MIDDLE = 'middle';
    const TYPE_FINAL = 'final';

    const GROUP_SOURCE = 'source';
    const GROUP_CALL_CENTER = 'call_center';
    const GROUP_LOGISTIC = 'logistic';
    const GROUP_DELIVERY = 'delivery';
    const GROUP_FINANCE = 'finance';

    protected static $statuses = null;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_status}}';
    }

    /**
     * @return OrderStatusQuery
     */
    public static function find()
    {
        return new OrderStatusQuery(get_called_class());
    }

    /**
     * @return array
     */
    public static function getGroupsCollection()
    {
        return [
            self::GROUP_SOURCE => Yii::t('common', 'Источник'),
            self::GROUP_CALL_CENTER => Yii::t('common', 'Колл-центр'),
            self::GROUP_LOGISTIC => Yii::t('common', 'Логистика'),
            self::GROUP_DELIVERY => Yii::t('common', 'Курьерка'),
            self::GROUP_FINANCE => Yii::t('common', 'Финансы'),
        ];
    }

    /**
     * @return array
     */
    public static function getTypesCollection()
    {
        return [
            self::TYPE_STARTING => Yii::t('common', 'Начальный статус'),
            self::TYPE_MIDDLE => Yii::t('common', 'Промежуточный статус'),
            self::TYPE_FINAL => Yii::t('common', 'Конечный статус'),
        ];
    }

    /**
     * @param integer|OrderStatus $status
     * @return boolean
     */
    public static function isFinalDeliveryStatus($status)
    {
        if ($status instanceof OrderStatus) {
            return $status->type == self::TYPE_FINAL;
        }
        return self::find()->where(['id' => $status, 'type' => self::TYPE_FINAL])->exists();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            ['comment', 'string'],
            ['sort', 'integer'],
            ['name', 'string', 'max' => 255],
            ['group', 'in', 'range' => array_keys(OrderStatus::getGroupsCollection())],
            ['type', 'in', 'range' => array_keys(OrderStatus::getTypesCollection())],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'sort' => Yii::t('common', 'Сортировка'),
            'name' => Yii::t('common', 'Название'),
            'comment' => Yii::t('common', 'Комментарий'),
            'group' => Yii::t('common', 'Группа'),
            'type' => Yii::t('common', 'Тип'),
        ];
    }

    /**
     * @return string
     */
    public function getLabelStyle()
    {
        $style = Label::STYLE_SUCCESS;

        if ($this->type == OrderStatus::TYPE_MIDDLE) {
            $style = Label::STYLE_WARNING;
        } elseif ($this->type == OrderStatus::TYPE_FINAL) {
            $style = Label::STYLE_DANGER;
        }

        return $style;
    }

    /**
     * @param $status
     * @param Country $country
     * @return array
     */
    public static function getPreviousStatusesForStatus($status, $country)
    {
        $statuses = [];

        $allowOrderStatuses = $country->getAllowOrderStatuses();

        foreach ($allowOrderStatuses as $allowStatus => $allowStatuses) {
            if (array_search($status, $allowStatuses) !== false) {
                if (array_search($allowStatus, $statuses) === false) {
                    $statuses[] = $allowStatus;
                }
            }
        }

        return $statuses;
    }

    /**
     * получить все финальные статусы
     * @return array
     */
    public static function getAllFinalStatuses()
    {
        return self::find()
            ->where(['type' => self::TYPE_FINAL])
            ->asArray()
            ->all();
    }


    /**
     * Список статусов для Апрува
     * @return array
     */
    public static function getApproveList()
    {
        return [
            OrderStatus::STATUS_CC_APPROVED,
            OrderStatus::STATUS_LOG_ACCEPTED,
            OrderStatus::STATUS_LOG_GENERATED,
            OrderStatus::STATUS_LOG_SET,
            OrderStatus::STATUS_LOG_PASTED,
            OrderStatus::STATUS_DELIVERY_ACCEPTED,
            OrderStatus::STATUS_DELIVERY_LEFT_WAREHOUSE,
            OrderStatus::STATUS_DELIVERY,
            OrderStatus::STATUS_DELIVERY_BUYOUT,
            OrderStatus::STATUS_DELIVERY_DENIAL,
            OrderStatus::STATUS_DELIVERY_RETURNED,
            OrderStatus::STATUS_FINANCE_MONEY_RECEIVED,
            OrderStatus::STATUS_DELIVERY_REJECTED,
            OrderStatus::STATUS_DELIVERY_LEFT_OUR_WAREHOUSE,
            OrderStatus::STATUS_DELIVERY_REDELIVERY,
            OrderStatus::STATUS_DELIVERY_LEFT_THEIR_WAREHOUSE,
            OrderStatus::STATUS_LOGISTICS_REJECTED,
            OrderStatus::STATUS_DELIVERY_DELAYED,
            OrderStatus::STATUS_DELIVERY_REFUND,
            OrderStatus::STATUS_LOGISTICS_ACCEPTED,
            OrderStatus::STATUS_DELIVERY_LOST,
            OrderStatus::STATUS_DELIVERY_PENDING,
            OrderStatus::STATUS_LOG_DEFERRED,
            OrderStatus::STATUS_RECLAMATION,
            OrderStatus::STATUS_CLIENT_REFUSED,
            OrderStatus::STATUS_LOG_SENT,
            OrderStatus::STATUS_LOG_RECEIVED,
        ];
    }

    /**
     * Список статусов для Не Апрува
     * @return array
     */
    public static function getNotApproveList()
    {
        return array_diff(self::getAllList(), self::getApproveList());
    }

    /**
     * Список статусов для Апрува в отчете Колл Центр
     * @return array
     */
    public static function getApproveCCList()
    {
        $return = OrderStatus::getApproveList();
        $return[] = OrderStatus::STATUS_CC_CHECKING;
        return $return;
    }

    /**
     * Список статусов в Процессе
     * @return array
     */
    public static function getProcessList()
    {
        return [
            OrderStatus::STATUS_CC_APPROVED,
            OrderStatus::STATUS_LOG_ACCEPTED,
            OrderStatus::STATUS_LOG_GENERATED,
            OrderStatus::STATUS_LOG_SET,
            OrderStatus::STATUS_LOG_PASTED,
            OrderStatus::STATUS_DELIVERY_ACCEPTED,
            OrderStatus::STATUS_DELIVERY_LEFT_WAREHOUSE,
            OrderStatus::STATUS_DELIVERY,
            OrderStatus::STATUS_DELIVERY_LEFT_OUR_WAREHOUSE,
            OrderStatus::STATUS_DELIVERY_REDELIVERY,
            OrderStatus::STATUS_DELIVERY_LEFT_THEIR_WAREHOUSE,
            OrderStatus::STATUS_DELIVERY_DELAYED,
            OrderStatus::STATUS_DELIVERY_PENDING,
            OrderStatus::STATUS_LOG_DEFERRED,
            OrderStatus::STATUS_RECLAMATION,
            OrderStatus::STATUS_LOG_SENT,
            OrderStatus::STATUS_LOG_RECEIVED,
            OrderStatus::STATUS_CC_CHECKING,
        ];
    }

    /**
     * Список статусов в Процессе в КС
     * @return array
     */
    public static function getProcessDeliveryList()
    {
        return [
            OrderStatus::STATUS_CC_APPROVED,
            OrderStatus::STATUS_LOG_ACCEPTED,
            OrderStatus::STATUS_LOG_GENERATED,
            OrderStatus::STATUS_LOG_SET,
            OrderStatus::STATUS_LOG_PASTED,
            OrderStatus::STATUS_DELIVERY_ACCEPTED,
            OrderStatus::STATUS_DELIVERY_LEFT_WAREHOUSE,
            OrderStatus::STATUS_DELIVERY,
            OrderStatus::STATUS_DELIVERY_LEFT_OUR_WAREHOUSE,
            OrderStatus::STATUS_DELIVERY_REDELIVERY,
            OrderStatus::STATUS_DELIVERY_LEFT_THEIR_WAREHOUSE,
            OrderStatus::STATUS_DELIVERY_DELAYED,
            OrderStatus::STATUS_DELIVERY_PENDING,
            OrderStatus::STATUS_LOG_DEFERRED,
            OrderStatus::STATUS_RECLAMATION,
            OrderStatus::STATUS_LOG_SENT,
            OrderStatus::STATUS_LOG_RECEIVED,
        ];
    }

    /**
     * Список статусов в Процессе в КС и у логистов
     * @return array
     */
    public static function getProcessDeliveryAndLogisticList()
    {
        return [
            OrderStatus::STATUS_LOG_ACCEPTED,
            OrderStatus::STATUS_LOG_GENERATED,
            OrderStatus::STATUS_LOG_SET,
            OrderStatus::STATUS_LOG_PASTED,
            OrderStatus::STATUS_DELIVERY_ACCEPTED,
            OrderStatus::STATUS_DELIVERY_LEFT_WAREHOUSE,
            OrderStatus::STATUS_DELIVERY,
            OrderStatus::STATUS_DELIVERY_LEFT_OUR_WAREHOUSE,
            OrderStatus::STATUS_DELIVERY_REDELIVERY,
            OrderStatus::STATUS_DELIVERY_LEFT_THEIR_WAREHOUSE,
            OrderStatus::STATUS_DELIVERY_DELAYED,
            OrderStatus::STATUS_DELIVERY_PENDING,
            OrderStatus::STATUS_LOG_SENT,
            OrderStatus::STATUS_LOG_RECEIVED,
        ];
    }

    /**
     * Список статусов Выкуп
     * @return array
     */
    public static function getBuyoutList()
    {
        return [
            OrderStatus::STATUS_DELIVERY_BUYOUT,
            OrderStatus::STATUS_FINANCE_MONEY_RECEIVED,
        ];
    }

    /**
     * Список статусов Выкуп, по которым не получены деньги
     * @return array
     */
    public static function getOnlyBuyoutList()
    {
        return [
            OrderStatus::STATUS_DELIVERY_BUYOUT,
        ];
    }

    /**
     * Список статусов Выкуп, по которым получены деньги
     * @return array
     */
    public static function getOnlyMoneyReceivedList()
    {
        return [
            OrderStatus::STATUS_FINANCE_MONEY_RECEIVED,
        ];
    }

    /**
     * Список статусов НеВыкуп для финансов и пр.
     * @return array
     */
    public static function getNotBuyoutList()
    {
        return [
            OrderStatus::STATUS_DELIVERY_DENIAL,
            OrderStatus::STATUS_DELIVERY_RETURNED,
            OrderStatus::STATUS_DELIVERY_REJECTED,
            OrderStatus::STATUS_LOGISTICS_REJECTED,
            OrderStatus::STATUS_DELIVERY_REFUND,
            OrderStatus::STATUS_LOGISTICS_ACCEPTED,
            OrderStatus::STATUS_DELIVERY_LOST,
            OrderStatus::STATUS_CLIENT_REFUSED,
        ];
    }

    /**
     * Список статусов НеВыкуп для доставки
     * @return array
     */
    public static function getNotBuyoutDeliveryList()
    {
        return [
            OrderStatus::STATUS_DELIVERY_DENIAL,
            OrderStatus::STATUS_DELIVERY_RETURNED,
            OrderStatus::STATUS_DELIVERY_REJECTED,
            OrderStatus::STATUS_LOGISTICS_REJECTED,
            OrderStatus::STATUS_DELIVERY_REFUND,
            OrderStatus::STATUS_LOGISTICS_ACCEPTED,
            OrderStatus::STATUS_DELIVERY_LOST,
            OrderStatus::STATUS_DELIVERY_LOST,
            OrderStatus::STATUS_CLIENT_REFUSED,
            OrderStatus::STATUS_CC_CHECKING,
        ];
    }

    /**
     * Список статусов НеВыкуп
     * @return array
     */
    public static function getOnlyNotBuyoutInDeliveryList()
    {
        return [
            OrderStatus::STATUS_DELIVERY_DENIAL,
            OrderStatus::STATUS_DELIVERY_RETURNED,
            OrderStatus::STATUS_DELIVERY_REFUND,
            OrderStatus::STATUS_LOGISTICS_ACCEPTED,
            OrderStatus::STATUS_DELIVERY_LOST,
        ];
    }

    /**
     * @return array
     */
    public static function getNotBuyoutInDeliveryProcessList()
    {
        return [
            OrderStatus::STATUS_DELIVERY_DENIAL,
            OrderStatus::STATUS_DELIVERY_REFUND,
        ];
    }

    /**
     * Список статусов НеВыкуп
     * @return array
     */
    public static function getNotBuyoutInProcessList()
    {
        return [
            OrderStatus::STATUS_DELIVERY_DENIAL,
            OrderStatus::STATUS_DELIVERY_REFUND,
        ];
    }

    /**
     * Список статусов НеВыкуп
     * @return array
     */
    public static function getOnlyNotBuyoutDoneList()
    {
        return [
            OrderStatus::STATUS_DELIVERY_RETURNED,
            OrderStatus::STATUS_LOGISTICS_ACCEPTED,
            OrderStatus::STATUS_DELIVERY_LOST,
        ];
    }

    /**
     * Список статусов НеВыкуп
     * @return array
     */
    public static function getInDeliveryAndFinanceList()
    {
        return [
            OrderStatus::STATUS_DELIVERY_ACCEPTED,
            OrderStatus::STATUS_DELIVERY_LEFT_WAREHOUSE,
            OrderStatus::STATUS_DELIVERY,
            OrderStatus::STATUS_DELIVERY_DENIAL,
            OrderStatus::STATUS_DELIVERY_RETURNED,
            OrderStatus::STATUS_DELIVERY_REJECTED,
            OrderStatus::STATUS_DELIVERY_LEFT_OUR_WAREHOUSE,
            OrderStatus::STATUS_DELIVERY_REDELIVERY,
            OrderStatus::STATUS_DELIVERY_LEFT_THEIR_WAREHOUSE,
            OrderStatus::STATUS_LOGISTICS_REJECTED,
            OrderStatus::STATUS_DELIVERY_DELAYED,
            OrderStatus::STATUS_DELIVERY_REFUND,
            OrderStatus::STATUS_LOGISTICS_ACCEPTED,
            OrderStatus::STATUS_DELIVERY_LOST,
            OrderStatus::STATUS_DELIVERY_BUYOUT,
            OrderStatus::STATUS_FINANCE_MONEY_RECEIVED,
        ];
    }

    /**
     * @return array
     */
    public static function getInitialList()
    {
        return [
            OrderStatus::STATUS_SOURCE_LONG_FORM,
            OrderStatus::STATUS_SOURCE_SHORT_FORM,
            OrderStatus::STATUS_CC_POST_CALL,
            OrderStatus::STATUS_CC_INPUT_QUEUE,
        ];
    }

    /**
     * Список всех статусов
     * @return array
     */
    public static function getAllList()
    {
        return [
            OrderStatus::STATUS_SOURCE_LONG_FORM,
            OrderStatus::STATUS_SOURCE_SHORT_FORM,
            OrderStatus::STATUS_CC_POST_CALL,
            OrderStatus::STATUS_CC_RECALL,
            OrderStatus::STATUS_CC_FAIL_CALL,
            OrderStatus::STATUS_CC_APPROVED,
            OrderStatus::STATUS_CC_REJECTED,
            OrderStatus::STATUS_LOG_ACCEPTED,
            OrderStatus::STATUS_LOG_GENERATED,
            OrderStatus::STATUS_LOG_SET,
            OrderStatus::STATUS_LOG_PASTED,
            OrderStatus::STATUS_DELIVERY_ACCEPTED,
            OrderStatus::STATUS_DELIVERY_LEFT_WAREHOUSE,
            OrderStatus::STATUS_DELIVERY,
            OrderStatus::STATUS_DELIVERY_BUYOUT,
            OrderStatus::STATUS_DELIVERY_DENIAL,
            OrderStatus::STATUS_DELIVERY_RETURNED,
            OrderStatus::STATUS_FINANCE_MONEY_RECEIVED,
            OrderStatus::STATUS_DELIVERY_REJECTED,
            OrderStatus::STATUS_DELIVERY_LEFT_OUR_WAREHOUSE,
            OrderStatus::STATUS_DELIVERY_REDELIVERY,
            OrderStatus::STATUS_DELIVERY_LEFT_THEIR_WAREHOUSE,
            OrderStatus::STATUS_LOGISTICS_REJECTED,
            OrderStatus::STATUS_DELIVERY_DELAYED,
            OrderStatus::STATUS_CC_TRASH,
            OrderStatus::STATUS_CC_DOUBLE,
            OrderStatus::STATUS_DELIVERY_REFUND,
            OrderStatus::STATUS_LOGISTICS_ACCEPTED,
            OrderStatus::STATUS_DELIVERY_LOST,
            OrderStatus::STATUS_CC_INPUT_QUEUE,
            OrderStatus::STATUS_DELIVERY_PENDING,
            OrderStatus::STATUS_LOG_DEFERRED,
            OrderStatus::STATUS_AUTOTRASH,
            OrderStatus::STATUS_RECLAMATION,
            OrderStatus::STATUS_AUTOTRASH_DOUBLE,
            OrderStatus::STATUS_CLIENT_REFUSED,
            OrderStatus::STATUS_LOG_SENT,
            OrderStatus::STATUS_LOG_RECEIVED,
            OrderStatus::STATUS_CC_CHECKING,
        ];
    }

    /**
     * Список статусов, в которых можно отредактировать предоплаченный заказ по API
     * @return array
     */
    public static function getEditablePrepaidOrderList()
    {
        return [
            OrderStatus::STATUS_CC_APPROVED,
            OrderStatus::STATUS_LOG_ACCEPTED,
            OrderStatus::STATUS_LOG_SET,
            OrderStatus::STATUS_DELIVERY_PENDING,
            OrderStatus::STATUS_LOG_DEFERRED,
        ];
    }

    /**
     * Список статусов в курьерке
     * @return array
     */
    public static function getInDeliveryStatusList()
    {
        return [
            OrderStatus::STATUS_LOG_ACCEPTED,
            OrderStatus::STATUS_LOG_GENERATED,
            OrderStatus::STATUS_LOG_SET,
            OrderStatus::STATUS_LOG_PASTED,
            OrderStatus::STATUS_DELIVERY_ACCEPTED,
            OrderStatus::STATUS_DELIVERY_LEFT_WAREHOUSE,
            OrderStatus::STATUS_DELIVERY,
            OrderStatus::STATUS_DELIVERY_LEFT_OUR_WAREHOUSE,
            OrderStatus::STATUS_DELIVERY_REDELIVERY,
            OrderStatus::STATUS_DELIVERY_LEFT_THEIR_WAREHOUSE,
            OrderStatus::STATUS_DELIVERY_DELAYED,
            OrderStatus::STATUS_DELIVERY_PENDING,
            OrderStatus::STATUS_LOG_DEFERRED,
            OrderStatus::STATUS_RECLAMATION,
            OrderStatus::STATUS_LOG_SENT,
            OrderStatus::STATUS_LOG_RECEIVED,
            OrderStatus::STATUS_DELIVERY_DENIAL,
            OrderStatus::STATUS_DELIVERY_RETURNED,
            OrderStatus::STATUS_DELIVERY_REFUND,
            OrderStatus::STATUS_LOGISTICS_ACCEPTED,
            OrderStatus::STATUS_DELIVERY_LOST,
            OrderStatus::STATUS_DELIVERY_BUYOUT,
            OrderStatus::STATUS_FINANCE_MONEY_RECEIVED,
        ];
    }

    /**
     * Список статусов в лидах
     * @return array
     */
    public static function getLeadStatusList()
    {
        return [
            OrderStatus::STATUS_SOURCE_LONG_FORM,
            OrderStatus::STATUS_SOURCE_SHORT_FORM,
            OrderStatus::STATUS_CC_POST_CALL,
            OrderStatus::STATUS_CC_INPUT_QUEUE,
        ];
    }

    /**
     * Список статусов отправленные в КЦ
     * @return array
     */
    public static function getSentToCCList()
    {
        return [
            OrderStatus::STATUS_CC_POST_CALL,
            OrderStatus::STATUS_CC_RECALL,
            OrderStatus::STATUS_CC_FAIL_CALL,
        ];
    }

    /**
     * Список статусов в холдах
     * @return array
     */
    public static function getHoldStatusList()
    {
        return [
            OrderStatus::STATUS_SOURCE_LONG_FORM,
            OrderStatus::STATUS_SOURCE_SHORT_FORM,
            OrderStatus::STATUS_CC_POST_CALL,
            OrderStatus::STATUS_CC_RECALL,
            OrderStatus::STATUS_CC_FAIL_CALL,
            OrderStatus::STATUS_CC_INPUT_QUEUE,
        ];
    }

    /**
     * Список статусов переданных курьерке
     * @return array
     */
    public static function getTransferredStatusList()
    {
        return [
            OrderStatus::STATUS_LOG_ACCEPTED,
            OrderStatus::STATUS_LOG_GENERATED,
            OrderStatus::STATUS_LOG_SET,
            OrderStatus::STATUS_LOG_PASTED,
            OrderStatus::STATUS_DELIVERY_PENDING,
            OrderStatus::STATUS_LOG_SENT,
        ];
    }

    /**
     * Список статусов ожидающих отправки в КС
     * @return array
     */
    public static function getReadyToSendStatusList()
    {
        return [
            OrderStatus::STATUS_CC_APPROVED,
            OrderStatus::STATUS_DELIVERY_PENDING,
        ];
    }

    /**
     * Список статусов отказов
     * @return array
     */
    public static function getCCRejectedStatusList()
    {
        return [
            OrderStatus::STATUS_CC_REJECTED,
            OrderStatus::STATUS_CC_TRASH,
            OrderStatus::STATUS_CC_DOUBLE,
        ];
    }

    /**
     * @param $statuses
     * @return array
     */
    public static function getStatusNamesForArray($statuses)
    {
        $statusNames = [];
        $statusMap = self::getStatusMap();

        foreach ($statuses as $status) {
            $statusNames[$status] = $status . '. ' . Yii::t('common', $statusMap[$status]);
        }
        return $statusNames;
    }

    /**
     * @return array|null
     */
    protected static function getStatusMap()
    {
        if (is_null(self::$statuses)) {
            self::$statuses = self::find()->collection();
        }

        return self::$statuses;
    }
}
