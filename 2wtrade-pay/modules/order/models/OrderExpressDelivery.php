<?php

namespace app\modules\order\models;

use Yii;

/**
 * This is the model class for table "order_express_delivery".
 *
 * @property integer $id
 * @property integer $order_id
 *
 * @property Order $order
 */
class OrderExpressDelivery extends \yii\db\ActiveRecord
{

    const SKIP_ERROR = 'OK';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_express_delivery}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id'], 'required'],
            [['order_id'], 'integer'],
            [['order_id'], 'unique', 'message' => self::SKIP_ERROR],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => Yii::t('common', 'Заказ'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @param integer $orderId
     * @param bool $checked
     * @return bool
     */
    public static function saveIt($orderId, $checked)
    {
        if ($checked) {
            return self::saveTrue($orderId);
        } else {
            return self::saveFalse($orderId);
        }
    }

    /**
     * @param integer $orderId
     * @return bool
     */
    public static function saveTrue($orderId)
    {
        $object = new self(['order_id' => $orderId]);
        if (!$object->save()) {
            if ($object->getFirstError('order_id') == self::SKIP_ERROR) {
                return true;
            }
            return false;
        }
        return true;
    }

    /**
     * @param integer $orderId
     * @return integer
     */
    public static function saveFalse($orderId)
    {
        if (self::deleteAll(['order_id' => $orderId])) {
            return true;
        }
        return false;
    }
}
