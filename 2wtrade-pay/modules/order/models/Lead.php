<?php

namespace app\modules\order\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\Country;
use app\models\Currency;
use app\models\Landing;
use app\models\Notification;
use app\models\OrderCreationDate;
use app\models\Partner;
use app\models\PartnerCountry;
use app\models\Product;
use app\models\ProductLanding;
use app\models\Source;
use app\models\SourceProduct;
use app\models\Timezone;
use app\modules\api\controllers\v1\LeadController;
use app\modules\api\models\ApiLog;
use app\modules\callcenter\jobs\SendCallCenterRequestJob;
use app\modules\delivery\models\Delivery;
use Yii;

/**
 * Class Lead
 * @property integer $order_id
 * @property integer $foreign_id
 * @property string $comment
 * @property double $revenue
 * @property string $ip
 * @property string $site
 * @property string $street
 * @property string $postal_code
 * @property string $address_detail
 * @property string $name_detail
 * @property string $city
 * @property string $district
 * @property double $price
 * @property double $delivery
 * @property string $phone
 * @property integer $is_validated_int
 * @property string $address
 * @property string $prefecture
 * @property string $name
 * @property double $total_price
 * @property integer $price_currency
 * @property string $goods_id
 * @property string $goods_array_json
 * @property integer $ts_spawn
 * @property string $country
 * @property string $currency_local
 * @property integer $num
 * @property string $sub_district
 * @property string $with_address
 * @property string $package_id
 * @property string $web_id
 * @property string $partner_id
 * @property string $email
 * @property string $payment_type
 * @property string $payment_amount
 * @property integer $source_id
 * @property string $source_status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $second_phone
 *
 * @property Order $order
 * @property LeadLog[] $logs
 */
class Lead extends ActiveRecordLogUpdateTime
{
    const STATUS_HOLD = 'hold';
    const STATUS_CONFIRMED = 'confirmed';
    const STATUS_CANCELLED = 'cancelled';
    const STATUS_TRASH = 'trash';
    const STATUS_UNCALLED = 'uncalled';
    const STATUS_RECALL = 'recall';

    const PRODUCT_GREEN_COFFEE = 7;
    const PRODUCT_GOJI_CREAM = 1;
    const PRODUCT_VARIKOSETTE = 36;
    const PRODUCT_INTOXIC = 25;
    const PRODUCT_NEOGELIO_MASK = 79;

    const PACKAGE_LABELS = [
        1 => 'no campaign',
        3 => '2+1',
        5 => '3+2',
        2 => '2 Green coffee + 1 Goji cream',
        6 => '2 Green coffee + 1 Varikosette',
        4 => '2 Green coffee + 1 Intoxic',
        7 => '2 Green coffee + 1 Neogelio Mask'
    ];

    /**
     * @var Order
     */
    public $order;

    /**
     * @var Country
     */
    public $orderCountry;

    /**
     * @var Product
     */
    public $orderProduct;

    /**
     * @var Landing
     */
    public $orderLanding;

    /**
     * @var ProductLanding
     */
    public $orderProductLanding;

    /**
     * @var bool
     */
    public $isDuplicate;

    /**
     * Массив продуктов для 2wstore
     */
    public $goods_array = [];

    /**
     * @var string
     */
    public $is_validated;

    /**
     * @var string Выполняемый экшен, при изменение записи
     */
    public $route;

    /**
     * @var array
     * порядок элементов в массиве не менять, а то не будет работать self::detectPackageByProducts
     */
    protected static $packages = [
        2 => [
            'action' => 'namedProducts',
            self::PRODUCT_GREEN_COFFEE => ['quantity' => 2, 'price' => null],
            self::PRODUCT_GOJI_CREAM => ['quantity' => 1, 'price' => 0]
        ],
        6 => [
            'action' => 'namedProducts',
            self::PRODUCT_GREEN_COFFEE => ['quantity' => 2, 'price' => null],
            self::PRODUCT_VARIKOSETTE => ['quantity' => 1, 'price' => 0]
        ],
        4 => [
            'action' => 'namedProducts',
            self::PRODUCT_GREEN_COFFEE => ['quantity' => 2, 'price' => null],
            self::PRODUCT_INTOXIC => ['quantity' => 1, 'price' => 0]
        ],
        7 => [
            'action' => 'namedProducts',
            self::PRODUCT_GREEN_COFFEE => ['quantity' => 2, 'price' => null],
            self::PRODUCT_NEOGELIO_MASK => ['quantity' => 1, 'price' => 0]
        ],
        5 => [
            ['quantity' => 3, 'price' => null],
            ['quantity' => 2, 'price' => 0],
        ],
        3 => [
            ['quantity' => 2, 'price' => null],
            ['quantity' => 1, 'price' => 0],
        ],
        1 => [
            ['quantity' => 1, 'price' => null],
        ],
    ];

    /**
     * @inheritdoc
     */
    public function load($data, $formName = null)
    {
        if (parent::load($data, $formName)) {
            $this->foreign_id = $this->order_id;
            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%lead}}';
    }

    /**
     * @param array $post
     * @return Lead
     * @throws \Exception
     */
    public static function createFromPost($post)
    {
        $model = new Lead();

        //АдКомбо для некоторых стран шлет поле formatted_address вместо address
        $post['address'] = $post['formatted_address'] ?? $post['address'];
        // Доп. номер телефона может быть в нескольких полях
        $post['second_phone'] = $post['second_phone'] ?? $post['cc_second_phone'] ?? $post['cc_additional11'] ?? null;

        $currency = Currency::findOne(['char_code' => $post['currency_local'] ?? null]);

        $post['price_currency'] = $currency ? $currency->id : null;

        if (!$model->load($post, '')) {
            throw new \Exception(__CLASS__ . '::' . __METHOD__);
        }

        return $model;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'order_id' => Yii::t('common', 'Номер заказа'),
            'foreign_id' => Yii::t('common', 'Внешний номер заказа'),
            'comment' => Yii::t('common', 'Комментарий'),
            'revenue' => Yii::t('common', 'Себестоимость'),
            'ip' => Yii::t('common', 'IP'),
            'site' => Yii::t('common', 'Сайт'),
            'street' => Yii::t('common', 'Улица'),
            'postal_code' => Yii::t('common', 'Почтовый код'),
            'address_detail' => 'Address Detail',
            'name_detail' => 'Name Detail',
            'city' => Yii::t('common', 'Город'),
            'district' => 'District',
            'price' => Yii::t('common', 'Цена'),
            'delivery' => 'Delivery',
            'phone' => Yii::t('common', 'Телефон заказчика'),
            'is_validated' => 'Is Validated',
            'address' => Yii::t('common', 'Адрес'),
            'prefecture' => 'Prefecture',
            'name' => Yii::t('common', 'Имя заказчика'),
            'total_price' => Yii::t('common', 'Итоговая цена'),
            'price_currency' => Yii::t('common', 'Валюта'),
            'goods_id' => Yii::t('common', 'Номер товара'),
            'goods_array_json' => Yii::t('common', 'Товары'),
            'ts_spawn' => Yii::t('common', 'Дата создания в источнике'),
            'country' => Yii::t('common', 'Страна'),
            'currency_local' => Yii::t('common', 'Валюта'),
            'num' => Yii::t('common', 'Количество'),
            'sub_district' => 'Sub District',
            'with_address' => 'With Address',
            'package_id' => 'Package ID',
            'web_id' => 'Web ID',
            'partner_id' => Yii::t('common', 'Партнер'),
            'email' => Yii::t('common', 'email'),
            'payment_type' => Yii::t('common', 'Тип оплаты'),
            'payment_amount' => Yii::t('common', 'Предоплата'),
            'source_status' => Yii::t('common', 'Статус в источнике'),
            'second_phone' => Yii::t('common', 'Дополнительный телефон'),
            'source_id' => Yii::t('common', 'Идентификатор источника'),
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['order_id', 'country', 'site', 'phone', 'name', 'revenue', 'source_id', 'foreign_id'], 'required'],
            [['source_id', 'foreign_id'], 'unique', 'targetAttribute' => ['source_id', 'foreign_id']],
            [
                ['goods_array', 'currency_local'],
                'required',
                'when' => function ($model) {
                    return is_null($model->goods_id);
                },
            ],
            [['order_id', 'num', 'ts_spawn', 'created_at', 'updated_at', 'price_currency', 'source_id'], 'integer'],
            [['price', 'total_price', 'revenue', 'delivery'], 'double'],
            [['country', 'partner_id', 'site', 'name'], 'string'],
            ['email', 'string'],
            [['ip'], 'string', 'max' => 100],
            [['comment'], 'string'],

            ['payment_type', 'string', 'max' => 20],
            [['currency_local', 'payment_amount'], 'string'],

            [['address'], 'string', 'max' => 500],

            [
                'country',
                'validateCountry',
                'when' => function ($model) {
                    return $model->partner_id == null;
                },
                'enableClientValidation' => false
            ],
            [
                'partner_id',
                'validatePartner',
            ],
            [
                'order_id',
                'validateOrderId',
                'when' => function ($model) {
                    return $model->isNewRecord;
                }
            ],
            ['site', 'validateSite'],
            [
                'goods_id',
                'validateGoodsId',
                'when' => function ($model) {
                    return !is_null($model->goods_id);
                },
            ],
            [
                'goods_array',
                'validateGoodsArray',
                'when' => function ($model) {
                    return !is_null($model->goods_array);
                }
            ],
            [
                [
                    'street',
                    'postal_code',
                    'address_detail',
                    'name_detail',
                    'city',
                    'district',
                    'prefecture',
                    'sub_district',
                    'with_address',
                    'package_id',
                    'web_id',
                    'second_phone'
                ],
                'safe'
            ],
            ['source_status', 'string', 'max' => 50],
            ['source_status', 'default', 'value' => self::STATUS_HOLD],
            [
                ['price_currency'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Currency::className(),
                'targetAttribute' => ['price_currency' => 'id']
            ],
            ['source_id', 'exist', 'targetClass' => Source::class, 'targetAttribute' => ['source_id' => 'id']],
        ];
    }

    /**
     * @param $attribute
     */
    public function validateGoodsArray($attribute)
    {
        $this->source_id = Yii::$app->apiUser->source->id;
        $this->goods_array_json = json_encode($this->goods_array);
        $products = [];
        $priceTotal = 0;
        foreach ($this->goods_array as $product) {
            $priceTotal += $product['price'] * (int)$product['quantity'];
            $products[] = $product;
            if (!Product::findOne($product['sku'])) {
                $this->addError($attribute, Yii::t('common', 'Неизвестный номер товара "{id}".', [
                    'id' => $product['sku'],
                ]));
            };

            $this->checkSourceProduct($attribute, $product['sku']);
        }
        !$priceTotal ?: ($this->price = $priceTotal);
        !$priceTotal ?: ($this->total_price = $priceTotal);
        $this->goods_array = $products;
    }

    /**
     * Находит цену последнего продукта с ид $sku для страны заказа
     * @param integer $sku
     * @return bool|string
     */
    public function pricePreviousProduct($sku)
    {
        $countryId = Country::find()->byCharCode($this->country)->one()->id;
        return OrderProduct::find()
            ->select([OrderProduct::tableName() . '.price'])
            ->leftJoin(Order::tableName(), OrderProduct::tableName() . '.order_id=' . Order::tableName() . '.id')
            ->leftJoin(Country::tableName(), OrderProduct::tableName() . '.order_id=' . Order::tableName() . '.id')
            ->where([Order::tableName() . '.country_id' => $countryId])
            ->andWhere(['=', OrderProduct::tableName() . '.product_id', $sku])
            ->andWhere(['!=', OrderProduct::tableName() . '.price', 0])
            ->orderBy(['order_id' => SORT_DESC])
            ->scalar();
    }

    /**
     * @param string $attribute
     */
    public function validatePartner($attribute)
    {
        if (!$partner = Partner::findOne(['foreign_id' => $this->partner_id,])) {
            $this->addError($attribute, Yii::t('common', 'Такого партнера нет.'));
            return;
        };

        //Проверяем активный ли партнер
        if (!$partner->active) {
            $this->addError($attribute, Yii::t('common', 'Партнер не активен.'));
            return;
        };

        //Проверяем активен ли он в стране
        $partnerCountry = PartnerCountry::find()
            ->innerJoinWith(['country'])
            ->where([PartnerCountry::tableName() . '.partner_id' => $partner->id])
            ->andWhere([Country::tableName() . '.char_code' => $this->country])
            ->one();

        //Если не активен в стране
        //Ошибка Тогда Партнер в этой стране не работает
        if (!$partnerCountry) {
            $this->addError($attribute, Yii::t('common', 'Партнер не работает в стране ') . $this->country);
            return;
        }
        $this->orderCountry = $partnerCountry->country;
    }

    /**
     * @param string $attribute
     */
    public function validateCountry($attribute)
    {
        if (!Country::findOne(['char_code' => $this->country])) {
            $this->addError($attribute, Yii::t('common', 'Неизвестный код страны "{code}".', [
                'code' => $this->country,
            ]));
            return;
        }
        $partnerCountry = PartnerCountry::find()
            ->innerJoinWith(['partner', 'country'])
            ->where([Partner::tableName() . '.default' => 1])
            ->andWhere([Partner::tableName() . '.active' => 1])
            ->andWhere([Country::tableName() . '.char_code' => $this->country])
            ->andWhere([Partner::tableName() . '.source_id' => Yii::$app->apiUser->source->id ?? $this->source_id])
            ->one();
        if (!$partnerCountry) {
            $this->addError($attribute, Yii::t('common', 'Нет партнера по умолчанию в стране {code}', ['code' => $this->country]));
            return;
        }
        $this->orderCountry = $partnerCountry->country;
    }

    /**
     * @param string $attribute
     */
    public function validateOrderId($attribute)
    {
        $this->isDuplicate = false;
        $orderFind = Order::find()->bySourceId(Yii::$app->apiUser->source->id)->byForeignId($this->order_id)->one();
        if ($orderFind !== null) {
            $this->addError($attribute, Yii::t('common', 'Заказ уже существует.'));
            $this->isDuplicate = true;
            $this->order = $orderFind;
        }
    }

    /**
     * @param string $attribute
     */
    public function validateGoodsId($attribute)
    {
        $this->orderProduct = Product::findOne($this->goods_id);

        if (!$this->orderProduct) {
            $this->addError($attribute, Yii::t('common', 'Неизвестный номер товара {id}.', [
                'id' => $this->goods_id,
            ]));
        }
        $this->checkSourceProduct($attribute, $this->goods_id);
    }

    public function validateSite()
    {
        $this->orderLanding = Landing::find()
            ->byUrl($this->site)
            ->one();

        if (!$this->orderLanding) {
            $this->orderLanding = new Landing();
            $this->orderLanding->url = $this->site;
        }
    }

    /**
     * @param $attribute
     * @param $productId
     */
    private function checkSourceProduct($attribute, $productId)
    {
        $sourceProduct = SourceProduct::find()
            ->where([
                'source_id' => Yii::$app->apiUser->source->id ?? $this->source_id,
                'product_id' => $productId
            ])
            ->exists();

        if (!$sourceProduct) {
            $this->addError($attribute, Yii::t('common', 'Товар {id} не привязан к источнику {source}.', [
                'id' => $productId,
                'source' => Yii::$app->apiUser->source->name ?? $this->source_id,
            ]));
        }
    }

    /**
     * @return string
     */
    public function create()
    {
        $apiLog = new ApiLog();
        $apiLog->type = ApiLog::TYPE_LEAD;
        $apiLog->input_data = json_encode(Yii::$app->request->post(), JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
        $apiLog->status = ApiLog::STATUS_FAIL;

        if (mb_strtolower($this->is_validated) == 'true') {
            $this->is_validated_int = 1;
        } else {
            $this->is_validated_int = 0;
        }

        $source = Yii::$app->apiUser->source;
        $this->source_id = $source->id;
        if (!$this->currency_local) {
            $country = Country::findOne(['char_code' => $this->country]);

            if ($country) {
                $this->price_currency = $country->currency->id;
            }
        }

        if (!$isActiveSource = $source->active) {
            $this->addError('site', "Source {$source->name} not active");
        }
        $this->validate();

        if ($this->validate()) {

            $transaction = Yii::$app->db->beginTransaction();

            if ($this->orderLanding->isNewRecord) {
                if (!$this->orderLanding->save()) {
                    $this->addError('site', Yii::t('common', 'Не удалось добавить новый лендинг "{landing}".', [
                        'landing' => $this->site,
                    ]));
                }
            }

            if (!$this->hasErrors() && $this->orderProduct) {
                $this->orderProductLanding = ProductLanding::find()
                    ->where(['product_id' => $this->orderProduct->id])
                    ->andWhere(['landing_id' => $this->orderLanding->id])
                    ->one();

                if (!$this->orderProductLanding) {
                    $this->orderProductLanding = new ProductLanding();
                    $this->orderProductLanding->product_id = $this->orderProduct->id;
                    $this->orderProductLanding->landing_id = $this->orderLanding->id;

                    if (!$this->orderProductLanding->save()) {
                        $this->addError('site', Yii::t('common', 'Не удалось сохранить связку товара с лендингом.'));
                    }
                }
            }

            if (!$this->hasErrors()) {
                $this->order = new Order;

                $this->order->source_id = $source->id;
                $this->order->call_center_type = $source->default_call_center_type;

                $this->order->foreign_id = $this->order_id;
                $this->order->country_id = $this->orderCountry->id;
                $this->order->landing_id = $this->orderLanding->id;

                if (empty($this->with_address) || mb_strtolower($this->with_address) == 'false') {
                    $this->order->status_id = OrderStatus::STATUS_SOURCE_SHORT_FORM;
                    $this->order->source_form = Order::TYPE_FORM_SHORT;
                } else {
                    $this->order->status_id = OrderStatus::STATUS_SOURCE_LONG_FORM;
                    $this->order->source_form = Order::TYPE_FORM_LONG;
                }
                //https://2wtrade-tasks.atlassian.net/browse/NPAY-1234
                if ((isset($this->payment_type) && $this->payment_type == Order::ORDER_PAYMENT_TYPE_PREPAID)) {
                    $this->order->payment_type = $this->payment_type;
                    $this->order->payment_amount = $this->payment_amount;
                    $this->order->status_id = OrderStatus::STATUS_CC_APPROVED;
                }
                $this->order->customer_ip = $this->ip;
                $this->order->customer_full_name = $this->name;
                $this->order->customer_phone = $this->phone;
                $this->order->customer_mobile = $this->second_phone ?? '';
                $this->order->customer_city = $this->city;
                $this->order->customer_address = $this->address;
                $this->order->customer_zip = $this->postal_code;
                $this->order->customer_street = $this->street;
                $this->order->customer_subdistrict = $this->sub_district;
                $this->order->customer_district = $this->district;
                $this->order->customer_province = $this->prefecture;

                if (isset($this->email)) {
                    $this->order->customer_email = $this->email;
                }

                $this->order->comment = $this->comment;

                $this->order->price = $this->price;
                $this->order->price_total = $this->total_price;
                $this->order->price_currency = $this->price_currency ?? $this->order->country->currency->id;
                $this->order->income = $this->revenue;
                $this->order->delivery = $this->delivery === null ? 0 : $this->delivery;

                !isset($this->web_id) ?: $this->order->webmaster_identifier = $this->web_id;

                if ($this->order->save()) {
                    $this->order_id = $this->order->id;
                    /**
                     * Запись даты создания заказа в источнике
                     */
                    if ($this->ts_spawn) {
                        // ERP-694
                        if ($partner = Partner::findOne(['foreign_id' => $this->partner_id,])) {
                            if ($timezone = Timezone::findOne(['id' => $partner->getAttribute('timezone_id')])
                            ) {
                                $this->ts_spawn = $this->ts_spawn - (isset($timezone['time_offset']) ? $timezone['time_offset'] : 0);
                            }
                        }

                        $orderCreationDate = new OrderCreationDate();
                        $orderCreationDate->order_id = $this->order->id;
                        $orderCreationDate->foreign_create_at = $this->ts_spawn;
                        $orderCreationDate->save();
                    }

                    $products = $this->prepareProducts();

                    if ($products) {
                        $apiLog->status = ApiLog::STATUS_SUCCESS;

                        $apiLog->output_data = json_encode([
                            'country' => $this->orderCountry->char_code,
                            'order_id' => $this->order->id,
                            'foreign_id' => $this->order->foreign_id,
                        ], JSON_UNESCAPED_UNICODE);
                    }

                    if ($this->order->payment_type == Order::ORDER_PAYMENT_TYPE_PREPAID) {
                        // для предоплаченных заказов сразу вызовем брокер и назначим КС если возможно
                        try {
                            $brokerResult = $this->order->findDeliveryByBroker($products);
                            if ($brokerResult) {
                                if ($brokerResult['deliveryId']) {
                                    Delivery::createRequest($brokerResult['deliveryId'], $this->order, false);
                                }
                            }
                        } catch (\Exception $e) {
                            // Никуда не передаем ошибку, чтобы нам дубли не слали
                            // $e->getMessage();
                        }
                    }

                    $this->save(false);

                } else {
                    $this->addErrors($this->order->getErrors());
                }
            }

            if ($apiLog->status == ApiLog::STATUS_SUCCESS) {
                $transaction->commit();
                Yii::$app->queueCallCenterSend->push(new SendCallCenterRequestJob(['orderId' => $this->order_id]));
                Yii::$app->notification->send(Notification::TRIGGER_LEAD_ADDED, null, $this->orderCountry->id);
            } else {
                $transaction->rollBack();
            }
        } else {
            if ($this->isDuplicate) {
                $apiLog->status = ApiLog::STATUS_DUPLICATE;

                $apiLog->output_data = json_encode([
                    'country' => $this->orderCountry->char_code,
                    'order_id' => $this->order->id,
                    'foreign_id' => $this->order->foreign_id,
                ], JSON_UNESCAPED_UNICODE);
            }
        }

        if ($apiLog->status == ApiLog::STATUS_FAIL) {
            $apiLog->output_data = json_encode([
                'errors' => json_encode($this->getErrors(), JSON_UNESCAPED_UNICODE),
                'source' => $source->name,
            ], JSON_UNESCAPED_UNICODE);
        }

        $apiLog->country_id = $this->orderCountry ? $this->orderCountry->id : null;
        $apiLog->save();
        if ($this->order instanceof Order && !$this->order->isNewRecord) {
            try {
                $createLog = new LeadCreateLog();
                $createLog->order_id = $this->order->id;
                $createLog->input_data = $apiLog->input_data;
                $createLog->output_data = $apiLog->output_data;
                if (!$createLog->save()) {
                    Yii::getLogger()->log($createLog->getErrors(), \yii\log\Logger::LEVEL_ERROR);
                }
            } catch (\Throwable $e) {
                Yii::getLogger()->log($e, \yii\log\Logger::LEVEL_ERROR);
            }
        }

        return $apiLog->status;
    }

    /**
     * редактирование предоплаченных заказов
     * @return array
     */
    public function edit()
    {
        $apiLog = new ApiLog();
        $apiLog->type = ApiLog::TYPE_LEAD;
        $apiLog->input_data = json_encode(Yii::$app->request->post(), JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
        $apiLog->status = ApiLog::STATUS_FAIL;
        $returnCode = 0;

        if (mb_strtolower($this->is_validated) == 'true') {
            $this->is_validated_int = 1;
        } else {
            $this->is_validated_int = 0;
        }
        $source = Yii::$app->apiUser->source;
        $this->source_id = $source->id;
        if (!$isActiveSource = $source->active) {
            $returnCode = LeadController::RETURN_CODE_ERROR_SOURCE;
            $this->addError('site', "Source {$source->name} not active");
        }

        // для того чтобы не проверять уникальность по номеру заказа в Lead, который мы не меняем
        $this->isNewRecord = false;

        if ($this->validate()) {

            $transaction = Yii::$app->db->beginTransaction();

            if ($this->orderLanding->isNewRecord) {
                if (!$this->orderLanding->save()) {
                    $returnCode = LeadController::RETURN_CODE_ERROR_LANDING;
                    $this->addError('site', Yii::t('common', 'Не удалось добавить новый лендинг "{landing}".', [
                        'landing' => $this->site,
                    ]));
                }
            }

            if (!$this->hasErrors() && $this->orderProduct) {
                $this->orderProductLanding = ProductLanding::find()
                    ->where(['product_id' => $this->orderProduct->id])
                    ->andWhere(['landing_id' => $this->orderLanding->id])
                    ->one();

                if (!$this->orderProductLanding) {
                    $this->orderProductLanding = new ProductLanding();
                    $this->orderProductLanding->product_id = $this->orderProduct->id;
                    $this->orderProductLanding->landing_id = $this->orderLanding->id;

                    if (!$this->orderProductLanding->save()) {
                        $returnCode = LeadController::RETURN_CODE_ERROR_PRODUCT_LANDING;
                        $this->addError('site', Yii::t('common', 'Не удалось сохранить связку товара с лендингом.'));
                    }
                }
            }

            if (!$this->hasErrors()) {

                $this->order = Order::find()
                    ->byForeignId($this->order_id)
                    ->bySourceId($source->id)
                    ->one();

                if (!$this->order) {
                    // вернуть ошибку не найден заказ по источнику и номеру
                    $returnCode = LeadController::RETURN_CODE_ERROR_NO_ORDER;
                    $this->addError('site', Yii::t('common', 'Не найден заказ по источнику и номеру'));
                }
            }

            if (!$this->hasErrors()) {
                if ($this->order->payment_type != Order::ORDER_PAYMENT_TYPE_PREPAID) {
                    // вернуть ошибку, что заказ не является предоплаченным
                    $returnCode = LeadController::RETURN_CODE_ERROR_NOT_PREPAID;
                    $this->addError('site', Yii::t('common', 'Заказ не является предоплаченным'));
                }
            }

            if (!$this->hasErrors()) {
                if (!in_array($this->order->status_id, OrderStatus::getEditablePrepaidOrderList())) {
                    // вернуть ошибку, что заказ в недопустимом для редактирования статусе
                    $returnCode = LeadController::RETURN_CODE_ERROR_EDIT_STATUS;
                    $this->addError('site', Yii::t('common', 'Заказ в недопустимом для редактирования статусе {status}',
                        [
                            'status' => $this->order->status_id
                        ]
                    ));
                }
            }

            if (!$this->hasErrors()) {

                $this->order->country_id = $this->orderCountry->id;
                $this->order->landing_id = $this->orderLanding->id;
                $this->order->payment_amount = $this->payment_amount;
                $this->order->customer_ip = $this->ip;
                $this->order->customer_full_name = $this->name;
                $this->order->customer_phone = $this->phone;
                $this->order->customer_city = $this->city;
                $this->order->customer_address = $this->address;
                $this->order->customer_zip = $this->postal_code;

                if (isset($this->email)) {
                    $this->order->customer_email = $this->email;
                }

                $this->order->comment = $this->comment;
                $this->order->price = $this->price;
                $this->order->price_total = $this->total_price;
                $this->order->income = $this->revenue;
                $this->order->delivery = $this->delivery === null ? 0 : $this->delivery;

                !isset($this->web_id) ?: $this->order->webmaster_identifier = $this->web_id;

                if ($this->order->save()) {

                    $products = $this->prepareProducts(false);

                    if ($products) {
                        $apiLog->status = ApiLog::STATUS_SUCCESS;

                        $apiLog->output_data = json_encode([
                            'country' => $this->orderCountry->char_code,
                            'order_id' => $this->order->id,
                            'foreign_id' => $this->order->foreign_id,
                        ], JSON_UNESCAPED_UNICODE);
                    }
                    try {
                        $brokerResult = $this->order->findDeliveryByBroker($products);
                        if ($brokerResult) {
                            if ($brokerResult['deliveryId']) {
                                // если брокер переназначил delivery, удаляем ранее созданный deliveryRequest
                                if ($this->order->deliveryRequest) {
                                    $this->order->deliveryRequest->delete();
                                }
                                Delivery::createRequest($brokerResult['deliveryId'], $this->order, false);
                            }
                        }
                    } catch (\Exception $e) {
                        // $e->getMessage();
                    }
                } else {
                    $returnCode = LeadController::RETURN_CODE_ERROR_ORDER_SAVE;
                    $this->addErrors($this->order->getErrors());
                }
            }

            if ($apiLog->status == ApiLog::STATUS_SUCCESS) {
                $transaction->commit();
            } else {
                $transaction->rollBack();
            }
        }

        if ($apiLog->status == ApiLog::STATUS_FAIL) {
            $apiLog->output_data = json_encode([
                'errors' => json_encode($this->getErrors(), JSON_UNESCAPED_UNICODE),
                'source' => $source->name,
            ], JSON_UNESCAPED_UNICODE);
        }

        $apiLog->country_id = $this->orderCountry ? $this->orderCountry->id : null;
        $apiLog->save();
        if ($this->order instanceof Order && !$this->order->isNewRecord) {
            try {
                $createLog = new LeadCreateLog();
                $createLog->order_id = $this->order->id;
                $createLog->input_data = $apiLog->input_data;
                $createLog->output_data = $apiLog->output_data;
                if (!$createLog->save()) {
                    Yii::getLogger()->log($createLog->getErrors(), \yii\log\Logger::LEVEL_ERROR);
                }
            } catch (\Throwable $e) {
                Yii::getLogger()->log($e, \yii\log\Logger::LEVEL_ERROR);
            }
        }

        return [
            'status' => $apiLog->status,
            'code' => $returnCode
        ];
    }

    /**
     * @param bool $create создание или редактирование лида
     * @return array|false
     */
    protected function prepareProducts($create = true)
    {

        if (!$create) {
            // удаляем ранее созданные продукты, т.к. они могли поменяться
            OrderProduct::deleteAll(['order_id' => $this->order->id]);
        }

        $products = [];
        //Для 2wstore
        if (!empty($this->goods_array)) {
            foreach ($this->goods_array as $product) {
                $orderProduct = new OrderProduct();
                $orderProduct->order_id = $this->order->id;
                $orderProduct->product_id = $product['sku'];
                $orderProduct->quantity = $product['quantity'];
                $orderProduct->price = $product['price'];

                $products[] = $orderProduct;
                if (!$orderProduct->save()) {
                    $this->addErrors($orderProduct->getErrors());
                    return false;
                }

                if ($create) {
                    // сохраним только в режиме создания
                    $leadProduct = new LeadProduct();
                    $leadProduct->order_id = $this->order->id;
                    $leadProduct->product_id = $product['sku'];
                    $leadProduct->quantity = $product['quantity'];
                    $leadProduct->price = $product['price'];
                    $leadProduct->save();
                }
            }
            return $products;
        }

        if ($this->package_id) {
            if (array_key_exists($this->package_id, self::$packages)) {
                $package = self::$packages[$this->package_id];
                $isNamedProducts = false;
                if (isset($package['action'])) { // акции с именованными продуктами
                    $isNamedProducts = true;
                }

                foreach ($package as $key => $info) {
                    if ($key === 'action') {
                        continue;
                    }

                    $orderProduct = new OrderProduct();
                    $leadProduct = new LeadProduct();
                    $orderProduct->order_id = $leadProduct->order_id = $this->order->id;
                    $orderProduct->quantity = $leadProduct->quantity = $info['quantity'];

                    if ($isNamedProducts) {
                        $orderProduct->product_id = $leadProduct->product_id = $key;
                    } else {
                        $orderProduct->product_id = $leadProduct->product_id = $this->goods_id;
                    }
                    $products[] = $orderProduct;

                    if (is_null($info['price'])) {
                        $this->order->price = $this->order->price / $info['quantity'];
                        $this->order->save(false, ['price']);

                        $orderProduct->price = $leadProduct->price = $this->order->price;
                    }

                    if (!$orderProduct->save()) {
                        $this->addErrors($orderProduct->getErrors());
                        return false;
                    } else {
                        if ($create) {
                            // сохраним только в режиме создания
                            $leadProduct->save(false);
                        }
                    }
                }

                return $products;
            } else {
                $this->addError('package_id', Yii::t('common', 'Неизвестный номер пакета "{package}".', [
                    'package' => $this->package_id,
                ]));
            }
        } else {
            $orderProduct = new OrderProduct();
            $orderProduct->order_id = $this->order->id;
            $orderProduct->product_id = $this->goods_id;
            $orderProduct->quantity = $this->num;
            $orderProduct->price = $this->order->price;

            $leadProduct = new LeadProduct();
            $leadProduct->order_id = $this->order->id;
            $leadProduct->product_id = $this->goods_id;
            $leadProduct->quantity = $this->num;
            $leadProduct->price = $this->order->price;

            $products[] = $orderProduct;
            if ($orderProduct->save()) {
                if ($create) {
                    // сохраним только в режиме создания
                    $leadProduct->save(false);
                }
                return $products;
            } else {
                $this->addErrors($orderProduct->getErrors());
            }
        }

        return false;
    }

    /**
     * Определить по массиву продуктов ID акции
     *
     * @param array $products
     * @return integer
     */
    public static function detectPackageByProducts($products)
    {

        if (!$products) {
            return false;
        }

        $countFree = [];
        $countPrice = [];
        // посчитаем число бесплатных продуктов и платных с группировкой по ID продуктов
        foreach ($products as $product) {
            $quantity = $product['quantity'] ?? ($product->quantity ?? 0);
            $price = $product['price'] ?? ($product->price ?? 0);
            $id = $product['product_id'] ?? ($product->product_id ?? 0);
            if ($id) {
                if ($price) {
                    $countPrice[$id] = ($countPrice[$id] ?? 0) + $quantity;
                } else {
                    $countFree[$id] = ($countFree[$id] ?? 0) + $quantity;
                }
            }
        }

        if (count($countFree) == 0) { // бесплатных товаров нет
            return 1; // package_id = 1 - без акции
        }

        // определим к какой акции подходят
        foreach (self::$packages as $packageID => $packageBody) {
            $fitFree = false;
            $fitPrice = false;
            $hasFree = false;
            $isNamedProducts = false;
            if (isset($packageBody['action'])) { // акция с именованными продуктами
                $isNamedProducts = true;
            }
            foreach ($packageBody as $key => $item) {
                if ($item === 'namedProducts') {
                    continue;
                }
                if ($item['price'] === 0) { // бесплатный товар
                    if ($isNamedProducts) { // акция с именованными товарами
                        if (isset($countFree[$key]) && $countFree[$key] == $item['quantity']) {
                            $fitFree = true;
                        }
                    } else {
                        if (in_array($item['quantity'], $countFree)) {
                            $fitFree = true;
                        }
                    }
                    $hasFree = true;
                } else {
                    if ($isNamedProducts) { // акция с именованными товарами
                        if (isset($countPrice[$key]) && $countPrice[$key] >= $item['quantity']) {
                            $fitPrice = true;
                        }
                    } else {
                        if (in_array($item['quantity'], $countPrice)) {
                            $fitPrice = true;
                        }
                    }
                }
            }
            if ($hasFree) {
                if ($fitFree && $fitPrice) {
                    return $packageID;
                }
            } else {
                if ($fitPrice) {
                    return $packageID;
                }
            }
        }
        return 1;
    }

    /**
     * Разделение массива с продуктами согласно акциям
     * @param array $products
     * @return array
     */
    public static function splitProductsToPackages($products)
    {
        $packages = self::$packages;
        $commonProducts = [];
        foreach ($products as $product) {
            if (!isset($commonProducts[$product['product_id']])) {
                $commonProducts[$product['product_id']] = $product;
            } else {
                $commonProducts[$product['product_id']]['quantity'] += $product['quantity'];
            }
        }

        $newProducts = [];
        foreach (array_keys($commonProducts) as $productId) {
            if (!isset($commonProducts[$productId])) {
                continue;
            }
            $product = $commonProducts[$productId];
            $possiblePackageIds = [];
            foreach ($packages as $packageId => $package) {
                $isPossible = true;
                if (isset($package['action']) && $package['action'] == 'namedProducts' && !isset($package[$product['product_id']])) {
                    continue;
                }
                if (isset($package['action']) && $package['action'] == 'namedProducts') {
                    foreach ($package as $id => $packageProperty) {
                        if ($id === 'action') {
                            continue;
                        }

                        if (($product['product_id'] == $id && $product['quantity'] < $packageProperty['quantity']) || (!isset($commonProducts[$id]) || $commonProducts[$id]['quantity'] < $packageProperty['quantity'])) {
                            $isPossible = false;
                            break;
                        }
                    }
                } else {
                    $packageQuantity = 0;
                    foreach ($package as $id => $packageProperty) {
                        if ($id === 'action') {
                            continue;
                        }
                        if (isset($packageProperty['quantity'])) {
                            $packageQuantity += $packageProperty['quantity'];
                        }
                    }
                    if ($packageQuantity > $product['quantity'] || $packageQuantity == 1) {
                        $isPossible = false;
                    }
                }
                if ($isPossible) {
                    $possiblePackageIds[] = $packageId;
                }
            }

            foreach ($possiblePackageIds as $packageId) {
                $package = $packages[$packageId];
                $success = true;
                while ($success) {
                    $bufferProducts = [];
                    if (isset($package['action']) && $package['action'] == 'namedProducts') {
                        foreach ($package as $key => $packageProperty) {
                            if ($key === 'action') {
                                continue;
                            }
                            if ($commonProducts[$key]['quantity'] < $packageProperty['quantity']) {
                                $success = false;
                                break;
                            }
                            $bufferProducts[] = [
                                'product_id' => $key,
                                'quantity' => $packageProperty['quantity'],
                                'price' => is_null($packageProperty['price']) ? $commonProducts[$key]['price'] : 0,
                            ];
                        }
                    } else {
                        $commonPackageQuantity = 0;
                        foreach ($package as $key => $packageProperty) {
                            if ($key === 'action') {
                                continue;
                            }

                            if (!isset($packageProperty['quantity'])) {
                                continue;
                            }
                            $commonPackageQuantity += $packageProperty['quantity'];
                            if ($commonPackageQuantity > $commonProducts[$productId]['quantity']) {
                                $success = false;
                                break;
                            }

                            $bufferProducts[] = [
                                'product_id' => $productId,
                                'quantity' => $packageProperty['quantity'],
                                'price' => is_null($packageProperty['price']) ? $commonProducts[$productId]['price'] : 0,
                            ];
                        }
                    }
                    if ($success) {
                        foreach ($bufferProducts as $bufferProduct) {
                            $commonProducts[$bufferProduct['product_id']]['quantity'] -= $bufferProduct['quantity'];
                            $newProducts[] = $bufferProduct;
                        }
                    }
                }
            }
        }

        foreach ($commonProducts as $productId => $product) {
            if ($product['quantity'] > 0) {
                $newProducts[] = $product;
            }
        }
        return $newProducts;
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (!$insert) {
            $groupId = uniqid();
            $dirtyAttributes = $this->getDirtyAttributes();
            foreach ($dirtyAttributes as $key => $item) {
                if ($item != $this->getOldAttribute($key)) {
                    $old = (string)$this->getOldAttribute($key);

                    $log = new LeadLog();
                    $log->order_id = $this->order_id;
                    $log->group_id = $groupId;
                    $log->field = $key;
                    $log->old = $old;
                    $log->new = $item;
                    $log->user_id = isset(Yii::$app->user) ? Yii::$app->user->id : null;
                    $log->route = $this->route ?: isset(Yii::$app->controller) ? Yii::$app->controller->getRoute() : null;
                    $log->save();
                }
            }
        }
        return parent::beforeSave($insert);
    }

    /**
     * @return array
     */
    public static function endStatuses()
    {
        return [
            self::STATUS_CONFIRMED,
            self::STATUS_CANCELLED,
            self::STATUS_TRASH,
        ];
    }

    public static function inProgressStatus()
    {
        return [
            self::STATUS_HOLD,
            self::STATUS_RECALL,
            self::STATUS_UNCALLED,
        ];
    }

    /***
     * @return array
     */
    public static function statusLabels()
    {
        return [
            self::STATUS_HOLD => Yii::t('common', 'В процессе'),
            self::STATUS_CONFIRMED => Yii::t('common', 'Подтвежден'),
            self::STATUS_TRASH => Yii::t('common', 'Треш'),
            self::STATUS_CANCELLED => Yii::t('common', 'Отменен'),
            self::STATUS_UNCALLED => Yii::t('common', 'Недозвон'),
            self::STATUS_RECALL => Yii::t('common', 'Перезвон'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriceCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'price_currency']);
    }

    /**
     * @return LeadLog[]
     */
    public function getLogs()
    {
        return LeadLog::find()->where(['order_id' => $this->order_id])->orderBy(['created_at' => SORT_DESC])->all();
    }
}