<?php

namespace app\modules\order\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\Currency;
use app\models\CurrencyRate;
use app\modules\deliveryreport\models\DeliveryReport;
use app\modules\deliveryreport\models\DeliveryReportRecord;
use app\modules\deliveryreport\models\PaymentOrder;
use Yii;

/**
 * This is the model class for table "order_finance_fact".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $payment_id
 * @property integer $pretension
 * @property double $price_cod
 * @property double $price_storage
 * @property double $price_fulfilment
 * @property double $price_packing
 * @property double $price_package
 * @property double $price_delivery
 * @property double $price_redelivery
 * @property double $price_delivery_back
 * @property double $price_delivery_return
 * @property double $price_address_correction
 * @property double $price_cod_service
 * @property double $price_vat
 * @property integer $price_cod_currency_id
 * @property double $price_cod_currency_rate
 * @property integer $price_storage_currency_id
 * @property double $price_storage_currency_rate
 * @property integer $price_fulfilment_currency_id
 * @property double $price_fulfilment_currency_rate
 * @property integer $price_packing_currency_id
 * @property double $price_packing_currency_rate
 * @property integer $price_package_currency_id
 * @property double $price_package_currency_rate
 * @property integer $price_delivery_currency_id
 * @property double $price_delivery_currency_rate
 * @property integer $price_redelivery_currency_id
 * @property double $price_redelivery_currency_rate
 * @property integer $price_delivery_back_currency_id
 * @property double $price_delivery_back_currency_rate
 * @property integer $price_delivery_return_currency_id
 * @property double $price_delivery_return_currency_rate
 * @property integer $price_address_correction_currency_id
 * @property double $price_address_correction_currency_rate
 * @property integer $price_cod_service_currency_id
 * @property double $price_cod_service_currency_rate
 * @property integer $price_vat_currency_id
 * @property double $price_vat_currency_rate
 * @property string $additional_prices
 * @property integer $delivery_report_id
 * @property integer $delivery_report_record_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property DeliveryReport $deliveryReport
 * @property DeliveryReportRecord $deliveryReportRecord
 * @property Order $order
 * @property PaymentOrder $payment
 */
class OrderFinanceFact extends OrderFinanceAbstract
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_finance_fact}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'order_id',
                    'payment_id',
                    'pretension',
                    'price_cod_currency_id',
                    'price_storage_currency_id',
                    'price_fulfilment_currency_id',
                    'price_packing_currency_id',
                    'price_package_currency_id',
                    'price_delivery_currency_id',
                    'price_redelivery_currency_id',
                    'price_delivery_back_currency_id',
                    'price_delivery_return_currency_id',
                    'price_address_correction_currency_id',
                    'price_cod_service_currency_id',
                    'price_vat_currency_id',
                    'delivery_report_id',
                    'delivery_report_record_id',
                    'created_at',
                    'updated_at',
                ],
                'integer'
            ],
            [
                [
                    'price_cod_currency_rate',
                    'price_storage_currency_rate',
                    'price_fulfilment_currency_rate',
                    'price_packing_currency_rate',
                    'price_package_currency_rate',
                    'price_delivery_currency_rate',
                    'price_redelivery_currency_rate',
                    'price_delivery_back_currency_rate',
                    'price_delivery_return_currency_rate',
                    'price_address_correction_currency_rate',
                    'price_cod_service_currency_rate',
                    'price_vat_currency_rate',
                ],
                'double'],
            [
                [
                    'price_cod',
                    'price_storage',
                    'price_fulfilment',
                    'price_packing',
                    'price_package',
                    'price_delivery',
                    'price_redelivery',
                    'price_delivery_back',
                    'price_delivery_return',
                    'price_address_correction',
                    'price_cod_service',
                    'price_vat',
                ],
                'number'
            ],
            [['additional_prices'], 'string'],
            [
                ['order_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Order::className(),
                'targetAttribute' => ['order_id' => 'id']
            ],
            [
                ['payment_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => PaymentOrder::className(),
                'targetAttribute' => ['payment_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'order_id' => Yii::t('common', 'Номер заказа'),
            'payment_id' => Yii::t('common', 'Платежка'),
            'pretension' => Yii::t('common', 'Претензия'),
            'price_cod' => Yii::t('common', 'COD'),
            'price_storage' => Yii::t('common', 'Стоимость хранения'),
            'price_fulfilment' => Yii::t('common', 'Стоимость обслуживания заказа'),
            'price_packing' => Yii::t('common', 'Стоимость упаковывания'),
            'price_package' => Yii::t('common', 'Стоимость упаковки'),
            'price_address_correction' => Yii::t('common', 'Стоимость корректировки адреса'),
            'price_delivery' => Yii::t('common', 'Стоимость доставки'),
            'price_redelivery' => Yii::t('common', 'Стоимость передоставки'),
            'price_delivery_back' => Yii::t('common', 'Стоимость возвращения'),
            'price_delivery_return' => Yii::t('common', 'Стоимость возврата'),
            'price_cod_service' => Yii::t('common', 'Плата за наложенный платеж'),
            'price_vat' => Yii::t('common', 'НДС'),
            'price_cod_currency_id' => Yii::t('common', 'COD, валюта'),
            'price_cod_currency_rate' => Yii::t('common', 'COD, курс'),
            'price_storage_currency_id' => Yii::t('common', 'Стоимость хранения, валюта'),
            'price_storage_currency_rate' => Yii::t('common', 'Стоимость хранения, курс'),
            'price_fulfilment_currency_id' => Yii::t('common', 'Стоимость обслуживания заказа, валюта'),
            'price_fulfilment_currency_rate' => Yii::t('common', 'Стоимость обслуживания заказа, курс'),
            'price_packing_currency_id' => Yii::t('common', 'Стоимость упаковывания, валюта'),
            'price_packing_currency_rate' => Yii::t('common', 'Стоимость упаковывания, курс'),
            'price_package_currency_id' => Yii::t('common', 'Стоимость упаковки, валюта'),
            'price_package_currency_rate' => Yii::t('common', 'Стоимость упаковки, курс'),
            'price_delivery_currency_id' => Yii::t('common', 'Стоимость доставки, валюта'),
            'price_delivery_currency_rate' => Yii::t('common', 'Стоимость доставки, курс'),
            'price_redelivery_currency_id' => Yii::t('common', 'Стоимость передоставки, валюта'),
            'price_redelivery_currency_rate' => Yii::t('common', 'Стоимость передоставки, курс'),
            'price_delivery_back_currency_id' => Yii::t('common', 'Стоимость возвращения, валюта'),
            'price_delivery_back_currency_rate' => Yii::t('common', 'Стоимость возвращения, курс'),
            'price_delivery_return_currency_id' => Yii::t('common', 'Стоимость возврата, валюта'),
            'price_delivery_return_currency_rate' => Yii::t('common', 'Стоимость возврата, курс'),
            'price_address_correction_currency_id' => Yii::t('common', 'Стоимость корректировки адреса, валюта'),
            'price_address_correction_currency_rate' => Yii::t('common', 'Стоимость корректировки адреса, курс'),
            'price_cod_service_currency_id' => Yii::t('common', 'Плата за наложенный платеж, валюта'),
            'price_cod_service_currency_rate' => Yii::t('common', 'Плата за наложенный платеж, курс'),
            'price_vat_currency_id' => Yii::t('common', 'НДС, валюта'),
            'price_vat_currency_rate' => Yii::t('common', 'НДС, курс'),
            'additional_prices' => Yii::t('common', 'Дополнительные расходы'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
            'delivery_report_id' => Yii::t('common', 'Номер отчета'),
            'delivery_report_record_id' => Yii::t('common', 'Номер строки отчета'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayment()
    {
        return $this->hasOne(PaymentOrder::className(), ['id' => 'payment_id']);
    }

    public function getDeliveryReport()
    {
        return $this->hasOne(DeliveryReport::className(), ['id' => 'delivery_report_id']);
    }

    public function getDeliveryReportRecord()
    {
        return $this->hasOne(DeliveryReportRecord::className(), ['id' => 'delivery_report_record_id']);
    }

    /**
     * @param boolean $insert
     * @return boolean
     */
    public function beforeSave($insert)
    {
        $groupId = uniqid();

        $dirtyAttributes = $this->getDirtyAttributes();

        foreach ($dirtyAttributes as $key => $item) {
            if ($item != $this->getOldAttribute($key) && $key != 'order_id') {
                $old = (string)$this->getOldAttribute($key);

                $log = new OrderLog();
                $log->order_id = $this->order_id;
                $log->group_id = $groupId;
                $log->field = 'order_finance_fact.' . $key;
                $log->old = mb_substr($old, 0, 250);
                $log->new = mb_substr($item, 0, 250);
                $log->user_id = isset(Yii::$app->user) ? Yii::$app->user->id : null;
                $log->route = isset(Yii::$app->controller) ? Yii::$app->controller->getRoute() : null;
                $log->save();
            }
        }
        return parent::beforeSave($insert);
    }

    /**
     * @return array
     */
    public static function getReportColumns(): array
    {
        $result['column'] = [
            'price_cod',
            'price_storage',
            'price_fulfilment',
            'price_packing',
            'price_package',
            'price_delivery',
            'price_redelivery',
            'price_delivery_back',
            'price_delivery_return',
            'price_address_correction',
            'price_cod_service',
            'price_vat',
        ];
        foreach ($result['column'] as $column) {
            $result['currency'][] = $column . '_currency_id';
            $result['rate'][] = $column . '_currency_rate';
        }
        return $result;
    }
}
