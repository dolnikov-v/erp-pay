<?php

namespace app\modules\order\models;

use app\modules\delivery\models\DeliveryRequest;
use Yii;

/**
 * This is the model class for table "order_finance_balance".
 *
 * @property integer $delivery_request_id
 * @property integer $order_id
 * @property double $price_cod
 * @property integer $price_cod_currency_id
 * @property double $price_storage
 * @property integer $price_storage_currency_id
 * @property double $price_fulfilment
 * @property integer $price_fulfilment_currency_id
 * @property double $price_packing
 * @property integer $price_packing_currency_id
 * @property double $price_package
 * @property integer $price_package_currency_id
 * @property double $price_delivery
 * @property integer $price_delivery_currency_id
 * @property double $price_redelivery
 * @property integer $price_redelivery_currency_id
 * @property double $price_delivery_back
 * @property integer $price_delivery_back_currency_id
 * @property double $price_delivery_return
 * @property integer $price_delivery_return_currency_id
 * @property double $price_address_correction
 * @property integer $price_address_correction_currency_id
 * @property double $price_cod_service
 * @property integer $price_cod_service_currency_id
 * @property double $price_vat
 * @property integer $price_vat_currency_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property DeliveryRequest $deliveryRequest
 * @property Order $order
 */
class OrderFinanceBalance extends OrderFinanceAbstract
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%order_finance_balance}}';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['order_id', 'delivery_request_id'], 'required'],
            [
                [
                    'order_id',
                    'delivery_request_id',
                    'created_at',
                    'updated_at',
                    'price_cod_currency_id',
                    'price_storage_currency_id',
                    'price_fulfilment_currency_id',
                    'price_packing_currency_id',
                    'price_package_currency_id',
                    'price_address_correction_currency_id',
                    'price_delivery_currency_id',
                    'price_redelivery_currency_id',
                    'price_delivery_back_currency_id',
                    'price_delivery_return_currency_id',
                    'price_cod_service_currency_id',
                    'price_vat_currency_id'
                ],
                'integer'
            ],
            [
                [
                    'price_cod',
                    'price_storage',
                    'price_fulfilment',
                    'price_packing',
                    'price_package',
                    'price_delivery',
                    'price_redelivery',
                    'price_delivery_back',
                    'price_delivery_return',
                    'price_address_correction',
                    'price_cod_service',
                    'price_vat'
                ],
                'double'
            ],
            [
                ['delivery_request_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => DeliveryRequest::className(),
                'targetAttribute' => ['delivery_request_id' => 'id']
            ],
            [
                ['order_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Order::className(),
                'targetAttribute' => ['order_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'delivery_request_id' => Yii::t('common', 'Заявка в службе доставки'),
            'order_id' => Yii::t('common', 'Номер заказ'),
            'price_cod' => Yii::t('common', 'COD'),
            'price_storage' => Yii::t('common', 'Стоимость хранения'),
            'price_fulfilment' => Yii::t('common', 'Стоимость обслуживания заказа'),
            'price_packing' => Yii::t('common', 'Стоимость упаковывания'),
            'price_package' => Yii::t('common', 'Стоимость упаковки'),
            'price_delivery' => Yii::t('common', 'Стоимость доставки'),
            'price_redelivery' => Yii::t('common', 'Стоимость передоставки'),
            'price_delivery_back' => Yii::t('common', 'Стоимость возвращения'),
            'price_delivery_return' => Yii::t('common', 'Стоимость возврата'),
            'price_address_correction' => Yii::t('common', 'Стоимость корректировки адреса'),
            'price_cod_service' => Yii::t('common', 'Плата за наложенный платеж'),
            'price_vat' => Yii::t('common', 'НДС'),
            'price_cod_currency_id' => Yii::t('common', 'COD, валюта'),
            'price_storage_currency_id' => Yii::t('common', 'Стоимость хранения, валюта'),
            'price_fulfilment_currency_id' => Yii::t('common', 'Стоимость обслуживания заказа, валюта'),
            'price_packing_currency_id' => Yii::t('common', 'Стоимость упаковывания, валюта'),
            'price_package_currency_id' => Yii::t('common', 'Стоимость упаковки, валюта'),
            'price_delivery_currency_id' => Yii::t('common', 'Стоимость доставки, валюта'),
            'price_redelivery_currency_id' => Yii::t('common', 'Стоимость передоставки, валюта'),
            'price_delivery_back_currency_id' => Yii::t('common', 'Стоимость возвращения, валюта'),
            'price_delivery_return_currency_id' => Yii::t('common', 'Стоимость возврата, валюта'),
            'price_address_correction_currency_id' => Yii::t('common', 'Стоимость корректировки адреса, валюта'),
            'price_cod_service_currency_id' => Yii::t('common', 'Плата за наложенный платеж, валюта'),
            'price_vat_currency_id' => Yii::t('common', 'НДС, валюта'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата изменения')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveryRequest()
    {
        return $this->hasOne(DeliveryRequest::className(), ['id' => 'delivery_request_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }
}