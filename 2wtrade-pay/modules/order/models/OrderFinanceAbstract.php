<?php

namespace app\modules\order\models;


use app\components\db\ActiveRecordLogUpdateTime;
use app\models\Currency;
use app\models\CurrencyRate;

/**
 * Class OrderFinanceAbstract
 * @package app\modules\order\models
 */
abstract class OrderFinanceAbstract extends ActiveRecordLogUpdateTime
{
    const FINANCE_TYPE_INCOME = 'income';
    const FINANCE_TYPE_COST = 'cost';

    const COLUMN_PRICE_COD = 'price_cod';
    const COLUMN_PRICE_STORAGE = 'price_storage';
    const COLUMN_PRICE_FULFILMENT = 'price_fulfilment';
    const COLUMN_PRICE_PACKING = 'price_packing';
    const COLUMN_PRICE_PACKAGE = 'price_package';
    const COLUMN_PRICE_ADDRESS_CORRECTION = 'price_address_correction';
    const COLUMN_PRICE_DELIVERY = 'price_delivery';
    const COLUMN_PRICE_REDELIVERY = 'price_redelivery';
    const COLUMN_PRICE_DELIVERY_BACK = 'price_delivery_back';
    const COLUMN_PRICE_DELIVERY_RETURN = 'price_delivery_return';
    const COLUMN_PRICE_COD_SERVICE = 'price_cod_service';
    const COLUMN_PRICE_VAT = 'price_vat';

    const TABLE_PRICE_COD_CURRENCY = 'price_cod_currency';
    const TABLE_PRICE_STORAGE_CURRENCY = 'price_storage_currency';
    const TABLE_PRICE_FULFILMENT_CURRENCY = 'price_fulfilment_currency';
    const TABLE_PRICE_PACKING_CURRENCY = 'price_packing_currency';
    const TABLE_PRICE_PACKAGE_CURRENCY = 'price_package_currency';
    const TABLE_PRICE_ADDRESS_CORRECTION_CURRENCY = 'price_address_correction_currency';
    const TABLE_PRICE_DELIVERY_CURRENCY = 'price_delivery_currency';
    const TABLE_PRICE_REDELIVERY_CURRENCY = 'price_redelivery_currency';
    const TABLE_PRICE_DELIVERY_BACK_CURRENCY = 'price_delivery_back_currency';
    const TABLE_PRICE_DELIVERY_RETURN_CURRENCY = 'price_delivery_return_currency';
    const TABLE_PRICE_COD_SERVICE_CURRENCY = 'price_cod_service_currency';
    const TABLE_PRICE_VAT_CURRENCY = 'price_vat_currency';

    const TABLE_PRICE_COD_CURRENCY_RATE = 'price_cod_currency_rate';
    const TABLE_PRICE_STORAGE_CURRENCY_RATE = 'price_storage_currency_rate';
    const TABLE_PRICE_FULFILMENT_CURRENCY_RATE = 'price_fulfilment_currency_rate';
    const TABLE_PRICE_PACKING_CURRENCY_RATE = 'price_packing_currency_rate';
    const TABLE_PRICE_PACKAGE_CURRENCY_RATE = 'price_package_currency_rate';
    const TABLE_PRICE_ADDRESS_CORRECTION_CURRENCY_RATE = 'price_address_correction_currency_rate';
    const TABLE_PRICE_DELIVERY_CURRENCY_RATE = 'price_delivery_currency_rate';
    const TABLE_PRICE_REDELIVERY_CURRENCY_RATE = 'price_redelivery_currency_rate';
    const TABLE_PRICE_DELIVERY_BACK_CURRENCY_RATE = 'price_delivery_back_currency_rate';
    const TABLE_PRICE_DELIVERY_RETURN_CURRENCY_RATE = 'price_delivery_return_currency_rate';
    const TABLE_PRICE_COD_SERVICE_CURRENCY_RATE = 'price_cod_service_currency_rate';
    const TABLE_PRICE_VAT_CURRENCY_RATE = 'price_vat_currency_rate';

    /**
     * @return array
     */
    public static function getCurrencyFields()
    {
        return [
            self::COLUMN_PRICE_COD => 'price_cod_currency_id',
            self::COLUMN_PRICE_STORAGE => 'price_storage_currency_id',
            self::COLUMN_PRICE_FULFILMENT => 'price_fulfilment_currency_id',
            self::COLUMN_PRICE_PACKING => 'price_packing_currency_id',
            self::COLUMN_PRICE_PACKAGE => 'price_package_currency_id',
            self::COLUMN_PRICE_ADDRESS_CORRECTION => 'price_address_correction_currency_id',
            self::COLUMN_PRICE_DELIVERY => 'price_delivery_currency_id',
            self::COLUMN_PRICE_REDELIVERY => 'price_redelivery_currency_id',
            self::COLUMN_PRICE_DELIVERY_BACK => 'price_delivery_back_currency_id',
            self::COLUMN_PRICE_DELIVERY_RETURN => 'price_delivery_return_currency_id',
            self::COLUMN_PRICE_COD_SERVICE => 'price_cod_service_currency_id',
            self::COLUMN_PRICE_VAT => 'price_vat_currency_id',
        ];
    }

    /**
     * @return array
     */
    public static function getFinanceTypeOfFields()
    {
        return [
            self::COLUMN_PRICE_COD => self::FINANCE_TYPE_INCOME,
            self::COLUMN_PRICE_STORAGE => self::FINANCE_TYPE_COST,
            self::COLUMN_PRICE_FULFILMENT => self::FINANCE_TYPE_COST,
            self::COLUMN_PRICE_PACKING => self::FINANCE_TYPE_COST,
            self::COLUMN_PRICE_PACKAGE => self::FINANCE_TYPE_COST,
            self::COLUMN_PRICE_ADDRESS_CORRECTION => self::FINANCE_TYPE_COST,
            self::COLUMN_PRICE_DELIVERY => self::FINANCE_TYPE_COST,
            self::COLUMN_PRICE_REDELIVERY => self::FINANCE_TYPE_COST,
            self::COLUMN_PRICE_DELIVERY_BACK => self::FINANCE_TYPE_COST,
            self::COLUMN_PRICE_DELIVERY_RETURN => self::FINANCE_TYPE_COST,
            self::COLUMN_PRICE_COD_SERVICE => self::FINANCE_TYPE_COST,
            self::COLUMN_PRICE_VAT => self::FINANCE_TYPE_COST,
        ];
    }

    /**
     * Связь между названиями таблиц с CurrencyRate и колонок с указанием ид валюты
     * @return array
     */
    public static function currencyRateRelationMap()
    {
        return [
            'price_cod_currency_id' => self::TABLE_PRICE_COD_CURRENCY_RATE,
            'price_storage_currency_id' => self::TABLE_PRICE_STORAGE_CURRENCY_RATE,
            'price_fulfilment_currency_id' => self::TABLE_PRICE_FULFILMENT_CURRENCY_RATE,
            'price_packing_currency_id' => self::TABLE_PRICE_PACKING_CURRENCY_RATE,
            'price_package_currency_id' => self::TABLE_PRICE_PACKAGE_CURRENCY_RATE,
            'price_address_correction_currency_id' => self::TABLE_PRICE_ADDRESS_CORRECTION_CURRENCY_RATE,
            'price_delivery_currency_id' => self::TABLE_PRICE_DELIVERY_CURRENCY_RATE,
            'price_redelivery_currency_id' => self::TABLE_PRICE_REDELIVERY_CURRENCY_RATE,
            'price_delivery_return_currency_id' => self::TABLE_PRICE_DELIVERY_RETURN_CURRENCY_RATE,
            'price_delivery_back_currency_id' => self::TABLE_PRICE_DELIVERY_BACK_CURRENCY_RATE,
            'price_cod_service_currency_id' => self::TABLE_PRICE_COD_SERVICE_CURRENCY_RATE,
            'price_vat_currency_id' => self::TABLE_PRICE_VAT_CURRENCY_RATE,
        ];
    }

    /**
     * @return array
     */
    public static function getPriceFields()
    {
        return [
            self::COLUMN_PRICE_COD,
            self::COLUMN_PRICE_STORAGE,
            self::COLUMN_PRICE_FULFILMENT,
            self::COLUMN_PRICE_PACKING,
            self::COLUMN_PRICE_PACKAGE,
            self::COLUMN_PRICE_ADDRESS_CORRECTION,
            self::COLUMN_PRICE_DELIVERY,
            self::COLUMN_PRICE_REDELIVERY,
            self::COLUMN_PRICE_DELIVERY_BACK,
            self::COLUMN_PRICE_DELIVERY_RETURN,
            self::COLUMN_PRICE_COD_SERVICE,
            self::COLUMN_PRICE_VAT,
        ];
    }

    /**
     * @param string $field
     * @return mixed|string
     */
    public static function getFinanceTypeOfField($field)
    {
        $types = self::getFinanceTypeOfFields();
        return $types[$field] ?? false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriceCodCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'price_cod_currency_id'])
            ->from([self::TABLE_PRICE_COD_CURRENCY => Currency::tableName()]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriceStorageCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'price_storage_currency_id'])
            ->from([self::TABLE_PRICE_STORAGE_CURRENCY => Currency::tableName()]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriceFulfilmentCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'price_fulfilment_currency_id'])
            ->from([self::TABLE_PRICE_FULFILMENT_CURRENCY => Currency::tableName()]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPricePackingCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'price_packing_currency_id'])
            ->from([self::TABLE_PRICE_PACKING_CURRENCY => Currency::tableName()]);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPricePackageCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'price_package_currency_id'])
            ->from([self::TABLE_PRICE_PACKAGE_CURRENCY => Currency::tableName()]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriceAddressCorrectionCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'price_address_correction_currency_id'])
            ->from([self::TABLE_PRICE_ADDRESS_CORRECTION_CURRENCY => Currency::tableName()]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriceDeliveryCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'price_delivery_currency_id'])
            ->from([self::TABLE_PRICE_DELIVERY_CURRENCY => Currency::tableName()]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriceRedeliveryCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'price_redelivery_currency_id'])
            ->from([self::TABLE_PRICE_REDELIVERY_CURRENCY => Currency::tableName()]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriceDeliveryReturnCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'price_delivery_return_currency_id'])
            ->from([self::TABLE_PRICE_DELIVERY_RETURN_CURRENCY => Currency::tableName()]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriceDeliveryBackCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'price_delivery_back_currency_id'])
            ->from([self::TABLE_PRICE_DELIVERY_BACK_CURRENCY => Currency::tableName()]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriceCodServiceCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'price_cod_service_currency_id'])
            ->from([self::TABLE_PRICE_COD_SERVICE_CURRENCY => Currency::tableName()]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriceVatCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'price_vat_currency_id'])
            ->from([self::TABLE_PRICE_VAT_CURRENCY => Currency::tableName()]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriceCodCurrencyRate()
    {
        return $this->hasOne(CurrencyRate::className(), ['currency_id' => 'price_cod_currency_id'])
            ->from([self::TABLE_PRICE_COD_CURRENCY_RATE => CurrencyRate::tableName()]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriceStorageCurrencyRate()
    {
        return $this->hasOne(CurrencyRate::className(), ['currency_id' => 'price_storage_currency_id'])
            ->from([self::TABLE_PRICE_STORAGE_CURRENCY_RATE => CurrencyRate::tableName()]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriceFulfilmentCurrencyRate()
    {
        return $this->hasOne(CurrencyRate::className(), ['currency_id' => 'price_fulfilment_currency_id'])
            ->from([self::TABLE_PRICE_FULFILMENT_CURRENCY_RATE => CurrencyRate::tableName()]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPricePackingCurrencyRate()
    {
        return $this->hasOne(CurrencyRate::className(), ['currency_id' => 'price_packing_currency_id'])
            ->from([self::TABLE_PRICE_PACKING_CURRENCY_RATE => CurrencyRate::tableName()]);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPricePackageCurrencyRate()
    {
        return $this->hasOne(CurrencyRate::className(), ['currency_id' => 'price_package_currency_id'])
            ->from([self::TABLE_PRICE_PACKAGE_CURRENCY_RATE => CurrencyRate::tableName()]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriceAddressCorrectionCurrencyRate()
    {
        return $this->hasOne(CurrencyRate::className(), ['currency_id' => 'price_address_correction_currency_id'])
            ->from([self::TABLE_PRICE_ADDRESS_CORRECTION_CURRENCY_RATE => CurrencyRate::tableName()]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriceDeliveryCurrencyRate()
    {
        return $this->hasOne(CurrencyRate::className(), ['currency_id' => 'price_delivery_currency_id'])
            ->from([self::TABLE_PRICE_DELIVERY_CURRENCY_RATE => CurrencyRate::tableName()]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriceRedeliveryCurrencyRate()
    {
        return $this->hasOne(CurrencyRate::className(), ['currency_id' => 'price_redelivery_currency_id'])
            ->from([self::TABLE_PRICE_REDELIVERY_CURRENCY_RATE => CurrencyRate::tableName()]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriceDeliveryReturnCurrencyRate()
    {
        return $this->hasOne(CurrencyRate::className(), ['currency_id' => 'price_delivery_return_currency_id'])
            ->from([self::TABLE_PRICE_DELIVERY_RETURN_CURRENCY_RATE => CurrencyRate::tableName()]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriceDeliveryBackCurrencyRate()
    {
        return $this->hasOne(CurrencyRate::className(), ['currency_id' => 'price_delivery_back_currency_id'])
            ->from([self::TABLE_PRICE_DELIVERY_BACK_CURRENCY_RATE => CurrencyRate::tableName()]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriceCodServiceCurrencyRate()
    {
        return $this->hasOne(CurrencyRate::className(), ['currency_id' => 'price_cod_service_currency_id'])
            ->from([self::TABLE_PRICE_COD_SERVICE_CURRENCY_RATE => CurrencyRate::tableName()]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriceVatCurrencyRate()
    {
        return $this->hasOne(CurrencyRate::className(), ['currency_id' => 'price_vat_currency_id'])
            ->from([self::TABLE_PRICE_VAT_CURRENCY_RATE => CurrencyRate::tableName()]);
    }
}