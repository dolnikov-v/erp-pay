<?php

namespace app\modules\order\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\Country;
use app\models\CurrencyRate;
use app\models\User;
use app\modules\order\models\query\MarketingListQuery;
use Yii;
use yii\db\Query;

/**
 * This is the model class for table "marketing_list".
 *
 * @property integer $id
 * @property integer $country_id
 * @property string $name
 * @property integer $user_id
 * @property integer $sent_at
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Country $country
 * @property User $user
 * @property MarketingListOrder[] $marketingListOrders
 * @property Order[] $orders
 */
class MarketingList extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%marketing_list}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'user_id'], 'required'],
            [['country_id', 'user_id', 'sent_at', 'created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 64],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'country_id' => 'Country ID',
            'name' => Yii::t('common', 'Название'),
            'user_id' => Yii::t('common', 'Пользователь'),
            'sent_at' => Yii::t('common', 'Дата отправки'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата обновления'),
            'use_days' => Yii::t('common', 'Срок использования')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMarketingListOrders()
    {
        return $this->hasMany(MarketingListOrder::className(), ['marketing_list_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOldOrders()
    {
        return $this->hasMany(Order::className(), ['id' => 'old_order_id'])->viaTable(MarketingListOrder::tableName(), ['marketing_list_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNewOrders()
    {
        return $this->hasMany(Order::className(), ['id' => 'new_order_id'])->viaTable(MarketingListOrder::tableName(), ['marketing_list_id' => 'id']);
    }

    /**
     * @return integer
     */
    public function getCountOldOrders()
    {
        return MarketingListOrder::find()->where(['marketing_list_id' => $this->id])->count();
    }

    /**
     * @return integer
     */
    public function getCountNewOrders()
    {
        $r = MarketingListOrder::find()->where(['marketing_list_id' => $this->id])->andWhere(['is not', 'new_order_id', null])->count();
        return $r;
    }

    /**
     * @return MarketingListQuery
     */
    public static function find()
    {
        return new MarketingListQuery(get_called_class());
    }

    /**
     * @return integer
     */
    public function getApprove()
    {
        $statuses = self::getMapStatuses();
        return MarketingListOrder::find()
            ->innerJoin(Order::tableName(), MarketingListOrder::tableName() . '.new_order_id = ' . Order::tableName() . '.id')
            ->where([MarketingListOrder::tableName() . '.marketing_list_id' => $this->id])
            ->andWhere([Order::tableName() . '.status_id' => $statuses[Yii::t('common', 'Подтверждено')]])
            ->count();
    }

    /**
     * @return integer
     */
    public function getBuyout()
    {
        $statuses = self::getMapStatuses();
        return MarketingListOrder::find()
            ->innerJoin(Order::tableName(), MarketingListOrder::tableName() . '.new_order_id = ' . Order::tableName() . '.id')
            ->where([MarketingListOrder::tableName() . '.marketing_list_id' => $this->id])
            ->andWhere([Order::tableName() . '.status_id' => $statuses[Yii::t('common', 'Выкуплено')]])
            ->count();
    }

    /**
     * @return integer
     */
    public function getProcess()
    {
        $statuses = self::getMapStatuses();
        return MarketingListOrder::find()
            ->innerJoin(Order::tableName(), MarketingListOrder::tableName() . '.new_order_id = ' . Order::tableName() . '.id')
            ->where([MarketingListOrder::tableName() . '.marketing_list_id' => $this->id])
            ->andWhere(['not in', Order::tableName() . '.status_id', $statuses[Yii::t('common', 'Не обработано')]])
            ->count();
    }

    /**
     * @return float
     */
    public function getAvgbuy() {

        $statuses = self::getMapStatuses();
        $toDollar = '/ ' . CurrencyRate::tableName() . '.rate';
        $query = new Query();
        $r = $query->select(['avgbuy' =>'AVG(' . Order::tableName() . '.price_total ' . $toDollar . ' + ' . Order::tableName() . '.delivery ' . $toDollar . ' )'])
            ->from(MarketingListOrder::tableName())
            ->join('inner join', Order::tableName(), MarketingListOrder::tableName() . '.new_order_id = ' . Order::tableName() . '.id')
            ->leftJoin(Country::tableName(), Order::tableName() . '.country_id = ' . Country::tableName() . '.id')
            ->leftJoin(CurrencyRate::tableName(), Country::tableName() . '.currency_id = ' . CurrencyRate::tableName() . '.currency_id')
            ->where([MarketingListOrder::tableName() . '.marketing_list_id' => $this->id])
            ->andWhere([Order::tableName() . '.status_id' => $statuses[Yii::t('common', 'Выкуплено')]])
            ->one();
        if (isset($r['avgbuy'])) return $r['avgbuy'];
        return 0;
    }


    /**
     * @return array
     */
    static public function getMapStatuses()
    {
        $mapStatuses = [
            Yii::t('common', 'Подтверждено') => [
                OrderStatus::STATUS_CC_APPROVED,
                OrderStatus::STATUS_LOG_ACCEPTED,
                OrderStatus::STATUS_LOG_GENERATED,
                OrderStatus::STATUS_LOG_SET,
                OrderStatus::STATUS_LOG_PASTED,
                OrderStatus::STATUS_DELIVERY_ACCEPTED,
                OrderStatus::STATUS_DELIVERY_LEFT_WAREHOUSE,
                OrderStatus::STATUS_DELIVERY,
                OrderStatus::STATUS_DELIVERY_BUYOUT,
                OrderStatus::STATUS_DELIVERY_DENIAL,
                OrderStatus::STATUS_DELIVERY_RETURNED,
                OrderStatus::STATUS_FINANCE_MONEY_RECEIVED,
                OrderStatus::STATUS_DELIVERY_REJECTED,
                OrderStatus::STATUS_DELIVERY_LEFT_OUR_WAREHOUSE,
                OrderStatus::STATUS_DELIVERY_REDELIVERY,
                OrderStatus::STATUS_DELIVERY_LEFT_THEIR_WAREHOUSE,
                OrderStatus::STATUS_LOGISTICS_REJECTED,
                OrderStatus::STATUS_DELIVERY_DELAYED,
                OrderStatus::STATUS_DELIVERY_REFUND,
                OrderStatus::STATUS_LOGISTICS_ACCEPTED,
                OrderStatus::STATUS_DELIVERY_LOST,
                OrderStatus::STATUS_DELIVERY_PENDING,
            ],
            Yii::t('common', 'Выкуплено') => [
                OrderStatus::STATUS_DELIVERY_BUYOUT,
                OrderStatus::STATUS_FINANCE_MONEY_RECEIVED,
            ],
            Yii::t('common', 'Не обработано') => [
                OrderStatus::STATUS_SOURCE_LONG_FORM,
                OrderStatus::STATUS_SOURCE_SHORT_FORM,
                OrderStatus::STATUS_CC_POST_CALL,
            ],
        ];
        return $mapStatuses;
    }

}
