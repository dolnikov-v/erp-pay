<?php

namespace app\modules\order\models;


use app\models\User;
use Yii;
use yii\mongodb\ActiveRecord;

/**
 * Class LeadLog
 * @package app\modules\order\models
 *
 * @property integer $order_id
 * @property integer $user_id
 * @property string $route
 * @property string $group_id
 * @property string $field
 * @property string $old
 * @property string $new
 * @property integer $created_at
 *
 * @property User $user
 */
class LeadLog extends ActiveRecord
{
    /**
     * @return array|string
     */
    public static function collectionName()
    {
        return 'lead_log';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return ['_id', 'order_id', 'user_id', 'route', 'group_id', 'field', 'old', 'new', 'created_at'];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['order_id', 'created_at', 'user_id'], 'integer'],
            [['route', 'group_id', 'field', 'old', 'new'], 'string']
        ];
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return User::find()->where(['id' => $this->user_id])->one();
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'order_id' => Yii::t('common', 'Заказ'),
            'group_id' => Yii::t('common', 'Группа'),
            'field' => Yii::t('common', 'Поле'),
            'old' => Yii::t('common', 'Старое значение'),
            'new' => Yii::t('common', 'Новое значение'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата обновления'),
            'user.username' => Yii::t('common', 'Имя пользователя'),
            'route' => Yii::t('common', 'Действие'),
            'comment' => Yii::t('common', 'Комментарий'),
        ];
    }
}