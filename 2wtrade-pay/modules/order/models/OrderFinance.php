<?php

namespace app\modules\order\models;

use app\components\db\ActiveRecordLogUpdateTime;
use Yii;

/**
 * Class OrderFinance
 *
 * @property integer $id
 * @property integer $order_id Номер заказа
 * @property string $payment Номер платежа
 * @property double $price_cod COD
 * @property double $price_storage Стоимость хранения
 * @property double $price_fulfilment Стоимость обслуживания заказа
 * @property double $price_packing Стоимость упаковывания
 * @property double $price_package Стоимость упаковки
 * @property double $price_address_correction Стоимость смены адреса
 * @property double $price_delivery Стоимость доставки
 * @property double $price_redelivery Стоимость передоставки
 * @property double $price_delivery_back Стоимость возвращения
 * @property double $price_delivery_return Стоимость возврата
 * @property double $price_cod_service Плата за наложенный платеж
 * @property double $price_vat НДС
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Order $order
 */
class OrderFinance extends ActiveRecordLogUpdateTime
{
    /**
     * @var string Выполняемый экшен, при изменение заказа
     */
    public $route;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_finance}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id'], 'required'],
            [['order_id', 'created_at', 'updated_at'], 'integer'],
            [
                [
                    'price_cod',
                    'price_storage',
                    'price_packing',
                    'price_package',
                    'price_address_correction',
                    'price_delivery',
                    'price_redelivery',
                    'price_delivery_back',
                    'price_delivery_return',
                    'price_cod_service',
                    'price_vat',
                    'price_fulfilment'
                ],
                'number'
            ],
            [['payment'], 'string', 'max' => 255],
            [
                ['order_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Order::className(),
                'targetAttribute' => ['order_id' => 'id']
            ],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'order_id' => Yii::t('common', 'Номер заказа'),
            'payment' => Yii::t('common', 'Номер платежа'),
            'price_cod' => Yii::t('common', 'COD'),
            'price_storage' => Yii::t('common', 'Стоимость хранения'),
            'price_packing' => Yii::t('common', 'Стоимость упаковывания'),
            'price_package' => Yii::t('common', 'Стоимость упаковки'),
            'price_address_correction' => Yii::t('common', 'Стоимость смены адреса'),
            'price_delivery' => Yii::t('common', 'Стоимость доставки'),
            'price_redelivery' => Yii::t('common', 'Стоимость передоставки'),
            'price_delivery_back' => Yii::t('common', 'Стоимость возвращения'),
            'price_delivery_return' => Yii::t('common', 'Стоимость возврата'),
            'price_cod_service' => Yii::t('common', 'Плата за наложенный платеж'),
            'price_vat' => Yii::t('common', 'НДС'),
            'price_fulfilment' => Yii::t('common', 'Стоимость обслуживания заказа')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @param boolean $insert
     * @return boolean
     */
    public function beforeSave($insert)
    {
        $groupId = uniqid();

        foreach ($this->getDirtyAttributes() as $key => $item) {
            if ($item != $this->getOldAttribute($key)) {
                $old = (string)$this->getOldAttribute($key);
                $log = new OrderLog();
                $log->order_id = $this->order_id;
                $log->group_id = $groupId;
                $log->field = self::tableName() . '.' . $key;
                $log->old = mb_substr($old, 0, 250);
                $log->new = mb_substr($item, 0, 250);
                $log->user_id = isset(Yii::$app->user) ? Yii::$app->user->id : null;
                $log->route = $this->route ?: isset(Yii::$app->controller) ? Yii::$app->controller->getRoute() : null;
                $log->save();
            }
        }

        return parent::beforeSave($insert);
    }
}
