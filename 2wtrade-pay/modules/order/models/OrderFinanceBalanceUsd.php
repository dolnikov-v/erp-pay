<?php

namespace app\modules\order\models;

use app\modules\delivery\models\DeliveryRequest;
use Yii;

/**
 * This is the model class for view "order_finance_balance_usd".
 *
 * @property integer $delivery_request_id
 * @property integer $order_id
 * @property double $price_cod
 * @property double $price_storage
 * @property double $price_fulfilment
 * @property double $price_packing
 * @property double $price_package
 * @property double $price_delivery
 * @property double $price_redelivery
 * @property double $price_delivery_back
 * @property double $price_delivery_return
 * @property double $price_address_correction
 * @property double $price_cod_service
 * @property double $price_vat
 * @property string $additional_prices
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property DeliveryRequest $deliveryRequest
 * @property Order $order
 */
class OrderFinanceBalanceUsd extends OrderFinanceAbstract
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%order_finance_balance_usd}}';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['order_id', 'delivery_request_id'], 'required'],
            [
                [
                    'order_id',
                    'delivery_request_id',
                    'created_at',
                    'updated_at',
                ],
                'integer'
            ],
            [
                [
                    'price_cod',
                    'price_storage',
                    'price_fulfilment',
                    'price_packing',
                    'price_package',
                    'price_delivery',
                    'price_redelivery',
                    'price_delivery_back',
                    'price_delivery_return',
                    'price_address_correction',
                    'price_cod_service',
                    'price_vat',
                ],
                'double'
            ],
            [['additional_prices'], 'string'],
            [
                ['delivery_request_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => DeliveryRequest::className(),
                'targetAttribute' => ['delivery_request_id' => 'id']
            ],
            [
                ['order_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Order::className(),
                'targetAttribute' => ['order_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'delivery_request_id' => Yii::t('common', 'Заявка в службе доставки'),
            'order_id' => Yii::t('common', 'Номер заказ'),
            'price_cod' => Yii::t('common', 'COD'),
            'price_storage' => Yii::t('common', 'Стоимость хранения'),
            'price_fulfilment' => Yii::t('common', 'Стоимость обслуживания заказа'),
            'price_packing' => Yii::t('common', 'Стоимость упаковывания'),
            'price_package' => Yii::t('common', 'Стоимость упаковки'),
            'price_delivery' => Yii::t('common', 'Стоимость доставки'),
            'price_redelivery' => Yii::t('common', 'Стоимость передоставки'),
            'price_delivery_back' => Yii::t('common', 'Стоимость возвращения'),
            'price_delivery_return' => Yii::t('common', 'Стоимость возврата'),
            'price_address_correction' => Yii::t('common', 'Стоимость корректировки адреса'),
            'price_cod_service' => Yii::t('common', 'Плата за наложенный платеж'),
            'price_vat' => Yii::t('common', 'НДС'),
            'additional_prices' => Yii::t('common', 'Дополнительные расходы'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата изменения')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveryRequest()
    {
        return $this->hasOne(DeliveryRequest::className(), ['id' => 'delivery_request_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }
}