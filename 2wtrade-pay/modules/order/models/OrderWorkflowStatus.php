<?php
namespace app\modules\order\models;

use app\modules\order\models\query\OrderWorkflowStatusQuery;
use Yii;
use yii\base\InvalidParamException;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * Class OrderWorkflowStatus
 * @property integer $id
 * @property integer $workflow_id
 * @property integer $status_id
 * @property string $parents
 * @property integer $created_at
 * @property integer $updated_at
 * @property OrderWorkflow $workflow
 * @property OrderStatus $status
 */
class OrderWorkflowStatus extends ActiveRecord
{
    const SCENARIO_IGNORE_PARENTS = 'ignore_parents';

    const TYPE_STARTING = 'starting';
    const TYPE_MIDDLE = 'middle';
    const TYPE_FINAL = 'final';

    /**
     * @var OrderStatus[]
     */
    private static $statuses = [];

    /**
     * @return array
     */
    public static function getTypesCollection()
    {
        return [
            self::TYPE_STARTING => '#9bbb59',
            self::TYPE_MIDDLE => '#f79646',
            self::TYPE_FINAL => '#c0504d',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_workflow_status}}';
    }

    /**
     * @return query\OrderWorkflowQuery
     */
    public static function find()
    {
        return new OrderWorkflowStatusQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['workflow_id', 'status_id'], 'required'],
            [
                'workflow_id',
                'exist',
                'targetClass' => '\app\modules\order\models\OrderWorkflow',
                'targetAttribute' => 'id',
                'message' => Yii::t('common', 'Неверное значение Workflow.'),
            ],
            [
                'status_id',
                'exist',
                'targetClass' => '\app\modules\order\models\OrderStatus',
                'targetAttribute' => 'id',
                'message' => Yii::t('common', 'Неверное значение статуса.'),
            ],
            ['parents', 'validateParents', 'skipOnEmpty' => false, 'except' => self::SCENARIO_IGNORE_PARENTS],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'workflow_id' => Yii::t('common', 'Workflow'),
            'status_id' => Yii::t('common', 'Статус'),
            'parents' => Yii::t('common', 'Статусы перехода'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkflow()
    {
        return $this->hasOne(OrderWorkflow::className(), ['id' => 'workflow_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(OrderStatus::className(), ['id' => 'status_id']);
    }

    /**
     * @return array
     */
    public function getParents()
    {
        return explode(',', $this->parents);
    }

    /**
     * @return OrderStatus[]
     */
    public function getParentsAsStatuses()
    {
        $collection = [];

        if (empty(self::$statuses)) {
            self::$statuses = OrderStatus::find()->all();
        }

        foreach ($this->getParents() as $parent) {
            foreach (self::$statuses as $status) {
                if ($status->id == $parent) {
                    $collection[] = $status;
                    break;
                }
            }
        }

        if ($collection) {
            usort($collection, function ($a, $b) {
                /** @var OrderStatus $a */
                /** @var OrderStatus $b */
                if ($a->type == $b->type) {
                    return 0;
                }

                return ($a->type < $b->type) ? 1 : -1;
            });
        }

        return $collection;
    }

    /**
     * @return OrderStatus[]|array
     */
    public function getAllChildren()
    {
        $children = $this->getParentsAsStatuses();
        $idList = [];
        foreach ($children as $child) {
            $idList[] = $child->id;
        }

        for ($i = 0; $i < count($children); $i++) {
            $buffer = $children[$i]->getParentsAsStatuses();
            foreach ($buffer as $buff) {
                if (!in_array($buff->id, $idList)) {
                    $children[] = $buff;
                    $idList[] = $buff->id;
                }
            }
        }

        return $children;
    }

    /**
     * @param integer $status_id
     * @return bool
     */
    public function checkStatusLinearity($status_id)
    {
        $children = $this->getAllChildren();

        $children = array_map(function ($model) {
            return $model->id;
        }, $children);

        return in_array($status_id, $children);
    }

    /**
     * @param string $attribute
     * @param array $params
     */
    public function validateParents($attribute, $params)
    {
        if ($this->status->type == OrderStatus::TYPE_FINAL) {
            if ($this->parents) {
                $this->addError($attribute, Yii::t('common', 'Конечный статус не может содержать статусы перехода.'));
            }
        } else {
            if (empty($this->parents)) {
                $this->addError($attribute, Yii::t('common', 'Необходимо указать статусы перехода.'));
            } else {
                $statuses = OrderStatus::find()
                    ->where(['in', 'id', $this->parents])
                    ->all();

                if (count($this->parents) != count($statuses)) {
                    $this->addError($attribute, Yii::t('common', 'Статусы перехода необходимо выбрать из списка.'));
                }
            }
        }
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        $this->parents = implode(',', $this->parents);

        return parent::beforeSave($insert);
    }

    /**
     * @param $id
     * @return mixed
     * @throws InvalidParamException
     */
    public static function createNewJsonScheme($id)
    {
        $result = [];
        $statuses = [];

        /** @var OrderWorkflowStatus[] $models */
        $models = OrderWorkflowStatus::find()
            ->where([OrderWorkflowStatus::tableName() . '.workflow_id' => $id])
            ->joinWith(['status'])
            ->all();

        foreach ($models as $model) {
            $parents = explode(',', $model->parents);
            foreach ($parents as $parent) {
                $statuses[] = $model->status_id;
                $statuses[] = $parent;
            }
        }

        $orderStatuses = OrderStatus::find()
            ->where(['IN', 'id', array_unique($statuses)])
            ->all();

        foreach ($orderStatuses as $status) {
            $result['nodeDataArray'][] = ["key" => $status->id, "color" => self::getTypesCollection()[$status->type]];
        }

        foreach ($models as $model) {
            $parents = explode(',', $model->parents);
            foreach ($parents as $parent) {
                $result['linkDataArray'][] = ["from" => $model->status_id, "to" => $parent];
            }
        }

        return json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    /**
     * @param $id
     * @return mixed
     */
    public static function updateOldJsonScheme($id)
    {
        $orderStatuses = ArrayHelper::map(OrderStatus::find()->all(), 'id', 'name');
        $newScheme = self::createNewJsonScheme($id);
        $oldScheme = OrderWorkflow::find()
            ->select('scheme')
            ->where(['id' => $id])
            ->asArray()
            ->one();

        if (!$oldScheme) {
            throw new InvalidParamException(Yii::t('common', 'Workflow с таким номером не найден.'));
        }

        $decodeNewScheme = json_decode($newScheme, true);
        $decodeOldScheme = json_decode($oldScheme['scheme'], true);
        if (!empty($decodeNewScheme['nodeDataArray']) && !empty($decodeNewScheme['linkDataArray'])) {
            foreach ($decodeNewScheme['nodeDataArray'] as $key => $new) {
                foreach ($decodeOldScheme['nodeDataArray'] as $old) {
                    if ($new['key'] == $old['key']) {
                        if (array_key_exists('loc', $old)) {
                            $decodeNewScheme['nodeDataArray'][$key]['loc'] = $old['loc'];
                        } else {
                            $decodeNewScheme['nodeDataArray'][$key]['loc'] = 1;
                        }
                    }
                }
                $keyStatus = $decodeNewScheme['nodeDataArray'][$key]['key'];
                $decodeNewScheme['nodeDataArray'][$key]['text'] = Yii::t('common', $orderStatuses[$keyStatus]);
            }

            foreach ($decodeNewScheme['linkDataArray'] as $key => $new) {
                foreach ($decodeOldScheme['linkDataArray'] as $old) {
                    if ($new['from'] == $old['from'] && $new['to'] == $old['to']) {
                        if (array_key_exists('points', $old)) {
                            $decodeNewScheme['linkDataArray'][$key]['points'] = $old['points'];
                        } else {
                            $decodeNewScheme['linkDataArray'][$key]['points'] = 1;
                        }
                    }
                }
            }
        } else {

        }

        return json_encode($decodeNewScheme, JSON_UNESCAPED_UNICODE);
    }
}
