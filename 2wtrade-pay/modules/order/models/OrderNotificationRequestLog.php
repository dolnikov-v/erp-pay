<?php

namespace app\modules\order\models;

use app\components\db\ActiveRecordLogCreateTime;
use app\models\User;
use Yii;

/**
 * OrderNotificationRequestLog
 *
 * @property integer $id
 * @property integer $order_notification_request_id
 * @property integer $user_id
 * @property string $route
 * @property string $group_id
 * @property string $field
 * @property string $old
 * @property string $new
 * @property integer $created_at
 *
 * @property User $user
 * @property OrderNotificationRequest $orderNotificationRequest
 */
class OrderNotificationRequestLog extends ActiveRecordLogCreateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_notification_request_log}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_notification_request_id'], 'required'],
            [['order_notification_request_id', 'user_id'], 'integer'],
            [['route', 'group_id', 'field', 'old', 'new'], 'string', 'max' => 255],
            [
                ['user_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => ['user_id' => 'id']
            ],
            [
                ['order_notification_request_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => OrderNotificationRequest::className(),
                'targetAttribute' => ['order_notification_request_id' => 'id']
            ],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'order_notification_request_id' => Yii::t('common', 'Заявка'),
            'group_id' => Yii::t('common', 'Группа'),
            'field' => Yii::t('common', 'Поле'),
            'old' => Yii::t('common', 'Старое значение'),
            'new' => Yii::t('common', 'Новое значение'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'user_id' => Yii::t('common', 'Пользователь'),
            'route' => Yii::t('common', 'Действие')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderNotificationRequest()
    {
        return $this->hasOne(OrderNotificationRequest::className(), ['id' => 'order_notification_request_id']);
    }
}
