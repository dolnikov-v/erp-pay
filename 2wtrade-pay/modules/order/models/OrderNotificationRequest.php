<?php

namespace app\modules\order\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\modules\delivery\models\Delivery;
use app\modules\order\jobs\OrderEmailNotificationJob;
use app\modules\order\jobs\OrderSmsNotificationJob;
use app\modules\smsnotification\components\GoogleApi;
use app\widgets\Label;
use Yii;
use yii\helpers\ArrayHelper;

/**
 *  OrderNotificationRequest
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $order_status_id
 * @property integer $delivery_id
 * @property integer $order_notification_id
 * @property string $email_to
 * @property string $email_status
 * @property string $sms_notification_text
 * @property string $email_notification_text
 * @property string $answer_notification_text
 * @property string $answer_at
 * @property string $status
 * @property string $code
 * @property string $phone_to
 * @property string $sms_status
 * @property string $sms_foreign_id
 * @property string $sms_foreign_status
 * @property string $sms_price
 * @property string $sms_api_error
 * @property integer $sms_api_code
 * @property integer $sms_date_created
 * @property integer $sms_date_updated
 * @property integer $sms_date_sent
 * @property string $sms_uri
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property OrderNotification $orderNotification
 * @property Order $order
 * @property Delivery $delivery
 * @property string|mixed $emailStatusLabel
 * @property mixed $labelStyleBySmsStatus
 * @property string|mixed $smsStatusLabel
 * @property mixed $labelStyleByEmailStatus
 * @property OrderStatus $orderStatus
 */
class OrderNotificationRequest extends ActiveRecordLogUpdateTime
{
    const EMAIL_STATUS_INACTIVE = 0;
    const EMAIL_STATUS_PENDING = 1;
    const EMAIL_STATUS_DONE = 2;
    const EMAIL_STATUS_ERROR = 3;

    const SMS_STATUS_INACTIVE = 'inactive';
    const SMS_STATUS_NEW = 'new';
    const SMS_STATUS_READY = 'ready_to_send';
    const SMS_STATUS_PENDING = 'pending';
    const SMS_STATUS_IN_PROGRESS = 'in_progress';
    const SMS_STATUS_DONE = 'done';
    const SMS_STATUS_ERROR = 'error';

    const SMS_FOREIGN_STATUS_ACCEPTED = 'accepted';
    const SMS_FOREIGN_STATUS_QUEUED = 'queued';
    const SMS_FOREIGN_STATUS_SENDING = 'sending';
    const SMS_FOREIGN_STATUS_SENT = 'sent';
    const SMS_FOREIGN_STATUS_RECEIVING = 'receiving';
    const SMS_FOREIGN_STATUS_RECEIVED = 'received';
    const SMS_FOREIGN_STATUS_DELIVERED = 'delivered';
    const SMS_FOREIGN_STATUS_UNDELIVERED = 'undelivered';
    const SMS_FOREIGN_STATUS_FAILED = 'failed';

    const ANSWER_TYPE_ALL = 0;
    const ANSWER_TYPE_NOT_ANSWERED = 1;
    const ANSWER_TYPE_ANSWERED = 2;
    const ANSWER_TYPE_READY_TO_SEND = 3;
    const ANSWER_TYPE_ERROR = 4;

    /**
     * @var string Выполняемый экшен, при изменение заявки
     */
    public $route;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_notification_request}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'order_notification_id'], 'required'],
            [
                [
                    'order_id',
                    'order_status_id',
                    'delivery_id',
                    'order_notification_id',
                    'sms_date_created',
                    'sms_date_updated',
                    'sms_date_sent',
                    'sms_api_code',
                    'email_status',
                ],
                'integer'
            ],
            [['sms_price'], 'number'],
            [['email_to', 'phone_to', 'sms_foreign_id', 'sms_foreign_status'], 'string', 'max' => 100],
            [['email_to'], 'email'],
            [['sms_status'], 'string'],
            [['sms_api_error'], 'string', 'max' => 255],
            [['sms_uri'], 'string', 'max' => 1000],
            [
                ['order_notification_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => OrderNotification::className(),
                'targetAttribute' => ['order_notification_id' => 'id']
            ],
            [
                ['order_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Order::className(),
                'targetAttribute' => ['order_id' => 'id']
            ],
            [
                ['order_status_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => OrderStatus::className(),
                'targetAttribute' => ['order_status_id' => 'id']
            ],
            [
                ['delivery_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Delivery::className(),
                'targetAttribute' => ['delivery_id' => 'id']
            ],
            ['sms_status', 'default', 'value' => self::SMS_STATUS_PENDING],
            [['created_at', 'updated_at'], 'safe'],

            [['sms_notification_text', 'email_notification_text', 'answer_text'], 'string', 'max' => 1000],
            [['code'], 'string', 'max' => 4],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'order_id' => Yii::t('common', 'Номер заказа'),
            'order_status_id' => Yii::t('common', 'Статус заказа'),
            'delivery_id' => Yii::t('common', 'Доставка'),
            'order_notification_id' => Yii::t('common', 'Уведомление'),
            'phone_to' => Yii::t('common', 'Телефон получателя'),
            'sms_status' => Yii::t('common', 'Статус SMS'),
            'sms_foreign_id' => Yii::t('common', 'Внешний номер SMS'),
            'sms_foreign_status' => Yii::t('common', 'Внешний статус SMS'),
            'sms_date_created' => Yii::t('common', 'Дата добавления (Служба SMS)'),
            'sms_date_updated' => Yii::t('common', 'Дата обновления (Служба SMS)'),
            'sms_date_sent' => Yii::t('common', 'Дата отправки (Служба SMS)'),
            'sms_price' => Yii::t('common', 'Цена SMS'),
            'sms_uri' => Yii::t('common', 'Uri SMS'),
            'email_to' => Yii::t('common', 'E-mail получателя'),
            'email_status' => Yii::t('common', 'Статус E-mail'),
            'code' => Yii::t('common', 'Код'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderNotification()
    {
        return $this->hasOne(OrderNotification::className(), ['id' => 'order_notification_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDelivery()
    {
        return $this->hasOne(Delivery::className(), ['id' => 'delivery_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderStatus()
    {
        return $this->hasOne(OrderStatus::className(), ['id' => 'order_status_id']);
    }

    /**
     * @param string $foreign_status
     * @return bool|string
     */
    public static function getSmsStatusByForeignStatus($foreign_status)
    {
        $statuses = self::getSmsStatusMap();

        if (isset($statuses[$foreign_status])) {
            return $statuses[$foreign_status];
        }

        return false;
    }

    /**
     * @return array
     */
    public static function getSmsStatusMap()
    {
        return [
            self::SMS_FOREIGN_STATUS_ACCEPTED => self::SMS_STATUS_IN_PROGRESS,
            self::SMS_FOREIGN_STATUS_DELIVERED => self::SMS_STATUS_DONE,
            self::SMS_FOREIGN_STATUS_FAILED => self::SMS_STATUS_ERROR,
            self::SMS_FOREIGN_STATUS_QUEUED => self::SMS_STATUS_IN_PROGRESS,
            self::SMS_FOREIGN_STATUS_RECEIVED => self::SMS_STATUS_DONE,
            self::SMS_FOREIGN_STATUS_RECEIVING => self::SMS_STATUS_IN_PROGRESS,
            self::SMS_FOREIGN_STATUS_SENDING => self::SMS_STATUS_IN_PROGRESS,
            self::SMS_FOREIGN_STATUS_SENT => self::SMS_STATUS_IN_PROGRESS,
            self::SMS_FOREIGN_STATUS_UNDELIVERED => self::SMS_STATUS_ERROR,
        ];
    }

    /**
     * @return mixed
     */
    public function getLabelStyleBySmsStatus()
    {
        $styles = [
            self::SMS_STATUS_INACTIVE => Label::STYLE_DARK,
            self::SMS_STATUS_PENDING => Label::STYLE_PRIMARY,
            self::SMS_STATUS_IN_PROGRESS => Label::STYLE_INFO,
            self::SMS_STATUS_DONE => Label::STYLE_DEFAULT,
            self::SMS_STATUS_ERROR => Label::STYLE_DANGER,
        ];

        return ArrayHelper::getValue($styles, $this->sms_status);
    }

    /**
     * @return mixed
     */
    public function getLabelStyleByEmailStatus()
    {
        $styles = [
            self::EMAIL_STATUS_INACTIVE => Label::STYLE_DARK,
            self::EMAIL_STATUS_PENDING => Label::STYLE_PRIMARY,
            self::EMAIL_STATUS_DONE => Label::STYLE_DEFAULT,
            self::EMAIL_STATUS_ERROR => Label::STYLE_DANGER,
        ];

        return ArrayHelper::getValue($styles, $this->email_status);
    }

    /**
     * @return mixed|string
     */
    public function getSmsStatusLabel()
    {
        $labels = self::getSmsStatusLabels();

        if (isset($labels[$this->sms_status])) {
            return $labels[$this->sms_status];
        }
        return $this->sms_status;
    }

    /**
     * @return mixed|string
     */
    public function getEmailStatusLabel()
    {
        $labels = self::getEmailStatusLabels();

        if (isset($labels[$this->email_status])) {
            return $labels[$this->email_status];
        }
        return $this->email_status;
    }

    /**
     * @return array
     */
    public static function getSmsStatusLabels()
    {
        return [
            self::SMS_STATUS_INACTIVE => Yii::t('common', 'Неактивно'),
            self::SMS_STATUS_PENDING => Yii::t('common', 'Ожидает'),
            self::SMS_STATUS_IN_PROGRESS => Yii::t('common', 'Выполняется'),
            self::SMS_STATUS_DONE => Yii::t('common', 'Завершена'),
            self::SMS_STATUS_ERROR => Yii::t('common', 'Ошибка'),
        ];
    }

    /**
     * @return array
     */
    public static function getEmailStatusLabels()
    {
        return [
            self::EMAIL_STATUS_INACTIVE => Yii::t('common', 'Неактивно'),
            self::EMAIL_STATUS_PENDING => Yii::t('common', 'Ожидает'),
            self::EMAIL_STATUS_DONE => Yii::t('common', 'Завершена'),
            self::EMAIL_STATUS_ERROR => Yii::t('common', 'Ошибка'),
        ];
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        $groupId = uniqid();
        $changedAttributeNames = array_keys($changedAttributes);

        foreach ($changedAttributes as $key => $item) {
            if ($this->getAttribute($key) != $item) {
                $log = new OrderNotificationRequestLog();
                $log->order_notification_request_id = $this->id;
                $log->group_id = $groupId;
                $log->field = $key;
                $log->old = (string)$item;
                $log->new = $this->getAttribute($key);
                $log->user_id = isset(Yii::$app->user) ? Yii::$app->user->id : null;
                $log->route = $this->route ?: isset(Yii::$app->controller) ? Yii::$app->controller->getRoute() : null;
                $log->save();
            }
        }

        if ((in_array('sms_status', $changedAttributeNames) || in_array('status', $changedAttributeNames)) && $this->sms_status == static::SMS_STATUS_PENDING && $this->status == static::SMS_STATUS_READY) {
            $this->queueTransport->push(new OrderSmsNotificationJob(['requestId' => $this->id]));
        }

        if ((in_array('email_status', $changedAttributeNames) || in_array('status', $changedAttributeNames)) && $this->email_status == static::EMAIL_STATUS_PENDING && $this->status == static::SMS_STATUS_READY) {
            $this->queueTransport->push(new OrderEmailNotificationJob(['requestId' => $this->id]));
        }
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @param Order $order
     */
    public static function createByOrder($order)
    {
        $notifications = OrderNotification::find()
            ->where([
                OrderNotification::tableName() . '.country_id' => $order->country_id,
                OrderNotification::tableName() . '.active' => 1,
            ])
            ->andWhere([
                'or',
                [OrderNotification::tableName() . '.sms_active' => 1],
                [OrderNotification::tableName() . '.email_active' => 1],
            ])
            ->all();

        if ($notifications) {
            foreach ($notifications as $notification) {
                /** @var OrderNotification $notification */

                $statuses = $notification->getOrderStatusesAsArray();
                $deliveries = $notification->getDeliveryAsArray();
                $deliveryId = ($order->deliveryRequest) ? $order->deliveryRequest->delivery_id : null;


                $checkSms = $notification->sms_active && $order->customer_phone;
                $checkEmail = $notification->email_active && $order->customer_email;
                $checkTransport = $checkSms || $checkEmail;
                $checkStatus = is_array($statuses) && in_array($order->status_id, $statuses);
                $checkDelivery = empty($deliveries) || (is_array($deliveries) && $deliveryId && in_array($deliveryId, $deliveries));

                $canCreate = $checkTransport && $checkStatus && $checkDelivery;
                if ($canCreate && $notification->is_just_one_time) {
                    $query = OrderNotificationRequest::find()
                        ->where([
                            OrderNotificationRequest::tableName() . '.order_id' => $order->id,
                            OrderNotificationRequest::tableName() . '.order_notification_id' => $notification->id,
                            OrderNotificationRequest::tableName() . '.order_status_id' => $order->status_id,
                        ]);

                    if ($deliveryId) {
                        $query->andWhere([OrderNotificationRequest::tableName() . '.delivery_id' => $deliveryId]);
                    }

                    $canCreate = !$query->exists();
                }
                if ($canCreate) {
                    //generate secure code
                    //код должен быть одинаковый, что для смс, что для мыла
                    //https://2wtrade-tasks.atlassian.net/browse/NPAY-1324

                    $rand = rand(1, 1000000);
                    $hash = sha1($rand);
                    $code = substr($hash, 5, 4);

                    $request = new OrderNotificationRequest();
                    $request->order_notification_id = $notification->id;
                    $request->order_status_id = $order->status_id;
                    $request->order_id = $order->id;
                    $request->delivery_id = $deliveryId;
                    $request->phone_to = $checkSms ? $order->customer_phone : null;
                    $request->email_to = $checkEmail ? $order->customer_email : null;
                    $request->sms_status = $checkSms ? OrderNotificationRequest::SMS_STATUS_PENDING : OrderNotificationRequest::SMS_STATUS_INACTIVE;
                    $request->email_status = $checkEmail ? OrderNotificationRequest::EMAIL_STATUS_PENDING : OrderNotificationRequest::EMAIL_STATUS_INACTIVE;
                    $request->code = $code;
                    $request->status = $notification->answerable == OrderNotification::IS_ANSWERABLE ? OrderNotificationRequest::SMS_STATUS_NEW : OrderNotificationRequest::SMS_STATUS_READY;
                    $request->save();
                }
            }
        }
    }

    /**
     * @return null|string
     * @throws \Exception
     */
    public function getFeedbackLink(): ?string
    {
        if ($issueCollector = $this->orderNotification->issueCollector) {
            return $issueCollector->form_link;
        }
        $responseUrl = GoogleApi::getShortUrl(['longUrl' => OrderNotification::RESPONSE_LINK . '/' . $this->id]);
        //Гугл апи вернул шортлинк
        if (isset($responseUrl['answer']->id)) {
            return $responseUrl['answer']->id . ' code: ' . $this->code;
        }
        throw new \Exception(Yii::t('common', 'Не удалось получить короткую ссылку: {error}', ['error' => $responseUrl['answer']->error->message]));
    }
}
