<?php

namespace app\modules\order\models;

use app\components\mongodb\ActiveRecord;
use app\modules\order\models\query\LeadCreateLogQuery;
use MongoDB\BSON\ObjectId;
use Yii;

/**
 * Class LeadCreateLog
 *
 * @property ObjectId $_id
 * @property integer $order_id
 * @property string $input_data
 * @property string $output_data
 */
class LeadCreateLog extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return 'lead_create_log';
    }

    /**
     * @return LeadCreateLogQuery|\yii\mongodb\ActiveQuery
     */
    public static function find()
    {
        return new LeadCreateLogQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['order_id'], 'required'],
            [['input_data', 'output_data'], 'string'],
            [['created_at'], 'safe'],
        ];
    }

    public function attributes()
    {
        return ['_id', 'order_id', 'input_data', 'output_data', 'created_at'];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'input_data' => Yii::t('common', 'Входящие данные'),
            'output_data' => Yii::t('common', 'Исходящие данные'),
            'created_at' => Yii::t('common', 'Время создания'),
            'order_id' => Yii::t('common', 'Заказ'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        if ($this->input_data && !is_string($this->input_data)) {
            $this->input_data = json_encode($this->input_data, JSON_UNESCAPED_UNICODE);
        }
        if ($this->output_data && !is_string($this->output_data)) {
            $this->output_data = json_encode($this->output_data, JSON_UNESCAPED_UNICODE);
        }
        return parent::beforeValidate();
    }

    /**
     * @return Order|null
     */
    public function getOrder()
    {
        return Order::findOne(['id' => $this->order_id]);
    }
}