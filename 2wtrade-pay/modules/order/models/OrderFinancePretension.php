<?php

namespace app\modules\order\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\modules\deliveryreport\models\DeliveryReport;
use app\modules\deliveryreport\models\DeliveryReportRecord;
use app\modules\logger\components\log\Logger;
use Yii;
use yii\db\ActiveQuery;
use yii\helpers\Console;
use yii\helpers\Json;

/**
 * This is the model class for table "order_finance_pretension".
 *
 * @property integer $id
 * @property integer $type
 * @property string $status
 * @property string $comment
 * @property integer $order_id
 * @property integer $delivery_report_record_id
 * @property integer $delivery_report_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property DeliveryReport $deliveryReport
 * @property DeliveryReportRecord $deliveryReportRecord
 * @property Order $order
 */
class OrderFinancePretension extends ActiveRecordLogUpdateTime
{
    const PRETENSION_TYPE_PRICE = 1;
    const PRETENSION_TYPE_STATUS = 2;
    const PRETENSION_TYPE_DOUBLE = 3;
    const PRETENSION_TYPE_DIFF_TARIFF = 4;
    const PRETENSION_TYPE_FALSE_STATUS = 5;
    const PRETENSION_TYPE_STILL_WAITING = 6;

    const PRETENSION_STATUS_IN_PROGRESS = 'in_progress ';
    const PRETENSION_STATUS_SEND = 'send';
    const PRETENSION_STATUS_RESOLVED = 'resolved';
    const PRETENSION_STATUS_CLOSED = 'closed ';

    const STATUS_BUYOUT = 'buyout';
    const STATUS_REJECT = 'reject';

    const BATCH_SIZE = 500;

    const COLUMN_DISABLED = 'is_delete';
    const COLUMN_ORDER_ID = 'order_id';

    /** @var array для мониторинга дублей из под консоли */
    public static $doubles = [];
    /** @var array для мониторинга кол-ва записей с претензиями из под консоли */
    public static $recordsWithPretension = [];
    /** @var array для мониторинга кол-ва претензий по типу */
    public static $list_pretensions = [
        self::PRETENSION_TYPE_PRICE => [],
        self::PRETENSION_TYPE_STATUS => [],
        self::PRETENSION_TYPE_DOUBLE => [],
        self::PRETENSION_TYPE_DIFF_TARIFF => [],
    ];

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%order_finance_pretension}}';
    }

    public static function clearStatic()
    {
        self::$doubles = [];
        self::$recordsWithPretension = [];
        self::$list_pretensions = [];
    }

    /**
     * @return array
     */
    public static function getRecordRejectStatuses()
    {
        return [
            OrderStatus::STATUS_DELIVERY_RETURNED,
            OrderStatus::STATUS_DELIVERY_REJECTED,
            OrderStatus::STATUS_DELIVERY_LOST
        ];
    }

    /**
     * @return array
     */
    public static function getRecordBuyOutStatuses()
    {
        return [
            OrderStatus::STATUS_FINANCE_MONEY_RECEIVED,
            OrderStatus::STATUS_DELIVERY_BUYOUT
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [
                [
                    'type',
                    'order_id',
                ], 'required'
            ],
            [
                [
                    'id',
                    'type',
                    'order_id',
                    'delivery_report_record_id',
                    'delivery_report_id',
                    'created_at',
                    'updated_at',
                    'user_id'
                ], 'integer'
            ],
            ['type', 'in', 'range' => array_keys(self::getTypeCollecion())],
            ['status', 'in', 'range' => array_keys(self::getStatusCollection())]
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'type' => yii::t('common', 'Тип'),
            'order_id' => yii::t('common', 'Заказ'),
            'delivery_report_record_id' => yii::t('common', 'Запись'),
            'delivery_report_id' => yii::t('common', 'Отчет'),
            'id' => yii::t('common', 'Номер'),
            'created_at' => yii::t('common', 'Создана'),
            'updated_at' => yii::t('common', 'Обновлена'),
            'user_id' => yii::t('common', 'Пользователь'),
            'status' => yii::t('common', 'Статус'),
        ];
    }

    /**
     * @return array
     */
    public static function getTypeCollecion()
    {
        return [
            self::PRETENSION_TYPE_PRICE => yii::t('common', 'Разница в ценах'),
            self::PRETENSION_TYPE_STATUS => yii::t('common', 'Смена статуса с Выкуп на Возврат'),
            self::PRETENSION_TYPE_DOUBLE => yii::t('common', 'Дублирование'),
            self::PRETENSION_TYPE_DIFF_TARIFF => yii::t('common', 'Расхождение в тарифах'),
            self::PRETENSION_TYPE_FALSE_STATUS => yii::t('common', 'Ложный статус'),
            self::PRETENSION_TYPE_STILL_WAITING => yii::t('common', 'Клиент все еще ждет заказ')
        ];
    }

    /**
     * @return array
     */
    public static function getStatusCollection()
    {
        return [
            self::PRETENSION_STATUS_IN_PROGRESS,
            self::PRETENSION_STATUS_SEND,
            self::PRETENSION_STATUS_RESOLVED,
            self::PRETENSION_STATUS_CLOSED
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getDeliveryReport()
    {
        return $this->hasOne(DeliveryReport::className(), ['id' => 'delivery_report_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getDeliveryReportRecord()
    {
        return $this->hasOne(DeliveryReportRecord::className(), ['id' => 'delivery_report_record_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * Проверка и создание претензий по отчету
     * @param DeliveryReport $deliveryReport
     * @param bool $console
     * @return array
     */
    public static function generatePretensionsByReport(DeliveryReport $deliveryReport, $console = false)
    {
        self::clearStatic();

        $result = [
            'success' => false,
            'message' => null
        ];

        if (!$deliveryReport instanceof DeliveryReport) {
            $result['message'] = yii::t('common', 'Ожидается объект отчета');
        } elseif ($deliveryReport->type != DeliveryReport::TYPE_FINANCIAL) {
            $result['message'] = yii::t('common', 'Претензия может быть создана только для записи финансового отчета');
        }

        foreach ($deliveryReport->getRecords()->batch(self::BATCH_SIZE) as $records) {
            foreach ($records as $record) {
                $pretensions = self::checkForPretension($record);

                foreach ($pretensions as $type => $pretension) {
                    if (!self::checkRecordWithPretension($record, $type) && $pretension['status'] === true) {
                        $saved = self::generatePretensionByRecord($record, $type, $console, $pretension['details']);

                        if (!$saved['success']) {
                            //логировать
                        } else {
                            if (!isset(self::$recordsWithPretension[$deliveryReport->id])) {
                                self::$recordsWithPretension[$deliveryReport->id] = [];
                            }
                            self::$recordsWithPretension[$deliveryReport->id][] = $record->id;

                            if (!isset(self::$list_pretensions[$deliveryReport->id])) {
                                self::$list_pretensions[$deliveryReport->id] = [];
                            }
                            self::$list_pretensions[$deliveryReport->id][$type][] = $record->id;
                        }

                    }
                }
            }
        }

        if ($console) {
            $pretensionCollection = OrderFinancePretension::getTypeCollecion();

            Console::output("Records count: " . count($deliveryReport->records) . " with pretensions " . count(self::$recordsWithPretension[$deliveryReport->id] ?? []));
            Console::output("Count pretensions by type: ");

            $count_by_type = "";

            foreach (self::$list_pretensions[$deliveryReport->id] ?? [] as $type => $pretension) {
                $count_by_type .= $type . " - " . $pretensionCollection[$type] . ": " . count($pretension) . PHP_EOL;
            }

            Console::output($count_by_type);

        }

        if ($result['message']) {
            return $result;
        }

        $result['success'] = true;
        $result['message'] = '';

        return $result;
    }

    /**
     * Создание претензий на запись отчета
     * @param DeliveryReportRecord $deliveryReportRecord
     * @param integer $type
     * @param  bool $console
     * @return array
     */
    public static function generatePretensionByRecord(DeliveryReportRecord $deliveryReportRecord, $type, $console = false, $details = '')
    {
        if ($console && is_string($details)) {
            $details = $details . PHP_EOL;
        }

        $pretensionCollection = OrderFinancePretension::getTypeCollecion();

        $preparedPretension = new OrderFinancePretension([
            'type' => $type,
            'status' => self::PRETENSION_STATUS_IN_PROGRESS,
            'comment' => null,
            'order_id' => $deliveryReportRecord->relatedOrder->id,
            'delivery_report_record_id' => $deliveryReportRecord->id,
            'delivery_report_id' => $deliveryReportRecord->report->id,
            'user_id' => null,
            'created_at' => time(),
            'updated_at' => time(),
        ]);

        if (!$preparedPretension->save()) {
            $error = $preparedPretension->getFirstErrorAsString();

            if ($console) {
                Console::error("Error " . $error . " save pretension #" . $type . "-" . $pretensionCollection[$type] . " to Record #" . $deliveryReportRecord->id . " order_id #" . $deliveryReportRecord->order_id);

                if ($type != OrderFinancePretension::PRETENSION_TYPE_DOUBLE) {
                    Console::output("Details: " . self::$doubles[$deliveryReportRecord->delivery_report_id][$deliveryReportRecord->id] . PHP_EOL);
                } else {
                    Console::output("Details: " . $details);
                }
            }

            return [
                'success' => false,
                'record' => $deliveryReportRecord,
                'message' => $error
            ];
        } else {
            if ($console) {
                Console::output("Record #" . $deliveryReportRecord->id . " order_id #" . $deliveryReportRecord->order_id . " have pretension: #" . $type . "-" . $pretensionCollection[$type]);

                if ($type == OrderFinancePretension::PRETENSION_TYPE_DOUBLE) {
                    Console::output("Details: ");
                    echo '<pre>' . print_r(self::$doubles[$deliveryReportRecord->delivery_report_id][$deliveryReportRecord->id], 1) . '</pre>';
                } else {
                    Console::output("Details: " . $details);
                }
            }
        }


        return [
            'success' => true,
            'message' => $preparedPretension
        ];
    }

    /**
     * Проверить соответствие ко всем видам претензий запись отчета
     * @param DeliveryReportRecord $deliveryReportRecord
     * @return array
     */
    public static function checkForPretension(DeliveryReportRecord $deliveryReportRecord)
    {
        $mapReportStatuses = $deliveryReportRecord->report->getStatuses();
        $status = $mapReportStatuses[$deliveryReportRecord->status] ?? false;

        $type = [];

        //в бд бардак, ордера может и не быть
        $financePrediction = $deliveryReportRecord->relatedOrder ? $deliveryReportRecord->relatedOrder->financePrediction : false;

        if (!$financePrediction) {
            return $type;
        } else {
            //не совпадают цены
            if (self::checkCod($financePrediction, $deliveryReportRecord)) {
                $type[self::PRETENSION_TYPE_PRICE] = [
                    'status' => true,
                    'details' => "financePrediction->price_cod: " . $financePrediction->price_cod . " > deliveryReportRecord->price_cod: " . $deliveryReportRecord->price_cod
                ];
            } else {
                $type[self::PRETENSION_TYPE_PRICE] = [
                    'status' => false
                ];
            }

            //в бд выкуп, в отчете отказ
            if (self::checkStatus($deliveryReportRecord, $status)) {
                $type[OrderFinancePretension::PRETENSION_TYPE_STATUS] = [
                    'status' => true,
                    'details' => "deliveryReportRecord->relatedOrder->status_id: " . $deliveryReportRecord->relatedOrder->status_id . ", deliveryReportRecord status: " . $status
                ];
            } else {
                $type[OrderFinancePretension::PRETENSION_TYPE_STATUS] = [
                    'status' => false
                ];
            }

            //по пути пропишем status_id у записи
            self::setStatusId($deliveryReportRecord, $status);

            //дубли - нас интересуют только отказы
            if (self::checkIsDouble($deliveryReportRecord, $status)) {
                $type[OrderFinancePretension::PRETENSION_TYPE_DOUBLE] = [
                    'status' => true,
                    'details' => self::$doubles[$deliveryReportRecord->delivery_report_id][$deliveryReportRecord->id]
                ];
            } else {
                $type[OrderFinancePretension::PRETENSION_TYPE_DOUBLE] = [
                    'status' => false
                ];
            }

            //различные тарифы
            $type[OrderFinancePretension::PRETENSION_TYPE_DIFF_TARIFF] = self::checkDiffTariff($financePrediction, $deliveryReportRecord);
        }

        return $type;
    }

    /**
     * @param DeliveryReportRecord $deliveryReportRecord
     * @param integer $status
     */
    public static function setStatusId($deliveryReportRecord, $status)
    {
        if (!$deliveryReportRecord->status_id) {
            $deliveryReportRecord->status_id = $status;
            //по пути пропишем status_id у записи
            if (!$deliveryReportRecord->save()) {
                /** @var Logger $logger */
                $logger = Yii::$app->get("processingLogger");
                $cronLog = $logger->getLogger([
                    'route' => __CLASS__,
                    'process_id' => getmypid(),
                ]);

                $cronLog("Error: " . $deliveryReportRecord->getFirstErrorAsString(), [
                    'action' => basename(__METHOD__),
                    'response' => 'delivery_report_record_id #' . $deliveryReportRecord->id . ' set status_id = ' . $status
                ]);

            }
        }
    }

    /**
     * Проверка COD
     * @param OrderFinancePrediction $financePrediction
     * @param DeliveryReportRecord $deliveryReportRecord
     * @return bool
     */
    public static function checkCod($financePrediction, $deliveryReportRecord)
    {
        return round($financePrediction->price_cod, 2) > round($deliveryReportRecord->price_cod, 2);
    }

    /**
     * Если в отчете отказ, а на самом деле выкуп
     * @param DeliveryReportRecord $deliveryReportRecord
     * @param $status
     * @return bool
     */
    public static function checkStatus($deliveryReportRecord, $status)
    {
        $lastChangeStatusToBuyOut = $deliveryReportRecord
            ->relatedOrder
            ->getOrderLogStatuses()
            ->where(['new' => OrderFinancePretension::getRecordBuyOutStatuses()])
            ->orderBy('id')->one();

        /** @var OrderLogStatus $lastChangeStatusToBuyOut */

        //рассматриваем одну КС
        if($deliveryReportRecord->relatedOrder->deliveryRequest->delivery_id == $deliveryReportRecord->report->delivery_id &&
           //статус выкупа у заказа должен быть получен не позже даты загрузки отчета
           $deliveryReportRecord->report->created_at >  ($lastChangeStatusToBuyOut ? $lastChangeStatusToBuyOut->created_at : 0)
        ) {
            return in_array($deliveryReportRecord->relatedOrder->status_id, self::getRecordBuyOutStatuses()) && in_array($status, self::getRecordRejectStatuses());
        }else{
            return false;
        }
    }

    /**
     * @param DeliveryReportRecord $deliveryReportRecord
     * @param integer $status
     * @return bool
     */
    public static function checkIsDouble($deliveryReportRecord, $status)
    {
        return !in_array($status, self::getRecordBuyOutStatuses()) && self::isDouble($deliveryReportRecord);
    }

    /**
     * @param OrderFinancePrediction $financePrediction
     * @param DeliveryReportRecord $deliveryReportRecord
     * @return array
     */
    public static function checkDiffTariff($financePrediction, $deliveryReportRecord)
    {
        $result = [
            'status' => false
        ];

        $fpStoragePrice = round($financePrediction->price_storage, 2);
        $fpPriceFulfilment = round($financePrediction->price_fulfilment, 2);
        $fpPricePacking = round($financePrediction->price_packing, 2);
        $fpPricePackage = round($financePrediction->price_package, 2);
        $fpPriceDelivery = round($financePrediction->price_delivery, 2);
        $fpPriceReDelivery = round($financePrediction->price_redelivery, 2);
        $fpPriceDeliveryBack = round($financePrediction->price_delivery_back, 2);
        $fpPriceDeliveryReturn = round($financePrediction->price_delivery_return, 2);
        $fpPriceCodService = round($financePrediction->price_cod_service, 2);
        $fpPriceVat = round($financePrediction->price_vat, 2);

        $drStoragePrice = round($deliveryReportRecord->price_storage, 2);
        $drPriceFulfilment = round($deliveryReportRecord->price_fulfilment, 2);
        $drPricePacking = round($deliveryReportRecord->price_packing, 2);
        $drPricePackage = round($deliveryReportRecord->price_package, 2);
        $drPriceDelivery = round($deliveryReportRecord->price_delivery, 2);
        $drPriceReDelivery = round($deliveryReportRecord->price_redelivery, 2);
        $drPriceDeliveryBack = round($deliveryReportRecord->price_delivery_back, 2);
        $drPriceDeliveryReturn = round($deliveryReportRecord->price_delivery_return, 2);
        $drPriceCodService = round($deliveryReportRecord->price_cod_service, 2);
        $drPriceVat = round($deliveryReportRecord->price_vat, 2);

        //различные тарифы
        if ($drStoragePrice > $fpStoragePrice  ||
            $drPriceFulfilment > $fpPriceFulfilment  ||
            $drPricePacking > $fpPricePacking ||
            $drPricePackage > $fpPricePackage ||
            $drPriceDelivery > $fpPriceDelivery ||
            $drPriceReDelivery > $fpPriceReDelivery ||
            $drPriceDeliveryBack > $fpPriceDeliveryBack ||
            $drPriceDeliveryReturn > $fpPriceDeliveryReturn ||
            $drPriceCodService > $fpPriceCodService ||
            $drPriceVat > $fpPriceVat
        ) {
            $details = "";

            if ($drStoragePrice > $fpStoragePrice) {
                $details .= "financePrediction->price_storage: " . $fpStoragePrice . " < deliveryReportRecord->price_storage: " . $drStoragePrice . PHP_EOL;
            }

            if ($drPriceFulfilment > $fpPriceFulfilment) {
                $details .= "financePrediction->price_fulfilment: " . $fpPriceFulfilment . " < deliveryReportRecord->price_fulfilment: " . $drPriceFulfilment . PHP_EOL;
            }

            if ($drPricePacking > $fpPricePacking) {
                $details .= "financePrediction->price_packing: " . $fpPricePacking . " < deliveryReportRecord->price_packing: " . $drPricePacking . PHP_EOL;
            }

            if ($drPricePackage > $fpPricePackage) {
                $details .= "financePrediction->price_package: " . $fpPricePackage . " < deliveryReportRecord->price_package: " . $drPricePackage . PHP_EOL;
            }

            if ($drPriceDelivery > $fpPriceDelivery) {
                $details .= "financePrediction->price_delivery: " . $fpPriceDelivery . " < deliveryReportRecord->price_delivery: " . $drPriceDelivery . PHP_EOL;
            }

            if ($drPriceReDelivery > $fpPriceReDelivery) {
                $details .= "financePrediction->price_redelivery: " . $fpPriceReDelivery . " < deliveryReportRecord->price_redelivery: " . $drPriceReDelivery . PHP_EOL;
            }

            if ($drPriceDeliveryBack > $fpPriceDeliveryBack) {
                $details .= "financePrediction->price_delivery_back: " . $fpPriceDeliveryBack . " < deliveryReportRecord->price_delivery_back: " . $drPriceDeliveryBack . PHP_EOL;
            }

            if ($drPriceDeliveryReturn > $fpPriceDeliveryReturn) {
                $details .= "financePrediction->price_delivery_return: " . $fpPriceDeliveryReturn . " < deliveryReportRecord->price_delivery_return: " . $drPriceDeliveryReturn . PHP_EOL;
            }

            if ($drPriceCodService > $fpPriceCodService) {
                $details .= "financePrediction->price_cod_service: " . $fpPriceCodService . " < deliveryReportRecord->price_cod_service: " . $drPriceCodService . PHP_EOL;
            }

            if ($drPriceVat > $fpPriceVat) {
                $details .= "financePrediction->price_vat: " . $fpPriceVat . " < deliveryReportRecord->price_vat: " . $drPriceVat . PHP_EOL;
            }

            $result = [
                'status' => true,
                'details' => $details
            ];
        }

        return $result;
    }

    /**
     * Проверка на дубль записи отчета
     * @param DeliveryReportRecord $deliveryReportRecord
     * @return bool
     */
    public static function isDouble(DeliveryReportRecord $deliveryReportRecord)
    {
        $mapReportStatuses = $deliveryReportRecord->report->getStatuses();
        $status = $mapReportStatuses[$deliveryReportRecord->status] ?? false;
        $is_double = false;

        //если статус не получилось смапить - выходим
        //нас интересуют только отказы
        if (!$status || in_array($status, OrderStatus::getBuyoutList())) {
            return false;
        }

        $doubles = [];

        foreach ($deliveryReportRecord->report->getRecords()->batch(self::BATCH_SIZE) as $records) {
            foreach ($records as $record) {
                if ($record->order_id == $deliveryReportRecord->order_id) {
                    $mappedStatus = $mapReportStatuses[$record->status] ?? false;
                    if ($mappedStatus) {
                        if (in_array($mappedStatus, self::getRecordBuyOutStatuses())) {
                            $doubles[self::STATUS_BUYOUT][] = $record->id;
                        } elseif (in_array($mappedStatus, self::getRecordRejectStatuses())) {
                            $doubles[self::STATUS_REJECT][] = $record->id;
                        }
                    }
                }
            }
        }

        //если есть выкупы по этому order_id в данном отчете - то все отказы автоматом дубль
        if (isset($doubles[self::STATUS_BUYOUT])) {
            $is_double = true;
        } else {

            if (isset($doubles[self::STATUS_REJECT])) {
                //если это единственный отказ по данному заказу - он не может быть дублем в рамках данного отчета
                if (count($doubles[self::STATUS_REJECT]) == 1) {
                    $is_double = false;
                } else {
                    //если отказов несколько - то все, кроме первого, являются дублями
                    foreach ($doubles as $k => $record_id) {
                        if ($k > 0 && $record_id == $deliveryReportRecord->id) {
                            $is_double = true;
                        }
                    }
                }
            }
        }

        if ($is_double) {
            //для мониторинга в консоле
            if (!isset(self::$doubles[$deliveryReportRecord->delivery_report_id])) {
                self::$doubles[$deliveryReportRecord->delivery_report_id] = [];
            }

            self::$doubles[$deliveryReportRecord->delivery_report_id][$deliveryReportRecord->id] = $doubles;
        }

        return $is_double;
    }

    /**
     * Проверка наличия конкретной пртензии у записи
     * @param $delivery_report_record_id
     * @param integer $type
     * @return integer bool
     */
    public static function checkRecordWithPretension($delivery_report_record_id, $type)
    {
        return OrderFinancePretension::find()
            ->where(['delivery_report_record_id' => $delivery_report_record_id, 'type' => $type])
            ->exists();
    }

    /**
     * Пометка "удалена" для всех претензий указанного заказа
     *
     * ERP-706
     *
     * @param $order_id integer OrderID
     */
    public static function disableByOrder($order_id)
    {
        if (!is_int($order_id)) {
            return false;
        }
        return self::updateAll([self::COLUMN_DISABLED => 1], self::COLUMN_ORDER_ID . '=:order_id', [':order_id' => $order_id]);
    }
}