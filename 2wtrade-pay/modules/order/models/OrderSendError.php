<?php

namespace app\modules\order\models;

use app\components\ModelTrait;
use Yii;
use yii\db\ActiveRecord;

/**
 * Class OrderSendError
 * @package app\modules\order\models
 *
 * @property string $type
 * @property integer $order_id
 * @property string $error
 * @property string $response
 * @property integer $cron_launched_at
 *
 * @property Order $order
 */
class OrderSendError extends ActiveRecord
{
    use ModelTrait;

    const TYPE_DELIVERY_SEND = 'delivery_send';
    const TYPE_CALL_CENTER_SEND = 'call_center_send';

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%order_send_error}}';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['order_id', 'type', 'response'], 'required'],
            [['order_id', 'cron_launched_at'], 'integer'],
            ['type', 'string', 'max' => 100],
            [['error', 'response'], 'string'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'type' => Yii::t('common', 'Тип'),
            'order_id' => Yii::t('common', 'Номер заказа'),
            'error' => Yii::t('common', 'Ошибка'),
            'response' => Yii::t('common', 'Ответ'),
            'cron_launched_at' => Yii::t('common', 'Дата последней проверки')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }
}