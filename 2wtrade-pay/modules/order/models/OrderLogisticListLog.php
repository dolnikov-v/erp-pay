<?php

namespace app\modules\order\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\models\User;
use Yii;

/**
 * This is the model class for table "order_logistic_list_log".
 *
 * @property integer $id
 * @property integer $list_id
 * @property integer $user_id
 * @property string $group_id
 * @property string $field
 * @property string $old
 * @property string $new
 * @property integer $created_at
 *
 * @property OrderLogisticList $list
 * @property User $user
 */
class OrderLogisticListLog extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_logistic_list_log}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecordLogUpdateTime::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['list_id'], 'required'],
            [['list_id', 'user_id', 'created_at'], 'integer'],
            [['group_id', 'field'], 'string', 'max' => 255],
            [['old', 'new'], 'string'],
            [['list_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrderLogisticList::className(), 'targetAttribute' => ['list_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'list_id' => Yii::t('common', 'Лист'),
            'group_id' => Yii::t('common', 'Группа'),
            'field' => Yii::t('common', 'Поле'),
            'old' => Yii::t('common', 'Старое значение'),
            'new' => Yii::t('common', 'Новое значение'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'user_id' => Yii::t('common', 'Пользователь'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getList()
    {
        return $this->hasOne(OrderLogisticList::className(), ['id' => 'list_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
