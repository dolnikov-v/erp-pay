<?php
namespace  app\modules\order\models;

use app\components\db\ActiveRecordLogUpdateTime;
use app\modules\order\models\query\SmsPollQuestionsQuery;
use app\models\Country;
use Yii;

/**
 * Class SmsPollQuestions
 * @package app\models
 * @property integer $id
 * @property integer $country_id
 * @property string $question_text
 * @property bool $is_active
 * @property integer $created_at
 * @property integer $updated_at
 */
class SmsPollQuestions extends ActiveRecordLogUpdateTime
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%sms_poll_questions}}';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['question_text', 'country_id'], 'required'],
            ['country_id', 'integer'],
            ['question_text', 'string', 'max' => 500]

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'question_text' => Yii::t('common', 'Текст вопроса'),
            'is_active' => Yii::t('common', 'Активен'),
            'country_id' => Yii::t('common', 'Страна'),
            'created_at' => Yii::t('common', 'Создан'),
            'updated_at' => Yii::t('common', 'Обновлен')

        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return SmsPollQuestionsQuery
     */
    public static function find()
    {
        return new SmsPollQuestionsQuery(get_called_class());
    }
}