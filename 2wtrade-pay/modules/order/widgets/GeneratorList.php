<?php
namespace app\modules\order\widgets;

use app\modules\delivery\models\Delivery;
use Yii;
use yii\base\InvalidParamException;

/**
 * Class GeneratorList
 * @package app\modules\order\widgets
 */
class GeneratorList extends Sender
{
    /**
     * @return array
     */
    public static function getAllowDelivery()
    {
        if (!Yii::$app->user->country) {
            throw new InvalidParamException(Yii::t('common', 'Отсутствует выбранная страна.'));
        }

        $delivery_ids = [];
        if (Yii::$app->user->getIsImplant()) {
            $delivery_ids = Yii::$app->user->getDeliveriesIds();
        }

        $deliveries = Delivery::find()
            ->byCountryId(Yii::$app->user->country->id)
            ->byId($delivery_ids)
            ->active()
            ->collection();

        return $deliveries;
    }

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('generator-list/modal', [
            'url' => $this->url,
            'deliveries' => self::getAllowDelivery(),
        ]);
    }
}
