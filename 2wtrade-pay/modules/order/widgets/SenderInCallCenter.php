<?php
namespace app\modules\order\widgets;

use Yii;

/**
 * Class SenderInCallCenter
 * @package app\modules\order\widgets
 */
class SenderInCallCenter extends Sender
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('sender-in-call-center/modal', [
            'url' => $this->url,
        ]);
    }
}
