<?php
namespace app\modules\order\widgets;

use app\modules\order\models\OrderLogisticListInvoice;
use app\widgets\Modal;
use Yii;
use yii\base\InvalidParamException;

/**
 * Class InvoicePrint
 * @package app\modules\order\widgets
 */
class InvoicePrint extends Modal
{
    /**
     * @var OrderLogisticListInvoice
     */
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        if (!$this->model instanceof OrderLogisticListInvoice) {
            throw new InvalidParamException(Yii::t('common', 'Необходимо инициализировать форму.'));
        }

        return $this->render('invoice-print/modal', [
            'model' => $this->model,
            'id' => 'modal_invoice_print',
            'title' => Yii::t('common', 'Печать накладной'),
        ]);
    }
}
