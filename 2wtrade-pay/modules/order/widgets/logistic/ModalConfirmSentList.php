<?php
namespace app\modules\order\widgets\logistic;

use app\widgets\Modal;
use Yii;

/**
 * Class ModalConfirmSentList
 * @package app\modules\order\widgets\logistic
 */
class ModalConfirmSentList extends Modal
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('modal-confirm-sent-list', [
            'id' => 'modal_confirm_sent_list',
            'size' => self::SIZE_MEDIUM,
            'color' => self::COLOR_WARNING,
            'title' => Yii::t('common', 'Подтверждение отправки'),
            'close' => true,
        ]);
    }
}
