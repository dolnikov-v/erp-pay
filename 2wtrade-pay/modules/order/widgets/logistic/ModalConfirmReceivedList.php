<?php
namespace app\modules\order\widgets\logistic;

use app\widgets\Modal;
use Yii;

/**
 * Class ModalConfirmReceivedList
 * @package app\modules\order\widgets\logistic
 */
class ModalConfirmReceivedList extends Modal
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('modal-confirm-received-list', [
            'id' => 'modal_confirm_received_list',
            'size' => self::SIZE_MEDIUM,
            'color' => self::COLOR_WARNING,
            'title' => Yii::t('common', 'Подтверждение получения'),
            'close' => true,
        ]);
    }
}
