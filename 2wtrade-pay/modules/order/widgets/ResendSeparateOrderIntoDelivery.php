<?php

namespace app\modules\order\widgets;

use app\modules\delivery\models\Delivery;
use app\modules\order\models\OrderStatus;
use Yii;
use yii\base\InvalidParamException;

/**
 * Class ResendSeparateOrderIntoDelivery
 * @package app\modules\order\widgets
 */
class ResendSeparateOrderIntoDelivery extends Sender
{
    /**
     * @return array
     */
    public static function getAllowStatusArray()
    {
        return [
            OrderStatus::STATUS_DELIVERY_DENIAL,
            OrderStatus::STATUS_DELIVERY_RETURNED,
            OrderStatus::STATUS_DELIVERY_REFUND
        ];
    }

    /**
     * @return array
     */
    public static function getAllowDelivery()
    {
        if (!Yii::$app->user->country) {
            throw new InvalidParamException(Yii::t('common', 'Отсутствует выбранная страна.'));
        }

        $deliveries = Delivery::find()
            ->byCountryId(Yii::$app->user->country->id)
            ->active()
            ->collection();

        return $deliveries;
    }

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('resend-separate-order-into-delivery/modal', [
            'url' => $this->url,
            'deliveries' => self::getAllowDelivery(),
        ]);
    }
}