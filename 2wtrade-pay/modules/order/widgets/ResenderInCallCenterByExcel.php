<?php
namespace app\modules\order\widgets;

/**
 * Class ResenderInCallCenter
 * @package app\modules\order\widgets
 */
class ResenderInCallCenterByExcel extends Sender
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('resender-in-call-center-by-excel/modal', [
            'url' => $this->url,
        ]);
    }
}
