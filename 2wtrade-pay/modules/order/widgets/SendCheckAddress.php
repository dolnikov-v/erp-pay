<?php
namespace app\modules\order\widgets;

/**
 * Class TransferToDistributor
 * @package app\modules\order\widgets
 */
class SendCheckAddress extends Sender
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('send-check-address/modal', [
            'url' => $this->url,
        ]);
    }
}
