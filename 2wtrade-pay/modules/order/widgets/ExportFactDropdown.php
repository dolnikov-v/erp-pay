<?php
namespace app\modules\order\widgets;

use yii\base\Widget;

/**
 * Class ExportFactDropdown
 * @package app\modules\order\widgets
 */
class ExportFactDropdown extends Widget
{
    /**
     * @var string
     */
    public $name = 'Экспорт';

    /**
     * @var array
     */
    public $extraParams = [];

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('export-fact-dropdown/index', [
            'name' => $this->name,
            'extraParams' => $this->extraParams,
        ]);
    }
}
