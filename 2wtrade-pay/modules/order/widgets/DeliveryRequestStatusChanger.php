<?php

namespace app\modules\order\widgets;

use app\modules\delivery\models\DeliveryRequest;


/**
 * Class DeliveryRequestStatusChanger
 * @package app\modules\order\widgets
 */
class DeliveryRequestStatusChanger extends Sender
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('delivery-request-status-changer/modal', [
            'url' => $this->url,
            'statuses' => DeliveryRequest::getStatusesCollection(),
        ]);
    }
}
