<?php
namespace app\modules\order\widgets;

use Yii;

/**
 * Class SecondDeliveryAttempt
 * @package app\modules\order\widgets
 */
class SecondDeliveryAttempt extends Sender
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('second-delivery-attempt/modal', [
            'url' => $this->url
        ]);
    }
}
