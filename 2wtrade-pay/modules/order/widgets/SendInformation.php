<?php
namespace app\modules\order\widgets;

/**
 * Class SendInformation
 * @package app\modules\order\widgets
 */
class SendInformation extends Sender
{/**
     * @return string
     */
    public function run()
    {
        return $this->render('send-information/modal', [
            'url' => $this->url
        ]);
    }
}
