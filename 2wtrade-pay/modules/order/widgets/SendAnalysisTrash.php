<?php
namespace app\modules\order\widgets;

/**
 * Class SendAnalysisTrash
 * @package app\modules\order\widgets
 */
class SendAnalysisTrash extends Sender
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('send-analysis-trash/modal', [
            'url' => $this->url,
        ]);
    }
}
