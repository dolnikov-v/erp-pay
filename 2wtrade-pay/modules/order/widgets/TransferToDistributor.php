<?php

namespace app\modules\order\widgets;


class TransferToDistributor extends Sender
{
    public $distributorCountry;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('transfer-to-distributor/modal', [
            'url' => $this->url,
            'distributorCountry' => $this->distributorCountry
        ]);
    }
}