<?php
namespace app\modules\order\widgets\marketing;

use app\widgets\Modal;
use Yii;

/**
 * Class ModalConfirmDeleteCoupon
 * @package app\modules\order\widgets\marketing
 */
class ModalConfirmDeleteCoupon extends Modal
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('modal-confirm-delete-coupon', [
            'id' => 'modal_confirm_delete_coupon',
            'size' => self::SIZE_MEDIUM,
            'color' => self::COLOR_DANGER,
            'title' => Yii::t('common', 'Подтверждение удаления'),
            'close' => true,
        ]);
    }
}
