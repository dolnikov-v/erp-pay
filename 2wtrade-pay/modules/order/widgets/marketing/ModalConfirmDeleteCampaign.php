<?php
namespace app\modules\order\widgets\marketing;

use app\widgets\Modal;
use Yii;

/**
 * Class ModalConfirmDeleteCampaign
 * @package app\modules\order\widgets\marketing
 */
class ModalConfirmDeleteCampaign extends Modal
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('modal-confirm-delete-campaign', [
            'id' => 'modal_confirm_delete_campaign',
            'size' => self::SIZE_MEDIUM,
            'color' => self::COLOR_DANGER,
            'title' => Yii::t('common', 'Подтверждение удаления'),
            'close' => true,
        ]);
    }
}
