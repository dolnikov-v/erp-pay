<?php
namespace app\modules\order\widgets\marketing;

use app\widgets\Modal;
use Yii;

/**
 * Class ModalConfirmDeleteList
 * @package app\modules\order\widgets\marketing
 */
class ModalConfirmDeleteList extends Modal
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('modal-confirm-delete-list', [
            'id' => 'modal_confirm_delete_list',
            'size' => self::SIZE_MEDIUM,
            'color' => self::COLOR_DANGER,
            'title' => Yii::t('common', 'Подтверждение удаления'),
            'close' => true,
        ]);
    }
}
