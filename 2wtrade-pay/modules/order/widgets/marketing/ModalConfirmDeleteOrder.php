<?php
namespace app\modules\order\widgets\marketing;

use app\widgets\Modal;
use Yii;

/**
 * Class ModalConfirmDeleteOrder
 * @package app\modules\order\widgets\marketing
 */
class ModalConfirmDeleteOrder extends Modal
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('modal-confirm-delete-order', [
            'id' => 'modal_confirm_delete_order',
            'size' => self::SIZE_MEDIUM,
            'color' => self::COLOR_DANGER,
            'title' => Yii::t('common', 'Подтверждение удаления'),
            'close' => true,
        ]);
    }
}
