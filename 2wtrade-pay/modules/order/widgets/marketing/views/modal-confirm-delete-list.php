<?php
use app\widgets\Button;
use app\widgets\ButtonLink;
use yii\helpers\Html;

/** @var string $id */
/** @var string $size */
/** @var string $color */
/** @var string $title */
/** @var string $close */
?>

<div id="<?= $id ?>" class="modal fade <?= $color ?> in" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog <?= $size ?>">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" aria-label="Close" data-dismiss="modal" type="button">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title"><?= $title ?></h4>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <?= Yii::t('common', 'Вы действительно хотите удалить маркетинговый лист?') ?>
                </div>
            </div>
            <div class="modal-footer text-right">
                <?= Button::widget([
                    'label' => Yii::t('common', 'Отмена'),
                    'size' => Button::SIZE_SMALL,
                    'attributes' => [
                        'data-dismiss' => 'modal',
                    ],
                ]) ?>

                <?= ButtonLink::widget([
                    'id' => 'modal_confirm_delete_list_link',
                    'label' => Yii::t('common', 'Удалить из списка'),
                    'url' => '#',
                    'style' => ButtonLink::STYLE_DANGER,
                    'size' => Button::SIZE_SMALL,
                ]) ?>
            </div>
        </div>
    </div>
</div>
