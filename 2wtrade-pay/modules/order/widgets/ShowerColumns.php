<?php

namespace app\modules\order\widgets;

use app\helpers\WbIcon;
use app\widgets\Link;
use yii\base\Widget;
use Yii;

/**
 * Class ShowerColumns
 * @package app\modules\order\widgets
 *
 * @property array $defaultColumns
 * @property array $chosenColumns
 */
class ShowerColumns extends Widget
{
    const COLUMN_CHECKBOX = 'checkbox';
    const COLUMN_RECORD_ID = 'id';
    const COLUMN_ACTIONS = 'actions';

    const COLUMN_SOURCE = 'source_id';
    const COLUMN_FOREIGN_ID = 'foreign_id';
    const COLUMN_CALL_CENTER_FOREIGN_ID = 'callCenterRequest.foreign_id';
    const COLUMN_CALL_CENTER_ID = 'callCenterRequest.id';
    const COLUMN_CALL_CENTER_NAME = 'callCenterRequest.call_center_id';
    const COLUMN_DELIVERY_ID = 'deliveryRequest.id';
    const COLUMN_DELIVERY_NAME = 'deliveryRequest.delivery_id';
    const COLUMN_DELIVERY_TRACKING = 'deliveryRequest.tracking';
    const COLUMN_DELIVERY_LABEL = 'delivery_label';
    const COLUMN_DELIVERY_API_ERROR = 'deliveryRequest.api_error';
    const COLUMN_PRODUCTS = 'orderProducts';
    const COLUMN_STATUS_ID = 'status_id';
    const COLUMN_CUSTOMER_FULL_NAME = 'customer_full_name';
    const COLUMN_CUSTOMER_ADDRESS = 'customer_address';
    const COLUMN_CUSTOMER_ZIP = 'customer_zip';
    const COLUMN_CUSTOMER_CITY = 'customer_city';
    const COLUMN_CUSTOMER_PROVINCE = 'customer_province';
    const COLUMN_CUSTOMER_DISTRICT = 'customer_district';
    const COLUMN_CUSTOMER_CITY_CODE = 'customer_city_code';
    const COLUMN_CUSTOMER_STREET_HOUSE = 'customer_street_house';
    const COLUMN_CUSTOMER_STREET = 'customer_street';
    const COLUMN_CUSTOMER_HOUSE = 'customer_house_number';
    const COLUMN_CUSTOMER_PHONE = 'customer_phone';
    const COLUMN_CUSTOMER_MOBILE = 'customer_mobile';
    const COLUMN_CUSTOMER_EMAIL = 'customer_email';
    const COLUMN_COMMENT = 'comment';
    const COLUMN_CALL_CENTER_COMMENT = 'callCenterRequest.comment';
    const COLUMN_DELIVERY_DATE = 'delivery_time_from';
    const COLUMN_CALL_CENTER_APPROVED_AT = 'callCenterRequest.approved_at';
    const COLUMN_CALL_CENTER_SENT_AT = 'callCenterRequest.sent_at';
    const COLUMN_DELIVERY_SENT_AT = "deliveryRequest.sent_at";
    const COLUMN_DELIVERY_SECOND_SENT_AT = "deliveryRequest.second_sent_at";
    const COLUMN_DELIVERY_SENT_CLARIFICATION_AT = "deliveryRequest.sent_clarification_at";
    const COLUMN_DELIVERY_ACCEPTED_AT = "deliveryRequest.accepted_at";
    const COLUMN_DELIVERY_APPROVED_AT = 'deliveryRequest.approved_at';
    const COLUMN_DELIVERY_PAID_AT = 'deliveryRequest.paid_at';
    const COLUMN_PRICE = 'price';
    const COLUMN_PRICE_TOTAL = 'price_total';
    const COLUMN_PRICE_TOTAL_WITH_DELIVERY = 'price_total_with_delivery';
    const COLUMN_DELIVERY = 'delivery';
    const COLUMN_CREATED_AT = 'created_at';
    const COLUMN_UPDATED_AT = 'updated_at';
    const COLUMN_SITE = 'landing.url';
    const COLUMN_LANDING_ID = 'landing_id';
    const COLUMN_ORDER_LOGISTIC_LIST_ID = 'orderLogisticList.id';
    const COLUMN_DELIVERY_PARTNER_NAME = 'deliveryRequest.deliveryPartner.name';
    const COLUMN_PENDING_DELIVERY_ID = 'pending_delivery_id';
    const COLUMN_CUSTOMER_SUBDISTRICT = 'customer_subdistrict';
    const COLUMN_CUSTOMER_ADDRESS_ADDITIONAL = 'customer_address_additional';
    const COLUMN_CALL_CENTER_AUTO_CHECK_ADDRESS = 'call_center_auto_check_address';

    const COLUMN_GROUP_DELIVERY_REQUEST = 'deliveryRequest';
    const COLUMN_IN_GROUP_DELIVERY_REQUEST_DELIVERY_ID = 'delivery_id';
    const COLUMN_GROUP_ORDER_LOGISTIC_LIST = 'orderLogisticList';
    const COLUMN_IN_GROUP_ORDER_LOGISTIC_LIST_ID = 'id';
    const COLUMN_WEBMASTER_IDENTIFIER = 'webmaster_identifier';
    const COLUMN_CALL_CENTER_API_ERROR = 'callCenterRequest.api_error';
    const COLUMN_CALL_CENTER_ANALYSIS_TRASH_AT = 'callCenterRequest.analysis_trash_at';
    const COLUMN_CALL_CENTER_ANALYSIS_TRASH_COMMENT = 'callCenterRequest.analysis_trash_comment';
    const ORIGINAL_ORDER_ID = 'originalOrder.id';
    const DUPLICATE_ORDER_ID = 'order.duplicate_order_id';
    const COLUMN_PARTNER_TRACKER = 'deliveryRequest.finance_tracking';

    const COLUMN_LAST_FOREIGN_OPERATOR = 'callCenterRequest.last_foreign_operator';
    const COLUMN_UNSHIPPING_REASON = 'deliveryRequest.unBuyoutReasonMapping';

    const COLUMN_PAYMENT_TYPE = 'payment_type';
    const COLUMN_PAYMENT_AMOUNT = 'payment_amount';
    const COLUMN_INCOME = 'income';
    const COLUMN_EXPRESS_DELIVERY = 'orderExpressDelivery';

    const COLUMN_FINANCE_PREDICTION_STORAGE = 'financePrediction.price_storage';
    const COLUMN_FINANCE_PREDICTION_PACKING = 'financePrediction.price_packing';
    const COLUMN_FINANCE_PREDICTION_PACKAGE = 'financePrediction.price_package';
    const COLUMN_FINANCE_PREDICTION_ADDRESS_CORRECTION = 'financePrediction.price_address_correction';
    const COLUMN_FINANCE_PREDICTION_DELIVERY = 'financePrediction.price_delivery';
    const COLUMN_FINANCE_PREDICTION_REDELIVERY = 'financePrediction.price_redelivery';
    const COLUMN_FINANCE_PREDICTION_DELIVERY_BACK = 'financePrediction.price_delivery_back';
    const COLUMN_FINANCE_PREDICTION_DELIVERY_RETURN = 'financePrediction.price_delivery_return';
    const COLUMN_FINANCE_PREDICTION_COD_SERVICE = 'financePrediction.price_cod_service';
    const COLUMN_FINANCE_PREDICTION_VAT = 'financePrediction.price_vat';
    const COLUMN_FINANCE_PREDICTION_FULFILMENT = 'financePrediction.price_fulfilment';

    const COLUMN_FINANCE_FACT_PRICE_STORAGE = 'financeFact.price_storage';
    const COLUMN_FINANCE_FACT_PACKING = 'financeFact.price_packing';
    const COLUMN_FINANCE_FACT_PACKAGE = 'financeFact.price_package';
    const COLUMN_FINANCE_FACT_ADDRESS_CORRECTION = 'financeFact.price_address_correction';
    const COLUMN_FINANCE_FACT_DELIVERY = 'financeFact.price_delivery';
    const COLUMN_FINANCE_FACT_REDELIVERY = 'financeFact.price_redelivery';
    const COLUMN_FINANCE_FACT_DELIVERY_BACK = 'financeFact.price_delivery_back';
    const COLUMN_FINANCE_FACT_DELIVERY_RETURN = 'financeFact.price_delivery_return';
    const COLUMN_FINANCE_FACT_COD_SERVICE = 'financeFact.price_cod_service';
    const COLUMN_FINANCE_FACT_VAT = 'financeFact.price_vat';
    const COLUMN_FINANCE_FACT_FULFILMENT = 'financeFact.price_fulfilment';
    const COLUMN_FINANCE_FACT_PRICE_COD = 'financeFact.price_cod';
    const COLUMN_PAYMENT_NAME = 'financeFact.payment_order_delivery.name';

    const COLUMN_CALL_CENTER_CHECK_REQUEST_RESULT = 'foreign_information';

    const COLUMN_CHECK_REQUESTS = 'callCenterCheckRequests';

    const COLUMN_TS_SPAWN = 'lead.ts_spawn';

    const COLUMN_PRETENSION = 'finance_fact_pretension';

    const COLUMN_PRICE_CURRENCY = 'price_currency';
    const COLUMN_CALLS_COUNT = 'callCenterRequest.callsCount';

    /** @var string */
    public $id = 'modal_shower_columns';

    /** @var array */
    protected $requireColumnsCollection;

    /** @var array */
    protected $manualColumnsCollection;

    /** @var string */
    protected $sessionKey = 'orders.columns';

    /**
     * @return array
     * @throws \Exception
     */
    public function run()
    {
        return [
            'icon' => $this->renderIcon(),
            'modal' => $this->renderModal(),
        ];
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function renderIcon()
    {
        return Link::widget([
            'style' => 'panel-action icon ' . WbIcon::SETTINGS,
            'url' => '#',
            'attributes' => [
                'title' => Yii::t('common', 'Показать/Скрыть столбцы'),
                'data-toggle' => 'modal',
                'data-target' => '#' . $this->id,
            ],
        ]);
    }

    /**
     * @return string
     */
    public function renderModal()
    {
        return $this->render('shower-columns/modal', [
            'id' => $this->id,
            'requireColumns' => $this->getRequireColumnsCollection(),
            'manualColumns' => $this->getManualColumnsCollection(),
            'chosenColumns' => $this->getChosenColumns(),
        ]);
    }

    /**
     * @param string $column
     * @return boolean
     */
    public function isChosenColumn($column)
    {
        $columns = $this->getChosenColumns();

        if (is_null($columns) || !is_array($columns)) {
            $columns = $this->getDefaultColumns();
            Yii::$app->session->set($this->sessionKey, $columns);
        }

        return in_array($column, $columns);
    }

    /**
     * @param [] $columns
     * @param [] $choices
     */
    public function toggleColumn($columns, $choices)
    {
        $allowColumns = array_keys($this->getManualColumnsCollection());

        if (is_array($columns) && !empty($columns)) {
            foreach ($columns as $k => $column) {

                if (in_array($column, $allowColumns)) {
                    $chosenColumns = $this->getChosenColumns();

                    if ($choices[$k]) {
                        if (!in_array($column, $chosenColumns)) {
                            $chosenColumns[] = $column;
                        }
                    } else {
                        $index = array_search($column, $chosenColumns);

                        if ($index !== false) {
                            unset($chosenColumns[$index]);
                        }
                    }

                    Yii::$app->session->set($this->sessionKey, array_values($chosenColumns));
                }
            }
        }
    }

    /**
     * @return array
     */
    protected function getRequireColumnsCollection()
    {
        if (is_null($this->requireColumnsCollection)) {
            $this->requireColumnsCollection = [
                self::COLUMN_CHECKBOX => Yii::t('common', 'Выделение заказа'),
                self::COLUMN_RECORD_ID => Yii::t('common', 'Номер заказа'),
                self::COLUMN_ACTIONS => Yii::t('common', 'Действия над заказом'),
            ];
        }

        return $this->requireColumnsCollection;
    }

    /**
     * @return array
     */
    public function getManualColumnsCollection()
    {
        if (is_null($this->manualColumnsCollection)) {
            $this->manualColumnsCollection = [
                self::COLUMN_SOURCE => Yii::t('common', 'Источник'),
                self::COLUMN_FOREIGN_ID => Yii::t('common', 'Номер в источнике'),
                self::COLUMN_CALL_CENTER_FOREIGN_ID => Yii::t('common', 'Номер в колл-центре'),
                self::COLUMN_ORDER_LOGISTIC_LIST_ID => Yii::t('common', 'Лист'),
                self::COLUMN_CALL_CENTER_ID => Yii::t('common', 'Заявка в колл-центре'),
                self::COLUMN_CALL_CENTER_API_ERROR => Yii::t('common', 'Ошибка КЦ'),
                self::COLUMN_CALL_CENTER_NAME => Yii::t('common', 'Колл-центр'),
                self::COLUMN_DELIVERY_ID => Yii::t('common', 'Заявка в службе доставки'),
                self::COLUMN_DELIVERY_NAME => Yii::t('common', 'Служба доставки'),
                self::COLUMN_DELIVERY_API_ERROR => Yii::t('common', 'Ошибка курьерки'),
                self::COLUMN_DELIVERY_PARTNER_NAME => Yii::t('common', 'Партнер КС'),
                self::COLUMN_DELIVERY_TRACKING => Yii::t('common', 'Трекер'),
                self::COLUMN_DELIVERY_LABEL => Yii::t('common', 'Этикетка'),
                self::COLUMN_STATUS_ID => Yii::t('common', 'Статус заказа'),
                self::COLUMN_PRODUCTS => Yii::t('common', 'Товары'),
                self::COLUMN_CUSTOMER_FULL_NAME => Yii::t('common', 'Полное имя заказчика'),
                self::COLUMN_CUSTOMER_ADDRESS => Yii::t('common', 'Адрес заказчика'),
                self::COLUMN_CUSTOMER_ZIP => Yii::t('common', 'Почтовый индекс заказчика'),
                self::COLUMN_CUSTOMER_CITY => Yii::t('common', 'Город заказчика'),
                self::COLUMN_CUSTOMER_PROVINCE => Yii::t('common', 'Провинция заказчика'),
                self::COLUMN_CUSTOMER_DISTRICT => Yii::t('common', 'Район заказчика'),
                self::COLUMN_CUSTOMER_CITY_CODE => Yii::t('common', 'Код города заказчика'),
                self::COLUMN_CUSTOMER_SUBDISTRICT => Yii::t('common', 'Квартал заказчика'),
                self::COLUMN_CUSTOMER_STREET_HOUSE => Yii::t('common', 'Улица и номер дома заказчика'),
                self::COLUMN_CUSTOMER_PHONE => Yii::t('common', 'Телефон заказчика'),
                self::COLUMN_CUSTOMER_MOBILE => Yii::t('common', 'Мобильный заказчика'),
                self::COLUMN_CUSTOMER_EMAIL => Yii::t('common', 'Email заказчика'),
                self::COLUMN_CUSTOMER_ADDRESS_ADDITIONAL => Yii::t('common', 'Доп. адрес заказчика'),
                self::COLUMN_COMMENT => Yii::t('common', 'Комментарий'),
                self::COLUMN_DELIVERY_SENT_AT => Yii::t('common', 'Дата отправки (курьерка)'),
                self::COLUMN_DELIVERY_SECOND_SENT_AT => Yii::t('common', 'Дата повторной отправки (курьерка)'),
                self::COLUMN_DELIVERY_SENT_CLARIFICATION_AT => Yii::t('common', 'Дата отправки на уточнение (курьерка)'),
                self::COLUMN_DELIVERY_ACCEPTED_AT => Yii::t('common', 'Дата принятия (курьерка)'),
                self::COLUMN_DELIVERY_APPROVED_AT => Yii::t('common', 'Дата апрува (курьерка)'),
                self::COLUMN_DELIVERY_PAID_AT => Yii::t('common', 'Дата оплаты (курьерка)'),
                self::COLUMN_DELIVERY_DATE => Yii::t('common', 'Дата доставки'),
                self::COLUMN_CALL_CENTER_APPROVED_AT => Yii::t('common', 'Дата апрува КЦ'),
                self::COLUMN_CALL_CENTER_SENT_AT => Yii::t('common', 'Дата отправки в КЦ'),
                self::COLUMN_CALL_CENTER_COMMENT => Yii::t('common', 'Комментарий КЦ'),
                self::COLUMN_PRICE => Yii::t('common', 'Начальная цена'),
                self::COLUMN_PRICE_TOTAL_WITH_DELIVERY => Yii::t('common', 'Конечная цена'),
                self::COLUMN_CREATED_AT => Yii::t('common', 'Дата создания'),
                self::COLUMN_UPDATED_AT => Yii::t('common', 'Дата изменения'),
                self::COLUMN_SITE => Yii::t('common', 'Лэндинг'),
                self::COLUMN_WEBMASTER_IDENTIFIER => Yii::t('common', 'ID вебмастера'),
                self::COLUMN_CALL_CENTER_ANALYSIS_TRASH_AT => Yii::t('common', 'Дата проверки треша в КЦ'),
                self::COLUMN_CALL_CENTER_ANALYSIS_TRASH_COMMENT => Yii::t('common', 'Результат проверки треша в КЦ'),
                self::DUPLICATE_ORDER_ID => Yii::t('common', 'Дубликат'),
                self::ORIGINAL_ORDER_ID => Yii::t('common', 'Оригинал'),
                self::COLUMN_PARTNER_TRACKER => Yii::t('common', 'Трекер партнера КС'),
                self::COLUMN_LAST_FOREIGN_OPERATOR => Yii::t('common', 'Оператор'),
                self::COLUMN_UNSHIPPING_REASON => Yii::t('common', 'Причина недоставки'),
                self::COLUMN_PAYMENT_TYPE => Yii::t('common', 'Тип оплаты'),
                self::COLUMN_PAYMENT_AMOUNT => Yii::t('common', 'Предоплата'),
                self::COLUMN_CALL_CENTER_AUTO_CHECK_ADDRESS => Yii::t('common', 'Автоматическая проверка адреса'),
                self::COLUMN_INCOME => Yii::t('common', 'Цена лида'),
                self::COLUMN_EXPRESS_DELIVERY => Yii::t('common', 'Экспресс'),
                self::COLUMN_FINANCE_PREDICTION_STORAGE => Yii::t('common', 'Стоимость хранения'),
                self::COLUMN_FINANCE_PREDICTION_PACKING => Yii::t('common', 'Стоимость упаковывания'),
                self::COLUMN_FINANCE_PREDICTION_PACKAGE => Yii::t('common', 'Стоимость упаковки'),
                self::COLUMN_FINANCE_PREDICTION_ADDRESS_CORRECTION => Yii::t('common', 'Стоимость корректировки адреса'),
                self::COLUMN_FINANCE_PREDICTION_DELIVERY => Yii::t('common', 'Стоимость доставки'),
                self::COLUMN_FINANCE_PREDICTION_REDELIVERY => Yii::t('common', 'Стоимость передоставки'),
                self::COLUMN_FINANCE_PREDICTION_DELIVERY_BACK => Yii::t('common', 'Стоимость возвращения'),
                self::COLUMN_FINANCE_PREDICTION_DELIVERY_RETURN => Yii::t('common', 'Стоимость возврата'),
                self::COLUMN_FINANCE_PREDICTION_COD_SERVICE => Yii::t('common', 'Плата за наложенный платеж'),
                self::COLUMN_FINANCE_PREDICTION_VAT => Yii::t('common', 'НДС'),
                self::COLUMN_FINANCE_PREDICTION_FULFILMENT => Yii::t('common', 'Стоимость обслуживания заказа'),
                self::COLUMN_CHECK_REQUESTS => Yii::t('common', 'Заявки на проверку заказа'),
                self::COLUMN_TS_SPAWN => Yii::t('common', 'Дата создания у партнера'),
                self::COLUMN_CALL_CENTER_CHECK_REQUEST_RESULT => Yii::t('common', 'Результат проверки в КЦ'),
                self::COLUMN_PRETENSION => Yii::t('common', 'Претензия'),
                self::COLUMN_PRICE_CURRENCY => Yii::t('common', 'Валюта'),
                self::COLUMN_DELIVERY => Yii::t('common', 'Цена доставки'),
                self::COLUMN_FINANCE_FACT_PRICE_STORAGE => Yii::t('common', 'Фактическая стоимость хранения'),
                self::COLUMN_FINANCE_FACT_PACKING => Yii::t('common', 'Фактическая стоимость упаковывания'),
                self::COLUMN_FINANCE_FACT_PACKAGE => Yii::t('common', 'Фактическая стоимость упаковки'),
                self::COLUMN_FINANCE_FACT_ADDRESS_CORRECTION => Yii::t('common', 'Фактическая стоимость корректировки адреса'),
                self::COLUMN_FINANCE_FACT_DELIVERY => Yii::t('common', 'Фактическая стоимость доставки'),
                self::COLUMN_FINANCE_FACT_REDELIVERY => Yii::t('common', 'Фактическая стоимость передоставки'),
                self::COLUMN_FINANCE_FACT_DELIVERY_BACK => Yii::t('common', 'Фактическая стоимость возвращения'),
                self::COLUMN_FINANCE_FACT_DELIVERY_RETURN => Yii::t('common', 'Фактическая стоимость возврата'),
                self::COLUMN_FINANCE_FACT_COD_SERVICE => Yii::t('common', 'Фактическая плата за наложенный платеж'),
                self::COLUMN_FINANCE_FACT_VAT => Yii::t('common', 'Фактический НДС'),
                self::COLUMN_FINANCE_FACT_FULFILMENT => Yii::t('common', 'Фактическая стоимость обслуживания заказа'),
                self::COLUMN_FINANCE_FACT_PRICE_COD => Yii::t('common', 'Фактический COD'),
                self::COLUMN_PAYMENT_NAME => Yii::t('common', 'Платежка'),
                self::COLUMN_CALLS_COUNT => Yii::t('common', 'Число звонков'),
            ];
        }

        asort($this->manualColumnsCollection);

        return $this->manualColumnsCollection;
    }

    /**
     * @return array
     */
    public function getChosenColumns()
    {
        $columns = Yii::$app->session->get($this->sessionKey);

        if (is_null($columns) || !is_array($columns)) {
            $columns = $this->getDefaultColumns();
            Yii::$app->session->set($this->sessionKey, $columns);
        }

        return $columns;
    }

    /**
     * @return array
     */
    protected function getDefaultColumns()
    {
        $requireColumns = array_keys($this->getRequireColumnsCollection());

        $manualColumns = [
            self::COLUMN_STATUS_ID,
            self::COLUMN_PRICE,
            self::COLUMN_PRICE_TOTAL_WITH_DELIVERY,
            self::COLUMN_CREATED_AT,
        ];

        return array_merge($requireColumns, $manualColumns);
    }


    public static function getFactColumns()
    {

        $columns = [
            self::COLUMN_FINANCE_FACT_PRICE_STORAGE,
            self::COLUMN_FINANCE_FACT_PACKING,
            self::COLUMN_FINANCE_FACT_PACKAGE,
            self::COLUMN_FINANCE_FACT_ADDRESS_CORRECTION,
            self::COLUMN_FINANCE_FACT_DELIVERY,
            self::COLUMN_FINANCE_FACT_REDELIVERY,
            self::COLUMN_FINANCE_FACT_DELIVERY_BACK,
            self::COLUMN_FINANCE_FACT_DELIVERY_RETURN,
            self::COLUMN_FINANCE_FACT_COD_SERVICE,
            self::COLUMN_FINANCE_FACT_VAT,
            self::COLUMN_FINANCE_FACT_FULFILMENT,
            self::COLUMN_FINANCE_FACT_PRICE_COD,
        ];

        return $columns;
    }
}
