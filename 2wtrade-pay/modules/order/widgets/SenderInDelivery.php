<?php
namespace app\modules\order\widgets;

use app\modules\delivery\models\Delivery;
use Yii;
use yii\base\InvalidParamException;

/**
 * Class SenderInDelivery
 * @package app\modules\order\widgets
 */
class SenderInDelivery extends Sender
{
    /**
     * @return array
     */
    public static function getAllowDelivery()
    {
        if (!Yii::$app->user->country) {
            throw new InvalidParamException(Yii::t('common', 'Отсутствует выбранная страна.'));
        }

        $deliveries = Delivery::find()
            ->byCountryId(Yii::$app->user->country->id)
            ->active()
            ->haveApi()
            ->collection();

        return $deliveries;
    }

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('sender-in-delivery/modal', [
            'url' => $this->url,
            'deliveries' => self::getAllowDelivery(),
        ]);
    }
}
