<?php
namespace app\modules\order\widgets;

/**
 * Class ResenderInCallCenterStillWaiting
 * @package app\modules\order\widgets
 */
class ResenderInCallCenterStillWaiting extends Sender
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('resender-in-call-center-still-waiting/modal', [
            'url' => $this->url,
        ]);
    }
}
