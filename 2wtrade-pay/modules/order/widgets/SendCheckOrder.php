<?php
namespace app\modules\order\widgets;

/**
 * Class SendCheckOrder
 * @package app\modules\order\widgets
 */
class SendCheckOrder extends Sender
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('send-check-order/modal', [
            'url' => $this->url,
        ]);
    }
}
