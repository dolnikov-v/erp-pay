<?php
namespace app\modules\order\widgets;

use app\modules\order\models\OrderStatus;
use Yii;

/**
 * Class ChangerStatus
 * @package app\modules\order\widgets
 */
class ChangerStatus extends Sender
{
    /**
     * @return array
     */
    public static function getAllowStatuses()
    {
        $ids = [];
        $allowStatuses = Yii::$app->user->country->getAllowOrderStatuses();

        foreach ($allowStatuses as $status => $parents) {
            if ($parents) {
                $ids = array_merge($ids, $parents);
            }
        }

        $ids = array_unique($ids);

        $statuses = OrderStatus::find()
            ->where(['in', 'id', $ids])
            ->collection();

        unset($statuses[OrderStatus::STATUS_CC_POST_CALL]);
        unset($statuses[OrderStatus::STATUS_DELIVERY_PENDING]);

        return $statuses;
    }

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('changer-status/modal', [
            'url' => $this->url,
            'statuses' => self::getAllowStatuses(),
        ]);
    }
}
