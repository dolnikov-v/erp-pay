<?php
use app\widgets\Modal;

/** @var string $url */
?>

<?= Modal::widget([
    'id' => 'modal_second_delivery_attempt',
    'title' => Yii::t('common', '2 попытка доставки'),
    'body' => $this->render('_modal-body', [
        'url' => $url
    ]),
]) ?>
