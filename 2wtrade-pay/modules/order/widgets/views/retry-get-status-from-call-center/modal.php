<?php
use app\widgets\Modal;

/** @var string $url */
?>

<?= Modal::widget([
    'id' => 'modal_retry_get_status_from_call_center',
    'title' => Yii::t('common', 'Обновление статуса заявок из КЦ'),
    'body' => $this->render('_modal-body', [
        'url' => $url,
    ]),
]) ?>
