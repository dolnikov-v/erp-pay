<?php
use app\widgets\Modal;

/** @var string $url */
?>

<?= Modal::widget([
    'id' => 'modal_send_check_address',
    'title' => Yii::t('common', 'Отправка на проверку адреса'),
    'body' => $this->render('_modal-body', [
        'url' => $url,
    ]),
]) ?>
