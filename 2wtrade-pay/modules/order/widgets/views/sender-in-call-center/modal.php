<?php
use app\widgets\Modal;

/** @var string $url */
?>

<?= Modal::widget([
    'id' => 'modal_send_in_call_center',
    'title' => Yii::t('common', 'Отпрака на обзвон в колл-центр'),
    'body' => $this->render('_modal-body', [
        'url' => $url,
    ]),
]) ?>
