<?php

use app\components\widgets\ActiveForm;
use app\models\Partner;
use app\models\PartnerCountry;
use app\widgets\Button;
use app\widgets\ProgressBar;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/** @var string $url */
/** @var array $distributorCountry */
?>

<?php $form = ActiveForm::begin([
    'enableClientValidation' => false,
    'action' => Url::toRoute('index'),
    'method' => 'post',
    'options' => [
        'data-sender-url' => $url,
    ]
]); ?>

<div class="row-with-text-start">
    <div class="text-center">

    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <?= $form->field(new \app\models\Country(), 'name')->select2List(ArrayHelper::map($distributorCountry, 'id', function ($item) {
            $roles = [];
            if ($item->curatorUser) {
                $roles = ArrayHelper::getColumn(Yii::$app->authManager->getRolesByUser($item->curatorUser->id), 'description');
            }
            return count($roles) > 0 ? $item->curatorUser->username . ' (' . implode(', ', array_values($roles)) . ') ' . $item['name'] : '(-) ' . $item['name'];

        }),
            [
                'prompt' => '-',
                'name' => 'country_id'
            ])->label(yii::t('common', 'Страна')); ?>
    </div>
</div>

<div class="row-with-text-stop" style="display: none;">
    <h5>
        <?= Yii::t('common', 'Отправка заказов') ?>
        <span class="pull-right sender-percent">0%</span>
    </h5>
    <?= ProgressBar::widget() ?>
</div>

<div class="padding-top-20">
    <div class="row-with-btn-start text-right">
        <?= Button::widget([
            'id' => 'start_transfer_to_distributor',
            'label' => Yii::t('common', 'Да'),
            'style' => Button::STYLE_SUCCESS,
            'size' => Button::SIZE_SMALL,
        ]) ?>

        <?= Button::widget([
            'label' => Yii::t('common', 'Закрыть'),
            'size' => Button::SIZE_SMALL,
            'attributes' => [
                'data-dismiss' => 'modal',
            ],
        ]) ?>
    </div>

    <div class="row-with-btn-stop" style="display: none;">
        <?= Button::widget([
            'id' => 'stop__transfer_to_distributor',
            'label' => Yii::t('common', 'Остановить'),
            'style' => Button::STYLE_DANGER . ' btn-block',
            'size' => Button::SIZE_SMALL,
        ]) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
