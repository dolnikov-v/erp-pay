<?php
use app\widgets\Modal;

/** @var string $url */
/** @var array $distributorCountry */
?>

<?= Modal::widget([
    'id' => 'modal_transfer_to_distributor',
    'title' => Yii::t('common', 'Передать дистрибьютору'),
    'body' => $this->render('_modal-body', [
        'url' => $url,
        'distributorCountry' => $distributorCountry
    ]),
]) ?>
