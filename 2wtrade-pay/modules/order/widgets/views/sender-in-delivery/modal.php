<?php
use app\widgets\Modal;

/** @var string $url */
/** @var array $deliveries */
?>

<?= Modal::widget([
    'id' => 'modal_sender_in_delivery',
    'title' => Yii::t('common', 'Передача в службу доставки'),
    'body' => $this->render('_modal-body', [
        'url' => $url,
        'deliveries' => $deliveries,
    ]),
]) ?>
