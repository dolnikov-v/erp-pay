<?php
use app\widgets\Modal;

/** @var string $url */
/** @var array $deliveries */
?>

<?= Modal::widget([
    'id' => 'modal_resend_separate_order',
    'title' => Yii::t('common', 'Переотправка отдельными заказом(в КЦ)'),
    'body' => $this->render('_modal-body', [
        'url' => $url,
    ]),
]) ?>
