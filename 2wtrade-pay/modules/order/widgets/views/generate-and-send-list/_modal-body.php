<?php
use app\components\widgets\ActiveForm;
use app\helpers\WbIcon;
use app\widgets\Button;
use app\widgets\ProgressBar;
use yii\helpers\Html;
use app\modules\order\components\ExcelBuilder;
use app\widgets\custom\Checkbox;
use app\widgets\Select2;
use app\modules\order\models\OrderLogisticListExcel;

/** @var array $checkedColumns */
/** @var array $partColumnsOne */
/** @var array $partColumnsTwo */
/** @var array $partColumnsThree */
?>

<?php $form = ActiveForm::begin([
    'enableClientValidation' => false,
    'action' => ['/order/list/send'],
    'method' => 'post',
]); ?>

<?= Html::hiddenInput('list_id', '', [
    'id' => 'generate_and_send_list_id',
]) ?>

<div class="required-generating-list">
    <div class="text-center font-size-16">
        <?= Yii::t('common', 'Необходимо сгенерировать лист') ?>
    </div>
</div>

<div class="row-with-columns generating-default-list margin-top-10">
    <div class="row">
        <div class="col-xs-12 text-center font-size-16 margin-bottom-10">
            <?= Yii::t('common', 'Доступные колонки') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-4">
            <div class="col-choice-columns">
                <?php foreach ($partColumnsOne as $column): ?>
                    <?= Checkbox::widget([
                        'id' => 'column_' . md5($column),
                        'name' => 'columns[]',
                        'value' => $column,
                        'label' => ExcelBuilder::getColumnLabel($column),
                        'checked' => in_array($column, $checkedColumns) ? true : false,
                    ]) ?>
                <?php endforeach; ?>
            </div>
        </div>

        <div class="col-xs-4">
            <div class="col-choice-columns">
                <?php foreach ($partColumnsTwo as $column): ?>
                    <?= Checkbox::widget([
                        'id' => 'column_' . md5($column),
                        'name' => 'columns[]',
                        'value' => $column,
                        'label' => ExcelBuilder::getColumnLabel($column),
                        'checked' => in_array($column, $checkedColumns) ? true : false,
                    ]) ?>
                <?php endforeach; ?>
            </div>
        </div>

        <div class="col-xs-4">
            <div class="col-choice-columns">
                <?php foreach ($partColumnsThree as $column): ?>
                    <?= Checkbox::widget([
                        'id' => 'column_' . md5($column),
                        'name' => 'columns[]',
                        'value' => $column,
                        'label' => ExcelBuilder::getColumnLabel($column),
                        'checked' => in_array($column, $checkedColumns) ? true : false,
                    ]) ?>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <div class="row margin-top-20">
        <div class="col-xs-12 text-center font-size-16 margin-bottom-10">
            <?= Yii::t('common', 'Формат листа') ?>
        </div>
        <div class="col-xs-12">
            <?= Select2::widget([
                'name' => 'format',
                'length' => -1,
                'items' => OrderLogisticListExcel::getFormatsCollection(),
            ]) ?>
        </div>
    </div>
</div>

<div class="row-with-text-progress generating-list-progress text-center" style="display: none;">
    <div class="text-center font-size-16 margin-bottom-15">
        <?= Yii::t('common', 'Идет генерация листа') ?>
    </div>
    <div class="row">
        <div class="col-xs-2"></div>
        <div class="col-xs-8 text-center">
            <div class="spinner"></div>
        </div>
        <div class="col-xs-8"></div>
    </div>
</div>

<div class="row-with-text-progress sending-list-progress" style="display: none;">
    <h5>
        <?= Yii::t('common', 'Отправка листа') ?>
        <span class="pull-right builder-percent">0%</span>
    </h5>
    <?= ProgressBar::widget() ?>
</div>

<div class="row-with-sending-text" style="display: none;">
    <div class="text-center font-size-16">
        <?= Yii::t('common', 'Лист сгенерирован, можно отправлять. Получатели:') ?>
        <div id="generate_and_send_mailing_list"></div>
    </div>
</div>

<div class="row-with-btn-start btn-send-list padding-top-20 text-right" style="display: none;">
    <?= Button::widget([
        'id' => 'start_sending_list',
        'label' => Yii::t('common', 'Отправить лист'),
        'style' => Button::STYLE_SUCCESS,
        'size' => Button::SIZE_SMALL,
    ]) ?>

    <?= Button::widget([
        'label' => Yii::t('common', 'Закрыть'),
        'size' => Button::SIZE_SMALL,
        'attributes' => [
            'data-dismiss' => 'modal',
        ],
    ]) ?>
</div>

<div class="row-with-btn-start btn-generating-list padding-top-20 text-right">
    <?= Button::widget([
        'type' => 'submit',
        'label' => Yii::t('common', 'Сгенерировать лист'),
        'style' => Button::STYLE_SUCCESS,
        'size' => Button::SIZE_SMALL,
    ]) ?>

    <?= Button::widget([
        'label' => Yii::t('common', 'Закрыть'),
        'size' => Button::SIZE_SMALL,
        'attributes' => [
            'data-dismiss' => 'modal',
        ],
    ]) ?>
</div>

<div class="row-with-btn-stop btn-sending-list" style="display: none;">
    <?= Button::widget([
        'id' => 'stop_sending_list',
        'label' => Yii::t('common', 'Остановить'),
        'style' => Button::STYLE_DANGER . ' btn-block',
        'size' => Button::SIZE_SMALL,
    ]) ?>
</div>

<?php ActiveForm::end(); ?>
