<?php
use app\widgets\Modal;

/** @var string $url */
?>

<?= Modal::widget([
    'id' => 'modal_resend_in_call_center_return_no_prod',
    'title' => Yii::t('common', 'Отправка в очередь return_no_prod'),
    'body' => $this->render('_modal-body', [
        'url' => $url,
    ]),
]) ?>
