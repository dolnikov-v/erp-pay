<?php
use app\widgets\Modal;

/** @var string $url */
/** @var array $deliveries */
?>

<?= Modal::widget([
    'id' => 'modal_send_post_sale',
    'title' => Yii::t('common', 'Отправка на постпродажу'),
    'body' => $this->render('_modal-body', [
        'url' => $url,
    ]),
]) ?>
