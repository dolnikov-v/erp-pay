<?php
use app\modules\order\components\exporter\ExporterCsv;
use app\modules\order\components\exporter\ExporterExcel;
use app\modules\order\components\exporter\ExporterZip;
use app\modules\order\assets\ExportDropdownAsset;

/** @var string $name */
/** @var array $extraParams */

ExportDropdownAsset::register($this);
?>

<div id="orders_export_box">
    <div class="btn-group">
        <button id="orders_export_btn" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-share-square-o"></i> <?= Yii::t('common', $name) ?><span class="caret"></span>
        </button>
        <ul id="orders_export_dropdown" class="dropdown-menu dropdown-menu-right">
            <li>
                <a class="export-xls" data-url="<?= ExporterExcel::getExporterUrl($extraParams) ?>" href="#" tabindex="-1">
                    <i class="text-success fa fa-file-excel-o"></i> Excel
                </a>
            </li>
            <li>
                <a class="export-csv" data-url="<?= ExporterCsv::getExporterUrl($extraParams) ?>" href="#" tabindex="-1">
                    <i class="text-primary fa fa-file-code-o"></i> CSV
                </a>
            </li>
            <?php if (Yii::$app->user->can('order.index.export.zip')): ?>
            <li>
                <hr style="margin: 5px;">
                <a class="export-zip" data-url="<?= ExporterZip::getExporterUrl($extraParams) ?>" href="#" tabindex="-1">
                    <i class="text-primary fa fa-file-image-o"></i> Labels to ZIP
                </a>
            </li>
            <?php endif; ?>
        </ul>
    </div>
</div>
