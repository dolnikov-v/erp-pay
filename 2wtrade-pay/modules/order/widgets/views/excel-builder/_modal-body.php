<?php
use app\components\widgets\ActiveForm;
use app\helpers\WbIcon;
use app\widgets\Button;
use app\widgets\ProgressBar;
use yii\helpers\Html;
use app\modules\order\components\ExcelBuilder;
use app\widgets\custom\Checkbox;
use app\widgets\Select2;
use app\modules\order\models\OrderLogisticListExcel;

/** @var array $checkedColumns */
/** @var array $partColumnsOne */
/** @var array $partColumnsTwo */
/** @var array $partColumnsThree */
?>

<?php $form = ActiveForm::begin([
    'enableClientValidation' => false,
    'action' => ['/order/list/build-excel'],
    'method' => 'post',
]); ?>

<?= Html::hiddenInput('list_id', '', [
    'id' => 'excel_builder_list_id',
]) ?>

<div class="row-with-columns">
    <div class="row">
        <div class="col-xs-12 text-center font-size-16 margin-bottom-10">
            <?= Yii::t('common', 'Доступные колонки') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-4">
            <div class="col-choice-columns">
                <?php foreach ($partColumnsOne as $column): ?>
                    <?= Checkbox::widget([
                        'id' => 'column_' . md5($column),
                        'name' => 'columns[]',
                        'value' => $column,
                        'label' => ExcelBuilder::getColumnLabel($column),
                        'checked' => in_array($column, $checkedColumns) ? true : false,
                    ]) ?>
                <?php endforeach; ?>
            </div>
        </div>

        <div class="col-xs-4">
            <div class="col-choice-columns">
                <?php foreach ($partColumnsTwo as $column): ?>
                    <?= Checkbox::widget([
                        'id' => 'column_' . md5($column),
                        'name' => 'columns[]',
                        'value' => $column,
                        'label' => ExcelBuilder::getColumnLabel($column),
                        'checked' => in_array($column, $checkedColumns) ? true : false,
                    ]) ?>
                <?php endforeach; ?>
            </div>
        </div>

        <div class="col-xs-4">
            <div class="col-choice-columns">
                <?php foreach ($partColumnsThree as $column): ?>
                    <?= Checkbox::widget([
                        'id' => 'column_' . md5($column),
                        'name' => 'columns[]',
                        'value' => $column,
                        'label' => ExcelBuilder::getColumnLabel($column),
                        'checked' => in_array($column, $checkedColumns) ? true : false,
                    ]) ?>
                <?php endforeach; ?>
            </div>
        </div>
    </div>

    <div class="row margin-top-20">
        <div class="col-xs-12 text-center font-size-16 margin-bottom-10">
            <?= Yii::t('common', 'Формат листа') ?>
        </div>
        <div class="col-xs-12">
            <?= Select2::widget([
                'name' => 'format',
                'length' => -1,
                'items' => OrderLogisticListExcel::getFormatsCollection(),
            ]) ?>
        </div>
    </div>
</div>

<div class="row-with-text-progress" style="display: none;">
    <h5>
        <?= Yii::t('common', 'Генерация листа') ?>
        <span class="pull-right builder-percent">0%</span>
    </h5>
    <?= ProgressBar::widget() ?>
</div>

<div class="row-with-text-finish" style="display: none;">
    <div class="margin-bottom-15 text-center">
        <a id="excel_builder_link_download" class="btn btn-dark" href="#">
            <i class="icon <?= WbIcon::DOWNLOAD ?>"
               aria-hidden="true"></i> <?= Yii::t('common', 'Скачать сгенерированный лист') ?>
        </a>
    </div>
</div>

<div class="row-with-btn-start padding-top-20 text-right">
    <?= Button::widget([
        'type' => 'submit',
        'label' => Yii::t('common', 'Сгенерировать'),
        'style' => Button::STYLE_SUCCESS,
        'size' => Button::SIZE_SMALL,
    ]) ?>

    <?= Button::widget([
        'label' => Yii::t('common', 'Закрыть'),
        'size' => Button::SIZE_SMALL,
        'attributes' => [
            'data-dismiss' => 'modal',
        ],
    ]) ?>
</div>

<?php ActiveForm::end(); ?>
