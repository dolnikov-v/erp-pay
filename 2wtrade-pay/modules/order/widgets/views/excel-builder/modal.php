<?php
use app\widgets\Modal;

/** @var string $id */
/** @var string $title */
/** @var array $checkedColumns */
/** @var array $partColumnsOne */
/** @var array $partColumnsTwo */
/** @var array $partColumnsThree */
?>

<?= Modal::widget([
    'id' => $id,
    'size' => Modal::SIZE_LARGE,
    'title' => $title,
    'body' => $this->render('_modal-body', [
        'checkedColumns' => $checkedColumns,
        'partColumnsOne' => $partColumnsOne,
        'partColumnsTwo' => $partColumnsTwo,
        'partColumnsThree' => $partColumnsThree,
    ]),
]) ?>
