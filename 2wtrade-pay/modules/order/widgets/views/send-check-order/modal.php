<?php
use app\widgets\Modal;

/** @var string $url */
/** @var array $deliveries */
?>

<?= Modal::widget([
    'id' => 'modal_send_check_order',
    'title' => Yii::t('common', 'Отправка на проверку'),
    'body' => $this->render('_modal-body', [
        'url' => $url,
    ]),
]) ?>
