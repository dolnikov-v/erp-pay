<?php
use app\components\widgets\ActiveForm;
use app\widgets\Button;
use app\widgets\InputGroupFile;
use app\widgets\InputText;
use app\widgets\ProgressBar;
use app\widgets\Select2;
use yii\helpers\Html;

/** @var string $url */
/** @var array $deliveries */
/** @var array $pretensions */
?>

<?php $form = ActiveForm::begin([
    'enableClientValidation' => false,
    'action' => $url,
    'method' => 'post',
    'options' => [
        'enctype' => 'multipart/form-data',
    ]
]); ?>

<div class="row-with-text-start">
    <div class="text-center">
        <?= Yii::t('common', 'Вы действительно хотите отправить претензию в службу доставки ?') ?>
    </div>
    <span class="help-block text-center">
        <?= Yii::t('common', 'Будет создан лист претензии с выбранными заказами') ?>
    </span>
</div>

<div class="row-with-text-start">
    <div class="row padding-top-10">
        <div class="col-xs-1"></div>
        <div class="col-xs-10"><?= Yii::t('common', 'Служба доставки') ?>:
            <?= Select2::widget([
                'name' => 'delivery',
                'items' => $deliveries,
                'length' => -1,
                'autoEllipsis' => true,
            ]) ?>
        </div>
        <div class="col-xs-1"></div>
    </div>
    <div class="row padding-top-10">
        <div class="col-xs-1"></div>
        <div class="col-xs-10"><?= Yii::t('common', 'Претензия') ?>:
            <?= Select2::widget([
                'name' => 'pretension',
                'items' => $pretensions,
                'length' => -1,
                'autoEllipsis' => true,
            ]) ?>
        </div>
        <div class="col-xs-1"></div>
    </div>
</div>

<div class="row-with-text-stop" style="display: none;">
    <h5>
        <?= Yii::t('common', 'Отправка заказов') ?>
        <span class="pull-right sender-percent">0%</span>
    </h5>
    <?= ProgressBar::widget() ?>
</div>

<div class="row-with-btn-start padding-top-20 text-right">
    <?= Button::widget([
        'type' => 'submit',
        'label' => Yii::t('common', 'Отправить'),
        'style' => Button::STYLE_SUCCESS,
        'size' => Button::SIZE_SMALL,
    ]) ?>

    <?= Button::widget([
        'label' => Yii::t('common', 'Закрыть'),
        'size' => Button::SIZE_SMALL,
        'attributes' => [
            'data-dismiss' => 'modal',
        ],
    ]) ?>
</div>

<?php ActiveForm::end(); ?>
