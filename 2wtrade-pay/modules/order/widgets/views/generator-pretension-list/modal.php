<?php

use app\widgets\Modal;

/** @var string $url */
/** @var array $deliveries */
/** @var array $pretensions */
?>

<?= Modal::widget([
    'id' => 'modal_generate_pretension_list',
    'title' => Yii::t('common', 'Сформировать претензию'),
    'body' => $this->render('_modal-body', [
        'url' => $url,
        'deliveries' => $deliveries,
        'pretensions' => $pretensions,
    ]),
]) ?>
