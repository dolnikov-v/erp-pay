<?php
use app\widgets\Modal;

/** @var string $url */
/** @var array $smspoll */
?>

<?= Modal::widget([
    'id' => 'modal_send_sms_poll',
    'title' => Yii::t('common', 'СМС опрос'),
    'body' => $this->render('_modal-body', [
        'url' => $url,
        'smspoll' => $smspoll,
    ]),
]) ?>
