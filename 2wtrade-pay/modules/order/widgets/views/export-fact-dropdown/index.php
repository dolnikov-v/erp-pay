<?php

use app\modules\order\components\exporter\ExporterFactexcel;
use app\modules\order\assets\ExportDropdownAsset;
use \app\modules\order\models\search\OrderFinanceFactSearch;

/** @var string $name */
/** @var array $extraParams */
/**
 * @var bool $exportExcel
 * @var bool $exportCsv
 * @var bool $exportLabel
 */

ExportDropdownAsset::register($this);
?>

<div id="orders_export_box">
    <div class="btn-group">
        <button id="orders_export_btn" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-share-square-o"></i> <?= Yii::t('common', $name) ?><span class="caret"></span>
        </button>
        <ul id="orders_export_dropdown" class="dropdown-menu dropdown-menu-right">
            <li>
                <a class="export-xls" data-url="<?= ExporterFactexcel::getExporterUrl(array_merge($extraParams, ['dataType' => OrderFinanceFactSearch::DATA_TYPE_FACT])) ?>" href="#"
                   tabindex="-1">
                    <i class="text-success fa fa-file-excel-o"></i> <?= Yii::t('common', 'Фактические данные') ?>
                </a>
            </li>
            <li>
                <a class="export-csv" data-url="<?= ExporterFactexcel::getExporterUrl(array_merge($extraParams, ['dataType' => OrderFinanceFactSearch::DATA_TYPE_PREDICTION])) ?>" href="#"
                   tabindex="-1">
                    <i class="text-primary fa fa-file-excel-o"></i> <?= Yii::t('common', 'Расчетные данные') ?>
                </a>
            </li>
        </ul>
    </div>
</div>
