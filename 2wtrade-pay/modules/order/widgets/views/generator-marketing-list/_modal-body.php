<?php
use app\components\widgets\ActiveForm;
use app\widgets\Button;
use app\widgets\InputGroupFile;
use app\widgets\InputText;
use app\widgets\ProgressBar;
use yii\helpers\Html;

/** @var string $url */
/** @var array $deliveries */
?>

<?php $form = ActiveForm::begin([
    'enableClientValidation' => false,
    'action' => $url,
    'method' => 'post',
    'options' => [
        'enctype' => 'multipart/form-data',
    ]
]); ?>

<div class="row-with-text-start">
    <div class="text-center">
        <?= Yii::t('common', 'Вы действительно хотите добавить выбранные заказы в маркетинговый лист?') ?>
    </div>
    <span class="help-block text-center">
    </span>
</div>

<div class="row-with-text-start padding-top-10">
    <div class="row">
        <div class="col-xs-4">
            <label>
                <?= Yii::t('common', 'Название листа') ?>
            </label>
        </div>
        <div class="col-xs-8">
            <?= InputText::widget([
                'id' => 'generator_marketing_list_name',
                'name' => 'name'
            ]) ?>
        </div>
    </div>

</div>

<div class="row-with-text-stop" style="display: none;">
    <h5>
        <?= Yii::t('common', 'Отправка заказов') ?>
        <span class="pull-right sender-percent">0%</span>
    </h5>
    <?= ProgressBar::widget() ?>
</div>

<div class="row-with-btn-start padding-top-20 text-right">
    <?= Button::widget([
        'type' => 'submit',
        'label' => Yii::t('common', 'Отправить'),
        'style' => Button::STYLE_SUCCESS,
        'size' => Button::SIZE_SMALL,
    ]) ?>

    <?= Button::widget([
        'label' => Yii::t('common', 'Закрыть'),
        'size' => Button::SIZE_SMALL,
        'attributes' => [
            'data-dismiss' => 'modal',
        ],
    ]) ?>
</div>

<?php ActiveForm::end(); ?>
