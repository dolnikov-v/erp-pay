<?php
use app\widgets\Modal;

/** @var string $url */
?>

<?= Modal::widget([
    'id' => 'modal_generate_marketing_list',
    'title' => Yii::t('common', 'Создание маркетингового листа'),
    'body' => $this->render('_modal-body', [
        'url' => $url
    ]),
]) ?>
