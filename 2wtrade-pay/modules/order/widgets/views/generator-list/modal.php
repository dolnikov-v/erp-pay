<?php
use app\widgets\Modal;

/** @var string $url */
/** @var array $deliveries */
?>

<?= Modal::widget([
    'id' => 'modal_generate_list',
    'title' => Yii::t('common', 'Генерация листа'),
    'body' => $this->render('_modal-body', [
        'url' => $url,
        'deliveries' => $deliveries,
    ]),
]) ?>
