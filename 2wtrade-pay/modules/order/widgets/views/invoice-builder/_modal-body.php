<?php
use app\components\widgets\ActiveForm;
use app\helpers\WbIcon;
use app\modules\order\models\OrderLogisticListInvoice;
use app\widgets\Button;
use app\widgets\ProgressBar;
use yii\helpers\Html;

/** @var app\modules\order\models\OrderLogisticListInvoice $model */
?>

<?php $form = ActiveForm::begin([
    'enableClientValidation' => false,
    'action' => ['/order/list/build-invoice'],
    'method' => 'post',
]); ?>

<?= Html::hiddenInput('list_id', '', [
    'id' => 'invoice_builder_list_id',
]) ?>

<div class="row-with-text-start">
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'template')->select2List(OrderLogisticListInvoice::getTemplatesCollection()) ?>
        </div>

        <div class="col-lg-6">
            <?= $form->field($model, 'format')->select2List(OrderLogisticListInvoice::getFormatsCollection()) ?>
        </div>
    </div>
</div>

<div class="row-with-text-progress" style="display: none;">
    <h5>
        <?= Yii::t('common', 'Генерация накладных') ?>
        <span class="pull-right builder-percent">0%</span>
    </h5>
    <?= ProgressBar::widget() ?>
</div>

<div class="row-with-text-finish" style="display: none;">
    <div class="margin-bottom-15 text-center">
        <a id="invoice_builder_link_download" class="btn btn-dark" href="#">
            <i class="icon <?= WbIcon::DOWNLOAD ?>" aria-hidden="true"></i> <?= Yii::t('common', 'Скачать накладные') ?>
        </a>
    </div>
</div>

<div class="row-with-btn-start padding-top-20 text-right">
    <?= Button::widget([
        'type' => 'submit',
        'label' => Yii::t('common', 'Сгенерировать'),
        'style' => Button::STYLE_SUCCESS,
        'size' => Button::SIZE_SMALL,
    ]) ?>

    <?= Button::widget([
        'label' => Yii::t('common', 'Закрыть'),
        'size' => Button::SIZE_SMALL,
        'attributes' => [
            'data-dismiss' => 'modal',
        ],
    ]) ?>
</div>

<?php ActiveForm::end(); ?>
