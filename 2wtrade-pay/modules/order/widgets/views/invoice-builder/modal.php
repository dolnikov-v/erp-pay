<?php
use app\widgets\Modal;

/** @var app\modules\order\models\OrderLogisticListInvoice $model */
/** @var string $id */
/** @var string $title */
?>

<?= Modal::widget([
    'id' => $id,
    'title' => $title,
    'body' => $this->render('_modal-body', [
        'model' => $model,
    ]),
]) ?>
