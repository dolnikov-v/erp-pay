<?php

use app\components\widgets\ActiveForm;
use app\widgets\Button;
use app\widgets\custom\Checkbox;
use app\widgets\ProgressBar;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var string $url */
/** @var array $statuses */
?>

<?php $form = ActiveForm::begin([
    'enableClientValidation' => false,
    'action' => Url::toRoute('index'),
    'method' => 'post',
    'options' => [
        'data-sender-url' => $url,
    ]
]); ?>

<div class="row-with-text-start">
    <div class="text-center">
        <?= Yii::t('common', 'Выбранные заказы будут переданы в очередь check_undelivery в КЦ для проверки. Вы уверены?') ?>
    </div>
</div>

<div class="row-with-text-stop" style="display: none;">
    <h5>
        <?= Yii::t('common', 'Отправка заказов') ?>
        <span class="pull-right sender-percent">0%</span>
    </h5>
    <?= ProgressBar::widget() ?>
</div>

<div class="padding-top-20">
    <div class="row-with-btn-start text-right">

        <div class="row">
            <div class="col-lg-1"></div>
            <div class="col-lg-10">
            <?= Checkbox::widget([
                'id' => 'no_repeat',
                'name' => 'no_repeat',
                'style' => 'text-left pull-left',
                'label' => Html::tag('span', Yii::t('common', 'Не отправлять в check_undelivery ранее проверенные заказы')),
                'checked' => true
            ]) ?>
            </div>
            <div class="col-lg-1"></div>
        </div>

        <div class="row">
            <div class="col-lg-1"></div>
            <div class="col-lg-10">
            <?= Checkbox::widget([
                'id' => 'dont_ask',
                'name' => 'dont_ask',
                'style' => 'text-left pull-left',
                'label' => Html::tag('span', Yii::t('common', 'Не спрашивать больше'))
            ]) ?>

            <?= Button::widget([
                'id' => 'start_send_check_undelivery_order',
                'label' => Yii::t('common', 'Да'),
                'style' => Button::STYLE_SUCCESS,
                'size' => Button::SIZE_SMALL,
            ]) ?>

            <?= Button::widget([
                'label' => Yii::t('common', 'Закрыть'),
                'size' => Button::SIZE_SMALL,
                'attributes' => [
                    'data-dismiss' => 'modal',
                ],
            ]) ?>
            </div>
            <div class="col-lg-1"></div>
        </div>
    </div>

    <div class="row-with-btn-stop" style="display: none;">
        <?= Button::widget([
            'id' => 'stop_send_check_undelivery_order',
            'label' => Yii::t('common', 'Остановить'),
            'style' => Button::STYLE_DANGER . ' btn-block',
            'size' => Button::SIZE_SMALL,
        ]) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
