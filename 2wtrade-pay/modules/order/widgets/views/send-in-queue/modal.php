<?php
use app\widgets\Modal;

/** @var string $url */
/** @var array $queues */
?>

<?= Modal::widget([
    'id' => 'modal_send_in_queue',
    'title' => Yii::t('common', 'Отправить в очередь'),
    'body' => $this->render('_modal-body', [
        'url' => $url,
    ]),
]) ?>
