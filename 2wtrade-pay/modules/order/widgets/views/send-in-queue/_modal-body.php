<?php
use app\widgets\ButtonProgress;

?>

<div class="row-with-text-start">
    <?php if (Yii::$app->user->can('order.index.sendcheckorder')): ?>
        <div class="margin-bottom-5 text-center">
            <?= ButtonProgress::widget([
                'id' => 'btn_send_check_order',
                'style' => ButtonProgress::STYLE_INFO . ' width-400',
                'label' => Yii::t('common', 'Отправить на проверку в КЦ'),
            ]) ?>
        </div>
    <?php endif; ?>

    <?php if (Yii::$app->user->can('order.index.sendcheckundeliveryorder')): ?>
        <div class="margin-bottom-5 text-center">
            <?= ButtonProgress::widget([
                'id' => 'btn_send_check_undelivery_order',
                'style' => ButtonProgress::STYLE_INFO . ' width-400',
                'label' => Yii::t('common', 'Отправить на проверку в КЦ (Не выкуп)'),
            ]) ?>
        </div>
    <?php endif; ?>

    <?php if (Yii::$app->user->can('order.index.resendincallcenternewqueue')): ?>
        <div class="margin-bottom-5 text-center">
            <?= ButtonProgress::widget([
                'id' => 'btn_resend_in_call_center_new_queue',
                'style' => ButtonProgress::STYLE_INFO . ' width-400',
                'label' => Yii::t('common', 'Повторно отправить на обзвон (новая очередь)'),
            ]) ?>
        </div>
    <?php endif; ?>

    <?php if (Yii::$app->user->can('order.index.resendincallcenterstillwaiting')): ?>
        <div class="margin-bottom-5 text-center">
            <?= ButtonProgress::widget([
                'id'    => 'btn_resend_in_call_center_still_waiting',
                'style' => ButtonProgress::STYLE_INFO . ' width-400',
                'label' => Yii::t('common', 'Проверка спроса (после задержки)'),
            ]) ?>
        </div>
    <?php endif; ?>

    <?php if (Yii::$app->user->can('order.index.resendincallcenterreturnnoprod')): ?>
        <div class="margin-bottom-5 text-center">
            <?= ButtonProgress::widget([
                'id'    => 'btn_resend_in_call_center_return_no_prod',
                'style' => ButtonProgress::STYLE_INFO . ' width-400',
                'label' => Yii::t('common', 'Перепроверка товаров'),
            ]) ?>
        </div>
    <?php endif; ?>

    <?php if (Yii::$app->user->can('order.index.sendanalysistrash')): ?>
        <div class="margin-bottom-5 text-center">
            <?= ButtonProgress::widget([
                'id' => 'btn_send_analysis_trash',
                'style' => ButtonProgress::STYLE_INFO . ' width-400',
                'label' => Yii::t('common', 'Проанализировать трэши'),
            ]) ?>
        </div>
    <?php endif; ?>

    <?php if (Yii::$app->user->can('order.index.sendcheckaddress')): ?>
        <div class="margin-bottom-5 text-center">
            <?= ButtonProgress::widget([
                'id' => 'btn_send_check_address',
                'style' => ButtonProgress::STYLE_INFO . ' width-400',
                'label' => Yii::t('common', 'Проверка адреса'),
            ]) ?>
        </div>
    <?php endif; ?>

    <?php if (Yii::$app->user->can('order.index.sendpostsell')): ?>
        <div class="margin-bottom-5 text-center">
            <?= ButtonProgress::widget([
                'id' => 'btn_send_post_sale',
                'style' => ButtonProgress::STYLE_INFO . ' width-400',
                'label' => Yii::t('common', 'Постпродажа'),
            ]) ?>
        </div>
    <?php endif; ?>

    <?php if (Yii::$app->user->can('order.index.sendinformation')): ?>
        <div class="margin-bottom-5 text-center">
            <?= ButtonProgress::widget([
                'id' => 'btn_send_information',
                'style' => ButtonProgress::STYLE_INFO . ' width-400',
                'label' => Yii::t('common', 'Information'),
            ]) ?>
        </div>
    <?php endif; ?>

</div>