<?php
use app\widgets\Modal;

/** @var string $url */
/** @var array $deliveries */
?>

<?= Modal::widget([
    'id' => 'modal_resend_separate_order_into_delivery',
    'title' => Yii::t('common', 'Переотправка отдельными заказом(В службу доставки)'),
    'body' => $this->render('_modal-body', [
        'url' => $url,
        'deliveries' => $deliveries
    ]),
]) ?>
