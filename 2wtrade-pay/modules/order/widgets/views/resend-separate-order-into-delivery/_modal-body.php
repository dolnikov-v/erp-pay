<?php
use app\components\widgets\ActiveForm;
use app\widgets\Button;
use app\widgets\custom\Checkbox;
use app\widgets\ProgressBar;
use app\widgets\Select2;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var string $url */
/** @var array $deliveries */
?>

<?php $form = ActiveForm::begin([
    'enableClientValidation' => false,
    'action' => Url::toRoute('index'),
    'method' => 'post',
    'options' => [
        'data-sender-url' => $url,
    ]
]); ?>

<div class="row-with-text-start">
    <div class="text-center">
        <?= Yii::t('common', 'Для выбранных заказов будет создана их копия и отправлена в указанную службу доставки.') ?>
    </div>
    <span class="help-block text-center">
        <?= Yii::t('common', 'Выберите службу доставки, в какую хотите передать выбранные заказы.') ?>
    </span>
    <div class="row help-block">
        <div class="col-xs-2"></div>
        <div class="col-xs-8">
            <?= Select2::widget([
                'id' => 'resend_separate_order_into_delivery_select',
                'items' => $deliveries,
                'length' => -1,
                'autoEllipsis' => true,
            ]) ?>
        </div>
        <div class="col-xs-2"></div>

    </div>

    <?php if (Yii::$app->user->can('order.index.ignore_order_age')): ?>
        <div class="row">
            <div class="col-xs-2"></div>
            <div class="col-xs-6">
                <?= Checkbox::widget([
                    'id' => 'order_age_ignore',
                    'name' => 'order_age_ignore',
                    'value' => false,
                    'label' => Yii::t('common', 'Игнорировать возраст заказа'),
                    'checked' =>  false,
                ]) ?>
            </div>
        </div>
    <?php endif; ?>

</div>

<div class="row-with-text-stop" style="display: none;">
    <h5>
        <?= Yii::t('common', 'Передача заказов') ?>
        <span class="pull-right sender-percent">0%</span>
    </h5>
    <?= ProgressBar::widget() ?>
</div>

<div class="padding-top-20">
    <div class="row-with-btn-start text-right">
        <?= Button::widget([
            'id' => 'start_resend_separate_order_into_delivery',
            'label' => Yii::t('common', 'Создать копию'),
            'style' => Button::STYLE_SUCCESS,
            'size' => Button::SIZE_SMALL,
        ]) ?>

        <?= Button::widget([
            'label' => Yii::t('common', 'Закрыть'),
            'size' => Button::SIZE_SMALL,
            'attributes' => [
                'data-dismiss' => 'modal',
            ],
        ]) ?>
    </div>

    <div class="row-with-btn-stop" style="display: none;">
        <?= Button::widget([
            'id' => 'stop_resend_separate_order_into_delivery',
            'label' => Yii::t('common', 'Остановить'),
            'style' => Button::STYLE_DANGER . ' btn-block',
            'size' => Button::SIZE_SMALL,
        ]) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
