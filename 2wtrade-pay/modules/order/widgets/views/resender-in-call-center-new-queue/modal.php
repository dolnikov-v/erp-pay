<?php
use app\widgets\Modal;

/** @var string $url */
?>

<?= Modal::widget([
    'id' => 'modal_resend_in_call_center_new_queue',
    'title' => Yii::t('common', 'Повторная отправка на обзвон (новая очередь)'),
    'body' => $this->render('_modal-body', [
        'url' => $url,
    ]),
]) ?>
