<?php
use app\widgets\Modal;

/** @var string $url */
/** @var array $deliveries */
?>

<?= Modal::widget([
    'id' => 'modal_send_information',
    'title' => Yii::t('common', 'Отправка в очередь "information"'),
    'body' => $this->render('_modal-body', [
        'url' => $url,
    ]),
]) ?>
