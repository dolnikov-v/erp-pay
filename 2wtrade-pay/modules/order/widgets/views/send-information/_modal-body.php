<?php
use app\components\widgets\ActiveForm;
use app\widgets\Button;
use app\widgets\ProgressBar;
use yii\helpers\Url;
use yii\helpers\Html;
use app\widgets\custom\Checkbox;

/** @var string $url */
/** @var array $statuses */
?>

<?php $form = ActiveForm::begin([
    'enableClientValidation' => false,
    'action' => Url::toRoute('index'),
    'method' => 'post',
    'options' => [
        'data-sender-url' => $url,
    ]
]); ?>

<div class="row-with-text-start">
    <div class="text-center">
        <?= Yii::t('common', 'Выбранные заказы будут переданы в очередь "information". Вы уверены?') ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-2"></div>
    <div class="col-lg-8">
        <?= Checkbox::widget([
            'id' => 'send_information_no_repeat',
            'name' => 'SendInformation[no_repeat]',
            'style' => 'text-left pull-left',
            'label' => Html::tag('span', Yii::t('common', 'Не отправлять ранее отправленные заказы')),
            'checked' => true
        ]) ?>
    </div>
    <div class="col-lg-2"></div>
</div>

<div class="row">
    <div class="col-xs-2"></div>
    <div class="col-xs-8">
        <?= Html::textarea('SendInformation[comment]', '', [
            'id' => 'send_information_comment',
            'placeholder' => Yii::t('common', 'Комментарий'),
            'style' => 'width: 100%;'
        ]) ?>
    </div>
    <div class="col-xs-2"></div>
</div>

<div class="row-with-text-stop" style="display: none;">
    <h5>
        <?= Yii::t('common', 'Отправка заказов') ?>
        <span class="pull-right sender-percent">0%</span>
    </h5>
    <?= ProgressBar::widget() ?>
</div>

<div class="padding-top-20">
    <div class="row-with-btn-start text-right">
        <?= Button::widget([
            'id' => 'start_send_information',
            'label' => Yii::t('common', 'Да'),
            'style' => Button::STYLE_SUCCESS,
            'size' => Button::SIZE_SMALL,
        ]) ?>

        <?= Button::widget([
            'label' => Yii::t('common', 'Закрыть'),
            'size' => Button::SIZE_SMALL,
            'attributes' => [
                'data-dismiss' => 'modal',
            ],
        ]) ?>
    </div>

    <div class="row-with-btn-stop" style="display: none;">
        <?= Button::widget([
            'id' => 'stop_send_information',
            'label' => Yii::t('common', 'Остановить'),
            'style' => Button::STYLE_DANGER . ' btn-block',
            'size' => Button::SIZE_SMALL,
        ]) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
