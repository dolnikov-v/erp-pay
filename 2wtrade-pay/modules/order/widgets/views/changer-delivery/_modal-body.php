<?php
use app\components\widgets\ActiveForm;
use app\widgets\Button;
use app\widgets\custom\Checkbox;
use app\widgets\ProgressBar;
use app\widgets\Select2;
use yii\bootstrap\Html;
use yii\helpers\Url;

/** @var string $url */
/** @var array $statuses */
?>

<?php $form = ActiveForm::begin([
    'enableClientValidation' => false,
    'action' => Url::toRoute('index'),
    'method' => 'post',
    'options' => [
        'data-sender-url' => $url,
    ]
]); ?>

<div class="row-with-text-start">
    <div class="text-center">
        <?= Yii::t('common', 'Выберите доставку, на которую хотите сменить выбранные заказы.') ?>
    </div>
    <div class="row help-block">
        <div class="col-xs-2"></div>
        <div class="col-xs-8">
            <?= Select2::widget([
                'id' => 'changer_delivery_select',
                'items' => $deliveries,
                'length' => -1,
                'autoEllipsis' => true,
            ]) ?>
        </div>
        <div class="col-xs-2"></div>
    </div>

    <div class="row">
        <div class="col-xs-2"></div>
        <div class="col-xs-8">
            <?= Html::textarea('ChangerDelivery[comment]', '', [
                'id' => 'changer_delivery_comment',
                'placeholder' => Yii::t('common', 'Комментарий'),
                'style' => 'width: 100%;'
            ]) ?>
        </div>
        <div class="col-xs-2"></div>
    </div>
    <?php if (Yii::$app->user->can('order.index.group.changedeliveryid')): ?>
        <div class="row text-center">
            <div class="col-xs-2"></div>
            <div class="col-xs-8">
                <?= Checkbox::widget([
                    'label' => Yii::t('common', 'Изменить только ид без проверок'),
                    'id' => 'changer_delivery_ignore'
                ]) ?>
            </div>
            <div class="col-xs-2"></div>
        </div>

    <?php endif; ?>
</div>

<div class="row-with-text-stop" style="display: none;">
    <h5>
        <?= Yii::t('common', 'Отправка заказов') ?>
        <span class="pull-right sender-percent">0%</span>
    </h5>
    <?= ProgressBar::widget() ?>
</div>

<div class="padding-top-20">
    <div class="row-with-btn-start text-right">
        <?= Button::widget([
            'id' => 'start_changer_delivery',
            'label' => Yii::t('common', 'Сменить доставку'),
            'style' => Button::STYLE_SUCCESS,
            'size' => Button::SIZE_SMALL,
        ]) ?>

        <?= Button::widget([
            'label' => Yii::t('common', 'Закрыть'),
            'size' => Button::SIZE_SMALL,
            'attributes' => [
                'data-dismiss' => 'modal',
            ],
        ]) ?>
    </div>

    <div class="row-with-btn-stop" style="display: none;">
        <?= Button::widget([
            'id' => 'stop_changer_delivery',
            'label' => Yii::t('common', 'Остановить'),
            'style' => Button::STYLE_DANGER . ' btn-block',
            'size' => Button::SIZE_SMALL,
        ]) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
