<?php
use app\widgets\Modal;

/** @var string $url */
/** @var array $deliveries */
?>

<?= Modal::widget([
    'id' => 'modal_changer_delivery',
    'title' => Yii::t('common', 'Смена доставки'),
    'body' => $this->render('_modal-body', [
        'url' => $url,
        'deliveries' => $deliveries,
    ]),
]) ?>
