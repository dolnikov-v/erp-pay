<?php
use app\widgets\Modal;

/** @var string $url */
?>

<?= Modal::widget([
    'id' => 'modal_resender_in_delivery',
    'title' => Yii::t('common', 'Повторная передача в службу доставки'),
    'body' => $this->render('_modal-body', [
        'url' => $url,
    ]),
]) ?>
