<?php
use app\modules\order\assets\WorkflowEditorAsset;
use yii\web\View;
use yii\helpers\Url;

/** @var string $id */
/** @var \app\modules\order\models\OrderWorkflow $model */

WorkflowEditorAsset::register($this);

$this->registerJs("var workflowScheme = " . $model->scheme . ";", View::POS_HEAD);
$this->registerJs("var I18n = {};", View::POS_HEAD);
$this->registerJs("I18n['Успешно сохранено.'] = '" . Yii::t('common', 'Успешно сохранено.') . "';", View::POS_HEAD);
$this->registerJs("I18n['Не удалось сохранить.'] = '" . Yii::t('common', 'Не удалось сохранить.') . "';", View::POS_HEAD);
?>

<div class="col-md-12 wrap-editor" data-id-scheme="<?= $model->id ?>"
     data-url="<?= Url::toRoute('/order/workflow/save-workflow-scheme') ?>">
    <div class="row">
        <div id="<?= $id ?>" class="go-schema"></div>
    </div>

    <?php if (Yii::$app->user->can('order.workflow.saveworkflowscheme')): ?>
        <div class="row">
            <button
                class="btn btn-success btn-sm save-workflow-scheme margin-top-20"><?= Yii::t('common', 'Сохранить схему') ?></button>
            <button
                class="btn btn-info btn-sm layout-workflow-scheme margin-top-20"><?= Yii::t('common', 'Упорядочить схему') ?></button>
        </div>
    <?php endif; ?>
</div>
