<?php
use app\widgets\custom\Checkbox;

/** @var array $requireColumns */
/** @var array $manualColumns */
/** @var array $chosenColumns */
?>

<div class="row row-header">
    <div class="col-xs-6">
        <h5><?= Yii::t('common', 'Обязательные') ?></h5>
    </div>
    <div class="col-xs-6">
        <h5><?= Yii::t('common', 'На выбор') ?></h5>
    </div>
</div>
<div class="row row-columns">
    <div class="col-xs-6">
        <div class="col-require-columns">
            <?php foreach ($requireColumns as $columnKey => $columnLabel): ?>
                <div>
                    <?= Checkbox::widget([
                        'id' => 'column_' . md5($columnKey),
                        'label' => $columnLabel,
                        'checked' => true,
                        'disabled' => true,
                    ]) ?>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    <div class="col-xs-6">
        <div class="col-choice-columns">
            <?php foreach ($manualColumns as $columnKey => $columnLabel): ?>
                <div>
                    <?= Checkbox::widget([
                        'id' => 'column_' . md5($columnKey),
                        'label' => $columnLabel,
                        'checked' => in_array($columnKey, $chosenColumns),
                        'attributes' => [
                            'class' => 'shower-columns-checkbox',
                            'data-column' => $columnKey,
                        ],
                    ]) ?>
                </div>
            <?php endforeach; ?>
        </div>
    </div>

    <div class="block-loading" style="display: none;"></div>
</div>
