<?php
use app\widgets\Button;

?>

<div class="text-right">
    <?= Button::widget([
        'id' => 'apply_shower_columns',
        'label' => Yii::t('common', 'Применить'),
        'size' => Button::SIZE_SMALL,
        'style' => Button::STYLE_PRIMARY,
        'attributes' => [
            'data-dismiss' => 'modal',
        ],
    ]) ?>
    <?= Button::widget([
        'label' => Yii::t('common', 'Закрыть'),
        'size' => Button::SIZE_SMALL,
        'attributes' => [
            'data-dismiss' => 'modal',
        ],
    ]) ?>
</div>
