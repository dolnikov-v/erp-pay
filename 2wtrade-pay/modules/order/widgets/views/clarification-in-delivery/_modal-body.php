<?php
use app\components\widgets\ActiveForm;
use app\widgets\Button;
use app\widgets\ProgressBar;
use app\widgets\Select2;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var string $url */
/** @var string $deliveryUrl */
/** @var array $deliveries */
?>

<?php $form = ActiveForm::begin([
    'enableClientValidation' => false,
    'action' => Url::toRoute('index'),
    'method' => 'post',
    'options' => [
        'data-sender-url' => $url,
        'data-delivery-url' => $deliveryUrl,
    ]
]); ?>

<div class="row-with-text-start">
    <div class="text-center">
        <?= Yii::t('common', 'Будет отправлено письмо в службу доставки с выгрузкой файла со списком заказов.') ?>
    </div>
    <div class="row">
        <div class="col-xs-1"></div>
        <div class="col-xs-10 emails_holder"></div>
        <div class="col-xs-1"></div>
    </div>
    <div class="row">
        <div class="col-xs-1"></div>
        <div class="col-xs-10">
            <?= Yii::t('common', 'Тема') ?>:
            <?= Html::textInput(
                'message_subject',
                '',
                [
                    'id' => 'message_subject',
                    'class' => 'form-control'
                ]
            ) ?>
        </div>
        <div class="col-xs-1"></div>
    </div>
    <div class="row">
        <div class="col-xs-1"></div>
        <div class="col-xs-10">
            <?= Yii::t('common', 'Сообщение') ?>:
            <?= Html::textarea(
                'message_body',
                '',
                [
                    'id' => 'message_body',
                    'class' => 'form-control'
                ]
            ) ?>
        </div>
        <div class="col-xs-1"></div>
    </div>
</div>

<div class="row-with-text-stop" style="display: none;">
    <h5>
        <?= Yii::t('common', 'Передача заказов') ?>
        <span class="pull-right sender-percent">0%</span>
    </h5>
    <?= ProgressBar::widget() ?>
</div>

<div class="padding-top-20">
    <div class="row-with-btn-start text-right">
        <?= Button::widget([
            'id' => 'start_clarification_in_delivery',
            'label' => Yii::t('common', 'Передать'),
            'style' => Button::STYLE_SUCCESS,
            'size' => Button::SIZE_SMALL,
        ]) ?>

        <?= Button::widget([
            'label' => Yii::t('common', 'Закрыть'),
            'size' => Button::SIZE_SMALL,
            'attributes' => [
                'data-dismiss' => 'modal',
            ],
        ]) ?>
    </div>

    <div class="row-with-btn-stop" style="display: none;">
        <?= Button::widget([
            'id' => 'stop_clarification_in_delivery',
            'label' => Yii::t('common', 'Остановить'),
            'style' => Button::STYLE_DANGER . ' btn-block',
            'size' => Button::SIZE_SMALL,
        ]) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
