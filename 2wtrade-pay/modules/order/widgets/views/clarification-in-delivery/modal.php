<?php
use app\widgets\Modal;

/** @var string $url */
/** @var string $deliveryUrl */
/** @var array $deliveries */
?>

<?= Modal::widget([
    'id' => 'modal_clarification_in_delivery',
    'title' => Yii::t('common', 'Отправить на уточнение в службу доставки'),
    'body' => $this->render('_modal-body', [
        'url' => $url,
        'deliveryUrl' => $deliveryUrl
    ]),
]) ?>
