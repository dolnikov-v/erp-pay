<?php
use app\widgets\Modal;

/** @var string $url */
?>

<?= Modal::widget([
    'id' => 'modal_resend_in_call_center_still_waiting',
    'title' => Yii::t('common', 'Отправка в очередь still_waiting'),
    'body' => $this->render('_modal-body', [
        'url' => $url,
    ]),
]) ?>
