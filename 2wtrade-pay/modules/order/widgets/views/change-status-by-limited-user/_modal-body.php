<?php
use app\components\widgets\ActiveForm;
use app\widgets\Button;
use app\widgets\Select2;
use app\widgets\ProgressBar;
use yii\helpers\Url;

/** @var string $url */
/** @var array $statuses */
?>

<?php $form = ActiveForm::begin([
    'enableClientValidation' => false,
    'action' => Url::toRoute('index'),
    'method' => 'post',
    'options' => [
        'data-sender-url' => $url,
    ]
]); ?>

    <div class="row-with-text-start">
        <div class="text-center">
            <?= Yii::t('common', 'Выберите статус, на который хотите сменить выбранные заказы.') ?>
        </div>
        <div class="row help-block">
            <div class="col-xs-2"></div>
            <div class="col-xs-8">
                <?= Select2::widget([
                    'id' => 'changer_status_select',
                    'items' => $statuses,
                    'length' => -1,
                    'autoEllipsis' => true,
                ]) ?>
            </div>
            <div class="col-xs-2"></div>
        </div>
    </div>

    <div class="row-with-text-stop" style="display: none;">
        <h5>
            <?= Yii::t('common', 'Смена статусов заказов') ?>
            <span class="pull-right sender-percent">0%</span>
        </h5>
        <?= ProgressBar::widget() ?>
    </div>

    <div class="padding-top-20">
        <div class="row-with-btn-start text-right">
            <?= Button::widget([
                'id' => 'start_change_status_by_limited_user',
                'label' => Yii::t('common', 'Сменить статус'),
                'style' => Button::STYLE_SUCCESS,
                'size' => Button::SIZE_SMALL,
            ]) ?>

            <?= Button::widget([
                'label' => Yii::t('common', 'Закрыть'),
                'size' => Button::SIZE_SMALL,
                'attributes' => [
                    'data-dismiss' => 'modal',
                ],
            ]) ?>
        </div>

        <div class="row-with-btn-stop" style="display: none;">
            <?= Button::widget([
                'id' => 'stop_change_status_by_limited_user',
                'label' => Yii::t('common', 'Остановить'),
                'style' => Button::STYLE_DANGER . ' btn-block',
                'size' => Button::SIZE_SMALL,
            ]) ?>
        </div>
    </div>

<?php ActiveForm::end(); ?>