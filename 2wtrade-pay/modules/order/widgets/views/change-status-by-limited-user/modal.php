<?php
use app\widgets\Modal;

/** @var string $url */
/** @var array $statuses */
?>

<?= Modal::widget([
    'id' => 'modal_change_status_by_limited_user',
    'title' => Yii::t('common', 'Смена статуса заказов'),
    'body' => $this->render('_modal-body', [
        'url' => $url,
        'statuses' => $statuses,
    ]),
]) ?>
