<?php
use app\widgets\Modal;

/** @var string $url */
/** @var array $statuses */
?>

<?= Modal::widget([
    'id' => 'modal_changer_status',
    'title' => Yii::t('common', 'Смена статуса'),
    'body' => $this->render('_modal-body', [
        'url' => $url,
        'statuses' => $statuses,
    ]),
]) ?>
