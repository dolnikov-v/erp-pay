<?php
use app\widgets\Modal;

/** @var string $url */
/** @var array $statuses */
?>

<?= Modal::widget([
    'id' => 'modal_call_center_request_status_changer',
    'title' => Yii::t('common', 'Смена статуса заявки в КЦ'),
    'body' => $this->render('_modal-body', [
        'url' => $url,
        'statuses' => $statuses,
    ]),
]) ?>
