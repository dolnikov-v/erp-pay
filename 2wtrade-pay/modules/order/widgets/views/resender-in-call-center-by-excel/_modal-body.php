<?php
use app\components\widgets\ActiveForm;
use app\widgets\Button;
use app\widgets\InputText;
use app\widgets\Select2;
use app\widgets\InputGroupFile;

/** @var string $url */
?>

<?php $form = ActiveForm::begin([
    'enableClientValidation' => false,
    'action' => $url,
    'method' => 'post',
    'options' => [
        'enctype' => 'multipart/form-data',
    ]
]); ?>

<div class="row-with-text-start">
    <div class="text-center">
        <?= Yii::t('common', 'Выберите Excel-файл для формирования списка заказов') ?>
    </div>
</div>

<div class="row margin-top-10">
    <div class="col-xs-2"></div>
    <div class="col-xs-8">
        <?= InputGroupFile::widget([
            'right' => false,
            'input' => InputText::widget([
                'type' => 'file',
                'name' => 'OrdersExcelFile',
            ]),
        ]) ?>

    </div>
    <div class="col-xs-2"></div>

    <div class="row-with-text-start">
        <div class="col-xs-12" style="margin-top: 20px;">
            <div class="text-center">
                <?= Yii::t('common', 'Выберите номер колонки Excel-файла, в которой находятся id заказов') ?>
            </div>
        </div>
        <div class="col-xs-4"></div>
        <div class="col-xs-4">
            <?= Select2::widget([
                'id' => 'column_num_selector',
                'name' => 'column_num',
                'items' => ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'],
                'autoEllipsis' => true,
            ]) ?>
        </div>
        <div class="col-xs-4"></div>
    </div>
</div>

<div class="row-with-text-stop" style="display: none;">
    <h5>
        <?= Yii::t('common', 'Загрузка списка заказов. Пожалуйста ждите...') ?>
    </h5>
</div>

<div class="padding-top-20">
    <div class="row-with-btn-start text-right">
        <?= Button::widget([
            'type' => 'submit',
            'label' => Yii::t('common', 'Загрузить'),
            'style' => Button::STYLE_SUCCESS,
            'size' => Button::SIZE_SMALL,
        ]) ?>

        <?= Button::widget([
            'label' => Yii::t('common', 'Закрыть'),
            'size' => Button::SIZE_SMALL,
            'attributes' => [
                'data-dismiss' => 'modal',
            ],
        ]) ?>
    </div>

    <div class="row-with-btn-stop" style="display: none;">
        <?= Button::widget([
            'id' => 'stop_resend_in_call_center_by_excel',
            'label' => Yii::t('common', 'Остановить'),
            'style' => Button::STYLE_DANGER . ' btn-block',
            'size' => Button::SIZE_SMALL,
        ]) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
