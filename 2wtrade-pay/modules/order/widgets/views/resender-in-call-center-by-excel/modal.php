<?php
use app\widgets\Modal;

/** @var string $url */
?>

<?= Modal::widget([
    'id' => 'modal_resend_in_call_center_by_excel',
    'title' => Yii::t('common', 'Формирование списка заказов из Excel-файла'),
    'body' => $this->render('_modal-body', [
        'url' => $url,
    ]),
]) ?>
