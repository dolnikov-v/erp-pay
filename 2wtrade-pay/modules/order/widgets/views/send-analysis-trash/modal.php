<?php
use app\widgets\Modal;

/** @var string $url */
/** @var array $deliveries */
?>

<?= Modal::widget([
    'id' => 'modal_send_analysis_trash',
    'title' => Yii::t('common', 'Проанализировать трэши'),
    'body' => $this->render('_modal-body', [
        'url' => $url,
    ]),
]) ?>
