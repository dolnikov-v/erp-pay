<?php
use app\components\widgets\ActiveForm;
use app\modules\order\models\OrderLogisticListInvoice;
use app\widgets\Button;

/** @var app\modules\order\models\OrderLogisticListInvoice $model */
?>

<?php $form = ActiveForm::begin([
    'enableClientValidation' => false,
    'action' => ['/order/index/print-invoice'],
    'method' => 'post',
]); ?>

<div class="row-with-text-start">
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'template')->select2List(OrderLogisticListInvoice::getTemplatesCollection()) ?>
        </div>

        <div class="col-lg-6">
            <?= $form->field($model, 'format')->select2List(OrderLogisticListInvoice::getFormatsCollection()) ?>
        </div>
        <div class="col-lg-6">
        <a id="invoice-download-link" class="btn btn-success" download='' style="display:none;">Download invoice file</a>
        </div>
        <div class="col-lg-6" hidden>
            <?= $form->field($model, 'order_id')->textInput(['id' => 'invoice_order_id']) ?>
        </div>

        <div class="col-lg-6" hidden>
            <?= $form->field($model, 'list_id')->textInput(['id' => 'invoice_list_id']) ?>
        </div>
    </div>
</div>

<div class="row-with-btn-start padding-top-20 text-right">
    <?= Button::widget([
        'type' => 'submit',
        'label' => Yii::t('common', 'Печать'),
        'style' => Button::STYLE_SUCCESS,
        'size' => Button::SIZE_SMALL,
    ]) ?>

    <?= Button::widget([
        'label' => Yii::t('common', 'Закрыть'),
        'size' => Button::SIZE_SMALL,
        'attributes' => [
            'data-dismiss' => 'modal',
        ],
    ]) ?>
</div>

<?php ActiveForm::end(); ?>
