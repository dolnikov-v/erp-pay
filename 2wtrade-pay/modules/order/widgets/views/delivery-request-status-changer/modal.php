<?php
use app\widgets\Modal;

/** @var string $url */
/** @var array $statuses */
?>

<?= Modal::widget([
    'id' => 'modal_delivery_request_status_changer',
    'title' => Yii::t('common', 'Смена статуса заявки в курьерке'),
    'body' => $this->render('_modal-body', [
        'url' => $url,
        'statuses' => $statuses,
    ]),
]) ?>
