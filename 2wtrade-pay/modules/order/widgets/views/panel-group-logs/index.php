<?php

use app\components\grid\DateColumn;
use app\components\grid\GridView;
use app\helpers\logs\Route;
use app\widgets\Label;

/** @var \yii\data\ArrayDataProvider $dataProvider */
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'user.username',
        [
            'attribute' => 'route',
            'content' => function ($model) {

                $route = Route::getRouteGroup($model->route);

                $style = Label::STYLE_PRIMARY;
                if ($route == Route::GROUP_CALL_CENTER) {
                    $style = Label::STYLE_INFO;
                } elseif ($route == Route::GROUP_DELIVERY) {
                    $style = Label::STYLE_SUCCESS;
                }

                return Label::widget([
                    'label' => $route != "" ? Route::getGroupDescription($route) : Yii::t('common', 'Неопределено'),
                    'style' => $route != "" ? $style : Label::STYLE_DEFAULT
                ]);
            }
        ],
        'field',
        'old',
        'new',
        [
            'attribute' => 'comment',
            'value' => function ($model) {
                return $model->comment ?? '';
            }
        ],
        [
            'class' => DateColumn::className(),
            'attribute' => 'created_at',
            'enableSorting' => false,
            'label' => Yii::t('common', 'Дата')
        ]
    ]
]) ?>
