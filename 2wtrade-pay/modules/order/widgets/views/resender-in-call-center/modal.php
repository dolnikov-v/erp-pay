<?php
use app\widgets\Modal;

/** @var string $url */
?>

<?= Modal::widget([
    'id' => 'modal_resend_in_call_center',
    'title' => Yii::t('common', 'Повторная отправка на обзвон'),
    'body' => $this->render('_modal-body', [
        'url' => $url,
    ]),
]) ?>
