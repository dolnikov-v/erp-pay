<?php
use app\components\widgets\ActiveForm;
use app\widgets\Button;
use app\widgets\ProgressBar;
use yii\helpers\Html;

/** @var string $url */
// /** @var array $deliveries */
?>

<?php $form = ActiveForm::begin([
    'enableClientValidation' => false,
    'action' => $url,
    'method' => 'post',
    'options' => [
        'enctype' => 'multipart/form-data',
    ]
]); ?>

<div class="row-with-text-start">
    <div class="text-center">
        <?= Yii::t('common', 'Вы действительно хотите удалить выбранные заказы из листов?') ?>
    </div>
    <span class="help-block text-center">
        <?php
        echo Yii::t('common', 'Заказам будет выставлен статус <br> {status} <br>', [
            'status' => Html::tag('strong', Yii::t('common', 'Логистика (принято, ожидается генерация листа)')),
        ]);
        echo Yii::t('common', 'Процесс может занять продолжительное время - не закрывайте окно и дождитесь окончания операции.<br>Перегенерация затронутых листов будет проведена автоматически<br>Не забудьте сформировать заново накладные и этикетки.');
        ?>
    </span>
</div>

<div class="row-with-text-stop" style="display: none;">
    <h5>
        <?= Yii::t('common', 'Идет удаление заказов из листов, пожалуйста ждите...') ?>
        <span class="pull-right sender-percent">0%</span>
    </h5>
    <?= ProgressBar::widget() ?>
</div>

<div class="row-with-btn-start padding-top-20 text-right">
    <?= Button::widget([
        'type' => 'submit',
        'label' => Yii::t('common', 'Удалить'),
        'style' => Button::STYLE_DANGER,
        'size' => Button::SIZE_SMALL,
    ]) ?>

    <?= Button::widget([
        'label' => Yii::t('common', 'Закрыть'),
        'size' => Button::SIZE_SMALL,
        'attributes' => [
            'data-dismiss' => 'modal',
        ],
    ]) ?>
</div>


<div class="row-with-btn-stop" style="display: none;">
    <?= Button::widget([
        'id' => 'stop_delete_orders_from_lists',
        'label' => Yii::t('common', 'Остановить'),
        'style' => Button::STYLE_DANGER . ' btn-block',
        'size' => Button::SIZE_SMALL,
    ]) ?>
</div>

<?php ActiveForm::end(); ?>
