<?php
use app\widgets\Modal;

/** @var string $url */
/** @var array $deliveries */
?>

<?= Modal::widget([
    'id' => 'modal_delete_orders_from_lists',
    'title' => Yii::t('common', 'Удаление выбранных заказов из листов'),
    'body' => $this->render('_modal-body', [
        'url' => $url,
    ]),
]) ?>
