<?php
namespace app\modules\order\widgets;

use app\modules\order\models\OrderWorkflow;
use Yii;
use yii\base\Widget;

/**
 * Class WorkflowEditor
 * @package app\modules\order\widgets
 */
class WorkflowEditor extends Widget
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var OrderWorkflow
     */
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('workflow-editor/index', [
            'id' => $this->id,
            'model' => $this->model
        ]);
    }
}
