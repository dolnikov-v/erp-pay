<?php
namespace app\modules\order\widgets;

use app\modules\delivery\models\Delivery;
use Yii;
use yii\base\InvalidParamException;

/**
 * Class DeleteOrdersFromLists
 * @package app\modules\order\widgets
 */
class DeleteOrdersFromLists extends Sender
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('delete-orders-from-lists/modal', [
            'url' => $this->url,
        ]);
    }
}
