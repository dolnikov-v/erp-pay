<?php
namespace app\modules\order\widgets;

/**
 * Class ResendInQueue
 * @package app\modules\order\widgets
 */
class SendInQueue extends Sender
{

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('send-in-queue/modal', [
            'url' => $this->url,
        ]);
    }
}
