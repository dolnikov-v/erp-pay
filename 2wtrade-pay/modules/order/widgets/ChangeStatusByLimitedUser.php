<?php
namespace app\modules\order\widgets;

use app\modules\order\models\OrderStatus;
use Yii;

/**
 * Class ChangeStatusByLimitedUser
 * @package app\modules\order\widgets
 */
class ChangeStatusByLimitedUser extends Sender
{

    /**
     * @return array
     */
    public static function getAllowStatuses()
    {
        $ids = OrderStatus::getBuyoutList();
        foreach (OrderStatus::getNotBuyoutList() as $status) {
            $ids[] = $status;
        };
        $ids = array_unique($ids);

        $statuses = OrderStatus::find()
            ->where(['in', 'id', $ids])
            ->collection();

        unset($statuses[OrderStatus::STATUS_FINANCE_MONEY_RECEIVED]);
        unset($statuses[OrderStatus::STATUS_LOGISTICS_REJECTED]);
        unset($statuses[OrderStatus::STATUS_LOGISTICS_ACCEPTED]);

        return $statuses;
    }

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('change-status-by-limited-user/modal', [
            'url' => $this->url,
            'statuses' => self::getAllowStatuses(),
        ]);
    }
}
