<?php
namespace app\modules\order\widgets;

/**
 * Class ResenderInCallCenterNewQueue
 * @package app\modules\order\widgets
 */
class ResenderInCallCenterNewQueue extends Sender
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('resender-in-call-center-new-queue/modal', [
            'url' => $this->url,
        ]);
    }
}
