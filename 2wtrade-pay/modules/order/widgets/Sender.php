<?php
namespace app\modules\order\widgets;

use Yii;
use yii\base\NotSupportedException;
use yii\base\Widget;

/**
 * Class Sender
 * @package app\modules\order\widgets
 */
class Sender extends Widget
{
    /**
     * @var string
     */
    public $url;

    /**
     * @throws NotSupportedException
     */
    public function run()
    {
        throw new NotSupportedException(Yii::t('common', 'Необходима реализация метода run().'));
    }
}
