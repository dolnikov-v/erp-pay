<?php
namespace app\modules\order\widgets;

use app\modules\order\models\OrderLogisticListInvoice;
use app\widgets\Modal;
use Yii;
use yii\base\InvalidParamException;

/**
 * Class InvoiceBuilder
 * @package app\modules\order\widgets
 */
class InvoiceBuilder extends Modal
{
    /**
     * @var OrderLogisticListInvoice
     */
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        if (!$this->model instanceof OrderLogisticListInvoice) {
            throw new InvalidParamException(Yii::t('common', 'Необходимо инициализировать форму.'));
        }

        return $this->render('invoice-builder/modal', [
            'model' => $this->model,
            'id' => 'modal_invoice_builder',
            'title' => Yii::t('common', 'Генерация накладных'),
        ]);
    }
}
