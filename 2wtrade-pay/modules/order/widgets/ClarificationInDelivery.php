<?php
namespace app\modules\order\widgets;

use Yii;

/**
 * Class ClarificationInDelivery
 * @package app\modules\order\widgets
 */
class ClarificationInDelivery extends Sender
{
    /**
     * @var string
     */
    public $deliveryUrl;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('clarification-in-delivery/modal', [
            'url' => $this->url,
            'deliveryUrl' => $this->deliveryUrl,
        ]);
    }
}
