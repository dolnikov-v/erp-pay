<?php

namespace app\modules\order\widgets;

use app\modules\order\models\OrderLogisticList;

/**
 * Class GeneratorPretensionList
 * @package app\modules\order\widgets
 */
class GeneratorPretensionList extends GeneratorList
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('generator-pretension-list/modal', [
            'url' => $this->url,
            'deliveries' => self::getAllowDelivery(),
            'pretensions' => OrderLogisticList::getPretensionTypesCollection(),
        ]);
    }
}
