<?php
namespace app\modules\order\widgets;

/**
 * Class ResenderInCallCenterReturnNoProd
 * @package app\modules\order\widgets
 */
class ResenderInCallCenterReturnNoProd extends Sender
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('resender-in-call-center-return-no-prod/modal', [
            'url' => $this->url,
        ]);
    }
}
