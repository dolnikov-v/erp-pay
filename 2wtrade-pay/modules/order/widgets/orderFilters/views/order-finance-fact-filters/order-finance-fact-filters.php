<?php

use app\widgets\Panel;

/* @var yii\web\View $this */
/* @var app\modules\order\models\search\OrderSearch $modelSearch */
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'content' => $this->render('_nav-tab-filter', [
        'modelSearch' => $modelSearch,
    ]),
    'collapse' => true,
]) ?>
