<?php
use app\components\widgets\ActiveForm;
use app\helpers\WbIcon;
use app\modules\order\widgets\orderFilters\filters\NumberFilter;
use app\modules\order\widgets\orderFilters\filters\TextFilter;
use app\widgets\Button;
use app\widgets\Select2;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var app\modules\order\models\search\OrderSearch $modelSearch */
?>

<?php $form = ActiveForm::begin([
    'enableClientValidation' => false,
    'action' => Url::toRoute('index'),
    'method' => 'get',
]); ?>


<?= $modelSearch->restore($form) ?>

<div id="row_selectable_filters" class="row">
    <div class="col-lg-12">
        <div class="form-group">
            <div class="control-label">
                <label><?= Yii::t('common', 'Параметр поиска') ?></label>
            </div>
            <div class="row">
                <div class="col-xs-4">
                    <div id="container_selectable_filters" class="container-selectable-filters">
                        <?= Select2::widget([
                            'id' => 'filter_orders_selectable_filters',
                            'items' => $modelSearch->selectableFilters,
                            'length' => false,
                        ]) ?>
                    </div>
                </div>
                <div class="col-xs-8">
                    <?= Button::widget([
                        'id' => 'add_selectable_filters',
                        'style' => Button::STYLE_SUCCESS . ' add-selectable-filters',
                        'icon' => WbIcon::PLUS,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <?= $form->submit(Yii::t('common', 'Применить')) ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

<div id="container_empty_filters" style="display: none;">
    <?= NumberFilter::widget(['form' => $form]) ?>
    <?= TextFilter::widget(['form' => $form]) ?>
</div>