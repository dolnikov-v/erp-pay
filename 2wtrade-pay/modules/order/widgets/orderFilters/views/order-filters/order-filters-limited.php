<?php
use app\widgets\Nav;
use app\widgets\Panel;

/* @var yii\web\View $this */
/* @var app\modules\order\models\search\OrderSearch $modelSearch */
?>

<?= Panel::widget([
    'nav' => new Nav([
        'tabs' => [
            [
                'label' => Yii::t('common', 'Фильтр поиска'),
                'content' => $this->render('_nav-tab-filter-limited', [
                    'modelSearch' => $modelSearch,
                ]),
            ],
        ]
    ]),
]) ?>
