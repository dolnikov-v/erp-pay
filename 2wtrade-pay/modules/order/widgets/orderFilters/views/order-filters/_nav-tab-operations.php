<?php
use app\assets\vendor\BootstrapLaddaAsset;
use app\components\widgets\ActiveForm;
use app\widgets\ButtonProgress;
use yii\helpers\Url;

BootstrapLaddaAsset::register($this);
?>

<?php $form = ActiveForm::begin([
    'enableClientValidation' => false,
    'action' => Url::toRoute('index'),
    'method' => 'get',
]); ?>

<?php
$marginBottomCallCenter = Yii::$app->user->can('order.index.createcallcenterrequest') || Yii::$app->user->can('order.index.resendcallcenterrequest') || Yii::$app->user->can('order.index.resendincallcenter')  || Yii::$app->user->can('order.index.sendanalysistrash');
$marginBottomDelivery = Yii::$app->user->can('order.index.sendindelivery') || Yii::$app->user->can('order.index.resendindelivery') || Yii::$app->user->can('order.index.clarificationindelivery') || Yii::$app->user->can('order.index.seconddeliveryattempt');
$marginBottomLogistic = Yii::$app->user->can('order.index.generatelist') || Yii::$app->user->can('order.index.generatepretensionlist');
$marginButtomSMSPoll = Yii::$app->user->can('order.index.sendsmspoll');
?>

<div class="row">
    <div class="col-md-4">

        <?php if (Yii::$app->user->can('order.index.resendincallcenter')): ?>
            <div class="margin-bottom-5">
                <?= ButtonProgress::widget([
                    'id' => 'btn_resend_in_call_center_by_excel',
                    'style' => ButtonProgress::STYLE_WARNING . ' width-300',
                    'label' => Yii::t('common', 'Загрузить список заказов из Excel-файла'),
                ]) ?>
            </div>
        <?php endif; ?>

        <?php if (Yii::$app->user->can('order.index.changestatus')): ?>
            <div class="margin-bottom-5">
                <?= ButtonProgress::widget([
                    'id' => 'btn_changer_status',
                    'style' => ButtonProgress::STYLE_DARK . ' width-300',
                    'label' => Yii::t('common', 'Сменить статус'),
                ]) ?>
            </div>
        <?php endif; ?>

        <?php if (Yii::$app->user->can('order.index.changedelivery')): ?>
            <div class="margin-bottom-5">
                <?= ButtonProgress::widget([
                    'id' => 'btn_changer_delivery',
                    'style' => ButtonProgress::STYLE_DARK . ' width-300',
                    'label' => Yii::t('common', 'Сменить доставку'),
                ]) ?>
            </div>
        <?php endif; ?>

        <?php if (Yii::$app->user->can('order.index.changecallcenterrequeststatus')): ?>
            <div class="margin-bottom-5">
                <?= ButtonProgress::widget([
                    'id' => 'btn_call_center_request_status_changer',
                    'style' => ButtonProgress::STYLE_DARK . ' width-300',
                    'label' => Yii::t('common', 'Сменить статус заявки в КЦ'),
                ]) ?>
            </div>
        <?php endif; ?>

        <?php if (Yii::$app->user->can('order.index.changedeliveryrequeststatus')): ?>
            <div class="margin-bottom-5">
                <?= ButtonProgress::widget([
                    'id' => 'btn_delivery_request_status_changer',
                    'style' => ButtonProgress::STYLE_DARK . ' width-300',
                    'label' => Yii::t('common', 'Сменить статус заявки в КС'),
                ]) ?>
            </div>
        <?php endif; ?>

        <?php if (Yii::$app->user->can('order.index.resendseparateorder')): ?>
            <div class="margin-bottom-5">
                <?= ButtonProgress::widget([
                    'id' => 'btn_resend_separate_order',
                    'style' => ButtonProgress::STYLE_DARK . ' width-300',
                    'label' => Yii::t('common', 'Переотправить отдельным заказом(в КЦ)'),
                ]) ?>
            </div>
        <?php endif; ?>
        <?php if (Yii::$app->user->can('order.index.resendseparateorderintodelivery')): ?>
            <div class="margin-bottom-5">
                <?= ButtonProgress::widget([
                    'id' => 'btn_resend_separate_order_into_delivery',
                    'style' => ButtonProgress::STYLE_DARK . ' width-300',
                    'label' => Yii::t('common', 'Переотправить отдельным заказом(в КС)'),
                ]) ?>
            </div>
        <?php endif; ?>

        <?php if (Yii::$app->user->can('order.index.transfertodistributor')): ?>
            <div class="margin-bottom-5">
                <?= ButtonProgress::widget([
                    'id' => 'btn_transfer_to_distributor',
                    'style' => ButtonProgress::STYLE_DARK . ' width-300',
                    'label' => Yii::t('common', 'Передать дистрибьютору'),
                ]) ?>
            </div>
        <?php endif; ?>

        <?php if (Yii::$app->user->can('order.index.generatemarketinglist')): ?>
            <div class="margin-bottom-5 margin-top-30">
                <?= ButtonProgress::widget([
                    'id' => 'btn_generate_marketinglist',
                    'style' => ButtonProgress::STYLE_WARNING . ' width-300',
                    'label' => Yii::t('common', 'Создать маркетинговый лист'),
                ]) ?>
            </div>
        <?php endif; ?>

    </div>

    <?php if ($marginButtomSMSPoll): ?>
        <div class="col-md-8">
            <?php if (Yii::$app->user->can('order.index.sendsmspoll')): ?>
                <div class="margin-bottom-30 text-right">
                    <?= ButtonProgress::widget([
                        'id' => 'btn_send_sms_poll',
                        'style' => ButtonProgress::STYLE_SUCCESS . ' width-400',
                        'label' => Yii::t('common', 'СМС опрос'),
                    ]) ?>
                </div>
            <?php endif; ?>
        </div>
    <?php endif; ?>

    <?php if ($marginBottomCallCenter): ?>
        <div class="col-md-8">
            <?php if (Yii::$app->user->can('order.index.createcallcenterrequest')): ?>
                <div class="margin-bottom-5 text-right">
                    <?= ButtonProgress::widget([
                        'id' => 'btn_send_in_call_center',
                        'style' => ButtonProgress::STYLE_INFO . ' width-400',
                        'label' => Yii::t('common', 'Отправить на обзвон'),
                    ]) ?>
                </div>
            <?php endif; ?>

            <?php if (Yii::$app->user->can('order.index.resendincallcenter')): ?>
                <div class="margin-bottom-5 text-right">
                    <?= ButtonProgress::widget([
                        'id' => 'btn_resend_in_call_center',
                        'style' => ButtonProgress::STYLE_INFO . ' width-400',
                        'label' => Yii::t('common', 'Повторно отправить на обзвон'),
                    ]) ?>
                </div>
            <?php endif; ?>

            <?php if (Yii::$app->user->can('order.index.retrygetstatusfromcallcenter')): ?>
                <div class="margin-bottom-5 text-right">
                    <?= ButtonProgress::widget([
                        'id' => 'btn_retry_get_status_from_call_center',
                        'style' => ButtonProgress::STYLE_INFO . ' width-400',
                        'label' => Yii::t('common', 'Обновить статус заявки в КЦ'),
                    ]) ?>
                </div>
            <?php endif; ?>

            <?php if (Yii::$app->user->can('order.index.resendincallcenterstillwaiting')
                || Yii::$app->user->can('order.index.resendincallcenterreturnnoprod')
                || Yii::$app->user->can('order.index.resendincallcenternewqueue')
                || Yii::$app->user->can('order.index.sendcheckorder')
                || Yii::$app->user->can('order.index.sendcheckundeliveryorder')
                || Yii::$app->user->can('order.index.sendanalysistrash')): ?>
            <div class="margin-bottom-5 text-right">
                <?= ButtonProgress::widget([
                    'id' => 'btn_send_in_queue',
                    'style' => ButtonProgress::STYLE_INFO . ' width-400',
                    'label' => Yii::t('common', 'Отправить в очередь'),
                ]) ?>
            </div>
            <?php endif; ?>

        </div>
    <?php endif; ?>
</div>

<?php if ($marginBottomDelivery): ?>
    <div class="row margin-top-30">
        <div class="col-md-4"></div>
        <div class="col-md-8">
            <?php if (Yii::$app->user->can('order.index.sendindelivery')): ?>
                <div class="margin-bottom-5 text-right">
                    <?= ButtonProgress::widget([
                        'id' => 'btn_sender_in_delivery',
                        'style' => ButtonProgress::STYLE_INFO . ' width-400',
                        'label' => Yii::t('common', 'Передать в службу доставки'),
                    ]) ?>
                </div>
            <?php endif; ?>

            <?php if (Yii::$app->user->can('order.index.resendindelivery')): ?>
                <div class="margin-bottom-5 text-right">
                    <?= ButtonProgress::widget([
                        'id' => 'btn_resender_in_delivery',
                        'style' => ButtonProgress::STYLE_INFO . ' width-400',
                        'label' => Yii::t('common', 'Повторно передать в службу доставки'),
                    ]) ?>
                </div>
            <?php endif; ?>

            <?php if (Yii::$app->user->can('order.index.clarificationindelivery')): ?>
                <div class="margin-bottom-5 text-right">
                    <?= ButtonProgress::widget([
                        'id' => 'btn_clarification_in_delivery',
                        'style' => ButtonProgress::STYLE_INFO . ' width-400',
                        'label' => Yii::t('common', 'Отправить на уточнение в службу доставки'),
                    ]) ?>
                </div>
            <?php endif; ?>

            <?php if (Yii::$app->user->can('order.index.seconddeliveryattempt')): ?>
                <div class="margin-bottom-5 text-right">
                    <?= ButtonProgress::widget([
                        'id' => 'btn_second_delivery_attempt',
                        'style' => ButtonProgress::STYLE_INFO . ' width-400',
                        'label' => Yii::t('common', '2 попытка доставки'),
                    ]) ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
<?php endif; ?>

<?php if ($marginBottomLogistic): ?>
    <div class="row margin-top-30">
        <div class="col-md-4"></div>
        <div class="col-md-8">
            <?php if (Yii::$app->user->can('order.index.generatelist')): ?>
                <div class="margin-bottom-5 text-right">
                    <?= ButtonProgress::widget([
                        'id' => 'btn_generate_list',
                        'style' => ButtonProgress::STYLE_INFO . ' width-400',
                        'label' => Yii::t('common', 'Генерация листа'),
                    ]) ?>
                </div>
            <?php endif; ?>

            <?php if (Yii::$app->user->can('order.index.generatelist')): ?>
                <div class="margin-bottom-5 text-right">
                    <?= ButtonProgress::widget([
                        'id' => 'btn_delete_orders_from_lists',
                        'style' => ButtonProgress::STYLE_DANGER . ' width-400',
                        'label' => Yii::t('common', 'Удаление выбранных заказов из листов'),
                    ]) ?>
                </div>
            <?php endif; ?>


            <?php if (Yii::$app->user->can('order.index.generatepretensionlist')): ?>
                <div class="margin-bottom-5 text-right">
                    <?= ButtonProgress::widget([
                        'id' => 'btn_generate_pretension_list',
                        'style' => ButtonProgress::STYLE_DARK . ' width-400',
                        'label' => Yii::t('common', 'Сформировать претензию'),
                    ]) ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
<?php endif; ?>

<?php ActiveForm::end(); ?>
