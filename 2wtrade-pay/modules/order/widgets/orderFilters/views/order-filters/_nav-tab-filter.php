<?php
use app\components\widgets\ActiveForm;
use app\helpers\WbIcon;
use app\modules\order\widgets\ExportDropdown;
use app\modules\order\widgets\orderFilters\filters\CallCenterFilter;
use app\modules\order\widgets\orderFilters\filters\CheckingTypeFilter;
use app\modules\order\widgets\orderFilters\filters\DateFilter;
use app\modules\order\widgets\orderFilters\filters\DeliveryFilter;
use app\modules\order\widgets\orderFilters\filters\DeliveryClarificationFilter;
use app\modules\order\widgets\orderFilters\filters\DeliveryDelayFilter;
use app\modules\order\widgets\orderFilters\filters\DeliveryExpressFilter;
use app\modules\order\widgets\orderFilters\filters\DeliveryZipGroupFilter;
use app\modules\order\widgets\orderFilters\filters\NoDoubleFilter;
use app\modules\order\widgets\orderFilters\filters\NumberFilter;
use app\modules\order\widgets\orderFilters\filters\OriginFilter;
use app\modules\order\widgets\orderFilters\filters\ProductFilter;
use app\modules\order\widgets\orderFilters\filters\LeadProductFilter;
use app\modules\order\widgets\orderFilters\filters\StatusCallCenterRequestFilter;
use app\modules\order\widgets\orderFilters\filters\StatusDeliveryRequestFilter;
use app\modules\order\widgets\orderFilters\filters\StatusFilter;
use app\modules\order\widgets\orderFilters\filters\TextFilter;
use app\widgets\Button;
use app\widgets\Select2;
use yii\helpers\Url;
use app\modules\order\widgets\orderFilters\filters\DeliveryPartnerFilter;
use app\modules\order\widgets\orderFilters\filters\TermsOfShippingFilter;
use app\modules\order\widgets\orderFilters\filters\DeliveryTermsFilter;
use app\modules\order\widgets\orderFilters\filters\MarketingListFilter;
use app\modules\order\widgets\orderFilters\filters\AutoCheckAddressFilter;
use app\modules\order\widgets\orderFilters\filters\UnshippingReasonFilter;
use app\modules\order\widgets\orderFilters\filters\PretensionFilter;
use app\modules\order\widgets\orderFilters\filters\DebtsFilter;

/** @var yii\web\View $this */
/** @var app\modules\order\models\search\OrderSearch $modelSearch */
?>

<?php $form = ActiveForm::begin([
    'enableClientValidation' => false,
    'action' => Url::toRoute('index'),
    'method' => 'get',
]); ?>

<?= DateFilter::widget([
    'form' => $form,
    'model' => $modelSearch->getDateFilter(),
]) ?>

<?= $modelSearch->restore($form) ?>

<div id="row_selectable_filters" class="row">
    <div class="col-lg-12">
        <div class="form-group">
            <div class="control-label">
                <label><?= Yii::t('common', 'Параметр поиска') ?></label>
            </div>
            <div class="row">
                <div class="col-xs-4">
                    <div id="container_selectable_filters" class="container-selectable-filters">
                        <?= Select2::widget([
                            'id' => 'filter_orders_selectable_filters',
                            'items' => $modelSearch->selectableFilters,
                            'length' => false,
                        ]) ?>
                    </div>
                </div>
                <div class="col-xs-8">
                    <?= Button::widget([
                        'id' => 'add_selectable_filters',
                        'style' => Button::STYLE_SUCCESS . ' add-selectable-filters',
                        'icon' => WbIcon::PLUS,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <?= $form->submit(Yii::t('common', 'Применить')) ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')) ?>

        <?php
        if (Yii::$app->session->has('orderlist-is-loaded-from-excel')) {
            echo $form->link(Yii::t('common', 'Отключить режим "из Excel"'), Url::toRoute('index/excel-order-list-clear'));
        }
        ?>

        <?php if (Yii::$app->user->can('order.index.export')): ?>
            <div class="pull-right">
                <?= ExportDropdown::widget() ?>
            </div>
        <?php endif; ?>
        <?php /* Тут должно быть свой отдельный can ? Если вообще эта функция актуальна */?>
        <?php if (Yii::$app->user->can('order.index.export')):?>
            <?php if (isset($modelSearch->getDeliveryClarificationFilter()->list[0]) && $modelSearch->getDeliveryClarificationFilter()->list[0]): ?>
                <div class="pull-right" style="margin-right: 20px">
                    <?= ExportDropdown::widget(['name' => 'Экспорт и отправка на уточнение в службу доставки', 'extraParams' => ['clarification' => 'send']]) ?>
                </div>
            <?php endif; ?>
        <?php endif; ?>

    </div>
</div>


<?php ActiveForm::end(); ?>

<div id="container_empty_filters" style="display: none;">
    <?= StatusFilter::widget(['form' => $form]) ?>
    <?= ProductFilter::widget(['form' => $form]) ?>
    <?= LeadProductFilter::widget(['form' => $form]) ?>
    <?= NumberFilter::widget(['form' => $form]) ?>
    <?= TextFilter::widget(['form' => $form]) ?>
    <?= DeliveryFilter::widget(['form' => $form]) ?>
    <?= DeliveryZipGroupFilter::widget(['form' => $form]) ?>
    <?= NoDoubleFilter::widget(['form' => $form]) ?>
    <?= DeliveryClarificationFilter::widget(['form' => $form]) ?>
    <?= DeliveryDelayFilter::widget(['form' => $form]) ?>
    <?= DeliveryExpressFilter::widget(['form' => $form]) ?>
    <?= CallCenterFilter::widget(['form' => $form]) ?>
    <?= StatusCallCenterRequestFilter::widget(['form' => $form]) ?>
    <?= StatusDeliveryRequestFilter::widget(['form' => $form]) ?>
    <?= OriginFilter::widget(['form' => $form]) ?>
    <?= DeliveryPartnerFilter::widget(['form' => $form]) ?>
    <?= TermsOfShippingFilter::widget(['form' => $form]) ?>
    <?= DeliveryTermsFilter::widget(['form' => $form]) ?>
    <?= MarketingListFilter::widget(['form' => $form]) ?>
    <?= CheckingTypeFilter::widget(['form' => $form]) ?>
    <?= AutoCheckAddressFilter::widget(['form' => $form]) ?>
    <?= UnshippingReasonFilter::widget(['form' => $form]) ?>
    <?= PretensionFilter::widget(['form' => $form]) ?>
    <?= DebtsFilter::widget(['form' => $form]) ?>
</div>


