<?php
namespace app\modules\order\widgets\orderFilters;

use app\modules\order\models\search\OrderSearch;
use yii\base\InvalidParamException;
use yii\base\Widget;
use Yii;

/**
 * Class OrderFilters
 * @package app\modules\order\widgets
 */
class OrderFilters extends Widget
{
    /** @var OrderSearch */
    public $modelSearch;

    /**
     * @return string
     */
    public function run()
    {
        if (!$this->modelSearch instanceof OrderSearch) {
            throw new InvalidParamException(Yii::t('common', 'Необходимо указать корректную модель поиска.'));
        }

        if (Yii::$app->user->isDeliveryManager) {
            return $this->render('order-filters/order-filters-limited', [
                'modelSearch' => $this->modelSearch,
            ]);
        }
        else {
            return $this->render('order-filters/order-filters', [
                'modelSearch' => $this->modelSearch,
            ]);
        }
    }
}
