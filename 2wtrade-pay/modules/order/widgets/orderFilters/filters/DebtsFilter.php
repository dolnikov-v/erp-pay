<?php
namespace app\modules\order\widgets\orderFilters\filters;

use app\modules\order\models\search\filters\DebtsFilter as DebtsFilterModel;
use yii\base\Widget;

/**
 * Class Debts
 * @package app\modules\order\widgets\orderFilters\filters
 */
class DebtsFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var StatusFilterModel */
    public $model;

    /** @var boolean */
    public $visible = true;

    /** @var boolean */
    public $formGroupMargin = true;

    /** @var boolean */
    private $useSelect2 = true;

    /**
     * @return string
     */
    public function run()
    {
        if (is_null($this->model)) {
            $this->model = new DebtsFilterModel();
            $this->useSelect2 = false;
        }

        return $this->render('debts-filter', [
            'form' => $this->form,
            'model' => $this->model,
            'visible' => $this->visible,
            'formGroupMargin' => $this->formGroupMargin,
            'useSelect2' => $this->useSelect2,
        ]);
    }
}
