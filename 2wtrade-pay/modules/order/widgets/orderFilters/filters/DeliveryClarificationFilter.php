<?php
namespace app\modules\order\widgets\orderFilters\filters;

use app\modules\order\models\search\filters\DeliveryClarificationFilter as DeliveryClarificationFilterModel;
use yii\base\Widget;

/**
 * Class DeliveryClarificationFilter
 * @package app\modules\order\widgets\orderFilters\filters
 */
class DeliveryClarificationFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var DeliveryClarificationFilterModel */
    public $model;

    /** @var boolean */
    public $visible = true;

    /** @var boolean */
    public $formGroupMargin = true;

    /** @var boolean */
    private $useSelect2 = true;

    /**
     * @return string
     */
    public function run()
    {
        if (is_null($this->model)) {
            $this->model = new DeliveryClarificationFilterModel();
            $this->useSelect2 = false;
        }

        return $this->render('delivery-clarification-filter', [
            'form' => $this->form,
            'model' => $this->model,
            'visible' => $this->visible,
            'formGroupMargin' => $this->formGroupMargin,
            'useSelect2' => $this->useSelect2,
        ]);
    }
}
