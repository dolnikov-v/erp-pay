<?php
namespace app\modules\order\widgets\orderFilters\filters;

use app\modules\order\models\search\filters\StatusDeliveryRequestFilter as StatusDeliveryRequestFilterModel;
use yii\base\Widget;

/**
 * Class StatusDeliveryRequestFilter
 * @package app\modules\order\widgets\orderFilters\filters
 */
class StatusDeliveryRequestFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var StatusDeliveryRequestFilterModel */
    public $model;

    /** @var boolean */
    public $visible = true;

    /** @var boolean */
    public $formGroupMargin = true;

    /** @var boolean */
    private $useSelect2 = true;

    /**
     * @return string
     */
    public function run()
    {
        if (is_null($this->model)) {
            $this->model = new StatusDeliveryRequestFilterModel();
            $this->useSelect2 = false;
        }

        return $this->render('status-delivery-request-filter', [
            'form' => $this->form,
            'model' => $this->model,
            'visible' => $this->visible,
            'formGroupMargin' => $this->formGroupMargin,
            'useSelect2' => $this->useSelect2,
        ]);
    }
}
