<?php
namespace app\modules\order\widgets\orderFilters\filters;

use app\modules\order\models\search\filters\StatusFilter as StatusFilterModel;
use yii\base\Widget;
use Yii;

/**
 * Class StatusFilter
 * @package app\modules\order\widgets\orderFilters\filters
 */
class StatusFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var StatusFilterModel */
    public $model;

    /** @var boolean */
    public $visible = true;

    /** @var boolean */
    public $formGroupMargin = true;

    /** @var boolean */
    private $useSelect2 = true;

    /**
     * @return string
     */
    public function run()
    {
        if (is_null($this->model)) {
            $this->model = new StatusFilterModel();
            $this->useSelect2 = false;
        }

        return $this->render('status-filter', [
            'form' => $this->form,
            'model' => $this->model,
            'visible' => $this->visible,
            'formGroupMargin' => $this->formGroupMargin,
            'useSelect2' => $this->useSelect2,
        ]);
    }
}
