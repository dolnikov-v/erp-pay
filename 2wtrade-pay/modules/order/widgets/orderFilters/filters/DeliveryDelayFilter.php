<?php
namespace app\modules\order\widgets\orderFilters\filters;

use app\modules\order\models\search\filters\DeliveryDelayFilter as DeliveryDelayFilterModel;
use yii\base\Widget;

/**
 * Class DeliveryDelayFilter
 * @package app\modules\order\widgets\orderFilters\filters
 */
class DeliveryDelayFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var DeliveryDelayFilterModel */
    public $model;

    /** @var boolean */
    public $visible = true;

    /** @var boolean */
    public $formGroupMargin = true;

    /** @var boolean */
    private $useSelect2 = true;

    /**
     * @return string
     */
    public function run()
    {
        if (is_null($this->model)) {
            $this->model = new DeliveryDelayFilterModel();
            $this->useSelect2 = false;
        }

        return $this->render('delivery-delay-filter', [
            'form' => $this->form,
            'model' => $this->model,
            'visible' => $this->visible,
            'formGroupMargin' => $this->formGroupMargin,
            'useSelect2' => $this->useSelect2,
        ]);
    }
}
