<?php
use app\helpers\WbIcon;
use app\widgets\Button;
use yii\helpers\Html;

/** @var app\components\widgets\ActiveForm $form */
/** @var app\modules\order\models\search\filters\LeadProductFilter $model */
/** @var boolean $visible */
/** @var boolean $formGroupMargin */
/** @var boolean $useSelect2 */
?>

<div class="row container-empty-filter" data-filter="lead-product"
    <?php if (!$visible): ?>style="display: none;"<?php endif; ?>>
    <div class="col-lg-12">
        <div class="form-group <?php if (!$formGroupMargin): ?>form-group-no-margin<?php endif; ?>">
            <label><?= Yii::t('common', 'Товар лида') ?></label>
            <div class="row">
                <div class="col-xs-4 relative-selectable-filters">
                    <label class="btn <?php if ($model->not): ?>btn-danger<?php else: ?>btn-default<?php endif; ?> not-selectable-filters">
                        NOT
                        <?= $form->field($model, 'not', ['template' => '{input}'])->hiddenInput([
                            'id' => false,
                            'name' => Html::getInputName($model, 'not') . '[]',
                        ])->label(false) ?>
                    </label>
                    <div class="container-selectable-filters">
                        <?= $form->field($model, 'product', ['template' => '{input}'])->dropDownList($model->products, [
                            'id' => false,
                            'name' => Html::getInputName($model, 'product') . '[]',
                            'data-minimum-results-for-search' => false,
                            'data-plugin' => $useSelect2 ? 'select2' : '',
                        ])->label(false) ?>
                    </div>
                </div>
                <div class="col-xs-8">
                    <?= Button::widget([
                        'style' => Button::STYLE_DANGER . ' remove-selectable-filters',
                        'icon' => WbIcon::MINUS,
                        'attributes' => [
                            'data-filter' => 'lead-product',
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
