<?php
/** @var app\components\widgets\ActiveForm $form */
/** @var app\modules\order\models\search\filters\DateFilter $model */

$dateRangePickerOptions = [
    'from' => ['disabled' => true],
    'to' => ['disabled' => true],
    'ranges' => ['disabled' => true],
];
?>

<div class="row">
    <div class="col-md-4">
        <?= $form->field($model, 'dateType')->select2List($model->dateTypes, [
            'length' => -1,
        ]) ?>
    </div>
    <div class="col-md-8">
        <?= $form->field($model, 'dateFrom')->dateRangePicker('dateFrom', 'dateTo', $dateRangePickerOptions) ?>
    </div>
</div>
<div class="row">
    <div class="col-xs-8">
        <?= $form->field($model, 'timezone')->select2List($model->timezones, ['value' => $model->timezone]) ?>
    </div>
</div>
