<?php

use app\helpers\WbIcon;
use app\widgets\Button;
use yii\bootstrap\Html;

/** @var \app\modules\order\models\search\filters\CheckingTypeFilter $model */
/** @var \app\components\widgets\ActiveForm $form */
/** @var boolean $visible */
/** @var boolean $formGroupMargin */
/** @var boolean $useSelect2 */
?>


<div class="row container-empty-filter" data-filter="checking-queues"
     <?php if (!$visible): ?>style="display: none;"<?php endif; ?>>
    <div class="col-lg-12">
        <div class="form-group <?php if (!$formGroupMargin): ?>form-group-no-margin<?php endif; ?>">
            <div class="row">
                <div class="col-xs-3">
                    <label><?= Yii::t('common', 'Поиск по типу проверяемой очереди') ?></label>
                </div>
                <div class="col-xs-3">
                    <label for="<?= Html::getInputName($model, 'informationStatus') ?>"><?= Yii::t('common',
                            'Статус проверки') ?></label>
                </div>
                <div class="col-xs-3">
                    <label for="<?= Html::getInputName($model, 'informationType') ?>"><?= Yii::t('common',
                            'Результат проверки') ?></label>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-3 relative-selectable-filters">
                    <label
                        class="btn <?php if ($model->not): ?>btn-danger<?php else: ?>btn-default<?php endif; ?> not-selectable-filters">
                        NOT
                        <?= $form->field($model, 'not', ['template' => '{input}'])->hiddenInput([
                            'id' => false,
                            'name' => Html::getInputName($model, 'not') . '[]',
                        ])->label(false) ?>
                    </label>
                    <div class="container-selectable-filters">
                        <?= $form->field($model, 'type', ['template' => '{input}'])->dropDownList($model->types, [
                            'id' => false,
                            'name' => Html::getInputName($model, 'type') . '[]',
                            'data-minimum-results-for-search' => -1,
                            'data-plugin' => $useSelect2 ? 'select2' : '',
                            'class' => 'checking-type-select',
                            'data-name-for-information' => Html::getInputName($model, 'informationType') . '[]'
                        ])->label(false) ?>
                    </div>
                </div>
                <div class="col-xs-3 relative-selectable-filters">
                    <?= $form->field($model, 'status', ['template' => '{input}'])->dropDownList($model->statuses, [
                        'id' => false,
                        'prompt' => '—',
                        'name' => Html::getInputName($model, 'status') . '[]',
                        'data-minimum-results-for-search' => false,
                        'data-plugin' => $useSelect2 ? 'select2' : '',
                    ])->label(false) ?>
                </div>
                <div class="col-xs-3 relative-selectable-filters">
                    <?php foreach ($model->types as $type => $label): ?>
                        <div
                            class="container-selectable-filters margin-left-0 <?= ($type == $model->type || (empty($model->type) && array_keys($model->types)[0] == $type) ? '' : 'hidden ') . 'checking-type-information-select' ?>"
                            data-checking-type="<?= $type ?>">
                            <?= Html::dropDownList($type == $model->type || (empty($model->type) && array_keys($model->types)[0] == $type) ? Html::getInputName($model,
                                    'informationType') . '[]' : '',
                                $type == $model->type || (empty($model->type) && array_keys($model->types)[0] == $type) ? $model->informationType : '',
                                isset($model->informationTypes[$type]) ? $model->informationTypes[$type] : [], [
                                    'prompt' => '—',
                                    'disabled' => !isset($model->informationTypes[$type]) || empty($model->informationTypes[$type]),
                                    'template' => '{input}',
                                    'data-minimum-results-for-search' => -1,
                                    'data-plugin' => $useSelect2 ? 'select2' : '',
                                ]) ?>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="col-xs-3">
                    <?= Button::widget([
                        'style' => Button::STYLE_DANGER . ' remove-selectable-filters',
                        'icon' => WbIcon::MINUS,
                        'attributes' => [
                            'data-filter' => 'checking-queues',
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
