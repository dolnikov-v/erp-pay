<?php
use app\helpers\WbIcon;
use app\widgets\Button;
use yii\helpers\Html;

/** @var app\components\widgets\ActiveForm $form */
/** @var app\modules\order\models\search\filters\MarketingListFilter $model */
/** @var boolean $visible */
/** @var boolean $formGroupMargin */
/** @var boolean $useSelect2 */
?>

<div class="row container-empty-filter" data-filter="marketing-list"
     <?php if (!$visible): ?>style="display: none;"<?php endif; ?>>
    <div class="col-lg-12">
        <div class="form-group <?php if (!$formGroupMargin): ?>form-group-no-margin<?php endif; ?>">
            <label><?= Yii::t('common', 'Маркетинг') ?></label>
            <div class="row">
                <div class="col-xs-6 relative-selectable-filters">
                    <?= $form->field($model, 'marketinglist', ['template' => '{input}'])->dropDownList($model->marketinglists, [
                        'id' => false,
                        'name' => Html::getInputName($model, 'marketinglist') . '[]',
                        'data-minimum-results-for-search' => -1,
                        'data-plugin' => $useSelect2 ? 'select2' : '',
                    ])->label(false) ?>
                </div>
                <div class="col-xs-6">
                    <?= Button::widget([
                        'style' => Button::STYLE_DANGER . ' remove-selectable-filters',
                        'icon' => WbIcon::MINUS,
                        'attributes' => [
                            'data-filter' => 'marketing-list',
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
