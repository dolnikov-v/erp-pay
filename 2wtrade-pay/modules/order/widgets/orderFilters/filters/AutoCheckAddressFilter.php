<?php

namespace app\modules\order\widgets\orderFilters\filters;


use app\widgets\Widget;
use app\modules\order\models\search\filters\AutoCheckAddressFilter as AutoCheckAddressFilterModel;

/**
 * Class AutoCheckAddressFilter
 * @package app\modules\order\widgets\orderFilters\filters
 */
class AutoCheckAddressFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var AutoCheckAddressFilterModel */
    public $model;

    /** @var boolean */
    public $visible = true;

    /** @var boolean */
    public $formGroupMargin = true;

    /** @var boolean */
    private $useSelect2 = true;

    /**
     * @return string
     */
    public function run()
    {
        if (is_null($this->model)) {
            $this->model = new AutoCheckAddressFilterModel();
            $this->useSelect2 = false;
        }

        return $this->render('auto-check-address-filter', [
            'form' => $this->form,
            'model' => $this->model,
            'visible' => $this->visible,
            'formGroupMargin' => $this->formGroupMargin,
            'useSelect2' => $this->useSelect2,
        ]);
    }
}