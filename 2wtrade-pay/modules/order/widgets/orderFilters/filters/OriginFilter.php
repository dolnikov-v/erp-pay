<?php
namespace app\modules\order\widgets\orderFilters\filters;

use app\modules\order\models\search\filters\OriginFilter as OriginFilterModel;
use yii\base\Widget;

/**
 * Class StatusDeliveryFilter
 * @package app\modules\order\widgets\orderFilters\filters
 */
class OriginFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var OriginFilterModel */
    public $model;

    /** @var boolean */
    public $visible = true;

    /** @var boolean */
    public $formGroupMargin = true;

    /** @var boolean */
    private $useSelect2 = true;

    /**
     * @return string
     */
    public function run()
    {
        if (is_null($this->model)) {
            $this->model = new OriginFilterModel();
            $this->useSelect2 = false;
        }

        return $this->render('origin-filter', [
            'form' => $this->form,
            'model' => $this->model,
            'visible' => $this->visible,
            'formGroupMargin' => $this->formGroupMargin,
            'useSelect2' => $this->useSelect2,
        ]);
    }
}
