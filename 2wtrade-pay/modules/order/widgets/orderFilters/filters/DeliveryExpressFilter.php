<?php
namespace app\modules\order\widgets\orderFilters\filters;

use app\modules\order\models\search\filters\DeliveryExpressFilter as DeliveryExpressFilterModel;
use yii\base\Widget;

/**
 * Class DeliveryExpressFilter
 * @package app\modules\order\widgets\orderFilters\filters
 */
class DeliveryExpressFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var DeliveryExpressFilterModel */
    public $model;

    /** @var boolean */
    public $visible = true;

    /** @var boolean */
    public $formGroupMargin = true;

    /** @var boolean */
    private $useSelect2 = true;

    /**
     * @return string
     */
    public function run()
    {
        if (is_null($this->model)) {
            $this->model = new DeliveryExpressFilterModel();
            $this->useSelect2 = false;
        }

        return $this->render('delivery-express-filter', [
            'form' => $this->form,
            'model' => $this->model,
            'visible' => $this->visible,
            'formGroupMargin' => $this->formGroupMargin,
            'useSelect2' => $this->useSelect2,
        ]);
    }
}
