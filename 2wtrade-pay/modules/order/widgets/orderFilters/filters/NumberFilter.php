<?php
namespace app\modules\order\widgets\orderFilters\filters;

use app\modules\order\models\search\filters\NumberFilter as NumberFilterModel;
use yii\base\Widget;
use Yii;

/**
 * Class NumberFilter
 * @package app\modules\order\widgets\orderFilters\filters
 */
class NumberFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var NumberFilterModel */
    public $model;

    /** @var boolean */
    public $visible = true;

    /** @var boolean */
    public $formGroupMargin = true;

    /** @var boolean */
    private $useSelect2 = true;

    /**
     * @return string
     */
    public function run()
    {
        if (is_null($this->model)) {
            $this->model = new NumberFilterModel();
            $this->useSelect2 = false;
        }

        return $this->render('number-filter', [
            'form' => $this->form,
            'model' => $this->model,
            'visible' => $this->visible,
            'formGroupMargin' => $this->formGroupMargin,
            'useSelect2' => $this->useSelect2,
        ]);
    }
}
