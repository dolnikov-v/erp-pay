<?php
namespace app\modules\order\widgets\orderFilters\filters;

use app\modules\order\models\search\filters\DeliveryZipGroupFilter as DeliveryZipGroupFilterModel;
use yii\base\Widget;

/**
 * Class DeliveryvZipGroupFilter
 * @package app\modules\order\widgets\orderFilters\filters
 */
class DeliveryZipGroupFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var DeliveryZipGroupFilterModel */
    public $model;

    /** @var boolean */
    public $visible = true;

    /** @var boolean */
    public $formGroupMargin = true;

    /** @var boolean */
    private $useSelect2 = true;

    /**
     * @return string
     */
    public function run()
    {
        if (is_null($this->model)) {
            $this->model = new DeliveryZipGroupFilterModel();
            $this->useSelect2 = false;
        }

        return $this->render('delivery-zipgroup-filter', [
            'form' => $this->form,
            'model' => $this->model,
            'visible' => $this->visible,
            'formGroupMargin' => $this->formGroupMargin,
            'useSelect2' => $this->useSelect2,
        ]);
    }
}
