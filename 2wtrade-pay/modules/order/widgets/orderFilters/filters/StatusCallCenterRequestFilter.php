<?php
namespace app\modules\order\widgets\orderFilters\filters;

use app\modules\order\models\search\filters\StatusCallCenterRequestFilter as StatusCallCenterRequestFilterModel;
use yii\base\Widget;

/**
 * Class StatusCallCenterRequestFilter
 * @package app\modules\order\widgets\orderFilters\filters
 */
class StatusCallCenterRequestFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var StatusCallCenterRequestFilterModel */
    public $model;

    /** @var boolean */
    public $visible = true;

    /** @var boolean */
    public $formGroupMargin = true;

    /** @var boolean */
    private $useSelect2 = true;

    /**
     * @return string
     */
    public function run()
    {
        if (is_null($this->model)) {
            $this->model = new StatusCallCenterRequestFilterModel();
            $this->useSelect2 = false;
        }

        return $this->render('status-call-center-request-filter', [
            'form' => $this->form,
            'model' => $this->model,
            'visible' => $this->visible,
            'formGroupMargin' => $this->formGroupMargin,
            'useSelect2' => $this->useSelect2,
        ]);
    }
}
