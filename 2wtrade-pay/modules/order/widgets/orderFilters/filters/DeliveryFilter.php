<?php
namespace app\modules\order\widgets\orderFilters\filters;

use app\modules\order\models\search\filters\DeliveryFilter as DeliveryFilterModel;
use yii\base\Widget;

/**
 * Class DeliveryFilter
 * @package app\modules\order\widgets\orderFilters\filters
 */
class DeliveryFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var DeliveryFilterModel */
    public $model;

    /** @var boolean */
    public $visible = true;

    /** @var boolean */
    public $formGroupMargin = true;

    /** @var boolean */
    private $useSelect2 = true;

    /**
     * @return string
     */
    public function run()
    {
        if (is_null($this->model)) {
            $this->model = new DeliveryFilterModel();
            $this->useSelect2 = false;
        }

        return $this->render('delivery-filter', [
            'form' => $this->form,
            'model' => $this->model,
            'visible' => $this->visible,
            'formGroupMargin' => $this->formGroupMargin,
            'useSelect2' => $this->useSelect2,
        ]);
    }
}
