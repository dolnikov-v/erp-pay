<?php

namespace app\modules\order\widgets\orderFilters\filters;


use app\widgets\Widget;
use app\components\widgets\ActiveForm;
use app\modules\order\models\search\filters\CheckingTypeFilter as Filter;

/**
 * Class CheckingTypeFilter
 * @package app\modules\order\widgets\orderFilters\filters
 */
class CheckingTypeFilter extends Widget
{
    /**
     * @var Filter
     */
    public $model;

    /**
     * @var ActiveForm
     */
    public $form;

    /** @var boolean */
    public $visible = true;

    /** @var boolean */
    public $formGroupMargin = true;

    /** @var boolean */
    private $useSelect2 = true;

    /**
     * @return string
     */
    public function run()
    {
        if(is_null($this->model)) {
            $this->model = new Filter();
            $this->useSelect2 = false;
        }
        return $this->render('checking-type-filter', [
            'model' => $this->model,
            'form' => $this->form,
            'visible' => $this->visible,
            'formGroupMargin' => $this->formGroupMargin,
            'useSelect2' => $this->useSelect2,
        ]);
    }
}
