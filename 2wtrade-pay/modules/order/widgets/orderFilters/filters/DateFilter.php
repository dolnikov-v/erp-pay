<?php
namespace app\modules\order\widgets\orderFilters\filters;

use app\modules\order\models\search\filters\DateFilter as DateFilterModel;
use yii\base\Widget;

/**
 * Class DateFilter
 * @package app\modules\order\widgets\orderFilters\filters
 */
class DateFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var DateFilterModel */
    public $model;

    /** @var integer */
    public $timeOffset;

    /**
     * @return string
     */
    public function run()
    {
        if (is_null($this->model)) {
            $this->model = new DateFilterModel();
        }

        return $this->render('date-filter', [
            'form' => $this->form,
            'model' => $this->model,
        ]);
    }
}
