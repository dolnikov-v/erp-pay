<?php
namespace app\modules\order\widgets\orderFilters\filters;

use app\modules\order\models\search\filters\TermsOfShippingFilter as TermsOfShippingFilterModel;
use yii\base\Widget;
use Yii;

/**
 * Class TermsOfShippingFilter
 * @package app\modules\order\widgets\orderFilters\filters
 */
class TermsOfShippingFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var TermsOfShippingFilter */
    public $model;

    /** @var boolean */
    public $visible = true;

    /** @var boolean */
    public $formGroupMargin = true;

    /** @var boolean */
    private $useSelect2 = true;

    /**
     * @return string
     */
    public function run()
    {
        if (is_null($this->model)) {
            $this->model = new TermsOfShippingFilterModel();
            $this->useSelect2 = false;
        }

        return $this->render('terms-of-shipping-filter', [
            'form' => $this->form,
            'model' => $this->model,
            'visible' => $this->visible,
            'formGroupMargin' => $this->formGroupMargin,
            'useSelect2' => $this->useSelect2,
        ]);
    }
}
