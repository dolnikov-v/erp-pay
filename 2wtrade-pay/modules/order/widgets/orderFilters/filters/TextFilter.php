<?php
namespace app\modules\order\widgets\orderFilters\filters;

use app\modules\order\models\search\filters\TextFilter as TextFilterModel;
use yii\base\Widget;
use Yii;

/**
 * Class TextFilter
 * @package app\modules\order\widgets\orderFilters\filters
 */
class TextFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var TextFilterModel */
    public $model;

    /** @var boolean */
    public $visible = true;

    /** @var boolean */
    public $formGroupMargin = true;

    /** @var boolean */
    private $useSelect2 = true;

    /**
     * @return string
     */
    public function run()
    {
        if (is_null($this->model)) {
            $this->model = new textFilterModel();
            $this->useSelect2 = false;
        }

        return $this->render('text-filter', [
            'form' => $this->form,
            'model' => $this->model,
            'visible' => $this->visible,
            'formGroupMargin' => $this->formGroupMargin,
            'useSelect2' => $this->useSelect2,
        ]);
    }
}
