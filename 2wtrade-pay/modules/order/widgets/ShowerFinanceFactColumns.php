<?php

namespace app\modules\order\widgets;

use Yii;

/**
 * Class ShowerFinanceFactColumns
 * @package app\modules\order\widgets
 *
 * @property array $defaultColumns
 * @property array $chosenColumns
 */
class ShowerFinanceFactColumns extends ShowerColumns
{
    /** @var string */
    protected $sessionKey = 'orderfinancefact.columns';

    const COLUMN_REPORT_NAME = 'name';
    const COLUMN_PAYMENT_NAME = 'payment';
    const COLUMN_NUM = 'name';

    const COLUMN_PRICE_COD = 'price_cod';
    const COLUMN_PRICE_STORAGE = 'price_storage';
    const COLUMN_PRICE_FULFILMENT = 'price_fulfilment';
    const COLUMN_PRICE_PACKING = 'price_packing';
    const COLUMN_PRICE_PACKAGE = 'price_package';
    const COLUMN_PRICE_ADDRESS_CORRECTION = 'price_address_correction';
    const COLUMN_PRICE_DELIVERY = 'price_delivery';
    const COLUMN_PRICE_REDELIVERY = 'price_redelivery';
    const COLUMN_PRICE_DELIVERY_BACK = 'price_delivery_back';
    const COLUMN_PRICE_DELIVERY_RETURN = 'price_delivery_return';
    const COLUMN_PRICE_COD_SERVICE = 'price_cod_service';
    const COLUMN_PRICE_VAT = 'price_vat';
    
    /**
     * @return array
     */
    protected function getRequireColumnsCollection()
    {
        if (is_null($this->requireColumnsCollection)) {
            $this->requireColumnsCollection = [
                self::COLUMN_NUM => Yii::t('common', 'Порядковый номер'),
                self::COLUMN_RECORD_ID => Yii::t('common', 'Номер заказа'),
            ];
        }

        return $this->requireColumnsCollection;
    }

    /**
     * @return array
     */
    public function getManualColumnsCollection()
    {
        if (is_null($this->manualColumnsCollection)) {
            $this->manualColumnsCollection = [
                self::COLUMN_REPORT_NAME => Yii::t('common', 'Отчёт'),
                self::COLUMN_PAYMENT_NAME => Yii::t('common', 'Платёжка'),
                self::COLUMN_PRICE_COD => Yii::t('common', 'COD'),
                self::COLUMN_PRICE_STORAGE => Yii::t('common', 'Стоимость хранения'),
                self::COLUMN_PRICE_PACKING => Yii::t('common', 'Стоимость упаковывания'),
                self::COLUMN_PRICE_PACKAGE => Yii::t('common', 'Стоимость упаковки'),
                self::COLUMN_PRICE_ADDRESS_CORRECTION => Yii::t('common', 'Стоимость корректировки адреса'),
                self::COLUMN_PRICE_DELIVERY => Yii::t('common', 'Стоимость доставки'),
                self::COLUMN_PRICE_REDELIVERY => Yii::t('common', 'Стоимость передоставки'),
                self::COLUMN_PRICE_DELIVERY_BACK => Yii::t('common', 'Стоимость возвращения'),
                self::COLUMN_PRICE_DELIVERY_RETURN => Yii::t('common', 'Стоимость возврата'),
                self::COLUMN_PRICE_COD_SERVICE => Yii::t('common', 'Плата за наложенный платеж'),
                self::COLUMN_PRICE_VAT => Yii::t('common', 'НДС'),
                self::COLUMN_PRICE_FULFILMENT => Yii::t('common', 'Стоимость обслуживания заказа'),
            ];
        }

        asort($this->manualColumnsCollection);

        return $this->manualColumnsCollection;
    }

    /**
     * @return array
     */
    protected function getDefaultColumns()
    {
        $requireColumns = array_keys($this->getRequireColumnsCollection());

        $manualColumns = [
            self::COLUMN_REPORT_NAME,
            self::COLUMN_PAYMENT_NAME,
            self::COLUMN_PRICE_COD,
            self::COLUMN_PRICE_DELIVERY,
        ];

        return array_merge($requireColumns, $manualColumns);
    }
}
