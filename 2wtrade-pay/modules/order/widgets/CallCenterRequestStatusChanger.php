<?php

namespace app\modules\order\widgets;

use app\modules\callcenter\models\CallCenterRequest;

/**
 * Class CallCenterRequestStatusChanger
 * @package app\modules\order\widgets
 */
class CallCenterRequestStatusChanger extends Sender
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('call-center-request-status-changer/modal', [
            'url' => $this->url,
            'statuses' => CallCenterRequest::getStatusesCollection(),
        ]);
    }
}
