<?php
namespace app\modules\order\widgets;

/**
 * Class GeneratorMarketingList
 * @package app\modules\order\widgets
 */
class GeneratorMarketingList extends Sender
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('generator-marketing-list/modal', [
            'url' => $this->url
        ]);
    }
}