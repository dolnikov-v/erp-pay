<?php
namespace app\modules\order\widgets;

use app\modules\order\models\OrderStatus;
use yii\base\Widget;

/**
 * Class ResendSeparateOrder
 * @package app\modules\order\widgets
 */
class ResendSeparateOrder extends Sender
{

    /**
     * @return array
     */
    public static function getAllowStatusArray()
    {
        return [
            OrderStatus::STATUS_DELIVERY_DENIAL,
            OrderStatus::STATUS_DELIVERY_RETURNED,
            OrderStatus::STATUS_DELIVERY_REFUND
        ];
    }

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('resend-separate-order/modal', [
            'url' => $this->url,
        ]);
    }
}
