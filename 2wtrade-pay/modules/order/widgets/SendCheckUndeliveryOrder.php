<?php

namespace app\modules\order\widgets;

/**
 * Class SendCheckUndeliveryOrder
 * @package app\modules\order\widgets
 */
class SendCheckUndeliveryOrder extends Sender
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('send-check-undelivery-order/modal', [
            'url' => $this->url,
        ]);
    }
}