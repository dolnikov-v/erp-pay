<?php

namespace app\modules\order\widgets;

/**
 * Class RetryGetStatusFromCallCenter
 * @package app\modules\order\widgets
 */
class RetryGetStatusFromCallCenter extends Sender
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('retry-get-status-from-call-center/modal', [
            'url' => $this->url
        ]);
    }
}
