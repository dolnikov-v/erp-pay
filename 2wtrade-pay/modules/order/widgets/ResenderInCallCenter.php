<?php
namespace app\modules\order\widgets;

/**
 * Class ResenderInCallCenter
 * @package app\modules\order\widgets
 */
class ResenderInCallCenter extends Sender
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('resender-in-call-center/modal', [
            'url' => $this->url,
        ]);
    }
}
