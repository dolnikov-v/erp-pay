<?php
namespace app\modules\order\widgets;

use app\modules\order\models\SmsPollQuestions;
use Yii;

/**
 * Class SendSmsPoll
 * @package app\modules\order\widgets
 */
class SendSmsPoll extends Sender
{
    /**
     * @return array
     */
    public static function getListSmsPoll()
    {
        $country_id = Yii::$app->user->country->id;

        $questions = SmsPollQuestions::find()
            ->where([SmsPollQuestions::tableName() . '.country_id' => $country_id])
            ->andWhere([SmsPollQuestions::tableName() . '.is_active' => true])
            ->collection();

        return $questions;
    }

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('send-sms-poll/modal', [
            'url' => $this->url,
            'smspoll' => self::getListSmsPoll(),
        ]);
    }
}
