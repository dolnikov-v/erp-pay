<?php
namespace app\modules\order\widgets;

/**
 * Class SendPostSell
 * @package app\modules\order\widgets
 */
class SendPostSell extends Sender
{/**
     * @return string
     */
    public function run()
    {
        return $this->render('send-post-sell/modal', [
            'url' => $this->url
        ]);
    }
}
