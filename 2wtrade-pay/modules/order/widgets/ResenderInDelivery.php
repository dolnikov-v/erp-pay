<?php
namespace app\modules\order\widgets;

use Yii;

/**
 * Class ResenderInDelivery
 * @package app\modules\order\widgets
 */
class ResenderInDelivery extends Sender
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('resender-in-delivery/modal', [
            'url' => $this->url,
        ]);
    }
}
