<?php
namespace app\modules\order\widgets;

use app\modules\delivery\models\Delivery;
use app\modules\order\models\OrderStatus;
use Yii;

/**
 * Class ChangerCourier
 * @package app\modules\order\widgets
 */
class ChangerDelivery extends Sender
{
    /**
     * @return array
     */
    public static function getAllowDeliveries()
    {
        $ids = [];
        $allowDeliveries = Yii::$app->user->country->getActiveDeliveries();

        foreach ($allowDeliveries->all() as $delivery) {
            $ids[] = $delivery->id;
        }

        $deliveries = Delivery::find()
            ->where(['in', 'id', $ids])
            ->collection();

        return $deliveries;
    }

    /**
     * @return array
     */
    public static function getAllowStatusArray()
    {
        return [OrderStatus::STATUS_CC_APPROVED, OrderStatus::STATUS_DELIVERY_REJECTED];
    }

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('changer-delivery/modal', [
            'url' => $this->url,
            'deliveries' => self::getAllowDeliveries(),
        ]);
    }
}
