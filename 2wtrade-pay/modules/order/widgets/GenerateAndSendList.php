<?php

namespace app\modules\order\widgets;

use app\modules\order\components\ExcelBuilder as ComponentExcelBuilder;
use app\widgets\Modal;
use Yii;

/**
 * Class GenerateAndSendList
 * @package app\modules\order\widgets
 */
class GenerateAndSendList extends Modal
{
    protected static $sessionKey = 'excel.builder.columns';

    /**
     * @param array $columns
     * @return boolean
     */
    public static function setCheckedColumns($columns)
    {
        $checkedColumns = [];

        if (is_array($columns)) {
            foreach ($columns as $column) {
                if (array_key_exists($column, ComponentExcelBuilder::$columns)) {
                    $checkedColumns[] = $column;
                }
            }
        }

        if ($checkedColumns) {
            Yii::$app->session->set(self::$sessionKey, $checkedColumns);

            return true;
        }

        return false;
    }

    /**
     * @return array
     */
    protected static function getCheckedColumns()
    {
        $checkedColumns = Yii::$app->session->get(self::$sessionKey);

        if (empty($checkedColumns)) {
            Yii::$app->session->set(self::$sessionKey, ComponentExcelBuilder::$columns);
            $checkedColumns = self::getCheckedColumns();
        }

        return $checkedColumns;
    }

    /**
     * @return string
     */
    public function run()
    {
        $count = ceil(count(ComponentExcelBuilder::$columns) / 3);

        return $this->render('generate-and-send-list/modal', [
            'id' => 'modal_generate_and_send_list',
            'title' => Yii::t('common', 'Отправка листа'),
            'checkedColumns' => self::getCheckedColumns(),
            'partColumnsOne' => array_slice(ComponentExcelBuilder::$columns, 0, $count),
            'partColumnsTwo' => array_slice(ComponentExcelBuilder::$columns, $count, $count),
            'partColumnsThree' => array_slice(ComponentExcelBuilder::$columns, $count + $count, $count),
        ]);
    }
}