<?php
namespace app\modules\order\components;

use app\helpers\Utils;
use app\modules\delivery\components\api\Transmitter;
use app\modules\order\components\exceptions\InvoiceBuilderException;
use app\modules\order\models\OrderLogisticListInvoice;
use kartik\mpdf\Pdf;
use Yii;

/**
 * Class InvoicePdfBuilder
 * @package app\modules\order\components
 */
class InvoicePdfBuilder extends InvoiceBuilder
{
    protected $format = OrderLogisticListInvoice::FORMAT_PDF;

    /**
     * @var
     */
    protected static $logoBase64;

    /**
     * @return array
     */
    public function generate()
    {
        set_time_limit(0);

        $result = [
            'status' => static::STATUS_FAIL,
            'message' => '',
        ];

        if ($archive = $this->archiveIsExists()) {
            $result['status'] = 'success';
            $result['message'] = $archive->archive;
        } else {
            $ids = explode(',', $this->list->ids);
            $invoices = [];

            try {

                if ($this->list->delivery->hasApiGetSortedOrderIds()) {
                    $transmitter = new Transmitter($this->list->delivery);
                    if ($transmitter) {
                        $ids = $transmitter->getSortedOrderIds($ids);
                    }
                }

                foreach ($ids as $id) {
                    $invoices[] = $this->createInvoiceByOrderId($id);
                }

                $fileName = $this->package($invoices);

                $result['status'] = static::STATUS_SUCCESS;
                $result['message'] = $fileName;
            } catch (InvoiceBuilderException $e) {
                $result['message'] = $e->getMessage();
            }
        }

        return $result;
    }

    /**
     * @param integer $orderId
     * @return OrderLogisticListInvoice
     * @throws InvoiceBuilderException
     */
    public function createInvoiceByOrderId($orderId)
    {
        $invoice = $this->getInvoiceByOrderId($orderId);

        if (empty($invoice->invoice)) {
            $fileName = Utils::uid() . '.' . $invoice->getExtension();
            $filePath = OrderLogisticListInvoice::getDownloadPath($fileName, true) . DIRECTORY_SEPARATOR . $fileName;

            $invoice->invoice = $fileName;
            $invoice->invoice_local = $fileName;
            $invoice->user_id = isset(Yii::$app->user) ? Yii::$app->user->id : 1;

            if ($invoice->save()) {
                $invoice->invoice_local = $this->getLocalName($invoice);
                $invoice->save(false, ['invoice_local']);
            } else {
                $error = $invoice->getErrorsAsArray()[0];

                throw new InvoiceBuilderException(Yii::t('common', $error), 400);
            }

            $templateFile = $this->getTemplateFilePath();

            $viewData['data'] = $this->getViewData($invoice);
            $content = Yii::$app->controller->renderFile($templateFile, $viewData);

            $document = new Pdf([
                'mode' => Pdf::MODE_UTF8,
                'options' => [
                    'autoScriptToLang' => true,
                    'autoLangToFont' => true,
                    'ignore_invalid_utf8' => true,
                    'tabSpaces' => 4
                ],

                'marginLeft' => 11,
                'marginRight' => 11,
                'marginTop' => 8,
                'marginBottom' => 8,
                'content' => $content,
                'filename' => $filePath,
                'destination' => Pdf::DEST_FILE,
                'cssFile' => Yii::getAlias('@templates') . DIRECTORY_SEPARATOR . self::$folderFiles . DIRECTORY_SEPARATOR . 'pdf-style.css',
            ]);

            $document->render();
        }

        return $invoice;
    }

    /**
     * @param OrderLogisticListInvoice $invoice
     * @return array
     */
    protected function getViewData($invoice)
    {
        $values = $this->getValues($invoice);

        if (is_null(self::$logoBase64)) {
            $logoPath = Yii::getAlias('@templates') . DIRECTORY_SEPARATOR . self::$folderFiles . DIRECTORY_SEPARATOR . 'pdf-logo.png';
            $logoContent = file_get_contents($logoPath);

            self::$logoBase64 = 'data:image/png;base64,' . base64_encode($logoContent);;
        }

        $values['logoBase64'] = self::$logoBase64;

        return $values;
    }

    /**
     * Print only one order invoice,
     * but this invoice must be in generated list
     * @param int $orderId
     * @return string
     */
    public function printOrderInvoice($orderId){
        $this->onlyOneInvoice = true;
        $invoice = $this->getInvoiceByOrderId($orderId);
        $fileName = Utils::uid() . '.' . $invoice->getExtension();
        $templateFile = $this->getTemplateFilePath();
        $viewData['data'] = $this->getViewData($invoice);
        $content = Yii::$app->controller->renderFile($templateFile, $viewData);

        $document = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            'options' => [
                'autoScriptToLang' => true,
                'autoLangToFont' => true,
                'ignore_invalid_utf8' => true,
                'tabSpaces' => 4
            ],
            'marginLeft' => 11,
            'marginRight' => 11,
            'marginTop' => 8,
            'marginBottom' => 8,
            'content' => $content,
            'filename' => $fileName,
            'destination' => Pdf::DEST_STRING,
            'cssFile' => Yii::getAlias('@templates') . DIRECTORY_SEPARATOR . self::$folderFiles . DIRECTORY_SEPARATOR . 'pdf-style.css',
        ]);
        return $document->render();
    }
}
