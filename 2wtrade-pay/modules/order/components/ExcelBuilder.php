<?php

namespace app\modules\order\components;

use app\modules\order\components\exceptions\ExcelBuilderException;
use app\modules\order\models\Order;
use app\modules\order\models\OrderLogisticList;
use app\modules\order\models\OrderLogisticListExcel;
use app\modules\order\widgets\ExcelBuilder as WidgetExcelBuilder;
use Yii;
use yii\base\InvalidParamException;

/**
 * Class ExcelBuilder
 * @package app\modules\order\components
 */
abstract class ExcelBuilder
{
    public static $columns = [
        'row_number',
        'id',
        'id_cc',
        'customer_full_name',
        'customer_phone',
        'customer_mobile',
        'customer_address',
        'customer_address_additional',
        'customer_address_add',
        'customer_province',
        'customer_city',
        'customer_street',
        'customer_house_number',
        'customer_zip',
        'region',
        'customer_district',
        'customer_subdistrict',
        'comment',
        'comment_call_center',
        'products_count',
        'products_name',
        'products',
        'price',
        'delivery',
        'sum',
        'date_delivery',
        'date_send',
        'express',
        'approve_date',
        'approve_operator',
        'customer_address_add_embedded', // Вложенный в customer_address_add
    ];

    public static $autoGenerateColumns = [
        'row_number',
        'id',
        'customer_full_name',
        'customer_phone',
        'customer_address',
        'customer_address_additional',
        'customer_province',
        'customer_city',
        'customer_street',
        'customer_house_number',
        'customer_zip',
        'region',
        'customer_district',
        'customer_subdistrict',
        'comment',
        'comment_call_center',
        'products_count',
        'products_name',
        'sum',
        'date_delivery',
        'date_send',
        'express',
        'approve_date',
        'approve_operator',
        'customer_address_add_embedded', // Вложенный в customer_address_add
    ];

    /**
     * @var OrderLogisticList
     */
    protected $list;

    /**
     * @var string
     */
    protected $format;

    /**
     * @var array
     */
    protected $selectedColumns = [];

    /**
     * @var string
     */
    protected $selectedColumnsHash;

    /**
     * @var bool
     */
    protected $waitingForSend;

    /**
     * @return array
     */
    public static function columnsLabels()
    {
        return [
            'id' => 'ID',
            'id_cc' => 'ID_CC',
            'customer_full_name' => 'CONTACT',
            'customer_phone' => 'PHONE',
            'customer_mobile' => 'MOBILE PHONE',
            'customer_address' => 'FULL ADDRESS',
            'customer_address_add' => 'ADDRESS DETAILS',
            'customer_province' => 'PROVINCE',
            'customer_city' => 'CITY',
            'customer_street' => 'STREET',
            'customer_house_number' => 'HOUSE NUMBER',
            'customer_zip' => 'POST CODE',
            'region' => 'REGION',
            'customer_district' => 'DISTRICT',
            'customer_subdistrict' => 'SUBDISTRICT',
            'comment' => 'COMMENT',
            'comment_call_center' => 'COMMENT_CALL_CENTER',
            'products_count' => 'PRODUCTS_COUNT',
            'products_name' => 'PRODUCTS_NAME',
            'products' => 'PRODUCTS',
            'price' => 'PRICE',
            'delivery' => 'DELIVERY',
            'sum' => 'SUM',
            'date_delivery' => 'DATE DELIVERY',
            'row_number' => '#',
            'date_send' => 'DATE SEND',
            'express' => 'EXPRESS',
            'approve_date' => 'DATE OF APPROVEMENT',
            'approve_operator' => "Operator",
            'customer_address_additional' => 'ADDITIONAL ADDRESS',
            'customer_address_add_embedded' => 'BLOCK OR INTERIOR',
        ];
    }

    /**
     * @param string $column
     * @return string
     */
    public static function getColumnLabel($column)
    {
        $labels = self::columnsLabels();

        return array_key_exists($column, $labels) ? $labels[$column] : '—';
    }

    /**
     * @param OrderLogisticList $list
     * @param array $columns
     * @param integer $waitingForSend
     * @throws InvalidParamException
     * @throws ExcelBuilderException
     */
    public function __construct($list, $columns, $waitingForSend = 0)
    {
        if (!$list instanceof OrderLogisticList) {
            throw new InvalidParamException(Yii::t('common',
                'Необходимо указать корректную модель для генерации Excel-листа.'));
        }

        $this->list = $list;

        if (!is_array($columns)) {
            throw new InvalidParamException(Yii::t('common', 'Необходимо передать массив колонок.'));
        }

        foreach ($columns as $column) {
            if (in_array($column, self::$columns)) {
                $this->selectedColumns[] = $column;
            }
        }

        if (empty($this->selectedColumns)) {
            throw new ExcelBuilderException(Yii::t('common', 'Необходимо выбрать доступные колонки.'));
        }

        $this->waitingForSend = $waitingForSend;

        WidgetExcelBuilder::setCheckedColumns($this->selectedColumns);

        asort($this->selectedColumns);

        $this->selectedColumnsHash = md5(implode(',', $this->selectedColumns));
    }

    /**
     * @return OrderLogisticListExcel
     */
    protected function excelIsExists()
    {
        $excel = OrderLogisticListExcel::find()
            ->byListId($this->list->id)
            ->byFormat($this->format)
            ->byColumnsHash($this->selectedColumnsHash)
            ->one();

        if (!is_null($excel)) {
            $filePath = OrderLogisticList::getDownloadPath(
                    $excel->system_file_name,
                    OrderLogisticList::DOWNLOAD_TYPE_EXCELS
                ) . DIRECTORY_SEPARATOR . $excel->system_file_name;

            if (!file_exists($filePath)) {
                return null;
            }
        }

        return $excel;
    }

    /**
     * @return Order[]
     * @throws ExcelBuilderException
     */
    protected function getOrders()
    {
        $ids = explode(',', $this->list->ids);

        $orders = Order::find()
            ->with(['orderProducts', 'orderProducts.product'])
            ->andWhere(['in', 'id', $ids])
            ->all();

        if (!$orders) {
            throw new ExcelBuilderException(Yii::t('common', 'Заказы не найдены.'), 400);
        }

        return $orders;
    }

    /**
     * @return string
     */
    public abstract function generate();

    /**
     * @param $key
     * @return string
     */
    protected function getChar($key)
    {
        $array = [];
        $range = range(65, 90);

        foreach ($range as $letter) {
            $array[] = chr($letter);
        }

        $str = "";
        do {
            $index = $key % 26;
            $str = $array[$index] . $str;
            $key /= 26;
            $key = intval($key) - 1;
        } while ($key >= 0);

        return $str;
    }
}
