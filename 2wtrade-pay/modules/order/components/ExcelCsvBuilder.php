<?php

namespace app\modules\order\components;

use app\helpers\Utils;
use app\modules\order\components\exceptions\ExcelBuilderException;
use app\modules\order\models\Order;
use app\modules\order\models\OrderLogisticList;
use app\modules\order\models\OrderLogisticListExcel;
use Yii;

/**
 * Class ExcelCsvBuilder
 * @package app\modules\order\components
 */
class ExcelCsvBuilder extends ExcelBuilder
{
    protected $format = OrderLogisticListExcel::FORMAT_CSV;

    /**
     * @return array
     */
    public function generate()
    {
        set_time_limit(0);

        $result = [
            'status' => 'fail',
            'message' => '',
        ];

        if ($excel = $this->excelIsExists()) {
            $result['status'] = 'success';
            $result['message'] = $excel->system_file_name;
        } else {
            try {
                $orders = $this->getOrders();

                $excel = new \PHPExcel();
                $excel->getProperties()->setCreator(Yii::$app->params['companyName']);
                $excel->setActiveSheetIndex(0);

                $sheet = $excel->getActiveSheet();

                $index = 0;

                foreach (self::$columns as $column) {
                    if (!in_array($column, $this->selectedColumns)) {
                        continue;
                    }

                    $char = $this->getChar($index);
                    $coordinate = $char . '1';

                    $sheet->setCellValue($coordinate, self::getColumnLabel($column));

                    $index++;
                }

                $number = 2;

                foreach ($orders as $order) {
                    $index = 0;

                    foreach (self::$columns as $column) {
                        if (!in_array($column, $this->selectedColumns)) {
                            continue;
                        }

                        $char = $this->getChar($index);
                        $coordinate = $char . $number;

                        if ($column == 'row_number') {
                            $value = $number - 1;
                        } else {
                            $value = $this->getValue($order, $column);
                        }
                        $sheet->setCellValue($coordinate, $value);

                        $index++;
                    }

                    $number++;
                }

                $writer = new \PHPExcel_Writer_CSV($excel);
                $writer->setDelimiter(',');
                $writer->setUseBOM(true);

                $fileName = Utils::uid() . '.' . 'tmp';
                $filePath = OrderLogisticList::getDownloadPath($fileName, OrderLogisticList::DOWNLOAD_TYPE_EXCELS,
                        true) . DIRECTORY_SEPARATOR . $fileName;

                $writer->save($filePath);

                $date = Yii::$app->formatter->asDate($this->list->created_at, 'php:YmdHi');

                $excelLocal = $date . '.' . OrderLogisticListExcel::getExtension($this->format);

                $excel = new OrderLogisticListExcel([
                    'list_id' => $this->list->id,
                    'system_file_name' => $fileName,
                    'orders_hash' => $this->list->getOrdersHash(),
                    'delivery_file_name' => $excelLocal,
                    'format' => $this->format,
                    'columns_hash' => $this->selectedColumnsHash,
                    'user_id' => Yii::$app->user->id,
                    'waiting_for_send' => $this->waitingForSend,
                ]);

                if ($excel->save()) {
                    $result['status'] = 'success';
                    $result['message'] = $fileName;
                } else {
                    $error = $excel->getErrorsAsArray()[0];

                    throw new ExcelBuilderException(Yii::t('common', $error), 400);
                }
            } catch (ExcelBuilderException $e) {
                $result['message'] = $e->getMessage();
            }
        }

        return $result;
    }

    /**
     * @param Order $order
     * @param string $column
     * @return integer
     */
    protected function getValue($order, $column)
    {
        $value = '';
        $customerAddressAdd = json_decode($order->customer_address_add, true);
        $dateTime = new \DateTime('now', new \DateTimeZone($this->list->country->timezone->timezone_id));
        switch ($column) {
            case 'id':
            case 'customer_full_name':
            case 'customer_phone':
            case 'customer_mobile':
            case 'customer_address':
            case 'customer_province':
            case 'customer_city':
            case 'customer_street':
            case 'customer_district':
            case 'customer_subdistrict':
            case 'customer_house_number':
            case 'customer_zip':
            case 'comment':
                $value = $order->$column;
                break;
            case 'customer_address_add':
                $data = json_decode($order->$column, 1);

                $details = [];

                if (is_array($data)) {
                    foreach ($data as $name => $value) {
                        $details[] = $name . ' : ' . $value;
                    }
                }

                $value = !empty($details) ? implode(', ', $details) : '';
                break;
            case 'id_cc':
                $value = (isset($order->callCenterRequest)) ? $order->callCenterRequest->foreign_id : '';
                break;
            case 'comment_call_center':
                $value = (isset($order->callCenterRequest)) ? $order->callCenterRequest->comment : '';
                break;
            case 'region':
                $value = $customerAddressAdd['customer_state'] ?? $customerAddressAdd['customer_region'] ?? '';
                break;
            case 'products_count':
                $lines = [];

                foreach ($order->orderProducts as $orderProduct) {
                    if (empty($lines[$orderProduct->product_id])) {
                        $lines[$orderProduct->product_id] = $orderProduct->quantity;
                    } else {
                        $lines[$orderProduct->product_id] += $orderProduct->quantity;
                    }
                }

                $value = implode(PHP_EOL, $lines);
                break;
            case 'products_name':
                $lines = [];

                foreach ($order->orderProducts as $orderProduct) {
                    $lines[] = $orderProduct->product->name;
                }

                $value = implode(PHP_EOL, $lines);
                break;
            case 'products':
                $linesParts = null;
                foreach ($order->orderProducts as $orderProduct) {
                    if (isset($linesParts[$orderProduct->product_id])) {
                        $linesParts[$orderProduct->product_id]['quantity'] += $orderProduct->quantity;
                    } else {
                        $linesParts[$orderProduct->product_id] = [
                            'name' => $orderProduct->product->name,
                            'quantity' => $orderProduct->quantity,
                        ];
                    }
                }

                $value = '';
                foreach ($linesParts as $linesPart) {
                    $value .= $linesPart['name'] . '-' . $linesPart['quantity'] . ', ';
                }

                $value = rtrim($value, ', ');
                break;
            case 'price':
                $lines = [];

                foreach ($order->orderProducts as $orderProduct) {
                    $lines[] = $orderProduct->price;
                }

                $value = implode(PHP_EOL, $lines);
                break;
            case 'delivery':
                $value = $order->delivery;
                break;
            case 'sum':
                $value = 0;

                foreach ($order->orderProducts as $orderProduct) {
                    $value += $orderProduct->price;
                }

                $value += $order->delivery;

                break;
            case 'date_delivery':
                $value = $dateTime->setTimestamp($order->delivery_time_from)->format('Y-m-d');
                break;
            case 'date_send':
                $value = $dateTime->format('Y-m-d');
                break;
            case 'express':
                $value = $order->getOrderExpressDelivery() ? 'yes' : '-';
                break;
            case 'approve_date':
                $value = $dateTime->setTimestamp($order->callCenterRequest->approved_at)->format('Y-m-d');
                break;
            case 'approve_operator':
                $value = $order->callCenterRequest->last_foreign_operator;
                break;
            case 'customer_address_additional':
                $value = $order->customer_address_additional;
                if (empty($value)) {
                    $value = $customerAddressAdd['customer_address_additional'] ?? '';
                }
                break;
            case 'customer_address_add_embedded':
                $value = $customerAddressAdd['customer_address_add'] ?? '';
                break;
        }

        return $value;
    }
}
