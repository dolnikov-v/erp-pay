<?php

namespace app\modules\order\components\exporter;

use app\models\User;
use app\modules\callcenter\models\CallCenterCheckRequest;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\models\OrderExport;
use app\modules\order\models\OrderLogisticList;
use app\modules\order\widgets\ShowerColumns;
use PHPExcel;
use PHPExcel_Writer_Excel2007;
use Yii;
use app\modules\order\models\Order;
use yii\helpers\ArrayHelper;
use app\models\Currency;

/**
 * Class ExporterExcel
 * @package app\modules\order\components\exporter
 */
class ExporterExcel extends Exporter
{
    protected static $type = self::TYPE_EXCEL;

    /**
     * @param OrderExport $orderExport
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Writer_Exception
     * @throws \yii\base\ExitException
     * @return array
     */
    public function sendFile($orderExport)
    {
        $excel = new PHPExcel();
        $excel->setActiveSheetIndex(0);

        // Установка автоматической ширины по заголовкам
        foreach (range('A', 'Z') as $columnID) {
            $excel->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        // Выбор страницы и её название
        $sheet = $excel->getActiveSheet();
        $sheet->setTitle(Yii::t('common', 'Заказы', [], $orderExport->language));

        // Установка заголовка
        $iHead = 0;
        $columns = json_decode($orderExport->columns, true);
        foreach ($columns as $item) {
            if (isset($item['subquery'])) {
                foreach ($item['subquery'] as $nameColumn) {
                    $sheet->setCellValue($this->printAlphabet($iHead) . 1, $nameColumn);
                    $iHead++;
                }
            } else {
                $sheet->setCellValue($this->printAlphabet($iHead) . 1, $item['name']);
                $iHead++;
            }
        }

        // Заполнение данными
        $offset = 0;
        $limit = 10000;

        //Получаем валюты
        $currencyCollection = Currency::find()->customCollection('id', 'char_code');

        $orderIds = [];
        while ($orders = $this->getRequestData($offset, $limit)) {

            foreach ($orders as $key => $order) {
                $i = 0;
                $order[ShowerColumns::COLUMN_PRICE_TOTAL_WITH_DELIVERY] = $order[ShowerColumns::COLUMN_PRICE_TOTAL] + $order[ShowerColumns::COLUMN_DELIVERY];
                $order[ShowerColumns::COLUMN_CUSTOMER_STREET_HOUSE] = $order[ShowerColumns::COLUMN_CUSTOMER_STREET] . ' ' . $order[ShowerColumns::COLUMN_CUSTOMER_HOUSE];

                if (isset($columns[ShowerColumns::COLUMN_GROUP_ORDER_LOGISTIC_LIST])) {
                    $order[ShowerColumns::COLUMN_GROUP_ORDER_LOGISTIC_LIST][ShowerColumns::COLUMN_IN_GROUP_ORDER_LOGISTIC_LIST_ID] = OrderLogisticList::firstListOrder($order[ShowerColumns::COLUMN_RECORD_ID]);
                }

                if (isset($columns[ShowerColumns::COLUMN_PRETENSION])) {
                    $order[ShowerColumns::COLUMN_PRETENSION] = $order['orderFinancePretensions'];
                }

                if (!isset($order[ShowerColumns::COLUMN_GROUP_DELIVERY_REQUEST]) || $order[ShowerColumns::COLUMN_GROUP_DELIVERY_REQUEST] == null) {
                    $order[ShowerColumns::COLUMN_GROUP_DELIVERY_REQUEST][ShowerColumns::COLUMN_IN_GROUP_DELIVERY_REQUEST_DELIVERY_ID] = $order[ShowerColumns::COLUMN_PENDING_DELIVERY_ID];
                }

                if (isset($columns[ShowerColumns::COLUMN_CALLS_COUNT])) {
                    $order[ShowerColumns::COLUMN_CALLS_COUNT] = isset($order['callCenterRequest']['callCenterRequestCallData']) ? sizeof($order['callCenterRequest']['callCenterRequestCallData']) : 0;
                }

                if (isset($columns[ShowerColumns::COLUMN_CALL_CENTER_CHECK_REQUEST_RESULT])) {
                    $foreign_information = "—";

                    if (isset($order['callCenterCheckRequests'])) {
                        $lastRequest = array_pop($order['callCenterCheckRequests']);

                        if (!empty($lastRequest)) {
                            $notDone = CallCenterCheckRequest::getSubStatusesForCheckUndelivery(true);
                            $done = CallCenterCheckRequest::getSubStatusesForCheckUndelivery();

                            if ($lastRequest['status'] != CallCenterCheckRequest::STATUS_DONE) {
                                $foreign_information = $notDone[CallCenterCheckRequest::FOREIGN_SUB_STATUS_EMPTY];
                            } else {
                                $foreign_information = isset($done[$lastRequest['foreign_information']])
                                    ? $done[$lastRequest['foreign_information']]
                                    : CallCenterCheckRequest::getStatusByName($lastRequest['status']);
                            }
                        }
                    }

                    $order[ShowerColumns::COLUMN_CALL_CENTER_CHECK_REQUEST_RESULT] = $foreign_information;
                }


                foreach (ShowerColumns::getFactColumns() as $factColumn) {
                    if (isset($columns[$factColumn])) {
                        $nameFact = explode(".", $factColumn)[1];
                        $valueFact = 0;
                        $return = 0;

                        foreach ($order['financeFact'] as $keyFact => $fact) {
                            $valueFact += $fact[$nameFact];
                            $currencyId = $fact[$nameFact . '_currency_id'];
                            $currencyRate = $fact[$nameFact . '_currency_rate'];

                            if ($valueFact > 0) {
                                $return =
                                    $valueFact . ' ' .
                                    ($currencyId ? $currencyCollection[$currencyId] ?? '' : '') .
                                    ($currencyRate > 0 ? ' ' . Yii::t('common', 'курс', [], $orderExport->language) . ': ' . $currencyRate : "");
                            }
                        }

                        $order[$factColumn] = $return;
                    }
                }


                $orderIds[] = $order['id'];
                foreach ($columns as $keyColumn => $column) {
                    if (isset($column['subquery'])) {
                        foreach ($column['subquery'] as $columnSub => $item) {
                            $dataCell = "—";
                            if (isset($order[$keyColumn][$columnSub])) {
                                $dataCell = $this->prepareMapping($keyColumn . "." . $columnSub, $order[$keyColumn][$columnSub], $orderExport->language, $orderExport->date_format);
                            }
                            $sheet->setCellValue($this->printAlphabet($i) . ($offset + $key + 2), $dataCell);
                            $i++;
                        }
                    } else {
                        $dataCell = $this->prepareMapping($keyColumn, ArrayHelper::getValue($order, $keyColumn), $orderExport->language, $orderExport->date_format);
                        $sheet->setCellValue($this->printAlphabet($i) . ($offset + $key + 2), $dataCell);
                        $i++;
                    }
                }
            }

            $offset += $limit;
        }

        if ($orderExport->after_action == OrderExport::ACTION_SEND_CLARIFICATION) {
            DeliveryRequest::sendClarification($orderIds);
        }

        $path = Yii::getAlias('@export') . DIRECTORY_SEPARATOR . $orderExport->user_id;

        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        $filename = 'orders-2wtrade-' . date('YmdHis') . '_' . str_pad(random_int(1, 99), 2, '0', STR_PAD_LEFT) . '.xlsx';

        $objWriter = new PHPExcel_Writer_Excel2007($excel);
        $objWriter->save($path . DIRECTORY_SEPARATOR . $filename);

        return [
            'path' => $path,
            'filename' => $filename,
            'lines' => sizeof($orderIds)
        ];
    }
}

