<?php

namespace app\modules\order\components\exporter;

use app\models\Currency;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\models\OrderExport;
use app\modules\order\models\OrderFinanceFact;
use PHPExcel;
use PHPExcel_Writer_Excel2007;
use Yii;

/**
 * Class ExporterFactexcel
 * @package app\modules\order\components\exporter
 */
class ExporterFactexcel extends ExporterExcel
{
    protected static $type = self::TYPE_FACT_EXCEL;

    /**
     * @return array
     */
    public function getShowerColumns(): array
    {
        $chosenColumns = $this->showerColumns->getChosenColumns();
        $necessaryColumns = [
            'id' => [
                'name' => Yii::t('common', 'Заказ'),
            ],
        ];
        if (in_array('name', $chosenColumns)) {
            $necessaryColumns['delivery_report_id'] = ['name' => Yii::t('common', 'ИД отчёта')];
            $necessaryColumns['name'] = ['name' => Yii::t('common', 'Отчёт')];
        }
        if (in_array('payment', $chosenColumns)) {
            $necessaryColumns['payment'] = ['name' => Yii::t('common', 'Платёжка')];
        }
        foreach (OrderFinanceFact::getReportColumns()['column'] as $data) {
            if (in_array($data, $chosenColumns)) {
                $necessaryColumns[$data] = ['name' => OrderFinanceFact::attributeLabels()[$data] ?? $data];
                $necessaryColumns[$data . '_currency_id'] = ['name' => OrderFinanceFact::attributeLabels()[$data . '_currency_id'] ?? $data . '_currency_id'];
                $necessaryColumns[$data . '_currency_rate'] = ['name' => OrderFinanceFact::attributeLabels()[$data . '_currency_rate'] ?? $data . '_currency_rate'];
            }
        }

        return $necessaryColumns;
    }

    /**
     * @param OrderExport $orderExport
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Writer_Exception
     * @throws \yii\base\ExitException
     * @return array
     */
    public function sendFile($orderExport)
    {
        $excel = new PHPExcel();
        $excel->setActiveSheetIndex(0);

        // Установка автоматической ширины по заголовкам
        foreach (range('A', 'Z') as $columnID) {
            $excel->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        // Выбор страницы и её название
        $sheet = $excel->getActiveSheet();
        $sheet->setTitle(Yii::t('common', 'Заказы', [], $orderExport->language));

        // Установка заголовка
        $iHead = 0;
        $columns = json_decode($orderExport->columns, true);
        foreach ($columns as $item) {
            if (isset($item['subquery'])) {
                foreach ($item['subquery'] as $nameColumn) {
                    $sheet->setCellValue($this->printAlphabet($iHead) . 1, $nameColumn);
                    $iHead++;
                }
            } else {
                $sheet->setCellValue($this->printAlphabet($iHead) . 1, $item['name']);
                $iHead++;
            }
        }

        // Заполнение данными
        $offset = 0;
        $limit = 10000;

        $currencies = Currency::find()->customCollection('id', 'char_code');
        $orderIds = [];
        while ($orders = $this->getRequestData($offset, $limit)) {

            foreach ($orders as $key => $order) {
                $i = 0;

                $orderIds[] = $order['id'];
                foreach ($columns as $keyColumn => $column) {
                    $dataCell = $this->prepareMapping($keyColumn, $order[$keyColumn], $orderExport->language, $orderExport->date_format);
                    if (strstr($keyColumn, '_currency_id')) {
                        $dataCell = $currencies[$dataCell] ?? $dataCell;
                    }
                    $sheet->setCellValue($this->printAlphabet($i) . ($offset + $key + 2), $dataCell);
                    $i++;
                }
            }

            $offset += $limit;
        }

        if ($orderExport->after_action == OrderExport::ACTION_SEND_CLARIFICATION) {
            DeliveryRequest::sendClarification($orderIds);
        }

        $path = Yii::getAlias('@export') . DIRECTORY_SEPARATOR . $orderExport->user_id;

        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        $filename = 'orders-2wtrade-' . date('YmdHis') . '_' . str_pad(random_int(1, 99), 2, '0', STR_PAD_LEFT) . '.xlsx';

        $objWriter = new PHPExcel_Writer_Excel2007($excel);
        $objWriter->save($path . DIRECTORY_SEPARATOR . $filename);

        return [
            'path' => $path,
            'filename' => $filename,
            'lines' => sizeof($orderIds)
        ];
    }
}

