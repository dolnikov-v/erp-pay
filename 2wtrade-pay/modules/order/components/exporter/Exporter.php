<?php

namespace app\modules\order\components\exporter;

use app\components\i18n\Formatter;
use app\models\Currency;
use app\modules\catalog\models\UnBuyoutReason;
use app\modules\order\models\Order;
use app\modules\order\models\OrderExport;
use app\modules\order\models\OrderFinancePretension;
use app\modules\order\models\query\OrderQuery;
use app\modules\order\widgets\ShowerColumns;
use Yii;
use yii\base\BaseObject;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;

/**
 * Class Exporter
 * @package app\modules\order\components\exporter
 */
abstract class Exporter extends BaseObject
{
    const TYPE_EXCEL = 'excel';
    const TYPE_CSV = 'csv';
    const TYPE_ZIP = 'zip';
    const TYPE_FACT_EXCEL = 'factExcel';

    /** @var ActiveDataProvider */
    public $dataProvider;

    /** @var ShowerColumns */
    public $showerColumns;

    /** @var array */
    public $orderStatus;

    /** @var array */
    public $products;

    /** @var array */
    public $deliveries;

    /** @var array */
    public $callCenters;

    /** @var array */
    public $unBuyoutReasons;

    /**
     * @var Formatter
     */
    public $formatter;

    /**
     * @var string
     */
    public $language;

    /**
     * @var string
     */
    public $timezoneId;

    /**
     * @var string
     */
    protected static $type;

    abstract public function sendFile($orderExport);

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        if (empty($this->formatter)) {
            $this->formatter = clone Yii::$app->formatter;
        }
        if ($this->timezoneId) {
            $this->formatter->setTimezone($this->timezoneId);
        }
        if ($this->language) {
            $this->formatter->setLocale($this->language);
        }
    }

    /**
     * @param $queryParams
     * @return array
     */
    public function orderFile($queryParams)
    {
        $is = OrderExport::find()->where(
            [
                'country_id' => Yii::$app->user->country->id,
                'user_id' => Yii::$app->user->id,
                'type' => static::$type,
                'query' => json_encode($queryParams),
                'columns' => json_encode($this->getShowerColumns()),
                'generated_at' => null
            ]
        )->one();

        if ($is) {
            return [
                'status' => 'fail',
                'message' => Yii::t('common', 'Такое задание уже есть в очереди.')
            ];
        } else {
            $model = new OrderExport();
            $model->country_id = Yii::$app->user->country->id;
            $model->user_id = Yii::$app->user->id;
            $model->date_format = Yii::$app->formatter->datetimeFormat;
            $model->language = Yii::$app->user->language->locale;
            $model->type = static::$type;
            $model->url = $this->getExporterUrl();
            $model->query = json_encode($queryParams);
            $model->columns = json_encode($this->getShowerColumns());

            $clarification = Yii::$app->request->get('clarification');
            if ($clarification == 'send') {
                $model->after_action = OrderExport::ACTION_SEND_CLARIFICATION;
            }

            if ($model->save()) {
                return [
                    'status' => 'success',
                    'message' => Yii::t('common', 'Задание на экспорт успешно создано.')
                ];
            } else {
                return [
                    'status' => 'fail',
                    'message' => $model->getFirstErrorAsString()
                ];
            }
        }
    }


    /**
     * @return string
     */
    public static function getExporterUrl($extraParams = null)
    {
        $queryParams = Yii::$app->request->getQueryParams();
        if ($extraParams && is_array($extraParams)) {
            $queryParams = array_merge($queryParams, $extraParams);
        }
        $queryParams['export'] = static::$type;
        $url = array_merge(['export'], $queryParams);

        return Url::to($url);
    }

    /**
     * @return array
     */
    public function getShowerColumns()
    {

        $exportProhibitedColumns = [
            ShowerColumns::COLUMN_CUSTOMER_FULL_NAME,
            ShowerColumns::COLUMN_CUSTOMER_ADDRESS,
            ShowerColumns::COLUMN_CUSTOMER_STREET_HOUSE,
            ShowerColumns::COLUMN_CUSTOMER_PHONE,
            ShowerColumns::COLUMN_CUSTOMER_MOBILE,
            ShowerColumns::COLUMN_CUSTOMER_EMAIL,
            ShowerColumns::COLUMN_CUSTOMER_ADDRESS_ADDITIONAL,
        ];


        $chosenColumns = $this->showerColumns->getChosenColumns();
        $allColumns = $this->showerColumns->getManualColumnsCollection();

        $necessaryColumns = [
            'id' => [
                'name' => '#',
            ]
        ];

        foreach ($allColumns as $key => $column) {
            if (in_array($key, $chosenColumns) && (!in_array($key, $exportProhibitedColumns) || Yii::$app->user->can('order.index.index.exportprohibitedcolumns'))) {
                $necessaryColumns += [
                    $key => [
                        'name' => $column
                    ]
                ];
            }
        }

        return $necessaryColumns;
    }

    /**
     * @param int $offset
     * @param int $limit
     * @return array
     */
    public function getRequestData($offset, $limit)
    {
        /** @var OrderQuery $query */
        /** @var array $arrayData */
        $query = $this->dataProvider->query;

        $query->with(['financeFact']);
        $query->orderBy(Order::tableName() . '.id');
        $query->limit($limit);
        $query->offset($offset);

        return $query->asArray()->all();
    }

    /**
     * @param $offset
     * @param $limit
     * @return Order[]
     */
    public function getRequestObject($offset, $limit)
    {
        /** @var OrderQuery $query */
        /** @var array $arrayData */
        $query = $this->dataProvider->query;

        $query->orderBy(Order::tableName() . '.id');
        $query->limit($limit);
        $query->offset($offset);

        return $query->all();
    }

    /**
     * @param $key
     * @return string
     */
    protected function printAlphabet($key)
    {
        $array = [];
        $range = range(65, 90);

        foreach ($range as $letter) {
            $array[] = chr($letter);
        }

        if ($key >= sizeof($array)) {
            // алфавита не хватило на число колонок вернем тиап AA, AB
            return $array[0] . $array[$key - sizeof($array)];
        }
        return $array[$key];
    }

    /**
     * @param $field
     * @param $dataCell
     * @param string $language
     * @param string $dateFormat
     * @param integer $decimals
     * @return mixed|string
     */
    protected function prepareMapping($field, $dataCell, $language = null, $dateFormat = null, $decimals = 2)
    {
        switch ($field) {
            case ShowerColumns::COLUMN_STATUS_ID:
                return $this->prepareStatusOrder($dataCell);
            case ShowerColumns::COLUMN_DELIVERY_DATE:
            case ShowerColumns::COLUMN_CALL_CENTER_APPROVED_AT:
            case ShowerColumns::COLUMN_CALL_CENTER_SENT_AT:
            case ShowerColumns::COLUMN_CREATED_AT:
            case ShowerColumns::COLUMN_UPDATED_AT:
            case ShowerColumns::COLUMN_DELIVERY_SENT_AT:
            case ShowerColumns::COLUMN_DELIVERY_ACCEPTED_AT:
            case ShowerColumns::COLUMN_DELIVERY_APPROVED_AT:
            case ShowerColumns::COLUMN_DELIVERY_PAID_AT:
            case ShowerColumns::COLUMN_TS_SPAWN:
                return $this->prepareDate($dataCell, $dateFormat);
            case ShowerColumns::COLUMN_PRODUCTS:
                return $this->prepareProducts($dataCell);
            case ShowerColumns::COLUMN_DELIVERY_NAME:
                return $this->prepareDelivery($dataCell);
            case ShowerColumns::COLUMN_CALL_CENTER_NAME:
                return $this->prepareCallCenter($dataCell);
            case ShowerColumns::COLUMN_EXPRESS_DELIVERY:
                return $this->prepareBoolean($dataCell, $language);
            case ShowerColumns::COLUMN_UNSHIPPING_REASON:
                return $this->prepareUnBuyoutReasons($dataCell, $language);
            case ShowerColumns::COLUMN_PRICE:
            case ShowerColumns::COLUMN_PRICE_TOTAL:
            case ShowerColumns::COLUMN_PRICE_TOTAL_WITH_DELIVERY:
            case ShowerColumns::COLUMN_FINANCE_PREDICTION_STORAGE:
            case ShowerColumns::COLUMN_FINANCE_PREDICTION_PACKING:
            case ShowerColumns::COLUMN_FINANCE_PREDICTION_PACKAGE:
            case ShowerColumns::COLUMN_FINANCE_PREDICTION_ADDRESS_CORRECTION:
            case ShowerColumns::COLUMN_FINANCE_PREDICTION_DELIVERY:
            case ShowerColumns::COLUMN_FINANCE_PREDICTION_REDELIVERY:
            case ShowerColumns::COLUMN_FINANCE_PREDICTION_DELIVERY_BACK:
            case ShowerColumns::COLUMN_FINANCE_PREDICTION_DELIVERY_RETURN:
            case ShowerColumns::COLUMN_FINANCE_PREDICTION_COD_SERVICE:
            case ShowerColumns::COLUMN_FINANCE_PREDICTION_VAT:
            case ShowerColumns::COLUMN_FINANCE_PREDICTION_FULFILMENT:
                return $this->preparePrice($dataCell, $decimals);
            case ShowerColumns::COLUMN_PRETENSION:
                return $this->preparePretension($dataCell);
            case ShowerColumns::COLUMN_PRICE_CURRENCY:
                return $this->preparePriceCurrency($dataCell);
            default:
                return $dataCell ? ltrim($dataCell, "=") : "—";
        }
    }

    /**
     * @param $callCenter
     * @return mixed|string
     */
    protected function prepareCallCenter($callCenter)
    {
        return array_key_exists($callCenter, $this->callCenters) ? $this->callCenters[$callCenter] : "—";
    }

    protected function preparePriceCurrency($currency_id)
    {
        $currencyCollection = Currency::find()->collection();

        return array_key_exists($currency_id, $currencyCollection) ? $currencyCollection[$currency_id] : "—";
    }

    /**
     * @param $delivery
     * @return mixed|string
     */
    protected function prepareDelivery($delivery)
    {
        return array_key_exists($delivery, $this->deliveries) ? $this->deliveries[$delivery] : "—";
    }

    /**
     * @param $order
     * @return string
     */
    protected function prepareStatusOrder($order)
    {
        return array_key_exists($order, $this->orderStatus) ? $this->orderStatus[$order] : "—";
    }

    /**
     * @param string $date
     * @param string $format
     * @return string
     */
    protected function prepareDate($date, $format = null)
    {
        if (!$date) return '—';
        return $this->formatter->asDatetime($date, $format);
    }

    /**
     * @param $products
     * @return string
     */
    protected function prepareProducts($products)
    {
        $data = [];

        if ($products) {
            foreach ($products as $key => $product) {
                $data[] = $this->products[$product['product_id']] . " - " . $product['quantity'] . (empty($product['price']) ? ' GIFT' : '');
            }
        }

        return implode(", ", $data);
    }

    /**
     * @param mixed $value
     * @param string $language
     * @return string
     */
    protected function prepareBoolean($value, $language = null)
    {
        return Yii::t('common', $value ? 'Да' : 'Нет', [], $language);
    }

    /**
     * @param mixed $value
     * @param string $language
     * @return string
     */
    protected function prepareUnBuyoutReasons($value, $language = null)
    {
        if (!$this->unBuyoutReasons) {
            $this->unBuyoutReasons = UnBuyoutReason::find()->collection();
        }
        $return = '';
        if (isset($this->unBuyoutReasons[$value['reason_id']])) {
            $return = Yii::t('common', $this->unBuyoutReasons[$value['reason_id']], [], $language);
        }
        if (!empty($value['foreign_reason'])) {
            $return .= ': ' . $value['foreign_reason'];
        }
        return $return;
    }

    /**
     * @param $value
     * @param null $decimals
     * @return string
     */
    protected function preparePrice($value, $decimals = null)
    {
        if (is_null($value) || $value == '') return '—';

        return $this->formatter->asDecimal($value, $decimals);
    }

    /**
     * @param mixed $value
     * @param string $language
     * @return string
     */
    protected function preparePretension($value, $language = null)
    {
        $pretensions = OrderFinancePretension::getTypeCollecion();

        $return = [];
        foreach ($value as $pretension) {
            if ($pretension['type'] && isset($pretensions[$pretension['type']])) {
                $return[] = Yii::t('common', $pretensions[$pretension['type']], [], $language);
            }
        }
        if ($return) {
            return implode(' ', $return);
        }
        return '';
    }

}