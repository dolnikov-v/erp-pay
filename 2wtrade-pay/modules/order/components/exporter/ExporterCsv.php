<?php

namespace app\modules\order\components\exporter;

use app\modules\callcenter\models\CallCenterCheckRequest;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\models\OrderExport;
use app\modules\order\models\OrderLogisticList;
use app\modules\order\widgets\ShowerColumns;
use Yii;
use yii\helpers\ArrayHelper;
use app\models\Currency;

/**
 * Class ExporterCsv
 * @package app\modules\order\components\exporter
 */
class ExporterCsv extends Exporter
{
    protected static $type = self::TYPE_CSV;

    static $delimiter = ',';

    function enclosure($item)
    {
        $quote = chr(34); // quote " character from ASCII table
        return $quote . addslashes(strval($item)) . $quote;
    }

    /**
     * @param OrderExport $orderExport
     * @return array
     */
    public function sendFile($orderExport)
    {
        $path = Yii::getAlias('@export') . DIRECTORY_SEPARATOR . $orderExport->user_id;

        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        $filename = 'orders-2wtrade-' . date('YmdHis') . '_' . str_pad(random_int(1, 99), 2, '0', STR_PAD_LEFT) . '.csv';

        $file = fopen($path . DIRECTORY_SEPARATOR . $filename, 'w');

        $columns = json_decode($orderExport->columns, true);
        $head = [];
        // Установка заголовка
        foreach ($columns as $item) {
            if (isset($item['subquery'])) {
                foreach ($item['subquery'] as $nameColumn) {
                    $head[] = self::enclosure($nameColumn);
                }
            } else {
                $head[] = self::enclosure($item['name']);
            }
        }
        fwrite($file, implode(self::$delimiter, $head) . PHP_EOL);

        // Заполнение данными
        $offset = 0;
        $limit = 10000;

        //Получаем валюты
        $currencyCollection = Currency::find()->customCollection('id', 'char_code');

        $orderIds = [];

        while ($orders = $this->getRequestData($offset, $limit)) {
            foreach ($orders as $key => $order) {
                $line = [];
                $orderIds[] = $order['id'];

                $order[ShowerColumns::COLUMN_PRICE_TOTAL_WITH_DELIVERY] = $order[ShowerColumns::COLUMN_PRICE_TOTAL] + $order[ShowerColumns::COLUMN_DELIVERY];
                $order[ShowerColumns::COLUMN_CUSTOMER_STREET_HOUSE] = $order[ShowerColumns::COLUMN_CUSTOMER_STREET] . ' ' . $order[ShowerColumns::COLUMN_CUSTOMER_HOUSE];

                if (isset($columns[ShowerColumns::COLUMN_GROUP_ORDER_LOGISTIC_LIST])) {
                    $order[ShowerColumns::COLUMN_GROUP_ORDER_LOGISTIC_LIST][ShowerColumns::COLUMN_IN_GROUP_ORDER_LOGISTIC_LIST_ID] = OrderLogisticList::firstListOrder($order[ShowerColumns::COLUMN_RECORD_ID]);
                }

                if (isset($columns[ShowerColumns::COLUMN_PRETENSION])) {
                    $order[ShowerColumns::COLUMN_PRETENSION] = $order['orderFinancePretensions'];
                }

                if (!isset($order[ShowerColumns::COLUMN_GROUP_DELIVERY_REQUEST]) || $order[ShowerColumns::COLUMN_GROUP_DELIVERY_REQUEST] == null) {
                    $order[ShowerColumns::COLUMN_GROUP_DELIVERY_REQUEST][ShowerColumns::COLUMN_IN_GROUP_DELIVERY_REQUEST_DELIVERY_ID] = $order[ShowerColumns::COLUMN_PENDING_DELIVERY_ID];
                }

                if (isset($columns[ShowerColumns::COLUMN_CALLS_COUNT])) {
                    $order[ShowerColumns::COLUMN_CALLS_COUNT] = isset($order['callCenterRequest']['callCenterRequestCallData']) ? sizeof($order['callCenterRequest']['callCenterRequestCallData']) : 0;
                }
                
                if (isset($columns[ShowerColumns::COLUMN_CALL_CENTER_CHECK_REQUEST_RESULT])) {
                    $foreign_information = "—";

                    if (isset($order['callCenterCheckRequests'])) {
                        $lastRequest = array_pop($order['callCenterCheckRequests']);

                        if (!empty($lastRequest)) {
                            $notDone = CallCenterCheckRequest::getSubStatusesForCheckUndelivery(true);
                            $done = CallCenterCheckRequest::getSubStatusesForCheckUndelivery();

                            if ($lastRequest['status'] != CallCenterCheckRequest::STATUS_DONE) {
                                $foreign_information = $notDone[CallCenterCheckRequest::FOREIGN_SUB_STATUS_EMPTY];
                            } else {
                                $foreign_information = isset($done[$lastRequest['foreign_information']])
                                    ? $done[$lastRequest['foreign_information']]
                                    : CallCenterCheckRequest::getStatusByName($lastRequest['status']);
                            }
                        }
                    }

                    $order[ShowerColumns::COLUMN_CALL_CENTER_CHECK_REQUEST_RESULT] = $foreign_information;
                }


                foreach (ShowerColumns::getFactColumns() as $factColumn) {
                    if (isset($columns[$factColumn])) {
                        $nameFact = explode(".", $factColumn)[1];
                        $valueFact = 0;
                        $return = 0;

                        foreach ($order['financeFact'] as $keyFact => $fact) {
                            $valueFact += $fact[$nameFact];
                            $currencyId = $fact[$nameFact . '_currency_id'];
                            $currencyRate = $fact[$nameFact . '_currency_rate'];

                            if ($valueFact > 0) {
                                $return =
                                    $valueFact . ' ' .
                                    ($currencyId ? $currencyCollection[$currencyId] ?? '' : '') .
                                    ($currencyRate > 0 ? ' ' . Yii::t('common', 'курс', [], $orderExport->language) . ': ' . $currencyRate : "");
                            }
                        }

                        $order[$factColumn] = $return;
                    }
                }


                foreach ($columns as $keyColumn => $column) {

                    if (isset($column['subquery'])) {
                        foreach ($column['subquery'] as $columnSub => $item) {
                            $dataCell = "—";
                            if (isset($order[$keyColumn][$columnSub])) {
                                $dataCell = $this->prepareMapping($keyColumn . "." . $columnSub, $order[$keyColumn][$columnSub], $orderExport->language, $orderExport->date_format);
                            }
                            $line[] = self::enclosure($dataCell);
                        }
                    } else {
                        $dataCell = $this->prepareMapping($keyColumn, ArrayHelper::getValue($order, $keyColumn), $orderExport->language, $orderExport->date_format);
                        $line[] = self::enclosure($dataCell);
                    }
                }
                fwrite($file, implode(self::$delimiter, $line) . PHP_EOL);
            }

            $offset += $limit;
        }

        if ($orderExport->after_action == OrderExport::ACTION_SEND_CLARIFICATION) {
            DeliveryRequest::sendClarification($orderIds);
        }

        fclose($file);

        return [
            'path' => $path,
            'filename' => $filename,
            'lines' => sizeof($orderIds)
        ];
    }
}
