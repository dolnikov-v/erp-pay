<?php

namespace app\modules\order\components\exporter;

use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\models\OrderExport;
use Yii;

/**
 * Class ExporterExcel
 * @package app\modules\order\components\exporter
 */
class ExporterZip extends Exporter
{
    protected static $type = self::TYPE_ZIP;

    public function sendFile($orderExport)
    {
        // Заполнение данными
        $offset = 0;
        $limit = 10000;
        $files = [];
        $orderIds = [];
        while ($orders = $this->getRequestObject($offset, $limit)) {
            foreach ($orders as $key => $order) {
                $orderIds[] = $order->id;
                $newPath = true;
                $pattern = Yii::getAlias('@downloads') . '/order_labels/' . $order->country_id .
                    DIRECTORY_SEPARATOR . $order->deliveryRequest->delivery_id .
                    DIRECTORY_SEPARATOR . $order->id . ".*";
                foreach (glob($pattern, GLOB_NOSORT) as $item) {
                    $newPath = false;
                    $files[] = [$item, mb_strstr($item, $order->id . '.', false)];
                }
                if ($newPath) {
                    $pattern = Yii::getAlias('@downloads') . '/order_labels/' . $order->id . ".*";
                    foreach (glob($pattern, GLOB_NOSORT) as $item) {
                        $files[] = [$item, mb_strstr($item, $order->id . '.', false)];
                    }
                }
            }
            $offset += $limit;
        }

        if ($orderExport->after_action == OrderExport::ACTION_SEND_CLARIFICATION) {
            DeliveryRequest::sendClarification($orderIds);
        }

        $path = Yii::getAlias('@export') . DIRECTORY_SEPARATOR . $orderExport->user_id;
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }
        $filename = 'orders-2wtrade-' . date('YmdHis') . '_' . str_pad(random_int(1, 99), 2, '0', STR_PAD_LEFT) . '.zip';
        $archive = new \ZipArchive();
        $archive->open($path . DIRECTORY_SEPARATOR . $filename, \ZipArchive::CREATE);
        if ($files) {
            foreach ($files as $file) {
                if (!empty($file[0])) {
                    $archive->addFile($file[0], $file[1] ?? $file[0]);
                }
            }
        }
        $archive->close();
//        TODO: не предусмотрена возможность успешного завершения выгрузки пустой выборки. Может быть 404 (архив пустым не генерится)!
//        необходимо либо доработать логику экспортера, либо в архив засовывать info.txt
        return [
            'path' => $path,
            'filename' => $filename,
            'lines' => sizeof($orderIds)
        ];
    }
}