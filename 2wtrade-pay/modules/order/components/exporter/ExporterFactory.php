<?php
namespace app\modules\order\components\exporter;

use yii\base\InvalidParamException;
use Yii;

/**
 * Class ExporterFactory
 * @package app\modules\order\components\exporter
 */
class ExporterFactory
{
    /**
     * @param string $type
     * @param array $config
     * @return Exporter
     */
    public static function build($type, $config = [])
    {
        $path = "\\app\\modules\\order\\components\\exporter\\Exporter" . ucfirst(strtolower($type));

        if (class_exists($path)) {
            return new $path($config);
        } else {
            throw new InvalidParamException(Yii::t('common', 'Неизвестный тип Exporter.'));
        }
    }

}
