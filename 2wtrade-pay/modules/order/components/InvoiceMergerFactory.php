<?php
namespace app\modules\order\components;

use app\models\Country;
use Yii;
use yii\base\InvalidParamException;

/**
 * Class InvoiceMergerFactory
 * @package app\modules\order\components
 */
abstract class InvoiceMergerFactory
{
    /**
     * @param $listFiles
     * @param string $template
     * @param string $format
     * @return InvoiceMerger
     */
    public static function getMerger($format, $listFiles, $nameFile, $params)
    {
        $class = "\\app\\modules\\order\\components\\Invoice" . ucfirst(strtolower($format)) . "Merger";

        if (class_exists($class)) {
            return new $class($listFiles, $nameFile, $params);
        } else {
            throw new InvalidParamException(Yii::t('common', 'Неизвестный тип InvoiceMerger.'));
        }
    }
}
