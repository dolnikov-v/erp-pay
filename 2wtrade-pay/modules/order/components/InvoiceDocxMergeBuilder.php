<?php
namespace app\modules\order\components;

use app\helpers\Utils;
use app\modules\order\components\exceptions\InvoiceBuilderException;
use app\modules\order\models\OrderLogisticListInvoice;
use app\modules\order\models\OrderLogisticList;
use PhpOffice\PhpWord\TemplateProcessor;
use PhpOffice\PhpWord\Shared\ZipArchive;
use Yii;

/**
 * Class InvoiceDocxMergerBuilder
 * @package app\modules\order\components
 */
class InvoiceDocxMergeBuilder extends InvoiceDocxBuilder
{
    protected $format = OrderLogisticListInvoice::FORMAT_DOCS;

    public function __construct(OrderLogisticList $list, $template, $country = null)
    {
        parent::__construct($list, $template, $country);
    }

    /**
     * @return array
     */
    public function generate()
    {
        set_time_limit(0);

        $result = [
            'status' => static::STATUS_FAIL,
            'message' => '',
        ];

        if ($archive = $this->archiveIsExists()) {
            $result['status'] = 'success';
            $result['message'] = $archive->archive;
        } else {
            $ids = explode(',', $this->list->ids);
            $invoices = [];

            try {
                foreach ($ids as $id) {
                    $invoices[] = $this->createInvoiceByOrderId($id);
                }

                $fileName = $this->package($invoices);

                $result['status'] = static::STATUS_SUCCESS;
                $result['message'] = $fileName;
            } catch (InvoiceBuilderException $e) {
                $result['message'] = $e->getMessage();
            }
        }

        return $result;
    }

    /**
     * @param integer $orderId
     * @return OrderLogisticListInvoice
     * @throws InvoiceBuilderException
     */
    public function createInvoiceByOrderId($orderId)
    {
        $invoice = $this->getInvoiceByOrderId($orderId);

        if (empty($invoice->invoice)) {
            $fileName = Utils::uid() . '.' . $invoice->getExtension();
            $filePath = OrderLogisticListInvoice::getDownloadPath($fileName, true) . DIRECTORY_SEPARATOR . $fileName;

            $invoice->invoice = $fileName;
            $invoice->invoice_local = $fileName;
            $invoice->user_id = isset(Yii::$app->user) ? Yii::$app->user->id : 1;

            if ($invoice->save()) {
                $invoice->invoice_local = $this->getLocalName($invoice);
                $invoice->save(false, ['invoice_local']);
            } else {
                $error = $invoice->getErrorsAsArray()[0];

                throw new InvoiceBuilderException(Yii::t('common', $error), 400);
            }

            $templateFile = $this->getTemplateFilePath();
            $document = new TemplateProcessor($templateFile);

            $this->setValues($document, $this->getValues($invoice));

            $document->saveAs($filePath);
        }

        return $invoice;
    }

    /**
     * @param TemplateProcessor $document
     * @param array $values
     */
    protected function setValues($document, $values)
    {
        foreach ($values as $key => $value) {
            $document->setValue($key, $value);
        }
    }

    /**
     * @param OrderLogisticListInvoice[] $invoices
     * @return string
     * @throws InvoiceBuilderException
     * @throws \PhpOffice\PhpWord\Exception\Exception
     */
    protected function package($invoices)
    {

        $zip = new ZipArchive();

        $fileName = Utils::uid() . '.zip';

        $filePath = OrderLogisticList::getDownloadPath(
                $fileName,
                OrderLogisticList::DOWNLOAD_TYPE_INVOICES,
                true
            ) . DIRECTORY_SEPARATOR . $fileName;

        $filesForMerge = [];

        foreach ($invoices as $k => $invoice) {
            $partPath = OrderLogisticListInvoice::getDownloadPath(
                    $invoice->invoice
                ) . DIRECTORY_SEPARATOR . $invoice->invoice;

            $filesForMerge[] = $partPath;
        }

        $merger = InvoiceMergerFactory::getMerger($this->format, $filesForMerge, $filePath, []);

        try {
            $is_merged = $merger->mergeFiles();
        } catch (\Exception $e) {
            throw new InvoiceBuilderException($e->getMessage());
        }

        if ($zip->open($filePath, ZipArchive::CREATE) !== true && !$is_merged['success']) {
            throw new InvoiceBuilderException(Yii::t(
                'common',
                'Невозможно открыть "{dir}."',
                [
                    'dir' => $filePath,
                ]
            ));
        }

        $zip->addFile(substr($filePath, 0, -4) . '.docx', $invoice->invoice_local);

        try {

            $zip->close();

            $archive = $this->createArchive($fileName);

            if (!$archive->save()) {
                throw new InvoiceBuilderException(Yii::t('common', 'Не удалось сохранить архив.'));
            }
        } catch (Exception $e) {
            throw new InvoiceBuilderException(Yii::t('common', 'Не удалось создать архив.'));
        }

        return $fileName;
    }
}
