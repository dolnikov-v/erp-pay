<?php
namespace app\modules\order\components;

use app\modules\order\models\OrderLogisticList;
use Yii;
use yii\base\InvalidParamException;

/**
 * Class ExcelBuilderFactory
 * @package app\modules\order\components
 */
abstract class ExcelBuilderFactory
{
    /**
     * @param OrderLogisticList $list
     * @param string $format
     * @param array $columns
     * @param integer $waitingForSend
     * @return InvoiceBuilder
     */
    public static function build($list, $format, $columns, $waitingForSend = 0)
    {
        $class = "\\app\\modules\\order\\components\\Excel" . ucfirst(strtolower($format)) . "Builder";

        if (class_exists($class)) {
            return new $class($list, $columns, $waitingForSend);
        } else {
            throw new InvalidParamException(Yii::t('common', 'Неизвестный тип ExcelBuilder.'));
        }
    }
}
