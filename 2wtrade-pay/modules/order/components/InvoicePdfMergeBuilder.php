<?php
namespace app\modules\order\components;

use app\helpers\Utils;
use app\modules\order\components\exceptions\InvoiceBuilderException;
use app\modules\order\models\OrderLogisticListInvoice;
use app\modules\order\models\OrderLogisticList;
use kartik\mpdf\Pdf;
use PhpOffice\PhpWord\Shared\ZipArchive;
use Yii;

/**
 * Class InvoicePdfMergerBuilder
 * @package app\modules\order\components
 */
class InvoicePdfMergeBuilder extends InvoicePdfBuilder
{
    protected $format = OrderLogisticListInvoice::FORMAT_PDF;

    /**
     * @var
     */
    protected static $logoBase64;

    /**
     * @return array
     */
    public function generate()
    {
        set_time_limit(0);

        $result = [
            'status' => static::STATUS_FAIL,
            'message' => '',
        ];

        if ($archive = $this->archiveIsExists()) {
            $result['status'] = 'success';
            $result['message'] = $archive->archive;
        } else {
            $ids = explode(',', $this->list->ids);
            $invoices = [];

            try {


                foreach ($ids as $id) {
                    $invoices[] = $this->createInvoiceByOrderId($id);
                }

                $fileName = $this->package($invoices);

                $result['status'] = static::STATUS_SUCCESS;
                $result['message'] = $fileName;

            } catch (InvoiceBuilderException $e) {
                $result['message'] = $e->getMessage();
            }
        }

        return $result;

    }

    /**
     * @param integer $orderId
     * @return OrderLogisticListInvoice
     * @throws InvoiceBuilderException
     */
    public function createInvoiceByOrderId($orderId)
    {
        $invoice = $this->getInvoiceByOrderId($orderId);


        if (empty($invoice->invoice)) {
            $fileName = Utils::uid() . '.' . $invoice->getExtension();
            $filePath = OrderLogisticListInvoice::getDownloadPath($fileName, true) . DIRECTORY_SEPARATOR . $fileName;

            $invoice->invoice = $fileName;
            $invoice->invoice_local = $fileName;
            $invoice->user_id = isset(Yii::$app->user) ? Yii::$app->user->id : 1;

            if ($invoice->save()) {
                $invoice->invoice_local = $this->getLocalName($invoice);
                $invoice->save(false, ['invoice_local']);
            } else {
                $error = $invoice->getErrorsAsArray()[0];

                throw new InvoiceBuilderException(Yii::t('common', $error), 400);
            }

            $templateFile = $this->getTemplateFilePath();

            $viewData['data'] = $this->getViewData($invoice);


            $content = Yii::$app->controller->renderFile($templateFile, $viewData);

            $document = new Pdf([
                'mode' => Pdf::MODE_UTF8,
                'options' => [
                    'autoScriptToLang' => true,
                    'autoLangToFont' => true,
                    'ignore_invalid_utf8' => true,
                    'tabSpaces' => 4
                ],

                'marginLeft' => 11,
                'marginRight' => 11,
                'marginTop' => 8,
                'marginBottom' => 8,
                'content' => $content,
                'filename' => $filePath,
                'destination' => Pdf::DEST_FILE,
                'cssFile' => Yii::getAlias('@templates') . DIRECTORY_SEPARATOR . self::$folderFiles . DIRECTORY_SEPARATOR . 'pdf-style.css',
            ]);

            $document->render();
        }

        return $invoice;
    }

    /**
     * @param OrderLogisticListInvoice $invoice
     * @return array
     */
    protected function getViewData($invoice)
    {
        $values = $this->getValues($invoice);

        if (is_null(self::$logoBase64)) {
            $logoPath = Yii::getAlias('@templates') . DIRECTORY_SEPARATOR . self::$folderFiles . DIRECTORY_SEPARATOR . 'pdf-logo.png';
            $logoContent = file_get_contents($logoPath);

            self::$logoBase64 = 'data:image/png;base64,' . base64_encode($logoContent);;
        }

        $values['logoBase64'] = self::$logoBase64;

        return $values;
    }

    /**
     * @param OrderLogisticListInvoice[] $invoices
     * @return string
     * @throws InvoiceBuilderException
     * @throws \PhpOffice\PhpWord\Exception\Exception
     */
    protected function package($invoices)
    {
        $zip = new ZipArchive();

        $fileName = Utils::uid() . '.zip';
        $filePath = OrderLogisticList::getDownloadPath(
                $fileName,
                OrderLogisticList::DOWNLOAD_TYPE_INVOICES,
                true
            ) . DIRECTORY_SEPARATOR . $fileName;

        if ($zip->open($filePath, ZipArchive::CREATE) !== true) {
            throw new InvoiceBuilderException(Yii::t(
                'common',
                'Невозможно открыть "{dir}."',
                [
                    'dir' => $filePath,
                ]
            ));
        }

        $filesForMerge = [];

        foreach ($invoices as $invoice) {
            $partPath = OrderLogisticListInvoice::getDownloadPath(
                    $invoice->invoice
                ) . DIRECTORY_SEPARATOR . $invoice->invoice;

            $filesForMerge[] = $partPath;
        }

        $merger = InvoiceMergerFactory::getMerger($this->format, $filesForMerge, $filePath, []);

        try {
            $is_merged = $merger->mergeFiles();
        } catch (\Exception $e) {
            throw new InvoiceBuilderException($e->getMessage());
        }

        if ($zip->open($filePath, ZipArchive::CREATE) !== true && !$is_merged['success']) {
            throw new InvoiceBuilderException(Yii::t(
                'common',
                'Невозможно открыть "{dir}."',
                [
                    'dir' => $filePath,
                ]
            ));
        }

        $zip->addFile(substr($filePath, 0, -4) . '.pdf', $invoice->invoice_local);

        try {

            $zip->close();

            $archive = $this->createArchive($fileName);

            if (!$archive->save()) {
                throw new InvoiceBuilderException(Yii::t('common', 'Не удалось сохранить архив.'));
            }
        } catch (Exception $e) {
            throw new InvoiceBuilderException(Yii::t('common', 'Не удалось создать архив.'));
        }

        return $fileName;
    }
}
