<?php
namespace app\modules\order\components;

use app\modules\order\models\OrderLogisticListInvoiceArchive;
use Yii;
use yii\base\InvalidParamException;

/**
 * Class InvoiceMerger
 * @package app\modules\order\components
 */
abstract class InvoiceMerger
{
    /**
     * @var []
     */
    protected $listFiles;

    /**
     * @var string
     */
    protected $nameFile;

    /**
     * @var []
     */
    protected $params;

    /**
     * @param [] $listFiles
     * @param string $nameFile
     * @param [] $params
     * @throws InvalidParamException
     */
    public function __construct($listFiles, $nameFile, $params = [])
    {
        if (empty($listFiles)) {
            throw new InvalidParamException(Yii::t(
                'common',
                'Отсутствуют данные для формирования накладных.'
            ));
        }

        $this->listFiles = $listFiles;
        $this->nameFile = $nameFile;
        $this->params = $params;
    }

    /**
     * @return OrderLogisticListInvoiceArchive
     */
    protected function archiveIsExists()
    {
        return OrderLogisticListInvoiceArchive::find()
            ->byListId($this->list->id)
            ->byTemplate($this->template)
            ->byFormat($this->format)
            ->one();
    }

    public abstract function mergeFiles();
}