<?php
namespace app\modules\order\components;

use app\helpers\Utils;
use app\modules\order\components\exceptions\InvoiceBuilderException;
use iio\libmergepdf\Merger;

use Yii;

/**
 * Class InvoicePdfMerger
 * @package app\modules\order\components
 */
class InvoicePdfMerger extends InvoiceMerger
{
    /**
     * InvoiceDocxMerger constructor.
     * @param  [] $listFiles
     * @param string $nameFile
     * @param [] $params
     * @return string
     */
    public function __construct($listFiles, $nameFile, array $params = [])
    {
        $this->listFiles = $listFiles;
        $this->nameFile = $nameFile;
        $this->outputFileName = substr($this->nameFile, 0, -4) . '.pdf';
        $this->params = $params;

        return $this->outputFileName;
    }

    /**
     * @return void
     * @throws InvoiceBuilderException
     */
    public function mergeFiles()
    {
        $PDFMerge =  new Merger();
        $is_merged = $PDFMerge->addIterator($this->listFiles);

        if(!file_put_contents($this->outputFileName, $PDFMerge->merge())){
            throw new InvoiceBuilderException(Yii::t('common', 'Ошибка объединения накладных.'));
        }
    }
}