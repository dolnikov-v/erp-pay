<?php
namespace app\modules\order\components\exceptions;

use yii\base\Exception;

/**
 * Class InvoiceBuilderException
 */
class InvoiceBuilderException extends Exception
{

}
