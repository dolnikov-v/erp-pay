<?php
namespace app\modules\order\components\exceptions;

use yii\base\Exception;

/**
 * Class ExcelBuilderException
 */
class ExcelBuilderException extends Exception
{

}
