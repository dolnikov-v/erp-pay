<?php
namespace app\modules\order\components;

use app\models\Country;
use app\modules\order\models\OrderLogisticList;
use app\modules\order\models\OrderLogisticListInvoice;
use Yii;
use yii\base\InvalidParamException;

/**
 * Class InvoiceBuilderFactory
 * @package app\modules\order\components
 */
abstract class InvoiceBuilderFactory
{
    /**
     * @param OrderLogisticList $list
     * @param string $template
     * @param string $format
     * @param Country $country
     * @return InvoiceBuilder
     */
    public static function build(OrderLogisticList $list, $template, $format, $country = null)
    {
        if (is_null($country) || !($country instanceof Country)) {
            $country = Yii::$app->user->country;
        }

        if ($template == OrderLogisticListInvoice::TEMPLATE_ALL) {
            $class = "\\app\\modules\\order\\components\\Invoice" . ucfirst(strtolower($format)) . "MergeBuilder";
        } else {
            $class = "\\app\\modules\\order\\components\\Invoice" . ucfirst(strtolower($format)) . "Builder";
        }

        if (class_exists($class)) {
            return new $class($list, $template, $country);
        } else {
            throw new InvalidParamException(Yii::t('common', 'Неизвестный тип InvoiceBuilder.'));
        }
    }
}
