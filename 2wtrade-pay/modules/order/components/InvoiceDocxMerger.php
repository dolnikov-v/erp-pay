<?php
namespace app\modules\order\components;

use app\helpers\Utils;
use app\modules\order\components\exceptions\InvoiceBuilderException;
use DocxMerge\DocxMerge;
use Yii;
use yii\helpers\BaseFileHelper;

/**
 * Class InvoiceDocxMerger
 * @package app\modules\order\components
 */
class InvoiceDocxMerger extends InvoiceMerger
{
    /**
     * InvoiceDocxMerger constructor.
     * @param  [] $listFiles
     * @param string $nameFile
     * @param [] $params
     * @return string
     */
    public function __construct($listFiles, $nameFile, array $params = [])
    {
        $this->listFiles = $listFiles;
        $this->nameFile = $nameFile;
        $this->outputFileName = substr($this->nameFile, 0, -4) . '.docx';
        $this->params = $params;

        return $this->outputFileName;
    }

    /**
     * @return array
     * @throws InvoiceBuilderException
     */
    public function mergeFiles()
    {
        $DocxMerge = new DocxMerge();
        $is_merged = $DocxMerge->merge($this->listFiles, $this->outputFileName);

        if ($is_merged == 0) {
            return ['success' => true, 'message' => $this->outputFileName];
        } elseif ($is_merged == -2) {
            throw new InvoiceBuilderException(Yii::t('common', 'Невозможно скопировать файл накладной.'));
        } elseif ($is_merged == -1) {
            throw new InvoiceBuilderException(Yii::t('common', 'В данном листе накладных не найдено.'));
        }
    }
}