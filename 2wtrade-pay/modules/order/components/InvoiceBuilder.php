<?php
namespace app\modules\order\components;

use app\helpers\Utils;
use app\models\Country;
use app\modules\delivery\models\Delivery;
use app\modules\order\components\exceptions\InvoiceBuilderException;
use app\modules\order\models\Order;
use app\modules\order\models\OrderLogisticList;
use app\modules\order\models\OrderLogisticListInvoice;
use app\modules\order\models\OrderLogisticListInvoiceArchive;
use PhpOffice\PhpWord\Exception\Exception;
use PhpOffice\PhpWord\Shared\ZipArchive;
use Yii;
use yii\base\InvalidParamException;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * Class InvoiceBuilder
 * @package app\modules\order\components
 */
abstract class InvoiceBuilder
{
    const STATUS_SUCCESS = 'success';
    const STATUS_FAIL = 'fail';

    /**
     * @var OrderLogisticList
     */
    protected $list;

    /**
     * @var string
     */
    protected $template;

    /**
     * @var string
     */
    protected $format;

    /**
     * @var string
     */
    protected $companyShortName;

    /**
     * @var string
     */
    protected $deliveryCharCode;

    /**
     * @var Order[]
     */
    protected $orders = [];

    /**
     * @var Country
     */
    protected $country = null;

    /**
     * @var string
     */
    protected static $folderFiles = 'invoice-files';

    public $onlyOneInvoice = false;

    public $orderInvoiceId = false;

    const INVOICE_TEMPLATE_DOCX_1 = 'invoice-docx-1.docx';
    const INVOICE_TEMPLATE_DOCX_2 = 'invoice-docx-2.docx';
    const INVOICE_TEMPLATE_DOCX_ALL = 'invoice-docx-all.docx';
    const INVOICE_TEMPLATE_PDF_1 = 'invoice-pdf-1.php';
    const INVOICE_TEMPLATE_PDF_2 = 'invoice-pdf-2.php';
    const INVOICE_TEMPLATE_PDF_ALL = 'invoice-pdf-all.php';


    /**
     * @param OrderLogisticList $list
     * @param string $template
     * @param Country $country
     * @throws InvalidParamException
     */
    public function __construct($list, $template, $country = null)
    {
        if (is_null($country) || !($country instanceof Country)) {
            $this->country = Yii::$app->user->country;
        } else {
            $this->country = $country;
        }
        if (!$list instanceof OrderLogisticList) {
            throw new InvalidParamException(Yii::t(
                'common',
                'Необходимо указать корректную модель для генерации накладной.'
            ));
        }

        $this->list = $list;
        $this->template = $template;

        $this->companyShortName = Yii::$app->params['companyShortName'];

        /** @var Delivery $delivery */
        $delivery = Delivery::find()
            ->byCountryId($list->country_id)
            ->active()
            ->orderBy(['id' => SORT_ASC])
            ->one();

        if (empty($delivery)) {
            throw new InvalidParamException(Yii::t('common', 'Для текущего листа отсутствует служба доставки.'));
        }

        $this->deliveryCharCode = $delivery->char_code;

        if (!$this->archiveIsExists()) {
            $ids = explode(',', $this->list->ids);

            $this->orders = Order::find()
                ->with(['orderProducts', 'orderProducts.product'])
                ->andWhere(['in', 'id', $ids])
                ->all();

            $this->orders = ArrayHelper::index($this->orders, 'id');
        }
    }

    /**
     * @param OrderLogisticListInvoice $invoice
     * @return string
     */
    public function getLocalName($invoice)
    {
        $parts = $this->getPartsLocalName($invoice);

        return implode('', $parts) . '.' . $invoice->getExtension();
    }

    /**
     * @return OrderLogisticListInvoiceArchive
     */
    protected function archiveIsExists()
    {
        return OrderLogisticListInvoiceArchive::find()
            ->byListId($this->list->id)
            ->byTemplate($this->template)
            ->byFormat($this->format)
            ->one();
    }

    /**
     * @param string $fileName
     * @return OrderLogisticListInvoiceArchive
     */
    protected function createArchive($fileName)
    {
        $number = str_pad($this->list->id, 4, '0', STR_PAD_LEFT);

        $archiveLocal = $this->list->country->char_code . '-' . $number . '-';

        switch ($this->template) {
            case OrderLogisticListInvoice::TEMPLATE_ONE;
                $endFile = 1;
                break;
            case OrderLogisticListInvoice::TEMPLATE_TWO;
                $endFile = 2;
                break;
            case OrderLogisticListInvoice::TEMPLATE_ALL;
                $endFile = 'all';
                break;
            default :
                $endFile = 1;
                break;
        }

        $archiveLocal .= $this->format . '-' . $endFile . '.zip';

        return new OrderLogisticListInvoiceArchive([
            'list_id' => $this->list->id,
            'template' => $this->template,
            'format' => $this->format,
            'archive' => $fileName,
            'archive_local' => $archiveLocal,
            'user_id' => isset(Yii::$app->user) ? Yii::$app->user->id : 1,
        ]);
    }

    /**
     * @param integer $orderId
     * @return OrderLogisticListInvoice
     */
    protected function getInvoiceByOrderId($orderId)
    {
        $where = [
            'list_id' => $this->list->id,
            'order_id' => $orderId,
            'template' => $this->template,
            'format' => $this->format,
        ];
        if (!$this->onlyOneInvoice) {
            $model = OrderLogisticListInvoice::find()
                ->where($where)
                ->one();
        }
        else{$model = OrderLogisticListInvoice::find()
            ->where(['order_id' => $orderId])
            ->one();
            $model->format = $this->format;
            $model->template = $this->template;
        };


        return $model ? $model : new OrderLogisticListInvoice($where);
    }

    /**
     * @param OrderLogisticListInvoice $invoice
     * @return Order
     * @throws InvoiceBuilderException
     */
    protected function getOrderInvoice($invoice)
    {
        $order = null;

        if (array_key_exists($invoice->order_id, $this->orders)) {
            $order = $this->orders[$invoice->order_id];
        } else {
            $order = Order::find()
                ->with(['orderProducts', 'orderProducts.product'])
                ->andWhere(['id' => $invoice->order_id])
                ->one();
        }

        if (!$order) {
            throw new InvoiceBuilderException(Yii::t('common', 'Заказ не найден.'), 400);
        }

        return $order;
    }

    /**
     * Генерим путь для инвойса, если есть шаблон в стране, тогда отдаем его иначе стандартный шаблон
     * @return string $filePath путь до шаблона инвойса
     */
    protected function getTemplateFilePath()
    {
        $filePath = '';

        if ($this->template == OrderLogisticListInvoice::TEMPLATE_ONE) {
            switch ($this->format) {
                case OrderLogisticListInvoice::FORMAT_DOCS:
                    $filePath = Yii::getAlias('@templates') . DIRECTORY_SEPARATOR . self::INVOICE_TEMPLATE_DOCX_1;

                    //добавим проверку шаблона для курьерки по чаркоду в стране ибо шаблон для каждой КС может быть уникальным
                    $courierFile = Yii::getAlias(
                            '@templates'
                        ) . DIRECTORY_SEPARATOR . $this->list->country->char_code . DIRECTORY_SEPARATOR . $this->list->delivery->char_code . DIRECTORY_SEPARATOR . self::INVOICE_TEMPLATE_DOCX_1;
                    if (file_exists($courierFile)) {
                        $filePath = $courierFile;
                    } else {
                        $countryFile = Yii::getAlias(
                                '@templates'
                            ) . DIRECTORY_SEPARATOR . $this->list->country->char_code . DIRECTORY_SEPARATOR . self::INVOICE_TEMPLATE_DOCX_1;
                        if (file_exists($countryFile)) {
                            $filePath = $countryFile;
                        }
                    }
                    break;
                case OrderLogisticListInvoice::FORMAT_PDF:
                    $filePath = Yii::getAlias('@templates') . DIRECTORY_SEPARATOR . self::INVOICE_TEMPLATE_PDF_1;

                    //добавим проверку шаблона для курьерки по чаркоду в стране ибо шаблон для каждой КС может быть уникальным
                    $courierFile = Yii::getAlias(
                            '@templates'
                        ) . DIRECTORY_SEPARATOR . $this->list->country->char_code . DIRECTORY_SEPARATOR . $this->list->delivery->char_code . DIRECTORY_SEPARATOR . self::INVOICE_TEMPLATE_PDF_1;
                    if (file_exists($courierFile)) {
                        $filePath = $courierFile;
                    } else {
                        $countryFile = Yii::getAlias(
                                '@templates'
                            ) . DIRECTORY_SEPARATOR . $this->list->country->char_code . DIRECTORY_SEPARATOR . self::INVOICE_TEMPLATE_PDF_1;
                        if (file_exists($countryFile)) {
                            $filePath = $countryFile;
                        }
                    }
                    break;
            }
        } elseif ($this->template == OrderLogisticListInvoice::TEMPLATE_ALL) {
            switch ($this->format) {
                case OrderLogisticListInvoice::FORMAT_DOCS:
                    $filePath = Yii::getAlias('@templates') . DIRECTORY_SEPARATOR . self::INVOICE_TEMPLATE_DOCX_ALL;

                    //добавим проверку шаблона для курьерки по чаркоду в стране ибо шаблон для каждой КС может быть уникальным
                    $courierFile = Yii::getAlias(
                            '@templates'
                        ) . DIRECTORY_SEPARATOR . $this->list->country->char_code . DIRECTORY_SEPARATOR . $this->list->delivery->char_code . DIRECTORY_SEPARATOR . self::INVOICE_TEMPLATE_DOCX_ALL;
                    if (file_exists($courierFile)) {
                        $filePath = $courierFile;
                    } else {
                        $countryFile = Yii::getAlias(
                                '@templates'
                            ) . DIRECTORY_SEPARATOR . $this->list->country->char_code . DIRECTORY_SEPARATOR . self::INVOICE_TEMPLATE_DOCX_ALL;
                        if (file_exists($countryFile)) {
                            $filePath = $countryFile;
                        }
                    }
                    break;
                case OrderLogisticListInvoice::FORMAT_PDF:
                    $filePath = Yii::getAlias('@templates') . DIRECTORY_SEPARATOR . self::INVOICE_TEMPLATE_PDF_ALL;

                    //добавим проверку шаблона для курьерки по чаркоду в стране ибо шаблон для каждой КС может быть уникальным
                    $courierFile = Yii::getAlias(
                            '@templates'
                        ) . DIRECTORY_SEPARATOR . $this->list->country->char_code . DIRECTORY_SEPARATOR . $this->list->delivery->char_code . DIRECTORY_SEPARATOR . self::INVOICE_TEMPLATE_PDF_ALL;
                    if (file_exists($courierFile)) {
                        $filePath = $courierFile;
                    } else {
                        $countryFile = Yii::getAlias(
                                '@templates'
                            ) . DIRECTORY_SEPARATOR . $this->list->country->char_code . DIRECTORY_SEPARATOR . self::INVOICE_TEMPLATE_PDF_ALL;
                        if (file_exists($countryFile)) {
                            $filePath = $countryFile;
                        }
                    }
                    break;
            }
        } else {
            switch ($this->format) {
                case OrderLogisticListInvoice::FORMAT_DOCS:
                    $filePath = Yii::getAlias('@templates') . DIRECTORY_SEPARATOR . self::INVOICE_TEMPLATE_DOCX_2;

                    //добавим проверку шаблона для курьерки по чаркоду в стране ибо шаблон для каждой КС может быть уникальным
                    $courierFile = Yii::getAlias(
                            '@templates'
                        ) . DIRECTORY_SEPARATOR . $this->list->country->char_code . DIRECTORY_SEPARATOR . $this->list->delivery->char_code . DIRECTORY_SEPARATOR . self::INVOICE_TEMPLATE_DOCX_2;
                    if (file_exists($courierFile)) {
                        $filePath = $courierFile;
                    } else {
                        $countryFile = Yii::getAlias(
                                '@templates'
                            ) . DIRECTORY_SEPARATOR . $this->list->country->char_code . DIRECTORY_SEPARATOR . self::INVOICE_TEMPLATE_DOCX_2;

                        if (file_exists($countryFile)) {
                            $filePath = $countryFile;
                        }
                    }
                    break;
                case OrderLogisticListInvoice::FORMAT_PDF:
                    $filePath = Yii::getAlias('@templates') . DIRECTORY_SEPARATOR . self::INVOICE_TEMPLATE_PDF_2;

                    //добавим проверку шаблона для курьерки по чаркоду в стране ибо шаблон для каждой КС может быть уникальным
                    $courierFile = Yii::getAlias(
                            '@templates'
                        ) . DIRECTORY_SEPARATOR . $this->list->country->char_code . DIRECTORY_SEPARATOR . $this->list->delivery->char_code . DIRECTORY_SEPARATOR . self::INVOICE_TEMPLATE_PDF_2;
                    if (file_exists($courierFile)) {
                        $filePath = $courierFile;
                    } else {
                        $countryFile = Yii::getAlias(
                                '@templates'
                            ) . DIRECTORY_SEPARATOR . $this->list->country->char_code . DIRECTORY_SEPARATOR . self::INVOICE_TEMPLATE_PDF_2;
                        if (file_exists($countryFile)) {
                            $filePath = $countryFile;
                        }
                    }
                    break;
            }
        }

        return $filePath;
    }

    /**
     * @param OrderLogisticListInvoice[] $invoices
     * @return string
     * @throws InvoiceBuilderException
     * @throws \PhpOffice\PhpWord\Exception\Exception
     */
    protected function package($invoices)
    {
        $zip = new ZipArchive();

        $fileName = Utils::uid() . '.zip';
        $filePath = OrderLogisticList::getDownloadPath(
                $fileName,
                OrderLogisticList::DOWNLOAD_TYPE_INVOICES,
                true
            ) . DIRECTORY_SEPARATOR . $fileName;

        if ($zip->open($filePath, ZipArchive::CREATE) !== true) {
            throw new InvoiceBuilderException(Yii::t(
                'common',
                'Невозможно открыть "{dir}."',
                [
                    'dir' => $filePath,
                ]
            ));
        }

        foreach ($invoices as $invoice) {
            $partPath = OrderLogisticListInvoice::getDownloadPath(
                    $invoice->invoice
                ) . DIRECTORY_SEPARATOR . $invoice->invoice;
            $zip->addFile($partPath, $invoice->invoice_local);
        }

        try {
            $zip->close();

            $archive = $this->createArchive($fileName);

            if (!$archive->save()) {
                throw new InvoiceBuilderException(Yii::t('common', 'Не удалось сохранить архив.'));
            }
        } catch (Exception $e) {
            throw new InvoiceBuilderException(Yii::t('common', 'Не удалось создать архив.'));
        }

        return $fileName;
    }

    /**
     * @param OrderLogisticListInvoice $invoice
     * @return array
     */
    public function getPartsLocalName($invoice)
    {
        $parts = [];

        $parts[] = $this->companyShortName;
        $parts[] = $this->deliveryCharCode;
        $parts[] = Yii::$app->formatter->asDate($this->list->created_at, 'php:ym');
        $parts[] = str_pad($invoice->id, 8, '0', STR_PAD_LEFT);

        return $parts;
    }

    /**
     * @param OrderLogisticListInvoice $invoice
     * @return array
     * @throws InvoiceBuilderException
     */
    protected function getValues($invoice)
    {
        if (!$this->country->currency) {
            throw new InvoiceBuilderException(Yii::t('common', 'Отсутствует валюта у страны.'), 400);
        }

        $values = [];

        $order = $this->getOrderInvoice($invoice);

        $parts = $this->getPartsLocalName($invoice);
        $values['InvPrint'] = implode('/', $parts);
        $amount = 0;
        $amountQuantity = 0;
        $index = 1;
        $amountTotal = 0;
        foreach ($order->orderProducts as $orderProduct) {
            $amount += $orderProduct->price;
            $amountQuantity += $orderProduct->quantity;
            $amountTotal += $orderProduct->price * $orderProduct->quantity;

            $tax = round(($orderProduct->price / 1.05) * 5) / 100;

            $values['P_name' . $index] = $orderProduct->product->name;
            $values['P_q' . $index] = $orderProduct->quantity;
            $values['P_pr' . $index] = $orderProduct->price - $tax;
            $values['T' . $index] = $tax;
            $values['Tt' . $index] = $orderProduct->price * $orderProduct->quantity;

            $index++;
        }

        for ($i = $index; $i <= 5; $i++) {
            $values['P_name' . $i] = ' ';
            $values['P_q' . $i] = ' ';
            $values['P_pr' . $i] = ' ';
            $values['T' . $i] = ' ';
            $values['Tt' . $i] = ' ';
        }
        //новый КЦ передаёт в правильно порядке и в этом поле - адрес клиента
        //@todo не нашел на КЦ где он передает это в массиве
        $customer_address_add = Json::decode($order->customer_address_add);

        if(!empty(count($customer_address_add))){
            $address = implode(", ", array_values($customer_address_add));

            //если вдруг пустые поля
            if(empty(strtr($address, [', ' => '']))){
                $address =  $order->customer_address;
            }

        }else{
            $address =  $order->customer_address;
        }

        $dateTime = new \DateTime('now', new \DateTimeZone($this->list->country->timezone->timezone_id));
        $values['CC'] = $this->country->currency->char_code;
        $values['Sum'] = $order->delivery + $order->price_total;
        $values['InvoiceDateOne'] = $dateTime->setTimestamp($this->list->created_at)->format('Y-m-d');
        $values['Pid'] = $order->id;
        $values['CallId'] = $order->callCenterRequest->id;
        $values['InvoiceDateTwo'] = $dateTime->setTimestamp($this->list->created_at)->format('Y-m-d');
        $values['Buyer'] = $order->customer_full_name;
        $values['Address'] = htmlspecialchars($order->customer_address); //htmlspecialchars($address);//
        $values['Phone'] = $order->customer_phone;
        $values['P_qS'] = $amountQuantity;
        $values['TS'] = $amountTotal;

        // Дата доставки с учетом сдвига по времени
        if (!is_numeric($order->delivery_time_from)) {
            $values['Delivery_time_from'] = '';
        } else {
            $values['Delivery_time_from'] = $dateTime->setTimestamp($order->delivery_time_from)->format('Y-m-d');
        }

        $values['CC_comment'] = ($order->getCallCenterRequest()->exists() ? $order->callCenterRequest->comment : $order->comment);

        $numberFormatter = new \NumberFormatter("en", \NumberFormatter::SPELLOUT);
        $values['Sum_word'] = ucwords($numberFormatter->format($values['Sum']));

        return $values;
    }

    /**
     * @return string
     */
    public abstract function generate();

    /**
     * @param integer $orderId
     * @return string
     */
    public abstract function createInvoiceByOrderId($orderId);

    /**
     * Print only one order invoice,
     * but this invoice must be in generated list
     * @param int $orderId
     * @return string
     */
    public abstract function printOrderInvoice($orderId);
}
