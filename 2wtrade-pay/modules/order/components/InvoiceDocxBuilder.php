<?php
namespace app\modules\order\components;

use app\helpers\Utils;
use app\modules\delivery\components\api\Transmitter;
use app\modules\delivery\components\api\TransmitterAdaptee;
use app\modules\order\components\exceptions\InvoiceBuilderException;
use app\modules\order\models\OrderLogisticListInvoice;
use PhpOffice\PhpWord\TemplateProcessor;
use Yii;

/**
 * Class InvoiceDocxBuilder
 * @package app\modules\order\components
 */
class InvoiceDocxBuilder extends InvoiceBuilder
{
    protected $format = OrderLogisticListInvoice::FORMAT_DOCS;

    /**
     * @return array
     */
    public function generate()
    {
        set_time_limit(0);

        $result = [
            'status' => static::STATUS_FAIL,
            'message' => '',
        ];

        if ($archive = $this->archiveIsExists()) {
            $result['status'] = 'success';
            $result['message'] = $archive->archive;
        } else {
            $ids = explode(',', $this->list->ids);
            $invoices = [];

            try {

                if ($this->list->delivery->hasApiGetSortedOrderIds()) {
                    $transmitter = new Transmitter($this->list->delivery);
                    if ($transmitter) {
                        $ids = $transmitter->getSortedOrderIds($ids);
                    }
                }

                foreach ($ids as $id) {
                    $invoices[] = $this->createInvoiceByOrderId($id);
                }

                $fileName = $this->package($invoices);

                $result['status'] = static::STATUS_SUCCESS;
                $result['message'] = $fileName;
            } catch (InvoiceBuilderException $e) {
                $result['message'] = $e->getMessage();
            }
        }

        return $result;
    }

    /**
     * @param integer $orderId
     * @return OrderLogisticListInvoice
     * @throws InvoiceBuilderException
     */
    public function createInvoiceByOrderId($orderId)
    {
        $invoice = $this->getInvoiceByOrderId($orderId);

        if (empty($invoice->invoice)) {
            $fileName = Utils::uid() . '.' . $invoice->getExtension();
            $filePath = OrderLogisticListInvoice::getDownloadPath($fileName, true) . DIRECTORY_SEPARATOR . $fileName;

            $invoice->invoice = $fileName;
            $invoice->invoice_local = $fileName;
            $invoice->user_id = isset(Yii::$app->user) ? Yii::$app->user->id : 1;

            if ($invoice->save()) {
                $invoice->invoice_local = $this->getLocalName($invoice);
                $invoice->save(false, ['invoice_local']);
            } else {
                $error = $invoice->getErrorsAsArray()[0];

                throw new InvoiceBuilderException(Yii::t('common', $error), 400);
            }

            $templateFile = $this->getTemplateFilePath();
            $document = new TemplateProcessor($templateFile);

            $this->setValues($document, $this->getValues($invoice));

            $document->saveAs($filePath);
        }

        return $invoice;
    }

    /**
     * @param TemplateProcessor $document
     * @param array $values
     */
    protected function setValues($document, $values)
    {
        foreach ($values as $key => $value) {
            $document->setValue($key, $value);
        }
    }

    /**
     * Print only one order invoice,
     * but this invoice must be in generated list
     * @param int $orderId
     * @return string
     */
    public function printOrderInvoice($orderId){
        $invoice = $this->getInvoiceByOrderId($orderId);
        $templateFile = $this->getTemplateFilePath();
        $document = new TemplateProcessor($templateFile);
        $this->setValues($document, $this->getValues($invoice));
        $tempFile = $document->save();
        return $docContents = file_get_contents($tempFile);
    }
}
