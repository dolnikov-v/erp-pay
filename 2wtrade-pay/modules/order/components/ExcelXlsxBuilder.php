<?php

namespace app\modules\order\components;

use app\helpers\Utils;
use app\modules\order\components\exceptions\ExcelBuilderException;
use app\modules\order\models\Order;
use app\modules\order\models\OrderLogisticList;
use app\modules\order\models\OrderLogisticListExcel;
use Yii;

/**
 * Class ExcelXlsxBuilder
 * @package app\modules\order\components
 */
class ExcelXlsxBuilder extends ExcelBuilder
{
    protected $format = OrderLogisticListExcel::FORMAT_XLSX;

    protected static $styleHeader = [
        'font' => [
            'bold' => true,
            'name' => 'Calibri',
            'size' => 11,
            'color' => [
                'rgb' => 'ffffff',
            ],
        ],
        'alignment' => [
            'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
        ],
        'fill' => [
            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
            'color' => [
                'rgb' => '4169e1',
            ],
        ],
        'borders' => [
            'bottom' => [
                'style' => \PHPExcel_Style_Border::BORDER_THIN,
            ],
            'left' => [
                'style' => \PHPExcel_Style_Border::BORDER_THIN,
            ],
            'right' => [
                'style' => \PHPExcel_Style_Border::BORDER_THIN,
            ],
        ],
    ];

    protected static $styleCell = [
        'font' => [
            'bold' => false,
            'name' => 'Calibri',
            'size' => 11,
            'color' => [
                'rgb' => '000000',
            ],
        ],
        'alignment' => [
            'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
        ],
        'borders' => [
            'bottom' => [
                'style' => \PHPExcel_Style_Border::BORDER_THIN,
            ],
            'left' => [
                'style' => \PHPExcel_Style_Border::BORDER_THIN,
            ],
            'right' => [
                'style' => \PHPExcel_Style_Border::BORDER_THIN,
            ],
        ],
    ];

    /**
     * @return array
     */
    public function generate()
    {
        set_time_limit(0);

        $result = [
            'status' => 'fail',
            'message' => '',
        ];

        if ($excel = $this->excelIsExists()) {
            $result['status'] = 'success';
            $result['message'] = $excel->system_file_name;
        } else {
            try {
                $orders = $this->getOrders();

                $excel = new \PHPExcel();
                $excel->getProperties()->setCreator(Yii::$app->params['companyName']);
                $sheet = $excel->setActiveSheetIndex(0);

                $index = 0;

                foreach (self::$columns as $column) {
                    if (!in_array($column, $this->selectedColumns)) {
                        continue;
                    }

                    $char = $this->getChar($index);
                    $coordinate = $char . '1';

                    $sheet->setCellValue($coordinate, self::getColumnLabel($column));
                    $sheet->getStyle($coordinate)->applyFromArray(self::$styleHeader);

                    $sheet->getColumnDimension($char)->setWidth($this->getColumnWidth($column));

                    $index++;
                }

                $number = 2;

                foreach ($orders as $order) {
                    $index = 0;

                    foreach (self::$columns as $column) {
                        if (!in_array($column, $this->selectedColumns)) {
                            continue;
                        }

                        $char = $this->getChar($index);
                        $coordinate = $char . $number;

                        if ($column == 'row_number') {
                            $value = $number - 1;
                        } else {
                            $value = $this->getValue($order, $column);
                        }
                        $sheet->setCellValue($coordinate, $value);
                        $sheet->getStyle($coordinate)->applyFromArray($this->getStyleCell($column));
                        $sheet->getStyle($coordinate)->getAlignment()->setWrapText(true);

                        $index++;
                    }

                    $number++;
                }

                $writer = \PHPExcel_IOFactory::createWriter($excel, 'Excel2007');

                $fileName = Utils::uid() . '.' . 'tmp';
                $filePath = OrderLogisticList::getDownloadPath($fileName, OrderLogisticList::DOWNLOAD_TYPE_EXCELS,
                        true) . DIRECTORY_SEPARATOR . $fileName;

                $writer->save($filePath);

                $date = Yii::$app->formatter->asDate($this->list->created_at, 'php:YmdHi');

                $excelLocal = $date . '.' . OrderLogisticListExcel::getExtension($this->format);

                $excel = new OrderLogisticListExcel([
                    'list_id' => $this->list->id,
                    'system_file_name' => $fileName,
                    'orders_hash' => $this->list->getOrdersHash(),
                    'delivery_file_name' => $excelLocal,
                    'format' => $this->format,
                    'columns_hash' => $this->selectedColumnsHash,
                    'user_id' => isset(Yii::$app->user) ? Yii::$app->user->id : 1,
                    'waiting_for_send' => $this->waitingForSend
                ]);

                if ($excel->save()) {
                    $result['status'] = 'success';
                    $result['message'] = $fileName;
                    $result['delivery_file_name'] = $excelLocal;
                } else {
                    $error = $excel->getErrorsAsArray()[0];

                    throw new ExcelBuilderException(Yii::t('common', $error), 400);
                }
            } catch (ExcelBuilderException $e) {
                $result['message'] = $e->getMessage();
            }
        }

        return $result;
    }

    /**
     * @param string $column
     * @return integer
     */
    protected function getColumnWidth($column)
    {
        $width = 25;

        switch ($column) {
            case 'approve_operator':
            case 'row_number':
            case 'id':
                $width = 10;
                break;
            case 'customer_full_name':
                $width = 20;
                break;
            case 'customer_phone':
            case 'customer_mobile':
                $width = 17;
                break;
            case 'customer_address_additional':
            case 'customer_address_add_embedded':
            case 'customer_address':
                $width = 30;
                break;
            case 'customer_address_add':
                $width = 60;
                break;
            case 'customer_zip':
                $width = 30;
                break;
            case 'region':
                $width = 30;
                break;
            case 'district':
                $width = 30;
                break;
            case 'comment':
                $width = 20;
                break;
            case 'products_count':
                $width = 30;
                break;
            case 'products_name':
                $width = 30;
                break;
            case 'products':
                $width = 40;
                break;
            case 'price':
                $width = 15;
                break;
            case 'delivery':
                $width = 14;
                break;
            case 'sum':
                $width = 15;
                break;
            case 'approve_date':
            case 'date_send':
            case 'date_delivery':
                $width = 20;
                break;
        }

        return $width;
    }

    /**
     * @param string $column
     * @return array
     */
    protected function getStyleCell($column)
    {
        $style = self::$styleCell;

        switch ($column) {
            case 'price':
                $style['font']['bold'] = true;
                break;
            case 'customer_address_add' :
                $style['alignment'] = [
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                    'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                ];
                break;
        }

        return $style;
    }

    /**
     * @param Order $order
     * @param string $column
     * @return integer
     */
    protected function getValue($order, $column)
    {
        $value = '';
        $customerAddressAdd = json_decode($order->customer_address_add, true);
        $dateTime = new \DateTime('now', new \DateTimeZone($this->list->country->timezone->timezone_id));
        switch ($column) {
            case 'id':
            case 'customer_full_name':
            case 'customer_phone':
            case 'customer_mobile':
            case 'customer_address':
            case 'customer_province':
            case 'customer_district':
            case 'customer_subdistrict':
            case 'customer_city':
            case 'customer_street':
            case 'customer_house_number':
            case 'customer_zip':
            case 'comment':
                $value = $order->$column;
                break;
            case 'customer_address_add':

                $data = json_decode($order->$column, 1);

                $details = [];

                if (is_array($data)) {
                    foreach ($data as $name => $value) {
                        $details[] = $name . ' : ' . $value;
                    }
                }

                $value = !empty($details) ? implode(PHP_EOL, $details) : '';

                break;
            case 'id_cc':
                $value = (isset($order->callCenterRequest)) ? $order->callCenterRequest->foreign_id : '';
                break;
            case 'comment_call_center':
                $value = (isset($order->callCenterRequest)) ? $order->callCenterRequest->comment : '';
                break;
            case 'region':
                $value = $customerAddressAdd['customer_state'] ?? $customerAddressAdd['customer_region'] ?? '';
                break;
            case 'products_count':
                $lines = [];

                foreach ($order->orderProducts as $orderProduct) {
                    if (empty($lines[$orderProduct->product_id])) {
                        $lines[$orderProduct->product_id] = $orderProduct->quantity;
                    } else {
                        $lines[$orderProduct->product_id] += $orderProduct->quantity;
                    }
                }

                $value = implode(PHP_EOL, $lines);
                break;
            case 'products_name':
                $lines = [];

                foreach ($order->orderProducts as $orderProduct) {
                    $lines[] = $orderProduct->product->name;
                }

                $value = implode(PHP_EOL, $lines);
                break;
            case 'products':
                $linesParts = $linesPartsGift = null;
                foreach ($order->orderProducts as $orderProduct) {
                    if (empty($orderProduct->price)) {
                        if (isset($linesParts[$orderProduct->product_id])) {
                            $linesPartsGift[$orderProduct->product_id]['quantity'] += $orderProduct->quantity;
                        } else {
                            $linesPartsGift[$orderProduct->product_id] = [
                                'name' => $orderProduct->product->name,
                                'quantity' => $orderProduct->quantity,
                            ];
                        }
                    } else {
                        if (isset($linesParts[$orderProduct->product_id])) {
                            $linesParts[$orderProduct->product_id]['quantity'] += $orderProduct->quantity;
                        } else {
                            $linesParts[$orderProduct->product_id] = [
                                'name' => $orderProduct->product->name,
                                'quantity' => $orderProduct->quantity,
                            ];
                        }
                    }
                }

                $value = '';
                if (is_array($linesParts)) {
                    foreach ($linesParts as $linesPart) {
                        $value .= $linesPart['name'] . '-' . $linesPart['quantity'] . ', ';
                    }
                }
                if (is_array($linesPartsGift)) {
                    foreach ($linesPartsGift as $linesPart) {
                        $value .= $linesPart['name'] . '-' . $linesPart['quantity'] . ' GIFT, ';
                    }
                }

                $value = rtrim($value, ', ');
                break;
            case 'price':
                $lines = [];

                foreach ($order->orderProducts as $orderProduct) {
                    $lines[] = $orderProduct->price;
                }

                $value = implode(PHP_EOL, $lines);
                break;
            case 'delivery':
                $value = $order->delivery;
                break;
            case 'sum':
                $value = 0;

                foreach ($order->orderProducts as $orderProduct) {
                    $value += ($orderProduct->price * $orderProduct->quantity);
                }

                $value += $order->delivery;

                break;
            case 'date_delivery':
                $value = $dateTime->setTimestamp($order->delivery_time_from)->format('Y-m-d');
                break;
            case 'date_send':
                $value = $dateTime->format('Y-m-d');
                break;
            case 'express':
                $value = $order->getOrderExpressDelivery() ? 'yes' : '-';
                break;
            case 'approve_date':
                $value = $dateTime->setTimestamp($order->callCenterRequest->approved_at)->format('Y-m-d');
                break;
            case 'approve_operator':
                $value = $order->callCenterRequest->last_foreign_operator;
                break;
            case 'customer_address_additional':
                $value = $order->customer_address_additional;
                if (empty($value)) {
                    $value = $customerAddressAdd['customer_address_additional'] ?? '';
                }
                break;
            case 'customer_address_add_embedded':
                $value = $customerAddressAdd['customer_address_add'] ?? $customerAddressAdd['customer_apartment_number'] ?? '';
                break;
        }

        return $value;
    }
}
