<?php
use app\components\grid\GridView;
use app\widgets\Panel;
use app\widgets\assets\custom\ModalConfirmDeleteAsset;
use app\widgets\custom\ModalConfirmDelete;
use app\modules\catalog\assets\ATimerAsset;
use app\modules\report\assets\SmsPollHistoryAsset;

ATimerAsset::register($this);
ModalConfirmDeleteAsset::register($this);
SmsPollHistoryAsset::register($this);


/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormSmsPollHistory $reportForm */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */
?>

<?= Panel::widget([
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'username',
                'label' => Yii::t('common', 'Куратор страны'),
                'enableSorting' => false,
            ]
        ],
    ])
]) ?>

<?= ModalConfirmDelete::widget() ?>
