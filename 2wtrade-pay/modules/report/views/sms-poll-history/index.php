<?php
use app\modules\report\assets\ReportAsset;
use app\modules\report\widgets\ReportFilterSmsPollHistory;
use app\widgets\Panel;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormSmsPollHistory $reportForm */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */
/** @var \app\modules\report\extensions\DataProvider $dataCuratorProvider */

$this->title = Yii::t('common', 'Смс опросы');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Отчёты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ReportAsset::register($this);
?>

<?= ReportFilterSmsPollHistory::widget([
    'reportForm' => $reportForm,
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Ответственный'),
    'border' => false,
    'withBody' => false,
    'content' => $this->render('_index-curators', [
        'dataProvider' => $dataCuratorProvider,
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Смс опросы'),
    'border' => false,
    'withBody' => false,
    'content' => $this->render('_index-content', [
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
    ]),
]) ?>
