<?php
use app\widgets\Panel;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormTopProduct $reportFormTopProduct */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */

$this->title = Yii::t('common', $title);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Отчеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $title];

?>


<?=Panel::widget([
    'title' => Yii::t('common', 'Отчет в разработке, задача {number}', ['number' => '<a href="https://2wtrade-tasks.atlassian.net/browse/'.$taskNumber .'" style="color:#62a8ea">'.$taskNumber.'</a>']),
    'border' => false,
    'withBody' => false,
]);?>
