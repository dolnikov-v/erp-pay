<?php
use app\components\grid\GridView;
use app\modules\report\models\ReportAdcombo;
use yii\bootstrap\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'rowOptions' => function ($data) {
        $options = [
            'class' => 'tr-vertical-align-middle'
        ];
        $flag = false;
        foreach ($data->differenceArray as $diff) {
            if (intval($diff) > 0) {
                $flag = true;
                break;
            }
        }
        if ($flag) {
            $options['class'] .= ' has-error';
        }
        return $options;
    },
    'columns' => [
        [
            'header' => Yii::t('common', 'Период'),
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'content' => function ($model) {
                return Yii::$app->formatter->asDatetime($model->from) . " - " . Yii::$app->formatter->asDatetime($model->to);
            }
        ],
        [
            'format' => 'raw',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'attribute' => 'adcombo_total_count',
            'value' => function ($data) {
                if (isset($data->differenceArray[ReportAdcombo::SHOW_ERROR_TYPE_TOTAL_ADCOMBO]) && $data->differenceArray[ReportAdcombo::SHOW_ERROR_TYPE_TOTAL_ADCOMBO] > 0) {
                    $link = Html::a($data->differenceArray[ReportAdcombo::SHOW_ERROR_TYPE_TOTAL_ADCOMBO], Url::toRoute([
                        'show-errors',
                        'id' => $data->id,
                        'type' => ReportAdcombo::SHOW_ERROR_TYPE_TOTAL_ADCOMBO
                    ]));
                    $content = "{$data->adcombo_total_count} (" . $link . ")";
                    return Html::tag('div', $content, ['class' => 'has-error']);
                } else {
                    return $data->adcombo_total_count;
                }
            }
        ],
        [
            'format' => 'raw',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'attribute' => 'confirmed_count',
            'value' => function ($data) {
                if (isset($data->differenceArray[ReportAdcombo::SHOW_ERROR_TYPE_CONFIRMED_ADCOMBO]) && $data->differenceArray[ReportAdcombo::SHOW_ERROR_TYPE_CONFIRMED_ADCOMBO] > 0) {
                    $link = Html::a($data->differenceArray[ReportAdcombo::SHOW_ERROR_TYPE_CONFIRMED_ADCOMBO],
                        Url::toRoute([
                            'show-errors',
                            'id' => $data->id,
                            'type' => ReportAdcombo::SHOW_ERROR_TYPE_CONFIRMED_ADCOMBO
                        ]));
                    $content = "{$data->confirmed_count} (" . $link . ")";
                    return Html::tag('div', $content, ['class' => 'has-error']);
                } else {
                    return $data->confirmed_count;
                }
            }
        ],
        [
            'format' => 'raw',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'attribute' => 'hold_count',
            'value' => function ($data) {
                if (isset($data->differenceArray[ReportAdcombo::SHOW_ERROR_TYPE_HOLD_ADCOMBO]) && $data->differenceArray[ReportAdcombo::SHOW_ERROR_TYPE_HOLD_ADCOMBO] > 0) {
                    $link = Html::a($data->differenceArray[ReportAdcombo::SHOW_ERROR_TYPE_HOLD_ADCOMBO], Url::toRoute([
                        'show-errors',
                        'id' => $data->id,
                        'type' => ReportAdcombo::SHOW_ERROR_TYPE_HOLD_ADCOMBO
                    ]));
                    $content = "{$data->hold_count} (" . $link . ")";
                    return Html::tag('div', $content, ['class' => 'has-error']);
                } else {
                    return $data->hold_count;
                }
            }
        ],
        [
            'format' => 'raw',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'attribute' => 'trash_count',
            'value' => function ($data) {
                if (isset($data->differenceArray[ReportAdcombo::SHOW_ERROR_TYPE_TRASH_ADCOMBO]) && $data->differenceArray[ReportAdcombo::SHOW_ERROR_TYPE_TRASH_ADCOMBO] > 0) {
                    $link = Html::a($data->differenceArray[ReportAdcombo::SHOW_ERROR_TYPE_TRASH_ADCOMBO], Url::toRoute([
                        'show-errors',
                        'id' => $data->id,
                        'type' => ReportAdcombo::SHOW_ERROR_TYPE_TRASH_ADCOMBO
                    ]));
                    $content = "{$data->trash_count} (" . $link . ")";
                    return Html::tag('div', $content, ['class' => 'has-error']);
                } else {
                    return $data->trash_count;
                }
            }
        ],
        [
            'format' => 'raw',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'attribute' => 'cancelled_count',
            'value' => function ($data) {
                if (isset($data->differenceArray[ReportAdcombo::SHOW_ERROR_TYPE_CANCELLED_ADCOMBO]) && $data->differenceArray[ReportAdcombo::SHOW_ERROR_TYPE_CANCELLED_ADCOMBO] > 0) {
                    $link = Html::a($data->differenceArray[ReportAdcombo::SHOW_ERROR_TYPE_CANCELLED_ADCOMBO],
                        Url::toRoute([
                            'show-errors',
                            'id' => $data->id,
                            'type' => ReportAdcombo::SHOW_ERROR_TYPE_CANCELLED_ADCOMBO
                        ]));
                    $content = "{$data->cancelled_count} (" . $link . ")";
                    return Html::tag('div', $content, ['class' => 'has-error']);
                } else {
                    return $data->cancelled_count;
                }
            }
        ],
        [
            'format' => 'raw',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'attribute' => 'our_total_count',
            'value' => function ($data) {
                if (isset($data->differenceArray[ReportAdcombo::SHOW_ERROR_TYPE_TOTAL_OUR]) && $data->differenceArray[ReportAdcombo::SHOW_ERROR_TYPE_TOTAL_OUR] > 0) {
                    $link = Html::a($data->differenceArray[ReportAdcombo::SHOW_ERROR_TYPE_TOTAL_OUR], Url::toRoute([
                        'show-errors',
                        'id' => $data->id,
                        'type' => ReportAdcombo::SHOW_ERROR_TYPE_TOTAL_OUR
                    ]));
                    $content = "{$data->our_total_count} (" . $link . ")";
                    return Html::tag('div', $content, ['class' => 'has-error']);
                } else {
                    return $data->our_total_count;
                }
            }
        ],
        [
            'format' => 'raw',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'attribute' => 'cc_count',
            'value' => function ($data) {
                if ($data->our_total_count - $data->cc_count > 0) {
                    $link = Html::a($data->our_total_count - $data->cc_count, Url::toRoute([
                        'show-errors',
                        'id' => $data->id,
                        'type' => ReportAdcombo::SHOW_ERROR_TYPE_CC
                    ]));
                    $content = "{$data->cc_count} (" . $link . ")";
                    return Html::tag('div', $content, ['class' => 'has-error']);
                } else {
                    return $data->cc_count;
                }
            }
        ],
        [
            'format' => 'raw',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'attribute' => 'our_confirmed_count',
            'value' => function ($data) {
                if (isset($data->differenceArray[ReportAdcombo::SHOW_ERROR_TYPE_CONFIRMED_OUR]) && $data->differenceArray[ReportAdcombo::SHOW_ERROR_TYPE_CONFIRMED_OUR] > 0) {
                    $link = Html::a($data->differenceArray[ReportAdcombo::SHOW_ERROR_TYPE_CONFIRMED_OUR], Url::toRoute([
                        'show-errors',
                        'id' => $data->id,
                        'type' => ReportAdcombo::SHOW_ERROR_TYPE_CONFIRMED_OUR
                    ]));
                    $content = "{$data->our_confirmed_count} (" . $link . ")";
                    return Html::tag('div', $content, ['class' => 'has-error']);
                } else {
                    return $data->our_confirmed_count;
                }
            }
        ],
        [
            'format' => 'raw',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'attribute' => 'our_hold_count',
            'value' => function ($data) {
                if (isset($data->differenceArray[ReportAdcombo::SHOW_ERROR_TYPE_HOLD_OUR]) && $data->differenceArray[ReportAdcombo::SHOW_ERROR_TYPE_HOLD_OUR] > 0) {
                    $link = Html::a($data->differenceArray[ReportAdcombo::SHOW_ERROR_TYPE_HOLD_OUR], Url::toRoute([
                        'show-errors',
                        'id' => $data->id,
                        'type' => ReportAdcombo::SHOW_ERROR_TYPE_HOLD_OUR
                    ]));
                    $content = "{$data->our_hold_count} (" . $link . ")";
                    return Html::tag('div', $content, ['class' => 'has-error']);
                } else {
                    return $data->our_hold_count;
                }
            }
        ],
        [
            'format' => 'raw',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'attribute' => 'our_trash_count',
            'value' => function ($data) {
                if (isset($data->differenceArray[ReportAdcombo::SHOW_ERROR_TYPE_TRASH_OUR]) && $data->differenceArray[ReportAdcombo::SHOW_ERROR_TYPE_TRASH_OUR] > 0) {
                    $link = Html::a($data->differenceArray[ReportAdcombo::SHOW_ERROR_TYPE_TRASH_OUR], Url::toRoute([
                        'show-errors',
                        'id' => $data->id,
                        'type' => ReportAdcombo::SHOW_ERROR_TYPE_TRASH_OUR
                    ]));
                    $content = "{$data->our_trash_count} (" . $link . ")";
                    return Html::tag('div', $content, ['class' => 'has-error']);
                } else {
                    return $data->our_trash_count;
                }
            }
        ],
        [
            'format' => 'raw',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'attribute' => 'our_cancelled_count',
            'value' => function ($data) {
                if (isset($data->differenceArray[ReportAdcombo::SHOW_ERROR_TYPE_CANCELLED_OUR]) && $data->differenceArray[ReportAdcombo::SHOW_ERROR_TYPE_CANCELLED_OUR] > 0) {
                    $link = Html::a($data->differenceArray[ReportAdcombo::SHOW_ERROR_TYPE_CANCELLED_OUR], Url::toRoute([
                        'show-errors',
                        'id' => $data->id,
                        'type' => ReportAdcombo::SHOW_ERROR_TYPE_CANCELLED_OUR
                    ]));
                    $content = "{$data->our_cancelled_count} (" . $link . ")";
                    return Html::tag('div', $content, ['class' => 'has-error']);
                } else {
                    return $data->our_cancelled_count;
                }
            }
        ],
        [
            'class' => \app\components\grid\DateColumn::className(),
            'attribute' => 'updated_at'
        ],
        'actions' => [
            'class' => \app\components\grid\ActionColumn::className(),
            'items' => [
                [
                    'label' => Yii::t('common', 'Перепроверить'),
                    'url' => function ($data) {
                        return Url::toRoute(\yii\helpers\ArrayHelper::merge(['recheck-record', 'id' => $data->id], Yii::$app->request->queryParams));
                    },
                    'can' => function () {
                        return Yii::$app->user->can('report.reportadcombo.recheckrecord');
                    },
                ]
            ]
        ]
    ]
]) ?>
