<?php

use app\widgets\Panel;
use app\components\grid\GridView;
use app\helpers\DataProvider;
use app\widgets\LinkPager;
use app\modules\report\widgets\ReportFilterAdcombo;
use app\modules\report\components\ReportFormAdcombo;
use app\modules\report\assets\ReportAdcomboAsset;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $reportForm ReportFormAdcombo */

ReportAdcomboAsset::register($this);

$this->title = Yii::t('common', 'Сверка с AdCombo');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Отчеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= ReportFilterAdcombo::widget([
    'reportForm' => $reportForm
]) ?>

<div class="report-adcombo-daily-index">
    <?= Panel::widget([
        'title' => Yii::t('common', 'Таблица с информацией'),
        'actions' => DataProvider::renderSummary($dataProvider),
        'withBody' => false,
        'content' => $this->render('_index-content', ['dataProvider' => $dataProvider]),
        'footer' => LinkPager::widget([
            'pagination' => $dataProvider->getPagination(),
            'counterPosition' => LinkPager::COUNTER_POSITION_RIGHT,
        ])
    ]) ?>
</div>
