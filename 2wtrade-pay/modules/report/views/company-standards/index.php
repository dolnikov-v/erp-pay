<?php
use app\modules\report\assets\ReportAsset;
use app\modules\report\widgets\ReportFilterCompanyStandards;
use app\widgets\Panel;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormCallCenter $reportForm */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */
/** @var array $total */
/** @var array $totalByCountry */
/** @var array $deliveryDataByMonth */

$this->title = Yii::t('common', 'Выполнение стандартов');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Отчеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ReportAsset::register($this);
?>

<?= ReportFilterCompanyStandards::widget([
    'reportForm' => $reportForm,
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Статистика'),
    'border' => false,
    'withBody' => false,
    'content' => $this->render('_index-content', [
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
        'total' => $total,
        'totalByCountry' => $totalByCountry,
        'deliveryDataByMonth' => $deliveryDataByMonth,
    ]),
]) ?>
