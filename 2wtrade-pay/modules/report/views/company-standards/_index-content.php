<?php

use \app\modules\report\models\CompanyStandards;
use \app\modules\report\models\CompanyStandardsDelivery;
use \kartik\grid\GridView;
use \yii\helpers\Html;
use \app\modules\report\assets\ReportCompanyStandardsAsset;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormCallCenter $reportForm */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */
/** @var array $total */
/** @var array $totalByCountry */
/** @var array $deliveryDataByMonth */

ReportCompanyStandardsAsset::register($this);

?>

<?= GridView::widget([
    'id' => 'table-company-standard',
    'dataProvider' => $dataProvider,
    'layout' => '{items}',
    'tableOptions' => ['class' => 'table table-striped'],
    'rowOptions' => ['class' => 'tr-vertical-align-middle'],
    'filterModel' => null,
    'showPageSummary' => true,
    'floatHeader' => true,
    'floatOverflowContainer' => false,
    'floatHeaderOptions' => ['scrollingTop' => '66', 'autoReflow' => true, 'top' => new yii\web\JsExpression('function(table) {return 66;}')],
    'headerRowOptions' => ['class' => 'h6'],
    'striped' => false,
    'pjax' => true,
    'hover' => true,
    'toggleDataContainer' => ['class' => 'btn-group mr-2'],
    'columns' => [
        [
            'class' => 'kartik\grid\SerialColumn',
            'hidden' => true
        ],
        [
            'attribute' => 'country.name',
            'label' => Yii::t('common', 'Страна'),
            'hAlign' => 'center',
            'vAlign' => 'middle',
            'group' => true,
            'groupedRow' => true,
            'groupFooter' => function ($model, $key, $index, $widget) use ($totalByCountry) {
                return [
                    'mergeColumns' => [[0, 1]],
                    'content' => [
                        2 => Yii::t('common', 'Итого по {country}', ['country' => $model->country->name]),
                        3 => Yii::$app->formatter->asDecimal($totalByCountry[$model->country_id]['admission_lead']) ?? '—',
                        4 => Yii::$app->formatter->asDecimal($totalByCountry[$model->country_id]['first_sms']) ?? '—',
                        5 => Yii::$app->formatter->asDecimal($totalByCountry[$model->country_id]['send_to_delivery']) ?? '—',
                        6 => Yii::$app->formatter->asDecimal($totalByCountry[$model->country_id]['first_call']) ?? '—',
                        7 => Yii::$app->formatter->asDecimal($totalByCountry[$model->country_id]['try_call_count']) ?? '—',
                        8 => Yii::$app->formatter->asDecimal($totalByCountry[$model->country_id]['try_call_days']) ?? '—',
                        9 => Yii::$app->formatter->asDecimal($totalByCountry[$model->country_id]['approve_percent']) ?? '—',
                        10 => Yii::$app->formatter->asDecimal($totalByCountry[$model->country_id]['avg_approve_check']) ?? '—',
                        11 => Yii::$app->formatter->asDecimal($totalByCountry[$model->country_id]['error_in_address']) ?? '—',
                        13 => Yii::$app->formatter->asDecimal($totalByCountry[$model->country_id]['accepted_time']) ?? '—',
                        14 => Yii::$app->formatter->asDecimal($totalByCountry[$model->country_id]['payment_time']) ?? '—',
                        15 => Yii::$app->formatter->asDecimal($totalByCountry[$model->country_id]['income_percent']) ?? '—',
                        16 => Yii::$app->formatter->asDecimal($totalByCountry[$model->country_id]['lead_cost_percent']) ?? '—',
                        17 => Yii::$app->formatter->asDecimal($totalByCountry[$model->country_id]['call_center_costs_percent']) ?? '—',
                        18 => Yii::$app->formatter->asDecimal($totalByCountry[$model->country_id]['product_costs_percent']) ?? '—',
                        19 => Yii::$app->formatter->asDecimal($totalByCountry[$model->country_id]['other_costs_percent']) ?? '—',
                    ],
                    'contentFormats' => [
                        3 => ['format' => ['decimal', 2]],
                        4 => ['format' => ['decimal', 2]],
                        5 => ['format' => ['decimal', 2]],
                        6 => ['format' => ['decimal', 2]],
                        7 => ['format' => ['decimal', 2]],
                        8 => ['format' => ['decimal', 2]],
                        9 => ['format' => ['decimal', 2]],
                        10 => ['format' => ['decimal', 2]],
                        11 => ['format' => ['decimal', 2]],
                        13 => ['format' => ['decimal', 2]],
                        14 => ['format' => ['decimal', 2]],
                        15 => ['format' => ['decimal', 2]],
                        16 => ['format' => ['decimal', 2]],
                        16 => ['format' => ['decimal', 2]],
                        17 => ['format' => ['decimal', 2]],
                        19 => ['format' => ['decimal', 2]],
                    ],
                    'contentOptions' => [
                        15 => ['title' => !empty($totalByCountry[$model->country_id]['income']) ? Yii::$app->formatter->asDecimal($totalByCountry[$model->country_id]['income']) . ' USD' : '',],
                        16 => ['title' => !empty($totalByCountry[$model->country_id]['lead_cost']) ? Yii::$app->formatter->asDecimal($totalByCountry[$model->country_id]['lead_cost']) . ' USD' : '',],
                        17 => ['title' => !empty($totalByCountry[$model->country_id]['call_center_costs']) ? Yii::$app->formatter->asDecimal($totalByCountry[$model->country_id]['call_center_costs']) . ' USD' : '',],
                        18 => ['title' => !empty($totalByCountry[$model->country_id]['product_costs']) ? Yii::$app->formatter->asDecimal($totalByCountry[$model->country_id]['product_costs']) . ' USD' : '',],
                        19 => ['title' => !empty($totalByCountry[$model->country_id]['other_costs']) ? Yii::$app->formatter->asDecimal($totalByCountry[$model->country_id]['other_costs']) . ' USD' : '',],
                    ],
                    'options' => ['class' => 'success table-success text-right'],
                ];
            },
        ],
        [
            'attribute' => 'month',
            'enableSorting' => false,
            'label' => CompanyStandards::getLabel('month'),
            'pageSummary' => Yii::t('common', 'Итого'),
        ],
        [
            'attribute' => 'admission_lead',
            'enableSorting' => false,
            'label' => CompanyStandards::getLabel('admission_lead'),
            'pageSummary' => Yii::$app->formatter->asDecimal($total['admission_lead']) ?? '—',
            'hAlign' => 'right',
            'format' => ['decimal', 2],
            'contentOptions' => function ($model) {
                return ['class' => $model->admission_lead > CompanyStandards::NORM_ADMISSION_LEAD ? 'custom-warning' : (!empty($model->admission_lead) && $model->admission_lead > 0 ? 'pale_green' : '')];
            },
        ],
        [
            'attribute' => 'first_sms',
            'enableSorting' => false,
            'label' => CompanyStandards::getLabel('first_sms'),
            'pageSummary' => Yii::$app->formatter->asDecimal($total['first_sms']) ?? '—',
            'hAlign' => 'right',
            'format' => ['decimal', 2],
            'contentOptions' => function ($model) {
                return ['class' => $model->first_sms > CompanyStandards::NORM_FIRST_SMS ? 'custom-warning' : (!empty($model->first_sms) && $model->first_sms > 0 ? 'pale_green' : '')];
            },
        ],
        [
            'attribute' => 'send_to_delivery',
            'enableSorting' => false,
            'label' => CompanyStandards::getLabel('send_to_delivery'),
            'value' => function ($array) {
                return !empty($array['send_to_delivery']) ? round($array['send_to_delivery'] / 3600, CompanyStandards::PRECISION) : null;
            },
            'pageSummary' => Yii::$app->formatter->asDecimal($total['send_to_delivery']) ?? '—',
            'hAlign' => 'right',
            'format' => ['decimal', 2],
            'contentOptions' => function ($model) {
                return ['class' => $model->send_to_delivery > CompanyStandards::NORM_SEND_TO_DELIVERY * 3600 ? 'custom-warning' : (!empty($model->send_to_delivery) && $model->send_to_delivery > 0 ? 'pale_green' : '')];
            },
        ],
        [
            'attribute' => 'first_call',
            'enableSorting' => false,
            'label' => CompanyStandards::getLabel('first_call'),
            'value' => function ($array) {
                return !empty($array['first_call']) ? round($array['first_call'] / 60, CompanyStandards::PRECISION) : null;
            },
            'pageSummary' => Yii::$app->formatter->asDecimal($total['first_call']) ?? '—',
            'hAlign' => 'right',
            'format' => ['decimal', 2],
            'contentOptions' => function ($model) {
                return ['class' => $model->first_call > CompanyStandards::NORM_FIRST_CALL * 60 ? 'custom-warning' : (!empty($model->first_call) && $model->first_call > 0 ? 'pale_green' : '')];
            },
        ],
        [
            'attribute' => 'try_call_count',
            'enableSorting' => false,
            'label' => CompanyStandards::getLabel('try_call_count'),
            'pageSummary' => Yii::$app->formatter->asDecimal($total['try_call_count']) ?? '—',
            'hAlign' => 'right',
            'format' => ['decimal', 2],
            'contentOptions' => function ($model) {
                return ['class' => $model->try_call_count > CompanyStandards::NORM_TRY_CALL_COUNT ? 'custom-warning' : (!empty($model->try_call_count) && $model->try_call_count > 0 ? 'pale_green' : '')];
            },
        ],
        [
            'attribute' => 'try_call_days',
            'enableSorting' => false,
            'label' => CompanyStandards::getLabel('try_call_days'),
            'pageSummary' => Yii::$app->formatter->asDecimal($total['try_call_days']) ?? '—',
            'hAlign' => 'right',
            'format' => ['decimal', 2],
            'contentOptions' => function ($model) {
                return ['class' => $model->try_call_days > CompanyStandards::NORM_TRY_CALL_DAYS ? 'custom-warning' : (!empty($model->try_call_days) && $model->try_call_days > 0 ? 'pale_green' : '')];
            },
        ],
        [
            'attribute' => 'approve_percent',
            'enableSorting' => false,
            'label' => CompanyStandards::getLabel('approve_percent'),
            'pageSummary' => Yii::$app->formatter->asDecimal($total['approve_percent']) ?? '—',
            'hAlign' => 'right',
            'format' => ['decimal', 2],
            'contentOptions' => function ($model) {
                return ['class' => $model->approve_percent > CompanyStandards::NORM_APPROVE_PERCENT ? 'custom-warning' : (!empty($model->approve_percent) && $model->approve_percent > 0 ? 'pale_green' : '')];
            },
        ],
        [
            'attribute' => 'avg_approve_check',
            'enableSorting' => false,
            'label' => CompanyStandards::getLabel('avg_approve_check'),
            'pageSummary' => Yii::$app->formatter->asDecimal($total['avg_approve_check']) ?? '—',
            'hAlign' => 'right',
            'format' => ['decimal', 2],
            'contentOptions' => function ($model) {
                return ['class' => $model->avg_approve_check < CompanyStandards::NORM_AVG_APPROVE_CHECK ? 'custom-warning ' : (!empty($model->avg_approve_check) && $model->avg_approve_check > 0 ? 'pale_green' : '')];
            },
        ],
        [
            'attribute' => 'error_in_address',
            'enableSorting' => false,
            'label' => CompanyStandards::getLabel('error_in_address'),
            'pageSummary' => Yii::$app->formatter->asDecimal($total['error_in_address']) ?? '—',
            'hAlign' => 'right',
        ],
        [
            'attribute' => 'companyStandardsDelivery.delivery.name',
            'enableSorting' => false,
            'label' => Yii::t('common', 'Курьерские службы'),
            'format' => 'raw',
            'value' => function ($model) {
                $data = [];
                $danger = $warning = false;
                foreach ($model->companyStandardsDelivery as $item) {
                    $data[] = $item->delivery->name;
                    if ($item->accepted_time < 0 || $item->payment_time < 0) {
                        $danger = true;
                        break;
                    } elseif (!$warning && ($item->accepted_time / 3600 > CompanyStandardsDelivery::NORM_ACCEPTED_TIME || $item->payment_time / 216000 > CompanyStandardsDelivery::NORM_PAYMENT_TIME)) {
                        $warning = true;
                    }
                }

                $options = [
                    'class' => ['view-delivery-modal', $danger ? 'parent-danger' : ($warning ? 'parent-warning' : null)],
                    'data-content' => GridView::widget([
                        'dataProvider' => new \yii\data\ArrayDataProvider(['allModels' => $model->companyStandardsDelivery, 'pagination' => false]),
                        'layout' => '{items}',
                        'tableOptions' => ['class' => 'table table-striped'],
                        'rowOptions' => ['class' => 'tr-vertical-align-middle'],
                        'filterModel' => null,
                        'columns' => [
                            [
                                'class' => 'kartik\grid\SerialColumn',
                            ],
                            'delivery.name',
                            [
                                'attribute' => 'accepted_time',
                                'format' => ['decimal', 2],
                                'value' => function ($model) {
                                    return $model->accepted_time ? round($model->accepted_time / 3600, CompanyStandardsDelivery::PRECISION) : null;
                                },
                                'contentOptions' => function ($model) {
                                    return ['class' => $model->accepted_time > CompanyStandardsDelivery::NORM_ACCEPTED_TIME * 3600 ? 'custom-warning' : (!empty($model->accepted_time) && $model->accepted_time > 0 ? 'pale_green' : '')];
                                },
                            ],
                            [
                                'attribute' => 'payment_time',
                                'format' => ['decimal', 2],
                                'value' => function ($model) {
                                    return $model->payment_time ? round($model->payment_time / 216000, CompanyStandardsDelivery::PRECISION) : null;
                                },
                                'contentOptions' => function ($model) {
                                    return ['class' => $model->payment_time > CompanyStandardsDelivery::NORM_PAYMENT_TIME * 216000 ? 'custom-warning' : (!empty($model->payment_time) && $model->payment_time > 0 ? 'pale_green' : '')];
                                },
                            ],
                        ]
                    ])
                ];

                return Html::tag('span', implode(', ', $data), $options);
            },
        ],
        [
            'attribute' => 'companyStandardsDelivery.accepted_time',
            'label' => CompanyStandardsDelivery::getLabel('accepted_time'),
            'hAlign' => 'right',
            'format' => ['decimal', 2],
            'pageSummary' => Yii::$app->formatter->asDecimal($total['accepted_time']) ?? '—',
            'value' => function ($model) use ($deliveryDataByMonth) {
                $data = $deliveryDataByMonth[$model->country_id][$model->month]['accepted_time'] ?? null;
                return $data ? $data : null;
            },
            'contentOptions' => function ($model) use ($deliveryDataByMonth) {
                $data = $deliveryDataByMonth[$model->country_id][$model->month]['accepted_time'] ?? null;
                return ['class' => $data > CompanyStandardsDelivery::NORM_ACCEPTED_TIME ? 'custom-warning' : (!empty($data) && $data > 0 ? 'pale_green' : '')];
            },
        ],
        [
            'attribute' => 'companyStandardsDelivery.payment_time',
            'label' => CompanyStandardsDelivery::getLabel('payment_time'),
            'hAlign' => 'right',
            'format' => ['decimal', 2],
            'pageSummary' => Yii::$app->formatter->asDecimal($total['payment_time']) ?? '—',
            'value' => function ($model) use ($deliveryDataByMonth) {
                $data = $deliveryDataByMonth[$model->country_id][$model->month]['payment_time'] ?? null;
                return $data ? $data : null;
            },
            'contentOptions' => function ($model) use ($deliveryDataByMonth) {
                $data = $deliveryDataByMonth[$model->country_id][$model->month]['payment_time'] ?? null;
                return ['class' => $data > CompanyStandardsDelivery::NORM_PAYMENT_TIME ? 'custom-warning' : (!empty($data) && $data > 0 ? 'pale_green' : '')];
            },
        ],
        [
            'attribute' => 'income',
            'enableSorting' => false,
            'label' => CompanyStandards::getLabel('income'),
            'pageSummary' => Yii::$app->formatter->asDecimal($total['income_percent']) ?? '—',
            'pageSummaryOptions' => ['title' => !empty($total['income']) ? Yii::$app->formatter->asDecimal($total['income']) . ' USD' : '',],
            'hAlign' => 'right',
            'format' => ['decimal', 2],
            'value' => function ($model) {
                return empty($model->income) || empty($model->costs) ? null : round(100 * $model->income / $model->costs, CompanyStandards::PRECISION);
            },
            'contentOptions' => function ($model) {
                return [
                    'class' => empty($model->costs) || !isset($model->income) || $model->income <= 0 ? '' : ((100 * $model->income / $model->costs) < CompanyStandards::NORM_INCOME ? 'custom-warning' : 'pale_green'),
                    'title' => !empty($model->income) ? Yii::$app->formatter->asDecimal($model->income) . ' USD' : '',
                ];
            },
        ],
        [
            'attribute' => 'lead_cost',
            'enableSorting' => false,
            'label' => CompanyStandards::getLabel('lead_cost'),
            'pageSummary' => Yii::$app->formatter->asDecimal($total['lead_cost_percent']) ?? '—',
            'pageSummaryOptions' => ['title' => !empty($total['lead_cost']) ? Yii::$app->formatter->asDecimal($total['lead_cost']) . ' USD' : '',],
            'hAlign' => 'right',
            'format' => ['decimal', 2],
            'value' => function ($model) {
                return empty($model->lead_cost) || empty($model->costs) ? null : round(100 * $model->lead_cost / $model->costs, CompanyStandards::PRECISION);
            },
            'contentOptions' => function ($model) {
                return [
                    'class' => empty($model->costs) || !isset($model->lead_cost) || $model->lead_cost <= 0 ? '' : ((100 * $model->lead_cost / $model->costs) > CompanyStandards::NORM_LEAD_COST ? 'custom-warning' : 'pale_green'),
                    'title' => !empty($model->lead_cost) ? Yii::$app->formatter->asDecimal($model->lead_cost) . ' USD' : '',
                ];
            },
        ],
        [
            'attribute' => 'call_center_costs',
            'enableSorting' => false,
            'label' => CompanyStandards::getLabel('call_center_costs'),
            'pageSummary' => Yii::$app->formatter->asDecimal($total['call_center_costs_percent']) ?? '—',
            'pageSummaryOptions' => ['title' => !empty($total['call_center_costs']) ? Yii::$app->formatter->asDecimal($total['call_center_costs']) . ' USD' : '',],
            'hAlign' => 'right',
            'format' => ['decimal', 2],
            'value' => function ($model) {
                return empty($model->call_center_costs) || empty($model->costs) ? null : round(100 * $model->call_center_costs / $model->costs, CompanyStandards::PRECISION);
            },
            'contentOptions' => function ($model) {
                return [
                    'class' => empty($model->costs) || !isset($model->call_center_costs) || $model->call_center_costs <= 0 ? '' : ((100 * $model->call_center_costs / $model->costs) > CompanyStandards::NORM_CALL_CENTER_COSTS ? 'custom-warning' : 'pale_green'),
                    'title' => !empty($model->call_center_costs) ? Yii::$app->formatter->asDecimal($model->call_center_costs) . ' USD' : '',
                ];
            },
        ],
        [
            'attribute' => 'product_costs',
            'enableSorting' => false,
            'label' => CompanyStandards::getLabel('product_costs'),
            'pageSummary' => Yii::$app->formatter->asDecimal($total['product_costs_percent']) ?? '—',
            'pageSummaryOptions' => ['title' => !empty($total['product_costs']) ? Yii::$app->formatter->asDecimal($total['product_costs']) . ' USD' : '',],
            'hAlign' => 'right',
            'format' => ['decimal', 2],
            'value' => function ($model) {
                return empty($model->product_costs) || empty($model->costs) ? null : round(100 * $model->product_costs / $model->costs, CompanyStandards::PRECISION);
            },
            'contentOptions' => function ($model) {
                return [
                    'class' => empty($model->costs) || empty($model->product_costs) || $model->product_costs <= 0 ? '' : ((100 * $model->product_costs / $model->costs) > CompanyStandards::NORM_PRODUCT_COSTS ? 'custom-warning' : 'pale_green'),
                    'title' => !empty($model->product_costs) ? Yii::$app->formatter->asDecimal($model->product_costs) . ' USD' : '',
                ];
            },
        ],
        [
            'attribute' => 'other_costs',
            'label' => CompanyStandards::getLabel('other_costs'),
            'pageSummary' => Yii::$app->formatter->asDecimal($total['other_costs_percent']) ?? '—',
            'pageSummaryOptions' => ['title' => !empty($total['other_costs']) ? Yii::$app->formatter->asDecimal($total['other_costs']) . ' USD' : '',],
            'hAlign' => 'right',
            'format' => ['decimal', 2],
            'value' => function ($model) {
                return empty($model->other_costs) || empty($model->costs) ? null : round(100 * $model->other_costs / $model->costs, CompanyStandards::PRECISION);
            },
            'contentOptions' => function ($model) {
                return [
                    'class' => empty($model->costs) || empty($model->other_costs) || $model->other_costs <= 0 ? '' : ((100 * $model->other_costs / $model->costs) > CompanyStandards::NORM_OTHER_COSTS ? 'custom-warning' : 'pale_green'),
                    'title' => !empty($model->other_costs) ? Yii::$app->formatter->asDecimal($model->other_costs) . ' USD' : '',
                ];
            },
        ],
    ]
])
?>

<?= \app\widgets\Modal::widget([
    'id' => 'delivery_modal',
    'title' => Yii::t('common', 'Данные по КС'),
    'body' => $this->render('_modal-delivery'),
]) ?>
