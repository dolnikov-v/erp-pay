<?php
use app\modules\report\extensions\GridViewTopProduct;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormProduct $reportForm */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */
?>

<div class="table-responsive">
    <?=GridViewTopProduct::widget([
    'id' => 'report_grid_top_product',
    'reportForm' => $reportFormTopProduct,
    'dataProvider' => $dataProvider,
]);?>
</div>

<?php

$this->registerJs('
    var gridview_id = "#report_grid_top_product";
    var columns = [1, 2];

    var column_data = [];
        column_start = [];
        rowspan = [];

    for (var i = 0; i < columns.length; i++) {
        column = columns[i];
        column_data[column] = "";
        column_start[column] = null;
        rowspan[column] = 1;
    }

    var row = 1;
    $(gridview_id+" table > tbody  > tr").each(function() {
        var col = 1;
        $(this).find("td").each(function(){
            for (var i = 0; i < columns.length; i++) {
                if(col==columns[i]){
                    if(column_data[columns[i]] == $(this).html()){
                        $(this).remove();
                        rowspan[columns[i]]++;
                        $(column_start[columns[i]]).attr("rowspan",rowspan[columns[i]]);
                    }
                    else{
                        column_data[columns[i]] = $(this).html();
                        rowspan[columns[i]] = 1;
                        column_start[columns[i]] = $(this);
                    }
                }
            }
            col++;
        })
        row++;
    });
');
?>
