<?php
use app\modules\report\assets\ReportAsset;
use app\modules\report\widgets\ReportFilterTopProduct;
use app\widgets\Panel;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormTopProduct $reportFormTopProduct */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */

$this->title = Yii::t('common', 'Топ товаров');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Отчеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ReportAsset::register($this);

?>

<?=ReportFilterTopProduct::widget([
    'reportForm' => $reportFormTopProduct,
]);?>

<?=Panel::widget([
    'title' => Yii::t('common', 'Топ товаров'),
    'border' => false,
    'withBody' => false,
    'content' => $this->render('_index-content', [
        'reportFormTopProduct' => $reportFormTopProduct,
        'dataProvider' => $dataProvider,
    ]),
]);?>