<?php
use app\helpers\DataProvider;
use app\modules\report\assets\ReportAsset;
use app\modules\report\widgets\ReportFilterDebtsInvoice;
use app\widgets\LinkPager;
use app\widgets\Panel;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormDebtsInvoice $reportForm */
/** @var \yii\data\ArrayDataProvider $dataProvider */

$this->title = Yii::t('common', 'Дебиторская задолженность по инвойсам');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Отчеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ReportAsset::register($this);
?>

<?= ReportFilterDebtsInvoice::widget(['reportForm' => $reportForm]); ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Дебиторская задолженность по инвойсам'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'alert' => Yii::t('common', 'Конвертация валют в USD осуществлялась по курсу на {day}', ['day' => Yii::$app->formatter->asDate(time())]),
    'alertStyle' => Panel::ALERT_PRIMARY,
    'border' => false,
    'withBody' => false,
    'content' => $this->render('_index-content', [
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
    ]),
]) ?>
