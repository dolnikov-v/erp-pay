<?php
use app\modules\order\models\OrderStatus;
use app\modules\order\models\search\filters\DateFilter;
use app\modules\report\components\invoice\ReportFormInvoiceList;
use app\modules\report\components\ReportFormDeliveryDebts;
use app\modules\report\extensions\DataColumnPercent;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var ReportFormDeliveryDebts $reportForm */
/** @var \yii\data\ArrayDataProvider $dataProvider */
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'export' => [
        'label' => Yii::t('common', 'Экспорт'),
        'showConfirmAlert' => false,
        'fontAwesome' => true,
        'target' => '_self',
        'header' => false,
    ],
    'exportConfig' => [
        GridView::EXCEL => [],
        GridView::PDF => [],
        GridView::CSV => [],
        GridView::JSON => [],
    ],
    'toolbar' => '{export}',
    'layout' => '<div id="report_export_box">{toolbar}</div>{items}',
    'tableOptions' => ['class' => 'table table-striped'],
    'rowOptions' => function ($model) {
        return ['class' => 'tr-vertical-align-middle '];
    },
    'columns' => [
        ['class' => 'kartik\grid\SerialColumn', 'width' => 0],
        [
            'attribute' => 'country_name',
            'label' => Yii::t('common', 'Страна'),
            'group' => true,  // enable grouping
            'groupFooter' => function ($model, $key, $index, $widget) {
                return [
                    'mergeColumns' => [[1, 4]],
                    'content' => [
                        1 => Yii::t('common', 'Итого') . ' ' . $model['country_name'],
                        5 => GridView::F_SUM,
                        6 => GridView::F_SUM,
                        9 => GridView::F_SUM,
                        10 => GridView::F_SUM,
                        11 => GridView::F_SUM,
                        12 => GridView::F_SUM,
                        13 => GridView::F_SUM,
                        14 => GridView::F_SUM,
                        15 => GridView::F_SUM,
                        16 => GridView::F_SUM,
                        17 => GridView::F_SUM,
                        18 => GridView::F_SUM,
                        19 => GridView::F_SUM,
                        20 => GridView::F_SUM,
                    ],
                    'contentFormats' => [
                        5 => ['format' => 'number', 'decimals' => 0],
                        6 => ['format' => 'number', 'decimals' => 0],
                        9 => ['format' => 'number', 'decimals' => 2, 'decPoint' => Yii::$app->get('formatter')->decimalSeparator, 'thousandSep' => Yii::$app->get('formatter')->thousandSeparator],
                        10 => ['format' => 'number', 'decimals' => 2, 'decPoint' => Yii::$app->get('formatter')->decimalSeparator, 'thousandSep' => Yii::$app->get('formatter')->thousandSeparator],
                        11 => ['format' => 'number', 'decimals' => 2, 'decPoint' => Yii::$app->get('formatter')->decimalSeparator, 'thousandSep' => Yii::$app->get('formatter')->thousandSeparator],
                        12 => ['format' => 'number', 'decimals' => 2, 'decPoint' => Yii::$app->get('formatter')->decimalSeparator, 'thousandSep' => Yii::$app->get('formatter')->thousandSeparator],
                        13 => ['format' => 'number', 'decimals' => 2, 'decPoint' => Yii::$app->get('formatter')->decimalSeparator, 'thousandSep' => Yii::$app->get('formatter')->thousandSeparator],
                        14 => ['format' => 'number', 'decimals' => 2, 'decPoint' => Yii::$app->get('formatter')->decimalSeparator, 'thousandSep' => Yii::$app->get('formatter')->thousandSeparator],
                        15 => ['format' => 'number', 'decimals' => 2, 'decPoint' => Yii::$app->get('formatter')->decimalSeparator, 'thousandSep' => Yii::$app->get('formatter')->thousandSeparator],
                        16 => ['format' => 'number', 'decimals' => 2, 'decPoint' => Yii::$app->get('formatter')->decimalSeparator, 'thousandSep' => Yii::$app->get('formatter')->thousandSeparator],
                        17 => ['format' => 'number', 'decimals' => 2, 'decPoint' => Yii::$app->get('formatter')->decimalSeparator, 'thousandSep' => Yii::$app->get('formatter')->thousandSeparator],
                        18 => ['format' => 'number', 'decimals' => 2, 'decPoint' => Yii::$app->get('formatter')->decimalSeparator, 'thousandSep' => Yii::$app->get('formatter')->thousandSeparator],
                        19 => ['format' => 'number', 'decimals' => 2, 'decPoint' => Yii::$app->get('formatter')->decimalSeparator, 'thousandSep' => Yii::$app->get('formatter')->thousandSeparator],
                        20 => ['format' => 'number', 'decimals' => 2, 'decPoint' => Yii::$app->get('formatter')->decimalSeparator, 'thousandSep' => Yii::$app->get('formatter')->thousandSeparator],
                    ],
                    'contentOptions' => [
                        5 => ['style' => 'text-align:right'],
                        6 => ['style' => 'text-align:right'],
                        9 => ['style' => 'text-align:right'],
                        10 => ['style' => 'text-align:right'],
                        11 => ['style' => 'text-align:right'],
                        12 => ['style' => 'text-align:right'],
                        13 => ['style' => 'text-align:right'],
                        14 => ['style' => 'text-align:right'],
                        15 => ['style' => 'text-align:right'],
                        16 => ['style' => 'text-align:right'],
                        17 => ['style' => 'text-align:right'],
                        18 => ['style' => 'text-align:right'],
                        19 => ['style' => 'text-align:right'],
                        20 => ['style' => 'text-align:right'],
                    ],
                    'options' => ['class' => 'warning', 'style' => 'font-weight:bold;']
                ];
            }
        ],
        [
            'attribute' => 'delivery_name',
            'label' => Yii::t('common', 'Курьерская Служба'),
            'group' => true,  // enable grouping
            'subGroupOf'=>1,
            'groupFooter' => function ($model, $key, $index, $widget) {
                return [
                    'mergeColumns' => [[2, 4]],
                    'content' => [
                        2 => Yii::t('common', 'Итого') . ' ' . $model['delivery_name'],
                        5 => GridView::F_SUM,
                        6 => GridView::F_SUM,
                        9 => GridView::F_SUM,
                        10 => GridView::F_SUM,
                        11 => GridView::F_SUM,
                        12 => GridView::F_SUM,
                        13 => GridView::F_SUM,
                        14 => GridView::F_SUM,
                        15 => GridView::F_SUM,
                        16 => GridView::F_SUM,
                        17 => GridView::F_SUM,
                        18 => GridView::F_SUM,
                        19 => GridView::F_SUM,
                        20 => GridView::F_SUM,
                    ],
                    'contentFormats' => [
                        5 => ['format' => 'number', 'decimals' => 0],
                        6 => ['format' => 'number', 'decimals' => 0],
                        9 => ['format' => 'number', 'decimals' => 2, 'decPoint' => Yii::$app->get('formatter')->decimalSeparator, 'thousandSep' => Yii::$app->get('formatter')->thousandSeparator],
                        10 => ['format' => 'number', 'decimals' => 2, 'decPoint' => Yii::$app->get('formatter')->decimalSeparator, 'thousandSep' => Yii::$app->get('formatter')->thousandSeparator],
                        11 => ['format' => 'number', 'decimals' => 2, 'decPoint' => Yii::$app->get('formatter')->decimalSeparator, 'thousandSep' => Yii::$app->get('formatter')->thousandSeparator],
                        12 => ['format' => 'number', 'decimals' => 2, 'decPoint' => Yii::$app->get('formatter')->decimalSeparator, 'thousandSep' => Yii::$app->get('formatter')->thousandSeparator],
                        13 => ['format' => 'number', 'decimals' => 2, 'decPoint' => Yii::$app->get('formatter')->decimalSeparator, 'thousandSep' => Yii::$app->get('formatter')->thousandSeparator],
                        14 => ['format' => 'number', 'decimals' => 2, 'decPoint' => Yii::$app->get('formatter')->decimalSeparator, 'thousandSep' => Yii::$app->get('formatter')->thousandSeparator],
                        15 => ['format' => 'number', 'decimals' => 2, 'decPoint' => Yii::$app->get('formatter')->decimalSeparator, 'thousandSep' => Yii::$app->get('formatter')->thousandSeparator],
                        16 => ['format' => 'number', 'decimals' => 2, 'decPoint' => Yii::$app->get('formatter')->decimalSeparator, 'thousandSep' => Yii::$app->get('formatter')->thousandSeparator],
                        17 => ['format' => 'number', 'decimals' => 2, 'decPoint' => Yii::$app->get('formatter')->decimalSeparator, 'thousandSep' => Yii::$app->get('formatter')->thousandSeparator],
                        18 => ['format' => 'number', 'decimals' => 2, 'decPoint' => Yii::$app->get('formatter')->decimalSeparator, 'thousandSep' => Yii::$app->get('formatter')->thousandSeparator],
                        19 => ['format' => 'number', 'decimals' => 2, 'decPoint' => Yii::$app->get('formatter')->decimalSeparator, 'thousandSep' => Yii::$app->get('formatter')->thousandSeparator],
                        20 => ['format' => 'number', 'decimals' => 2, 'decPoint' => Yii::$app->get('formatter')->decimalSeparator, 'thousandSep' => Yii::$app->get('formatter')->thousandSeparator],
                    ],
                    'contentOptions' => [
                        5 => ['style' => 'text-align:right'],
                        6 => ['style' => 'text-align:right'],
                        9 => ['style' => 'text-align:right'],
                        10 => ['style' => 'text-align:right'],
                        11 => ['style' => 'text-align:right'],
                        12 => ['style' => 'text-align:right'],
                        13 => ['style' => 'text-align:right'],
                        14 => ['style' => 'text-align:right'],
                        15 => ['style' => 'text-align:right'],
                        16 => ['style' => 'text-align:right'],
                        17 => ['style' => 'text-align:right'],
                        18 => ['style' => 'text-align:right'],
                        19 => ['style' => 'text-align:right'],
                        20 => ['style' => 'text-align:right'],
                    ],
                    'options' => ['class' => 'custom-warning', 'style' => 'font-style:italic;']
                ];
            }
        ],
        [
            'attribute' => 'debt_month',
            'label' => Yii::t('common', 'Период формирования ДЗ'),
            'hAlign' => 'center',
            'format' => 'raw',
            'value' => function ($model) {
                return Yii::$app->formatter->asMonth($model['debt_month']);
            }
        ],
        [
            'attribute' => 'period',
            'label' => Yii::t('common', 'Период выставления инвойса КС'),
            'hAlign' => 'center',
            'format' => 'raw',
            'value' => function ($model) {
                $periods = explode(',', $model['period']);
                $lines = [];
                foreach ($periods as $period) {
                    $date = explode('-', $period);
                    $lines[] = Yii::$app->formatter->asDateUTC($date[0]) . ' - ' . Yii::$app->formatter->asDateUTC($date[1]);
                }
                return implode(', ', $lines);
            }
        ],
        [
            'attribute' => 'buyout_count',
            'label' => Yii::t('common', 'Кол-во Заказов ВЫКУП'),
            'hAlign' => 'right',
        ],
        [
            'attribute' => 'process',
            'label' => Yii::t('common', 'Кол-во Заказов в ПРОЦЕССЕ'),
            'hAlign' => 'right',
        ],
        [
            'attribute' => 'process_percent',
            'label' => Yii::t('common', '% в ПРОЦЕССЕ от общего числа заказов'),
            'hAlign' => 'right',
        ],
        [
            'attribute' => 'currency',
            'label' => Yii::t('common', 'Наименование локальной валюты'),
            'hAlign' => 'center',
        ],
        [
            'attribute' => 'buyout_local',
            'label' => Yii::t('common', 'Сумма ВЫКУПа в местной валюте'),
            'format' => ['decimal', 2],
            'hAlign' => 'right',
        ],
        [
            'attribute' => 'buyout_usd',
            'label' => Yii::t('common', 'Сумма ВЫКУПа в USD'),
            'format' => ['decimal', 2],
            'hAlign' => 'right',
        ],
        [
            'attribute' => 'service_local',
            'label' => Yii::t('common', 'Цена услуг СЕРВИСа КС в местной валюте'),
            'format' => ['decimal', 2],
            'hAlign' => 'right',
        ],
        [
            'attribute' => 'service_usd',
            'label' => Yii::t('common', 'Цена услуг СЕРВИСа КС в USD'),
            'format' => ['decimal', 2],
            'hAlign' => 'right',
        ],
        [
            'attribute' => 'total_local',
            'label' => Yii::t('common', 'К ОПЛАТЕ в местной валюте'),
            'format' => ['decimal', 2],
            'hAlign' => 'right',
        ],
        [
            'attribute' => 'total_usd',
            'label' => Yii::t('common', 'К ОПЛАТЕ в USD'),
            'format' => ['decimal', 2],
            'hAlign' => 'right',
        ],
        [
            'attribute' => 'payment_local',
            'label' => Yii::t('common', 'ОПЛАЧЕНО в местной валюте'),
            'format' => ['decimal', 2],
            'hAlign' => 'right',
        ],
        [
            'attribute' => 'payment_usd',
            'label' => Yii::t('common', 'ОПЛАЧЕНО в USD'),
            'format' => ['decimal', 2],
            'hAlign' => 'right',
        ],
        [
            'attribute' => 'debt_local',
            'label' => Yii::t('common', 'Дебиторская задолженность в местной валюте'),
            'format' => ['decimal', 2],
            'hAlign' => 'right',
        ],
        [
            'attribute' => 'debt_usd',
            'label' => Yii::t('common', 'Дебиторская задолженность в USD'),
            'format' => ['decimal', 2],
            'hAlign' => 'right',
        ],
/*
        [
            'label' => Yii::t('common', 'Курсовая разница'),
            'format' => ['decimal', 2],
            'hAlign' => 'right',
        ],
        [
            'label' => Yii::t('common', 'Дебиторская задолженность в USD с учетом курсовой разницы'),
            'format' => ['decimal', 2],
            'hAlign' => 'right',
        ],
*/
    ],
]) ?>
