<?php
use app\modules\report\assets\ReportAsset;
use app\modules\report\assets\FilterCountryDeliveryAsset;
use app\modules\report\widgets\ReportFilterWorkflow;
use app\widgets\Nav;
use app\widgets\Panel;
use yii\web\View;
use yii\helpers\Url;

/** @var app\modules\report\components\ReportFormWorkflow $reportForm */
/** @var yii\data\ArrayDataProvider $dataProviderToCallCenter */
/** @var yii\data\ArrayDataProvider $dataProviderToDelivery */
/** @var yii\data\ArrayDataProvider $dataProviderToDeliveryApproved */
/** @var yii\data\ArrayDataProvider $dataProviderToDeliveryReturned */
/** @var yii\data\ArrayDataProvider $dataProviderToDeliveryPaid */
/** @var array $countries */
/** @var array $series */

$this->title = Yii::t('common', 'Отчет по workflow');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Отчеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

$this->registerJs("var I18n = {};", View::POS_HEAD);
$this->registerJs("I18n['getCountryUrl'] = '" . Url::toRoute(['get-country']) . "';", View::POS_HEAD);

ReportAsset::register($this);
FilterCountryDeliveryAsset::register($this);
?>

<?=ReportFilterWorkflow::widget([
    'reportForm' => $reportForm,
]);?>

<?=Panel::widget([
    'nav' => new Nav([
        'tabs' => [
            [
                'label' => Yii::t('common', 'График'),
                'content' => $this->render('_tab-graph', [
                    'countries' => $countries,
                    'series' => $series,
                ]),
            ],
            [
                'label' => Yii::t('common', 'Создан-Отправлен в колл-центр'),
                'content' => $this->render('_tab', [
                    'dataProvider' => $dataProviderToCallCenter,
                    'reportForm' => $reportForm,
                ]),
            ],
            [
                'label' => Yii::t('common', 'Отправлен в колл-центр-Отправлен в службу доставки'),
                'content' => $this->render('_tab', [
                    'dataProvider' => $dataProviderToDelivery,
                    'reportForm' => $reportForm,
                ]),
            ],
            [
                'label' => Yii::t('common', 'Одобрен в колл-центре-Отправлен в службу доставки'),
                'content' => $this->render('_tab', [
                    'dataProvider' => $dataProviderToDelivery,
                    'reportForm' => $reportForm,
                ]),
            ],
            [
                'label' => Yii::t('common', 'Отправлен в службу доставки-Доставлен'),
                'content' => $this->render('_tab', [
                    'dataProvider' => $dataProviderToDeliveryApproved,
                    'reportForm' => $reportForm,
                ]),
            ],
            [
                'label' => Yii::t('common', 'Доставлен-Возврат'),
                'content' => $this->render('_tab', [
                    'dataProvider' => $dataProviderToDeliveryReturned,
                    'reportForm' => $reportForm,
                ]),
            ],
            [
                'label' => Yii::t('common', 'Доставлен-Оплачен'),
                'content' => $this->render('_tab', [
                    'dataProvider' => $dataProviderToDeliveryPaid,
                    'reportForm' => $reportForm,
                ]),
            ],
        ],
    ]),
]);?>
