<?php
use app\modules\report\extensions\GridViewWorkflow;

/** @var \yii\web\View $this */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */
/** @var app\modules\report\components\ReportFormWorkflow $reportForm */
?>

<div class="table-responsive">
    <?= GridViewWorkflow::widget([
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
        'resizableColumns' => false,
        'showPageSummary' => false,
        'layout' => '<div id="report_export_box">{toolbar}</div>{items}{pager}',
    ]) ?>
</div>

<?php

?>
