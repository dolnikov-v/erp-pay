<?php
use miloschuman\highcharts\Highcharts;

/** @var \yii\web\View $this */
/** @var array $countries */
/** @var array $series */
?>

<div class="table-responsive">
    <?php
    echo Highcharts::widget([
        'options' => [
            'chart' => ['type' => 'bar', 'height' => 1000],
            'title' => ['text' => ''],
            'xAxis' => [
                'categories' => $countries,
            ],
            'yAxis' => [
                'min' => 0,
                'title' => ['text' => Yii::t('common', 'Часы')],
                'labels' => ['overflow' => 'justify']
            ],
            'plotOptions' => [
                'series' => [
                    'stacking' => 'normal',
                ],
            ],
            'credits' => [
                'enabled'=> false
            ],

            'series' => $series,
        ],
    ]);
    ?>
</div>
