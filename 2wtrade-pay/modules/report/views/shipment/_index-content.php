<?php
use app\modules\report\extensions\GridView;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormSale $reportForm */
/** @var \yii\data\ArrayDataProvider $dataProvider */
?>

<div class="table-responsive">
    <?= GridView::widget([
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
        'resizableColumns' => false,
        'showPageSummary' => false,
        'layout' => '<div id="report_export_box">{toolbar}</div>{items}',
        'columns' => [
            ['attribute' => 'country_name', 'label' => Yii::t('common', 'Страна'), 'format' => 'html'],
            ['attribute' => 'delivery_name', 'label' => Yii::t('common', 'Служба доставки'), 'format' => 'html'],
            ['attribute' => 'product_name', 'label' => Yii::t('common', 'Товар'), 'format' => 'html'],
            ['attribute' => 'count_product', 'label' => Yii::t('common', 'Количество'), 'format' => 'html'],
        ],
    ]) ?>
</div>
