<?php
use app\modules\report\assets\ReportAsset;
use app\modules\report\assets\ReportDeliveryClosedPeriodsAsset;
use app\modules\report\widgets\ReportFilterDeliveryClosedPeriods;
use app\widgets\Panel;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormDeliveryClosedPeriods $reportForm */
/** @var \yii\data\ArrayDataProvider $dataProvider */


$this->title = Yii::t('common', 'Закрытые периоды');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Отчеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ReportAsset::register($this);
ReportDeliveryClosedPeriodsAsset::register($this);
?>

<?= ReportFilterDeliveryClosedPeriods::widget([
    'reportForm' => $reportForm,
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Закрытые периоды'),
    'border' => false,
    'withBody' => false,
    'content' => $this->render('_index-content', [
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
    ]),
]) ?>