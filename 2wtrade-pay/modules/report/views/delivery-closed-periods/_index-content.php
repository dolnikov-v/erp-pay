<?php
use app\modules\report\extensions\GridView;
use app\modules\report\components\ReportFormDeliveryClosedPeriods;
use yii\helpers\Html;
use app\widgets\Modal;
use app\widgets\Button;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormDeliveryClosedPeriods $reportForm */
/** @var \app\modules\report\extensions\DataProviderClosePeriods $dataProvider */

$modalId = 'payment-per-period-modal';
?>

<div class="table-responsive">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'resizableColumns' => false,
        'showPageSummary' => false,
        'columns' => [
            [
                'group' => true,
                'groupOddCssClass' => '',
                'groupEvenCssClass' => '',
                'attribute' => 'date',
                'label' => Yii::t('common', 'Месяц'),
                'content' => function ($model) use ($modalId) {

                    $date = explode('-', $model['date']);
                    $text = $date[0] . ', ' . Yii::t('common', ReportFormDeliveryClosedPeriods::monthName($date[1]));

                    return Html::a($text, '#' . $modalId, [
                        'data-toggle' => 'modal',
                        'data-id' => $modalId,
                        'data-month' => $date[1],
                        'data-year' => $date[0],
                    ]);
                },
            ],
            [
                'attribute' => 'delivery_name',
                'label' => Yii::t('common', 'Курьерка'),
                'contentOptions' => function ($model, $key, $index) {
                    $class = ($model['in_process'] + $model['in_delivery'] + $model['buyout'] > 0) ? ' danger' : ' success';
                    return [
                        'key' => $key,
                        'index' => $index,
                        'class' => $class
                    ];
                }
            ],
            [
                'attribute' => 'in_delivery',
                'label' => Yii::t('common', 'В курьерке'),
                'content' => function ($model) {
                    $percent = number_format($model['in_delivery'] / $model['count'] * 100, 2, '.', '');
                    return $model['in_delivery'] . ' (' . $percent . '%)';
                },
                'contentOptions' => function ($model, $key, $index) {
                    $class = ($model['in_delivery'] > 0) ? ' danger' : ' success';
                    return [
                        'key' => $key,
                        'index' => $index,
                        'class' => $class
                    ];
                }
            ],
            [
                'attribute' => 'buyout',
                'label' => Yii::t('common', 'Выкуплено'),
                'content' => function ($model) {
                    $percent = number_format($model['buyout'] / $model['count'] * 100, 2, '.', '');
                    return $model['buyout'] . ' (' . $percent . '%)';
                },
                'contentOptions' => function ($model, $key, $index) {
                    $class = ($model['buyout'] > 0) ? ' danger' : ' success';
                    return [
                        'key' => $key,
                        'index' => $index,
                        'class' => $class
                    ];
                }
            ],
            [
                'attribute' => 'in_process',
                'label' => Yii::t('common', 'В процессе'),
                'content' => function ($model) {
                    $percent = number_format($model['in_process'] / $model['count'] * 100, 2, '.', '');
                    return $model['in_process'] . ' (' . $percent . '%)';
                },
                'contentOptions' => function ($model, $key, $index) {
                    $class = ($model['in_process'] > 0) ? ' danger' : ' success';
                    return [
                        'key' => $key,
                        'index' => $index,
                        'class' => $class
                    ];
                }
            ],
            [
                'attribute' => 'not_buyout',
                'label' => Yii::t('common', 'Не выкуплено'),
                'content' => function ($model) {
                    $percent = number_format($model['not_buyout'] / $model['count'] * 100, 2, '.', '');
                    return $model['not_buyout'] . ' (' . $percent . '%)';
                },
                'contentOptions' => function ($model, $key, $index) {
                    $class = ($model['in_process'] + $model['in_delivery'] + $model['buyout'] > 0) ? ' danger' : ' success';
                    return [
                        'key' => $key,
                        'index' => $index,
                        'class' => $class
                    ];
                }
            ],
            [
                'attribute' => 'money_received',
                'label' => Yii::t('common', 'Деньги получены'),
                'content' => function ($model) {
                    $percent = number_format($model['money_received'] / $model['count'] * 100, 2, '.', '');
                    return $model['money_received'] . ' (' . $percent . '%)';
                },
                'contentOptions' => function ($model, $key, $index) {
                    $class = ($model['in_process'] + $model['in_delivery'] + $model['buyout'] > 0) ? ' danger' : ' success';
                    return [
                        'key' => $key,
                        'index' => $index,
                        'class' => $class
                    ];
                }
            ],
            [
                'attribute' => 'count',
                'label' => Yii::t('common', 'Все'),
                'content' => function ($model) {
                    return $model['count'] . ' (100.00%)';
                },
                'contentOptions' => function ($model, $key, $index) {
                    $class = ($model['in_process'] + $model['in_delivery'] + $model['buyout'] > 0) ? ' danger' : ' success';
                    return [
                        'key' => $key,
                        'index' => $index,
                        'class' => $class
                    ];
                }
            ],
        ],
    ]) ?>
</div>

<?= Modal::widget([
    'id' => $modalId,
    'size' => Modal::SIZE_LARGE,
    'title' => Yii::t('common', 'Платежи за период'),
    'body' => '...',
    'footer' => Html::tag('div',
        Button::widget([
            'label' => Yii::t('common', 'Закрыть'),
            'size' => Button::SIZE_SMALL,
            'attributes' => [
                'data-dismiss' => 'modal',
            ],
        ]),
        ['class' => 'text-right']
    ),
]) ?>



