<?php
use app\components\grid\GridView;

/** @var \yii\web\View $this */
/** @var \yii\data\ArrayDataProvider $dataProvider */

?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'attribute' => 'payment',
            'label' => Yii::t('common', 'Платеж'),
        ],
        [
            'attribute' => 'amount_per_period',
            'label' => Yii::t('common', 'За указанный период ($)'),
            'content' => function($model) {
                return number_format($model['amount_per_period'], 2, '.', ' ');
            },
        ],
        [
            'attribute' => 'amount_except_period',
            'label' => Yii::t('common', 'Остальное ($)'),
            'content' => function($model) {
                return number_format($model['amount_except_period'], 2, '.', ' ');
            },
        ],
    ],
]) ?>
