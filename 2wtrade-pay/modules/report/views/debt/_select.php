<?php
/** @var \yii\web\View $this */
/** @var array $deliveries */
?>

<option value="">—</option>
<?php
foreach ($deliveries as $key => $value) {
    ?>
    <option value="<?= $key ?>"><?= $value ?></option>
    <?php
}
?>
