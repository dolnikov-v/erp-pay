<?php
use app\modules\report\extensions\GridView;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormDebt $reportForm */
/** @var \yii\data\ArrayDataProvider $dataProvider */
?>

<div class="table-responsive">
    <?= GridView::widget([
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
        'resizableColumns' => false,
        'showPageSummary' => false,
        'layout' => '<div id="report_export_box">{toolbar}</div>{items}{pager}',
        'columns' => [
            [
                'attribute' => 'name',
                'label' => Yii::t('common', 'Страна') . "/" . Yii::t('common', 'Служба доставки') . "/" . Yii::t('common', 'Статус задолженности') . "/" . Yii::t('common', 'Месяц'),
                'format' => 'html'
            ],
            ['attribute' => 'current', 'label' => Yii::t('common', 'Текущая'), 'format' => 'html'],
            ['attribute' => 'overdue', 'label' => Yii::t('common', 'Просроченная'), 'format' => 'html'],
            ['attribute' => 'doubtful', 'label' => Yii::t('common', 'Сомнительная'), 'format' => 'html'],
        ],
    ]) ?>
</div>
