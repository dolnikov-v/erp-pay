<?php
use app\modules\report\extensions\GridViewTeam;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormTeam $reportForm */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */
/** @var \app\modules\report\extensions\DataProvider $dataProviderDetail */
/** @var \app\modules\report\extensions\DataProvider $dataProviderDetailPrevious */
/** @var \app\modules\report\extensions\DataProvider $dataProviderDetailPrevious2 */
/** @var array $previousDates */
/** @var array $previousDates2 */
/** @var boolean $showGeneral */
/** @var boolean $showProducts */
?>

<?php if ($dataProvider->allModels): ?>
    <div class="table-responsive">
        <?= GridViewTeam::widget([
            'id' => 'report_grid_team' . ($showProducts ? '_products' : ''),
            'reportForm' => $reportForm,
            'dataProvider' => $dataProvider,
            'showPageSummary' => $reportForm->person ? false : true,
            'resizableColumns' => false,
            'tableOptions' => ['class' => 'table table-report table-striped table-hover table-bordered tl-auto'],
            'showGeneral' => $showGeneral,
            'showProducts' => $showProducts,
        ]) ?>
    </div>
    <?php if ($reportForm->person): ?>
        <h4 class="panel-title font-size-14"><?= Yii::t('common', 'Группировка по направлению, товару') ?></h4>
        <div class="table-responsive">
            <?= GridViewTeam::widget([
                'id' => 'report_grid_team_detail',
                'reportForm' => $reportForm,
                'dataProvider' => $dataProviderDetail,
                'showPageSummary' => false,
                'resizableColumns' => false,
                'detailMode' => true
            ]) ?>
        </div>
        <h4 class="panel-title font-size-14"><?= Yii::t('common', 'Прошлый период') .
            ' ' . Yii::$app->formatter->asDate($previousDates['dateFrom']) .
            ' - ' . Yii::$app->formatter->asDate($previousDates['dateTo']) ?></h4>
        <div class="table-responsive">
            <?= GridViewTeam::widget([
                'id' => 'report_grid_team_detail',
                'reportForm' => $reportForm,
                'dataProvider' => $dataProviderDetailPrevious,
                'showPageSummary' => false,
                'resizableColumns' => false,
                'detailMode' => true
            ]) ?>
        </div>
        <h4 class="panel-title font-size-14"><?= Yii::t('common', 'Прошлый период') .
            ' ' . Yii::$app->formatter->asDate($previousDates2['dateFrom']) .
            ' - ' . Yii::$app->formatter->asDate($previousDates2['dateTo']) ?></h4>
        <div class="table-responsive">
            <?= GridViewTeam::widget([
                'id' => 'report_grid_team_detail',
                'reportForm' => $reportForm,
                'dataProvider' => $dataProviderDetailPrevious2,
                'showPageSummary' => false,
                'resizableColumns' => false,
                'detailMode' => true
            ]) ?>
        </div>
    <?php endif; ?>
<?php endif; ?>