<?php
use app\modules\report\assets\ReportAsset;
use app\modules\report\assets\ReportTeamAsset;
use app\modules\report\widgets\ReportFilterTeam;
use app\widgets\Panel;
use app\modules\report\extensions\GridViewTeamTotal;
use yii\data\ArrayDataProvider;
use app\widgets\Nav;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormTeam $reportForm */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */
/** @var \app\modules\report\extensions\DataProvider $dataProviderDetail */
/** @var \app\modules\report\extensions\DataProvider $dataProviderDetailPrevious */
/** @var \app\modules\report\extensions\DataProvider $dataProviderDetailPrevious2 */
/** @var array $previousDates */
/** @var array $previousDates2 */

$this->title = Yii::t('common', 'Статистика по операторам КЦ');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Отчеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ReportAsset::register($this);
ReportTeamAsset::register($this);
?>

<?= ReportFilterTeam::widget([
    'reportForm' => $reportForm,
]) ?>


<?php if (!$reportForm->person && $reportForm->teamLeads): ?>
    <?php

    $dataTotalProvider = new ArrayDataProvider([
        'allModels' => $reportForm->teamLeads,
        'sort' => [
            'defaultOrder' => ['buyout' => SORT_DESC],
            'attributes' => $reportForm->sortAttributes,
        ]
    ]);
    $dataTotalProvider->pagination->pageSize = $dataTotalProvider->pagination->totalCount;

    ?>

    <?= Panel::widget([
        'title' => Yii::t('common', 'Эффективность команд операторов КЦ'),
        'border' => false,
        'withBody' => false,
        'alert' => $reportForm->panelAlert,
        'alertStyle' => $reportForm->panelAlertStyle,
        'nav' => new Nav([
            'tabs' => [
                0 => [
                    'label' => Yii::t('common', 'Общие данные'),
                    'content' => GridViewTeamTotal::widget([
                        'id' => 'report_grid_team_total',
                        'reportForm' => $reportForm,
                        'dataProvider' => $dataTotalProvider,
                        'showPageSummary' => true,
                        'resizableColumns' => false,
                        'showGeneral' => true,
                        'showProducts' => false,
                        'tableOptions' => ['class' => 'table table-report table-striped table-hover table-bordered tl-auto'],
                    ]),
                ],
                1 => [
                    'label' => Yii::t('common', 'По товарам'),
                    'content' => GridViewTeamTotal::widget([
                        'id' => 'report_grid_team_total_products',
                        'reportForm' => $reportForm,
                        'dataProvider' => $dataTotalProvider,
                        'showPageSummary' => true,
                        'resizableColumns' => false,
                        'showGeneral' => false,
                        'showProducts' => true,
                        'tableOptions' => ['class' => 'table table-report table-striped table-hover table-bordered tl-auto'],
                    ]),
                ],
            ]
        ]),
    ]) ?>
<?php endif; ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Эффективность операторов внутри команд КЦ'),
    'alert' => $reportForm->panelAlert,
    'alertStyle' => $reportForm->panelAlertStyle,
    'border' => false,
    'withBody' => false,
    'nav' => new Nav([
        'activeTab' => 3,
        'tabs' => [
            3 => [
                'label' => Yii::t('common', 'Общие данные'),
                'content' => $this->render('_index-content', [
                    'reportForm' => $reportForm,
                    'dataProvider' => $dataProvider,
                    'dataProviderDetail' => $dataProviderDetail,
                    'dataProviderDetailPrevious' => $dataProviderDetailPrevious,
                    'dataProviderDetailPrevious2' => $dataProviderDetailPrevious2,
                    'previousDates' => $previousDates,
                    'previousDates2' => $previousDates2,
                    'showGeneral' => true,
                    'showProducts' => false,
                ]),
            ],
            4 => [
                'label' => Yii::t('common', 'По товарам'),
                'content' => $this->render('_index-content', [
                    'reportForm' => $reportForm,
                    'dataProvider' => $dataProvider,
                    'dataProviderDetail' => $dataProviderDetail,
                    'dataProviderDetailPrevious' => $dataProviderDetailPrevious,
                    'dataProviderDetailPrevious2' => $dataProviderDetailPrevious2,
                    'previousDates' => $previousDates,
                    'previousDates2' => $previousDates2,
                    'showGeneral' => false,
                    'showProducts' => true,
                ]),
            ],
        ]
    ])
]) ?>
