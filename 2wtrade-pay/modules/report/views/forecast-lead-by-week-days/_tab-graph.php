<?php
use app\modules\report\widgets\GraphForecastLeadByWeekDays;

/** @var \yii\web\View $this */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */
?>

<div class="table-responsive">
    <?=GraphForecastLeadByWeekDays::widget([
        'dataProvider' => $dataProvider,
        'reportForm' => $reportForm
    ]);?>
</div>