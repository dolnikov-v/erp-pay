<?php
use app\modules\report\extensions\GridViewRatingProduct;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormProductRating $reportForm */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */
?>

<div class="table-responsive">
    <?= GridViewRatingProduct::widget([
        'id' => 'report_grid_product_rating',
        'reportForm' => $reportForm,
        'showPageSummary' => false,
        'dataProvider' => $dataProvider,
    ]); ?>
</div>