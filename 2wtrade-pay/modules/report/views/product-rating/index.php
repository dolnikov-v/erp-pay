<?php
use app\modules\report\assets\ReportAsset;
use app\modules\report\widgets\ReportFilterProductRating;
use app\widgets\Panel;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormProductRating $reportForm */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */

$this->title = Yii::t('common', 'Рейтинг товаров');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Отчеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ReportAsset::register($this);
?>

<?=ReportFilterProductRating::widget([
    'reportForm' => $reportForm,
]);?>

<?=Panel::widget([
    'title' => Yii::t('common', 'Рейтинг товаров'),
    'border' => false,
    'withBody' => false,
    'content' => $this->render('_index-content', [
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
    ]),
]);?>