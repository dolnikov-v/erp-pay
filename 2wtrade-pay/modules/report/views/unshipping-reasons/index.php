<?php
use app\modules\report\assets\ReportAsset;
use app\modules\report\widgets\ReportFilterUnshippingReasons;
use app\widgets\Panel;
use app\widgets\Nav;
use app\modules\report\assets\HighChartsNavAsset;
use app\modules\report\assets\FilterCountryDeliveryPartnersAsset;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormUnshippingReasons $reportForm */
/** @var \yii\data\ArrayDataProvider $dataProvider */

$this->title = Yii::t('common', 'Отчёт Анализ невыкупов');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Отчеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ReportAsset::register($this);
HighChartsNavAsset::register($this);
FilterCountryDeliveryPartnersAsset::register($this);
?>

<?= ReportFilterUnshippingReasons::widget([
    'reportForm' => $reportForm,
]) ?>

<?php
$tabs = [];
foreach ($dataProvider->allModels['deliveries'] as $deliveryId => $deliveryName) {
    $tabs[] = [
        'label' => Yii::t('common', $deliveryName),
        'content' => $this->render('_index-content', [
            'reportForm' => $reportForm,
            'data' => $dataProvider->allModels['data'][$deliveryId],
            'deliveryId' => $deliveryId,
            'deliveryName' => $deliveryName,
            'deliveryTotal' => $dataProvider->allModels['deliveryTotals'][$deliveryId] ?? 0,
        ]),
    ];
}
?>
<?= Panel::widget([
    'alert' => $reportForm->panelAlert,
    'alertStyle' => $reportForm->panelAlertStyle,
    'nav' => new Nav([
        'tabs' => $tabs
    ]),
]) ?>