<?php
use app\modules\catalog\models\UnBuyoutReason;
use yii\helpers\Html;
use yii\helpers\Url;
use app\helpers\report\Route;
use miloschuman\highcharts\Highcharts;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormUnshippingReasons $reportForm */
/** @var array $data */
/** @var integer $deliveryId */
/** @var string $deliveryName */
/** @var integer $deliveryTotal */


$categories = UnBuyoutReason::getCategories();

$selectedProducts = [];
$selectedCategories = [];
$series = [];
foreach ($data as $reasonId => $reasonData) {
    foreach ($reasonData as $product) {
        $selectedProducts[$product['product_id']] = $product['product'];
        $selectedCategories[$product['category']] = $selectedCategories[$product['category']] ?? [];

        $selectedCategories[$product['category']][$product['reason_id']] = $selectedCategories[$product['category']][$product['reason_id']] ?? 0;
        $selectedCategories[$product['category']][$product['reason_id']] += $product['cnt'];

        $c = $series[$product['reason_id']]['data'][$product['category']] ?? 0;
        $series[$product['reason_id']] = [
            'name' => $product['reason'],
            'data' => [
                $product['category'] => $c + $product['cnt'],
            ]
        ];
    }
}

$sortOrder = [];
foreach ($selectedCategories as $categoryId => $categoryData) {
    $sortOrder[$categoryId] = array_sum($categoryData);
}
arsort($sortOrder);

$categoriesNames = [];
foreach ($sortOrder as $categoryId => $categoryData) {
    $categoriesNames[] = $categories[$categoryId];
}

$tmp = [];
foreach ($series as $s) {
    $d = [];
    foreach ($sortOrder as $categoryId => $categoryData) {
        $d[] = $s['data'][$categoryId] ?? null;
    }
    $tmp[] = [
        'name' => $s['name'],
        'data' => $d,
    ];
}
$series = $tmp;

$dateFilter = $reportForm->getDailyFilter();
$statuses = $reportForm->getMapStatuses();

?>
<div class="table-responsive">
    <?= Highcharts::widget([
        'options' => [
            'chart' => [
                'type' => 'column'
            ],
            'credits' => [
                'enabled' => false
            ],
            'title' => [
                'text' => ''
            ],
            'xAxis' => [
                'categories' => $categoriesNames,
            ],
            'yAxis' => [
                'min' => 0,
                'title' => [
                    'text' => Yii::t('common', 'Количество')
                ],
                'stackLabels' => [
                    'enabled' => true,
                    'style' => [
                        'fontWeight' => 'bold',
                        'color' => 'black'
                    ]
                ],
            ],
            'legend' => [
                'align' => 'right',
                'x' => 0,
                'verticalAlign' => 'top',
                'y' => 0,
                'floating' => false,
                'backgroundColor' => 'white',
                'borderColor' => '#CCC',
                'borderWidth' => 1,
                'shadow' => false
            ],
            'tooltip' => [
                'headerFormat' => '<b>{point.x}</b><br/>',
                'pointFormat' => '{series.name}: {point.y}<br/>' . Yii::t('common', 'Всего') . ': {point.stackTotal}'
            ],
            'plotOptions' => [
                'column' => [
                    'stacking' => 'normal',
                    'dataLabels' => [
                        'enabled' => false,
                        'color' => 'white',
                    ]
                ]
            ],
            'series' => $series
        ],
    ]);
    ?>
    <table class="table table-bordered table-report" style="margin-top: 50px">
        <tr>
            <th colspan="2" rowspan="2" class="text-center text-middle"><?= Yii::t('common', 'Причина невыкупа') ?></th>
            <th colspan="<?= sizeof($selectedProducts) ?>"
                class="text-center"><?= Yii::t('common', 'Наименование товара') ?></th>
        </tr>
        <tr>
            <?php foreach ($selectedProducts as $productId => $productName): ?>
                <th class="text-center"><?= $productName ?></th>
            <?php endforeach; ?>
        </tr>
        <?php
        $lastCategory = 0;
        $totals = [];
        $lastSlug = '';
        ?>
        <?php foreach ($data as $reasonId => $reasonData): ?>
            <?php $reason = reset($reasonData); ?>
            <tr>
                <?php if ($lastCategory != $reason['category']): ?>
                    <td class="text-middle" rowspan="<?= sizeof($selectedCategories[$reason['category']]) ?>">
                        <?= ((isset($categories[$reason['category']])) ? Yii::t('common', $categories[$reason['category']]) : '') ?>
                        <?php $lastCategory = $reason['category']; ?>
                    </td>
                <?php endif; ?>
                <td>
                    <?= ((isset($reason['reason'])) ? Yii::t('common', $reason['reason']) : '') ?>
                </td>
                <?php foreach ($selectedProducts as $productId => $productName): ?>
                    <td class="text-right">
                        <?php if (isset($reasonData[$productId]['cnt']) && !empty($reasonData[$productId]['cnt'])): ?>
                            <?=
                            Html::a($reasonData[$productId]['cnt'],
                                (Yii::$app->user->can('order.index.index') ?
                                    Url::toRoute([
                                        '/order/index/index/',
                                        'force_country_slug' => $lastSlug = $reasonData[$productId]['slug'] ?? '',
                                        'DateFilter' => Route::getOrderDateFilter([
                                            'from' => Yii::$app->formatter->asDate($dateFilter->from),
                                            'to' => Yii::$app->formatter->asDate($dateFilter->to),
                                            'type' => $dateFilter->type,
                                        ]),
                                        'UnshippingReasonFilter' => [
                                            'unshippingReason' => $reasonData[$productId]['reason_id'],
                                        ],
                                        'ProductFilter' => [
                                            'product' => $productId
                                        ],
                                        'StatusFilter' => [
                                            'status' => $statuses['not_buyout']
                                        ],
                                        'DeliveryFilter' => [
                                            'delivery' => $reasonData[$productId]['delivery_id']
                                        ]
                                    ])
                                    : '#'),
                                [
                                    'target' => '_blank'
                                ])
                            ?>
                            <?php
                            $totals[$productId] = $totals[$productId] ?? 0;
                            $totals[$productId] += $reasonData[$productId]['cnt'];
                            ?>
                        <?php endif; ?>
                    </td>
                <?php endforeach; ?>
            </tr>
        <?php endforeach; ?>
        <tr class="warning">
            <td><?= Yii::t('common', 'Итого') ?></td>
            <td></td>
            <?php foreach ($selectedProducts as $productId => $productName): ?>
                <td class="text-right"><?= Html::a($totals[$productId],
                        (Yii::$app->user->can('order.index.index') ?
                            Url::toRoute([
                                '/order/index/index/',
                                'force_country_slug' => $lastSlug ?? '',
                                'DateFilter' => Route::getOrderDateFilter([
                                    'from' => Yii::$app->formatter->asDate($dateFilter->from),
                                    'to' => Yii::$app->formatter->asDate($dateFilter->to),
                                    'type' => $dateFilter->type,
                                ]),
                                'ProductFilter' => [
                                    'product' => $productId
                                ],
                                'StatusFilter' => [
                                    'status' => $statuses['not_buyout']
                                ],
                                'DeliveryFilter' => [
                                    'delivery' => $deliveryId
                                ]
                            ])
                            : '#'),
                        [
                            'target' => '_blank'
                        ])
                    ?>
                </td>
            <?php endforeach; ?>
        </tr>
        <tr class="report-summary">
            <td colspan="2"><?= Yii::t('common', 'Итого по') . ' ' . $deliveryName ?> </td>
            <td class="text-right"
                colspan="<?= sizeof($totals) ?>">
                <?php
                $sum = array_sum($totals);
                $percent = $deliveryTotal ? $sum * 100 / $deliveryTotal : 0;
                ?>
                <?= Html::a(array_sum($totals) . ' (' . Yii::$app->formatter->asDecimal($percent, 2) . '%)',
                    (Yii::$app->user->can('order.index.index') ?
                        Url::toRoute([
                            '/order/index/index/',
                            'force_country_slug' => $lastSlug ?? '',
                            'DateFilter' => Route::getOrderDateFilter([
                                'from' => Yii::$app->formatter->asDate($dateFilter->from),
                                'to' => Yii::$app->formatter->asDate($dateFilter->to),
                                'type' => $dateFilter->type,
                            ]),
                            'StatusFilter' => [
                                'status' => $statuses['not_buyout']
                            ],
                            'DeliveryFilter' => [
                                'delivery' => $deliveryId
                            ]
                        ])
                        : '#'),
                    [
                        'target' => '_blank'
                    ])
                ?>
            </td>
        </tr>
    </table>
</div>