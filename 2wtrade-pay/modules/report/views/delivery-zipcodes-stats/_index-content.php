<?php
use app\modules\report\extensions\GridViewDeliveryZipcodesStats;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormDeliveryZipcodesStats $reportForm */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */
?>
<?php if ($dataProvider->allModels): ?>
    <div class="table-responsive">
        <?= GridViewDeliveryZipcodesStats::widget([
            'id' => 'report_grid_delivery',
            'reportForm' => $reportForm,
            'dataProvider' => $dataProvider,
        ]) ?>
    </div>
<?php endif;?>