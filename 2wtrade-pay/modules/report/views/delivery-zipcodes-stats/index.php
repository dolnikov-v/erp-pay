<?php
use app\modules\report\assets\ReportAsset;
use app\modules\report\widgets\ReportFilterDeliveryZipcodesStats;
use app\widgets\Panel;


/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormDeliveryZipcodesStats $reportForm */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */


$this->title = Yii::t('common', 'Статистика по группам индексов доставки');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Отчеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ReportAsset::register($this);
?>

<?= ReportFilterDeliveryZipcodesStats::widget([
    'reportForm' => $reportForm,
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Статистика по группам индексов доставки'),
    'alert' => $reportForm->panelAlert,
    'alertStyle' => $reportForm->panelAlertStyle,
    'border' => false,
    'withBody' => false,
    'content' => $this->render('_index-content', [
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
    ]),
]) ?>


