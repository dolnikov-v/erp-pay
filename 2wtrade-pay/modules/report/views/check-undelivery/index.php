<?php
/** @var \yii\web\View $this */
use app\modules\report\assets\ReportAsset;
use app\modules\report\widgets\ReportFilterCheckUndelivery;
use app\widgets\Panel;

/** @var \app\modules\report\extensions\DataProvider $dataProvider */
/** @var \app\modules\report\components\ReportFormCheckUndelivery $reportForm */

$this->title = Yii::t('common', 'Результаты не выкупа');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Отчеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ReportAsset::register($this);
?>


<?= ReportFilterCheckUndelivery::widget([
    'reportForm' => $reportForm,
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Колл-центры'),
    'border' => false,
    'withBody' => false,
    'content' => $this->render('_index-content', [
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
    ]),
]) ?>
