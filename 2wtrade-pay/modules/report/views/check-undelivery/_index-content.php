<?php

use app\modules\report\extensions\GridViewCheckUndelivery;

/** @var \app\modules\report\extensions\DataProvider $dataProvider */
/** @var \app\modules\report\components\ReportFormCheckUndelivery $reportForm */

?>

<div class="table-responsive">
    <?= GridViewCheckUndelivery::widget([
        'id' => 'report_grid_check_undelivery',
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
    ]) ?>
</div>
