<?php
use app\modules\report\extensions\GridViewFinanceCountries;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormFinanceCountries $reportForm */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */
?>

<div class="table-responsive">
    <?= GridViewFinanceCountries::widget([
        'id' => 'report_grid_finance_countries',
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
    ]) ?>
</div>
