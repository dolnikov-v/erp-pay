<?php
use app\modules\report\assets\ReportAsset;
use app\modules\report\widgets\ReportFilterForeignOperator;
use app\widgets\Panel;
use app\widgets\Nav;
use app\modules\report\assets\ReportForeignOperatorAsset;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormForeignOperator $reportForm */
/** @var \app\modules\report\extensions\DataProvider $dataProviderOld */
/** @var \app\modules\report\extensions\DataProvider $dataProviderNew */
/** @var integer $approvePlan */

$this->title = Yii::t('common', 'Выкуп по операторам');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Отчеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ReportAsset::register($this);
ReportForeignOperatorAsset::register($this);
?>

<?= ReportFilterForeignOperator::widget([
    'reportForm' => $reportForm,
]) ?>

<?= Panel::widget([
    'alert' => $reportForm->panelAlert,
    'alertStyle' => $reportForm->panelAlertStyle,
    'title' => Yii::t('common', 'Выкуп по операторам'),
    'border' => false,
    'withBody' => false,
    'nav' => new Nav([
        'tabs' => [
            [
                'label' => Yii::t('common', 'Опытные'),
                'content' => $this->render('_tab-old', [
                    'dataProvider' => $dataProviderOld,
                    'reportForm' => $reportForm,
                    'approvePlan' => $approvePlan,
                ]),
            ],
            [
                'label' => Yii::t('common', 'Новички'),
                'content' => $this->render('_tab-new', [
                    'dataProvider' => $dataProviderNew,
                    'reportForm' => $reportForm,
                    'approvePlan' => $approvePlan,
                ]),
            ],

        ],
    ]),
]) ?>
