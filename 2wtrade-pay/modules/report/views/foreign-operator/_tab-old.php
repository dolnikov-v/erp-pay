<?php
use app\modules\report\extensions\GridViewForeignOperator;
use app\modules\report\controllers\ForeignOperatorController;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormForeignOperator $reportForm */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */
/** @var integer $approve_good_percent */
/** @var integer $approvePlan */

?>
<div class="table-responsive">
    <?= GridViewForeignOperator::widget([
        'id' => 'report_grid_foreignoperator_old',
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
        'rowOptions' => function ($data) use ($approvePlan) {
            $options = ['class' => ''];

            $approve = $data[Yii::t('common', 'Подтверждено')];

            $p = 0;
            if ($data["count_lead"])
                $p = $approve * 100 / $data["count_lead"];

            $positive = ($p >= ForeignOperatorController::APPROVE_GOOD_PERCENT && $approve >= $approvePlan * $data['working_days'] );

            $options['class'] = ($positive) ? 'has-ok' : 'has-error';

            return $options;
        },
        'showPageSummary' => false,
        'resizableColumns' => false,
    ]) ?>
</div>
