<?php
use app\modules\report\extensions\GridView;
use app\modules\report\components\ReportFormForecastLead;
use yii\helpers\Html;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormForecastLead $reportForm */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */
$days = (Yii::$app->formatter->asTimeStamp($reportForm->getForecastLeadFilter()->to) + 86400 - Yii::$app->formatter->asTimeStamp($reportForm->getForecastLeadFilter()->from)) / 86400;
?>

<div class="table-responsive">
    <?= GridView::widget([
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
        'resizableColumns' => false,
        'showPageSummary' => false,
        'layout' => '{items}',
        'rowOptions' => function ($model) use ($days) {
            $options = ['class' => 'tr-vertical-align-middle '];
            if ($model['need_stock_day'] !== false) {
                $countProducts = $model['need_stock_day'] * ReportFormForecastLead::PRODUCT_NORM_DAY;
                if ($countProducts <= $model['balance']) {
                    $options['class'] .= 'custom-green';
                    $options['title'] = Yii::t('common', 'Количество необходимого товара превышено');
                } else {
                    $options['class'] .= 'custom-error';
                    $options['title'] = Yii::t('common', 'Необходимо докупить {count} шт товара', ['count' => number_format(($countProducts - $model['balance']), 2, ',', ' ')]);
                }
            }
            return $options;
        },
        'columns' => [
            [
                'label' => Yii::t('common', 'Страна'),
                'group' => true,
                'groupedRow' => true,
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'value' => function ($model) {
                    return ReportFormForecastLead::$countries[$model['country_id']];
                },
                'groupFooter' => function () {
                    $response = [
                        'content' => [
                            7 => GridView::F_SUM,
                            8 => GridView::F_SUM,
                        ],
                        'contentFormats' => [
                            7 => ['format' => 'number', 'decimals' => 2, 'decPoint'=>',', 'thousandSep'=>' '],
                            8 => ['format' => 'number', 'decimals' => 2, 'decPoint'=>',', 'thousandSep'=>' '],
                        ],
                        'contentOptions' => [
                            7 => ['style' => 'text-align:right'],
                            8 => ['style' => 'text-align:right'],

                        ],
                        'options' => ['class' => 'warning', 'style' => 'font-weight:bold;']
                    ];
                    return $response;
                }
            ],
            [
                'label' => Yii::t('common', 'Продукт'),
                'value' => function ($model) {
                    return ReportFormForecastLead::$products[$model['product_id']];
                },
                'contentOptions' => [
                    'class' => 'custom-active'
                ]
            ],
            [
                'label' => Yii::t('common', 'Число лидов в день'),
                'format' => 'raw',
                'options' => ['class' => 'rowInputGrid'],
                'contentOptions' => ['style' => 'text-align:center'],
                'value' => function ($model) {
                    return Html::textInput('leads_day_' . $model['country_id'] . '_' . $model['product_id'], $model['count_leads'], ['class' => 'inputGrid form-control']);
                },
            ],
            [
                'label' => Yii::t('common', 'Доля одобрения'),
                'format' => 'raw',
                'options' => ['class' => 'rowInputGrid'],
                'contentOptions' => ['style' => 'text-align:center'],
                'value' => function ($model) {
                    return Html::textInput('percent_approved_' . $model['country_id'] . '_' . $model['product_id'], $model['approve_percent'], ['class' => 'inputGrid form-control']);
                },
            ],
            [
                'label' => Yii::t('common', 'Cр кол-во продуктов в заказе'),
                'contentOptions' => ['style' => 'text-align:right'],
                'value' => function ($model) {
                    if ($model['avg_count_product'] === false) {
                        return Yii::t('common', 'Нет данных для расчета');
                    }
                    return number_format($model['avg_count_product'], 2, ',', ' ');
                }
            ],
            [
                'label' => Yii::t('common', 'Остатки на складе'),
                'attribute' => 'balance',
                'format'=>['decimal',2],
                'contentOptions' => ['style' => 'text-align:right'],
            ],
            [
                'label' => Yii::t('common', 'Необходимо товара на {day} дней', ['day' => ReportFormForecastLead::PRODUCT_NORM_DAY]),
                'contentOptions' => function ($model) {
                    $options = ['style' => 'text-align:right'];
                    if ($model['count_leads'] !== false && $model['approve_percent'] !== false && $model['avg_count_product'] !== false) {
                        $options['title'] = $model['count_leads'] . ' * ' . $model['approve_percent'] . ' * ' . $model['avg_count_product'] . ' * ' . ReportFormForecastLead::PRODUCT_NORM_DAY;
                    }
                    return $options;
                },
                'value' => function ($model) {
                    if ($model['need_stock_day'] === false) {
                        return Yii::t('common', 'Нет данных для расчета');
                    }
                    return number_format(($model['need_stock_day'] * ReportFormForecastLead::PRODUCT_NORM_DAY), 2, ',', ' ');
                }
            ],
            [
                'label' => Yii::t('common', 'Запас товара, дней'),
                'contentOptions' => function ($model) {
                    $options = ['style' => 'text-align:right'];
                    if ($model['stock_days'] !== false) {
                        $options['title'] = $model['balance'] . ' / ' . $model['need_stock_day'];
                    }
                    return $options;
                },
                'value' => function ($model) {
                    if ($model['stock_days'] === false) {
                        return Yii::t('common', 'Нет данных для расчета');
                    }
                    return number_format($model['stock_days'], 2, ',', ' ');
                }
            ],
        ],
    ]) ?>
</div>

