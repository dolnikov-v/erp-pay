<?php
use app\modules\report\extensions\GridView;
use app\modules\report\components\ReportFormForecastLead;
use yii\helpers\Html;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormForecastLead $reportForm */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */
?>

<div class="table-responsive">
    <?= GridView::widget([
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
        'resizableColumns' => false,
        'showPageSummary' => false,
        'layout' => '{items}',
        'columns' => [
            [
                'label' => Yii::t('common', 'Страна'),
                'format' => 'raw',
                'group' => true,
                'groupedRow' => true,
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'value' => function ($model) {
                    return '<b>' . ReportFormForecastLead::$countries[$model['country_id']] . '</b><br>'
                        . Yii::t('common', 'Ср число звонков на заказ') . ' : '
                        . Html::textInput('avg_calls_order_' . $model['country_id'], number_format($model['avg_calls_order'], 2), ['class' => 'inputGrid form-control']) . '<br>'
                        . Yii::t('common', 'Ср число звонков оператора') . ' : '
                        . Html::textInput('avg_calls_oper_' . $model['country_id'], number_format($model['avg_calls_oper'], 2), ['class' => 'inputGrid form-control']);
                },
            ],
            [
                'label' => Yii::t('common', 'Продукт'),
                'value' => function ($model) {
                    return ReportFormForecastLead::$products[$model['product_id']];
                },
                'contentOptions' => [
                    'class' => 'custom-active'
                ]
            ],
            [
                'label' => Yii::t('common', 'Доля одобрения'),
                'format' => 'raw',
                'options' => ['class' => 'rowInputGrid'],
                'contentOptions' => ['style' => 'text-align:center'],
                'value' => function ($model) {
                    return Html::textInput('percent_approved_' . $model['country_id'] . '_' . $model['product_id'], $model['percent_approve'], ['class' => 'inputGrid form-control']);
                },
            ],
            [
                'label' => Yii::t('common', 'Ср кол-во товара в заказе'),
                'format' => 'raw',
                'options' => ['class' => 'rowInputGrid'],
                'contentOptions' => ['style' => 'text-align:center'],
                'value' => function ($model) {
                    return Html::textInput('avg_quantity_' . $model['country_id'] . '_' . $model['product_id'], $model['avg_quantity'], ['class' => 'inputGrid form-control']);
                },
            ],
            [
                'label' => Yii::t('common', 'Доля товара в заказе'),
                'format' => 'raw',
                'options' => ['class' => 'rowInputGrid'],
                'contentOptions' => ['style' => 'text-align:center'],
                'value' => function ($model) {
                    return Html::textInput('part_quantity_' . $model['country_id'] . '_' . $model['product_id'], $model['part_quantity'], ['class' => 'inputGrid form-control']);
                },
            ],
            [
                'label' => Yii::t('common', 'Кол-во операторов в день'),
                'group' => true,
                'subGroupOf' => 0,
                'hAlign' => 'center',
                'vAlign' => 'top',
                'value' => function ($model) {
                    if ($model['operators_fact'] === false) {
                        return Yii::t('common', 'Нет данных для расчета');
                    }
                    return number_format($model['operators_fact'], 2, ',', ' ');
                },
            ],
            [
                'label' => Yii::t('common', 'Прогноз лидов по товару (Max по одному виду)'),
                'contentOptions' => function ($model) {
                    $options = ['style' => 'text-align:right'];
                    if ($model['percent_approve'] !== false && $model['avg_quantity'] !== false && $model['part_quantity'] !== false && !is_null($model['avg_quantity']) && $model['avg_quantity'] != 0) {
                        $options['title'] = '(' . $model['product_balance'] . ' - ' . $model['need_product'] . ' - ' . '(' . $model['holds'] . ' * ' . $model['percent_approve'] . ' * ' . $model['avg_quantity'] . ' * ' . $model['part_quantity'] . '))' . ' / ' . $model['avg_quantity'];
                    }
                    return $options;
                },
                'value' => function ($model) {
                    if ($model['percent_approve'] === false || $model['avg_quantity'] === false || $model['part_quantity'] === false) {
                        return Yii::t('common', 'Нет данных для расчета');
                    }
                    if (is_null($model['avg_quantity']) || $model['avg_quantity'] == 0) {
                        $result = 0;
                    } else {
                        $result =
                            ($model['product_balance'] - $model['need_product'] - ($model['holds'] * $model['percent_approve'] * $model['avg_quantity'] * $model['part_quantity'])) / $model['avg_quantity'];
                    }
                    return number_format($result, 2, ',', ' ');
                },
            ],
            [
                'label' => Yii::t('common', 'Прогноз лидов по товару (По % соотношению)'),
                'contentOptions' => function ($model) {
                    $options = ['style' => 'text-align:right'];
                    if ($model['percent_approve'] !== false && $model['avg_quantity'] !== false && $model['part_quantity'] !== false && !is_null($model['part_quantity']) && $model['part_quantity'] != 0) {
                        $options['title'] = '((' . $model['product_balance'] . ' - ' . $model['need_product'] . ' - (' . $model['holds'] . ' * ' . $model['percent_approve'] . ' * ' . $model['avg_quantity'] . ' * ' . $model['part_quantity'] . ')) - ' . $model['avg_quantity'] . ') / ' . $model['part_quantity'];
                    }
                    return $options;
                },
                'value' => function ($model) {
                    if ($model['percent_approve'] === false || $model['avg_quantity'] === false || $model['part_quantity'] === false) {
                        return Yii::t('common', 'Нет данных для расчета');
                    }
                    if (is_null($model['part_quantity']) || $model['part_quantity'] == 0) {
                        $result = 0;
                    } else {
                        $result =
                            (($model['product_balance'] - $model['need_product'] - ($model['holds'] * $model['percent_approve'] * $model['avg_quantity'] * $model['part_quantity'])) - $model['avg_quantity']) / $model['part_quantity'];
                    }
                    return number_format($result, 2, ',', ' ');
                },
            ],
            [
                'label' => Yii::t('common', 'Прогноз лидов по операторам'),
                'group' => true,
                'subGroupOf' => 0,
                'hAlign' => 'center',
                'vAlign' => 'top',
                'contentOptions' => function ($model) {
                    $options = ['style' => 'text-align:right'];
                    if (!is_null($model['avg_calls_order']) && $model['avg_calls_order'] != 0) {
                        $options['title'] = $model['avg_calls_oper'] . ' * ' . $model['operators_fact'] . ' / ' . $model['avg_calls_order'];
                    }
                    return $options;
                },
                'value' => function ($model) {
                    if ($model['forecast_oper'] === false) {
                        return Yii::t('common', 'Нет данных для расчета: Ср число звонков на заказ = 0');
                    }
                    return number_format($model['forecast_oper'], 2, ',', ' ');
                },
                'groupFooter' => function ($model) use ($totalByOneType, $totalByRatio) {
                    $response = [
                        'content' => [
                            6 => !is_null($totalByOneType[$model['country_id']]) ?
                                number_format($totalByOneType[$model['country_id']], 2, ',', ' ') : '—',
                            7 => !is_null($totalByRatio[$model['country_id']]) ?
                                number_format($totalByRatio[$model['country_id']], 2, ',', ' ') : '—',
                            9 => GridView::F_SUM,
                        ],
                        'contentFormats' => [
                            9 => ['format' => 'number', 'decimals' => 2, 'decPoint'=>',', 'thousandSep'=>' '],
                        ],
                        'contentOptions' => [
                            6 => ['style' => 'text-align:right'],
                            7 => ['style' => 'text-align:right'],
                            9 => ['style' => 'text-align:right'],

                        ],
                        'options' => ['class' => 'warning', 'style' => 'font-weight:bold;']
                    ];
                    return $response;
                }

            ],
            [
                'label' => Yii::t('common', 'Текущий холд'),
                'contentOptions' => ['style' => 'text-align:right'],
                'value' => function ($model) {
                    return number_format($model['holds'], 2, ',', ' ');
                }
            ],
            /*[
                'label' => Yii::t('common', 'Отправлено в КЦ'),
                'format'=>['decimal', 2],
                'contentOptions' => ['style' => 'text-align:right'],
                'attribute' => 'in_call_center',
            ],
            [
                'label' => Yii::t('common', 'Одобрено в КЦ'),
                'format'=>['decimal', 2],
                'contentOptions' => ['style' => 'text-align:right'],
                'attribute' => 'approved',
            ],*/
        ],
    ]) ?>
</div>
