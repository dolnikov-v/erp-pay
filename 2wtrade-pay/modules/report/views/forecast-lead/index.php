<?php
use app\modules\report\assets\ReportAsset;
use app\modules\report\widgets\ReportFilterForecastLead;
use app\widgets\Panel;
use app\widgets\Nav;
use app\modules\report\assets\ReportDeliveryDebtsAsset;
use app\components\widgets\ActiveForm;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormForecastLead $reportForm */
/** @var \yii\data\ArrayDataProvider $dataProviderLead */
/** @var \yii\data\ArrayDataProvider $dataProviderOpers */
/** @var \yii\data\ArrayDataProvider $dataProviderProducts */

$this->title = Yii::t('common', 'Прогнозирование лидов');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Отчеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ReportAsset::register($this);
ReportDeliveryDebtsAsset::register($this);
?>

<?= ReportFilterForecastLead::widget([
    'reportForm' => $reportForm,
]) ?>

<?=Panel::widget([
    'alert' => Yii::t('common', 'Нет данных для расчета означает, что заказов не было по данной стране, товару, колл-центру за последние 7 дней'),
    'nav' => new Nav([
        'tabs' => [
            [
                'label' => Yii::t('common', 'Лиды'),
                'content' => $this->render('_tab_lead', [
                    'dataProvider' => $dataProviderLead,
                    'reportForm' => $reportForm,
                    'totalByRatio' => $totalByRatio,
                    'totalByOneType' => $totalByOneType
                ]),
            ],
            [
                'label' => Yii::t('common', 'Операторы'),
                'content' => $this->render('_tab_opers', [
                    'dataProvider' => $dataProviderOpers,
                    'reportForm' => $reportForm,
                ]),
            ],
            [
                'label' => Yii::t('common', 'Товары'),
                'content' => $this->render('_tab_product', [
                    'dataProvider' => $dataProviderProducts,
                    'reportForm' => $reportForm,
                ]),
            ],
        ],
    ]),
]); ?>

<?php ActiveForm::end(); ?>
