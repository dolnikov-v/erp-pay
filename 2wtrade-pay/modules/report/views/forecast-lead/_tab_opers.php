<?php
use app\modules\report\extensions\GridView;
use app\modules\report\components\ReportFormForecastLead;
use yii\helpers\Html;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormForecastLead $reportForm */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */
$days = abs(Yii::$app->formatter->asTimeStamp($reportForm->getForecastLeadFilter()->to) + 86400 - Yii::$app->formatter->asTimeStamp($reportForm->getForecastLeadFilter()->from)) / 86400;
?>

<div class="table-responsive">
    <?= GridView::widget([
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
        'resizableColumns' => false,
        'showPageSummary' => false,
        'layout' => '{items}',
        'rowOptions' => function ($model) use ($days) {
            $options = ['class' => 'tr-vertical-align-middle '];
            if ($model['leads_day'] === false && $model['avg_calls_order'] === false && $model['avg_calls_oper'] === false && !is_null($model['avg_calls_oper']) && $model['avg_calls_oper'] != 0) {
                if (($model['operators_fact'] * $days) < ( ($model['need_called'] + $model['leads_day'] * $days) * $model['avg_calls_order'] / $model['avg_calls_oper'] / $days )) {
                    $options['class'] .= 'custom-error';
                    $options['title'] = Yii::t('common', 'Не хватает операторов');
                } else {
                    $options['class'] .= 'custom-success';
                }
            }
            return $options;
        },
        'columns' => [
            [
                'label' => Yii::t('common', 'Страна'),
                'format' => 'raw',
                'group' => true,
                'groupedRow' => true,
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'value' => function ($model) {
                    return '<b>' . ReportFormForecastLead::$countries[$model['country_id']] . '</b><br>'
                        . Yii::t('common', 'Число лидов в день') . ' : '
                        . Html::textInput('leads_day_all_' . $model['country_id'], number_format($model['leads_day'], 2), ['class' => 'inputGrid form-control']);
                },
                'groupFooter' => function () {
                    $response = [
                        'content' => [
                            4 => GridView::F_SUM,
                            5 => GridView::F_SUM,
                        ],
                        'contentFormats' => [
                            4 => ['format' => 'number', 'decimals' => 2, 'decPoint'=>',', 'thousandSep'=>' '],
                            5 => ['format' => 'number', 'decimals' => 2, 'decPoint'=>',', 'thousandSep'=>' '],
                        ],
                        'contentOptions' => [
                            4 => ['style' => 'text-align:right'],
                            5 => ['style' => 'text-align:right'],

                        ],
                        'options' => ['class' => 'warning', 'style' => 'font-weight:bold;']
                    ];
                    return $response;
                }
            ],
            [
                'label' => Yii::t('common', 'Колл-центр'),
                'value' => function ($model) {
                    return ReportFormForecastLead::$callCenters[$model['call_center_id']];
                },
                'contentOptions' => [
                    'class' => 'custom-active'
                ]
            ],
            [
                'label' => Yii::t('common', 'Ср кол-во звонков на заказ'),
                'format' => 'raw',
                'options' => ['class' => 'rowInputGrid'],
                'contentOptions' => ['style' => 'text-align:center'],
                'value' => function ($model) {
                    return Html::textInput('avg_calls_order_' . $model['country_id'] . '_' . $model['call_center_id'], $model['avg_calls_order'], ['class' => 'inputGrid form-control']);
                },
            ],
            [
                'label' => Yii::t('common', 'Ср кол-во звонков оператора'),
                'format' => 'raw',
                'options' => ['class' => 'rowInputGrid'],
                'contentOptions' => ['style' => 'text-align:center'],
                'value' => function ($model) {
                    return Html::textInput('avg_calls_oper_' . $model['country_id'] . '_' . $model['call_center_id'], $model['avg_calls_oper'], ['class' => 'inputGrid form-control']);
                },
            ],
            [
                'label' => Yii::t('common', 'Необходимо операторов на 1 рабочий дней'),
                'contentOptions' => function ($model) use ($days) {
                    $options = ['style' => 'text-align:right'];
                    if ($model['leads_day'] !== false && $model['avg_calls_order'] !== false && $model['avg_calls_oper'] !== false && !is_null($model['avg_calls_oper']) && $model['avg_calls_oper'] != 0) {
                        $options['title'] = '(' . $model['need_called'] . '+' . $model['leads_day'] . '*' . $days . ') * ' . $model['avg_calls_order'] . '/' . $model['avg_calls_oper'] . '/' . $days;
                    }
                    return $options;
                },
                'value' => function ($model) use ($days) {
                    if ($model['leads_day'] === false || $model['avg_calls_order'] === false || $model['avg_calls_oper'] === false) {
                        return Yii::t('common', 'Нет данных для расчета');
                    }
                    if (is_null($model['avg_calls_oper']) || $model['avg_calls_oper'] == 0) {
                        return Yii::t('common', 'Нет данных для расчета: Ср кол-во звонков оператора = 0');
                    }
                    return number_format(( ($model['need_called'] + $model['leads_day'] * $days) * $model['avg_calls_order'] / $model['avg_calls_oper'] / $days ), 2, ',', ' ');
                },
            ],
            [
                'label' => Yii::t('common', 'Операторы факт в 1 рабочий день'),
                'contentOptions' => ['style' => 'text-align:right'],
                'value' => function ($model) use ($days) {
                    if ($model['operators_fact'] === false) {
                        return Yii::t('common', 'Нет данных для расчета');
                    }
                    return number_format($model['operators_fact'], 2, ',', ' ');
                }
            ],
        ],
    ]) ?>
</div>
