<?php
/** @var app\modules\report\components\ReportFormValidateFinance $reportForm */
?>

<?= $this->render('_load-form', [
    'reportForm' => $reportForm,
]) ?>
