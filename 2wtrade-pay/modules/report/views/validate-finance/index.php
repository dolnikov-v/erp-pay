<?php
use app\modules\report\assets\ReportAsset;
use app\modules\report\assets\ReportValidateFinanceAsset;
use app\widgets\Panel;
use app\widgets\Nav;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormValidateFinance $reportForm */
/** @var yii\data\ArrayDataProvider $dataHistory */
/** @var array $sumsArray */
/** @var string $file */

$this->title = Yii::t('common', 'Сверка финансового отчета');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Отчеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ReportAsset::register($this);
ReportValidateFinanceAsset::register($this);

if ($file !== null) {
    $view = '_tab-history-one';
    $dataArray = [
        'reportForm' => $reportForm,
        'dataHistory' => $dataHistory,
        'sumsArray' => $sumsArray,
        'model' => $model,
    ];
} else {
    $view = '_tab-history';
    $dataArray = [
        'reportForm' => $reportForm,
        'dataHistory' => $dataHistory,
    ];
}
?>

<?= Panel::widget([
    'nav' => new Nav([
        'tabs' => [
            [
                'label' => Yii::t('common', 'История загрузок'),
                'content' => $this->render($view, $dataArray),
            ],
            [
                'label' => Yii::t('common', 'Загрузка файла'),
                'content' => $this->render('_tab-load', [
                    'reportForm' => $reportForm,
                ]),
            ],
        ]
    ]),
]) ?>
