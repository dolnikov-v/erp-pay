<?php
use app\modules\report\extensions\GridView;
use app\modules\report\widgets\ReportFilterValidateFinance;
use yii\helpers\Html;
use yii\helpers\Url;
use app\widgets\LinkPager;

/** @var app\modules\report\components\ReportFormValidateFinance $reportForm */
/** @var yii\data\ArrayDataProvider $dataHistory */
/** @var array $sumsArray */
?>

<?= ReportFilterValidateFinance::widget([
    'reportForm' => $reportForm,
]) ?>

<div class="table-responsive margin-top-20">
    <?= GridView::widget([
        'layout' => '<div id="report_export_box">{toolbar}</div>{items}<div class="panel-footer clearfix  margin-top-15  padding-right-0">{pager}</div>',
        'dataProvider' => $dataHistory,
        'resizableColumns' => false,
        'showPageSummary' => false,
        'export' => false,
        'pager' => [
            'class' => 'app\widgets\LinkPager',
            'counterPosition' => LinkPager::COUNTER_POSITION_RIGHT,
        ],
        'columns' => [
            [
                'value' => function($model) {
                    return Html::a($model['file_name_trim'] . ' ' . $model['created'], Url::to(['validate-finance/index']) . '?file=' . $model['id']);
                },
                'label' => Yii::t('common', 'Отчеты'),
                'format' => 'html',
                'headerOptions' => ['class' => 'text-left']
            ],
        ]
    ]) ?>
</div>
