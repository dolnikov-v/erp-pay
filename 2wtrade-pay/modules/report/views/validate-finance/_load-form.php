<?php
use app\widgets\InputGroupFile;
use app\widgets\InputText;
use yii\helpers\Html;
use app\components\widgets\ActiveForm;
use yii\helpers\Url;
use app\widgets\ProgressBar;
use yii\web\View;

/** @var yii\web\View $this */
/** @var app\modules\report\components\ReportFormValidateFinance $reportForm */
/** @var \app\modules\report\models\ReportValidateFinance $reportModel */

$this->registerJs("var I18n = {};", View::POS_HEAD);
$this->registerJs("I18n['Файл обработан'] = '" . Yii::t('common',
        'Файл обработан') . "';", View::POS_HEAD);

?>
<div class="table-responsive padding-20" id="validate-finance">
<?php
$form = ActiveForm::begin([
    'options' => [
        'enctype' => 'multipart/form-data',
    ],
    'action' => Url::to(['validate-finance/load']),
    'method' => 'post',
]); ?>
<div class="row">
    <div class="col-md-3">
        <?= $form->field($reportForm->model, 'row_begin')->textInput(['value' => (isset($reportForm->model->row_begin)) ? $reportForm->model->row_begin : 0]) ?>
    </div>
</div>
<div class="row">
    <div class="col-md-3">
        <?= $form->field($reportForm->model, 'cell_order_id')->checkTextInput() ?>
    </div>
    <div class="col-md-3">
        <?= $form->field($reportForm->model, 'cell_tracking')->checkTextInput() ?>
    </div>
    <div class="col-md-3">
        <?= $form->field($reportForm->model, 'cell_delivery_cost_percent')->checkTextInput() ?>
    </div>
    <div class="col-md-3">
        <?= $form->field($reportForm->model, 'cell_fulfillment_cost')->checkTextInput() ?>
    </div>
</div>
<div class="row">
    <div class="col-md-3">
        <?= $form->field($reportForm->model, 'cell_nds')->checkTextInput() ?>
    </div>
    <div class="col-md-3">
        <?= $form->field($reportForm->model, 'cell_fulfillment_nds')->checkTextInput() ?>
    </div>
</div>
<div class="row">
    <div class="col-md-3">
        <?= $form->field($reportForm->model, 'cell_delivery_price')->checkTextInput() ?>
    </div>
    <div class="col-md-3">
        <?= $form->field($reportForm->model, 'cell_total_price')->checkTextInput() ?>
    </div>
    <div class="col-md-3">
        <?= $form->field($reportForm->model, 'cell_status')->checkTextInput() ?>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <?= InputGroupFile::widget([
            'right' => false,
            'input' => InputText::widget([
                'type' => 'file',
                'name' => Html::getInputName($reportForm, 'loadFile'),
                'id' => 'loadFile',
            ]),
        ]) ?>
    </div>
    <div class="col-md-8">
        <?= Html::submitButton(Yii::t('common', 'Отправить'), ['class' => 'btn btn-success']) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

<div class="row-with-text-progress margin-top-15" style="display: none;">
    <h5>
        <?= Yii::t('common', 'Обработка файла') ?>
        <span class="pull-right builder-percent">0%</span>
    </h5>
    <?= ProgressBar::widget() ?>
</div>

<div class="row-with-text-finish" style="display: none;">
    <br>
    <br>
    <div class="margin-bottom-15 text-center">
        <a href="#" class="finish-link"><?=Yii::t('common', 'Обработка завершена, перейти к деталям')?></a>
    </div>
</div>

</div>
