<?php
use app\modules\report\extensions\GridView;
use app\modules\report\components\ReportFormValidateFinance;
use app\widgets\LinkPager;
use app\widgets\ButtonLink;
use app\components\widgets\ActiveForm;
use app\modules\order\models\OrderStatus;
use yii\helpers\Url;
use yii\helpers\Html;

/** @var app\modules\report\components\ReportFormValidateFinance $reportForm */
/** @var yii\data\ArrayDataProvider $dataHistory */
/** @var app\modules\report\models\ReportValidateFinance $model */
/** @var array $sumsArray */
?>

<div class="row">


    <div class="col-md-4">
        <div class="h4"><?= Yii::t('common', 'Итоги') ?></div>


        <table class="table table-report table-striped table-hover table-bordered">
            <tr>
                <th></th>
                <th><?= Yii::t('common', 'База') ?></th>
                <th><?= Yii::t('common', 'Файл') ?></th>
            </tr>
            <tr>
                <td><?= Yii::t('common', 'Итого по проценту от заказа для КС') ?></td>
                <td align="right"><?= $sumsArray['sum_delivery_cost_percent'] ?></td>
                <td align="right"><?= $sumsArray['sum_delivery_cost_percent_file'] ?></td>
            </tr>
            <tr>
                <td><?= Yii::t('common', 'Итого по cтоимости фулфиллмента') ?></td>
                <td align="right"><?= $sumsArray['sum_fulfillment_cost'] ?></td>
                <td align="right"><?= $sumsArray['sum_fulfillment_cost_file'] ?></td>
            </tr>
            <tr>
                <td><?= Yii::t('common', 'Итого по НДС на доставку') ?></td>
                <td align="right"><?= $sumsArray['sum_nds'] ?></td>
                <td align="right"><?= $sumsArray['sum_nds_file'] ?></td>
            </tr>
            <tr>
                <td><?= Yii::t('common', 'Итого по НДС на фулфиллмент') ?></td>
                <td align="right"><?= $sumsArray['sum_fulfillment_nds'] ?></td>
                <td align="right"><?= $sumsArray['sum_fulfillment_nds_file'] ?></td>
            </tr>
            <tr>
                <td><?= Yii::t('common', 'Итого по стоимости доставки') ?></td>
                <td align="right"><?= $sumsArray['sum_delivery_price'] ?></td>
                <td align="right"><?= $sumsArray['sum_delivery_price_file'] ?></td>
            </tr>
            <tr>
                <td><?= Yii::t('common', 'Итого по общей сумме') ?></td>
                <td align="right"><?= $sumsArray['sum_total_price'] ?></td>
                <td align="right"><?= $sumsArray['sum_total_price_file'] ?></td>
            </tr>
            <tr>
                <td><?= Yii::t('common', 'Всего предупреждений') ?></td>
                <td colspan="2" align="center"><?= $sumsArray['count_warnings'] ?></td>
            </tr>
            <tr>
                <td><?= Yii::t('common', 'Всего ошибок') ?></td>
                <td colspan="2" align="center"><?= $sumsArray['count_errors'] ?></td>
            </tr>
        </table>
    </div>


    <div class="col-md-6">

        <?php if ($model->cell_status): ?>

            <?php
            $form = ActiveForm::begin([
                'action' => Url::to(['validate-finance/status/' . $model->id]),
                'method' => 'post',
            ]); ?>

            <div class="h4"><?= Yii::t('common', 'Соответсвие статусов в файле и наших') ?></div>

            <?php
            $statusMap = json_decode($model->status_map, true);

            $statuses = OrderStatus::find()->collection();

            if (isset($statusMap['fileStatus'])) {

                $i = 0;
                foreach ($statusMap['fileStatus'] as $fileStatus):?>

                    <div class="row">
                        <div class="col-md-4">
                            <?= ($i + 1) . '. ' . $fileStatus ?>
                        </div>
                        <div class="col-md-8">
                            <?= \app\widgets\Select2::widget([
                                'name' => 'satatus[' . $i . ']',
                                'items' => $statuses,
                                'value' => $statusMap['map'][$fileStatus] ?? 0,
                                'prompt' => Yii::t('common', 'Выберите статус')
                            ]) ?>
                        </div>
                    </div>
                    <?php $i++;
                endforeach;
            } else {
                echo Yii::t('common', 'Статусы в файле не найдены');
            }

            ?>
            <br>
            <div>
                <?= Html::submitButton(Yii::t('common', 'Сверить статусы'), ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        <?php endif; ?>
    </div>

    <div class="col-md-2">
        <div class="pull-right">
            <?= Html::a(Yii::t('common', 'Назад'), \yii\helpers\Url::toRoute('/report/validate-finance/index')) ?>
        </div>
    </div>

</div>

<div id="report_export_to_excel" style="margin-right: 210px; position: absolute; right: 20px; top: 15px;">
    <?php echo ButtonLink ::widget([
        "id" => "btn_export_report_to_excel",
        "url" => Url::toRoute(['validate-finance/export-to-excel', 'type' => 'correct'] + Yii::$app->request->queryParams),
        "style" => ButtonLink::STYLE_DEFAULT . " width-200",
        "size" => ButtonLink::SIZE_SMALL,
        "label" => Yii::t("common", "Экспорт в Excel (без ошибок)"),
    ]); ?>
</div>

<div id="report_export_to_excel" style="position: absolute; right: 20px; top: 15px;">
    <?php echo ButtonLink ::widget([
        "id" => "btn_export_report_to_excel",
        "url" => Url::toRoute(['validate-finance/export-to-excel', 'type' => 'error'] + Yii::$app->request->queryParams),
        "style" => ButtonLink::STYLE_DEFAULT . " width-200",
        "size" => ButtonLink::SIZE_SMALL,
        "label" => Yii::t("common", "Экспорт в Excel (ошибки)"),
    ]); ?>
</div>

<div class="table-responsive margin-top-20">
    <?= GridView::widget([
        'layout' => '<div id="report_export_box">{toolbar}</div>{items}<div class="panel-footer clearfix  margin-top-15  padding-right-0">{pager}</div>',
        'dataProvider' => $dataHistory,
        'reportForm' => $reportForm,
        'resizableColumns' => false,
        'showPageSummary' => false,
        'export' => false,
        'tableOptions' => [
            'class' => 'table table-report table-striped table-hover table-bordered',
        ],
        'rowOptions' => function ($model) {
            return ReportFormValidateFinance::check($model);
        },
        'pager' => [
            'class' => 'app\widgets\LinkPager',
            'counterPosition' => LinkPager::COUNTER_POSITION_RIGHT,
        ],
        'columns' => [
            [
                'label' => Yii::t('common', 'Номер заказа'),
                'attribute' => 'order_id',
            ],
            [
                'label' => Yii::t('common', 'Номер заказа (файл)'),
                'attribute' => 'order_id_file',
            ],
            [
                'label' => Yii::t('common', 'Трек номер'),
                'attribute' => 'tracking',
            ],
            [
                'label' => Yii::t('common', 'Трек номер (файл)'),
                'attribute' => 'tracking_file',
            ],
            [
                'label' => Yii::t('common', 'Процент от заказа для КС'),
                'attribute' => 'delivery_cost_percent',
                'visible' => $model->cell_delivery_cost_percent
            ],
            [
                'label' => Yii::t('common', 'Процент от заказа для КС (файл)'),
                'attribute' => 'delivery_cost_percent_file',
                'visible' => $model->cell_delivery_cost_percent
            ],
            [
                'label' => Yii::t('common', 'Стоимость фулфиллмента'),
                'attribute' => 'fulfillment_cost',
                'visible' => $model->cell_fulfillment_cost
            ],
            [
                'label' => Yii::t('common', 'Стоимость фулфиллмента (файл)'),
                'attribute' => 'fulfillment_cost_file',
                'visible' => $model->cell_fulfillment_cost
            ],
            [
                'label' => Yii::t('common', 'НДС на доставку'),
                'attribute' => 'nds',
                'visible' => $model->cell_nds
            ],
            [
                'label' => Yii::t('common', 'НДС на доставку (файл)'),
                'attribute' => 'nds_file',
                'visible' => $model->cell_nds
            ],
            [
                'label' => Yii::t('common', 'НДС на фулфиллмент'),
                'attribute' => 'fulfillment_nds',
                'visible' => $model->cell_fulfillment_nds
            ],
            [
                'label' => Yii::t('common', 'НДС на фулфиллмент (файл)'),
                'attribute' => 'fulfillment_nds_file',
                'visible' => $model->cell_fulfillment_nds
            ],
            [
                'label' => Yii::t('common', 'Стоимость доставки'),
                'attribute' => 'delivery_price',
                'visible' => $model->cell_delivery_price
            ],
            [
                'label' => Yii::t('common', 'Стоимость доставки (файл)'),
                'attribute' => 'delivery_price_file',
                'visible' => $model->cell_delivery_price
            ],
            [
                'label' => Yii::t('common', 'Общая сумма'),
                'attribute' => 'total_price',
                'visible' => $model->cell_total_price,
            ],
            [
                'label' => Yii::t('common', 'Общая сумма (файл)'),
                'attribute' => 'total_price_file',
                'visible' => $model->cell_total_price,
            ],
            [
                'label' => Yii::t('common', 'Статус'),
                'attribute' => 'status',
                'visible' => $model->cell_status,
            ],
            [
                'label' => Yii::t('common', 'Статус (файл)'),
                'attribute' => 'status_file',
                'visible' => $model->cell_status,
            ],
            [
                'label' => Yii::t('common', 'Текст ошибки'),
                'attribute' => 'error',
                'value' => function ($line) {
                    return trim($line['warning'] . ' ' . $line['error']);
                },
            ],
        ]
    ]) ?>
</div>
