<?php
use app\modules\report\assets\ReportAsset;
use app\modules\report\widgets\ReportFilterDeliveryPayment;
use app\widgets\LinkPager;
use app\widgets\Panel;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormDeliveryPayment $reportForm */
/** @var \yii\data\ArrayDataProvider $dataProvider */

$this->title = Yii::t('common', 'Поступления ДС');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Отчеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ReportAsset::register($this);
?>

<?= ReportFilterDeliveryPayment::widget([
    'reportForm' => $reportForm,
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Поступления ДС'),
    'border' => false,
    'withBody' => false,
    'content' => $this->render('_index-content', [
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
    ]),
    'footer' => LinkPager::widget([
            'pagination' => $dataProvider->getPagination(),
            'counterPosition' => LinkPager::COUNTER_POSITION_RIGHT
        ]),
]) ?>
