<?php
use app\modules\report\extensions\GridView;
use \app\models\Currency;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormDeliveryPayment $reportForm */
/** @var \yii\data\ArrayDataProvider $dataProvider */
?>

<div class="table-responsive">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'resizableColumns' => false,
        'showPageSummary' => false,
        'layout' => '<div id="report_export_box">{export}</div>{items}',
        'tableOptions' => ['class' => 'table table-striped stick'],
        'columns' => [
            [
                'attribute' => 'delivery_id',
                'label' => Yii::t('common', 'ID КС'),
            ],
            [
                'attribute' => 'paid_at',
                'label' => Yii::t('common', 'Дата поступления ДС'),
                'value' => function ($model) {
                    return Yii::$app->formatter->asDate($model['paid_at']);
                }
            ],
            [
                'attribute' => 'receiving_bank',
                'label' => Yii::t('common', 'Банк-отправитель'),
            ],
            [
                'attribute' => 'payer',
                'label' => Yii::t('common', 'Плательщик'),
            ],
            [
                'attribute' => 'bank_recipient',
                'label' => Yii::t('common', 'Банк-получатель'),
            ],
            [
                'label' => Yii::t('common', 'Название КС'),
                'value' => function ($model) {
                    /** @var $model \app\modules\deliveryreport\models\PaymentOrder */
                    return $model->delivery->name;
                }
            ],
            [
                'label' => Yii::t('common', 'Группа КС'),
                'value' => function ($model) {
                    /** @var $model \app\modules\deliveryreport\models\PaymentOrder */
                    return $model->delivery->group_name;
                }
            ],
            [
                'label' => Yii::t('common', 'Страна КС'),
                'value' => function ($model) {
                    /** @var $model \app\modules\deliveryreport\models\PaymentOrder */
                    return $model->delivery->country->name;
                }
            ],
            [
                'attribute' => 'name',
                'label' => Yii::t('common', 'Назначение платежа'),
            ],
            [
                'attribute' => 'currency_id',
                'label' => Yii::t('common', 'Валюта платежа'),
                'value' => function ($model) {
                    /** @var $model \app\modules\deliveryreport\models\PaymentOrder */
                    return $model->currency_id ? $model->currency->char_code : '';
                }
            ],
            [
                'attribute' => 'sum',
                'label' => Yii::t('common', 'Сумма платежа (в валюте платежа)'),
                'hAlign' => 'right',
                'format' => ['decimal', 2],
            ],
            [
                'attribute' => 'rate',
                'label' => Yii::t('common', 'Курс USD на дату поступления ДС'),
                'hAlign' => 'right',
                'value' => function ($model) {
                    /** @var $model \app\modules\deliveryreport\models\PaymentOrder */
                    $from = $model->currency_id;
                    $to = Currency::getUSD()->id;
                    if (!($rate = $model->getRate($from, $to))) {
                        if (!($rate = Currency::find()->convertValueToCurrency(1, $to, $from, $model->paid_at))) {
                            $rate = null;
                        }
                    }
                    if ($rate) {
                        $rate = round($rate, 4);
                    }
                    return $rate ?? '-';
                }
            ],
            [
                'attribute' => 'income',
                'label' => Yii::t('common', 'Сумма платежа в USD'),
                'hAlign' => 'right',
                'format' => ['decimal', 2],
                'value' => function ($model) {
                    /** @var $model \app\modules\deliveryreport\models\PaymentOrder */
                    $from = $model->currency_id;
                    $to = Currency::getUSD()->id;
                    if (!($rate = $model->getRate($from, $to))) {
                        if (!($rate = Currency::find()->convertValueToCurrency(1, $to, $from, $model->paid_at))) {
                            $rate = null;
                        }
                    }
                    if ($rate) {
                        return round($model->sum / round($rate, 4), 4);
                    }
                    return '-';
                }
            ],
        ]
    ]) ?>
</div>
