<?php
use app\modules\report\assets\ReportAsset;
use app\modules\report\widgets\ReportFilterForecastWorkloadOperatorsByHours;
use app\widgets\Nav;
use app\widgets\Panel;
use yii\web\View;


/** @var app\modules\report\components\ReportFormAdvertising $reportForm */
/** @var yii\data\ArrayDataProvider $dataProvider */
/** @var yii\data\ArrayDataProvider $daysProvider */

$this->title = Yii::t('common', 'Прогноз загруженности операторов по часам');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Отчеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

$this->registerJs("var I18n = {};", View::POS_HEAD);
ReportAsset::register($this);
?>

<?=ReportFilterForecastWorkloadOperatorsByHours::widget([
    'reportForm' => $reportForm,
]);?>


<?=Panel::widget([
    'nav' => new Nav([
        'tabs' => [
            [
                'label' => Yii::t('common', 'График'),
                'content' => $this->render('_tab-graph', [
                    'dataProvider' => $dataProvider,
                    'reportForm' => $reportForm,
                ]),
            ],
            [
                'label' => Yii::t('common', 'Таблица'),
                'content' => $this->render('_tab-table', [
                    'dataProvider' => $dataProvider,
                    'reportForm' => $reportForm,
                ]),
            ],

        ],
    ]),
]);?>
