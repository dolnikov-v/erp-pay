<?php
use app\modules\report\widgets\GraphForecastWorkloadOperatorsByHours;

/** @var \yii\web\View $this */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */
?>

<div class="table-responsive">
    <?=GraphForecastWorkloadOperatorsByHours::widget([
        'dataProvider' => $dataProvider,
        'reportForm' => $reportForm
    ]);?>
</div>