<?php

use app\components\grid\DateColumn;
use app\helpers\DataProvider;
use app\modules\report\extensions\GridView;
use app\modules\report\widgets\ReportFilterFindAdcombo;
use app\widgets\LinkPager;

/** @var \app\modules\report\components\ReportFormFindAdcombo $findAdcomboForm */
/** @var \yii\data\ArrayDataProvider $adcomboOrderProvider */

?>

<?= ReportFilterFindAdcombo::widget(['reportForm' => $findAdcomboForm]) ?>

<div class="table-responsive margin-top-20">
    <?= DataProvider::renderSummary($adcomboOrderProvider, [
        'class' => 'summary text-right width-full'
    ]) ?>
    <?= GridView::widget([
        'dataProvider' => $adcomboOrderProvider,
        'resizableColumns' => false,
        'showPageSummary' => false,
        'export' => false,
        'rowOptions' => [
            'class' => 'text-center'
        ],
        'columns' => [
            [
                'header' => Yii::t('common', 'Номер заказа'),
                'attribute' => 'order.id'
            ],
            [
                'header' => Yii::t('common', 'Внешний номер'),
                'attribute' => 'foreign_id'
            ],
            [
                'header' => Yii::t('common', 'Имя покупателя (AdCombo)'),
                'attribute' => 'name'
            ],
            [
                'header' => Yii::t('common', 'Номер телефона (AdCombo)'),
                'attribute' => 'phone'
            ],
            [
                'header' => Yii::t('common', 'Статус (AdCombo)'),
                'attribute' => 'stateName'
            ],
            [
                'class' => DateColumn::className(),
                'header' => Yii::t('common', 'Дата создания (AdCombo)'),
                'attribute' => 'created_at',
                'formatType' => 'Datetime'
            ],
            [
                'header' => Yii::t('common', 'Имя покупателя'),
                'attribute' => 'order.customer_full_name'
            ],
            [
                'header' => Yii::t('common', 'Номер телефона'),
                'attribute' => 'order.customer_phone'
            ],
            [
                'header' => Yii::t('common', 'Статус'),
                'attribute' => 'orderStateName'
            ],
            [
                'class' => DateColumn::className(),
                'header' => Yii::t('common', 'Дата создания'),
                'attribute' => 'order.created_at',
                'formatType' => 'Datetime'
            ],
        ]
    ]) ?>
</div>

<span class="panel-footer clearfix">
        <?= LinkPager::widget([
            'pagination' => $adcomboOrderProvider->getPagination(),
            'counterPosition' => LinkPager::COUNTER_POSITION_RIGHT
        ]) ?>
</span>
