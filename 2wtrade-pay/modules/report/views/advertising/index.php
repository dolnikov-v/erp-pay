<?php
use app\modules\report\assets\ReportAsset;
use app\modules\report\assets\ReportAdversitingAsset;
use app\widgets\Panel;
use app\widgets\Nav;

/** @var app\modules\report\components\ReportFormAdvertising $reportForm */
/** @var yii\data\ArrayDataProvider $dataProvider */
/** @var yii\data\ArrayDataProvider $dataHistory */
/** @var yii\data\ArrayDataProvider $daysProvider */
/** @var integer $day */
/** @var \app\modules\report\components\ReportFormFindAdcombo $findAdcomboForm */
/** @var \yii\data\ArrayDataProvider $adcomboOrderProvider */

$this->title = Yii::t('common', 'Затраты на рекламу');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Отчеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ReportAsset::register($this);
ReportAdversitingAsset::register($this);
?>

<?= Panel::widget([
    'nav' => new Nav([
        'tabs' => [
            [
                'label' => Yii::t('common', 'Текущий месяц'),
                'content' => $this->render('_tab-current', [
                    'reportForm' => $reportForm,
                ]),
            ],
            [
                'label' => Yii::t('common', 'История загрузок'),
                'content' => $this->render('_tab-history', [
                    'reportForm' => $reportForm,
                    'dataProvider' => $daysProvider,
                    'dataHistory' => $dataHistory,
                    'day' => $day,
                    'labels' => $labels,
                ]),
            ],
            [
                'label' => Yii::t('common', 'Загрузка файла'),
                'content' => $this->render('_tab-load', [
                    'reportForm' => $reportForm,
                    'dataProvider' => $dataProvider,
                    'labels' => $labels,
                ]),
            ],
            [
                'label' => Yii::t('common', 'Поиск в AdCombo'),
                'content' => $this->render('_tab-find-adcombo', [
                    'findAdcomboForm' => $findAdcomboForm,
                    'adcomboOrderProvider' => $adcomboOrderProvider
                ])
            ]
        ]
    ]),
]) ?>
