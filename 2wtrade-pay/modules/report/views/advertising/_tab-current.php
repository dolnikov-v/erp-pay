<?php
use app\modules\report\components\ReportFormAdvertising;
use app\modules\report\extensions\GridView;

?>

<div class="table-responsive">
    <?= GridView::widget([
        'dataProvider' => ReportFormAdvertising::getDataCurrent(),
        'resizableColumns' => false,
        'showPageSummary' => false,
        'export' => false,
        'layout' => '{items}',
        'columns' => [
            ['attribute' => 'country_name', 'label' => Yii::t('common', 'Страна')],
            [
                'attribute' => 'summa',
                'label' => Yii::t('common', 'Сумма') . ', USD',
                'format' => ['decimal', 2],
                'visible' => Yii::$app->user->can('view_finance_director')
            ],
        ]
    ]) ?>
</div>
