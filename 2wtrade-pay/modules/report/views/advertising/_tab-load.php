<?php
use app\modules\report\components\ReportFormAdvertising;
use app\modules\report\extensions\GridView;

/** @var app\modules\report\components\ReportFormAdvertising $reportForm */
/** @var yii\data\ArrayDataProvider $dataProvider */
?>

<?= $this->render('_load-form', [
    'reportForm' => $reportForm,
]) ?>

<?php if ($dataProvider): ?>
    <div class="table-responsive margin-top-20">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'resizableColumns' => false,
            'showPageSummary' => false,
            'export' => false,
            'rowOptions' => function ($model) {
                return ReportFormAdvertising::check($model);
            },
            'columns' => [
                ['attribute' => 'foreign_id', 'label' => $labels['foreign_id']],
                ['attribute' => 'country_id', 'label' => $labels['country_code']],
                ['attribute' => 'created_at', 'label' => $labels['created_at']],
                ['attribute' => 'status_id', 'label' => $labels['status_id']],
                ['attribute' => 'price', 'label' => $labels['price']],
                ['attribute' => 'foreign_id_file', 'label' => $labels['foreign_id_file']],
                ['attribute' => 'country_id_file', 'label' => $labels['country_code_file']],
                ['attribute' => 'created_at_file', 'label' => $labels['created_at_file']],
                ['attribute' => 'status_id_file', 'label' => $labels['status_id_file']],
                ['attribute' => 'price_file', 'label' => $labels['price_file']],
                ['attribute' => 'difference', 'label' => $labels['difference']],
                ['attribute' => 'error', 'visible' => false],
            ]
        ]) ?>
    </div>
<?php endif; ?>
