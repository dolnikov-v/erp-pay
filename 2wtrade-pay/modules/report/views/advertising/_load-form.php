<?php
use app\widgets\InputGroupFile;
use app\widgets\InputText;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/** @var app\modules\report\components\ReportFormAdvertising $reportForm */

$form = ActiveForm::begin([
    'options' => [
        'enctype' => 'multipart/form-data',
    ],
    'action' => Url::to(['advertising/index']).'#tab2',
    'method' => 'post',
    'enableClientValidation' => false,
]); ?>

<div class="row">
    <div class="col-md-4">
        <?= InputGroupFile::widget([
            'right' => false,
            'input' => InputText::widget([
                'type' => 'file',
                'name' => Html::getInputName($reportForm, 'loadFile'),
            ]),
        ]) ?>
    </div>
    <div class="col-md-8">
        <?= Html::submitButton(Yii::t('common', 'Отправить'), ['class' => 'btn btn-success']) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
