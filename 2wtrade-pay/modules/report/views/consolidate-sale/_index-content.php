<?php
use app\modules\report\extensions\GridViewConsolidateSale;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormConsolidateSale $reportForm */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */
?>

<div class="table-responsive">
    <?= GridViewConsolidateSale::widget([
        'id' => 'report_grid_finance',
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
        'showPageSummary' => false,
        'resizableColumns' => false
    ]) ?>
</div>
