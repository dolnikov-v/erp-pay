<?php
use app\modules\report\assets\ReportAsset;
use app\modules\report\widgets\ReportFilterConsolidateSale;
use app\widgets\Panel;
use app\modules\report\assets\ReportConsolidateSaleAsset;
/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormConsolidateSale $reportForm */
/** @var yii\data\ArrayDataProvider $dataProvider */

$this->title = Yii::t('common', 'Сводный отчёт по продажам');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Отчеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ReportAsset::register($this);
ReportConsolidateSaleAsset::register($this);
?>


<?=ReportFilterConsolidateSale::widget([
    'reportForm' => $reportForm,
]);?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Сводный отчёт по продажам'),
    'alert' => $reportForm->panelAlert,
    'alertStyle' => $reportForm->panelAlertStyle,
    'border' => false,
    'withBody' => false,
    'content' => $this->render('_index-content', [
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
    ]),
]) ?>