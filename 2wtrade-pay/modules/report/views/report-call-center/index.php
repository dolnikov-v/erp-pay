<?php

use app\modules\report\assets\ReportCallCenterAsset;
use app\modules\report\widgets\ActionNav;
use app\modules\report\widgets\ReportFilterCallCenterCompare;
use app\widgets\Panel;
use app\helpers\DataProvider;
use app\modules\report\assets\ActionNavAsset;

/**@var $reportForm \app\modules\report\components\ReportFormCallCenterCompare */
/**@var $dataProvider \yii\data\ActiveDataProvider */
/**@var $this \yii\web\View */

$this->title = Yii::t('common', 'Сверка с КЦ');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Отчеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ReportCallCenterAsset::register($this);
ActionNavAsset::register($this);

?>

<?= ReportFilterCallCenterCompare::widget([
    'reportForm' => $reportForm
]) ?>

<div class="report-call-center-index">
    <?= Panel::widget([
        'nav' => new ActionNav([
            'tabs' => [
                [
                    'label' => Yii::t('common', 'Логи'),
                    'action' => 'index'
                ],
                [
                    'label' => Yii::t('common', 'Поиск в КЦ'),
                    'action' => 'find-in-call-center'
                ],
            ],
            'actions' => DataProvider::renderSummary($dataProvider)
        ]),
        'content' => $this->render('_index-content', ['dataProvider' => $dataProvider])
    ]) ?>
</div>
