<?php
use app\components\grid\GridView;
use app\components\grid\DateColumn;
use app\widgets\Label;
use yii\bootstrap\Html;
use yii\helpers\Url;
use app\modules\report\widgets\ReportCallCenterShowerColumns;
use app\components\grid\ActionColumn;
use app\components\Notifier;

/**@var $dataProvider \yii\data\ArrayDataProvider */
/** @var $showerColumns \app\modules\report\widgets\ReportCallCenterShowerColumns */
/**@var $this \yii\web\View */

?>

<?php
// Уведомление об ручном обновлении статуса
if (Yii::$app->session->has('income-from-update-manually')) {
    $res = Yii::$app->session->get('income-from-update-manually');
    if (mb_strlen($res['result']['result_text']) > 100) {
        $res['result']['result_text'] = mb_substr($res['result']['result_text'], 0, 100);
    }
    $res['result']['result_text'] = str_replace(array("\r","\n"), " ", $res['result']['result_text']); //страхуемся от переносов строк

    Yii::$app->notifier->addNotification($res['result']['result_text'], $res['result']['result_type']);
    unset($_SESSION['income-from-update-manually']);
}
?>

<div id="records_content">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => [
            'class' => 'text-center'
        ],
        'columns' => [
            [
                'headerOptions' => ['class' => 'text-center'],
                'header' => Yii::t('common', 'Номер заказа'),
                'attribute' => 'order_id',
                'content' => function ($item) {
                    return Html::a($item->order_id, Url::toRoute([
                        '/order/index/index',
                        'NumberFilter' => [
                            'number' => $item->order_id,
                            'entity' => 'id'
                        ]
                    ]));
                }
            ],
            [
                'headerOptions' => ['class' => 'text-center'],
                'header' => Yii::t('common', 'Внешний номер'),
                'attribute' => 'order.callCenterRequest.foreign_id'
            ],
            [
                'headerOptions' => ['class' => 'text-center'],
                'header' => Yii::t('common', 'Внешний номер (КЦ)'),
                'attribute' => 'foreign_id'
            ],
            [
                'headerOptions' => ['class' => 'text-center'],
                'header' => Yii::t('common', 'Полное имя заказчика (КЦ)'),
                'attribute' => 'name',
                'visible' => $showerColumns->isChosenColumn(ReportCallCenterShowerColumns::COLUMN_CUSTOMER_FULL_NAME),
            ],
            [
                'headerOptions' => ['class' => 'text-center'],
                'header' => Yii::t('common', 'Телефон заказчика (КЦ)'),
                'attribute' => 'phone',
                'visible' => $showerColumns->isChosenColumn(ReportCallCenterShowerColumns::COLUMN_CUSTOMER_PHONE),
            ],
            [
                'headerOptions' => ['class' => 'text-center'],
                'header' => Yii::t('common', 'Адрес заказчика (КЦ)'),
                'attribute' => 'address',
                'visible' => $showerColumns->isChosenColumn(ReportCallCenterShowerColumns::COLUMN_CUSTOMER_ADDRESS),
            ],
            [
                'headerOptions' => ['class' => 'text-center'],
                'header' => Yii::t('common', 'Город заказчика (КЦ)'),
                'attribute' => 'customerCity',
                'visible' => $showerColumns->isChosenColumn(ReportCallCenterShowerColumns::COLUMN_CUSTOMER_CITY),
            ],
            [
                'headerOptions' => ['class' => 'text-center'],
                'header' => Yii::t('common', 'Провинция заказчика (КЦ)'),
                'attribute' => 'customerProvince',
                'visible' => $showerColumns->isChosenColumn(ReportCallCenterShowerColumns::COLUMN_CUSTOMER_PROVINCE),
            ],
            [
                'headerOptions' => ['class' => 'text-center'],
                'header' => Yii::t('common', 'Улица заказчика (КЦ)'),
                'attribute' => 'customerStreet',
                'visible' => $showerColumns->isChosenColumn(ReportCallCenterShowerColumns::COLUMN_CUSTOMER_STREET_HOUSE),
            ],
            [
                'headerOptions' => ['class' => 'text-center'],
                'header' => Yii::t('common', 'Статус (КЦ)'),
                'attribute' => 'statusName',
                'visible' => $showerColumns->isChosenColumn(ReportCallCenterShowerColumns::COLUMN_STATUS),
            ],
            [
                'headerOptions' => ['class' => 'text-center'],
                'header' => Yii::t('common', 'Товары (КЦ)'),
                'content' => function ($item) {
                    $content = '';
                    foreach ($item->orderProducts as $prod) {
                        $content .= Label::widget([
                            'label' => "{$prod->product->name} - {$prod->quantity}",
                            'style' => Label::STYLE_INFO
                        ]);
                        $content .= Html::tag('br');
                    }
                    return $content;
                },
                'visible' => $showerColumns->isChosenColumn(ReportCallCenterShowerColumns::COLUMN_PRODUCTS),
            ],
            [
                'headerOptions' => ['class' => 'text-center'],
                'header' => Yii::t('common', 'Конечная цена (КЦ)'),
                'attribute' => 'price',
                'visible' => $showerColumns->isChosenColumn(ReportCallCenterShowerColumns::COLUMN_PRICE),
            ],
            [
                'headerOptions' => ['class' => 'text-center'],
                'class' => DateColumn::className(),
                'header' => Yii::t('common', 'Дата изменения (КЦ)'),
                'attribute' => 'last_update',
                'formatType' => 'Datetime',
                'visible' => $showerColumns->isChosenColumn(ReportCallCenterShowerColumns::COLUMN_UPDATED_AT),
            ],
            [
                'headerOptions' => ['class' => 'text-center'],
                'header' => Yii::t('common', 'Комментарий (КЦ)'),
                'attribute' => 'comment',
                'visible' => $showerColumns->isChosenColumn(ReportCallCenterShowerColumns::COLUMN_CALL_CENTER_COMMENT),
            ],
            [
                'headerOptions' => ['class' => 'text-center'],
                'header' => Yii::t('common', 'Полное имя заказчика'),
                'attribute' => 'order.customer_full_name',
                'visible' => $showerColumns->isChosenColumn(ReportCallCenterShowerColumns::COLUMN_CUSTOMER_FULL_NAME),
            ],
            [
                'headerOptions' => ['class' => 'text-center'],
                'header' => Yii::t('common', 'Телефон заказчика'),
                'attribute' => 'order.customer_phone',
                'visible' => $showerColumns->isChosenColumn(ReportCallCenterShowerColumns::COLUMN_CUSTOMER_PHONE),
            ],
            [
                'headerOptions' => ['class' => 'text-center'],
                'header' => Yii::t('common', 'Адрес заказчика'),
                'attribute' => 'order.customer_address',
                'visible' => $showerColumns->isChosenColumn(ReportCallCenterShowerColumns::COLUMN_CUSTOMER_ADDRESS),
            ],
            [
                'headerOptions' => ['class' => 'text-center'],
                'header' => Yii::t('common', 'Город заказчика'),
                'attribute' => 'order.customer_city',
                'visible' => $showerColumns->isChosenColumn(ReportCallCenterShowerColumns::COLUMN_CUSTOMER_CITY),
            ],
            [
                'headerOptions' => ['class' => 'text-center'],
                'header' => Yii::t('common', 'Провинция заказчика'),
                'attribute' => 'order.customer_province',
                'visible' => $showerColumns->isChosenColumn(ReportCallCenterShowerColumns::COLUMN_CUSTOMER_PROVINCE),
            ],
            [
                'headerOptions' => ['class' => 'text-center'],
                'header' => Yii::t('common', 'Улица заказчика'),
                'attribute' => 'order.customer_street',
                'visible' => $showerColumns->isChosenColumn(ReportCallCenterShowerColumns::COLUMN_CUSTOMER_STREET_HOUSE),
            ],
            [
                'headerOptions' => ['class' => 'text-center'],
                'header' => Yii::t('common', 'Статус'),
                'attribute' => 'orderStateName',
                'visible' => $showerColumns->isChosenColumn(ReportCallCenterShowerColumns::COLUMN_STATUS),
            ],
            [
                'headerOptions' => ['class' => 'text-center'],
                'header' => Yii::t('common', 'Товары'),
                'content' => function ($item) {
                    $content = '';
                    foreach ($item->order->orderProducts as $prod) {
                        $content .= Label::widget([
                            'label' => "{$prod->product->name} - {$prod->quantity}",
                            'style' => Label::STYLE_INFO
                        ]);
                        $content .= Html::tag('br');
                    }
                    return $content;
                },
                'visible' => $showerColumns->isChosenColumn(ReportCallCenterShowerColumns::COLUMN_PRODUCTS),
            ],
            [
                'headerOptions' => ['class' => 'text-center'],
                'header' => Yii::t('common', 'Конечная цена'),
                'attribute' => 'order.price_total',
                'visible' => $showerColumns->isChosenColumn(ReportCallCenterShowerColumns::COLUMN_PRICE),
            ],
            [
                'headerOptions' => ['class' => 'text-center'],
                'class' => DateColumn::className(),
                'header' => Yii::t('common', 'Дата изменения'),
                'attribute' => 'order.callCenterRequest.updated_at',
                'formatType' => 'Datetime',
                'visible' => $showerColumns->isChosenColumn(ReportCallCenterShowerColumns::COLUMN_UPDATED_AT),
            ],
            [
                'headerOptions' => ['class' => 'text-center'],
                'header' => Yii::t('common', 'Комментарий (КЦ)'),
                'attribute' => 'order.callCenterRequest.comment',
                'visible' => $showerColumns->isChosenColumn(ReportCallCenterShowerColumns::COLUMN_CALL_CENTER_COMMENT),
            ],
            'actions' => [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Просмотреть ответ КЦ'),
                        'url' => function () {
                            return '#';
                        },
                        'style' => 'show-call-center-response',
                        'attributes' => function ($model) {
                            return [
                                'data-id' => $model->order_id,
                            ];
                        },
                        'can' => function() {
                            return true;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Обновить'),
                        'url' => function ($item) {
                            if (!isset(Yii::$app->request->queryParams['query-from-session']))
                            {
                                $params = Yii::$app->request->queryParams;
                                Yii::$app->session->set('report-report-call-center-query-params', $params);
                            }
                            return Url::toRoute(['update-manually', 'id' => $item->order_id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('report.reportcallcenter.index');
                        },
                    ],
                ]
            ]
        ]
    ])
    ?>
</div>