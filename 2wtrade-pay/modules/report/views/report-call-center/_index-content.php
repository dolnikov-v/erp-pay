<?php

use app\components\grid\GridView;
use app\modules\report\models\ReportCallCenter;
use yii\bootstrap\Html;
use yii\helpers\Url;
use app\components\grid\ActionColumn;

/**@var $dataProvider \yii\data\ActiveDataProvider */
/**@var $this \yii\web\View */

?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'rowOptions' => function ($data) {
        $options = [
            'class' => 'tr-vertical-align-middle'
        ];
        $flag = false;
        foreach ($data->differenceArray as $diff) {
            if (intval($diff) > 0) {
                $flag = true;
                break;
            }
        }
        if ($flag) {
            $options['class'] .= ' has-error';
        }
        return $options;
    },
    'columns' => [
        [
            'header' => Yii::t('common', 'Период'),
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'content' => function ($model) {
                return Yii::$app->formatter->asDatetime($model->from) . " - " . Yii::$app->formatter->asDatetime($model->to);
            }
        ],
        [
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'attribute' => 'ccTotalCount'
        ],
        [
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'attribute' => 'cc_post_call_count',
            'content' => function ($item) {
                if (isset($item->differenceArray[ReportCallCenter::SHOW_ERROR_CC_POST_CALL]) && $item->differenceArray[ReportCallCenter::SHOW_ERROR_CC_POST_CALL] > 0) {
                    $link = Html::a($item->differenceArray[ReportCallCenter::SHOW_ERROR_CC_POST_CALL], Url::toRoute([
                        'show-errors',
                        'id' => $item->id,
                        'type' => ReportCallCenter::SHOW_ERROR_CC_POST_CALL
                    ]));
                    $content = "{$item->cc_post_call_count} (" . $link . ")";
                    return Html::tag('div', $content, ['class' => 'has-error']);
                } else {
                    return $item->cc_post_call_count;
                }
            }
        ],
        [
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'attribute' => 'cc_approved_count',
            'content' => function ($item) {
                if (isset($item->differenceArray[ReportCallCenter::SHOW_ERROR_CC_APPROVED]) && $item->differenceArray[ReportCallCenter::SHOW_ERROR_CC_APPROVED] > 0) {
                    $link = Html::a($item->differenceArray[ReportCallCenter::SHOW_ERROR_CC_APPROVED], Url::toRoute([
                        'show-errors',
                        'id' => $item->id,
                        'type' => ReportCallCenter::SHOW_ERROR_CC_APPROVED
                    ]));
                    $content = "{$item->cc_approved_count} (" . $link . ")";
                    return Html::tag('div', $content, ['class' => 'has-error']);
                } else {
                    return $item->cc_approved_count;
                }
            }
        ],
        [
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'attribute' => 'cc_fail_call_count',
            'content' => function ($item) {
                if (isset($item->differenceArray[ReportCallCenter::SHOW_ERROR_CC_FAIL_CALL]) && $item->differenceArray[ReportCallCenter::SHOW_ERROR_CC_FAIL_CALL] > 0) {
                    $link = Html::a($item->differenceArray[ReportCallCenter::SHOW_ERROR_CC_FAIL_CALL], Url::toRoute([
                        'show-errors',
                        'id' => $item->id,
                        'type' => ReportCallCenter::SHOW_ERROR_CC_FAIL_CALL
                    ]));
                    $content = "{$item->cc_fail_call_count} (" . $link . ")";
                    return Html::tag('div', $content, ['class' => 'has-error']);
                } else {
                    return $item->cc_fail_call_count;
                }
            }
        ],
        [
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'attribute' => 'cc_recall_count',
            'content' => function ($item) {
                if (isset($item->differenceArray[ReportCallCenter::SHOW_ERROR_CC_RECALL]) && $item->differenceArray[ReportCallCenter::SHOW_ERROR_CC_RECALL] > 0) {
                    $link = Html::a($item->differenceArray[ReportCallCenter::SHOW_ERROR_CC_RECALL], Url::toRoute([
                        'show-errors',
                        'id' => $item->id,
                        'type' => ReportCallCenter::SHOW_ERROR_CC_RECALL
                    ]));
                    $content = "{$item->cc_recall_count} (" . $link . ")";
                    return Html::tag('div', $content, ['class' => 'has-error']);
                } else {
                    return $item->cc_recall_count;
                }
            }
        ],
        [
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'attribute' => 'cc_rejected_count',
            'content' => function ($item) {
                if (isset($item->differenceArray[ReportCallCenter::SHOW_ERROR_CC_REJECTED]) && $item->differenceArray[ReportCallCenter::SHOW_ERROR_CC_REJECTED] > 0) {
                    $link = Html::a($item->differenceArray[ReportCallCenter::SHOW_ERROR_CC_REJECTED], Url::toRoute([
                        'show-errors',
                        'id' => $item->id,
                        'type' => ReportCallCenter::SHOW_ERROR_CC_REJECTED
                    ]));
                    $content = "{$item->cc_rejected_count} (" . $link . ")";
                    return Html::tag('div', $content, ['class' => 'has-error']);
                } else {
                    return $item->cc_rejected_count;
                }
            }
        ],
        [
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'attribute' => 'cc_double_count',
            'content' => function ($item) {
                if (isset($item->differenceArray[ReportCallCenter::SHOW_ERROR_CC_DOUBLE]) && $item->differenceArray[ReportCallCenter::SHOW_ERROR_CC_DOUBLE] > 0) {
                    $link = Html::a($item->differenceArray[ReportCallCenter::SHOW_ERROR_CC_DOUBLE], Url::toRoute([
                        'show-errors',
                        'id' => $item->id,
                        'type' => ReportCallCenter::SHOW_ERROR_CC_DOUBLE
                    ]));
                    $content = "{$item->cc_double_count} (" . $link . ")";
                    return Html::tag('div', $content, ['class' => 'has-error']);
                } else {
                    return $item->cc_double_count;
                }
            }
        ],
        [
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'attribute' => 'cc_trash_count',
            'content' => function ($item) {
                if (isset($item->differenceArray[ReportCallCenter::SHOW_ERROR_CC_TRASH]) && $item->differenceArray[ReportCallCenter::SHOW_ERROR_CC_TRASH] > 0) {
                    $link = Html::a($item->differenceArray[ReportCallCenter::SHOW_ERROR_CC_TRASH], Url::toRoute([
                        'show-errors',
                        'id' => $item->id,
                        'type' => ReportCallCenter::SHOW_ERROR_CC_TRASH
                    ]));
                    $content = "{$item->cc_trash_count} (" . $link . ")";
                    return Html::tag('div', $content, ['class' => 'has-error']);
                } else {
                    return $item->cc_trash_count;
                }
            }
        ],
        [
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'attribute' => 'ourTotalCount'
        ],
        [
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'attribute' => 'our_post_call_count',
            'content' => function ($item) {
                if (isset($item->differenceArray[ReportCallCenter::SHOW_ERROR_OUR_POST_CALL]) && $item->differenceArray[ReportCallCenter::SHOW_ERROR_OUR_POST_CALL] > 0) {
                    $link = Html::a($item->differenceArray[ReportCallCenter::SHOW_ERROR_OUR_POST_CALL], Url::toRoute([
                        'show-errors',
                        'id' => $item->id,
                        'type' => ReportCallCenter::SHOW_ERROR_OUR_POST_CALL
                    ]));
                    $content = "{$item->our_post_call_count} (" . $link . ")";
                    return Html::tag('div', $content, ['class' => 'has-error']);
                } else {
                    return $item->our_post_call_count;
                }
            }
        ],
        [
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'attribute' => 'our_approved_count',
            'content' => function ($item) {
                if (isset($item->differenceArray[ReportCallCenter::SHOW_ERROR_OUR_APPROVED]) && $item->differenceArray[ReportCallCenter::SHOW_ERROR_OUR_APPROVED] > 0) {
                    $link = Html::a($item->differenceArray[ReportCallCenter::SHOW_ERROR_OUR_APPROVED], Url::toRoute([
                        'show-errors',
                        'id' => $item->id,
                        'type' => ReportCallCenter::SHOW_ERROR_OUR_APPROVED
                    ]));
                    $content = "{$item->our_approved_count} (" . $link . ")";
                    return Html::tag('div', $content, ['class' => 'has-error']);
                } else {
                    return $item->our_approved_count;
                }
            }
        ],
        [
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'attribute' => 'our_fail_call_count',
            'content' => function ($item) {
                if (isset($item->differenceArray[ReportCallCenter::SHOW_ERROR_OUR_FAIL_CALL]) && $item->differenceArray[ReportCallCenter::SHOW_ERROR_OUR_FAIL_CALL] > 0) {
                    $link = Html::a($item->differenceArray[ReportCallCenter::SHOW_ERROR_OUR_FAIL_CALL], Url::toRoute([
                        'show-errors',
                        'id' => $item->id,
                        'type' => ReportCallCenter::SHOW_ERROR_OUR_FAIL_CALL
                    ]));
                    $content = "{$item->our_fail_call_count} (" . $link . ")";
                    return Html::tag('div', $content, ['class' => 'has-error']);
                } else {
                    return $item->our_fail_call_count;
                }
            }
        ],
        [
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'attribute' => 'our_recall_count',
            'content' => function ($item) {
                if (isset($item->differenceArray[ReportCallCenter::SHOW_ERROR_OUR_RECALL]) && $item->differenceArray[ReportCallCenter::SHOW_ERROR_OUR_RECALL] > 0) {
                    $link = Html::a($item->differenceArray[ReportCallCenter::SHOW_ERROR_OUR_RECALL], Url::toRoute([
                        'show-errors',
                        'id' => $item->id,
                        'type' => ReportCallCenter::SHOW_ERROR_OUR_RECALL
                    ]));
                    $content = "{$item->our_recall_count} (" . $link . ")";
                    return Html::tag('div', $content, ['class' => 'has-error']);
                } else {
                    return $item->our_recall_count;
                }
            }
        ],
        [
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'attribute' => 'our_rejected_count',
            'content' => function ($item) {
                if (isset($item->differenceArray[ReportCallCenter::SHOW_ERROR_OUR_REJECTED]) && $item->differenceArray[ReportCallCenter::SHOW_ERROR_OUR_REJECTED] > 0) {
                    $link = Html::a($item->differenceArray[ReportCallCenter::SHOW_ERROR_OUR_REJECTED], Url::toRoute([
                        'show-errors',
                        'id' => $item->id,
                        'type' => ReportCallCenter::SHOW_ERROR_OUR_REJECTED
                    ]));
                    $content = "{$item->our_rejected_count} (" . $link . ")";
                    return Html::tag('div', $content, ['class' => 'has-error']);
                } else {
                    return $item->our_rejected_count;
                }
            }
        ],
        [
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'attribute' => 'our_double_count',
            'content' => function ($item) {
                if (isset($item->differenceArray[ReportCallCenter::SHOW_ERROR_OUR_DOUBLE]) && $item->differenceArray[ReportCallCenter::SHOW_ERROR_OUR_DOUBLE] > 0) {
                    $link = Html::a($item->differenceArray[ReportCallCenter::SHOW_ERROR_OUR_DOUBLE], Url::toRoute([
                        'show-errors',
                        'id' => $item->id,
                        'type' => ReportCallCenter::SHOW_ERROR_OUR_DOUBLE
                    ]));
                    $content = "{$item->our_double_count} (" . $link . ")";
                    return Html::tag('div', $content, ['class' => 'has-error']);
                } else {
                    return $item->our_double_count;
                }
            }
        ],
        [
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'attribute' => 'our_trash_count',
            'content' => function ($item) {
                if (isset($item->differenceArray[ReportCallCenter::SHOW_ERROR_OUR_TRASH]) && $item->differenceArray[ReportCallCenter::SHOW_ERROR_OUR_TRASH] > 0) {
                    $link = Html::a($item->differenceArray[ReportCallCenter::SHOW_ERROR_OUR_TRASH], Url::toRoute([
                        'show-errors',
                        'id' => $item->id,
                        'type' => ReportCallCenter::SHOW_ERROR_OUR_TRASH
                    ]));
                    $content = "{$item->our_trash_count} (" . $link . ")";
                    return Html::tag('div', $content, ['class' => 'has-error']);
                } else {
                    return $item->our_trash_count;
                }
            }
        ],
        [
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'attribute' => 'differentProductCount',
            'content' => function ($item) {
                if ($item->differentProductCount > 0) {
                    $link = Html::a($item->differentProductCount, Url::toRoute([
                        'show-errors',
                        'id' => $item->id,
                        'type' => ReportCallCenter::SHOW_ERROR_NOT_FOUND_PRODUCTS
                    ]));
                    $content = $link;
                    return Html::tag('div', $content, ['class' => 'has-error']);
                } else {
                    return $item->differentProductCount;
                }
            }
        ],
        [
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'attribute' => 'differentProductQuantityCount',
            'content' => function ($item) {
                if ($item->differentProductQuantityCount > 0) {
                    $link = Html::a($item->differentProductQuantityCount, Url::toRoute([
                        'show-errors',
                        'id' => $item->id,
                        'type' => ReportCallCenter::SHOW_ERROR_NOT_EQUAL_PRODUCT_QUANTITY
                    ]));
                    $content = $link;
                    return Html::tag('div', $content, ['class' => 'has-error']);
                } else {
                    return $item->differentProductQuantityCount;
                }
            }
        ],
        [
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'attribute' => 'differentProductPriceCount',
            'content' => function ($item) {
                if ($item->differentProductPriceCount > 0) {
                    $link = Html::a($item->differentProductPriceCount, Url::toRoute([
                        'show-errors',
                        'id' => $item->id,
                        'type' => ReportCallCenter::SHOW_ERROR_NOT_EQUAL_PRODUCT_PRICE
                    ]));
                    $content = $link;
                    return Html::tag('div', $content, ['class' => 'has-error']);
                } else {
                    return $item->differentProductPriceCount;
                }
            }
        ],
        'actions' => [
            'class' => ActionColumn::className(),
            'items' => [
                [
                    'label' => Yii::t('common', 'Перепроверить'),
                    'url' => function ($item) {
                        return Url::toRoute(['recheck-log', 'id' => $item->id]);
                    },
                    'can' => function () {
                        return Yii::$app->user->can('report.reportcallcenter.index');
                    },
                ]
            ]
        ]
    ]
]) ?>
