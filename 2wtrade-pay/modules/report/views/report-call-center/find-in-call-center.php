<?php

use app\widgets\Panel;
use app\modules\report\widgets\ActionNav;
use app\modules\report\widgets\ReportFilterFindCallCenter;
use app\widgets\LinkPager;
use app\modules\report\assets\ReportCallCenterAsset;
use app\modules\report\assets\ReportCallCenterFinderAsset;
use app\helpers\DataProvider;
use app\modules\report\assets\ActionNavAsset;
use app\modules\report\widgets\ReportCallCenterResponseViewer;

/**@var $reportForm \app\modules\report\components\ReportFormCallCenterCompare */
/**@var $dataProvider \yii\data\ArrayDataProvider */
/** @var $showerColumns \app\modules\report\widgets\ReportCallCenterShowerColumns */
/**@var $this \yii\web\View */

$this->title = Yii::t('common', 'Сверка с КЦ');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Отчеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ReportCallCenterAsset::register($this);
ReportCallCenterFinderAsset::register($this);
ActionNavAsset::register($this);
?>

<?= ReportFilterFindCallCenter::widget([
    'reportForm' => $reportForm
]) ?>

<div class="report-call-center-find-in-call-center">
    <?= Panel::widget([
        'nav' => new ActionNav([
            'tabs' => [
                [
                    'label' => Yii::t('common', 'Логи'),
                    'action' => 'index'
                ],
                [
                    'label' => Yii::t('common', 'Поиск в КЦ'),
                    'action' => 'find-in-call-center'
                ],
            ],
            'actions' => DataProvider::renderSummary($dataProvider) . $showerColumns->renderIcon()
        ]),
        'content' => $this->render('_find-in-call-center-content', [
            'dataProvider' => $dataProvider,
            'showerColumns' => $showerColumns
        ]),
        'footer' => LinkPager::widget([
            'pagination' => $dataProvider->getPagination(),
            'counterPosition' => LinkPager::COUNTER_POSITION_RIGHT
        ])
    ]) ?>
    <div class="hidden call-center-responses">
        <?php foreach ($dataProvider->models as $item): ?>
            <div id="call_center_response_<?= $item->order_id ?>">
                <?= $item->responseMessage ?>
            </div>
        <?php endforeach; ?>
    </div>
</div>

<?= $showerColumns->renderModal(); ?>

<?= ReportCallCenterResponseViewer::widget(); ?>
