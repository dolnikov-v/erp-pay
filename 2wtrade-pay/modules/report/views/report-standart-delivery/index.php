<?php
use app\modules\report\assets\ReportAsset;
use app\modules\report\widgets\ReportFilterStandartDelivery;
use app\widgets\Panel;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormDaily $reportForm */
/** @var \yii\data\ActiveDataProvider $dataProvider */

$this->title = Yii::t('common', 'Сроки доставки');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Отчеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ReportAsset::register($this);
?>

<?= ReportFilterStandartDelivery::widget([
    'reportForm' => $reportForm,
]) ?>

<?php
    echo Panel::widget([
        'title' => Yii::t('common', ''),
        'alert' => $reportForm->panelAlert,
        'alertStyle' => $reportForm->panelAlertStyle,
        'border' => false,
        'withBody' => false,
        'content' => $this->render('_index-content', [
            'reportForm' => $reportForm,
            'dataProvider' => $dataProvider,
        ]),
    ]);
?>
