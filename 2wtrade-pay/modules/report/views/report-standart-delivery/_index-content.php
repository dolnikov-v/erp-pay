<?php
use app\modules\report\extensions\GridViewStandartDelivery;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormStandartDelivery $reportForm */
/** @var \yii\data\ArrayDataProvider $dataProvider */
?>

<div class="table-responsive">
    <?= GridViewStandartDelivery::widget([
        'id' => 'report_grid_standart_delivery',
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
        'showPageSummary' => false,
        'showFooter' => true,
    ]) ?>
</div>
