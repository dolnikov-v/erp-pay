<?php
use app\modules\report\extensions\GridViewStatus;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormStatus $reportForm */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */
?>

<div class="table-responsive">
    <?= GridViewStatus::widget([
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
        'resizableColumns' => false,
    ]) ?>
</div>
