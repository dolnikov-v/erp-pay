<?php
use app\modules\report\assets\ReportAsset;
use app\modules\report\widgets\ReportFilterCallCenterCheckRequest;
use app\widgets\Panel;
use app\widgets\Nav;
use app\widgets\ButtonLink;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormCallCenterCheckRequest $reportForm */
/** @var \app\modules\report\extensions\DataProvider $dataProviderReject */
/** @var \app\modules\report\extensions\DataProvider $dataProviderHold */
/** @var \app\modules\report\extensions\DataProvider $dataProviderBuyout */

$this->title = Yii::t('common', 'Проверка Курьерских служб');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Отчеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ReportAsset::register($this);
?>

<?= ReportFilterCallCenterCheckRequest::widget([
    'reportForm' => $reportForm,
]) ?>

<?= Panel::widget([
    'title' => ButtonLink::widget([
            "id" => "btn_export_excel",
            'icon' => '<i class="fa fa-table"></i>',
            "url" => Url::toRoute(['call-center-check-request/index'] + Yii::$app->request->queryParams + ['export' => 'excel']),
            "style" => ButtonLink::STYLE_SUCCESS . " pull-right",
            "label" => Yii::t("common", "Экспорт в Excel"),
            "attributes" => ["style" => 'text-align: right']
        ]) . '<br clear="all">',
    'nav' => new Nav([
        'tabs' => [
            [
                'label' => Yii::t('common', 'Отказы КС'),
                'content' => $this->render('_tab', [
                    'tabName' => 'reject',
                    'dataProvider' => $dataProviderReject,
                    'reportForm' => $reportForm,
                ]),
            ],
            [
                'label' => Yii::t('common', 'Заказы в процессе'),
                'content' => $this->render('_tab', [
                    'tabName' => 'hold',
                    'dataProvider' => $dataProviderHold,
                    'reportForm' => $reportForm,
                ]),
            ],
            [
                'label' => Yii::t('common', 'Выкуп'),
                'content' => $this->render('_tab', [
                    'tabName' => 'buyout',
                    'dataProvider' => $dataProviderBuyout,
                    'reportForm' => $reportForm,
                ]),
            ],
        ],
    ]),
]); ?>