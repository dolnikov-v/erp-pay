<?php
use app\modules\report\extensions\GridViewCallCenterCheckRequest;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormCallCenterCheckRequest $reportForm */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */
/** @var string $tabName */
?>

<div class="table-responsive">
    <?= GridViewCallCenterCheckRequest::widget([
        'id' => 'report_grid_call_center_check_request_' . $tabName,
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
        'export' => false
    ]) ?>
</div>
