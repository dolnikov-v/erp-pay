<?php
use app\modules\report\widgets\GraphApproveByCountry;

/** @var \yii\web\View $this */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */
?>

<div class="table-responsive">
    <?=GraphApproveByCountry::widget([
    'dataProvider' => $dataProvider,
    'reportForm' => $reportForm
]);?>
</div>