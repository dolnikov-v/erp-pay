<?php

use app\modules\report\extensions\GridViewApproveByCountry;

/** @var \yii\web\View $this */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */
/** @var \app\modules\report\components\ReportFormApproveByCountry $reportForm */
?>


<br>
<br>
<div class="table-responsive">
    <?= GridViewApproveByCountry::widget([
        'id' => 'report_approve_by_country',
        'dataProvider' => $dataProvider,
        'reportForm' => $reportForm,
        'layout' => '{items}',
        'tableOptions' => ['class' => 'table table-striped'],
        'rowOptions' => ['class' => 'tr-vertical-align-middle '],
    ]); ?>
</div>