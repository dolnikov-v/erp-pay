<?php
use app\modules\report\extensions\GridViewSalary;
use app\modules\report\extensions\DetailViewSalary;
use app\components\widgets\PrintThis;
use app\components\grid\GridView;
use app\widgets\ButtonLink;
use app\modules\report\extensions\GridViewTimeSheetDetail;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormSalary $reportForm */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */
?>

<?php if ($dataProvider->allModels): ?>
    <?php if ($reportForm->person): ?>
        <div class="pull-right">
            <?= PrintThis::widget([
                'htmlOptions' => [
                    'id' => 'PrintThis',
                    'btnClass' => 'btn btn-info',
                    'btnId' => 'btnPrintThis',
                    'btnText' => Yii::t('common', 'Печать'),
                    'btnIcon' => 'fa fa-print'
                ],
                'options' => [
                    'debug' => false,
                    'importCSS' => true,
                    'importStyle' => false,
                    'pageTitle' => Yii::t('common', 'Расчет ЗП'),
                    'removeInline' => false,
                    'printDelay' => 333,
                    'header' => null,
                    'formValues' => true,
                ]
            ]) . ' ' .
            ButtonLink::widget([
                "id" => "btn_export_excel",
                'icon' => '<i class="fa fa-table"></i>',
                "url" => Url::toRoute(['salary/index'] + Yii::$app->request->queryParams + ['export' => 'excel']),
                "style" => ButtonLink::STYLE_SUCCESS . " pull-right",
                "label" => Yii::t("common", "Экспорт в Excel"),
                "attributes" => ["style" => 'margin-left: 10px']
            ]) . ' ' .
            ButtonLink::widget([
                "id" => "btn_export_pdf",
                'icon' => '<i class="fa fa-file"></i>',
                "url" => Url::toRoute(['salary/index'] + Yii::$app->request->queryParams + ['export' => 'pdf']),
                "style" => ButtonLink::STYLE_SUCCESS . " pull-right",
                "label" => Yii::t("common", "Экспорт в PDF"),
                "attributes" => ["style" => "margin-left: 10px", "target" => "_blank"]
            ]);
            ?>
        </div>

        <div id="PrintThis">
            <h3 class="panel-title"><?= $reportForm->getDateWorkFilter()->from . ' - ' . $reportForm->getDateWorkFilter()->to ?></h3>
            <?= DetailViewSalary::widget([
                'reportForm' => $reportForm,
                'model' => $dataProvider->allModels[0]
            ]); ?>
            <?php if ($dataProvider->allModels[0]['penalties']): ?>
                <h4 class="panel-title"><?= Yii::t('common', 'Штрафы') ?> </h4>
                <?php
                $dataPenaltyProvider = new \yii\data\ArrayDataProvider([
                    'allModels' => $dataProvider->allModels[0]['penalties'],
                ]);
                ?>
                <?= GridView::widget([
                    'dataProvider' => $dataPenaltyProvider,
                    'columns' => [
                        [
                            'attribute' => 'penalty_type_id',
                            'content' => function ($model) {
                                if (!empty($model->penalty_type_id)) {
                                    return $model->penaltyType->name;
                                }
                                return '';
                            }
                        ],
                        [
                            'attribute' => 'date',
                            'headerOptions' => ['class' => 'text-center'],
                            'contentOptions' => ['class' => 'text-center'],
                            'content' => function ($model) {
                                return Yii::$app->formatter->asDate($model->date);
                            },
                        ],
                        [
                            'attribute' => 'comment',
                        ],
                        [
                            'attribute' => 'penalty_sum_usd',
                            'headerOptions' => ['class' => 'text-right'],
                            'contentOptions' => ['class' => 'text-right'],
                            'format' => ['decimal', 2],
                            'visible' => $reportForm->getCalcSalaryUSD()
                        ],
                        [
                            'attribute' => 'penalty_sum_local',
                            'headerOptions' => ['class' => 'text-right'],
                            'contentOptions' => ['class' => 'text-right'],
                            'format' => ['decimal', 2],
                            'visible' => $reportForm->getCalcSalaryLocal()
                        ],
                    ]
                ]); ?>
            <?php endif; ?>
            <?php if ($dataProvider->allModels[0]['bonuses']): ?>
                <h4 class="panel-title"><?= Yii::t('common', 'Бонусы') ?> </h4>
                <?php
                $dataBonusProvider = new \yii\data\ArrayDataProvider([
                    'allModels' => $dataProvider->allModels[0]['bonuses'],
                ]);
                ?>
                <?= GridView::widget([
                    'dataProvider' => $dataBonusProvider,
                    'columns' => [
                        [
                            'attribute' => 'bonus_type_id',
                            'content' => function ($model) {
                                if (!empty($model->bonus_type_id)) {
                                    return $model->bonusType->name;
                                }
                                return '';
                            }
                        ],
                        [
                            'attribute' => 'date',
                            'headerOptions' => ['class' => 'text-center'],
                            'contentOptions' => ['class' => 'text-center'],
                            'content' => function ($model) {
                                return Yii::$app->formatter->asDate($model->date);
                            },
                        ],
                        [
                            'attribute' => 'comment',
                        ],
                        [
                            'attribute' => 'bonus_percent',
                            'label' => Yii::t('common', 'Процент'),
                            'headerOptions' => ['class' => 'text-right'],
                            'contentOptions' => ['class' => 'text-right'],
                            'format' => ['decimal', 2],
                        ],
                        [
                            'attribute' => 'bonus_percent_hour',
                            'label' => Yii::t('common', 'Процент часа'),
                            'headerOptions' => ['class' => 'text-right'],
                            'contentOptions' => ['class' => 'text-right'],
                            'format' => ['decimal', 2],
                        ],
                        [
                            'attribute' => 'bonus_hour',
                            'label' => Yii::t('common', 'Час'),
                            'headerOptions' => ['class' => 'text-right'],
                            'contentOptions' => ['class' => 'text-right'],
                            'format' => ['decimal', 2],
                        ],
                        [
                            'attribute' => 'bonus_sum_usd',
                            'label' => Yii::t('common', 'Сумма в USD'),
                            'headerOptions' => ['class' => 'text-right'],
                            'contentOptions' => ['class' => 'text-right'],
                            'format' => ['decimal', 2],
                            'visible' => $reportForm->getCalcSalaryUSD()
                        ],
                        [
                            'attribute' => 'bonus_sum_local',
                            'label' => Yii::t('common', 'Сумма в местной валюте'),
                            'headerOptions' => ['class' => 'text-right'],
                            'contentOptions' => ['class' => 'text-right'],
                            'format' => ['decimal', 2],
                            'visible' => $reportForm->getCalcSalaryLocal()
                        ],
                    ]
                ]); ?>
            <?php endif; ?>
            <?php if ($dataProvider->allModels[0]['times']): ?>
                <h4 class="panel-title"><?= Yii::t('common', 'Табель') ?> </h4>
                <?php
                $dataTimeSheetProvider = new \yii\data\ArrayDataProvider([
                    'allModels' => [
                        'times' => $dataProvider->allModels[0]['times'],
                    ],
                ]);
                ?>
                <div class="table-responsive wrap-report-time-sheet">
                    <?= GridViewTimeSheetDetail::widget([
                        'dataProvider' => $dataTimeSheetProvider,
                        'dates' => $dataProvider->allModels[0]['dates'],
                        'plan' => $dataProvider->allModels[0]['plan'],
                        'person' => $dataProvider->allModels[0],
                        'showPageSummary' => false,
                    ]); ?>
                </div>
            <?php endif; ?>
        </div>
    <?php else: ?>
        <?= GridViewSalary::widget([
            'id' => 'report_grid_salary',
            'reportForm' => $reportForm,
            'dataProvider' => $dataProvider,
            'showPageSummary' => true,
            'tableOptions' => ['class' => 'table table-report table-striped table-hover table-bordered'],
        ]) ?>
    <?php endif; ?>
<?php endif; ?>