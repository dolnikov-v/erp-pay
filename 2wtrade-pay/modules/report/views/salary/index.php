<?php

use app\modules\report\assets\ReportAsset;
use app\modules\report\assets\ReportSalaryAsset;
use app\modules\report\assets\ReportTimeSheetAsset;
use app\modules\report\widgets\ReportFilterSalary;
use app\modules\report\extensions\GridViewSalaryTotal;
use app\widgets\Panel;
use app\widgets\ButtonConfirmLink;
use yii\helpers\Url;
use app\widgets\Modal;

/** @var \yii\web\View $this */
/** @var array $totals */
/** @var \app\modules\report\components\ReportFormSalary $reportForm */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */

$this->title = Yii::t('common', 'Расчет ЗП');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Отчеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ReportAsset::register($this);
ReportSalaryAsset::register($this);
ReportTimeSheetAsset::register($this);
?>

<?= ReportFilterSalary::widget([
    'reportForm' => $reportForm,
]) ?>

<?php if ($dataProvider->allModels): ?>
    <?php if (!$reportForm->person): ?>
        <?php
        $dataTotalProvider = new \yii\data\ArrayDataProvider([
            'allModels' => $totals,
        ]);
        ?>
        <?= Panel::widget([
            'title' => Yii::t('common', 'Расчет ЗП'),
            'border' => false,
            'withBody' => false,
            'alert' => $reportForm->panelAlert,
            'alertStyle' => $reportForm->panelAlertStyle,
            'content' => GridViewSalaryTotal::widget([
                'id' => 'report_grid_salary_total',
                'reportForm' => $reportForm,
                'dataProvider' => $dataTotalProvider,
                'showPageSummary' => sizeof($totals) > 1 ? true : false,
                'tableOptions' => ['class' => 'table table-report table-striped table-hover table-bordered table-totals'],
            ])
        ]) ?>
    <?php endif; ?>
<?php endif; ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Детальный расчет ЗП'),
    'border' => false,
    'withBody' => false,
    'alert' => $reportForm->panelAlert,
    'alertStyle' => $reportForm->panelAlertStyle,
    'content' => $this->render('_index-content', [
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
        'totals' => $totals,
    ]),
]) ?>


<?php if (
    !$reportForm->closedMode &&
    !$reportForm->person &&
    isset($reportForm->officeFilter['office']) &&
    sizeof($reportForm->officeFilter['office']) == 1 &&
    Yii::$app->user->can('report.salary.close')
): ?>
    <?= Panel::widget([
        'title' => Yii::t('common', 'Закрыть расчет ЗП'),
        'border' => false,
        'withBody' => true,
        'content' => ButtonConfirmLink::widget([
            'label' => Yii::t('common', 'Подтвердить и закрыть расчет за выбранный период'),
            'title' => Yii::t('common', 'Подтвердить расчет'),
            'confirm' => Yii::t('common', 'Вы уверены в верности данных расчета?'),
            'url' => Url::current(['state' => 'save']),
            'style' => ButtonConfirmLink::STYLE_SUCCESS,
            'size' => ButtonConfirmLink::SIZE_SMALL,
            'modalColor' => Modal::COLOR_WARNING,
            'modalButtonStyle' => ButtonConfirmLink::STYLE_SUCCESS,
        ])
    ]) ?>
<?php endif; ?>