<?php

use app\helpers\DataProvider;
use app\widgets\Panel;

/** @var \yii\data\ArrayDataProvider $dataProvider */
/** @var array $dateRange */
/** @var \app\modules\delivery\models\Delivery $delivery */
/** @var \yii\web\View $this */

$this->title = Yii::t('common', 'Список платежек для {delivery}', ['delivery' => $delivery->name]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Отчеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Дебиторская задолженность'), 'url' => 'index'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => ($dateRange['from'] && $dateRange['to']) ? Yii::t('common', 'Список платежек по заказам, утвержденным в период {from} - {to}', ['from' => Yii::$app->formatter->asDate($dateRange['from']), 'to' => Yii::$app->formatter->asDate($dateRange['to'])]) : Yii::t('common', 'Список платежек по заказам с неизвестной датой утверждения'),
    'border' => false,
    'withBody' => false,
    'content' => $this->render('_show-payments-content', [
        'dataProvider' => $dataProvider,
        'dateRange' => $dateRange
    ]),
]) ?>
