<?php
use app\modules\order\models\OrderStatus;
use app\modules\order\models\search\filters\DateFilter;
use app\modules\report\components\ReportFormDeliveryDebtsOld;
use app\modules\report\extensions\DataColumnPercent;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var ReportFormDeliveryDebtsOld $reportForm */
/** @var \yii\data\ArrayDataProvider $dataProvider */
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'layout' => '{items}',
    'tableOptions' => ['class' => 'table table-striped'],
    'rowOptions' => ['class' => 'tr-vertical-align-middle'],
    'columns' => [
        ['class' => 'kartik\grid\SerialColumn', 'width' => 0],
        [
            'attribute' => 'country_name',
            'label' => Yii::t('common', 'Страна'),
            'group' => true,
            'hAlign' => 'center',
            'vAlign' => 'middle',
            'groupedRow' => true,
            'groupOddCssClass' => 'kv-grouped-row',
            'groupEvenCssClass' => 'kv-grouped-row',
            'groupFooter' => function ($model) {
                $totalBuyout = $model['total_country_buyout'] . ' (' . DataColumnPercent::calculatePercent($model['total_country_buyout'], $model['total_country_vsego']) . ')';
                $totalMoneyReceived = $model['total_country_money_received'] . ' (' . DataColumnPercent::calculatePercent($model['total_country_money_received'], $model['total_country_buyout']) . ')';
                $response = [
                    'mergeColumns' => [[0, 3]],
                    'content' => [
                        4 => $totalBuyout,
                        5 => $totalMoneyReceived,
                        6 => GridView::F_SUM,
                        7 => GridView::F_SUM,
                        8 => GridView::F_SUM,
                        9 => GridView::F_SUM,
                        10 => GridView::F_SUM,
                        11 => GridView::F_SUM,
                    ],
                    'contentFormats' => [
//                        5 => ['format' => 'number', 'decimals' => 2],
//                        6 => ['format' => 'number', 'decimals' => 2],
                        6 => ['format' => 'number', 'decimals' => 2, 'thousandSep' => ' ', 'decPoint' => '.',],
                        7 => ['format' => 'number', 'decimals' => 2, 'thousandSep' => ' ', 'decPoint' => '.',],
                        8 => ['format' => 'number', 'decimals' => 2, 'thousandSep' => ' ', 'decPoint' => '.',],
                        9 => ['format' => 'number', 'decimals' => 2, 'thousandSep' => ' ', 'decPoint' => '.',],
                        10 => ['format' => 'number', 'decimals' => 2, 'thousandSep' => ' ', 'decPoint' => '.',],
                        11 => ['format' => 'number', 'decimals' => 2, 'thousandSep' => ' ', 'decPoint' => '.',],
                    ],
                    'contentOptions' => [
                        4 => ['style' => 'text-align:right'],
                        5 => ['style' => 'text-align:right'],
                        6 => ['style' => 'text-align:right'],
                        7 => ['style' => 'text-align:right'],
                        8 => ['style' => 'text-align:right'],
                        9 => ['style' => 'text-align:right'],
                        10 => ['style' => 'text-align:right'],
                        11 => ['style' => 'text-align:right'],

                    ],
                    'options' => ['class' => 'warning', 'style' => 'font-weight:bold;']
                ];
                return $response;
            }
        ],
        [
            'attribute' => 'delivery_name',
            'label' => Yii::t('common', 'Служба доставки'),
            'group' => true,
            'hAlign' => 'center',
            'vAlign' => 'middle',
            'subGroupOf' => 1,
            'groupOddCssClass' => '',
            'groupEvenCssClass' => '',
        ],
        [
            'attribute' => 'last_date_payment',
            'label' => Yii::t('common', 'Дата последней оплаты'),
            'group' => true,
            'subGroupOf' => 2,
            'hAlign' => 'center',
            'vAlign' => 'middle',
            'groupOddCssClass' => '',
            'groupEvenCssClass' => '',
            'format' => 'date',
            'headerOptions' => ['style' => 'width:10%;']
        ],
        [
            'attribute' => 'buyout',
            'label' => Yii::t('common', 'Выкуплено заказов'),
            'class' => DataColumnPercent::className(),
            'totalCountAttribute' => 'vsego',
            'hAlign' => 'right',
            'link' => function ($model) {
                return Url::toRoute([
                    '/order/index/index',
                    'force_country_slug' => $model['country_slug'],
                    'StatusFilter' => [
                        'not' => [],
                        'status' => OrderStatus::getBuyoutList(),
                    ],
                    'DeliveryFilter' => [
                        'delivery' => [$model['delivery_id']]
                    ]
                ]);
            }
        ],
        [
            'attribute' => 'money_received',
            'label' => Yii::t('common', 'Оплачено заказов'),
            'class' => DataColumnPercent::className(),
            'totalCountAttribute' => 'buyout',
            'hAlign' => 'right',
            'link' => function ($model) {
                return Url::toRoute([
                    '/order/index/index',
                    'force_country_slug' => $model['country_slug'],
                    'StatusFilter' => [
                        'not' => [],
                        'status' => OrderStatus::getOnlyMoneyReceivedList(),
                    ],
                    'DeliveryFilter' => [
                        'delivery' => [$model['delivery_id']]
                    ]
                ]);
            }
        ],
        [
            'attribute' => 'common_end_sum_dollars',
            'label' => Yii::t('common', 'Долг КС'),
            'hAlign' => 'right',
            'format' => 'raw',
            'value' => function ($model) {
                return Html::a(number_format($model['common_end_sum_dollars'], 2, '.', ' ') . " USD / " . number_format($model['common_end_sum'], 2, '.', ' ') . " {$model['currency']}", Url::toRoute([
                    '/order/index/index',
                    'force_country_slug' => $model['country_slug'],
                    'StatusFilter' => [
                        'not' => [],
                        'status' => array_merge(OrderStatus::getBuyoutList(), OrderStatus::getOnlyNotBuyoutInDeliveryList()),
                    ],
                    'DeliveryFilter' => [
                        'delivery' => [$model['delivery_id']]
                    ]
                ]));
            }
        ],
        [
            'attribute' => 'end_sum_done_dollars',
            'label' => Yii::t('common', 'Получено от КС'),
            'hAlign' => 'right',
            'format' => 'raw',
            'value' => function ($model) {
                return Html::a(number_format($model['end_sum_done_dollars'], 2, '.', ' ') . " USD / " . number_format($model['end_sum_done'], 2, '.', ' ') . " {$model['currency']}", Url::toRoute([
                    '/order/index/index',
                    'force_country_slug' => $model['country_slug'],
                    'StatusFilter' => [
                        'not' => [],
                        'status' => array_merge(OrderStatus::getOnlyMoneyReceivedList(), OrderStatus::getOnlyNotBuyoutDoneList()),
                    ],
                    'DeliveryFilter' => [
                        'delivery' => [$model['delivery_id']]
                    ]
                ]));
            }
        ],
        [
            'attribute' => 'end_sum_dollars',
            'label' => Yii::t('common', 'Остаток долга'),
            'hAlign' => 'right',
            'format' => 'raw',
            'value' => function ($model) {
                return Html::a(number_format($model['end_sum_dollars'], 2, '.', ' ') . " USD / " . number_format($model['end_sum'], 2, '.', ' ') . " {$model['currency']}", Url::toRoute([
                    '/order/index/index',
                    'force_country_slug' => $model['country_slug'],
                    'StatusFilter' => [
                        'not' => [],
                        'status' => array_merge(OrderStatus::getOnlyBuyoutList(), OrderStatus::getNotBuyoutInProcessList())
                    ],
                    'DeliveryFilter' => [
                        'delivery' => [$model['delivery_id']]
                    ]
                ]));
            }
        ],
        [
            'attribute' => 'common_charges_dollars',
            'label' => Yii::t('common', 'Оплата услуг КС'),
            'hAlign' => 'right',
            'format' => 'raw',
            'value' => function ($model) {
                return Html::a(number_format($model['common_charges_dollars'], 2, '.', ' ') . " USD / " . number_format($model['common_charges_dollars'], 2, '.', ' ') . " {$model['currency']}", Url::toRoute([
                    '/order/index/index',
                    'force_country_slug' => $model['country_slug'],
                    'StatusFilter' => [
                        'not' => [],
                        'status' => array_merge(OrderStatus::getBuyoutList(), OrderStatus::getOnlyNotBuyoutInDeliveryList()),
                    ],
                    'DeliveryFilter' => [
                        'delivery' => [$model['delivery_id']]
                    ]
                ]));
            }
        ],
    ],
]) ?>
