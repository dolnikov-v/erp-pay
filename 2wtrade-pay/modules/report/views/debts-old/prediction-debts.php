<?php

use app\modules\report\assets\ReportAsset;
use app\modules\report\assets\ReportDeliveryDebtsAsset;
use app\modules\report\widgets\ReportFilterDeliveryDebts;
use app\widgets\Panel;
use app\helpers\DataProvider;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormDeliveryDebts $reportForm */
/** @var \yii\data\ArrayDataProvider $dataProvider */
/** @var \yii\data\ArrayDataProvider $commonDataProvider */

$this->title = Yii::t('common', 'Плановая дебиторская задолженность (Старая)');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Отчеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ReportAsset::register($this);
ReportDeliveryDebtsAsset::register($this);
?>

<?= ReportFilterDeliveryDebts::widget(['reportForm' => $reportForm]); ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Плановая дебиторская задолженность'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'alert' => Yii::t('common', 'Конвертация валют в USD осуществлялась по курсу на {day}', ['day' => Yii::$app->formatter->asDate(time())]),
    'alertStyle' => Panel::ALERT_PRIMARY,
    'border' => false,
    'withBody' => false,
    'content' => $this->render('_prediction-debts-content', [
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
    ]),
]) ?>