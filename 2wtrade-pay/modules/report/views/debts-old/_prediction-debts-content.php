<?php
use app\modules\order\models\OrderStatus;
use app\modules\order\models\search\filters\DateFilter;
use app\modules\report\components\ReportFormDeliveryDebtsOld;
use app\modules\report\extensions\DataColumnPercent;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var ReportFormDeliveryDebtsOld $reportForm */
/** @var \yii\data\ArrayDataProvider $dataProvider */
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'layout' => '{items}',
    'tableOptions' => ['class' => 'table table-striped'],
    'rowOptions' => ['class' => 'tr-vertical-align-middle '],
    'columns' => [
        ['class' => 'kartik\grid\SerialColumn', 'width' => 0],
        [
            'attribute' => 'country_name',
            'label' => Yii::t('common', 'Страна'),
            'group' => true,
            'hAlign' => 'center',
            'vAlign' => 'middle',
            'groupedRow' => true,
            'groupOddCssClass' => 'custom-info',
            'groupEvenCssClass' => 'custom-info',
            'groupFooter' => function ($model) {
                $totalBuyout = $model['total_country_buyout'] . ' (' . DataColumnPercent::calculatePercent($model['total_country_buyout'], $model['total_country_vsego']) . ')';
                $totalInProcess = $model['total_country_in_process'] . ' (' . DataColumnPercent::calculatePercent($model['total_country_in_process'], $model['total_country_vsego']) . ')';
                $totalInUnBuyout = $model['total_country_unbuyout'] . ' (' . DataColumnPercent::calculatePercent($model['total_country_unbuyout'], $model['total_country_vsego']) . ')';
                $totalMoneyReceived = $model['total_country_money_received'] . ' (' . DataColumnPercent::calculatePercent($model['total_country_money_received'], $model['total_country_buyout']) . ')';
                $response = [
                    'mergeColumns' => [[0, 3]],
                    'content' => [
                        4 => number_format($model['total_country_end_sum_dollars'], 2, ',', ' ') . " USD",
                        5 => number_format($model['total_country_charges_buyout_dollars'], 2, ',', ' ') . " USD",
                        6 => number_format($model['total_country_charges_in_process_dollars'], 2, ',', ' ') . " USD",
                        7 => number_format($model['total_country_charges_unbuyout_dollars'], 2, ',', ' ') . " USD",
                        8 => number_format($model['total_country_sum_total_dollars'], 2, ',', ' ') . " USD",
                    ],
                    'contentFormats' => [
//                        5 => ['format' => 'number', 'decimals' => 2],
//                        6 => ['format' => 'number', 'decimals' => 2],
//                        7 => ['format' => 'number', 'decimals' => 2, 'thousandSep' => ' ', 'decPoint' => '.',],
//                        8 => ['format' => 'number', 'decimals' => 2, 'thousandSep' => ' ', 'decPoint' => '.',],
//                        9 => ['format' => 'number', 'decimals' => 2, 'thousandSep' => ' ', 'decPoint' => '.',],
                        /*10 => ['format' => 'number', 'decimals' => 2, 'thousandSep' => ' ', 'decPoint' => '.',],
                        11 => ['format' => 'number', 'decimals' => 2, 'thousandSep' => ' ', 'decPoint' => '.',],
                        12 => ['format' => 'number', 'decimals' => 2, 'thousandSep' => ' ', 'decPoint' => '.',],*/
                    ],
                    'contentOptions' => [
                        4 => ['style' => 'text-align:right'],
                        5 => ['style' => 'text-align:right'],
                        6 => ['style' => 'text-align:right'],
                        7 => ['style' => 'text-align:right'],
                        8 => ['style' => 'text-align:right'],
                        9 => ['style' => 'text-align:right'],
                        10 => ['style' => 'text-align:right'],
                        11 => ['style' => 'text-align:right'],
                        12 => ['style' => 'text-align:right'],

                    ],
                    'options' => ['class' => 'warning', 'style' => 'font-weight:bold;']
                ];
                return $response;
            }
        ],
        [
            'attribute' => 'delivery_name',
            'label' => Yii::t('common', 'Служба доставки'),
            'group' => true,
            'hAlign' => 'center',
            'vAlign' => 'middle',
            'subGroupOf' => 1,
            'groupOddCssClass' => 'active',
            'groupEvenCssClass' => '',
            'contentOptions' => [
                'class' => 'custom-active h4'
            ]
        ],
        [
            'attribute' => 'start_month_time',
            'label' => Yii::t('common', 'Месяц'),
            'hAlign' => 'center',
            'format' => 'raw',
            'value' => function ($model) {
                return Yii::$app->formatter->asMonth($model['start_month_time']);
            }
        ],
        [
            'attribute' => 'end_sum_dollars',
            'label' => Yii::t('common', 'Текущий доход'),
            'hAlign' => 'right',
            'format' => 'raw',
            'value' => function ($model) {
                return Html::a(number_format($model['end_sum_dollars'], 2, ',', ' ') . " USD", Url::toRoute([
                    '/order/index/index',
                    'force_country_slug' => $model['country_slug'],
                    'DateFilter' => $model['start_month_time'] ? [
                        'dateType' => DateFilter::TYPE_DELIVERY_SENT_AT_NOT_EMPTY,
                        'dateFrom' => Yii::$app->formatter->asDate(strtotime('first day of this month', $model['start_month_time'])),
                        'dateTo' => Yii::$app->formatter->asDate(strtotime('last day of this month', $model['start_month_time'])),
                    ] : [],
                    'StatusFilter' => [
                        'not' => [],
                        'status' => array_merge(OrderStatus::getBuyoutList(), OrderStatus::getOnlyNotBuyoutInDeliveryList()),
                    ],
                    'DeliveryFilter' => [
                        'delivery' => [$model['delivery_id']]
                    ]
                ]), [
                    'title' => number_format($model['end_sum'], 2, '.', ' ') . " {$model['currency']}"
                ]);
            }
        ],
        [
            'attribute' => 'charges_buyout_dollars',
            'label' => Yii::t('common', 'Расходы на КС на выкупленные заказы'),
            'hAlign' => 'right',
            'format' => 'raw',
            'value' => function ($model) {
                return Html::a(number_format($model['charges_buyout_dollars'], 2, ',', ' ') . " USD", Url::toRoute([
                    '/order/index/index',
                    'force_country_slug' => $model['country_slug'],
                    'DateFilter' => $model['start_month_time'] ? [
                        'dateType' => DateFilter::TYPE_DELIVERY_SENT_AT_NOT_EMPTY,
                        'dateFrom' => Yii::$app->formatter->asDate(strtotime('first day of this month', $model['start_month_time'])),
                        'dateTo' => Yii::$app->formatter->asDate(strtotime('last day of this month', $model['start_month_time'])),
                    ] : [],
                    'StatusFilter' => [
                        'not' => [],
                        'status' => OrderStatus::getBuyoutList(),
                    ],
                    'DeliveryFilter' => [
                        'delivery' => [$model['delivery_id']]
                    ]
                ]), [
                    'title' => number_format($model['charges_buyout'], 2, '.', ' ') . " {$model['currency']}"
                ]);
            }
        ],
        [
            'attribute' => 'charges_in_process_dollars',
            'label' => Yii::t('common', 'Расходы на КС на заказы в процессе'),
            'hAlign' => 'right',
            'format' => 'raw',
            'value' => function ($model) {
                return Html::a(number_format($model['charges_in_process_dollars'], 2, ',', ' ') . " USD", Url::toRoute([
                    '/order/index/index',
                    'force_country_slug' => $model['country_slug'],
                    'DateFilter' => $model['start_month_time'] ? [
                        'dateType' => DateFilter::TYPE_DELIVERY_SENT_AT_NOT_EMPTY,
                        'dateFrom' => Yii::$app->formatter->asDate(strtotime('first day of this month', $model['start_month_time'])),
                        'dateTo' => Yii::$app->formatter->asDate(strtotime('last day of this month', $model['start_month_time'])),
                    ] : [],
                    'StatusFilter' => [
                        'not' => [],
                        'status' => OrderStatus::getBuyoutList(),
                    ],
                    'DeliveryFilter' => [
                        'delivery' => [$model['delivery_id']]
                    ]
                ]), [
                    'title' => number_format($model['charges_buyout'], 2, '.', ' ') . " {$model['currency']}"
                ]);
            }
        ],
        [
            'attribute' => 'charges_unbuyout_dollars',
            'label' => Yii::t('common', 'Расходы на КС на отказы'),
            'hAlign' => 'right',
            'format' => 'raw',
            'value' => function ($model) {
                return Html::a(number_format($model['charges_unbuyout_dollars'], 2, ',', ' ') . " USD", Url::toRoute([
                    '/order/index/index',
                    'force_country_slug' => $model['country_slug'],
                    'DateFilter' => $model['start_month_time'] ? [
                        'dateType' => DateFilter::TYPE_DELIVERY_SENT_AT_NOT_EMPTY,
                        'dateFrom' => Yii::$app->formatter->asDate(strtotime('first day of this month', $model['start_month_time'])),
                        'dateTo' => Yii::$app->formatter->asDate(strtotime('last day of this month', $model['start_month_time'])),
                    ] : [],
                    'StatusFilter' => [
                        'not' => [],
                        'status' => OrderStatus::getOnlyNotBuyoutInDeliveryList(),
                    ],
                    'DeliveryFilter' => [
                        'delivery' => [$model['delivery_id']]
                    ]
                ]), [
                    'title' => number_format($model['charges_unbuyout'], 2, '.', ' ') . " {$model['currency']}",
                ]);
            }
        ],
        [
            'attribute' => 'sum_total_dollars',
            'label' => Yii::t('common', 'Сумма COD'),
            'hAlign' => 'right',
            'format' => 'raw',
            'value' => function ($model) {
                return Html::a(number_format($model['sum_total_dollars'], 2, ',', ' ') . " USD", Url::toRoute([
                    '/order/index/index',
                    'force_country_slug' => $model['country_slug'],
                    'DateFilter' => $model['start_month_time'] ? [
                        'dateType' => DateFilter::TYPE_DELIVERY_SENT_AT_NOT_EMPTY,
                        'dateFrom' => Yii::$app->formatter->asDate(strtotime('first day of this month', $model['start_month_time'])),
                        'dateTo' => Yii::$app->formatter->asDate(strtotime('last day of this month', $model['start_month_time'])),
                    ] : [],
                    'StatusFilter' => [
                        'not' => [],
                        'status' => array_merge(OrderStatus::getBuyoutList(), OrderStatus::getOnlyNotBuyoutInDeliveryList()),
                    ],
                    'DeliveryFilter' => [
                        'delivery' => [$model['delivery_id']]
                    ]
                ]), [
                    'title' => number_format($model['sum_total'], 2, ',', ' ') . " {$model['currency']}"
                ]);
            }
        ],
    ],
]) ?>
