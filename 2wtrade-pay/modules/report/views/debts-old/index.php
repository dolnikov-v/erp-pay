<?php
use app\helpers\DataProvider;
use app\modules\report\assets\ReportAsset;
use app\modules\report\assets\ReportDeliveryDebtsAsset;
use app\modules\report\components\ReportFormDeliveryDebtsOld;
use app\modules\report\widgets\ReportFilterDeliveryDebts;
use app\widgets\LinkPager;
use app\widgets\Panel;

/** @var \yii\web\View $this */
/** @var ReportFormDeliveryDebtsOld $reportForm */
/** @var \yii\data\ArrayDataProvider $dataProvider */
/** @var integer $oldTime */

$this->title = Yii::t('common', 'Дебиторская задолженность');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Отчеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ReportAsset::register($this);
ReportDeliveryDebtsAsset::register($this);
?>

<?= ReportFilterDeliveryDebts::widget(['reportForm' => $reportForm]); ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Дебиторская задолженность'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'alert' => Yii::t('common', 'Конвертация валют в USD осуществлялась по курсу на {day}', ['day' => Yii::$app->formatter->asDate(time())]) . '<br>' .
        Yii::t('common', 'Последний пересчет данных: {date}', ['date' => $oldTime ? Yii::$app->formatter->asDatetime($oldTime) : '-'])
    ,
    'alertStyle' => Panel::ALERT_PRIMARY,
    'border' => false,
    'withBody' => false,
    'content' => $this->render('_index-content', [
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
    ]),
]) ?>
