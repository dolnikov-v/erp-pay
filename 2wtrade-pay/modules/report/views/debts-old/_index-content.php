<?php
use app\modules\order\models\OrderStatus;
use app\modules\order\models\search\filters\DateFilter;
use app\modules\report\components\invoice\ReportFormInvoiceList;
use app\modules\report\components\ReportFormDeliveryDebtsOld;
use app\modules\report\extensions\DataColumnPercent;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var ReportFormDeliveryDebtsOld $reportForm */
/** @var \yii\data\ArrayDataProvider $dataProvider */
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'export' => [
        'label' => Yii::t('common', 'Экспорт'),
        'showConfirmAlert' => false,
        'fontAwesome' => true,
        'target' => '_self',
        'header' => false,
    ],
    'exportConfig' => [
        GridView::EXCEL => [],
        GridView::PDF => [],
        GridView::CSV => [],
        GridView::JSON => [],
    ],
    'toolbar' => '{export}',
    'layout' => '<div id="report_export_box">{toolbar}</div>{items}',
    'tableOptions' => ['class' => 'table table-striped'],
    'rowOptions' => function ($model) {
        $options = ['class' => 'tr-vertical-align-middle '];
        /*if (empty($model['in_process']) && ($model['buyout'] - $model['money_received']) == 0) {
            $options['class'] .= 'custom-success';
        }*/
        return $options;
    },
    'columns' => [
        ['class' => 'kartik\grid\SerialColumn', 'width' => 0],
        [
            'attribute' => 'country_name',
            'group' => true,
            'hAlign' => 'center',
            'vAlign' => 'middle',
            'groupedRow' => true,
            'groupOddCssClass' => 'custom-info',
            'groupEvenCssClass' => 'custom-info',
            'groupFooter' => function ($model) {
                $totalBuyout = $model['total_country_orders_buyout'] . ' (' . DataColumnPercent::calculatePercent($model['total_country_orders_buyout'], $model['total_country_orders_total']) . ')';
                $totalMoneyReceived = $model['total_country_orders_money_received'] . ' (' . DataColumnPercent::calculatePercent($model['total_country_orders_money_received'], $model['total_country_orders_buyout']) . ')';
                $totalInProcess = $model['total_country_orders_in_process'] . ' (' . DataColumnPercent::calculatePercent($model['total_country_orders_in_process'], $model['total_country_orders_total']) . ')';
                $totalUnBuyout = $model['total_country_orders_unbuyout'] . ' (' . DataColumnPercent::calculatePercent($model['total_country_orders_unbuyout'], $model['total_country_orders_total']) . ')';
                $totalNotMoneyReceived = ($model['total_country_orders_buyout'] - $model['total_country_orders_money_received']) . ' (' . DataColumnPercent::calculatePercent($model['total_country_orders_buyout'] - $model['total_country_orders_money_received'], $model['total_country_orders_buyout']) . ')';
                $response = [
                    'mergeColumns' => [[0, 3]],
                    'content' => [
                        0 => Yii::t('common', 'Итого по') . ' ' . $model['country_name'],
                        4 => Yii::$app->formatter->asDecimal($model['total_country_end_sum_dollars'], 2),
                        5 => Yii::$app->formatter->asDecimal($model['total_country_money_received_sum_total_dollars'] - $model['total_country_charges_accepted_dollars'], 2),
                        6 => Yii::$app->formatter->asDecimal($model['total_country_buyout_sum_total_dollars'] - $model['total_country_charges_not_accepted_dollars'], 2),
                        7 => $totalInProcess,
                        8 => $totalUnBuyout,
                    ],
                    'contentOptions' => [
                        4 => ['style' => 'text-align:right'],
                        5 => ['style' => 'text-align:right'],
                        6 => ['style' => 'text-align:right'],
                        7 => ['style' => 'text-align:right'],
                        8 => ['style' => 'text-align:right'],

                    ],
                    'options' => ['class' => 'warning', 'style' => 'font-weight:bold;']
                ];
                return $response;
            }
        ],
        [
            'attribute' => 'delivery_name',
            'label' => Yii::t('common', 'Служба доставки'),
            'group' => true,
            'groupFooter' => function ($model) {
                $totalBuyout = $model['total_delivery_orders_buyout'] . ' (' . DataColumnPercent::calculatePercent($model['total_delivery_orders_buyout'], $model['total_delivery_orders_total']) . ')';
                $totalMoneyReceived = $model['total_delivery_orders_money_received'] . ' (' . DataColumnPercent::calculatePercent($model['total_delivery_orders_money_received'], $model['total_delivery_orders_buyout']) . ')';
                $totalInProcess = $model['total_delivery_orders_in_process'] . ' (' . DataColumnPercent::calculatePercent($model['total_delivery_orders_in_process'], $model['total_delivery_orders_total']) . ')';
                $totalUnBuyout = $model['total_delivery_orders_unbuyout'] . ' (' . DataColumnPercent::calculatePercent($model['total_delivery_orders_unbuyout'], $model['total_delivery_orders_total']) . ')';
                $totalNotMoneyReceived = ($model['total_delivery_orders_buyout'] - $model['total_delivery_orders_money_received']) . ' (' . DataColumnPercent::calculatePercent($model['total_delivery_orders_buyout'] - $model['total_delivery_orders_money_received'], $model['total_delivery_orders_buyout']) . ')';
                $response = [
                    'mergeColumns' => [[0, 3]],
                    'content' => [
                        0 => Yii::t('common', 'Итого по') . ' ' . $model['delivery_name'],
                        4 => Yii::$app->formatter->asDecimal($model['total_delivery_end_sum_dollars'], 2),
                        5 => Yii::$app->formatter->asDecimal($model['total_delivery_money_received_sum_total_dollars'] - $model['total_delivery_charges_accepted_dollars'], 2),
                        6 => Yii::$app->formatter->asDecimal($model['total_delivery_buyout_sum_total_dollars'] - $model['total_delivery_charges_not_accepted_dollars'], 2),
                        7 => $totalInProcess,
                        8 => $totalUnBuyout,
                    ],
                    'contentOptions' => [
                        4 => ['style' => 'text-align:right'],
                        5 => ['style' => 'text-align:right'],
                        6 => ['style' => 'text-align:right'],
                        7 => ['style' => 'text-align:right'],
                        8 => ['style' => 'text-align:right'],

                    ],
                    'options' => ['class' => 'custom-warning', 'style' => 'font-style:italic;']
                ];
                return $response;
            },
            'hAlign' => 'center',
            'vAlign' => 'middle',
            'subGroupOf' => 1,
            'groupOddCssClass' => '',
            'groupEvenCssClass' => '',
            'contentOptions' => [
                'class' => 'custom-active h4'
            ],
            'format' => 'raw',
            'value' => function ($model) {
                return Html::a($model['delivery_name'], Url::toRoute([
                    '/report/invoice/index',
                    ReportFormInvoiceList::FORM_NAME => [
                        'from' => Yii::$app->formatter->asDate(strtotime('first day of this month', strtotime($model['total_delivery_min_month']))),
                        'to' => Yii::$app->formatter->asDate(strtotime('last day of this month', strtotime($model['total_delivery_max_month']))),
                        'country_ids' => [$model['country_id']],
                        'delivery_ids' => [$model['delivery_id']],
                    ],
                ]));
            }
        ],
        [
            'attribute' => 'month',
            'label' => Yii::t('common', 'Месяц'),
            'hAlign' => 'center',
            'format' => 'raw',
            'value' => function ($model) {
                return Html::a(Yii::$app->formatter->asMonth($model['month']), Url::toRoute([
                    '/report/invoice/index',
                    ReportFormInvoiceList::FORM_NAME => [
                        'from' => Yii::$app->formatter->asDate(strtotime('first day of this month', strtotime($model['month']))),
                        'to' => Yii::$app->formatter->asDate(strtotime('last day of this month', strtotime($model['month']))),
                        'country_ids' => [$model['country_id']],
                        'delivery_ids' => [$model['delivery_id']],
                    ],
                ]));
            }
        ],
        [
            'label' => Yii::t('common', 'Выкуплено заказов на сумму, {currency}', ['currency' => 'USD']),
            'attribute' => 'end_sum_dollars',
            'hAlign' => 'right',
            'format' => 'raw',
            'headerOptions' => [
                'data-container' => 'body',
                'data-toggle' => 'tooltip',
                'title' => Yii::t('common', 'Сумма COD для заказов в статусах ({buyout_statuses}) МИНУС сумма расходов на доставку заказов в статусах ({charges_statuses})', [
                    'buyout_statuses' => implode(', ', OrderStatus::getStatusNamesForArray(OrderStatus::getBuyoutList())),
                    'charges_statuses' => implode(', ', OrderStatus::getStatusNamesForArray(array_merge(OrderStatus::getBuyoutList(), OrderStatus::getNotBuyoutDeliveryList())))
                ]),
            ],
            'value' => function ($model) {
                return Html::a(Yii::$app->formatter->asDecimal($model['end_sum_dollars'], 2), Url::toRoute([
                    '/order/index/index',
                    'force_country_slug' => $model['country_slug'],
                    'DateFilter' => [
                        'dateType' => DateFilter::TYPE_DELIVERY_SENT_AT_NOT_EMPTY,
                        'dateFrom' => Yii::$app->formatter->asDate(strtotime('first day of this month', strtotime($model['month']))),
                        'dateTo' => Yii::$app->formatter->asDate(strtotime('last day of this month', strtotime($model['month']))),
                    ],
                    'StatusFilter' => [
                        'not' => [],
                        'status' => array_merge(OrderStatus::getBuyoutList(), OrderStatus::getNotBuyoutDeliveryList()),
                    ],
                    'DeliveryFilter' => [
                        'delivery' => [$model['delivery_id']]
                    ]
                ]));
            }
        ],
        [
            'label' => Yii::t('common', 'Денег получено от выкупленных заказов'),
            'attribute' => 'money_received_sum_total_dollars',
            'hAlign' => 'right',
            'format' => 'raw',
            'headerOptions' => [
                'data-container' => 'body',
                'data-toggle' => 'tooltip',
                'title' => Yii::t('common', 'Сумма COD для заказов в статусах ({buyout_statuses}) МИНУС сумма расходов на доставку заказов в статусах ({charges_statuses})', [
                    'buyout_statuses' => implode(', ', OrderStatus::getStatusNamesForArray(OrderStatus::getOnlyMoneyReceivedList())),
                    'charges_statuses' => implode(', ', OrderStatus::getStatusNamesForArray(array_merge(OrderStatus::getOnlyMoneyReceivedList(), OrderStatus::getOnlyNotBuyoutDoneList())))
                ]),
            ],
            'value' => function ($model) {
                return Html::a(Yii::$app->formatter->asDecimal($model['money_received_sum_total_dollars'] - $model['charges_accepted_dollars'], 2), Url::toRoute([
                    '/order/index/index',
                    'force_country_slug' => $model['country_slug'],
                    'DateFilter' => [
                        'dateType' => DateFilter::TYPE_DELIVERY_SENT_AT_NOT_EMPTY,
                        'dateFrom' => Yii::$app->formatter->asDate(strtotime('first day of this month', strtotime($model['month']))),
                        'dateTo' => Yii::$app->formatter->asDate(strtotime('last day of this month', strtotime($model['month']))),
                    ],
                    'StatusFilter' => [
                        'not' => [],
                        'status' => array_merge(OrderStatus::getOnlyMoneyReceivedList(), OrderStatus::getOnlyNotBuyoutInDeliveryList()),
                    ],
                    'DeliveryFilter' => [
                        'delivery' => [$model['delivery_id']]
                    ]
                ]));
            }
        ],
        [
            'label' => Yii::t('common', 'Денег не получено от выкупленных заказов'),
            'attribute' => 'buyout_sum_total_dollars',
            'hAlign' => 'right',
            'format' => 'raw',
            'headerOptions' => [
                'data-container' => 'body',
                'data-toggle' => 'tooltip',
                'title' => Yii::t('common', 'Сумма COD для заказов в статусах ({buyout_statuses}) МИНУС сумма расходов на доставку заказов в статусах ({charges_statuses})', [
                    'buyout_statuses' => implode(', ', OrderStatus::getStatusNamesForArray(OrderStatus::getOnlyBuyoutList())),
                    'charges_statuses' => implode(', ', OrderStatus::getStatusNamesForArray(array_merge(OrderStatus::getOnlyBuyoutList(), OrderStatus::getNotBuyoutInDeliveryProcessList())))
                ]),
            ],
            'value' => function ($model) {
                return Html::a(Yii::$app->formatter->asDecimal($model['buyout_sum_total_dollars'] - $model['charges_not_accepted_dollars'], 2), Url::toRoute([
                    '/order/index/index',
                    'force_country_slug' => $model['country_slug'],
                    'DateFilter' => [
                        'dateType' => DateFilter::TYPE_DELIVERY_SENT_AT_NOT_EMPTY,
                        'dateFrom' => Yii::$app->formatter->asDate(strtotime('first day of this month', strtotime($model['month']))),
                        'dateTo' => Yii::$app->formatter->asDate(strtotime('last day of this month', strtotime($model['month']))),
                    ],
                    'StatusFilter' => [
                        'not' => [],
                        'status' => array_merge(OrderStatus::getOnlyBuyoutList(), OrderStatus::getNotBuyoutInDeliveryProcessList()),
                    ],
                    'DeliveryFilter' => [
                        'delivery' => [$model['delivery_id']]
                    ]
                ]));
            }
        ],
        [
            'attribute' => 'orders_in_process',
            'label' => Yii::t('common', 'В процессе'),
            'class' => DataColumnPercent::className(),
            'totalCountAttribute' => 'orders_total',
            'hAlign' => 'right',
            'headerOptions' => [
                'data-container' => 'body',
                'data-toggle' => 'tooltip',
                'title' => Yii::t('common', 'Заказы в статусах: {statuses}', ['statuses' => implode(', ', OrderStatus::getStatusNamesForArray(OrderStatus::getProcessDeliveryList()))]),
            ],
            'link' => function ($model) {
                return Url::toRoute([
                    '/order/index/index',
                    'force_country_slug' => $model['country_slug'],
                    'DateFilter' => [
                        'dateType' => DateFilter::TYPE_DELIVERY_SENT_AT_NOT_EMPTY,
                        'dateFrom' => Yii::$app->formatter->asDate(strtotime('first day of this month', strtotime($model['month']))),
                        'dateTo' => Yii::$app->formatter->asDate(strtotime('last day of this month', strtotime($model['month']))),
                    ],
                    'StatusFilter' => [
                        'not' => [],
                        'status' => OrderStatus::getProcessDeliveryList(),
                    ],
                    'DeliveryFilter' => [
                        'delivery' => [$model['delivery_id']]
                    ]
                ]);
            }
        ],
        [
            'attribute' => 'orders_unbuyout',
            'label' => Yii::t('common', 'Отказы'),
            'class' => DataColumnPercent::className(),
            'totalCountAttribute' => 'orders_total',
            'hAlign' => 'right',
            'headerOptions' => [
                'data-container' => 'body',
                'data-toggle' => 'tooltip',
                'title' => Yii::t('common', 'Заказы в статусах: {statuses}', ['statuses' => implode(', ', OrderStatus::getStatusNamesForArray(OrderStatus::getNotBuyoutDeliveryList()))]),
            ],
            'link' => function ($model) {
                return Url::toRoute([
                    '/order/index/index',
                    'force_country_slug' => $model['country_slug'],
                    'DateFilter' => [
                        'dateType' => DateFilter::TYPE_DELIVERY_SENT_AT_NOT_EMPTY,
                        'dateFrom' => Yii::$app->formatter->asDate(strtotime('first day of this month', strtotime($model['month']))),
                        'dateTo' => Yii::$app->formatter->asDate(strtotime('last day of this month', strtotime($model['month']))),
                    ],
                    'StatusFilter' => [
                        'not' => [],
                        'status' => OrderStatus::getNotBuyoutDeliveryList(),
                    ],
                    'DeliveryFilter' => [
                        'delivery' => [$model['delivery_id']]
                    ]
                ]);
            }
        ],
    ],
]) ?>
