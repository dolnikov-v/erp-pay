<?php
use app\modules\report\extensions\GridView;
use yii\helpers\Html;
use app\helpers\report\Route;
use app\modules\order\models\OrderStatus;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormDaily $reportForm */
/** @var \yii\data\ArrayDataProvider $dataProvider */
?>

<div class="table-responsive">
    <?= GridView::widget([
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
        'resizableColumns' => false,
        'showPageSummary' => false,
        'columns' => [
            [
                'attribute' => 'created',
                'label' => Yii::t('common', 'Создано'),
                'format' => 'html',
                'value' => function ($model) {
                    return Html::a(
                        $model['created'],
                        [
                            '/order/index/index',
                            'DateFilter' => Route::getOrderDateFilter([
                                'from' => Yii::$app->formatter->asDate($model['timeFrom']),
                                'to' => Yii::$app->formatter->asDate($model['timeTo']),
                                'type' => 'created_at',
                            ]),
                        ],
                        [
                            'target' => '_blank',
                        ]
                    );
                }
            ],
            [
                'attribute' => 'call_sent',
                'label' => Yii::t('common', 'Отправлено в колл-центр'),
                'format' => 'html',
                'value' => function ($model) {
                    return Html::a(
                        $model['call_sent'],
                        [
                            '/order/index/index',
                            'DateFilter' => Route::getOrderDateFilter([
                                'from' => Yii::$app->formatter->asDate($model['timeFrom']),
                                'to' => Yii::$app->formatter->asDate($model['timeTo']),
                                'type' => 'c_sent_at',
                            ]),
                        ],
                        [
                            'target' => '_blank',
                        ]
                    );
                }
            ],
            [
                'attribute' => 'call_approved',
                'label' => Yii::t('common', 'Одобрено колл-центром'),
                'format' => 'html',
                'value' => function ($model) {
                    return Html::a(
                        $model['call_approved'],
                        [
                            '/order/index/index',
                            'DateFilter' => Route::getOrderDateFilter([
                                'from' => Yii::$app->formatter->asDate($model['timeFrom']),
                                'to' => Yii::$app->formatter->asDate($model['timeTo']),
                                'type' => 'c_approved_at',
                            ]),
                        ],
                        [
                            'target' => '_blank',
                        ]
                    );
                }
            ],
            [
                'attribute' => 'delivery_sent',
                'label' => Yii::t('common', 'Принято курьерской службой'),
                'format' => 'html',
                'value' => function ($model) {
                    return Html::a(
                        $model['delivery_sent'],
                        [
                            '/order/index/index',
                            'DateFilter' => Route::getOrderDateFilter([
                                'from' => Yii::$app->formatter->asDate($model['timeFrom']),
                                'to' => Yii::$app->formatter->asDate($model['timeTo']),
                                'type' => 'd_sent_at',
                            ]),
                        ],
                        [
                            'target' => '_blank',
                        ]
                    );
                }
            ],
            [
                'attribute' => 'delivery_error',
                'label' => Yii::t('common', 'Отклонено курьерской службой'),
                'format' => 'html',
                'value' => function ($model) {
                    return Html::a(
                        $model['delivery_error'],
                        [
                            '/order/index/index',
                            'DateFilter' => Route::getOrderDateFilter([
                                'from' => Yii::$app->formatter->asDate($model['timeFrom']),
                                'to' => Yii::$app->formatter->asDate($model['timeTo']),
                                'type' => 'd_created_at',
                            ]),
                            'StatusDeliveryRequestFilter' => [
                                'not' => [null => null],
                                'statusDeliveryRequest' => [null => 'error'],
                            ],
                            'StatusFilter' => Route::getOrderStatusFilter(['status' => OrderStatus::STATUS_DELIVERY_REJECTED]),
                        ],
                        [
                            'target' => '_blank',
                        ]
                    );
                }
            ],
        ],
    ]) ?>
</div>
