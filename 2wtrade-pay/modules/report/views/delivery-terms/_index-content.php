<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\report\components\filters\DateFilter;
use app\helpers\i18n\Formatter;
use app\modules\order\models\OrderStatus;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormDeliveryTerms $reportForm */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */
$allGroups = [];
if ($dataProvider->allModels) {
    foreach ($dataProvider->allModels as $model) {
        if ($model['id'] != 'old') {
            $allGroups[] = $model['id'];
        }
    }
}
?>
<?php if ($dataProvider->allModels): ?>
    <div class="wrap-report-delivery-terms">

        <?php foreach ($dataProvider->allModels as $model): ?>

            <?php if ($model['total'] || 1):?>

                <h4 class="panel-subtitle"><?= Yii::t('common', $model['name']) ?></h4>

                <table class="report-delivery-terms">
                    <tr>
                        <th><?= Yii::t('common', 'День') ?></th>
                        <?php foreach ($model['days'] as $day => $orders): ?>
                            <td <?= ($day !== 'clarification' && $day > $model['max_term_days']) ? 'class="over"' : '' ?>>
                                <?php
                                if ($day === 'clarification') {
                                    print Yii::t('common', 'на уточнении');
                                }
                                else
                                   print $day;
                                ?>
                            </td>
                        <?php endforeach; ?>
                    </tr>
                    <tr>
                        <th><?= Yii::t('common', 'Заказы') ?></th>
                        <?php foreach ($model['days'] as $day => $orders): ?>
                            <?php
                            $statuses = $reportForm->getMapStatuses();

                            $percent = '';
                            if ($model['total'] && $orders) {
                                $percent = ' (' . round($orders * 100 / $model['total'], 2) . '%)';
                            }

                            if ($day === 'clarification') {
                                $dateFrom = '';
                                $dateTo = '';
                                $notClarification = 0;
                            } else {
                                $dateFrom = Yii::$app->formatter->asDate(strtotime('-' . $day . 'days'), Formatter::getDateFormat());
                                $dateTo = $dateFrom;
                                $notClarification = 1;
                            }

                            $route = [
                                '/order/index/index',
                                'DeliveryFilter' => [
                                    'delivery' => $reportForm->getDeliveryFilter()->delivery
                                ],
                                'StatusFilter' => [
                                    'status' => $statuses['in_process']
                                ],
                                'DeliveryClarificationFilter' => [
                                    'not' => $notClarification,
                                    'list' => 1
                                ]
                            ];

                            if ($day !== 'clarification') {
                                $route['DateFilter'] = [
                                    'dateFrom' => $dateFrom,
                                    'dateTo' => $dateTo,
                                    'dateType' => DateFilter::TYPE_DELIVERY_TIME_FROM
                                ];
                            }

                            if ($model['id'] == 'old') {
                                $route['DeliveryZipGroupFilter'] = [
                                    'not' => array_fill(0, sizeof($allGroups), 1),
                                    'list' => $allGroups
                                ];
                            } else {
                                $route['DeliveryZipGroupFilter'] = [
                                    'list' => $model['id']
                                ];

                            }
                            if ($reportForm->getProductFilter()->product) {
                                $route['ProductFilter'] = [
                                    'product' => $reportForm->getProductFilter()->product
                                ];
                            }
                            if ($reportForm->getWebmasterFilter()->webmaster) {
                                $route['TextFilter'] = [
                                    'entity' => 'webmaster',
                                    'text' => $reportForm->getWebmasterFilter()->webmaster
                                ];
                            }
                            $ordersLink = Html::a($orders, Url::toRoute($route), [
                                'target' => '_blank',
                            ]);
                            ?>
                            <td><?= $ordersLink . $percent ?></td>
                        <?php endforeach; ?>
                    </tr>
                </table>
            <?php endif; ?>
        <?php endforeach; ?>
    </div>
<?php endif; ?>