<?php
use app\modules\report\assets\ReportAsset;
use app\modules\report\assets\ReportDeliveryTermsAsset;
use app\modules\report\widgets\ReportFilterDeliveryTerms;
use app\widgets\Panel;


/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormDeliveryTerms $reportForm */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */


$this->title = Yii::t('common', 'Стандартные сроки доставки');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Отчеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ReportAsset::register($this);
ReportDeliveryTermsAsset::register($this);
?>

<?= ReportFilterDeliveryTerms::widget([
    'reportForm' => $reportForm,
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Стандартные сроки доставки'),
    'alert' => $reportForm->panelAlert,
    'alertStyle' => $reportForm->panelAlertStyle,
    'border' => false,
    'withBody' => false,
    'content' => $this->render('_index-content', [
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
    ]),
]) ?>