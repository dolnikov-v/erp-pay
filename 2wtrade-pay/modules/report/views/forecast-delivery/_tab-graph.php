<?php
use miloschuman\highcharts\Highcharts;

/** @var \yii\web\View $this */
/** @var array $intervals */
/** @var array $series */

$series[0]['name'] = Yii::t('common', 'Выкуп');
$series[1]['name'] = Yii::t('common', 'Невыкуп');
$series[2]['name'] = Yii::t('common', 'В процессе');
?>

<div class="table-responsive">
    <?php
    echo Highcharts::widget([
        'options' => [
            'chart' => ['height' => 1000],
            'title' => ['text' => ''],
            'xAxis' => [
                'categories' => $intervals,
            ],
            'yAxis' => [
                'min' => 0,
                'title' => ['text' => Yii::t('common', 'Количество заказов')],
                'labels' => ['overflow' => 'justify']
            ],
            'credits' => [
                'enabled'=> false
            ],

            'series' => $series
        ],
    ]);
    ?>
</div>
