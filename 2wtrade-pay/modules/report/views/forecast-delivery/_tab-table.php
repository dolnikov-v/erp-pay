<?php
use app\modules\report\extensions\GridView;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormForecastDelivery $reportForm */
/** @var \yii\data\ArrayDataProvider $dataProvider */
?>

<div class="table-responsive">
    <?= GridView::widget([
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
        'resizableColumns' => false,
        'showPageSummary' => false,
        'layout' => '<div id="report_export_box">{toolbar}</div>{items}{pager}',
        'columns' => [
            [
                'label' => Yii::t('common', 'Дата'),
                'value' => function($model) {
                    return Yii::$app->formatter->asDate($model['interval']);
                }
            ],
            [
                'label' => Yii::t('common', 'Выкуп'),
                'attribute' => 'buyout',
            ],
            [
                'label' => Yii::t('common', 'В процессе'),
                'attribute' => 'process',
            ],
            [
                'label' => Yii::t('common', 'Невыкуп'),
                'attribute' => 'notbuyout',
            ],
        ],
    ]) ?>
</div>
