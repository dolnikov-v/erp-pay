<?php
use app\modules\report\assets\ReportAsset;
use app\modules\report\widgets\ReportFilterForecastDelivery;
use app\widgets\Panel;
use app\widgets\Nav;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormForecastDelivery $reportForm */
/** @var \yii\data\ArrayDataProvider $dataProvider */
/** @var array $intervals */
/** @var array $series */

$this->title = Yii::t('common', 'Прогнозирование доставки');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Отчеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ReportAsset::register($this);
?>

<?= ReportFilterForecastDelivery::widget([
    'reportForm' => $reportForm,
]) ?>

<?=Panel::widget([
    'nav' => new Nav([
        'tabs' => [
            [
                'label' => Yii::t('common', 'График'),
                'content' => $this->render('_tab-graph', [
                    'intervals' => $intervals,
                    'series' => $series,
                ]),
            ],
            [
                'label' => Yii::t('common', 'Таблица'),
                'content' => $this->render('_tab-table', [
                    'reportForm' => $reportForm,
                    'dataProvider' => $dataProvider,
                ]),
            ],
         ],
    ]),
]);?>
