<?php
use app\modules\report\extensions\GridViewDeliverySummary;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormDelivery $reportForm */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */
?>

<div class="table-responsive">
    <?= GridViewDeliverySummary::widget([
        'id' => 'report_grid_delivery_summary',
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
        'export' => false,
    ]) ?>
</div>
