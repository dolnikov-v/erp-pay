<?php
use app\modules\report\extensions\GridViewDelivery;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormDelivery $reportForm */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */
?>

<div class="table-responsive">
    <?= GridViewDelivery::widget([
        'id' => 'report_grid_delivery',
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
    ]) ?>
</div>
