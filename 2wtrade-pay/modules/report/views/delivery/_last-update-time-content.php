<?php

use kartik\grid\GridView;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormDeliveryLastUpdate $updateTimeReportForm */
/** @var \yii\data\ArrayDataProvider $lastUpdateDataProvider */

?>

<div class="last-update-time-content table-responsive">
    <?= GridView::widget([
        'id' => 'report_last_update_time',
        'dataProvider' => $lastUpdateDataProvider,
        'layout' => '{items}',
        'tableOptions' => ['class' => 'table table-striped'],
        'rowOptions' => ['class' => 'tr-vertical-align-middle'],
        'columns' => [
            [
                'attribute' => 'country_name',
                'label' => Yii::t('common', 'Страна'),
                'group' => true,
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'content' => function ($model) {
                    return Yii::t('common', $model['country_name']);
                }
            ],
            [
                'attribute' => 'delivery_name',
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'label' => Yii::t('common', 'Служба доставки')
            ],
            [
                'attribute' => 'last_report_update',
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'label' => Yii::t('common', 'Дата последнего обновления информации из отчетов'),
                'format' => 'date',
                'contentOptions' => function ($model) {
                    $out = [];
                    if (!empty($model['last_report_update']) && $model['last_report_update'] < strtotime('-7 days', strtotime('midnight'))) {
                        $out['style'] = 'color: red; font-weight: bold;';
                    }
                    return $out;
                },
            ],
            [
                'attribute' => 'last_api_update',
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'label' => Yii::t('common', 'Дата полного обновления информации по АПИ'),
                'format' => 'date',
                'contentOptions' => function ($model) {
                    $out = [];
                    if ($model['last_api_update'] && $model['last_api_update'] < strtotime('-3 days', strtotime('midnight'))) {
                        $out['style'] = 'color: red; font-weight: bold;';
                    }
                    return $out;
                },
            ],
            [
                'attribute' => 'last_fin_report_update',
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'label' => Yii::t('common', 'Дата последнего обновления фин. отчета'),
                'format' => 'date',
                'contentOptions' => function ($model) {
                    $out = [];
                    if (!empty($model['last_fin_report_update']) && $model['last_fin_report_update'] < strtotime('-3 days', strtotime('midnight'))) {
                        $out['style'] = 'color: red; font-weight: bold;';
                    }
                    return $out;
                },
            ],
        ]
    ]) ?>
</div>
