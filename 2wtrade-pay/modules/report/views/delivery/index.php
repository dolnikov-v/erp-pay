<?php
use app\modules\report\assets\ReportAsset;
use app\modules\report\assets\ReportDeliveryAsset;
use app\modules\report\assets\FilterCountryDeliveryAsset;
use app\modules\report\widgets\DeliveryModal;
use app\modules\report\widgets\ReportFilterDelivery;
use app\widgets\Panel;


/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormDelivery $reportForm */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */
/** @var \app\modules\report\components\ReportFormDeliveryLastUpdate $updateTimeReportForm */
/** @var \yii\data\ArrayDataProvider $lastUpdateDataProvider */
/** @var \yii\data\ArrayDataProvider $summaryProvider */


$this->title = Yii::t('common', 'Доставка');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Отчеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ReportAsset::register($this);
ReportDeliveryAsset::register($this);
FilterCountryDeliveryAsset::register($this);
?>

<?= DeliveryModal::widget() ?>

<?= ReportFilterDelivery::widget([
    'reportForm' => $reportForm,
]) ?>

<?= Panel::widget([
    'id' => 'report_delivery_last_update_time',
    'title' => Yii::t('common', 'Сводная таблица по времени последнего обновления данных'),
    'border' => false,
    'withBody' => true,
    'collapse' => true,
    'fullScreenBody' => true,
    'content' => $this->render('_last-update-time-content', [
        'updateTimeReportForm' => $updateTimeReportForm,
        'lastUpdateDataProvider' => $lastUpdateDataProvider,
    ]),
]) ?>

<?php
    if (!Yii::$app->user->isImplant) {
        echo Panel::widget([
            'id' => 'delivery_report_summary_table_panel',
            'title' => Yii::t('common', 'Сводная таблица'),
            'border' => false,
            'withBody' => true,
            'fullScreenBody' => true,
            'collapse' => true,
            'content' => $this->render('_summary-content', [
                'reportForm' => $reportForm,
                'dataProvider' => $summaryProvider,
            ])
        ]);
    }
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Доставка'),
    'border' => false,
    'withBody' => false,
    'content' => $this->render('_index-content', [
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
    ]),
]) ?>
