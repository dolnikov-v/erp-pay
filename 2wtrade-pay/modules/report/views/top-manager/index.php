<?php
use app\modules\report\assets\ReportAsset;
use app\widgets\Panel;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormTopManager $reportFormTopManager */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */

$this->title = Yii::t('common', 'Топ по менеджерам');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Отчеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ReportAsset::register($this);
?>

<?=Panel::widget([
    'border' => false,
    'withBody' => false,
    'content' => $this->render('_index-content', [
        'reportFormTopManager' => $reportFormTopManager,
        'dataProvider' => $dataProvider,
    ]),
]);?>