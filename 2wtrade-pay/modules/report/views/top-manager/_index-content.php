<?php
use app\modules\report\extensions\GridViewTopManager;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormTopManager $reportFormTopManager */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */
?>

<div class="table-responsive">
    <?=GridViewTopManager::widget([
        'id' => 'report_grid_top_manager',
        'reportForm' => $reportFormTopManager,
        'dataProvider' => $dataProvider,
    ]);?>
</div>