<?php
use app\modules\report\extensions\GridViewConsolidateByCountry;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormConsolidateByCountry $reportForm */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */
?>

<div class="table-responsive">
    <?= GridViewConsolidateByCountry::widget([
        'id' => 'report_grid_finance',
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
        'showPageSummary' => false,
        'resizableColumns' => false,
        'rowOptions' => function ($data) {
            $options = ['class' => ''];

            $proffit = $data[Yii::t('common', 'Доход')] -
                $data[Yii::t('common', 'Расходы на доставку')] -
                $data[Yii::t('common', 'Расходы на рекламу')];

            if ($proffit < 0)
                $options['class'] = 'has-error';

            if ($proffit > 0)
                $options['class'] = 'has-ok';

            return $options;
        },
    ]) ?>
</div>
