<?php
use app\modules\report\assets\ReportAsset;
use app\modules\report\widgets\ReportFilterConsolidateByCountry;
use app\widgets\Panel;
use app\modules\report\assets\ReportConsolidateByCountryAsset;
/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormConsolidateByCountry $reportForm */
/** @var yii\data\ArrayDataProvider $dataProvider */

$this->title = Yii::t('common', 'Сводный отчёт по стране');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Отчеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ReportAsset::register($this);
ReportConsolidateByCountryAsset::register($this);
?>


<?=ReportFilterConsolidateByCountry::widget([
    'reportForm' => $reportForm,
]);?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Сводный отчёт по стране'),
    'alert' => $reportForm->panelAlert,
    'alertStyle' => $reportForm->panelAlertStyle,
    'border' => false,
    'withBody' => false,
    'content' => $this->render('_index-content', [
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
    ]),
]) ?>