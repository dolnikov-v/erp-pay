<?php
use app\modules\report\extensions\GridViewCallCenter;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormCallCenter $reportForm */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */
?>

<div class="table-responsive">
    <?= GridViewCallCenter::widget([
        'id' => 'report_grid_call_center',
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
    ]) ?>
</div>
