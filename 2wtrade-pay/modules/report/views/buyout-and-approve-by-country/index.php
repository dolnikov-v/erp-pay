<?php
use app\modules\report\assets\ReportAsset;
use app\modules\report\widgets\ReportFilterApproveByCountry;
use app\modules\report\widgets\ReportFilterFinance;
use app\widgets\Panel;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormBuyoutAndApproveByCountry $reportForm */
/** @var yii\data\ArrayDataProvider $dataProvider */

$this->title = Yii::t('common', 'Выкуп и апрув по странам');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Отчеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ReportAsset::register($this);
?>

<?= \app\modules\report\widgets\ReportFilterBuyoutAndApproveByCountry::widget([
    'reportForm' => $reportForm,
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Выкуп и апрув по странам'),
    'alert' => $reportForm->panelAlert,
    'alertStyle' => $reportForm->panelAlertStyle,
    'border' => false,
    'withBody' => false,
    'content' => $this->render('_index-content', [
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
    ]),
]) ?>
