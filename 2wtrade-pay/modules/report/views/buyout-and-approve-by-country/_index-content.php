<?php
use app\modules\report\extensions\GridViewBuyoutAndApproveByCountry;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormBuyoutAndApproveByCountry $reportForm */
/** @var yii\data\ArrayDataProvider $dataProvider */
?>

<div class="table-responsive">
    <?= GridViewBuyoutAndApproveByCountry::widget([
        'id' => 'report_grid_buyout_and_approve',
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
    ]) ?>
</div>
