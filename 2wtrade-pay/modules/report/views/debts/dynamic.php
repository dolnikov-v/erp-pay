<?php

use app\modules\report\assets\ReportAsset;
use app\modules\report\assets\ReportDeliveryDebtsDynamicAsset;
use app\modules\report\widgets\ReportFilterDeliveryDebts;
use app\widgets\Panel;
use app\modules\report\widgets\GraphDebtsDynamic;
use app\widgets\Button;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormDeliveryDebts $reportForm */
/** @var \yii\data\ArrayDataProvider $dataProvider */
/** @var \app\modules\report\components\debts\models\DebtsDailyRequest $debtsDailyRequest */
/** @var integer $createdAt */

$this->title = $title ?? Yii::t('common', 'Динамика дебиторской задолженности');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Отчеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ReportAsset::register($this);
ReportDeliveryDebtsDynamicAsset::register($this);

?>

<?= ReportFilterDeliveryDebts::widget(['reportForm' => $reportForm]); ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Дебиторская задолженность') . ' <div class="pull-right">' .
        Button::widget([
            "label" => Yii::t("common", "Пересчитать данные"),
            "style" => Button::STYLE_WARNING,
            "id" => "debts_dynamic_reload",
            "attributes" => [
                "title" => Yii::t("common", "Запустить пересчет данных по выбранному фильтру. По окончании расчетов вы получите уведомление, обновите страницу"),
                "data-url" => Url::toRoute([
                    'dynamic-reload',
                    'country_id' => $debtsDailyRequest->country_id,
                    'delivery_id' => $debtsDailyRequest->delivery_id,
                    'from' => $debtsDailyRequest->date_from,
                    'to' => $debtsDailyRequest->date_to,
                ])
            ]
        ]) . '</div>',
    'border' => false,
    'withBody' => false,
    'alert' => Yii::t('common', 'Последний пересчет данных: {date}', ['date' => $createdAt ? Yii::$app->formatter->asDatetime($createdAt) : '-']),
    'content' => GraphDebtsDynamic::widget([
        'dataProvider' => $dataProvider,
        'reportForm' => $reportForm,
        'interval' => $debtsDailyRequest->interval
    ])
]) ?>
