<?php

?>

<div id="sum-popup-container" class="kv-grid-container hidden">
    <table class="kv-grid-table table table-bordered table-striped kv-table-wrap">
        <thead>
        <tr>
            <th style="width: 30.01%;"><?= Yii::t('common', 'Название') ?></th>
            <th class="kv-align-center" style="width: 20%;"><?= Yii::t('common', 'Количество единиц, шт.') ?></th>
            <th class="kv-align-center" style="width: 20%;"><?= Yii::t('common', 'Цена за единицу, USD') ?></th>
            <th class="kv-align-center" style="width: 30%;"><?= Yii::t('common', 'Сумма, USD') ?></th>
        </tr>
        </thead>
        <tbody class="table-content">
        <tr>
            <td></td>
            <td class="kv-align-center"></td>
            <td class="kv-align-center"></td>
            <td class="kv-align-center"></td>
        </tr>
        </tbody>
        <tbody class="table-footer kv-page-summary-container">
        <tr class="warning kv-page-summary">
            <td colspan="3"><?= Yii::t('common', 'Итого') ?></td>
            <td class="kv-align-center"></td>
        </tr>
        </tbody>
    </table>
</div>

<p><?= Yii::t('common', 'Учтено заказов:') ?> <a id="link_to_orders" href="#"></a></p>

<div class="table-responsive">

</div>