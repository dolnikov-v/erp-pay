<?php

use app\modules\order\models\search\filters\DateFilter;
use app\modules\order\models\search\filters\TextFilter;
use app\modules\report\extensions\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var \yii\data\ArrayDataProvider $dataProvider */
/** @var array $dateRange */

?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'resizableColumns' => false,
    'showPageSummary' => false,
    'export' => false,
    'layout' => '{items}',
    'columns' => [
        [
            'attribute' => 'payment',
            'label' => Yii::t('common', 'Номер платежки'),
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
        ],
        [
            'attribute' => 'paid_at',
            'label' => Yii::t('common', 'Дата платежа'),
            'format' => 'date',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
        ],
        [
            'attribute' => 'sum_total',
            'format' => 'raw',
            'label' => Yii::t('common', 'Сумма, относящаяся к указанному периоду'),
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-right'],
            'value' => function ($model) use ($dateRange) {
                return Html::a(number_format($model['sum_total'],
                        2) . " {$model['currency_char_code']}", Url::toRoute([
                    '/order/index/index',
                    'TextFilter' => [
                        'entity' => TextFilter::ENTITY_PAYMENT_NUMBER,
                        'text' => $model['payment'],
                    ],
                    'DateFilter' => ($dateRange['from'] && $dateRange['to']) ? [
                        'dateFrom' => Yii::$app->formatter->asDate($dateRange['from']),
                        'dateTo' => Yii::$app->formatter->asDate($dateRange['to']),
                        'dateType' => DateFilter::TYPE_ORDER_AND_DELIVERY_APPROVE
                    ] : [],
                    'force_country_slug' => $model['country_slug'],
                ]));
            }
        ],
        [
            'attribute' => 'payment_sum',
            'format' => 'raw',
            'label' => Yii::t('common', 'Сумма платежа'),
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-right'],
            'value' => function ($model) use ($dateRange) {
                return Html::a(!is_null($model['payment_sum']) ? number_format($model['payment_sum'],
                        2) . " {$model['currency_char_code']}" : '—', Url::toRoute([
                    '/order/index/index',
                    'TextFilter' => [
                        'entity' => TextFilter::ENTITY_PAYMENT_NUMBER,
                        'text' => $model['payment'],
                    ],
                    'force_country_slug' => $model['country_slug'],
                ]));
            }
        ],
        [
            'attribute' => 'sum_total_dollars',
            'format' => 'raw',
            'label' => Yii::t('common', 'Сумма, относящаяся к указанному периоду (USD)'),
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-right'],
            'value' => function ($model) use ($dateRange) {
                return Html::a(!is_null($model['sum_total_dollars']) ? number_format($model['sum_total_dollars'], 2) . " USD" : '—', Url::toRoute([
                    '/order/index/index',
                    'TextFilter' => [
                        'entity' => TextFilter::ENTITY_PAYMENT_NUMBER,
                        'text' => $model['payment'],
                    ],
                    'DateFilter' => ($dateRange['from'] && $dateRange['to']) ? [
                        'dateFrom' => Yii::$app->formatter->asDate($dateRange['from']),
                        'dateTo' => Yii::$app->formatter->asDate($dateRange['to']),
                        'dateType' => DateFilter::TYPE_ORDER_AND_DELIVERY_APPROVE
                    ] : [],
                    'force_country_slug' => $model['country_slug'],
                ]));
            }
        ],
        [
            'attribute' => 'payment_sum_dollars',
            'format' => 'raw',
            'label' => Yii::t('common', 'Сумма платежа (USD)'),
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-right'],
            'value' => function ($model) {
                return Html::a(!is_null($model['payment_sum_dollars']) ? number_format($model['payment_sum_dollars'], 2) . " USD" : '—', Url::toRoute([
                    '/order/index/index',
                    'TextFilter' => [
                        'entity' => TextFilter::ENTITY_PAYMENT_NUMBER,
                        'text' => $model['payment'],
                    ],
                    'force_country_slug' => $model['country_slug'],
                ]));
            }
        ],
    ]
]) ?>
