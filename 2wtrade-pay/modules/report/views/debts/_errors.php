<?php
/**
 * @var array[] $errors
 *
 * $arrors =
    [
        [delivery_id] =>
            [
                'name' => string,
                'errors' => [m.Y => [category => (array)days, ...], ...]
            ]
    ]
 */

?>

<?php if (!empty($errors)): ?>
    <?php foreach ($errors as $deliveryID => $errorByDelivery): ?>
        <b><?= $errorByDelivery['name'] ?></b><br/>
        <?php foreach ($errorByDelivery['errors'] as $date => $errorByMonth): ?>
            <p><?= $date ?><br/>
                <?php
                foreach ($errorByMonth as $category => $days):
                    echo "&nbsp;&nbsp;";
                    switch ($category):
                        case 'contract':
                            echo Yii::t('common', 'Отсутствуют контракты за следующие дни: {days}', ['days' => implode(', ', $days)]);
                            break;
                    endswitch;
                endforeach; ?>
            </p>
        <?php endforeach; ?>
    <?php endforeach; ?>
<?php endif; ?>