<?php

use app\models\Currency;
use kartik\grid\GridView;

/** @var \yii\data\ActiveDataProvider $dataProvider */
?>

<div class="table-responsive" id="payments-body">
    <?php if (isset($dataProvider)): ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'toolbar' => '',
            'layout' => '{items}',
            'showPageSummary' => true,
            'tableOptions' => ['class' => 'table table-striped stick'],
            'columns' => [
                [
                    'attribute' => 'paid_at',
                    'enableSorting' => false,
                    'label' => Yii::t('common', 'Дата оплаты'),
                    'value' => function ($model) {
                        return Yii::$app->formatter->asDate($model->paid_at);
                    },
                    'mergeHeader' => true,
                ],
                [
                    'attribute' => 'name',
                    'enableSorting' => false,
                    'mergeHeader' => true,
                ],
                [
                    'attribute' => 'sum',
                    'enableSorting' => false,
                    'hAlign' => 'right',
                    'format' => ['decimal', 2],
                    'headerOptions' => ['class' => 'width-150'],
                    'mergeHeader' => true,
                ],
                [
                    'attribute' => 'currency_id',
                    'enableSorting' => false,
                    'value' => function ($model) {
                        /** @var $model \app\modules\deliveryreport\models\PaymentOrder */
                        return $model->currency_id ? $model->currency->char_code : '';
                    },
                    'mergeHeader' => true,
                ],
                [
                    'attribute' => 'rate',
                    'enableSorting' => false,
                    'hAlign' => 'right',
                    'label' => Yii::t('common', 'Курс USD на дату поступления ДС'),
                    'value' => function ($model) {
                        /** @var $model \app\modules\deliveryreport\models\PaymentOrder */
                        $from = $model->currency_id;
                        $to = Currency::getUSD()->id;
                        if (!($rate = $model->getRate($to, $from))) {
                            if (!($rate = Currency::find()->convertValueToCurrency(1, $to, $from, $model->paid_at))) {
                                $rate = null;
                            }
                        }
                        if ($rate) {
                            $rate = round($rate, 4);
                        }
                        return $rate ?? '-';
                    },
                    'mergeHeader' => true,
                    'pageSummary' => Yii::t('common', 'Итого'),
                    'pageSummaryOptions' => ['style' => ['text-align' => 'right']],
                ],
                [
                    'label' => Yii::t('common', 'Сумма, USD'),
                    'attribute' => 'sum',
                    'value' => function ($model) {
                        /** @var $model \app\modules\deliveryreport\models\PaymentOrder */
                        $from = $model->currency_id;
                        $to = Currency::getUSD()->id;
                        if (!($rate = $model->getRate($to, $from))) {
                            if (!($rate = Currency::find()->convertValueToCurrency(1, $to, $from, $model->paid_at))) {
                                $rate = null;
                            }
                        }
                        if ($rate) {
                            $rate = round($rate, 4);
                        }
                        return $rate ? round($model->sum / $rate, 4) : '-';
                    },
                    'pageSummary' => true,
                    'enableSorting' => false,
                    'hAlign' => 'right',
                    'format' => ['decimal', 2],
                    'mergeHeader' => true,
                ],
            ]
        ]) ?>
    <?php endif; ?>
</div>