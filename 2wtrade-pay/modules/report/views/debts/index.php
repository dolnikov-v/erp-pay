<?php

use app\helpers\DataProvider;
use app\modules\report\assets\ReportAsset;
use app\modules\report\assets\ReportDeliveryDebtsAsset;
use app\modules\report\widgets\ReportFilterDeliveryDebts;
use app\widgets\Panel;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormDeliveryDebts $reportForm */
/** @var \yii\data\ArrayDataProvider $dataProvider */
/** @var integer $oldTime */
/** @var float $totalCounter */
/** @var float $totalCounterCurrent */
/** @var string $title */
/** @var array $errors */

$this->title = $title ?? Yii::t('common', 'Дебиторская задолженность');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Отчеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ReportAsset::register($this);
ReportDeliveryDebtsAsset::register($this);

$currencyAlert = Yii::t('common', 'Конвертация валют в USD осуществлялась по курсу на {day}', ['day' => $oldTime ? Yii::$app->formatter->asDate($oldTime) : Yii::$app->formatter->asDate(time())]);

$this->params['customTitle'] = '';
if (isset($totalCounterCurrent)) {
    $this->params['customTitle'] .= '<h3>' . Yii::t('common', 'Дебиторская задолженность с {date}: {currency_icon} {sum}', [
            'date' => Yii::$app->formatter->asDate($reportForm::DATE_FROM_DEBTS),
            'currency_icon' => '<i class="fa-usd"></i>',
            'sum' => Yii::$app->formatter->asDecimal($totalCounterCurrent, 2)
        ]) . '</h3>';
}

if (isset($totalCounter)) {
    $this->params['customTitle'] .= '<h3>' . Yii::t('common', 'Дебиторская задолженность всего: {currency_icon} {sum}', [
            'currency_icon' => '<i class="fa-usd"></i>',
            'sum' => Yii::$app->formatter->asDecimal($totalCounter, 2)
        ]) . '</h3>';
}

$debtsTotalByFilter = array_sum(\yii\helpers\ArrayHelper::getColumn($dataProvider->getModels(), 'buyout_sum_total_dollars')) - array_sum(\yii\helpers\ArrayHelper::getColumn($dataProvider->getModels(), 'charges_not_accepted_dollars'));

?>

<?= ReportFilterDeliveryDebts::widget(['reportForm' => $reportForm]); ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Дебиторская задолженность') . '<div class="debts_counter"><i class="fa-usd"></i>' . Yii::$app->formatter->asDecimal($debtsTotalByFilter, 2) . '</div>',
    'actions' => DataProvider::renderSummary($dataProvider),
    'alert' => (Yii::$app->user->can('catalog.currencyrate.index') ? \yii\helpers\Html::a($currencyAlert, \yii\helpers\Url::toRoute([
            '/catalog/currency-rate/index',
            'CurrencyRateHistorySearch' => ['created_at' => $oldTime ? Yii::$app->formatter->asDate($oldTime) : Yii::$app->formatter->asDate(time())]
        ])) : $currencyAlert) . '<br>' .
        Yii::t('common', 'Последний пересчет данных: {date}', ['date' => $oldTime ? Yii::$app->formatter->asDatetime($oldTime) : '-'])
    ,
    'alertStyle' => Panel::ALERT_PRIMARY,
    'warning' => !empty($errors) ? $this->render('_errors', ['errors' => $errors]) : '',
    'border' => false,
    'withBody' => false,
    'content' => $this->render('_index-content', [
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
    ]),
]) ?>
