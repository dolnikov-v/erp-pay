<?php

use app\modules\order\models\OrderStatus;
use app\modules\order\models\search\filters\DateFilter;
use app\modules\report\components\ReportFormDeliveryDebts;
use app\modules\report\extensions\DataColumnPercent;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use app\assets\StickHeaderAsset;
use app\widgets\Modal;
use app\modules\order\models\OrderFinanceBalanceUsd;
use app\modules\order\models\OrderFinanceFactUsd;
use \app\modules\order\models\search\filters\DebtsFilter;

/** @var \yii\web\View $this */
/** @var ReportFormDeliveryDebts $reportForm */
/** @var \yii\data\ArrayDataProvider $dataProvider */
StickHeaderAsset::register($this);

$showPageSummary = true;
if (isset($reportForm->getCountryDeliverySelectMultipleFilter()->country_ids)) {
    $countryIds = $reportForm->getCountryDeliverySelectMultipleFilter()->country_ids;
    if (is_array($countryIds) && sizeof($countryIds) == 1) {
        $showPageSummary = false;
    }
}

?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'toolbar' => '',
    'layout' => '<div id="report_export_box">{toolbar}</div>{items}',
    'tableOptions' => ['class' => 'table table-striped stick'],
    'rowOptions' => function ($model) {
        $options = ['class' => 'tr-vertical-align-middle '];
        /*if (empty($model['in_process']) && ($model['buyout'] - $model['money_received']) == 0) {
            $options['class'] .= 'custom-success';
        }*/
        return $options;
    },
    'columns' => [
        ['class' => 'kartik\grid\SerialColumn', 'width' => 0],
        [
            'attribute' => 'country_name',
            'group' => true,
            'hAlign' => 'center',
            'vAlign' => 'middle',
            'groupedRow' => true,
            'groupOddCssClass' => 'custom-info',
            'groupEvenCssClass' => 'custom-info',
            'groupFooter' => function ($model) {
                $totalInProcess = $model['total_country_orders_in_process'] . ' (' . DataColumnPercent::calculatePercent($model['total_country_orders_in_process'], $model['total_country_orders_total']) . ')';
                $totalUnBuyout = $model['total_country_orders_unbuyout'] . ' (' . DataColumnPercent::calculatePercent($model['total_country_orders_unbuyout'], $model['total_country_orders_total']) . ')';
                $response = [
                    'mergeColumns' => [[0, 3]],
                    'content' => [
                        0 => Yii::t('common', 'Итого по') . ' ' . $model['country_name'],
                        4 => Yii::$app->formatter->asDecimal($model['total_country_end_sum_dollars'], 2),
                        5 => Yii::$app->formatter->asDecimal($model['total_country_charges_accepted_dollars'] + $model['total_country_charges_not_accepted_dollars'], 2),
                        6 => Yii::$app->formatter->asDecimal($model['total_country_money_received_sum_total_dollars'], 2),
                        7 => Yii::$app->formatter->asDecimal($model['total_country_charges_accepted_dollars'], 2),
                        8 => Yii::$app->formatter->asDecimal($model['total_country_buyout_sum_total_dollars'], 2),
                        9 => Yii::$app->formatter->asDecimal($model['total_country_charges_not_accepted_dollars'], 2),
                        10 => Yii::$app->formatter->asDecimal($model['total_country_buyout_sum_total_dollars'] - $model['total_country_charges_not_accepted_dollars'], 2),
                        11 => $totalInProcess,
                        12 => $totalUnBuyout,
                    ],
                    'contentOptions' => [
                        4 => ['style' => 'text-align:right'],
                        5 => ['style' => 'text-align:right'],
                        6 => ['style' => 'text-align:right'],
                        7 => ['style' => 'text-align:right'],
                        8 => ['style' => 'text-align:right'],
                        9 => ['style' => 'text-align:right'],
                        10 => ['style' => 'text-align:right'],
                        11 => ['style' => 'text-align:right'],
                        12 => ['style' => 'text-align:right'],
                    ],
                    'options' => ['class' => 'warning', 'style' => 'font-weight:bold;']
                ];
                return $response;
            }
        ],
        [
            'attribute' => 'delivery_name',
            'label' => Yii::t('common', 'Служба доставки'),
            'group' => true,
            'groupFooter' => function ($model) use ($reportForm) {
                $totalInProcess = $model['total_delivery_orders_in_process'] . ' (' . DataColumnPercent::calculatePercent($model['total_delivery_orders_in_process'], $model['total_delivery_orders_total']) . ')';
                $totalUnBuyout = $model['total_delivery_orders_unbuyout'] . ' (' . DataColumnPercent::calculatePercent($model['total_delivery_orders_unbuyout'], $model['total_delivery_orders_total']) . ')';
                $contentDiff = [];
                foreach (array_unique(array_merge(OrderFinanceBalanceUsd::getPriceFields(), OrderFinanceFactUsd::getPriceFields(), ['expenses'])) as $key) {
                    if ($key !== 'price_cod' && !empty($model['total_delivery_' . $key])) {
                        $contentDiff[] = [($reportForm->attributeLabels()[$key] ?? $key), $model['total_delivery_' . $key . '_count'], $model['total_delivery_' . $key]];
                    }
                }
                $contentFact = [];
                foreach (array_unique(array_merge(OrderFinanceFactUsd::getPriceFields(), ['expenses'])) as $key) {
                    if ($key !== 'price_cod' && !empty($model['total_delivery_' . $key . '_fact'])) {
                        $contentFact[] = [($reportForm->attributeLabels()[$key] ?? $key), $model['total_delivery_' . $key . '_count_fact'], $model['total_delivery_' . $key . '_fact']];
                    }
                }
                $contentBalance = [];
                foreach (array_unique(array_merge(OrderFinanceBalanceUsd::getPriceFields(), ['expenses'])) as $key) {
                    if ($key !== 'price_cod' && !empty($model['total_delivery_' . $key . '_balance'])) {
                        $contentBalance[] = [($reportForm->attributeLabels()[$key] ?? $key), $model['total_delivery_' . $key . '_count_balance'], $model['total_delivery_' . $key . '_balance']];
                    }
                }
                $response = [
                    'mergeColumns' => [[0, 3]],
                    'content' => [
                        0 => Yii::t('common', 'Итого по') . ' ' . $model['delivery_name'],
                        4 => Html::tag('span', Yii::$app->formatter->asDecimal($model['total_delivery_end_sum_dollars'], 2), [
                                'class' => $model['total_delivery_price_cod'] ? 'view-sum-modal' : '',
                                'data-count' => $model['total_delivery_price_cod_count'],
                                'data-url' => Url::toRoute([
                                    '/order/index/index',
                                    'force_country_slug' => $model['country_slug'],
                                    'DateFilter' => [
                                        'dateType' => DateFilter::TYPE_DELIVERY_SENT_AT_NOT_EMPTY,
                                        'dateFrom' => Yii::$app->formatter->asDate($reportForm->monthFilter->from),
                                        'dateTo' => Yii::$app->formatter->asDate(strtotime('last day of this month', $reportForm->monthFilter->to)),
                                    ],
                                    'StatusFilter' => [
                                        'not' => [],
                                        'status' => OrderStatus::getInDeliveryAndFinanceList(),
                                    ],
                                    'DeliveryFilter' => [
                                        'delivery' => [$model['delivery_id']]
                                    ],
                                    'DebtsFilter' => [
                                        'debtsStatus' => [DebtsFilter::STATUS_COD_FACT, DebtsFilter::STATUS_COD_BALANCE]
                                    ],
                                ]),
                                'data-popup' => json_encode([[($reportForm->attributeLabels()['price_cod'] ?? 'price_cod'), $model['total_delivery_price_cod_count'], $model['total_delivery_price_cod']]], JSON_UNESCAPED_UNICODE),
                            ]),
                        5 => Html::tag('span', Yii::$app->formatter->asDecimal($model['total_delivery_charges_accepted_dollars'] + $model['total_delivery_charges_not_accepted_dollars'], 2), [
                                'class' => $contentDiff ? 'view-sum-modal' : '',
                                'data-count' => $model['total_delivery_month_count_all'],
                                'data-url' => Url::toRoute([
                                    '/order/index/index',
                                    'force_country_slug' => $model['country_slug'],
                                    'DateFilter' => [
                                        'dateType' => DateFilter::TYPE_DELIVERY_SENT_AT_NOT_EMPTY,
                                        'dateFrom' => Yii::$app->formatter->asDate($reportForm->monthFilter->from),
                                        'dateTo' => Yii::$app->formatter->asDate(strtotime('last day of this month', $reportForm->monthFilter->to)),
                                    ],
                                    'StatusFilter' => [
                                        'not' => [],
                                        'status' => OrderStatus::getInDeliveryAndFinanceList(),
                                    ],
                                    'DeliveryFilter' => [
                                        'delivery' => [$model['delivery_id']]
                                    ],
                                    'DebtsFilter' => [
                                        'debtsStatus' => [DebtsFilter::STATUS_DELIVERY_FACT, DebtsFilter::STATUS_DELIVERY_BALANCE]
                                    ],
                                ]),
                                'data-popup' => json_encode($contentDiff, JSON_UNESCAPED_UNICODE),
                            ]),
                        6 => Html::tag('span', Yii::$app->formatter->asDecimal($model['total_delivery_money_received_sum_total_dollars'], 2), [
                                'class' => $model['total_delivery_price_cod_fact'] ? 'view-sum-modal' : '',
                                'data-count' => $model['total_delivery_price_cod_count_fact'],
                                'data-url' => Url::toRoute([
                                    '/order/index/index',
                                    'force_country_slug' => $model['country_slug'],
                                    'DateFilter' => [
                                        'dateType' => DateFilter::TYPE_DELIVERY_SENT_AT_NOT_EMPTY,
                                        'dateFrom' => Yii::$app->formatter->asDate($reportForm->monthFilter->from),
                                        'dateTo' => Yii::$app->formatter->asDate(strtotime('last day of this month', $reportForm->monthFilter->to)),
                                    ],
                                    'StatusFilter' => [
                                        'not' => [],
                                        'status' => OrderStatus::getInDeliveryAndFinanceList(),
                                    ],
                                    'DeliveryFilter' => [
                                        'delivery' => [$model['delivery_id']]
                                    ],
                                    'DebtsFilter' => [
                                        'debtsStatus' => [DebtsFilter::STATUS_COD_FACT]
                                    ],
                                ]),
                                'data-popup' => json_encode([[($reportForm->attributeLabels()['price_cod'] ?? 'price_cod'), $model['total_delivery_price_cod_count_fact'], $model['total_delivery_price_cod_fact']]], JSON_UNESCAPED_UNICODE),
                            ]),
                        7 => Html::tag('span', Yii::$app->formatter->asDecimal($model['total_delivery_charges_accepted_dollars'], 2), [
                                'class' => $contentFact ? 'view-sum-modal' : '',
                                'data-count' => $model['total_delivery_month_count_all_fact'],
                                'data-url' => Url::toRoute([
                                    '/order/index/index',
                                    'force_country_slug' => $model['country_slug'],
                                    'DateFilter' => [
                                        'dateType' => DateFilter::TYPE_DELIVERY_SENT_AT_NOT_EMPTY,
                                        'dateFrom' => Yii::$app->formatter->asDate($reportForm->monthFilter->from),
                                        'dateTo' => Yii::$app->formatter->asDate(strtotime('last day of this month', $reportForm->monthFilter->to)),
                                    ],
                                    'StatusFilter' => [
                                        'not' => [],
                                        'status' => OrderStatus::getInDeliveryAndFinanceList(),
                                    ],
                                    'DeliveryFilter' => [
                                        'delivery' => [$model['delivery_id']]
                                    ],
                                    'DebtsFilter' => [
                                        'debtsStatus' => [DebtsFilter::STATUS_DELIVERY_FACT]
                                    ],
                                ]),
                                'data-popup' => json_encode($contentFact, JSON_UNESCAPED_UNICODE),
                            ]),
                        8 => Html::tag('span', Yii::$app->formatter->asDecimal($model['total_delivery_buyout_sum_total_dollars'], 2), [
                                'class' => $model['total_delivery_price_cod_balance'] ? 'view-sum-modal' : '',
                                'data-count' => $model['total_delivery_price_cod_count_balance'],
                                'data-url' => Url::toRoute([
                                    '/order/index/index',
                                    'force_country_slug' => $model['country_slug'],
                                    'DateFilter' => [
                                        'dateType' => DateFilter::TYPE_DELIVERY_SENT_AT_NOT_EMPTY,
                                        'dateFrom' => Yii::$app->formatter->asDate($reportForm->monthFilter->from),
                                        'dateTo' => Yii::$app->formatter->asDate(strtotime('last day of this month', $reportForm->monthFilter->to)),
                                    ],
                                    'StatusFilter' => [
                                        'not' => [],
                                        'status' => OrderStatus::getInDeliveryAndFinanceList(),
                                    ],
                                    'DeliveryFilter' => [
                                        'delivery' => [$model['delivery_id']]
                                    ],
                                    'DebtsFilter' => [
                                        'debtsStatus' => [DebtsFilter::STATUS_COD_BALANCE]
                                    ],
                                ]),
                                'data-popup' => json_encode([[($reportForm->attributeLabels()['price_cod'] ?? 'price_cod'), $model['total_delivery_price_cod_count_balance'], $model['total_delivery_price_cod_balance']]], JSON_UNESCAPED_UNICODE),
                            ]),
                        9 => Html::tag('span', Yii::$app->formatter->asDecimal($model['total_delivery_charges_not_accepted_dollars'], 2), [
                                'class' => $contentBalance ? 'view-sum-modal' : '',
                                'data-count' => $model['total_delivery_month_count_all_balance'],
                                'data-url' => Url::toRoute([
                                    '/order/index/index',
                                    'force_country_slug' => $model['country_slug'],
                                    'DateFilter' => [
                                        'dateType' => DateFilter::TYPE_DELIVERY_SENT_AT_NOT_EMPTY,
                                        'dateFrom' => Yii::$app->formatter->asDate($reportForm->monthFilter->from),
                                        'dateTo' => Yii::$app->formatter->asDate(strtotime('last day of this month', $reportForm->monthFilter->to)),
                                    ],
                                    'StatusFilter' => [
                                        'not' => [],
                                        'status' => OrderStatus::getInDeliveryAndFinanceList(),
                                    ],
                                    'DeliveryFilter' => [
                                        'delivery' => [$model['delivery_id']]
                                    ],
                                    'DebtsFilter' => [
                                        'debtsStatus' => [DebtsFilter::STATUS_DELIVERY_BALANCE]
                                    ],
                                ]),
                                'data-popup' => json_encode($contentBalance, JSON_UNESCAPED_UNICODE),
                            ]),
                        10 => Yii::$app->formatter->asDecimal($model['total_delivery_buyout_sum_total_dollars'] - $model['total_delivery_charges_not_accepted_dollars'], 2),
                        11 => Html::a($totalInProcess,
                            Url::toRoute([
                                '/order/index/index',
                                'force_country_slug' => $model['country_slug'],
                                'DateFilter' => [
                                    'dateType' => DateFilter::TYPE_DELIVERY_SENT_AT_NOT_EMPTY,
                                    'dateFrom' => Yii::$app->formatter->asDate($reportForm->monthFilter->from),
                                    'dateTo' => Yii::$app->formatter->asDate(strtotime('last day of this month', $reportForm->monthFilter->to)),
                                ],
                                'StatusFilter' => [
                                    'not' => [],
                                    'status' => OrderStatus::getProcessDeliveryList(),
                                ],
                                'DeliveryFilter' => [
                                    'delivery' => [$model['delivery_id']]
                                ]
                            ])),
                        12 => Html::a($totalUnBuyout,
                            Url::toRoute([
                                '/order/index/index',
                                'force_country_slug' => $model['country_slug'],
                                'DateFilter' => [
                                    'dateType' => DateFilter::TYPE_DELIVERY_SENT_AT_NOT_EMPTY,
                                    'dateFrom' => Yii::$app->formatter->asDate($reportForm->monthFilter->from),
                                    'dateTo' => Yii::$app->formatter->asDate(strtotime('last day of this month', $reportForm->monthFilter->to)),
                                ],
                                'StatusFilter' => [
                                    'not' => [],
                                    'status' => OrderStatus::getOnlyNotBuyoutInDeliveryList(),
                                ],
                                'DeliveryFilter' => [
                                    'delivery' => [$model['delivery_id']]
                                ]
                            ])),
                    ],
                    'contentOptions' => [
                        4 => ['style' => 'text-align:right'],
                        5 => ['style' => 'text-align:right'],
                        6 => ['style' => 'text-align:right'],
                        7 => ['style' => 'text-align:right'],
                        8 => ['style' => 'text-align:right'],
                        9 => ['style' => 'text-align:right'],
                        10 => ['style' => 'text-align:right'],
                        11 => ['style' => 'text-align:right'],
                        12 => ['style' => 'text-align:right'],
                    ],
                    'options' => ['class' => 'custom-warning', 'style' => 'font-style:italic;']
                ];
                return $response;
            },
            'hAlign' => 'center',
            'vAlign' => 'middle',
            'subGroupOf' => 1,
            'groupOddCssClass' => '',
            'groupEvenCssClass' => '',
            'contentOptions' => [
                'class' => 'custom-active h4'
            ],
            'format' => 'raw',
            'value' => function ($model) {
                $options = [];
                if (Yii::$app->user->can('report.debts.getpayments')) {
                    $options = [
                        'class' => 'view-payments-modal',
                        'data-url' => Url::toRoute([
                            'get-payments',
                            'from' => $model['total_delivery_min_month'],
                            'to' => $model['total_delivery_max_month'],
                            'country_id' => [$model['country_id']],
                            'delivery_id' => [$model['delivery_id']],
                        ])
                    ];
                }
                return Html::tag('span', $model['delivery_name'], $options);
            },
            'pageSummary' => Yii::t('common', 'Итого'),
        ],
        [
            'attribute' => 'month',
            'label' => Yii::t('common', 'Месяц'),
            'hAlign' => 'center',
            'format' => 'raw',
            'value' => function ($model) {
                $options = [];
                if (Yii::$app->user->can('report.debts.getpayments')) {
                    $options = [
                        'class' => 'view-payments-modal',
                        'data-url' => Url::toRoute([
                            'get-payments',
                            'month' => $model['month'],
                            'country_id' => [$model['country_id']],
                            'delivery_id' => [$model['delivery_id']],
                        ])
                    ];
                }
                return Html::tag('span', Yii::$app->formatter->asMonth($model['month']), $options);
            }
        ],
        [
            'label' => Yii::t('common', 'Выкуплено заказов на сумму, {currency}', ['currency' => 'USD']),
            'attribute' => 'end_sum_dollars',
            'hAlign' => 'right',
            'format' => ['decimal', 2],
            'headerOptions' => [
                'data-container' => 'body',
                'data-toggle' => 'tooltip',
                'title' => Yii::t('common', 'Сумма COD для заказов в статусах ({buyout_statuses})', [
                    'buyout_statuses' => implode(', ', OrderStatus::getStatusNamesForArray(OrderStatus::getBuyoutList()))
                ]),
            ],
            'content' => function ($model) use ($reportForm) {
                return Html::tag('span', Yii::$app->formatter->asDecimal($model['end_sum_dollars'], 2), [
                    'class' => !empty($model['price_cod']) ? 'view-sum-modal' : '',
                    'data-popup' => json_encode([[($reportForm->attributeLabels()['price_cod'] ?? 'price_cod'), $model['price_cod_count'] ?? 0, $model['price_cod'] ?? 0]], JSON_UNESCAPED_UNICODE),
                    'data-count' => $model['price_cod_count'] ?? 0,
                    'data-url' => Url::toRoute([
                        '/order/index/index',
                        'force_country_slug' => $model['country_slug'],
                        'DateFilter' => [
                            'dateType' => DateFilter::TYPE_DELIVERY_SENT_AT_NOT_EMPTY,
                            'dateFrom' => Yii::$app->formatter->asDate(strtotime('first day of this month', strtotime($model['month']))),
                            'dateTo' => Yii::$app->formatter->asDate(strtotime('last day of this month', strtotime($model['month']))),
                        ],
                        'StatusFilter' => [
                            'not' => [],
                            'status' => OrderStatus::getInDeliveryAndFinanceList(),
                        ],
                        'DeliveryFilter' => [
                            'delivery' => [$model['delivery_id']]
                        ],
                        'DebtsFilter' => [
                            'debtsStatus' => [DebtsFilter::STATUS_COD_FACT, DebtsFilter::STATUS_COD_BALANCE]
                        ],
                    ]),
                ]);
            },
            'pageSummary' => true
        ],
        [
            'label' => Yii::t('common', 'Услуги КС, {currency}', ['currency' => 'USD']),
            'attribute' => 'charges_total_dollars',
            'hAlign' => 'right',
            'format' => ['decimal', 2],
            'headerOptions' => [
                'data-container' => 'body',
                'data-toggle' => 'tooltip',
                'title' => Yii::t('common', 'Сумма расходов на доставку заказов в статусах ({charges_statuses})', [
                    'charges_statuses' => implode(', ', OrderStatus::getStatusNamesForArray(array_merge(OrderStatus::getBuyoutList(), OrderStatus::getNotBuyoutDeliveryList())))
                ]),
            ],
            'content' => function ($model) use ($reportForm) {
                $popupContent = [];
                foreach (array_unique(array_merge(OrderFinanceBalanceUsd::getPriceFields(), OrderFinanceFactUsd::getPriceFields(), ['expenses'])) as $key) {
                    if ($key !== 'price_cod' && !empty($model[$key])) {
                        $popupContent[] = [($reportForm->attributeLabels()[$key] ?? $key), $model[$key . '_count'], $model[$key]];
                    }
                }
                return Html::tag('span', Yii::$app->formatter->asDecimal($model['charges_total_dollars'], 2), [
                    'class' => !empty($popupContent) ? 'view-sum-modal' : '',
                    'data-popup' => json_encode($popupContent, JSON_UNESCAPED_UNICODE),
                    'data-count' => $model['month_count_all'] ?? 0,
                    'data-url' => Url::toRoute([
                        '/order/index/index',
                        'force_country_slug' => $model['country_slug'],
                        'DateFilter' => [
                            'dateType' => DateFilter::TYPE_DELIVERY_SENT_AT_NOT_EMPTY,
                            'dateFrom' => Yii::$app->formatter->asDate(strtotime('first day of this month', strtotime($model['month']))),
                            'dateTo' => Yii::$app->formatter->asDate(strtotime('last day of this month', strtotime($model['month']))),
                        ],
                        'StatusFilter' => [
                            'not' => [],
                            'status' => OrderStatus::getInDeliveryAndFinanceList(),
                        ],
                        'DeliveryFilter' => [
                            'delivery' => [$model['delivery_id']]
                        ],
                        'DebtsFilter' => [
                            'debtsStatus' => [DebtsFilter::STATUS_DELIVERY_FACT, DebtsFilter::STATUS_DELIVERY_BALANCE]
                        ],
                    ]),
                ]);
            },
            'pageSummary' => true
        ],
        [
            'label' => Yii::t('common', 'Денег получено от выкупленных заказов, {currency}', ['currency' => 'USD']),
            'attribute' => 'money_received_sum_total_dollars',
            'hAlign' => 'right',
            'format' => ['decimal', 2],
            'headerOptions' => [
                'data-container' => 'body',
                'data-toggle' => 'tooltip',
                'title' => Yii::t('common', 'Сумма COD для заказов в статусах ({buyout_statuses})', [
                    'buyout_statuses' => implode(', ', OrderStatus::getStatusNamesForArray(OrderStatus::getOnlyMoneyReceivedList()))
                ]),
                'class' => 'pale_green'
            ],
            'content' => function ($model) use ($reportForm) {
                return Html::tag('span', Yii::$app->formatter->asDecimal($model['money_received_sum_total_dollars'], 2), [
                    'class' => !empty($model['price_cod_fact']) ? 'view-sum-modal' : '',
                    'data-count' => $model['price_cod_count_fact'] ?? 0,
                    'data-url' => Url::toRoute([
                        '/order/index/index',
                        'force_country_slug' => $model['country_slug'],
                        'DateFilter' => [
                            'dateType' => DateFilter::TYPE_DELIVERY_SENT_AT_NOT_EMPTY,
                            'dateFrom' => Yii::$app->formatter->asDate(strtotime('first day of this month', strtotime($model['month']))),
                            'dateTo' => Yii::$app->formatter->asDate(strtotime('last day of this month', strtotime($model['month']))),
                        ],
                        'StatusFilter' => [
                            'not' => [],
                            'status' => OrderStatus::getInDeliveryAndFinanceList(),
                        ],
                        'DeliveryFilter' => [
                            'delivery' => [$model['delivery_id']]
                        ],
                        'DebtsFilter' => [
                            'debtsStatus' => [DebtsFilter::STATUS_COD_FACT]
                        ],
                    ]),
                    'data-popup' => json_encode([[($reportForm->attributeLabels()['price_cod'] ?? 'price_cod'), $model['price_cod_count_fact'] ?? 0, $model['price_cod_fact'] ?? 0]], JSON_UNESCAPED_UNICODE),
                ]);
            },
            'contentOptions' => ['class' => 'pale_green'],
            'pageSummary' => true
        ],
        [
            'label' => Yii::t('common', 'Услуги КС, {currency}', ['currency' => 'USD']),
            'attribute' => 'charges_accepted_dollars',
            'hAlign' => 'right',
            'format' => ['decimal', 2],
            'headerOptions' => [
                'data-container' => 'body',
                'data-toggle' => 'tooltip',
                'title' => Yii::t('common', 'Сумма расходов на доставку заказов в статусах ({charges_statuses})', [
                    'charges_statuses' => implode(', ', OrderStatus::getStatusNamesForArray(array_merge(OrderStatus::getOnlyMoneyReceivedList(), OrderStatus::getOnlyNotBuyoutDoneList())))
                ]),
                'class' => 'pale_green'
            ],
            'content' => function ($model) use ($reportForm) {
                $popupContent = [];
                foreach (array_merge(OrderFinanceFactUsd::getPriceFields(), ['expenses']) as $key) {
                    if ($key !== 'price_cod' && !empty($model[$key . '_fact'])) {
                        $popupContent[] = [($reportForm->attributeLabels()[$key] ?? $key), $model[$key . '_count_fact'], $model[$key . '_fact']];
                    }
                }
                return Html::tag('span', Yii::$app->formatter->asDecimal($model['charges_accepted_dollars'], 2), [
                    'class' => !empty($popupContent) ? 'view-sum-modal' : '',
                    'data-count' => $model['month_count_all_fact'] ?? 0,
                    'data-url' => Url::toRoute([
                        '/order/index/index',
                        'force_country_slug' => $model['country_slug'],
                        'DateFilter' => [
                            'dateType' => DateFilter::TYPE_DELIVERY_SENT_AT_NOT_EMPTY,
                            'dateFrom' => Yii::$app->formatter->asDate(strtotime('first day of this month', strtotime($model['month']))),
                            'dateTo' => Yii::$app->formatter->asDate(strtotime('last day of this month', strtotime($model['month']))),
                        ],
                        'StatusFilter' => [
                            'not' => [],
                            'status' => OrderStatus::getInDeliveryAndFinanceList(),
                        ],
                        'DeliveryFilter' => [
                            'delivery' => [$model['delivery_id']]
                        ],
                        'DebtsFilter' => [
                            'debtsStatus' => [DebtsFilter::STATUS_DELIVERY_FACT]
                        ],
                    ]),
                    'data-popup' => json_encode($popupContent, JSON_UNESCAPED_UNICODE),
                ]);
            },
            'contentOptions' => ['class' => 'pale_green'],
            'pageSummary' => true
        ],
        [
            'label' => Yii::t('common', 'Денег не получено от выкупленных заказов, {currency}', ['currency' => 'USD']),
            'attribute' => 'buyout_sum_total_dollars',
            'hAlign' => 'right',
            'format' => ['decimal', 2],
            'headerOptions' => [
                'data-container' => 'body',
                'data-toggle' => 'tooltip',
                'title' => Yii::t('common', 'Сумма COD для заказов в статусах ({buyout_statuses})', [
                    'buyout_statuses' => implode(', ', OrderStatus::getStatusNamesForArray(OrderStatus::getOnlyBuyoutList())),
                ]),
                'class' => 'pale_red'
            ],
            'content' => function ($model) use ($reportForm) {
                return Html::tag('span', Yii::$app->formatter->asDecimal($model['buyout_sum_total_dollars'], 2), [
                    'class' => !empty($model['price_cod_balance']) ? 'view-sum-modal' : '',
                    'data-count' => $model['price_cod_count_balance'] ?? 0,
                    'data-url' => Url::toRoute([
                        '/order/index/index',
                        'force_country_slug' => $model['country_slug'],
                        'DateFilter' => [
                            'dateType' => DateFilter::TYPE_DELIVERY_SENT_AT_NOT_EMPTY,
                            'dateFrom' => Yii::$app->formatter->asDate(strtotime('first day of this month', strtotime($model['month']))),
                            'dateTo' => Yii::$app->formatter->asDate(strtotime('last day of this month', strtotime($model['month']))),
                        ],
                        'StatusFilter' => [
                            'not' => [],
                            'status' => OrderStatus::getInDeliveryAndFinanceList(),
                        ],
                        'DeliveryFilter' => [
                            'delivery' => [$model['delivery_id']]
                        ],
                        'DebtsFilter' => [
                            'debtsStatus' => [DebtsFilter::STATUS_COD_BALANCE]
                        ],
                    ]),
                    'data-popup' => json_encode([[($reportForm->attributeLabels()['price_cod'] ?? 'price_cod'), $model['price_cod_count_balance'] ?? 0, $model['price_cod_balance'] ?? 0]], JSON_UNESCAPED_UNICODE),
                ]);
            },
            'contentOptions' => ['class' => 'pale_red'],
            'pageSummary' => true
        ],
        [
            'label' => Yii::t('common', 'Услуги КС, {currency}', ['currency' => 'USD']),
            'attribute' => 'charges_not_accepted_dollars',
            'hAlign' => 'right',
            'format' => ['decimal', 2],
            'headerOptions' => [
                'data-container' => 'body',
                'data-toggle' => 'tooltip',
                'title' => Yii::t('common', 'Сумма расходов на доставку заказов в статусах ({charges_statuses})', [
                    'charges_statuses' => implode(', ', OrderStatus::getStatusNamesForArray(array_merge(OrderStatus::getOnlyBuyoutList(), OrderStatus::getNotBuyoutInDeliveryProcessList())))
                ]),
                'class' => 'pale_red'
            ],
            'content' => function ($model) use ($reportForm) {
                $popupContent = [];
                foreach (array_merge(OrderFinanceFactUsd::getPriceFields(), ['expenses']) as $key) {
                    if ($key !== 'price_cod' && !empty($model[$key . '_balance'])) {
                        $popupContent[] = [($reportForm->attributeLabels()[$key] ?? $key), $model[$key . '_count_balance'], $model[$key . '_balance']];
                    }
                }
                return Html::tag('span', Yii::$app->formatter->asDecimal($model['charges_not_accepted_dollars'], 2), [
                    'class' => !empty($popupContent) ? 'view-sum-modal' : '',
                    'data-count' => $model['month_count_all_balance'] ?? 0,
                    'data-url' => Url::toRoute([
                        '/order/index/index',
                        'force_country_slug' => $model['country_slug'],
                        'DateFilter' => [
                            'dateType' => DateFilter::TYPE_DELIVERY_SENT_AT_NOT_EMPTY,
                            'dateFrom' => Yii::$app->formatter->asDate(strtotime('first day of this month', strtotime($model['month']))),
                            'dateTo' => Yii::$app->formatter->asDate(strtotime('last day of this month', strtotime($model['month']))),
                        ],
                        'StatusFilter' => [
                            'not' => [],
                            'status' => OrderStatus::getInDeliveryAndFinanceList(),
                        ],
                        'DeliveryFilter' => [
                            'delivery' => [$model['delivery_id']]
                        ],
                        'DebtsFilter' => [
                            'debtsStatus' => [DebtsFilter::STATUS_DELIVERY_BALANCE]
                        ],
                    ]),
                    'data-popup' => json_encode($popupContent, JSON_UNESCAPED_UNICODE),
                ]);
            },
            'contentOptions' => ['class' => 'pale_red'],
            'pageSummary' => true
        ],
        [
            'label' => Yii::t('common', 'Сумма задолженности'),
            'hAlign' => 'right',
            'format' => ['decimal', 2],
            'headerOptions' => [
                'data-container' => 'body',
                'data-toggle' => 'tooltip',
                'title' => Yii::t('common', 'Разница колонок "Денег не получено от выкупленных заказов" и "Услуги КС"'),
                'class' => 'pale_red'
            ],
            'value' => function ($model) {
                return ($model['buyout_sum_total_dollars'] - $model['charges_not_accepted_dollars']);
            },
            'contentOptions' => ['class' => 'pale_red'],
            'pageSummary' => true
        ],
        [
            'attribute' => 'orders_in_process',
            'label' => Yii::t('common', 'В процессе'),
            'class' => DataColumnPercent::className(),
            'totalCountAttribute' => 'orders_total',
            'hAlign' => 'right',
            'headerOptions' => [
                'data-container' => 'body',
                'data-toggle' => 'tooltip',
                'title' => Yii::t('common', 'Заказы в статусах: {statuses}', ['statuses' => implode(', ', OrderStatus::getStatusNamesForArray(OrderStatus::getProcessDeliveryList()))]),
            ],
            'link' => function ($model) {
                return Url::toRoute([
                    '/order/index/index',
                    'force_country_slug' => $model['country_slug'],
                    'DateFilter' => [
                        'dateType' => DateFilter::TYPE_DELIVERY_SENT_AT_NOT_EMPTY,
                        'dateFrom' => Yii::$app->formatter->asDate(strtotime('first day of this month', strtotime($model['month']))),
                        'dateTo' => Yii::$app->formatter->asDate(strtotime('last day of this month', strtotime($model['month']))),
                    ],
                    'StatusFilter' => [
                        'not' => [],
                        'status' => OrderStatus::getProcessDeliveryList(),
                    ],
                    'DeliveryFilter' => [
                        'delivery' => [$model['delivery_id']]
                    ]
                ]);
            }
        ],
        [
            'attribute' => 'orders_unbuyout',
            'label' => Yii::t('common', 'Отказы'),
            'class' => DataColumnPercent::className(),
            'totalCountAttribute' => 'orders_total',
            'hAlign' => 'right',
            'headerOptions' => [
                'data-container' => 'body',
                'data-toggle' => 'tooltip',
                'title' => Yii::t('common', 'Заказы в статусах: {statuses}', ['statuses' => implode(', ', OrderStatus::getStatusNamesForArray(OrderStatus::getOnlyNotBuyoutInDeliveryList()))]),
            ],
            'link' => function ($model) {
                return Url::toRoute([
                    '/order/index/index',
                    'force_country_slug' => $model['country_slug'],
                    'DateFilter' => [
                        'dateType' => DateFilter::TYPE_DELIVERY_SENT_AT_NOT_EMPTY,
                        'dateFrom' => Yii::$app->formatter->asDate(strtotime('first day of this month', strtotime($model['month']))),
                        'dateTo' => Yii::$app->formatter->asDate(strtotime('last day of this month', strtotime($model['month']))),
                    ],
                    'StatusFilter' => [
                        'not' => [],
                        'status' => OrderStatus::getOnlyNotBuyoutInDeliveryList(),
                    ],
                    'DeliveryFilter' => [
                        'delivery' => [$model['delivery_id']]
                    ]
                ]);
            }
        ],
    ],
    'showPageSummary' => $showPageSummary
]) ?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'export' => [
        'label' => Yii::t('common', 'Экспорт'),
        'showConfirmAlert' => false,
        'fontAwesome' => true,
        'target' => '_self',
        'header' => false,
    ],
    'exportConfig' => [
        GridView::EXCEL => [
            'filename' => 'Debts_from_' . Yii::$app->formatter->asDate($reportForm->getMonthFilter()->from, 'php:Y-m') . '_to_' . Yii::$app->formatter->asDate($reportForm->getMonthFilter()->to, 'php:Y-m') . '_' . date('YmdHis')
        ],
        GridView::PDF => [
            'filename' => 'Debts_from_' . Yii::$app->formatter->asDate($reportForm->getMonthFilter()->from, 'php:Y-m') . '_to_' . Yii::$app->formatter->asDate($reportForm->getMonthFilter()->to, 'php:Y-m') . '_' . date('YmdHis')
        ],
        GridView::CSV => [
            'filename' => 'Debts_from_' . Yii::$app->formatter->asDate($reportForm->getMonthFilter()->from, 'php:Y-m') . '_to_' . Yii::$app->formatter->asDate($reportForm->getMonthFilter()->to, 'php:Y-m') . '_' . date('YmdHis')
        ],
        GridView::JSON => [
            'filename' => 'Debts_from_' . Yii::$app->formatter->asDate($reportForm->getMonthFilter()->from, 'php:Y-m') . '_to_' . Yii::$app->formatter->asDate($reportForm->getMonthFilter()->to, 'php:Y-m') . '_' . date('YmdHis')
        ],
    ],
    'toolbar' => '{export}',
    'layout' => '<div id="report_export_box">{toolbar}</div>{items}',
    'tableOptions' => ['style' => 'display: none;'],
    'columns' => [
        ['class' => 'kartik\grid\SerialColumn', 'width' => 0],
        [
            'attribute' => 'country_name',
            'contentOptions' => ['style' => 'text-align:center;vertical-align:middle;'],
            'group' => true,
            'hAlign' => 'center',
            'vAlign' => 'middle',
            'groupedRow' => true,
            'groupOddCssClass' => 'custom-info',
            'groupEvenCssClass' => 'custom-info',
            'groupFooter' => function ($model) {
                $totalInProcess = $model['total_country_orders_in_process'] . ' (' . DataColumnPercent::calculatePercent($model['total_country_orders_in_process'], $model['total_country_orders_total']) . ')';
                $totalUnBuyout = $model['total_country_orders_unbuyout'] . ' (' . DataColumnPercent::calculatePercent($model['total_country_orders_unbuyout'], $model['total_country_orders_total']) . ')';
                $response = [
                    'mergeColumns' => [[0, 3]],
                    'content' => [
                        0 => Yii::t('common', 'Итого по') . ' ' . $model['country_name'],
                        4 => Yii::$app->formatter->asDecimal($model['total_country_end_sum_dollars'], 2),
                        5 => Yii::$app->formatter->asDecimal($model['total_country_charges_accepted_dollars'] + $model['total_country_charges_not_accepted_dollars'], 2),
                        6 => Yii::$app->formatter->asDecimal($model['total_country_money_received_sum_total_dollars'], 2),
                        7 => Yii::$app->formatter->asDecimal($model['total_country_charges_accepted_dollars'], 2),
                        8 => Yii::$app->formatter->asDecimal($model['total_country_buyout_sum_total_dollars'], 2),
                        9 => Yii::$app->formatter->asDecimal($model['total_country_charges_not_accepted_dollars'], 2),
                        10 => $totalInProcess,
                        11 => $totalUnBuyout,
                    ],
                    'contentOptions' => [
                        4 => ['style' => 'text-align:right'],
                        5 => ['style' => 'text-align:right'],
                        6 => ['style' => 'text-align:right'],
                        7 => ['style' => 'text-align:right'],
                        8 => ['style' => 'text-align:right'],
                        9 => ['style' => 'text-align:right'],
                        10 => ['style' => 'text-align:right'],
                        11 => ['style' => 'text-align:right'],
                    ],
                    'options' => ['class' => 'warning', 'style' => 'font-weight:bold;']
                ];
                return $response;
            }
        ],
        [
            'attribute' => 'delivery_name',
            'label' => Yii::t('common', 'Служба доставки'),
            'contentOptions' => ['style' => 'text-align:center;vertical-align:middle;'],
            'groupOddCssClass' => '',
            'groupEvenCssClass' => '',
            'group' => true,
            'groupFooter' => function ($model) use ($reportForm) {
                $totalInProcess = $model['total_delivery_orders_in_process'] . ' (' . DataColumnPercent::calculatePercent($model['total_delivery_orders_in_process'], $model['total_delivery_orders_total']) . ')';
                $totalUnBuyout = $model['total_delivery_orders_unbuyout'] . ' (' . DataColumnPercent::calculatePercent($model['total_delivery_orders_unbuyout'], $model['total_delivery_orders_total']) . ')';
                $response = [
                    'mergeColumns' => [[0, 3]],
                    'content' => [
                        0 => Yii::t('common', 'Итого по') . ' ' . $model['delivery_name'],
                        4 => Yii::$app->formatter->asDecimal($model['total_delivery_end_sum_dollars'], 2),
                        5 => Yii::$app->formatter->asDecimal($model['total_delivery_charges_accepted_dollars'] + $model['total_delivery_charges_not_accepted_dollars'], 2),
                        6 => Yii::$app->formatter->asDecimal($model['total_delivery_money_received_sum_total_dollars'], 2),
                        7 => Yii::$app->formatter->asDecimal($model['total_delivery_charges_accepted_dollars'], 2),
                        8 => Yii::$app->formatter->asDecimal($model['total_delivery_buyout_sum_total_dollars'], 2),
                        9 => Yii::$app->formatter->asDecimal($model['total_delivery_charges_not_accepted_dollars'], 2),
                        10 => Yii::$app->formatter->asDecimal($model['total_delivery_buyout_sum_total_dollars'] - $model['total_delivery_charges_not_accepted_dollars'], 2),
                        11 => $totalInProcess,
                        12 => $totalUnBuyout,
                    ],
                    'contentOptions' => [
                        4 => ['style' => 'text-align:right'],
                        5 => ['style' => 'text-align:right'],
                        6 => ['style' => 'text-align:right'],
                        7 => ['style' => 'text-align:right'],
                        8 => ['style' => 'text-align:right'],
                        9 => ['style' => 'text-align:right'],
                        10 => ['style' => 'text-align:right'],
                        11 => ['style' => 'text-align:right'],
                        12 => ['style' => 'text-align:right'],
                    ],
                    'options' => ['class' => 'custom-warning', 'style' => 'font-style:italic;']
                ];
                return $response;
            },
            'subGroupOf' => 1,
            'pageSummary' => Yii::t('common', 'Итого'),
        ],
        [
            'attribute' => 'month',
            'label' => Yii::t('common', 'Месяц'),
            'format' => 'raw',
            'value' => function ($model) {
                return Yii::$app->formatter->asMonth($model['month']);
            }
        ],
        [
            'label' => Yii::t('common', 'Выкуплено заказов на сумму, {currency}', ['currency' => 'USD']),
            'attribute' => 'end_sum_dollars',
            'format' => ['decimal', 2],
            'xlFormat' => "\#\,\#\#0\.00",
            'content' => function ($model) {
                return Yii::$app->formatter->asDecimal($model['end_sum_dollars'], 2);
            },
            'pageSummary' => true
        ],
        [
            'label' => Yii::t('common', 'Услуги КС, {currency}', ['currency' => 'USD']),
            'attribute' => 'charges_total_dollars',
            'format' => ['decimal', 2],
            'xlFormat' => "\#\,\#\#0\.00",
            'content' => function ($model) {
                return Yii::$app->formatter->asDecimal($model['charges_total_dollars'], 2);
            },
            'pageSummary' => true
        ],
        [
            'label' => Yii::t('common', 'Денег получено от выкупленных заказов, {currency}', ['currency' => 'USD']),
            'attribute' => 'money_received_sum_total_dollars',
            'format' => ['decimal', 2],
            'xlFormat' => "\#\,\#\#0\.00",
            'content' => function ($model) {
                return Yii::$app->formatter->asDecimal($model['money_received_sum_total_dollars'], 2);
            },
            'pageSummary' => true
        ],
        [
            'label' => Yii::t('common', 'Услуги КС, {currency}', ['currency' => 'USD']),
            'attribute' => 'charges_accepted_dollars',
            'format' => ['decimal', 2],
            'xlFormat' => "\#\,\#\#0\.00",
            'content' => function ($model) {
                return Yii::$app->formatter->asDecimal($model['charges_accepted_dollars'], 2);
            },
            'pageSummary' => true
        ],
        [
            'label' => Yii::t('common', 'Денег не получено от выкупленных заказов, {currency}', ['currency' => 'USD']),
            'attribute' => 'buyout_sum_total_dollars',
            'format' => ['decimal', 2],
            'xlFormat' => "\#\,\#\#0\.00",
            'content' => function ($model) {
                return Yii::$app->formatter->asDecimal($model['buyout_sum_total_dollars'], 2);
            },
            'pageSummary' => true
        ],
        [
            'label' => Yii::t('common', 'Услуги КС, {currency}', ['currency' => 'USD']),
            'attribute' => 'charges_not_accepted_dollars',
            'format' => ['decimal', 2],
            'xlFormat' => "\#\,\#\#0\.00",
            'content' => function ($model) {
                return Yii::$app->formatter->asDecimal($model['charges_not_accepted_dollars'], 2);
            },
            'pageSummary' => true
        ],
        [
            'label' => Yii::t('common', 'Сумма задолженности'),
            'format' => ['decimal', 2],
            'xlFormat' => "\#\,\#\#0\.00",
            'value' => function ($model) {
                return ($model['buyout_sum_total_dollars'] - $model['charges_not_accepted_dollars']);
            },
            'pageSummary' => true
        ],
        [
            'attribute' => 'orders_in_process',
            'contentOptions' => ['style' => 'text-align:right;'],
            'label' => Yii::t('common', 'В процессе'),
            'class' => DataColumnPercent::className(),
            'totalCountAttribute' => 'orders_total',
        ],
        [
            'attribute' => 'orders_unbuyout',
            'contentOptions' => ['style' => 'text-align:right;'],
            'label' => Yii::t('common', 'Отказы'),
            'class' => DataColumnPercent::className(),
            'totalCountAttribute' => 'orders_total',
        ],
    ],
    'showPageSummary' => $showPageSummary
]) ?>

<?= Modal::widget([
    'id' => 'payments_modal',
    'title' => Yii::t('common', 'Список платежей'),
    'body' => $this->render('_modal-payments'),
    'size' => Modal::SIZE_LARGE,
]) ?>

<?= Modal::widget([
    'id' => 'sum_modal',
    'title' => Yii::t('common', 'Список учтенных данных'),
    'body' => $this->render('_modal-sum'),
]) ?>