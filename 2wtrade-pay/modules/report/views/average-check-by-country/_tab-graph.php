<?php
use app\modules\report\widgets\GraphAverageCheckByCountry;
use app\modules\report\widgets\GraphAverageCheckByBuyOutCountry;

/** @var \yii\web\View $this */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */
?>

<div class="table-responsive">
    <?=GraphAverageCheckByCountry::widget([
    'dataProvider' => $dataProvider,
    'reportForm' => $reportForm
]);?>
</div>

<div class="table-responsive">
    <?=GraphAverageCheckByBuyOutCountry::widget([
        'dataProvider' => $dataProvider,
        'reportForm' => $reportForm
    ]);?>
</div>