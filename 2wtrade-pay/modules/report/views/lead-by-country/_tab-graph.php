<?php
use app\modules\report\widgets\GraphLeadByCountry;

/** @var \yii\web\View $this */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */
?>

<div class="table-responsive">
    <?=GraphLeadByCountry::widget([
    'dataProvider' => $dataProvider,
    'reportForm' => $reportForm
]);?>
</div>