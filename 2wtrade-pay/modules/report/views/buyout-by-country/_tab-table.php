<?php

use app\modules\report\extensions\GridViewBuyoutByCountry;

/** @var \yii\web\View $this */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */
?>
<br>
<br>

<div class="table-responsive">
    <?= GridViewBuyoutByCountry::widget([
        'id' => 'report_buyout_by_country',
        'dataProvider' => $dataProvider,
        'reportForm' => $reportForm,
        'layout' => '{items}',
        'tableOptions' => ['class' => 'table table-striped'],
        'rowOptions' => ['class' => 'tr-vertical-align-middle '],
    ]); ?>
</div>