<?php
use app\modules\report\widgets\GraphBuyoutByCountry;

/** @var \yii\web\View $this */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */
?>

<div class="table-responsive">
    <?=GraphBuyoutByCountry::widget([
    'dataProvider' => $dataProvider,
    'reportForm' => $reportForm
]);?>
</div>