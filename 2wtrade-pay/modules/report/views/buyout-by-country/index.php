<?php
use app\helpers\DataProvider;
use app\modules\report\assets\ReportBuyoutByCountryAsset;
use app\modules\report\assets\ReportAsset;
use app\modules\report\widgets\ReportFilterBuyoutByCountry;
use app\widgets\Nav;
use app\widgets\Panel;
use yii\helpers\Url;
use yii\web\View;
/** @var app\modules\report\components\ReportFormBuyOutByCountry $reportForm */
/** @var yii\data\ArrayDataProvider $dataProvider */
/** @var yii\data\ArrayDataProvider $daysProvider */

$this->title = Yii::t('common', 'Выкуп по группам стран');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Отчеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

$this->registerJs("var I18n = {};", View::POS_HEAD);
$this->registerJs("I18n['getCountryUrl'] = '" . Url::toRoute(['get-country']) . "';", View::POS_HEAD);

ReportAsset::register($this);
ReportBuyoutByCountryAsset::register($this);
?>

<?=ReportFilterBuyoutByCountry::widget([
    'reportForm' => $reportForm,
]);?>

<?=Panel::widget([
    'nav' => new Nav([
        'tabs' => [
            [
                'label' => Yii::t('common', 'Графики'),
                'content' => $this->render('_tab-graph', [
                    'dataProvider' => $dataProvider,
                    'reportForm' => $reportForm,
                ]),
            ],
            [
                'label' => Yii::t('common', 'Таблица'),
                'content' => $this->render('_tab-table', [
                    'dataProvider' => $dataProvider,
                    'reportForm' => $reportForm,
                ]),
            ],
        ],
    ]),
]);?>
