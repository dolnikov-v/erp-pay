<?php
use app\helpers\DataProvider;
use app\modules\report\widgets\ReportFilterCallCenterOperatorsOnline;
use app\modules\report\assets\ReportAsset;
use app\widgets\Nav;
use app\widgets\Panel;
use yii\helpers\Url;
use yii\web\View;
use app\modules\report\assets\ReportCallCenterOperatorsOnlineAsset;
/** @var app\modules\report\components\ReportFormAdvertising $reportForm */
/** @var yii\data\ArrayDataProvider $dataProvider */
/** @var yii\data\ArrayDataProvider $daysProvider */

$this->title = Yii::t('common', 'Операторы онлайн');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Отчеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ReportAsset::register($this);
ReportCallCenterOperatorsOnlineAsset::register($this);
?>

<?=ReportFilterCallCenterOperatorsOnline::widget([
    'reportForm' => $reportForm,
]);?>

<?=Panel::widget([
    'nav' => new Nav([
        'tabs' => [
            [
                'label' => Yii::t('common', 'Таблица'),
                'content' => $this->render('_tab-table', [
                    'dataProvider' => $dataProvider,
                    'reportForm' => $reportForm,
                ]),
            ]
        ],
    ]),
]);?>
