<?php
use app\modules\report\extensions\GridViewCallCenterOperatorsOnline;

/** @var \yii\web\View $this */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */
/** @var app\modules\report\components\ReportFormAdvertising $reportForm */
?>

<br>
<br>
<div class="table-responsive">
    <?= GridViewCallCenterOperatorsOnline::widget([
        'id' => 'report_call_center_operators_online',
        'dataProvider' => $dataProvider,
        'reportForm' => $reportForm,
    ]); ?>
</div>

