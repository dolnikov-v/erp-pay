<?php
use app\modules\report\extensions\GridViewFinance;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormFinance $reportForm */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */
?>

<div class="table-responsive">
    <?= GridViewFinance::widget([
        'id' => 'report_grid_finance',
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
    ]) ?>
</div>
