<?php
use app\modules\report\assets\ReportAsset;
use app\modules\report\assets\FilterCountryDeliveryPartnersAsset;
use app\modules\report\widgets\ReportFilterFinance;
use app\widgets\Panel;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormFinance $reportForm */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */

$this->title = Yii::t('common', 'Финансы');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Отчеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ReportAsset::register($this);
FilterCountryDeliveryPartnersAsset::register($this);
?>

<?= ReportFilterFinance::widget([
    'reportForm' => $reportForm,
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Финансы'),
    'alert' => $reportForm->panelAlert,
    'alertStyle' => $reportForm->panelAlertStyle,
    'border' => false,
    'withBody' => false,
    'content' => $this->render('_index-content', [
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
    ]),
]) ?>
