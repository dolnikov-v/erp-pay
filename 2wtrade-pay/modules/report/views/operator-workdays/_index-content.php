<?php
use app\modules\report\extensions\GridViewOperatorWorkdays;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormOperatorWorkdays $reportForm */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */
?>

<div class="table-responsive">
    <?= GridViewOperatorWorkdays::widget([
        'id' => 'report_grid_operator_workdays',
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
        'showPageSummary' => false,
        'resizableColumns' => false,
        'tableOptions' => ['class' => 'table table-report table-striped table-hover table-bordered tl-auto', 'style'=>'font-size: 75%'],
    ]) ?>
</div>