<?php
use app\modules\report\assets\ReportAsset;
use app\modules\report\widgets\ReportFilterOperatorWorkdays;
use app\modules\report\assets\ReportOperatorWorkdaysAsset;
use app\widgets\Panel;


/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormOperatorWorkdays $reportForm */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */


$this->title = Yii::t('common', 'Отчёт по рабочим дням операторов');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Отчеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ReportAsset::register($this);
ReportOperatorWorkdaysAsset::register($this);
?>

<?= ReportFilterOperatorWorkdays::widget([
    'reportForm' => $reportForm,
])
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Отчёт по рабочим дням операторов'),
    'alert' => $reportForm->panelAlert,
    'alertStyle' => $reportForm->panelAlertStyle,
    'border' => false,
    'withBody' => false,
    'content' => $this->render('_index-content', [
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider
    ]),
]) ?>


