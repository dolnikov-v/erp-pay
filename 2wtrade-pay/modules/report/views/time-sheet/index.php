<?php
use app\modules\report\assets\ReportAsset;
use app\modules\report\widgets\ReportFilterTimeSheet;
use app\widgets\Panel;
use app\modules\report\assets\ReportTimeSheetAsset;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormTimeSheet $reportForm */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */
/** @var array $dates */

$this->title = Yii::t('common', 'Табель');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Отчеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ReportAsset::register($this);
ReportTimeSheetAsset::register($this);
?>

<?= ReportFilterTimeSheet::widget([
    'reportForm' => $reportForm,
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Табель'),
    'border' => true,
    'withBody' => false,
    'alert' => $reportForm->panelAlert,
    'alertStyle' => $reportForm->panelAlertStyle,
    'content' => $this->render('_index-content', [
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
        'dates' => $dates,
    ]),
]) ?>
