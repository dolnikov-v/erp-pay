<?php
use app\modules\report\extensions\GridViewTimeSheet;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormTimeSheet $reportForm */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */
/** @var array $dates */
?>

<?php if ($dataProvider->allModels): ?>
    <div class="table-responsive wrap-report-time-sheet">
        <?= GridViewTimeSheet::widget([
            'id' => 'report_grid_TimeSheet',
            'reportForm' => $reportForm,
            'dataProvider' => $dataProvider,
            'showPageSummary' => true,
            'resizableColumns' => false,
            'tableOptions' => ['class' => 'table table-report table-striped table-hover table-bordered tl-auto'],
            'dates' => $dates,
        ]) ?>
    </div>
<?php endif; ?>