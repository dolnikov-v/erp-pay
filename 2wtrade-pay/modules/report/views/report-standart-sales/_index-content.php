<?php
use app\modules\report\extensions\GridView;
use yii\helpers\Html;
use app\helpers\report\Route;
use app\modules\order\models\OrderStatus;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormStandartSales $reportForm */
/** @var \yii\data\ArrayDataProvider $dataProvider */
?>

</br>
</br>
</br>
<div class="table-responsive">
    <?= GridView::widget([
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
        'resizableColumns' => false,
        'showPageSummary' => false,
        'columns' => [
            [
                'attribute' => 'country_name',
                'label' => Yii::t('common', 'Страна'),
                'format' => 'html',
                'value' => function ($model) {
                    return yii::t('common', $model['country_name']);
                }
            ],
            [
                'attribute' => 'standart_approve',
                'label' => Yii::t('common', 'Эталонный апрув, %'),
                'contentOptions' => ['style'=>'text-align:center'],
                'format' => 'html',
                'value' => function ($model) {
                    return round($model['standart_approve'], 1);
                }
            ],
            [
                'attribute' => 'all_approve',
                'label' => Yii::t('common', 'Общий апрув, %'),
                'contentOptions' => ['style'=>'text-align:center'],
                'format' => 'html',
                'value' => function ($model) {
                    return round($model['all_approve'], 1);
                }
            ],
            [
                'attribute' => 'standart_vikup',
                'label' => Yii::t('common', 'Эталонный выкуп, %'),
                'contentOptions' => ['style'=>'text-align:center'],
                'format' => 'html',
                'value' => function ($model) {
                    return round($model['standart_vikup'], 1);
                }
            ],
            [
                'attribute' => 'all_vikup',
                'label' => Yii::t('common', 'Общий выкуп, %'),
                'contentOptions' => ['style'=>'text-align:center'],
                'format' => 'html',
                'value' => function ($model) {
                    return round($model['all_vikup'], 1);
                }
            ],
            [
                'attribute' => 'standart_sr_cek_kc',
                'label' => Yii::t('common', 'Эталонный ср. чек по КЦ, ($)'),
                'contentOptions' => ['style'=>'text-align:center'],
                'format' => 'html',
                'value' => function ($model) {
                    return round($model['standart_sr_cek_kc'], 1);
                }
            ],
            [
                'attribute' => 'all_sr_cek_kc',
                'label' => Yii::t('common', 'Ср. чек по КЦ, ($)'),
                'contentOptions' => ['style'=>'text-align:center'],
                'format' => 'html',
                'value' => function ($model) {
                    return round($model['all_sr_cek_kc'], 1);
                }
            ],

        ],
    ]) ?>
</div>
