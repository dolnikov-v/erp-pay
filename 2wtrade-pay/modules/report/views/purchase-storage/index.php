<?php
use app\modules\report\assets\ReportAsset;
use app\widgets\Panel;
use app\modules\report\widgets\ReportFilterPurchaseStorage;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormPurchaseStorage $reportForm */
/** @var \yii\data\ArrayDataProvider $dataProvider */


$this->title = Yii::t('common', 'Прогнозирование закупок');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Отчеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ReportAsset::register($this);
?>

<?= ReportFilterPurchaseStorage::widget([
    'reportForm' => $reportForm,
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Прогнозирование закупок'),
    'border' => false,
    'withBody' => false,
    'content' => $this->render('_index-content', [
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
    ]),
]) ?>


