<?php
use app\modules\report\extensions\GridViewStoragePurchaseProduct;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormPurchaseStorage $reportForm */
/** @var \yii\data\ArrayDataProvider $dataProvider */
?>

<div class="table-responsive">
    <?= GridViewStoragePurchaseProduct::widget([
        'id' => 'report_grid_purchase_product',
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
        'showPageSummary' => false,
    ]) ?>
</div>
