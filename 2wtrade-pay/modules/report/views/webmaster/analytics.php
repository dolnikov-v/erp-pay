<?php

use app\modules\report\assets\ReportAsset;
use app\modules\report\widgets\ReportFilterWebmasterAnalytics;
use app\widgets\Panel;
use app\widgets\Nav;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormWebmasterAnalytics $reportForm */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */
/** @var \app\modules\report\extensions\DataProvider $dataProviderStatus */

$this->title = Yii::t('common', 'Аналитика вебмастеров');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Отчеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ReportAsset::register($this);

?>

<?= ReportFilterWebmasterAnalytics::widget([
    'reportForm' => $reportForm,
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Аналитика вебмастеров'),
    'border' => false,
    'withBody' => false,

    'nav' => new Nav([
        'tabs' => [
            0 => [
                'label' => Yii::t('common', 'По звонкам'),
                'content' => $this->render('_analytics-content', [
                    'reportForm' => $reportForm,
                    'dataProvider' => $dataProvider,
                ]),
            ],
            1 => [
                'label' => Yii::t('common', 'По статусам'),
                'content' => $this->render('_analytics-content-status', [
                    'reportForm' => $reportForm,
                    'dataProvider' => $dataProviderStatus,
                ]),
            ],
        ]
    ])
]) ?>
