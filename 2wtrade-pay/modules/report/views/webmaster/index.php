<?php
use app\modules\report\assets\ReportAsset;
use app\modules\report\widgets\ReportFilterWebmaster;
use app\widgets\Panel;
use app\modules\report\assets\FilterCountryDeliveryAsset;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormWebmaster $reportForm */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */

$this->title = Yii::t('common', 'Рейтинг вебмастеров');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Отчеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ReportAsset::register($this);
FilterCountryDeliveryAsset::register($this);
?>

<?= ReportFilterWebmaster::widget([
    'reportForm' => $reportForm,
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Рейтинг вебмастеров'),
    'border' => false,
    'withBody' => false,
    'content' => $this->render('_index-content', [
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
    ]),
]) ?>
