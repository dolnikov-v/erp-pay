<?php

use app\modules\report\extensions\GridViewWebmasterAnalytics;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormWebmasterAnalytics $reportForm */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */
?>

<div class="table-responsive">
    <?= GridViewWebmasterAnalytics::widget([
        'id' => 'report_grid_webmaster_analytics_status',
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
        'showPageSummary' => true,
        'byStatus' => true,
        'tableOptions' => ['class' => 'table table-report table-striped table-hover table-bordered'],
    ]) ?>
</div>
