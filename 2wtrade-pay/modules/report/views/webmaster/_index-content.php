<?php
use app\modules\report\extensions\GridViewWebmaster;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormWebmaster $reportForm */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */
?>

<div class="table-responsive">
    <?= GridViewWebmaster::widget([
        'id' => 'report_grid_webmaster',
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
        'showPageSummary' => false
    ]) ?>
</div>
