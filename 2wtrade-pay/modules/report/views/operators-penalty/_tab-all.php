<?php
use app\modules\report\extensions\GridViewOperatorsPenaltyAll;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormOperatorsPenalty $reportForm */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */

?>
<div class="table-responsive">
    <?= GridViewOperatorsPenaltyAll::widget([
        'id' => 'report_grid_operator_penalty_all',
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
        'showPageSummary' => false,
        'resizableColumns' => false,
    ]) ?>
</div>
