<?php
use app\modules\report\extensions\GridViewOperatorsPenaltyTeamLead;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormOperatorsPenalty $reportForm */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */

?>
<div class="table-responsive">
    <?= GridViewOperatorsPenaltyTeamLead::widget([
        'id' => 'report_grid_operators_penalty_teamlead',
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
        'showPageSummary' => false,
        'resizableColumns' => false,
    ]) ?>
</div>
