<?php
use app\modules\report\assets\ReportAsset;
use app\modules\report\widgets\ReportFilterOperatorsPenalty;
use app\widgets\Panel;
use app\widgets\Nav;
use app\modules\report\assets\ReportForeignOperatorAsset;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormOperatorsPenalty $reportForm */
/** @var \app\modules\report\extensions\DataProvider $dataProviderAll */
/** @var \app\modules\report\extensions\DataProvider $dataProviderTeamLead */


$this->title = Yii::t('common', 'Штрафы операторов');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Отчеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ReportAsset::register($this);
ReportForeignOperatorAsset::register($this);
?>

<?= ReportFilterOperatorsPenalty::widget([
    'reportForm' => $reportForm,
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Штрафы операторов'),
    'border' => false,
    'withBody' => false,
    'nav' => new Nav([
        'tabs' => [
            [
                'label' => Yii::t('common', 'По операторам'),
                'content' => $this->render('_tab-all', [
                    'dataProvider' => $dataProviderAll,
                    'reportForm' => $reportForm,
                ]),
            ],
            [
                'label' => Yii::t('common', 'По тимлидам'),
                'content' => $this->render('_tab-teamlead', [
                    'dataProvider' => $dataProviderTeamLead,
                    'reportForm' => $reportForm,
                ]),
            ],

        ],
    ]),
]) ?>
