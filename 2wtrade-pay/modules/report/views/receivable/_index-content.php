<?php
use app\modules\report\extensions\GridViewReceivable as GridView;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormReceivable $reportForm */
/** @var \yii\data\ArrayDataProvider $dataProvider */
/** @var array $aliases */
/** @var array $aliasesHead */
?>

<div class="table-responsive">
    <?= GridView::widget([
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
        'resizableColumns' => false,
        'showPageSummary' => false,
        'layout' => '<div id="report_export_box">{toolbar}</div>{items}{pager}',
        'columns' => $aliases,
        'columnsHead' => $aliasesHead,
    ]) ?>
</div>
