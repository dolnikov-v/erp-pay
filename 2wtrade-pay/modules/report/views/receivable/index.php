<?php
use app\modules\report\assets\ReportAsset;
use app\modules\report\widgets\ReportFilterReceivable;
use app\widgets\Panel;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormReceivable $reportForm */
/** @var \yii\data\ArrayDataProvider $dataProvider */
/** @var array $aliases */
/** @var array $aliasesHead */

$this->title = Yii::t('common', 'Финансовый по дебиторской задолженности');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Отчеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ReportAsset::register($this);
?>

<?= ReportFilterReceivable::widget([
    'reportForm' => $reportForm,
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Финансовый по дебиторской задолженности'),
    'border' => false,
    'withBody' => false,
    'content' => $this->render('_index-content', [
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
        'aliases' => $aliases,
        'aliasesHead' => $aliasesHead,
    ]),
]) ?>
