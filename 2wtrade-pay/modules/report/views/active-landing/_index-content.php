<?php
use app\modules\report\extensions\GridViewActiveLanding;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormActiveLanding $reportForm */
/** @var \yii\data\ArrayDataProvider $dataProvider */
?>

<div class="table-responsive">
    <?= GridViewActiveLanding::widget([
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
    ]) ?>
</div>
