<?php
/**
 * Created by PhpStorm.
 * User: smykov
 * Date: 29.10.16
 * Time: 17:26
 */
/* @var yii\web\View $this */
/* @var \app\models\User $model */
/* @var array $stats */

use yii\helpers\ArrayHelper;


?>

<table class="table tables-striped margin-top-20">
    <tr>
        <th colspan="2"><?= Yii::t('common', 'Страна') ?></th>
        <th style="padding-left: 0;"><?= Yii::t('common', 'Дата') ?></th>
        <th><?= Yii::t('common', 'Апрув КЦ (%)') ?></th>
        <th><?= Yii::t('common', 'Средний чек по КЦ') ?></th>
        <th><?= Yii::t('common', 'Не отправленных заказов') ?></th>
        <th><?= Yii::t('common', 'Текущий выкуп') ?></th>
        <th><?= Yii::t('common', 'Денег получено от КС') ?></th>
        <th><?= Yii::t('common', 'Курьерка отклонено заказов') ?></th>
    </tr>
    <?php if ($model->countries):
        $currentCountry = 0;
        ?>

        <?php foreach ($model->countries as $country):
        if (empty($stats[$country->id])) {
            continue;
        }
        $countStats = count($stats[$country->id]); ?>
        <?php foreach ($stats[$country->id] as $date => $statistic):

        ?>
        <tr<?= ((($statistic->percentBuyout < $statistic->minPercentBuyout) || ($statistic->minAvgCheckCC > 0 && $statistic->avgCheckApprovedCC < $statistic->minAvgCheckCC)) ? ' style="background:lightpink"' : '') ?>>
            <?php if ($currentCountry != $country->id):
                $currentCountry = $country->id; ?>
                <td style="background:white" rowspan="<?= $countStats ?>" class="width-50">
                    <span class="flag-icon flag-icon-<?= mb_strtolower($country->char_code) ?>"></span>
                </td>
                <td style="background:white" rowspan="<?= $countStats ?>"><?= $country->name ?></td>
            <?php endif; ?>
            <td style="padding-left: 0; background:white; border-left: 1px solid #e4eaec;"><?= str_replace(" ", "&nbsp;", $date); ?></td>
            <td style="background:white"><?= $statistic->percentApprovedCC ?> %</td>
            <td style="background:white">$ <?= $statistic->avgCheckApprovedCC ?></td>
            <td style="background:white"><?= $statistic->totalNotSent ?></td>
            <td style="background:white"><?= $statistic->percentBuyout ?> %</td>
            <td style="background:white">$ <?= $statistic->sumMoneyReceived ?> (<?= $statistic->moneyReceivedPrecent ?> %)</td>
            <td style="background:white"><?= $statistic->deliveryRejected ?></td>
        </tr>
    <?php endforeach; ?>
    <?php endforeach; ?>
        <tr<?= ((($stats['total']->percentBuyout < $stats['total']->minPercentBuyout) || ($stats['total']->minAvgCheckCC > 0 && $stats['total']->avgCheckApprovedCC < $stats['total']->minAvgCheckCC)) ? ' style="background:lightpink"' : '') ?>>
            <td colspan="3"  style="background:white"><?= Yii::t('common', 'Итого:') ?></td>
            <td style="background:white"><?= $stats['total']->percentApprovedCC ?> %</td>
            <td style="background:white">$ <?= $stats['total']->avgCheckApprovedCC ?></td>
            <td style="background:white"><?= $stats['total']->totalNotSent ?> </td>
            <td style="background:white"><?= $stats['total']->percentBuyout ?> %</td>
            <td <?= (($stats['total']->sumMoneyReceived >= 3) ? ' style="background:white"' : '') ?>><?= $stats['total']->sumMoneyReceived ?></td>
            <td style="background:white"><?= $stats['total']->deliveryRejected ?></td>
        </tr>

    <?php endif; ?>
</table>