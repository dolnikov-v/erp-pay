<?php
use app\modules\report\assets\ReportAsset;
use app\modules\report\widgets\ReportFilterCallCenter;
use app\modules\report\assets\ReportApproveByCountryAsset;
use app\modules\report\widgets\ReportFilterConsolidateInfo;
use app\widgets\Panel;
use app\helpers\DataProvider;
use app\widgets\Nav;
use yii\helpers\Url;
use yii\web\View;
/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormCallCenter $reportForm */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */

$this->title = Yii::t('common', 'Сводный отчет');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Отчеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

$this->registerJs("var I18n = {};", View::POS_HEAD);
$this->registerJs("I18n['getCountryUrl'] = '" . Url::toRoute(['get-country']) . "';", View::POS_HEAD);

ReportAsset::register($this);
ReportApproveByCountryAsset::register($this);
?>


<?=ReportFilterConsolidateInfo::widget([
    'reportForm' => $reportForm,
]);?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Сводный отчет'),
    'border' => false,
    'withBody' => false,
    'content' => $this->render('_index-content', [
        'model' => $model,
        'stats' => $stats,
        'timezones' => $timezones,
    ]),
]) ?>