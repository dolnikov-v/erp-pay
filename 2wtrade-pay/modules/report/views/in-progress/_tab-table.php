<?php
use app\modules\report\extensions\GridViewInProgress;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormInProgress $reportForm */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */
?>

<div class="table-responsive">
    <?= GridViewInProgress::widget([
        'id' => 'report_grid_in_progress',
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
    ]) ?>
</div>
