<?php
use app\modules\report\widgets\GraphInProgress;

/** @var \yii\web\View $this */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */
/** @var \app\modules\report\components\ReportFormInProgress $reportForm */
?>

<div class="table-responsive">
    <?=GraphInProgress::widget([
    'dataProvider' => $dataProvider,
    'reportForm' => $reportForm
]);?>
</div>