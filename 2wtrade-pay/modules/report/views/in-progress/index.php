<?php
use app\modules\report\assets\ReportAsset;
use app\modules\report\widgets\ReportFilterInProgress;
use app\widgets\Nav;
use app\widgets\Panel;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormInProgress $reportForm */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */

$this->title = Yii::t('common', 'Заказы "В процессе"');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Отчеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ReportAsset::register($this);
?>

<?= ReportFilterInProgress::widget([
    'reportForm' => $reportForm,
]) ?>

<?=Panel::widget([
    'nav' => new Nav([
        'tabs' => [
            [
                'label' => Yii::t('common', 'Графики'),
                'content' => $this->render('_tab-graph', [
                    'dataProvider' => $dataProvider,
                    'reportForm' => $reportForm,
                ]),
            ],
            [
                'label' => Yii::t('common', 'Таблица'),
                'content' => $this->render('_tab-table', [
                    'dataProvider' => $dataProvider,
                    'reportForm' => $reportForm,
                ]),
            ],

        ],
    ]),
]);?>

