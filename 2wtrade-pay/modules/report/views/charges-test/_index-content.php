<?php
use app\modules\report\extensions\GridView;
use app\widgets\Label;
use app\modules\order\models\OrderStatus;
use app\modules\order\models\OrderFinancePrediction;
use app\models\Product;
use yii\helpers\Html;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormChargesTest $reportForm */
/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var \app\modules\order\models\OrderFinancePrediction[] $predictions */

$statuses = OrderStatus::find()->collection();
$products = Product::find()->collection();

$columns = [
    [
        'attribute' => 'id',
        'label' => Yii::t('common', '#'),
        'enableSorting' => false,
    ],
    [
        'attribute' => 'status_id',
        'content' => function ($model) use ($statuses) {
            return Label::widget([
                'label' => array_key_exists($model->status_id, $statuses) ? $statuses[$model->status_id] : '—',
            ]);
        },
        'enableSorting' => false,
    ],
    [
        'attribute' => 'customer_zip',
        'enableSorting' => false,
    ],
    [
        'attribute' => 'customer_city',
        'enableSorting' => false,
    ],
    [
        'attribute' => 'customer_province',
        'enableSorting' => false,
    ],
    [
        'label' => Yii::t('common', 'Товары'),
        'content' => function ($model) use ($products) {
            $return = '';
            foreach ($model->orderProducts as $orderProduct) {
                $return .= Html::tag('div', Label::widget([
                    'label' => $products[$orderProduct->product_id] . ' - ' . $orderProduct->quantity,
                    'style' => Label::STYLE_INFO,
                ]));
            }
            return $return;
        },
        'enableSorting' => false
    ],
    [
        'attribute' => 'price_total',
        'enableSorting' => false,
    ],
];

$orderFinancePrediction = new OrderFinancePrediction();
$labels = $orderFinancePrediction->attributeLabels();
foreach (OrderFinancePrediction::getCurrencyFields() as $key => $val) {
    $columns[] = [
        'attribute' => $key,
        'label' => $labels[$key] ?? $key,
        'content' => function ($model) use ($key, $predictions) {
            return $predictions[$model->id]->$key ?? '';
        },
        'enableSorting' => false,
    ];
}
?>

<div class="table-responsive" style="width: 100%;overflow-x: auto; overflow-y: hidden;">
    <?= GridView::widget([
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
        'resizableColumns' => false,
        'showPageSummary' => false,
        'layout' => '{items}{pager}',
        'tableOptions' => ['class' => 'table table-report table-striped table-hover table-bordered tl-auto'],
        'columns' => $columns
    ]) ?>
</div>
