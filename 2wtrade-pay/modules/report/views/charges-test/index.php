<?php
use app\modules\report\assets\ReportAsset;
use app\modules\report\widgets\ReportFilterChargesTest;
use app\widgets\Panel;
use kartik\grid\GridView;
use app\modules\delivery\components\charges\ChargesCalculatorAbstract;
use app\modules\order\models\OrderFinancePrediction;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormChargesTest $reportForm */
/** @var \yii\data\ArrayDataProvider $invoiceDataProvider */
/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var \app\modules\order\models\OrderFinancePrediction[] $predictions */
/** @var array $totals */

$this->title = Yii::t('common', 'Тестирование калькулятора доставки');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Службы доставки'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ReportAsset::register($this);
?>

<?= ReportFilterChargesTest::widget([
    'reportForm' => $reportForm,
]) ?>

<?php if ($reportForm->getContractFilter()->contract_id): ?>
    <div class="alert alert-info margin-bottom-0">
        <div class="row">
            <div class="col-lg-9">
                <p class="margin-bottom-0"><?= Yii::t('common', 'Учтено заказов: {count_orders}', ['count_orders' => sizeof($predictions)]) ?></p>
                <p class="margin-bottom-0"><?= Yii::t('common', 'Всего собрано денег (COD): {cod} USD', ['cod' => Yii::$app->formatter->asDecimal($totals['total_cod_sum'], 2)]) ?></p>
            </div>
        </div>
    </div>
    <?= GridView::widget([
        'dataProvider' => $invoiceDataProvider,
        'layout' => '{items}',
        'showPageSummary' => true,
        'columns' => [
            [
                'attribute' => 'label',
                'label' => Yii::t('common', 'Название'),
                'pageSummary' => Yii::t('common', 'Итого'),
                'width' => '30%'
            ],
            [
                'attribute' => 'quantity',
                'label' => Yii::t('common', 'Количество единиц'),
                'hAlign' => 'center',
                'width' => '20%',
                'value' => function ($row) {
                    if (is_double($row['quantity'])) {
                        $row['quantity'] = Yii::$app->formatter->asDecimal($row['quantity'], 2);
                    } elseif (is_numeric($row['quantity'])) {
                        $row['quantity'] = Yii::$app->formatter->asDecimal($row['quantity']);
                    }
                    switch ($row['calculating_type']) {
                        case ChargesCalculatorAbstract::CALCULATING_TYPE_NUMBER:
                            $row['quantity'] = Yii::t('common', '{quantity} шт.', ['quantity' => $row['quantity']]);
                            break;
                        case ChargesCalculatorAbstract::CALCULATING_TYPE_PERCENT:
                            $row['quantity'] .= ' USD';
                            break;
                        case ChargesCalculatorAbstract::CALCULATING_TYPE_MONTHLY_CHARGE:
                            $row['quantity'] = Yii::t('common', '{quantity} мес.', ['quantity' => $row['quantity']]);
                            break;
                    }
                    return $row['quantity'];
                }
            ],
            [
                'attribute' => 'formula',
                'label' => Yii::t('common', 'Цена за единицу, USD'),
                'hAlign' => 'center',
                'width' => '20%',
                'value' => function ($row) use ($reportForm) {
                    if (is_numeric($row['formula'])) {
                        return Yii::$app->formatter->asDecimal($row['formula'], 2);
                    } else {
                        return $row['formula'];
                    }
                }
            ],
            [
                'attribute' => 'usd_sum',
                'label' => Yii::t('common', 'Сумма, USD'),
                'pageSummary' => Yii::$app->formatter->asDecimal($totals['total_sum_usd'], 2),
                'hAlign' => 'center',
                'width' => '30%',
                'format' => 'raw',
                'value' => function ($item) {
                    return ($item['finance_type'] == OrderFinancePrediction::FINANCE_TYPE_COST ? '(' : '') . Yii::$app->formatter->asDecimal($item['usd_sum'], 2) . ($item['finance_type'] == OrderFinancePrediction::FINANCE_TYPE_COST ? ')' : '');
                }
            ]
        ]
    ]) ?>

<?php endif; ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Заказы'),
    'border' => false,
    'withBody' => false,
    'alert' => $reportForm->panelAlert,
    'alertStyle' => $reportForm->panelAlertStyle,
    'content' => $this->render('_index-content', [
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
        'predictions' => $predictions,
    ]),
]) ?>
