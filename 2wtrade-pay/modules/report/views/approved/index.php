<?php
use app\modules\report\assets\ReportAsset;
use app\modules\report\widgets\ReportFilterApproved;
use app\widgets\Panel;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormApproved $reportForm */
/** @var \yii\data\ArrayDataProvider $dataProvider */
/** @var array $columns */

$this->title = Yii::t('common', 'Новые лиды и одобренные по часам');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Отчеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ReportAsset::register($this);
?>

<?= ReportFilterApproved::widget([
    'reportForm' => $reportForm,
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Новые лиды и одобренные по часам'),
    'alert' => Yii::t('common', 'В отчете используется местное время. В числителе - одобренные, в знаменателе - все заказы по часам.'),
    'border' => false,
    'withBody' => false,
    'content' => $this->render('_index-content', [
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
    ]),
]) ?>
