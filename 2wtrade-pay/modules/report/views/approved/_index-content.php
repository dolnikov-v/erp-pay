<?php
use app\modules\report\extensions\GridView;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormApproved $reportForm */
/** @var \yii\data\ArrayDataProvider $dataProvider */
/** @var array $columns */
$columns = [
    /*[
        'attribute' => 'country_name',
        'label' => Yii::t('common', 'Страна'),
    ],*/
    [
        'attribute' => 'day',
        'label' => Yii::t('common', 'Дата'),
    ],
];
for ($i = 8; $i < 24; $i++) {
    $strI = $i;
    if ($i < 10) {
        $strI = '0' . $i;
    }
    $columns[] = [
        'label' => $strI . ':00',
        'value' => function ($model) use ($strI) {
            if ($model['all'] == 0) {
                return '0 (0%) / 0 (0%)';
            }
            return $model['hour_' . $strI] . ' (' . (round($model['hour_' . $strI] * 100 / $model['all'])) . '%)' . ' / ' .
                $model['hour_leads_' . $strI] . ' (' . (round($model['hour_leads_' . $strI] * 100 / $model['all'])) . '%)';
        },
    ];
}
$columns[] = [
    'attribute' => 'all',
    'value' => function ($model) {
        return $model['all'] . ' (100%)';
    },
    'label' => Yii::t('common', 'Всего заказов'),
];
?>

<div class="table-responsive">
    <?= GridView::widget([
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
        'resizableColumns' => false,
        'showPageSummary' => false,
        'layout' => '<div id="report_export_box">{toolbar}</div>{items}{pager}',
        'tableOptions' => ['id' => 'approved_report'],
        'columns' => $columns
    ]) ?>
</div>
