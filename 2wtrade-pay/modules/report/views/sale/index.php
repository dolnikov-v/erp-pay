<?php
use app\modules\report\assets\ReportAsset;
use app\modules\report\widgets\ReportFilterSale;
use app\widgets\Panel;
use app\widgets\LinkPager;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormSale $reportForm */
/** @var \yii\data\ArrayDataProvider $dataProvider */

$this->title = Yii::t('common', 'Отчет по продажам');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Отчеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ReportAsset::register($this);
?>

<?= ReportFilterSale::widget([
    'reportForm' => $reportForm,
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Отчет по продажам'),
    'border' => false,
    'withBody' => false,
    'content' => $this->render('_index-content', [
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
    ]),
//    'footer' => LinkPager::widget([
//        'pagination' => $dataProvider->getPagination(),
//        'counterPosition' => LinkPager::COUNTER_POSITION_RIGHT,
//    ]),
]) ?>
