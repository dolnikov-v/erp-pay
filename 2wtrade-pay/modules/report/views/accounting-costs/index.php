<?php
use app\modules\report\assets\ReportAsset;
use app\modules\report\widgets\ReportFilterAccountingCosts;
use app\widgets\Panel;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormAccountingCosts $reportForm */
/** @var \yii\data\ArrayDataProvider $dataProvider */

$this->title = Yii::t('common', 'Расходы из 1С');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Отчеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ReportAsset::register($this);
?>

<?= ReportFilterAccountingCosts::widget([
    'reportForm' => $reportForm,
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Расходы из 1С'),
    'border' => false,
    'withBody' => false,
    'content' => $this->render('_index-content', [
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
    ]),
]) ?>
