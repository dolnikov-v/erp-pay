<?php
use app\modules\report\extensions\GridView;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormAccountingCosts $reportForm */
/** @var \yii\data\ArrayDataProvider $dataProvider */
?>

<div class="table-responsive">
    <?= GridView::widget([
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
        'resizableColumns' => false,
        'showPageSummary' => false,
        'columns' => [
            ['attribute' => 'date_article', 'label' => Yii::t('common', 'Дата'),
                'value' => function ($model) {
                    return Yii::$app->formatter->asDate($model['date_article'], "d.M.Y");
                }
            ],
            ['attribute' => 'code', 'label' => Yii::t('common', 'Код Статьи')],
            ['attribute' => 'name', 'label' => Yii::t('common', 'Наименование')],
            ['attribute' => 'country', 'label' => Yii::t('common', 'Страна')],
            ['attribute' => 'subdivision', 'label' => Yii::t('common', 'Подразделение')],
            ['attribute' => 'turnover', 'label' => Yii::t('common', 'Сумма Оборот'),
                'value' => function ($model) {
                    return number_format($model['turnover'], 2);
                },
                'hAlign' => 'right'
            ],
            ['attribute' => 'incoming', 'label' => Yii::t('common', 'Сумма Приход'),
                'value' => function ($model) {
                    return number_format($model['incoming'], 2);
                },
                'hAlign' => 'right'
            ],
            ['attribute' => 'outcoming', 'label' => Yii::t('common', 'Сумма Расход'),
                'value' => function ($model) {
                    return number_format($model['outcoming'], 2);
                },
                'hAlign' => 'right'
            ],
        ]
    ]) ?>
</div>
