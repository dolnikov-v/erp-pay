<?php
use app\modules\report\extensions\GridViewProduct;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormProduct $reportForm */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */
?>

<div class="table-responsive">
    <?= GridViewProduct::widget([
        'id' => 'report_grid_product',
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
    ]) ?>
</div>
