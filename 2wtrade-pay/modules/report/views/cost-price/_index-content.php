<?php
use app\modules\report\extensions\GridView;
use app\modules\report\components\ReportFormCostPrice;
use yii\helpers\Html;
use app\helpers\report\Route;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormCostPrice $reportForm */
/** @var \yii\data\ArrayDataProvider $dataProvider */
?>

<div class="table-responsive">
    <?= GridView::widget([
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
        'resizableColumns' => false,
        'showPageSummary' => false,
        'layout' => '<div id="report_export_box">{toolbar}</div>{items}{pager}',
        'columns' => [
            ['attribute' => 'country_name', 'label' => Yii::t('common', 'Страна'), 'group' => true, 'groupOddCssClass' => '', 'groupEvenCssClass' => ''],
            [
                'content' => function($model) {
                    return $model['create_date_year'] . ', ' . Yii::t('common', ReportFormCostPrice::monthName($model['create_date_month']));
                },
                'label' => Yii::t('common', 'Месяц'),
                'group' => true,
                'groupOddCssClass' => '',
                'groupEvenCssClass' => ''
            ],
            ['attribute' => 'product_name', 'label' => Yii::t('common', 'Товар')],
            ['attribute' => 'price_order', 'label' => Yii::t('common', 'Средний чек')],
            ['attribute' => 'price_storage', 'label' => Yii::t('common', 'Себестоимость')],
            [
                'label' => Yii::t('common', 'Заказы'),
                'format' => 'html',
                'value' => function ($model) use ($reportForm) {
                    $reportFilter = $reportForm->getCostPriceFilter();
                    return Html::a(
                        Yii::t('common', 'Список заказов'),
                        [
                            '/order/index/index',
                            'DateFilter' => Route::getOrderDateFilter([
                                'from' => Yii::$app->formatter->asDate($reportFilter->from),
                                'to' => Yii::$app->formatter->asDate($reportFilter->to),
                                'type' => 'created_at',
                            ]),
                            'ProductFilter' => ['product' => $model['product_id']]
                        ],
                        [
                            'target' => '_blank',
                        ]
                    );
                }
            ],
        ],
    ]) ?>
</div>
