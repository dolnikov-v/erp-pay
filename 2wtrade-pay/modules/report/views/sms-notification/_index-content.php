<?php
use app\components\grid\GridView;
use yii\bootstrap\Html;
use app\widgets\Panel;
use app\widgets\Label;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use app\components\grid\ActionColumn;
use app\helpers\DataProvider;
use app\components\grid\IdColumn;
use app\components\grid\DateColumn;
use app\widgets\ButtonLink;
use app\widgets\assets\custom\ModalConfirmDeleteAsset;
use app\widgets\custom\ModalConfirmDelete;
use app\modules\catalog\assets\ATimerAsset;
use app\modules\report\assets\SmsPollHistoryAsset;

ATimerAsset::register($this);
ModalConfirmDeleteAsset::register($this);
SmsPollHistoryAsset::register($this);


/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormSmsPollHistory $reportForm */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */
?>

<?= Panel::widget([
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function ($model) {
            return [
                'class' => 'tr-vertical-align-middle ' . (is_null($model['answer_text']) ? 'text-muted' : ''),
            ];
        },
        'columns' => [
            [
                'class' => IdColumn::className(),
            ],
            [
                'attribute' => 'order_id',
                'headerOptions' => ['class' => 'width-100 text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return $model['order_id'];
                },
                'label' => Yii::t('common', '№ заказа'),
                'enableSorting' => false,
            ],
            [
                'class' => DateColumn::className(),
                'headerOptions' => ['class' => 'width-150 text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'attribute' => 'order_created_at',
                'label' => Yii::t('common', 'Дата создания'),
            ],
            [
                'attribute' => 'status_id',
                'headerOptions' => ['class' => 'width-100 text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return $model['status_id'];
                },
                'label' => Yii::t('common', 'Статус'),
                'enableSorting' => false,
            ],
            [
                'headerOptions' => ['class' => 'width-500'],
                'content' => function ($model) {
                    $text = !empty($model['sms_notification_text']) ? $model['sms_notification_text'] : $model['email_notification_text'];
                    return Yii::t('common', $text);
                },
                'label' => Yii::t('common', 'Текст уведомления'),
                'enableSorting' => false,
            ],
            [
                'attribute' => 'answer_text',
                'headerOptions' => ['class' => 'width-200'],
                'content' => function ($model) {
                    return Yii::t('common', $model['answer_text']);
                },
                'label' => Yii::t('common', 'Ответ'),
                'enableSorting' => false,
            ],
            [
                'class' => DateColumn::className(),
                'headerOptions' => ['class' => 'width-150 text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'attribute' => 'created_at',
                'label' => Yii::t('common', 'Дата отправки'),
            ],
            [
                'class' => DateColumn::className(),
                'headerOptions' => ['class' => 'width-150 text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'attribute' => 'answered_at',
                'label' => Yii::t('common', 'Дата ответа'),
            ],
            [
                'attribute' => 'api_error',
                'headerOptions' => ['class' => 'width-100 text-center'],
                'contentOptions' => [
                    'class' => 'text-center  api_error',
                    'data-container' => 'body',
                    'data-toggle' => 'tooltip',
                    'title' => ''
                ],
                'content' => function ($model) {
                    return '<div class="error_text">' . $model['sms_api_error'] . '</div>' . Label::widget([
                        'label' => !is_null($model['sms_api_error']) ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => !is_null($model['sms_api_error']) ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'label' => Yii::t('common', 'Ошибка'),
                'enableSorting' => false,
            ],

        ],
    ]),
    'footer' => LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>

<?= ModalConfirmDelete::widget() ?>
