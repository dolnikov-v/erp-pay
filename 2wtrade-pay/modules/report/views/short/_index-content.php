<?php
use app\modules\report\extensions\GridViewShort;

/** @var \yii\web\View $this */
/** @var \app\modules\report\components\ReportFormShort $reportForm */
/** @var \app\modules\report\extensions\DataProvider $dataProvider */
?>

<div class="table-responsive">
    <?= GridViewShort::widget([
        'reportForm' => $reportForm,
        'dataProvider' => $dataProvider,
        'resizableColumns' => false,
    ]) ?>
</div>
