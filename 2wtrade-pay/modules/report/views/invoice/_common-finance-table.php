<?php
/**
 * @var \yii\web\View $this
 * @var array $data
 * @var \app\modules\report\components\invoice\ReportFormInvoice $reportForm
 * @var \app\modules\delivery\models\Delivery $delivery
 */
use app\helpers\report\Route;
use app\modules\delivery\components\charges\ChargesCalculatorAbstract;
use app\modules\order\models\OrderFinancePrediction;
use app\modules\order\models\search\filters\DateFilter;
use app\modules\report\components\invoice\ReportFormInvoice;
use app\widgets\ButtonProgress;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

//если по финансовому отчету - то период брать у отчета
if($reportForm->type_filter == ReportFormInvoice::FILTER_TYPE_FINANCE){
    $from = $reportForm->financeReport->period_from;
    $to = $reportForm->financeReport->period_to;
}else {
    $timestamps = $reportForm->getDateFilter()->getFilteredTimestamps(Yii::$app->request->queryParams);

    $from = $timestamps['from'];
    if ($data['contract']->date_from > $from) {
        $from = $data['contract']->date_from;
    }

    $to = $timestamps['to'];
    if (!empty($data['contract']->date_to) && $data['contract']->date_to < $to) {
        $to = $data['contract']->date_to;
    }
}
?>

<div class="alert alert-info margin-bottom-0">
    <div class="row">
        <div class="col-lg-9">
            <p class="margin-bottom-0"><?= Yii::t('common', 'Учтено заказов: {link}', [
                    'link' => Html::a(Yii::$app->formatter->asInteger($data['totalData']['orders_count']), $data['totalData']['orders_count'] > 0 ? Url::toRoute([
                        '/order/index/index',
                        'StatusFilter' => ['status' => $reportForm->getStatuses()],
                        'DateFilter' => Route::getOrderDateFilter([
                            'from' => empty($from) ? '' : Yii::$app->formatter->asDate($from),
                            'to' => empty($to) ? '' : Yii::$app->formatter->asDate($to),
                            'type' => DateFilter::TYPE_DELIVERY_SENT_AT_NOT_EMPTY
                        ]),
                        'DeliveryFilter' => ['delivery' => $data['contract']->delivery->id],
                        'force_country_slug' => $data['contract']->delivery->country->slug,
                    ]) : null)
                ]) ?></p>
            <p class="margin-bottom-0"><?= Yii::t('common', 'Всего собрано денег (COD): {cod} {currency}', [
                    'cod' => Yii::$app->formatter->asDecimal($data['totalData']['total_cod_sum']),
                    'currency' => $delivery->country->currency->char_code
                ]) ?></p>
        </div>
        <?php if (Yii::$app->user->can('report.invoice.createinvoice') && count($data['dataProvider']->allModels)) : ?>
            <div class="col-lg-3 text-right">
                <?php
                    $attributes = [];
                    if ($reportForm->includeDoneOrders) {
                        $attributes['title'] = Yii::t('common', 'Недоступно для завершенных заказов');
                        $attributes['disabled'] = 'disabled';
                    }
                    else {
                        $attributes['data-url'] = Url::toRoute([
                            'create-invoice',
                            'contract' => $data['contract']->id,
                            'from' => $from,
                            'to' => $to,
                            'includeDoneOrders' => $reportForm->includeDoneOrders,
                        ]);
                    }
                ?>
                <?= ButtonProgress::widget([
                    'label' => Yii::t('common', 'Сгенерировать инвойс'),
                    'style' => ButtonProgress::STYLE_PRIMARY . ' width-300 button-generate-invoice',
                    'attributes' => $attributes
                ]) ?>
            </div>
        <?php endif; ?>
    </div>
</div>
<?= GridView::widget([
    'dataProvider' => $data['dataProvider'],
    'layout' => '{items}',
    'showPageSummary' => true,
    'columns' => [
        [
            'attribute' => 'label',
            'label' => Yii::t('common', 'Название'),
            'pageSummary' => Yii::t('common', 'Итого'),
            'width' => '30%'
        ],
        [
            'attribute' => 'quantity',
            'label' => Yii::t('common', 'Количество единиц'),
            'hAlign' => 'center',
            'width' => '20%',
            'value' => function ($row) use ($reportForm) {
                if (is_double($row['quantity'])) {
                    $row['quantity'] = Yii::$app->formatter->asDecimal($row['quantity'], 2);
                } elseif (is_numeric($row['quantity'])) {
                    $row['quantity'] = Yii::$app->formatter->asDecimal($row['quantity']);
                }
                switch ($row['calculating_type']) {
                    case ChargesCalculatorAbstract::CALCULATING_TYPE_NUMBER:
                        $row['quantity'] = Yii::t('common', '{quantity} шт.', ['quantity' => $row['quantity']]);
                        break;
                    case ChargesCalculatorAbstract::CALCULATING_TYPE_PERCENT:
                        $row['quantity'] .= " {$reportForm->getCurrency()->char_code}";
                        break;
                    case ChargesCalculatorAbstract::CALCULATING_TYPE_MONTHLY_CHARGE:
                        $row['quantity'] = Yii::t('common', '{quantity} мес.', ['quantity' => $row['quantity']]);
                        break;
                }
                return $row['quantity'];
            }
        ],
        [
            'attribute' => 'formula',
            'label' => Yii::t('common', 'Цена за единицу, {currency}', ['currency' => $reportForm->getCurrency()->char_code]),
            'hAlign' => 'center',
            'width' => '20%',
            'value' => function ($row) use ($reportForm) {
                if (is_numeric($row['formula'])) {
                    return Yii::$app->formatter->asDecimal($row['formula'], $reportForm->roundPrecision);
                } else {
                    return $row['formula'];
                }
            }
        ],
        [
            'attribute' => 'usd_sum',
            'label' => Yii::t('common', 'Сумма, {currency}', ['currency' => $reportForm->getCurrency()->char_code]),
            'pageSummary' => Yii::$app->formatter->asDecimal($data['totalData']['total_sum_usd'], 2),
            'hAlign' => 'center',
            'width' => '30%',
            'format' => 'raw',
            'value' => function ($item) use ($reportForm) {
                return ($item['finance_type'] == OrderFinancePrediction::FINANCE_TYPE_COST ? '(' : '') . Yii::$app->formatter->asDecimal($item['usd_sum'], 2) . ($item['finance_type'] == OrderFinancePrediction::FINANCE_TYPE_COST ? ')' : '');
            }
        ]
    ]
]) ?>
