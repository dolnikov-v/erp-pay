<?php
/**
 * @var \yii\web\View $this
 * @var \app\modules\report\components\invoice\ReportFormInvoice $reportForm
 * @var \app\modules\delivery\models\Delivery $delivery
 */
$timestamps = $reportForm->getDateFilter()->getFilteredTimestamps(Yii::$app->request->queryParams);
if (!empty($timestamps['from']) && !empty($timestamps['to'])) {
    $label = Yii::t('common', 'Для {courier} не указаны тарифы за период {from} - {to}', [
        'courier' => $delivery->name,
        'from' => Yii::$app->formatter->asDate($timestamps['from']),
        'to' => Yii::$app->formatter->asDate($timestamps['to'])
    ]);
} elseif (empty($timestamps['from']) && empty($timestamps['to'])) {
    $label = Yii::t('common', 'Для {courier} не указаны тарифы', [
        'courier' => $delivery->name,
    ]);
} elseif (!empty($timestamps['from'])) {
    $label = Yii::t('common', 'Для {courier} не указаны тарифы c {from}', [
        'courier' => $delivery->name,
        'from' => Yii::$app->formatter->asDate($timestamps['from'])
    ]);
} else {
    $label = Yii::t('common', 'Для {courier} не указаны тарифы до {to}', [
        'courier' => $delivery->name,
        'to' => Yii::$app->formatter->asDate($timestamps['to'])
    ]);
}
?>

<div class="text-center">
    <h4><?= $label ?> </h4>
</div>
