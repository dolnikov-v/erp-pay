<?php
/**
 * @var \yii\web\View $this
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var \app\modules\report\components\invoice\ReportFormInvoiceList $reportForm
 * @var array $queryParams
 */

use app\components\grid\ActionColumn;
use app\components\grid\DateColumn;
use app\components\grid\GridView;
use app\helpers\DataProvider;
use app\modules\report\models\Invoice;
use app\widgets\Label;
use app\widgets\LinkPager;
use app\widgets\Panel;
use yii\helpers\Html;
use yii\helpers\Url;

$queryParams = $queryParams ?? Yii::$app->request->queryParams;
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Инвойсы'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'fullScreenBody' => true,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'delivery.country.name',
                'label' => Yii::t('common', 'Страна'),
                'headerOptions' => [
                    'class' => 'text-center'
                ],
                'contentOptions' => [
                    'class' => 'text-center'
                ],
            ],
            [
                'attribute' => 'delivery.name',
                'label' => Yii::t('common', 'Служба доставки'),
                'format' => 'raw',
                'content' => function ($model) {
                    /**
                     * @var Invoice $model
                     */
                    if (Yii::$app->user->can('delivery.control.contractedit')) {
                        return Html::a($model->delivery->name, Url::toRoute([
                            '/delivery/control/contract-edit',
                            'id' => $model->contract_id,
                            'force_country_slug' => $model->delivery->country->slug,
                        ]));
                    }
                    return $model->delivery->name;
                },
                'headerOptions' => [
                    'class' => 'text-center'
                ],
                'contentOptions' => [
                    'class' => 'text-center'
                ],
            ],
            [
                'attribute' => 'name',
                'headerOptions' => [
                    'class' => 'text-center'
                ],
                'contentOptions' => [
                    'class' => 'text-center'
                ],
            ],
            [
                'label' => Yii::t('common', 'Период'),
                'value' => function ($model) {
                    /**
                     * @var Invoice $model
                     */
                    if ($model->period_from && $model->period_to) {
                        $out = Yii::$app->formatter->asDate($model->period_from) . ' - ' . Yii::$app->formatter->asDate($model->period_to);
                    } elseif ($model->period_from) {
                        $out = Yii::t('common', 'С {date}', ['date' => Yii::$app->formatter->asDate($model->period_from)]);
                    } elseif ($model->period_to) {
                        $out = Yii::t('common', 'До {date}', ['date' => Yii::$app->formatter->asDate($model->period_to)]);
                    } else {
                        $out = Yii::t('common', 'Не указан');
                    }

                    return $out;
                },
                'headerOptions' => [
                    'class' => 'text-center'
                ],
                'contentOptions' => [
                    'class' => 'text-center'
                ],
            ],
            [
                'attribute' => 'statusLabel',
                'headerOptions' => [
                    'class' => 'text-center'
                ],
                'contentOptions' => [
                    'class' => 'text-center'
                ],
                'content' => function ($model) {
                    /**
                     * @var Invoice $model
                     */
                    return Label::widget([
                        'label' => $model->statusLabel,
                        'style' => $model->getStatusLabelStyle(),
                    ]);
                }
            ],
            [
                'label' => Yii::t('common', 'Сумма тотал'),
                'attribute' => 'total',
                'format' => 'raw',
                'headerOptions' => [
                    'class' => 'text-center'
                ],
                'contentOptions' => [
                    'class' => 'text-center'
                ],
                'value' => function ($model, $key, $row, $column) {
                    return \Yii::$app->formatter->asDecimal($model[$column->attribute] ?: 0, 2) . " {$model->currency->char_code}";
                }
            ],
            [
                'label' => Yii::t('common', 'Сумма получено'),
                'attribute' => 'sumTotalReceived',
                'format' => ['decimal', 2],
                'headerOptions' => [
                    'class' => 'text-center'
                ],
                'contentOptions' => [
                    'class' => 'text-center'
                ],
                'value' => function ($model, $key, $row, $column) {
                    return $model[$column->attribute] ?: 0;
                }
            ],
            [
                'label' => Yii::t('common', 'Сумма получено (по заказам)'),
                'attribute' => 'sumTotalByOrders',
                'format' => ['decimal', 2],
                'headerOptions' => [
                    'class' => 'text-center'
                ],
                'contentOptions' => [
                    'class' => 'text-center'
                ],
                'value' => function ($model) {
                    return $model['sumTotalByOrders'] - $model['additional_charges'];
                }
            ],
            [
                'attribute' => 'user.username',
                'headerOptions' => [
                    'class' => 'text-center'
                ],
                'contentOptions' => [
                    'class' => 'text-center'
                ],
            ],
            [
                'attribute' => 'created_at',
                'class' => DateColumn::className(),
                'headerOptions' => [
                    'class' => 'text-center'
                ],
                'contentOptions' => [
                    'class' => 'text-center'
                ],
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Скачать инвойс'),
                        'url' => function ($model) use ($queryParams) {
                            return Url::toRoute(array_merge($queryParams, [
                                'download-invoice',
                                'id' => $model->id,
                                'type' => Invoice::FILE_INVOICE
                            ]));
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('report.invoice.downloadinvoice') && !in_array($model->status, [
                                    Invoice::STATUS_QUEUE,
                                    Invoice::STATUS_GENERATING
                                ]);
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Скачать список заказов'),
                        'url' => function ($model) use ($queryParams) {
                            return Url::toRoute(array_merge($queryParams, [
                                'download-invoice',
                                'id' => $model->id,
                                'type' => Invoice::FILE_ORDERS
                            ]));
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('report.invoice.downloadinvoice') && !in_array($model->status, [
                                    Invoice::STATUS_QUEUE,
                                    Invoice::STATUS_GENERATING
                                ]);
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Отправить КС'),
                        'url' => function () {
                            return '#';
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('report.invoice.sendinvoice') && in_array($model->status, [Invoice::STATUS_GENERATED]);
                        },
                        'style' => 'send-invoice-link',
                        'attributes' => function ($model) {
                            return [
                                'data-href' => Url::toRoute(['send-invoice', 'id' => $model->id]),
                            ];
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Закрыть'),
                        'url' => function () {
                            return '#';
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('report.invoice.closeinvoice') && in_array($model->status, [Invoice::STATUS_GENERATED]);
                        },
                        'style' => 'close-invoice-link',
                        'attributes' => function ($model) {
                            return [
                                'data-href' => Url::toRoute(['close-invoice', 'id' => $model->id]),
                            ];
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Перегенерировать'),
                        'url' => function ($model) use ($queryParams) {
                            return Url::toRoute(array_merge($queryParams, ['regenerate-invoice', 'id' => $model->id]));
                        },
                        'style' => 'regenerate-invoice-button',
                        'can' => function ($model) {
                            return Yii::$app->user->can('report.invoice.regenerateinvoice') && in_array($model->status, [
                                    Invoice::STATUS_GENERATED,
                                ]);
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Согласовать'),
                        'url' => function ($model) use ($queryParams) {
                            return Url::toRoute(array_merge($queryParams, ['approve-invoice', 'id' => $model->id]));
                        },
                        'style' => 'approve-invoice-button',
                        'can' => function ($model) {
                            return Yii::$app->user->can('report.invoice.approveinvoice') && in_array($model->status, [
                                    Invoice::STATUS_GENERATED,
                                ]);
                        }
                    ],
                    [
                        'can' => function ($model) {
                            return Yii::$app->user->can('report.invoice.deleteinvoice') && !in_array($model->status, [
                                    Invoice::STATUS_GENERATING,
                                    Invoice::STATUS_DONE,
                                    Invoice::STATUS_CLOSING,
                                    Invoice::STATUS_APPROVED,
                                ]) && (
                                    Yii::$app->user->can('report.invoice.regenerateinvoice') && in_array($model->status, [
                                        Invoice::STATUS_GENERATED,
                                    ])
                                    || Yii::$app->user->can('report.invoice.downloadinvoice') && !in_array($model->status, [
                                        Invoice::STATUS_QUEUE,
                                        Invoice::STATUS_GENERATING
                                    ])
                                    || Yii::$app->user->can('report.invoice.closeinvoice') && in_array($model->status, [Invoice::STATUS_GENERATED])
                                    || Yii::$app->user->can('report.invoice.sendinvoice') && in_array($model->status, [Invoice::STATUS_GENERATED])
                                );
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить'),
                        'url' => function () {
                            return '#';
                        },
                        'style' => 'confirm-delete-link',
                        'attributes' => function ($model) use ($queryParams) {
                            return [
                                'data-href' => Url::toRoute(array_merge($queryParams, [
                                    '/report/invoice/delete-invoice',
                                    'id' => $model->id
                                ])),
                            ];
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('report.invoice.deleteinvoice') && !in_array($model->status, [
                                    Invoice::STATUS_GENERATING,
                                    Invoice::STATUS_DONE,
                                    Invoice::STATUS_CLOSING,
                                    Invoice::STATUS_APPROVED,
                                ]);
                        }
                    ]
                ]
            ]
        ]
    ]),
    'footer' => LinkPager::widget([
        'pagination' => $dataProvider->getPagination(),
        'counterPosition' => LinkPager::COUNTER_POSITION_RIGHT,
    ])
]) ?>

