<?php

use app\modules\report\assets\InvoiceAsset;
use app\modules\report\assets\ReportAsset;
use app\modules\report\widgets\invoice\InvoiceModalClose;
use app\modules\report\widgets\invoice\InvoiceModalCreate;
use app\modules\report\widgets\invoice\InvoiceModalEmailSender;
use app\modules\report\widgets\invoice\ReportFilterFinanceTab;
use app\modules\report\widgets\invoice\ReportFilterInvoice;
use app\modules\report\widgets\invoice\ReportFilterInvoiceList;
use app\modules\report\widgets\invoice\ReportFilterInvoiceTab;
use app\widgets\assets\custom\ModalConfirmDeleteAsset;
use app\widgets\custom\ModalConfirmDelete;
use app\widgets\Button;
use yii\bootstrap\Modal;
use app\widgets\Nav;
use app\widgets\Panel;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 * @var \app\modules\report\components\invoice\ReportFormInvoice $reportForm
 * @var array $allData
 * @var \app\modules\delivery\models\Delivery $delivery
 * @var \yii\data\ActiveDataProvider $invoicesDataProvider
 * @var \app\modules\report\components\invoice\ReportFormInvoiceList $invoicesReportForm
 */


$this->title = Yii::t('common', 'Инвойсирование');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Финансы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

ReportAsset::register($this);
InvoiceAsset::register($this);
ModalConfirmDeleteAsset::register($this);

?>

<?= Panel::widget([
    'title' => yii::t('common', 'Фильтр'),
    'nav' => new Nav([
        'tabs' => [
            [
                'label' => yii::t('common', 'По периоду'),
                'content' => ReportFilterInvoiceTab::widget([
                    'reportForm' => $reportForm
                ])
            ],
            [
                'label' => yii::t('common', 'По финансовому отчету'),
                'content' => ReportFilterFinanceTab::widget([
                    'reportForm' => $reportForm
                ])
            ]
        ]
    ])
]); ?>

    <div id="preview_invoice_finance_table" class="preview-invoice-finance-table">
        <?= $this->render('_index-finance-table', [
            'reportForm' => $reportForm,
            'allData' => $allData,
            'delivery' => $delivery,
        ]) ?>
    </div>
<?php if (Yii::$app->user->can('report.invoice.index.invoicelist')): ?>
    <div class="invoices-table-filter">
        <?= ReportFilterInvoiceList::widget([
            'reportForm' => $invoicesReportForm
        ]) ?>
    </div>
    <div id="invoices_table_container"
         data-reload-url="<?= Url::toRoute(array_merge(Yii::$app->request->queryParams, ['reload-invoice-list'])) ?>">
        <?= $this->render('_index-invoices-table', [
            'dataProvider' => $invoicesDataProvider,
            'reportForm' => $reportForm,
        ]) ?>
    </div>
<?php endif; ?>

<?php if (Yii::$app->user->can('report.invoice.createinvoice')): ?>
    <?= InvoiceModalCreate::widget([]) ?>
<?php endif; ?>

<?php if (Yii::$app->user->can('report.invoice.closeinvoice')): ?>
    <?= InvoiceModalClose::widget([]) ?>
<?php endif; ?>
<?php if (Yii::$app->user->can('report.invoice.sendinvoice')): ?>
    <?= InvoiceModalEmailSender::widget([]) ?>
<?php endif; ?>
<?= ModalConfirmDelete::widget() ?>

<?= $this->render('_tpl-requisite', []); ?>

<?php Modal::begin([
    'id' => 'set-requisite-to-invoice',
    'header' => "<h4>" . yii::t('common', 'Укажите реквизиты') . "</h4>",
    'footer' =>
        Button::widget([
            'id' => 'action-button',
            'label' => Yii::t('common', 'Продолжить'),
            'size' => Button::SIZE_SMALL,
            'style' => Button::STYLE_INFO,
            'attributes' => [
                'data-dismiss' => 'modal',
                'disabled' => 'disabled'
            ],
        ]) .
        Button::widget([
            'id' => 'close-button',
            'label' => Yii::t('common', 'Закрыть'),
            'size' => Button::SIZE_SMALL,
            'attributes' => [
                'data-dismiss' => 'modal',
            ],
        ])
]);
?>
<div class="row">
    <div class=" col-lg-12">
        <div class="spinner"></div>
        <div class="list-requisites">
        </div>
    </div>
</div>

<?php Modal::end(); ?>

