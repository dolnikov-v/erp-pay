<?php

use app\widgets\Nav;
use app\widgets\Panel;

/**
 * @var \yii\web\View $this
 * @var \app\modules\report\components\invoice\ReportFormInvoice $reportForm
 * @var array $allData
 * @var \app\modules\delivery\models\Delivery $delivery
 */

$nav = [];
if (count($allData) > 1) {
    foreach ($allData as $data) {
        $label = '';
        if (empty($data['contract']->date_from) && empty($data['contract']->date_to)) {
            $label = Yii::t('common', 'Бессрочный контракт');
        } elseif (!empty($data['contract']->date_from) && !empty($data['contract']->date_to)) {
            $label = Yii::t('common', 'Контракт с {from} - {to}', [
                'from' => Yii::$app->formatter->asDate($data['contract']->date_from),
                'to' => Yii::$app->formatter->asDate($data['contract']->date_to)
            ]);
        } elseif (!empty($data['contract']->date_from)) {
            $label = Yii::t('common', 'Конракт с {from}', ['from' => Yii::$app->formatter->asDate($data['contract']->date_from)]);
        } elseif (!empty($data['contract']->date_to)) {
            $label = Yii::t('common', 'Конракт до {to}', ['to' => Yii::$app->formatter->asDate($data['contract']->date_to)]);
        }
        $nav[] = [
            'content' => $this->render('_common-finance-table', [
                'data' => $data,
                'reportForm' => $reportForm,
                'delivery' => $delivery,
            ]),
            'label' => $label,
        ];
    }
}

?>

<?= Panel::widget([
    'nav' => empty($nav) ? false : new Nav(['tabs' => $nav]),
    'title' => Yii::t('common', 'Инвойс'),
    'content' => empty($nav) ? (count($allData) == 1 ? $this->render('_common-finance-table', [
        'data' => array_values($allData)[0],
        'reportForm' => $reportForm,
        'delivery' => $delivery,
    ]) : $this->render('_common-finance-table-not-found', [
        'reportForm' => $reportForm,
        'delivery' => $delivery,
    ])) : '',
    'collapse' => true,
    'fullScreenBody' => true,
    'id' => 'common_finance_invoice',
    'border' => false,
]) ?>
