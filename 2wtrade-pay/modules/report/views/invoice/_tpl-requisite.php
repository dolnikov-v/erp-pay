<?php

use app\widgets\custom\Radio;

?>

<div class="tpl_requisite hidden">
    <div class="clickable">

        <div class="col-lg-1 radioPosition">
            <?= Radio::widget([
                'id' => 'requisite_{id}',
                'name' => 'requisite_delivery_id',
                'value' => '{id}',
                'checked' => false,
                'disabled' => false,
                'label' => '&nbsp;'
            ]) ?>
        </div>

        <div class="col-lg-11">
            <div class="row toggle" data-id="{id}">
                <div class="col-lg-4"><strong><?= yii::t('common', 'Компания') ?></strong></div>
                <div class="col-lg-8">{name} ({bank_account})</div>
            </div>
            <div class="row info toggle_{id} hidden">
                <div class="col-lg-4"><strong><?= yii::t('common', 'Город') ?></strong></div>
                <div class="col-lg-8">{city}</div>

                <div class="col-lg-4"><strong><?= yii::t('common', 'Адрес') ?></strong></div>
                <div class="col-lg-8">{address}</div>

                <div class="col-lg-4"><strong><?= yii::t('common', 'Банк получателя') ?></strong></div>
                <div class="col-lg-8">{bank}</div>

                <div class="col-lg-4"><strong><?= yii::t('common', 'Адрес банка') ?></strong></div>
                <div class="col-lg-8">{bank_address}</div>

                <div class="col-lg-4"><strong><?= yii::t('common', '№ счета') ?></strong></div>
                <div class="col-lg-8">{bank_account}</div>

                <div class="col-lg-4"><strong><?= yii::t('common', 'SWIFT код') ?></strong></div>
                <div class="col-lg-8">{swift_code}</div>
            </div>
        </div>
        <p>&nbsp;</p>
    </div>
</div>

