<?php

namespace app\modules\report\widgets;


use app\widgets\Widget;

/**
 * Class ReportCallCenterResponseViewer
 * @package app\modules\report\widgets
 */
class ReportCallCenterResponseViewer extends Widget
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('report-call-center-response-viewer/modal');
    }
}
