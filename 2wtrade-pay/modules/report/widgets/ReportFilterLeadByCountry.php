<?php
namespace app\modules\report\widgets;

/**
 * Class ReportFilterLeadByCountry
 * @package app\modules\report\widgets
 */
class ReportFilterLeadByCountry extends ReportFilter
{
    /** @var $ReportFormLeadByCountry */
    public $reportForm;

    public function apply()
    {
        return $this->render('report-filter-lead-by-country/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}
