<?php

namespace app\modules\report\widgets;

use app\modules\delivery\models\Delivery;
use yii\helpers\ArrayHelper;
use yii\db\Expression;

/**
 * Class GraphBuyoutByCountry
 * @package app\modules\report\widgets
 */
class GraphBuyoutByCountry extends GraphByCountriesIntervals
{
    /** @var array */
    protected $products = [];
    /** @var array */
    protected $deliveries = [];
    /** @var array */
    protected $countries = [];

    /**
     * @return string
     */
    public function run()
    {
        $series = $this->generateSeries();

        return $this->render('graph-buyout-by-country/index', [
            'seriesVykup' => $series['vykup'],
            'seriesPercent' => $series['percent'],
            'dates' => $this->formatDates(array_values($this->dates)),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->interval = $this->reportForm->getTypeDatePartFilter()->type_date_part;
        $this->data = $this->prepareData($this->dataProvider->allModels);
        $this->dates = $this->generateDates();
        $this->countries = $this->generateCountries();
        $this->products = $this->generateProducts();
        $this->deliveries = $this->generateDeliveries();
    }

    /**
     * @return array
     */
    protected function generateProducts()
    {
        $products = ArrayHelper::getColumn($this->dataProvider->allModels, 'product_name');
        $products = array_unique($products);
        asort($products);

        return $products;
    }

    /**
     * @return array
     */
    protected function generateDeliveries()
    {
        $deliveries = ArrayHelper::getColumn($this->dataProvider->allModels, 'delivery_name');
        $delivery_id = ArrayHelper::getColumn($this->dataProvider->allModels, 'delivery_id');
        $deliveries = array_combine($delivery_id, $deliveries);

        return $deliveries;
    }

    /**
     * @return array
     */
    protected function generateCountries()
    {
        $countries = ArrayHelper::getColumn($this->dataProvider->allModels, 'country_name');
        $country_ids = ArrayHelper::getColumn($this->dataProvider->allModels, 'country_id');
        $countries = array_combine($country_ids, $countries);

        return $countries;
    }

    /**
     * Собирает пары  country id - delivery id
     * @return Delivery[]
     */
    public function getCombination()
    {
        $query = Delivery::find()
            ->select([
                'couple' => new Expression('CONCAT(' . Delivery::tableName() . '.country_id, "-", ' . Delivery::tableName() . '.id)')
            ])
            ->asArray()
            ->all();

        return ArrayHelper::getColumn($query, 'couple');
    }

    /**
     * @param array $params
     * @return array
     */
    protected function sumDataParams($params)
    {
        $data = [];
        $combination = $this->getCombination();
        $deliveries = $this->generateDeliveries();
        //курьерка не указана в фильтре
        sort($deliveries);

        foreach ($this->countries as $k => $country) {
            foreach ($this->deliveries as $j => $delivery) {
                $pair = $k . '-' . $j;
                if (in_array($pair, $combination) || is_null($deliveries[0])) {
                    $delivery = !empty($delivery) ? ' (' . $delivery . ') ' : ' ';

                    foreach ($this->products as $product) {
                        foreach ($this->dates as $date) {
                            foreach ($params as $param) {
                                $data[$country . $delivery . $product][$date][$param] = 0;
                            }
                        }
                    }
                }
            }
        }

        foreach ($this->data as $model) {
            $country = $model['country_name'];
            $product = isset($model['product_name']) ? $model['product_name'] : '';
            $delivery = isset($model['delivery_name']) ? ' (' . $model['delivery_name'] . ') ' : ' ';

            $date = $model['date'];

            foreach ($params as $param) {
                if (isset($data[$country . $delivery . $product])) {
                    $data[$country . $delivery . $product][$date][$param] += $model[$param];
                }
            }
        }
        return $data;
    }

    /**
     * @return array
     */
    private function generateSeries()
    {
        $data = $this->sumDataParams(['vykupleno', 'podtverzdeno']);

        $seriesList = [
            'vykup' => [],
            'percent' => []
        ];

        foreach ($data as $name => $v) {
            $values = [];
            foreach ($v as $date => $value) {
                $values['vykup'][] = round($value['vykupleno'], 2);
                $values['percent'][] = ($value['podtverzdeno']) ? round($value['vykupleno'] / $value['podtverzdeno'] * 100, 2) : 0;
            }

            $seriesList['vykup'][] = [
                'name' => $name,
                'data' => $values['vykup']
            ];

            $seriesList['percent'][] = [
                'name' => $name,
                'data' => $values['percent']
            ];
        }
        return $seriesList;
    }
}
