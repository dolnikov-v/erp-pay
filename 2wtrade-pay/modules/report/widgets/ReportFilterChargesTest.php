<?php
namespace app\modules\report\widgets;

use app\modules\report\components\ReportFormChargesTest;

/**
 * Class ReportFilterDelivery
 * @package app\modules\report\widgets
 */
class ReportFilterChargesTest extends ReportFilter
{
    /** @var ReportFormChargesTest */
    public $reportForm;

    /**
     * @return string
     */
    public function apply()
    {
        return $this->render('report-filter-charges-test/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}
