<?php

namespace app\modules\report\widgets;

/**
 * Class ReportFilterWorkflow
 * @package app\modules\report\widgets
 */
class ReportFilterWorkflow extends ReportFilter
{
    /** @var \app\modules\report\components\ReportFormWorkflow */
    public $reportForm;

    public function apply()
    {
        return $this->render('report-filter-workflow/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}
