<?php
namespace app\modules\report\widgets;

use app\modules\report\components\ReportFormStandartSales;

/**
 * Class ReportFilterDaily
 * @package app\modules\report\widgets
 */
class ReportFilterDailyStandartOperators extends ReportFilter
{
    /**
     * @var ReportFormStandartSales
     */
    public $reportForm;

    /**
     * @return string
     */
    public function apply()
    {
        return $this->render('report-filter-daily-standart-operators/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}
