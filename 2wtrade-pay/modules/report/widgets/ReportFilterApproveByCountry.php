<?php

namespace app\modules\report\widgets;

/**
 * Class ReportFilterApproveByCountry
 * @package app\modules\report\widgets
 */
class ReportFilterApproveByCountry extends ReportFilter
{
    /** @var \app\modules\report\components\ReportFormApproveByCountry */
    public $reportForm;

    public function apply()
    {
        return $this->render('report-filter-approve-by-country/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}
