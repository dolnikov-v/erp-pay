<?php
namespace app\modules\report\widgets;

/**
 * Class ReportFilterForecastLeadByWeekDays
 * @package app\modules\report\widgets
 */
class ReportFilterForecastLeadByWeekDays extends ReportFilter
{
    public $reportForm;

    public function apply()
    {
        return $this->render('report-filter-forecast-lead-by-week-days/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}
