<?php
namespace app\modules\report\widgets;

use app\modules\report\components\ReportFormForecastDelivery;

/**
 * Class ReportFilterForecastDelivery
 * @package app\modules\report\widgets
 */
class ReportFilterForecastDelivery extends ReportFilter
{
    /**
     * @var ReportFormForecastDelivery
     */
    public $reportForm;

    /**
     * @return string
     */
    public function apply()
    {
        return $this->render('report-filter-forecast-delivery/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}