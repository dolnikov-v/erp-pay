<?php
namespace app\modules\report\widgets;

use app\modules\order\widgets\Sender;
use app\models\User;

/**
 * Class ExportReportToMail
 * @package app\modules\order\widgets
 */
class ExportReportToMail extends Sender
{

    private function getMailList() {
        $userMails = User::find()
            ->select(['email'])
            ->where(['IS NOT', User::tableName() .'.email', null])
            ->andWhere(['<>', User::tableName() .'.email', ''])
            ->andWhere(['=', User::tableName() .'.status', 0])
            ->asArray()
            ->all();

        $mails = [];
        foreach ($userMails as $mail) {
            $mails[] = $mail['email'];
        }

        return $mails;
    }

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('export-report-to-mail/modal', [
            'url' => $this->url,
            'mails' => self::getMailList(),
        ]);
    }
}
