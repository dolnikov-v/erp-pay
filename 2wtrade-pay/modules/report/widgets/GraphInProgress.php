<?php

namespace app\modules\report\widgets;

use app\modules\report\components\filters\TypeDatePartFilter;
use yii\helpers\ArrayHelper;
use yii;

/**
 * Class GraphInProgress
 * @package app\modules\report\widgets
 */
class GraphInProgress extends GraphByCountriesIntervals
{

    /** @var   $dataProvider*/
    public $dataProvider;

    /** @var array */
    protected $products = [];

    /** @var string */
    public $in_progress;

    /** @var array  */
    public $data = [];

    /** @var array  */
    public $dates = [];

    /**
     * @return string
     */
    public function run()
    {
        $series = $this->generateSeries();

        return $this->render('graph-in-progress/index', [
            'inProgress' => $series[$this->in_progress],
            'dates' => $this->formatDates(array_values($this->dates)),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->interval = TypeDatePartFilter::TYPE_DATE_PART_DAY;
        $this->data = $this->prepareData($this->dataProvider->allModels);
        $this->dates = $this->generateDates();
        $this->countries = $this->generateCountries();
    }

    protected function generateDates()
    {
        if ($dates = ArrayHelper::getColumn($this->data, 'date')) {
            $dates = array_unique($dates);
            $dates = array_values($dates);
            asort($dates);
        }

        return $dates;
    }

    /**
     * @param array $data
     * @return array
     */
    protected function prepareData($data)
    {
        foreach ($data as &$model) {

            $year = $model['yearcreatedat'];
            $month = str_pad($model['monthcreatedat'], 2, '0', STR_PAD_LEFT);
            $day = str_pad($model['daycreatedat'], 2, '0', STR_PAD_LEFT);
            $week = str_pad($model['weekcreatedat'], 2, '0', STR_PAD_LEFT);

            switch ($this->interval) {

                case TypeDatePartFilter::TYPE_DATE_PART_DAY:
                    $model['date'] = $year . '-' . $month . '-' . $day;
                    break;

                case TypeDatePartFilter::TYPE_DATE_PART_MONTH:
                    $model['date'] = $year . '-' . $month;
                    break;

                case TypeDatePartFilter::TYPE_DATE_PART_WEEK:
                    $model['date'] = $year . '-' . $week;
                    break;
            }

        }
        unset($model);

        return $data;
    }

    /**
     * @param array $params
     * @return array
     */
    protected function sumDataParams($params)
    {

        $data = [];

        foreach ($this->countries as $country) {
            foreach ($this->dates as $date) {
                foreach ($params as $param) {
                    $data[$country][$date][$param] = 0;
                }
            }
        }


        foreach ($this->data as $model) {
            $country = $model['country_name'];

            $date = $model['date'];
            foreach ($params as $param) {
                $data[$country][$date][$param] += $model[$param];
            }
        }

        return $data;
    }

    /**
     * @return array
     */
    private function generateSeries()
    {
        $this->in_progress = Yii::t('common', 'В процессе');

        $data = $this->sumDataParams([$this->in_progress]);

        foreach ($data as &$country) {
            foreach ($country as &$date) {
                $date[$this->in_progress] = round($date[$this->in_progress], 2);
            }
            unset($date);
        }
        unset($country);

        $seriesList = [
            $this->in_progress => [],
        ];
        foreach ($seriesList as $name => &$series) {
            foreach ($this->countries as $country) {
                $series[] = [
                    'name' => $country,
                    'data' => ArrayHelper::getColumn($data[$country], $name, false),
                ];
            }
        }
        unset($series);

        return $seriesList;
    }
}
