<?php

namespace app\modules\report\widgets;


use app\modules\order\widgets\ShowerColumns;
use Yii;

/**
 * Class ReportCallCenterShowerColumns
 * @package app\modules\report\widgets
 */
class ReportCallCenterShowerColumns extends ShowerColumns
{

    const COLUMN_ORDER_ID = 'order_id';
    const COLUMN_FOREIGN_ID = 'foreign_id';
    const COLUMN_ORDER_FOREIGN_ID = 'order.foreign_id';
    const COLUMN_CUSTOMER_FULL_NAME = 'name';
    const COLUMN_CUSTOMER_PHONE = 'phone';
    const COLUMN_STATUS = 'statusName';
    const COLUMN_UPDATED_AT = 'last_update';
    const COLUMN_CUSTOMER_ADDRESS = 'address';
    const COLUMN_CUSTOMER_CITY = 'city';
    const COLUMN_CUSTOMER_DISTRICT = 'district';
    const COLUMN_CUSTOMER_PROVINCE = 'province';
    const COLUMN_CUSTOMER_STREET_HOUSE = 'street';
    const COLUMN_CALL_CENTER_COMMENT = 'comment';

    /** @var string */
    protected $sessionKey = 'report-call-center.columns';

    /**
     * @return string
     */
    public function renderModal()
    {
        return $this->render('report-call-center-shower-columns/modal', [
            'id' => $this->id,
            'requireColumns' => $this->getRequireColumnsCollection(),
            'manualColumns' => $this->getManualColumnsCollection(),
            'chosenColumns' => $this->getChosenColumns(),
        ]);
    }

    /**
     * @return array
     */
    protected function getRequireColumnsCollection()
    {
        if (is_null($this->requireColumnsCollection)) {
            $this->requireColumnsCollection = [
                self::COLUMN_ORDER_ID => Yii::t('common', 'Номер заказа'),
                self::COLUMN_FOREIGN_ID => Yii::t('common', 'Внешний номер'),
            ];
        }

        return $this->requireColumnsCollection;
    }

    /**
     * @return array
     */
    public function getManualColumnsCollection()
    {
        if (is_null($this->manualColumnsCollection)) {
            $this->manualColumnsCollection = [
                self::COLUMN_CUSTOMER_FULL_NAME => Yii::t('common', 'Полное имя заказчика'),
                self::COLUMN_CUSTOMER_PHONE => Yii::t('common', 'Телефон заказчика'),
                self::COLUMN_CUSTOMER_ADDRESS => Yii::t('common', 'Адрес заказчика'),
                self::COLUMN_CUSTOMER_CITY => Yii::t('common', 'Город заказчика'),
                self::COLUMN_CUSTOMER_PROVINCE => Yii::t('common', 'Провинция заказчика'),
                self::COLUMN_CUSTOMER_STREET_HOUSE => Yii::t('common', 'Улица и номер дома заказчика'),
                self::COLUMN_STATUS => Yii::t('common', 'Статус заказа'),
                self::COLUMN_PRODUCTS => Yii::t('common', 'Товары'),
                self::COLUMN_PRICE => Yii::t('common', 'Конечная цена'),
                self::COLUMN_DELIVERY => Yii::t('common', 'Цена доставки'),
                self::COLUMN_UPDATED_AT => Yii::t('common', 'Дата изменения'),
                self::COLUMN_CALL_CENTER_COMMENT => Yii::t('common', 'Комментарий КЦ'),
            ];
        }

        return $this->manualColumnsCollection;
    }
}
