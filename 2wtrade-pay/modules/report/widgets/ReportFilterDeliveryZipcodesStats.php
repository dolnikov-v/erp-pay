<?php
namespace app\modules\report\widgets;

use app\modules\report\components\ReportFormDeliveryZipcodesStats;

/**
 * Class ReportFilterDeliveryZipcodesStats
 * @package app\modules\report\widgets
 */
class ReportFilterDeliveryZipcodesStats extends ReportFilter
{
    /** @var ReportFormDeliveryZipcodesStats */
    public $reportForm;

    /**
     * @return string
     */
    public function apply()
    {
        return $this->render('report-filter-delivery-zipcodes-stats/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}
