<?php
namespace app\modules\report\widgets;

use app\modules\report\components\ReportFormInProgress;

/**
 * Class ReportFilterInProgress
 * @package app\modules\report\widgets
 */
class ReportFilterInProgress extends ReportFilter
{
    /** @var ReportFormInProgress */
    public $reportForm;

    /**
     * @return string
     */
    public function apply()
    {
        return $this->render('report-filter-in-progress/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}
