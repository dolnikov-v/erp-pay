<?php

namespace app\modules\report\widgets;

use app\modules\report\components\ReportFormCheckUndelivery;

/**
 * Class ReportFilterCheckUndelivery
 * @package app\modules\report\widgets
 */
class ReportFilterCheckUndelivery extends ReportFilter
{
    /** @var ReportFormCheckUndelivery */
    public $reportForm;

    /**
     * @return string
     */
    public function apply()
    {
        return $this->render('report-filter-check-undelivery/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}
