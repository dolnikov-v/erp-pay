<?php
namespace app\modules\report\widgets;

use app\modules\order\widgets\Sender;
use app\models\User;

/**
 * Class DeliveryModal
 * @package app\modules\report\widgets
 */
class DeliveryModal extends Sender
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('delivery-modal/modal');
    }
}
