<?php
namespace app\modules\report\widgets;

use app\modules\report\components\ReportFormTimeSheet;

/**
 * Class ReportFilterTimeSheet
 * @package app\modules\report\widgets
 */
class ReportFilterTimeSheet extends ReportFilter
{
    /** @var ReportFormTimeSheet */
    public $reportForm;

    /**
     * @return string
     */
    public function apply()
    {
        return $this->render('report-filter-time-sheet/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}
