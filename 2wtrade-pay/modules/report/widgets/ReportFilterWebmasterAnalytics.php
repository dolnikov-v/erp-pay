<?php
namespace app\modules\report\widgets;

use app\modules\report\components\ReportFormWebmaster;

/**
 * Class ReportFilterWebmaster
 * @package app\modules\report\widgets
 */
class ReportFilterWebmasterAnalytics extends ReportFilter
{
    /** @var ReportFormWebmaster */
    public $reportForm;

    /**
     * @return string
     */
    public function apply()
    {
        return $this->render('report-filter-webmaster-analytics/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}
