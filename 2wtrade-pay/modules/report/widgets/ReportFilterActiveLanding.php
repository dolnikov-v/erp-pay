<?php
namespace app\modules\report\widgets;

use app\modules\report\components\ReportFormActiveLanding;

/**
 * Class ReportFilterActiveLanding
 * @package app\modules\report\widgets
 */
class ReportFilterActiveLanding extends ReportFilter
{
    /**
     * @var ReportFormActiveLanding
     */
    public $reportForm;

    /**
     * @return string
     */
    public function apply()
    {
        return $this->render('report-filter-active-landing/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }

}
