<?php
namespace app\modules\report\widgets;

use yii\helpers\ArrayHelper;

/**
 * Class GraphLeadByCountry
 * @package app\modules\report\widgets
 */
class GraphLeadByCountry extends GraphByCountriesIntervals
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('graph-lead-by-country/index', [
            'series' => $this->generateSeries(),
            'dates' => $this->dates,
        ]);
    }

    /**
     * @return array
     */
    private function generateSeries()
    {
        $data = $this->sumDataParams(['lidov']);
        foreach ($data as &$country) {
            foreach ($country as &$date) {
                $date['lidov'] = round($date['lidov'], 2);
            } unset($date);
        } unset($country);

        $series = [];
        foreach ($this->countries as $country) {
            $series[] = [
                'name' => $country,
                'data' => ArrayHelper::getColumn($data[$country], 'lidov', false),
            ];
        }

        return $series;
    }
}
