<?php
namespace app\modules\report\widgets;

use app\modules\report\components\ReportForm;
use Yii;
use yii\base\Widget;

/**
 * Class ReportGroupBy
 * @package app\modules\report\widgets
 */
class ReportGroupBy extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var ReportForm */
    public $reportForm;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('report-group-by', [
            'form' => $this->form,
            'reportForm' => $this->reportForm,
        ]);
    }
}
