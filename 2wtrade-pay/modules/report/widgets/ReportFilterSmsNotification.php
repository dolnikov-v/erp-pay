<?php
namespace app\modules\report\widgets;

use app\modules\report\components\ReportFormSmsNotification;

/**
 * Class ReportFilterSmsNotification
 * @package app\modules\report\widgets
 */
class ReportFilterSmsNotification extends ReportFilter
{
    /**
     * @var ReportFilterSmsPollHistory
     */
    public $reportForm;

    /**
     * @return string
     */
    public function apply()
    {
        return $this->render('report-sms-notification/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}