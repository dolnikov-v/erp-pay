<?php
namespace app\modules\report\widgets;

use app\modules\report\components\ReportFormDaily;

/**
 * Class ReportFilterDaily
 * @package app\modules\report\widgets
 */
class ReportFilterDaily extends ReportFilter
{
    /**
     * @var ReportFormDaily
     */
    public $reportForm;

    /**
     * @return string
     */
    public function apply()
    {
        return $this->render('report-filter-daily/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}
