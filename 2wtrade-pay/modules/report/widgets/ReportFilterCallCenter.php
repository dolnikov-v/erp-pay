<?php
namespace app\modules\report\widgets;

use app\modules\report\components\ReportFormCallCenter;

/**
 * Class ReportFilterCallCenter
 * @package app\modules\report\widgets
 */
class ReportFilterCallCenter extends ReportFilter
{
    /** @var ReportFormCallCenter */
    public $reportForm;

    /**
     * @return string
     */
    public function apply()
    {
        return $this->render('report-filter-call-center/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}
