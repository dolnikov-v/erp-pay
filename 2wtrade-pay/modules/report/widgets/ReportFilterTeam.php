<?php
namespace app\modules\report\widgets;

use app\modules\report\components\ReportFormTeam;

/**
 * Class ReportFilterTeam
 * @package app\modules\report\widgets
 */
class ReportFilterTeam extends ReportFilter
{
    /** @var ReportFormTeam */
    public $reportForm;

    /**
     * @return string
     */
    public function apply()
    {
        return $this->render('report-filter-team/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}
