<?php
namespace app\modules\report\widgets;

use app\modules\report\components\ReportFormApproved;

/**
 * Class ReportFilterApproved
 * @package app\modules\report\widgets
 */
class ReportFilterApproved extends ReportFilter
{
    /** @var ReportFormApproved */
    public $reportForm;

    /**
     * @return string
     */
    public function apply()
    {
        return $this->render('report-filter-approved/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}
