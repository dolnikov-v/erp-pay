<?php
namespace app\modules\report\widgets;

/**
 * Class ReportFilterForecastWorkloadOperatorsByHours
 * @package app\modules\report\widgets
 */
class ReportFilterForecastWorkloadOperatorsByHours extends ReportFilter
{
    public $reportForm;

    public function apply()
    {
        return $this->render('report-filter-forecast-workload-operators-by-hours/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}
