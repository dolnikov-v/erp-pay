<?php
namespace app\modules\report\widgets;

use app\modules\report\components\ReportFormPurchaseStorage;

/**
 * Class ReportFilterPurchaseStorage
 * @package app\modules\report\widgets
 */
class ReportFilterPurchaseStorage extends ReportFilter
{
    /** @var ReportFormPurchaseStorage */
    public $reportForm;

    /**
     * @return string
     */
    public function apply()
    {
        return $this->render('report-filter-purchase-storage/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}