<?php
namespace app\modules\report\widgets;

use app\modules\report\components\ReportFormCallCenterCheckRequest;

/**
 * Class ReportFilterCallCenter
 * @package app\modules\report\widgets
 */
class ReportFilterCallCenterCheckRequest extends ReportFilter
{
    /** @var ReportFormCallCenterCheckRequest */
    public $reportForm;

    /**
     * @return string
     */
    public function apply()
    {
        return $this->render('report-filter-call-center-check-request/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}
