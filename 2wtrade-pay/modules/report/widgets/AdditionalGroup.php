<?php
namespace app\modules\report\widgets;

use yii\base\Widget;

/**
 * Class AdditionalGroup
 * @package app\modules\report\widgets
 */
class AdditionalGroup extends Widget
{
    /**
     * @var \app\components\widgets\ActiveForm
     */
    public $form;

    /**
     * @var \app\modules\report\components\ReportForm
     */
    public $model;

    /**
     * @var string
     */
    protected $modalId = 'modal_additional_group';

    /**
     * @return string
     */
    public function renderButton()
    {
        return $this->render('additional-group/button', [
            'modalId' => $this->modalId,
        ]);
    }

    /**
     * @return string
     */
    public function renderModal()
    {
        return $this->render('additional-group/modal', [
            'form' => $this->form,
            'model' => $this->model,
            'modalId' => $this->modalId,
        ]);
    }
}
