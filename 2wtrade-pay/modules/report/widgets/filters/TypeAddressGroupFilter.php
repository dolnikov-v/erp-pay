<?php

namespace app\modules\report\widgets\filters;

use yii\base\Widget;
/**
 * Class TypeOrderSourceFilter
 * @package app\modules\report\widgets\filters
 */
class TypeAddressGroupFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var \app\modules\report\components\filters\TypeAddressGroupFilter */
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('type-address-group-filter/filter', [
            'form' => $this->form,
            'model' => $this->model,
        ]);
    }
}