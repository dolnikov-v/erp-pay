<?php
namespace app\modules\report\widgets\filters;

use yii\base\Widget;

/**
 * Class ReportShipmentFilter
 * @package app\modules\report\widgets\filters
 */
class ReportShipmentFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var \app\modules\report\components\filters\ReportShipmentFilter */
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('shipment-filter/filter', [
            'form' => $this->form,
            'model' => $this->model,
        ]);
    }
}