<?php
namespace app\modules\report\widgets\filters;

use yii\base\Widget;

/**
 * Class OrderLogisticListFilter
 * @package app\modules\report\widgets\filters
 */
class OrderLogisticListFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var \app\modules\report\components\filters\OrderLogisticListFilter */
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('list-filter/filter', [
            'form' => $this->form,
            'model' => $this->model,
        ]);
    }
}
