<?php
namespace app\modules\report\widgets\filters;

use yii\base\Widget;

/**
 * Class AnswerTypeSmsPollHistoryFilter
 * @package app\modules\report\widgets\filters
 */
class AnswerTypeSmsPollHistoryFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var \app\modules\report\components\filters\AnswerTypeSmsPollHistoryFilter */
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('answer-type-sms-poll-history-filter/filter', [
            'form' => $this->form,
            'model' => $this->model,
        ]);
    }
}
