<?php
namespace app\modules\report\widgets\filters;

use yii\base\Widget;

/**
 * Class DebtFilter
 * @package app\modules\report\widgets\filters
 */
class DebtFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var \app\modules\report\components\filters\DebtFilter */
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('debt-filter/filter', [
            'form' => $this->form,
            'model' => $this->model,
        ]);
    }
}
