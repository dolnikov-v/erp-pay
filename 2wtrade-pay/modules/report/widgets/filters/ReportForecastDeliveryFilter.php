<?php
namespace app\modules\report\widgets\filters;

use yii\base\Widget;

/**
 * Class ReportForecastDeliveryFilter
 * @package app\modules\report\widgets\filters
 */
class ReportForecastDeliveryFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var \app\modules\report\components\filters\ReportForecastDeliveryFilter */
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('forecast-delivery-filter/filter', [
            'form' => $this->form,
            'model' => $this->model,
        ]);
    }
}