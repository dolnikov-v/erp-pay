<?php
namespace app\modules\report\widgets\filters;

use yii\base\Widget;

/**
 * Class AnswerTypeSmsNotificationFilter
 * @package app\modules\report\widgets\filters
 */
class AnswerTypeSmsNotificationFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var \app\modules\report\components\filters\AnswerTypeSmsNotificationFilter */
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('answer-type-sms-notification-filter/filter', [
            'form' => $this->form,
            'model' => $this->model,
        ]);
    }
}
