<?php
namespace app\modules\report\widgets\filters;

use yii\base\Widget;

/**
 * Class ReportValidateFinanceFilter
 * @package app\modules\report\widgets\filters
 */
class ReportValidateFinanceFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var \app\modules\report\components\filters\ReportValidateFinanceFilter */
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('report-validate-finance-filter/filter', [
            'form' => $this->form,
            'model' => $this->model,
        ]);
    }
}