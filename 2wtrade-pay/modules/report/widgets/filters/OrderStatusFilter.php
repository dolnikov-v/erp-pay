<?php
namespace app\modules\report\widgets\filters;

use yii\base\Widget;

/**
 * Class OrderStatusFilter
 * @package app\modules\report\widgets\filters
 */
class OrderStatusFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var \app\modules\report\components\filters\OrderStatusFilter */
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('orderStatus-filter/filter', [
            'form' => $this->form,
            'model' => $this->model,
        ]);
    }
}
