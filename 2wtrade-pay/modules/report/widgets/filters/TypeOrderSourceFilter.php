<?php

namespace app\modules\report\widgets\filters;

use yii\base\Widget;
/**
 * Class TypeOrderSourceFilter
 * @package app\modules\report\widgets\filters
 */
class TypeOrderSourceFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var \app\modules\report\components\filters\TypeOrderSourceFilter */
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('type-order-source-filter/filter', [
            'form' => $this->form,
            'model' => $this->model,
        ]);
    }
}