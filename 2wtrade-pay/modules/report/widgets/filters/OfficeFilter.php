<?php
namespace app\modules\report\widgets\filters;

use yii\base\Widget;

/**
 * Class OfficeFilter
 * @package app\modules\report\widgets\filters
 */
class OfficeFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var \app\modules\report\components\filters\OfficeFilter */
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('office-filter/filter', [
            'form' => $this->form,
            'model' => $this->model,
        ]);
    }
}
