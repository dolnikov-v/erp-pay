<?php
namespace app\modules\report\widgets\filters;

use yii\base\Widget;

/**
 * Class LeadProductFilter
 * @package app\modules\report\widgets\filters
 */
class LeadProductFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var \app\modules\report\components\filters\LeadProductFilter */
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('lead-product-filter/filter', [
            'form' => $this->form,
            'model' => $this->model,
        ]);
    }
}
