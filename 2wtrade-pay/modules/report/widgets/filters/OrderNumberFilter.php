<?php

namespace app\modules\report\widgets\filters;

use yii\base\Widget;
/**
 * Class OrderNumberFilter
 * @package app\modules\report\widgets\filters
 */
class OrderNumberFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var \app\modules\report\components\filters\OrderNumberFilter */
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('order-number-filter/filter', [
            'form' => $this->form,
            'model' => $this->model,
        ]);
    }
}