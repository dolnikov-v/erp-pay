<?php
namespace app\modules\report\widgets\filters;

use yii\base\Widget;

/**
 * Class DateFilter
 * @package app\modules\report\widgets\filters
 */
class DateFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var \app\modules\report\components\filters\DateFilter */
    public $model;

    public $showSelectType = false;

    public $timeOffset;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('date-filter/filter', [
            'form' => $this->form,
            'model' => $this->model,
            'showSelectType' => $this->showSelectType,
            'timeOffset' => $this->timeOffset,
        ]);
    }
}
