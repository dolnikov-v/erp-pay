<?php
namespace app\modules\report\widgets\filters;

use yii\base\Widget;

/**
 * Class ReportForecastLeadFilter
 * @package app\modules\report\widgets\filters
 */
class ReportForecastLeadFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var \app\modules\report\components\filters\ReportForecastLeadFilter */
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('forecast-lead-filter/filter', [
            'form' => $this->form,
            'model' => $this->model,
        ]);
    }
}