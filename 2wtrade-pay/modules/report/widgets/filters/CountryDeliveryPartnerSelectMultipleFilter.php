<?php

namespace app\modules\report\widgets\filters;

use yii\base\Widget;

/**
 * Class CountryDeliveryPartnerSelectMultipleFilter
 * @package app\modules\report\widgets\filters
 */
class CountryDeliveryPartnerSelectMultipleFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var \app\modules\report\components\filters\CountryDeliveryPartnerSelectMultipleFilter */
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('country-delivery-partner-multiple-select-filter/filter', [
            'form' => $this->form,
            'model' => $this->model,
        ]);
    }
}