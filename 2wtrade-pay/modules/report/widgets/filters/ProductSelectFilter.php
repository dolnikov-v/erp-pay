<?php

namespace app\modules\report\widgets\filters;

use yii\base\Widget;

/**
 * Class ProductSelectFilter
 * @package app\modules\report\widgets\filters
 */
class ProductSelectFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var \app\modules\report\components\filters\ProductSelectFilter */
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('product-select-filter/filter', [
            'form' => $this->form,
            'model' => $this->model,
        ]);
    }
}