<?php

namespace app\modules\report\widgets\filters;

use yii\base\Widget;
/**
 * Class CountrySelectFilter
 * @package app\modules\report\widgets\filters
 */
class CountrySelectFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var \app\modules\report\components\filters\CountrySelectFilter */
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('country-select-filter/filter', [
            'form' => $this->form,
            'model' => $this->model,
        ]);
    }
}