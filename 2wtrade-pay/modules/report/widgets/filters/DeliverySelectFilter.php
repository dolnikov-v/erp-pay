<?php

namespace app\modules\report\widgets\filters;

use yii\base\Widget;

/**
 * Class DeliverySelectFilter
 * @package app\modules\report\widgets\filters
 */
class DeliverySelectFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var \app\modules\report\components\filters\DeliverySelectFilter */
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('delivery-select-filter/filter', [
            'form' => $this->form,
            'model' => $this->model,
        ]);
    }
}