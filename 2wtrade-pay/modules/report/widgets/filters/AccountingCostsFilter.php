<?php
namespace app\modules\report\widgets\filters;

use yii\base\Widget;

/**
 * Class AccountingCostsFilter
 * @package app\modules\report\widgets\filters
 */
class AccountingCostsFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var \app\modules\report\components\filters\AccountingCostsFilter */
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('accounting-costs-filter/filter', [
            'form' => $this->form,
            'model' => $this->model,
        ]);
    }
}
