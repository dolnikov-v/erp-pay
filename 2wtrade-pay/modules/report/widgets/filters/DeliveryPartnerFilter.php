<?php

namespace app\modules\report\widgets\filters;


use app\widgets\Widget;

/**
 * Class DeliveryPartnerFilter
 * @package app\modules\report\widgets\filters
 */
class DeliveryPartnerFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var \app\modules\report\components\filters\DeliveryPartnerFilter */
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('delivery-partner-filter/filter', [
            'form' => $this->form,
            'model' => $this->model,
        ]);
    }
}
