<?php
namespace app\modules\report\widgets\filters;

use yii\base\Widget;

/**
 * Class CallCenterFilter
 * @package app\modules\report\widgets\filters
 */
class CallCenterFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var \app\modules\report\components\filters\CallCenterFilter */
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('call-center-filter/filter', [
            'form' => $this->form,
            'model' => $this->model,
        ]);
    }
}
