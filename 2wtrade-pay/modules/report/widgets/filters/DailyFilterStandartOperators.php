<?php

namespace app\modules\report\widgets\filters;

use yii\base\Widget;
/**
 * Class DailyFilterStandartOperators
 * @package app\modules\report\widgets\filters
 */
class DailyFilterStandartOperators extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var \app\modules\report\components\filters\DailyFilterStandartOperators */
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('daily-filter/filter', [
            'form' => $this->form,
            'model' => $this->model,
            'options' => []
        ]);
    }
}
