<?php
namespace app\modules\report\widgets\filters;

use yii\base\Widget;

/**
 * Class WebmasterFilter
 * @package app\modules\report\widgets\filters
 */
class WebmasterFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var \app\modules\report\components\filters\WebmasterFilter */
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('webmaster-filter/filter', [
            'form' => $this->form,
            'model' => $this->model,
        ]);
    }
}
