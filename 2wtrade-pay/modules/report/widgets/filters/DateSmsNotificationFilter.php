<?php
namespace app\modules\report\widgets\filters;

use yii\base\Widget;

/**
 * Class DateSmsNotificationFilter
 * @package app\modules\report\widgets\filters
 */
class DateSmsNotificationFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var \app\modules\report\components\filters\DateSmsNotificationFilter */
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('date-sms-notification-filter/filter', [
            'form' => $this->form,
            'model' => $this->model,
        ]);
    }
}
