<?php

/** @var app\components\widgets\ActiveForm $form */
/** @var app\modules\report\components\filters\AnswerTypeSmsPollHistoryFilter $model */

?>

<?= $form->field($model, 'type_poll')->select2List($model->types_poll, [
    'id' => false,
]) ?>
