<?php

/** @var app\components\widgets\ActiveForm $form */
/** @var app\modules\report\components\filters\OfficeFilter $model */


?>

<?= $form->field($model, 'office')->select2ListMultiple($model->offices) ?>
