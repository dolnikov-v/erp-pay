<?php
/** @var app\components\widgets\ActiveForm $form */
/** @var app\modules\report\components\filters\CountrySelectFilter $model */
?>

<?=$form->field($model, 'country_ids')->select2ListMultiple($model->countries->collection(), ['value' => $model->country_ids]);?>