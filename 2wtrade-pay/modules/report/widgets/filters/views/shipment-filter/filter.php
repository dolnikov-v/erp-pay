<?php
use app\widgets\assets\DatePickerAsset;

/** @var app\components\widgets\ActiveForm $form */
/** @var app\modules\report\components\filters\ReportShipmentFilter $model */

DatePickerAsset::register($this);
?>
<div class="col-lg-6">
    <?= $form->field($model, 'date')->dateRangePicker('from', 'to') ?>
</div>
<div class="col-lg-3" id="sale_delivery">
    <?= $form->field($model, 'delivery')->select2List($model->deliveries, [
        'prompt' => '—',
        'id' => false,
    ]) ?>
</div>
