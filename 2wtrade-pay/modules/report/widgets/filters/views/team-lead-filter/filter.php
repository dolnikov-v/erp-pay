<?php

/** @var app\components\widgets\ActiveForm $form */
/** @var app\modules\report\components\filters\TeamLeadFilter $model */

?>

<?= $form->field($model, 'teamLead')->select2List($model->teamLeads, [
    'prompt' => '—'
]) ?>


