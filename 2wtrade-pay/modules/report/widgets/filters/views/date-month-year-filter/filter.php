<?php
use app\widgets\assets\DatePickerAsset;

/** @var app\components\widgets\ActiveForm $form */
/** @var app\modules\report\components\filters\DateMonthYearFilter $model */

DatePickerAsset::register($this);
?>

<div class="col-lg-3">
    <?= $form->field($model, 'date')->datePicker([
        'type' => 'month',
    ]) ?>
</div>
