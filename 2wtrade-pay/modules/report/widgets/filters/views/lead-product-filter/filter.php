<?php
/** @var app\components\widgets\ActiveForm $form */
/** @var app\modules\report\components\filters\LeadProductFilter $model */
?>

<?= $form->field($model, 'leadProduct')->select2List($model->products, [
    'prompt' => '—',
    'id' => false,
    'length' => false,
]) ?>


