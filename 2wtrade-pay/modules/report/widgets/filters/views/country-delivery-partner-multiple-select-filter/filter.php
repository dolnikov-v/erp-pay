<?php
/** @var app\components\widgets\ActiveForm $form */
/** @var app\modules\report\components\filters\CountryDeliveryPartnerSelectMultipleFilter $model */
?>
<div class="container-countrydeliverypartnersmultiple">
    <div class="col-lg-4 countrymultiple">
        <?=$form->field($model, 'country_ids')->select2ListMultiple($model->countries->collection(), ['value' => $model->country_ids]);?>
    </div>
    <div class="col-lg-4 deliverymultiple">
        <?=$form->field($model, 'delivery_ids')->select2ListMultiple($model->deliveries->collection(), ['value' => $model->delivery_ids]);?>
    </div>
    <div class="col-lg-4 partnersmultiple">
        <?=$form->field($model, 'partners_values')->select2ListMultiple($model->partners, ['value' => $model->partners_values]);?>
    </div>
</div>