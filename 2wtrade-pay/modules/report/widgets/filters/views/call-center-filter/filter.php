<?php

/** @var app\components\widgets\ActiveForm $form */
/** @var app\modules\report\components\filters\CallCenterFilter $model */


if ($model->multiple): ?>
    <?= $form->field($model, 'callCenter')->select2ListMultiple($model->callCenters) ?>
<?php else: ?>
    <?= $form->field($model, 'callCenter')->select2List($model->callCenters, [
        'prompt' => '—'
    ]) ?>
<?php endif; ?>