<?php
use app\widgets\assets\DatePickerAsset;

/** @var app\components\widgets\ActiveForm $form */
/** @var app\modules\report\components\filters\ReportForecastDeliveryFilter $model */

DatePickerAsset::register($this);
?>
<div class="col-lg-6">
    <?= $form->field($model, 'date')->dateRangePicker('from', 'to') ?>
</div>
<div class="col-lg-3">
    <?= $form->field($model, 'type')->select2List($model->types) ?>
</div>