<?php

/** @var app\components\widgets\ActiveForm $form */
/** @var app\modules\report\components\filters\OrderStatusFilter $model */

?>

<?= $form->field($model, 'orderStatus')->select2ListMultiple($model->orderStatuses, [
    'prompt' => '—',
    'id' => false,
    'multiple' => 'multiple',
]) ?>


