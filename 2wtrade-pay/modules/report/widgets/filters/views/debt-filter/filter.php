<?php

/** @var app\components\widgets\ActiveForm $form */
/** @var app\modules\report\components\filters\DebtFilter $model */

echo $form->field($model, 'country', ['labelOptions' => ['label' => false]])->countryDelivery('country', 'delivery', $model->country, $model->delivery);
?>
