<?php
use app\widgets\assets\DatePickerAsset;
use app\widgets\DateRangePicker;

/** @var app\components\widgets\ActiveForm $form */
/** @var app\modules\report\components\filters\ReportForecastLeadFilter $model */

DatePickerAsset::register($this);
?>
<div class="col-lg-6">
    <?= $form->field($model, 'date')->dateRangePicker('from', 'to', [
        'from' => ['disabled' => true],
        'minDate' => date("Y-m-d"),
        'ranges' => ['list' => DateRangePicker::getFutureRanges()]
    ]) ?>
</div>
