<?php
/** @var app\components\widgets\ActiveForm $form */
/** @var app\modules\report\components\filters\OrderNumberFilter $model */
?>

<?= $form->field($model, 'number')->textInput(['value' => $model->number,])?>