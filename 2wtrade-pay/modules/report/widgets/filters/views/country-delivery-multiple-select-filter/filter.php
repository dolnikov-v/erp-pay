<?php

use app\modules\report\assets\FilterCountryDeliveryAsset;

/** @var app\components\widgets\ActiveForm $form */
/** @var app\modules\report\components\filters\CountryDeliverySelectMultipleFilter $model */

FilterCountryDeliveryAsset::register($this);
?>
<div class="container-countrydeliverymultiple row">
    <div class="col-lg-6 countrymultiple">
        <?= $form->field($model, 'country_ids')
            ->select2List($model->countries->collection(), [
                'value' => $model->country_ids,
                'multiple' => $model->multipleCountrySelect ? 'multiple' : '',
                'length' =>$model->multipleCountrySelect ? -1 : false,
            ]); ?>
    </div>
    <div class="col-lg-6 deliverymultiple">
        <?= $form->field($model, 'delivery_ids')
            ->select2List($model->deliveries->collection(), [
                'value' => $model->country_ids ? $model->delivery_ids : [],
                'multiple' => $model->multipleDeliverySelect ? 'multiple' : '',
                'length' => $model->multipleDeliverySelect ? -1 : false,
            ]); ?>
    </div>
</div>