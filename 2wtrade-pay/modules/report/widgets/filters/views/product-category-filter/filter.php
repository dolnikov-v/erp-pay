<?php

/** @var app\components\widgets\ActiveForm $form */
/** @var app\modules\report\components\filters\ProductCategoryFilter $model */

?>

<?= $form->field($model, 'product_category')->select2List($model->product_categories, [
    'prompt' => '—',
    'id' => false,
]) ?>
