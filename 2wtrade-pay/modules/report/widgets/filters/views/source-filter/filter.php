<?php

/** @var app\components\widgets\ActiveForm $form */
/** @var app\modules\report\components\filters\SourceFilter $model */

?>
<?= $form->field($model, 'source')->select2List($model->sources, [
    'prompt' => '—',
]) ?>