<?php
use app\widgets\assets\DatePickerAsset;

/** @var app\components\widgets\ActiveForm $form */
/** @var app\modules\report\components\filters\DailyFilter $model */
/** @var array $options */

DatePickerAsset::register($this);
?>

<div class="col-lg-6">
    <?= $form->field($model, 'date')->dateRangePicker('from', 'to', $options) ?>
</div>
