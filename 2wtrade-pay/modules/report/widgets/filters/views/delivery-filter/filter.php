<?php

/** @var app\components\widgets\ActiveForm $form */
/** @var app\modules\report\components\filters\DeliveryFilter $model */

?>

<?= $form->field($model, 'delivery')->select2List($model->deliveries, [
    'prompt' => '—',
    'id' => false,
]) ?>


