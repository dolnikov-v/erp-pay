<?php
use app\widgets\assets\DatePickerAsset;

/** @var app\components\widgets\ActiveForm $form */
/** @var app\modules\report\components\filters\ReportAdvertisingFilter $model */

DatePickerAsset::register($this);
?>

<div class="col-lg-6">
    <div class="form-group">
        <?= $form->field($model, 'date')->dateRangePicker('from', 'to') ?>
    </div>
</div>
