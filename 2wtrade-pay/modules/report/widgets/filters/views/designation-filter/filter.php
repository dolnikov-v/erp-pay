<?php

/** @var app\components\widgets\ActiveForm $form */
/** @var app\modules\report\components\filters\DesignationFilter $model */

?>

<?= $form->field($model, 'designation')->select2ListMultiple($model->designations) ?>


