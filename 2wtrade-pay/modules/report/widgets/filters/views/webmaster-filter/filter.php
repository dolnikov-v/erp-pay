<?php
/** @var app\components\widgets\ActiveForm $form */
/** @var app\modules\report\components\filters\WebmasterFilter $model */
?>

<?= $form->field($model, 'webmaster')->select2List($model->webmasters, [
    'prompt' => '—',
    'id' => false,
]) ?>


