<?php
/** @var app\components\widgets\ActiveForm $form *//**
 * @var app\modules\report\components\filters\ProductSelect $model */;
?>

<?= $form->field($model, 'product_ids')->select2ListMultiple($model->products->collection(), ['value' => $model->product_ids]); ?>