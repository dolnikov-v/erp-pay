<?php

/** @var app\components\widgets\ActiveForm $form */
/** @var app\modules\report\components\filters\AnswerTypeSmsNotificationFilter $model */

?>

<?= $form->field($model, 'type_notification')->select2List($model->types, [
    'id' => false,
]) ?>
