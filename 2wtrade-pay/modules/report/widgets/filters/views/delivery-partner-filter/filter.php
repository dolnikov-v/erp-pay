<?php

/** @var app\components\widgets\ActiveForm $form */
/** @var app\modules\report\components\filters\DeliveryPartnerFilter $model */

?>

<?= $form->field($model, 'partner')->select2List($model->partners, [
    'prompt' => '—',
    'id' => false,
]) ?>


