<?php
/** @var app\components\widgets\ActiveForm $form *//** @var app\modules\report\components\filters\DeliverySelect $model */; ?>

<?= $form->field($model, 'delivery_ids')->select2ListMultiple($model->deliveries->collection(), ['value' => $model->delivery_ids]); ?>