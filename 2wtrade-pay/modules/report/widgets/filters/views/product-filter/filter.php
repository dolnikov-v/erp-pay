<?php
/** @var app\components\widgets\ActiveForm $form */
/** @var app\modules\report\components\filters\ProductFilter $model */
?>

<?= $form->field($model, 'product')->select2List($model->products, [
    'prompt' => '—',
    'id' => false,
    'length' => false
]) ?>


