<?php

/** @var app\components\widgets\ActiveForm $form */
/** @var app\modules\report\components\filters\CuratorUserFilter $model */

;?>

<?=$form->field($model, 'user_ids')->select2ListMultiple($model->curatorUsers, ['value' => $model->user_ids]);?>