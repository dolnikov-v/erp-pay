<?php

/** @var app\components\widgets\ActiveForm $form */
/** @var app\modules\report\components\filters\ForeignOperatorFilter $model */

$list = $model->operators ? array_combine($model->operators, $model->operators) : null;
?>

<?= $form->field($model, 'operators')->select2ListMultiple($list, [
    'id' => false,
    'multiple' => 'multiple',
    'length' => false,
    'tags' => true,
]) ?>