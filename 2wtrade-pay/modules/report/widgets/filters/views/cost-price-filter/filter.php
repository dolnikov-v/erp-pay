<?php
use app\widgets\assets\DatePickerAsset;

/** @var app\components\widgets\ActiveForm $form */
/** @var app\modules\report\components\filters\CostPriceFilter $model */

DatePickerAsset::register($this);
?>

<div class="col-lg-6">
    <?= $form->field($model, 'date')->dateRangePicker('from', 'to') ?>
</div>
<div class="col-lg-2">
    <?= $form->field($model, 'product')->select2List($model->products, [
        'prompt' => '—',
        'id' => false,
    ]) ?>
</div>
