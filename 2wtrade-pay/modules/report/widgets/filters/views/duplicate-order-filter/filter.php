<?php
/** @var app\components\widgets\ActiveForm $form */
/** @var app\modules\report\components\filters\DuplicateOrderFilter $model */
?>

<?= $form->field($model, 'type_duplicate')->select2List($model->types_duplicate) ?>
