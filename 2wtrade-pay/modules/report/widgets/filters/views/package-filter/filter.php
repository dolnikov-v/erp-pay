<?php
/** @var app\components\widgets\ActiveForm $form */
/** @var app\modules\report\components\filters\PackageFilter $model */
?>

<?= $form->field($model, 'package')->select2List($model->packages, [
    'prompt' => Yii::t('common', 'Маркетинговая акция'),
    'id' => false,
]) ?>


