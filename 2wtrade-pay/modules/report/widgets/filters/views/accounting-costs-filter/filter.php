<?php

/** @var app\components\widgets\ActiveForm $form */
/** @var app\modules\report\components\filters\AccountingCostsFilter $model */
?>

<div class="col-lg-12">
    <?= $form->field($model, 'date')->dateRangePicker('from', 'to') ?>
</div>
