<?php
/** @var app\components\widgets\ActiveForm $form */
/** @var app\modules\report\components\filters\ContractFilter $model */
?>

<?= $form->field($model, 'contract_id')
    ->select2List($model->contracts, [
        'value' => $model->contract_id,
        'prompt' => '-',
        'length' => 1
    ]); ?>