<?php

/** @var app\components\widgets\ActiveForm $form */
/** @var app\modules\report\components\filters\CallCenterMonthlyStatementFilter $model */

?>

<?= $form->field($model, 'monthlyStatement')->select2List($model->monthlyStatements, [
    'prompt' => '—'
]) ?>


