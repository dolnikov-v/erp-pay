<?php

/** @var app\components\widgets\ActiveForm $form */
/** @var app\modules\report\components\filters\TypeAddressGroupFilter $model */

?>

<?=$form->field($model, 'type_address_group')->select2List($model->typeAdressGroupFilter);?>