<?php

/** @var app\components\widgets\ActiveForm $form */
/** @var app\modules\report\components\filters\TypeOrderSourceFilter $model */

?>

<?=$form->field($model, 'type_order_source')->select2List($model->typeOrderSourceFilter);?>