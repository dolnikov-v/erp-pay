<?php
use app\widgets\assets\DatePickerAsset;

/** @var app\components\widgets\ActiveForm $form */
/** @var app\modules\report\components\filters\DateFilter $model */
/** @var bool $showSelectType */

DatePickerAsset::register($this);
?>

<div class="row">
    <div class="col-xs-<?= $showSelectType ? 8 : 12 ?>">
        <?= $form->field($model, 'date')->dateRangePicker('from', 'to') ?>
    </div>
    <?php if ($showSelectType): ?>
        <div class="col-xs-4">
            <?= $form->field($model, 'type')->select2List($model->types) ?>
        </div>
    <?php endif; ?>
</div>
<div class="row">
    <div class="col-xs-8">
        <?= $form->field($model, 'timezone')->select2List($model->timezones, ['value' => $model->timezone]) ?>
    </div>
</div>
