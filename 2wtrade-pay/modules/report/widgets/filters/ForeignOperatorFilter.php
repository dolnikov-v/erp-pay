<?php
namespace app\modules\report\widgets\filters;

use yii\base\Widget;

/**
 * Class ForeignOperatorFilter
 * @package app\modules\report\widgets\filters
 */
class ForeignOperatorFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var \app\modules\report\components\filters\ForeignOperatorFilter */
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('foreign-operator-filter/filter', [
            'form' => $this->form,
            'model' => $this->model,
        ]);
    }
}
