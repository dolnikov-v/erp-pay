<?php
namespace app\modules\report\widgets\filters;

use yii\base\Widget;

/**
 * Class ReceivableFilter
 * @package app\modules\report\widgets\filters
 */
class ReceivableFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var \app\modules\report\components\filters\ReceivableFilter */
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('receivable-filter/filter', [
            'form' => $this->form,
            'model' => $this->model,
        ]);
    }
}
