<?php

namespace app\modules\report\widgets\filters;

use yii\base\Widget;
/**
 * Class ContractFilter
 * @package app\modules\report\widgets\filters
 */
class ContractFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var \app\modules\report\components\filters\ContractFilter */
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('contract-filter/filter', [
            'form' => $this->form,
            'model' => $this->model,
        ]);
    }
}