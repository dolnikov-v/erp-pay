<?php
namespace app\modules\report\widgets\filters;

use yii\base\Widget;

/**
 * Class CallCenterMonthlyStatementFilterWidget
 * @package app\modules\report\widgets\filters
 */
class CallCenterMonthlyStatementFilterWidget extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var \app\modules\report\components\filters\CallCenterMonthlyStatementFilter */
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('call-center-monthly-statement-filter/filter', [
            'form' => $this->form,
            'model' => $this->model,
        ]);
    }
}
