<?php
namespace app\modules\report\widgets\filters;

use yii\base\Widget;

/**
 * Class DateSmsPollHistoryFilter
 * @package app\modules\report\widgets\filters
 */
class DateSmsPollHistoryFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var \app\modules\report\components\filters\DateSmsPollHistoryFilter */
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('date-sms-poll-history-filter/filter', [
            'form' => $this->form,
            'model' => $this->model,
        ]);
    }
}
