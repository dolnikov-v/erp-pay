<?php

namespace app\modules\report\widgets\filters;

use yii\base\Widget;

/**
 * Class ProductCategoryFilter
 * @package app\modules\report\widgets\filters
 */
class ProductCategoryFilter extends Widget
{
    /**
     * @var \app\components\widgets\ActiveForm
     */
    public $form;

    /**
     * @var \app\modules\report\components\filters\DeliveryFilter
     */
    public $model;

    /**
     * @return string
     */
    public function run(): string
    {
        return $this->render('product-category-filter/filter', [
            'form' => $this->form,
            'model' => $this->model,
        ]);
    }
}
