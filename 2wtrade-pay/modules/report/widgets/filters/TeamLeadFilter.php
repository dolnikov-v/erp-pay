<?php
namespace app\modules\report\widgets\filters;

use yii\base\Widget;

/**
 * Class TeamLeadFilter
 * @package app\modules\report\widgets\filters
 */
class TeamLeadFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var \app\modules\report\components\filters\TeamLeadFilter */
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('team-lead-filter/filter', [
            'form' => $this->form,
            'model' => $this->model,
        ]);
    }
}
