<?php
namespace app\modules\report\widgets\filters;

use yii\base\Widget;

/**
 * Class DateFilter
 * @package app\modules\report\widgets\filters
 */
class DateFilterTopProduct extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var \app\modules\report\components\filters\DateFilter */
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('date-filter-top-product/filter', [
            'form' => $this->form,
            'model' => $this->model,
        ]);
    }
}