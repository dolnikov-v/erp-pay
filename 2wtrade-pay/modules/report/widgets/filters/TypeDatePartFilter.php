<?php

namespace app\modules\report\widgets\filters;

use yii\base\Widget;
/**
 * Class TypeDatePartFilter
 * @package app\modules\report\widgets\filters
 */
class TypeDatePartFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var \app\modules\report\components\filters\CountrySelectFilter */
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('type-date-part-filter/filter', [
            'form' => $this->form,
            'model' => $this->model,
        ]);
    }
}