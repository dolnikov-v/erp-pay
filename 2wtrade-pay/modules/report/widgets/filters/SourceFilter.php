<?php
namespace app\modules\report\widgets\filters;

use yii\base\Widget;

/**
 * Class SourceFilter
 * @package app\modules\report\widgets\filters
 */
class SourceFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var \app\modules\report\components\filters\SourceFilter */
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('source-filter/filter', [
            'form' => $this->form,
            'model' => $this->model,
        ]);
    }
}
