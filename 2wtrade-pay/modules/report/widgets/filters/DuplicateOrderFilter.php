<?php
namespace app\modules\report\widgets\filters;

use yii\base\Widget;

/**
 * Class DuplicateOrderFilter
 * @package app\modules\report\widgets\filters
 */
class DuplicateOrderFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var \app\modules\report\components\filters\DuplicateOrderFilter */
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('duplicate-order-filter/filter', [
            'form' => $this->form,
            'model' => $this->model,
        ]);
    }
}