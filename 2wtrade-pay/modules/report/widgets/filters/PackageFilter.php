<?php
namespace app\modules\report\widgets\filters;

use yii\base\Widget;

/**
 * Фильтр по маркетинговым акциям
 * Class PackageFilter
 * @package app\modules\report\widgets\filters
 */
class PackageFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var \app\modules\report\components\filters\PackageFilter */
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('package-filter/filter', [
            'form' => $this->form,
            'model' => $this->model,
        ]);
    }
}
