<?php
namespace app\modules\report\widgets\filters;

use yii\base\Widget;

/**
 * Class DesignationFilter
 * @package app\modules\report\widgets\filters
 */
class DesignationFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var \app\modules\report\components\filters\DesignationFilter */
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('designation-filter/filter', [
            'form' => $this->form,
            'model' => $this->model,
        ]);
    }
}
