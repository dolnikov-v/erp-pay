<?php
namespace app\modules\report\widgets\filters;

use yii\base\Widget;

/**
 * Class ProductFilter
 * @package app\modules\report\widgets\filters
 */
class ProductFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var \app\modules\report\components\filters\ProductFilter */
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('product-filter/filter', [
            'form' => $this->form,
            'model' => $this->model,
        ]);
    }
}
