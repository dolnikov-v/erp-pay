<?php
namespace app\modules\report\widgets\filters;

use yii\base\Widget;

/**
 * Class DeliveryFilter
 * @package app\modules\report\widgets\filters
 */
class DeliveryFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var \app\modules\report\components\filters\DeliveryFilter */
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('delivery-filter/filter', [
            'form' => $this->form,
            'model' => $this->model,
        ]);
    }
}
