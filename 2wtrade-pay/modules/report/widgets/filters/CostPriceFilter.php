<?php
namespace app\modules\report\widgets\filters;

use yii\base\Widget;
/**
 * Class CostPriceFilter
 * @package app\modules\report\widgets\filters
 */
class CostPriceFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var \app\modules\report\components\filters\CostPriceFilter */
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('cost-price-filter/filter', [
            'form' => $this->form,
            'model' => $this->model,
        ]);
    }
}