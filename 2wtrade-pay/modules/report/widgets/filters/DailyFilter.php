<?php

namespace app\modules\report\widgets\filters;

use yii\base\Widget;
/**
 * Class DailyFilter
 * @package app\modules\report\widgets\filters
 */
class DailyFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var \app\modules\report\components\filters\DailyFilter */
    public $model;

    public $options;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('daily-filter/filter', [
            'form' => $this->form,
            'model' => $this->model,
            'options' => $this->options,
        ]);
    }
}
