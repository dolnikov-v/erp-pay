<?php

namespace app\modules\report\widgets\filters;

use yii\base\Widget;

/**
 * Class CountryDeliverySelectMultipleFilter
 * @package app\modules\report\widgets\filters
 */
class CountryDeliverySelectMultipleFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var \app\modules\report\components\filters\CountryDeliverySelectMultipleFilter */
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('country-delivery-multiple-select-filter/filter', [
            'form' => $this->form,
            'model' => $this->model,
        ]);
    }
}