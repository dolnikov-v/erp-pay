<?php
namespace app\modules\report\widgets\filters;

use yii\base\Widget;

/**
 * Class ReportAdvertisingFilter
 * @package app\modules\report\widgets\filters
 */
class ReportAdvertisingFilter extends Widget
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var \app\modules\report\components\filters\ReportAdvertisingFilter */
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('report-advertising-filter/filter', [
            'form' => $this->form,
            'model' => $this->model,
        ]);
    }
}
