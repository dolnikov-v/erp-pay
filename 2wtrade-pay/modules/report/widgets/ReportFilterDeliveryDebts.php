<?php

namespace app\modules\report\widgets;

use app\modules\report\components\ReportFormDeliveryDebts;

/**
 * Class ReportFilterDeliveryDebts
 * @package app\modules\report\widgets
 */
class ReportFilterDeliveryDebts extends ReportFilter
{
    /** @var ReportFormDeliveryDebts */
    public $reportForm;

    /**
     * @return string
     */
    public function apply()
    {
        return $this->render('report-filter-delivery-debts/filter', [
            'reportForm' => $this->reportForm
        ]);
    }
}
