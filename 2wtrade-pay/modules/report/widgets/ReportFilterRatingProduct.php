<?php
namespace app\modules\report\widgets;

/**
 * Class ReportFilterRatingProduct
 * @package app\modules\report\widgets
 */
class ReportFilterRatingProduct extends ReportFilter
{
    /**
     * @var \app\modules\report\components\ReportFormRatingProduct
     */
    public $reportForm;

    /**
     * @return string
     */
    public function apply()
    {
        return $this->render('report-filter-rating-product/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}