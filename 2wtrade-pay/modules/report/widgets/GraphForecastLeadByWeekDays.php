<?php
namespace app\modules\report\widgets;

use yii\base\Widget;

/**
 * Class ReportFilter
 * @package app\modules\report\widgets
 */
class GraphForecastLeadByWeekDays extends Widget
{
    /**
     * @var $ReportFormLeadByCountry
     */
    public $reportForm;

    /**
     * @var $dataProvider
     */
    public $dataProvider;

    /** @var $weekdays */
    private $weekdays = [];

    /** @var $leadcounts */
    private $leadcounts = [];

    /**
     * @return string
     */
    public function run()
    {
        $this->initData();

        return $this->render('graph-forecast-lead-by-week-days/index', [
            'leadcounts' => $this->leadcounts,
            'weekdays' => $this->weekdays,
        ]);
    }

    private function initData()
    {
        $this->weekdays = $this->generateWeekDays();
        $this->leadcounts = $this->generateLeadCounts();
    }

    /**
     * @return array
     */
    private function generateLeadCounts()
    {
        $leadcounts = [];
        $tmp = [];
        foreach ($this->dataProvider->allModels as $model) {
            $tmp[] = $model['leadcount'];
        }

        $counter = 0;
        foreach ($tmp as $item) {
            $leadcounts[] = Array('type' => 'column', 'name' => $this->weekdays[$counter], 'data' => Array($item));
            $counter++;
        }

        return $leadcounts;
    }

    /**
     * @return array
     */
    private function generateWeekDays()
    {
        $weekdays = [];
        foreach ($this->dataProvider->allModels as $model) {
            $weekdays[] = $model['weekday'];
        }

        return $weekdays;
    }

}
