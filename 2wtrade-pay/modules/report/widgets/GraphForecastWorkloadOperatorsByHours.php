<?php
namespace app\modules\report\widgets;

use yii\base\Widget;
use Yii;

/**
 * Class ReportFilter
 * @package app\modules\report\widgets
 */
class GraphForecastWorkloadOperatorsByHours extends Widget
{
    /**
     * @var $ReportFormForecastWorkloadOperatorsByHours
     */
    public $reportForm;

    /**
     * @var $dataProvider
     */
    public $dataProvider;

    /** @var $hours */
    private $hours = [];

    /** @var $leadcounts */
    private $leadcounts = [];

    /**
     * @return string
     */
    public function run()
    {
        $this->initData();

        return $this->render('graph-forecast-workload-operators-by-hours/index', [
            'leadcounts' => $this->leadcounts,
            'hours' => $this->hours,
        ]);
    }

    private function initData()
    {
        $this->hours = $this->generateHours();
        $this->leadcounts = $this->generateLeadCounts();
    }

    /**
     * @return array
     */
    private function generateLeadCounts()
    {
        $leadcounts = [];
        $tmp = [];
        foreach ($this->dataProvider->allModels as $model) {
            $tmp[] = intval($model['leadcount']);
        }

        $leadcounts[] = Array('type' => 'column', 'name' => Yii::t('common', 'Прогноз загрузки операторов по часам'), 'data' => $tmp);

        return $leadcounts;
    }

    /**
     * @return array
     */
    private function generateHours()
    {
        $hours = [];
        foreach ($this->dataProvider->allModels as $model) {
            $hours[] = $model['hour'];
        }

        return $hours;
    }

}
