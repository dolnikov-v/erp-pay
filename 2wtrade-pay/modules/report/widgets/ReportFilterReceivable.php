<?php
namespace app\modules\report\widgets;

use app\modules\report\components\ReportFormReceivable;

/**
 * Class ReportFilterReceivable
 * @package app\modules\report\widgets
 */
class ReportFilterReceivable extends ReportFilter
{
    /**
     * @var ReportFormReceivable
     */
    public $reportForm;

    /**
     * @return string
     */
    public function apply()
    {
        return $this->render('report-filter-receivable/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}
