<?php
namespace app\modules\report\widgets;
use app\modules\report\components\ReportFormValidateFinance;

/**
 * Class ReportFilterValidateFinance
 * @package app\modules\report\widgets
 */
class ReportFilterValidateFinance extends ReportFilter
{
    /** @var ReportFormValidateFinance */
    public $reportForm;

    /**
     * @return string
     */
    public function apply()
    {
        return $this->render('report-filter-validate-finance/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}