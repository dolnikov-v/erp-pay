<?php
namespace app\modules\report\widgets;

use app\modules\report\components\ReportFormForecastLead;

/**
 * Class ReportFilterForecastLead
 * @package app\modules\report\widgets
 */
class ReportFilterForecastLead extends ReportFilter
{
    /**
     * @var ReportFormForecastLead
     */
    public $reportForm;

    /**
     * @return string
     */
    public function apply()
    {
        return $this->render('report-filter-forecast-lead/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}