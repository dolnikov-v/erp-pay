<?php

namespace app\modules\report\widgets;

use app\modules\report\components\ReportFormFindCallCenter;

/**
 * Class ReportFilterFindCallCenter
 * @package app\modules\report\widgets
 */
class ReportFilterFindCallCenter extends ReportFilter
{
    /** @var ReportFormFindCallCenter */
    public $reportForm;

    /**
     * @return string
     */
    public function apply()
    {
        return $this->render('report-filter-find-call-center/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}
