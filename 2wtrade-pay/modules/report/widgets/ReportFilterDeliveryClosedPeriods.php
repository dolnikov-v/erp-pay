<?php
namespace app\modules\report\widgets;

use app\modules\report\components\ReportFormDeliveryClosedPeriods;

/**
 * Class ReportFilterDeliveryClosedPeriods
 * @package app\modules\report\widgets
 */
class ReportFilterDeliveryClosedPeriods extends ReportFilter
{
    /** @var ReportFormDeliveryClosedPeriods */
    public $reportForm;

    /**
     * @return string
     */
    public function apply()
    {
        return $this->render('report-filter-delivery-closed-periods/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}
