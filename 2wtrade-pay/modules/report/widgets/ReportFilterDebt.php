<?php
namespace app\modules\report\widgets;

use app\modules\report\components\ReportFormDebt;

/**
 * Class ReportFilterDebt
 * @package app\modules\report\widgets
 */
class ReportFilterDebt extends ReportFilter
{
    /**
     * @var ReportFormDebt
     */
    public $reportForm;

    /**
     * @return string
     */
    public function apply()
    {
        return $this->render('report-filter-debt/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }

}
