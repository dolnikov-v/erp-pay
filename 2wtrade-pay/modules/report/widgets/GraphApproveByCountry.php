<?php

namespace app\modules\report\widgets;

use yii\helpers\ArrayHelper;

/**
 * Class GraphApproveByCountry
 * @package app\modules\report\widgets
 */
class GraphApproveByCountry extends GraphByCountriesIntervals
{
    /** @var array */
    protected $products = [];

    /**
     * @return string
     */
    public function run()
    {
        $series = $this->generateSeries();

        return $this->render('graph-approve-by-country/index', [
            'seriesApprove' => $series['approve'],
            'seriesPercent' => $series['percent'],
            'dates' => $this->formatDates(array_values($this->dates)),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->interval = $this->reportForm->getTypeDatePartFilter()->type_date_part;
        $this->data = $this->prepareData($this->dataProvider->allModels);
        $this->dates = $this->generateDates();
        $this->countries = $this->generateCountries();
        $this->products = $this->generateProducts();
    }

    /**
     * @return array
     */
    protected function generateProducts()
    {
        $products = ArrayHelper::getColumn($this->dataProvider->allModels, 'product_name');
        $products = array_unique($products);
        asort($products);

        return $products;
    }

    /**
     * @param array $params
     * @return array
     */
    protected function sumDataParams($params)
    {
        $data = [];

        foreach ($this->countries as $country) {
            foreach ($this->products as $product) {
                foreach ($this->dates as $date) {
                    foreach ($params as $param) {
                        $data[$country . ' ' . $product][$date][$param] = 0;
                    }
                }
            }
        }

        foreach ($this->data as $model) {
            $country = $model['country_name'];

            $product = isset($model['product_name']) ? $model['product_name'] : '';

            $date = $model['date'];
            foreach ($params as $param) {
                $data[$country . ' ' . $product][$date][$param] += $model[$param];
            }
        }

        return $data;
    }

    /**
     * @return array
     */
    private function generateSeries()
    {
        $data = $this->sumDataParams(['podtverzdeno', 'vse']);

        foreach ($data as &$country) {
            foreach ($country as &$date) {
                $date['approve'] = round($date['podtverzdeno'], 2);
                $date['percent'] = ($date['vse']) ? round($date['podtverzdeno'] / $date['vse'] * 100, 2) : 0;
            }
            unset($date);
        }
        unset($country);

        $seriesList = [
            'approve' => [],
            'percent' => [],
        ];
        foreach ($seriesList as $name => &$series) {
            foreach ($this->countries as $country) {
                foreach ($this->products as $product) {
                    $series[] = [
                        'name' => $country . ' ' . $product,
                        'data' => ArrayHelper::getColumn($data[$country . ' ' . $product], $name, false),
                    ];
                }
            }
        }
        unset($series);

        return $seriesList;
    }
}
