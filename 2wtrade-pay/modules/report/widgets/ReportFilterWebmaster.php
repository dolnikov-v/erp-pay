<?php
namespace app\modules\report\widgets;

use app\modules\report\components\ReportFormWebmaster;

/**
 * Class ReportFilterWebmaster
 * @package app\modules\report\widgets
 */
class ReportFilterWebmaster extends ReportFilter
{
    /** @var ReportFormWebmaster */
    public $reportForm;

    /**
     * @return string
     */
    public function apply()
    {
        return $this->render('report-filter-webmaster/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}
