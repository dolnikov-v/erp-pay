<?php

namespace app\modules\report\widgets;

use app\widgets\Nav;

/**
 * Class LinkNav
 * @package app\modules\report\widgets
 */
class ActionNav extends Nav
{
    /**
     * @var bool | string
     */
    public $actions = false;

    /**
     * @return string
     */
    public function run()
    {
        return $this->renderTabs();
    }

    /**
     * @return string
     */
    public function renderTabs()
    {
        return $this->render('action-nav', [
            'id' => $this->id,
            'typeNav' => $this->typeNav,
            'typeTabs' => $this->typeTabs,
            'tabs' => $this->tabs,
            'actions' => $this->actions,
        ]);
    }

    /**
     * @return string
     */
    public function renderContent()
    {
        return '';
    }
}
