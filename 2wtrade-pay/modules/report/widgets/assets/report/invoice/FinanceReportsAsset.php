<?php
namespace app\modules\report\widgets\assets\report\invoice;

class FinanceReportsAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@app/web/widgets/report/invoice';

    public $js = [
        'finance-reports.js',
    ];

    public $css = [
        'finance-reports.css',
    ];

    public $depends = [
        'app\assets\SiteAsset',
    ];
}