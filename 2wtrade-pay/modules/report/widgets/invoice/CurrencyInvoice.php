<?php

namespace app\modules\report\widgets\invoice;

use app\widgets\Widget;

class CurrencyInvoice extends Widget
{
    public $reportForm;

    public $localForm;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('currency-invoice/index', [
            'reportForm' => $this->reportForm,
            'localForm' => $this->localForm
        ]);
    }
}