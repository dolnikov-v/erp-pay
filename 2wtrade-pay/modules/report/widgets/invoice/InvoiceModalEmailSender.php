<?php

namespace app\modules\report\widgets\invoice;


use app\modules\report\models\Invoice;
use app\widgets\Widget;

/**
 * Class InvoiceModalEmailSender
 * @package app\modules\report\widgets\invoice
 */
class InvoiceModalEmailSender extends Widget
{
    /**
     * @var Invoice
     */
    public $invoice;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('invoice-modal-email-sender/modal');
    }

    /**
     * @return string
     */
    public function renderSettingsBlock()
    {
        return $this->render('invoice-modal-email-sender/_settings-body', [
            'invoice' => $this->invoice,
        ]);
    }
}