<?php

namespace app\modules\report\widgets\invoice;

use app\components\widgets\ActiveForm;
use app\modules\report\widgets\ReportFilter;

/**
 * Class ReportFilterFinanceTab
 * @package app\modules\report\widgets\invoice
 */
class ReportFilterFinanceTab extends ReportFilter
{
    /**
     * @var null|ActiveForm
     */
    public $form = null;

    /**
     * @return string
     */
    public function apply()
    {
        return $this->render('report-filter-finance-tab/_content', [
            'reportForm' => $this->reportForm,
            'form' => $this->form,
        ]);
    }
}