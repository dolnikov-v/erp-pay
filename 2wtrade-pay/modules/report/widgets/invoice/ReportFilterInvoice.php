<?php

namespace app\modules\report\widgets\invoice;

use app\components\widgets\ActiveForm;
use app\modules\report\widgets\ReportFilter;

/**
 * Class ReportFilterInvoice
 * @package app\modules\report\widgets
 */
class ReportFilterInvoice extends ReportFilter
{
    /**
     * @var null|ActiveForm
     */
    public $form = null;

    /**
     * @return string
     */
    public function apply()
    {
        return $this->render('report-filter-invoice/filter', [
            'reportForm' => $this->reportForm,
            'form' => $this->form,
        ]);
    }
}