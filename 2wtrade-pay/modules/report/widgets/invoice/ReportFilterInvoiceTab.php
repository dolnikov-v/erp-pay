<?php

namespace app\modules\report\widgets\invoice;


class ReportFilterInvoiceTab extends ReportFilterInvoice
{
    /**
     * @return string
     */
    public function apply()
    {
        return $this->render('report-filter-invoice/_content', [
            'reportForm' => $this->reportForm,
            'form' => $this->form,
        ]);
    }
}