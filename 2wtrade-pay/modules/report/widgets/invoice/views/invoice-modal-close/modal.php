<?php

use app\widgets\Modal;

/**
 * @var \yii\web\View $this
 * @var \app\modules\report\models\Invoice $invoice
 * @var \app\modules\report\models\InvoiceCurrency[] $currencies
 */

?>

<?= Modal::widget([
    'id' => 'modal_invoice_modal_close',
    'title' => Yii::t('common', 'Закрытие инвойса'),
    'body' => $this->render('_modal-body', [
        'invoice' => $invoice,
        'currencies' => $currencies,
    ])
]) ?>
