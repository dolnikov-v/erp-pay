<?php
/**
 * @var \yii\web\View $this
 * @var \app\modules\report\models\Invoice $invoice
 * @var \app\modules\report\models\InvoiceCurrency[] $currencies
 */
use app\widgets\Button;
use yii\web\View;

$this->registerJs("if(typeof I18n == 'undefined'){ var I18n = {}; }", View::POS_HEAD);
$this->registerJs("I18n['Не удалось получить информацию об инвойсе.'] = '" . Yii::t('common', 'Не удалось получить информацию об инвойсе.') . "';", View::POS_HEAD);
$this->registerJs("I18n['Не удалось закрыть инвойс.'] = '" . Yii::t('common', 'Не удалось закрыть инвойс.') . "';", View::POS_HEAD);
?>

<div class="loading-block">
    <div class="row margin-bottom-20">
        <div class="col-xs-12 text-center loading-information">
            <?= Yii::t('common', 'Загрузка информации о инвойсе') ?>
        </div>
        <div class="col-xs-12 text-center closing-invoice">
            <?= Yii::t('common', 'Идет закрытие инвойса...') ?>
        </div>
    </div>
    <div class="row text-center">
        <div class="col-xs-2"></div>
        <div class="col-xs-8 text-center">
            <div class="invoice-generate-spinner"></div>
        </div>
    </div>
</div>

<div class="setting-block">
    <div class="settings-block-settings">
    </div>
    <div class="settings-block-buttons padding-top-20">
        <div class="row-with-btn-start text-right">
            <?= Button::widget([
                'id' => 'start_closing_invoice',
                'label' => Yii::t('common', 'Закрыть инвойс'),
                'style' => Button::STYLE_SUCCESS,
                'size' => Button::SIZE_SMALL,
            ]) ?>

            <?= Button::widget([
                'label' => Yii::t('common', 'Закрыть'),
                'size' => Button::SIZE_SMALL,
                'attributes' => [
                    'data-dismiss' => 'modal',
                ],
            ]) ?>
        </div>
    </div>
</div>