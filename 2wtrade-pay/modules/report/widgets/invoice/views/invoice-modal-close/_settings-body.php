<?php
/**
 * @var \yii\web\View $this
 * @var \app\modules\report\models\Invoice $invoice
 * @var \app\modules\report\models\InvoiceCurrency[] $currencies
 * @var \app\modules\deliveryreport\models\PaymentOrder $payment
 */

use app\components\widgets\ActiveForm;
use app\models\Currency;
use yii\helpers\Url;

$form = ActiveForm::begin([
    'action' => Url::toRoute(['close-invoice', 'id' => $invoice->id]),
    'options' => [
        'class' => 'form-invoice-data'
    ],
]);
?>

<?php if ($currencies): ?>
    <div class="currencies-block">
        <div class="row">
            <div class="col-lg-12">
                <h4><?= Yii::t('common', 'Укажите курсы валют (относительно доллара):') ?></h4>
            </div>
        </div>
        <?php foreach ($currencies as $currency): ?>
            <div class="row currencies-block-currency">
                <div class="col-xs-8">
                    <?= $form->field($currency, 'rate')->textInput([
                        'name' => "{$currency->formName()}[{$currency->currency_id}][rate]"
                    ])->label(Yii::t('common', $currency->currency->name)) ?>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>
<?php if ($payment): ?>
    <div class="payment-block">
        <div class="row padding-bottom-10">
            <div class="col-lg-12">
                <h4><?= Yii::t('common', 'Укажите данные о платеже:') ?></h4>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-8">
                <?= $form->field($payment, 'name')->textInput() ?>
            </div>
            <div class="col-xs-4 text-center">
                <?= $form->field($payment, 'paid_at')->datePicker([
                    'maxDate' => date('Y-m-d', time()),
                    'minDate' => $invoice->period_to ? date('Y-m-d', $invoice->period_to) : ($invoice->period_from ? date('Y-m-d', $invoice->period_from) : null)
                ]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <?= $form->field($payment, 'sum')->textInput() ?>
            </div>
            <div class="col-lg-6">
                <?= $form->field($payment, 'currency_id')->select2List(Currency::find()->collection()) ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php ActiveForm::end(); ?>