<?php

use app\components\widgets\ActiveForm;
use app\widgets\Button;
use app\widgets\DateTimePicker;
use app\widgets\ProgressBar;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/** @var yii\web\View $this */
/** @var string $url */

$this->registerJs("if(typeof I18n == 'undefined'){ var I18n = {}; }", View::POS_HEAD);
$this->registerJs("I18n['Не удалось сгенерировать инвойс.'] = '" . Yii::t('common', 'Не удалось сгенерировать инвойс.') . "';", View::POS_HEAD);
$this->registerJs("I18n['Инвойс успешно создан, ожидайте генерации файлов.'] = '" . Yii::t('common', 'Инвойс успешно создан, ожидайте генерации файлов.') . "';", View::POS_HEAD);
?>

<?php $form = ActiveForm::begin([
    'enableClientValidation' => false,
    'action' => $url,
    'method' => 'post',
    'options' => [
        'data-query-params' => json_encode(Yii::$app->request->queryParams),
    ]
]); ?>

<div class="row-with-text-start">
    <div class="inputs margin-bottom-10">
        <div class="row">
            <div class="col-xs-3"></div>
            <div class="col-xs-6">
                <label for="payment_due_date"><?= Yii::t('common', 'Предполагаемая дата платежа') ?></label>
                <?= DateTimePicker::widget([
                    'minDate' => date('Y-m-d'),
                    'maxDate' => date('Y-m-d', strtotime("+6month")),
                    'name' => 'payment_due_date',
                    'attributes' => [
                        'style' => 'text-align: center;'
                    ]
                ]) ?></div>
        </div>
    </div>
    <div class="text-center">
        <?= Yii::t('common', 'Вы действительно хотите сгенерировать инвойс согласно указанным параметрам?') ?>
    </div>
</div>
<div class="row-with-text-progress hidden">
    <div class="row margin-bottom-20">
        <div class="col-xs-12 text-center">
            <?= Yii::t('common', 'Идет генерация инвойса...') ?>
        </div>
    </div>
    <div class="row text-center">
        <div class="col-xs-2"></div>
        <div class="col-xs-8 text-center">
            <div class="invoice-generate-spinner"></div>
        </div>
    </div>
</div>
<div class="padding-top-20">
    <div class="row-with-btn-start text-right">
        <?= Button::widget([
            'id' => 'start_create_invoice',
            'label' => Yii::t('common', 'Сгенерировать'),
            'style' => Button::STYLE_SUCCESS,
            'size' => Button::SIZE_SMALL,
        ]) ?>

        <?= Button::widget([
            'label' => Yii::t('common', 'Закрыть'),
            'size' => Button::SIZE_SMALL,
            'attributes' => [
                'data-dismiss' => 'modal',
            ],
        ]) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
