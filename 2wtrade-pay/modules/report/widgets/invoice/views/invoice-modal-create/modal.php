<?php
use app\widgets\Modal;

/** @var string $url */
/** @var string $updateUrl */
/** @var \app\modules\deliveryreport\models\DeliveryReport $reportModel */
?>

<?= Modal::widget([
    'id' => 'modal_create_invoice',
    'title' => Yii::t('common', 'Генерация инвойса'),
    'body' => $this->render('_modal-body', [
        'url' => $url
    ]),
]) ?>
