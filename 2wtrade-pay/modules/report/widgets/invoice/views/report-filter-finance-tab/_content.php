<?php

use app\components\widgets\ActiveForm;
use app\modules\report\widgets\invoice\CurrencyInvoice;
use yii\helpers\Url;
use app\modules\report\widgets\assets\report\invoice\FinanceReportsAsset;
use yii\web\View;

/**
 * @var \yii\web\View $this
 * @var \app\modules\report\components\invoice\ReportFormInvoice $reportForm
 * @var \app\components\widgets\ActiveForm $form
 */

FinanceReportsAsset::register($this);

$this->registerJs("var I18n = {};", View::POS_HEAD);
$this->registerJs("I18n['Финансовые отчёты не найдены'] = '" . Yii::t('common', 'Финансовые отчёты не найдены') . "';", View::POS_HEAD);

$queryParams = Yii::$app->request->queryParams;
$filterFinanceReport = isset($queryParams['s']['financeReportId']) ? $queryParams['s']['financeReportId'] : null;

if (isset($queryParams[$reportForm->formName()])) {
    unset($queryParams[$reportForm->formName()]);
}

$localForm = $form ?: ActiveForm::begin([
    'action' => array_merge($queryParams, ['index'], ['#' => 'tab1']),
    'method' => 'get',
    'id' => 'by-finance-report'
]);
?>

    <div class="row">
        <div class="col-lg-6">
            <?= $reportForm->getCountryDeliverySelectMultipleFilter()->restore($localForm) ?>
        </div>

        <div class="col-lg-5">
            <?= $localForm->field($reportForm, 'financeReportId')->select2List([], ['disabled' => 'disabled']) ?>
        </div>
        <div class="col-lg-1 padding-25">
            <div class="load-finance-reports"></div>
        </div>

        <?= $localForm->field($reportForm, 'type_filter')->hiddenInput(['value' => $reportForm::FILTER_TYPE_FINANCE])->label(false); ?>
        <?= $localForm->field($reportForm, 'activeFinanceReport')->hiddenInput(['value' => $filterFinanceReport])->label(false); ?>
    </div>
<hr/>
<?= CurrencyInvoice::widget([
    'localForm' => $localForm,
    'reportForm' => $reportForm
]); ?>

<?php if (!$form): ?>
    <div class="row">
        <div class="col-md-12">
            <?= $localForm->submit(Yii::t('common', 'Применить')) ?>
            <?= $localForm->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')) ?>
        </div>
        <div class="pull-right">
            <?= $localForm->field($reportForm, 'includeDoneOrders')
                ->checkboxCustom(['checked' => $reportForm->includeDoneOrders]) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
<?php else: ?>
    <div class="row">
        <div class="col-md-12">
            <div class="pull-right">
                <?= $localForm->field($reportForm, 'includeDoneOrders')
                    ->checkboxCustom(['checked' => $reportForm->includeDoneOrders]) ?>
            </div>
        </div>
    </div>
<?php endif; ?>