<?php

use app\widgets\Modal;

/**
 * @var \yii\web\View $this
 */

?>

<?= Modal::widget([
    'id' => 'modal_invoice_modal_email_sender',
    'title' => Yii::t('common', 'Отправка инвойса'),
    'body' => $this->render('_modal-body')
]) ?>
