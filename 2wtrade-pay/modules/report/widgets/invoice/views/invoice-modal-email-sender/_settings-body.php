<?php
/**
 * @var \yii\web\View $this
 * @var \app\modules\report\models\Invoice $invoice
 */

use app\components\widgets\ActiveForm;
use app\widgets\custom\Checkbox;
use yii\helpers\Url;

$form = ActiveForm::begin([
    'action' => Url::toRoute(['send-invoice', 'id' => $invoice->id]),
    'method' => 'POST',
    'options' => [
        'class' => 'form-invoice-data'
    ],
]);
?>

<div class="invoice-files-block margin-bottom-20">
    <?php if ($invoice->orders_filename || $invoice->invoice_filename): ?>
        <div class="row">
            <div class="col-lg-12">
                <h4><?= Yii::t('common', 'Отметьте файлы, которые необходимо отправить:') ?></h4>
            </div>
        </div>
        <div class="invoice-files-list">
            <div class="row">
                <?php if ($invoice->invoice_filename): ?>
                    <div class="col-lg-6 text-center">
                        <?= Checkbox::widget([
                            'label' => Yii::t('common', 'Инвойс'),
                            'name' => 'invoice_file',
                            'style' => 'margin-bottom-0'
                        ]) ?>
                    </div>
                <?php endif; ?>
                <?php if ($invoice->orders_filename): ?>
                    <div class="col-lg-6">
                        <?= Checkbox::widget([
                            'label' => Yii::t('common', 'Таблица с заказами'),
                            'name' => 'orders_file',
                            'style' => 'margin-bottom-0'
                        ]) ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    <?php else: ?>
        <div class="row">
            <div class="col-lg-12 text-center">
                <h4><?= Yii::t('common', 'Файлы инвойса не были сгенерированы.') ?></h4>
            </div>
        </div>
    <?php endif; ?>
</div>

<div class="email-list-block">
    <?php if ($invoice->delivery->invoiceEmailList): ?>
        <div class="row">
            <div class="col-lg-12">
                <h4><?= Yii::t('common', 'Отметьте e-mail адреса:') ?></h4>
            </div>
        </div>

        <div class="email-list margin-left-30">
            <?php foreach ($invoice->delivery->invoiceEmailList as $email): ?>
                <div class="row">
                    <div class="col-lg-12">
                        <?= Checkbox::widget([
                            'label' => $email,
                            'name' => 'emails[]',
                            'value' => $email,
                            'style' => 'margin-bottom-0'
                        ]) ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    <?php else: ?>
        <div class="row">
            <div class="col-lg-12 text-center">
                <h4><?= Yii::t('common', 'E-mail адреса не указаны в настройках КС.') ?></h4>
            </div>
        </div>
    <?php endif; ?>
</div>

<?php ActiveForm::end(); ?>
