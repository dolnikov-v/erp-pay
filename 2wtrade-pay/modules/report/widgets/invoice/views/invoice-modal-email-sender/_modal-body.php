<?php

use app\widgets\Button;
use app\widgets\ButtonProgress;
use yii\web\View;

/**
 * @var \yii\web\View $this
 */

$this->registerJs("if(typeof I18n == 'undefined'){ var I18n = {}; }", View::POS_HEAD);
$this->registerJs("I18n['Не удалось получить информацию об инвойсе.'] = '" . Yii::t('common', 'Не удалось получить информацию об инвойсе.') . "';", View::POS_HEAD);
$this->registerJs("I18n['Не удалось отправить инвойс.'] = '" . Yii::t('common', 'Не удалось отправить инвойс.') . "';", View::POS_HEAD);
?>

<div class="loading-block">
    <div class="row margin-bottom-20">
        <div class="col-xs-12 text-center loading-information">
            <?= Yii::t('common', 'Загрузка информации о инвойсе') ?>
        </div>
    </div>
    <div class="row text-center">
        <div class="col-xs-2"></div>
        <div class="col-xs-8 text-center">
            <div class="invoice-generate-spinner"></div>
        </div>
    </div>
</div>

<div class="setting-block">
    <div class="settings-block-settings">
    </div>
    <div class="settings-block-buttons padding-top-20">
        <div class="row-with-btn-start text-right">
            <?= ButtonProgress::widget([
                'id' => 'start_sending_invoice',
                'label' => Yii::t('common', 'Отправить'),
                'style' => Button::STYLE_SUCCESS,
                'size' => Button::SIZE_SMALL,
            ]) ?>

            <?= Button::widget([
                'label' => Yii::t('common', 'Закрыть'),
                'size' => Button::SIZE_SMALL,
                'attributes' => [
                    'data-dismiss' => 'modal',
                ],
            ]) ?>
        </div>
    </div>
</div>
