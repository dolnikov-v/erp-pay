<?php

use app\widgets\Panel;

/**
 * @var \yii\web\View $this
 * @var \app\modules\report\components\invoice\ReportFormInvoiceList $reportForm
 * @var \app\components\widgets\ActiveForm $form
 */

?>

<?= Panel::widget([
    'id' => 'report_filter_invoice_list',
    'title' => Yii::t('common', 'Фильтр по инвойсам'),
    'content' => $this->render('_content', [
        'reportForm' => $reportForm,
        'form' => $form,
    ]),
    'collapse' => true,
]) ?>
