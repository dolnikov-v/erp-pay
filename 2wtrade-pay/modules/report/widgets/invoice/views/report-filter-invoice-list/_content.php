<?php

use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 * @var \app\modules\report\components\invoice\ReportFormInvoiceList $reportForm
 * @var \app\components\widgets\ActiveForm $form
 */

$queryParams = Yii::$app->request->queryParams;
if (isset($queryParams[$reportForm->formName()])) {
    unset($queryParams[$reportForm->formName()]);
}

$localForm = $form ?: ActiveForm::begin([
    'action' => array_merge($queryParams, ['index']),
    'method' => 'get',
    'enableClientValidation' => false,
]);

?>

<div class="row">
    <div class="col-lg-6">
        <?= $reportForm->getDateFilter()->restore($localForm) ?>
    </div>
    <div class="col-lg-4">
        <?= $reportForm->getInvoiceStatusFilter()->restore($localForm) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= $reportForm->getCountryDeliverySelectMultipleFilter()->restore($localForm) ?>
    </div>
</div>

<?php if (!$form): ?>
    <div class="row">
        <div class="col-md-12">
            <?= $localForm->submit(Yii::t('common', 'Применить')) ?>
            <?= $localForm->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
<?php endif; ?>
