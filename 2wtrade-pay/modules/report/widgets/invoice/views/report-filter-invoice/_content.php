<?php

use app\components\widgets\ActiveForm;
use app\modules\report\widgets\invoice\CurrencyInvoice;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 * @var \app\modules\report\components\invoice\ReportFormInvoice $reportForm
 * @var \app\components\widgets\ActiveForm $form
 */

$queryParams = Yii::$app->request->queryParams;
if (isset($queryParams[$reportForm->formName()])) {
    unset($queryParams[$reportForm->formName()]);
}

$localForm = $form ?: ActiveForm::begin([
    'action' => array_merge($queryParams, ['index'], ['#' => 'tab0']),
    'method' => 'get',
]);


?>

<div class="row">
    <div class="col-lg-6">
        <?= $reportForm->getDateFilter()->restore($localForm) ?>
    </div>
    <div class="col-lg-6">
        <?= $reportForm->getCountryDeliverySelectMultipleFilter()->restore($localForm) ?>
    </div>

    <?= $localForm->field($reportForm, 'type_filter')->hiddenInput(['value' => $reportForm::FILTER_TYPE_PERIOD])->label(false); ?>
</div>
<hr/>
<?= CurrencyInvoice::widget([
    'localForm' => $localForm,
    'reportForm' => $reportForm
]); ?>

<?php if (!$form): ?>
    <div class="row">
        <div class="col-md-12">
            <?= $localForm->submit(Yii::t('common', 'Применить')) ?>
            <?= $localForm->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')) ?>
            <div class="pull-right">
                <?= $localForm->field($reportForm, 'includeDoneOrders')
                    ->checkboxCustom(['checked' => $reportForm->includeDoneOrders]) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
<?php else: ?>
    <div class="row">
        <div class="col-md-12">
            <div class="pull-right">
                <?= $localForm->field($reportForm, 'includeDoneOrders')
                    ->checkboxCustom(['checked' => $reportForm->includeDoneOrders]) ?>
            </div>
        </div>
    </div>
<?php endif; ?>
