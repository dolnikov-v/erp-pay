<?php

use app\widgets\Panel;

/**
 * @var \yii\web\View $this
 * @var \app\modules\report\components\invoice\ReportFormInvoice $reportForm
 * @var \app\components\widgets\ActiveForm $form
 */

?>

<?= Panel::widget([
    'id' => 'report_filter_invoice',
    'title' => Yii::t('common', 'Фильтр'),
    'content' => $this->render('_content', [
        'reportForm' => $reportForm,
        'form' => $form,
    ]),
    'collapse' => true,
]) ?>
