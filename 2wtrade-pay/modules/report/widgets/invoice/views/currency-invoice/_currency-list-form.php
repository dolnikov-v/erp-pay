<?php

use app\models\Currency;
use app\modules\report\components\invoice\ReportFormInvoice;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var integer $currency_id
 * @var string $date
 * @var double $rate
 * @var \app\components\widgets\ActiveForm $form
 * @var \app\modules\report\components\invoice\ReportFormInvoice $reportForm
 * @var array $currencies
 */

?>

<div class="preview-invoice-currencies-row" data-url="<?= Url::toRoute('currency-rate') ?>">
    <div class="col-lg-3 preview-invoice-currencies-currency-id">
        <?= $form->field($reportForm, 'currencies')->select2List($currencies ?? Currency::find()->where([
                '!=',
                'id',
                $reportForm->currencyId
            ])->collection(), [
            'name' => Html::getInputName($reportForm, 'currencies') . '[]',
            'value' => $currency_id ?? false,
            'prompt' => '—',
            'length' => false,
        ])->label(false) ?>
    </div>
    <div class="col-lg-3 preview-invoice-currencies-date">
        <?= $form->field($reportForm, 'dates')->datePicker([
            'value' => $date ?? null,
            'name' => Html::getInputName($reportForm, 'dates') . '[]',
            'attributes' => [
                'style' => 'text-align: center;',
                'id' => Html::getInputId($reportForm, 'dates'),
                'value' => $date ?? null,
                'data-init' => isset($date) && !empty($date) ? 0 : 1,
            ],
            'maxDate' => date('Y-m-d'),
        ])->label(false) ?>
    </div>
    <div class="col-lg-3 preview-invoice-currencies-rate">
        <?= $form->field($reportForm, 'rates')->textInput([
            'name' => Html::getInputName($reportForm, 'rates') . '[]',
            'value' => !empty($rate) ? round($rate, ReportFormInvoice::CURRENCY_RATE_ROUND_PRECISION) : null,
        ])->label(false) ?>
    </div>
</div>
