<?php
use app\models\Currency;
use app\widgets\ChangeableList;

/** @var \app\modules\report\components\invoice\ReportFormInvoice $reportForm*/
/** @var \app\components\widgets\ActiveForm $localForm */

$changeableListItems = [];
$currencies = Currency::find()->collection();

foreach ($reportForm->rates as $id => $rate) {
    if (!empty($rate) || !empty($reportForm->dates[$id]) || !empty($reportForm->currencies[$id])) {
        $changeableListItems[] = $this->render('_currency-list-form', [
            'form' => $localForm,
            'reportForm' => $reportForm,
            'currency_id' => $reportForm->currencies[$id],
            'date' => $reportForm->dates[$id],
            'rate' => $rate,
            'currencies' => $currencies,
        ]);
    }
}
?>

<div class="row preview-invoice-currency-settings">
    <div class="col-lg-8">
        <?=ChangeableList::widget([
            'label' => '<h4>' . Yii::t('common', 'Курсы валют (относительно выбранной валюты инвойса)') . '</h4>',
            'template' => $this->render('_currency-list-form', [
                'form' => $localForm,
                'reportForm' => $reportForm,
                'currencies' => $currencies,
            ]),
            'items' => $changeableListItems,
            'cssClass' => 'preview-invoice-currencies',
            'callbackJsFuncAfterAddRow' => 'InvoiceHandler.updateCurrencyListTriggers',
        ]) ?>
    </div>
    <div class="col-lg-2"></div>
    <div class="col-lg-2">
        <div class="row">
            <div class="col-lg-12 preview-invoice-currency-settings-invoice-currency-id">
                <?= $localForm->field($reportForm, 'currencyId')->select2List($currencies, ['length' => false]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <?= $localForm->field($reportForm, 'roundPrecision')->textInput(['size' => 'text-right']) ?>
            </div>
        </div>
    </div>
</div>