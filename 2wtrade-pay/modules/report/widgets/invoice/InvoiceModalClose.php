<?php

namespace app\modules\report\widgets\invoice;


use app\modules\deliveryreport\models\PaymentOrder;
use app\modules\report\models\Invoice;
use app\widgets\Widget;

/**
 * Class InvoiceModalClose
 * @package app\modules\report\widgets\invoice
 */
class InvoiceModalClose extends Widget
{
    /**
     * @var null | Invoice
     */
    public $invoice = null;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('invoice-modal-close/modal', [
            'invoice' => $this->invoice,
            'currencies' => $this->getCurrencies(),
        ]);
    }

    /**
     * @return array
     */
    protected function getCurrencies()
    {
        $currencies = [];
        if ($this->invoice && $this->invoice->currencyRates) {
            $currencies = $this->invoice->currencyRates;
        }

        return $currencies;
    }

    /**
     * @return string
     */
    public function renderSettingsBlock()
    {
        $payment = new PaymentOrder();
        return $this->render('invoice-modal-close/_settings-body', [
            'invoice' => $this->invoice,
            'currencies' => $this->getCurrencies(),
            'payment' => $payment,
        ]);
    }
}