<?php

namespace app\modules\report\widgets\invoice;


use app\widgets\Widget;

/**
 * Class InvoiceModalCreate
 * @package app\modules\report\widgets\invoice
 */
class InvoiceModalCreate extends Widget
{
    /**
     * @var string
     */
    public $url;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('invoice-modal-create/modal', ['url' => $this->url]);
    }
}