<?php
namespace app\modules\report\widgets;

use app\modules\report\components\ReportFormConsolidateByCountry;

/**
 * Class ReportFilterConsolidateByCountry
 * @package app\modules\report\widgets
 */
class ReportFilterConsolidateByCountry extends ReportFilter
{
    /** @var ReportFormConsolidateByCountry */
    public $reportForm;

    public function apply()
    {
        return $this->render('report-filter-consolidate-by-country/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}