<?php
namespace app\modules\report\widgets;
use app\modules\report\components\ReportFormAdvertising;

/**
 * Class ReportFilterStatus
 * @package app\modules\report\widgets
 */
class ReportFilterAdvertising extends ReportFilter
{
    /** @var ReportFormAdvertising */
    public $reportForm;

    /**
     * @return string
     */
    public function apply()
    {
        return $this->render('report-filter-advertising/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}
