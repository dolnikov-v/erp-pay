<?php
namespace app\modules\report\widgets;

use app\modules\report\components\ReportFormDeliveryPayment;

/**
 * Class ReportFilterDeliveryPayment
 * @package app\modules\report\widgets
 */
class ReportFilterDeliveryPayment extends ReportFilter
{
    /** @var ReportFormDeliveryPayment */
    public $reportForm;

    /**
     * @return string
     */
    public function apply()
    {
        return $this->render('report-filter-delivery-payment/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}
