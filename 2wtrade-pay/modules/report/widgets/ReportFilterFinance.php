<?php
namespace app\modules\report\widgets;

use app\modules\report\components\ReportFormFinance;

/**
 * Class ReportFilterFinance
 * @package app\modules\report\widgets
 */
class ReportFilterFinance extends ReportFilter
{
    /** @var ReportFormFinance */
    public $reportForm;

    /**
     * @return string
     */
    public function apply()
    {
        return $this->render('report-filter-finance/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}
