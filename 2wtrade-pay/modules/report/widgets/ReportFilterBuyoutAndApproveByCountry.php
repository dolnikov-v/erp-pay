<?php
namespace app\modules\report\widgets;

use app\modules\report\components\ReportFormBuyoutAndApproveByCountry;

/**
 * Class ReportFilterBuyoutAndApproveByCountry
 * @package app\modules\report\widgets
 */
class ReportFilterBuyoutAndApproveByCountry extends ReportFilter
{
    /** @var ReportFormBuyoutAndApproveByCountry */
    public $reportForm;

    /**
     * @return string
     */
    public function apply()
    {
        return $this->render('report-filter-buyout-and-approve-by-country/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}
