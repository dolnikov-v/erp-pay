<?php
namespace app\modules\report\widgets;

use app\modules\report\components\ReportFormOperatorWorkdays;

/**
 * Class ReportFilterDeliveryZipcodesStats
 * @package app\modules\report\widgets
 */
class ReportFilterOperatorWorkdays extends ReportFilter
{
    /** @var ReportFormOperatorWorkdays */
    public $reportForm;

    /**
     * @return string
     */
    public function apply()
    {
        return $this->render('report-filter-operator-workdays/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}
