<?php
namespace app\modules\report\widgets;

use app\modules\report\components\ReportFormForeignOperator;

/**
 * Class ReportFilterForeignOperator
 * @package app\modules\report\widgets
 */
class ReportFilterForeignOperator extends ReportFilter
{
    /** @var ReportFormForeignOperator */
    public $reportForm;

    /**
     * @return string
     */
    public function apply()
    {
        return $this->render('report-filter-foreign-operator/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}
