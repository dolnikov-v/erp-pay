<?php

namespace app\modules\report\widgets;

/**
 * Class ReportFilterCallCenterDelayByCountry
 * @package app\modules\report\widgets
 */
class ReportFilterCallCenterDelayByCountry extends ReportFilter
{
    public $reportForm;

    public function apply()
    {
        return $this->render('report-filter-delivery-delay-by-country/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}
