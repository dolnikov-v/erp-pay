<?php
namespace app\modules\report\widgets;

use app\modules\report\components\ReportFormOperatorsPenalty;

/**
 * Class ReportFilterOperatorsPenalty
 * @package app\modules\report\widgets
 */
class ReportFilterOperatorsPenalty extends ReportFilter
{
    /** @var ReportFormOperatorsPenalty */
    public $reportForm;

    /**
     * @return string
     */
    public function apply()
    {
        return $this->render('report-filter-operators-penalty/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}
