<?php

namespace app\modules\report\widgets;

/**
 * Class ReportGroupByWorkflow
 * @package app\modules\report\widgets
 */
class ReportGroupByWorkflow extends ReportGroupBy
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('report-group-by-workflow', [
            'form' => $this->form,
            'reportForm' => $this->reportForm,
        ]);
    }
}