<?php
namespace app\modules\report\widgets;

use app\modules\report\components\ReportFormFinanceCountries;

/**
 * Class ReportFilterFinanceCountries
 * @package app\modules\report\widgets
 */
class ReportFilterFinanceCountries extends ReportFilter
{
    /** @var ReportFormFinanceCountries */
    public $reportForm;

    /**
     * @return string
     */
    public function apply()
    {
        return $this->render('report-filter-finance-countries/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}
