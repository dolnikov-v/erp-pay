<?php

namespace app\modules\report\widgets;

/**
 * Class ReportFilterApproveByCountry
 * @package app\modules\report\widgets
 */
class ReportFilterConsolidateInfo extends ReportFilter
{
    /** @var ReportFormAppproveByCountry */
    public $reportForm;

    public function apply()
    {
        return $this->render('report-filter-consolidate-info/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}