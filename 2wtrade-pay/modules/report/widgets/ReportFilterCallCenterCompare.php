<?php

namespace app\modules\report\widgets;

use app\modules\report\components\ReportFormCallCenterCompare;

/**
 * Class ReportFilterCallCenterCompare
 * @package app\modules\report\widgets
 */
class ReportFilterCallCenterCompare extends ReportFilter
{
    /** @var ReportFormCallCenterCompare */
    public $reportForm;

    public function apply()
    {
        return $this->render('report-filter-call-center-compare/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}
