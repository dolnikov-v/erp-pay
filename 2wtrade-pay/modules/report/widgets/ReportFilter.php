<?php
namespace app\modules\report\widgets;

use app\modules\report\components\ReportForm;
use yii\base\InvalidParamException;
use yii\base\Widget;
use Yii;

/**
 * Class ReportFilter
 * @package app\modules\report\widgets
 */
abstract class ReportFilter extends Widget
{
    /** @var ReportForm */
    public $reportForm;

    /**
     * @return string
     */
    public abstract function apply();

    /**
     * @return string
     */
    public function run()
    {
        if (!$this->reportForm instanceof ReportForm) {
            throw new InvalidParamException(Yii::t('common', 'Необходимо указать корректную форму.'));
        }

        return $this->apply();
    }
}
