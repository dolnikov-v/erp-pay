<?php
namespace app\modules\report\widgets;

use app\modules\report\components\ReportFormShort;

/**
 * Class ReportFilterShort
 * @package app\modules\report\widgets
 */
class ReportFilterShort extends ReportFilter
{
    /** @var ReportFormShort */
    public $reportForm;

    /**
     * @return string
     */
    public function apply()
    {
        return $this->render('report-filter-short/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}
