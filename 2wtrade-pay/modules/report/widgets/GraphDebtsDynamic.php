<?php

namespace app\modules\report\widgets;

use app\modules\delivery\models\Delivery;
use app\modules\report\components\filters\TypeDatePartFilter;
use app\modules\report\components\ReportFormDeliveryDebts;
use yii\helpers\ArrayHelper;
use yii\db\Expression;
use yii\base\Widget;
use app\modules\report\extensions\DataProvider;
use Yii;

/**
 * Class GraphDebtsDynamic
 * @package app\modules\report\widgets
 */
class GraphDebtsDynamic extends Widget
{
    /**
     * @var ReportFormDeliveryDebts
     */
    public $reportForm;

    /**
     * @var DataProvider
     */
    public $dataProvider;

    /**
     * @var array
     */
    protected $data = [];

    /** @var array */
    protected $dates = [];

    /** @var array */
    protected $countries = [];

    /** @var array */
    protected $deliveries = [];

    /**
     * @var string
     */
    public $interval;

    /**
     * @param array $data
     * @return array
     */
    protected function prepareData($data)
    {
        foreach ($data as &$model) {

            switch ($this->interval) {
                case TypeDatePartFilter::TYPE_DATE_PART_DAY:
                    $model['date'] = $model['year'] . '-' . $model['month'] . '-' . $model['day'];
                    break;

                case TypeDatePartFilter::TYPE_DATE_PART_MONTH:
                    $model['date'] = $model['year'] . '-' . $model['month'];
                    break;

                case TypeDatePartFilter::TYPE_DATE_PART_WEEK:
                    $model['date'] = $model['year'] . '-' . $model['week'];
                    break;
            }
        }
        unset($model);

        return $data;
    }

    /**
     * @return string
     */
    public function run()
    {
        $series = $this->generateSeries();

        return $this->render('graph-debts-dynamic/index', [
            'seriesAmount' => $series['amount'],
            'dates' => $this->formatDates(array_values($this->dates)),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->data = $this->prepareData($this->dataProvider->allModels);
        $this->dates = $this->generateDates();
        $this->countries = $this->generateCountries();
        $this->deliveries = $this->generateDeliveries();
    }

    /**
     * @return array
     */
    protected function generateDates()
    {
        if ($dates = ArrayHelper::getColumn($this->data, 'date')) {
            $dates = array_unique($dates);
            $dates = array_values($dates);
            asort($dates);
        }

        return $dates;
    }

    /**
     * @param string $date
     * @return string
     */
    protected function formatDate($date)
    {
        switch ($this->interval) {

            case TypeDatePartFilter::TYPE_DATE_PART_DAY:
                $date = Yii::$app->formatter->asDate(strtotime($date));
                break;

            case TypeDatePartFilter::TYPE_DATE_PART_MONTH:
                $date = Yii::$app->formatter->asMonth(strtotime($date . '-01'));
                break;

            case TypeDatePartFilter::TYPE_DATE_PART_WEEK:
                list($year, $week) = explode('-', $date);
                if ($week == 0) {
                    $time = strtotime($year . '-01-01');
                } else {
                    $timestamp = mktime(0, 0, 0, 1, 1, $year) + ($week * 7 * 86400);
                    $time = $timestamp - 86400 * (date('N', $timestamp) - 1);
                }
                $date = Yii::$app->formatter->asDate($time);
                break;
        }

        return $date;
    }

    /**
     * @param array
     * @return array
     */
    protected function formatDates($dates)
    {
        foreach ($dates as &$date) {
            $date = $this->formatDate($date);
        }
        unset($date);

        return $dates;
    }

    /**
     * @return array
     */
    protected function generateDeliveries()
    {
        $deliveries = ArrayHelper::getColumn($this->dataProvider->allModels, 'delivery_name');
        $delivery_id = ArrayHelper::getColumn($this->dataProvider->allModels, 'delivery_id');
        $deliveries = array_combine($delivery_id, $deliveries);

        return $deliveries;
    }

    /**
     * @return array
     */
    protected function generateCountries()
    {
        $countries = ArrayHelper::getColumn($this->dataProvider->allModels, 'country_name');
        $country_ids = ArrayHelper::getColumn($this->dataProvider->allModels, 'country_id');
        $countries = array_combine($country_ids, $countries);

        return $countries;
    }

    /**
     * Собирает пары  country id - delivery id
     * @return Delivery[]
     */
    public function getCombination()
    {
        $query = Delivery::find()
            ->select([
                'couple' => new Expression('CONCAT(' . Delivery::tableName() . '.country_id, "-", ' . Delivery::tableName() . '.id)')
            ])
            ->asArray()
            ->all();

        return ArrayHelper::getColumn($query, 'couple');
    }

    /**
     * @param array $params
     * @return array
     */
    protected function sumDataParams($params)
    {
        $data = [];
        $combination = $this->getCombination();
        $deliveries = $this->generateDeliveries();
        //курьерка не указана в фильтре
        sort($deliveries);

        foreach ($this->countries as $k => $country) {
            foreach ($this->deliveries as $j => $delivery) {
                $pair = $k . '-' . $j;
                if (in_array($pair, $combination) || is_null($deliveries[0])) {
                    $delivery = !empty($delivery) ? ' (' . $delivery . ') ' : ' ';

                    foreach ($this->dates as $date) {
                        foreach ($params as $param) {
                            $data[$country . $delivery][$date][$param] = 0;
                        }
                    }
                }
            }
        }

        foreach ($this->data as $model) {
            $country = $model['country_name'];
            $delivery = isset($model['delivery_name']) ? ' (' . $model['delivery_name'] . ') ' : ' ';

            $date = $model['date'];

            foreach ($params as $param) {
                if (isset($data[$country . $delivery])) {
                    $data[$country . $delivery][$date][$param] += $model[$param];
                }
            }
        }
        return $data;
    }

    /**
     * @return array
     */
    private function generateSeries()
    {
        $data = $this->sumDataParams(['amount']);

        $seriesList = [
            'amount' => [],
        ];

        foreach ($data as $name => $v) {
            $values = [];
            foreach ($v as $date => $value) {
                $values['amount'][] = $value['amount'] ? round($value['amount'], 2) : null;
            }

            $seriesList['amount'][] = [
                'name' => $name,
                'data' => $values['amount']
            ];

        }
        return $seriesList;
    }
}
