<?php
namespace app\modules\report\widgets;

use app\modules\report\components\ReportFormShipment;

/**
 * Class ReportFilterShipment
 * @package app\modules\report\widgets
 */
class ReportFilterShipment extends ReportFilter
{
    /**
     * @var ReportFormSale
     */
    public $reportForm;

    /**
     * @return string
     */
    public function apply()
    {
        return $this->render('report-filter-shipment/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}