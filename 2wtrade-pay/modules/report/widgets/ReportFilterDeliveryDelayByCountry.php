<?php

namespace app\modules\report\widgets;

/**
 * Class ReportFilterDeliveryDelayByCountry
 * @package app\modules\report\widgets
 */
class ReportFilterDeliveryDelayByCountry extends ReportFilter
{
    public $reportForm;

    public function apply()
    {
        return $this->render('report-filter-delivery-delay-by-country/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}
