<?php
namespace app\modules\report\widgets;

use app\modules\callcenter\models\CallCenterRequest;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use Yii;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * Class ResendSeparateOrder
 *
 * @package app\modules\order\widgets
 */
class ConsolidateInfo
{

    private $_userId;

    private $_countryIds;

    private $_dateFrom;

    public function __construct($countries, $dateFrom, $userId = null)
    {
        $this->_userId = $userId;
        $this->_countries = $countries;
        $this->_dateFrom = $dateFrom;
    }

    /**
     * TODO: Черновое решение задачи
     * TODO: учесть еще что могут быть не отправленые, в различных статусах, но в адкомбо статус апрув, и за рекламу им заплатили,
     * TODO: но по разным причинам заказы перешли в различные статусы
     * TODO: нужно считать как не отправленные заказы у которых есть adcombo_confirm но статусы не косающииеся действий курьерки
     *
     * @param $model
     *
     * @return array
     */
    public function prepareStats()
    {
        //читаем кеш если он есть
        if ($this->_userId != null) {
            $cache = Yii::$app->cache->get('profile_statistic_user_' . $this->_userId);
            if (!empty($cache)) {
                //return $cache;
            }
        }

        $roles = ArrayHelper::map(Yii::$app->user->identity->roles, 'item_name', 'item_name');

        $object = self::container();
        //получаем список курсов для стран
        $countryRate = ArrayHelper::map($this->_countries, 'id', 'currencyRate.rate');
        //получаем список кодов стран для стран
        $countryCharCodes = ArrayHelper::map($this->_countries, 'id', 'char_code');
        //список стран
        $countryIds = array_keys($countryRate);

        //шаблон общего запроса, выбираем все заказы в процессе, выкупы и не выкупы. (id статусов взяты из статы финансов из соответствующих колонок)
        $query = (new Query())
            ->from(Order::tableName())
            ->where(['country_id' => $countryIds])
            ->andWhere(['>=', Order::tableName() . '.created_at', $this->_dateFrom]);
        //запрос основной статистики
        $stats = clone $query;
        $stats = $stats->select([
                'country_id',
                'FROM_UNIXTIME(' . Order::tableName() . '.created_at, \'%M %Y\') as date',
                'COUNT(' . Order::tableName() . '.id) as cnt',
                '(SUM(' . Order::tableName() . '.price_total) + SUM(' . Order::tableName() . '.delivery)) as sumPrice',
                'status_id',
            ]
        )
            ->andWhere(['status_id' => self::getStatusMap()])
            ->groupBy(['country_id', 'date', 'status_id'])
            ->orderBy([Order::tableName() . '.created_at' => SORT_DESC])
            ->all();
        //подзапрос для подсчета среднего количества просрочки отправки заказов
        $tmpQuery = clone $query;
        $tmpQuery->select([
                Order::tableName() . '.*',
                '(UNIX_TIMESTAMP(NOW()) - ' . CallCenterRequest::tableName() . '.approved_at) as delaySent',
            ]
        )
            ->join('INNER JOIN', CallCenterRequest::tableName(),
                Order::tableName() . '.id=' . CallCenterRequest::tableName() . '.order_id')
            ->andWhere(['status_id' => [6, 31, 19]]);
        //запрос для подсчета среднего количества просрочки отправки заказов
        $averageDelaySent = (new Query())
            ->from(['(' . $tmpQuery->createCommand()->rawSql . ') as ' . Order::tableName()])
            ->select([
                    'country_id',
                    'FROM_UNIXTIME(' . Order::tableName() . '.created_at, \'%M %Y\') as date',
                    'AVG(delaySent) as averageDelaySent',
                ]
            )
            ->andWhere(['>=', Order::tableName() . '.created_at', strtotime('2016-09-01 00:00:00 GMT')])
            ->groupBy(['country_id', 'date'])
            ->orderBy([Order::tableName() . '.created_at' => SORT_DESC])
            ->all();

        $data = [];
        //создаем объект для итоговой стоки
        $data['total'] = clone $object;
        //кошелек
        $data['wallet'] = 0;
        //сумированее показателей по статусам
        foreach ($stats as $stat) {
            if (!isset($data[$stat['country_id']][$stat['date']]) || !is_object($data[$stat['country_id']][$stat['date']])) {
                $data[$stat['country_id']][$stat['date']] = clone $object;
            }
            switch ($stat['status_id']) {
                //статусы выкупа
                case OrderStatus::STATUS_DELIVERY_BUYOUT:
                    $data[$stat['country_id']][$stat['date']]->totalBuyout += $stat['cnt'];
                    $data['total']->totalBuyout += $stat['cnt'];
                    //конвертировать валюту
                    $data[$stat['country_id']][$stat['date']]->sumMoneyApprovedCC += $this->convertCurrency($stat['sumPrice'],
                        $stat['country_id'], $countryRate);
                    $data[$stat['country_id']][$stat['date']]->sumMoneyBuyout += $this->convertCurrency($stat['sumPrice'],
                        $stat['country_id'], $countryRate);
                    break;
                case OrderStatus::STATUS_FINANCE_MONEY_RECEIVED:
                    $data[$stat['country_id']][$stat['date']]->totalMoneyReceived += $stat['cnt'];
                    //конвертировать валюту
                    $data[$stat['country_id']][$stat['date']]->sumMoneyApprovedCC += $this->convertCurrency($stat['sumPrice'],
                        $stat['country_id'], $countryRate);
                    $data[$stat['country_id']][$stat['date']]->sumMoneyReceived += $this->convertCurrency($stat['sumPrice'],
                        $stat['country_id'], $countryRate);
                    break;
                //статусы не отправленных заказов
                case OrderStatus::STATUS_CC_APPROVED:
                case OrderStatus::STATUS_DELIVERY_PENDING:
                case OrderStatus::STATUS_DELIVERY_REJECTED:
                    $data[$stat['country_id']][$stat['date']]->sumMoneyApprovedCC += $this->convertCurrency($stat['sumPrice'],
                        $stat['country_id'], $countryRate);
                    $data[$stat['country_id']][$stat['date']]->totalNotSent += $stat['cnt'];
                    $data['total']->totalNotSent += $stat['cnt'];

                    if ($stat['status_id'] == OrderStatus::STATUS_DELIVERY_REJECTED) {
                        $data[$stat['country_id']][$stat['date']]->deliveryRejected += $stat['cnt'];
                        $data['total']->deliveryRejected += $stat['cnt'];
                    }

                    break;
                //статусы закзов в процессе
                case OrderStatus::STATUS_LOG_ACCEPTED:
                case OrderStatus::STATUS_LOG_GENERATED:
                case OrderStatus::STATUS_LOG_SET:
                case OrderStatus::STATUS_LOG_PASTED:
                case OrderStatus::STATUS_DELIVERY_ACCEPTED:
                case OrderStatus::STATUS_DELIVERY_LEFT_WAREHOUSE:
                case OrderStatus::STATUS_DELIVERY:
                case OrderStatus::STATUS_DELIVERY_LEFT_OUR_WAREHOUSE:
                case OrderStatus::STATUS_DELIVERY_REDELIVERY:
                case OrderStatus::STATUS_DELIVERY_LEFT_THEIR_WAREHOUSE:
                case OrderStatus::STATUS_DELIVERY_DELAYED:
                    $data[$stat['country_id']][$stat['date']]->sumMoneyApprovedCC += $this->convertCurrency($stat['sumPrice'],
                        $stat['country_id'], $countryRate);
                    $data[$stat['country_id']][$stat['date']]->totalInProcess += $stat['cnt'];
                    $data['total']->totalInProcess += $stat['cnt'];
                    break;
                //статусы не выкупленых заказов
                case OrderStatus::STATUS_DELIVERY_DENIAL:
                case OrderStatus::STATUS_DELIVERY_RETURNED:
                case OrderStatus::STATUS_LOGISTICS_REJECTED:
                case OrderStatus::STATUS_DELIVERY_REFUND:
                case OrderStatus::STATUS_LOGISTICS_ACCEPTED:
                case OrderStatus::STATUS_DELIVERY_LOST:
                    $data[$stat['country_id']][$stat['date']]->sumMoneyApprovedCC += $this->convertCurrency($stat['sumPrice'],
                        $stat['country_id'], $countryRate);
                    $data[$stat['country_id']][$stat['date']]->totalNotBuyout += $stat['cnt'];
                    $data['total']->totalNotBuyout += $stat['cnt'];
                    break;
                case OrderStatus::STATUS_CC_POST_CALL:
                case OrderStatus::STATUS_CC_RECALL:
                    $data[$stat['country_id']][$stat['date']]->totalProcessCC += $stat['cnt'];
                    $data['total']->totalProcessCC += $stat['cnt'];
                    break;
                case OrderStatus::STATUS_CC_FAIL_CALL:
                case OrderStatus::STATUS_CC_REJECTED:
                case OrderStatus::STATUS_CC_TRASH:
                case OrderStatus::STATUS_CC_DOUBLE:
                    $data[$stat['country_id']][$stat['date']]->totalTrashCC += $stat['cnt'];
                    $data['total']->totalTrashCC += $stat['cnt'];
                    break;
            }
        }
        //подсчет показателей
        foreach ($data as $country_id => $country) {
            if (!is_numeric($country_id)) {
                continue;
            }


            foreach ($country as $date => $res) {

                //если роль менеджер кц, то процент считать 45
                if (in_array('callcenter.manager', $roles)) {
                    $res->minPercentBuyout = 45;
                }
                //если роль куратор то процент апрува КЦ считать 45
                //если роль куратор по странам лат. америки, то процент считать 50
                if (in_array('country.curator', $roles)) {
                    $res->minPercentApprovedCC = 45;
                    if (in_array($countryCharCodes[$country_id],
                        ['MX', 'CL', 'CO', 'PE', 'AR', 'PY'])
                    ) {
                        $res->minPercentBuyout = 50;
                    }
                }
                //если нет выкупов и не отпрввленных заказов, удаляем инфу т.к. нет смысла выводить
                //                if (empty($data[$country_id][$date]->totalNotSent) && empty($data[$country_id][$date]->totalBuyout)) {
                //                    unset($data[$country_id][$date]);
                //                    continue;
                //                }
                $data[$country_id][$date]->totalApprovedCC = $res->totalBuyout + $res->totalMoneyReceived + $res->totalNotSent + $res->totalInProcess + $res->totalNotBuyout;
                //подсчет среднего чека апрува кц
                if (($res->totalApprovedCC) == 0) {
                    $data[$country_id][$date]->avgCheckApprovedCC = number_format(0, 2);
                } else {
                    $data[$country_id][$date]->avgCheckApprovedCC = number_format(($res->sumMoneyApprovedCC) / ($res->totalApprovedCC),
                        2);
                }
                //подсчет среднего чека выкупа
                if (($res->totalBuyout) == 0) {
                    $data[$country_id][$date]->avgCheckBuyout = number_format(0, 2);
                } else {
                    $data[$country_id][$date]->avgCheckBuyout = number_format(($res->sumMoneyBuyout + $res->sumMoneyReceived) / ($res->totalBuyout + $res->totalMoneyReceived),
                        2);
                }

                //подсчет процена выкупа
                if (($res->totalNotSent + $res->totalInProcess + $res->totalBuyout + $res->totalMoneyReceived + $res->totalNotBuyout) == 0) {
                    $data[$country_id][$date]->percentBuyout = number_format(0, 2);
                } else {
                    $data[$country_id][$date]->percentBuyout = number_format(($res->totalBuyout + $res->totalMoneyReceived) / ($res->totalNotSent + $res->totalInProcess + $res->totalBuyout + $res->totalMoneyReceived + $res->totalNotBuyout) * 100,
                        2);
                }
                //подсчет процена невыкупа
                if (($res->totalNotSent + $res->totalInProcess + $res->totalBuyout + $res->totalMoneyReceived + $res->totalNotBuyout) == 0) {
                    $data[$country_id][$date]->percentNotBuyout = number_format(0, 2);
                } else {
                    $data[$country_id][$date]->percentNotBuyout = number_format($res->totalNotBuyout / ($res->totalNotSent + $res->totalInProcess + $res->totalBuyout + $res->totalMoneyReceived + $res->totalNotBuyout) * 100,
                        2);
                }
                //подсчет процена апрува кц
                if (($res->totalApprovedCC + $res->totalProcessCC + $res->totalTrashCC) == 0) {
                    $data[$country_id][$date]->percentApprovedCC = number_format(0, 2);
                } else {
                    $data[$country_id][$date]->percentApprovedCC = number_format(($res->totalApprovedCC) / ($res->totalApprovedCC + $res->totalProcessCC + $res->totalTrashCC) * 100,
                        2);
                }
                //подсчет процента полученых денег
                if (($res->totalBuyout + $res->totalMoneyReceived) == 0) {
                    $data[$country_id][$date]->moneyReceivedPrecent = number_format(0, 2);
                } else {
                    $data[$country_id][$date]->moneyReceivedPrecent = number_format($res->totalMoneyReceived / ($res->totalBuyout + $res->totalMoneyReceived) * 100,
                        2);
                }
                $data['total']->totalMoneyReceived += $res->totalMoneyReceived;
                $data['total']->sumMoneyReceived += $res->sumMoneyReceived;
                $data[$country_id][$date]->sumMoneyReceived = number_format($data[$country_id][$date]->sumMoneyReceived,
                    2);
                $data['total']->sumMoneyBuyout += $res->sumMoneyBuyout;
                $data[$country_id][$date]->sumMoneyBuyout = number_format($data[$country_id][$date]->sumMoneyBuyout, 2);
                $data['total']->sumMoneyApprovedCC += $res->sumMoneyApprovedCC;
                $data[$country_id][$date]->sumMoneyApprovedCC = number_format($data[$country_id][$date]->sumMoneyApprovedCC,
                    2);

                //если % выкупа больше $minPercentBuyout и % апрува КЦ больше $minPercentApprovedCC, то считаем вознагрождение кураторам
                if (($data[$country_id][$date]->percentBuyout >= $res->minPercentBuyout) && ($data[$country_id][$date]->percentApprovedCC >= $res->minPercentApprovedCC)) {
                    $data['wallet'] += ($res->totalBuyout + $res->totalMoneyReceived) * 0.05;
                }
                //считамм убыток компании
                $data[$country_id][$date]->lossOfCompany = number_format($res->totalNotSent * 15, 2);

            }
        }
        //подсчет средней просрочки
        $sumAverageDelaySent = 0;
        foreach ($averageDelaySent as $stat) {
            if (!isset($data[$stat['country_id']][$stat['date']]) || !is_object($data[$stat['country_id']][$stat['date']])) {
                $data[$stat['country_id']][$stat['date']] = clone $object;
            }
            $data[$stat['country_id']][$stat['date']]->averageDelaySent = number_format((round($stat['averageDelaySent']) / 60 / 60 / 24),
                1);
            $sumAverageDelaySent += $data[$stat['country_id']][$stat['date']]->averageDelaySent;
        }
        if (empty($averageDelaySent)) {
            $data['total']->averageDelaySent = number_format(0, 1);
        } else {
            $data['total']->averageDelaySent = number_format(($sumAverageDelaySent / count($averageDelaySent)), 1);
        }
        //итоговый пооцен выкупа
        if (($data['total']->totalNotSent + $data['total']->totalInProcess + $data['total']->totalBuyout + $data['total']->totalMoneyReceived + $data['total']->totalNotBuyout) == 0) {
            $data['total']->percentBuyout = number_format(0, 2);
        } else {
            $data['total']->percentBuyout = number_format(($data['total']->totalBuyout + $data['total']->totalMoneyReceived) / ($data['total']->totalNotSent + $data['total']->totalInProcess + $data['total']->totalBuyout + $data['total']->totalMoneyReceived + $data['total']->totalNotBuyout) * 100,
                2);
        }
        //итоговый пооцен невыкупа
        if (($data['total']->totalNotSent + $data['total']->totalInProcess + $data['total']->totalBuyout + $data['total']->totalMoneyReceived + $data['total']->totalNotBuyout) == 0) {
            $data['total']->percentNotBuyout = number_format(0, 2);
        } else {
            $data['total']->percentNotBuyout = number_format($data['total']->totalNotBuyout / ($data['total']->totalNotSent + $data['total']->totalInProcess + $data['total']->totalBuyout + $data['total']->totalMoneyReceived + $data['total']->totalNotBuyout) * 100,
                2);
        }
        //итоговый пооцен полученых денег
        if (($data['total']->totalBuyout + $data['total']->totalMoneyReceived) == 0) {
            $data['total']->moneyReceivedPrecent = number_format(0, 2);
        } else {
            $data['total']->moneyReceivedPrecent = number_format($data['total']->totalMoneyReceived / ($data['total']->totalBuyout + $data['total']->totalMoneyReceived) * 100,
                2);
        }
        $data['total']->totalApprovedCC = $data['total']->totalBuyout + $data['total']->totalMoneyReceived + $data['total']->totalNotSent + $data['total']->totalInProcess + $data['total']->totalNotBuyout;
        //подсчет процена апрува кц
        if (($data['total']->totalApprovedCC + $data['total']->totalProcessCC + $data['total']->totalTrashCC) == 0) {
            $data['total']->percentApprovedCC = number_format(0, 2);
        } else {
            $data['total']->percentApprovedCC = number_format(($data['total']->totalApprovedCC) / ($data['total']->totalApprovedCC + $data['total']->totalProcessCC + $data['total']->totalTrashCC) * 100,
                2);
        }

        //подсчет среднего чека апрува кц
        if (($data['total']->totalApprovedCC) == 0) {
            $data['total']->avgCheckApprovedCC = number_format(0, 2);
        } else {
            $data['total']->avgCheckApprovedCC = number_format(($data['total']->sumMoneyApprovedCC) / ($data['total']->totalApprovedCC),
                2);
        }
        //подсчет среднего чека выкупа
        if (($data['total']->totalBuyout) == 0) {
            $data['total']->avgCheckBuyout = number_format(0, 2);
        } else {
            $data['total']->avgCheckBuyout = number_format(($data['total']->sumMoneyBuyout + $data['total']->sumMoneyReceived) / ($data['total']->totalBuyout + $data['total']->totalMoneyReceived),
                2);
        }

        //сумма полученых денег
        $data['total']->sumMoneyReceived = number_format($data['total']->sumMoneyReceived, 2);
        //сумма полученых денег
        $data['total']->sumMoneyBuyout = number_format($data['total']->sumMoneyBuyout, 2);
        //сумма полученых денег
        $data['total']->sumMoneyApprovedCC = number_format($data['total']->sumMoneyApprovedCC, 2);
        //итоговый убыток компании
        $data['total']->lossOfCompany = number_format($data['total']->totalNotSent * 15, 2);

        //запись в кеш
        if ($this->_userId != null) {
            Yii::$app->cache->add('profile_statistic_user_' . $this->_userId, $data, 60 * 60);
        }

        return $data;
    }

    private static function container()
    {
        return (object)[
            'percentBuyout'        => 0,
            'notSended'            => 0,
            'totalBuyout'          => 0,
            'totalMoneyReceived'   => 0,
            'sumMoneyReceived'     => 0,
            'sumMoneyBuyout'       => 0,
            'sumMoneyApprovedCC'   => 0,
            'avgCheckBuyout'       => 0,
            'avgCheckApprovedCC'   => 0,
            'moneyReceivedPrecent' => 0,
            'totalInProcess'       => 0,
            'totalNotBuyout'       => 0,
            'percentNotBuyout'     => 0,
            'totalApprovedCC'      => 0,
            'percentApprovedCC'    => 0,
            'totalProcessCC'       => 0,
            'totalTrashCC'         => 0,
            'totalNotSent'         => 0,
            'deliveryRejected'     => 0,
            'lossOfCompany'        => 0,
            'averageDelaySent'     => 0,
            'minPercentBuyout'     => 65,
            'minPercentApprovedCC' => 0,
            'minAvgCheckCC'        => 0,
        ];
    }

    private static function getStatusMap()
    {
        return [
            OrderStatus::STATUS_CC_POST_CALL,
            OrderStatus::STATUS_CC_RECALL,
            OrderStatus::STATUS_CC_FAIL_CALL,
            OrderStatus::STATUS_CC_APPROVED,
            OrderStatus::STATUS_CC_REJECTED,
            OrderStatus::STATUS_LOG_ACCEPTED,
            OrderStatus::STATUS_LOG_GENERATED,
            OrderStatus::STATUS_LOG_SET,
            OrderStatus::STATUS_LOG_PASTED,
            OrderStatus::STATUS_DELIVERY_ACCEPTED,
            OrderStatus::STATUS_DELIVERY_LEFT_WAREHOUSE,
            OrderStatus::STATUS_DELIVERY,
            OrderStatus::STATUS_DELIVERY_BUYOUT,
            OrderStatus::STATUS_DELIVERY_DENIAL,
            OrderStatus::STATUS_DELIVERY_RETURNED,
            OrderStatus::STATUS_FINANCE_MONEY_RECEIVED,
            OrderStatus::STATUS_DELIVERY_REJECTED,
            OrderStatus::STATUS_DELIVERY_LEFT_OUR_WAREHOUSE,
            OrderStatus::STATUS_DELIVERY_REDELIVERY,
            OrderStatus::STATUS_DELIVERY_LEFT_THEIR_WAREHOUSE,
            OrderStatus::STATUS_LOGISTICS_REJECTED,
            OrderStatus::STATUS_DELIVERY_DELAYED,
            OrderStatus::STATUS_CC_TRASH,
            OrderStatus::STATUS_CC_DOUBLE,
            OrderStatus::STATUS_DELIVERY_REFUND,
            OrderStatus::STATUS_LOGISTICS_ACCEPTED,
            OrderStatus::STATUS_DELIVERY_LOST,
            OrderStatus::STATUS_DELIVERY_PENDING,
        ];
    }

    /**
     * @param $sum
     * @param $country_id
     * @param $countryRate
     *
     * @return float
     */
    private function convertCurrency($sum, $country_id, $countryRate)
    {
        if (empty($countryRate[$country_id])) {
            return 0;
        }
        $dollar = $sum / $countryRate[$country_id];

        return $dollar;
    }
}
