<?php
namespace app\modules\report\widgets;

use app\modules\report\components\ReportFormDeliveryTerms;

/**
 * Class ReportFilterDeliveryTerms
 * @package app\modules\report\widgets
 */
class ReportFilterDeliveryTerms extends ReportFilter
{
    /** @var ReportFormDeliveryTerms */
    public $reportForm;

    /**
     * @return string
     */
    public function apply()
    {
        return $this->render('report-filter-delivery-terms/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}
