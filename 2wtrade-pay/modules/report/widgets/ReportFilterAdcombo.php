<?php

namespace app\modules\report\widgets;

use app\modules\report\components\ReportFormAdcombo;

/**
 * Class ReportFilterAdCombo
 * @package app\modules\report\widgets
 */
class ReportFilterAdcombo extends ReportFilter
{
    /** @var ReportFormAdcombo */
    public $reportForm;

    /**
     * @return string
     */
    public function apply()
    {
        return $this->render('report-filter-adcombo/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}
