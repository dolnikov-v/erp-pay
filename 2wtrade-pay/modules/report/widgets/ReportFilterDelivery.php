<?php
namespace app\modules\report\widgets;

use app\modules\report\components\ReportFormDelivery;

/**
 * Class ReportFilterDelivery
 * @package app\modules\report\widgets
 */
class ReportFilterDelivery extends ReportFilter
{
    /** @var ReportFormDelivery */
    public $reportForm;

    /**
     * @return string
     */
    public function apply()
    {
        return $this->render('report-filter-delivery/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}
