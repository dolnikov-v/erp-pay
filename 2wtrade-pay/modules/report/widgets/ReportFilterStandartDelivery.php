<?php
namespace app\modules\report\widgets;

use app\modules\report\components\ReportFormStandartDelivery;

/**
 * Class ReportFilterStandartDelivery
 * @package app\modules\report\widgets
 */
class ReportFilterStandartDelivery extends ReportFilter
{
    /** @var ReportFormStandartDelivery */
    public $reportForm;

    /**
     * @return string
     */
    public function apply()
    {
        return $this->render('report-filter-standart-delivery/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}
