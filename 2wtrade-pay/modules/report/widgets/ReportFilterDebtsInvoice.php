<?php

namespace app\modules\report\widgets;

use app\modules\report\components\ReportFormDebtsInvoice;

/**
 * Class ReportFilterDebtsInvoice
 * @package app\modules\report\widgets
 */
class ReportFilterDebtsInvoice extends ReportFilter
{
    /** @var ReportFormDebtsInvoice */
    public $reportForm;

    /**
     * @return string
     */
    public function apply()
    {
        return $this->render('report-filter-debts-invoice/filter', [
            'reportForm' => $this->reportForm
        ]);
    }
}
