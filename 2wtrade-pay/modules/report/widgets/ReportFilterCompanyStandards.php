<?php
namespace app\modules\report\widgets;

use app\modules\report\components\ReportFormCompanyStandards;

/**
 * Class ReportFilterCompanyStandards
 * @package app\modules\report\widgets
 */
class ReportFilterCompanyStandards extends ReportFilter
{
    /** @var ReportFormCompanyStandards */
    public $reportForm;

    /**
     * @return string
     */
    public function apply()
    {
        return $this->render('report-filter-company-standards/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}
