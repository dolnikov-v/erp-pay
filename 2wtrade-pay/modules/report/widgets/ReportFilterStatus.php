<?php
namespace app\modules\report\widgets;

use app\modules\report\components\ReportFormStatus;

/**
 * Class ReportFilterStatus
 * @package app\modules\report\widgets
 */
class ReportFilterStatus extends ReportFilter
{
    /** @var ReportFormStatus */
    public $reportForm;

    /**
     * @return string
     */
    public function apply()
    {
        return $this->render('report-filter-status/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}
