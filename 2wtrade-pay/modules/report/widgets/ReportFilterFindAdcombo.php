<?php

namespace app\modules\report\widgets;

use app\modules\report\components\ReportFormFindAdcombo;

/**
 * Class ReportFilterFindAdcombo
 * @package app\modules\report\widgets
 */
class ReportFilterFindAdcombo extends ReportFilter
{
    /** @var ReportFormFindAdcombo */
    public $reportForm;

    /**
     * @return string
     */
    public function apply()
    {
        return $this->render('report-filter-find-adcombo/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}
