<?php
namespace app\modules\report\widgets;

use app\modules\report\components\ReportFormConsolidateSale;

/**
 * Class ReportFilterConsolidateSale
 * @package app\modules\report\widgets
 */
class ReportFilterConsolidateSale extends ReportFilter
{
    /** @var ReportFormConsolidateSale */
    public $reportForm;

    public function apply()
    {
        return $this->render('report-filter-consolidate-sale/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}