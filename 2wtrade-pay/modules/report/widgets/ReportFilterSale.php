<?php
namespace app\modules\report\widgets;

use app\modules\report\components\ReportFormSale;

/**
 * Class ReportFilterSale
 * @package app\modules\report\widgets
 */
class ReportFilterSale extends ReportFilter
{
    /**
     * @var ReportFormSale
     */
    public $reportForm;

    /**
     * @return string
     */
    public function apply()
    {
        return $this->render('report-filter-sale/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}