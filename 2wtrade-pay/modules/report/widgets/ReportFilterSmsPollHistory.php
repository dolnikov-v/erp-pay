<?php
namespace app\modules\report\widgets;

use app\modules\report\components\ReportFormSmsPollHistory;

/**
 * Class ReportFilterSmsPollHistory
 * @package app\modules\report\widgets
 */
class ReportFilterSmsPollHistory extends ReportFilter
{
    /**
     * @var ReportFilterSmsPollHistory
     */
    public $reportForm;

    /**
     * @return string
     */
    public function apply()
    {
        return $this->render('report-sms-poll-history/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}