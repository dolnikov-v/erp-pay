<?php
namespace app\modules\report\widgets;

use Yii;
use app\modules\report\components\filters\TypeDatePartFilter;
use app\modules\report\components\ReportFormApproveByCountry;
use app\modules\report\extensions\DataProvider;
use yii\base\Widget;
use yii\helpers\ArrayHelper;

/**
 * Class ReportFilter
 * @package app\modules\report\widgets
 */
class GraphByCountriesIntervals extends Widget
{
    /**
     * @var ReportFormApproveByCountry
     */
    public $reportForm;

    /**
     * @var DataProvider
     */
    public $dataProvider;

    /**
     * @var array
     */
    protected $data = [];

    /** @var array */
    protected $dates = [];

    /** @var array */
    protected $countries = [];

    /**
     * @var string
     */
    protected $interval;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->interval = $this->reportForm->getTypeDatePartFilter()->type_date_part;
        $this->data = $this->prepareData($this->dataProvider->allModels);
        $this->dates = $this->generateDates();
        $this->countries = $this->generateCountries();
    }

    /**
     * @return array
     */
    protected function generateDates()
    {
        if ($dates = ArrayHelper::getColumn($this->data, 'date')) {
            $dates = array_unique($dates);
            $dates = array_values($dates);
            asort($dates);
        }

        return $dates;
    }

    /**
     * @return array
     */
    protected function generateCountries()
    {
        $countries = ArrayHelper::getColumn($this->dataProvider->allModels, 'country_name');
        $countries = array_unique($countries);
        asort($countries);

        return $countries;
    }

    /**
     * @param array $data
     * @return array
     */
    protected function prepareData($data)
    {
        foreach ($data as &$model) {

            $year = $model['yearcreatedat'];
            $month = str_pad($model['monthcreatedat'], 2, '0', STR_PAD_LEFT);
            $day = str_pad($model['daycreatedat'], 2, '0', STR_PAD_LEFT);
            $week = str_pad($model['weekcreatedat'], 2, '0', STR_PAD_LEFT);

            switch ($this->interval) {

                case TypeDatePartFilter::TYPE_DATE_PART_DAY:
                    $model['date'] = $year . '-' . $month . '-' . $day;
                    break;

                case TypeDatePartFilter::TYPE_DATE_PART_MONTH:
                    $model['date'] = $year . '-' . $month;
                    break;

                case TypeDatePartFilter::TYPE_DATE_PART_WEEK:
                    $model['date'] = $year . '-' . $week;
                    break;
            }

        }
        unset($model);

        return $data;
    }

    /**
     * @param array $params
     * @return array
     */

    protected function sumDataParams($params)
    {
        $data = [];

        foreach ($this->countries as $country) {
                foreach ($this->dates as $date) {
                    foreach ($params as $param) {
                        $data[$country][$date][$param] = 0;
                    }
                }
        }

        foreach ($this->data as $model) {
            $country = $model['country_name'];

            $date = $model['date'];
            foreach ($params as $param) {
                $data[$country][$date][$param] += $model[$param];
            }
        }

        return $data;
    }

    /**
     * @param string $date
     * @return string
     */
    protected function formatDate($date)
    {
        switch ($this->interval) {

            case TypeDatePartFilter::TYPE_DATE_PART_DAY:
                $date = Yii::$app->formatter->asDate(strtotime($date));
                break;

            case TypeDatePartFilter::TYPE_DATE_PART_MONTH:
                $date = Yii::$app->formatter->asMonth(strtotime($date . '-01'));
                break;

            case TypeDatePartFilter::TYPE_DATE_PART_WEEK:
                list($year, $week) = explode('-', $date);
                if ($week == 0) {
                    $time = strtotime($year . '-01-01');
                } else {
                    $timestamp = mktime(0, 0, 0, 1, 1, $year) + ($week * 7 * 86400);
                    $time = $timestamp - 86400 * (date('N', $timestamp) - 1);
                }
                $date = Yii::$app->formatter->asDate($time);
                break;
        }

        return $date;
    }

    /**
     * @param array
     * @return array
     */
    protected function formatDates($dates)
    {
        foreach ($dates as &$date) {
            $date = $this->formatDate($date);
        }
        unset($date);

        return $dates;
    }
}
