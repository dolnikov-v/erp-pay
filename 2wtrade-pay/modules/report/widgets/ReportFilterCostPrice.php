<?php
namespace app\modules\report\widgets;

use app\modules\report\components\ReportFormCostPrice;

/**
 * Class ReportFilterCostPrice
 * @package app\modules\report\widgets
 */
class ReportFilterCostPrice extends ReportFilter
{
    /**
     * @var ReportFormCostPrice
     */
    public $reportForm;

    /**
     * @return string
     */
    public function apply()
    {
        return $this->render('report-filter-cost-price/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}