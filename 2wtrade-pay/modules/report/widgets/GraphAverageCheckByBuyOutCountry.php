<?php
namespace app\modules\report\widgets;

use yii\helpers\ArrayHelper;

/**
 * Class GraphAverageCheckByCountry
 * @package app\modules\report\widgets
 */
class GraphAverageCheckByBuyOutCountry extends GraphByCountriesIntervals
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('graph-average-check-by-buyout-country/index', [
            'series' => $this->generateSeries(),
            'dates' => $this->formatDates($this->dates),
        ]);
    }

    /**
     * @return array
     */
    private function generateSeries()
    {
        $data = $this->sumDataParams(['vykup']);
        foreach ($data as &$country) {
            foreach ($country as &$date) {
                $date['vykup'] = round($date['vykup'], 2);
            } unset($date);
        } unset($country);

        $series = [];
        foreach ($this->countries as $country) {
            $series[] = [
                'name' => $country,
                'data' => ArrayHelper::getColumn($data[$country], 'vykup', false),
            ];
        }

        return $series;
    }
}
