<?php
namespace app\modules\report\widgets;

use app\modules\report\components\ReportFormAccountingCosts;

/**
 * Class ReportFilterAccountingCosts
 * @package app\modules\report\widgets
 */
class ReportFilterAccountingCosts extends ReportFilter
{
    /**
     * @var ReportFormAccountingCosts
     */
    public $reportForm;

    /**
     * @return string
     */
    public function apply()
    {
        return $this->render('report-filter-accounting-costs/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }

}
