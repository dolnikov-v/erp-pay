<?php

namespace app\modules\report\widgets;

/**
 * Class ReportFilterApproveByCountry
 * @package app\modules\report\widgets
 */
class ReportFilterBuyoutByCountry extends ReportFilter
{
    /** @var ReportFormAppproveByCountry */
    public $reportForm;

    public function apply()
    {
        return $this->render('report-filter-buyout-by-country/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}
