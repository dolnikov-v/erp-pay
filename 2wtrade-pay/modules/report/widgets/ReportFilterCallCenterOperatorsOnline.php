<?php

namespace app\modules\report\widgets;

/**
 * Class ReportFilterCallCenterOperatorsOnline
 * @package app\modules\report\widgets
 */
class ReportFilterCallCenterOperatorsOnline extends ReportFilter
{
    /** @var ReportFilterCallCenterOperatorsOnline */
    public $reportForm;

    public function apply()
    {
        return $this->render('report-filter-call-center-operators-online/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}
