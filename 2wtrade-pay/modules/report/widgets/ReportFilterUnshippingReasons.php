<?php
namespace app\modules\report\widgets;

/**
 * Class ReportFilterUnshippingReasons
 * @package app\modules\report\widgets
 */
class ReportFilterUnshippingReasons extends ReportFilter
{
    /**
     * @var ReportFilterUnshippingReasons
     */
    public $reportForm;

    /**
     * @return string
     */
    public function apply()
    {
        return $this->render('report-filter-unshipping-reasons/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}