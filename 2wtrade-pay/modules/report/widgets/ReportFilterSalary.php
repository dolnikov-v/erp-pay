<?php
namespace app\modules\report\widgets;

use app\modules\report\components\ReportFormSalary;

/**
 * Class ReportFilterSalary
 * @package app\modules\report\widgets
 */
class ReportFilterSalary extends ReportFilter
{
    /** @var ReportFormSalary */
    public $reportForm;

    /**
     * @return string
     */
    public function apply()
    {
        return $this->render('report-filter-salary/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}
