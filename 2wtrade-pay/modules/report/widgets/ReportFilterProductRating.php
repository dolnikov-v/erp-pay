<?php
namespace app\modules\report\widgets;

use app\modules\report\components\ReportFormProduct;

/**
 * Class ReportFilterProduct
 * @package app\modules\report\widgets
 */
class ReportFilterProductRating extends ReportFilter
{
    /** @var ReportFormProduct */
    public $reportForm;

    /**
     * @return string
     */
    public function apply()
    {
        return $this->render('report-filter-product-rating/filter', [
            'reportForm' => $this->reportForm,
        ]);
    }
}