<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/** @var app\modules\report\components\ReportFormApproved $reportForm */
?>

<?php
$form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    'enableClientValidation' => false,
]);
?>

<div class="row">
    <div class="col-lg-9">
        <?= $reportForm->getDateFilter()->restore($form) ?>
    </div>
    <div class="col-md-3">
        <?= $reportForm->getCallCenterFilter()->restore($form) ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')) ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')) ?>

        <div class="pull-right">
            <?= $reportForm->getCountryFilter()->restore($form) ?>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>
