<?php
use app\components\widgets\ActiveForm;
use app\modules\report\assets\FilterCountryDeliveryAsset;
use yii\helpers\Url;

/** @var app\modules\report\components\ReportFormDeliveryDebts $reportForm */
FilterCountryDeliveryAsset::register($this);
?>

<?php
$form = ActiveForm::begin([
    'action' => [Yii::$app->controller->action->id],
    'method' => 'get',
    'enableClientValidation' => false,
]);
?>

<div class="row">
    <div class="col-lg-3">
        <?= $reportForm->getMonthFilter()->restore($form) ?>
    </div>
    <div class="col-lg-9">
        <?= $reportForm->getCountryDeliverySelectMultipleFilter()->restore($form) ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')) ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute(Yii::$app->controller->action->id)) ?>

        <!--<div class="pull-right">
            <a class="btn btn-default btn-sm"
               href="<? /*= Url::toRoute(array_merge(['export',], Yii::$app->request->queryParams)) */ ?>">
                <i class="fa fa-share-square-o"></i> <? /*= Yii::t('common', 'Экспорт') */ ?>
            </a>
        </div>-->
    </div>
</div>

<?php ActiveForm::end(); ?>
