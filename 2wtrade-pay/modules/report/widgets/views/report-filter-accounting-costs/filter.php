<?php
use app\widgets\Panel;

/** @var yii\web\View $this */
/** @var app\modules\report\components\ReportForm $reportForm */
?>

<?= Panel::widget([
    'id' => 'report_filter_accounting_costs',
    'title' => Yii::t('common', 'Фильтр'),
    'content' => $this->render('_content', [
        'reportForm' => $reportForm,
    ]),
    'collapse' => true,
]) ?>
