<?php
use app\widgets\Panel;

/** @var yii\web\View $this */
/** @var app\modules\report\components\ReportFormDelivery $reportForm */
?>

<?= Panel::widget([
    'id' => 'report_filter_delivery',
    'title' => Yii::t('common', 'Фильтр'),
    'content' => $this->render('_content', [
        'reportForm' => $reportForm,
    ]),
    'collapse' => true,
]) ?>
