<?php
use app\components\widgets\ActiveForm;
use app\modules\report\widgets\AdditionalGroup;
use yii\helpers\Url;

/** @var app\modules\report\components\ReportFormWebmaster $reportForm */
?>

<?php
$form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    'enableClientValidation' => false,
]);

?>

<div class="row">
    <div class="col-lg-9">
        <?= $reportForm->getDateFilter()->restore($form) ?>
    </div>
    <div class="col-lg-3">
        <?= $reportForm->getProductFilter()->restore($form) ?>
    </div>
    <div class="col-lg-3">
        <?= $reportForm->getCalculatePercentFilter()->restore($form) ?>
    </div>
</div>
<div class="row">
    <?= $reportForm->getCountryDeliverySelectMultipleFilter()->restore($form) ?>
</div>

<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')) ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
