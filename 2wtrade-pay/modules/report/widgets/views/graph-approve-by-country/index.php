<?php
use miloschuman\highcharts\Highcharts;
/**
 * @var array $dates
 * @var array $seriesApprove
 * @var array $seriesPercent
 */
?>

<br>
<br>

<?= Highcharts::widget([
    'options' => [
        'title' => ['text' => ' '],
        'credits' => [
            'enabled'=> false
        ],
        'xAxis' => [
            'categories' => $dates,
        ],
        'yAxis' => [
            'title' => ['text' => Yii::t('common', 'Количество апрувов')],
        ],
        'series' => $seriesApprove,
    ],
]); ?>

<br>
<br>

<?= Highcharts::widget([
    'options' => [
        'title' => ['text' => ' '],
        'credits' => [
            'enabled'=> false
        ],
        'xAxis' => [
            'categories' => $dates,
        ],
        'yAxis' => [
            'title' => ['text' => Yii::t('common', '% апрувов')],
        ],
        'series' => $seriesPercent,
    ],
]); ?>
