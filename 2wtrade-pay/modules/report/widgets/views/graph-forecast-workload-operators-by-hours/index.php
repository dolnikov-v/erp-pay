<?php
use miloschuman\highcharts\Highcharts;


/** @var app\modules\report\components\ReportFormAdvertising $reportForm */
/** @var yii\data\ArrayDataProvider $dataProvider */
/** @var integer $day */

?>

<br>
<br>
<?php
echo Highcharts::widget([
    'options' => [
        'title' => ['text' => ' '],
        'credits' => [
            'enabled'=> false
        ],
        'xAxis' => [
            'categories' => $hours,
        ],
        'yAxis' => [
            'title' => ['text' => Yii::t('common', 'Прогноз количества лидов')],
        ],
        'series' => $leadcounts
    ],
]);
?>
