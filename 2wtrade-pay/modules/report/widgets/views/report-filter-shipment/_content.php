<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/** @var app\modules\report\components\ReportFormShipment $reportForm */
?>

<?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>

<div class="row">
    <?= $reportForm->getShipmentFilter()->restore($form) ?>
    <div class="col-lg-3" id="sale_all_countries">
        <p>&nbsp;</p>
        <?= $reportForm->getCountryFilter()->restore($form) ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')) ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
