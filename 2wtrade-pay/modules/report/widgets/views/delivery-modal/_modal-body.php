<?php

/** @var string $url */
?>

<h4 class="modal-title modal-zip-code-title text-center">
    <span class="glyphicon glyphicon-cog glyphicon-cog-animate"></span>&nbsp;
    Идет поиск Zip-кодов, пожалуста подождите...
</h4>
<div class="show-zip-codes">
    <textarea id="delivery_modal_zip_codes_area" style="width: 90%; margin-left: 5%; height: 150px;"></textarea>
</div>
