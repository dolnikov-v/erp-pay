<?php
use app\widgets\Modal;
?>

<?= Modal::widget([
    'id' => 'modal_delivery',
    'title' => Yii::t('common', 'Список ZIP-кодов'),
    'body' => $this->render('_modal-body'),
    'footer' => '<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>',
]) ?>
