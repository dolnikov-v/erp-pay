<?php
use app\components\widgets\ActiveForm;
use app\modules\report\widgets\AdditionalGroupInProgress;
use yii\helpers\Url;

/** @var app\modules\report\components\ReportFormInProgress $reportForm */
?>

<?php
$form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    'enableClientValidation' => false,
]);

$additionalGroupInProgress = AdditionalGroupInProgress::begin([
    'form' => $form,
    'model' => $reportForm,
]);
?>

<div class="row">
    <div class="col-lg-9">
        <?= $reportForm->getDateFilter()->restore($form) ?>
    </div>
    <div class="col-lg-3">
        <?= $reportForm->restoreGroupBy($form) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-3">
        <?= $reportForm->getCountrySelectFilter()->restore($form) ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')) ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')) ?>

        <div class="pull-right">
            <?= $additionalGroupInProgress->renderButton() ?>
        </div>
    </div>
</div>

<?= $additionalGroupInProgress->renderModal() ?>

<?php AdditionalGroupInProgress::end(); ?>
<?php ActiveForm::end(); ?>
