<?php
use app\components\widgets\ActiveForm;
use app\modules\report\widgets\AdditionalGroup;
use yii\helpers\Url;

/** @var app\modules\report\components\ReportFormSalary $reportForm */
?>

<?php
$form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    'enableClientValidation' => false,
]);

?>

<div class="row">
    <?= $reportForm->getDateWorkFilter()->restore($form) ?>
    <div class="col-lg-3">
        <?= $reportForm->getCallCenterMonthlyStatementFilter()->restore($form) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-3">
        <?= $reportForm->getOfficeFilter()->restore($form) ?>
    </div>
    <div class="col-lg-3">
        <?= $reportForm->getCallCenterFilter()->restore($form) ?>
    </div>
    <div class="col-lg-3">
        <?= $reportForm->getTeamLeadFilter()->restore($form) ?>
    </div>
    <div class="col-lg-3">
        <?= $reportForm->getDesignationFilter()->restore($form) ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')) ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
