<?php

use yii\helpers\Url;

/** @var string $id */
/** @var string $typeNav */
/** @var string $typeTabs */
/** @var array $tabs */
/** @var boolean $actions */
?>



<?php if ($actions): ?>
    <div class="pull-right action-nav-actions">
        <?php if (!is_bool($actions)): ?>
            <?= $actions ?>
        <?php endif; ?>
    </div>
<?php endif; ?>


<ul <?php if ($id): ?>id="<?= $id ?>"<?php endif; ?> class="nav <?= $typeNav ?> <?= $typeTabs ?>"
    data-plugin="nav-tabs">
    <?php foreach ($tabs as $key => $tab): ?>
        <li class="<?php if (Yii::$app->requestedAction->id == $tab['action']): ?>active<?php endif; ?>">
            <a href="<?= Url::to([$tab['action']]) ?>">
                <?php if (isset($tab['icon'])): ?>
                    <i class="icon <?= $tab['icon'] ?>"></i>
                <?php endif; ?>
                <?= $tab['label'] ?>
            </a>
        </li>
    <?php endforeach; ?>
</ul>
