<?php
use app\components\widgets\ActiveForm;
use app\modules\report\widgets\AdditionalGroupTopProduct;
use yii\helpers\Url;

/** @var app\modules\report\components\ReportFormProduct $reportFormTopProduct */
?>

<?php

$form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    'enableClientValidation' => false,
]);

$additionalGroup = AdditionalGroupTopProduct::begin([
    'form' => $form,
    'model' => $reportFormTopProduct,
]);
?>

<div class="row">
    <div class="col-lg-9">
        <?= $reportFormTopProduct->getDateFilter()->restore($form) ?>
    </div>

    <div class="col-lg-3">
        <?= $reportFormTopProduct->getProductFilter()->restore($form) ?>
    </div>
    <div class="col-lg-3">
        <?= $reportFormTopProduct->getCountrySelectFilter()->restore($form) ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')) ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')) ?>

        <div class="pull-right">
            <?= $additionalGroup->renderButton() ?>
        </div>
    </div>
</div>

<?= $additionalGroup->renderModal() ?>

<?php AdditionalGroupTopProduct::end(); ?>
<?php ActiveForm::end(); ?>
