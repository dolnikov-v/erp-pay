<?php
use app\widgets\Panel;

/** @var yii\web\View $this */
/** @var app\modules\report\components\ReportFormProduct $reportFormTopProduct */
?>

<?=Panel::widget([
    'id' => 'report_filter_product',
    'title' => Yii::t('common', 'Фильтр'),
    'content' => $this->render('_content', [
        'reportFormTopProduct' => $reportFormTopProduct,
    ]),
    'collapse' => true,
]);?>
