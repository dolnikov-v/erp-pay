<?php

use app\components\widgets\ActiveForm;
use app\widgets\custom\Checkbox;
use yii\helpers\Url;

/**@var $this \yii\web\View */
/**@var $reportForm \app\modules\report\components\ReportFormFindAdcombo */

?>


<?php $form = ActiveForm::begin([
    'action' => Url::toRoute(['index', '#' => 'tab3']),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>

<div class="row">
    <?= $reportForm->getDailyFilter()->restore($form) ?>
    <div class="col-lg-3">
        <?= $reportForm->getTypeFilter()->restore($form) ?>
    </div>
    <div class="col-lg-3">
        <?= $reportForm->getIdFilter()->restore($form) ?>
    </div>
    <input type="hidden" name="query-type" value="adcombo">
</div>

<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')) ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute(['index', '#' => 'tab3'])) ?>

        <div class="pull-right">
            <?= Checkbox::widget([
                'label' => Yii::t('common', 'Поиск в нашей базе'),
                'checked' => $reportForm->getSearchInBase(),
                'name' => 'search-in-base'
            ]) ?>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>
