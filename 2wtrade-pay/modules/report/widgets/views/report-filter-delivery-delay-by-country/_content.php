<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/** @var app\modules\report\components\ReportFormDeliveryDelayByCountry $reportForm */
?>

<?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>

<div class="row">
    <div class="col-lg-3">
        <?= $reportForm->getCountrySelectFilter()->restore($form) ?>
    </div>
    <div class="col-lg-3">
        <?= $reportForm->getCuratorUserFilter()->restore($form) ?>
    </div>

    <?php if (Yii::$app->user->isSuperadmin) { ?>
            <div class="col-lg-3">
            <?= $form->field($reportForm, 'all_countries')
                    ->checkbox([
                        'label' => Yii::t('common','Все страны'),
                        'labelOptions' => [
                            'style' => 'padding-left:20px;'
                        ],
                    ]);
            ?>
            </div>
    <?php } ?>
</div>

<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')) ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>