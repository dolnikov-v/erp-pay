<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/** @var app\modules\report\components\ReportFormValidateFinance $reportForm */
/** @var app\components\widgets\ActiveForm $form */
?>

<?php $form = ActiveForm::begin([
    'action' => Url::to(['validate-finance/index']) . '#tab0',
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>

<div class="row">
    <?= $reportForm->restore($form) ?>
</div>

<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')) ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute(['index', '#' => 'tab0'])) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
