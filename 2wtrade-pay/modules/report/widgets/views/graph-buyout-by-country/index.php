<?php
use miloschuman\highcharts\Highcharts;
/**
 * @var array $dates
 * @var array $seriesVykup
 * @var array $seriesPercent
 */

echo Highcharts::widget([
    'options' => [
        'title' => ['text' => ' '],
        'credits' => [
            'enabled' => false,
        ],
        'xAxis' => [
            'categories' => $dates,
        ],
        'yAxis' => [
            'title' => ['text' => Yii::t('common', 'Количество выкупов')],
        ],
        'series' => $seriesVykup,
    ],
]);
?>
<br>
<br>

<?= Highcharts::widget([
    'options' => [
        'title' => ['text' => ' '],
        'credits' => [
            'enabled'=> false
        ],
        'xAxis' => [
            'categories' => $dates,
        ],
        'yAxis' => [
            'title' => ['text' => Yii::t('common', '% выкупа')],
        ],
        'series' => $seriesPercent,
    ],
]); ?>