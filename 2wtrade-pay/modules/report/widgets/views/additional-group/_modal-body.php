<?php
/** @var \app\components\widgets\ActiveForm $form */
/** @var \app\modules\report\components\ReportForm $model */
?>

<div class="row margin-bottom-20">
    <div class="col-lg-6">
        <?= $model->getCountryFilter()->restore($form) ?>
    </div>
    <div class="col-lg-6">
        <?= $model->getCalculatePercentFilter()->restore($form) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        <?= $model->getPaymentAdvertisingFilter()->restore($form) ?>
    </div>
    <div class="col-lg-6">
        <?= $model->getCalculateDollarFilter()->restore($form) ?>
    </div>
</div>
