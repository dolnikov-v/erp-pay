<?php
use app\widgets\Modal;

/** @var \app\components\widgets\ActiveForm $form */
/** @var \app\modules\report\components\ReportForm $model */
/** @var string $modalId */
?>

<?= Modal::widget([
    'id' => $modalId,
    'size' => Modal::SIZE_MEDIUM,
    'title' => Yii::t('common', 'Дополнительные параметры'),
    'body' => $this->render('_modal-body', [
        'form' => $form,
        'model' => $model,
    ]),
    'footer' => $this->render('_model-footer'),
]) ?>
