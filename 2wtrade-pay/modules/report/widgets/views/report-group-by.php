<?php
/** @var \app\components\widgets\ActiveForm $form */
/** @var \app\modules\report\components\ReportForm $reportForm */
?>

<?= $form->field($reportForm, 'groupBy')->select2List($reportForm->groupByCollection, [
    'name' => 's[groupBy]'
]) ?>


