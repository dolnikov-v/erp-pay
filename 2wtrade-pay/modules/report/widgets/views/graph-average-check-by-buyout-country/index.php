<?php
use miloschuman\highcharts\Highcharts;
/**
 * @var array $dates
 * @var array $series
 */

echo Highcharts::widget([
    'options' => [
        'title' => ['text' => ' '],
        'credits' => [
            'enabled' => false,
        ],
        'xAxis' => [
            'categories' => $dates,
        ],
        'yAxis' => [
            'title' => ['text' => Yii::t('common', 'Средний чек по выкупам, USD')],
        ],
        'series' => $series,
    ],
]);
