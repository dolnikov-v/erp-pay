<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/** @var app\modules\report\components\ReportFormForecastLead $reportForm */
?>

<?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
]); ?>

<div class="row">
    <?= $reportForm->getForecastLeadFilter()->restore($form) ?>
</div>
<div class="row">
    <div class="col-lg-3">
        <?= $reportForm->getCountrySelectFilter()->restore($form) ?>
    </div>
    <div class="col-lg-3">
        <?= $reportForm->getOrderProductSelectFilter()->restore($form) ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')) ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')) ?>
    </div>
</div>
