<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/** @var app\modules\report\components\ReportFormDeliveryPayment $reportForm */
?>

<?php
$form = ActiveForm::begin([
    'action' => [Yii::$app->controller->action->id],
    'method' => 'get',
    'enableClientValidation' => false,
]);
?>

<div class="row">
    <div class="col-lg-3">
        <?= $reportForm->getMonthFilter()->restore($form) ?>
    </div>
    <div class="col-lg-9">
        <?= $reportForm->getCountryDeliverySelectMultipleFilter()->restore($form) ?>
    </div>
    <div class="col-lg-6">
        <?= $reportForm->getPaymentNameFilter()->restore($form) ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')) ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute(Yii::$app->controller->action->id)) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
