<?php

use app\components\widgets\ActiveForm;
use yii\helpers\Url;
use app\widgets\custom\Checkbox;

/**@var $this \yii\web\View */
/**@var $reportForm \app\modules\report\components\ReportFormFindCallCenter */

?>

<?php $form = ActiveForm::begin([
    'action' => Url::to(['find-in-call-center']),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>

<div class="row">
    <div class="col-lg-6">
        <?= $reportForm->getIdFilter()->restore($form) ?>
    </div>
    <div class="col-lg-3">
        <?= $reportForm->getCallCenterFilter()->restore($form) ?>
    </div>
    <div class="col-lg-3">
        <?= $reportForm->getStatusFilter()->restore($form) ?>
    </div>
</div>

<div class="row">
    <?= $reportForm->getDailyFilter()->restore($form) ?>
</div>

<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')) ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute([''])) ?>

        <div class="pull-right">
            <?= Checkbox::widget([
                'label' => Yii::t('common', 'Поиск в нашей базе'),
                'checked' => $reportForm->getSearchInBase(),
                'name' => 'search-in-base'
            ]) ?>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>
