<?php

use app\widgets\Panel;

/**@var $this \yii\web\View */
/**@var $reportForm \app\modules\report\components\ReportFormFindCallCenter */

?>

<?= Panel::widget([
    'id' => 'report_filter_adcombo',
    'title' => Yii::t('common', 'Фильтр'),
    'content' => $this->render('_content', [
        'reportForm' => $reportForm,
    ]),
    'collapse' => true,
]) ?>
