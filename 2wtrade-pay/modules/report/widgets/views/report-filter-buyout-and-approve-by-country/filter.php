<?php
use app\widgets\Panel;

/** @var yii\web\View $this */
/** @var app\modules\report\components\ReportFormBuyoutAndApproveByCountry $reportForm */
?>

<?= Panel::widget([
    'id' => 'report-filter-buyout-and-approve-by-country',
    'title' => Yii::t('common', 'Фильтр'),
    'content' => $this->render('_content', [
        'reportForm' => $reportForm,
    ]),
    'collapse' => true,
]) ?>
