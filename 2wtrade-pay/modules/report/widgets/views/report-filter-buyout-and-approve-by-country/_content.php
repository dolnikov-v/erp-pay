<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/** @var app\modules\report\components\ReportFormBuyoutAndApproveByCountry $reportForm */
?>

<?php
$form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    'enableClientValidation' => false,
]);
?>

<div class="row">
    <div class="col-lg-9">
        <?= $reportForm->getDateFilter()->restore($form) ?>
    </div>
    <div class="col-lg-3">
        <?= $reportForm->getCountrySelectFilter()->restore($form) ?>
    </div>
    <?php if ($reportForm->groupLabel == 'delivery_ids'):?>
        <?= yii\helpers\Html::hiddenInput("s[groupLabel]", $reportForm->groupLabel);?>
    <?php endif;?>
</div>

<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')) ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
