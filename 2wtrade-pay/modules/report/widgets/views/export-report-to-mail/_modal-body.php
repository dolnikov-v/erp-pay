<?php
use app\components\widgets\ActiveForm;
use app\widgets\Button;
use app\widgets\ProgressBar;
use app\widgets\Select2;
use yii\helpers\Url;

/** @var string $url */
/** @var array $mails */
?>

<?php
$form = ActiveForm::begin([
    'enableClientValidation' => false,
    'action' => Url::toRoute('index'),
    'method' => 'post',
    'options' => [
        'data-url' => $url,
    ]
]);
?>

<div class="row-with-text-start">
    <div class="text-center">
        <?= Yii::t('common', 'Выберите из списка или введите самостоятельно список e-mail') ?>
    </div>
    <div class="text-center">
        <div class="col-xs-10 col-xs-offset-1">
            <?= Select2::widget([
                'id' => 'emails_for_report_selector',
                'items' => $mails,
                'multiple' => true,
                'length' => -1,
                'autoEllipsis' => true,
                'tags' => true,
            ]);
            ?>
        </div>
    </div>
</div>

<div class="row-with-text-stop" style="display: none;">
    <h5>
        <?= Yii::t('common', 'Отправка отчета на указанные e-mail, пожалуста, подождите...') ?>
        <span class="pull-right sender-percent">0%</span>
    </h5>
    <?= ProgressBar::widget() ?>
</div>

<div class="clearfix col-xs-12" style="height: 20px;"></div>
<div class="padding-top-20">
    <div class="row-with-btn-start text-right">
        <?= Button::widget([
            'id' => 'start_export_report_to_mail',
            'label' => Yii::t('common', 'Отправить'),
            'style' => Button::STYLE_SUCCESS,
            'size' => Button::SIZE_SMALL,
        ]) ?>

        <?= Button::widget([
            'label' => Yii::t('common', 'Закрыть'),
            'size' => Button::SIZE_SMALL,
            'attributes' => [
                'data-dismiss' => 'modal',
            ],
        ]) ?>
    </div>

    <div class="row-with-btn-stop" style="display: none;">
        <?= Button::widget([
            'id' => 'stop_export_report_to_mail',
            'label' => Yii::t('common', 'Остановить'),
            'style' => Button::STYLE_DANGER . ' btn-block',
            'size' => Button::SIZE_SMALL,
        ]) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
