<?php
use app\widgets\Modal;

/** @var string $url */
/** @var array $mails */
?>

<?= Modal::widget([
    'id' => 'modal_report_sent_to_mail',
    'title' => Yii::t('common', 'Отправить отчет на Email'),
    'body' => $this->render('_modal-body', [
        'url' => $url,
        'mails' => $mails,
    ]),
]) ?>
