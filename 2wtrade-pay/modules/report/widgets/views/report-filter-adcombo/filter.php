<?php
use app\widgets\Panel;

/** @var yii\web\View $this */
/** @var app\modules\report\components\ReportFormAdcombo $reportForm */
?>

<?= Panel::widget([
    'id' => 'report_filter_adcombo',
    'title' => Yii::t('common', 'Фильтр'),
    'content' => $this->render('_content', [
        'reportForm' => $reportForm,
    ]),
    'collapse' => true,
]) ?>
