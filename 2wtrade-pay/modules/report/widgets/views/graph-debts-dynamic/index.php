<?php

use miloschuman\highcharts\Highcharts;

/**
 * @var array $dates
 * @var array $seriesAmount
 */
?>

<?= Highcharts::widget([
    'options' => [
        'title' => ['text' => ' '],
        'credits' => [
            'enabled' => false,
        ],
        'xAxis' => [
            'categories' => $dates,
        ],
        'yAxis' => [
            'title' => ['text' => Yii::t('common', 'Сумма, USD')],
        ],
        'series' => $seriesAmount,
        'plotOptions' => [
            'series' => [
                'connectNulls' => true
            ]
        ]
    ],
]);
?>