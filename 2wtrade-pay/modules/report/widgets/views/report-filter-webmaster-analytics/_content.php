<?php

use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/** @var app\modules\report\components\ReportFormWebmasterAnalytics $reportForm */
?>

<?php
$form = ActiveForm::begin([
    'action' => ['analytics'],
    'method' => 'get',
    'enableClientValidation' => false,
]);
?>

<div class="row">
    <div class="col-lg-9">
        <?= $reportForm->getDateFilter()->restore($form) ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-3">
        <?= $reportForm->getCountrySelectFilter()->restore($form) ?>
    </div>
    <div class="col-lg-3">
        <?= $reportForm->getCallCenterFilter()->restore($form) ?>
    </div>
    <div class="col-lg-3">
        <?= $reportForm->getProductFilter()->restore($form) ?>
    </div>
    <div class="col-lg-3">
        <?= $reportForm->getWebmasterFilter()->restore($form) ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')) ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('analytics')) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
