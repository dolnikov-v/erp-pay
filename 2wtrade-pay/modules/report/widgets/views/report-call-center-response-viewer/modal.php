<?php
use app\widgets\Modal;

/** @var \yii\web\View $this */
?>

<?= Modal::widget([
    'id' => 'modal_report_call_center_response_viewer',
    'title' => Yii::t('common', 'Просмотр ответа от КЦ'),
    'body' => $this->render('_modal-body')
]) ?>
