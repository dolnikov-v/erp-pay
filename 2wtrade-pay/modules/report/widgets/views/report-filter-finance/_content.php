<?php
use app\components\widgets\ActiveForm;
use app\modules\report\widgets\AdditionalGroupFinance;
use yii\helpers\Url;

/** @var app\modules\report\components\ReportFormFinance $reportForm */
?>

<?php
$form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    'enableClientValidation' => false,
]);

$additionalGroupFinance = AdditionalGroupFinance::begin([
    'form' => $form,
    'model' => $reportForm,
]);
?>

<div class="row">
    <div class="col-lg-9">
        <?= $reportForm->getDateFilter()->restore($form) ?>
    </div>
    <div class="col-lg-3">
        <?= $reportForm->restoreGroupBy($form) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-3">
        <?= $reportForm->getProductFilter()->restore($form) ?>
    </div>
    <div class="col-lg-3">
        <?= $reportForm->getCallCenterFilter()->restore($form) ?>
    </div>
    <div class="col-lg-3">
        <?= $reportForm->getOrderLogisticListFilter()->restore($form) ?>
    </div>
    <div class="col-lg-3">
        <?= $reportForm->getPackageFilter()->restore($form) ?>
    </div>
</div>
<div class="row">
    <?= $reportForm->getCountryDeliveryPartnerSelectMultipleFilter()->restore($form) ?>
</div>

<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')) ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')) ?>

        <div class="pull-right">
            <?= $additionalGroupFinance->renderButton() ?>
        </div>
    </div>
</div>

<?= $additionalGroupFinance->renderModal() ?>

<?php AdditionalGroupFinance::end(); ?>
<?php ActiveForm::end(); ?>
