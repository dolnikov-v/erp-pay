<?php
use app\helpers\WbIcon;
use app\widgets\ButtonLink;
use yii\helpers\Html;

/** @var string $modalId */
?>

<?= ButtonLink::widget([
    'url' => '#' . $modalId,
    'label' => Yii::t('common', 'Дополнительные параметры'),
    'icon' => Html::tag('i', '', ['class' => 'icon ' . WbIcon::SETTINGS]),
    'size' => ButtonLink::SIZE_SMALL,
    'attributes' => [
        'data-toggle' => 'modal',
        'data-id' => $modalId,
    ],
]) ?>
