<?php
/** @var \app\components\widgets\ActiveForm $form */
/** @var \app\modules\report\components\ReportForm $model */
?>

<div class="row margin-bottom-20">
    <div class="col-lg-6">
        <?= $model->getCountryFilter()->restore($form) ?>
    </div>
</div>
