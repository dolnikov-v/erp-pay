<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/** @var app\modules\report\components\ReportFormLeadByCountry $reportForm */
?>

<?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>

<div class="row">
    <?= $reportForm->getDailyFilter()->restore($form) ?>
    <div class="col-lg-3">
        <?= $reportForm->getCuratorUserFilter()->restore($form) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-3">
        <?= $reportForm->getProductSelectFilter()->restore($form) ?>
    </div>
    <div class="col-lg-3">
        <?= $reportForm->getCountrySelectFilter()->restore($form) ?>
    </div>
    <div class="col-lg-3">
        <?= $reportForm->getTypeDatePartFilter()->restore($form) ?>
    </div>
    <div class="col-lg-3">
        <?= $reportForm->getTypeOrderSourceFilter()->restore($form) ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')) ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>