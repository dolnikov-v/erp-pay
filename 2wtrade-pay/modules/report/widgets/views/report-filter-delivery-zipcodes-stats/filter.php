<?php
use app\widgets\Panel;

/** @var yii\web\View $this */
/** @var app\modules\report\components\ReportFormDeliveryZipcodesStats $reportForm */
?>

<?= Panel::widget([
    'id' => 'report_filter_delivery_zipcodes_stats',
    'title' => Yii::t('common', 'Фильтр'),
    'content' => $this->render('_content', [
        'reportForm' => $reportForm,
    ]),
    'collapse' => true,
]) ?>
