<?php
use app\components\widgets\ActiveForm;
use yii\helpers\Url;

/** @var app\modules\report\components\ReportFormReceivable $reportForm */
?>

<?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>

<div class="row">
    <?= $reportForm->getReceivableFilter()->restore($form) ?>
    <div class="col-md-7">
        <?= $reportForm->getReceivableDebtFilter()->restore($form) ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')) ?>
        <?= $form->link(Yii::t('common', 'Сбросить параметры'), Url::toRoute('index')) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
