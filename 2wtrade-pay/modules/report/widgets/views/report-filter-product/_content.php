<?php
use app\components\widgets\ActiveForm;
use app\modules\report\widgets\AdditionalGroup;
use yii\helpers\Url;

/** @var app\modules\report\components\ReportFormProduct $reportForm */
?>

<?php
$form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    'enableClientValidation' => false,
]);

$additionalGroup = AdditionalGroup::begin([
    'form' => $form,
    'model' => $reportForm,
]);
?>

<div class="row">
    <div class="col-lg-9">
        <?= $reportForm->getDateFilter()->restore($form) ?>
    </div>
    <div class="col-lg-3">
        <?= $reportForm->restoreGroupBy($form) ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-3">
        <?= $reportForm->getProductFilter()->restore($form) ?>
    </div>
    <div class="col-lg-3">
        <?= $reportForm->getCallCenterFilter()->restore($form) ?>
    </div>
    <div class="col-lg-3">
        <?= $reportForm->getDeliveryFilter()->restore($form) ?>
    </div>
    <div class="col-lg-3">
        <?= $reportForm->getOrderStatusFilter()->restore($form) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-3">
        <?= $reportForm->getDeliveryPartnerFilter()->restore($form) ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')) ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')) ?>

        <div class="pull-right">
            <?= $additionalGroup->renderButton() ?>
        </div>
    </div>
</div>

<?= $additionalGroup->renderModal() ?>

<?php AdditionalGroup::end(); ?>
<?php ActiveForm::end(); ?>
