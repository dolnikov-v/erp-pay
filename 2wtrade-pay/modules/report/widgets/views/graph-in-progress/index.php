<?php
use miloschuman\highcharts\Highcharts;
/**
 * @var array $dates
 * @var array $seriesApprove
 * @var array $seriesPercent
 */
?>

<br>
<br>

<?= Highcharts::widget([
    'options' => [
        'title' => ['text' => ' '],
        'plotOptions' => [
                'line' => [
                    'dataLabels' => [
                         'enabled' => true
                    ]
                ]
        ],
        'credits' => [
            'enabled'=> false
        ],
        'xAxis' => [
            'categories' => $dates,
        ],
        'yAxis' => [
            'title' => ['text' => Yii::t('common', 'Количество заказов "В процессе"')],
        ],
        'series' => $inProgress,
    ],
]); ?>

<br>
<br>

