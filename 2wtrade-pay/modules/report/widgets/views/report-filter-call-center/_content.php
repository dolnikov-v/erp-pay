<?php
use app\components\widgets\ActiveForm;
use app\modules\report\widgets\AdditionalGroupCallCenter;
use yii\helpers\Url;

/** @var app\modules\report\components\ReportFormCallCenter $reportForm */
?>

<?php
$form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    'enableClientValidation' => false,
]);

$additionalGroup = AdditionalGroupCallCenter::begin([
    'form' => $form,
    'model' => $reportForm,
]);
?>

<div class="row">
    <div class="col-lg-9">
        <?= $reportForm->getDateFilter()->restore($form) ?>
    </div>
    <div class="col-lg-3">
        <?= $reportForm->restoreGroupBy($form) ?>
    </div>
    <div class="col-lg-3">
        <?= $reportForm->getProductCategoryFilter()->restore($form) ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-3">
        <?= $reportForm->getSourceFilter()->restore($form) ?>
    </div>
    <div class="col-lg-3">
        <?= $reportForm->getLeadProductFilter()->restore($form) ?>
    </div>
    <div class="col-lg-3">
        <?= $reportForm->getCallCenterFilter()->restore($form) ?>
    </div>
    <div class="col-lg-3">
        <?= $reportForm->getCountrySelectFilter()->restore($form) ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')) ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')) ?>

        <div class="pull-right">
            <?= $additionalGroup->renderButton() ?>
        </div>
    </div>
</div>

<?= $additionalGroup->renderModal() ?>

<?php AdditionalGroupCallCenter::end(); ?>
<?php ActiveForm::end(); ?>
