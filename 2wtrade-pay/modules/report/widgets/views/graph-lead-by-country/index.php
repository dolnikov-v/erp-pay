<?php
use miloschuman\highcharts\Highcharts;
/**
 * @var array $dates
 * @var array $series
 */
?>

<br>
<br>

<?= Highcharts::widget([
    'options' => [
        'title' => ['text' => ' '],
        'credits' => [
            'enabled'=> false
        ],
        'xAxis' => [
            'categories' => $dates,
        ],
        'yAxis' => [
            'title' => ['text' => Yii::t('common', 'Количество лидов')],
        ],
        'series' => $series,
    ],
]); ?>
