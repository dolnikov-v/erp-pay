<?php
namespace app\modules\report\widgets;

use yii\helpers\ArrayHelper;

/**
 * Class GraphAverageCheckByCountry
 * @package app\modules\report\widgets
 */
class GraphAverageCheckByCountry extends GraphByCountriesIntervals
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('graph-average-check-by-country/index', [
            'series' => $this->generateSeries(),
            'dates' => $this->formatDates($this->dates),
        ]);
    }

    /**
     * @return array
     */
    private function generateSeries()
    {
        $data = $this->sumDataParams(['sr_cek_kc']);
        foreach ($data as &$country) {
            foreach ($country as &$date) {
                $date['sr_cek_kc'] = round($date['sr_cek_kc'], 2);
            } unset($date);
        } unset($country);

        $series = [];
        foreach ($this->countries as $country) {
            $series[] = [
                'name' => $country,
                'data' => ArrayHelper::getColumn($data[$country], 'sr_cek_kc', false),
            ];
        }

        return $series;
    }
}
