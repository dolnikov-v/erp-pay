<?php
namespace app\modules\report\components;

use app\modules\callcenter\models\CallCenter;
use app\modules\callcenter\models\CallCenterStandartOperator;
use app\modules\order\models\Order;
use app\models\Country;
use app\models\CurrencyRate;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\order\models\OrderStatus;
use app\models\User;
use yii\data\ActiveDataProvider;
use app\modules\report\components\filters\DailyFilterStandartOperators;
use yii\db\Expression;
use Yii;

/**
 * Class ReportFormStandartSales
 * @package app\modules\report\components\
 */
class ReportFormStandartSales extends ReportForm
{
    /**
     * @var DailyFilterStandartOperators
     */
    protected $DailyFilterStandartOperators;

    /** @var array */
    public $mapStatus = [];

    /**
     * @return array
     */
    public function getMapStatuses()
    {
        return  [
            Yii::t('common', 'Подтверждено') => OrderStatus::getApproveList(),
            Yii::t('common', 'Выкуп') => OrderStatus::getBuyoutList(),
            Yii::t('common', 'Все') => OrderStatus::getAllList(),
        ];
    }

    /**
     * Получить список id стран, КЦ у которых имеют эталонных операторов
     * @return []
     */
    public function getNotemptyCCOperators()
    {
        $query = CallCenterStandartOperator::find()
            ->select([
                CallCenter::tableName() . '.country_id'
            ])
            ->leftJoin(CallCenter::tableName(), [CallCenter::tableName() . '.id' => new Expression(CallCenterStandartOperator::tableName() . '.call_center_id')])
            ->asArray()
            ->groupBy([CallCenter::tableName() . '.country_id'])
            ->all();

        $notEmptyCCcountry = [];

        foreach ($query as $v) {
            $notEmptyCCcountry[] = $v['country_id'];
        }

        return $notEmptyCCcountry;
    }

    /**
     * Получить список всех эталонных операторов, закреплённыз за всеми КЦ
     * @return int|mixed
     */
    public function getListStandartOperatorsByCC()
    {
        $listStandartOperators = CallCenter::find()
            ->select([
                'standart_operators' => new Expression("GROUP_CONCAT(DISTINCT " . CallCenterStandartOperator::tableName() . ".operator_id SEPARATOR ', ')")
            ])
            ->leftJoin(CallCenterStandartOperator::tableName(), [CallCenterStandartOperator::tableName() . '.call_center_id' => new Expression(CallCenter::tableName() . '.id')])
            ->asArray()
            ->all();

        return isset($listStandartOperators[0]) && !empty($listStandartOperators[0]['standart_operators']) ? $listStandartOperators[0]['standart_operators'] : null;
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function apply($params)
    {
        $countries_ids = array_keys(User::getAllowCountries());
        $notEmptyCCCountry = $this->getNotemptyCCOperators();
        $this->mapStatus = $this->getMapStatuses();

        //Найти общие страны между странами пользователя и странами. КЦ которых, имею эталонных операторов
        if (!empty($notEmptyCCCountry)) {
            $countries_ids = array_intersect($countries_ids, $notEmptyCCCountry);
        }

        $listStandartOperastors = !empty($this->getListStandartOperatorsByCC()) ? $this->getListStandartOperatorsByCC() : 0;

        $this->query = Order::find()
            ->select([
                'id' => Country::tableName() . '.id',
                'country_name' => Country::tableName() . '.name',
                'standart_approve' => new Expression(
                    "(
                        COUNT(" . CallCenterRequest::tableName() . ".last_foreign_operator in(" . $listStandartOperastors . ") 
                        and " . Order::tableName() . ".status_id in (" . implode(",", $this->mapStatus[Yii::t('common', 'Подтверждено')]) . ") OR NULL) * 100/
                        COUNT(" . CallCenterRequest::tableName() . ".last_foreign_operator in(" . $listStandartOperastors . ") 
                        and " . Order::tableName() . ".status_id IN(" . implode(",", $this->mapStatus[Yii::t('common', 'Все')]) . ") OR NULL)
                     )"
                ),
                'standart_vikup' => new Expression(
                    "(
                        COUNT(" . CallCenterRequest::tableName() . ".last_foreign_operator in(" . $listStandartOperastors . ") 
                        and " . Order::tableName() . ".status_id in (" . implode(",", $this->mapStatus[Yii::t('common', 'Выкуп')]) . ") OR NULL) * 100/ 
                        COUNT(" . CallCenterRequest::tableName() . ".last_foreign_operator in(" . $listStandartOperastors . ") 
                        and " . Order::tableName() . ".status_id IN(" . implode(",", $this->mapStatus[Yii::t('common', 'Подтверждено')]) . ") OR NULL)
                      )"
                ),
                'all_approve' => new Expression(
                    "(
                        COUNT(" . Order::tableName() . ".status_id in (" . implode(",", $this->mapStatus[Yii::t('common', 'Подтверждено')]) . ") OR NULL)*100/ 
                        COUNT(" . Order::tableName() . ".status_id IN(" . implode(",", $this->mapStatus[Yii::t('common', 'Все')]) . ") OR NULL)
                     )"
                ),
                'all_vikup' => new Expression(
                    "(
                        COUNT(" . Order::tableName() . ".status_id in (" . implode(",", $this->mapStatus[Yii::t('common', 'Выкуп')]) . ") OR NULL)*100/
                        COUNT(" . Order::tableName() . ".status_id IN(" . implode(",", $this->mapStatus[Yii::t('common', 'Подтверждено')]) . ") OR NULL)
                      )"
                ),
                'all_sr_cek_kc' => new Expression(
                    "AVG(
                              CASE WHEN " . Order::tableName() . ".status_id IN (" . implode(",", $this->mapStatus[Yii::t('common', 'Все')]) . ") 
					          THEN (" . Order::tableName() . ".price_total  + " . Order::tableName() . ".delivery  ) /
					          " . CurrencyRate::tableName() . ".rate
					          ELSE NULL 
					          END
					 )"
                ),
                'standart_sr_cek_kc' => new Expression(
                    "AVG(
                              CASE WHEN " . CallCenterRequest::tableName() . ".last_foreign_operator in(" . $listStandartOperastors . ") and 
                              " . Order::tableName() . ".status_id IN (" . implode(",", $this->mapStatus[Yii::t('common', 'Все')]) . ") 
					          THEN (" . Order::tableName() . ".price_total  + " . Order::tableName() . ".delivery  ) /
					          " . CurrencyRate::tableName() . ".rate 
					          ELSE NULL 
					          END
					 )"
                ),
            ])
            ->leftJoin(Country::tableName(), [Country::tableName() . '.id' => new Expression(Order::tableName() . '.country_id')])
            ->leftJoin(CallCenterRequest::tableName(), [CallCenterRequest::tableName() . '.order_id' => new Expression(Order::tableName() . '.id')])
            ->leftJoin(CurrencyRate::tableName(), [Country::tableName() . '.currency_id' => new Expression(CurrencyRate::tableName() . '.currency_id')])
            ->where(['in', Order::tableName() . '.country_id', $countries_ids])
            ->groupBy(Order::tableName() . '.country_id')
            ->asArray();

        $this->applyFilters($params);
        $this->query->all();


        $dataProvider = new ActiveDataProvider([
            'query' => $this->query,
            'pagination' => false
        ]);


        return $dataProvider;
    }

    /**
     * @return DailyFilterStandartOperators
     */
    public function getDailyFilterStandartOperators()
    {
        if (is_null($this->DailyFilterStandartOperators)) {
            $this->DailyFilterStandartOperators = new DailyFilterStandartOperators();
        }

        return $this->DailyFilterStandartOperators;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $output = $this->getDailyFilterStandartOperators()->restore($form);
        return $output;
    }

    /**
     * @param array $params
     * @return $this
     */
    protected function applyFilters($params)
    {
        $this->getDailyFilterStandartOperators()->apply($this->query, $params);

        return $this;
    }
}
