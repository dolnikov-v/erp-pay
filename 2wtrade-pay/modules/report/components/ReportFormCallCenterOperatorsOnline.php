<?php

namespace app\modules\report\components;

use app\models\Country;
use app\modules\callcenter\models\CallCenter;
use app\modules\callcenter\models\CallCenterUser;
use app\modules\callcenter\models\CallCenterWorkTime;
use app\modules\report\components\filters\DateWorkFilter;
use app\modules\report\components\filters\TypeDatePartFilter;
use app\modules\report\components\filters\OfficeFilter;
use app\modules\report\components\filters\CallCenterFilter;
use app\modules\report\extensions\Query;
use app\modules\salary\models\Office;
use yii\data\ArrayDataProvider;
use yii\db\Expression;

/**
 * Class ReportFormCallCenterOperatorsOnline
 * @package app\modules\report\components
 */
class ReportFormCallCenterOperatorsOnline extends ReportForm
{
    /**
     * @var DateWorkFilter
     */
    protected $dailyFilter;

    /**
     * @var TypeDatePartFilter
     */
    protected $typeDatePartFilter;

    /**
     * @var OfficeFilter
     */
    protected $officeFilter;

    /**
     * @var CallCenterFilter
     */
    protected $callCenterFilter;

    /**
     * @param array $params
     * @return ArrayDataProvider
     */
    public function apply($params)
    {
        $this->load($params);

        $this->query = new Query();
        $this->query->from([CallCenterUser::tableName()]);
        $this->query->leftJoin(CallCenter::tableName(), CallCenter::tableName() . '.id=' . CallCenterUser::tableName() . '.callcenter_id');
        $this->query->leftJoin(Country::tableName(), Country::tableName() . '.id=' . CallCenter::tableName() . '.country_id');
        $this->query->leftJoin(CallCenterWorkTime::tableName(), CallCenterWorkTime::tableName() . '.call_center_user_id=' . CallCenterUser::tableName() . '.id');
        $this->query->andWhere(['is not', Country::tableName() . '.id', null]);
        $this->query->andWhere(['is not', CallCenterUser::tableName() . '.user_login', null]);
        $this->query->andWhere([
            'or',
            [CallCenterUser::tableName().'.user_role' => null],
            [CallCenterUser::tableName().'.user_role' => CallCenterUser::USER_ROLE_OPER],
        ]);
        $this->applyFilters($params);
        $this->buildSelect();
        $this->query->orderBy(
            [
                'country_name' => SORT_ASC,
                'groupbyname' => SORT_ASC
            ]);

        $this->query->groupBy(['country_id', 'groupbyname']);

        $dataArray = $this->query->all();

        $dataProvider = new ArrayDataProvider([
            'allModels' => $dataArray,
            'pagination' => false,
        ]);

        return $dataProvider;
    }

    /**
     * @inheritdoc
     */
    protected function buildSelect()
    {

        $types = [
            'year' => "DATE_FORMAT(" . CallCenterWorkTime::tableName() . ".date, \"%Y\")",
            'month' => "DATE_FORMAT(" . CallCenterWorkTime::tableName() . ".date, \"%m\")",
            'week' => "DATE_FORMAT(" . CallCenterWorkTime::tableName() . ".date, \"%u\")",
            'day' => "DATE_FORMAT(" . CallCenterWorkTime::tableName() . ".date, \"%d\")",
        ];

        $typeDatePart = $this->getTypeDatePartFilter()->type_date_part;

        $this->query->select(
            array_merge(
                $types,
                [
                    'country_name' => Country::tableName() . '.name',
                    'country_id' => Country::tableName() . '.id',
                    'listOperators' => new Expression('GROUP_CONCAT(distinct ' . CallCenterUser::tableName() . '.user_login SEPARATOR ", ")'),
                    'countOperators' => new Expression('count( distinct ' . CallCenterWorkTime::tableName() . '.call_center_user_id)'),
                    'groupbyname' => $typeDatePart == 'day' ? CallCenterWorkTime::tableName() . '.date' : $types[$this->getTypeDatePartFilter()->type_date_part],
                ]));

        return $this;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $content = '';
        return $content;
    }

    /**
     * @param array $params
     * @return ReportForm
     */
    public function applyFilters($params)
    {
        $dateParams = $this->getDateWorkFilter()->apply($this->query, $params);
        foreach ($dateParams as $d) {
            $this->query->andFilterWhere($d);
        }
        $this->getTypeDatePartFilter()->apply($this->query, $params);
        $this->getOfficeFilter()->apply($this->query, $params, false);
        $this->getCallCenterFilter()->apply($this->query, $params);
        return $this;
    }

    public function getDateWorkFilter()
    {
        if (is_null($this->dailyFilter)) {
            $this->dailyFilter = new DateWorkFilter();
        }
        return $this->dailyFilter;
    }

    /**
     * @return TypeDatePartFilter
     */
    public function getTypeDatePartFilter()
    {
        if (is_null($this->typeDatePartFilter)) {
            $this->typeDatePartFilter = new TypeDatePartFilter();
        }
        return $this->typeDatePartFilter;
    }

    public function getOfficeFilter()
    {
        if (is_null($this->officeFilter)) {
            $this->officeFilter = new OfficeFilter(
                [
                    'onlyType' => Office::TYPE_CALL_CENTER
                ]
            );
        }
        return $this->officeFilter;
    }

    public function getCallCenterFilter()
    {
        if (is_null($this->callCenterFilter)) {
            $this->callCenterFilter = new CallCenterFilter([
                'toCallCenterUser' => true,
                'multiple' => true,
                'label' => 'Направление',
                'officeId' => $this->officeFilter->office
            ]);
        }
        return $this->callCenterFilter;
    }
}
