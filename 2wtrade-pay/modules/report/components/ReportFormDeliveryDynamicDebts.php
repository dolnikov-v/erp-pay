<?php

namespace app\modules\report\components;

use app\modules\report\components\filters\DebtsMonthFilter;
use Yii;

/**
 * Class ReportFormDeliveryDynamicDebts
 * @package app\modules\report\components
 */
class ReportFormDeliveryDynamicDebts extends ReportFormDeliveryDebts
{
    /**
     * @return DebtsMonthFilter
     */
    public function getMonthFilter()
    {
        if (is_null($this->monthFilter)) {
            $this->monthFilter = new DebtsMonthFilter([
                'from' => Yii::$app->formatter->asMonth(strtotime(' - 1month', time())),
                'to' => Yii::$app->formatter->asMonth(time()),
            ]);
        }
        return $this->monthFilter;
    }
}
