<?php

namespace app\modules\report\components;

use app\models\Country;
use app\modules\access\widgets\Delivery;
use app\modules\callcenter\models\CallCenterCheckRequest;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use app\modules\report\components\filters\CountryDeliverySelectMultipleFilter;
use app\modules\report\components\filters\DateFilter;
use app\modules\report\controllers\CallCenterCheckRequestController;
use app\modules\report\extensions\DataProvider;
use app\modules\report\extensions\Query;

use Yii;
use yii\db\Expression;

/**
 * Class ReportFormCallCenterCheckRequest
 * @package app\modules\report\components\
 */
class ReportFormCallCenterCheckRequest extends ReportForm
{

    public $groupBy = self::GROUP_BY_CHECK_SENT_AT;

    protected $mapStatuses = [];

    protected $loadStatuses = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @param array $params
     * @return DataProvider
     */
    public function applyReject($params)
    {
        $this->loadStatuses = [
            OrderStatus::STATUS_DELIVERY_DENIAL,
            OrderStatus::STATUS_DELIVERY_RETURNED,
            OrderStatus::STATUS_DELIVERY_REJECTED,
            OrderStatus::STATUS_LOGISTICS_REJECTED,
            OrderStatus::STATUS_DELIVERY_REFUND,
            OrderStatus::STATUS_LOGISTICS_ACCEPTED,
            OrderStatus::STATUS_DELIVERY_LOST,
        ];
        return $this->apply($params);
    }

    /**
     * @param array $params
     * @return DataProvider
     */
    public function applyHold($params)
    {
        $this->loadStatuses = [
            OrderStatus::STATUS_DELIVERY_ACCEPTED,
            OrderStatus::STATUS_DELIVERY_LEFT_WAREHOUSE,
            OrderStatus::STATUS_DELIVERY,
            OrderStatus::STATUS_DELIVERY_LEFT_OUR_WAREHOUSE,
            OrderStatus::STATUS_DELIVERY_REDELIVERY,
            OrderStatus::STATUS_DELIVERY_LEFT_THEIR_WAREHOUSE,
            OrderStatus::STATUS_DELIVERY_DELAYED,
            OrderStatus::STATUS_LOG_SENT,
            OrderStatus::STATUS_LOG_RECEIVED,
        ];
        return $this->apply($params);
    }

    /**
     * @param array $params
     * @return DataProvider
     */
    public function applyBuyout($params)
    {
        $this->loadStatuses = [
            OrderStatus::STATUS_DELIVERY_BUYOUT
        ];
        return $this->apply($params);
    }

    /**
     * @param array $params
     * @return DataProvider
     */
    public function apply($params)
    {
        $this->getDateFilter()->type = DateFilter::TYPE_CHECK_SENT_AT;

        $this->groupByCollection = [
            self::GROUP_BY_CHECK_SENT_AT => Yii::t('common', 'Дата отправки на обзвон'),
            self::GROUP_BY_TS_SPAWN => Yii::t('common', 'Дата создания у партнера (лид)'),
            self::GROUP_BY_DELIVERY_SENT_AT_NOT_EMPTY => Yii::t('common', 'Дата передачи заказа в курьерку'),
        ];

        $this->load($params);

        $this->query = new Query();
        $this->query->from([Order::tableName()]);
        $this->query->leftJoin(Country::tableName(), Country::tableName() . '.id=' . Order::tableName() . '.country_id');
        $this->query->leftJoin(CallCenterCheckRequest::tableName(),
            CallCenterCheckRequest::tableName() . '.order_id=' . Order::tableName() . '.id and ' .
            CallCenterCheckRequest::tableName() . '.id = (SELECT MAX(id) FROM ' . CallCenterCheckRequest::tableName() . ' WHERE order_id = ' . Order::tableName() . '.id)');
        $this->applyFilters($params);
        $this->buildSelect();
        $this->query->andWhere([
            CallCenterCheckRequest::tableName() . '.type' => CallCenterCheckRequest::TYPE_CHECKUNDELIVERY
        ]);
        $this->query->andWhere([Order::tableName() . '.status_id' => $this->loadStatuses]);
        $this->query->andWhere(['NOT IN', Order::tableName() . '.source_id', Yii::$app->user->rejectedSources]);

        $dataProvider = new DataProvider([
            'form' => $this,
            'query' => $this->query,
            'statuses' => $this->loadStatuses,
        ]);

        return $dataProvider;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $output = '';

        return $output;
    }

    /**
     * @inheritdoc
     */
    protected function buildSelect()
    {
        $this->query->addSelect(['country_id' => Order::tableName() . '.country_id']);
        $this->query->addSelect([Yii::t('common', 'Всего') => 'COUNT(' . Order::tableName() . '.id)']);

        $this->query->addInConditionCount(CallCenterCheckRequest::tableName() . '.foreign_information', [
            CallCenterCheckRequest::FOREIGN_SUB_STATUS_REJECT_STILL_WAITING
        ], 'waiting');

        $this->query->addInConditionCount(callCenterCheckRequest::tableName() . '.foreign_information', [
            CallCenterCheckRequest::FOREIGN_SUB_STATUS_ALREADY_RECEIVED
        ], 'received');

        $this->query->addInConditionCount(CallCenterCheckRequest::tableName() . '.foreign_information', [
            CallCenterCheckRequest::FOREIGN_SUB_STATUS_REJECT_NO_LONGER_NEEDED,
        ], 'notneeded');

        $this->query->addInConditionCount(CallCenterCheckRequest::tableName() . '.foreign_information', [
            CallCenterCheckRequest::FOREIGN_SUB_STATUS_REJECT_DELIVERY_NOT_CONTACTED_CUSTOMER
        ], 'notcontacted');

        $this->query->addInConditionCount(CallCenterCheckRequest::tableName() . '.foreign_information', [
            CallCenterCheckRequest::FOREIGN_SUB_STATUS_REJECT_AUTO,
        ], 'auto');

        $this->query->addNotInConditionCount(CallCenterCheckRequest::tableName() . '.foreign_information', [
            CallCenterCheckRequest::FOREIGN_SUB_STATUS_REJECT_STILL_WAITING,
            CallCenterCheckRequest::FOREIGN_SUB_STATUS_ALREADY_RECEIVED,
            CallCenterCheckRequest::FOREIGN_SUB_STATUS_REJECT_NO_LONGER_NEEDED,
            CallCenterCheckRequest::FOREIGN_SUB_STATUS_REJECT_DELIVERY_NOT_CONTACTED_CUSTOMER,
            CallCenterCheckRequest::FOREIGN_SUB_STATUS_REJECT_AUTO
        ], 'notdone');

        return $this;
    }


    /**
     * @return DateFilter
     */
    public function getDateFilter()
    {
        if (is_null($this->dateFilter)) {
            $this->dateFilter = new DateFilter();
            $this->dateFilter->types = [
                DateFilter::TYPE_CHECK_SENT_AT => Yii::t('common', 'Дата отправки на обзвон'),
                DateFilter::TYPE_TS_SPAWN => Yii::t('common', 'Дата создания у партнера (лид)'),
                DateFilter::TYPE_DELIVERY_SENT_AT_NOT_EMPTY => Yii::t('common', 'Дата передачи заказа в курьерку'),
            ];
            $this->dateFilter->possibleTypes = [
                DateFilter::TYPE_CHECK_SENT_AT,
                DateFilter::TYPE_TS_SPAWN,
                DateFilter::TYPE_DELIVERY_SENT_AT_NOT_EMPTY
            ];
        }

        return $this->dateFilter;
    }


    /**
     * @return CountryDeliverySelectMultipleFilter
     */
    public function getCountryDeliverySelectMultipleFilter()
    {
        if (is_null($this->countryDeliverySelectMultipleFilter)) {
            $this->countryDeliverySelectMultipleFilter = new CountryDeliverySelectMultipleFilter();
            $this->countryDeliverySelectMultipleFilter->multipleCountrySelect = false;
            $this->countryDeliverySelectMultipleFilter->country_ids = [Yii::$app->user->country->id];
        }
        return $this->countryDeliverySelectMultipleFilter;
    }

    /**
     * @param array $params
     * @return ReportForm
     */
    protected function applyFilters($params)
    {
        $this->getDateFilter()->apply($this->query, $params);
        $this->getCountryDeliverySelectMultipleFilter()->apply($this->query, $params);
        return $this;
    }
}
