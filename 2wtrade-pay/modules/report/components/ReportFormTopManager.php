<?php
namespace app\modules\report\components;

use app\models\Country;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use yii\data\ArrayDataProvider;
use app\modules\report\extensions\Query;
use app\models\User;
use app\models\UserCountry;
use app\models\AuthAssignment;
use app\models\CurrencyRate;

/**
 * Class ReportFormTopManager
 * @package app\modules\report\components
 */
class ReportFormTopManager extends ReportForm
{
    /**
     * One month (31 days)
     */
    const MONTH = 2678400;

/**
 * @inheritdoc
 */
    public function init()
    {
        parent::init();
    }

/**
 * @var $params
 * @return ArrayDataProvider
 */
    public function apply($params)
    {
        $data = [];
        $n = 0;
        foreach ($this->getUserCountries() as $userCountries) {
            $countries = $userCountries['countries'];
            $userName = $userCountries['username'];
            $query = new Query();
            $query->from([Order::tableName()]);


            $toDollar = '/ ' . CurrencyRate::tableName() . '.rate';
            $query->leftJoin(Country::tableName(), Order::tableName() . '.country_id=' . Country::tableName() . '.id');
            $query->leftJoin(CurrencyRate::tableName(), Country::tableName() . '.currency_id = ' . CurrencyRate::tableName() . '.currency_id');


            $buyoutCase = $query->buildCaseCondition(
                '(' . Order::tableName() . '.price_total ' . $toDollar . ')',
                Order::tableName() . '.status_id',
                OrderStatus::getBuyoutList()
            );

            $query->where([">=",Order::tableName() . '.created_at', time() - self::MONTH]);
            $query->andWhere("country_id IN ($countries)");
            $query->addSumCondition($buyoutCase, 'income');

            $query->addCondition('COUNT(' . Order::tableName() .'.id)', 'allorders');
            $query->addInConditionCount(Order::tableName() . '.status_id', OrderStatus::getApproveList(), 'approvedorders');
            $queryData = $query->all();
            $data[$n] = $queryData[0];
            $approvedPercent = $data[$n]['allorders'] ? round($data[$n]['approvedorders']*100/$data[$n]['allorders'],2) : null;
            $data[$n]['userName'] = $userName;
            $data[$n]['approvePercent'] = $approvedPercent;
            $n++;
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'sort' => [
                'defaultOrder' => [
                    'income' => SORT_DESC
                ],
                'attributes' => [
                    'income', 'userName', 'approvePercent'
                ],
            ]
        ]);

        return $dataProvider;
    }

/**
 * @param \app\components\widgets\ActiveForm $form
 * @return string
 */
    public function restore($form)
    {
        $output = '';

        return $output;
    }

    /**
     * Возвращает список стран для каждого менеджера КЦ
     * @return array
     */
    public function getUserCountries()
    {
        $countryQuery = new Query();
        $countryQuery->from([UserCountry::tableName()]);
        $countryQuery->innerJoin(AuthAssignment::tableName(), UserCountry::tableName() . '.user_id = ' . AuthAssignment::tableName() . '.user_id');
        $countryQuery->innerJoin(User::tableName(), User::tableName() . '.id = ' . AuthAssignment::tableName() . '.user_id');
        $countryQuery->where(AuthAssignment::tableName() . ".item_name = 'callcenter.manager'" );
        $countryQuery->andWhere(User::tableName() . ".username != 'smykov'");
        $countryQuery->select(User::tableName() . '.username');
        $countryQuery->addSelect("GROUP_CONCAT(" . UserCountry::tableName() . ".country_id) AS countries");
        $countryQuery->groupBy(User::tableName() . '.username');
        return $countryQuery->all();
    }
}
