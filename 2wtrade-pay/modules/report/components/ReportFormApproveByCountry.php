<?php

namespace app\modules\report\components;

use app\models\Country;
use app\models\Product;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\order\models\Order;
use app\modules\order\models\OrderProduct;
use app\modules\order\models\OrderStatus;
use app\modules\report\components\filters\CountrySelectFilter;
use app\modules\report\components\filters\CuratorUserFilter;
use app\modules\report\components\filters\ProductSelectFilter;
use app\modules\report\components\filters\DailyFilterDefaultLastMonth;
use app\modules\report\components\filters\TypeDatePartFilter;
use app\modules\report\components\filters\DuplicateOrderFilter;
use app\modules\report\extensions\Query;
use Yii;
use yii\data\ArrayDataProvider;

/**
 * Class ReportFormApproveByCountry
 * @package app\modules\report\components
 */
class ReportFormApproveByCountry extends ReportForm
{
    /**
     * @var $dailyFilter
     */
    protected $dailyFilter;

    /**
     * @var CountrySelectFilter
     */
    protected $countrySelectFilter;

    /**
     * @var ProductSelectFilter
     */
    protected $productSelectFilter;

    /**
     * @var curatorUserFilter
     */
    protected $curatorUserFilter;

    /**
     * @var TypeDatePartFilter
     */
    protected $typeDatePartFilter;

    /**
     * @var DuplicateOrderFilter
     */
    protected $duplicateOrderFilter;

    /**
     * @var array Маппинг статусов
     */
    protected $mapStatuses = [];

    /**
     * @var []
     */
    protected $params;

    /**
     * @param array $params
     * @return ArrayDataProvider
     */
    public function apply($params)
    {
        $this->load($params);

        $subOquery = Order::find()
            ->select([
                Order::tableName() . '.status_id',
                Order::tableName() . '.id',
                Order::tableName() . '.country_id',
                OrderProduct::tableName() . '.order_id',
                'product_id' => OrderProduct::tableName() . '.product_id',
            ])
            ->leftJoin(OrderProduct::tableName(), OrderProduct::tableName() . '.order_id=' . Order::tableName() . '.id')
            ->leftJoin(CallCenterRequest::tableName(), CallCenterRequest::tableName() . '.order_id =' . OrderProduct::tableName() . '.order_id')
            ->where([">=", CallCenterRequest::tableName() . '.sent_at', strtotime($this->searchQuery['s']['from'])])
            ->andWhere(["<=", CallCenterRequest::tableName() . '.sent_at', strtotime($this->searchQuery['s']['to']) + 86399])
            ->groupBy([OrderProduct::tableName() . '.order_id']);

        if (isset($params['s']['product_ids']) && count($params['s']['product_ids']) > 0) {
            $subOquery->andFilterWhere(['in', OrderProduct::tableName() . '.product_id', $params['s']['product_ids']]);
        }
        $subOquery = $this->applyFiltersSubQuery($subOquery, $params);

        $this->query = new Query();
        $this->query->from(['order' => $subOquery]);
        $this->query->leftJoin(Country::tableName(), 'order.country_id=' . Country::tableName() . '.id');
        $this->query->leftJoin(CallCenterRequest::tableName(), CallCenterRequest::tableName() . '.order_id=' . 'order.id')
            ->where([">=", CallCenterRequest::tableName() . '.sent_at', strtotime($this->searchQuery['s']['from'])])
            ->andWhere(["<=", CallCenterRequest::tableName() . '.sent_at', strtotime($this->searchQuery['s']['to']) + 86399]);

        $this->params = $params;
        $this->buildSelect()->applyFilters($params);

        if (isset($this->params['s']['product_ids']) && count($this->params['s']['product_ids']) > 0) {
            $this->query->andFilterWhere(['in', 'order.product_id', $params['s']['product_ids']]);
            $this->query->leftJoin(Product::tableName(), Product::tableName() . '.id=' . 'order.product_id');
            $this->query->groupBy([Country::tableName() . '.id', 'order.product_id', 'groupbyname']);
        } else {
            $this->query->addGroupBy(['country_name', 'groupbyname']);
        }

        $dataArray = $this->query->all();

        $dataProvider = new ArrayDataProvider([
            'allModels' => $dataArray,
            'pagination' => false,
        ]);

        return $dataProvider;
    }

    /**
     * @inheritdoc
     */
    protected function buildSelect()
    {
        if (isset($this->params['s']['product_ids']) && count($this->params['s']['product_ids']) > 0) {
            $this->query->select([
                'country_name' => Country::tableName() . '.name',
                'product_name' => Product::tableName() . '.name',
                'yearcreatedat' => "DATE_FORMAT(FROM_UNIXTIME(" . CallCenterRequest::tableName() . ".sent_at), \"%Y\")",
                'monthcreatedat' => "DATE_FORMAT(FROM_UNIXTIME(" . CallCenterRequest::tableName() . ".sent_at), \"%m\")",
                'weekcreatedat' => "DATE_FORMAT(FROM_UNIXTIME(" . CallCenterRequest::tableName() . ".sent_at), \"%u\")",
                'daycreatedat' => "DATE_FORMAT(FROM_UNIXTIME(" . CallCenterRequest::tableName() . ".sent_at), \"%d\")",
                'groupbyname' => "DATE_FORMAT(FROM_UNIXTIME(" . CallCenterRequest::tableName() . ".sent_at), \"%d.%m.%Y\")",
            ]);


        } else {
            $this->query->select([
                'country_name' => Country::tableName() . '.name',
                'yearcreatedat' => "DATE_FORMAT(FROM_UNIXTIME(" . CallCenterRequest::tableName() . ".sent_at), \"%Y\")",
                'monthcreatedat' => "DATE_FORMAT(FROM_UNIXTIME(" . CallCenterRequest::tableName() . ".sent_at), \"%m\")",
                'weekcreatedat' => "DATE_FORMAT(FROM_UNIXTIME(" . CallCenterRequest::tableName() . ".sent_at), \"%u\")",
                'daycreatedat' => "DATE_FORMAT(FROM_UNIXTIME(" . CallCenterRequest::tableName() . ".sent_at), \"%d\")",
                'groupbyname' => "DATE_FORMAT(FROM_UNIXTIME(" . CallCenterRequest::tableName() . ".sent_at), \"%d.%m.%Y\")",
            ]);
        }

        $mapStatuses = $this->getMapStatuses();
        foreach ($mapStatuses as $name => $statuses) {
            $this->query->addInConditionCount('order.status_id', $statuses, $name);
        }

        return $this;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $content = '';
        return $content;
    }

    /**
     * @return array
     */
    public function getMapStatuses()
    {
        if (empty($this->mapStatuses)) {
            $this->mapStatuses = [
                Yii::t('common', 'Подтверждено') => OrderStatus::getApproveList(),
                Yii::t('common', 'Все') => OrderStatus::getAllList(),
            ];
        }

        return $this->mapStatuses;
    }

    /**
     * @param array $params
     * @return bool
     */
    public function applyFilters($params)
    {
        $this->getDailyFilter();
        $this->getCountrySelectFilter()->apply($this->query, $params);
        $this->getProductSelectFilter()->apply($this->query, $params);
        $this->getCuratorUserFilter()->apply($this->query, $params);
        $this->getTypeDatePartFilter()->apply($this->query, $params);
        return true;
    }

    /**
     * @param \app\modules\order\models\query\OrderQuery $query
     * @param array $params
     * @return \app\modules\order\models\query\OrderQuery
     */
    public function applyFiltersSubQuery($query, $params)
    {
        $this->getDuplicateOrderFilter()->apply($query, $params);
        return $query;
    }

    /**
     * @return DailyFilterDefaultLastMonth
     */
    public function getDailyFilter()
    {
        if (is_null($this->dailyFilter)) {
            $this->dailyFilter = new DailyFilterDefaultLastMonth();
            $this->dailyFilter->type = DailyFilterDefaultLastMonth::TYPE_ORDER_CREATED_AT;
            if (isset($this->searchQuery['s']['from']) && isset($this->searchQuery['s']['from'])) {
                $this->dailyFilter->from = $this->searchQuery['s']['from'];
                $this->dailyFilter->to = $this->searchQuery['s']['to'];
            } else {
                $this->dailyFilter->from = date('d.m.Y', time() - (60 * 60 * 24 * 31));
                $this->dailyFilter->to = date('d.m.Y', time());
            }
        }

        return $this->dailyFilter;
    }

    /**
     * @return CountrySelectFilter
     */
    public function getCountrySelectFilter()
    {
        if (is_null($this->countrySelectFilter)) {
            $this->countrySelectFilter = new CountrySelectFilter();
        }
        return $this->countrySelectFilter;
    }

    /**
     * @return ProductSelectFilter
     */
    public function getProductSelectFilter()
    {
        if (is_null($this->productSelectFilter)) {
            $this->productSelectFilter = new ProductSelectFilter();
        }
        return $this->productSelectFilter;
    }

    /**
     * @return CuratorUserFilter
     */
    public function getCuratorUserFilter()
    {
        if (is_null($this->curatorUserFilter)) {
            $this->curatorUserFilter = new CuratorUserFilter();
        }
        return $this->curatorUserFilter;
    }

    /**
     * @return TypeDatePartFilter
     */
    public function getTypeDatePartFilter()
    {
        if (is_null($this->typeDatePartFilter)) {
            $this->typeDatePartFilter = new TypeDatePartFilter();
        }
        return $this->typeDatePartFilter;
    }

    /**
     * @return DuplicateOrderFilter
     */
    public function getDuplicateOrderFilter()
    {
        if (is_null($this->duplicateOrderFilter)) {
            $this->duplicateOrderFilter = new DuplicateOrderFilter();
        }
        return $this->duplicateOrderFilter;
    }

}
