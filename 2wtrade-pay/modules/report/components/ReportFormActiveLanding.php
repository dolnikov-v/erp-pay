<?php

namespace app\modules\report\components;

use app\models\Landing;
use app\modules\order\models\Order;
use app\modules\report\components\filters\DailyFilter;
use app\modules\report\extensions\DataProvider;
use app\modules\report\extensions\Query;
use yii\data\ArrayDataProvider;

/**
 * Class ReportFormActiveLanding
 * @package app\modules\report\components
 */
class ReportFormActiveLanding extends ReportForm
{
    /**
     * @var DailyFilter
     */
    protected $dailyFilter;

    public function init()
    {
        $this->groupBy = self::GROUP_BY_LANDING;

        parent::init();
    }

    /**
     * @param array $params
     * @return ArrayDataProvider
     */
    public function apply($params)
    {
        $this->load($params);

        $this->query = new Query();
        $this->query->from([Order::tableName()]);
        $this->buildSelect()->applyFilters($params);
        $this->query->orderBy(['count_lead' => SORT_DESC]);
        $dataProvider = new DataProvider([
            'query' => $this->query,
            'form' => $this,
        ]);

        $dataProvider->pagination->pageSize = $dataProvider->pagination->totalCount;

        return $dataProvider;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $output = '';

        return $output;
    }

    /**
     * @inheritdoc
     */
    protected function buildSelect()
    {
        $this->query->select([
            'landing_url' => Landing::tableName() . '.url',
            'count_lead' => 'COUNT('. Order::tableName() . '.id)',
        ]);
        $this->buildSelectMapStatuses();

        return $this;
    }

    /**
     * @param array $params
     * @return ReportForm
     */
    protected function applyFilters($params)
    {
        $this->getDailyFilter()->apply($this->query, $params);
        $this->getProductFilter()->apply($this->query, $params);
        $this->getCountryFilter()->apply($this->query, $params);

        return $this;
    }

    /**
     * @return DailyFilter
     */
    public function getDailyFilter()
    {
        if (is_null($this->dailyFilter)) {
            $this->dailyFilter = new DailyFilter();
            $this->dailyFilter->type = DailyFilter::TYPE_ORDER_CREATED_AT;
        }

        return $this->dailyFilter;
    }

}