<?php

namespace app\modules\report\components;

use app\models\Country;
use app\models\Currency;
use app\modules\order\models\Lead;
use app\modules\order\models\Order;
use app\modules\order\models\OrderFinanceFact;
use app\modules\order\models\OrderStatus;
use app\modules\report\components\filters\DateFilter;
use app\modules\report\extensions\DataProvider;
use app\modules\report\extensions\Query;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * Class ReportFormFinance
 * @package app\modules\report\components
 */
class ReportFormFinance extends ReportForm
{
    /**
     * @var array
     */
    public static $currencies;

    /**
     * @var string Группировка по умолчанию
     */
    public $groupBy = self::GROUP_BY_DELIVERY_SENT_AT_NOT_EMPTY;

    /**
     * @var array Маппинг статусов
     */
    protected $mapStatuses = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if (!is_array(self::$currencies)) {
            $currency = Country::find()
                ->joinWith(['currencyRate']);

            self::$currencies = ArrayHelper::map($currency->all(), 'id', 'currencyRate.rate');
        }

        $this->groupByCollection[self::GROUP_BY_PRODUCTS] = Yii::t('common', 'Товар');
        $this->groupByCollection[self::GROUP_BY_COUNTRY_PRODUCTS] = Yii::t('common', 'Страна') . ' \\ ' . Yii::t('common', 'Товар');
        $this->groupByCollection[self::GROUP_BY_COUNTRY_DATE] = Yii::t('common', 'Страна') . ' \\ ' . Yii::t('common', 'Дата');
        $this->groupByCollection[self::GROUP_BY_DELIVERY] = Yii::t('common', 'Курьерская служба');
    }

    /**
     * @param array $params
     *
     * @return DataProvider
     * @throws \yii\web\HttpException
     */
    public function apply($params)
    {
        if (empty($params)) {
            $params[$this->formName()]['country_ids'] = [Yii::$app->user->getCountry()->id];
        }

        $this->getDateFilter()->type = DateFilter::TYPE_DELIVERY_SENT_AT_NOT_EMPTY;
        $this->load($params);

        $this->query = new Query();
        $this->query->from([Order::tableName()]);
        $this->query->andWhere([Order::tableName() . '.duplicate_order_id' => null]);
        $this->applyFilters($params);
        $this->prepareCurrencyRate();
        $this->buildSelect();

        $dataProvider = new DataProvider([
            'form' => $this,
            'query' => $this->query,
        ]);

        $dataProvider->pagination->pageSize = $dataProvider->pagination->totalCount;

        return $dataProvider;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $output = '';

        return $output;
    }

    /**
     * @return array
     */
    public function getMapStatuses()
    {
        if (empty($this->mapStatuses)) {
            $this->mapStatuses = [
                Yii::t('common', 'Выкуплено') => OrderStatus::getBuyoutList(),
                Yii::t('common', 'В процессе') => OrderStatus::getProcessList(),
                Yii::t('common', 'Не выкуплено') => OrderStatus::getNotBuyoutList(),
                Yii::t('common', 'Деньги получены') => OrderStatus::getOnlyMoneyReceivedList(),
                Yii::t('common', 'Холды') => OrderStatus::getHoldStatusList(),
                Yii::t('common', 'Отказ') => OrderStatus::getCCRejectedStatusList(),
            ];

            $this->mapStatuses[Yii::t('common', 'Всего заказов')] = $this->getMapStatusesAsArray();
        }

        return $this->mapStatuses;
    }

    /**
     * @inheritdoc
     */
    protected function buildSelect()
    {
        $mapStatuses = $this->getMapStatuses();

        foreach ($mapStatuses as $name => $statuses) {
            $this->query->addInConditionCount(Order::tableName() . '.status_id', $statuses, $name);
        }

        $this->query->leftJoin(Country::tableName(), Order::tableName() . '.country_id=' . Country::tableName() . '.id');
        $this->query->addCondition(Country::tableName() . '.id', 'country_id');

        if ($this->searchQuery[$this->formName()]['package']??null) {
            $this->query->leftJoin(Lead::tableName(), Order::tableName() . '.id=' . Lead::tableName() . '.order_id');
        }

        $convertPriceTotalToCurrencyId = Country::tableName() . '.currency_id';
        if ($this->dollar) {
            $usdCurrency = Currency::getUSD();
            $convertPriceTotalToCurrencyId = $usdCurrency->id;
        }


        $case = $this->query->buildCaseCondition(
            'convertToCurrencyByCountry(' . Order::tableName() . '.price_total + ' . Order::tableName() . '.delivery, ' . Order::tableName() . '.price_currency,' . $convertPriceTotalToCurrencyId . ')',
            Order::tableName() . '.status_id',
            OrderStatus::getApproveList()
        );

        $caseBuyout = $this->query->buildCaseCondition(
            'convertToCurrencyByCountry(' . Order::tableName() . '.price_total + ' . Order::tableName() . '.delivery, ' . Order::tableName() . '.price_currency,' . $convertPriceTotalToCurrencyId . ')',
            Order::tableName() . '.status_id',
            OrderStatus::getBuyoutList()
        );

        $this->query->addAvgCondition($case, Yii::t('common', 'Средний чек апрувленный'));
        $this->query->addAvgCondition($caseBuyout, Yii::t('common', 'Средний чек выкупной'));

        $statuses = $mapStatuses[Yii::t('common', 'Выкуплено')];

        $case = $this->query->buildCaseCondition(
            '(' . Order::tableName() . '.price_total + ' . Order::tableName() . '.delivery)',
            Order::tableName() . '.status_id',
            $statuses
        );

        $this->query->addSumCondition($case, Yii::t('common', 'Доход по выкупам'));

        $caseIncome = $this->query->buildCaseCondition(
            Order::tableName() . '.income',
            Order::tableName() . '.status_id',
            array_merge(OrderStatus::getBuyoutList(), OrderStatus::getProcessList(), OrderStatus::getNotBuyoutList())
        );

        $this->query->addSumCondition($caseIncome, Yii::t('common', 'Цена за лиды'));


        $this->query->leftJoin(OrderFinanceFact::tableName(),
            OrderFinanceFact::tableName() . '.order_id=' . Order::tableName() . '.id and ' . OrderFinanceFact::tableName() . '.pretension is not null');

        $this->query->addCountDistinctCondition(OrderFinanceFact::tableName() . '.order_id', Yii::t('common', 'Претензия'));

        $this->query->andWhere(['in', Order::tableName() . '.status_id', $this->getMapStatusesAsArray()]);

        return $this;
    }

    /**
     * @param array $params
     * @return ReportForm
     */
    protected function applyFilters($params)
    {
        $this->getDateFilter()->apply($this->query, $params);
        $this->getProductFilter()->apply($this->query, $params);
        $this->getPackageFilter()->apply($this->query, $params);
        $this->getCallCenterFilter()->apply($this->query, $params);
        $this->getCountryDeliveryPartnerSelectMultipleFilter()->apply($this->query, $params);
        $this->getOrderLogisticListFilter()->apply($this->query, $params);

        $this->applyBaseFilters($params);

        return $this;
    }

    /**
     * @param array $params
     * @return ReportForm
     */
    protected function applyBaseFilters($params)
    {
        $this->getCalculatePercentFilter()->apply($this->query, $params);
        $this->getCalculateDollarFilter()->apply($this->query, $params);

        return $this;
    }
}
