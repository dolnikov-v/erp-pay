<?php

namespace app\modules\report\components;

use app\models\Country;
use app\modules\callcenter\models\CallCenter;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use app\modules\report\components\filters\CountrySelectFilter;
use app\modules\report\components\filters\CuratorUserFilter;
use app\modules\report\extensions\Query;
use yii\data\ArrayDataProvider;

/**
 * Class ReportFormCallCenterDelayByCountry
 * @package app\modules\report\components
 */
class ReportFormCallCenterDelayByCountry extends ReportForm
{
    /**
     * @var $all_countries
     */
    public $all_countries;

    /**
     * @var CountrySelectFilter
     */
    protected $countrySelectFilter;

    /**
     * @var curatorUserFilter
     */
    protected $curatorUserFilter;

    /**
     * @var array Маппинг статусов
     */
    protected $mapStatuses = [];

    /**
     * @param array $params
     * @return ArrayDataProvider
     */
    public function apply($params)
    {
        $this->load($params);

        $start_day = strtotime('now 00:00:00');

        $this->query = new Query();
        $this->query->from(CallCenterRequest::tableName());
        $this->query->leftJoin(Order::tableName(), CallCenterRequest::tableName() . '.order_id=' . Order::tableName() . '.id');
        $this->query->leftJoin(CallCenter::tableName(), CallCenterRequest::tableName() . '.call_center_id=' . CallCenter::tableName() . '.id');
        $this->query->leftJoin(Country::tableName(), CallCenter::tableName() . '.country_id=' . Country::tableName() . '.id');
        $this->query->where(['IN',CallCenterRequest::tableName() .".status" ,[ CallCenterRequest::STATUS_IN_PROGRESS, CallCenterRequest::STATUS_ERROR]]);
        $this->query->andWhere([Order::tableName() .".status_id" => OrderStatus::getSentToCCList()]);

        $this->buildSelect()->applyFilters($params);

        $dataArray = $this->query->all();

        // Объединяем периоды для несегодняшних дат
        $resArray = [];
        $todayArray = Array('day' => date('d', $start_day), 'month' => date('m', $start_day), 'year' => date('Y', $start_day));
        foreach ($dataArray as $item) {
            if ($todayArray['day'] == $item['dayofdate'] &&
                $todayArray['month'] == $item['monthofdate'] &&
                $todayArray['year'] == $item['yearofdate'])
            {
                $resArray[] = $item;
                continue;
            }
            if (!empty($resArray)) {
                if ($item['country_name'] == $resArray[count($resArray) - 1]['country_name'] &&
                    $item['callcenter_name'] == $resArray[count($resArray) - 1]['callcenter_name'] &&
                    $item['yearofdate'] == $resArray[count($resArray) - 1]['yearofdate'] &&
                    $item['monthofdate'] == $resArray[count($resArray) - 1]['monthofdate'] &&
                    $item['dayofdate'] == $resArray[count($resArray) - 1]['dayofdate']
                )
                {
                    $resArray[count($resArray) - 1]['order_count'] += $item['order_count'];
                    //$resArray[count($resArray) - 1]['orders'] .= $item['orders'];
                }
                else {
                    $resArray[] = $item;
                }
            }
            else {
                $resArray[] = $item;
            }
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $resArray,
            'pagination' => false,
        ]);

        return $dataProvider;
    }

    /**
     * @inheritdoc
     */
    protected function buildSelect()
    {
        $dateTypes = "COALESCE(" .
                     CallCenter::tableName() . ".cc_update_response_at,"  .
                     CallCenter::tableName() . ".cc_send_response_at,"  .
                     CallCenter::tableName() . ".sent_at,"  .
                     CallCenter::tableName() . ".created_at  
        )";
        $this->query->select([
            //'orders' => 'GROUP_CONCAT(DISTINCT ' . CallCenterRequest::tableName() . '.order_id)',
            'rep_country_id' => CallCenter::tableName(). '.country_id',
            'country_name' => Country::tableName(). '.name',
            'slug' => Country::tableName(). '.slug',
            'yearofdate' => "DATE_FORMAT(FROM_UNIXTIME( $dateTypes), \"%Y\")",
            'monthofdate' => "DATE_FORMAT(FROM_UNIXTIME( $dateTypes), \"%m\")",
            'dayofdate' => "DATE_FORMAT(FROM_UNIXTIME( $dateTypes), \"%d\")",
            'interval' => "FLOOR(DATE_FORMAT(FROM_UNIXTIME( $dateTypes), \"%H\")/4)",
            'min_delay' => "min(UNIX_TIMESTAMP(NOW()) -  $dateTypes)",
            'avg_delay' => "avg(UNIX_TIMESTAMP(NOW()) -  $dateTypes)",
            'max_delay' => "max(UNIX_TIMESTAMP(NOW()) -  $dateTypes)",
            'rep_callcenter_id' => CallCenterRequest::tableName() .'.call_center_id',
            'callcenter_name' => CallCenter::tableName() .'.name',
            'order_count' => "COUNT(" . CallCenterRequest::tableName() . ".`order_id`)",
        ]);

        $this->query->groupBy(['rep_country_id', 'yearofdate', 'monthofdate', 'dayofdate', 'interval', 'rep_callcenter_id']);

        return $this;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $content = '';
        return $content;
    }

    /**
     * @param array $params
     * @return bool
     */
    public function applyFilters($params)
    {
        $this->getCountrySelectFilter()->apply($this->query, $params);
        $this->getCuratorUserFilter()->apply($this->query, $params);

        return true;
    }

    /**
     * @return CountrySelectFilter
     */
    public function getCountrySelectFilter()
    {
        if (is_null($this->countrySelectFilter)) {
            $this->countrySelectFilter = new CountrySelectFilter();
        }
        return $this->countrySelectFilter;
    }

    /**
     * @return CuratorUserFilter
     */
    public function getCuratorUserFilter()
    {
        if (is_null($this->curatorUserFilter)) {
            $this->curatorUserFilter = new CuratorUserFilter();
        }
        return $this->curatorUserFilter;
    }

}
