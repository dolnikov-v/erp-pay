<?php
namespace app\modules\report\components;

use app\models\Country;
use app\models\CurrencyRate;
use app\models\OrderCreationDate;
use app\models\Product;
use app\models\Timezone;
use app\modules\callcenter\models\CallCenter;
use app\modules\callcenter\models\CallCenterRequestHistory;
use app\modules\salary\models\Designation;
use app\modules\salary\models\Person;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\callcenter\models\CallCenterUser;
use app\modules\callcenter\models\CallCenterWorkTime;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\models\Order;
use app\modules\order\models\OrderProduct;
use app\modules\order\models\OrderStatus;
use app\modules\report\components\filters\DateFilter;
use app\modules\report\extensions\Query;
use yii\data\ArrayDataProvider;
use app\modules\report\components\filters\DateWorkFilter;
use app\modules\report\components\filters\OfficeFilter;
use app\modules\report\components\filters\CallCenterFilter;
use app\modules\report\components\filters\TeamLeadFilter;
use Yii;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use app\widgets\Panel;

/**
 * Class ReportFormTeam
 * @package app\modules\report\components
 */
class ReportFormTeam extends ReportForm
{
    /**
     * @var array Маппинг статусов
     */
    protected $mapStatuses = [];

    public $groupBy = self::GROUP_BY_LAST_FOREIGN_OPERATOR;

    /*
     * "Галочка" конвертировать в доллар всегда отмечена
     * @var string
     */
    public $dollar = 'on';

    /**
     * @var DateWorkFilter
     */
    protected $dailyFilter;

    /**
     * @var OfficeFilter
     */
    protected $officeFilter;

    /**
     * @var CallCenterFilter
     */
    protected $callCenterFilter;

    /**
     * @var TeamLeadFilter
     */
    protected $teamLeadFilter;

    /**
     * @var integer
     */
    public $totalCount;

    /**
     * @var integer
     */
    public $person = null;

    /**
     * @var []
     */
    public $products = [];

    /**
     * @var []
     */
    public $teamLeads = [];

    /**
     * @var []
     */
    public $totals = [];

    /**
     * @var []
     */
    public $sortAttributes = [];

    /**
     * @return array
     */
    public function getMapStatuses()
    {
        if (empty($this->mapStatuses)) {
            $this->mapStatuses = [
                'approve' => OrderStatus::getApproveList(),
                'buyout' => OrderStatus::getBuyoutList(),
                'process' => OrderStatus::getProcessList(),
                'notbuyout' => OrderStatus::getNotBuyoutList(),
                'total' => OrderStatus::getAllList(),
            ];
        }
        return $this->mapStatuses;
    }

    /**
     * @param array $params
     * @return ArrayDataProvider
     */
    public function apply($params)
    {
        if (empty($params['s']['callCenter']) && empty($params['s']['office'][0])) {
            $this->panelAlert = Yii::t('common', 'Выберите Офис или Направление');
            $this->panelAlertStyle = Panel::ALERT_DANGER;
        }

        $this->getDateFilter()->type = DateFilter::TYPE_CALL_CENTER_SENT_AT;
        $this->load($params);

        $person = $params['person'] ?? null;

        $this->query = new Query();

        $allModels = [];
        if (!$this->panelAlert) {

            $this->panelAlert = Yii::t('common', 'Осуществлялась конвертация валют в USD');
            $this->panelAlertStyle = Panel::ALERT_PRIMARY;

            $this->query->from([Person::tableName()]);

            $this->query->leftJoin(CallCenterUser::tableName(), CallCenterUser::tableName() . '.person_id=' . Person::tableName() . '.id');

            $this->query->leftJoin(Designation::tableName(), Designation::tableName() . '.id=' . Person::tableName() . '.designation_id');
            $this->query->leftJoin(CallCenter::tableName(), CallCenter::tableName() . '.id=' . CallCenterUser::tableName() . '.callcenter_id');
            $this->query->leftJoin(Country::tableName(), Country::tableName() . '.id=' . CallCenter::tableName() . '.country_id');

            $this->query->leftJoin(Person::tableName() . ' as parent', Person::tableName() . '.parent_id=parent.id');
            $this->query->leftJoin(Designation::tableName() . ' as parent_designation', 'parent.designation_id=parent_designation.id');

            $this->buildSelect()->applyFilters($params);
            $groupBy = Person::tableName() . ".id";

            $this->query->andWhere([Designation::tableName() . '.calc_work_time' => 1]);
            $this->query->andWhere(['is not', CallCenterUser::tableName() . '.id', null]);

            $this->getDateFilter()->load($params);
            $dateFrom = date("Y-m-d", strtotime($this->getDateFilter()->from));
            $dateTo = date("Y-m-d", strtotime($this->getDateFilter()->to));

            $this->query->andWhere([
                'or',
                ['<=', Person::tableName() . '.start_date', date("Y-m-d", strtotime($dateTo))],
                ['is', Person::tableName() . '.start_date', null]
            ]);

            $this->query->andWhere([
                'or',
                ['between', Person::tableName() . '.dismissal_date', date("Y-m-d", strtotime($dateFrom)), date("Y-m-d", strtotime($dateTo))],
                ['>', Person::tableName() . '.dismissal_date', date("Y-m-d", strtotime($dateTo))],
                ['is', Person::tableName() . '.dismissal_date', null]
            ]);

            $this->query->groupBy([$groupBy]);
            $this->query->orderBy([
                Person::tableName() . '.active' => SORT_DESC,
                Person::tableName() . '.name' => SORT_ASC,
            ]);

            if ($person) {
                $this->query->andWhere([
                    $groupBy => $person
                ]);
            }

            $allModels = $this->query->all();

            if ($person && sizeof($allModels) == 1) {
                $this->person = $person;
            }

            // собираем всех операторов для сотрудника колл-центра
            $operators = [];
            $users = [];
            foreach ($allModels as &$model) {
                $model['team_lead_code'] = $model['team_lead_code'] ? $model['team_lead_code'] : '_';
                $model['operators'] = [];
                if ($model['call_center_user_identity']) {
                    if (strpos($model['call_center_user_identity'], ',') !== false) {
                        $identities = explode(',', $model['call_center_user_identity']);
                    } else {
                        $identities = [$model['call_center_user_identity']];
                    }
                    foreach ($identities as $identity) {
                        if (strpos($identity, ':') !== false) {
                            list($callCenterId, $operatorId) = explode(':', $identity);
                            if ($callCenterId && $operatorId) {
                                $model['operators'][$callCenterId][] = $operatorId;
                                $operators[$callCenterId][] = $operatorId;
                            }
                        }
                    }
                }
                if ($model['call_center_user_ids']) {
                    $tmp = explode(',', $model['call_center_user_ids']);
                    $model['users'] = $tmp;
                    if ($tmp) {
                        $users = array_merge($users, $tmp);
                    }
                }
            }
            unset($model);

            if ($operators) {

                $this->getMapStatuses();
                $this->query = new Query();
                $this->query->from([Order::tableName()]);
                $this->query->select([
                    'last_foreign_operator' => CallCenterRequest::tableName() . '.last_foreign_operator',
                    'call_center_id' => CallCenterRequest::tableName() . '.call_center_id',
                ]);
                $toDollar = '';
                if ($this->dollar == 'on' || $this->dollar) {
                    $toDollar = '/ ' . CurrencyRate::tableName() . '.rate';
                }
                $case_approve = $this->query->buildCaseCondition(
                    //'convertToCurrencyByCountry(' . Order::tableName() . '.price_total, ' . CurrencyRate::tableName() . '.currency_id, ' . Order::tableName() . '.price_currency) ' . $toDollar,
                    Order::tableName() . '.price_total ' . $toDollar,
                    Order::tableName() . '.status_id',
                    OrderStatus::getApproveList()
                );
                $case_buyout = $this->query->buildCaseCondition(
                    //'convertToCurrencyByCountry(' . Order::tableName() . '.price_total, ' . CurrencyRate::tableName() . '.currency_id, ' . Order::tableName() . '.price_currency) ' . $toDollar,
                    Order::tableName() . '.price_total ' . $toDollar,
                    Order::tableName() . '.status_id',
                    OrderStatus::getBuyoutList()
                );
                $this->query->addAvgCondition($case_approve, 'srednijcek');
                $this->query->addAvgCondition($case_buyout, 'srednijvykupnojcek');

                $this->query->addAvgCondition(
                    'IF(sub.min_called_at IS NOT NULL,sub.min_called_at+' . Timezone::tableName() . '.time_offset,' . CallCenterRequest::tableName() . '.approved_at)' .
                    '-' .
                    'COALESCE(' . OrderCreationDate::tableName() . '.foreign_create_at,' . Order::tableName() . '.created_at)',
                    'vremaotveta');

                $this->query->addSelect([
                    'product_id' => OrderProduct::find()
                        ->select([
                            OrderProduct::tableName() . '.product_id'
                        ])
                        ->where([OrderProduct::tableName() . '.order_id' => new Expression(Order::tableName() . '.id')])
                        ->limit(1)
                ]);

                $this->buildSelectMapStatuses();
                $this->query->leftJoin(CallCenterRequest::tableName(), CallCenterRequest::tableName() . '.order_id = ' . Order::tableName() . '.id');
                $this->query->leftJoin(DeliveryRequest::tableName(), DeliveryRequest::tableName() . '.order_id = ' . Order::tableName() . '.id');
                $this->query->leftJoin(Country::tableName(), Order::tableName() . '.country_id=' . Country::tableName() . '.id');
                $this->query->leftJoin(Timezone::tableName(), Country::tableName() . '.timezone_id=' . Timezone::tableName() . '.id');
                $this->query->leftJoin(CurrencyRate::tableName(), Country::tableName() . '.currency_id = ' . CurrencyRate::tableName() . '.currency_id');
                $this->query->leftJoin(OrderCreationDate::tableName(), Order::tableName() . '.id=' . OrderCreationDate::tableName() . '.order_id');

                $subQuery = new Query();
                $subQuery->select(
                    [
                        'min_called_at' => 'min(called_at)',
                        'call_center_request_id'
                    ])
                    ->from(CallCenterRequestHistory::tableName())
                    ->leftJoin(CallCenterRequest::tableName(), CallCenterRequest::tableName() . '.id = ' . CallCenterRequestHistory::tableName() . '.call_center_request_id')
                    ->groupBy(
                        CallCenterRequestHistory::tableName() . '.call_center_request_id'
                    );

                $this->applyOrderFilters($params);

                $condition = ['or'];
                foreach ($operators as $callCenterId => $ccOperators) {
                    $condition[] = [
                        CallCenterRequest::tableName() . '.call_center_id' => $callCenterId,
                        CallCenterRequest::tableName() . '.last_foreign_operator' => $ccOperators,
                    ];
                }
                $this->query->andWhere($condition);
                $subQuery->andWhere($condition);

                $this->query->leftJoin(['sub' => $subQuery], CallCenterRequest::tableName() . '.id=sub.call_center_request_id');

                $groupBy = [
                    'call_center_id',
                    'last_foreign_operator',
                    'product_id'
                ];

                $this->query->groupBy($groupBy);
                $orders = $this->query->all();

                $orders = ArrayHelper::index($orders, 'product_id', ['call_center_id', 'last_foreign_operator']);

                if ($this->person) {

                    $this->query->addSelect([
                        'country' => Country::tableName() . '.name',
                    ]);

                    $this->query->addSelect([
                        Query::GROUP_BY_NAME => OrderProduct::find()
                            ->select([
                                OrderProduct::tableName() . '.product_id'
                            ])
                            ->where([OrderProduct::tableName() . '.order_id' => new Expression(Order::tableName() . '.id')])
                            ->limit(1)
                    ]);

                    $this->query->addSelect([
                        'product_id' => OrderProduct::find()
                            ->select([
                                OrderProduct::tableName() . '.product_id'
                            ])
                            ->where([OrderProduct::tableName() . '.order_id' => new Expression(Order::tableName() . '.id')])
                            ->limit(1)
                    ]);

                    $this->query->addSelect([
                        Query::GROUP_BY_LABEL => OrderProduct::find()
                            ->joinWith(['product'])
                            ->select([
                                Product::tableName() . '.name'
                            ])
                            ->where([OrderProduct::tableName() . '.order_id' => new Expression(Order::tableName() . '.id')])
                            ->limit(1)
                    ]);
                    $this->query->groupBy([
                        'country_id',
                        Query::GROUP_BY_NAME
                    ]);
                    $this->query->orderBy(Query::GROUP_BY_LABEL);

                    $allModels[0]['orders'] = $this->query->all();


                    $timeFrom = Yii::$app->formatter->asTimestamp($params['s']['from']);
                    $timeTo = Yii::$app->formatter->asTimestamp($params['s']['to']);
                    $diff = ($timeTo - $timeFrom) / 86400 + 1;

                    // произвольная выборка
                    $intervals = [
                        [$timeFrom - $diff * 2 * 86400, $timeTo - $diff * 2 * 86400],
                        [$timeFrom - $diff * 86400, $timeTo - $diff * 86400],
                    ];


                    if ($diff > 16 && $diff <= 31) {
                        // считаем что выборка за месяц
                        $df1 = date_create($params['s']['from'] . ' first day of -1 month');
                        $df2 = date_create($params['s']['from'] . ' first day of -2 month');
                        $dt1 = date_create($params['s']['from'] . ' last day of -1 month');
                        $dt2 = date_create($params['s']['from'] . ' last day of -2 month');

                        $intervals = [
                            [$df1->getTimestamp(), $dt1->getTimestamp()],
                            [$df2->getTimestamp(), $dt2->getTimestamp()],
                        ];
                    }
                    if ($diff >= 12 && $diff <= 16) {
                        // за 2 недели
                        if (date('j', $timeFrom) == 1) {
                            // с первого по 15 число
                            $df = date_create($params['s']['from'] . ' first day of -1 month');
                            $dt = date_create($params['s']['from'] . ' last day of -1 month');
                            $intervals = [
                                [$df->getTimestamp(), $df->getTimestamp() + ($diff - 1) * 86400],
                                [$df->getTimestamp() + $diff * 86400, $dt->getTimestamp()],
                            ];
                        }
                        if (date('j', $timeFrom) == 16) {
                            // с 16 ое по конец месяца
                            $df1 = date_create($params['s']['from'] . ' first day of 0 month');
                            $df2 = date_create($params['s']['from'] . ' first day of -1 month');
                            $dt1 = date_create($params['s']['from'] . ' last day of -1 month');

                            $intervals = [
                                [$df1->getTimestamp(), $df1->getTimestamp() + 14 * 86400],
                                [$df2->getTimestamp() + 15 * 86400, $dt1->getTimestamp()],
                            ];
                        }
                    }


                    $this->query->where($condition);

                    $dateFrom = $intervals[0][0];
                    $dateTo = $intervals[0][1] + 86399;

                    $this->query->andFilterWhere(['>=', $this->getDateFilter()->getFilteredAttribute(), $dateFrom]);
                    $this->query->andFilterWhere(['<=', $this->getDateFilter()->getFilteredAttribute(), $dateTo]);
                    $allModels[0]['ordersPreviousDates'] = [
                        'dateFrom' => $dateFrom,
                        'dateTo' => $dateTo,
                    ];
                    $allModels[0]['ordersPrevious'] = $this->query->all();


                    $this->query->where($condition);

                    $dateFrom = $intervals[1][0];
                    $dateTo = $intervals[1][1] + 86399;
                    $this->query->andFilterWhere(['>=', $this->getDateFilter()->getFilteredAttribute(), $dateFrom]);
                    $this->query->andFilterWhere(['<=', $this->getDateFilter()->getFilteredAttribute(), $dateTo]);
                    $allModels[0]['ordersPreviousDates2'] = [
                        'dateFrom' => $dateFrom,
                        'dateTo' => $dateTo,
                    ];
                    $allModels[0]['ordersPrevious2'] = $this->query->all();
                }

                // посчитаем звонки операторов
                $this->query = new Query();
                $this->query->from([CallCenterWorkTime::tableName()]);
                $this->query->select([
                    'days' => 'GROUP_CONCAT(' . CallCenterWorkTime::tableName() . '.date)',
                    'avg_calls' => 'AVG(' . CallCenterWorkTime::tableName() . '.number_of_calls)',
                    'call_center_user_id' => CallCenterWorkTime::tableName() . '.call_center_user_id',
                ]);
                $this->query
                    ->where([
                        'is not', CallCenterWorkTime::tableName() . '.number_of_calls', null
                    ])
                    ->andWhere([
                        CallCenterWorkTime::tableName() . '.call_center_user_id' => $users
                    ])
                    ->andWhere(['>=', 'date', date("Y-m-d", strtotime($this->getDateFilter()->from))])
                    ->andWhere(['<=', 'date', date("Y-m-d", strtotime($this->getDateFilter()->to))]);
                $this->query->groupBy([
                    'call_center_user_id',
                ]);

                $calls = ArrayHelper::index($this->query->all(), 'call_center_user_id');

                //почитаем заказы по дате апрува для определения результативных звонков
                $this->query = new Query();
                $this->query->from([Order::tableName()]);
                $this->query->select([
                    'last_foreign_operator' => CallCenterRequest::tableName() . '.last_foreign_operator',
                    'call_center_id' => CallCenterRequest::tableName() . '.call_center_id',
                ]);
                $this->query->addInConditionCount(Order::tableName() . '.status_id', OrderStatus::getApproveList(), 'approve');
                $this->query->leftJoin(CallCenterRequest::tableName(), CallCenterRequest::tableName() . '.order_id = ' . Order::tableName() . '.id');
                $this->query
                    ->andWhere(['>=', CallCenterRequest::tableName() . '.approved_at', Yii::$app->formatter->asTimestamp($this->getDateFilter()->from)])
                    ->andWhere(['<=', CallCenterRequest::tableName() . '.approved_at', Yii::$app->formatter->asTimestamp($this->getDateFilter()->to) + 86399]);

                $condition = ['or'];
                foreach ($operators as $callCenterId => $ccOperators) {
                    $condition[] = [
                        CallCenterRequest::tableName() . '.call_center_id' => $callCenterId,
                        CallCenterRequest::tableName() . '.last_foreign_operator' => $ccOperators,
                    ];
                }
                $this->query->andWhere($condition);
                $this->query->groupBy([
                    'call_center_id',
                    'last_foreign_operator'
                ]);
                $ordersApproved = ArrayHelper::index($this->query->all(), 'last_foreign_operator', ['call_center_id']);

                // по всем сотрудникам

                $allProducts = [];
                foreach ($allModels as &$model) {
                    $model['approve'] = 0;
                    $model['buyout'] = 0;
                    $model['notbuyout'] = 0;
                    $model['process'] = 0;
                    $model['total'] = 0;
                    $model['srednij_cek'] = [];
                    $model['srednij_vykupnoj_cek'] = [];
                    $model['avg_calls'] = 0;
                    $model['avg_approve_calls'] = 0;
                    $model['work_days'] = [];
                    $model['product_buyout'] = [];
                    $model['count_buyout'] = 0;
                    $model['count_approve'] = 0;
                    $model['vrema_otveta'] = 0;

                    foreach ($model['operators'] as $callCenterId => $ccOperators) {
                        foreach ($ccOperators as $operatorId) {
                            if (isset($orders[$callCenterId][$operatorId])) {
                                $orderByProduct = $orders[$callCenterId][$operatorId];
                                foreach ($orderByProduct as $order) {
                                    $model['total'] += $order['total'];
                                    $model['approve'] += $order['approve'];
                                    $model['buyout'] += $order['buyout'];
                                    $model['notbuyout'] += $order['notbuyout'];
                                    $model['process'] += $order['process'];
                                    if ($order['srednijcek']) {
                                        $model['srednij_cek'][] = $order['srednijcek'];
                                    }
                                    if ($order['srednijvykupnojcek']) {
                                        $model['srednij_vykupnoj_cek'][] = $order['srednijvykupnojcek'];
                                    }
                                    $model['vrema_otveta'] += $order['vremaotveta'];
                                    if ($order['approve']) {
                                        $model['count_approve']++;
                                    }
                                    if ($order['buyout']) {
                                        $model['product_approve' . $order['product_id']] = $order['approve'];
                                        $model['product_buyout' . $order['product_id']] = $order['buyout'];
                                        $model['product_buyout' . $order['product_id'] . '_percent'] = $order['approve'] > 0 ? $order['buyout'] * 100 / $order['approve'] : 0;
                                        $allProducts[$order['product_id']] = $order['product_id'];
                                        $model['count_buyout']++;
                                    }
                                }
                            }

                            if (isset($ordersApproved[$callCenterId][$operatorId])) {
                                $order = $ordersApproved[$callCenterId][$operatorId];
                                $model['avg_approve_calls'] += $order['approve'] ?? 0;
                            }
                        }
                    }

                    if (!empty($model['users'])) {
                        foreach ($model['users'] as $userId) {
                            $model['avg_calls'] += $calls[$userId]['avg_calls'] ?? 0;
                            if (!empty($calls[$userId]['days'])) {
                                foreach (explode(',', $calls[$userId]['days']) as $date) {
                                    $model['work_days'][$date] = $date;
                                }
                            }
                        }
                    }

                    $model['avg_approve_calls'] = $model['work_days'] ? $model['avg_approve_calls'] / sizeof($model['work_days']) : 0;

                    $model['total'] = $model['vsego'] = $model['approve'];
                    $model['approve_percent'] = $model['total'] ? $model['approve'] * 100 / $model['total'] : 0;
                    $model['buyout_percent'] = $model['total'] ? $model['buyout'] * 100 / $model['total'] : 0;
                    $model['notbuyout_percent'] = $model['total'] ? $model['notbuyout'] * 100 / $model['total'] : 0;
                    $model['process_percent'] = $model['total'] ? $model['process'] * 100 / $model['total'] : 0;

                    $model['srednij_cek'] = $model['srednij_cek'] ? array_sum($model['srednij_cek']) / count($model['srednij_cek']) : 0;
                    $model['srednij_vykupnoj_cek'] = $model['srednij_vykupnoj_cek'] ? array_sum($model['srednij_vykupnoj_cek']) / count($model['srednij_vykupnoj_cek']) : 0;
                    $model['vrema_otveta'] = $model['count_approve'] ? ($model['vrema_otveta'] / $model['count_approve']) / 3600 : 0;

                }
                unset($model);

                $this->products = Product::find()->where(['id' => array_keys($allProducts)])->orderBy(['name' => SORT_ASC])->collection();

                if ($this->person) {
                    foreach ($allModels[0]['orders'] as &$model) {
                        $model['total'] = $model['vsego'] = $model['approve'];
                        $model['approve_percent'] = $model['total'] ? $model['approve'] * 100 / $model['total'] : 0;
                        $model['buyout_percent'] = $model['total'] ? $model['buyout'] * 100 / $model['total'] : 0;
                        $model['notbuyout_percent'] = $model['total'] ? $model['notbuyout'] * 100 / $model['total'] : 0;
                        $model['process_percent'] = $model['total'] ? $model['process'] * 100 / $model['total'] : 0;
                    }
                    foreach ($allModels[0]['ordersPrevious'] as &$model) {
                        $model['total'] = $model['vsego'] = $model['approve'];
                        $model['approve_percent'] = $model['total'] ? $model['approve'] * 100 / $model['total'] : 0;
                        $model['buyout_percent'] = $model['total'] ? $model['buyout'] * 100 / $model['total'] : 0;
                        $model['notbuyout_percent'] = $model['total'] ? $model['notbuyout'] * 100 / $model['total'] : 0;
                        $model['process_percent'] = $model['total'] ? $model['process'] * 100 / $model['total'] : 0;
                    }
                }
                unset($model);

                $this->totalCount = array_sum(ArrayHelper::getColumn($allModels, 'approve', []));
            } else {
                $this->panelAlert = Yii::t('common', 'Регионы не настроены');
                $this->panelAlertStyle = Panel::ALERT_DANGER;
            }
        }

        $this->sortAttributes = [
            'buyout' => [
                'asc' => ['team_sort' => SORT_ASC, 'buyout_percent' => SORT_ASC, 'buyout' => SORT_ASC],
                'desc' => ['team_sort' => SORT_ASC, 'buyout_percent' => SORT_DESC, 'buyout' => SORT_DESC]
            ],
            'approve' => [
                'asc' => ['team_sort' => SORT_ASC, 'approve_percent' => SORT_ASC, 'approve' => SORT_ASC],
                'desc' => ['team_sort' => SORT_ASC, 'approve_percent' => SORT_DESC, 'approve' => SORT_DESC]
            ],
            'notbuyout' => [
                'asc' => ['team_sort' => SORT_ASC, 'notbuyout_percent' => SORT_ASC, 'notbuyout' => SORT_ASC],
                'desc' => ['team_sort' => SORT_ASC, 'notbuyout_percent' => SORT_DESC, 'notbuyout' => SORT_DESC]
            ],
            'process' => [
                'asc' => ['team_sort' => SORT_ASC, 'process_percent' => SORT_ASC, 'process' => SORT_ASC],
                'desc' => ['team_sort' => SORT_ASC, 'process_percent' => SORT_DESC, 'process' => SORT_DESC]
            ],
            'total' => [
                'asc' => ['team_sort' => SORT_ASC, 'vsego' => SORT_ASC],
                'desc' => ['team_sort' => SORT_ASC, 'vsego' => SORT_DESC]
            ],
            'srednij_cek' => [
                'asc' => ['team_sort' => SORT_ASC, 'srednij_cek' => SORT_ASC],
                'desc' => ['team_sort' => SORT_ASC, 'srednij_cek' => SORT_DESC]
            ],
            'srednij_vykupnoj_cek' => [
                'asc' => ['team_sort' => SORT_ASC, 'srednij_vykupnoj_cek' => SORT_ASC],
                'desc' => ['team_sort' => SORT_ASC, 'srednij_vykupnoj_cek' => SORT_DESC]
            ],
            'avg_calls' => [
                'asc' => ['team_sort' => SORT_ASC, 'avg_calls' => SORT_ASC],
                'desc' => ['team_sort' => SORT_ASC, 'avg_calls' => SORT_DESC],
            ],
            'avg_approve_calls' => [
                'asc' => ['team_sort' => SORT_ASC, 'avg_approve_calls' => SORT_ASC],
                'desc' => ['team_sort' => SORT_ASC, 'avg_approve_calls' => SORT_DESC],
            ],
            'vrema_otveta' => [
                'asc' => ['team_sort' => SORT_ASC, 'vrema_otveta' => SORT_ASC],
                'desc' => ['team_sort' => SORT_ASC, 'vrema_otveta' => SORT_DESC],
            ]
        ];

        foreach ($this->products as $productId => $productName) {
            $attribute = 'product_buyout' . $productId;
            $this->sortAttributes[$attribute] = [
                'asc' => ['team_sort' => SORT_ASC, $attribute . '_percent' => SORT_ASC],
                'desc' => ['team_sort' => SORT_ASC, $attribute . '_percent' => SORT_DESC],
            ];
        }

        if (!$this->person) {

            // формируем группировку по командам
            $teamLeads = [];
            foreach ($allModels as $model) {
                if (!isset($teamLeads[$model['team_lead_code']])) {
                    $teamLeads[$model['team_lead_code']] = [
                        'code' => '',
                        'name' => '',
                        'operators' => 0,
                        'approve' => 0,
                        'buyout' => 0,
                        'notbuyout' => 0,
                        'process' => 0,
                        'vsego' => 0,
                        'srednij_cek' => [],
                        'srednij_vykupnoj_cek' => [],
                        'avg_calls' => [],
                        'avg_approve_calls' => [],
                        'vrema_otveta' => [],
                    ];
                    foreach ($this->products as $productId => $productName) {
                        $teamLeads[$model['team_lead_code']]['product_buyout' . $productId] = 0;
                        $teamLeads[$model['team_lead_code']]['product_approve' . $productId] = 0;
                    }
                }
                $teamLeads[$model['team_lead_code']]['code'] = $model['team_lead_code'];
                $teamLeads[$model['team_lead_code']]['name'] = $model['parent_name'];
                $teamLeads[$model['team_lead_code']]['operators']++;
                $teamLeads[$model['team_lead_code']]['approve'] += $model['approve'];
                $teamLeads[$model['team_lead_code']]['buyout'] += $model['buyout'];
                $teamLeads[$model['team_lead_code']]['notbuyout'] += $model['notbuyout'];
                $teamLeads[$model['team_lead_code']]['process'] += $model['process'];
                $teamLeads[$model['team_lead_code']]['vsego'] += $model['vsego'];
                if ($model['srednij_cek']) {
                    $teamLeads[$model['team_lead_code']]['srednij_cek'][] = $model['srednij_cek'];
                }
                if ($model['srednij_vykupnoj_cek']) {
                    $teamLeads[$model['team_lead_code']]['srednij_vykupnoj_cek'][] = $model['srednij_vykupnoj_cek'];
                }
                if ($model['avg_calls']) {
                    $teamLeads[$model['team_lead_code']]['avg_calls'][] = $model['avg_calls'];
                }
                if ($model['avg_approve_calls']) {
                    $teamLeads[$model['team_lead_code']]['avg_approve_calls'][] = $model['avg_approve_calls'];
                }
                if ($model['vrema_otveta']) {
                    $teamLeads[$model['team_lead_code']]['vrema_otveta'][] = $model['vrema_otveta'];
                }
                foreach ($this->products as $productId => $productName) {
                    $attribute = 'product_buyout' . $productId;
                    if (isset($model[$attribute])) {
                        $teamLeads[$model['team_lead_code']][$attribute] += $model[$attribute];
                    }
                    $attribute = 'product_approve' . $productId;
                    if (isset($model[$attribute])) {
                        $teamLeads[$model['team_lead_code']][$attribute] += $model[$attribute];
                    }
                }
            }

            // считаем проценты для каждой команды
            foreach ($teamLeads as &$teamLead) {

                if ($teamLead['operators']) {
                    $teamLead['srednij_cek'] = $teamLead['srednij_cek'] ? array_sum($teamLead['srednij_cek']) / count($teamLead['srednij_cek']) : 0;
                    $teamLead['srednij_vykupnoj_cek'] = $teamLead['srednij_vykupnoj_cek'] ? array_sum($teamLead['srednij_vykupnoj_cek']) / count($teamLead['srednij_vykupnoj_cek']) : 0;
                    $teamLead['avg_calls'] = ($teamLead['avg_calls']) ? array_sum($teamLead['avg_calls']) / count($teamLead['avg_calls']) : 0;
                    $teamLead['avg_approve_calls'] = ($teamLead['avg_approve_calls']) ? array_sum($teamLead['avg_approve_calls']) / count($teamLead['avg_approve_calls']) : 0;
                    $teamLead['vrema_otveta'] = ($teamLead['vrema_otveta']) ? array_sum($teamLead['vrema_otveta']) / count($teamLead['vrema_otveta']) : 0;
                }

                $teamLead['approve_percent'] = $teamLead['vsego'] ? $teamLead['approve'] * 100 / $teamLead['vsego'] : 0;
                $teamLead['buyout_percent'] = $teamLead['vsego'] ? $teamLead['buyout'] * 100 / $teamLead['vsego'] : 0;
                $teamLead['notbuyout_percent'] = $teamLead['vsego'] ? $teamLead['notbuyout'] * 100 / $teamLead['vsego'] : 0;
                $teamLead['process_percent'] = $teamLead['vsego'] ? $teamLead['process'] * 100 / $teamLead['vsego'] : 0;

                foreach ($this->products as $productId => $productName) {
                    $teamLead['product_buyout' . $productId . '_percent'] =
                        $teamLead['product_approve' . $productId] > 0 ?
                            $teamLead['product_buyout' . $productId] * 100 / $teamLead['product_approve' . $productId] : 0;
                }
            }
            unset($teamLead);


            // сортировка команд
            $prefix = $defPrefix = 1;
            $sortKey = $defSortKey = 'buyout';
            if (isset($params['sort'])) {
                $prefix = substr($params['sort'], 0, 1) == '-';
                $sortKey = $params['sort'];
                if ($prefix) {
                    $sortKey = substr($sortKey, 1);
                }
                if (!isset($this->sortAttributes[$sortKey])) {
                    $sortKey = $defSortKey;
                }
            }

            $selectedSort = $this->sortAttributes[$sortKey][$prefix ? 'desc' : 'asc'] ?? $this->sortAttributes['buyout']['desc'];

            ArrayHelper::multisort($teamLeads, array_keys($selectedSort), array_values($selectedSort));

            // задать сортировку всех операторов согласно порядку команды
            $teamSort = [];
            $i = 0;
            foreach ($teamLeads as $k => $v) {
                $teamSort[$k] = $i;
                $i++;
            }

            foreach ($allModels as &$model) {
                $model['team_sort'] = $teamSort[$model['team_lead_code']];
            }

            $this->teamLeads = $teamLeads;


            $totals = [
                'srednij_cek' => [],
                'srednij_vykupnoj_cek' => [],
                'avg_calls' => [],
                'avg_approve_calls' => [],
                'vrema_otveta' => [],
            ];

            foreach ($teamLeads as $teamLead) {
                if ($teamLead['srednij_cek']) {
                    $totals['srednij_cek'][] = $teamLead['srednij_cek'];
                }
                if ($teamLead['srednij_vykupnoj_cek']) {
                    $totals['srednij_vykupnoj_cek'][] = $teamLead['srednij_vykupnoj_cek'];
                }
                if ($teamLead['avg_calls']) {
                    $totals['avg_calls'][] = $teamLead['avg_calls'];
                }
                if ($teamLead['avg_approve_calls']) {
                    $totals['avg_approve_calls'][] = $teamLead['avg_approve_calls'];
                }
                if ($teamLead['vrema_otveta']) {
                    $totals['vrema_otveta'][] = $teamLead['vrema_otveta'];
                }
            }

            $totals['srednij_cek'] = $totals['srednij_cek'] ? array_sum($totals['srednij_cek']) / count($totals['srednij_cek']) : 0;
            $totals['srednij_vykupnoj_cek'] = $totals['srednij_vykupnoj_cek'] ? array_sum($totals['srednij_vykupnoj_cek']) / count($totals['srednij_vykupnoj_cek']) : 0;
            $totals['avg_calls'] = $totals['avg_calls'] ? array_sum($totals['avg_calls']) / count($totals['avg_calls']) : 0;
            $totals['avg_approve_calls'] = $totals['avg_approve_calls'] ? array_sum($totals['avg_approve_calls']) / count($totals['avg_approve_calls']) : 0;
            $totals['vrema_otveta'] = $totals['vrema_otveta'] ? array_sum($totals['vrema_otveta']) / count($totals['vrema_otveta']) : 0;

            $this->totals = $totals;
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $allModels,
            'sort' => [
                'defaultOrder' => ['buyout' => SORT_DESC],
                'attributes' => $this->sortAttributes,
            ]
        ]);
        $dataProvider->pagination->pageSize = $dataProvider->pagination->totalCount;

        return $dataProvider;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $output = '';

        return $output;
    }


    /**
     * @inheritdoc
     */
    protected function buildSelect()
    {
        $this->query->select([
            'id' => Person::tableName() . '.id',
            'fio' => Person::tableName() . '.name',
            'active' => Person::tableName() . '.active',
            'parent_id' => Person::tableName() . '.parent_id',
            'team_lead_code' => 'CONCAT(' . Person::tableName() . '.parent_id,"_",' . Person::tableName() . '.working_shift_id' . ')',
            'parent_name' => 'parent.name',
            'parent_designation' => 'parent_designation.name',
            'designation' => Designation::tableName() . '.name',
            'designation_id' => Designation::tableName() . '.id',
            'designation_team_lead' => Designation::tableName() . '.team_lead',
            'country' => Country::tableName() . '.name',
            'user_id' => CallCenterUser::tableName() . '.user_id',
            'user_login' => CallCenterUser::tableName() . '.user_login',
            'groupbyname' => CallCenterUser::tableName() . '.user_id',
            'callcenter_id' => CallCenterUser::tableName() . '.callcenter_id',
            'call_center_user_identity' => 'GROUP_CONCAT(CONCAT_WS(\':\', ' . CallCenterUser::tableName() . '.callcenter_id' . ',' . CallCenterUser::tableName() . '.user_id))',
            'call_center_user_ids' => 'GROUP_CONCAT(' . CallCenterUser::tableName() . '.id)',
            'call_center_user_logins' => 'GROUP_CONCAT(' . CallCenterUser::tableName() . '.user_login)',
            'country_names' => 'GROUP_CONCAT(' . Country::tableName() . '.name)',
            'country_id' => Country::tableName() . '.id',   // это не достоверно для множественных направлений, но для еденичных будет нормально работать ссылка на заказы
        ]);
        return $this;
    }


    public function getDateWorkFilter()
    {
        if (is_null($this->dailyFilter)) {
            $this->dailyFilter = new DateWorkFilter();
        }
        return $this->dailyFilter;
    }

    public function getOfficeFilter()
    {
        if (is_null($this->officeFilter)) {
            $this->officeFilter = new OfficeFilter(['selectFirst' => false]);
        }
        return $this->officeFilter;
    }

    public function getCallCenterFilter()
    {
        if (is_null($this->callCenterFilter)) {
            $this->callCenterFilter = new CallCenterFilter([
                'toUser' => true,
                'label' => 'Направление',
                'officeId' => $this->officeFilter->office
            ]);
        }
        return $this->callCenterFilter;
    }

    public function getTeamLeadFilter()
    {
        if (is_null($this->teamLeadFilter)) {
            $this->teamLeadFilter = new TeamLeadFilter([
                'officeId' => $this->officeFilter->office
            ]);
        }
        return $this->teamLeadFilter;
    }

    /**
     * @param array $params
     * @return ReportForm
     */
    protected function applyFilters($params)
    {
        $this->getOfficeFilter()->apply($this->query, $params);
        $this->getCallCenterFilter()->apply($this->query, $params);
        $this->getTeamLeadFilter()->apply($this->query, $params);
        return $this;
    }

    /**
     * @param array $params
     * @return ReportForm
     */
    protected function applyOrderFilters($params)
    {
        $this->getDateFilter()->apply($this->query, $params);
        return $this;
    }

    /**
     * @return integer
     */
    public function getTotalCount()
    {
        return $this->totalCount;
    }
}
