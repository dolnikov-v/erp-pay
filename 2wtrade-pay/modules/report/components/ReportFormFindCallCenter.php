<?php

namespace app\modules\report\components;

use app\modules\callcenter\models\CallCenter;
use app\modules\callcenter\models\CallCenterFullRequest;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use app\modules\report\components\filters\CallCenterFilter;
use app\modules\report\components\filters\DailyFilter;
use app\modules\report\components\filters\FindAdcomboIdFilter;
use app\modules\report\components\filters\FindCallCenterStatusFilter;
use Yii;
use yii\base\InvalidParamException;
use yii\helpers\ArrayHelper;


/**
 * Class ReportFormFindCallCenter
 * @package app\modules\report\components
 */
class ReportFormFindCallCenter extends ReportForm
{
    /**
     * @var DailyFilter
     */
    protected $dailyFilter;

    /**
     * @var FindAdcomboIdFilter
     */
    protected $idFilter;

    /**
     * @var CallCenterFilter
     */
    protected $callCenterFilter;

    /**
     * @var FindCallCenterStatusFilter
     */
    protected $statusFilter;

    /**
     * @var array
     */
    protected $queryParams;

    /**
     * @var bool
     */
    protected $searchInBase;

    /**
     * @param array $params
     * @return \app\modules\order\models\Order[]|array|\yii\db\ActiveRecord[]
     */
    public function apply($params)
    {
        $this->searchInBase = isset($params['search-in-base']);

        if ($this->searchInBase) {
            $this->query = CallCenterFullRequest::find()->joinWith('order');
        } else {
            $this->query = Order::find()->joinWith('callCenterRequest');
            $this->query->select([Order::tableName() . '.id']);
            $this->query->andWhere(['!=', Order::tableName() . '.status_id', OrderStatus::STATUS_SOURCE_SHORT_FORM]);
            $this->query->andWhere([
                'or',
                ['is', Order::tableName() . '.source_form', null],
                [Order::tableName() . '.source_form' => Order::TYPE_FORM_SHORT]
            ]);
        }

        $this->query->where([Order::tableName() . '.country_id' => Yii::$app->user->country->id]);

        $this->queryParams = [];

        $this->applyFilters($params);

        if ($this->searchInBase) {
            $orders = $this->query->all();
        } else {
            $batchSize = 300;
            $orders = [];
            $callCenters = [];
            if ($this->getCallCenterFilter()->callCenter != '') {
                $callCenters[] = CallCenter::findOne($this->getCallCenterFilter()->callCenter);
            } else {
                $callCenters = CallCenter::find()->active()->byCountryId(Yii::$app->user->country->id)->all();
            }

            $query = clone $this->query;

            $query->andWhere(['is', CallCenterRequest::tableName() . '.id', null]);

            $orderWithoutRequests = $query->all();
            $orderWithoutRequests = ArrayHelper::index($orderWithoutRequests, 'id');

            foreach ($callCenters as $callCenter) {
                $query = clone $this->query;
                $query->andWhere([
                    CallCenterRequest::tableName() . '.call_center_id' => $callCenter->id
                ]);

                foreach ($query->batch($batchSize) as $records) {
                    $recordIds = [];
                    foreach ($records as $record) {
                        if(!isset($recordIds[$record->call_center_type])) {
                            $recordIds[$record->call_center_type] = [];
                        }
                        $recordIds[$record->call_center_type][] = $record->id;
                    }
                    foreach ($recordIds as $type => $ids) {
                        try {
                            $requests = ReportCallCenterComparator::query($callCenter, $ids, $type);
                        } catch (InvalidParamException $e) {
                            continue;
                        }
                        $orders = ArrayHelper::merge($orders, $requests);
                    }
                }

                if (!empty($orderWithoutRequests)) {
                    $recordIds = [];
                    foreach ($orderWithoutRequests as $record) {
                        if(!isset($recordIds[$record->call_center_type])) {
                            $recordIds[$record->call_center_type] = [];
                        }
                        $recordIds[$record->call_center_type][] = $record->id;
                    }
                    foreach ($recordIds as $type => $ids) {
                        $totalCount = count($ids);
                        $offset = 0;
                        while ($offset < $totalCount) {
                            $batch = array_slice($ids, $offset, $batchSize);
                            $offset += $batchSize;
                            try {
                                $requests = ReportCallCenterComparator::query($callCenter, $batch, $type);
                            } catch (InvalidParamException $e) {
                                continue;
                            }

                            $orders = ArrayHelper::merge($orders, $requests);

                            $records = [];
                            foreach ($requests as $request) {
                                $records[] = $orderWithoutRequests[$request->order_id];
                                unset($orderWithoutRequests[$request->order_id]);
                            }
                        }
                    }
                }
            }

            $ids = $this->getIdFilter()->applyParams($params);

            if (!empty($ids) || !empty($this->getStatusFilter()->status)) {
                $orders = array_filter($orders, function ($item) use ($ids) {
                    if (!empty($ids) && !in_array($item->foreign_id, $ids)) {
                        return false;
                    }
                    if (!empty($this->getStatusFilter()->status) && $item->status != $this->getStatusFilter()->status) {
                        return false;
                    }
                    return true;
                });
            }
        }

        return $orders;
    }

    /**
     * @param array $params
     * @return $this
     */
    public function applyFilters($params)
    {
        $this->getDailyFilter()->apply($this->query, $params);
        if ($this->searchInBase) {
            $this->getIdFilter()->apply($this->query, $params);
            $this->getCallCenterFilter()->apply($this->query, $params);
            $this->getStatusFilter()->apply($this->query, $params);
        } else {
            $ids = $this->getIdFilter()->applyParams($params);
            if (!empty($ids)) {
                $query = clone $this->query;

                $query->andWhere([CallCenterRequest::tableName() . '.foreign_id' => $ids]);
                if ($query->count() == count($ids)) {
                    $this->query = $query;
                }
            }
        }
        return $this;
    }

    /**
     * @return DailyFilter
     */
    public function getDailyFilter()
    {
        if (is_null($this->dailyFilter)) {
            $this->dailyFilter = new DailyFilter();
            $this->dailyFilter->type = DailyFilter::TYPE_ORDER_CREATED_AT;
        }
        return $this->dailyFilter;
    }

    /**
     * @return FindAdcomboIdFilter
     */
    public function getIdFilter()
    {
        if (is_null($this->idFilter)) {
            $this->idFilter = new FindAdcomboIdFilter();
            $this->idFilter->type = FindAdcomboIdFilter::TYPE_CALL_CENTER_FULL_REQUEST_FOREIGN_ID;
        }
        return $this->idFilter;
    }

    /**
     * @return CallCenterFilter
     */
    public function getCallCenterFilter()
    {
        if (is_null($this->callCenterFilter)) {
            $this->callCenterFilter = new CallCenterFilter();
            $this->callCenterFilter->type = CallCenterFilter::TYPE_CALL_CENTER_FULL_REQUEST_CC_ID;
        }

        return $this->callCenterFilter;
    }

    /**
     * @return FindCallCenterStatusFilter
     */
    public function getStatusFilter()
    {
        if (is_null($this->statusFilter)) {
            $this->statusFilter = new FindCallCenterStatusFilter();
        }
        return $this->statusFilter;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return '';
    }

    /**
     * @return bool
     */
    public function getSearchInBase()
    {
        return $this->searchInBase;
    }
}
