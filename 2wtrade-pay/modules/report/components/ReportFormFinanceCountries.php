<?php
namespace app\modules\report\components;

use app\models\Country;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use app\modules\report\extensions\DataProvider;
use app\modules\report\extensions\Query;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * Class ReportFormFinanceCountries
 * @package app\modules\report\components
 */
class ReportFormFinanceCountries extends ReportForm
{
    /**
     * @var array
     */
    public static $currencies;

    /**
     * @var string Группировка по умолчанию
     */
    public $groupBy = self::GROUP_BY_COUNTRY;

    /**
     * @var array Маппинг статусов
     */
    protected $mapStatuses = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if (!is_array(self::$currencies)) {
            $currency = Country::find()
                ->joinWith(['currencyRate']);

            self::$currencies = ArrayHelper::map($currency->all(), 'id', 'currencyRate.rate');
        }
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'groupBy' => Yii::t('common', 'Группировка'),
            'Country' => Yii::t('common', 'Страна'),
        ];
    }

    /**
     * @param array $params
     * @return DataProvider
     */
    public function apply($params)
    {
        //$this->getDateFilter()->type = DateFilter::TYPE_DELIVERY_SENT_AT;

        $this->load($params);

        $this->query = new Query();
        $this->query->from([Order::tableName()]);
        $this->query->leftJoin(Country::tableName(), Order::tableName() . '.country_id=' . Country::tableName() . '.id');

        $this->applyFilters($params);

        $this->prepareCurrencyRate();
        $this->buildSelect();
        //print_r($this->query->createCommand()->sql);
        //$this->countries = Country::find()->collection();
        //$contriesArray = $this->countries;
        //print_r($contriesArray);
        $dataProvider = new DataProvider([
            'form' => $this,
            'query' => $this->query,
        ]);

        $dataProvider->pagination->pageSize = $dataProvider->pagination->totalCount;

        return $dataProvider;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $output = '';

        return $output;
    }

    /**
     * @return array
     */
    public function getMapStatuses()
    {
        if (empty($this->mapStatuses)) {
            $this->mapStatuses = [
                Yii::t('common', 'В процессе') => OrderStatus::getProcessDeliveryList(),
                Yii::t('common', 'Не выкуплено') => OrderStatus::getNotBuyoutList(),
                Yii::t('common', 'Выкуплено') => OrderStatus::getBuyoutList(),
                Yii::t('common', 'Деньги получены') => [
                    OrderStatus::STATUS_FINANCE_MONEY_RECEIVED
                ],
            ];

            $this->mapStatuses[Yii::t('common', 'Всего заказов')] = $this->getMapStatusesAsArray();
        }

        return $this->mapStatuses;
    }

    /**
     * @inheritdoc
     */
    protected function buildSelect()
    {
        $mapStatuses = $this->getMapStatuses();

        foreach ($mapStatuses as $name => $statuses) {
            $this->query->addInConditionCount(Order::tableName() . '.status_id', $statuses, $name);
        }
        $this->query->addCondition(Order::tableName() . '.country_id', 'Country');
        $this->query->addCondition(Order::tableName() . '.country_id', 'country_id');

        $statuses = $this->getMapStatusesAsArray();

        $case = $this->query->buildCaseCondition(
            '(' . Order::tableName() . '.price_total + ' . Order::tableName() . '.delivery )',
            Order::tableName() . '.status_id',
            $statuses
        );

        $this->query->addAvgCondition($case, Yii::t('common', 'Ср. чек КЦ'));

        $statuses = $mapStatuses[Yii::t('common', 'Выкуплено')];

        $case = $this->query->buildCaseCondition(
            '(' . Order::tableName() . '.price_total + ' . Order::tableName() . '.delivery)',
            Order::tableName() . '.status_id',
            $statuses
        );

        $this->query->addSumCondition($case, Yii::t('common', 'Доход по выкупам'));

        /*$case = $this->query->buildCaseCondition(
            '(' . Order::tableName() . '.price_total + ' . Order::tableName() . '.delivery)',
            Order::tableName() . '.status_id',
            $statuses
        );

        $this->query->addAvgCondition($case, Yii::t('common', 'Ср. доход по выкупам'));*/

        $this->query->addSumCondition(Order::tableName() . '.income', Yii::t('common', 'Цена за лиды'));
        $this->query->andWhere(['in', Order::tableName() . '.status_id', $this->getMapStatusesAsArray()]);
        $this->query->groupBy('country_id');

        return $this;
    }

    /**
     * @param array $params
     * @return ReportForm
     */
    protected function applyFilters($params)
    {
        $this->getDateFilter()->apply($this->query, $params);
        //$this->getCountryFilter()->apply($this->query, $params);
        //$this->countryFilter->all = true;
        $this->getProductFilter()->apply($this->query, $params);

        $this->getCallCenterFilter()->apply($this->query, $params);
        $this->getDeliveryFilter()->apply($this->query, $params);
        $this->getDeliveryPartnerFilter()->apply($this->query, $params);
        $this->getCalculatePercentFilter()->apply($this->query, $params);
        $this->getCalculateDollarFilter()->apply($this->query, $params);
        //$this->applyBaseFilters($params);

        return $this;
    }
}
