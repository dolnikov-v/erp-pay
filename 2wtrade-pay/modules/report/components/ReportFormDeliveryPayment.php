<?php
namespace app\modules\report\components;

use app\modules\deliveryreport\models\PaymentOrder;
use app\modules\report\components\filters\CountryDeliverySelectMultipleFilter;
use app\modules\report\components\filters\DeliveryPaymentMonthFilter;
use app\modules\report\components\filters\PaymentNameFilter;
use yii\data\ArrayDataProvider;
use Yii;

/**
 * Class ReportFormDeliveryPayment
 * @package app\modules\report\components\
 */
class ReportFormDeliveryPayment extends ReportForm
{
    /**
     * @var DeliveryPaymentMonthFilter
     */
    public $monthFilter = null;

    /**
     * @var PaymentNameFilter
     */
    public $paymentNameFilter = null;

    /**
     * @param array $params
     * @return ArrayDataProvider
     */
    public function apply($params)
    {
        $this->query = PaymentOrder::find()
            ->joinWith('delivery.country', false);

        $this->applyFilters($params);
        $this->query->orderBy(['paid_at' => SORT_DESC]);
        $data = $this->query->all();

        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
        ]);

        return $dataProvider;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return '';
    }

    /**
     * @return DeliveryPaymentMonthFilter
     */
    public function getMonthFilter()
    {
        if (is_null($this->monthFilter)) {
            $this->monthFilter = new DeliveryPaymentMonthFilter([
                'from' => Yii::$app->formatter->asMonth(strtotime(' - 1month', time())),
                'to' => Yii::$app->formatter->asMonth(time())
            ]);
        }
        return $this->monthFilter;
    }
    /**
     * @return PaymentNameFilter
     */
    public function getPaymentNameFilter()
    {
        if (is_null($this->paymentNameFilter)) {
            $this->paymentNameFilter = new PaymentNameFilter();
        }
        return $this->paymentNameFilter;
    }

    /**
     * @return CountryDeliverySelectMultipleFilter
     */
    public function getCountryDeliverySelectMultipleFilter()
    {
        if (is_null($this->countryDeliverySelectMultipleFilter)) {
            $this->countryDeliverySelectMultipleFilter = new CountryDeliverySelectMultipleFilter([
                'deliveryType' => CountryDeliverySelectMultipleFilter::DELIVERY_TYPE_DELIVERY,
            ]);
        }

        return $this->countryDeliverySelectMultipleFilter;
    }

    /**
     * @param array $params
     * @return $this
     */
    protected function applyFilters($params)
    {
        $this->getMonthFilter()->apply($this->query, $params);
        $this->getCountryDeliverySelectMultipleFilter()->apply($this->query, $params);
        $this->getPaymentNameFilter()->apply($this->query, $params);
        return $this;
    }
}
