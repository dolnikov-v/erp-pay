<?php

namespace app\modules\report\components;

use app\models\Country;
use app\models\User;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryApiClass;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\deliveryreport\models\DeliveryReport;
use app\modules\report\components\filters\DeliveryLastUpdateCountryDeliveryFilter;
use app\modules\report\extensions\Query;
use Yii;
use yii\data\ArrayDataProvider;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * Class ReportFormDeliveryLastUpdate
 * @package app\modules\report\components
 */
class ReportFormDeliveryLastUpdate extends ReportForm
{
    /**
     * @var DeliveryLastUpdateCountryDeliveryFilter
     */
    protected $deliveryLastUpdateCountryDeliveryFilter = null;

    /**
     * @param array $params
     * @return ArrayDataProvider
     */
    public function apply($params)
    {
        if (empty($params)) {
            $params['s']['country_ids'] = [Yii::$app->user->getCountry()->id];
            $params['s']['delivery_ids'] = array_keys(ArrayHelper::map(Delivery::getUserDeliveries(), 'id', 'name'));
        }

        $user = User::find()->where(['id' => \Yii::$app->user->id])->one();
        $query = DeliveryRequest::find()->joinWith(['delivery.country', 'delivery.apiClass']);
        $query->where([
            DeliveryRequest::tableName() . '.status' => [DeliveryRequest::STATUS_IN_PROGRESS, DeliveryRequest::STATUS_IN_LIST],
            Delivery::tableName() . '.active' => 1,
            Country::tableName() . '.active' => 1,
        ]);

        if (!$user->isSuperadmin) {
            $query->andWhere([Country::tableName() . '.id' => ArrayHelper::getColumn($user->userCountry, 'country_id')]);
            if ($user->userDelivery) {
                $query->andWhere([Delivery::tableName() . '.id' => ArrayHelper::getColumn($user->userDelivery, 'delivery_id')]);
            }
        }
        $query->groupBy([DeliveryRequest::tableName() . '.delivery_id']);

        $query->select([
            'country_id' => Country::tableName() . '.id',
            'delivery_id' => Delivery::tableName() . '.id',
            'country_name' => Country::tableName() . '.name',
            'delivery_name' => Delivery::tableName() . '.name',
            'last_api_update' => "CASE WHEN (". DeliveryApiClass::tableName().".can_tracking = 1 and ".Delivery::tableName().".can_tracking = 1) THEN MIN(" . DeliveryRequest::tableName() . '.cron_launched_at) ELSE NULL END',
            'last_report_update' => 'MIN(COALESCE(' . DeliveryRequest::tableName() . '.sent_at, ' . DeliveryRequest::tableName() . '.created_at))',
        ]);

        $this->getCountryDeliverySelectMultipleFilter()->apply($query, $params);

        $models = $query->createCommand()->queryAll();
        $models = ArrayHelper::index($models, 'delivery_id');

        $query = DeliveryReport::find()->joinWith(['country', 'delivery']);
        $query->where([
            Delivery::tableName() . '.active' => 1,
            Country::tableName() . '.active' => 1,
        ]);

        if (!$user->isSuperadmin) {
            $query->andWhere([Country::tableName() . '.id' => ArrayHelper::getColumn($user->userCountry, 'country_id')]);
            if ($user->userDelivery) {
                $query->andWhere([Delivery::tableName() . '.id' => ArrayHelper::getColumn($user->userDelivery, 'delivery_id')]);
            }
        }

        $query->groupBy([DeliveryReport::tableName() . '.delivery_id']);

        $query->select([
            'country_id' => Country::tableName() . '.id',
            'delivery_id' => Delivery::tableName() . '.id',
            'country_name' => Country::tableName() . '.name',
            'delivery_name' => Delivery::tableName() . '.name',
            'last_report_update' => "MAX(COALESCE(" . DeliveryReport::tableName() . '.period_to, ' . DeliveryReport::tableName() . '.created_at))',
            'last_fin_report_update' =>
                "MAX(
                    CASE WHEN " .DeliveryReport::tableName(). ".type = '" . DeliveryReport::TYPE_FINANCIAL . "'
                        THEN COALESCE(" . DeliveryReport::tableName() . '.period_to, ' . DeliveryReport::tableName() . '.created_at)
                        ELSE NULL
                    END
                )',
            'last_api_update' => new Expression('NULL'),
        ]);
        
        $this->getCountryDeliverySelectMultipleFilter()->apply($query, $params);

        $reportModels = $query->createCommand()->queryAll();

        foreach ($reportModels as $model) {
            if (isset($models[$model['delivery_id']])) {
                $models[$model['delivery_id']]['last_report_update'] = $model['last_report_update'];
                $models[$model['delivery_id']]['last_fin_report_update'] = $model['last_fin_report_update'];
            } else {
                $models[] = $model;
            }
        }

        usort($models, function ($a, $b) {
            if ($a['country_id'] == $b['country_id']) {
                return 0;
            }

            return (\Yii::t('common', $a['country_name']) < \Yii::t('common', $b['country_name'])) ? -1 : 1;
        });

        return new ArrayDataProvider([
            'allModels' => $models,
            'pagination' => false
        ]);
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return '';
    }

    /**
     * @return DeliveryLastUpdateCountryDeliveryFilter
     */
    public function getCountryDeliverySelectMultipleFilter()
    {
        if (is_null($this->deliveryLastUpdateCountryDeliveryFilter)) {
            $this->deliveryLastUpdateCountryDeliveryFilter = new DeliveryLastUpdateCountryDeliveryFilter();
        }

        return $this->deliveryLastUpdateCountryDeliveryFilter;
    }
}