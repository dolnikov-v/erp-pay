<?php
namespace app\modules\report\components;

use app\models\Product;
use app\modules\order\models\Order;
use app\modules\order\models\OrderProduct;
use app\modules\order\models\OrderStatus;
use app\modules\report\components\filters\DateFilter;
use app\modules\report\components\filters\DateFilterTopProduct;
use app\modules\report\components\filters\ProductFilter;
use app\modules\report\components\filters\TopProductFilter;
use app\modules\report\extensions\Query;
use app\modules\storage\models\Storage;
use app\modules\storage\models\StoragePart;
use yii\data\ArrayDataProvider;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class ReportFormProductRating
 * @package app\modules\report\components
 */
class ReportFormProductRating extends ReportForm
{
    /**
     * @var string Группировка по умолчанию
     */
    public $groupBy = self::GROUP_BY_PRODUCTS;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @return array
     */
    public function getMapStatuses()
    {
        return [
            'approve' => [
                OrderStatus::STATUS_CC_APPROVED,
                OrderStatus::STATUS_LOG_ACCEPTED,
                OrderStatus::STATUS_LOG_GENERATED,
                OrderStatus::STATUS_LOG_SET,
                OrderStatus::STATUS_LOG_PASTED,
                OrderStatus::STATUS_DELIVERY_ACCEPTED,
                OrderStatus::STATUS_DELIVERY_LEFT_WAREHOUSE,
                OrderStatus::STATUS_DELIVERY,
                OrderStatus::STATUS_DELIVERY_DENIAL,
                OrderStatus::STATUS_DELIVERY_RETURNED,
                OrderStatus::STATUS_DELIVERY_REJECTED,
                OrderStatus::STATUS_DELIVERY_LEFT_OUR_WAREHOUSE,
                OrderStatus::STATUS_DELIVERY_REDELIVERY,
                OrderStatus::STATUS_DELIVERY_LEFT_THEIR_WAREHOUSE,
                OrderStatus::STATUS_LOGISTICS_REJECTED,
                OrderStatus::STATUS_DELIVERY_DELAYED,
                OrderStatus::STATUS_DELIVERY_REFUND,
                OrderStatus::STATUS_LOGISTICS_ACCEPTED,
                OrderStatus::STATUS_DELIVERY_LOST,
                OrderStatus::STATUS_DELIVERY_PENDING,
                OrderStatus::STATUS_LOG_DEFERRED,
            ],
            'buyout' => OrderStatus::getBuyoutList(),
            'all' => OrderStatus::getAllList(),
        ];
    }

    /**
     * @param array $params
     * @return ArrayDataProvider
     */
    public function apply($params)
    {
        $this->load($params);

        $this->query = new Query();
        $this->query->from([OrderProduct::tableName()]);

        $this->query->innerJoin(Order::tableName(), Order::tableName() . '.id = ' . OrderProduct::tableName() . '.order_id');
        $this->query->innerJoin(Product::tableName(), Product::tableName() . '.id = ' . OrderProduct::tableName() . '.product_id');

        $statuses = $this->getMapStatuses();

        $country = Yii::$app->user->country->id;

        $this->query->where([
            Order::tableName() . '.status_id' => $statuses['all'],
            Order::tableName() . '.country_id' => $country,
        ]);

        $this->query->groupBy([OrderProduct::tableName() . '.product_id']);

        $this->buildSelect()->applyFilters($params);

        $models = $this->query
            ->indexBy('productId')
            ->all();

        $items = [];

        if ($products = ArrayHelper::getColumn($models, 'productId')) {

            $balances = StoragePart::find()
                ->select([
                    'productId' => StoragePart::tableName() . '.product_id',
                    'quantity' => 'sum(' . StoragePart::tableName() . '.balance)',
                ])
                ->joinWith(['storage'], false)
                ->where([
                    Storage::tableName() . '.country_id' => $country,
                    StoragePart::tableName() . '.product_id' => $products,
                ])
                ->groupBy(StoragePart::tableName() . '.product_id')
                ->asArray()
                ->indexBy('productId')
                ->all();

            $nextParts = StoragePart::find()
                ->select([
                    'productId' => StoragePart::tableName() . '.product_id',
                    'expectedAt' => StoragePart::tableName() . '.expected_at',
                ])
                ->joinWith(['storage'], false)
                ->where([
                    Storage::tableName() . '.country_id' => $country,
                    StoragePart::tableName() . '.product_id' => $products,
                ])
                ->andWhere(['not', [StoragePart::tableName() . '.expected' => null]])
                ->orderBy([StoragePart::tableName() . '.expected_at' => SORT_ASC])
                ->distinct()
                ->asArray()
                ->indexBy('productId')
                ->all();

            $query = Order::find()
                ->select([
                    'productId' => OrderProduct::tableName() . '.product_id',
                    'quantity' => 'sum(' . OrderProduct::tableName() . '.quantity)',
                ])
                ->joinWith(['orderProducts'], false)
                ->where([
                    Order::tableName() . '.country_id' => $country,
                    OrderProduct::tableName() . '.product_id' => $products,
                    Order::tableName() . '.status_id' => array_merge($statuses['buyout'])
                ])
                ->groupBy('productId');

            $this->getDateFilter()->apply($query, $params);

            $quantitiesByMonth = $query
                ->asArray()
                ->indexBy('productId')
                ->all();

            $rate = Yii::$app->user->country->currencyRate->rate;

            foreach ($models as $id => &$model) {

                $income = null;
                if ($model['count']) {
                    $income = ($model['avg'] / $rate)
                        * ($model['approveCount'] / $model['count'])
                        * ($model['buyoutCount'] / $model['count']);

                    $income = round($income, 2);
                }

                $quantityInStock = (isset($balances[$id])) ? $balances[$id]['quantity'] : 0;
                $expectedAt = (isset($nextParts[$id])) ? $nextParts[$id]['expectedAt'] : null;

                $quantityPerDay = ($quantityInStock === 0) ? 0 : null;
                if ($expectedAt) {
                    $days = ($expectedAt - time()) / 86400;
                    $quantityPerDay = round($quantityInStock / $days, 2);
                }

                $quantityByMonth = (isset($quantitiesByMonth[$id])) ? $quantitiesByMonth[$id]['quantity'] : null;
                if ($quantityByMonth) {
                    $quantityByMonth = round($quantityByMonth / 31, 2);
                }

                $items[] = [
                    'productName' => $model['productName'],
                    'income' => $income,
                    'quantityInStock' => $quantityInStock,
                    'quantityPerDay' => $quantityPerDay,
                    'quantityEnoughForMonth' => $quantityByMonth,
                ];

                ArrayHelper::multisort($items, 'income', SORT_DESC);

            } unset($model);
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $items,
            'pagination' => false,
        ]);

        return $dataProvider;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $output = '';

        return $output;
    }

    /**
     * @inheritdoc
     */
    protected function buildSelect()
    {
        $statuses = $this->getMapStatuses();

        $price = Order::tableName() . '.price_total + ' . Order::tableName() . '.delivery';

        $this->query->addSelect([
            'avg' => 'avg(if(' . Order::tableName() . '.status_id in(' . join(', ', $statuses['buyout']) . '), ' . $price . ', NULL))',
            'approveCount' => 'count(if(' . Order::tableName() . '.status_id in(' . join(', ', $statuses['approve']) . '), 1, NULL))',
            'buyoutCount' => 'count(if(' . Order::tableName() . '.status_id in(' . join(', ', $statuses['buyout']) . '), 1, NULL))',
            'count' => 'count(if(' . Order::tableName() . '.status_id in (' . join(', ', $statuses['all']) . '), 1, NUll))',
            'productName' => Product::tableName() . '.name',
            'productId' => Product::tableName() . '.id',
        ]);

        return $this;
    }

    /**
     * @param array $params
     * @return ReportForm
     */
    protected function applyFilters($params)
    {
        $this->getDateFilter()->apply($this->query, $params);
        $this->getProductFilter()->apply($this->query, $params);

        return $this;
    }

    /**
     * @return ProductFilter
     */
    public function getProductFilter()
    {
        if (is_null($this->productFilter)) {
            $this->productFilter = new TopProductFilter(['reportForm' => $this]);
        }

        return $this->productFilter;
    }

    /**
     * @inheritdoc
     */
    protected function prepareQuery()
    {
        $closure = $this->getQueryClosure();

        if ($closure instanceof \Closure) {
            $closure($this->query);
        }

        return $this;
    }

    /**
     * @return DateFilter
     */
    public function getDateFilter()
    {
        if (is_null($this->dateFilter)) {
            $this->dateFilter = new DateFilterTopProduct();
        }

        return $this->dateFilter;
    }
}
