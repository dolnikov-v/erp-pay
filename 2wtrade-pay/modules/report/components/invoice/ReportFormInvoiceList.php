<?php

namespace app\modules\report\components\invoice;

use app\components\data\QueryDataProvider;
use app\models\Country;
use app\models\CurrencyRate;
use app\modules\deliveryreport\models\PaymentOrder;
use app\modules\order\models\Order;
use app\modules\order\models\OrderFinancePrediction;
use app\modules\order\models\OrderStatus;
use app\modules\report\components\filters\CountryDeliverySelectMultipleFilter;
use app\modules\report\components\filters\DateFilter;
use app\modules\report\components\filters\InvoiceStatusFilter;
use app\modules\report\components\ReportForm;
use app\modules\report\models\Invoice;
use app\modules\report\models\InvoiceCurrency;
use app\modules\report\models\InvoiceOrder;
use app\modules\report\models\InvoicePayment;
use app\modules\report\widgets\invoice\ReportFilterInvoiceList;
use yii\base\Exception;
use yii\data\ActiveDataProvider;
use yii\db\Expression;

/**
 * Class ReportFormInvoiceList
 * @package app\modules\report\components
 */
class ReportFormInvoiceList extends ReportForm
{
    const FORM_NAME = 't';

    /**
     * @var null|InvoiceStatusFilter
     */
    protected $invoiceStatusFilter = null;

    /**
     * @param array $params
     * @return QueryDataProvider
     */
    public function apply($params)
    {
        $subQuery = InvoiceOrder::find()
            ->joinWith(['invoice invoice_sub', 'order.financePrediction'], false)
            ->groupBy([InvoiceOrder::tableName() . '.invoice_id']);
        $subQuery->where([InvoiceOrder::tableName() . '.invoice_id' => new Expression(Invoice::tableName() . '.id'), Order::tableName().'.status_id' => array_merge(OrderStatus::getOnlyMoneyReceivedList(), OrderStatus::getOnlyNotBuyoutDoneList())]);
        $select = [
            'income' => [],
            'costs' => [],
        ];
        $currencyFields = OrderFinancePrediction::getCurrencyFields();
        $currencyRateRelationMap = OrderFinancePrediction::currencyRateRelationMap();
        foreach (array_keys($currencyFields) as $field) {
            $relationName = $currencyRateRelationMap[$currencyFields[$field]];
            $subQuery->leftJoin("invoice_currency as {$relationName}_temp", "{$relationName}_temp.invoice_id = " . InvoiceOrder::tableName() . ".invoice_id and {$relationName}_temp.currency_id = " . OrderFinancePrediction::tableName() . ".{$currencyFields[$field]}");
            $type = OrderFinancePrediction::getFinanceTypeOfField($field) == OrderFinancePrediction::FINANCE_TYPE_INCOME ? 'income' : 'costs';
            $tableName = OrderFinancePrediction::tableName();
            $invoiceTableName = 'invoice_sub';
            $countryTableName = Country::tableName();
            $select[$type][] = "CASE WHEN {$tableName}.{$field} IS NOT NULL AND {$tableName}.{$field} != 0 THEN CASE WHEN {$relationName}_temp.rate IS NOT NULL THEN {$tableName}.{$field} / {$relationName}_temp.rate ELSE convertToCurrencyByCountry({$tableName}.{$field}, COALESCE({$tableName}.{$currencyFields[$field]}, {$countryTableName}.currency_id), {$invoiceTableName}.currency_id) END ELSE 0 END";
        }
        $subQuery->select([
            'SUM((' . implode('+', $select['income']) . ') - (' . implode('+', $select['costs']) . '))'
        ]);

        $subQueryPayments = InvoicePayment::find()
            ->joinWith([
                'payment',
                'invoice AS invoice_sub',
                'invoice.delivery.country'
            ], false)
            ->groupBy([InvoicePayment::tableName() . '.invoice_id']);
        $subQueryPayments->where([InvoicePayment::tableName() . '.invoice_id' => new Expression(Invoice::tableName() . '.id')]);
        $subQueryPayments->leftJoin(CurrencyRate::tableName() . ' payment_currency_rate_default', 'payment_currency_rate_default.currency_id = ' . PaymentOrder::tableName() . '.currency_id');
        $subQueryPayments->leftJoin(InvoiceCurrency::tableName() . ' payment_invoice_currency', 'payment_invoice_currency.invoice_id = ' . InvoicePayment::tableName() . '.invoice_id and payment_invoice_currency.currency_id = ' . PaymentOrder::tableName() . '.currency_id');
        $subQueryPayments->leftJoin(CurrencyRate::tableName(), CurrencyRate::tableName() . '.currency_id = ' . Country::tableName() . '.currency_id');
        $subQueryPayments->select([
            'SUM(IFNULL(' . PaymentOrder::tableName() . '.sum, 0) / COALESCE(payment_invoice_currency.rate, payment_currency_rate_default.rate, ' . CurrencyRate::tableName() . '.rate))',
        ]);

        $this->query = Invoice::find()->joinWith([
            'delivery.country'
        ])->orderBy([Invoice::tableName() . '.id' => SORT_DESC]);
        $this->applyFilters($params);

        $this->query->select([
            'sumTotalByOrders' => $subQuery,
            'sumTotalReceived' => $subQueryPayments,
            Invoice::tableName() . '.*'
        ]);

        return new QueryDataProvider([
            'query' => $this->query,
            'sort' => false,
        ]);
    }

    /**
     * @param array $params
     * @return $this
     */
    public function applyFilters($params)
    {
        $this->getDateFilter()->apply($this->query, $params);
        $this->getCountryDeliverySelectMultipleFilter()->apply($this->query, $params);
        $this->getInvoiceStatusFilter()->apply($this->query, $params);
        return $this;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return ReportFilterInvoiceList::widget([
            'form' => $form,
            'reportForm' => $this,
        ]);
    }

    /**
     * @return CountryDeliverySelectMultipleFilter
     */
    public function getCountryDeliverySelectMultipleFilter()
    {
        if (is_null($this->countryDeliverySelectMultipleFilter)) {
            $this->countryDeliverySelectMultipleFilter = new CountryDeliverySelectMultipleFilter([
                'countryType' => CountryDeliverySelectMultipleFilter::COUNTRY_TYPE_DELIVERY,
                'deliveryType' => CountryDeliverySelectMultipleFilter::DELIVERY_TYPE_DELIVERY,
                'formName' => $this->formName(),
            ]);
        }

        return $this->countryDeliverySelectMultipleFilter;
    }

    /**
     * @return DateFilter
     */
    public function getDateFilter()
    {
        if (is_null($this->dateFilter)) {
            $this->dateFilter = new DateFilter([
                'type' => DateFilter::TYPE_INVOICE_PERIOD,
                'showSelectType' => false,
                'formName' => $this->formName(),
                'from' => '',
                'to' => '',
            ]);
        }

        return $this->dateFilter;
    }

    /**
     * @return InvoiceStatusFilter|null
     */
    public function getInvoiceStatusFilter()
    {
        if (is_null($this->invoiceStatusFilter)) {
            $this->invoiceStatusFilter = new InvoiceStatusFilter([
                'formName' => $this->formName()
            ]);
        }

        return $this->invoiceStatusFilter;
    }

    /**
     * @return string
     */
    public function formName()
    {
        return self::FORM_NAME;
    }
}