<?php

namespace app\modules\report\components\invoice;


use app\models\Currency;
use app\models\CurrencyRate;
use app\modules\delivery\components\charges\ChargesCalculatorAbstract;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryContract;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\deliveryreport\models\DeliveryReport;
use app\modules\deliveryreport\models\DeliveryReportRecord;
use app\modules\report\components\ReportForm;
use app\modules\report\extensions\Query;
use app\modules\report\models\Invoice;
use app\modules\report\models\InvoiceCurrency;
use app\modules\report\models\InvoiceOrder;
use app\modules\order\models\Order;
use app\modules\order\models\OrderFinancePrediction;
use app\modules\order\models\OrderStatus;
use app\modules\report\components\filters\CountryDeliverySelectMultipleFilter;
use app\modules\report\components\filters\DateFilter;
use app\modules\report\widgets\invoice\ReportFilterInvoice;
use Aws\Ses\SesClient;
use Yii;
use yii\data\ArrayDataProvider;
use yii\db\ActiveQuery;
use yii\db\Exception;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * Вывод информации о возможном инвойсе
 * Class ReportFormInvoice
 * @package app\modules\report\components\invoice
 */
class ReportFormInvoice extends ReportForm
{

    const FINANCE_TYPE_INCOME = 'income';
    const FINANCE_TYPE_COST = 'cost';
    const ROUND_PRECISION = 2;
    const CURRENCY_RATE_ROUND_PRECISION = 7;

    const FILTER_TYPE_PERIOD = 'period';
    const FILTER_TYPE_FINANCE = 'finance';

    public $includeDoneOrders = false;

    public $roundPrecision = ReportFormInvoice::ROUND_PRECISION;

    public $currencyId = null;

    public $currencies = [];
    public $dates = [];
    public $rates = [];

    /**
     * @var null|Currency
     */
    protected $_currency = null;
    /**
     * @var null|CurrencyRate
     */
    protected $_currencyRate = null;

    /** @var  integer */
    public $country_id;

    /** @var  integer */
    public $delivery_id;

    /** @var  DeliveryReport */
    public $financeReport;

    /** @var  integer */
    public $financeReportId;

    /** @var  integer */
    public $activeFinanceReport;

    /** @var  string */
    public $type_filter;

    /** @var array */
    public $order_ids;

    /** @var null|array */
    protected $_customCurrencyRates = null;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['currencyId'], 'required'],
            ['includeDoneOrders', 'string'],
            ['roundPrecision', 'integer', 'min' => 0, 'max' => 10],
            [['currencies', 'dates', 'rates', 'type_filter', 'financeReportId'], 'safe'],
            ['currencyId', 'integer'],
            [
                'currencyId',
                'exist',
                'targetClass' => Currency::class,
                'targetAttribute' => 'id'
            ],
        ];
    }

    public function init()
    {
        parent::init();
        $this->currencyId = Currency::find()->where(['char_code' => 'USD'])->select('id')->scalar();
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getFinanceReports()
    {
        $query = DeliveryReport::find()
            ->where([
                'status' => [DeliveryReport::STATUS_COMPLETE, DeliveryReport::STATUS_IN_WORK],
                'type' => DeliveryReport::TYPE_FINANCIAL
            ]);

        if ($this->delivery_id) {
            $query->andWhere(['delivery_id' => $this->delivery_id]);
        }

        if ($this->country_id) {
            $query->andWhere(['country_id' => $this->country_id]);
        }

        return $query->all();
    }

    /**
     * @return array
     */
    public function getFinanceReportsCollection()
    {
        return ArrayHelper::map($this->getFinanceReports(), 'id', function ($model) {
            return $model['name'] . '(' . Yii::$app->formatter->asDate($model['period_from']) . ' - ' . Yii::$app->formatter->asDate($model['period_to']) . ')';
        });
    }

    /**
     * @param array $params
     * @return array
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\base\Exception
     */
    public function apply($params)
    {
        $this->load($params);
        $this->getCountryDeliverySelectMultipleFilter()->prepareDeliveryList($params);
        $deliveryIds = $this->getCountryDeliverySelectMultipleFilter()->getDeliveryIds($params);
        $deliveryId = array_shift($deliveryIds);
        $delivery = Delivery::find()->byId($deliveryId)->one();

        $output = [];
        $contracts = $delivery->deliveryContracts;
        $dateRange = $this->getDateFilter()->getFilteredTimestamps($params);
        $commonData = [
            'orders_count' => 0,
            'total_cod_sum' => 0,
        ];

        if (is_array($contracts)) {
            $this->removeInvoiceCurrencyFromCustomCurrencyRates();
            $this->prepareTemporaryRateTables();
            $commonCurrencyIds = [];
            foreach ($contracts as $contract) {
                if (!$contract->chargesCalculatorModel) {
                    continue;
                }
                $checkFrom = true;
                $checkTo = true;
                $contract->date_from = strtotime($contract->date_from);
                $contract->date_to = strtotime($contract->date_to);
                $contract->date_from = $contract->date_from ? strtotime('00:00:00', $contract->date_from) : 0;
                $contract->date_to = $contract->date_to ? strtotime('23:59:59', $contract->date_to) : 0;

                if (!empty($contract->date_from)) {
                    $checkFrom = (empty($dateRange['from']) || $dateRange['from'] <= $contract->date_from) && (empty($dateRange['to']) || $dateRange['to'] >= $contract->date_from);
                }

                if (!empty($contract->date_to)) {
                    $checkTo = (empty($dateRange['from']) || $dateRange['from'] <= $contract->date_to) && (empty($dateRange['to']) || $dateRange['to'] >= $contract->date_to);
                }

                if (!$checkFrom && !$checkTo) {
                    if (!empty($dateRange['from'])) {
                        $checkFrom = (empty($contract->date_from) || $contract->date_from <= $dateRange['from']) && (empty($contract->date_to) || $contract->date_to >= $dateRange['from']);
                    }

                    if (!empty($dateRange['to'])) {
                        $checkTo = (empty($contract->date_from) || $contract->date_from <= $dateRange['to']) && (empty($contract->date_to) || $contract->date_to >= $dateRange['to']);
                    }
                }

                if (!$checkTo && !$checkFrom) {
                    continue;
                }

                $type_filter = $this->getInvoiceFilterType($params);

                if ($type_filter == ReportFormInvoice::FILTER_TYPE_FINANCE) {
                    if (!$this->financeReportId) {
                        throw new \yii\base\Exception(Yii::t('common', 'Ошибка определения ид финансового отчёта (financeReportId ) из параметров'));
                    } else {
                        $this->setCurrentFinanceReport($params);
                    }
                }

                $result = $this->getFinanceData($delivery, $contract, $params);

                $commonData['total_cod_sum'] += $result['totalData']['total_cod_sum'];
                $commonData['orders_count'] += $result['totalData']['orders_count'];
                $commonCurrencyIds = array_unique(array_merge($commonCurrencyIds, $result['currencies']));

                $dataProvider = new ArrayDataProvider([
                    'allModels' => $result['rows'],
                    'sort' => false,
                ]);
                $output[$contract->id] = [
                    'dataProvider' => $dataProvider,
                    'totalData' => $result['totalData'],
                    'contract' => $contract
                ];
            }
            /**
             * @var CurrencyRate[] $rates
             */
            $rates = CurrencyRate::find()
                ->select([
                    'currency_id',
                    'rate',
                    'updated_at'
                ])
                ->where(['currency_id' => $commonCurrencyIds])->andWhere([
                    '!=',
                    CurrencyRate::tableName() . '.currency_id',
                    $this->currencyId
                ])->all();
            foreach ($rates as $rate) {
                if (!in_array($rate->currency_id, $this->currencies)) {
                    $this->currencies[] = $rate->currency_id;
                    $this->rates[] = $rate->rate / $this->getCurrencyRate()->rate;
                    $this->dates[] = Yii::$app->formatter->asDate(time());
                }
            }
            foreach (array_keys($output) as $key) {
                if (count($output) > 1 && count($output[$key]['dataProvider']->allModels) == 0) {
                    unset($output[$key]);
                }
            }
            $this->dropTemporaryRateTables();
        }

        return [
            'output' => $output,
            'delivery' => $delivery,
            'commonData' => $commonData,
        ];
    }

    /**
     * Получение предварительной информации об инвойсе для конкретной КС и контракта
     * @param Delivery $delivery
     * @param DeliveryContract $contract
     * @param array $params
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     */
    protected function getFinanceData($delivery, $contract, $params)
    {
        $dateRange = $this->getDateFilter()->getFilteredTimestamps($params);

        $customCurrencyRates = $this->getCustomCurrencyRates();
        if (empty($customCurrencyRates) || empty($defaultCurrencyRate = $customCurrencyRates[$delivery->country->currency_id])) {
            $defaultCurrencyRate = round($delivery->country->currencyRate->rate / $this->getCurrencyRate()->rate, static::CURRENCY_RATE_ROUND_PRECISION);
        }

        $currencyRateRelations = OrderFinancePrediction::currencyRateRelationMap();
        $this->query = OrderFinancePrediction::find()
            ->joinWith([
                'order.country',
                'order.deliveryRequest',
                'invoiceOrder',
            ], false);

        foreach ($currencyRateRelations as $columnName => $relationName) {
            $this->query->leftJoin($relationName . '_temp', "{$relationName}_temp.currency_id = " . OrderFinancePrediction::tableName() . ".{$columnName}");
        }

        $this->query->where([Order::tableName() . '.status_id' => $this->getStatuses()]);
        if (!$this->includeDoneOrders) {
            // если не стоит галка Учитывать завершенные заказы, то берем которые еще не в инвойсах
            $this->query->andWhere(['is', InvoiceOrder::tableName() . '.invoice_id', null]);
        }
        $this->applyFilters($params);

        //по фильтру по финансовому отчету - период не учитывать
        if ($this->type_filter != ReportFormInvoice::FILTER_TYPE_FINANCE) {
            if ($contract->date_from > 0) {
                $this->query->andWhere([
                    '>=',
                    $this->getDateFilter()->getFilteredAttribute($params),
                    $contract->date_from
                ]);
            }

            if ($contract->date_to > 0) {
                $this->query->andWhere([
                    '<=',
                    $this->getDateFilter()->getFilteredAttribute($params),
                    $contract->date_to
                ]);
            }
        }

        if ($this->type_filter == ReportFormInvoice::FILTER_TYPE_FINANCE) {
            $this->query->innerJoin(DeliveryReportRecord::tableName(), DeliveryReportRecord::tableName() . '.order_id=' . Order::tableName() . '.id');
            $this->query->andWhere(['<>', 'record_status', DeliveryReportRecord::STATUS_DELETED]);
            $this->query->andWhere(['delivery_report_id' => $this->financeReportId]);
        }

        $periodFrom = $contract->date_from > 0 && $contract->date_from > $dateRange['from'] ? $contract->date_from : $dateRange['from'];
        $periodTo = $contract->date_to > 0 && $contract->date_to < $dateRange['to'] ? $contract->date_to : $dateRange['to'];

        $months = static::getMonthsWithoutInvoices($contract->delivery_id, $periodFrom, $periodTo);

        $result = Yii::$app->db->useMaster(function () use ($contract, $defaultCurrencyRate, $months) {
            return self::prepareRows($this->query, $contract, $this->currencyId, $defaultCurrencyRate, $this->getCustomCurrencyRates(), $this->includeDoneOrders, count($months));
        });

        return $result;
    }

    /**
     * Поиск месяцев в указанном промежутке, для которых не сгенерено ни одного инвойса для определенной КС
     * @param integer $deliveryId
     * @param integer $periodFrom // timestamp
     * @param integer $periodTo // timestamp
     * @param null|integer $invoiceId // Ид инвойса, который исключается при подсчете пустых месяцев
     * @return array
     */
    public static function getMonthsWithoutInvoices($deliveryId, $periodFrom, $periodTo, $invoiceId = null)
    {
        $query = Invoice::find()->where(['delivery_id' => $deliveryId]);

        $condition = [];
        if ($periodFrom) {
            $condition[] = [
                'and',
                ['<=', new Expression('IFNULL(' . Invoice::tableName() . '.period_from, 0)'), $periodFrom],
                [
                    '>=',
                    new Expression('IFNULL(' . Invoice::tableName() . '.period_to, UNIX_TIMESTAMP())'),
                    $periodFrom
                ],
            ];
        } else {
            $periodFrom = 0;
        }

        if ($periodTo) {
            $condition[] = [
                'and',
                ['<=', new Expression('IFNULL(' . Invoice::tableName() . '.period_from, 0)'), $periodTo],
                [
                    '>=',
                    new Expression('IFNULL(' . Invoice::tableName() . '.period_to, UNIX_TIMESTAMP())'),
                    $periodTo
                ],
            ];
        } else {
            $periodTo = time();
        }

        if (count($condition) > 1) {
            array_unshift($condition, 'or');
        } elseif ($condition) {
            $condition = $condition[0];
        }
        if ($condition) {
            $query->andWhere($condition);
        }
        /**
         * @var Invoice[] $invoices
         */
        $invoices = $query->select(['period_from', 'period_to', 'id'])->all();

        $months = [];
        $periodFrom = strtotime('first day of this month', $periodFrom);
        while ($periodFrom <= $periodTo) {
            $months[] = date('Y-m', $periodFrom);
            $periodFrom = strtotime("+1month", $periodFrom);
        }
        $months = array_unique($months);

        foreach ($invoices as $invoice) {
            if ($invoice->id == $invoiceId) {
                continue;
            }
            if (!$invoice->period_from) {
                $invoice->period_from = 0;
            }
            if (!$invoice->period_to) {
                $invoice->period_to = time();
            }
            $invoice->period_from = strtotime('first day of this month', $invoice->period_from);
            while ($invoice->period_from <= $invoice->period_to) {
                $month = date('Y-m', $invoice->period_from);
                $index = array_search($month, $months);
                if (!is_null($index)) {
                    unset($months[$index]);
                }
                $invoice->period_from = strtotime('+1month', $invoice->period_from);
            }
        }

        return $months;
    }

    /**
     * @throws Exception
     */
    protected function prepareTemporaryRateTables()
    {
        $currencyRateRelations = OrderFinancePrediction::currencyRateRelationMap();
        foreach ($currencyRateRelations as $relationName) {
            Yii::$app->db->createCommand("CREATE TEMPORARY TABLE {$relationName}_temp (currency_id INTEGER, rate DOUBLE)")
                ->execute();
        }
        $batchInsert = [];
        foreach ($this->getCustomCurrencyRates() as $currencyId => $rate) {
            $batchInsert[] = [
                'currency_id' => $currencyId,
                'rate' => $rate
            ];
        }
        if (!empty($batchInsert)) {
            foreach ($currencyRateRelations as $relationName) {
                Yii::$app->db->createCommand()->batchInsert("{$relationName}_temp", [
                    'currency_id',
                    'rate',
                ], $batchInsert)->execute();
            }
        }
    }

    /**
     * Удаление временных таблиц с валютами
     * @throws Exception
     */
    protected function dropTemporaryRateTables()
    {
        $currencyRateRelations = OrderFinancePrediction::currencyRateRelationMap();
        foreach ($currencyRateRelations as $relationName) {
            Yii::$app->db->createCommand("DROP TABLE {$relationName}_temp")
                ->execute();
        }
    }

    /**
     * @param array $params
     * @return $this
     */
    public function applyFilters($params)
    {
        $this->getCountryDeliverySelectMultipleFilter()->apply($this->query, $params);

        //по фильтру по финансовому отчету - период не учитывать
        if ($this->type_filter != ReportFormInvoice::FILTER_TYPE_FINANCE) {
            $this->getDateFilter()->apply($this->query, $params);
        }
        return $this;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     * @throws \Exception
     */
    public function restore($form)
    {
        return ReportFilterInvoice::widget([
            'reportForm' => $this,
            'form' => $form,
        ]);
    }

    /**
     * @param ActiveQuery|Query $query
     * @param DeliveryContract $contract
     * @param integer $invoiceCurrencyId
     * @param $defaultCurrencyRate
     * @param array $currencyRates
     * @param $includeDoneOrders
     * @param integer $monthlyChargesCount
     * @return array
     * @throws Exception
     * @throws NotFoundHttpException
     */
    public static function prepareRows($query, $contract, $invoiceCurrencyId, $defaultCurrencyRate, $currencyRates, $includeDoneOrders, $monthlyChargesCount)
    {
        $invoiceCurrencyRate = CurrencyRate::find()
            ->select('rate')
            ->where(['currency_id' => $invoiceCurrencyId])
            ->scalar();
        $fields = self::getDisplayedFields($contract, $invoiceCurrencyId, $defaultCurrencyRate, $includeDoneOrders);

        $select = [
            'orders_count' => 'COUNT(DISTINCT ' . OrderFinancePrediction::tableName() . '.order_id)',
        ];
        foreach ($fields as $name => $properties) {
            if (isset($properties['query'])) {
                $select[$name] = $properties['query'];
            }
            if (isset($properties['count_query'])) {
                $select[$name . '_count'] = $properties['count_query'];
            }
            if (isset($properties['query_usd'])) {
                $select[$name . '_usd'] = $properties['query_usd'];
            }
            if (isset($properties['currency_ids'])) {
                $select[$name . '_currency_ids'] = $properties['currency_ids'];
            }
        }
        $query->select($select);
        $items = $query->createCommand()->queryOne();
        $totalData = [
            'total_sum' => 0,
            'total_sum_usd' => 0,
            'orders_count' => $items['orders_count'],
            'total_cod_sum' => 0,
        ];

        $rows = [];
        $commonCurrencyIds = [];
        foreach ($fields as $name => $properties) {
            if ($properties['calculating_type']['type'] == ChargesCalculatorAbstract::CALCULATING_TYPE_MONTHLY_CHARGE) {
                if (isset($properties['calculating_type']['currency_id']) && !isset($properties['calculating_type']['currency_rate'])) {
                    $currencyRate = CurrencyRate::find()
                        ->where(['currency_id' => $properties['calculating_type']['currency_id']])
                        ->one();
                    if (!$currencyRate) {
                        throw new NotFoundHttpException(Yii::t('common', 'Не удалось определить курс валюты для валюты #{currency}', ['currency' => $properties['calculating_type']['currency_id']]));
                    }
                    $fields[$name]['calculating_type']['currency_rate'] = $properties['calculating_type']['currency_rate'] = $currencyRate->rate;
                }
                $items[$name] = $properties['calculating_type']['charge'] * $monthlyChargesCount;
                $items[$name . '_usd'] = $items[$name] / ($currencyRates[$properties['calculating_type']['currency_id']] ?? isset($properties['calculating_type']['currency_rate']) ? $properties['calculating_type']['currency_rate'] / $invoiceCurrencyRate : null ?? $defaultCurrencyRate);
                $commonCurrencyIds[] = $properties['calculating_type']['currency_id'];
            } else {
                if (!isset($items[$name . '_usd'])) {
                    $items[$name . '_usd'] = round($items[$name] / $defaultCurrencyRate, self::ROUND_PRECISION);
                } else {
                    $items[$name . '_usd'] = round($items[$name . '_usd'], self::ROUND_PRECISION);
                }
                // вытаскиваем ид валют, чтобы вывести пользователю их список с рейтами, если он их сам не вбил
                if (isset($items[$name . '_currency_ids']) && !empty($items[$name . '_currency_ids'])) {
                    $currencyIds = explode(',', $items[$name . '_currency_ids']);
                    $commonCurrencyIds = array_merge($commonCurrencyIds, $currencyIds);
                }
            }

            if ($properties['finance_type'] == OrderFinancePrediction::FINANCE_TYPE_INCOME) {
                $totalData['total_cod_sum'] += $items[$name];
            }
        }

        foreach ($fields as $name => $properties) {
            if (empty($items[$name . '_usd'])) {
                continue;
            }
            if ($name != OrderFinancePrediction::COLUMN_VAT_INCLUDED) {
                // не включаем значения поля "НДС включен" в итоги
                $totalData['total_sum'] += $items[$name] * ($properties['finance_type'] == OrderFinancePrediction::FINANCE_TYPE_COST ? -1 : 1);
                $totalData['total_sum_usd'] += $items[$name . '_usd'] * ($properties['finance_type'] == OrderFinancePrediction::FINANCE_TYPE_COST ? -1 : 1);
            }
            $rows[$name] = [
                'local_sum' => $items[$name],
                'usd_sum' => $items[$name . '_usd'],
                'label' => $properties['label'],
                'quantity' => '',
                'formula' => '',
                'finance_type' => $properties['finance_type'],
                'calculating_type' => $properties['calculating_type']['type'],
            ];
            if (isset($properties['calculating_type']['type'])) {
                switch ($properties['calculating_type']['type']) {
                    case ChargesCalculatorAbstract::CALCULATING_TYPE_PERCENT:
                        $fieldSum = 0;
                        foreach ($properties['calculating_type']['fields'] as $sumFieldName) {
                            $fieldSum += $items[$sumFieldName . '_usd'];
                        }
                        $rows[$name]['quantity'] = $fieldSum;
                        $rows[$name]['formula'] = $properties['calculating_type']['visible_formula'];
                        break;
                    case ChargesCalculatorAbstract::CALCULATING_TYPE_NUMBER:
                        if (isset($items[$name . '_count'])) {
                            $rows[$name]['quantity'] = $items[$name . '_count'];
                            $rows[$name]['formula'] = $items[$name . '_usd'] / $rows[$name]['quantity'];
                        }
                        break;
                    case ChargesCalculatorAbstract::CALCULATING_TYPE_MONTHLY_CHARGE:
                        $rows[$name]['quantity'] = $monthlyChargesCount;
                        $rows[$name]['formula'] = $properties['calculating_type']['charge'] / ($currencyRates[$properties['calculating_type']['currency_id']] ?? isset($properties['calculating_type']['currency_rate']) ? $properties['calculating_type']['currency_rate'] / $invoiceCurrencyRate : null ?? $defaultCurrencyRate);
                        break;
                }
            }
        }

        return [
            'rows' => $rows,
            'totalData' => $totalData,
            'currencies' => array_unique($commonCurrencyIds),
        ];
    }

    /**
     * @param DeliveryContract $contract
     * @param integer $invoiceCurrencyId
     * @param float $defaultCurrencyRate
     * @param bool $includeDoneOrders
     * @return array
     */
    public static function getDisplayedFields($contract, $invoiceCurrencyId, $defaultCurrencyRate, $includeDoneOrders = false)
    {
        $fields = [
            OrderFinancePrediction::COLUMN_PRICE_COD => [
                'query' => 'SUM(CASE WHEN ' . Order::tableName() . '.status_id in (' . implode(',', array_merge(OrderStatus::getOnlyBuyoutList(), $includeDoneOrders ? OrderStatus::getOnlyMoneyReceivedList() : [])) . ') THEN ' . OrderFinancePrediction::tableName() . ".price_cod ELSE 0 END)",
                'query_usd' => 'SUM(CASE WHEN ' . Order::tableName() . '.status_id in ("' . implode('","', array_merge(OrderStatus::getOnlyBuyoutList(), $includeDoneOrders ? OrderStatus::getOnlyMoneyReceivedList() : [])) . '") THEN ' . static::prepareSelectQueryForPriceField('price_cod', 'price_cod_currency_id', OrderFinancePrediction::TABLE_PRICE_COD_CURRENCY_RATE, $invoiceCurrencyId, $defaultCurrencyRate) . " ELSE 0 END)",
                'count_query' => 'COUNT(DISTINCT CASE WHEN ' . Order::tableName() . '.status_id in (' . implode(',', array_merge(OrderStatus::getOnlyBuyoutList(), $includeDoneOrders ? OrderStatus::getOnlyMoneyReceivedList() : [])) . ') THEN ' . OrderFinancePrediction::tableName() . '.order_id ELSE NULL END)',
                'label' => Yii::t('common', 'Собрано денег'),
            ],
            OrderFinancePrediction::COLUMN_PRICE_STORAGE => [
                'label' => Yii::t('common', 'Плата за хранение'),
            ],
            OrderFinancePrediction::COLUMN_PRICE_FULFILMENT => [
                'label' => Yii::t('common', 'Плата за обслуживание'),
            ],
            OrderFinancePrediction::COLUMN_PRICE_PACKING => [
                'label' => Yii::t('common', 'Плата за упаковывание'),
            ],
            OrderFinancePrediction::COLUMN_PRICE_PACKAGE => [
                'label' => Yii::t('common', 'Плата за упаковочный материал'),
            ],
            OrderFinancePrediction::COLUMN_PRICE_ADDRESS_CORRECTION => [
                'label' => Yii::t('common', 'Плата за коррекцию адреса'),
            ],
            OrderFinancePrediction::COLUMN_PRICE_DELIVERY => [
                'label' => Yii::t('common', 'Плата за доставку'),
            ],
            OrderFinancePrediction::COLUMN_PRICE_REDELIVERY => [
                'label' => Yii::t('common', 'Плата за повторную доставку'),
            ],
            OrderFinancePrediction::COLUMN_PRICE_DELIVERY_BACK => [
                'label' => Yii::t('common', 'Плата за возвращение'),
            ],
            OrderFinancePrediction::COLUMN_PRICE_DELIVERY_RETURN => [
                'label' => Yii::t('common', 'Плата за обратную доставку'),
            ],
            OrderFinancePrediction::COLUMN_PRICE_COD_SERVICE => [
                'label' => Yii::t('common', 'Плата за наложенный платеж'),
            ],
            OrderFinancePrediction::COLUMN_PRICE_VAT => [
                'label' => Yii::t('common', 'НДС'),
            ],
            OrderFinancePrediction::COLUMN_VAT_INCLUDED => [
                'label' => Yii::t('common', 'НДС включен в стоимость'),
                'related' => OrderFinancePrediction::COLUMN_PRICE_VAT,
            ],
        ];

        $currencyFields = OrderFinancePrediction::getCurrencyFields();
        $currencyRateRelationMap = OrderFinancePrediction::currencyRateRelationMap();
        foreach (array_keys($fields) as $key) {


            $fields[$key]['finance_type'] = OrderFinancePrediction::getFinanceTypeOfField($key);

            $fields[$key]['calculating_type'] = $contract->chargesCalculator->getCalculatingTypeForField($key);

            $relatedKey = $fields[$key]['related'] ?? $key;
            if ($key != $relatedKey) {
                $fields[$key]['calculating_type'] = $fields[$relatedKey]['calculating_type'];
            }

            if ($fields[$key]['calculating_type']['type'] == ChargesCalculatorAbstract::CALCULATING_TYPE_MONTHLY_CHARGE) {
                continue;
            }
            if (!$fields[$key]['calculating_type']['type'] == ChargesCalculatorAbstract::CALCULATING_TYPE_NUMBER) {
                unset($fields[$key]['count_query']);
            }
            if (isset($fields[$key]['calculating_type']['query'])) {
                $fields[$key]['count_query'] = $fields[$key]['calculating_type']['query'];
            }

            if (!isset($fields[$key]['query'])) {
                $fields[$key]['query'] = 'SUM(' . OrderFinancePrediction::tableName() . ".{$key})";
            }

            if (!isset($fields[$key]['query_usd'])) {
                $fields[$key]['query_usd'] = 'SUM(' . static::prepareSelectQueryForPriceField($key, $currencyFields[$relatedKey], $currencyRateRelationMap[$currencyFields[$relatedKey]], $invoiceCurrencyId, $defaultCurrencyRate) . ")";
            }

            if (!isset($fields[$key]['count_query'])) {
                $fields[$key]['count_query'] = 'COUNT(DISTINCT CASE WHEN (' . OrderFinancePrediction::tableName() . ".{$key} <> 0 and " . OrderFinancePrediction::tableName() . ".{$key} is not null) THEN " . OrderFinancePrediction::tableName() . '.order_id ELSE NULL END)';
            }

            if (!isset($fields[$key]['currency_ids'])) {
                $fields[$key]['currency_ids'] = "GROUP_CONCAT(DISTINCT IFNULL(" . OrderFinancePrediction::tableName() . ".{$currencyFields[$relatedKey]}, {$contract->delivery->country->currency_id}))";
            }
        }
        return $fields;
    }

    /**
     * @param $field
     * @param $currencyIdField
     * @param $currencyRateTableName
     * @param $invoiceCurrencyId
     * @param $defaultCurrencyRate
     * @return string
     */
    protected static function prepareSelectQueryForPriceField($field, $currencyIdField, $currencyRateTableName, $invoiceCurrencyId, $defaultCurrencyRate)
    {
        $tableName = OrderFinancePrediction::tableName();
        return "CASE WHEN {$tableName}.{$field} IS NOT NULL AND {$tableName}.{$field} != 0 THEN CASE WHEN {$currencyRateTableName}_temp.rate IS NOT NULL THEN {$tableName}.{$field} / {$currencyRateTableName}_temp.rate WHEN {$tableName}.{$currencyIdField} IS NOT NULL THEN convertToCurrencyByCountry({$tableName}.{$field}, {$tableName}.{$currencyIdField}, {$invoiceCurrencyId}) ELSE {$tableName}.{$field} / {$defaultCurrencyRate} END ELSE 0 END";
    }

    /**
     * @return CountryDeliverySelectMultipleFilter
     */
    public function getCountryDeliverySelectMultipleFilter()
    {
        if (is_null($this->countryDeliverySelectMultipleFilter)) {
            $this->countryDeliverySelectMultipleFilter = new CountryDeliverySelectMultipleFilter([
                'multipleCountrySelect' => false,
                'multipleDeliverySelect' => false,
                'country_ids' => Yii::$app->user->country->id,
            ]);
        }

        return $this->countryDeliverySelectMultipleFilter;
    }

    /**
     * @return DateFilter
     */
    public function getDateFilter()
    {
        if (is_null($this->dateFilter)) {
            $this->dateFilter = new DateFilter([
                'type' => DateFilter::TYPE_DELIVERY_SENT_AT_NOT_EMPTY,
                'showSelectType' => false,
            ]);
        }

        return $this->dateFilter;
    }

    /**
     * @return array
     */
    public function getStatuses()
    {
        return array_merge(
            OrderStatus::getOnlyBuyoutList(),
            OrderStatus::getNotBuyoutInDeliveryProcessList(),
            $this->includeDoneOrders ? OrderStatus::getOnlyMoneyReceivedList() : [],
            $this->includeDoneOrders ? OrderStatus::getOnlyNotBuyoutDoneList() : []);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'includeDoneOrders' => Yii::t('common', 'Учитывать завершенные заказы'),
            'rates' => Yii::t('common', 'Курс'),
            'currencies' => Yii::t('common', 'Валюта'),
            'dates' => Yii::t('common', 'Дата'),
            'roundPrecision' => Yii::t('common', 'Точность округления (знаков)'),
            'financeReportId' => Yii::t('common', 'Финансовый отчет'),
            'currencyId' => Yii::t('common', 'Валюта инвойса'),
        ];
    }

    /**
     * @param $contractId
     * @param $from
     * @param $to
     * @param $paymentDate
     * @param $requisiteDeliveryId
     * @param array $params
     * @throws Exception
     * @throws \Throwable
     * @return Invoice
     */
    public function createInvoice(int $contractId, ?int $from, ?int $to, ?int $paymentDate, int $requisiteDeliveryId, $params = [])
    {
        if (!$requisiteDeliveryId) {
            throw new Exception(Yii::t('common', 'Невозможно сгенерировать инвойс, отсутствуют реквизиты КС.'));
        }

        $contract = DeliveryContract::find()->where(['id' => $contractId])->one();

        if (!$contract->delivery->legal_name || empty($contract->delivery->legal_name)) {
            throw new Exception(Yii::t('common', 'Невозможно сгенерировать инвойс, отсутствует юридическое название КС.'));
        }

        $transaction = Yii::$app->db->beginTransaction();
        try {
            if ($paymentDate) {
                $paymentDate = Yii::$app->formatter->asTimestamp($paymentDate);
            }
            $invoice = new Invoice([
                'delivery_id' => $contract->delivery_id,
                'contract_id' => $contract->id,
                'period_from' => $from,
                'period_to' => $to,
                'user_id' => isset(Yii::$app->user) ? Yii::$app->user->id : null,
                'name' => strtoupper(date('MdYHis')),
                'payment_date' => $paymentDate,
                'round_precision' => empty($this->roundPrecision) && $this->roundPrecision !== 0 ? static::ROUND_PRECISION : $this->roundPrecision,
                'requisite_delivery_id' => $requisiteDeliveryId,
                'currency_id' => $this->currencyId
            ]);
            if (!$invoice->save()) {
                throw new Exception($invoice->getFirstErrorAsString());
            }

            $monthOfDebts = $invoice->checkInvoiceDebts();

            $query = OrderFinancePrediction::find()
                ->select([
                    'id' => OrderFinancePrediction::tableName() . '.order_id',
                    'month' => 'FROM_UNIXTIME(COALESCE(' . DeliveryRequest::tableName() . '.second_sent_at,'
                        . DeliveryRequest::tableName() . '.sent_at,'
                        . DeliveryRequest::tableName() . '.created_at), "%Y-%m")'
                ])
                ->joinWith(['order.deliveryRequest', 'invoiceOrder'], false)
                ->where([DeliveryRequest::tableName() . '.delivery_id' => $contract->delivery_id])
                ->andWhere([Order::tableName() . '.status_id' => $this->getStatuses()])
                ->andWhere(['is', InvoiceOrder::tableName() . '.invoice_id', null]);
            if ($from) {
                $query->andWhere([
                    '>=',
                    $this->getDateFilter()->getFilteredAttribute(),
                    $from
                ]);
            }
            if ($to) {
                $query->andWhere([
                    '<=',
                    $this->getDateFilter()->getFilteredAttribute(),
                    $to
                ]);
            }

            $orders = ArrayHelper::map($query->asArray()->all(), 'id', 'month');

            if (empty($orders)) {
                throw new Exception(Yii::t('common', 'Невозможно сгенерировать инвойс, отсутствуют записи удовлетворяющие указанным параметрам.'));
            }

            if (empty($defaultCurrencyRate = $this->getCustomCurrencyRates()[$contract->delivery->country->currency_id])) {
                $defaultCurrencyRate = round($contract->delivery->country->currencyRate->rate / $this->getCurrencyRate()->rate, static::CURRENCY_RATE_ROUND_PRECISION);
            }

            $fields = static::getDisplayedFields($contract, $this->currencyId, $defaultCurrencyRate, $this->includeDoneOrders);
            $select = [];

            foreach ($fields as $name => $properties) {
                if (isset($properties['currency_ids'])) {
                    $select[$name . '_currency_ids'] = $properties['currency_ids'];
                }
            }

            $query->select($select);
            $items = $query->createCommand()->queryOne();

            $commonCurrencyIds = [];
            foreach ($fields as $name => $properties) {
                // вытаскиваем ид валют, чтобы вывести пользователю их список с рейтами, если он их сам не вбил
                if (isset($items[$name . '_currency_ids']) && !empty($items[$name . '_currency_ids'])) {
                    $currencyIds = explode(',', $items[$name . '_currency_ids']);
                    $commonCurrencyIds = array_merge($commonCurrencyIds, $currencyIds);
                    $commonCurrencyIds = array_unique($commonCurrencyIds);
                }
            }

            /**
             * @var CurrencyRate[] $rates
             */
            $rates = CurrencyRate::find()
                ->select([
                    'currency_id',
                    'rate',
                    'updated_at'
                ])
                ->where(['currency_id' => $commonCurrencyIds])->andWhere([
                    '!=',
                    'currency_id',
                    $this->currencyId
                ])->all();

            $currencies = [];
            foreach ($rates as $rate) {
                $currencies[$rate->currency_id] = [
                    'currency_id' => $rate->currency_id,
                    'rate' => !empty($customCurrencyRate = $this->getCustomCurrencyRates()[$rate->currency_id]) ? $customCurrencyRate : ($rate->rate / $invoice->currencyRate->rate),
                    'invoice_id' => $invoice->id,
                    'invoice_currency_id' => $this->currencyId
                ];
            }

            if ($currencies) {
                InvoiceCurrency::getDb()->createCommand()->batchInsert(InvoiceCurrency::tableName(), [
                    'currency_id',
                    'rate',
                    'invoice_id',
                    'invoice_currency_id'
                ], $currencies)->execute();
            }

            $batchSize = 300;
            $offset = 0;
            while ($offset < count($orders)) {
                $batch = array_slice($orders, $offset, $batchSize, true);
                $offset += count($batch);
                $insert = [];
                foreach ($batch as $orderId => $month) {
                    $insert[] = [
                        'invoice_id' => $invoice->id,
                        'order_id' => $orderId,
                        'invoice_debt_id' => $monthOfDebts[$month] ?? null,
                    ];
                }
                InvoiceOrder::getDb()->createCommand()->batchInsert(InvoiceOrder::tableName(), [
                    'invoice_id',
                    'order_id',
                    'invoice_debt_id'
                ], $insert)->execute();
            }

            $invoice->recalculateTotals();
            $invoice->recalculateTotalsDebts();
            if (!empty($params['status'])) {
                $invoice->status = $params['status'];
            }
            if (!$invoice->save()) {
                throw new Exception($invoice->getFirstErrorAsString());
            }
            $transaction->commit();
            return $invoice;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    /**
     * @param $params
     * @return string
     */
    function getInvoiceFilterType($params)
    {
        if (!empty($params)) {
            if ($this->load($params)) {
                return $this->type_filter;
            }
        }

        return ReportFormInvoice::FILTER_TYPE_PERIOD;
    }

    function setCurrentFinanceReport($params)
    {
        if ($this->load($params)) {
            if ($this->financeReportId) {
                $this->financeReport = DeliveryReport::findOne(['id' => $this->financeReportId]);
            }
        }
    }

    /**
     * @return Currency
     */
    public function getCurrency()
    {
        if (is_null($this->_currency)) {
            $this->_currency = Currency::find()->where(['id' => $this->currencyId])->one();
        }
        return $this->_currency;
    }

    /**
     * @return CurrencyRate|array|\yii\db\ActiveRecord
     */
    public function getCurrencyRate()
    {
        if (is_null($this->_currencyRate)) {
            $this->_currencyRate = CurrencyRate::find()->where(['currency_id' => $this->currencyId])->one();
        }
        return $this->_currencyRate;
    }

    /**
     * @return array|null
     */
    protected function getCustomCurrencyRates()
    {
        if (is_null($this->_customCurrencyRates)) {
            $this->removeInvoiceCurrencyFromCustomCurrencyRates();
            $this->_customCurrencyRates = [];
            foreach ($this->rates as $id => $rate) {
                if (!empty($rate) && !empty($this->currencies[$id])) {
                    $this->_customCurrencyRates[$this->currencies[$id]] = $rate;
                }
            }
        }
        return $this->_customCurrencyRates;
    }

    /**
     * Удаление из заданных пользователем курсов валют, курс для валюты инвойса
     */
    protected function removeInvoiceCurrencyFromCustomCurrencyRates()
    {
        if (($index = array_search($this->currencyId, $this->currencies)) !== false) {
            unset($this->currencies[$index]);
            if (array_key_exists($index, $this->rates)) {
                unset($this->rates[$index]);
            }
            if (array_key_exists($index, $this->dates)) {
                unset($this->dates[$index]);
            }
        }
    }
}