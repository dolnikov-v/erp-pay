<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 18.01.2018
 * Time: 17:08
 */

namespace app\modules\report\components\invoice\generators;


use app\modules\report\models\Invoice;
use Yii;

/**
 * Class InvoiceGenerator
 * @package app\modules\report\components\invoice\generators
 */
abstract class InvoiceBuilder
{
    const FILE_EXTENSION = 'doc';

    /**
     * @var string
     */
    public $format;

    /**
     * @var Invoice
     */
    protected $invoice;

    /**
     * InvoiceExcelGenerator constructor.
     * @param Invoice $invoice
     * @param string $format
     */
    public function __construct($invoice, $format = null)
    {
        $this->invoice = $invoice;

        if ($format) {
            $this->format = $format;
        }
    }

    /**
     * @return string
     */
    public abstract function generate();

    /**
     * @return string
     */
    protected function getFilename()
    {
        $filename = sha1($this->invoice->name . '_' . $this->invoice->id) . '.' . $this->format;
        return $filename;
    }


    /**
     * @param string $templateFileName
     * @return string
     */
    protected function getTemplateFilePath($templateFileName)
    {
        $filePath = Yii::getAlias('@templates') . DIRECTORY_SEPARATOR . $templateFileName;

        //добавим проверку шаблона для курьерки по чаркоду в стране ибо шаблон для каждой КС может быть уникальным
        $courierFile = Yii::getAlias(
                '@templates'
            ) . DIRECTORY_SEPARATOR . $this->invoice->delivery->country->char_code . DIRECTORY_SEPARATOR . $this->invoice->delivery->char_code . DIRECTORY_SEPARATOR . $templateFileName;
        if (file_exists($courierFile)) {
            $filePath = $courierFile;
        } else {
            $countryFile = Yii::getAlias(
                    '@templates'
                ) . DIRECTORY_SEPARATOR . $this->invoice->delivery->country->char_code . DIRECTORY_SEPARATOR . $templateFileName;
            if (file_exists($countryFile)) {
                $filePath = $countryFile;
            }
        }

        return $filePath;
    }
}