<?php

namespace app\modules\report\components\invoice\generators;


use app\modules\delivery\components\charges\ChargesCalculatorAbstract;
use app\modules\order\models\OrderFinancePrediction;
use app\modules\report\components\invoice\ReportFormInvoice;
use app\modules\report\models\Invoice;
use app\modules\report\models\InvoiceCurrency;
use kartik\mpdf\Pdf;
use PhpOffice\PhpWord\TemplateProcessor;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class InvoicePdfGenerator
 * @package app\modules\report\components\invoice\generators
 */
class InvoiceFinanceBuilder extends InvoiceBuilder
{
    const FILE_FORMAT_PDF = 'pdf';
    const FILE_FORMAT_DOCX = 'docx';

    const INVOICE_TEMPLATE_DOCX = 'finance-invoice.docx';

    const INVOICE_TEMPLATE_PDF = 'finance-invoice.php';

    const INVOICE_TEMPLATE_PDF_HEADER = 'finance-invoice-header.php';
    const INVOICE_TEMPLATE_PDF_FOOTER = 'finance-invoice-footer.php';

    const FOLDER_FILES = 'invoice-files';

    /**
     * @var string
     */
    public $format = self::FILE_FORMAT_PDF;

    /**
     * @return string
     * @throws \Exception
     */
    public function generate()
    {
        set_time_limit(0);

        $data = $this->prepareData();
        $filename = $this->getFilename();
        $filePath = Invoice::getUploadPath($filename, true) . DIRECTORY_SEPARATOR . $filename;
        $period = '';
        $fromDate = date('d/m/Y', $this->invoice->period_from);
        $toDate = date('d/m/Y', $this->invoice->period_to);
        if ($this->invoice->period_from && $this->invoice->period_to) {
            $period = "{$fromDate}-{$toDate}";
        } elseif ($this->invoice->period_from) {
            $period = "after {$fromDate}";
        } elseif ($this->invoice->period_to) {
            $period = "before {$toDate}";
        }

        $currencyRateStr = [];
        foreach ($this->invoice->currencyRates as $currencyRate) {
            $rate = Yii::$app->formatter->asDecimal(1 / $currencyRate->rate, ReportFormInvoice::ROUND_PRECISION);
            $currencyRateStr[] = "1 {$currencyRate->currency->char_code} = {$rate} {$currencyRate->invoiceCurrency->char_code}";
        }
        $currencyRateStr = implode(', ', $currencyRateStr);
        $totalAmount = Yii::$app->formatter->asDecimal($data['totalData']['total_sum_usd'], ReportFormInvoice::ROUND_PRECISION);
        $invoiceDate = date('d/m/Y', $this->invoice->created_at);
        $paymentDate = $this->invoice->payment_date ? date('d/m/Y', $this->invoice->payment_date) : '';
        switch ($this->format) {
            case self::FILE_FORMAT_DOCX: {
                $templateFilePath = $this->getTemplateFilePath(self::INVOICE_TEMPLATE_DOCX);
                if (!file_exists($templateFilePath)) {
                    throw new \Exception(Yii::t('common', 'Не найден файл с шаблоном {template}', ['template' => self::INVOICE_TEMPLATE_DOCX]));
                }
                $document = new TemplateProcessor($templateFilePath);
                $i = 1;

                $document->cloneRow('row_number', count($data['data']));
                foreach ($data['data'] as $attribute => $values) {
                    $document->setValue("row_number#{$i}", $i);
                    foreach ($values as $valueName => $value) {
                        $document->setValue("row_{$valueName}#{$i}", $value);
                    }
                    $i++;
                }
                $document->setValue('total_amount', $totalAmount);
                $document->setValue('InvoiceName', $this->invoice->name);
                $document->setValue('InvoiceDate', $invoiceDate);
                $document->setValue('DeliveryBankName', $this->invoice->contract->bank_name);
                $document->setValue('DeliveryBankAccountNumber', $this->invoice->contract->bank_account);
                $document->setValue('DeliveryAddress', $this->invoice->delivery->address);
                $document->setValue('DeliveryName', $this->invoice->delivery->legal_name);
                $document->setValue('row_total_number', $i);
                $document->setValue('Period', $period);
                $document->setValue('currencyRateStr', $currencyRateStr);
                $document->setValue('PaymentDate', $paymentDate);
                $document->setValue('requisiteDelivery', $this->invoice->requisiteDelivery);

                $document->saveAs($filePath);
                break;
            }
            case self::FILE_FORMAT_PDF: {
                $templateFilePath = $this->getTemplateFilePath(self::INVOICE_TEMPLATE_PDF);
                $this->format = self::FILE_FORMAT_PDF;
                $filePath = Invoice::getUploadPath($filename, true) . DIRECTORY_SEPARATOR . $filename;
                $logoPath = Yii::getAlias('@templates') . DIRECTORY_SEPARATOR . self::FOLDER_FILES . DIRECTORY_SEPARATOR . 'logo.png';
                $logoContent = file_get_contents($logoPath);

                $logoBase64 = 'data:image/png;base64,' . base64_encode($logoContent);

                $signaturePath = Yii::getAlias('@templates') . DIRECTORY_SEPARATOR . self::FOLDER_FILES . DIRECTORY_SEPARATOR . 'signature.png';
                $signatureContent = file_get_contents($signaturePath);
                $signatureBase64 = 'data:image/png;base64,' . base64_encode($signatureContent);

                $content = Yii::$app->controller->renderFile($templateFilePath, [
                    'rows' => $data['data'],
                    'total_amount' => $totalAmount,
                    'invoiceName' => $this->invoice->name,
                    'invoiceDate' => $invoiceDate,
                    'deliveryBankName' => $this->invoice->contract->bank_name,
                    'deliveryBankAccountNumber' => $this->invoice->contract->bank_account,
                    'deliveryAddress' => $this->invoice->delivery->address,
                    'deliveryName' => $this->invoice->delivery->legal_name,
                    'logoBase64' => $logoBase64,
                    'period' => $period,
                    'paymentDate' => $paymentDate,
                    'requisiteDelivery' => $this->invoice->requisiteDelivery,
                    'signatureBase64' => $signatureBase64,
                    'invoiceCurrencyCharCode' => $this->invoice->currency->char_code,
                    'currencyRateStr' => $currencyRateStr
                ]);
                $document = new Pdf([
                    'mode' => Pdf::MODE_UTF8,
                    'options' => [
                        'autoScriptToLang' => true,
                        'autoLangToFont' => true,
                        'ignore_invalid_utf8' => true,
                        'tabSpaces' => 4
                    ],

                    'marginLeft' => 11,
                    'marginRight' => 11,
                    'marginTop' => 43,
                    'marginBottom' => 30,
                    'content' => $content,
                    'filename' => $filePath,
                    'destination' => Pdf::DEST_FILE,
                    'cssFile' => Yii::getAlias('@templates') . DIRECTORY_SEPARATOR . self::FOLDER_FILES . DIRECTORY_SEPARATOR . 'finance-invoice.css',
                ]);
                $templateHeaderFilePath = $this->getTemplateFilePath(self::INVOICE_TEMPLATE_PDF_HEADER);
                $headerContent = Yii::$app->controller->renderFile($templateHeaderFilePath, [
                    'logoBase64' => $logoBase64,
                    'requisiteDelivery' => $this->invoice->requisiteDelivery,
                ]);
                $templateFooterFilePath = $this->getTemplateFilePath(self::INVOICE_TEMPLATE_PDF_FOOTER);
                $footerContent = Yii::$app->controller->renderFile($templateFooterFilePath, [
                    'signatureBase64' => $signatureBase64,
                    'invoiceDate' => $invoiceDate,
                    'requisiteDelivery' => $this->invoice->requisiteDelivery,
                ]);
                $document->getApi()->SetHTMLHeader($headerContent);
                $document->getApi()->SetHTMLFooter($footerContent);
                $document->render();
                break;
            }
            default:
                throw new \Exception(Yii::t('common', 'Указан неподдерживаемый формат файла для генерации финансового инвойса.'));
        }

        return $filename;
    }

    /**
     * @return array
     */
    protected static function financeAttributeLabels()
    {
        return [
            OrderFinancePrediction::COLUMN_PRICE_COD => 'COD Collected',
            OrderFinancePrediction::COLUMN_PRICE_STORAGE => 'Storage',
            OrderFinancePrediction::COLUMN_PRICE_FULFILMENT => 'Fulfilment',
            OrderFinancePrediction::COLUMN_PRICE_PACKING => 'Packing',
            OrderFinancePrediction::COLUMN_PRICE_PACKAGE => 'Package',
            OrderFinancePrediction::COLUMN_PRICE_ADDRESS_CORRECTION => 'Address correction',
            OrderFinancePrediction::COLUMN_PRICE_DELIVERY => 'Delivery charges',
            OrderFinancePrediction::COLUMN_PRICE_REDELIVERY => 'Redelivery charges',
            OrderFinancePrediction::COLUMN_PRICE_DELIVERY_BACK => 'Delivery back charges',
            OrderFinancePrediction::COLUMN_PRICE_DELIVERY_RETURN => 'Delivery return charges',
            OrderFinancePrediction::COLUMN_PRICE_COD_SERVICE => 'COD service',
            OrderFinancePrediction::COLUMN_PRICE_VAT => 'VAT',
        ];
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function prepareData()
    {
        $result = $this->invoice->getFinanceData();

        $financeAttributeLabels = self::financeAttributeLabels();

        $outData = [];
        foreach ($result['rows'] as $attribute => $properties) {
            switch ($properties['calculating_type']) {
                case ChargesCalculatorAbstract::CALCULATING_TYPE_NUMBER:
                    $properties['quantity'] .= ' pcs.';
                    break;
                case ChargesCalculatorAbstract::CALCULATING_TYPE_PERCENT:
                    $properties['quantity'] = Yii::$app->formatter->asDecimal($properties['quantity'], ReportFormInvoice::ROUND_PRECISION) . ' ' . $this->invoice->currency->char_code;
                    break;
                case ChargesCalculatorAbstract::CALCULATING_TYPE_MONTHLY_CHARGE:
                    $properties['quantity'] .= ' mon.';
                    break;
            }

            $outData[$attribute] = [
                'desc' => $financeAttributeLabels[$attribute],
                'qty' => is_double($properties['quantity']) ? Yii::$app->formatter->asDecimal($properties['quantity'], ReportFormInvoice::ROUND_PRECISION) : $properties['quantity'],
                'unit' => $properties['finance_type'] == OrderFinancePrediction::FINANCE_TYPE_INCOME ? '' : (is_numeric($properties['formula']) ? Yii::$app->formatter->asDecimal($properties['formula'], $this->invoice->round_precision) : $properties['formula']),
                'amount' => ($properties['finance_type'] == OrderFinancePrediction::FINANCE_TYPE_COST ? '(' : '') . Yii::$app->formatter->asDecimal($properties['usd_sum'], ReportFormInvoice::ROUND_PRECISION) . ($properties['finance_type'] == OrderFinancePrediction::FINANCE_TYPE_COST ? ')' : ''),
            ];
        }

        return [
            'data' => $outData,
            'totalData' => $result['totalData'],
        ];
    }
}