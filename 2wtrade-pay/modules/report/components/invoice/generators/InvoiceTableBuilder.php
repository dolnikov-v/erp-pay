<?php

namespace app\modules\report\components\invoice\generators;

use app\models\CurrencyRate;
use app\models\Product;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\models\Order;
use app\modules\order\models\OrderFinancePrediction;
use app\modules\order\models\OrderProduct;
use app\modules\order\models\OrderStatus;
use app\modules\report\components\invoice\ReportFormInvoice;
use app\modules\report\models\Invoice;
use app\modules\report\models\InvoiceOrder;
use PHPExcel_Cell_DataType;
use Yii;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use app\modules\report\components\filters\DateFilter;

/**
 * Class InvoiceExcelGenerator
 * @package app\modules\report\components\invoice\generators
 */
class InvoiceTableBuilder extends InvoiceBuilder
{
    public $format = 'xlsx';

    const FILE_FORMAT_XLSX = 'xlsx';

    const INVOICE_TEMPLATE_XLSX = 'invoice-finance-table.xlsx';

    protected $currencyRates = [];

    /**
     * @return \yii\db\ActiveQuery
     */
    protected function prepareQuery()
    {
        $query = OrderFinancePrediction::find();
        $query->joinWith(['invoice'], false);
        $query->where([Invoice::tableName() . '.id' => $this->invoice->id]);
        $query->with([
            'order.deliveryRequest',
            'order.status',
            'order.callCenterRequest',
            'order.orderProducts.product'
        ]);

        return $query;
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function generate()
    {
        $this->currencyRates = ArrayHelper::map(CurrencyRate::find()->all(), 'currency_id', 'rate');
        $invoiceCurrencyRates = ArrayHelper::map($this->invoice->currencyRates, 'currency_id', 'rate');
        foreach ($invoiceCurrencyRates as $currencyId => $rate) {
            $this->currencyRates[$currencyId] = $rate;
        }
        $filename = $this->getFilename();
        $filePath = Invoice::getUploadPath($filename, true) . DIRECTORY_SEPARATOR . $filename;
        switch ($this->format) {
            case self::FILE_FORMAT_XLSX:
                {
                    $templateFilePath = $this->getTemplateFilePath(self::INVOICE_TEMPLATE_XLSX);
                    if (!file_exists($templateFilePath)) {
                        throw new \Exception(Yii::t('common', 'Не найден файл с шаблоном {template}', ['template' => self::FILE_FORMAT_XLSX]));
                    }
                    $objPHPExcel = \PHPExcel_IOFactory::load($templateFilePath);
                    $objPHPExcel->setActiveSheetIndex(1);
                    $activeSheet = $objPHPExcel->getActiveSheet();
                    $rowNum = 7;
                    $activeSheet->setCellValueExplicit('B2', $this->invoice->delivery->name, PHPExcel_Cell_DataType::TYPE_STRING);
                    $activeSheet->setCellValueExplicit('B4', date('d.m.Y', $this->invoice->period_from ?? $this->invoice->contract->date_from ?? $this->invoice->contract->created_at));
                    $activeSheet->setCellValueExplicit('D4', date('d.m.Y', $this->invoice->period_to ?? time()));
                    $activeSheet->setCellValueExplicit('F2',($this->invoice->currency_id ? $this->invoice->currency->char_code : 'USD'));
                    if (count($this->invoice->currencyRates) > 3) {
                        $activeSheet->insertNewRowBefore(5, count($this->invoice->currencyRates) - 3);
                        $rowNum += count($this->invoice->currencyRates) - 3;
                    }
                    foreach ($this->invoice->currencyRates as $key => $rate) {
                        $activeSheet->setCellValueExplicit('I' . (2 + $key), "1 {$rate->currency->char_code} = " . Yii::$app->formatter->asDecimal(1 / $rate->rate, ReportFormInvoice::ROUND_PRECISION) . " {$rate->invoiceCurrency->char_code}");
                    }
                    $orderStart = $rowNum;
                    $query = $this->prepareQuery();
                    // выкуп / невыкуп
                    $redeemed = $unredeemed = $redeemedCOD = $allCOD = 0;
                    foreach ($query->batch(500) as $orderFinances) {
                        foreach ($orderFinances as $orderFinance) {
                            /** @var OrderFinancePrediction $orderFinance **/
                            if ($orderFinance->order->status_id == OrderStatus::STATUS_DELIVERY_BUYOUT) {
                                $redeemed++;
                                $redeemedCOD += ($orderFinance->price_cod / ($orderFinance->price_cod_currency_id ? $this->currencyRates[$orderFinance->price_cod_currency_id] : $this->invoice->delivery->country->currencyRate->rate));
                            } else {
                                $unredeemed++;
                            }
                            $allCOD += ($orderFinance->price_cod / ($orderFinance->price_cod_currency_id ? $this->currencyRates[$orderFinance->price_cod_currency_id] : $this->invoice->delivery->country->currencyRate->rate));
                            $row = $this->prepareOrders($orderFinance);
                            $products = $this->prepareProducts($orderFinance);
                            if (count($products) > 1) {
                                foreach ($row as $column => $value) {
                                    $activeSheet->mergeCells($column . $rowNum . ':' . $column . ($rowNum + count($products) - 1));
                                }
                            }
                            foreach ($products as $key => $product) {
                                if ($key === 0) {
                                    $row += $products[0];
                                    foreach ($row as $column => $value) {
                                        if (!is_array($value)) {
                                            $activeSheet->setCellValueExplicit($column . $rowNum, $value, is_numeric($value) ? PHPExcel_Cell_DataType::TYPE_NUMERIC : PHPExcel_Cell_DataType::TYPE_STRING);
                                        } else {
                                            $type = $value['type'] ?? PHPExcel_Cell_DataType::TYPE_STRING;
                                            $activeSheet->setCellValueExplicit($column . $rowNum, $value['value'] ?? $value[0], $type);
                                        }
                                    }
                                } else {
                                    foreach ($product as $column => $value) {
                                        $activeSheet->setCellValueExplicit($column . ($rowNum + $key), $value, is_numeric($value) ? PHPExcel_Cell_DataType::TYPE_NUMERIC : PHPExcel_Cell_DataType::TYPE_STRING);
                                    }
                                }
                            }
                            if (count($products) > 1) {
                                $rowNum += count($products) - 1;
                            }
                            $rowNum++;
                        }
                    }
                    $this->addExcelFooter($activeSheet, $rowNum);

                    $objPHPExcel->setActiveSheetIndex(0);
                    $activeSheet = $objPHPExcel->getActiveSheet();
                    $activeSheet->setCellValueExplicit('B3', $this->invoice->delivery->name, PHPExcel_Cell_DataType::TYPE_STRING);
                    $activeSheet->setCellValueExplicit('B5', date('d.m.Y', $this->invoice->period_from ?? $this->invoice->contract->date_from ?? $this->invoice->contract->created_at));
                    $activeSheet->setCellValueExplicit('D5', date('d.m.Y', $this->invoice->period_to ?? time()));
                    $activeSheet->setCellValueExplicit('B7', $redeemed, PHPExcel_Cell_DataType::TYPE_STRING);
                    $activeSheet->setCellValueExplicit('B8', $unredeemed, PHPExcel_Cell_DataType::TYPE_STRING);
                    $activeSheet->setCellValueExplicit('B11', $redeemedCOD, PHPExcel_Cell_DataType::TYPE_STRING);
                    $activeSheet->setCellValueExplicit('B15', number_format($allCOD, ReportFormInvoice::ROUND_PRECISION), PHPExcel_Cell_DataType::TYPE_STRING);

                    $activeSheet->setCellValueExplicit('B16', '=SUM(Orders!J' . $orderStart . ':J' . ($rowNum - 1) . ')', PHPExcel_Cell_DataType::TYPE_FORMULA);
                    $activeSheet->setCellValueExplicit('B13', '=B16-SUM(Orders!I' . $orderStart . ':I' . ($rowNum - 1) . ')', PHPExcel_Cell_DataType::TYPE_FORMULA);

                    $this->addExcelFooter($activeSheet, 18);

                    $objPHPExcel->setActiveSheetIndex(2);
                    $activeSheet = $objPHPExcel->getActiveSheet();
                    $activeSheet->setCellValueExplicit('C3', date('d.m.Y', $this->invoice->period_from ?? $this->invoice->contract->date_from ?? $this->invoice->contract->created_at));
                    $activeSheet->setCellValueExplicit('E3', date('d.m.Y', $this->invoice->period_to ?? time()));
                    $products = [];
                    foreach ($this->getAllOrders()->all() as $product) {
                        if (empty($products[$product['product_id']])) {
                            $products[$product['product_id']] = $product;
                        }
                        if (in_array($product['status_id'], OrderStatus::getProcessDeliveryAndLogisticList())) {
                            $products[$product['product_id']]['process'] = $product['quantity'];
                        } elseif (in_array($product['status_id'], OrderStatus::getOnlyBuyoutList())) {
                            $products[$product['product_id']]['buyout'] = $product['quantity'];
                        } else {
                            $products[$product['product_id']]['not'] = $product['quantity'];
                        }
                    }
                    $rowNum = 6;
                    if ($products) {
                        foreach ($products as $product) {
                            $activeSheet->setCellValueExplicit('A' . $rowNum, $product['name'], PHPExcel_Cell_DataType::TYPE_STRING);
                            $activeSheet->setCellValueExplicit('B' . $rowNum, $product['process'] ?? 0, PHPExcel_Cell_DataType::TYPE_NUMERIC);
                            $activeSheet->setCellValueExplicit('C' . $rowNum, $product['buyout'] ?? 0, PHPExcel_Cell_DataType::TYPE_NUMERIC);
                            $rowNum++;
                        }
                    }
                    $this->addExcelFooter($activeSheet, $rowNum);

                    $objPHPExcel->setActiveSheetIndex(3);
                    $activeSheet = $objPHPExcel->getActiveSheet();
                    $activeSheet->setCellValueExplicit('B3', date('d.m.Y', $this->invoice->period_to ?? time()));
                    $notIncludedOrders = $this->getNotIncludeOrders();
                    $rowNum = 6;
                    foreach ($notIncludedOrders->batch(500) as $orderFinances) {
                        /**
                         * @var OrderFinancePrediction $orderFinance
                         */
                        foreach ($orderFinances as $orderFinance) {
                            $row = $this->preparePendingOrders($orderFinance);
                            $products = $this->prepareProducts($orderFinance);
                            if (count($products) > 1) {
                                foreach ($row as $column => $value) {
                                    $activeSheet->mergeCells($column . $rowNum . ':' . $column . ($rowNum + count($products) - 1));
                                }
                            }
                            foreach ($products as $key => $product) {
                                if ($key === 0) {
                                    $row += $products[0];
                                    foreach ($row as $column => $value) {
                                        if (!is_array($value)) {
                                            $activeSheet->setCellValueExplicit($column . $rowNum, $value, is_numeric($value) ? PHPExcel_Cell_DataType::TYPE_NUMERIC : PHPExcel_Cell_DataType::TYPE_STRING);
                                        } else {
                                            $type = $value['type'] ?? PHPExcel_Cell_DataType::TYPE_STRING;
                                            $activeSheet->setCellValueExplicit($column . $rowNum, $value['value'] ?? $value[0], $type);
                                        }
                                    }
                                } else {
                                    foreach ($product as $column => $value) {
                                        $activeSheet->setCellValueExplicit($column . ($rowNum + $key), $value, is_numeric($value) ? PHPExcel_Cell_DataType::TYPE_NUMERIC : PHPExcel_Cell_DataType::TYPE_STRING);
                                    }
                                }
                            }
                            if (count($products) > 1) {
                                $rowNum += count($products) - 1;
                            }
                            $rowNum++;
                        }
                    }
                    $this->addExcelFooter($activeSheet, $rowNum);

                    $objPHPExcel->setActiveSheetIndex(0);
                    $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
                    $objWriter->save($filePath);
                    break;
                }
            default:
                throw new \Exception(Yii::t('common', 'Указан неподдерживаемый формат файла для генерации финансового инвойса.'));
        }

        return $filename;
    }

    /**
     * @param OrderFinancePrediction $orderFinance
     * @return array
     */
    protected function prepareProducts(OrderFinancePrediction $orderFinance): array
    {
        $order = $orderFinance->order;
        $products = [];
        foreach ($order->orderProducts as $product) {
            $products[] = [
                'C' => $product->product->name,
                'D' => $product->quantity,
                'E' => $product->price,
                'F' => $product->price * $product->quantity,
            ];
        }
        return $products;
    }

    /**
     * @param OrderFinancePrediction $orderFinance
     * @return array
     */
    protected function prepareOrders(OrderFinancePrediction $orderFinance): array
    {
        $order = $orderFinance->order;
        return [
            'A' => $orderFinance->order->id,
            'B' => Yii::t('common', $order->status->name),
            'H' => '',
            'G' => [
                'value' => number_format(
                    ($orderFinance->price_cod / ($orderFinance->price_cod_currency_id ? $this->currencyRates[$orderFinance->price_cod_currency_id] : $this->invoice->delivery->country->currencyRate->rate)) +
                    ($orderFinance->price_fulfilment / ($orderFinance->price_fulfilment_currency_id ? $this->currencyRates[$orderFinance->price_fulfilment_currency_id] : $this->invoice->delivery->country->currencyRate->rate)) +
                    ($orderFinance->price_packing / ($orderFinance->price_packing_currency_id ? $this->currencyRates[$orderFinance->price_packing_currency_id] : $this->invoice->delivery->country->currencyRate->rate)) +
                    ($orderFinance->price_package / ($orderFinance->price_package_currency_id ? $this->currencyRates[$orderFinance->price_package_currency_id] : $this->invoice->delivery->country->currencyRate->rate)) +
                    ($orderFinance->price_delivery / ($orderFinance->price_delivery_currency_id ? $this->currencyRates[$orderFinance->price_delivery_currency_id] : $this->invoice->delivery->country->currencyRate->rate)) +
                    ($orderFinance->price_redelivery / ($orderFinance->price_redelivery_currency_id ? $this->currencyRates[$orderFinance->price_redelivery_currency_id] : $this->invoice->delivery->country->currencyRate->rate)) +
                    ($orderFinance->price_delivery_back / ($orderFinance->price_delivery_back_currency_id ? $this->currencyRates[$orderFinance->price_delivery_back_currency_id] : $this->invoice->delivery->country->currencyRate->rate)) +
                    ($orderFinance->price_delivery_return / ($orderFinance->price_delivery_return_currency_id ? $this->currencyRates[$orderFinance->price_delivery_return_currency_id] : $this->invoice->delivery->country->currencyRate->rate)) +
                    ($orderFinance->price_address_correction / ($orderFinance->price_address_correction_currency_id ? $this->currencyRates[$orderFinance->price_address_correction_currency_id] : $this->invoice->delivery->country->currencyRate->rate)) +
                    ($orderFinance->price_cod_service / ($orderFinance->price_cod_service_currency_id ? $this->currencyRates[$orderFinance->price_cod_service_currency_id] : $this->invoice->delivery->country->currencyRate->rate)) +
                    (!empty($orderFinance->vat_included) ? 0 : ($orderFinance->price_vat / ($orderFinance->price_vat_currency_id ? $this->currencyRates[$orderFinance->price_vat_currency_id] : $this->invoice->delivery->country->currencyRate->rate)))
                    , ReportFormInvoice::ROUND_PRECISION),
                'type' => PHPExcel_Cell_DataType::TYPE_NUMERIC,
            ],
            'I' => [
                'value' => '=G15+H15',
                'type' => PHPExcel_Cell_DataType::TYPE_FORMULA
            ],
            'J' => [
                'value' => '=F15',
                'type' => PHPExcel_Cell_DataType::TYPE_FORMULA
            ],
        ];
    }

    /**
     * @param OrderFinancePrediction $orderFinance
     * @return array
     */
    protected function preparePendingOrders(OrderFinancePrediction $orderFinance): array
    {
        $order = $orderFinance->order;
        return [
            'A' => $orderFinance->order->id,
            'B' => Yii::t('common', $order->status->name),
            'G' => date('d.m.Y', $orderFinance->order->created_at),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    protected function getNotIncludeOrders()
    {
        $query = OrderFinancePrediction::find()
            ->joinWith(['order.deliveryRequest', 'invoiceOrder'], false)
            ->where([DeliveryRequest::tableName() . '.delivery_id' => $this->invoice->delivery_id])
            ->where([DeliveryRequest::tableName() . '.status' => DeliveryRequest::STATUS_IN_PROGRESS])
            ->andWhere([Order::tableName() . '.status_id' => OrderStatus::getProcessDeliveryAndLogisticList()])
            ->andWhere(['is', InvoiceOrder::tableName() . '.invoice_id', null]);
        if ($this->invoice->period_from) {
            $query->andWhere([
                '>=',
                (new DateFilter([
                    'type' => DateFilter::TYPE_DELIVERY_SENT_AT_NOT_EMPTY,
                    'showSelectType' => false,
                ]))->getFilteredAttribute(),
                $this->invoice->period_from
            ]);
        }
        if ($this->invoice->period_to) {
            $query->andWhere([
                '<=',
                (new DateFilter([
                    'type' => DateFilter::TYPE_DELIVERY_SENT_AT_NOT_EMPTY,
                    'showSelectType' => false,
                ]))->getFilteredAttribute(),
                $this->invoice->period_to
            ]);
        }
        return $query;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    protected function getAllOrders()
    {
        $query = OrderFinancePrediction::find()
            ->select(Product::tableName() . '.name, ' . Order::tableName() . '.status_id, ' . OrderProduct::tableName() . '.*')
            ->joinWith(['order.deliveryRequest', 'invoiceOrder', 'order.orderProducts', 'order.products'], false)
            ->where([DeliveryRequest::tableName() . '.delivery_id' => $this->invoice->delivery_id])
            ->andWhere([Order::tableName() . '.status_id' => array_merge(
                OrderStatus::getProcessDeliveryAndLogisticList(),
                OrderStatus::getOnlyBuyoutList(),
                OrderStatus::getNotBuyoutInDeliveryProcessList()
            )])
            ->andWhere(new Expression(InvoiceOrder::tableName() . '.invoice_id is null or ' . InvoiceOrder::tableName() .  '.invoice_id = :id', ['id' => $this->invoice->id]))
            ->groupBy(new Expression(OrderProduct::tableName() . '.product_id, ' . Order::tableName() . '.status_id'));
        if ($this->invoice->period_from) {
            $query->andWhere([
                '>=',
                (new DateFilter([
                    'type' => DateFilter::TYPE_DELIVERY_SENT_AT_NOT_EMPTY,
                    'showSelectType' => false,
                ]))->getFilteredAttribute(),
                $this->invoice->period_from
            ]);
        }
        if ($this->invoice->period_to) {
            $query->andWhere([
                '<=',
                (new DateFilter([
                    'type' => DateFilter::TYPE_DELIVERY_SENT_AT_NOT_EMPTY,
                    'showSelectType' => false,
                ]))->getFilteredAttribute(),
                $this->invoice->period_to
            ]);
        }
        return $query->asArray();
    }

    /**
     * @param \PHPExcel_Worksheet $activeSheet
     * @param int $rowNum
     * @throws \PHPExcel_Exception
     */
    protected function addExcelFooter(\PHPExcel_Worksheet $activeSheet, int $rowNum) {
        $rowNum++;
        $activeSheet->setCellValueExplicit('A' . $rowNum, 'Couriers Rep _____');
        $activeSheet->mergeCells('A' . $rowNum. ':B' . $rowNum);
        $activeSheet->setCellValueExplicit('C' . $rowNum, '2WTrade Rep _____');
        $activeSheet->mergeCells('C' . $rowNum. ':D' . $rowNum);
    }
}