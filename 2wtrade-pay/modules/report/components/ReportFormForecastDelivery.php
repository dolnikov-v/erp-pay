<?php

namespace app\modules\report\components;

use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use yii\data\ArrayDataProvider;
use app\modules\report\components\filters\ReportForecastDeliveryFilter;
use app\modules\report\components\filters\ProductSelectFilter;
use app\modules\report\components\filters\DeliveryCurrentCountrySelectFilter;
use Yii;

/**
 * Class ReportFormForecastDelivery
 * @package app\modules\report\components
 */
class ReportFormForecastDelivery extends ReportForm
{
    /**
     * @var ReportForecastDeliveryFilter
     */
    protected $forecastDeliveryFilter;

    /**
     * @var ProductSelectFilter
     */
    protected $productSelectFilter;

    /**
     * @var DeliveryCurrentCountrySelectFilter
     */
    protected $deliveryCurrentCountrySelectFilter;

    /**
     * @param array $params
     * @return array
     */
    public function apply($params)
    {
        $this->query = Order::find()
            ->joinWith([
                'deliveryRequest', 'callCenterRequest'
            ]);

        $this->query->addSelect(['id' => 'FROM_UNIXTIME(' . $this->getForecastDeliveryFilter()->selectedType . ', "%Y-%m-%d")']);

        $condition = 'if(' . Order::tableName() . '.status_id in (' . join(', ', OrderStatus::getBuyoutList()) . '), 1, 0)';
        $this->query->addSelect(['buyout' => 'sum(' . $condition . ')']);

        $condition = 'if(' . Order::tableName() . '.status_id in (' . join(', ', OrderStatus::getNotBuyoutList()) . '), 1, 0)';
        $this->query->addSelect(['notbuyout' => 'sum(' . $condition . ')']);

        $condition = 'if(' . Order::tableName() . '.status_id in (' . join(', ', OrderStatus::getProcessList()) . '), 1, 0)';
        $this->query->addSelect(['process' => 'sum(' . $condition . ')']);

        $this->applyFilters($params);

        $this->query->groupBy(['id']);
        $this->query->orderBy('id');
        $dataArray = $this->query->asArray()->all();
        $intervals = $this->getIntervals();

        $resultArray = [];
        foreach($dataArray as $dataItem) {
            $dataDate = strtotime($dataItem['id']);
            foreach($intervals as $interval) {
                if (($dataDate + 1) < ($interval - 86400 * 21) && ($dataDate + 1) > ($interval - 86400 * 28)) {
                    if (isset($resultArray[$interval])) {
                        $resultArray[$interval]['count']++;
                        $resultArray[$interval]['buyout'] += $dataItem['buyout'];
                        $resultArray[$interval]['notbuyout'] += $dataItem['notbuyout'];
                        $resultArray[$interval]['process'] += $dataItem['process'];
                    } else {
                        $resultArray[$interval] = [
                            'interval' => $interval,
                            'count' => 1,
                            'buyout' => $dataItem['buyout'],
                            'notbuyout' => $dataItem['notbuyout'],
                            'process' => $dataItem['process'],
                        ];
                    }
                }
            }
        }

        $series = [
            0 => ['name' => '', 'data' => null], // выкуп
            1 => ['name' => '', 'data' => null], // невыкуп
            2 => ['name' => '', 'data' => null], // в процессе
        ];
        $intervalsData = null;
        foreach ($resultArray as $resultItemKey => $resultItemValue) {
            try {
                $percentBuyout = round($resultArray[$resultItemKey]['buyout'] / ($resultArray[$resultItemKey]['buyout'] + round($resultArray[$resultItemKey]['notbuyout']/$resultArray[$resultItemKey]['count']) + round($resultArray[$resultItemKey]['process']/$resultArray[$resultItemKey]['count'])), 2);
            } catch (\Exception $e) {
                $percentBuyout = 0;
            }
            try {
                $percentNotBuyout = round($resultArray[$resultItemKey]['notbuyout'] / (round($resultArray[$resultItemKey]['buyout']/$resultArray[$resultItemKey]['count']) + $resultArray[$resultItemKey]['notbuyout'] + round($resultArray[$resultItemKey]['process']/$resultArray[$resultItemKey]['count'])), 2);
            } catch (\Exception $e) {
                $percentNotBuyout = 0;
            }
            try {
                $percentProcess = round($resultArray[$resultItemKey]['process'] / (round($resultArray[$resultItemKey]['buyout']/$resultArray[$resultItemKey]['count']) + round($resultArray[$resultItemKey]['notbuyout']/$resultArray[$resultItemKey]['count']) + $resultArray[$resultItemKey]['process']), 2);
            } catch (\Exception $e) {
                $percentProcess = 0;
            }

            $resultArray[$resultItemKey]['buyout'] = round($resultArray[$resultItemKey]['buyout']/$resultArray[$resultItemKey]['count']);
            $resultArray[$resultItemKey]['notbuyout'] = round($resultArray[$resultItemKey]['notbuyout']/$resultArray[$resultItemKey]['count']);
            $resultArray[$resultItemKey]['process'] = round($resultArray[$resultItemKey]['process']/$resultArray[$resultItemKey]['count']);
            $series[0]['data'][] = $resultArray[$resultItemKey]['buyout'];
            $series[1]['data'][] = $resultArray[$resultItemKey]['notbuyout'];
            $series[2]['data'][] = $resultArray[$resultItemKey]['process'];

            $resultArray[$resultItemKey]['buyout'] .= ' (' . $percentBuyout . '%)';
            $resultArray[$resultItemKey]['notbuyout'] .= ' (' . $percentNotBuyout . '%)';
            $resultArray[$resultItemKey]['process'] .= ' (' . $percentProcess . '%)';

            $intervalsData[] = Yii::$app->formatter->asDate($resultItemKey);
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $resultArray,
            'pagination' => false
        ]);

        return [
            'series' => $series,
            'intervals' => $intervalsData,
            'dataProvider' => $dataProvider,
        ];
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return '';
    }

    /**
     * @return ReportForecastDeliveryFilter
     */
    public function getForecastDeliveryFilter()
    {
        if (is_null($this->forecastDeliveryFilter)) {
            $this->forecastDeliveryFilter = new ReportForecastDeliveryFilter();
        }

        return $this->forecastDeliveryFilter;
    }

    /**
     * @return ProductSelectFilter
     */
    public function getProductSelectFilter()
    {
        if (is_null($this->productSelectFilter)) {
            $this->productSelectFilter = new ProductSelectFilter();
        }
        return $this->productSelectFilter;
    }

    /**
     * @return DeliveryCurrentCountrySelectFilter
     */
    public function getDeliveryCurrentCountrySelectFilter()
    {
        if (is_null($this->deliveryCurrentCountrySelectFilter)) {
            $this->deliveryCurrentCountrySelectFilter = new DeliveryCurrentCountrySelectFilter();
        }
        return $this->deliveryCurrentCountrySelectFilter;
    }

    /**
     * @param array $params
     * @return $this
     */
    protected function applyFilters($params)
    {
        $this->getForecastDeliveryFilter()->apply($this->query, $params);
        $this->getCountryFilter()->apply($this->query, $params);
        $this->getDeliveryCurrentCountrySelectFilter()->apply($this->query, $params);
        $this->getProductSelectFilter()->apply($this->query, $params);

        return $this;
    }

    /**
     * @return array
     */
    protected function getIntervals()
    {
        $from = strtotime($this->getForecastDeliveryFilter()->from);
        $to = strtotime($this->getForecastDeliveryFilter()->to);
        $intervals[$from] = $from;
        while (($from + 86400) <= $to) {
            $from += 86400;
            $intervals[$from] = $from;
        }
        return $intervals;
    }
}