<?php
namespace app\modules\report\components;

use app\models\Country;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use app\modules\report\components\filters\CountrySelectFilter;
use app\modules\report\components\filters\ProductSelectFilter;
use app\modules\report\components\filters\CuratorUserFilter;
use app\modules\report\components\filters\DailyFilterDefaultLastMonth;
use app\modules\report\components\filters\TypeDatePartFilter;
use app\modules\report\components\filters\TypeOrderSourceFilter;
use app\modules\report\extensions\Query;
use Yii;
use yii\data\ArrayDataProvider;

/**
 * Class ReportFormLeadByCountry
 * @package app\modules\report\components
 */
class ReportFormLeadByCountry extends ReportForm
{
    /**
     * @var $DailyFilter
     */
    protected $dailyFilter;

    /**
     * @var $CountrySelectFilter
     */
    protected $countrySelectFilter;

    /**
     * @var $ProductSelectFilter
     */
    protected $productSelectFilter;

    /**
     * @var $curatorUserFilter
     */
    protected $curatorUserFilter;

    /**
     * @var $TypeDatePartFilter
     */
    protected $typeDatePartFilter;

    /**
     * @var $TypeOrderSourceFilter
     */
    protected $typeOrderSourceFilter;

    /**
     * @var array Маппинг статусов
     */
    protected $mapStatuses = [];

    /**
     * @param array $params
     * @return ArrayDataProvider
     */
    public function apply($params)
    {
        $this->load($params);

        $this->query = new Query();
        $this->query->from([Order::tableName()]);
        $this->query->andWhere([Order::tableName() . '.duplicate_order_id' => null]);
        $this->query->leftJoin(Country::tableName(),
            Order::tableName() . '.country_id=' . Country::tableName() . '.id');

        $this->buildSelect()->applyFilters($params);

        $dataArray = $this->query->all();

        $dataProvider = new ArrayDataProvider([
            'allModels' => $dataArray,
            'pagination' => false,
        ]);

        return $dataProvider;
    }

    /**
     * @inheritdoc
     */
    protected function buildSelect()
    {
        $this->query->select([
            'country_name' => Country::tableName() . '.name',
            'yearcreatedat' => "DATE_FORMAT(FROM_UNIXTIME(" . Order::tableName() . ".created_at), \"%Y\")",
            'monthcreatedat' => "DATE_FORMAT(FROM_UNIXTIME(" . Order::tableName() . ".created_at), \"%m\")",
            'weekcreatedat' => "DATE_FORMAT(FROM_UNIXTIME(" . Order::tableName() . ".created_at), \"%u\")",
            'daycreatedat' => "DATE_FORMAT(FROM_UNIXTIME(" . Order::tableName() . ".created_at), \"%d\")",
            'groupbyname' => "DATE_FORMAT(FROM_UNIXTIME(" . Order::tableName() . ".created_at), \"%d.%m.%Y\")",
        ])->groupBy(['country_name', 'groupbyname']);

        $mapStatuses = $this->getMapStatuses();
        foreach ($mapStatuses as $name => $statuses) {
            $sumCond = $this->query->buildIfCondition(Order::tableName() . '.status_id', $statuses, Order::tableName().'.id', 'NULL');
            $this->query->addCountDistinctCondition($sumCond, $name);
        }

        return $this;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $content = '';
        return $content;
    }

    /**
     * @return array
     */
    public function getMapStatuses()
    {
        if (empty($this->mapStatuses)) {
            $this->mapStatuses = [
                Yii::t('common', 'Лидов') => OrderStatus::getAllList(),
            ];
        }

        return $this->mapStatuses;
    }

    /**
     * @param array $params
     * @return bool
     */
    public function applyFilters($params)
    {
        $this->getDailyFilter();        //->apply($this->query, $params);
        $this->getCountrySelectFilter()->apply($this->query, $params);
        $this->getProductSelectFilter()->apply($this->query, $params);
        $this->getCuratorUserFilter()->apply($this->query, $params);
        $this->getTypeDatePartFilter()->apply($this->query, $params);
        $this->getTypeOrderSourceFilter()->apply($this->query, $params);

        return true;
    }

    /**
     * @return $DailyFilter
     */
    public function getDailyFilter()
    {
        if (is_null($this->dailyFilter)) {
            $this->dailyFilter = new DailyFilterDefaultLastMonth();
            $this->dailyFilter->type = DailyFilterDefaultLastMonth::TYPE_ORDER_CREATED_AT;
        }

        if (isset($this->searchQuery['s']['from']) && isset($this->searchQuery['s']['from'])) {
            $this->dailyFilter->from = $this->searchQuery['s']['from'];
            $this->dailyFilter->to = $this->searchQuery['s']['to'];
        } else {
            $this->dailyFilter->from = date('d.m.Y', time() - (60 * 60 * 24 * 31));
            $this->dailyFilter->to = date('d.m.Y', time());
        }

        $this->query->andWhere([">=", Order::tableName() . ".created_at", Yii::$app->formatter->asTimestamp($this->dailyFilter->from)]);
        $this->query->andWhere(["<=", Order::tableName() . ".created_at", Yii::$app->formatter->asTimestamp($this->dailyFilter->to) + 86399]);

        return $this->dailyFilter;
    }

    /**
     * @return $CountryFilter
     */
    public function getCountrySelectFilter()
    {
        if (is_null($this->countrySelectFilter)) {
            $this->countrySelectFilter = new CountrySelectFilter();
        }
        return $this->countrySelectFilter;
    }

    /**
     * @return $ProductFilter
     */
    public function getProductSelectFilter()
    {
        if (is_null($this->productSelectFilter)) {
            $this->productSelectFilter = new ProductSelectFilter();
        }
        return $this->productSelectFilter;
    }

    /**
     * @return CuratorUserFilter
     */
    public function getCuratorUserFilter()
    {
        if (is_null($this->curatorUserFilter)) {
            $this->curatorUserFilter = new CuratorUserFilter();
        }
        return $this->curatorUserFilter;
    }

    /**
     * @return TypeDatePartFilter
     */
    public function getTypeDatePartFilter()
    {
        if (is_null($this->typeDatePartFilter)) {
            $this->typeDatePartFilter = new TypeDatePartFilter();
        }
        return $this->typeDatePartFilter;
    }

    /**
     * @return TypeOrderSourceFilter
     */
    public function getTypeOrderSourceFilter()
    {
        if (is_null($this->typeOrderSourceFilter)) {
            $this->typeOrderSourceFilter = new TypeOrderSourceFilter();
        }
        return $this->typeOrderSourceFilter;
    }

}
