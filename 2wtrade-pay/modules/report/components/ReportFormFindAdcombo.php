<?php
namespace app\modules\report\components;

use app\modules\report\components\filters\DailyFilter;
use app\modules\report\components\filters\FindAdcomboIdFilter;
use app\modules\report\components\filters\FindAdcomboTypeFilter;
use app\modules\report\helpers\AdcomboApi;
use app\modules\report\models\AdcomboRequest;
use Yii;
use yii\base\InvalidParamException;
use yii\db\Query;
use yii\web\BadRequestHttpException;

/**
 * Class ReportFormFindAdcombo
 * @package app\modules\report\components
 */
class ReportFormFindAdcombo extends ReportForm
{
    /**
     * @var Query
     */
    public $query;

    /**
     * @var DailyFilter
     */
    protected $dailyFilter;
    /**
     * @var FindAdcomboTypeFilter
     */
    protected $typeFilter;

    /**
     * @var FindAdcomboIdFilter
     */
    protected $idFilter;

    /**
     * @var bool
     */
    protected $searchInBase;

    /**
     * @var array
     */
    protected $adcomboParams;

    /**
     * @param array $params
     * @return mixed
     * @throws BadRequestHttpException | InvalidParamException
     */
    public function apply($params)
    {
        $this->searchInBase = isset($params['search-in-base']);

        if ($this->searchInBase) {
            $this->query = AdcomboRequest::find()->where([AdcomboRequest::tableName() . '.country_code' => strtoupper(Yii::$app->user->country->char_code)]);
        }

        $this->adcomboParams = [];

        $this->applyFilters($params);

        if ($this->adcomboParams['timeTo'] == 0 && $this->adcomboParams['timeFrom'] == 0 && empty($this->adcomboParams['id'])) {
            throw new InvalidParamException(Yii::t('common', 'Необходимо указать период или внешний Id.'));
        }

        if (($this->adcomboParams['timeTo'] != 0 || $this->adcomboParams['timeFrom'] != 0) && ($this->adcomboParams['timeTo'] == 0 || $this->adcomboParams['timeFrom'] == 0)) {
            throw new InvalidParamException(Yii::t('common', 'Некорректно задан период.'));
        }

        if ($this->adcomboParams['timeFrom'] < strtotime("-30 days", $this->adcomboParams['timeTo'])) {
            throw new InvalidParamException(Yii::t('common', 'Период не может превышать 30 дней.'));
        }

        if ($this->adcomboParams['timeTo'] > 0) {
            $this->adcomboParams['timeTo'] += 86399;
        }

        if ($this->searchInBase) {
            $adcomboOrders = $this->query->all();
        } else {
            $adcomboOrders = [];
            $totalCount = count($this->adcomboParams['id']);
            $offset = 0;
            $batchSize = 50;
            do {
                $batch = array_slice($this->adcomboParams['id'], $offset, $batchSize);
                $offset += $batchSize;
                $buffer = AdcomboApi::query($batch, $this->adcomboParams['timeFrom'], $this->adcomboParams['timeTo'],
                    $this->adcomboParams['state'], Yii::$app->user->country->char_code);
                $adcomboOrders = array_merge($adcomboOrders, $buffer);
            } while ($offset < $totalCount);
        }

        return $adcomboOrders;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return $this
     */
    public function restore($form)
    {
        return $this;
    }

    /**
     * @param array $params
     * @return $this
     */
    protected function applyFilters($params)
    {
        $result = $this->getDailyFilter()->applyParams($params);
        $this->getTypeFilter()->applyParams($params);
        $ids = $this->getIdFilter()->applyParams($params);
        $this->adcomboParams = array_merge($this->adcomboParams, $result);
        $this->adcomboParams['state'] = $this->getTypeFilter()->state;
        $this->adcomboParams['id'] = $ids;
        if ($this->searchInBase) {
            $this->getDailyFilter()->apply($this->query, $params);
            $this->getTypeFilter()->apply($this->query, $params);
            $this->getIdFilter()->apply($this->query, $params);
        }
        return $this;
    }

    /**
     * @return DailyFilter
     */
    public function getDailyFilter()
    {
        if (is_null($this->dailyFilter)) {
            $this->dailyFilter = new DailyFilter();
            $this->dailyFilter->type = DailyFilter::TYPE_ADCOMBO_CREATED_AT;
        }
        return $this->dailyFilter;
    }

    /**
     * @return FindAdcomboTypeFilter
     */
    public function getTypeFilter()
    {
        if (is_null($this->typeFilter)) {
            $this->typeFilter = new FindAdcomboTypeFilter();
        }
        return $this->typeFilter;
    }

    /**
     * @return FindAdcomboIdFilter
     */
    public function getIdFilter()
    {
        if (is_null($this->idFilter)) {
            $this->idFilter = new FindAdcomboIdFilter();
        }
        return $this->idFilter;
    }

    /**
     * @return bool
     */
    public function getSearchInBase()
    {
        return $this->searchInBase;
    }
}
