<?php

namespace app\modules\report\components;

use app\models\Country;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use app\modules\report\components\filters\CountrySelectFilter;
use app\modules\report\components\filters\DailyFilterDefaultLastMonth;
use app\modules\report\extensions\Query;
use Yii;
use yii\data\ArrayDataProvider;

/**
 * Class ReportFormConsolidateInfo
 * @package app\modules\report\components
 */
class ReportFormConsolidateInfo extends ReportForm
{
    /**
     * @var DailyFilterDefaultLastMonth
     */
    protected $dailyFilter;

    /**
     * @var CountrySelectFilter
     */
    protected $countrySelectFilter;

    /**
     * @var array Маппинг статусов
     */
    protected $mapStatuses = [];

    /**
     * @param array $params
     * @return ArrayDataProvider
     */
    public function apply($params)
    {
        $this->load($params);

        $this->query = new Query();
        $this->query->from([Order::tableName()]);

        $this->buildSelect()->applyFilters($params);

        $dataArray = $this->query->all();

        $dataProvider = new ArrayDataProvider([
            'allModels' => $dataArray,
        ]);

        return $dataProvider;
    }

    /**
     * @inheritdoc
     */
    protected function buildSelect()
    {
        $this->query->select([
            'country_name' => Country::tableName() . '.name',
            'yearcreatedat' => 'Year(From_unixtime(' . Order::tableName() . '.created_at))',
            'monthcreatedat' => 'Month(From_unixtime(' . Order::tableName() . '.created_at))',
            'weekcreatedat' => 'Week(From_unixtime(' . Order::tableName() . '.created_at))',
            'daycreatedat' => 'Day(From_unixtime(' . Order::tableName() . '.created_at))',
        ]);
        $this->query->leftJoin(Country::tableName(), Order::tableName() . '.country_id=' . Country::tableName() . '.id');

        $this->buildSelectMapStatuses();

        return $this;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $content = '';
        return $content;
    }

    /**
     * @return array
     */
    public function getMapStatuses()
    {
        if (empty($this->mapStatuses)) {
            $this->mapStatuses = [
                Yii::t('common', 'Подтверждено') => OrderStatus::getApproveList(),
            ];
        }

        return $this->mapStatuses;
    }

    /**
     * @param array $params
     * @return bool
     */
    public function applyFilters($params)
    {
        $this->getDailyFilter()->apply($this->query, $params);
        $this->getCountrySelectFilter()->apply($this->query, $params);

        return true;
    }

    /**
     * @return DailyFilterDefaultLastMonth
     */
    public function getDailyFilter()
    {
        if (is_null($this->dailyFilter)) {
            $this->dailyFilter = new DailyFilterDefaultLastMonth();
            $this->dailyFilter->type = DailyFilterDefaultLastMonth::TYPE_ORDER_CREATED_AT;
        }

        return $this->dailyFilter;
    }

    /**
     * @return CountrySelectFilter
     */
    public function getCountrySelectFilter()
    {
        if (is_null($this->countrySelectFilter)) {
            $this->countrySelectFilter = new CountrySelectFilter();
        }
        return $this->countrySelectFilter;
    }

}
