<?php

namespace app\modules\report\components;

use app\models\Country;
use app\models\Currency;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use app\modules\report\components\filters\CountrySelectFilter;
use app\modules\report\components\filters\ProductSelectFilter;
use app\modules\report\components\filters\CuratorUserFilter;
use app\modules\report\components\filters\DailyFilterDefaultLastMonth;
use app\modules\report\components\filters\TypeDatePartFilter;
use app\modules\report\extensions\Query;
use Yii;
use yii\data\ArrayDataProvider;
use app\models\CurrencyRate;

/**
 * Class ReportFormAverageCheckByCountry
 * @package app\modules\report\components
 */
class ReportFormAverageCheckByCountry extends ReportForm
{
    /**
     * @var DailyFilterDefaultLastMonth
     */
    protected $dailyFilter;

    /**
     * @var CountrySelectFilter
     */
    protected $countrySelectFilter;

    /**
     * @var ProductSelectFilter
     */
    protected $productSelectFilter;

    /**
     * @var curatorUserFilter
     */
    protected $curatorUserFilter;

    /**
     * @var TypeDatePartFilter
     */
    protected $typeDatePartFilter;

    /**
     * @var array Маппинг статусов
     */
    protected $mapStatuses = [];

    /*
     * "Галочка" конвертировать в доллар всегда отмечена
     * @var string
     */
    public $dollar = 'on';

    /**
     * @param array $params
     * @return ArrayDataProvider
     */
    public function apply($params)
    {
        $this->load($params);

        $this->query = new Query();
        $this->query->from([Order::tableName()]);

        $this->buildSelect()->applyFilters($params);

        $dataArray = $this->query->all();

        $dataProvider = new ArrayDataProvider([
            'allModels' => $dataArray,
        ]);

        return $dataProvider;
    }

    /**
     * @inheritdoc
     */
    protected function buildSelect()
    {
        $this->query->select([
            'country_name' => Country::tableName() . '.name',
            'yearcreatedat' => 'Year(From_unixtime(' . Order::tableName() . '.created_at))',
            'monthcreatedat' => 'Month(From_unixtime(' . Order::tableName() . '.created_at))',
            'weekcreatedat' => 'Week(From_unixtime(' . Order::tableName() . '.created_at))',
            'daycreatedat' => 'Day(From_unixtime(' . Order::tableName() . '.created_at))',
        ]);

        $statuses = $this->getMapStatusesAsArray();

        $this->query->leftJoin(Country::tableName(),
            Order::tableName() . '.country_id=' . Country::tableName() . '.id');

        $convertPriceTotalToCurrencyId = Country::tableName() . '.currency_id';
        if ($this->dollar) {
            $usdCurrency = Currency::getUSD();
            $convertPriceTotalToCurrencyId = $usdCurrency->id;
        }

        $caseApprove = $this->query->buildCaseCondition(
            'convertToCurrencyByCountry(' . Order::tableName() . '.price_total, ' . Order::tableName() . '.price_currency,' . $convertPriceTotalToCurrencyId . ')',
            Order::tableName() . '.status_id',
            $statuses
        );

        $caseBuyOut = $this->query->buildCaseCondition(
            'convertToCurrencyByCountry(' . Order::tableName() . '.price_total + ' . Order::tableName() . '.delivery, ' . Order::tableName() . '.price_currency,' . $convertPriceTotalToCurrencyId . ')',
            Order::tableName() . '.status_id',
            OrderStatus::getBuyoutList()
        );

        $this->query->addAvgCondition($caseApprove, Yii::t('common', 'Ср. чек КЦ'));
        $this->query->addAvgCondition($caseBuyOut, Yii::t('common', 'Выкуп'));

        $this->buildSelectMapStatuses();

        return $this;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $content = '';
        return $content;
    }

    /**
     * @return array
     */
    public function getMapStatuses()
    {
        if (empty($this->mapStatuses)) {
            $this->mapStatuses = [
                Yii::t('common', 'В процессе') => OrderStatus::getProcessDeliveryList(),
                Yii::t('common', 'Не выкуплено') => OrderStatus::getNotBuyoutList(),
                Yii::t('common', 'Выкуплено') => OrderStatus::getBuyoutList(),
                Yii::t('common', 'Деньги получены') => [
                    OrderStatus::STATUS_FINANCE_MONEY_RECEIVED
                ],
            ];

            $this->mapStatuses[Yii::t('common', 'Всего заказов')] = $this->getMapStatusesAsArray();
        }

        return $this->mapStatuses;
    }

    /**
     * @param array $params
     * @return bool
     */
    public function applyFilters($params)
    {
        $this->getDailyFilter()->apply($this->query, $params);
        $this->getCountrySelectFilter()->apply($this->query, $params);
        $this->getProductSelectFilter()->apply($this->query, $params);
        $this->getCuratorUserFilter()->apply($this->query, $params);
        $this->getTypeDatePartFilter()->apply($this->query, $params);

        return true;
    }

    /**
     * @return DailyFilterDefaultLastMonth
     */
    public function getDailyFilter()
    {
        if (is_null($this->dailyFilter)) {
            $this->dailyFilter = new DailyFilterDefaultLastMonth();
            $this->dailyFilter->type = DailyFilterDefaultLastMonth::TYPE_ORDER_CREATED_AT;
        }

        return $this->dailyFilter;
    }

    /**
     * @return CountrySelectFilter
     */
    public function getCountrySelectFilter()
    {
        if (is_null($this->countrySelectFilter)) {
            $this->countrySelectFilter = new CountrySelectFilter();
        }
        return $this->countrySelectFilter;
    }

    /**
     * @return ProductSelectFilter
     */
    public function getProductSelectFilter()
    {
        if (is_null($this->productSelectFilter)) {
            $this->productSelectFilter = new ProductSelectFilter();
        }
        return $this->productSelectFilter;
    }

    /**
     * @return CuratorUserFilter
     */
    public function getCuratorUserFilter()
    {
        if (is_null($this->curatorUserFilter)) {
            $this->curatorUserFilter = new CuratorUserFilter();
        }
        return $this->curatorUserFilter;
    }

    /**
     * @return TypeDatePartFilter
     */
    public function getTypeDatePartFilter()
    {
        if (is_null($this->typeDatePartFilter)) {
            $this->typeDatePartFilter = new TypeDatePartFilter();
        }
        return $this->typeDatePartFilter;
    }

}
