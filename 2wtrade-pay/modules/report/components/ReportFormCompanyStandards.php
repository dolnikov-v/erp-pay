<?php

namespace app\modules\report\components;

use app\models\Country;
use app\modules\report\components\filters\CompanyStandardsMonthFilter;
use app\modules\report\components\filters\CountryDeliverySelectMultipleFilter;
use app\modules\report\models\CompanyStandards;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use Yii;

/**
 * Class ReportFormCompanyStandards
 * @package app\modules\report\components\
 */
class ReportFormCompanyStandards extends ReportForm
{
    public $groupBy = ReportForm::GROUP_BY_CREATED_AT;

    /**
     * @var CountryDeliverySelectMultipleFilter
     */
    protected $countryDeliverySelectMultipleFilter;

    /**
     * @var CompanyStandardsMonthFilter
     */
    protected $monthFilter;

    /**
     * @param array $params
     * @return ActiveDataProvider|ArrayDataProvider
     */
    public function apply($params)
    {
        if (empty($params)) {
            $params[$this->getCountryDeliverySelectMultipleFilter()
                ->formName()]['country_ids'] = [Yii::$app->user->country->id];
        }
        $sort[CompanyStandards::tableName() . '.country_id'] = SORT_ASC;
        if (!isset($params['sort'])) {
            $sort[CompanyStandards::tableName() . '.month'] = SORT_ASC;
        }

        $this->load($params);

        $this->query = CompanyStandards::find()
            ->innerJoin(Country::tableName(), 'country.id=' . CompanyStandards::tableName() . '.country_id')
            ->joinWith('companyStandardsDelivery')
            ->joinWith('companyStandardsDelivery.delivery');

        $this->query->orderBy($sort);
        $this->applyFilters($params);
        $dataProvider = new ActiveDataProvider([
            'query' => $this->query,
            'pagination' => false,
        ]);
        return $dataProvider;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $output = '';

        return $output;
    }

    /**
     * @inheritdoc
     */
    protected function buildSelect()
    {
        return $this;
    }

    /**
     * @param array $params
     * @return ReportForm
     */
    protected function applyFilters($params)
    {
        $this->getMonthFilter()->apply($this->query, $params);
        $this->getCountryDeliverySelectMultipleFilter()->apply($this->query, $params);

        return $this;
    }

    /**
     * @return CountryDeliverySelectMultipleFilter
     */
    public function getCountryDeliverySelectMultipleFilter()
    {
        if (is_null($this->countryDeliverySelectMultipleFilter)) {
            $this->countryDeliverySelectMultipleFilter = new CountryDeliverySelectMultipleFilter([
                'deliveryType' => CountryDeliverySelectMultipleFilter::DELIVERY_TYPE_DELIVERY,
            ]);
        }

        return $this->countryDeliverySelectMultipleFilter;
    }

    /**
     * @return CompanyStandardsMonthFilter
     */
    public function getMonthFilter()
    {
        if (is_null($this->monthFilter)) {
            $this->monthFilter = new CompanyStandardsMonthFilter([
                'from' => Yii::$app->formatter->asMonth(strtotime(' - 1month', time())),
                'to' => Yii::$app->formatter->asMonth(time())
            ]);
        }
        return $this->monthFilter;
    }
}
