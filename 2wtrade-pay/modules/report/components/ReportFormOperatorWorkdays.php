<?php
namespace app\modules\report\components;

use app\modules\callcenter\models\CallCenterRequest;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use app\modules\report\components\filters\DailyFilterDefaultLastMonth;
use app\modules\report\extensions\Query;
use app\models\Country;
use Yii;
use yii\data\ArrayDataProvider;


/**
 * Class ReportFormOperatorWorkdays
 * @package app\modules\report\components
 */
class ReportFormOperatorWorkdays extends ReportForm
{
    /**
     * @var array Маппинг статусов
     */
    protected $mapStatuses = [];

    /**
     * @var DailyFilterDefaultLastMonth
     */
    protected $dailyFilter;

    /**
     * @var array $operators
     */
    private $operators = [];

    /**
     * @param array $params
     * @return ArrayDataProvider
     */
    public function apply($params)
    {
        $this->load($params);

        $this->query = new Query();
        $this->query->from([Order::tableName()]);

        $this->buildSelect()
            ->applyFilters($params);

        $this->query->orderBy(['last_foreign_operator' => SORT_ASC]);

        $rowData = $this->query->all();

        $dateFrom = date("Y-m-d", strtotime($this->dailyFilter->from));
        $dateTo = date("Y-m-d", strtotime($this->dailyFilter->to));

        $dates = [];
        while (strtotime($dateFrom) <= strtotime($dateTo)) {
            $dates[$dateFrom] = [];
            $dateFrom = date("Y-m-d", strtotime("+1 day", strtotime($dateFrom)));
        }

        foreach ($rowData as $row) {
            $this->operators[$row['country'] .'-' .$row['call_center_id'] .'-' .$row['last_foreign_operator']][$row['date']] = ['podtverzdeno' => $row['podtverzdeno'], 'vsego' => $row['vsego']];
        }

        $allModels = $this->buildResultModel($dates);

        $dataProvider = new ArrayDataProvider([
            'allModels' => $allModels,
        ]);

        $dataProvider->pagination->pageSize = $dataProvider->pagination->totalCount;

        return $dataProvider;

    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $output = '';

        return $output;
    }

    /**
     * @return array
     */
    public function getMapStatuses()
    {
        if (empty($this->mapStatuses)) {
            $this->mapStatuses = [
                Yii::t('common', 'Подтверждено') => OrderStatus::getApproveList(),
                Yii::t('common', 'Всего') => OrderStatus::getAllList(),
            ];
        }

        return $this->mapStatuses;
    }

    /**
     * @inheritdoc
     */
    protected function buildSelect()
    {
        $this->getMapStatuses();

        $this->query->select([
            'last_foreign_operator' =>  CallCenterRequest::tableName() . '.last_foreign_operator',
            'date' => "DATE_FORMAT(FROM_UNIXTIME(" . CallCenterRequest::tableName() . ".approved_at), \"%Y-%m-%d\")",
            'country' => Country::tableName() .'.char_code',
            'call_center_id' => CallCenterRequest::tableName() . '.call_center_id',
        ]);

        $this->query->leftJoin(CallCenterRequest::tableName(), CallCenterRequest::tableName() .'.order_id=' .Order::tableName() .'.id');
        $this->query->leftJoin(Country::tableName(), Country::tableName() .'.id=' .Order::tableName() .'.country_id');

        $this->buildSelectMapStatuses();

        $this->query->andWhere(['is not', CallCenterRequest::tableName() . '.last_foreign_operator', null]);
        $this->query->andWhere(['is not', CallCenterRequest::tableName() . '.approved_at', null]);

        $this->query->groupBy([CallCenterRequest::tableName() . '.last_foreign_operator', 'date']);

        return $this;
    }

    /**
     * @return DailyFilterDefaultLastMonth
     */
    public function getDailyFilter()
    {
        if (is_null($this->dailyFilter)) {
            $this->dailyFilter = new DailyFilterDefaultLastMonth();
            $this->dailyFilter->type = DailyFilterDefaultLastMonth::TYPE_CALL_CENTER_APPROVED_AT;
        }

        return $this->dailyFilter;
    }

    /**
     * @param array $params
     * @return ReportForm
     */
    protected function applyFilters($params)
    {
        $this->getDailyFilter()->apply($this->query, $params);
        $this->applyBaseFilters($params);
        return $this;
    }

    /**
     * Cтроим итоговую модель
     * @param $dateArray
     * @return array
     */
    private function buildResultModel($dateArray) {

        $result = [];
        foreach ($this->operators as $op_key => $operator) {
            $total = [];
            $days = 0;
            foreach ($dateArray as $d_key => $date) {
                if (key_exists($d_key, $operator)) {
                    $total[$d_key] = $operator[$d_key]['podtverzdeno'];
                    $days++;
                }
                else {
                    $total[$d_key] = '-';
                }
            }

            $result[] = Array('operator' => $op_key, 'workdays' => $days, 'data' => $total);
        }

        return $result;
    }

}
