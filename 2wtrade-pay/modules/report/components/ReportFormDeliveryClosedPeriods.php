<?php

namespace app\modules\report\components;

use app\modules\delivery\models\Delivery;
use app\modules\report\extensions\DataProviderClosePeriods;
use yii\data\ArrayDataProvider;
use app\modules\report\extensions\Query;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class ReportFormDeliveryClosedPeriods
 * @package app\modules\report\components
 */
class ReportFormDeliveryClosedPeriods extends ReportForm
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->groupByCollection = [
            self::GROUP_BY_CREATED_AT => Yii::t('common', 'Дата создания'),
            self::GROUP_BY_UPDATED_AT => Yii::t('common', 'Дата обновления'),
            self::GROUP_BY_CALL_CENTER_SENT_AT => Yii::t('common', 'Дата отправки (колл-центр)'),
            self::GROUP_BY_CALL_CENTER_APPROVED_AT => Yii::t('common', 'Дата одобрения (колл-центр)'),
            self::GROUP_BY_DELIVERY_CREATED_AT => Yii::t('common', 'Дата создания (курьерка)'),
            self::GROUP_BY_DELIVERY_SENT_AT => Yii::t('common', 'Дата отправки (курьерка)'),
            self::GROUP_BY_DELIVERY_SENT_AT_NOT_EMPTY => Yii::t('common', 'Дата передачи заказа в курьерку'),
            self::GROUP_BY_DELIVERY_RETURNED_AT => Yii::t('common', 'Дата возврата (курьерка)'),
            self::GROUP_BY_DELIVERY_APPROVED_AT => Yii::t('common', 'Дата утверждения (курьерка)'),
            self::GROUP_BY_DELIVERY_ACCEPTED_AT => Yii::t('common', 'Дата принятия (курьерка)'),
            self::GROUP_BY_DELIVERY_PAID_AT => Yii::t('common', 'Дата оплаты (курьерка)'),
        ];
    }

    /**
     * @var string Группировка по умолчанию
     */
    public $groupBy = self::GROUP_BY_DELIVERY_SENT_AT_NOT_EMPTY;


    /**
     * @var array Маппинг статусов
     */
    protected $mapStatuses = [];

    /**
     * @param array $params
     * @return ArrayDataProvider
     */
    public function apply($params = [])
    {
        $this->load($params);

        $this->query = new Query();
        $this->query
            ->from([Order::tableName()])
            ->where([
                Order::tableName() . '.status_id' => $this->getMapStatusesAsArray(),
                Order::tableName() . '.country_id' => Yii::$app->user->country->id,
            ])
            ->groupBy(['date', 'delivery_id'])
            ->orderBy(['date' => SORT_DESC]);

        $this->buildSelect()
            ->applyFilters($params);

        $dataProvider = new DataProviderClosePeriods([
            'form' => $this,
            'query' => $this->query,
        ]);

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getMapStatuses()
    {
        if (empty($this->mapStatuses)) {
            $this->mapStatuses = [
                'buyout' => [ // выкуплено
                    OrderStatus::STATUS_DELIVERY_BUYOUT
                ],
                'in_process' =>  [ // В процессе
                    OrderStatus::STATUS_CC_APPROVED,
                    OrderStatus::STATUS_LOG_ACCEPTED,
                    OrderStatus::STATUS_LOG_GENERATED,
                    OrderStatus::STATUS_LOG_SET,
                    OrderStatus::STATUS_LOG_PASTED,
                    OrderStatus::STATUS_DELIVERY_PENDING,
                ],
                'in_delivery' => [ // В курьерке
                    OrderStatus::STATUS_DELIVERY_ACCEPTED,
                    OrderStatus::STATUS_DELIVERY_LEFT_WAREHOUSE,
                    OrderStatus::STATUS_DELIVERY,
                    OrderStatus::STATUS_DELIVERY_LEFT_OUR_WAREHOUSE,
                    OrderStatus::STATUS_DELIVERY_REDELIVERY,
                    OrderStatus::STATUS_DELIVERY_LEFT_THEIR_WAREHOUSE,
                    OrderStatus::STATUS_DELIVERY_DELAYED,
                ],
                'not_buyout' => OrderStatus::getNotBuyoutList(),
                'money_received' => [ // деньги получены
                    OrderStatus::STATUS_FINANCE_MONEY_RECEIVED,
                ],
            ];
        }

        return $this->mapStatuses;
    }

    /**
     * @param string $value
     * @return string
     */
    public static function monthName($value)
    {
        $months = [
            'Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
            'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь',
        ];

        return ArrayHelper::getValue($months, intval($value) - 1);
    }

    /**
     * @inheritdoc
     */
    protected function buildSelect()
    {
        $this->query->addSelect([
            'delivery_id' => Delivery::tableName() . '.id',
            'delivery_name' => Delivery::tableName() . '.name',
            'count' => 'count(1)',
        ]);

        foreach ($this->getMapStatuses() as $name => $statuses) {
            $condition = 'if(' . Order::tableName() . '.status_id in (' . join(', ', $statuses) . '), 1, 0)';
            $this->query->addSelect([$name => 'sum(' . $condition . ')']);
        }

        return $this;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $output = '';

        return $output;
    }

    protected function applyFilters($params)
    {
        $this->getProductFilter()->apply($this->query, $params);
        $this->getDeliveryFilter()->apply($this->query, $params);
        $this->applyBaseFilters($params);

        return $this;
    }

}