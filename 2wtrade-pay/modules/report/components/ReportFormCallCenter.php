<?php

namespace app\modules\report\components;

use app\models\Country;
use app\models\Currency;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use app\modules\report\components\filters\DateFilter;
use app\modules\report\components\filters\LeadProductFilter;
use app\modules\report\extensions\DataProvider;
use app\modules\report\extensions\Query;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * Class ReportFormCallCenter todo - сейчас этот класс, как и DataProviderCallCenter к нему, - куски говна, нужно будет переписать это всё "красиво"
 * @package app\modules\report\components\
 */
class ReportFormCallCenter extends ReportForm
{
    const SUB_QUERY_ALIAS = 'orders';

    /**
     * @var array
     */
    public static $currencies;

    /**
     * @var string Группировка по умолчанию
     */
    public $groupBy = self::GROUP_BY_CALL_CENTER_SENT_AT;

    /**
     * @var array Маппинг статусов
     */
    protected $mapStatuses = [];

    /**
     * @var LeadProductFilter
     */
    protected $leadProductFilter;

    /**
     * @inheritdoc
     * @var array $oper_activites
     */
    public function init()
    {
        parent::init();

        if (!is_array(self::$currencies)) {
            $currency = Country::find()
                ->joinWith(['currencyRate']);

            self::$currencies = ArrayHelper::map($currency->all(), 'id', 'currencyRate.rate');
        }

        $this->groupByCollection[self::GROUP_BY_COUNTRY_DATE] = Yii::t('common', 'Страна') . ' \\ ' . Yii::t('common', 'Дата');
        $this->groupByCollection[self::GROUP_BY_LEAD_PRODUCTS] = Yii::t('common', 'Товар лида');
        $this->groupByCollection[self::GROUP_BY_PRODUCT_CATEGORY] = Yii::t('common', 'Категория товаров');
        $this->groupByCollection[self::GROUP_BY_COUNTRY_LEAD_PRODUCTS] = Yii::t('common', 'Страна') . ' \\ ' . Yii::t('common', 'Товар лида');
        $this->groupByCollection[self::GROUP_BY_YEAR_MONTH] = Yii::t('common', 'Год, месяц');
    }

    /**
     * @param array $params
     * @return DataProvider
     */
    public function apply($params)
    {
        if (empty($params)) {
            $params['s']['country_ids'] = [Yii::$app->user->getCountry()->id];
        }

        $this->getDateFilter()->type = DateFilter::TYPE_CALL_CENTER_SENT_AT;
        $this->load($params);

        $this->query = new Query();
        $this->query->from([Order::tableName()]);
        $this->query->andWhere([Order::tableName() . '.duplicate_order_id' => null]);
        $this->query->leftJoin(Country::tableName(), Country::tableName() . '.id=' . Order::tableName() . '.country_id');
        $this->query->addCondition(Country::tableName() . '.id', 'country_id');
        $this->query->leftJoin(CallCenterRequest::tableName(), CallCenterRequest::tableName() . '.order_id=' . Order::tableName() . '.id');
        $this->query->leftJoin(DeliveryRequest::tableName(), DeliveryRequest::tableName() . '.order_id=' . Order::tableName() . '.id');
        $this->applyFilters($params);
        // Исключаем запрещенные источники заказов
        $this->query->andWhere(['NOT IN', Order::tableName() . '.source_id', Yii::$app->user->rejectedSources]);
        /*
         *      Среднее число поптыток дозвона не можем достоверно получить, т.к. у нас нет информации по звонкам: на какой заказ был звонок
         *
                $this->query->leftJoin(CallCenterUser::tableName(),
                    CallCenterUser::tableName() . '.user_id=' . CallCenterRequest::tableName() . '.last_foreign_operator and '.
                    CallCenterUser::tableName() . '.callcenter_id=' . CallCenterRequest::tableName() . '.call_center_id'
                );

                $subQuery = CallCenterWorkTime::find()
                    ->select([
                        'avg_calls' => 'AVG(' . CallCenterWorkTime::tableName() . '.number_of_calls)',
                        'sum_calls' => 'SUM(' . CallCenterWorkTime::tableName() . '.number_of_calls)',
                        'call_center_user_id' => CallCenterWorkTime::tableName() . '.call_center_user_id'
                    ])
                    ->where([
                        'and',
                        ['>=', CallCenterWorkTime::tableName() . '.date', date('Y-m-d', strtotime($this->getDateFilter()->from))],
                        ['<=', CallCenterWorkTime::tableName() . '.date', date('Y-m-d', strtotime($this->getDateFilter()->to))]
                    ])
                    ->groupBy('call_center_user_id');

                $this->query->leftJoin(['calls' => $subQuery], 'calls.call_center_user_id = ' . CallCenterUser::tableName() . '.id');
        */

        $this->buildSelect();
        $dataProvider = new DataProvider([
            'form' => $this,
            'query' => $this->query,
        ]);

        return $dataProvider;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $output = '';

        return $output;
    }

    /**
     * @return array
     */
    public function getMapStatuses()
    {
        if (empty($this->mapStatuses)) {
            $this->mapStatuses = [
                Yii::t('common', 'Обработка') => OrderStatus::getInitialList(),
                Yii::t('common', 'Перезвоны') => [
                    OrderStatus::STATUS_CC_RECALL,
                ],
                Yii::t('common', 'Недозвон') => [
                    OrderStatus::STATUS_CC_FAIL_CALL,
                ],
                Yii::t('common', 'Подтверждено') => OrderStatus::getApproveCCList(),
                Yii::t('common', 'Отклонено') => [
                    OrderStatus::STATUS_CC_REJECTED,
                ],
                Yii::t('common', 'Брак') => [
                    OrderStatus::STATUS_CC_TRASH,
                    OrderStatus::STATUS_CC_DOUBLE,
                ],
                Yii::t('common', 'На проверке КЦ') => [
                    OrderStatus::STATUS_CC_CHECKING,
                ],
            ];
        }


        return $this->mapStatuses;
    }

    /**
     * @inheritdoc
     */
    protected function buildSelect()
    {
        $convertPriceTotalToCurrencyId = Country::tableName() . '.currency_id';
        if ($this->dollar) {
            $usdCurrency = Currency::getUSD();
            $convertPriceTotalToCurrencyId = $usdCurrency->id;
        }

        $caseApprove = $this->query->buildCaseCondition(
            'convertToCurrencyByCountry(' . Order::tableName() . '.price_total + ' . Order::tableName() . '.delivery, ' . Order::tableName() . '.price_currency,' . $convertPriceTotalToCurrencyId . ')',
            Order::tableName() . '.status_id',
            OrderStatus::getApproveList()
        );
        $this->query->addAvgCondition($caseApprove, Yii::t('common', 'Средний чек апрувленный'));

        $this->query->addCountDistinctCondition(
            CallCenterRequest::tableName() . '.call_center_id, ' . CallCenterRequest::tableName() . '.last_foreign_operator',
            Yii::t('common', 'Операторов'));
        /*
                $this->query->addSelect([
                    'avg_calls_by_opers' => 'AVG(calls.avg_calls)',
                ]);
        */
        $this->buildSelectMapStatuses();

        return $this;
    }

    /**
     * @return LeadProductFilter
     */
    public function getLeadProductFilter()
    {
        if (is_null($this->leadProductFilter)) {
            $this->leadProductFilter = new LeadProductFilter(['reportForm' => $this]);
        }

        return $this->leadProductFilter;
    }


    /**
     * @param array $params
     * @return ReportForm
     */
    protected function applyFilters($params)
    {
        $this->getDateFilter()->apply($this->query, $params);
        $this->getLeadProductFilter()->apply($this->query, $params);
        $this->getProductCategoryFilter()->apply($this->query, $params);
        $this->getCallCenterFilter()->apply($this->query, $params);
        $this->getCountrySelectFilter()->apply($this->query, $params);
        $this->getSourceFilter()->apply($this->query, $params);
        $this->applyBaseFilters($params);

        return $this;
    }

    /**
     * @param array $params
     * @return ReportForm
     */
    protected function applyBaseFilters($params)
    {
        $this->getCalculatePercentFilter()->apply($this->query, $params);
        $this->getPaymentAdvertisingFilter()->apply($this->query, $params);
        $this->getCalculateDollarFilter()->apply($this->query, $params);

        return $this;
    }
}
