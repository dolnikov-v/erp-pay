<?php
namespace app\modules\report\components;

use app\modules\callcenter\models\CallCenterRequest;
use app\modules\callcenter\models\CallCenterRequestUpdateResponse;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use app\modules\report\extensions\Query;
use yii\data\ArrayDataProvider;

/**
 * Class ReportFormLeadByCountry
 * @package app\modules\report\components
 */
class ReportFormForecastWorkloadOperatorsByHours extends ReportForm
{

    /** @var array */
    private static $ccMapStatuses = [
        1 => OrderStatus::STATUS_CC_POST_CALL,
        4 => OrderStatus::STATUS_CC_APPROVED,
        5 => OrderStatus::STATUS_CC_FAIL_CALL,
        6 => OrderStatus::STATUS_CC_RECALL,
        7 => OrderStatus::STATUS_CC_REJECTED,
        8 => OrderStatus::STATUS_CC_REJECTED,
        9 => OrderStatus::STATUS_CC_APPROVED,
        10 => OrderStatus::STATUS_CC_REJECTED,
        11 => OrderStatus::STATUS_CC_REJECTED,
        12 => OrderStatus::STATUS_CC_APPROVED,
        13 => OrderStatus::STATUS_CC_REJECTED,
        15 => OrderStatus::STATUS_CC_DOUBLE,
        17 => OrderStatus::STATUS_CC_TRASH,
        19 => OrderStatus::STATUS_CC_TRASH
    ];


    /**
     * @param array $params
     * @return ArrayDataProvider
     */
    public function apply($params)
    {
        $this->load($params);
        // Считаем количество дней в периоде (для среднего)
        $period = floor(((strtotime($params['s']['to']) + 86399) - strtotime($params['s']['from']))/(60*60*24))+1;

        // Получаем рабочие часы операторов за указанный период
        $workHours = $this->getWorkHours($params);

        $this->query = new Query();
        $this->query->from([Order::tableName()]);
        $this->query->where(["IS NOT", Order::tableName() .".foreign_id", NULL]);
        $this->buildSelect()->applyFilters($params);

        $dataArray = $this->query->all();

        // Перебрасываем лиды в нерабочих часах на первый рабочий час операторов
        if (!empty($dataArray) && !empty($workHours)) {
            $firstWorkHourIndex = intval($workHours[0]);
            $indexToRemove = [];
            foreach ($dataArray as $item) {
                if (!in_array($item['hour'], $workHours)) {
                    $indexToRemove[] = intval($item['hour']);
                    $dataArray[$firstWorkHourIndex]['leadcount'] += $item['leadcount'];
                }
            }
            foreach ($indexToRemove as $removeItem) {
                unset($dataArray[$removeItem]);
            }
            $dataArray = array_values($dataArray);
            foreach ($dataArray as &$item) {
                $item['leadcount'] = $item['leadcount'] / $period;
            }
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $dataArray,
            'pagination' => false,
        ]);

        return $dataProvider;
    }

    /**
     * @inheritdoc
     */
    protected function buildSelect()
    {
        $this->query->select([
            'hour' => "DATE_FORMAT(FROM_UNIXTIME(" .Order::tableName() .".updated_at), \"%H\")",
        ]);
        $this->query->addInConditionCount('status_id', OrderStatus::getAllList(), 'leadcount');
        $this->query->groupBy(['hour']);

        return $this;
    }

    /**
     * @param array $params
     * @return $this
     */
    public function applyFilters($params)
    {
        $this->getDateFilter()->apply($this->query, $params);
        $this->getCountryFilter()->apply($this->query, $params);
        $this->getProductFilter()->apply($this->query, $params);

        return $this;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $content = '';
        return $content;
    }


    /**
     * Возвращаем массив рабочих часов операторов из распарсенного json, согласно полученных параметров фильтров
     * @param $params
     * @return array
     * @throws \yii\mongodb\Exception
     * @throws \Exception
     */
    public function getWorkHours($params)
    {
        $from = strtotime($params['s']['from']);
        $to = strtotime($params['s']['to']) + 86399;
        $resArray = [];
        $totalArray = [];

        $offset = 0;
        $limit = 1000;
        $repeat = true;

        while ($repeat) {
            $this->query = new Query();
            $this->query->select([CallCenterRequest::tableName() .'.id']);
            $this->query->from([Order::tableName()]);
            $this->query->leftJoin(CallCenterRequest::tableName(), CallCenterRequest::tableName() .'.order_id=' .Order::tableName() .'.id');
            $this->query->where(["IS NOT", CallCenterRequest::tableName() .'.cc_update_response_at', NULL]);
            $this->query->offset($offset);
            $this->query->limit($limit);
            $this->applyFilters($params);
            $result = $this->query->column();

            if (empty($result)) {
                $repeat = false;
            }
            $offset += $limit;

            $result = CallCenterRequestUpdateResponse::find()->where(['call_center_request_id' => array_map('intval', $result)])->lastResponses();
            foreach ($result as $item) {
                $responseArray = json_decode($item->response, true);
                $historyArray = $responseArray['history'];
                if (!empty($historyArray)) {
                    foreach ($historyArray as $historyItem) {
                        $tmp = strtotime($historyItem['oe_changed']);
                        if ($tmp >= $from && $tmp <= $to) {
                            $hour = date("H", strtotime($historyItem['oe_changed']));
                            if (!in_array($hour, $resArray)) {
                                $resArray[] = $hour;
                            }
                        }
                    }
                }
            }
            sort($resArray);

            foreach ($resArray as $hour) {
                if (!in_array($hour, $totalArray)) {
                    $totalArray[] = $hour;
                }
            }
        }
        sort($totalArray);

        return $totalArray;
    }

}
