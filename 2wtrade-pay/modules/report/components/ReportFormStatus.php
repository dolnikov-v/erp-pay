<?php
namespace app\modules\report\components;

use app\modules\order\models\Order;
use app\modules\report\extensions\DataProvider;
use app\modules\report\extensions\Query;
use Yii;

/**
 * Class ReportFormStatus
 * @package app\modules\report\components
 */
class ReportFormStatus extends ReportForm
{
    /**
     * @var string Группировка по умолчанию
     */
    public $groupBy = self::GROUP_BY_CREATED_AT;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->groupByCollection[self::GROUP_BY_PRODUCTS] = Yii::t('common', 'Товар');
    }

    /**
     * @param array $params
     * @return DataProvider
     */
    public function apply($params)
    {
        $this->load($params);

        $this->query = new Query();
        $this->query->from(Order::tableName());
        $this->query->andWhere([Order::tableName() . '.duplicate_order_id' => null]);
        // Исключаем запрещенные источники заказов
        $this->query->andWhere(['NOT IN', Order::tableName() .'.source_id', Yii::$app->user->rejectedSources]);
        $this->buildSelect()
            ->applyFilters($params);

        $dataProvider = new DataProvider([
            'form' => $this,
            'query' => $this->query,
        ]);



        return $dataProvider;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $output = '';

        return $output;
    }

    /**
     * @inheritdoc
     */
    protected function buildSelect()
    {
        $this->buildSelectStatuses();
        return $this;
    }

    /**
     * @param array $params
     * @return ReportForm
     */
    protected function applyFilters($params)
    {
        $this->getDateFilter()->apply($this->query, $params);
        $this->getProductFilter()->apply($this->query, $params);
        $this->getCallCenterFilter()->apply($this->query, $params);
        $this->getDeliveryFilter()->apply($this->query, $params);
        $this->getDeliveryPartnerFilter()->apply($this->query, $params);
        $this->getSourceFilter()->apply($this->query, $params);

        $this->applyBaseFilters($params);

        return $this;
    }
}
