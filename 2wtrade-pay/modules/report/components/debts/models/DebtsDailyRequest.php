<?php

namespace app\modules\report\components\debts\models;

use app\components\base\Model;

class DebtsDailyRequest extends Model
{
    /**
     * @var null|int|array
     */
    public $country_id;

    /**
     * @var null|int|array
     */
    public $delivery_id;

    /**
     * @var string
     */
    public $date_from;

    /**
     * @var string
     */
    public $date_to;

    /**
     * @var string
     */
    public $interval;

    public function init()
    {
        $dStart = new \DateTime($this->date_from);
        $dEnd  = new \DateTime($this->date_to);

        $dDiff = $dStart->diff($dEnd);

        $this->interval = 'day';
        if ($dDiff->days > 31) {
            $this->interval = 'week';
        }
        if ($dDiff->days > 180) {
            $this->interval = 'month';
        }
    }
}