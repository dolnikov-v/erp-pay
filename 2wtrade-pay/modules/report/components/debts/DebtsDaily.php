<?php

namespace app\modules\report\components\debts;

use app\components\base\Component;
use app\models\Country;
use app\models\CurrencyRate;
use app\models\CurrencyRateHistory;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\deliveryreport\models\PaymentOrder;
use app\modules\order\models\Order;
use app\modules\order\models\OrderFinancePrediction;
use app\modules\order\models\OrderStatus;
use app\modules\report\components\debts\models\DebtsDailyRequest;
use app\modules\report\models\ReportDebtsDaily;
use Yii;

/**
 * Class DebtsDaily
 * @package app\modules\report\components\debts
 */
class DebtsDaily extends Component
{

    /**
     * @var array
     */
    private $rates = [];

    /**
     * @var array
     */
    private $ratesHistory = [];


    /**
     * @param array $currencyId
     * @param array $dates
     */
    private function setRates($currencyId, $dates)
    {
        if (!$this->rates) {
            $rates = CurrencyRate::find()->all();
            foreach ($rates as $row) {
                /** @var CurrencyRate $row */
                if ($row->rate) {
                    $this->rates[$row->currency_id] = $row->rate;
                }
            }
        }

        if (!$this->ratesHistory) {
            $ratesHistory = CurrencyRateHistory::find()
                ->where(['currency_id' => array_keys($currencyId)])
                ->andWhere(['between', 'date', min($dates), max($dates)])
                ->all();

            foreach ($ratesHistory as $row) {
                /** @var CurrencyRateHistory $row */
                if ($row->rate) {
                    $this->ratesHistory[$row->currency_id][$row->date] = $row->rate;
                }
            }
        }
    }

    /**
     * @param $currencyId
     * @param null $date
     * @return int|mixed
     */
    private function getCurrencyRate($currencyId, $date = null)
    {
        if (!$date && $this->rates && isset($this->rates[$currencyId])) {
            return $this->rates[$currencyId];
        }

        if ($date && $this->ratesHistory && isset($this->ratesHistory[$currencyId][$date])) {
            return $this->ratesHistory[$currencyId][$date];
        }

        $ratesHistory = CurrencyRateHistory::find();
        $ratesHistory->where(['currency_id' => $currencyId]);
        if ($date) {
            $ratesHistory->andWhere(['date' => $date]);
        }

        foreach ($ratesHistory->all() as $row) {
            /** @var CurrencyRateHistory $row */
            if ($row->rate) {
                $this->ratesHistory[$row->currency_id][$row->date] = $row->rate;
            }
        }

        if ($date && isset($this->ratesHistory[$currencyId][$date])) {
            return $this->ratesHistory[$currencyId][$date];
        }

        return $this->rates[$currencyId] ?? 0;

    }

    /**
     * @param DebtsDailyRequest $request
     * @return array
     */
    public function loadBaseData(DebtsDailyRequest $request)
    {
        $query = Order::find()
            ->select([
                'date' => 'DATE_FORMAT(FROM_UNIXTIME(' . DeliveryRequest::tableName() . '.approved_at), "%Y-%m-%d")',
                'delivery_id' => DeliveryRequest::tableName() . '.delivery_id',
                'currency_id' => OrderFinancePrediction::tableName() . '.price_cod_currency_id',
                'amount' => 'SUM(CASE WHEN ' . Order::tableName() . '.status_id in ("' . implode('","', OrderStatus::getBuyoutList()) . '") THEN ' . OrderFinancePrediction::tableName() . '.price_cod ELSE 0 END)',
            ])
            ->joinWith(['deliveryRequest', 'financePrediction'], false);

        if ($request->country_id) {
            $query->andWhere([Order::tableName() . '.country_id' => $request->country_id]);
        }

        if ($request->delivery_id) {
            $query->andWhere([DeliveryRequest::tableName() . '.delivery_id' => $request->delivery_id]);
        }
        $query->andWhere(['is not', DeliveryRequest::tableName() . '.delivery_id', null]);

        if ($request->date_from) {
            $query->andWhere([
                '>=',
                DeliveryRequest::tableName() . '.approved_at',
                strtotime($request->date_from . ' 00:00:00')
            ]);
        }

        if ($request->date_to) {
            $query->andWhere([
                '<=',
                DeliveryRequest::tableName() . '.approved_at',
                strtotime($request->date_to . ' 23:23:59')
            ]);
        }
        $query->andWhere(['is not', DeliveryRequest::tableName() . '.approved_at', null]);

        $query->groupBy([
            'delivery_id',
            'date',
            'currency_id'
        ]);
        $query->orderBy([
            'delivery_id' => SORT_ASC,
            'date' => SORT_ASC,
        ]);

        $buyoutData = $query->asArray()->all();

        $currencyId = [];
        $dates = [];
        foreach ($buyoutData as $row) {
            if ($row['currency_id']) {
                $currencyId[$row['currency_id']] = $row['currency_id'];
            }
            if ($row['date']) {
                $dates[$row['date']] = $row['date'];
            }
        }

        if ($request->date_from) {
            $dates[$request->date_from] = $request->date_from;
        }
        if ($request->date_to) {
            $dates[$request->date_to] = $request->date_to;
        }

        if (!$dates) {
            $dates[] = date('Y-m-d');
        }
        $this->setRates($currencyId, $dates);

        $data = [];
        foreach ($buyoutData as $row) {
            if ($row['date']) {
                $rate = $this->getCurrencyRate($row['currency_id'], $row['date']);
                if ($rate) {
                    $data[$row['delivery_id']][$row['date']] = $row['amount'] / $rate;
                }
            }
        }


        // выбираем и вычитаем расходы с группировкой по валюте
        foreach (OrderFinancePrediction::getFinanceTypeOfFields() as $field => $typeOfField) {
            if ($typeOfField == OrderFinancePrediction::FINANCE_TYPE_COST) {

                $costQuery = clone $query;
                $costQuery->addSelect([
                    'currency_id' => OrderFinancePrediction::tableName() . '.' . $field . '_currency_id',
                    'amount' => 'SUM(CASE WHEN ' . Order::tableName() . '.status_id in ("' . implode('","', array_merge(OrderStatus::getBuyoutList(), OrderStatus::getNotBuyoutDeliveryList())) . '") THEN ' . OrderFinancePrediction::tableName() . '.' . $field . ' ELSE 0 END)',
                ]);
                $costQuery->andWhere(['>', OrderFinancePrediction::tableName() . '.' . $field, 0]);
                $costData = $costQuery->asArray()->all();

                foreach ($costData as $row) {
                    if ($row['date']) {
                        $data[$row['delivery_id']][$row['date']] = $data[$row['delivery_id']][$row['date']] ?? 0;
                        $rate = $this->getCurrencyRate($row['currency_id'], $row['date']);
                        if ($rate) {
                            $data[$row['delivery_id']][$row['date']] -= $row['amount'] / $rate;
                        }
                    }
                }
            }
        }

        // платежки
        $query = PaymentOrder::find()
            ->select([
                'date' => 'DATE_FORMAT(FROM_UNIXTIME(' . PaymentOrder::tableName() . '.paid_at), "%Y-%m-%d")',
                'delivery_id' => PaymentOrder::tableName() . '.delivery_id',
                'currency_id' => PaymentOrder::tableName() . '.currency_id',
                'sum' => PaymentOrder::tableName() . '.sum',
            ])
            ->joinWith('delivery', false);

        if ($request->country_id) {
            $query->andWhere([Delivery::tableName() . '.country_id' => $request->country_id]);
        }

        if ($request->delivery_id) {
            $query->andWhere([PaymentOrder::tableName() . '.delivery_id' => $request->delivery_id]);
        }

        if ($request->date_from) {
            $query->andWhere([
                '>=',
                PaymentOrder::tableName() . '.paid_at',
                strtotime($request->date_from . ' 00:00:00')
            ]);
        }

        if ($request->date_to) {
            $query->andWhere([
                '<=',
                PaymentOrder::tableName() . '.paid_at',
                strtotime($request->date_to . ' 23:23:59')
            ]);
        }

        $query->groupBy([
            'delivery_id',
            'date',
            'currency_id'
        ]);

        $query->orderBy([
            'delivery_id' => SORT_ASC,
            'date' => SORT_ASC,
        ]);

        $paymentData = $query->asArray()->all();

        foreach ($paymentData as $row) {
            if ($row['date']) {
                $data[$row['delivery_id']][$row['date']] = $data[$row['delivery_id']][$row['date']] ?? 0;
                $rate = $this->getCurrencyRate($row['currency_id'], $row['date']);
                if ($rate) {
                    $data[$row['delivery_id']][$row['date']] -= $row['sum'] / $rate;
                }
            }
        }

        return $data;
    }

    /**
     * @param array $data
     * @return bool
     * @throws \Exception
     */
    public function saveBaseData($data)
    {
        $insert = [];
        $deliveryIDs = [];
        $dates = [];
        foreach ($data as $deliveryID => $deliveryData) {
            if ($deliveryID) {
                $deliveryIDs[$deliveryID] = $deliveryID;
                foreach ($deliveryData as $date => $amount) {
                    $insert[] = [
                        'delivery_id' => $deliveryID,
                        'date' => $date,
                        'amount' => $amount,
                        'created_at' => time()
                    ];
                    $dates[$date] = $date;
                }
            }
        }
        if ($insert) {
            $transaction = ReportDebtsDaily::getDb()->beginTransaction();
            try {
                if ($deliveryIDs || $dates) {
                    ReportDebtsDaily::deleteAll([
                        'delivery_id' => $deliveryIDs,
                        'date' => $dates
                    ]);
                }
                Yii::$app->db->createCommand()->batchInsert(
                    ReportDebtsDaily::tableName(),
                    [
                        'delivery_id',
                        'date',
                        'amount',
                        'created_at',
                    ],
                    $insert
                )->execute();
                $transaction->commit();
                return true;
            } catch (\Exception $e) {
                return false;
            }
        }
        return false;
    }


    /**
     * @param DebtsDailyRequest $request
     * @return integer
     */
    public function getCreatedAt(DebtsDailyRequest $request)
    {
        $query = ReportDebtsDaily::find()
            ->select([
                'created_at' => 'MAX(' . ReportDebtsDaily::tableName() . '.created_at)',
            ])
            ->joinWith(['delivery'], false);

        if ($request->country_id) {
            $query->andWhere([Delivery::tableName() . '.country_id' => $request->country_id]);
        }

        if ($request->delivery_id) {
            $query->andWhere([ReportDebtsDaily::tableName() . '.delivery_id' => $request->delivery_id]);
        }

        if ($request->date_from) {
            $query->andWhere([
                '>=',
                ReportDebtsDaily::tableName() . '.date',
                $request->date_from
            ]);
        }

        if ($request->date_to) {
            $query->andWhere([
                '<=',
                ReportDebtsDaily::tableName() . '.date',
                $request->date_to
            ]);
        }

        return $query->scalar();
    }

    /**
     * @param DebtsDailyRequest $request
     * @return array
     */
    public function getSavedData(DebtsDailyRequest $request)
    {

        $amountSelect = '(SELECT SUM(c.amount) FROM ' . ReportDebtsDaily::tableName() . ' c WHERE c.date <= ' . ReportDebtsDaily::tableName() . '.date and c.delivery_id = ' . ReportDebtsDaily::tableName() . '.delivery_id)';

        $query = ReportDebtsDaily::find()
            ->select([
                'country_id' => Country::tableName() . '.id',
                'country_name' => Country::tableName() . '.name',
                'delivery_id' => ReportDebtsDaily::tableName() . '.delivery_id',
                'delivery_name' => Delivery::tableName() . '.name',
                'year' => 'DATE_FORMAT(' . ReportDebtsDaily::tableName() . '.date, "%Y")',
                'month' => 'DATE_FORMAT(' . ReportDebtsDaily::tableName() . '.date, "%m")',
                'week' => 'DATE_FORMAT(' . ReportDebtsDaily::tableName() . '.date, "%u")',
                'day' => 'DATE_FORMAT(' . ReportDebtsDaily::tableName() . '.date, "%d")',
                'amount' => $amountSelect,
            ])
            ->joinWith(['delivery', 'delivery.country'], false);

        if ($request->country_id) {
            $query->andWhere([Delivery::tableName() . '.country_id' => $request->country_id]);
        }

        if ($request->delivery_id) {
            $query->andWhere([ReportDebtsDaily::tableName() . '.delivery_id' => $request->delivery_id]);
        }

        if ($request->date_from) {
            $query->andWhere([
                '>=',
                ReportDebtsDaily::tableName() . '.date',
                $request->date_from
            ]);
        }

        if ($request->date_to) {
            $query->andWhere([
                '<=',
                ReportDebtsDaily::tableName() . '.date',
                $request->date_to
            ]);
        }

        $groupBy = [];
        $groupBy[] = ReportDebtsDaily::tableName() . '.delivery_id';
        $groupBy[] = 'year';
        $groupBy[] = 'month';
        if ($request->interval == 'day') {
            $groupBy[] = 'week';
            $groupBy[] = 'day';
        }
        if ($request->interval == 'week') {
            $groupBy[] = 'week';
        }

        $query->groupBy($groupBy);

        $query->orderBy([
            'delivery_id' => SORT_ASC,
            'date' => SORT_ASC,
        ]);

        $data = $query->asArray()->all();

        return $data;
    }
}