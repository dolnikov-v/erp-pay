<?php
namespace app\modules\report\components;

use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use app\modules\report\extensions\DataProvider;
use app\modules\report\extensions\Query;
use Yii;

/**
 * Class ReportFormShort
 * @package app\modules\report\components
 */
class ReportFormShort extends ReportForm
{
    /**
     * @var string Группировка по умолчанию
     */
    public $groupBy = self::GROUP_BY_CREATED_AT;

    /**
     * @var array Маппинг статусов
     */
    protected $mapStatuses = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->groupByCollection[self::GROUP_BY_PRODUCTS] = Yii::t('common', 'Товар');
    }

    /**
     * @param array $params
     * @return DataProvider
     */
    public function apply($params)
    {
        $this->load($params);

        $this->query = new Query();
        $this->query->from(Order::tableName());

        $this->buildSelect()
            ->applyFilters($params);

        $dataProvider = new DataProvider([
            'form' => $this,
            'query' => $this->query,
        ]);

        return $dataProvider;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $output = '';

        return $output;
    }

    /**
     * @return array
     */
    public function getMapStatuses()
    {
        if (empty($this->mapStatuses)) {
            $this->mapStatuses = [
                Yii::t('common', 'Обработка') => OrderStatus::getInitialList(),
                Yii::t('common', 'Перезвоны') => [
                    OrderStatus::STATUS_CC_RECALL,
                ],
                Yii::t('common', 'Подтверждено') => [
                    OrderStatus::STATUS_CC_APPROVED,
                    OrderStatus::STATUS_LOG_ACCEPTED,
                    OrderStatus::STATUS_DELIVERY_PENDING,
                    OrderStatus::STATUS_LOG_DEFERRED,
                ],
                Yii::t('common', 'Распечатано') => [
                    OrderStatus::STATUS_LOG_GENERATED,
                    OrderStatus::STATUS_LOG_SET,
                    OrderStatus::STATUS_LOG_PASTED,
                ],
                Yii::t('common', 'Покинуло наш склад') => [
                    OrderStatus::STATUS_DELIVERY_LEFT_WAREHOUSE,
                    OrderStatus::STATUS_DELIVERY_LEFT_OUR_WAREHOUSE,
                ],
                Yii::t('common', 'Принято курьерской службой') => [
                    OrderStatus::STATUS_DELIVERY_ACCEPTED,
                    OrderStatus::STATUS_DELIVERY_REDELIVERY,
                    OrderStatus::STATUS_DELIVERY_LEFT_THEIR_WAREHOUSE,
                    OrderStatus::STATUS_DELIVERY_DELAYED,
                ],
                Yii::t('common', 'Выкупленные') => [
                    OrderStatus::STATUS_DELIVERY_BUYOUT,
                ],
                Yii::t('common', 'Деньги получены') => [
                    OrderStatus::STATUS_FINANCE_MONEY_RECEIVED,
                ],
                Yii::t('common', 'Доставлен') => [
                    OrderStatus::STATUS_DELIVERY,
                ],
                Yii::t('common', 'Невыкуплен') => [
                    OrderStatus::STATUS_DELIVERY_DENIAL,
                    OrderStatus::STATUS_DELIVERY_RETURNED,
                    OrderStatus::STATUS_DELIVERY_REFUND,
                    OrderStatus::STATUS_DELIVERY_LOST,
                ],
                Yii::t('common', 'Брак') => [
                    OrderStatus::STATUS_CC_TRASH,
                ],
                Yii::t('common', 'Логистика отказ') => [
                    OrderStatus::STATUS_LOGISTICS_REJECTED,
                ],
                Yii::t('common', 'Возврат') => [
                    OrderStatus::STATUS_DELIVERY_REJECTED,
                    OrderStatus::STATUS_LOGISTICS_ACCEPTED,
                ],
                Yii::t('common', 'Колл центр отказ') => [
                    OrderStatus::STATUS_CC_FAIL_CALL,
                    OrderStatus::STATUS_CC_REJECTED,
                ],
                Yii::t('common', 'Дубль') => [
                    OrderStatus::STATUS_CC_DOUBLE,
                ],
            ];
        }

        return $this->mapStatuses;
    }

    /**
     * @inheritdoc
     */
    protected function buildSelect()
    {
        switch ($this->groupBy) {
            case self::GROUP_BY_PRODUCTS:
                $this->buildSelectMapProducts();
                break;
            default:
                $this->buildSelectMapStatuses();
                break;
        }

        return $this;
    }

    /**
     * @param array $params
     * @return ReportForm
     */
    protected function applyFilters($params)
    {
        $this->getDateFilter()->apply($this->query, $params);
        $this->getProductFilter()->apply($this->query, $params);
        $this->getCallCenterFilter()->apply($this->query, $params);
        $this->getDeliveryFilter()->apply($this->query, $params);
        $this->getDeliveryFilter()->apply($this->query, $params);

        $this->applyBaseFilters($params);

        return $this;
    }
}
