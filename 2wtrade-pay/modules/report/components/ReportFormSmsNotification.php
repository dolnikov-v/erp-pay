<?php
namespace app\modules\report\components;

use app\modules\order\models\Order;
use app\modules\order\models\OrderNotification;
use app\modules\order\models\OrderStatus;
use app\modules\order\models\OrderNotificationRequest;
use app\modules\report\extensions\DataProvider;
use app\modules\report\extensions\Query;
use app\modules\report\components\ReportForm;
use Yii;

/**
 * Class ReportFormSmsNotification
 * @package app\modules\report\components
 */
class ReportFormSmsNotification extends ReportForm
{
    /**
     * @var string Группировка по умолчанию
     */
    public $groupBy = ReportForm::GROUPBY_NOTIFICATION_CREATED_AT;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @param array $params
     * @return DataProvider
     */
    public function apply($params)
    {
        $this->load($params);


        $this->query = new Query();
        $this->query->from([OrderNotificationRequest::tableName()]);
        $this->query->select([
            OrderNotificationRequest::tableName().'.*',
            'status_id' => Order::tableName().'.status_id',
            'order_created_at' => Order::tableName().'.created_at'
        ]);
        $this->query->leftJoin(Order::tableName(), Order::tableName() . '.id=' . OrderNotificationRequest::tableName() . '.order_id');
        $this->query->leftJoin(OrderNotification::tableName(), OrderNotification::tableName() . '.id=' . OrderNotificationRequest::tableName() . '.order_notification_id');
        $this->query->where([OrderNotification::tableName().'.answerable' => OrderNotification::IS_ANSWERABLE]);

        $this->buildSelect()->applyFilters($params);

        $dataProvider = new DataProvider([
            'form' => $this,
            'query' => $this->query
        ]);

        $dataProvider->pagination->pageSize = 25;

        return $dataProvider;

    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $output = '';

        return $output;
    }

    /**
     * @inheritdoc
     */
    protected function buildSelect()
    {
        return $this;
    }

    /**
     * @param array $params
     * @return ReportForm
     */
    protected function applyFilters($params)
    {
        $this->getDateSmsNotificationFilter()->apply($this->query, $params);
        $this->getAnswerTypeSmsNotificationFilter()->apply($this->query, $params);

        $this->applyBaseFilters($params);

        return $this;
    }
}
