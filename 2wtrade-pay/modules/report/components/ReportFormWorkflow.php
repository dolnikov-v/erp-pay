<?php

namespace app\modules\report\components;

use app\models\Country;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\delivery\models\Delivery;
use app\modules\order\models\Order;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\report\components\filters\DateFilter;
use app\modules\report\extensions\Query;
use yii\data\ArrayDataProvider;
use app\modules\report\extensions\DataProviderWorkflow;
use app\modules\report\widgets\ReportGroupByWorkflow;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class ReportFormWorkflow
 * @package app\modules\report\components
 */
class ReportFormWorkflow extends ReportForm
{
    const GROUP_BY_DELIVERY = 'd_id';
    const GROUP_BY_COUNTRY_DELIVERY = 'c_id_d_id';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->groupByCollection[self::GROUP_BY_DELIVERY] = Yii::t('common', 'Курьерка');
        $this->groupByCollection[self::GROUP_BY_COUNTRY_DELIVERY] = Yii::t('common', 'Страна \ Курьерка');
        $this->groupBy = ReportFormWorkflow::GROUP_BY_CREATED_AT;
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'groupBy' => Yii::t('common', 'Группировка'),
        ];
    }

    /**
     * @param array $params
     * @return DataProviderWorkflow
     */
    public function apply($params)
    {
        if (empty($params)) {
            $params['s']['country_ids'] = [Yii::$app->user->getCountry()->id];
            $params['s']['delivery_ids'] = array_keys(ArrayHelper::map(Delivery::getUserDeliveries(), 'id', 'name'));
        }
        $params['type'] = DateFilter::TYPE_CALL_CENTER_SENT_AT;

        $this->load($params);

        $this->query = new Query();
        $this->query->from([CallCenterRequest::tableName()]);
        $this->buildSelectToCallCenter()->applyFilters($params);

        $dataProviderToCallCenter = new DataProviderWorkflow([
            'form' => $this,
            'query' => $this->query
        ]);

        return $dataProviderToCallCenter;
    }

    /**
     * @param array $params
     * @return DataProviderWorkflow
     */
    public function applyToDelivery($params)
    {
        $params['type'] = DateFilter::TYPE_DELIVERY_SENT_AT;

        $this->load($params);

        $this->query = new Query();
        $this->query->from([DeliveryRequest::tableName()]);
        $this->buildSelectToDelivery()->applyFilters($params);

        $dataProviderToDelivery = new DataProviderWorkflow([
            'form' => $this,
            'query' => $this->query
        ]);

        return $dataProviderToDelivery;
    }

    /**
     * @param array $params
     * @return DataProviderWorkflow
     */
    public function applyToDeliveryCallCenterApproved($params)
    {
        $params['type'] = DateFilter::TYPE_DELIVERY_SENT_AT;

        $this->load($params);

        $this->query = new Query();
        $this->query->from([DeliveryRequest::tableName()]);
        $this->buildSelectDeliveryCallCenterApproved()->applyFilters($params);

        $dataProviderToDelivery = new DataProviderWorkflow([
            'form' => $this,
            'query' => $this->query
        ]);

        return $dataProviderToDelivery;
    }

    /**
     * @param array $params
     * @return DataProviderWorkflow
     */
    public function applyToDeliveryApproved($params)
    {
        $params['type'] = DateFilter::TYPE_DELIVERY_APPROVED_AT;

        $this->load($params);

        $this->query = new Query();
        $this->query->from([DeliveryRequest::tableName()]);
        $this->buildSelectToDeliveryApproved()->applyFilters($params);

        $dataProviderToDeliveryApproved = new DataProviderWorkflow([
            'form' => $this,
            'query' => $this->query
        ]);

        return $dataProviderToDeliveryApproved;
    }

    /**
     * @param array $params
     * @return DataProviderWorkflow
     */
    public function applyToDeliveryReturned($params)
    {
        $params['type'] = DateFilter::TYPE_DELIVERY_RETURNED_AT;

        $this->load($params);

        $this->query = new Query();
        $this->query->from([DeliveryRequest::tableName()]);
        $this->buildSelectToDeliveryReturned()->applyFilters($params);

        $dataProviderToDeliveryReturned = new DataProviderWorkflow([
            'form' => $this,
            'query' => $this->query
        ]);

        return $dataProviderToDeliveryReturned;
    }

    /**
     * @param array $params
     * @return DataProviderWorkflow
     */
    public function applyToDeliveryPaid($params)
    {
        $params['type'] = DateFilter::TYPE_DELIVERY_PAID_AT;

        $this->load($params);

        $this->query = new Query();
        $this->query->from([DeliveryRequest::tableName()]);
        $this->buildSelectToDeliveryPaid()->applyFilters($params);

        $dataProviderToDeliveryPaid = new DataProviderWorkflow([
            'form' => $this,
            'query' => $this->query
        ]);

        return $dataProviderToDeliveryPaid;
    }

    /**
     * @param integer $value
     * @return string
     */
    public static function monthName($value)
    {
        switch ($value) {
            case 1:
                return 'Январь';
            case 2:
                return 'Февраль';
            case 3:
                return 'Март';
            case 4:
                return 'Апрель';
            case 5:
                return 'Май';
            case 6:
                return 'Июнь';
            case 7:
                return 'Июль';
            case 8:
                return 'Август';
            case 9:
                return 'Сентябрь';
            case 10:
                return 'Октябрь';
            case 11:
                return 'Ноябрь';
            case 12:
                return 'Декабрь';
            default:
                return '';
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $content = '';
        return $content;
    }

    /**
     * @param array $params
     * @param string $type
     * @return bool
     */
    public function applyFilters($params)
    {
        $dateFilter = $this->getDateFilter();
        $dateFilter->type = $params['type'];
        $dateFilter->apply($this->query, $params);
        $this->getCountryDeliverySelectMultipleFilter()->apply($this->query, $params);
        $this->getSourceFilter()->apply($this->query, $params);
        return true;
    }

    /**
     * @return DateFilter
     */
    public function getDateFilter()
    {
        if (is_null($this->dateFilter)) {
            $this->dateFilter = new DateFilter();
        }

        return $this->dateFilter;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restoreGroupBy($form)
    {
        return ReportGroupByWorkflow::widget([
            'form' => $form,
            'reportForm' => $this,
        ]);
    }

    /**
     * @param ArrayDataProvider $dataProviderToCallCenter
     * @param ArrayDataProvider $dataProviderToDelivery
     * @param ArrayDataProvider $dataProviderToDeliveryCallCenterApproved
     * @param ArrayDataProvider $dataProviderToDeliveryApproved
     * @param ArrayDataProvider $dataProviderToDeliveryReturned
     * @param ArrayDataProvider $dataProviderToDeliveryPaid
     * @return array
     */
    public function getDataGraph($dataProviderToCallCenter,
                                 $dataProviderToDelivery,
                                 $dataProviderToDeliveryCallCenterApproved,
                                 $dataProviderToDeliveryApproved,
                                 $dataProviderToDeliveryReturned,
                                 $dataProviderToDeliveryPaid)
    {
        $data = null;
        foreach ($dataProviderToCallCenter->allModels as $row) {
            if (isset($data[$row['country_id']])) {
                try {
                    $data[$row['country_id']]['avgToCallCenter'] += $row['calcparamavg'] / $row['calcparamcount'];
                } catch (\Exception $e) {
                }
            } else {
                $data[$row['country_id']]['name'] = $row['country_name'];
                try {
                    $data[$row['country_id']]['avgToCallCenter'] = $row['calcparamavg'] / $row['calcparamcount'];
                } catch (\Exception $e) {
                    $data[$row['country_id']]['avgToCallCenter'] = 0;
                }
                $data[$row['country_id']]['avgToDelivery'] = 0;
                $data[$row['country_id']]['avgToDeliveryCallCenterApproved'] = 0;
                $data[$row['country_id']]['avgToDeliveryApproved'] = 0;
                $data[$row['country_id']]['avgToDeliveryReturned'] = 0;
                $data[$row['country_id']]['avgToDeliveryPaid'] = 0;
            }
        }

        foreach ($dataProviderToDelivery->allModels as $row) {
            if (isset($data[$row['country_id']])) {
                try {
                    $data[$row['country_id']]['avgToDelivery'] += $row['calcparamavg'] / $row['calcparamcount'];
                } catch (\Exception $e) {
                }
            } else {
                $data[$row['country_id']]['name'] = $row['country_name'];
                try {
                    $data[$row['country_id']]['avgToDelivery'] = $row['calcparamavg'] / $row['calcparamcount'];
                } catch (\Exception $e) {
                    $data[$row['country_id']]['avgToDelivery'] = 0;
                }
                $data[$row['country_id']]['avgToDeliveryCallCenterApproved'] = 0;
                $data[$row['country_id']]['avgToDeliveryApproved'] = 0;
                $data[$row['country_id']]['avgToDeliveryReturned'] = 0;
                $data[$row['country_id']]['avgToDeliveryPaid'] = 0;
            }
        }

        foreach ($dataProviderToDeliveryCallCenterApproved->allModels as $row) {
            if (isset($data[$row['country_id']])) {
                try {
                    $data[$row['country_id']]['avgToDeliveryCallCenterApproved'] += $row['calcparamavg'] / $row['calcparamcount'];
                } catch (\Exception $e) {
                }
            } else {
                $data[$row['country_id']]['name'] = $row['country_name'];
                try {
                    $data[$row['country_id']]['avgToDeliveryCallCenterApproved'] = $row['calcparamavg'] / $row['calcparamcount'];
                } catch (\Exception $e) {
                    $data[$row['country_id']]['avgToDeliveryCallCenterApproved'] = 0;
                }
                $data[$row['country_id']]['avgToDeliveryApproved'] = 0;
                $data[$row['country_id']]['avgToDeliveryReturned'] = 0;
                $data[$row['country_id']]['avgToDeliveryPaid'] = 0;
            }
        }

        foreach ($dataProviderToDeliveryApproved->allModels as $row) {
            if (isset($data[$row['country_id']])) {
                try {
                    $data[$row['country_id']]['avgToDeliveryApproved'] += $row['calcparamavg'] / $row['calcparamcount'];
                } catch (\Exception $e) {
                }
            } else {
                $data[$row['country_id']]['name'] = $row['country_name'];
                try {
                    $data[$row['country_id']]['avgToDeliveryApproved'] = $row['calcparamavg'] / $row['calcparamcount'];
                } catch (\Exception $e) {
                    $data[$row['country_id']]['avgToDeliveryApproved'] = 0;
                }
                $data[$row['country_id']]['avgToDeliveryReturned'] = 0;
                $data[$row['country_id']]['avgToDeliveryPaid'] = 0;
            }
        }

        foreach ($dataProviderToDeliveryReturned->allModels as $row) {
            if (isset($data[$row['country_id']])) {
                try {
                    $data[$row['country_id']]['avgToDeliveryReturned'] += $row['calcparamavg'] / $row['calcparamcount'];
                } catch (\Exception $e) {
                }
            } else {
                $data[$row['country_id']]['name'] = $row['country_name'];
                try {
                    $data[$row['country_id']]['avgToDeliveryReturned'] = $row['calcparamavg'] / $row['calcparamcount'];
                } catch (\Exception $e) {
                    $data[$row['country_id']]['avgToDeliveryReturned'] = 0;
                }
                $data[$row['country_id']]['avgToDeliveryPaid'] = 0;
            }
        }

        foreach ($dataProviderToDeliveryPaid->allModels as $row) {
            if (isset($data[$row['country_id']])) {
                try {
                    $data[$row['country_id']]['avgToDeliveryPaid'] += $row['calcparamavg'] / $row['calcparamcount'];
                } catch (\Exception $e) {
                }
            } else {
                $data[$row['country_id']]['name'] = $row['country_name'];
                try {
                    $data[$row['country_id']]['avgToDeliveryPaid'] = $row['calcparamavg'] / $row['calcparamcount'];
                } catch (\Exception $e) {
                    $data[$row['country_id']]['avgToDeliveryPaid'] = 0;
                }
            }
        }

        $countries = null;
        $series = [
            [
                'name' => Yii::t('common', 'Доставлен-Оплачен'),
                'data' => null
            ],
            [
                'name' => Yii::t('common', 'Доставлен-Возврат'),
                'data' => null
            ],
            [
                'name' => Yii::t('common', 'Отправлен в службу доставки-Доставлен'),
                'data' => null
            ],
            [
                'name' => Yii::t('common', 'Одобрен в колл-центре-Отправлен в службу доставки'),
                'data' => null
            ],
            [
                'name' => Yii::t('common', 'Отправлен в колл-центр-Отправлен в службу доставки'),
                'data' => null
            ],
            [
                'name' => Yii::t('common', 'Создан-Отправлен в колл-центр'),
                'data' => null
            ],
        ];
        if (count($data) > 0) {
            foreach ($data as $row) {
                $countries[] = $row['name'];
                $series[0]['data'][] = isset($row['avgToDeliveryPaid']) ? round($row['avgToDeliveryPaid'] / 3600, 2) : 0;
                $series[1]['data'][] = isset($row['avgToDeliveryReturned']) ? round($row['avgToDeliveryReturned'] / 3600, 2) : 0;
                $series[2]['data'][] = isset($row['avgToDeliveryApproved']) ? round($row['avgToDeliveryApproved'] / 3600, 2) : 0;
                $series[3]['data'][] = isset($row['avgToDeliveryCallCenterApproved']) ? round($row['avgToDeliveryCallCenterApproved'] / 3600, 2) : 0;
                $series[4]['data'][] = isset($row['avgToDelivery']) ? round($row['avgToDelivery'] / 3600, 2) : 0;
                $series[5]['data'][] = isset($row['avgToCallCenter']) ? round($row['avgToCallCenter'] / 3600, 2) : 0;
            }
        }
        return [
            'countries' => $countries,
            'series' => $series
        ];
    }

    /**
     * @param $hours integer
     * @param $minutes integer
     * @param $sec integer
     * @return string
     */
    public static function getTime($hours, $minutes, $sec)
    {
        if ($hours < 10) {
            $hours = '0' . $hours;
        }
        if ($minutes < 10) {
            $minutes = '0' . $minutes;
        }
        if ($sec < 10) {
            $sec = '0' . $sec;
        }
        return $hours . ':' . $minutes . ':' . $sec;
    }

    /**
     * @inheritdoc
     */
    protected function buildSelectToCallCenter()
    {
        $this->query->select([
            'country_id' => Country::tableName() . '.id',
            'country_name' => Country::tableName() . '.name',
            'calcparamcount' => 'COUNT(1)',
            'yearcreatedat' => 'Year(From_unixtime(' . CallCenterRequest::tableName() . '.sent_at))',
            'monthcreatedat' => 'Month(From_unixtime(' . CallCenterRequest::tableName() . '.sent_at))',
            'weekcreatedat' => 'Week(From_unixtime(' . CallCenterRequest::tableName() . '.sent_at))',
            'daycreatedat' => 'Day(From_unixtime(' . CallCenterRequest::tableName() . '.sent_at))',
        ]);
        $this->query->addAvgCondition(CallCenterRequest::tableName() . '.sent_at - ' . Order::tableName() . '.created_at', 'calcparamavg', true);
        $this->query->addMinCondition(CallCenterRequest::tableName() . '.sent_at - ' . Order::tableName() . '.created_at', 'calcparammin', true);
        $this->query->addMaxCondition(CallCenterRequest::tableName() . '.sent_at - ' . Order::tableName() . '.created_at', 'calcparammax', true);
        $this->query->innerJoin(Order::tableName(), CallCenterRequest::tableName() . '.order_id=' . Order::tableName() . '.id');
        $this->query->innerJoin(DeliveryRequest::tableName(), DeliveryRequest::tableName() . '.order_id=' . Order::tableName() . '.id');
        $this->query->innerJoin(Country::tableName(), Order::tableName() . '.country_id=' . Country::tableName() . '.id');
        $this->query->where(['is not', CallCenterRequest::tableName() . '.sent_at', null]);
        $this->query->andWhere(['is not', Order::tableName() . '.created_at', null]);

        return $this;
    }

    /**
     * @inheritdoc
     */
    protected function buildSelectToDelivery()
    {
        $this->query->select([
            'country_id' => Country::tableName() . '.id',
            'country_name' => Country::tableName() . '.name',
            'calcparamcount' => 'COUNT(1)',
            'yearcreatedat' => 'Year(From_unixtime(' . DeliveryRequest::tableName() . '.sent_at))',
            'monthcreatedat' => 'Month(From_unixtime(' . DeliveryRequest::tableName() . '.sent_at))',
            'weekcreatedat' => 'Week(From_unixtime(' . DeliveryRequest::tableName() . '.sent_at))',
            'daycreatedat' => 'Day(From_unixtime(' . DeliveryRequest::tableName() . '.sent_at))',
        ]);
        $this->query->addAvgCondition(DeliveryRequest::tableName() . '.sent_at - ' . CallCenterRequest::tableName() . '.sent_at', 'calcparamavg', true);
        $this->query->addMinCondition(DeliveryRequest::tableName() . '.sent_at - ' . CallCenterRequest::tableName() . '.sent_at', 'calcparammin', true);
        $this->query->addMaxCondition(DeliveryRequest::tableName() . '.sent_at - ' . CallCenterRequest::tableName() . '.sent_at', 'calcparammax', true);
        $this->query->innerJoin(CallCenterRequest::tableName(), DeliveryRequest::tableName() . '.order_id=' . CallCenterRequest::tableName() . '.order_id');
        $this->query->innerJoin(Order::tableName(), DeliveryRequest::tableName() . '.order_id=' . Order::tableName() . '.id');
        $this->query->innerJoin(Country::tableName(), Order::tableName() . '.country_id=' . Country::tableName() . '.id');
        $this->query->where(['is not', CallCenterRequest::tableName() . '.sent_at', null]);
        $this->query->andWhere(['is not', DeliveryRequest::tableName() . '.sent_at', null]);

        return $this;
    }

    /**
     * @inheritdoc
     */
    protected function buildSelectDeliveryCallCenterApproved()
    {
        $this->query->select([
            'country_id' => Country::tableName() . '.id',
            'country_name' => Country::tableName() . '.name',
            'calcparamcount' => 'COUNT(1)',
            'yearcreatedat' => 'Year(From_unixtime(' . DeliveryRequest::tableName() . '.sent_at))',
            'monthcreatedat' => 'Month(From_unixtime(' . DeliveryRequest::tableName() . '.sent_at))',
            'weekcreatedat' => 'Week(From_unixtime(' . DeliveryRequest::tableName() . '.sent_at))',
            'daycreatedat' => 'Day(From_unixtime(' . DeliveryRequest::tableName() . '.sent_at))',
        ]);
        $this->query->addAvgCondition(DeliveryRequest::tableName() . '.sent_at - ' . CallCenterRequest::tableName() . '.approved_at', 'calcparamavg', true);
        $this->query->addMinCondition(DeliveryRequest::tableName() . '.sent_at - ' . CallCenterRequest::tableName() . '.approved_at', 'calcparammin', true);
        $this->query->addMaxCondition(DeliveryRequest::tableName() . '.sent_at - ' . CallCenterRequest::tableName() . '.approved_at', 'calcparammax', true);
        $this->query->innerJoin(CallCenterRequest::tableName(), DeliveryRequest::tableName() . '.order_id=' . CallCenterRequest::tableName() . '.order_id');
        $this->query->innerJoin(Order::tableName(), DeliveryRequest::tableName() . '.order_id=' . Order::tableName() . '.id');
        $this->query->innerJoin(Country::tableName(), Order::tableName() . '.country_id=' . Country::tableName() . '.id');
        $this->query->where(['is not', CallCenterRequest::tableName() . '.approved_at', null]);
        $this->query->andWhere(['is not', DeliveryRequest::tableName() . '.sent_at', null]);

        return $this;
    }

    /**
     * @inheritdoc
     */
    protected function buildSelectToDeliveryApproved()
    {
        $this->query->select([
            'country_id' => Country::tableName() . '.id',
            'country_name' => Country::tableName() . '.name',
            'calcparamcount' => 'COUNT(1)',
            'yearcreatedat' => 'Year(From_unixtime(' . DeliveryRequest::tableName() . '.approved_at))',
            'monthcreatedat' => 'Month(From_unixtime(' . DeliveryRequest::tableName() . '.approved_at))',
            'weekcreatedat' => 'Week(From_unixtime(' . DeliveryRequest::tableName() . '.approved_at))',
            'daycreatedat' => 'Day(From_unixtime(' . DeliveryRequest::tableName() . '.approved_at))',
        ]);
        $this->query->addAvgCondition(DeliveryRequest::tableName() . '.approved_at - ' . DeliveryRequest::tableName() . '.sent_at', 'calcparamavg', true);
        $this->query->addMinCondition(DeliveryRequest::tableName() . '.approved_at - ' . DeliveryRequest::tableName() . '.sent_at', 'calcparammin', true);
        $this->query->addMaxCondition(DeliveryRequest::tableName() . '.approved_at - ' . DeliveryRequest::tableName() . '.sent_at', 'calcparammax', true);
        $this->query->innerJoin(CallCenterRequest::tableName(), DeliveryRequest::tableName() . '.order_id=' . CallCenterRequest::tableName() . '.order_id');
        $this->query->innerJoin(Order::tableName(), DeliveryRequest::tableName() . '.order_id=' . Order::tableName() . '.id');
        $this->query->innerJoin(Country::tableName(), Order::tableName() . '.country_id=' . Country::tableName() . '.id');
        $this->query->where(['is not', DeliveryRequest::tableName() . '.sent_at', null]);
        $this->query->andWhere(['is not', DeliveryRequest::tableName() . '.approved_at', null]);

        return $this;
    }

    /**
     * @inheritdoc
     */
    protected function buildSelectToDeliveryReturned()
    {
        $this->query->select([
            'country_id' => Country::tableName() . '.id',
            'country_name' => Country::tableName() . '.name',
            'calcparamcount' => 'COUNT(1)',
            'yearcreatedat' => 'Year(From_unixtime(' . DeliveryRequest::tableName() . '.returned_at))',
            'monthcreatedat' => 'Month(From_unixtime(' . DeliveryRequest::tableName() . '.returned_at))',
            'weekcreatedat' => 'Week(From_unixtime(' . DeliveryRequest::tableName() . '.returned_at))',
            'daycreatedat' => 'Day(From_unixtime(' . DeliveryRequest::tableName() . '.returned_at))',
        ]);
        $this->query->addAvgCondition(DeliveryRequest::tableName() . '.returned_at - ' . DeliveryRequest::tableName() . '.approved_at', 'calcparamavg', true);
        $this->query->addMinCondition(DeliveryRequest::tableName() . '.returned_at - ' . DeliveryRequest::tableName() . '.approved_at', 'calcparammin', true);
        $this->query->addMaxCondition(DeliveryRequest::tableName() . '.returned_at - ' . DeliveryRequest::tableName() . '.approved_at', 'calcparammax', true);
        $this->query->innerJoin(CallCenterRequest::tableName(), DeliveryRequest::tableName() . '.order_id=' . CallCenterRequest::tableName() . '.order_id');
        $this->query->innerJoin(Order::tableName(), DeliveryRequest::tableName() . '.order_id=' . Order::tableName() . '.id');
        $this->query->innerJoin(Country::tableName(), Order::tableName() . '.country_id=' . Country::tableName() . '.id');
        $this->query->where(['is not', DeliveryRequest::tableName() . '.returned_at', null]);
        $this->query->andWhere(['is not', DeliveryRequest::tableName() . '.approved_at', null]);

        return $this;
    }

    /**
     * @inheritdoc
     */
    protected function buildSelectToDeliveryPaid()
    {
        $this->query->select([
            'country_id' => Country::tableName() . '.id',
            'country_name' => Country::tableName() . '.name',
            'calcparamcount' => 'COUNT(1)',
            'yearcreatedat' => 'Year(From_unixtime(' . DeliveryRequest::tableName() . '.paid_at))',
            'monthcreatedat' => 'Month(From_unixtime(' . DeliveryRequest::tableName() . '.paid_at))',
            'weekcreatedat' => 'Week(From_unixtime(' . DeliveryRequest::tableName() . '.paid_at))',
            'daycreatedat' => 'Day(From_unixtime(' . DeliveryRequest::tableName() . '.paid_at))',
        ]);
        $this->query->addAvgCondition(DeliveryRequest::tableName() . '.paid_at - ' . DeliveryRequest::tableName() . '.approved_at', 'calcparamavg', true);
        $this->query->addMinCondition(DeliveryRequest::tableName() . '.paid_at - ' . DeliveryRequest::tableName() . '.approved_at', 'calcparammin', true);
        $this->query->addMaxCondition(DeliveryRequest::tableName() . '.paid_at - ' . DeliveryRequest::tableName() . '.approved_at', 'calcparammax', true);
        $this->query->innerJoin(CallCenterRequest::tableName(), DeliveryRequest::tableName() . '.order_id=' . CallCenterRequest::tableName() . '.order_id');
        $this->query->innerJoin(Order::tableName(), DeliveryRequest::tableName() . '.order_id=' . Order::tableName() . '.id');
        $this->query->innerJoin(Country::tableName(), Order::tableName() . '.country_id=' . Country::tableName() . '.id');
        $this->query->where(['is not', DeliveryRequest::tableName() . '.paid_at', null]);
        $this->query->andWhere(['is not', DeliveryRequest::tableName() . '.approved_at', null]);

        return $this;
    }
}
