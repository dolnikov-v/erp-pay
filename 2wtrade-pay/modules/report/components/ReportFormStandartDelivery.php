<?php
namespace app\modules\report\components;

use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use app\modules\report\components\filters\DailyFilter;
use app\modules\report\components\filters\TypeAddressGroupFilter;
use app\modules\report\extensions\Query;
use app\widgets\Panel;
use Yii;
use yii\data\ArrayDataProvider;


/**
 * Class ReportFormStandartDelivery
 * @package app\modules\report\components
 */
class ReportFormStandartDelivery extends ReportForm
{
    /**
     * @var bool
     */
    public $dollar = true;
    /**
     * @var string Группировка по умолчанию
     */
    public $groupBy = self::GROUP_BY_CUSTOMER_PROVINCE;

    /**
     * @var array Маппинг статусов
     */
    protected $mapStatuses = [];

    /**
     * @var $dailyFilter
     */
    protected $dailyFilter;

    /**
     * @var $dailyFilter
     */
    protected $typeAddressGroupFilter;

    /**
     * @var $commonTable
     */
    protected $commonTable;

    /**
     * @var $deliveryIds array
     */
    protected $deliveryIds;


    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->groupByCollection[self::GROUP_BY_CUSTOMER_PROVINCE] = Yii::t('common', 'Провинция');
        $this->panelAlert = Yii::t('common', "(Ср. эталонное время доставки, в часах) / Ср. время доставки, в часах / Количество доставок");
        $this->panelAlertStyle = Panel::ALERT_WARNING;
        $this->withPercent = 'off';
    }

    /**
     * @param array $params
     * @return ArrayDataProvider
     */
    public function apply($params)
    {
        $this->load($params);

        $condition = 'if(' . DeliveryRequest::tableName() . '.approved_at > 0 and ('
            . DeliveryRequest::tableName() . '.sent_at > 0 or ' . DeliveryRequest::tableName() . '.accepted_at > 0), '
            . '(' . DeliveryRequest::tableName() . '.approved_at - if(' . DeliveryRequest::tableName() . '.accepted_at > 0, '
            . DeliveryRequest::tableName() . '.accepted_at, ' . DeliveryRequest::tableName() . '.sent_at)) / 3600, null)';

        $this->query = new Query();
        $this->query->from([Order::tableName()]);
        $this->query->innerJoin(DeliveryRequest::tableName(), DeliveryRequest::tableName() .'.order_id=' .Order::tableName() .'.id');
        $this->query->select([
            'customer_province' => Order::tableName() .'.customer_province',
            'customer_city' => Order::tableName() .'.customer_city',
            'customer_zip' => Order::tableName() .'.customer_zip',
            'delivery_time' => $condition,
            'num' => 'count(1)',
            'delivery_id' => DeliveryRequest::tableName() .'.delivery_id',
        ]);

        $this->buildSelect()->applyFilters($params);
        $this->groupBy = $this->typeAddressGroupFilter->type_address_group;
        $this->query->andWhere(['IS NOT', $condition, null])
            ->andWhere(['>=', $condition, 0])
            ->groupBy([$this->groupBy, 'delivery_time', 'delivery_id'])
            ->orderBy($this->groupBy .', delivery_time, delivery_id');

        $this->commonTable = $this->query->all();

        $allModels = $this->buildResultModel();

        $dataProvider = new ArrayDataProvider([
            'allModels' => $allModels,
        ]);

        $dataProvider->pagination->pageSize = $dataProvider->pagination->totalCount;

        return $dataProvider;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $output = '';

        return $output;
    }

    /**
     * @return array
     */
    public function getMapStatuses()
    {
        if (empty($this->mapStatuses)) {
            $this->mapStatuses = [
                Yii::t('common', 'Выкуплено') => OrderStatus::getBuyoutList(),
            ];
        }

        return $this->mapStatuses;
    }

    /**
     * @inheritdoc
     */
    protected function buildSelect()
    {
        switch ($this->groupBy) {
            case self::GROUP_BY_PRODUCTS:
                $this->buildSelectMapProducts();
                break;
            default:
                $this->buildSelectMapStatuses();
                break;
        }

        return $this;
    }


    protected function buildSelectMapStatuses()
    {
        $this->query->andWhere(['in', Order::tableName() . '.status_id', $this->getMapStatusesAsArray()]);
    }

    /**
     * @param array $params
     * @return ReportForm
     */
    protected function applyFilters($params)
    {
        $this->getDailyFilter()->apply($this->query, $params);
        $this->getProductFilter()->apply($this->query, $params);
        $this->getTypeAddressGroupFilter()->apply($this->query, $params);
        $this->applyBaseFilters($params);

        return $this;
    }

    /**
     * @return DailyFilter
     */
    public function getDailyFilter()
    {
        if (is_null($this->dailyFilter)) {
            $this->dailyFilter = new DailyFilter();
        }

        $this->dailyFilter->type = DailyFilter::TYPE_DELIVERY_APPROVED_AT;

        return $this->dailyFilter;
    }

    /**
     * @return TypeAddressGroupFilter
     */
    public function getTypeAddressGroupFilter()
    {
        if (is_null($this->typeAddressGroupFilter)) {
            $this->typeAddressGroupFilter = new TypeAddressGroupFilter();
        }

        return $this->typeAddressGroupFilter;
    }



    private function buildResultModel() {

        // Получаем полный список курьерок и наименований geo
        $this->deliveryIds = [];
        $geoArray = [];
        foreach ($this->commonTable as $item) {
            if (!in_array($item['delivery_id'], $this->deliveryIds)) {
                $this->deliveryIds[] = $item['delivery_id'];
            }
            if (!in_array($item[$this->groupBy], $geoArray)) {
                $geoArray[] = $item[$this->groupBy];
            }
        }
        sort($this->deliveryIds);

        // Перебираем все полученные geo-объекты и строим итоговую модель
        $result = [];
        $deviders = [];
        foreach ($geoArray as $geo) {
            $tempArray = $this->getFilteredListByGeo($geo);
            $deviders = $this->findDeviders($tempArray);
            $avgFastAndSlow = $this->findFastAndSlowAvgValue($tempArray, $deviders);
            $afs = [];
            foreach ($this->deliveryIds as $d_item) {
                if (key_exists($d_item, $avgFastAndSlow)) {
                    $afs[$d_item] = ['fast' => $avgFastAndSlow[$d_item]['avg_fast'], 'slow' => $avgFastAndSlow[$d_item]['avg_slow'], 'count' => $avgFastAndSlow[$d_item]['count']];
                }
                else {
                    $afs[$d_item] = ['fast' => '-', 'slow' => '-', 'count' => '-'];
                }
            }

            $empty = true;
            foreach ($afs as $a) {
                if ($a['fast'] != '-' || $a['slow'] != '-') {
                    $empty = false;
                    break;
                }
            }
            if (!$empty) {
                $result[] = ['geo' => $geo, $afs];
            }
        }

        return $result;
    }

    /**
     * Получаем отфильтрованные по geo массив
     * @param $name
     * @return array
     */
    private function getFilteredListByGeo($name) {

        $result = [];
        foreach ($this->commonTable as $key => $row) {
            if ($row[$this->groupBy] == $name) {
                $result[$row['delivery_id']][] = $row;
                unset($this->commonTable[$key]);
            }
        }

        return $result;
    }

    /**
     * Находим делители 20% самых быстрых доставок для каждой из курьерок
     * @param $deliveries
     * @return array
     */
    private function findDeviders($deliveries) {

        $resArray = [];
        $d_array = [];

        foreach ($deliveries as $key => $dlvrs) {
            // Строим ряд по времени доставок этой курьерки
            foreach ($dlvrs as $d) {
                $repeatCount = intval($d['num']);
                if (!is_numeric($repeatCount)) {
                    break;
                }
                while ($repeatCount) {
                    $d_array[] = $d['delivery_time'];
                    $repeatCount--;
                }
            }
            // находим индекс максимального из 20% самых быстрых доставок курьерки
            $idx = abs(round((count($d_array) - 1) / 5));
            // находим собственно значение для сравнения быстрая доставка или нет
            $resArray[$key] = $d_array[$idx];
            unset($d_array);
        }

        return $resArray;
    }

    /**
     * Находим средние самых быстрых и медленных доставок для каждой из курьерок
     * @param $deliveries
     * @param $devider
     * @return array
     */
    private function findFastAndSlowAvgValue($deliveries, $devider) {

        $resArray = [];

        foreach ($deliveries as $dkey => $dlvrs) {
            $fast = null;
            $slow = null;
            $f_count = 0;
            $s_count = 0;
            foreach ($dlvrs as $d) {
                if ($d['delivery_time'] <= $devider[$d['delivery_id']]) {
                    $fast += $d['delivery_time'];
                    $f_count++;
                }
                else {
                    $slow += $d['delivery_time'];
                    $s_count++;
                }
            }

            $avg_fast = '-';
            $avg_slow = '-';

            if ($f_count > 0) {
                $avg_fast = intval($fast/$f_count);
            }

            if ($s_count > 0) {
                $avg_slow = intval($slow/$s_count);
            }
/*
            if(is_numeric($avg_fast)) {
                $avg_fast = $this->formatOutput($avg_fast);
            }

            if(is_numeric($avg_slow)) {
                $avg_slow = $this->formatOutput($avg_slow);
            }
*/
            $resArray[$dkey] = ['avg_fast' => $avg_fast, 'avg_slow' => $avg_slow, 'count' => intval($f_count + $s_count)];
        }

        return $resArray;
    }


    /**
     * Форматируем вывод времени
     * @param $value
     * @return string
     */
    public function formatOutput($value = 0) {

        $str = '';

        $diff = intval($value);
        if ($diff >= 0 && is_numeric($value)) {
            $diff = $value;
        }

        $days = floor($diff/24);
        $hours = $diff%24;

        if ($days == 0 && $hours == 0) {
            $str = '-';
        }
        else {
            $str = $days . Yii::t("common", " дн.") . ', ' . $hours . Yii::t("common", " ч.");
        }

        return $str;
    }


    /**
     * @param $arr
     * @return array
     */
    public function getDeliveriesNames($arr) {

        $result = [];
        if (empty($arr)) {
            return $result;
        }

        $arrayKeys = array_keys($arr);

        $result = Delivery::find()
            ->select(['id', 'name'])
            ->where(['IN', Delivery::tableName() .'.id', $arrayKeys])
            ->asArray()
            ->all();


        return $result;
    }

}
