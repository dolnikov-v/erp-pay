<?php

namespace app\modules\report\components;


use app\modules\report\components\filters\DebtsMonthFilter;
use app\modules\report\models\ReportDeliveryDebts;

/**
 * Class ReportFormDeliveryNewDebts
 * @package app\modules\report\components
 */
class ReportFormDeliveryNewDebts extends ReportFormDeliveryDebts
{
    // Ебаный костыль для долбоебов менеджеров и руководства, которые не могут пользоваться фильтром или которые ленивые жопы, отсекающий все данные до этого времени
    const REPORT_START_FROM = '2018-12-01';

    /*
     * С какого месяца расчет задодлженности для счетчика
     */
    const DATE_FROM_DEBTS = '2018-12-01';

    /**
     * @return DebtsMonthFilter
     */
    public function getMonthFilter()
    {
        if (is_null($this->monthFilter)) {
            $this->monthFilter = new DebtsMonthFilter([
                'from' => \Yii::$app->formatter->asMonth(strtotime(' - 1month', time())),
                'to' => \Yii::$app->formatter->asMonth(time()),
                'minDate' => static::REPORT_START_FROM
            ]);
        }
        return $this->monthFilter;
    }

    public function getTotalCounter()
    {
        $all = ReportDeliveryDebts::find()
            ->joinWith('delivery.country', false)
            ->where([
                'is_partner' => 0,
                'is_distributor' => 0,
            ])
            ->andWhere([
                'is not',
                ReportDeliveryDebts::tableName() . '.created_at',
                null
            ])
            ->andWhere(['>=', ReportDeliveryDebts::tableName() . '.month', static::REPORT_START_FROM])
            ->all();

        $totalCounterCurrent = 0;
        $totalCounter = 0;
        foreach ($all as $item) {
            /** @var ReportDeliveryDebts $item */
            $totalCounter += $item->decodedData['buyout_sum_total_dollars'] - $item->decodedData['charges_not_accepted_dollars'];
            if ($item->month >= static::DATE_FROM_DEBTS) {
                $totalCounterCurrent += $item->decodedData['buyout_sum_total_dollars'] - $item->decodedData['charges_not_accepted_dollars'];
            }
        }

        return [
            'current' => $totalCounterCurrent,
            'all' => $totalCounter,
        ];
    }
}