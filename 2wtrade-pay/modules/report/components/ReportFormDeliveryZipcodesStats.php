<?php
namespace app\modules\report\components;

use app\modules\delivery\models\DeliveryZipcodes;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use app\modules\report\components\filters\DateFilter;
use app\modules\report\extensions\DataProvider;
use app\modules\report\extensions\Query;
use app\widgets\Panel;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class ReportFormDeliveryZipcodesStats
 * @package app\modules\report\components
 */
class ReportFormDeliveryZipcodesStats extends ReportForm
{
    /**
     * @var bool
     */
    public $dollar = true;
    /**
     * @var string Группировка по умолчанию
     */
    public $groupBy = self::GROUP_BY_DELIVERY_ZIPCODES;

    /**
     * @var array Маппинг статусов
     */
    protected $mapStatuses = [];

    protected $allGroups = [];
    
    /**
     * @param array $params
     * @return DataProvider
     */
    public function apply($params)
    {

        $this->getDateFilter()->type = DateFilter::TYPE_DELIVERY_SENT_AT;

        $this->load($params);

        $this->query = new Query();
        $this->query->from([Order::tableName()]);

        $this->buildSelect()
            ->applyFilters($params);
        $this->query->orderBy(['delivery_zipcodes_name' => SORT_ASC]);

        if (!$this->deliveryFilter->delivery) {
            $this->panelAlert = Yii::t('common', 'Выберите службу доставки');
            $this->panelAlertStyle = Panel::ALERT_DANGER;
        }

        $dataProvider = new DataProvider([
            'form' => $this,
            'query' => $this->query,
        ]);
        $dataProvider->pagination->pageSize = $dataProvider->pagination->totalCount;

        return $dataProvider;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $output = '';

        return $output;
    }

    /**
     * @return array
     */
    public function getMapStatuses()
    {
        if (empty($this->mapStatuses)) {
            $this->mapStatuses = [
                Yii::t('common', 'Выкуплено') => OrderStatus::getBuyoutList(),
                Yii::t('common', 'В процессе') => OrderStatus::getProcessDeliveryAndLogisticList(),
                Yii::t('common', 'Не выкуплено') => OrderStatus::getNotBuyoutList(),
            ];
        }
        return $this->mapStatuses;
    }

    /**
     * @inheritdoc
     */
    protected function buildSelect()
    {
        $this->query->select([
            'delivery_zipcodes_id' => DeliveryZipcodes::tableName() . '.id',
            'delivery_zipcodes_name' => DeliveryZipcodes::tableName() . '.name'
        ]);
        $this->buildSelectMapStatuses();
        return $this;
    }

    /**
     * @param array $params
     * @return ReportForm
     */
    protected function applyFilters($params)
    {
        $this->getDateFilter()->apply($this->query, $params);
        $this->getDeliveryFilter()->apply($this->query, $params);
        $this->applyBaseFilters($params);
        return $this;
    }

    public function getAllGroups() {
        if (!empty($this->deliveryFilter->delivery) && empty($this->allGroups)) {
            $groups = DeliveryZipcodes::find()->select('id')->byDeliveryId($this->deliveryFilter->delivery)->all();
            $this->allGroups = ArrayHelper::getColumn($groups, 'id');
        }
        return $this->allGroups;
    }
}
