<?php

namespace app\modules\report\components;

use app\models\Country;
use app\models\Product;
use app\modules\catalog\models\UnBuyoutReason;
use app\modules\catalog\models\UnBuyoutReasonMapping;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\delivery\models\DeliveryRequestUnBuyoutReason;
use app\modules\order\models\Order;
use app\modules\order\models\OrderProduct;
use app\modules\order\models\OrderStatus;
use app\modules\report\components\filters\DailyFilterDefaultLastMonth;
use app\modules\report\components\filters\ProductSelectFilter;
use app\widgets\Panel;
use yii\data\ArrayDataProvider;
use Yii;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * Class ReportFormUnshippingReasons
 * @package app\modules\report\components
 */
class ReportFormUnshippingReasons extends ReportForm
{
    /**
     * @var $dailyFilter
     */
    public $dailyFilter;

    /**
     * @var $productSelectFilter
     */
    public $productSelectFilter;

    /**
     * @var $reasonsList
     */
    public $reasonsList;

    /**
     * @var array Маппинг статусов
     */
    protected $mapStatuses = [];

    /**
     * @return array
     */
    public function getMapStatuses()
    {
        if (empty($this->mapStatuses)) {
            $this->mapStatuses = [
                'not_buyout' => OrderStatus::getNotBuyoutDeliveryList(),
            ];
        }
        return $this->mapStatuses;
    }

    /**
     * @param array $params
     * @return ArrayDataProvider
     */
    public function apply($params)
    {
        if (empty($params)) {
            $params['s']['country_ids'] = [Yii::$app->user->getCountry()->id];
        }

        $this->reasonsList = DeliveryRequest::getUnshippingReasons();

        $this->getMapStatuses();

        $this->query = Order::find()
            ->select([
                'cnt' => 'COUNT(DISTINCT(' . Order::tableName() . '.id))',
                'delivery_id' => DeliveryRequest::tableName() . '.delivery_id',
                'delivery' => Delivery::tableName() . '.name',
                'product_id' => OrderProduct::tableName() . '.product_id',
                'product' => Product::tableName() . '.name',
                'reason_id' => UnBuyoutReasonMapping::tableName() . '.reason_id',
                'reason' => UnBuyoutReason::tableName() . '.name',
                'category' => UnBuyoutReason::tableName() . '.category',
                'slug' => Country::tableName() . '.slug'
            ])
            ->leftJoin(Country::tableName(), Country::tableName() . '.id=' . Order::tableName() . '.country_id')
            ->leftJoin(DeliveryRequest::tableName(), DeliveryRequest::tableName() . '.order_id=' . Order::tableName() . '.id')
            ->leftJoin(Delivery::tableName(), Delivery::tableName() . '.id=' . DeliveryRequest::tableName() . '.delivery_id')
            ->innerJoin(DeliveryRequestUnBuyoutReason::tableName(), DeliveryRequestUnBuyoutReason::tableName() . '.delivery_request_id=' . DeliveryRequest::tableName() . '.id')
            ->leftJoin(UnBuyoutReasonMapping::tableName(), UnBuyoutReasonMapping::tableName() . '.id=' . DeliveryRequestUnBuyoutReason::tableName() . '.reason_mapping_id')
            ->leftJoin(UnBuyoutReason::tableName(), UnBuyoutReason::tableName() . '.id=' . UnBuyoutReasonMapping::tableName() . '.reason_id')
            ->leftJoin(OrderProduct::tableName(), OrderProduct::tableName() . '.order_id=' . Order::tableName() . '.id and ' . OrderProduct::tableName() . '.price > 0')
            ->leftJoin(Product::tableName(), Product::tableName() . '.id=' . OrderProduct::tableName() . '.product_id')
            ->andWhere(['is not', UnBuyoutReasonMapping::tableName() . '.reason_id', null])
            ->andWhere(['is not', OrderProduct::tableName() . '.product_id', null])
            ->andWhere([Order::tableName() . '.status_id' => $this->mapStatuses['not_buyout']])
            ->orderBy([
                UnBuyoutReason::tableName() . '.category' => SORT_ASC,
                UnBuyoutReason::tableName() . '.name' => SORT_ASC,
                Product::tableName() . '.name' => SORT_ASC,
            ]);

        $this->applyFilters($params);

        if (isset($params['s']['product_ids'])) {
            $this->query->andWhere([OrderProduct::tableName() . '.product_id' => $params['s']['product_ids']]);
        }

        // Исключаем запрещенные источники заказов
        $this->query->andWhere(['NOT IN', Order::tableName() . '.source_id', Yii::$app->user->rejectedSources]);

        $this->query->groupBy([
            DeliveryRequest::tableName() . '.delivery_id',
            OrderProduct::tableName() . '.product_id',
            UnBuyoutReasonMapping::tableName() . '.reason_id',
        ]);

        $rowData = $this->query->asArray()->all();

        if (!$rowData) {
            $this->panelAlert = Yii::t('common', 'Ничего не найдено');
            $this->panelAlertStyle = Panel::ALERT_DANGER;
        }

        $allModels = [
            'deliveries' => [],
            'data' => []
        ];

        if ($rowData) {

            $deliveries = ArrayHelper::map($rowData, 'delivery_id', 'delivery');

            $this->query = Order::find()
                ->select([
                    'cnt' => 'COUNT(DISTINCT(' . Order::tableName() . '.id))',
                    'delivery_id' => DeliveryRequest::tableName() . '.delivery_id',
                ])
                ->leftJoin(DeliveryRequest::tableName(), DeliveryRequest::tableName() . '.order_id=' . Order::tableName() . '.id')
                ->andWhere([DeliveryRequest::tableName() . '.delivery_id' => array_keys($deliveries)])
                ->andWhere([Order::tableName() . '.status_id' => $this->mapStatuses['not_buyout']]);

            $this->applyFilters($params);

            if (isset($params['s']['product_ids'])) {
                $this->query->andWhere([OrderProduct::tableName() . '.product_id' => $params['s']['product_ids']]);
            }

            // Исключаем запрещенные источники заказов
            $this->query->andWhere(['NOT IN', Order::tableName() . '.source_id', Yii::$app->user->rejectedSources]);

            $this->query->groupBy([
                DeliveryRequest::tableName() . '.delivery_id',
            ]);

            $rowDeliveryData = $this->query->asArray()->all();

            $allModels = [
                'deliveries' => $deliveries,
                'deliveryTotals' => ArrayHelper::map($rowDeliveryData, 'delivery_id', 'cnt'),
                'data' => ArrayHelper::index($rowData, 'product_id', ['delivery_id', 'reason_id'])
            ];
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $allModels,
        ]);

        $dataProvider->pagination->pageSize = $dataProvider->pagination->totalCount;

        return $dataProvider;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return '';
    }

    /**
     * @param array $params
     * @return $this
     */
    protected function applyFilters($params)
    {
        $this->getDailyFilter()->apply($this->query, $params);
        $this->getCountryDeliveryPartnerSelectMultipleFilter()->apply($this->query, $params);
        $this->getProductSelectFilter()->apply($this->query, $params);
        $this->getSourceFilter()->apply($this->query, $params);
        return $this;
    }


    /**
     * @return DailyFilterDefaultLastMonth
     */
    public function getDailyFilter()
    {
        if (is_null($this->dailyFilter)) {
            $this->dailyFilter = new DailyFilterDefaultLastMonth();
            $this->dailyFilter->type = DailyFilterDefaultLastMonth::TYPE_DELIVERY_SENT_AT_NOT_EMPTY;
        }

        return $this->dailyFilter;
    }

    /**
     * @return ProductSelectFilter
     */
    public function getProductSelectFilter()
    {
        if (is_null($this->productSelectFilter)) {
            $this->productSelectFilter = new ProductSelectFilter();
        }
        return $this->productSelectFilter;
    }
}
