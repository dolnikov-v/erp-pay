<?php

namespace app\modules\report\components;

use app\models\Country;
use app\modules\order\models\Order;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\delivery\models\Delivery;
use app\modules\order\models\OrderStatus;
use app\modules\report\components\filters\CountrySelectFilter;
use app\modules\report\components\filters\ProductSelectFilter;
use app\modules\report\components\filters\DeliverySelectFilter;
use app\modules\report\components\filters\CuratorUserFilter;
use app\modules\report\components\filters\DailyFilterWithTimeOffset;
use app\modules\report\components\filters\TypeDatePartFilter;
use app\modules\report\extensions\Query;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\order\models\OrderProduct;
use app\models\Product;
use Yii;
use yii\data\ArrayDataProvider;

/**
 * Class ReportFormBuyoutByCountry
 * @package app\modules\report\components
 */
class ReportFormBuyoutByCountry extends ReportForm
{
    /**
     * @var DailyFilterWithTimeOffset
     */
    protected $dailyFilter;

    /**
     * @var CountrySelectFilter
     */
    protected $countrySelectFilter;

    /**
     * @var ProductSelectFilter
     */
    protected $productSelectFilter;

    /**
     * @var DeliverySelectFilter
     */
    protected $deliverySelectFilter;

    /**
     * @var curatorUserFilter
     */
    protected $curatorUserFilter;

    /**
     * @var TypeDatePartFilter
     */
    protected $typeDatePartFilter;

    /**
     * @var array Маппинг статусов
     */
    protected $mapStatuses = [];

    /**
     * @var []
     */
    protected $params;

    /**
     * @param array $params
     * @return ArrayDataProvider
     */
    public function apply($params)
    {
        $this->load($params);

        $subOquery = Order::find()
            ->select([
                Order::tableName() . '.created_at',
                Order::tableName() . '.status_id',
                Order::tableName() . '.id',
                Order::tableName() . '.country_id',
                OrderProduct::tableName() . '.order_id',
                'product_id' => OrderProduct::tableName() . '.product_id',
                'delivery_id' => DeliveryRequest::tableName() . '.delivery_id',
            ])
            ->leftJoin(OrderProduct::tableName(), OrderProduct::tableName() . '.order_id=' . Order::tableName() . '.id')
            ->leftJoin(CallCenterRequest::tableName(), CallCenterRequest::tableName() . '.order_id =' . OrderProduct::tableName() . '.order_id')
            ->leftJoin(DeliveryRequest::tableName(), DeliveryRequest::tableName() . '.order_id=' . Order::tableName() . '.id')
            ->where([">=", CallCenterRequest::tableName() . '.sent_at', strtotime($this->searchQuery['s']['from'])])
            ->andWhere(["<=", CallCenterRequest::tableName() . '.sent_at', strtotime($this->searchQuery['s']['to']) + 86399])
            ->groupBy([OrderProduct::tableName() . '.order_id']);

        if (isset($params['s']['delivery_ids']) && count($params['s']['delivery_ids']) > 0) {
            $subOquery->andFilterWhere(['in', DeliveryRequest::tableName() . '.delivery_id', $params['s']['delivery_ids']]);
        }

        if (isset($params['s']['product_ids']) && count($params['s']['product_ids']) > 0) {
            $subOquery->andFilterWhere(['in', OrderProduct::tableName() . '.product_id', $params['s']['product_ids']]);
        }

        $this->query = new Query();
        $this->query->from(['order' => $subOquery]);

        $this->params = $params;
        $this->buildSelect()->applyFilters($params);

        if (isset($this->params['s']['product_ids']) && count($this->params['s']['product_ids']) > 0) {
            $this->query->leftJoin(Product::tableName(), Product::tableName() . '.id=' . 'order.product_id');
            $this->query->andFilterWhere(['in', 'order.product_id', $params['s']['product_ids']]);
            $this->query->addGroupBy('order.product_id');
        }

        if (isset($this->params['s']['delivery_ids']) && count($this->params['s']['delivery_ids']) > 0) {
            $this->query->leftJoin(DeliveryRequest::tableName(), DeliveryRequest::tableName() . '.order_id=' . Order::tableName() . '.id');
            $this->query->leftJoin(Delivery::tableName(), Delivery::tableName() . '.id=' . DeliveryRequest::tableName() . '.delivery_id');
            $this->query->andFilterWhere(['in', 'order.delivery_id', $params['s']['delivery_ids']]);
            $this->query->addGroupBy('order.delivery_id');
        }

        $this->query->addGroupBy(['country_name', 'groupbyname']);

        $dataArray = $this->query->all();

        $dataProvider = new ArrayDataProvider([
            'allModels' => $dataArray,
            'pagination' => false,
        ]);

        return $dataProvider;
    }

    /**
     * @inheritdoc
     */
    protected function buildSelect()
    {
        $this->query->select([
            'country_name' => Country::tableName() . '.name',
            'country_id' => Country::tableName() . '.id',
            'yearcreatedat' => 'Year(From_unixtime(' . Order::tableName() . '.created_at))',
            'monthcreatedat' => 'Month(From_unixtime(' . Order::tableName() . '.created_at))',
            'weekcreatedat' => 'Week(From_unixtime(' . Order::tableName() . '.created_at), 1)',
            'daycreatedat' => 'Day(From_unixtime(' . Order::tableName() . '.created_at))',
            'groupbyname' => "DATE_FORMAT(FROM_UNIXTIME(" . Order::tableName() . ".created_at), \"%d.%m.%Y\")",
        ]);

        if (isset($this->params['s']['product_ids']) && count($this->params['s']['product_ids']) > 0) {
            $this->query->addSelect([
                'product_name' => Product::tableName() . '.name',
            ]);
        }

        if (isset($this->params['s']['delivery_ids']) && count($this->params['s']['delivery_ids']) > 0) {
            $this->query->addSelect([
                'delivery_name' => Delivery::tableName() . '.name',
                'delivery_id' => Delivery::tableName() . '.id',
            ]);
        }

        $this->query->leftJoin(Country::tableName(),
            Order::tableName() . '.country_id=' . Country::tableName() . '.id');

        $this->buildSelectMapStatuses();

        return $this;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $content = '';
        return $content;
    }

    /**
     * @return array
     */
    public function getMapStatuses()
    {
        if (empty($this->mapStatuses)) {
            $this->mapStatuses = [
                Yii::t('common', 'Выкуплено') => OrderStatus::getBuyoutList(),
                Yii::t('common', 'Подтверждено') => [                           // список взят из delivery_report, чтобы совпадали данные в%
                    OrderStatus::STATUS_DELIVERY_ACCEPTED,
                    OrderStatus::STATUS_DELIVERY_LEFT_WAREHOUSE,
                    OrderStatus::STATUS_DELIVERY,
                    OrderStatus::STATUS_DELIVERY_LEFT_OUR_WAREHOUSE,
                    OrderStatus::STATUS_DELIVERY_REDELIVERY,
                    OrderStatus::STATUS_DELIVERY_LEFT_THEIR_WAREHOUSE,
                    OrderStatus::STATUS_DELIVERY_DELAYED,
                    OrderStatus::STATUS_LOG_ACCEPTED,
                    OrderStatus::STATUS_LOG_GENERATED,
                    OrderStatus::STATUS_LOG_SET,
                    OrderStatus::STATUS_LOG_PASTED,
                    OrderStatus::STATUS_DELIVERY_DENIAL,
                    OrderStatus::STATUS_DELIVERY_RETURNED,
                    OrderStatus::STATUS_DELIVERY_REJECTED,
                    OrderStatus::STATUS_LOGISTICS_REJECTED,
                    OrderStatus::STATUS_DELIVERY_REFUND,
                    OrderStatus::STATUS_LOGISTICS_ACCEPTED,
                    OrderStatus::STATUS_DELIVERY_LOST,
                    OrderStatus::STATUS_DELIVERY_BUYOUT,
                    OrderStatus::STATUS_FINANCE_MONEY_RECEIVED,
                ],
            ];
        }

        return $this->mapStatuses;
    }

    /**
     * @param array $params
     * @return bool
     */
    public function applyFilters($params)
    {
        $this->getDailyFilter()->apply($this->query, $params);
        $this->getCountrySelectFilter()->apply($this->query, $params);
        $this->getProductSelectFilter()->apply($this->query, $params);
        $this->getCuratorUserFilter()->apply($this->query, $params);
        $this->getTypeDatePartFilter()->apply($this->query, $params);
        $this->getDeliverySelectFilter()->apply($this->query, $params);

        return true;
    }

    /**
     * @return DailyFilterWithTimeOffset
     */
    public function getDailyFilter()
    {
        if (is_null($this->dailyFilter)) {
            $this->dailyFilter = new DailyFilterWithTimeOffset();
            $this->dailyFilter->type = DailyFilterWithTimeOffset::TYPE_ORDER_CREATED_AT;
        }

        return $this->dailyFilter;
    }

    /**
     * @return CountrySelectFilter
     */
    public function getCountrySelectFilter()
    {
        if (is_null($this->countrySelectFilter)) {
            $this->countrySelectFilter = new CountrySelectFilter();
        }
        return $this->countrySelectFilter;
    }

    /**
     * @return ProductSelectFilter
     */
    public function getProductSelectFilter()
    {
        if (is_null($this->productSelectFilter)) {
            $this->productSelectFilter = new ProductSelectFilter();
        }
        return $this->productSelectFilter;
    }

    /**
     * @return DeliverySelectFilter
     */
    public function getDeliverySelectFilter()
    {
        if (is_null($this->deliverySelectFilter)) {
            $this->deliverySelectFilter = new DeliverySelectFilter();
        }
        return $this->deliverySelectFilter;
    }

    /**
     * @return CuratorUserFilter
     */
    public function getCuratorUserFilter()
    {
        if (is_null($this->curatorUserFilter)) {
            $this->curatorUserFilter = new CuratorUserFilter();
        }
        return $this->curatorUserFilter;
    }

    /**
     * @return TypeDatePartFilter
     */
    public function getTypeDatePartFilter()
    {
        if (is_null($this->typeDatePartFilter)) {
            $this->typeDatePartFilter = new TypeDatePartFilter();
        }
        return $this->typeDatePartFilter;
    }

}
