<?php

namespace app\modules\report\components;

use app\modules\callcenter\models\CallCenter;
use app\modules\callcenter\models\CallCenterFullRequest;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use app\modules\report\models\ReportCallCenter;
use Yii;
use yii\base\InvalidParamException;
use yii\helpers\ArrayHelper;
use yii\httpclient\Client;
use yii\web\BadRequestHttpException;

/**
 * Class ReportCallCenterComparator
 * @package app\modules\report\components
 */
class ReportCallCenterComparator
{
    /** @var array */
    public static $mapStatuses = [
        OrderStatus::STATUS_CC_POST_CALL => CallCenterFullRequest::STATUS_POST_CALL,
        OrderStatus::STATUS_CC_APPROVED => CallCenterFullRequest::STATUS_APPROVED,
        OrderStatus::STATUS_CC_FAIL_CALL => CallCenterFullRequest::STATUS_FAIL_CALL,
        OrderStatus::STATUS_CC_RECALL => CallCenterFullRequest::STATUS_RECALL,
        OrderStatus::STATUS_CC_REJECTED => CallCenterFullRequest::STATUS_REJECTED,
        OrderStatus::STATUS_CC_DOUBLE => CallCenterFullRequest::STATUS_DOUBLE,
        OrderStatus::STATUS_CC_TRASH => CallCenterFullRequest::STATUS_TRASH,
    ];

    /**
     * @param ReportCallCenter $model
     * @return array
     * @throws BadRequestHttpException
     */
    public static function compareWithCallCenters($model)
    {
        $batchSize = 300;
        $model->difference = '';
        $model->cc_trash_count = $model->our_trash_count = 0;
        $model->cc_rejected_count = $model->our_rejected_count = 0;
        $model->cc_recall_count = $model->our_recall_count = 0;
        $model->cc_post_call_count = $model->our_post_call_count = 0;
        $model->cc_approved_count = $model->our_approved_count = 0;
        $model->cc_double_count = $model->our_double_count = 0;
        $model->cc_fail_call_count = $model->our_fail_call_count = 0;
        $model->difference_count = 0;


        $errors = [
            ReportCallCenter::SHOW_ERROR_CC_APPROVED => 0,
            ReportCallCenter::SHOW_ERROR_CC_POST_CALL => 0,
            ReportCallCenter::SHOW_ERROR_CC_FAIL_CALL => 0,
            ReportCallCenter::SHOW_ERROR_CC_RECALL => 0,
            ReportCallCenter::SHOW_ERROR_CC_REJECTED => 0,
            ReportCallCenter::SHOW_ERROR_CC_DOUBLE => 0,
            ReportCallCenter::SHOW_ERROR_CC_TRASH => 0,
            ReportCallCenter::SHOW_ERROR_OUR_POST_CALL => 0,
            ReportCallCenter::SHOW_ERROR_OUR_FAIL_CALL => 0,
            ReportCallCenter::SHOW_ERROR_OUR_RECALL => 0,
            ReportCallCenter::SHOW_ERROR_OUR_REJECTED => 0,
            ReportCallCenter::SHOW_ERROR_OUR_DOUBLE => 0,
            ReportCallCenter::SHOW_ERROR_OUR_TRASH => 0,
            ReportCallCenter::SHOW_ERROR_OUR_APPROVED => 0,
            ReportCallCenter::SHOW_ERROR_NOT_FOUND_PRODUCTS => 0,
            ReportCallCenter::SHOW_ERROR_NOT_EQUAL_PRODUCT_QUANTITY => 0,
            ReportCallCenter::SHOW_ERROR_NOT_EQUAL_PRODUCT_PRICE => 0,
        ];

        $callCenters = CallCenter::find()->active()->byCountryId($model->country_id)->all();

        $query = Order::find()->joinWith('callCenterRequest')->where([
            '>=',
            Order::tableName() . '.created_at',
            $model->from
        ]);
        $query->andWhere(['<=', Order::tableName() . '.created_at', $model->to]);
        $query->byCountryId($model->country_id);
        $query->andWhere([
            'or',
            ['is', Order::tableName() . '.source_form', null],
            [Order::tableName() . '.source_form' => Order::TYPE_FORM_SHORT]
        ]);
        $query->andWhere(['is', CallCenterRequest::tableName() . '.id', null]);
        $query->andWhere(['!=', Order::tableName() . '.status_id', OrderStatus::STATUS_SOURCE_SHORT_FORM]);
        $query->with('orderProducts');

        $orderWithoutRequests = $query->all();
        $orderWithoutRequests = ArrayHelper::index($orderWithoutRequests, 'id');

        foreach ($callCenters as $callCenter) {

            $query = Order::find()->innerJoinWith('callCenterRequest')->where([
                '>=',
                Order::tableName() . '.created_at',
                $model->from
            ]);
            $query->andWhere(['<=', Order::tableName() . '.created_at', $model->to]);
            $query->byCountryId($model->country_id);
            $query->andWhere([
                'or',
                ['is', Order::tableName() . '.source_form', null],
                [Order::tableName() . '.source_form' => Order::TYPE_FORM_SHORT]
            ]);
            $query->andWhere([
                CallCenterRequest::tableName() . '.call_center_id' => $callCenter->id
            ]);
            $query->andWhere(['!=', Order::tableName() . '.status_id', OrderStatus::STATUS_SOURCE_SHORT_FORM]);

            foreach ($query->batch($batchSize) as $records) {
                /**
                 * @var Order[] $records
                 */
                $recordIds = [];
                foreach ($records as $record) {
                    if(!isset($recordIds[$record->call_center_type])) {
                        $recordIds[$record->call_center_type] = [];
                    }
                    $recordIds[$record->call_center_type][] = $record->id;
                }
                $requests = [];
                foreach ($recordIds as $type => $ids) {
                    try {
                        $requests = ArrayHelper::merge($requests, self::query($callCenter, $ids, $type));
                    } catch (InvalidParamException $e) {
                    }
                }

                self::compare($model, $requests, $records, $errors);
            }

            if (!empty($orderWithoutRequests)) {
                $recordIds = [];
                foreach ($orderWithoutRequests as $order) {
                    if(!isset($recordIds[$order->call_center_type])) {
                        $recordIds[$order->call_center_type] = [];
                    }
                    $recordIds[$order->call_center_type][] = $order->id;
                }
                foreach ($recordIds as $type => $ids) {
                    $totalCount = count($ids);
                    $offset = 0;
                    while ($offset < $totalCount) {
                        $batch = array_slice($ids, $offset, $batchSize);
                        $offset += $batchSize;
                        try {
                            $requests = self::query($callCenter, $batch, $type);

                        } catch (InvalidParamException $e) {
                            continue;
                        }

                        $records = [];
                        foreach ($requests as $request) {
                            $records[] = $orderWithoutRequests[$request->order_id];
                            unset($orderWithoutRequests[$request->order_id]);
                        }

                        self::compare($model, $requests, $records, $errors);
                    }
                }
            }
        }

        if (!empty($orderWithoutRequests)) {
            self::compare($model, [], $orderWithoutRequests, $errors);
        }

        $model->difference = json_encode($errors);

        return $errors;
    }

    /**
     * @param ReportCallCenter $model
     * @param CallCenterFullRequest[] $requests
     * @param Order[] $records
     * @param array $errors
     * @return mixed
     * @throws BadRequestHttpException | \Exception
     */
    protected static function compare($model, $requests, $records, &$errors)
    {
        if (!empty($requests)) {
            $existedRequests = CallCenterFullRequest::find()->where([
                'order_id' => ArrayHelper::getColumn($requests, 'order_id')
            ])->all();

            $existedRequests = ArrayHelper::index($existedRequests, 'foreign_id');
        }
        $transaction = Yii::$app->db->beginTransaction();
        try {
            foreach ($records as $record) {
                $orderStatus = CallCenterFullRequest::STATUS_APPROVED;
                if (isset(self::$mapStatuses[$record->status_id])) {
                    $orderStatus = self::$mapStatuses[$record->status_id];
                }
                switch ($orderStatus) {
                    case CallCenterFullRequest::STATUS_POST_CALL:
                        $model->our_post_call_count++;
                        $ourErrorType = ReportCallCenter::SHOW_ERROR_OUR_POST_CALL;
                        break;
                    case CallCenterFullRequest::STATUS_FAIL_CALL:
                        $model->our_fail_call_count++;
                        $ourErrorType = ReportCallCenter::SHOW_ERROR_OUR_FAIL_CALL;
                        break;
                    case CallCenterFullRequest::STATUS_RECALL:
                        $model->our_recall_count++;
                        $ourErrorType = ReportCallCenter::SHOW_ERROR_OUR_RECALL;
                        break;
                    case CallCenterFullRequest::STATUS_REJECTED:
                        $model->our_rejected_count++;
                        $ourErrorType = ReportCallCenter::SHOW_ERROR_OUR_REJECTED;
                        break;
                    case CallCenterFullRequest::STATUS_DOUBLE:
                        $model->our_double_count++;
                        $ourErrorType = ReportCallCenter::SHOW_ERROR_OUR_DOUBLE;
                        break;
                    case CallCenterFullRequest::STATUS_TRASH:
                        $model->our_double_count++;
                        $ourErrorType = ReportCallCenter::SHOW_ERROR_OUR_TRASH;
                        break;
                    default:
                        $model->our_approved_count++;
                        $ourErrorType = ReportCallCenter::SHOW_ERROR_OUR_APPROVED;
                }

                if (isset($requests[$record->id])) {
                    switch ($requests[$record->id]->status) {
                        case CallCenterFullRequest::STATUS_POST_CALL:
                            $model->cc_post_call_count++;
                            $ccErrorType = ReportCallCenter::SHOW_ERROR_CC_POST_CALL;
                            break;
                        case CallCenterFullRequest::STATUS_APPROVED:
                            $model->cc_approved_count++;
                            $ccErrorType = ReportCallCenter::SHOW_ERROR_CC_APPROVED;
                            break;
                        case CallCenterFullRequest::STATUS_FAIL_CALL:
                            $model->cc_fail_call_count++;
                            $ccErrorType = ReportCallCenter::SHOW_ERROR_CC_FAIL_CALL;
                            break;
                        case CallCenterFullRequest::STATUS_RECALL:
                            $model->cc_recall_count++;
                            $ccErrorType = ReportCallCenter::SHOW_ERROR_CC_RECALL;
                            break;
                        case CallCenterFullRequest::STATUS_REJECTED:
                            $model->cc_rejected_count++;
                            $ccErrorType = ReportCallCenter::SHOW_ERROR_CC_REJECTED;
                            break;
                        case CallCenterFullRequest::STATUS_DOUBLE:
                            $model->cc_double_count++;
                            $ccErrorType = ReportCallCenter::SHOW_ERROR_CC_DOUBLE;
                            break;
                        case CallCenterFullRequest::STATUS_TRASH:
                            $model->cc_double_count++;
                            $ccErrorType = ReportCallCenter::SHOW_ERROR_CC_TRASH;
                            break;
                        default:
                            throw new BadRequestHttpException(Yii::t('common',
                                'Колл-центр вернул неизвестный статус.'));
                    }


                    if (isset($existedRequests[$requests[$record->id]->foreign_id])) {
                        $request = $existedRequests[$requests[$record->id]->foreign_id];
                        foreach ($requests[$record->id]->attributes as $key => $value) {
                            if (!empty($value)) {
                                $request[$key] = $value;
                            }
                        }

                        if (!$request->save()) {
                            $errors = $request->errors;
                        }
                    } else {
                        $requests[$record->id]->save();
                    }

                    if (
                        ($requests[$record->id]->status != CallCenterFullRequest::STATUS_APPROVED
                            && $requests[$record->id]->status != $orderStatus)
                        || ($requests[$record->id]->status == CallCenterFullRequest::STATUS_APPROVED
                            && $ourErrorType != ReportCallCenter::SHOW_ERROR_OUR_APPROVED)
                    ) {
                        $errors[$ourErrorType]++;
                        $errors[$ccErrorType]++;
                        $model->difference_count++;
                    }

                    $orderProducts = $record->orderProducts;
                    $buffer = [];
                    foreach ($orderProducts as $prod) {
                        if (!isset($buffer[$prod->product_id])) {
                            $buffer[$prod->product_id] = [
                                'quantity' => $prod->quantity,
                                'price' => $prod->price
                            ];
                        } else {
                            $buffer[$prod->product_id]['quantity'] += $prod->quantity;
                            $buffer[$prod->product_id]['price'] += $prod->price;
                        }
                    }
                    $orderProducts = $buffer;

                    $recordProducts = $requests[$record->id]->orderProducts;
                    $buffer = [];

                    $ccPrice = 0;
                    foreach ($recordProducts as $prod) {
                        if (!isset($buffer[$prod->product_id])) {
                            $buffer[$prod->product_id] = [
                                'quantity' => $prod->quantity,
                                'price' => $prod->price
                            ];
                        } else {
                            $buffer[$prod->product_id]['quantity'] += $prod->quantity;
                            $buffer[$prod->product_id]['price'] += $prod->price;
                        }
                        $ccPrice += $prod->quantity * $prod->price;
                    }
                    $recordProducts = $buffer;

                    $foundFlag = false;
                    $quantityFlag = false;
                    foreach ($recordProducts as $id => $prod) {
                        if (!isset($orderProducts[$id])) {
                            if (!$foundFlag) {
                                $foundFlag = true;
                            }
                            continue;
                        }
                        if (!$quantityFlag && $prod['quantity'] != $orderProducts[$id]['quantity']) {
                            $quantityFlag = true;
                        }
                        unset($orderProducts[$id]);
                    }

                    if ($foundFlag || count($orderProducts) > 0) {
                        $errors[ReportCallCenter::SHOW_ERROR_NOT_FOUND_PRODUCTS]++;
                        $model->difference_count++;
                    } else {
                        if ($quantityFlag) {
                            $errors[ReportCallCenter::SHOW_ERROR_NOT_EQUAL_PRODUCT_QUANTITY]++;
                            $model->difference_count++;
                        }

                        if ($ccPrice != $record->price_total) {
                            $errors[ReportCallCenter::SHOW_ERROR_NOT_EQUAL_PRODUCT_PRICE]++;
                            $model->difference_count++;
                        }
                    }

                } else {
                    $errors[$ourErrorType]++;
                }
            }
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }

        return $errors;
    }

    /**
     * @param ReportCallCenter $model
     * @param string $type
     * @return array
     */
    public static function getIdsWithError($model, $type)
    {
        $errors = self::getModelErrors($model);

        $buffer = array_filter($errors, function ($item) use ($type) {
            return $item['type'] == $type;
        });

        return $buffer;
    }

    /**
     * @param ReportCallCenter $model
     * @throws BadRequestHttpException
     * @return array
     */
    public static function getModelErrors($model)
    {
        $query = Order::find()->where(['>=', Order::tableName() . '.created_at', $model->from]);
        $query->andWhere(['<=', Order::tableName() . '.created_at', $model->to]);
        $query->andWhere([
            'or',
            ['is', Order::tableName() . '.source_form', null],
            [Order::tableName() . '.source_form' => Order::TYPE_FORM_SHORT]
        ]);
        $query->byCountryId($model->country_id);
        $query->andWhere(['!=', Order::tableName() . '.status_id', OrderStatus::STATUS_SOURCE_SHORT_FORM]);

        $query->joinWith(['callCenterFullRequest', 'callCenterRequest']);

        $errors = [];

        foreach ($query->batch(1000) as $records) {
            foreach ($records as $record) {
                $orderStatus = CallCenterFullRequest::STATUS_APPROVED;
                if (isset(self::$mapStatuses[$record->status_id])) {
                    $orderStatus = self::$mapStatuses[$record->status_id];
                }

                switch ($orderStatus) {
                    case CallCenterFullRequest::STATUS_POST_CALL:
                        $ourErrorType = ReportCallCenter::SHOW_ERROR_OUR_POST_CALL;
                        break;
                    case CallCenterFullRequest::STATUS_FAIL_CALL:
                        $ourErrorType = ReportCallCenter::SHOW_ERROR_OUR_FAIL_CALL;
                        break;
                    case CallCenterFullRequest::STATUS_RECALL:
                        $ourErrorType = ReportCallCenter::SHOW_ERROR_OUR_RECALL;
                        break;
                    case CallCenterFullRequest::STATUS_REJECTED:
                        $ourErrorType = ReportCallCenter::SHOW_ERROR_OUR_REJECTED;
                        break;
                    case CallCenterFullRequest::STATUS_DOUBLE:
                        $ourErrorType = ReportCallCenter::SHOW_ERROR_OUR_DOUBLE;
                        break;
                    case CallCenterFullRequest::STATUS_TRASH:
                        $ourErrorType = ReportCallCenter::SHOW_ERROR_OUR_TRASH;
                        break;
                    default:
                        $ourErrorType = ReportCallCenter::SHOW_ERROR_OUR_APPROVED;
                }


                if (!empty($record->callCenterFullRequest)) {
                    switch ($record->callCenterFullRequest->status) {
                        case CallCenterFullRequest::STATUS_POST_CALL:
                            $ccErrorType = ReportCallCenter::SHOW_ERROR_CC_POST_CALL;
                            break;
                        case CallCenterFullRequest::STATUS_APPROVED:
                            $ccErrorType = ReportCallCenter::SHOW_ERROR_CC_APPROVED;
                            break;
                        case CallCenterFullRequest::STATUS_FAIL_CALL:
                            $ccErrorType = ReportCallCenter::SHOW_ERROR_CC_FAIL_CALL;
                            break;
                        case CallCenterFullRequest::STATUS_RECALL:
                            $ccErrorType = ReportCallCenter::SHOW_ERROR_CC_RECALL;
                            break;
                        case CallCenterFullRequest::STATUS_REJECTED:
                            $ccErrorType = ReportCallCenter::SHOW_ERROR_CC_REJECTED;
                            break;
                        case CallCenterFullRequest::STATUS_DOUBLE:
                            $ccErrorType = ReportCallCenter::SHOW_ERROR_CC_DOUBLE;
                            break;
                        case CallCenterFullRequest::STATUS_TRASH:
                            $ccErrorType = ReportCallCenter::SHOW_ERROR_CC_TRASH;
                            break;
                        default:
                            throw new BadRequestHttpException(Yii::t('common',
                                'Колл-центр вернул неизвестный статус.'));
                    }

                    $orderProducts = $record->orderProducts;
                    $buffer = [];
                    foreach ($orderProducts as $prod) {
                        if (!isset($buffer[$prod->product_id])) {
                            $buffer[$prod->product_id] = [
                                'quantity' => $prod->quantity,
                                'price' => $prod->price
                            ];
                        } else {
                            $buffer[$prod->product_id]['quantity'] += $prod->quantity;
                            $buffer[$prod->product_id]['price'] += $prod->price;
                        }
                    }
                    $orderProducts = $buffer;

                    $recordProducts = $record->callCenterFullRequest->orderProducts;
                    $buffer = [];
                    foreach ($recordProducts as $prod) {
                        if (!isset($buffer[$prod->product_id])) {
                            $buffer[$prod->product_id] = [
                                'quantity' => $prod->quantity,
                                'price' => $prod->price
                            ];
                        } else {
                            $buffer[$prod->product_id]['quantity'] += $prod->quantity;
                            $buffer[$prod->product_id]['price'] += $prod->price;
                        }
                    }
                    $recordProducts = $buffer;

                    $foundFlag = false;
                    $quantityFlag = false;
                    foreach ($recordProducts as $id => $prod) {
                        if (!isset($orderProducts[$id])) {
                            if (!$foundFlag) {
                                $foundFlag = true;
                            }
                            continue;
                        }
                        if (!$quantityFlag && $prod['quantity'] != $orderProducts[$id]['quantity']) {
                            $quantityFlag = true;
                        }
                        unset($orderProducts[$id]);
                    }

                    if ($foundFlag || count($orderProducts) > 0) {
                        $errors[] = [
                            'order_id' => $record->id,
                            'foreign_id' => $record->callCenterFullRequest->foreign_id,
                            'type' => ReportCallCenter::SHOW_ERROR_NOT_FOUND_PRODUCTS
                        ];
                    } else {

                        if ($quantityFlag) {
                            $errors[] = [
                                'order_id' => $record->id,
                                'foreign_id' => $record->callCenterFullRequest->foreign_id,
                                'type' => ReportCallCenter::SHOW_ERROR_NOT_EQUAL_PRODUCT_QUANTITY
                            ];
                        }

                        if ($record->price_total != $record->callCenterFullRequest->price) {
                            $errors[] = [
                                'order_id' => $record->id,
                                'foreign_id' => $record->callCenterFullRequest->foreign_id,
                                'type' => ReportCallCenter::SHOW_ERROR_NOT_EQUAL_PRODUCT_PRICE
                            ];
                        }
                    }

                    if (
                        ($record->callCenterFullRequest->status != CallCenterFullRequest::STATUS_APPROVED
                            && $record->callCenterFullRequest->status != $orderStatus)
                        || ($record->callCenterFullRequest->status == CallCenterFullRequest::STATUS_APPROVED
                            && $ourErrorType != ReportCallCenter::SHOW_ERROR_OUR_APPROVED)
                    ) {
                        $errors[] = [
                            'order_id' => $record->id,
                            'foreign_id' => $record->callCenterFullRequest->foreign_id,
                            'type' => $ourErrorType
                        ];
                        $errors[] = [
                            'order_id' => $record->id,
                            'foreign_id' => $record->callCenterFullRequest->foreign_id,
                            'type' => $ccErrorType
                        ];
                    }
                } else {
                    $errors[] = [
                        'order_id' => $record->id,
                        'foreign_id' => '',
                        'type' => $ourErrorType
                    ];
                }
            }
        }

        return $errors;
    }


    /**
     * @param $callCenter
     * @param $orderIds
     * @param string $type
     * @return array
     * @throws \yii\base\InvalidParamException
     * @throws \yii\web\BadRequestHttpException
     */
    public static function query($callCenter, $orderIds, $type = '')
    {
        $ids = implode(',', $orderIds);

        $time = time();
        $token = md5($callCenter->api_key . $time . $callCenter->api_pid);

        $params = [
            'token' => $token,
            'time' => $time,
            'pid' => $callCenter->api_pid,
            'type' => $type,
            'order_ids' => $ids
        ];

        $url = rtrim($callCenter->url, '/') . "/api/method/GetOrdersByIds";

        $client = new Client();
        $response = $client->createRequest()->setMethod('get')->setUrl($url)->setData($params)->send();

        if (!$response->isOk) {
            throw new BadRequestHttpException(Yii::t('common', 'Колл-центр в данный момент недоступен.'));
        }

        $data = $response->data;
        if (isset($data['success']) && $data['success'] == false) {
            $msg = Yii::t('common', 'Колл-центр ответил с ошибкой.');
            if (isset($data['error']['msg'])) {
                $msg .= " {$data['error']['msg']}";
            }
            throw new InvalidParamException($msg);
        }

        $requests = [];

        foreach ($data['response'] as $orderId => $item) {
            $model = new CallCenterFullRequest();
            $model->call_center_id = $callCenter->id;
            $model->order_id = $orderId;
            $model->foreign_id = $item['id'];
            $model->status = $item['status'];
            if (isset($item['sub_status'])) {
                $model->sub_status = $item['sub_status'];
            }
            $model->name = $item['name'];
            $model->phone = $item['phone'];
            $model->address = $item['address'];
            $model->address_components = json_encode($item['address_components']);
            $model->products = json_encode($item['products']);
            $model->shipping = json_encode($item['shipping']);
            $model->history = json_encode($item['history']);
            $model->delivery = $item['delivery'];
            $model->comment = $item['comment'];
            $model->last_update = Yii::$app->formatter->asTimestamp($item['last_update']);
            $model->response_message = $response->content;
            $requests[$orderId] = $model;
        }

        return $requests;
    }
}
