<?php
namespace app\modules\report\components;

use app\models\Country;
use app\models\Currency;
use app\models\CurrencyRate;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use app\modules\report\components\filters\DateFilter;
use app\modules\report\extensions\DataProvider;
use app\modules\report\extensions\Query;
use Yii;

/**
 * Class ReportFormInProgress
 * @package app\modules\report\components
 */
class ReportFormInProgress extends ReportForm
{
    /**
     * @var string Группировка по умолчанию
     */
    public $groupBy = self::GROUP_BY_DELIVERY_SENT_AT_NOT_EMPTY;

    /**
     * @var array Маппинг статусов
     */
    protected $mapStatuses = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->groupByCollection[self::GROUP_BY_COUNTRY_DATE] = Yii::t('common', 'Страна') . ' \\ ' . Yii::t('common', 'Дата');
    }

    /**
     * @param array $params
     * @return DataProvider
     */
    public function apply($params)
    {
        $this->getDateFilter()->type = DateFilter::TYPE_DELIVERY_SENT_AT;
        $this->load($params);

        $this->query = new Query();
        $this->query->from([Order::tableName()]);

        $this->applyFilters($params);

        $this->prepareCurrencyRate();

        $this->buildSelect();


        //нужно рисовать несколько кривых
        $params = yii::$app->request->get('s');
        if(isset($params['country_ids']) && count($params['country_ids']) > 1){
            $this->query->addGroupBy([Country::tableName().'.country_id']);
        }

        $dataProvider = new DataProvider([
            'form' => $this,
            'query' => $this->query,
        ]);

        $dataProvider->pagination->pageSize = $dataProvider->pagination->totalCount;

        return $dataProvider;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $output = '';

        return $output;
    }

    /**
     * @return array
     */
    public function getMapStatuses()
    {
        if (empty($this->mapStatuses)) {
            $this->mapStatuses = [
                Yii::t('common', 'Выкуплено') => OrderStatus::getBuyoutList(),
                Yii::t('common', 'В процессе') => OrderStatus::getProcessDeliveryList(),
                Yii::t('common', 'Не выкуплено') => OrderStatus::getNotBuyoutList(),
                Yii::t('common', 'Деньги получены') => [
                    OrderStatus::STATUS_FINANCE_MONEY_RECEIVED
                ],
            ];

            $this->mapStatuses[Yii::t('common', 'Всего заказов')] = $this->getMapStatusesAsArray();
        }

        return $this->mapStatuses;
    }

    /**
     * @inheritdoc
     */
    protected function buildSelect()
    {
        $mapStatuses = $this->getMapStatuses();

        foreach ($mapStatuses as $name => $statuses) {
            $this->query->addInConditionCount(Order::tableName() . '.status_id', $statuses, $name);
        }

        $statuses = $this->getMapStatusesAsArray();

        $toDollar = '';
        $fromDollar = '';
        $this->query->leftJoin(Country::tableName(), Order::tableName() . '.country_id=' . Country::tableName() . '.id');
        $this->query->leftJoin(CurrencyRate::tableName(), Country::tableName() . '.currency_id = ' . CurrencyRate::tableName() . '.currency_id');
        if ($this->dollar || $this->dollar) {
            $toDollar = '/ ' . CurrencyRate::tableName() . '.rate';
        } else {
            $fromDollar = '* ' . CurrencyRate::tableName() . '.rate';
        }

        $convertPriceTotalToCurrencyId = Country::tableName() . '.currency_id';
        if ($this->dollar) {
            $usdCurrency = Currency::getUSD();
            $convertPriceTotalToCurrencyId = $usdCurrency->id;
        }

        $caseApprove = $this->query->buildCaseCondition(
            'convertToCurrencyByCountry(' . Order::tableName() . '.price_total + ' . Order::tableName() . '.delivery, ' . Order::tableName() . '.price_currency,' . $convertPriceTotalToCurrencyId . ')',
            Order::tableName() . '.status_id',
            OrderStatus::getApproveList()
        );

        $caseBuyout = $this->query->buildCaseCondition(
            'convertToCurrencyByCountry(' . Order::tableName() . '.price_total + ' . Order::tableName() . '.delivery, ' . Order::tableName() . '.price_currency,' . $convertPriceTotalToCurrencyId . ')',
            Order::tableName() . '.status_id',
            OrderStatus::getBuyoutList()
        );

        $this->query->addAvgCondition($caseApprove, Yii::t('common', 'Средний чек апрувленный'));
        $this->query->addAvgCondition($caseBuyout, Yii::t('common', 'Средний чек выкупной'));

        $statuses = $mapStatuses[Yii::t('common', 'Выкуплено')];

        $case = $this->query->buildCaseCondition(
            '(' . Order::tableName() . '.price_total ' . $toDollar . ' + ' . Order::tableName() . '.delivery ' . $toDollar . ' )',
            Order::tableName() . '.status_id',
            $statuses
        );

        $this->query->addSumCondition($case, Yii::t('common', 'Доход по выкупам'));
        $this->query->addSumCondition(Order::tableName() . '.income ' . $fromDollar, Yii::t('common', 'Цена за лиды'));
        $this->query->andWhere(['in', Order::tableName() . '.status_id', $this->getMapStatusesAsArray()]);

        if ($this->groupBy == self::GROUP_BY_COUNTRY_DATE) {
            $selectDate = $this->getDateFilter()->getFilteredAttribute();
            $this->query->addSelect([
                'country_id' => Country::tableName() . '.id',
                'country_name' => Country::tableName() . '.name',
                'date' => 'DATE_FORMAT(FROM_UNIXTIME(' . $selectDate . '), "%Y-%m-%d")',
            ]);

            $this->groupBy = $this->getDateFilter()->type;
        }

        $this->query->addSelect([
            'country_name' => Country::tableName() . '.name',
            'yearcreatedat' => "DATE_FORMAT(FROM_UNIXTIME(" . $this->getGroupField() . "), \"%Y\")",
            'monthcreatedat' => "DATE_FORMAT(FROM_UNIXTIME(" . $this->getGroupField() . "), \"%m\")",
            'weekcreatedat' => "DATE_FORMAT(FROM_UNIXTIME(" . $this->getGroupField() . "), \"%u\")",
            'daycreatedat' => "DATE_FORMAT(FROM_UNIXTIME(" . $this->getGroupField() . "), \"%d\")",
        ]);

        return $this;
    }

    /**
     * @return string
     */
    public function getGroupField()
    {
        switch ($this->groupBy) {
            case self::GROUP_BY_CREATED_AT:
                $field = Order::tableName() . '.created_at';
                break;
            case self::GROUP_BY_UPDATED_AT:
                $field = Order::tableName() . '.updated_at';
                break;
            case self::GROUP_BY_CALL_CENTER_SENT_AT:
                $field = CallCenterRequest::tableName() . '.sent_at';
                break;
            case self::GROUP_BY_CALL_CENTER_APPROVED_AT:
                $field = CallCenterRequest::tableName() . '.approved_at';
                break;
            case self::GROUP_BY_DELIVERY_CREATED_AT:
                $field = DeliveryRequest::tableName() . '.created_at';
                break;
            case self::GROUP_BY_DELIVERY_SENT_AT:
                $field = DeliveryRequest::tableName() . '.sent_at';
                break;
            case self::GROUP_BY_DELIVERY_RETURNED_AT:
                $field = DeliveryRequest::tableName() . '.returned_at';
                break;
            case self::GROUP_BY_DELIVERY_APPROVED_AT:
                $field = DeliveryRequest::tableName() . '.approved_at';
                break;
            case self::GROUP_BY_DELIVERY_ACCEPTED_AT:
                $field = DeliveryRequest::tableName() . '.accepted_at';
                break;
            case self::GROUP_BY_DELIVERY_PAID_AT:
                $field = DeliveryRequest::tableName() . '.paid_at';
                break;
            default :
                $field = Order::tableName() . '.created_at';
                break;
        }

        return $field;
    }

    /**
     * @param array $params
     * @return ReportForm
     */
    protected function applyFilters($params)
    {

        $this->getDateFilter()->apply($this->query, $params);
        $this->getCountrySelectFilter()->apply($this->query, $params);

        $this->applyBaseFilters($params);

        return $this;
    }

    /**
     * @param array $params
     * @return ReportForm
     */
    protected function applyBaseFilters($params)
    {
        $this->getCalculatePercentFilter()->apply($this->query, $params);
        $this->getPaymentAdvertisingFilter()->apply($this->query, $params);
        $this->getCalculateDollarFilter()->apply($this->query, $params);

        return $this;
    }
}
