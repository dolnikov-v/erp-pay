<?php
namespace app\modules\report\components;

use app\modules\order\models\Order;
use app\modules\order\models\OrderProduct;
use app\modules\report\extensions\DataProvider;
use app\modules\report\extensions\Query;
use Yii;

/**
 * Class ReportFormProduct
 * @package app\modules\report\components
 */
class ReportFormProduct extends ReportForm
{
    /**
     * @var string Группировка по умолчанию
     */
    public $groupBy = self::GROUP_BY_CREATED_AT;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->groupByCollection[self::GROUP_BY_PRODUCTS] = Yii::t('common', 'Товар');
    }

    /**
     * @param array $params
     * @return DataProvider
     */
    public function apply($params)
    {
        $this->load($params);

        $this->query = new Query();
        $this->query->from([Order::tableName()]);

        //if ($this->groupBy != self::GROUP_BY_PRODUCTS) {
        $this->query->leftJoin(OrderProduct::tableName(), OrderProduct::tableName() . '.order_id = ' . Order::tableName() . '.id');
        //}

        $this->buildSelect()
            ->applyFilters($params);

        $dataProvider = new DataProvider([
            'form' => $this,
            'query' => $this->query,
        ]);

        $dataProvider->pagination->pageSize = $dataProvider->pagination->totalCount;

        return $dataProvider;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $output = '';

        return $output;
    }

    /**
     * @inheritdoc
     */
    protected function buildSelect()
    {
        switch ($this->groupBy) {
            case self::GROUP_BY_PRODUCTS:
                $this->query->addCondition('COUNT(DISTINCT ' . Order::tableName() . '.id)', Yii::t('common', 'Всего заказов'));
                $this->query->addCondition('SUM(' . OrderProduct::tableName() . '.quantity)', Yii::t('common', 'Всего товаров'));
                break;
            default:
                $this->query->addCondition('COUNT(DISTINCT ' . Order::tableName() . '.id)', Yii::t('common', 'Всего заказов'));
                $this->query->addCondition('SUM(' . OrderProduct::tableName() . '.quantity)', Yii::t('common', 'Всего товаров'));
                break;
        }

        return $this;
    }

    /**
     * @param array $params
     * @return ReportForm
     */
    protected function applyFilters($params)
    {
        $this->getDateFilter()->apply($this->query, $params);
        $this->getProductFilter()->apply($this->query, $params);
        $this->getCallCenterFilter()->apply($this->query, $params);
        $this->getDeliveryFilter()->apply($this->query, $params);
        $this->getOrderStatusFilter()->apply($this->query, $params);
        $this->getDeliveryFilter()->apply($this->query, $params);

        $this->applyBaseFilters($params);

        return $this;
    }
}
