<?php

namespace app\modules\report\components;

use PHPExcel_Writer_Excel2007;
use Yii;
use yii\db\ActiveQuery;

/**
 * Class DeliveryDebtsExcelXlsxBuilder
 * @package app\modules\report\components
 */
class DeliveryDebtsExcelXlsxBuilder
{
    const CELL_TYPE_COUNTRY = 'country';
    const CELL_TYPE_DELIVERY = 'delivery';
    const CELL_TYPE_MONTH = 'start_month_time';
    const CELL_SUM = 'sum_total';
    const CELL_SUM_DOLLARS = 'sum_total_dollars';
    const CELL_CHARGES = 'charges';
    const CELL_CHARGES_DOLLARS = 'charges_dollars';
    const CELL_END_SUM = 'end_sum';
    const CELL_END_SUM_DOLLARS = 'end_sum_dollars';

    protected static $styleCountryHeader = [
        'font' => [
            'bold' => true,
            'name' => 'Calibri',
            'size' => 16,
            'color' => [
                'rgb' => 'ffffff',
            ],
        ],
        'alignment' => [
            'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
        ],
        'fill' => [
            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
            'color' => [
                'rgb' => '4169e1',
            ],
        ],
        'borders' => [
            'bottom' => [
                'style' => \PHPExcel_Style_Border::BORDER_MEDIUM,
            ],
            'left' => [
                'style' => \PHPExcel_Style_Border::BORDER_MEDIUM,
            ],
            'right' => [
                'style' => \PHPExcel_Style_Border::BORDER_MEDIUM,
            ],
        ],
    ];

    protected static $styleDeliveryCell = [
        'font' => [
            'bold' => true,
            'name' => 'Calibri',
            'size' => 14,
            'color' => [
                'rgb' => '000000',
            ],
        ],
        'alignment' => [
            'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
        ],
        'borders' => [
            'top' => [
                'style' => \PHPExcel_Style_Border::BORDER_MEDIUM,
            ],
            'bottom' => [
                'style' => \PHPExcel_Style_Border::BORDER_MEDIUM,
            ],
            'left' => [
                'style' => \PHPExcel_Style_Border::BORDER_MEDIUM,
            ],
            'right' => [
                'style' => \PHPExcel_Style_Border::BORDER_MEDIUM,
            ],
        ],
    ];

    protected static $styleSumCell = [
        'font' => [
            'bold' => false,
            'name' => 'Calibri',
            'size' => 12,
            'color' => [
                'rgb' => '000000',
            ],
        ],
        'alignment' => [
            'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
            'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
        ],
        'borders' => [
            'top' => [
                'style' => \PHPExcel_Style_Border::BORDER_THIN,
            ],
            'bottom' => [
                'style' => \PHPExcel_Style_Border::BORDER_THIN,
            ],
            'left' => [
                'style' => \PHPExcel_Style_Border::BORDER_THIN,
            ],
            'right' => [
                'style' => \PHPExcel_Style_Border::BORDER_THIN,
            ],
        ],
    ];

    protected static $styleMonthCell = [
        'font' => [
            'bold' => false,
            'name' => 'Calibri',
            'size' => 12,
            'color' => [
                'rgb' => '000000',
            ],
        ],
        'alignment' => [
            'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
        ],
        'borders' => [
            'top' => [
                'style' => \PHPExcel_Style_Border::BORDER_THIN,
            ],
            'bottom' => [
                'style' => \PHPExcel_Style_Border::BORDER_THIN,
            ],
            'left' => [
                'style' => \PHPExcel_Style_Border::BORDER_THIN,
            ],
            'right' => [
                'style' => \PHPExcel_Style_Border::BORDER_THIN,
            ],
        ],
    ];

    /**
     * @var array
     */
    protected static $columns = [
        self::CELL_TYPE_DELIVERY => 25,
        self::CELL_TYPE_MONTH => 15,
        self::CELL_SUM => 30,
        self::CELL_CHARGES => 30,
        self::CELL_END_SUM => 30,
        self::CELL_SUM_DOLLARS => 30,
        self::CELL_CHARGES_DOLLARS => 30,
        self::CELL_END_SUM_DOLLARS => 30,
    ];

    /**
     * @var array
     */
    protected $records;

    /**
     * DeliveryDebtsExcelXlsxBuilder constructor.
     * @param array $records
     */
    public function __construct($records)
    {
        $this->records = $records;
    }

    /**
     * @inheritdoc
     */
    public function sendFile()
    {
        $excel = $this->generate();
        $filename = "Export_DeliveryDebts_" . date("Y-m-d_H-i-s") . '.xlsx';

        header('Content-Type:xlsx:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition:attachment;filename="' . $filename . '"');

        $objWriter = new PHPExcel_Writer_Excel2007($excel);
        $objWriter->save('php://output');

        Yii::$app->end();
    }

    /**
     * @return \PHPExcel
     */
    public function generate()
    {
        $bufferRecords = [];

        foreach ($this->records as $record) {
            if (!isset($bufferRecords[$record['delivery_id']])) {
                $bufferRecords[$record['delivery_id']] = [];
            }
            $bufferRecords[$record['delivery_id']][] = $record;
        }

        $records = [];
        foreach ($bufferRecords as $delivery_id => $record) {
            if (!isset($records[$record[0]['country_name']])) {
                $records[$record[0]['country_name']] = [];
            }
            $records[$record[0]['country_name']][$delivery_id] = $record;
        }

        $excel = new \PHPExcel();
        $excel->getProperties()->setCreator(Yii::$app->params['companyName']);
        $sheet = $excel->setActiveSheetIndex(0);

        $index = 0;

        foreach (self::$columns as $column => $width) {
            $char = chr(65 + $index);
            $sheet->getColumnDimension($char)->setWidth($width);
            $index++;
        }

        $rowNumber = 1;

        foreach ($records as $countryName => $deliveries) {
            $sheet->setCellValue('A' . $rowNumber, $countryName);
            $sheet->getStyle('A' . $rowNumber)->applyFromArray(self::$styleCountryHeader);
            $sheet->mergeCells('A' . $rowNumber . ':H' . $rowNumber);
            $sheet->getRowDimension($rowNumber)->setRowHeight(35);
            $rowNumber++;
            foreach ($deliveries as $delivery_id => $models) {
                $sheet->getRowDimension($rowNumber)->setRowHeight(25);
                $sheet->setCellValue('A' . $rowNumber, $models[0]['delivery_name']);
                $sheet->getStyle('A' . $rowNumber)->applyFromArray(self::$styleDeliveryCell);
                $sheet->mergeCells('A' . $rowNumber . ':A' . ($rowNumber + count($models) - 1));
                foreach ($models as $model) {
                    $sheet->setCellValue('B' . $rowNumber, Yii::$app->formatter->asMonth($model['start_month_time']));
                    $sheet->setCellValue('C' . $rowNumber,
                        number_format($model['sum_total'], 2) . ' ' . $model['currency']);
                    $sheet->setCellValue('D' . $rowNumber,
                        number_format($model['charges'], 2) . ' ' . $model['currency']);
                    $sheet->setCellValue('E' . $rowNumber,
                        number_format($model['end_sum'], 2) . ' ' . $model['currency']);
                    $sheet->setCellValue('F' . $rowNumber,
                        number_format($model['sum_total_dollars'], 2) . ' USD');
                    $sheet->setCellValue('G' . $rowNumber,
                        number_format($model['charges_dollars'], 2) . ' USD');
                    $sheet->setCellValue('H' . $rowNumber,
                        number_format($model['end_sum_dollars'], 2) . ' USD');

                    $sheet->getStyle('B' . $rowNumber)->applyFromArray(self::$styleMonthCell);
                    $sheet->getStyle('C' . $rowNumber)->applyFromArray(self::$styleSumCell);
                    $sheet->getStyle('D' . $rowNumber)->applyFromArray(self::$styleSumCell);
                    $sheet->getStyle('E' . $rowNumber)->applyFromArray(self::$styleSumCell);
                    $sheet->getStyle('F' . $rowNumber)->applyFromArray(self::$styleSumCell);
                    $sheet->getStyle('G' . $rowNumber)->applyFromArray(self::$styleSumCell);
                    $sheet->getStyle('H' . $rowNumber)->applyFromArray(self::$styleSumCell);
                    $sheet->getStyle('H' . $rowNumber)->getBorders()->getRight()->setBorderStyle(\PHPExcel_Style_Border::BORDER_MEDIUM);
                    $sheet->getStyle('A' . $rowNumber)->applyFromArray(self::$styleDeliveryCell);
                    $sheet->getRowDimension($rowNumber)->setRowHeight(25);
                    $rowNumber++;
                }
                $sheet->getStyle('B' . ($rowNumber - count($models)))->getBorders()->getTop()->setBorderStyle(\PHPExcel_Style_Border::BORDER_MEDIUM);
                $sheet->getStyle('C' . ($rowNumber - count($models)))->getBorders()->getTop()->setBorderStyle(\PHPExcel_Style_Border::BORDER_MEDIUM);
                $sheet->getStyle('D' . ($rowNumber - count($models)))->getBorders()->getTop()->setBorderStyle(\PHPExcel_Style_Border::BORDER_MEDIUM);
                $sheet->getStyle('E' . ($rowNumber - count($models)))->getBorders()->getTop()->setBorderStyle(\PHPExcel_Style_Border::BORDER_MEDIUM);
                $sheet->getStyle('F' . ($rowNumber - count($models)))->getBorders()->getTop()->setBorderStyle(\PHPExcel_Style_Border::BORDER_MEDIUM);
                $sheet->getStyle('G' . ($rowNumber - count($models)))->getBorders()->getTop()->setBorderStyle(\PHPExcel_Style_Border::BORDER_MEDIUM);
                $sheet->getStyle('H' . ($rowNumber - count($models)))->getBorders()->getTop()->setBorderStyle(\PHPExcel_Style_Border::BORDER_MEDIUM);

                $sheet->getStyle('B' . ($rowNumber - 1))->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_MEDIUM);
                $sheet->getStyle('C' . ($rowNumber - 1))->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_MEDIUM);
                $sheet->getStyle('D' . ($rowNumber - 1))->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_MEDIUM);
                $sheet->getStyle('E' . ($rowNumber - 1))->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_MEDIUM);
                $sheet->getStyle('F' . ($rowNumber - 1))->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_MEDIUM);
                $sheet->getStyle('G' . ($rowNumber - 1))->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_MEDIUM);
                $sheet->getStyle('H' . ($rowNumber - 1))->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_MEDIUM);
            }

            $rowNumber += 2;
        }

        return $excel;
    }
}
