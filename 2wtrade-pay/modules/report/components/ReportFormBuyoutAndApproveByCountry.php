<?php
namespace app\modules\report\components;


use app\models\Country;
use app\models\Product;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\models\Order;
use app\modules\order\models\OrderProduct;
use app\modules\report\components\filters\DateFilter;
use app\modules\report\extensions\Query;
use yii\data\ArrayDataProvider;
use app\modules\order\models\OrderStatus;

/**
 * Class ReportFormBuyoutAndApproveByCountry
 * @package app\modules\report\components\
 *
 * @property array $mapStatuses
 */
class ReportFormBuyoutAndApproveByCountry extends ReportForm
{
    public $groupBy = ['country', 'product'];

    public $groupLabel = 'country';

    public $groupName = 'country.name';

    /**
     * @param array $params
     * @return ArrayDataProvider
     */
    public function apply($params)
    {
        $this->getDateFilter()->type = DateFilter::TYPE_CALL_CENTER_SENT_AT;
        $this->load($params);

        if (!empty($params['s']['country_ids'])) {
            $this->applyGroupLabel($params);
        }

        $this->query = new Query();
        $this->query->from([Order::tableName()]);
        $this->query->leftJoin(CallCenterRequest::tableName(), CallCenterRequest::tableName() . '.order_id=' . Order::tableName() . '.id');
        $this->query->leftJoin(DeliveryRequest::tableName(), DeliveryRequest::tableName() . '.order_id=' . Order::tableName() . '.id');
        if ($this->groupLabel == 'delivery_ids') {
            $this->query->leftJoin(Delivery::tableName(), Delivery::tableName() . '.id= ' . $this->groupName);
        }
        $this->query->leftJoin(Country::tableName(), Order::tableName() . '.country_id=' . Country::tableName() . '.id');
        $this->query->leftJoin(OrderProduct::tableName(), Order::tableName() . '.id=' . OrderProduct::tableName() . '.order_id');
        $this->query->leftJoin(Product::tableName(), OrderProduct::tableName() . '.product_id=' . Product::tableName() . '.id');


        $this->applyFilters($params);
        $this->buildSelect();
        $this->query->groupBy($this->groupBy);


        $dataArray = $this->query->all();

        $dataProvider = new ArrayDataProvider([
            'allModels' => $dataArray,
            'sort' => [
                'attributes' => [
                    'country',
                    'product',
                    'approved' => ['default' => SORT_DESC,],
                    'buyout',
                    'allorders'
                ],
            ],
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $dataProvider->pagination->pageSize = $dataProvider->pagination->totalCount;

        return $dataProvider;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $output = '';

        return $output;
    }

    /**
     * @inheritdoc
     */
    protected function buildSelect()
    {
        $select = [
            $this->groupLabel => $this->groupName,
            'country_id' => Order::tableName() . '.country_id',
            'product' => Product::tableName() . '.name'
        ];

        if ($this->groupLabel == 'delivery_ids') {
            $select['delivery_name'] = Delivery::tableName() . '.name';
        }

        $this->query->select($select);

        $this->query->addCountDistinctCondition(Order::tableName() .'.id', 'allorders');
        $sumCondition = $this->query->buildIfCondition(Order::tableName() . '.status_id', OrderStatus::getApproveList(), OrderProduct::tableName() . '.order_id', 'NULL');
        $this->query->addCountDistinctCondition($sumCondition, 'approved');
        $sumCondition = $this->query->buildIfCondition(Order::tableName() . '.status_id', OrderStatus::getBuyoutList(), OrderProduct::tableName() . '.order_id', 'NULL');
        $this->query->addCountDistinctCondition($sumCondition, 'buyout');
        return $this;
    }

    /**
     * @param array $params
     * @return ReportForm
     */
    protected function applyFilters($params)
    {
        $this->getDateFilter()->apply($this->query, $params);
        $this->getCountrySelectFilter()->apply($this->query, $params);
        return $this;
    }

    /**
     * @param $params
     */
    protected function applyGroupLabel($params)
    {
        if (!empty($params['s']['groupLabel'])) {
            $this->groupLabel = $params['s']['groupLabel'];
            $this->groupBy = [$this->groupLabel, 'product'];
            $this->groupName = 'COALESCE(' . DeliveryRequest::tableName() . '.delivery_id,' . Order::tableName() . '.pending_delivery_id)';
        }
    }
}
