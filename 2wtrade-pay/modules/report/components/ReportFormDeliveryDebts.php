<?php

namespace app\modules\report\components;

use app\models\Country;
use app\models\Currency;
use app\modules\delivery\components\charges\ChargesCalculatorAbstract;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryContract;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\deliveryreport\models\DeliveryReport;
use app\modules\deliveryreport\models\DeliveryReportCosts;
use app\modules\order\models\Order;
use app\modules\order\models\OrderFinanceBalanceUsd;
use app\modules\order\models\OrderFinanceFactUsd;
use app\modules\order\models\OrderStatus;
use app\modules\report\components\filters\CountryDeliverySelectMultipleFilter;
use app\modules\report\components\filters\CountrySelectFilter;
use app\modules\report\components\filters\DebtsMonthFilter;
use app\modules\report\components\filters\DeliveryFilter;
use app\modules\report\models\ReportDeliveryDebts;
use Yii;
use yii\data\ArrayDataProvider;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * Class ReportFormDeliveryDebts
 * @package app\modules\report\components
 */
class ReportFormDeliveryDebts extends ReportForm
{
    // Ебаный костыль для долбоебов менеджеров и руководства, которые не могут пользоваться фильтром или которые ленивые жопы, отсекающий все данные после этого времени
    const REPORT_END_TO = '2018-11-30';

    const PRECISION = 2;

    /**
     * @var DebtsMonthFilter
     */
    public $monthFilter = null;

    /**
     * @var CountrySelectFilter
     */
    public $countrySelectFilter = null;

    /*
     * С какого месяца расчет задодлженности для счетчика
     */
    const DATE_FROM_DEBTS = '2018-05-01';

    /**
     * @var array
     */
    public $reportsErrors = [];

    /**
     * @param array $params
     * @param bool $checkErrors
     * @return ArrayDataProvider
     */
    public function apply($params, bool $checkErrors = false)
    {
        if (empty($params)) {
            $params[$this->getCountryDeliverySelectMultipleFilter()
                ->formName()]['country_ids'] = [Yii::$app->user->country->id];
        }

        $records = $this->loadRecordsFromTable($params);

        if ($checkErrors) {
            $this->reportsErrors = $this->checkReports($params[$this->formName()] ?? []);
        }

        $records = static::calculateTotalsForRecords($records);

        if (isset($params['sort'])) {
            $records = static::sortRecords($records, $params['sort']);
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $records,
            'pagination' => false,
            'sort' => [
                'attributes' => [
                    'month' => [
                        'asc' => [],
                        'desc' => [],
                    ],
                    'end_sum_dollars' => [
                        'asc' => [],
                        'desc' => [],
                    ],
                    'money_received_sum_total_dollars' => [
                        'asc' => [],
                        'desc' => [],
                    ],
                    'buyout_sum_total_dollars' => [
                        'asc' => [],
                        'desc' => [],
                    ],
                    'orders_in_process' => [
                        'asc' => [],
                        'desc' => [],
                    ],
                    'orders_unbuyout' => [
                        'asc' => [],
                        'desc' => [],
                    ],
                ]
            ]
        ]);

        return $dataProvider;
    }

    /**
     * @param array $params
     * @return array
     */
    protected function loadRecordsFromTable($params)
    {
        $this->query = ReportDeliveryDebts::find()
            ->joinWith(['delivery.country'], false)
            ->orderBy([
                Country::tableName() . '.id' => SORT_ASC,
                Delivery::tableName() . '.id' => SORT_ASC,
                ReportDeliveryDebts::tableName() . '.month' => SORT_ASC
            ]);
        $this->applyFilters($params);
        $this->query->andWhere([
            'is not', ReportDeliveryDebts::tableName() . '.created_at', null
        ]);

        return ArrayHelper::getColumn($this->query->all(), 'decodedData');
    }

    /**
     * @return null|array
     * @param array|null $params
     */
    protected function checkReports(?array $params = []): ?array
    {
        if (isset(Yii::$app->user) && Yii::$app->user->language->locale == 'es-ES') {
            if (isset($params['from'])) {
                $params['from'] = str_replace('/', '.', $params['from']);
            }
            if (isset($params['to'])) {
                $params['to'] = str_replace('/', '.', $params['to']);
            }
        }
        $deliveryRequestQuery = DeliveryRequest::find()
            ->select(new Expression("country.name as country, delivery_request.*, delivery.name, delivery_contract.id contract, DATE_FORMAT(FROM_UNIXTIME(".DeliveryRequest::clearTableName().".created_at), '%Y-%m-%d') as date"))
            ->joinWith(['delivery', 'deliveryContract', 'delivery.country'], false)
            ->orderBy('date')
            ->groupBy([DeliveryRequest::clearTableName().'.delivery_id', 'date']);
        if (!empty($params['country_ids'])) {
            $deliveryRequestQuery->andWhere(['country_id' => $params['country_ids']]);
        }
        if (!empty($params['delivery_ids'])) {
            $deliveryRequestQuery->andWhere([Delivery::tableName() . '.id' => $params['delivery_ids']]);
        }
        if (!empty($params['from'])) {
            $deliveryRequestQuery->andWhere(['>=', DeliveryRequest::clearTableName().'.created_at', strtotime('01.' . $params['from'])]);
        }
        if (!empty($params['to'])) {
            $deliveryRequestQuery->andWhere(['<=', DeliveryRequest::clearTableName().'.created_at', strtotime(date('Y-m-t', strtotime('01.' . $params['to'])))]);
        }
        $deliveryRequestQuery->asArray();
        $errors = [];
        foreach ($deliveryRequestQuery->batch(1000) as $requests) {
            /** @var DeliveryRequest $request */
            foreach ($requests as $request) {
                if (empty($request['contract'])) {
                    if (empty($errors[$request['delivery_id']]))
                    {
                        $errors[$request['delivery_id']]['name'] = $request['country'] . ' - ' . $request['name'];
                    }
                    $errors[$request['delivery_id']]['errors'][date('m.Y', strtotime($request['date']))]['contract'][] = date('d', strtotime($request['date'])); ;
                }
            }
        }
        return $errors;
    }

    /**
     * @param integer $deliveryId
     * @return Query
     * @throws \Exception
     */
    protected static function prepareQuery($deliveryId = null)
    {
        $query = DeliveryRequest::find()->joinWith(['order', 'delivery.country']);

        $requestTimeSend = 'COALESCE(' . DeliveryRequest::tableName() . '.second_sent_at, ' . DeliveryRequest::tableName() . '.sent_at, ' . DeliveryRequest::tableName() . '.created_at)';
        $fromQuery = DeliveryRequest::find()->select([
            'delivery_id',
            'order_id',
            'id',
            'coalesced_sent_at' => $requestTimeSend,
            'coalesced_sent_date' => "DATE(FROM_UNIXTIME({$requestTimeSend}))",
            'coalesced_paid_date' => "DATE(FROM_UNIXTIME(COALESCE(" . DeliveryRequest::tableName() . '.paid_at,' . DeliveryRequest::tableName() . '.done_at)))',
            'sent_at',
            'created_at'
        ]);
        $query->from([DeliveryRequest::tableName() => $fromQuery]);

        // Джойним конракт
        $requestTimeSend = DeliveryRequest::tableName() . '.coalesced_sent_date';
        $firstCaseCondition = DeliveryContract::tableName() . '.date_from IS NOT NULL AND ' . DeliveryContract::tableName() . ".date_to IS NOT NULL";
        $secondCaseCondition = DeliveryContract::tableName() . '.date_from IS NOT NULL AND ' . DeliveryContract::tableName() . ".date_to IS NULL";
        $thirdCaseCondition = DeliveryContract::tableName() . '.date_from IS NULL AND ' . DeliveryContract::tableName() . ".date_to IS NOT NULL";

        $firstCaseCheck = "{$requestTimeSend} >= " . DeliveryContract::tableName() . ".date_from AND {$requestTimeSend}" . ' <= ' . DeliveryContract::tableName() . '.date_to';
        $secondCaseCheck = "{$requestTimeSend} >= " . DeliveryContract::tableName() . '.date_from';
        $thirdCaseCheck = "{$requestTimeSend} <= " . DeliveryContract::tableName() . '.date_to';
        $query->leftJoin(DeliveryContract::tableName(), DeliveryContract::tableName() . '.delivery_id = ' . DeliveryRequest::tableName() . '.delivery_id AND ' . "CASE WHEN {$firstCaseCondition} THEN {$firstCaseCheck} WHEN {$secondCaseCondition} THEN {$secondCaseCheck} WHEN {$thirdCaseCondition} THEN {$thirdCaseCheck} ELSE 1 END");

        $select = [
            'income' => [],
            'costs' => [],
        ];

        $selectFactCharges = [
            'income' => [],
            'costs' => [],
        ];

        $aliasFact = 'finance_fact';
        $aliasBalance = 'finance_balance';
        $selectPopup = [];

        $subQuery = OrderFinanceFactUsd::find()
            ->select([
                'order_id' => 'order_id',
                'count_all' => 'count(DISTINCT order_id)',
            ])->joinWith(['deliveryReport'], false)->groupBy(['order_id']);
        foreach (OrderFinanceFactUsd::getPriceFields() as $columnName) {
            $subQuery->addSelect([$columnName => "SUM(IFNULL({$columnName}, 0))"]);
            $selectPopup[] = "SUM(IFNULL({$aliasFact}.{$columnName}, 0)) as {$columnName}_fact";
            $selectPopup[] = "SUM(CASE WHEN {$aliasFact}.{$columnName} THEN 1 ELSE 0 END) as {$columnName}_count_fact";
        }

        $balanceUsdQuery = (new Query())->select(
            'DISTINCT `order_finance_balance`.`delivery_request_id` AS `delivery_request_id`, `order_finance_balance`.`order_id` AS `order_id`,
               (CASE WHEN ((`order_finance_balance`.`price_cod` IS NOT NULL) AND (`order_finance_balance`.`price_cod` <> 0)) THEN
                   (CASE WHEN ((`price_cod_currency_rate_temp`.`rate` IS NOT NULL) AND (`price_cod_currency_rate_temp`.`rate` <> 0)) THEN (`order_finance_balance`.`price_cod` / `price_cod_currency_rate_temp`.`rate`)
                    ELSE CONVERTTOCURRENCYBYCOUNTRY(`order_finance_balance`.`price_cod`, COALESCE(`order_finance_balance`.`price_cod_currency_id`, `order`.`price_currency`, `country`.`currency_id`), `usd_currency`.`id`)
                    END)
                ELSE 0 END) AS `price_cod`,
               (CASE WHEN ((`order_finance_balance`.`price_storage` IS NOT NULL) AND (`order_finance_balance`.`price_storage` <> 0)) THEN
                   (CASE WHEN ((`price_storage_currency_rate_temp`.`rate` IS NOT NULL) AND (`price_storage_currency_rate_temp`.`rate` <> 0)) THEN (`order_finance_balance`.`price_storage` / `price_storage_currency_rate_temp`.`rate`)
                    ELSE CONVERTTOCURRENCYBYCOUNTRY(`order_finance_balance`.`price_storage`, COALESCE(`order_finance_balance`.`price_storage_currency_id`, `order`.`price_currency`, `country`.`currency_id`), `usd_currency`.`id`)
                    END)
                ELSE 0 END) AS `price_storage`,
               (CASE WHEN ((`order_finance_balance`.`price_fulfilment` IS NOT NULL) AND (`order_finance_balance`.`price_fulfilment` <> 0)) THEN
                   (CASE WHEN ((`price_fulfilment_currency_rate_temp`.`rate` IS NOT NULL) AND (`price_fulfilment_currency_rate_temp`.`rate` <> 0)) THEN (`order_finance_balance`.`price_fulfilment` / `price_fulfilment_currency_rate_temp`.`rate`)
                    ELSE CONVERTTOCURRENCYBYCOUNTRY(`order_finance_balance`.`price_fulfilment`, COALESCE(`order_finance_balance`.`price_fulfilment_currency_id`, `order`.`price_currency`, `country`.`currency_id`), `usd_currency`.`id`)
                    END)
                ELSE 0 END) AS `price_fulfilment`,
               (CASE WHEN ((`order_finance_balance`.`price_packing` IS NOT NULL) AND (`order_finance_balance`.`price_packing` <> 0)) THEN
                   (CASE WHEN ((`price_packing_currency_rate_temp`.`rate` IS NOT NULL) AND (`price_packing_currency_rate_temp`.`rate` <> 0)) THEN (`order_finance_balance`.`price_packing` / `price_packing_currency_rate_temp`.`rate`)
                    ELSE CONVERTTOCURRENCYBYCOUNTRY(`order_finance_balance`.`price_packing`, COALESCE(`order_finance_balance`.`price_packing_currency_id`, `order`.`price_currency`, `country`.`currency_id`), `usd_currency`.`id`)
                    END)
                ELSE 0 END) AS `price_packing`,
               (CASE WHEN ((`order_finance_balance`.`price_package` IS NOT NULL) AND (`order_finance_balance`.`price_package` <> 0)) THEN
                   (CASE WHEN ((`price_package_currency_rate_temp`.`rate` IS NOT NULL) AND (`price_package_currency_rate_temp`.`rate` <> 0)) THEN (`order_finance_balance`.`price_package` / `price_package_currency_rate_temp`.`rate`)
                    ELSE CONVERTTOCURRENCYBYCOUNTRY(`order_finance_balance`.`price_package`, COALESCE(`order_finance_balance`.`price_package_currency_id`, `order`.`price_currency`, `country`.`currency_id`), `usd_currency`.`id`)
                    END)
                ELSE 0 END) AS `price_package`,
               (CASE WHEN ((`order_finance_balance`.`price_delivery` IS NOT NULL) AND (`order_finance_balance`.`price_delivery` <> 0)) THEN
                   (CASE WHEN ((`price_delivery_currency_rate_temp`.`rate` IS NOT NULL) AND (`price_delivery_currency_rate_temp`.`rate` <> 0)) THEN (`order_finance_balance`.`price_delivery` / `price_delivery_currency_rate_temp`.`rate`)
                    ELSE CONVERTTOCURRENCYBYCOUNTRY(`order_finance_balance`.`price_delivery`, COALESCE(`order_finance_balance`.`price_delivery_currency_id`, `order`.`price_currency`, `country`.`currency_id`), `usd_currency`.`id`)
                    END)
                ELSE 0 END) AS `price_delivery`,
               (CASE WHEN ((`order_finance_balance`.`price_redelivery` IS NOT NULL) AND (`order_finance_balance`.`price_redelivery` <> 0)) THEN
                   (CASE WHEN ((`price_redelivery_currency_rate_temp`.`rate` IS NOT NULL) AND (`price_redelivery_currency_rate_temp`.`rate` <> 0)) THEN (`order_finance_balance`.`price_redelivery` / `price_redelivery_currency_rate_temp`.`rate`)
                    ELSE CONVERTTOCURRENCYBYCOUNTRY(`order_finance_balance`.`price_redelivery`, COALESCE(`order_finance_balance`.`price_redelivery_currency_id`, `order`.`price_currency`, `country`.`currency_id`), `usd_currency`.`id`)
                    END)
                ELSE 0 END) AS `price_redelivery`,
               (CASE WHEN ((`order_finance_balance`.`price_delivery_back` IS NOT NULL) AND (`order_finance_balance`.`price_delivery_back` <> 0)) THEN
                   (CASE WHEN ((`price_delivery_back_currency_rate_temp`.`rate` IS NOT NULL) AND (`price_delivery_back_currency_rate_temp`.`rate` <> 0)) THEN (`order_finance_balance`.`price_delivery_back` / `price_delivery_back_currency_rate_temp`.`rate`)
                    ELSE CONVERTTOCURRENCYBYCOUNTRY(`order_finance_balance`.`price_delivery_back`, COALESCE(`order_finance_balance`.`price_delivery_back_currency_id`, `order`.`price_currency`, `country`.`currency_id`), `usd_currency`.`id`)
                    END)
                ELSE 0 END) AS `price_delivery_back`,
               (CASE WHEN ((`order_finance_balance`.`price_delivery_return` IS NOT NULL) AND (`order_finance_balance`.`price_delivery_return` <> 0)) THEN
                   (CASE WHEN ((`price_delivery_return_currency_rate_temp`.`rate` IS NOT NULL) AND (`price_delivery_return_currency_rate_temp`.`rate` <> 0)) THEN (`order_finance_balance`.`price_delivery_return` / `price_delivery_return_currency_rate_temp`.`rate`)
                    ELSE CONVERTTOCURRENCYBYCOUNTRY(`order_finance_balance`.`price_delivery_return`, COALESCE(`order_finance_balance`.`price_delivery_return_currency_id`, `order`.`price_currency`, `country`.`currency_id`), `usd_currency`.`id`)
                    END)
                ELSE 0 END) AS `price_delivery_return`,
               (CASE WHEN ((`order_finance_balance`.`price_address_correction` IS NOT NULL) AND (`order_finance_balance`.`price_address_correction` <> 0)) THEN
                   (CASE WHEN ((`price_address_correction_currency_rate_temp`.`rate` IS NOT NULL) AND (`price_address_correction_currency_rate_temp`.`rate` <> 0)) THEN (`order_finance_balance`.`price_address_correction` / `price_address_correction_currency_rate_temp`.`rate`)
                    ELSE CONVERTTOCURRENCYBYCOUNTRY(`order_finance_balance`.`price_address_correction`, COALESCE(`order_finance_balance`.`price_address_correction_currency_id`, `order`.`price_currency`, `country`.`currency_id`), `usd_currency`.`id`)
                    END)
                ELSE 0 END) AS `price_address_correction`,
               (CASE WHEN ((`order_finance_balance`.`price_cod_service` IS NOT NULL) AND (`order_finance_balance`.`price_cod_service` <> 0)) THEN
                   (CASE WHEN ((`price_cod_service_currency_rate_temp`.`rate` IS NOT NULL) AND (`price_cod_service_currency_rate_temp`.`rate` <> 0)) THEN (`order_finance_balance`.`price_cod_service` / `price_cod_service_currency_rate_temp`.`rate`)
                    ELSE CONVERTTOCURRENCYBYCOUNTRY(`order_finance_balance`.`price_cod_service`, COALESCE(`order_finance_balance`.`price_cod_service_currency_id`, `order`.`price_currency`, `country`.`currency_id`), `usd_currency`.`id`)
                    END)
                ELSE 0 END) AS `price_cod_service`,
               (CASE WHEN ((`order_finance_balance`.`price_vat` IS NOT NULL) AND (`order_finance_balance`.`price_vat` <> 0)) THEN
                   (CASE WHEN ((`price_vat_currency_rate_temp`.`rate` IS NOT NULL) AND (`price_vat_currency_rate_temp`.`rate` <> 0)) THEN (`order_finance_balance`.`price_vat` / `price_vat_currency_rate_temp`.`rate`)
                    ELSE CONVERTTOCURRENCYBYCOUNTRY(`order_finance_balance`.`price_vat`, COALESCE(`order_finance_balance`.`price_vat_currency_id`, `order`.`price_currency`, `country`.`currency_id`), `usd_currency`.`id`)
                    END)
                ELSE 0 END) AS `price_vat`,
               `order_finance_balance`.`additional_prices` AS `additional_prices`,
               `order_finance_balance`.`created_at` AS `created_at`,
               `order_finance_balance`.`updated_at` AS `updated_at`'
        )->from(
            '((((((((((((((((`order_finance_balance`
                LEFT JOIN `order` ON ((`order`.`id` = `order_finance_balance`.`order_id`)))
                LEFT JOIN `country` ON ((`country`.`id` = `order`.`country_id`)))
                JOIN (SELECT `currency`.`id` AS `id` FROM `currency` WHERE (`currency`.`char_code` = \'USD\') LIMIT 1) `usd_currency`)
                LEFT JOIN `invoice_order` ON ((`order_finance_balance`.`order_id` = `invoice_order`.`order_id`)))
                LEFT JOIN `invoice_currency` `price_cod_currency_rate_temp` ON (((`price_cod_currency_rate_temp`.`invoice_id` = `invoice_order`.`invoice_id`) AND (`price_cod_currency_rate_temp`.`invoice_currency_id` = `usd_currency`.`id`) AND (`price_cod_currency_rate_temp`.`currency_id` = `order_finance_balance`.`price_cod_currency_id`))))
                LEFT JOIN `invoice_currency` `price_storage_currency_rate_temp` ON (((`price_storage_currency_rate_temp`.`invoice_id` = `invoice_order`.`invoice_id`) AND (`price_storage_currency_rate_temp`.`invoice_currency_id` = `usd_currency`.`id`) AND (`price_storage_currency_rate_temp`.`currency_id` = `order_finance_balance`.`price_storage_currency_id`))))
                LEFT JOIN `invoice_currency` `price_fulfilment_currency_rate_temp` ON (((`price_fulfilment_currency_rate_temp`.`invoice_id` = `invoice_order`.`invoice_id`) AND (`price_fulfilment_currency_rate_temp`.`invoice_currency_id` = `usd_currency`.`id`) AND (`price_fulfilment_currency_rate_temp`.`currency_id` = `order_finance_balance`.`price_fulfilment_currency_id`))))
                LEFT JOIN `invoice_currency` `price_packing_currency_rate_temp` ON (((`price_packing_currency_rate_temp`.`invoice_id` = `invoice_order`.`invoice_id`) AND (`price_packing_currency_rate_temp`.`invoice_currency_id` = `usd_currency`.`id`) AND (`price_packing_currency_rate_temp`.`currency_id` = `order_finance_balance`.`price_packing_currency_id`))))
                LEFT JOIN `invoice_currency` `price_package_currency_rate_temp` ON (((`price_package_currency_rate_temp`.`invoice_id` = `invoice_order`.`invoice_id`) AND (`price_package_currency_rate_temp`.`invoice_currency_id` = `usd_currency`.`id`) AND (`price_package_currency_rate_temp`.`currency_id` = `order_finance_balance`.`price_package_currency_id`))))
                LEFT JOIN `invoice_currency` `price_address_correction_currency_rate_temp` ON (((`price_address_correction_currency_rate_temp`.`invoice_id` = `invoice_order`.`invoice_id`) AND (`price_address_correction_currency_rate_temp`.`invoice_currency_id` = `usd_currency`.`id`) AND (`price_address_correction_currency_rate_temp`.`currency_id` = `order_finance_balance`.`price_address_correction_currency_id`))))
                LEFT JOIN `invoice_currency` `price_delivery_currency_rate_temp` ON (((`price_delivery_currency_rate_temp`.`invoice_id` = `invoice_order`.`invoice_id`) AND (`price_delivery_currency_rate_temp`.`invoice_currency_id` = `usd_currency`.`id`) AND (`price_delivery_currency_rate_temp`.`currency_id` = `order_finance_balance`.`price_delivery_currency_id`))))
                LEFT JOIN `invoice_currency` `price_redelivery_currency_rate_temp` ON (((`price_redelivery_currency_rate_temp`.`invoice_id` = `invoice_order`.`invoice_id`) AND (`price_redelivery_currency_rate_temp`.`invoice_currency_id` = `usd_currency`.`id`) AND (`price_redelivery_currency_rate_temp`.`currency_id` = `order_finance_balance`.`price_redelivery_currency_id`))))
                LEFT JOIN `invoice_currency` `price_delivery_back_currency_rate_temp` ON (((`price_delivery_back_currency_rate_temp`.`invoice_id` = `invoice_order`.`invoice_id`) AND (`price_delivery_back_currency_rate_temp`.`invoice_currency_id` = `usd_currency`.`id`) AND (`price_delivery_back_currency_rate_temp`.`currency_id` = `order_finance_balance`.`price_delivery_back_currency_id`))))
                LEFT JOIN `invoice_currency` `price_delivery_return_currency_rate_temp` ON (((`price_delivery_return_currency_rate_temp`.`invoice_id` = `invoice_order`.`invoice_id`) AND (`price_delivery_return_currency_rate_temp`.`invoice_currency_id` = `usd_currency`.`id`) AND (`price_delivery_return_currency_rate_temp`.`currency_id` = `order_finance_balance`.`price_delivery_return_currency_id`))))
                LEFT JOIN `invoice_currency` `price_cod_service_currency_rate_temp` ON (((`price_cod_service_currency_rate_temp`.`invoice_id` = `invoice_order`.`invoice_id`) AND (`price_cod_service_currency_rate_temp`.`invoice_currency_id` = `usd_currency`.`id`) AND (`price_cod_service_currency_rate_temp`.`currency_id` = `order_finance_balance`.`price_cod_service_currency_id`))))
                LEFT JOIN `invoice_currency` `price_vat_currency_rate_temp` ON (((`price_vat_currency_rate_temp`.`invoice_id` = `invoice_order`.`invoice_id`) AND (`price_vat_currency_rate_temp`.`invoice_currency_id` = `usd_currency`.`id`) AND (`price_vat_currency_rate_temp`.`currency_id` = `order_finance_balance`.`price_vat_currency_id`))))'
        )->innerJoin(DeliveryRequest::tableName(), '`order_finance_balance`.`delivery_request_id` = ' . DeliveryRequest::tableName() . '.`id`');
        if($deliveryId) {
            $balanceUsdQuery->andWhere([DeliveryRequest::tableName() . '.delivery_id' => $deliveryId]);
        }
        $subQueryBalance = (new Query())
            ->select([
                'order_id' => 'balance.order_id',
                'count_all' => 'count(DISTINCT ' . 'balance.order_id)',
            ])
            ->from(['balance' => $balanceUsdQuery])
            ->groupBy(['order_id']);
        foreach (OrderFinanceBalanceUsd::getPriceFields() as $columnName) {
            $subQueryBalance->addSelect([$columnName => "SUM(CASE WHEN {$columnName} IS NOT NULL AND {$columnName} != 0 THEN {$columnName} ELSE 0 END)"]);
            $selectPopup[] = "SUM(IFNULL({$aliasBalance}.{$columnName}, 0)) as {$columnName}_balance";
            $selectPopup[] = "SUM(CASE WHEN {$aliasBalance}.{$columnName} THEN 1 ELSE 0 END) as {$columnName}_count_balance";
        }

        if($deliveryId) {
            $query->andWhere([DeliveryRequest::tableName().'.delivery_id' => $deliveryId]);
            $subQuery->andWhere([DeliveryReport::tableName().'.delivery_id' => $deliveryId]);
        }

        $query->leftJoin([$aliasFact => $subQuery], $aliasFact . '.order_id=' . Order::tableName() . '.id');
        $query->leftJoin([$aliasBalance => $subQueryBalance], $aliasBalance . '.order_id=' . Order::tableName() . '.id');

        foreach (OrderFinanceBalanceUsd::getPriceFields() as $key) {
            $type = OrderFinanceBalanceUsd::getFinanceTypeOfField($key) == OrderFinanceBalanceUsd::FINANCE_TYPE_INCOME ? 'income' : 'costs';
            $select[$type][] = "IFNULL({$aliasBalance}.{$key}, 0)";
            $selectFactCharges[$type][] = "IFNULL({$aliasFact}.{$key}, 0)";
        }

        $query->select([
            'month' => 'FROM_UNIXTIME(' . DeliveryRequest::tableName() . '.coalesced_sent_at, "%Y-%m-01")',
            'delivery_id' => DeliveryRequest::tableName() . '.delivery_id',
            'contract_id' => DeliveryContract::tableName() . '.id',
            'country_id' => Delivery::tableName() . '.country_id',
            'delivery_name' => Delivery::tableName() . '.name',
            'country_name' => Country::tableName() . '.name',
            'country_slug' => Country::tableName() . '.slug',
            'orders_in_process' => 'COUNT(' . Order::tableName() . '.status_id IN (' . implode(',', OrderStatus::getProcessDeliveryAndLogisticList()) . ') OR NULL)',
            'orders_unbuyout' => 'COUNT(' . Order::tableName() . '.status_id IN (' . implode(',', OrderStatus::getOnlyNotBuyoutInDeliveryList()) . ') OR NULL)',
            'orders_total' => 'COUNT(DISTINCT ' . DeliveryRequest::tableName() . '.id)',
            'orders_buyout' => 'COUNT(' . Order::tableName() . '.status_id IN (' . implode(',', OrderStatus::getBuyoutList()) . ') OR NULL)',
            'orders_money_received' => 'COUNT(' . Order::tableName() . '.status_id IN (' . implode(',', OrderStatus::getOnlyMoneyReceivedList()) . ') OR NULL)',

            'money_received_sum_total_dollars' => "SUM(" . implode('+', $selectFactCharges['income']) . ")",
            'charges_accepted_dollars' => 'SUM(' . implode('+', $selectFactCharges['costs']) . ')',

            'buyout_sum_total_dollars' => "SUM(" . implode('+', $select['income']) . ')',
            'charges_not_accepted_dollars' => 'SUM(' . implode('+', $select['costs']) . ')',
        ]);

        if (!empty($selectPopup)) {
            $query->addSelect($selectPopup);
//            кол-во учтенных по услугам
            $query->addSelect([
                'month_count_all_fact' => "SUM(IF({$aliasFact}.order_id AND (" . implode(' OR ', array_map(function ($val) use ($aliasFact) {return $aliasFact . '.' . $val;}, array_filter(OrderFinanceFactUsd::getPriceFields(), function ($val){return $val != 'price_cod';}))) . '), 1, NULL))',
                'month_count_all_balance' => "SUM(IF({$aliasBalance}.order_id AND (" . implode(' OR ', array_map(function ($val) use ($aliasBalance) {return $aliasBalance . '.' . $val;}, array_filter(OrderFinanceBalanceUsd::getPriceFields(), function ($val){return $val != 'price_cod';}))) . '), 1, NULL))',
                'month_count_all' => "COUNT(DISTINCT IFNULL({$aliasBalance}.order_id, {$aliasFact}.order_id))",
            ]);
//            сумарное кол-во по фин.колонкам (суммировать из факта и баланса нельзя из-за возможности разбития оплаты)
            foreach (OrderFinanceBalanceUsd::getPriceFields() + OrderFinanceFactUsd::getPriceFields() as $field) {
                $query->addSelect([
                    "{$field}_count" => "SUM(IF({$aliasBalance}.{$field} OR {$aliasFact}.{$field}, 1, 0))"
                ]);
            }
        }

        $query->andWhere([
            Country::tableName().'.is_partner' => 0,
            Country::tableName().'.is_distributor' => 0,
//            из-за заказов, которые отклонены до курьерки, но имеют связь с ней
            Order::tableName() . '.status_id' => OrderStatus::getInDeliveryAndFinanceList(),
        ]);

        $query->groupBy(['delivery_id', 'month', 'contract_id']);
        $query->orderBy(['country_id' => SORT_ASC, 'delivery_id' => SORT_ASC, 'month' => SORT_ASC]);

        return $query;
    }

    /**
     * @param Query $query
     * @param Delivery $delivery
     * @return array
     * @throws \yii\db\Exception
     */
    protected static function getRecordsFromQuery(Query $query, Delivery $delivery)
    {
        $priceFields = OrderFinanceFactUsd::getPriceFields();
        $tempRecords = ArrayHelper::index($query->createCommand()->queryAll(), null, ['delivery_id', 'month']);
        $sumFields = array_unique(array_merge([
            'orders_in_process',
            'orders_unbuyout',
            'orders_total',
            'orders_buyout',
            'orders_money_received',
            'money_received_sum_total_dollars',
            'charges_accepted_dollars',
            'buyout_sum_total_dollars',
            'charges_not_accepted_dollars',
            'month_count_all',
            'month_count_all_fact',
            'month_count_all_balance',
        ], static::getPopupFields()));

        $usdId = Currency::getUSD()->id;
//        издержки из отчетов
        $expenses = DeliveryReport::find()
            ->select([
// rate тут не только к USD, но и к EUR, при этом мы не храним к чему он... пока вырежем, его либо доработать либо вырезать нахер под корень
                'expense' => "convert_value_to_currency_by_date(total_costs, country.currency_id, {$usdId}, FROM_UNIXTIME(period_from, '%Y-%m-01'))",
                'month_count' => 'IFNULL(TIMESTAMPDIFF(MONTH, FROM_UNIXTIME(period_from, "%Y-%m-01"), FROM_UNIXTIME(period_to, "%Y-%m-01")) + 1, 1)',
                'period_from' => 'FROM_UNIXTIME(period_from, "%Y-%m-01")',
                'period_to' => 'FROM_UNIXTIME(period_to, "%Y-%m-01")',
            ])
            ->joinWith(['country'])
            ->where(['delivery_id' => $delivery->id])
            ->andWhere(['OR',
                ['>', 'sum_total', 0],
                ['>', 'total_costs', 0],
            ])
            ->asArray()->all();
        $expensesByMonth = $expensesCountByMonth = [];
//        TODO: переделать отображение комиссии банка
        foreach ($expenses as $expense) {
            if ($avg = round($expense['expense'] / $expense['month_count'], self::PRECISION)) {
                for ($i = 0; $i < $expense['month_count']; $i++) {
                    $month = date('Y-m-01', strtotime("+{$i} month", strtotime($expense['period_from'])));
                    $expensesByMonth[$month] += $avg;
                    $expensesCountByMonth[$month]++;
                }
            }
        }
//        TODO: реализовать отображение доп.издержек (без привязки к репортам - дата + курьерка)

        static $contracts = null;
        if (is_null($contracts)) {
            $contracts = DeliveryContract::find()->with(['chargesCalculatorModel'])->indexBy('id')->all();
        }
        $records = [];

        foreach ($tempRecords as $months) {
            foreach ($months as $month => $contractsData) {
                $monthData = [];
                $balanceExpenses = $expensesByMonth[$month] ?? 0;
                foreach ($contractsData as $data) {

                    if ($data['contract_id']) {
                        $contract = $contracts[$data['contract_id']];

                        if ($contract->chargesCalculatorModel) {
                            foreach ($priceFields as $fieldName) {
                                $type = $contract->chargesCalculator->getCalculatingTypeForField($fieldName);

//                                Это будет хренью если в месяце будет 2 контракта и в 1 из них будет помесячные платежи, а в другом за ордер
                                if ($type["type"] == ChargesCalculatorAbstract::CALCULATING_TYPE_MONTHLY_CHARGE) {
                                    $charge = round($type['charge'] / $type['currency_rate'], self::PRECISION);
                                    if ($charge > $balanceExpenses) {
                                        $charge -= $balanceExpenses;
                                        $balanceExpenses = 0;
                                        $data[$fieldName . '_balance'] += $charge;
                                        $data[$fieldName . '_count_balance'] += 1;
                                        $data[$fieldName . '_count'] += 1;
                                        $data['charges_not_accepted_dollars'] += $charge;
                                    } else {
                                        $balanceExpenses -= $charge;
                                    }
                                }
                            }
                        }
                    }
                    if (empty($monthData)) {
                        $monthData = $data;
                    } else {
                        foreach ($sumFields as $fieldName) {
                            $monthData[$fieldName] += $data[$fieldName];
                        }
                    }
                }
                if (!empty($balanceExpenses)) {
                    $monthData['expenses_balance'] = -$balanceExpenses;
                    $monthData['expenses_count_balance'] = $expensesCountByMonth[$month];
                    $monthData['charges_not_accepted_dollars'] -= $balanceExpenses;
                }
                if (!empty($expensesByMonth[$month])) {
                    $monthData['expenses_fact'] = $expensesByMonth[$month];
                    $monthData['expenses_count_fact'] = $monthData['expenses_count'] = $expensesCountByMonth[$month];
                    $monthData['charges_accepted_dollars'] += $expensesByMonth[$month];
                }

                $monthData['only_buyout'] = $monthData['orders_total'] - $monthData['orders_in_process'];
                $monthData['sum_total_dollars'] = $monthData['buyout_sum_total_dollars'] + $monthData['money_received_sum_total_dollars'];
                $monthData['charges_total_dollars'] = $monthData['charges_not_accepted_dollars'] + $monthData['charges_accepted_dollars'];
                $monthData['end_sum_dollars'] = $monthData['sum_total_dollars'];
                foreach (array_unique(array_merge(OrderFinanceBalanceUsd::getPriceFields(), OrderFinanceFactUsd::getPriceFields(), ['expenses'])) as $field) {
                    $monthData[$field . '_fact'] = round($monthData[$field . '_fact'], self::PRECISION);
                    $monthData[$field . '_balance'] = round($monthData[$field . '_balance'], self::PRECISION);
                    $monthData[$field] = round($monthData[$field . '_fact'] + $monthData[$field . '_balance'], self::PRECISION);
                }
                $records[] = $monthData;
            }
        }

        return $records;
    }

    /**
     * @param array $records
     * @return array
     */
    protected static function calculateTotalsForRecords($records)
    {
        $columnsForCalculatingTotals = [
            'end_sum_dollars',
            'charges_not_accepted_dollars',
            'charges_accepted_dollars',
            'orders_in_process',
            'orders_unbuyout',
            'orders_buyout',
            'orders_total',
            'orders_money_received',
            'buyout_sum_total_dollars',
            'money_received_sum_total_dollars',
            'month_count_all',
            'month_count_all_fact',
            'month_count_all_balance',
            'expenses',
            'expenses_fact',
            'expenses_balance',
        ];
        $columnsForCalculatingTotals = array_unique(array_merge($columnsForCalculatingTotals, static::getPopupFields()));
        $countryTotal = $deliveryTotal = [];
        foreach ($records as $record) {
            foreach ($columnsForCalculatingTotals as $column) {
                $countryTotal[$record['country_id']]["total_country_{$column}"] = ($countryTotal[$record['country_id']]["total_country_{$column}"] ?? 0) + ($record[$column] ?? 0);
                $deliveryTotal[$record['delivery_id']]["total_delivery_{$column}"] = ($deliveryTotal[$record['delivery_id']]["total_delivery_{$column}"] ?? 0) + ($record[$column] ?? 0);
            }
            if (!isset($countryTotal[$record['country_id']]['total_country_min_month']) || $countryTotal[$record['country_id']]['total_country_min_month'] > $record['month']) {
                $countryTotal[$record['country_id']]['total_country_min_month'] = $record['month'];
            }
            if (!isset($deliveryTotal[$record['delivery_id']]['total_delivery_min_month']) || $deliveryTotal[$record['delivery_id']]['total_delivery_min_month'] > $record['month']) {
                $deliveryTotal[$record['delivery_id']]['total_delivery_min_month'] = $record['month'];
            }

            if (!isset($countryTotal[$record['country_id']]['total_country_max_month']) || $countryTotal[$record['country_id']]['total_country_max_month'] < $record['month']) {
                $countryTotal[$record['country_id']]['total_country_max_month'] = $record['month'];
            }
            if (!isset($deliveryTotal[$record['delivery_id']]['total_delivery_max_month']) || $deliveryTotal[$record['delivery_id']]['total_delivery_max_month'] < $record['month']) {
                $deliveryTotal[$record['delivery_id']]['total_delivery_max_month'] = $record['month'];
            }
        }

        foreach ($records as $key => $record) {
            $records[$key] = array_merge($countryTotal[$record['country_id']], $deliveryTotal[$record['delivery_id']], $record);
        }

        return $records;
    }

    /**
     * @param array $records
     * @param string $sortKey
     * @param string $defaultSortKey
     * @return array
     */
    public static function sortRecords($records, $sortKey, $defaultSortKey = 'month')
    {
        $prefix = substr($sortKey, 0, 1) == '-';
        if ($prefix) {
            $sortKey = substr($sortKey, 1);
        }
        if (in_array($sortKey, [
            'end_sum_dollars',
            'money_received_sum_total_dollars',
            'buyout_sum_total_dollars',
            'orders_in_process',
            'orders_unbuyout',
        ])) {
            $totalKeyType = 'sum';
        } else {
            $totalKeyType = 'maxValue';
        }

        $countrySum = [];
        $deliverySum = [];
        foreach ($records as $key => $record) {
            if ($totalKeyType == 'sum') {
                if (!isset($countrySum[$record['country_id']])) {
                    $countrySum[$record['country_id']] = 0;
                }
                if (!isset($deliverySum[$record['delivery_id']])) {
                    $deliverySum[$record['delivery_id']] = 0;
                }

                $countrySum[$record['country_id']] += $record[$sortKey];
                $deliverySum[$record['delivery_id']] += $record[$sortKey];
            } else {
                if (!isset($countrySum[$record['country_id']]) || (($countrySum[$record['country_id']] > $record[$sortKey]) ^ $prefix)) {
                    $countrySum[$record['country_id']] = $record[$sortKey];
                }
                if (!isset($deliverySum[$record['delivery_id']]) || (($deliverySum[$record['delivery_id']] > $record[$sortKey]) ^ $prefix)) {
                    $deliverySum[$record['delivery_id']] = $record[$sortKey];
                }
            }
        }

        usort($records, function ($itemA, $itemB) use ($countrySum, $deliverySum, $prefix, $sortKey, $defaultSortKey) {
            $answer = 0;
            if ($itemA['country_id'] != $itemB['country_id']) {
                if ($countrySum[$itemA['country_id']] == $countrySum[$itemB['country_id']]) {
                    $answer = $itemA['country_id'] < $itemB['country_id'] ? -1 : 1;
                } else {
                    $answer = ($countrySum[$itemA['country_id']] < $countrySum[$itemB['country_id']]) ? -1 : 1;
                }
            } else {
                if ($itemA['delivery_id'] != $itemB['delivery_id']) {
                    if ($deliverySum[$itemA['delivery_id']] == $deliverySum[$itemB['delivery_id']]) {
                        $answer = $itemA['delivery_id'] < $itemB['delivery_id'] ? -1 : 1;
                    } else {
                        $answer = ($deliverySum[$itemA['delivery_id']] < $deliverySum[$itemB['delivery_id']]) ? -1 : 1;
                    }
                } else {
                    if ($itemA[$sortKey] != $itemB[$sortKey]) {
                        $answer = $itemA[$sortKey] < $itemB[$sortKey] ? -1 : 1;
                    } elseif ($itemA[$defaultSortKey] != $itemB[$defaultSortKey]) {
                        $answer = $itemA[$defaultSortKey] < $itemB[$defaultSortKey] ? -1 : 1;
                    }
                }
            }

            return $prefix ? -1 * $answer : $answer;
        });

        return $records;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return '';
    }

    /**
     * @param array $params
     * @return $this
     */
    public function applyFilters($params)
    {
        $this->getMonthFilter()->apply($this->query, $params);
        $this->getCountryDeliverySelectMultipleFilter()->apply($this->query, $params);
        return $this;
    }

    /**
     * @param $query
     * @param $params
     * @param $groupByMonth
     * @return $this
     */
    public function applyFiltersForQuery($query, $params, $groupByMonth = true)
    {
        if ($groupByMonth) {
            $this->getMonthFilter()->apply($query, $params);
        }
        $this->getCountryDeliverySelectMultipleFilter()->apply($query, $params);
        return $this;
    }

    /**
     * @return DebtsMonthFilter
     */
    public function getMonthFilter()
    {
        if (is_null($this->monthFilter)) {
            $this->monthFilter = new DebtsMonthFilter([
                'from' => Yii::$app->formatter->asMonth(strtotime(' - 1month', strtotime(static::REPORT_END_TO))),
                'to' => Yii::$app->formatter->asMonth(strtotime(static::REPORT_END_TO)),
                'maxDate' => static::REPORT_END_TO
            ]);
        }
        return $this->monthFilter;
    }

    /**
     * @return CountryDeliverySelectMultipleFilter
     */
    public function getCountryDeliverySelectMultipleFilter()
    {
        if (is_null($this->countryDeliverySelectMultipleFilter)) {
            $this->countryDeliverySelectMultipleFilter = new CountryDeliverySelectMultipleFilter([
                'deliveryType' => CountryDeliverySelectMultipleFilter::DELIVERY_TYPE_DELIVERY,
            ]);
        }

        return $this->countryDeliverySelectMultipleFilter;
    }

    /**
     * @return DeliveryFilter
     */
    public function getDeliveryFilter()
    {
        if (is_null($this->deliveryFilter)) {
            $this->deliveryFilter = new DeliveryFilter([
                'type' => DeliveryFilter::TYPE_DELIVERY,
                'reportForm' => $this
            ]);
        }
        return $this->deliveryFilter;
    }

    /**
     * Подготовка данных и сохранение их в таблицу ReportDeliveryDebts
     * @param integer $deliveryId
     * @param integer $countryId
     * @throws \Exception
     */
    public static function prepareData($deliveryId = null, $countryId = null)
    {
        $queryDeliveries = Delivery::find()
            ->with(['deliveryContracts.chargesCalculatorModel'])
            ->orderBy(['country_id' => SORT_ASC, 'id' => SORT_ASC]);
        if (!is_null($deliveryId)) {
            $queryDeliveries->andWhere(['id' => $deliveryId]);
        }
        if (!is_null($countryId)) {
            $queryDeliveries->andWhere(['country_id' => $countryId]);
        }

        $deliveries = $queryDeliveries->all();
        $countDelivery = 1;
        foreach ($deliveries as $delivery) {
            $query = static::prepareQuery($delivery->id);

            echo "Delivery ID: " . $delivery->id . " count: " . $countDelivery . PHP_EOL;

            $records = static::getRecordsFromQuery($query, $delivery);

            echo "Count records: " . count($records) . PHP_EOL;

            echo "saveRecordsToTable Start" . PHP_EOL;

            ReportDeliveryDebts::deleteAll([
                'is_old_data' => 0,
                'delivery_id' => $delivery->id
            ]);

            static::saveRecordsToTable($records);

            ReportDeliveryDebts::updateAll([
                'created_at' => time(),
                'is_old_data' => 0
            ], ['created_at' => null]);

            echo "saveRecordsToTable Done" . PHP_EOL;

            $countDelivery++;
        }
        echo "Run ReportDeliveryDebts::updateAll" . PHP_EOL;
    }

    /**
     * @param array $records
     */
    protected static function saveRecordsToTable($records)
    {
        if ($records) {
            $records = array_map(function ($item) {
                return [
                    'delivery_id' => $item['delivery_id'],
                    'month' => $item['month'],
                    'data' => json_encode($item, JSON_UNESCAPED_UNICODE),
                ];
            }, $records);

            ReportDeliveryDebts::getDb()->createCommand()->batchInsert(ReportDeliveryDebts::tableName(), [
                'delivery_id',
                'month',
                'data'
            ], $records)->execute();
        }
    }

    public function getTotalCounter()
    {
        $all = ReportDeliveryDebts::find()
            ->joinWith('delivery.country', false)
            ->where([
                'is_partner' => 0,
                'is_distributor' => 0,
            ])
            ->andWhere([
                'is not',
                ReportDeliveryDebts::tableName() . '.created_at',
                null
            ])
            ->andWhere(['<=', ReportDeliveryDebts::tableName() . '.month', static::REPORT_END_TO])
            ->all();

        $totalCounterCurrent = 0;
        $totalCounter = 0;
        foreach ($all as $item) {
            /** @var ReportDeliveryDebts $item */

            $totalCounter += $item->decodedData['buyout_sum_total_dollars'] - $item->decodedData['charges_not_accepted_dollars'];
            if ($item->month >= static::DATE_FROM_DEBTS) {
                $totalCounterCurrent += $item->decodedData['buyout_sum_total_dollars'] - $item->decodedData['charges_not_accepted_dollars'];
            }
        }

        return [
            'current' => $totalCounterCurrent,
            'all' => $totalCounter,
        ];
    }

//    TODO: переделать бы это нормально, то бесполезная хрень получилась! Каунты нужно отдельно вытащить, наверное
    /**
     * @return array
     */
    static public function getDiffFields()
    {
        return array_merge(
            array_unique(array_merge(OrderFinanceBalanceUsd::getPriceFields(), OrderFinanceFactUsd::getPriceFields())),
            array_map(function ($val) {return $val . '_count';}, array_unique(OrderFinanceBalanceUsd::getPriceFields() + OrderFinanceFactUsd::getPriceFields())),
            ['expenses', 'expenses_count']
        );
    }

    /**
     * @return array
     */
    static public function getFactFields()
    {
        return array_merge(
            array_map(function ($val) {return $val . '_fact';}, OrderFinanceFactUsd::getPriceFields()),
            array_map(function ($val) {return $val . '_count_fact';}, OrderFinanceFactUsd::getPriceFields()),
            ['expenses_fact', 'expenses_count_fact']
        );
    }

    /**
     * @return array
     */
    static public function getBalanceFields()
    {
        return array_merge(
            array_map(function ($val) {return $val . '_balance';}, OrderFinanceBalanceUsd::getPriceFields()),
            array_map(function ($val) {return $val . '_count_balance';}, OrderFinanceBalanceUsd::getPriceFields()),
            ['expenses_balance', 'expenses_count_balance']
        );
    }

    /**
     * @return array
     */
    static public function getPopupFields()
    {
        return array_merge(
            static::getBalanceFields(),
            static::getFactFields(),
            static::getDiffFields()
        );
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'price_cod' => Yii::t('common', 'Сумма COD'),
            'price_storage' => Yii::t('common', 'Сумма за хранение'),
            'price_fulfilment' => Yii::t('common', 'Сумма за обслуживание заказов'),
            'price_packing' => Yii::t('common', 'Сумма за упаковывание'),
            'price_package' => Yii::t('common', 'Сумма за упаковки'),
            'price_address_correction' => Yii::t('common', 'Сумма за корректировки адреса'),
            'price_delivery' => Yii::t('common', 'Сумма за доставки'),
            'price_redelivery' => Yii::t('common', 'Сумма за передоставки'),
            'price_delivery_back' => Yii::t('common', 'Сумма за возвращения'),
            'price_delivery_return' => Yii::t('common', 'Сумма за возвраты'),
            'price_cod_service' => Yii::t('common', 'Сумма наложенного платежа'),
            'price_vat' => Yii::t('common', 'Сумма НДС'),
            'expenses' => Yii::t('common', 'Издержки'),
        ]);
    }
}
