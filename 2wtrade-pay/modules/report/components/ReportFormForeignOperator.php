<?php

namespace app\modules\report\components;

use app\models\Currency;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\report\extensions\Query;
use app\modules\report\extensions\DataProvider;
use app\modules\report\components\filters\DailyFilterDefaultLastMonth;
use app\models\Country;
use app\models\CurrencyRate;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class ReportFormForeignOperator
 * @package app\modules\report\components
 */
class ReportFormForeignOperator extends ReportForm
{
    /**
     * @var string Группировка по умолчанию
     */
    public $groupBy = self::GROUP_BY_LAST_FOREIGN_OPERATOR;

    /**
     * @var array Маппинг статусов
     */

    protected $mapStatuses = [];
    /**
     * @inheritdoc
     * @var array $oper_activites
     */

    /*
     * "Галочка" конвертировать в доллар всегда отмечена
     * @var string
     */
    public $dollar = 'on';

    /**
     * @var DailyFilterDefaultLastMonth
     */
    protected $dailyFilter;

    /**
     * Установка ID страны для внешнего источника
     * @var bool
     */
    public $ext_country_id = false;


    public function init()
    {
        parent::init();
        $this->groupByCollection[self::GROUP_BY_LAST_FOREIGN_OPERATOR] = Yii::t('common', 'Внешний оператор');
    }

    /**
     * @param array $params
     * @return DataProvider
     */
    public function apply($params)
    {
        $this->load($params);
        $this->prepareCurrencyRate();

        $this->query = new Query();
        $this->query->from([Order::tableName()]);
        $this->buildSelect()->applyFilters($params);
        $this->query->andWhere(['not', [CallCenterRequest::tableName() . '.last_foreign_operator' => null]]);
        $this->query->andWhere(['not', [CallCenterRequest::tableName() . '.last_foreign_operator' => [0, -1]]]);

        $this->query->groupBy([CallCenterRequest::tableName() . '.last_foreign_operator']);
        $this->query->orderBy(['sum' => SORT_DESC]);

        $dataProvider = new DataProvider([
            'form' => $this,
            'query' => $this->query,
        ]);
        return $dataProvider;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $output = '';

        return $output;
    }

    /**
     * @return array
     */
    public function getMapStatuses()
    {
        if (empty($this->mapStatuses)) {
            $this->mapStatuses = [
                Yii::t('common', 'Подтверждено') => OrderStatus::getApproveList(),
                Yii::t('common', 'Выкуплено') => OrderStatus::getBuyoutList(),
                Yii::t('common', 'В процессе') => [
                    OrderStatus::STATUS_CC_APPROVED,
                    OrderStatus::STATUS_LOG_ACCEPTED,
                    OrderStatus::STATUS_LOG_GENERATED,
                    OrderStatus::STATUS_LOG_SET,
                    OrderStatus::STATUS_LOG_PASTED,
                    OrderStatus::STATUS_DELIVERY_PENDING,
                ],
                Yii::t('common', 'Треш') => [
                    OrderStatus::STATUS_CC_TRASH
                ],
                Yii::t('common', 'Дубль') => [
                    OrderStatus::STATUS_CC_DOUBLE
                ],
                Yii::t('common', 'Недозвон') => [
                    OrderStatus::STATUS_CC_FAIL_CALL
                ],
                Yii::t('common', 'В курьерке') => [
                    OrderStatus::STATUS_DELIVERY_ACCEPTED,
                    OrderStatus::STATUS_DELIVERY_LEFT_WAREHOUSE,
                    OrderStatus::STATUS_DELIVERY,
                    OrderStatus::STATUS_DELIVERY_LEFT_OUR_WAREHOUSE,
                    OrderStatus::STATUS_DELIVERY_REDELIVERY,
                    OrderStatus::STATUS_DELIVERY_LEFT_THEIR_WAREHOUSE,
                    OrderStatus::STATUS_DELIVERY_DELAYED,
                ],
                Yii::t('common', 'Всего') => OrderStatus::getAllList(),
            ];
        }

        return $this->mapStatuses;
    }

    /**
     * @inheritdoc
     */
    protected function buildSelect()
    {
        $this->getMapStatuses();

        $this->query->leftJoin(Country::tableName(),
            Order::tableName() . '.country_id=' . Country::tableName() . '.id');

        $convertPriceTotalToCurrencyId = Country::tableName() . '.currency_id';
        if ($this->dollar) {
            $usdCurrency = Currency::getUSD();
            $convertPriceTotalToCurrencyId = $usdCurrency->id;
        }

        $case = $this->query->buildCaseCondition(
            'convertToCurrencyByCountry(' . Order::tableName() . '.price_total, ' . Order::tableName() . '.price_currency,' . $convertPriceTotalToCurrencyId . ')',
            Order::tableName() . '.status_id',
            OrderStatus::getApproveList()
        );

        $case_buy = $this->query->buildCaseCondition(
            'convertToCurrencyByCountry(' . Order::tableName() . '.price_total + ' . Order::tableName() . '.delivery, ' . Order::tableName() . '.price_currency,' . $convertPriceTotalToCurrencyId . ')',
            Order::tableName() . '.status_id',
            OrderStatus::getBuyoutList()
        );

        $case_sum = $this->query->buildCaseCondition(
            'convertToCurrencyByCountry(' . Order::tableName() . '.price_total, ' . Order::tableName() . '.price_currency,' . $convertPriceTotalToCurrencyId . ')',
            Order::tableName() . '.status_id',
            OrderStatus::getBuyoutList()
        );

        $alterVikupProcent = '((COUNT(`order`.status_id IN(' . join(', ', OrderStatus::getBuyoutList()) . ') OR NULL) / COUNT(`order`.status_id IN(' . join(', ', OrderStatus::getApproveList()) . ') OR NULL)) * 100)';

        $this->query->select([
            'last_foreign_operator' => CallCenterRequest::tableName() . '.last_foreign_operator',
            'id' => CallCenterRequest::tableName() . '.last_foreign_operator',
            'count_lead' => 'COUNT(' . Order::tableName() . '.id)',
            'vikup_alter_procent' => $alterVikupProcent,
            'sum' => 'SUM(' . (string)$case_sum . ')',
            'sr_cek_kc' => 'AVG(' . (string)$case . ')',
            'sr_cek_buy' => 'AVG(' . (string)$case_buy . ')',
            'working_days' => 'count(distinct DATE_FORMAT(FROM_UNIXTIME(' . CallCenterRequest::tableName() . '.created_at), "%Y-%m-%d"))',
        ]);

        $this->buildSelectMapStatuses();

        return $this;
    }

    /**
     * @param array $params
     * @return ReportForm
     */
    protected function applyFilters($params)
    {
        $this->getDailyFilter()->apply($this->query, $params);
        $this->getProductFilter()->apply($this->query, $params);
        $this->getCallCenterFilter()->apply($this->query, $params);
        $this->getDeliveryFilter()->apply($this->query, $params);
        $this->getDeliveryPartnerFilter()->apply($this->query, $params);
        $this->getForeignOperatorFilter()->apply($this->query, $params);

        if (is_numeric($this->ext_country_id)) {
            $this->getCountryFilter()->ext_country_id = $this->ext_country_id;
        }
        $this->applyBaseFilters($params);

        return $this;
    }

    /**
     * @return DailyFilterDefaultLastMonth
     */
    public function getDailyFilter()
    {
        if (is_null($this->dailyFilter)) {
            $this->dailyFilter = new DailyFilterDefaultLastMonth();
            $this->dailyFilter->type = DailyFilterDefaultLastMonth::TYPE_ORDER_CREATED_AT;
        }

        return $this->dailyFilter;
    }

    /**
     * &param string $date
     * @return array
     */
    public function getWorkingDays($date)
    {
        $date = date("Y-m-d", strtotime($date));
        $working_days = CallCenterRequest::find()
            ->select([
                'working_days' => 'DATEDIFF("' . $date . '",  DATE_FORMAT(FROM_UNIXTIME(MIN(' . CallCenterRequest::tableName() . '.created_at)), "%Y-%m-%d"))',
                'last_foreign_operator'
            ])
            ->where(['not', [CallCenterRequest::tableName() . '.last_foreign_operator' => null]])
            ->where(['not', [CallCenterRequest::tableName() . '.last_foreign_operator' => [0, -1]]])
            ->groupBy('last_foreign_operator')
            ->asArray()
            ->all();

        return ArrayHelper::map($working_days, "last_foreign_operator", "working_days");
    }
}
