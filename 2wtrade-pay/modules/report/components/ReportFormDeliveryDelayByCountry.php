<?php

namespace app\modules\report\components;

use app\models\Country;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryApiClass;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\report\components\filters\CountrySelectFilter;
use app\modules\report\components\filters\CuratorUserFilter;
use app\modules\report\extensions\Query;
use yii\data\ArrayDataProvider;

/**
 * Class ReportFormDeliveryDelayByCountry
 * @package app\modules\report\components
 */
class ReportFormDeliveryDelayByCountry extends ReportForm
{
    /**
     * @var $all_countries
     */
    public $all_countries;

    /**
     * @var CountrySelectFilter
     */
    protected $countrySelectFilter;

    /**
     * @var curatorUserFilter
     */
    protected $curatorUserFilter;

    /**
     * @var array Маппинг статусов
     */
    protected $mapStatuses = [];

    /**
     * @param array $params
     * @return ArrayDataProvider
     */
    public function apply($params)
    {
        $this->load($params);

        $start_day = strtotime('now 00:00:00');

        $this->query = new Query();
        $this->query->from(DeliveryRequest::tableName());
        $this->query->leftJoin(Delivery::tableName(), DeliveryRequest::tableName() . '.delivery_id=' . Delivery::tableName() . '.id');
        $this->query->leftJoin(DeliveryApiClass::tableName(), Delivery::tableName() . '.api_class_id=' . DeliveryApiClass::tableName() . '.id');
        $this->query->leftJoin(Country::tableName(), Delivery::tableName() . '.country_id=' . Country::tableName() . '.id');
        $this->query->where([Delivery::tableName() . '.can_tracking' => 1]);
        $this->query->andWhere([DeliveryApiClass::tableName() . '.can_tracking' => 1]);
        $this->query->andWhere(["OR",
            ["LIKE", DeliveryRequest::tableName() . ".status", "in_progress"],
            ["AND",
                ["LIKE", DeliveryRequest::tableName() . ".status", "done"],
                [">=", DeliveryRequest::tableName() . '.cron_launched_at', $start_day],
                ["<=", DeliveryRequest::tableName() . '.cron_launched_at', time()]
            ]
        ]);

        $this->buildSelect()->applyFilters($params);

        $dataArray = $this->query->all();

        $dataProvider = new ArrayDataProvider([
            'allModels' => $dataArray,
            'pagination' => false,
        ]);

        return $dataProvider;
    }

    /**
     * @inheritdoc
     */
    protected function buildSelect()
    {
        $this->query->select([
            'rep_country_id' => Delivery::tableName() . '.country_id',
            'country_name' => Country::tableName() . '.name',
            'yearcreatedat' => "DATE_FORMAT(FROM_UNIXTIME(" . DeliveryRequest::tableName() . ".cron_launched_at), \"%Y\")",
            'monthcreatedat' => "DATE_FORMAT(FROM_UNIXTIME(" . DeliveryRequest::tableName() . ".cron_launched_at), \"%m\")",
            'weekcreatedat' => "DATE_FORMAT(FROM_UNIXTIME(" . DeliveryRequest::tableName() . ".cron_launched_at), \"%u\")",
            'daycreatedat' => "DATE_FORMAT(FROM_UNIXTIME(" . DeliveryRequest::tableName() . ".cron_launched_at), \"%d\")",
            'rep_delivery_id' => DeliveryRequest::tableName() . '.delivery_id',
            'delivery_name' => Delivery::tableName() . '.name',
            'order_count' => "COUNT(" . DeliveryRequest::tableName() . ".`order_id`)",
        ]);

        $this->query->groupBy(['rep_country_id', 'yearcreatedat', 'monthcreatedat', 'daycreatedat', 'rep_delivery_id']);

        return $this;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $content = '';
        return $content;
    }

    /**
     * @param array $params
     * @return bool
     */
    public function applyFilters($params)
    {
        $this->getCountrySelectFilter()->apply($this->query, $params);
        $this->getCuratorUserFilter()->apply($this->query, $params);

        return true;
    }

    /**
     * @return CountrySelectFilter
     */
    public function getCountrySelectFilter()
    {
        if (is_null($this->countrySelectFilter)) {
            $this->countrySelectFilter = new CountrySelectFilter();
        }
        return $this->countrySelectFilter;
    }

    /**
     * @return CuratorUserFilter
     */
    public function getCuratorUserFilter()
    {
        if (is_null($this->curatorUserFilter)) {
            $this->curatorUserFilter = new CuratorUserFilter();
        }
        return $this->curatorUserFilter;
    }

}
