<?php

namespace app\modules\report\components;

use app\modules\api\models\LeadStatus;
use app\modules\order\models\Order;
use app\modules\report\helpers\AdcomboApi;
use app\modules\report\models\AdcomboRequest;
use app\modules\report\models\ReportAdcombo;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class ReportAdcomboComparator
 * @package app\modules\report\components
 */
class ReportAdcomboComparator
{
    /**
     * @param $record_id
     * @return array
     */
    public static function getCompareErrors($record_id)
    {
        $errors = [];
        $model = ReportAdcombo::findOne($record_id);

        $query = AdcomboRequest::find()->where(['country_code' => Yii::$app->user->country->char_code]);
        $query->andWhere(['>=', 'created_at', $model->from]);
        $query->andWhere(['<=', 'created_at', $model->to]);

        $adcomboOrders = $query->all();

        $foreignIds = ArrayHelper::getColumn($adcomboOrders, 'foreign_id');

        $query = Order::find()->select(['id', 'foreign_id', 'status_id'])->where([
            '>=',
            'created_at',
            $model->from
        ])->andWhere(['<=', 'created_at', $model->to])->andWhere([
            '!=',
            'foreign_id',
            ''
        ])->byCountryId($model->country_id);

        $notFindAdcomboOrder = [];
        foreach ($query->batch(500) as $orders) {
            foreach ($orders as $order) {
                $index = array_search($order->foreign_id, $foreignIds);
                if ($index === false) {
                    $notFindAdcomboOrder[$order->foreign_id] = [
                        'order_id' => $order->id,
                        'foreign_id' => $order->foreign_id,
                        'type' => ReportAdcombo::ERROR_ADCOMBO_ORDER_NOT_FOUND,
                        'order_state' => LeadStatus::$mapOrderStatusToLeadStatus[$order->status_id]
                    ];
                    continue;
                }
                if (LeadStatus::$mapOrderStatusToLeadStatus[$order->status_id] != $adcomboOrders[$index]->state) {
                    $errors[] = [
                        'order_id' => $order->id,
                        'foreign_id' => $order->foreign_id,
                        'type' => ReportAdcombo::ERROR_STATUS_NOT_EQUAL,
                        'foreign_state' => $adcomboOrders[$index]->state,
                        'order_state' => LeadStatus::$mapOrderStatusToLeadStatus[$order->status_id]
                    ];
                }
                unset($foreignIds[$index]);
            }
        }

        $offset = 0;
        $totalCount = count($foreignIds);
        $batchSize = 200;

        while ($offset < $totalCount) {
            $batch = array_slice($foreignIds, $offset, $batchSize);
            $offset += $batchSize;

            $orders = Order::find()->where(['foreign_id' => $batch])->with('callCenterRequest')->all();

            foreach ($orders as $order) {
                $index = array_search($order->foreign_id, $foreignIds);
                if (LeadStatus::$mapOrderStatusToLeadStatus[$order->status_id] != $adcomboOrders[$index]->state) {
                    $errors[] = [
                        'order_id' => $order->id,
                        'foreign_id' => $order->foreign_id,
                        'type' => ReportAdcombo::ERROR_STATUS_NOT_EQUAL,
                        'foreign_state' => $adcomboOrders[$index]->state,
                        'order_state' => LeadStatus::$mapOrderStatusToLeadStatus[$order->status_id]
                    ];
                }
                unset($foreignIds[$index]);
            }
        }

        foreach ($foreignIds as $key => $item) {
            $errors[] = [
                'foreign_id' => $adcomboOrders[$key]->foreign_id,
                'type' => ReportAdcombo::ERROR_OUR_ORDER_NOT_FOUND,
                'foreign_state' => $adcomboOrders[$key]->state,
            ];
        }

        $ourAdcomboOrders = AdcomboRequest::find()->where(['foreign_id' => array_keys($notFindAdcomboOrder)])->all();

        foreach ($ourAdcomboOrders as $item) {
            unset($notFindAdcomboOrder[$item->foreign_id]);
        }

        foreach ($notFindAdcomboOrder as $item) {
            $errors[] = $item;
        }

        return $errors;
    }

    /**
     * @param ReportAdcombo $record
     * @throws \Exception
     */
    public static function compareWithAdcombo($record)
    {
        $adcomboOrders = AdcomboApi::query([], $record->from, $record->to, '', $record->country->char_code);

        $record->difference = '';
        $record->hold_count = $record->our_hold_count = 0;
        $record->confirmed_count = $record->our_confirmed_count = 0;
        $record->trash_count = $record->our_trash_count = 0;
        $record->cancelled_count = $record->our_cancelled_count = 0;
        $record->difference_count = 0;
        $record->cc_count = 0;
        $record->our_total_count = 0;
        $record->adcombo_total_count = count($adcomboOrders);

        $query = Order::find()->select(['id', 'foreign_id', 'status_id'])->where([
            '>=',
            'created_at',
            $record->from
        ])->andWhere(['<=', 'created_at', $record->to])->andWhere([
            '!=',
            'foreign_id',
            ''
        ])->byCountryId($record->country->id);

        $foreignIds = [];
        $offset = 0;
        $totalCount = $record->adcombo_total_count;
        $batchSize = 200;
        while ($offset < $totalCount) {
            $batch = array_slice($adcomboOrders, $offset, $batchSize, true);
            $offset += $batchSize;
            $adcomboRequests = AdcomboRequest::find()->where([
                'foreign_id' => ArrayHelper::getColumn($batch, 'foreign_id')
            ])->all();
            $adcomboRequests = ArrayHelper::index($adcomboRequests, 'foreign_id');
            $transaction = Yii::$app->db->beginTransaction();
            try {
                foreach ($batch as $key => $item) {
                    switch ($item->state) {
                        case LeadStatus::STATUS_HOLD:
                            $record->hold_count++;
                            break;
                        case LeadStatus::STATUS_CONFIRMED:
                            $record->confirmed_count++;
                            break;
                        case LeadStatus::STATUS_TRASH:
                            $record->trash_count++;
                            break;
                        case LeadStatus::STATUS_CANCELLED:
                            $record->cancelled_count++;
                            break;
                    }
                    $foreignIds[$key] = $item->foreign_id;

                    if (array_key_exists($item->foreign_id, $adcomboRequests)) {
                        $adcomboRequest = $adcomboRequests[$item->foreign_id];
                    } else {
                        $adcomboRequest = new AdcomboRequest();
                        $adcomboRequest->foreign_id = $item->foreign_id;
                    }
                    $updated = false;
                    foreach ($adcomboRequest->attributes as $name => $value) {
                        if ($value != $item->$name) {
                            $updated = true;
                            break;
                        }
                    }
                    if ($updated) {
                        $adcomboRequest->country_code = $item->country_code;
                        $adcomboRequest->name = $item->name;
                        $adcomboRequest->email = $item->email;
                        $adcomboRequest->phone = $item->phone;
                        $adcomboRequest->created_at = $item->created_at;
                        $adcomboRequest->state = $item->state;
                        $adcomboRequest->save();
                    }
                }
                $transaction->commit();
            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            }
        }

        $errors = [];

        $notFindAdcomboOrder = [];

        foreach ($query->batch() as $orders) {
            foreach ($orders as $order) {
                $index = array_search($order->foreign_id, $foreignIds);
                if ($index === false) {
                    $notFindAdcomboOrder[$order->foreign_id] = [
                        'order_id' => $order->id,
                        'foreign_id' => $order->foreign_id,
                        'type' => ReportAdcombo::ERROR_ADCOMBO_ORDER_NOT_FOUND,
                        'order_state' => LeadStatus::$mapOrderStatusToLeadStatus[$order->status_id],
                        'has_cc' => !is_null($order->callCenterRequest)
                    ];
                    continue;
                }

                $record->our_total_count++;

                if ($order->callCenterRequest) {
                    $record->cc_count++;
                }
                switch (LeadStatus::$mapOrderStatusToLeadStatus[$order->status_id]) {
                    case LeadStatus::STATUS_HOLD:
                        $record->our_hold_count++;
                        break;
                    case LeadStatus::STATUS_CONFIRMED:
                        $record->our_confirmed_count++;
                        break;
                    case LeadStatus::STATUS_TRASH:
                        $record->our_trash_count++;
                        break;
                    case LeadStatus::STATUS_CANCELLED:
                        $record->our_cancelled_count++;
                        break;
                }

                if (LeadStatus::$mapOrderStatusToLeadStatus[$order->status_id] != $adcomboOrders[$index]->state) {
                    $errors[] = [
                        'order_id' => $order->id,
                        'foreign_id' => $order->foreign_id,
                        'type' => ReportAdcombo::ERROR_STATUS_NOT_EQUAL,
                        'foreign_state' => $adcomboOrders[$index]->state,
                        'order_state' => LeadStatus::$mapOrderStatusToLeadStatus[$order->status_id]
                    ];
                    $record->difference_count++;
                }
                unset($foreignIds[$index]);
            }
        }

        $offset = 0;
        $totalCount = count($foreignIds);

        while ($offset < $totalCount) {
            $batch = array_slice($foreignIds, $offset, $batchSize);
            $offset += $batchSize;

            $ids = $batch;

            $orders = Order::find()->where(['foreign_id' => $ids])->with('callCenterRequest')->all();

            foreach ($orders as $order) {
                $record->our_total_count++;
                if ($order->callCenterRequest) {
                    $record->cc_count++;
                }

                switch (LeadStatus::$mapOrderStatusToLeadStatus[$order->status_id]) {
                    case LeadStatus::STATUS_HOLD:
                        $record->our_hold_count++;
                        break;
                    case LeadStatus::STATUS_CONFIRMED:
                        $record->our_confirmed_count++;
                        break;
                    case LeadStatus::STATUS_TRASH:
                        $record->our_trash_count++;
                        break;
                    case LeadStatus::STATUS_CANCELLED:
                        $record->our_cancelled_count++;
                        break;
                }

                $index = array_search($order->foreign_id, $foreignIds);
                if (LeadStatus::$mapOrderStatusToLeadStatus[$order->status_id] != $adcomboOrders[$index]->state) {
                    $errors[] = [
                        'order_id' => $order->id,
                        'foreign_id' => $order->foreign_id,
                        'type' => ReportAdcombo::ERROR_STATUS_NOT_EQUAL,
                        'foreign_state' => $adcomboOrders[$index]->state,
                        'order_state' => LeadStatus::$mapOrderStatusToLeadStatus[$order->status_id]
                    ];
                    $record->difference_count++;
                }
                unset($foreignIds[$index]);
            }

        }

        $ourAdcomboOrders = AdcomboRequest::find()->where(['foreign_id' => array_keys($notFindAdcomboOrder)])->all();

        foreach ($ourAdcomboOrders as $item) {
            unset($notFindAdcomboOrder[$item->foreign_id]);
        }

        foreach ($notFindAdcomboOrder as $item) {
            $record->our_total_count++;
            if ($item['has_cc']) {
                $record->cc_count++;
            }
            switch ($item['order_state']) {
                case LeadStatus::STATUS_HOLD:
                    $record->our_hold_count++;
                    break;
                case LeadStatus::STATUS_CONFIRMED:
                    $record->our_confirmed_count++;
                    break;
                case LeadStatus::STATUS_TRASH:
                    $record->our_trash_count++;
                    break;
                case LeadStatus::STATUS_CANCELLED:
                    $record->our_cancelled_count++;
                    break;
            }
            $errors[] = $item;
        }

        $notFindAdcomboOrderCount = count($notFindAdcomboOrder);

        foreach ($foreignIds as $key => $item) {
            $errors[] = [
                'foreign_id' => $adcomboOrders[$key]->foreign_id,
                'type' => ReportAdcombo::ERROR_OUR_ORDER_NOT_FOUND,
                'foreign_state' => $adcomboOrders[$key]->state,
            ];
        }

        $difference = [];

        $difference[ReportAdcombo::SHOW_ERROR_TYPE_TOTAL_ADCOMBO] = count($foreignIds);

        $difference[ReportAdcombo::SHOW_ERROR_TYPE_TOTAL_OUR] = $notFindAdcomboOrderCount;

        $difference[ReportAdcombo::SHOW_ERROR_TYPE_CONFIRMED_ADCOMBO] = self::getAdcomboFilteredErrorCount($errors,
            LeadStatus::STATUS_CONFIRMED);
        $difference[ReportAdcombo::SHOW_ERROR_TYPE_HOLD_ADCOMBO] = self::getAdcomboFilteredErrorCount($errors,
            LeadStatus::STATUS_HOLD);
        $difference[ReportAdcombo::SHOW_ERROR_TYPE_TRASH_ADCOMBO] = self::getAdcomboFilteredErrorCount($errors,
            LeadStatus::STATUS_TRASH);
        $difference[ReportAdcombo::SHOW_ERROR_TYPE_CANCELLED_ADCOMBO] = self::getAdcomboFilteredErrorCount($errors,
            LeadStatus::STATUS_CANCELLED);
        $difference[ReportAdcombo::SHOW_ERROR_TYPE_CONFIRMED_OUR] = self::getOurFilteredErrorCount($errors,
            LeadStatus::STATUS_CONFIRMED);
        $difference[ReportAdcombo::SHOW_ERROR_TYPE_HOLD_OUR] = self::getOurFilteredErrorCount($errors,
            LeadStatus::STATUS_HOLD);
        $difference[ReportAdcombo::SHOW_ERROR_TYPE_TRASH_OUR] = self::getOurFilteredErrorCount($errors,
            LeadStatus::STATUS_TRASH);
        $difference[ReportAdcombo::SHOW_ERROR_TYPE_CANCELLED_OUR] = self::getOurFilteredErrorCount($errors,
            LeadStatus::STATUS_CANCELLED);

        $record->difference = json_encode($difference);
    }

    /**
     * @param $errors
     * @param $state
     * @return int
     */
    protected static function getAdcomboFilteredErrorCount($errors, $state)
    {
        $elements = array_filter($errors, function ($item) use ($state) {
            return ($item['type'] == ReportAdcombo::ERROR_OUR_ORDER_NOT_FOUND || $item['type'] == ReportAdcombo::ERROR_STATUS_NOT_EQUAL) && $item['foreign_state'] == $state;
        });

        return count($elements);
    }

    /**
     * @param $errors
     * @param $state
     * @return int
     */
    protected static function getOurFilteredErrorCount($errors, $state)
    {
        $elements = array_filter($errors, function ($item) use ($state) {
            return ($item['type'] == ReportAdcombo::ERROR_ADCOMBO_ORDER_NOT_FOUND || $item['type'] == ReportAdcombo::ERROR_STATUS_NOT_EQUAL) && $item['order_state'] == $state;
        });

        return count($elements);
    }
}
