<?php

namespace app\modules\report\components;

use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use app\modules\order\models\OrderProduct;
use app\modules\delivery\models\Delivery;
use app\models\Country;
use app\models\Product;
use app\modules\report\components\filters\ReportShipmentFilter;
use yii\data\ArrayDataProvider;

/**
 * Class ReportFormShipment
 * @package app\modules\report\components
 */
class ReportFormShipment extends ReportForm
{
    /**
     * @var ReportShipmentFilter
     */
    protected $shipmentFilter;

    /**
     * @param array $params
     * @return ArrayDataProvider
     */
    public function apply($params)
    {
        $statusArray = [
            OrderStatus::STATUS_LOG_GENERATED,
            OrderStatus::STATUS_DELIVERY_ACCEPTED,
            OrderStatus::STATUS_DELIVERY,
            OrderStatus::STATUS_DELIVERY_BUYOUT,
            OrderStatus::STATUS_DELIVERY_DENIAL,
            OrderStatus::STATUS_DELIVERY_RETURNED,
            OrderStatus::STATUS_FINANCE_MONEY_RECEIVED,
            OrderStatus::STATUS_DELIVERY_REDELIVERY,
            OrderStatus::STATUS_DELIVERY_LEFT_THEIR_WAREHOUSE,
            OrderStatus::STATUS_DELIVERY_DELAYED,
            OrderStatus::STATUS_DELIVERY_REFUND,
            OrderStatus::STATUS_LOGISTICS_ACCEPTED,
        ];

        $this->query = Order::find()
            ->joinWith([
                'country',
                'deliveryRequest',
                'products',
                'deliveryRequest.delivery'
            ])
            ->select([
                'country_id' => Country::tableName() . '.id',
                'country_name' => Country::tableName() . '.name',
                'delivery_id' => Delivery::tableName() . '.id',
                'delivery_name' => Delivery::tableName() . '.name',
                'id' => 'CONCAT(' . Country::tableName() . '.id, ' . Delivery::tableName() . '.id, ' . Product::tableName() . '.id)',
                'product_name' => Product::tableName() . '.name',
                'count_product' => 'SUM(' . OrderProduct::tableName() . '.quantity)',
            ])
            ->where(['IN', Order::tableName() . '.status_id', $statusArray]);
        $this->applyFilters($params);
        $this->query->groupBy([Country::tableName() . '.id', Delivery::tableName() . '.id', Product::tableName() . '.id']);
        $this->query->orderBy('country_name, delivery_name, product_name');
        $data = $this->query->asArray()->all();

        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
        ]);

        return $dataProvider;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return '';
    }

    /**
     * @return ReportShipmentFilter
     */
    public function getShipmentFilter()
    {
        if (is_null($this->shipmentFilter)) {
            $this->shipmentFilter = new ReportShipmentFilter();
        }

        return $this->shipmentFilter;
    }

    /**
     * @param array $params
     * @return $this
     */
    protected function applyFilters($params)
    {
        $this->getShipmentFilter()->apply($this->query, $params);
        $this->getCountryFilter()->apply($this->query, $params);

        return $this;
    }
}
