<?php
namespace app\modules\report\components;

use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use app\modules\report\models\SmsPollHistory;
use app\modules\report\extensions\DataProvider;
use app\modules\report\extensions\Query;
use Yii;

/**
 * Class ReportFormSmsPollHistory
 * @package app\modules\report\components
 */
class ReportFormSmsPollHistory extends ReportForm
{
    /**
     * @var string Группировка по умолчанию
     */
    public $groupBy = ReportForm::GROUPBY_SMS_CREATED_AT;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }

    public function getMapStatuses()
    {
        return [
            Yii::t('common', 'Все') => OrderStatus::getAllList()
        ];
    }

    /**
     * @param array $params
     * @return DataProvider
     */
    public function apply($params)
    {
        $this->load($params);


        $this->query = new Query();
        $this->query->from([SmsPollHistory::tableName()]);
        $this->query->select([
            'id' => SmsPollHistory::tableName() . '.id',
            'question_text' => SmsPollHistory::tableName() . '.question_text',
            'created_at' => SmsPollHistory::tableName() . '.created_at',
            'answered_at' => 'NULLIF(' . SmsPollHistory::tableName() . '.answered_at,0)',
            'answer_text' => SmsPollHistory::tableName() . '.answer_text',
            'order_id' => Order::tableName() . '.id',
            'order_created_at' => Order::tableName() . '.created_at',
            'status_id' => Order::tableName() . '.status_id',
            'api_error' => SmsPollHistory::tableName() . '.api_error',
            'customer_full_name' => Order::tableName() . '.customer_full_name',
            'customer_address' => Order::tableName() . '.customer_address',
            'customer_mobile' => Order::tableName() . '.customer_mobile',
        ]);
        $this->query->leftJoin(Order::tableName(), Order::tableName() . '.id=' . SmsPollHistory::tableName() . '.order_id');

        $this->buildSelect()->applyFilters($params);

        $dataProvider = new DataProvider([
            'form' => $this,
            'query' => $this->query
        ]);

        $dataProvider->pagination->pageSize = 25;

        return $dataProvider;

    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $output = '';

        return $output;
    }

    /**
     * @inheritdoc
     */
    protected function buildSelect()
    {
        return $this;
    }

    /**
     * @param array $params
     * @return ReportForm
     */
    protected function applyFilters($params)
    {
        $this->getDateSmsPollHistoryFilter()->apply($this->query, $params);
        $this->getAnswerTypeSmsPollHistoryFilter()->apply($this->query, $params);

        $this->applyBaseFilters($params);

        return $this;
    }
}
