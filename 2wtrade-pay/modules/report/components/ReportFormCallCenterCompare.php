<?php

namespace app\modules\report\components;

use app\modules\report\components\filters\AdcomboDateFilter;
use app\modules\report\components\filters\AdcomboTypeFilter;
use app\modules\report\models\ReportCallCenter;
use yii\data\ActiveDataProvider;
use Yii;

/**
 * Class ReportFormCallCenterCompare
 * @package app\modules\report\components
 */
class ReportFormCallCenterCompare extends ReportForm
{
    /**
     * @var AdcomboDateFilter
     */
    protected $dailyFilter;

    /**
     * @var AdcomboTypeFilter
     */
    protected $typeFilter;

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function apply($params)
    {
        $this->query = ReportCallCenter::find();

        $this->query->where([ReportCallCenter::tableName() . '.country_id' => Yii::$app->user->country->id]);

        $this->applyFilters($params);

        $this->query->orderBy(ReportCallCenter::tableName() . '.from desc');

        $dataProvider = new ActiveDataProvider([
            'query' => $this->query,
        ]);

        return $dataProvider;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $content = '';
        return $content;
    }

    /**
     * @param array $params
     * @return bool
     */
    public function applyFilters($params)
    {
        $this->getDailyFilter()->apply($this->query, $params);
        $this->getTypeFilter()->apply($this->query, $params);

        return true;
    }

    /**
     * @return AdcomboDateFilter
     */
    public function getDailyFilter()
    {
        if (is_null($this->dailyFilter)) {
            $this->dailyFilter = new AdcomboDateFilter();
            $this->dailyFilter->type = AdcomboDateFilter::TYPE_REPORT_CALL_CENTER;
        }
        return $this->dailyFilter;
    }

    /**
     * @return AdcomboTypeFilter
     */
    public function getTypeFilter()
    {
        if (is_null($this->typeFilter)) {
            $this->typeFilter = new AdcomboTypeFilter();
            $this->typeFilter->attribute = AdcomboTypeFilter::ATTR_CALL_CENTER_TYPE;
        }
        return $this->typeFilter;
    }
}
