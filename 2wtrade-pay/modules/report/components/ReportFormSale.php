<?php

namespace app\modules\report\components;

use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use app\modules\order\models\OrderProduct;
use app\modules\delivery\models\Delivery;
use app\models\Country;
use app\models\Product;
use app\modules\report\components\filters\DateFilter;
use app\modules\report\components\filters\DeliveryFilter;
use yii\data\ArrayDataProvider;

/**
 * Class ReportFormSale
 * @package app\modules\report\components
 */
class ReportFormSale extends ReportForm
{
    /**
     * @param array $params
     * @return ArrayDataProvider
     */
    public function apply($params)
    {
        $this->getDateFilter()->type = DateFilter::TYPE_DELIVERY_APPROVED_AT;
        $this->getDeliveryFilter()->type = DeliveryFilter::TYPE_DELIVERY;

        $this->query = Order::find()
            ->joinWith([
                'country',
                'deliveryRequest',
                'callCenterRequest',
                'products',
                'deliveryRequest.delivery'
            ])
            ->select([
                'country_id' => Country::tableName() . '.id',
                'country_name' => Country::tableName() . '.name',
                'delivery_id' => Delivery::tableName() . '.id',
                'delivery_name' => Delivery::tableName() . '.name',
                'id' => 'CONCAT(' . Country::tableName() . '.id, ' . Delivery::tableName() . '.id, ' . Product::tableName() . '.id)',
                'product_name' => Product::tableName() . '.name',
                'count_product' => 'SUM(' . OrderProduct::tableName() . '.quantity)',
            ])
            ->where(['IN', Order::tableName() . '.status_id', OrderStatus::getBuyoutList()]);
        $this->applyFilters($params);
        $this->query->groupBy([Country::tableName() . '.id', Delivery::tableName() . '.id', Product::tableName() . '.id']);
        $this->query->orderBy('country_name, delivery_name, product_name');
        $data = $this->query->asArray()->all();

        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'pagination' => false
        ]);

        return $dataProvider;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return '';
    }

    /**
     * @param array $params
     * @return $this
     */
    protected function applyFilters($params)
    {
        $this->getDateFilter()->apply($this->query, $params);
        $this->getDeliveryFilter()->apply($this->query, $params);
        $this->getCountryFilter()->apply($this->query, $params);

        return $this;
    }
}
