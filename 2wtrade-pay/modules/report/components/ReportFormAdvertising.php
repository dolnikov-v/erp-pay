<?php
namespace app\modules\report\components;

use app\helpers\i18n\Formatter;
use app\helpers\Utils;
use app\models\Country;
use app\models\CurrencyRate;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use app\modules\report\models\ReportAdvertising;
use Yii;
use yii\base\Exception;
use yii\data\ArrayDataProvider;
use yii\web\UploadedFile;
use app\modules\report\components\filters\ReportAdvertisingFilter;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class ReportFormAdvertising
 * @package app\modules\report\components
 */
class ReportFormAdvertising extends ReportForm
{
    /**
     * @var UploadedFile
     */
    public $loadFile;

    /**
     * @var string
     */
    private $fileName;

    /**
     * @var array
     */
    private $fileTitle = ['COUNTRY CODE', 'LEAD ID', 'DATE CREATED', 'STATUS', 'PRICE'];

    /**
     * @var ReportAdvertisingFilter
     */
    protected $dateFilter;

    /**
     * @var array Карта статусов из загружаемого файла
     */
    private $mapStatuses = [
        OrderStatus::STATUS_CC_APPROVED => 300,
        OrderStatus::STATUS_LOG_ACCEPTED => 300,
        OrderStatus::STATUS_LOG_GENERATED => 300,
        OrderStatus::STATUS_LOG_SET => 300,
        OrderStatus::STATUS_LOG_PASTED => 300,
        OrderStatus::STATUS_DELIVERY_ACCEPTED => 300,
        OrderStatus::STATUS_DELIVERY_LEFT_WAREHOUSE => 300,
        OrderStatus::STATUS_DELIVERY => 300,
        OrderStatus::STATUS_DELIVERY_BUYOUT => 300,
        OrderStatus::STATUS_DELIVERY_DENIAL => 300,
        OrderStatus::STATUS_DELIVERY_RETURNED => 300,
        OrderStatus::STATUS_FINANCE_MONEY_RECEIVED => 300,
        OrderStatus::STATUS_DELIVERY_REJECTED => 300,
        OrderStatus::STATUS_DELIVERY_LEFT_OUR_WAREHOUSE => 300,
        OrderStatus::STATUS_DELIVERY_REDELIVERY => 300,
        OrderStatus::STATUS_DELIVERY_LEFT_THEIR_WAREHOUSE => 300,
        OrderStatus::STATUS_LOGISTICS_REJECTED => 300,
        OrderStatus::STATUS_DELIVERY_DELAYED => 300,
        OrderStatus::STATUS_DELIVERY_REFUND => 300,
        OrderStatus::STATUS_DELIVERY_LOST => 300,
        OrderStatus::STATUS_CC_INPUT_QUEUE => 300,
        OrderStatus::STATUS_DELIVERY_PENDING => 300,
    ];

    /**
     * @var array Карта стран из загружаемого файла
     */
    private $mapCountries = [
        'CL' => 216, // Чили
        'IN' => 121, // Индия
        'CO' => 171, // Колумбия
        'MX' => 37, // Мексика
        'KH' => 246, // Камбоджа
        'JP' => 271, // Япония
        'PK' => 261, // Пакистан
    ];

    /**
     * @param array $row
     * @return array
     */
    public static function check($row)
    {
        if ($row['error'] == 1) {
            return [
                'class' => 'error'
            ];
        }

        return [];
    }

    /**
     * Получить данные по странам за текущий месяц
     * @return ArrayDataProvider
     */
    public static function getDataCurrent()
    {
        $dataArray = Order::find()
            ->select([
                'summa' => 'sum(' . Order::tableName() . '.price)/' . CurrencyRate::tableName() . '.rate',
                'country_name' => Country::tableName() . '.name',
            ])
            ->leftJoin(Country::tableName(), Order::tableName() . '.country_id=' . Country::tableName() . '.id')
            ->leftJoin(CurrencyRate::tableName(), Country::tableName() . '.currency_id = ' . CurrencyRate::tableName() . '.currency_id')
            ->where(Order::tableName() . '.status_id in (' . join(', ', [
                    OrderStatus::STATUS_CC_APPROVED,
                    OrderStatus::STATUS_LOG_ACCEPTED,
                    OrderStatus::STATUS_LOG_GENERATED,
                    OrderStatus::STATUS_LOG_SET,
                    OrderStatus::STATUS_LOG_PASTED,
                    OrderStatus::STATUS_DELIVERY_ACCEPTED,
                    OrderStatus::STATUS_DELIVERY_LEFT_WAREHOUSE,
                    OrderStatus::STATUS_DELIVERY,
                    OrderStatus::STATUS_DELIVERY_BUYOUT,
                    OrderStatus::STATUS_DELIVERY_DENIAL,
                    OrderStatus::STATUS_DELIVERY_RETURNED,
                    OrderStatus::STATUS_FINANCE_MONEY_RECEIVED,
                    OrderStatus::STATUS_DELIVERY_REJECTED,
                    OrderStatus::STATUS_DELIVERY_LEFT_OUR_WAREHOUSE,
                    OrderStatus::STATUS_DELIVERY_REDELIVERY,
                    OrderStatus::STATUS_DELIVERY_LEFT_THEIR_WAREHOUSE,
                    OrderStatus::STATUS_LOGISTICS_REJECTED,
                    OrderStatus::STATUS_DELIVERY_DELAYED,
                    OrderStatus::STATUS_DELIVERY_REFUND,
                    OrderStatus::STATUS_DELIVERY_LOST,
                    OrderStatus::STATUS_CC_INPUT_QUEUE,
                    OrderStatus::STATUS_DELIVERY_PENDING,
                ]) . ')')
            ->groupBy(Country::tableName() . '.name')
            ->orderBy(Country::tableName() . '.name')
            ->asArray()
            ->all();

        $dataProvider = new ArrayDataProvider([
            'allModels' => $dataArray,
            'pagination' => false,
        ]);

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['loadFile'], 'file', 'checkExtensionByMimeType' => false, 'skipOnEmpty' => false, 'extensions' => 'xls, xlsx'],
        ];
    }

    /**
     * @param boolean $prepareDir
     * @return string
     */
    public function getUploadPath($prepareDir = false)
    {
        $dir = Yii::getAlias('@uploads') . DIRECTORY_SEPARATOR . 'advertising';

        if ($prepareDir) {
            Utils::prepareDir($dir);
        }

        return $dir;
    }


    /**
     * @return string|boolean
     */
    public function upload()
    {
        $this->loadFile = UploadedFile::getInstance($this, 'loadFile');

        if ($this->validate()) {
            $this->fileName = $this->getUploadPath(true) . DIRECTORY_SEPARATOR . time() . '.' . $this->loadFile->extension;
            return $this->loadFile->saveAs($this->fileName);
        } else {
            return false;
        }
    }

    /**
     * @return ArrayDataProvider
     * @throws Exception
     */
    public function parse()
    {
        try {
            $objPHPExcel = \PHPExcel_IOFactory::load($this->fileName);
            $objPHPExcel->setActiveSheetIndex(0);
            $aSheet = $objPHPExcel->getActiveSheet();
            $arr = [];
            $i = 0;

            foreach ($aSheet->getRowIterator() as $row) {
                $cellIterator = $row->getCellIterator();
                $item = [];

                foreach ($cellIterator as $cell) {
                    if ($i == 0) {
                        array_push($item, strtoupper($cell->getCalculatedValue()));
                    } else {
                        array_push($item, $cell->getCalculatedValue());
                    }
                }

                if ($i == 0 && $item !== $this->fileTitle) {
                    throw new Exception(Yii::t('common', 'Неверный формат заголовка файла'));
                }

                if ($i > 0) {
                    $arr[$item[1]] = $item;
                }

                unset($item);
                $i++;
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }

        unlink($this->fileName);

        return $this->apply($arr);
    }

    /**
     * @param array $params
     * @return ArrayDataProvider
     * @throws Exception
     */
    public function apply($params)
    {
        $this->query = Order::find()
            ->select([
                'foreign_id' => Order::tableName() . '.foreign_id',
                'country_id' => Country::tableName() . '.char_code',
                'created_at' => 'DATE_FORMAT(FROM_UNIXTIME((' . Order::tableName() . '.created_at)), "' . Formatter::getMysqlDateFormat() . '")',
                'status_id' => Order::tableName() . '.status_id',
                'price' => Order::tableName() . '.price',
            ]);
        $flag = true;
        foreach ($params as $param) {
            if ($flag) {
                $this->query->orFilterWhere(['=', Order::tableName() . '.foreign_id', $param[1]]);
                $flag = false;
            } else {
                $this->query->orFilterWhere(['=', Order::tableName() . '.foreign_id', $param[1]]);
            }
        }

        $this->query->leftJoin(Country::tableName(), Order::tableName() . '.country_id=' . Country::tableName() . '.id');

        $dataBaseArray = $this->query->asArray()->all();

        $dataArray = [];

        foreach ($params as $item) {
            $i = $this->inArray($dataBaseArray, $item[1]);

            if ($i !== false) {
                $statusId = '';
                if (isset($this->mapStatuses[$dataBaseArray[$i]['status_id']])) {
                    $statusId = $this->mapStatuses[$dataBaseArray[$i]['status_id']];
                }

                $countryId = '';

                if (isset($this->mapCountries[$dataBaseArray[$i]['country_id']])) {
                    $countryId = $this->mapCountries[$dataBaseArray[$i]['country_id']];
                }

                try {
                    $priceFile = (string)((integer)$item[4]);
                    $priceBase = (string)((integer)$dataBaseArray[$i]['price']);
                    $difference = $priceFile - $priceBase;

                    if ($priceFile != (string)$item[4] || $priceBase != (string)$dataBaseArray[$i]['price']) {
                        $difference = Yii::t('common', 'Не числовой формат');
                    }
                } catch (Exception $e) {
                    $difference = Yii::t('common', 'Не числовой формат');
                }

                $row = [
                    'foreign_id' => $dataBaseArray[$i]['foreign_id'],
                    'country_id' => $countryId,
                    'created_at' => $dataBaseArray[$i]['created_at'],
                    'status_id' => $statusId,
                    'price' => $dataBaseArray[$i]['price'],
                    'foreign_id_file' => $item[1],
                    'country_id_file' => $item[0],
                    'created_at_file' => $item[2],
                    'status_id_file' => $item[3],
                    'price_file' => $item[4],
                    'difference' => $difference,
                    'error' => '—',
                ];
            } else {
                $row = [
                    'foreign_id' => '—',
                    'country_id' => '—',
                    'created_at' => '—',
                    'status_id' => '—',
                    'price' => '—',
                    'foreign_id_file' => $item[1],
                    'country_id_file' => $item[0],
                    'created_at_file' => $item[2],
                    'status_id_file' => $item[3],
                    'price_file' => $item[4],
                    'difference' => '—',
                    'error' => '—',
                ];
            }

            if ($row['foreign_id'] == '—' ||
                $row['foreign_id'] != $row['foreign_id_file'] ||
                $row['country_id'] != $row['country_id_file'] ||
                $row['created_at'] != $row['created_at_file'] ||
                $row['status_id'] != $row['status_id_file'] ||
                $row['price'] != $row['price_file']
            ) {
                $row['error'] = 1;
            } else {
                $row['error'] = 0;
            }

            $dataArray[] = $row;
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $dataArray,
        ]);

        $errorMessage = $this->saveToReportAdvertising($dataArray);
        if ($errorMessage) {
            throw new Exception(Yii::t('common', $errorMessage));
        }

        return $dataProvider;
    }

    /**
     * @param array $dataArray
     * @return string
     */
    private function saveToReportAdvertising($dataArray)
    {
        $day = time();
        $transaction = \Yii::$app->db->beginTransaction();
        foreach ($dataArray as $item) {
            try {
                $report = new ReportAdvertising();
                $report->foreign_id = (string)$item['foreign_id'];
                $report->foreign_id_file = (string)$item['foreign_id_file'];
                $report->country_code = (string)$item['country_id'];
                $report->country_code_file = (string)$item['country_id_file'];
                $report->created_at = (string)$item['created_at'];
                $report->created_at_file = (string)$item['created_at_file'];
                $report->status_id = (string)$item['status_id'];
                $report->status_id_file = (string)$item['status_id_file'];
                $report->price = $item['price'];
                $report->price_file = (string)$item['price_file'];
                $report->difference = (string)$item['difference'];
                $report->error = $item['error'];
                $report->day = $day;
                $result = $report->save();
                if (!$result) {
                    $transaction->rollBack();
                    return Yii::t('common', 'Ошибки') . ': ' . implode(', ', $report->getErrorsAsArray());
                }
            } catch (Exception $e) {
                $transaction->rollBack();
                return Yii::t('common', 'Ошибка') . ': ' . $e->getMessage();
            }
        }
        $transaction->commit();
        return null;
    }

    /**
     * @param array $searchArray
     * @param string $foreignId
     * @return integer|boolean
     */
    public function inArray($searchArray, $foreignId)
    {
        foreach ($searchArray as $key => $value) {
            if ($searchArray[$key]['foreign_id'] == $foreignId) {
                return $key;
            }
        }

        return false;
    }

    /**
     * @return ReportAdvertisingFilter
     */
    public function getDateFilter()
    {
        if (is_null($this->dateFilter)) {
            $this->dateFilter = new ReportAdvertisingFilter();
        }

        return $this->dateFilter;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $output = '';

        $output .= $this->getDateFilter()->restore($form);

        return $output;
    }

    /**
     * @param array $params
     * @return array
     */
    public function getHistory($params)
    {
        $this->query = ReportAdvertising::find()
            ->select([
                'id' => 'id',
                'day' => 'day',
                'day_normal' => 'FROM_UNIXTIME(day, "' . Formatter::getMysqlDateFormat() . ' %H:%i")',
                'foreign_id' => 'foreign_id',
                'foreign_id_file' => 'foreign_id_file',
                'country_code' => 'country_code',
                'country_code_file' => 'country_code_file',
                'created_at' => 'created_at',
                'created_at_file' => 'created_at_file',
                'status_id' => 'status_id',
                'status_id_file' => 'status_id_file',
                'price' => 'price',
                'price_file' => 'price_file',
                'difference' => 'difference',
                'error' => 'error',
            ]);
        $this->applyFilters($params);
        $this->query->orderBy('day');

        $resultArray = null;
        $historyArray = $this->query->asArray()->all();
        foreach ($historyArray as $historyItem) {
            $resultArray[$historyItem['day']]['allModels'][] = [
                'id' => $historyItem['id'],
                'foreign_id' => $historyItem['foreign_id'],
                'foreign_id_file' => $historyItem['foreign_id_file'],
                'country_code' => $historyItem['country_code'],
                'country_code_file' => $historyItem['country_code_file'],
                'created_at' => $historyItem['created_at'],
                'created_at_file' => $historyItem['created_at_file'],
                'status_id' => $historyItem['status_id'],
                'status_id_file' => $historyItem['status_id_file'],
                'price' => $historyItem['price'],
                'price_file' => $historyItem['price_file'],
                'difference' => $historyItem['difference'],
                'error' => $historyItem['error'],
            ];
            $resultArray[$historyItem['day']]['day_normal'] = $historyItem['day_normal'];
        }

        $daysArray = null;
        if ($resultArray) {
            $daysArray = [];
            foreach ($resultArray as $key => $value) {
                $daysArray[] = [
                    'day' => Html::a($resultArray[$key]['day_normal'], Url::to(['advertising/index']) .
                        '?s[from]=' . Yii::$app->request->queryParams['s']['from'] .
                        '&s[to]=' . Yii::$app->request->queryParams['s']['to'] .
                        '&day=' . $key . '#tab1')
                ];
            }
        }

        return [
            'dataHistory' => $resultArray,
            'daysArray' => $daysArray,
        ];
    }

    /**
     * @param array $params
     * @return ReportFormAdvertising
     */
    protected function applyFilters($params)
    {
        $this->getDateFilter()->apply($this->query, $params);

        return $this;
    }
}
