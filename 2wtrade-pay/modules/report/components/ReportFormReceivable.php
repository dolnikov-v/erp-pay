<?php
namespace app\modules\report\components;

use app\modules\delivery\models\Delivery;
use app\models\Country;
use yii\data\ArrayDataProvider;
use app\modules\report\models\ReportDebts;
use app\modules\report\components\filters\ReceivableFilter;
use Yii;
use app\modules\report\components\filters\ReceivableDebtFilter;

class ReportFormReceivable extends ReportForm
{
    /**
     * @var ReceivableFilter
     */
    protected $receivableFilter;

    /**
     * @var ReceivableDebtFilter
     */
    protected $receivableDebtFilter;

    /**
     * @var integer - текущий год
     */
    private $currentYear;

    /**
     * @var array
     */
    private $aliases = [];

    /**
     * @var array
     */
    private $aliasesHead;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->currentYear = date("Y");
        parent::init();
    }

    /**
     * @param array $params
     * @return array
     */
    public function apply($params)
    {
        $filterDates = $this->applyFilters($params);
        if ($filterDates === false || $filterDates['to'] == $filterDates['from']) {
            return [];
        }
        $from = $filterDates['from'];
        $to = $filterDates['to'];
        $this->query = ReportDebts::find()
            ->joinWith([
                'country',
                'delivery',
            ])
            ->select([
                'month_year' => ReportDebts::tableName() . '.month_year',
                'country_id' => ReportDebts::tableName() . '.country_id',
                'country_name' => Country::tableName() . '.name',
                'delivery_id' => ReportDebts::tableName() . '.delivery_id',
                'delivery_name' => Delivery::tableName() . '.name',
                'debts_from' => 'CASE FROM_UNIXTIME(' . ReportDebts::tableName() . '.day, "%Y-%m-%d")
                        WHEN FROM_UNIXTIME(' . $to . ', "%Y-%m-%d") THEN 0
                        WHEN FROM_UNIXTIME(' . $from . ', "%Y-%m-%d") THEN IFNULL(' . ReportDebts::tableName() . '.debts,0)
                        END',
                'potential_revenue_from' => 'CASE FROM_UNIXTIME(' . ReportDebts::tableName() . '.day, "%Y-%m-%d")
                        WHEN FROM_UNIXTIME(' . $to . ', "%Y-%m-%d") THEN 0
                        WHEN FROM_UNIXTIME(' . $from . ', "%Y-%m-%d") THEN IFNULL(' . ReportDebts::tableName() . '.potential_revenue,0)
                        END',
                'revenue_from' => 'CASE FROM_UNIXTIME(' . ReportDebts::tableName() . '.day, "%Y-%m-%d")
                        WHEN FROM_UNIXTIME(' . $to . ', "%Y-%m-%d") THEN 0
                        WHEN FROM_UNIXTIME(' . $from . ', "%Y-%m-%d") THEN IFNULL(' . ReportDebts::tableName() . '.revenue,0)
                        END',
                'debts_to' => 'CASE FROM_UNIXTIME(' . ReportDebts::tableName() . '.day, "%Y-%m-%d")
                        WHEN FROM_UNIXTIME(' . $from . ', "%Y-%m-%d") THEN 0
                        WHEN FROM_UNIXTIME(' . $to . ', "%Y-%m-%d") THEN IFNULL(' . ReportDebts::tableName() . '.debts,0)
                        END',
                'potential_revenue_to' => 'CASE FROM_UNIXTIME(' . ReportDebts::tableName() . '.day, "%Y-%m-%d")
                        WHEN FROM_UNIXTIME(' . $from . ', "%Y-%m-%d") THEN 0
                        WHEN FROM_UNIXTIME(' . $to . ', "%Y-%m-%d") THEN IFNULL(' . ReportDebts::tableName() . '.potential_revenue,0)
                        END',
                'revenue_to' => 'CASE FROM_UNIXTIME(' . ReportDebts::tableName() . '.day, "%Y-%m-%d")
                        WHEN FROM_UNIXTIME(' . $from . ', "%Y-%m-%d") THEN 0
                        WHEN FROM_UNIXTIME(' . $to . ', "%Y-%m-%d") THEN IFNULL(' . ReportDebts::tableName() . '.revenue,0)
                        END',
                'refund' => 'CASE FROM_UNIXTIME(' . ReportDebts::tableName() . '.day, "%Y-%m-%d")
                        WHEN FROM_UNIXTIME(' . $from . ', "%Y-%m-%d") THEN -IFNULL(' . ReportDebts::tableName() . '.revenue,0)
                        WHEN FROM_UNIXTIME(' . $to . ', "%Y-%m-%d") THEN IFNULL(' . ReportDebts::tableName() . '.revenue,0)
                        END'
            ]);
        $this->applyFiltersReceivableDebt($params);
        $this->query->orderBy(Country::tableName() . '.name, ' . Delivery::tableName() . '.name, ' . ReportDebts::tableName() . '.month_year');
        $data = $this->query->asArray()->all();

        $resultArray = [];
        foreach ($data as $item) {
            $alias = $this->getAlias($item['month_year']);
            $this->aliases[$alias] = $alias;
            if (isset($resultArray[$item['country_id'] . '_' . $item['delivery_id']])) {
                $resultArray[$item['country_id'] . '_' . $item['delivery_id']]['debts_from_' . $alias] += $item['debts_from'];
                $resultArray[$item['country_id'] . '_' . $item['delivery_id']]['potential_revenue_from_' . $alias] += $item['potential_revenue_from'];
                $resultArray[$item['country_id'] . '_' . $item['delivery_id']]['revenue_from_' . $alias] += $item['revenue_from'];

                $resultArray[$item['country_id'] . '_' . $item['delivery_id']]['debts_to_' . $alias] += $item['debts_to'];
                $resultArray[$item['country_id'] . '_' . $item['delivery_id']]['potential_revenue_to_' . $alias] += $item['potential_revenue_to'];
                $resultArray[$item['country_id'] . '_' . $item['delivery_id']]['revenue_to_' . $alias] += $item['revenue_to'];

                $resultArray[$item['country_id'] . '_' . $item['delivery_id']]['refund'] += $item['refund'];

                $resultArray[$item['country_id'] . '_' . $item['delivery_id']]['debts_all'] += $item['debts_to'] - $item['debts_from'];
                $resultArray[$item['country_id'] . '_' . $item['delivery_id']]['potential_revenue_all'] += $item['potential_revenue_to'] - $item['potential_revenue_from'];
                $resultArray[$item['country_id'] . '_' . $item['delivery_id']]['revenue_all'] += $item['revenue_to'] - $item['revenue_from'];
                $resultArray[$item['country_id'] . '_' . $item['delivery_id']]['refund_all'] += $item['refund'];
            } else {
                $resultArray[$item['country_id'] . '_' . $item['delivery_id']] = [
                    'country_name' => $item['country_name'],
                    'delivery_name' => $item['delivery_name'],
                    'debts_from_' . $alias => $item['debts_from'],
                    'potential_revenue_from_' . $alias => $item['potential_revenue_from'],
                    'revenue_from_' . $alias => $item['revenue_from'],
                    'debts_to_' . $alias => $item['debts_to'],
                    'potential_revenue_to_' . $alias => $item['potential_revenue_to'],
                    'revenue_to_' . $alias => $item['revenue_to'],
                    'debts_' . $alias => 0,
                    'potential_revenue_' . $alias => 0,
                    'revenue_' . $alias => 0,
                    'refund' => 0,
                    'debts_all' => $item['debts_to'] - $item['debts_from'],
                    'potential_revenue_all' => $item['potential_revenue_to'] - $item['potential_revenue_from'],
                    'revenue_all' => $item['revenue_to'] - $item['revenue_from'],
                    'refund_all' => $item['refund'],
                ];
            }
        }

        foreach ($resultArray as $resultKey => $resultValue) {
            foreach ($resultArray[$resultKey] as $key => $value) {
                if ($key != 'country_name' && $key != 'delivery_name' && $key != 'refund' && !strpos($key, 'all')) {
                    $index = substr($key, -6);
                    $resultArray[$resultKey]['debts_' . $index] = $this->nullToZeroValue($resultArray[$resultKey]['debts_to_' . $index]) . '-' . $this->nullToZeroValue($resultArray[$resultKey]['debts_from_' . $index]) . '=' . ($this->nullToZeroValue($resultArray[$resultKey]['debts_to_' . $index]) - $this->nullToZeroValue($resultArray[$resultKey]['debts_from_' . $index]));
                    $resultArray[$resultKey]['potential_revenue_' . $index] = $this->nullToZeroValue($resultArray[$resultKey]['potential_revenue_to_' . $index]) . '-' . $this->nullToZeroValue($resultArray[$resultKey]['potential_revenue_from_' . $index]) . '=' . ($this->nullToZeroValue($resultArray[$resultKey]['potential_revenue_to_' . $index]) - $this->nullToZeroValue($resultArray[$resultKey]['potential_revenue_from_' . $index]));
                    $resultArray[$resultKey]['revenue_' . $index] = $this->nullToZeroValue($resultArray[$resultKey]['revenue_to_' . $index]) . '-' . $this->nullToZeroValue($resultArray[$resultKey]['revenue_from_' . $index]) . '=' . ($this->nullToZeroValue($resultArray[$resultKey]['revenue_to_' . $index]) - $this->nullToZeroValue($resultArray[$resultKey]['revenue_from_' . $index]));
                    continue;
                }
            }
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $resultArray,
        ]);

        $this->getColumnsGrid();

        return [
            'dataProvider' => $dataProvider,
            'aliases' => $this->aliases,
            'aliasesHead' => $this->aliasesHead,
        ];
    }

    /**
     * @return ReceivableFilter
     */
    public function getReceivableFilter()
    {
        if (is_null($this->receivableFilter)) {
            $this->receivableFilter = new ReceivableFilter();
        }

        return $this->receivableFilter;
    }

    /**
     * @return ReceivableDebtFilter
     */
    public function getReceivableDebtFilter()
    {
        if (is_null($this->receivableDebtFilter)) {
            $this->receivableDebtFilter = new ReceivableDebtFilter();
        }

        return $this->receivableDebtFilter;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return '';
    }

    /**
     * @param array $params
     * @return array
     */
    protected function applyFilters($params)
    {
        return $this->getReceivableFilter()->applyParams($params);
    }

    /**
     * @param array $params
     * @return $this
     */
    protected function applyFiltersReceivableDebt($params)
    {
        $this->getReceivableDebtFilter()->apply($this->query, $params);

        return $this;
    }

    /**
     * Алиасы колонок
     * @param string(yyyy-mm-dd) $date
     * @return string
     */
    private function getAlias($date)
    {
        $month = substr($date, 5, 2);
        $year = substr($date, 0, 4);
        $result = $year;
        if ($year == $this->currentYear) {
            switch ($month) {
                case '01':
                case '02':
                case '03':
                    $result .= '_1';
                    break;
                case '04':
                case '05':
                case '06':
                    $result .= '_2';
                    break;
                case '07':
                case '08':
                case '09':
                    $result .= '_3';
                    break;
                case '10':
                case '11':
                case '12':
                    $result .= '_4';
                    break;
            }
        }
        return $result;
    }

    /**
     * Шапка грида
     */
    private function getColumnsGrid()
    {
        $countAliases = count($this->aliases);
        $colspan = $countAliases + 1;
        $this->aliasesHead = [
            ['label' => '', 'options' => []],
            ['label' => '', 'options' => []],
            ['label' => Yii::t('common', 'Задолженность (статус "Курьерка (выкуп)")'), 'options' => ['colspan' => $colspan]],
            ['label' => Yii::t('common', 'Заказы в работе у курьера. Заказы "после" отправки курьеру, не финишированы (не возвращено отправителю, выручка не перечислена), но до возврата'), 'options' => ['colspan' => $colspan]],
            ['label' => Yii::t('common', 'Выручка (статус "Финансы (деньги получены)")'), 'options' => ['colspan' => $colspan]],
            ['label' => Yii::t('common', 'Отказ покупателя (фактически состоявшийся возврат)'), 'options' => []],
        ];

        $columns = [
            ['attribute' => 'country_name', 'label' => Yii::t('common', 'Страна'), 'headerOptions' => ['rowspan' => 2]],
            ['attribute' => 'delivery_name', 'label' => Yii::t('common', 'Служба доставки'), 'headerOptions' => ['rowspan' => 2]],
        ];

        $i = 1;
        foreach ($this->aliases as $alias) {
            $year = substr($alias, 0, 4);
            $quarter = substr($alias, 5);
            $name = '';
            if (!empty($quarter)) {
                $name .= $quarter . ' квартал ';
            }
            $name .= $year;
            $columns[] = ['attribute' => 'debts_' . $alias, 'label' => Yii::t('common', $name)];
            if ($i == $countAliases) {
                $columns[] = ['attribute' => 'debts_all', 'label' => Yii::t('common', 'Итого')];
            }
            $columns[] = ['attribute' => 'potential_revenue_' . $alias, 'label' => Yii::t('common', $name)];
            if ($i == $countAliases) {
                $columns[] = ['attribute' => 'potential_revenue_all', 'label' => Yii::t('common', 'Итого')];
            }
            $columns[] = ['attribute' => 'revenue_' . $alias, 'label' => Yii::t('common', $name)];
            if ($i == $countAliases) {
                $columns[] = ['attribute' => 'revenue_all', 'label' => Yii::t('common', 'Итого')];
            }
            $i++;
        }
        $columns[] = ['attribute' => 'refund', 'label' => Yii::t('common', 'Итого')];

        $this->aliases = $columns;
    }

    /**
     * @param integer $value
     * @return integer
     */
    private function nullToZeroValue($value)
    {
        if ($value == null) {
            return 0;
        }
        return $value;
    }
}
