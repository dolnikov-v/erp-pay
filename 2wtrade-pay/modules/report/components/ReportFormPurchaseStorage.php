<?php

namespace app\modules\report\components;

use app\models\Country;
use app\models\Product;
use app\modules\order\models\Order;
use app\modules\order\models\OrderProduct;
use app\modules\report\components\filters\StorageProductPurchaseSelectFilter;
use app\modules\storage\models\Storage;
use app\modules\storage\models\StorageProduct;
use app\modules\storage\models\StorageProductPurchase;
use Yii;
use yii\data\ArrayDataProvider;
use yii\db\Expression;

/**
 * https://2wtrade-tasks.atlassian.net/browse/NPAY-1487
 * Class ReportFormPurchaseStorage
 * @package app\modules\report\components
 */
class ReportFormPurchaseStorage extends ReportForm
{
    /**
     * @var string Группировка по умолчанию
     */
    public $groupBy = self::GROUP_BY_PRODUCTS;

    /**
     * @var StorageProductPurchaseSelectFilter
     */
    protected $productSelectFilter;

    public function init()
    {
        parent::init();

        $this->groupByCollection = [];
        $this->groupByCollection[self::GROUP_BY_PRODUCTS] = Yii::t('common', 'Товар') . '\\' . Yii::t('common', 'Страна');
        $this->groupByCollection[self::GROUP_BY_COUNTRY_PRODUCTS] = Yii::t('common', 'Страна') . ' \\ ' . Yii::t('common', 'Товар');
    }

    /**
     * @param array $params
     * @return ArrayDataProvider
     */
    public function apply($params)
    {
        $this->load($params);
        $this->query = StorageProductPurchase::find()
            ->joinWith(['product', 'country'])
            ->select([
                'id' => StorageProductPurchase::tableName() . '.id',
                'product_name' => Product::tableName() . '.name',
                'product_id' => StorageProductPurchase::tableName() . '.product_id',
                'country_name' => Country::tableName() . '.name',
                'country_id' => Country::tableName() . '.id',
                'last_purchase_date' => StorageProductPurchase::tableName() . '.last_purchase_date',
                'plan_lead_day' => StorageProductPurchase::tableName() . '.plan_lead_day',
                'country_slug' => Country::tableName() . '.slug',
            ]);

        $balanceQuery = StorageProduct::find()
            ->select(['sum(' . StorageProduct::tableName() . '.balance)'])
            ->joinWith(['storage'])
            ->where([StorageProduct::tableName() . '.product_id' => new Expression(StorageProductPurchase::tableName() . '.product_id')])
            ->andWhere([Storage::tableName() . '.country_id' => new Expression(StorageProductPurchase::tableName() . '.country_id')]);

        $ordersQuery = Order::find()
            ->select(['COUNT(DISTINCT ' . Order::tableName() . '.`id`) / 3'])
            ->joinWith(['orderProducts'])
            ->andWhere([OrderProduct::tableName() . '.product_id' => new Expression(StorageProductPurchase::tableName() . '.product_id')])
            ->andWhere([Order::tableName() . '.country_id' => new Expression(StorageProductPurchase::tableName() . '.country_id')])
            ->andWhere(['>=', Order::tableName() . '.created_at', time() - 3 * 86400])
            ->andWhere(['is', Order::tableName() . '.duplicate_order_id', null]);

        if ($this->groupBy == self::GROUP_BY_PRODUCTS) {
            $this->query->orderBy('`product_name`, `country_name`');
        } elseif ($this->groupBy == self::GROUP_BY_COUNTRY_PRODUCTS) {
            $this->query->orderBy('`country_name`, `product_name`');
        }

        $this->query->addSelect([
            'balance' => $balanceQuery
        ]);

        $this->query->addSelect([
            'orders' => $ordersQuery
        ]);

        $this->applyFilters($params);

        $dataProvider = new ArrayDataProvider([
            'allModels' => $this->query->asArray()->all(),
            'pagination' => [
                'pageSize' => 0
            ]
        ]);

        return $dataProvider;
    }

    /**
     * @return StorageProductPurchaseSelectFilter
     */
    public function getProductSelectFilter()
    {
        if (is_null($this->productSelectFilter)) {
            $this->productSelectFilter = new StorageProductPurchaseSelectFilter();
        }
        return $this->productSelectFilter;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return '';
    }

    /**
     * @param array $params
     * @return $this
     */
    protected function applyFilters($params)
    {
        $this->getProductSelectFilter()->apply($this->query, $params);
        $this->getCountrySelectFilter()->apply($this->query, $params);

        return $this;
    }
}