<?php
namespace app\modules\report\components\filters;

use app\modules\report\widgets\filters\AccountingCostsFilter as AccountingCostsFilterWidget;
use Yii;

/**
 * Class AccountingCostsFilter
 * @package app\modules\report\components\filters
 */
class AccountingCostsFilter extends Filter
{
    public $from;
    public $to;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->from = Yii::$app->formatter->asDate(time());
        $this->to = Yii::$app->formatter->asDate(time());
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['from', 'to'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'date' => Yii::t('common', 'Дата'),
        ];
    }

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate()) {
            if ($this->from && $dateFrom = Yii::$app->formatter->asDate($this->from, "Y-m-d")) {
                $query->andFilterWhere(['>=', 'date_article', $dateFrom]);
            }

            if ($this->to && $dateTo = Yii::$app->formatter->asDate($this->to, "Y-m-d")) {
                $query->andFilterWhere(['<=', 'date_article', $dateTo]);
            }
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return AccountingCostsFilterWidget::widget([
            'form' => $form,
            'model' => $this,
        ]);
    }

}
