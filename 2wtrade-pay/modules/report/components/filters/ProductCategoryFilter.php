<?php

namespace app\modules\report\components\filters;

use app\models\ProductCategory;
use app\models\ProductLinkCategory;
use app\modules\order\models\Order;
use app\modules\order\models\OrderProduct;
use app\modules\report\components\ReportForm;
use app\modules\report\widgets\filters\ProductCategoryFilter as ProductCategoryWidget;
use Yii;
use yii\db\Expression;

/**
 * Class ProductCategoryFilter
 * @package app\modules\report\components\filters
 */
class ProductCategoryFilter extends Filter
{
    /**
     * @var integer
     */
    public $product_category;

    /**
     * @var ReportForm
     */
    public $reportForm;

    /**
     * @var array
     */
    public $product_categories = [];

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            ['product_category', 'safe'],
        ];
    }

    public function init()
    {
        $this->product_categories = ProductCategory::find()
            ->collection();

        parent::init();
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            'product_category' => Yii::t('common', 'Категория товаров')
        ];
    }

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

         if ($this->validate() and !empty($this->product_category)) {
             $queryFrom = OrderProduct::find()->select(new Expression('1'));
             $queryFrom->leftJoin(ProductLinkCategory::tableName(), ProductLinkCategory::tableName() . '.product_id = ' . OrderProduct::tableName() . '.product_id');
             $queryFrom->where([
                 OrderProduct::tableName() . '.order_id' => new Expression(Order::tableName() . '.id'),
                 ProductLinkCategory::tableName() . '.category_id' => $this->product_category
             ]);
             $queryFrom->limit(1);
             $query->andWhere(['exists', $queryFrom]);
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     * @throws \Exception
     */
    public function restore($form): string
    {
        return ProductCategoryWidget::widget([
            'form' => $form,
            'model' => $this
        ]);
    }
}
