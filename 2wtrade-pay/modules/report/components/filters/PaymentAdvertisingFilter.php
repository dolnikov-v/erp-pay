<?php
namespace app\modules\report\components\filters;

use app\helpers\PayStatus;
use app\modules\order\models\Order;
use app\widgets\custom\Checkbox;
use Yii;
use yii\helpers\Html;

/**
 * Class PaymentAdvertisingFilter
 * @package app\modules\report\components\filters
 */
class PaymentAdvertisingFilter extends Filter
{
    /**
     * @var string
     */
    public $advertising;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['advertising', 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'advertising' => Yii::t('common', 'Оплата за рекламу')
        ];
    }

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate() && $this->advertising) {
            $statuses = PayStatus::getStatusesByPayStatuses([
                PayStatus::STATUS_ACCEPTED,
                PayStatus::STATUS_SHIPPED,
                PayStatus::STATUS_NOT_BUYOUT,
            ]);

            $query->andWhere(['in', Order::tableName() . '.status_id', $statuses]);
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return Checkbox::widget([
            'id' => Html::getInputId($this, 'advertising'),
            'style' => 'checkbox-inline',
            'label' => Yii::t('common', 'Оплата за рекламу'),
            'checked' => $this->advertising ? true : false,
            'name' => Html::getInputName($this, 'advertising'),
        ]);
    }
}
