<?php
namespace app\modules\report\components\filters;

use app\models\Product;
use app\modules\order\models\Order;
use app\modules\order\models\OrderProduct;
use app\modules\report\components\ReportForm;
use app\modules\report\widgets\filters\ProductFilter as ProductFilterWidget;
use Yii;
use yii\db\Query;

/**
 * Class ProductFilter
 * @package app\modules\report\components\filters
 */
class TopProductFilter extends Filter
{
    /**
     * @var integer
     */
    public $product;

    /**
     * @var ReportForm
     */
    public $reportForm;

    /**
     * @var array
     */
    public $products = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->products = Product::find()->collection();

        parent::init();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['product'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'product' => Yii::t('common', 'Товар'),
        ];
    }

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate()) {
            if ($this->product) {

                $query->andFilterWhere(['=', 'order_product.product_id', $this->product]);
            }
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return ProductFilterWidget::widget([
            'form' => $form,
            'model' => $this
        ]);
    }
}
