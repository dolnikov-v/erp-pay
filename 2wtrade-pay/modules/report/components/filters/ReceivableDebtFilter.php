<?php
namespace app\modules\report\components\filters;

use app\modules\delivery\models\Delivery;
use app\models\Country;

/**
 * Class ReceivableDebtFilter
 * @package app\modules\report\components\filters
 */
class ReceivableDebtFilter extends DebtFilter
{
    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate()) {
            if ($this->delivery) {
                $query->andWhere([Delivery::tableName() . '.id' => $this->delivery]);
            }
            if ($this->country) {
                $query->andWhere([Country::tableName() . '.id' => $this->country]);
            }
        }
    }
}
