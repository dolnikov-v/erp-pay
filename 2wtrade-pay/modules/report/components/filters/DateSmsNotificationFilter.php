<?php
namespace app\modules\report\components\filters;

use app\modules\order\models\Order;
use app\modules\order\models\OrderNotificationRequest;
use app\modules\report\widgets\filters\DateSmsNotificationFilter as DateSmsNotificationFilterWidget;
use Yii;
use yii\base\InvalidParamException;

/**
 * Class DateSmsNotificationFilter
 * @package app\modules\report\components\filters
 */
class DateSmsNotificationFilter extends Filter
{
    const TYPE_IGNORE = '';
    const TYPE_CREATED_AT = 'order_notification_request.created_at';
    const TYPE_ANSWERED_AT = 'order_notification_request.answered_at';

    public $from;
    public $to;
    public $type = self::TYPE_CREATED_AT;

    public $types = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->from = Yii::$app->formatter->asDate(time());
        $this->to = Yii::$app->formatter->asDate(time());

        if (Yii::$app->user->language->locale == 'es-ES') {                         // исключение для испанского представления даты
            $this->from = str_replace('/', '.', $this->from);
            $this->to = str_replace('/', '.', $this->to);
        }

        $this->types = [
            self::TYPE_IGNORE => Yii::t('common', 'Игнорировать'),
            self::TYPE_CREATED_AT => Yii::t('common', 'Дата создания'),
            self::TYPE_ANSWERED_AT => Yii::t('common', 'Дата ответа'),
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['type', 'in', 'range' => array_keys($this->types)],
            [['from', 'to'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'date' => Yii::t('common', 'Дата'),
            'type' => Yii::t('common', 'Тип'),
        ];
    }

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if (Yii::$app->user->language->locale == 'es-ES') {                         // исключение для испанского представления даты
            $this->from = str_replace('/', '.', $this->from);
            $this->to = str_replace('/', '.', $this->to);
        }

        if ($this->validate()) {
            if ($this->type) {

                if ($this->from && $dateFrom = Yii::$app->formatter->asTimestamp($this->from)) {
                    $query->andFilterWhere(['>=', $this->getFilteredAttribute(), $dateFrom]);
                }

                if ($this->to && $dateTo = Yii::$app->formatter->asTimestamp($this->to)) {
                    $query->andFilterWhere(['<=', $this->getFilteredAttribute(), $dateTo + 86399]);
                }

            }
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return DateSmsNotificationFilterWidget::widget([
            'form' => $form,
            'model' => $this,
        ]);
    }

    /**
     * @return string
     */
    public function getFilteredAttribute()
    {
        $attributes = [
            self::TYPE_CREATED_AT => OrderNotificationRequest::tableName() . '.created_at',
            self::TYPE_ANSWERED_AT => OrderNotificationRequest::tableName() . '.answered_at',
        ];

        if (!array_key_exists($this->type, $attributes)) {
            throw new InvalidParamException(Yii::t('common', 'Неизвестный тип в условии.'));
        }

        return $attributes[$this->type];
    }
}
