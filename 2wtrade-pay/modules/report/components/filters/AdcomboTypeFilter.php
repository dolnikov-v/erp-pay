<?php

namespace app\modules\report\components\filters;

use app\modules\report\models\ReportAdcombo;
use app\modules\report\models\ReportCallCenter;
use Yii;
use yii\base\InvalidParamException;

/**
 * Class AdComboTypeFilter
 * @package app\modules\report\components\filters
 */
class AdcomboTypeFilter extends Filter
{
    const ATTR_ADCOMBO_TYPE = 'adcombo_type';
    const ATTR_CALL_CENTER_TYPE = 'call_center_type';

    public $type = ReportAdcombo::TYPE_DAILY;

    public $types = [];

    public $attribute;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->types = [
            ReportAdcombo::TYPE_HOURLY => Yii::t('common', 'Ежечасная сверка'),
            ReportAdcombo::TYPE_DAILY => Yii::t('common', 'Ежедневная сверка'),
            ReportAdcombo::TYPE_WEEKLY => Yii::t('common', 'Еженедельная сверка'),
            ReportAdcombo::TYPE_MONTHLY => Yii::t('common', 'Ежемесячная сверка'),
        ];

        $this->attribute = self::ATTR_ADCOMBO_TYPE;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [
                'type',
                'in',
                'range' => [ReportAdcombo::TYPE_DAILY, ReportAdcombo::TYPE_MONTHLY, ReportAdcombo::TYPE_WEEKLY, ReportAdcombo::TYPE_HOURLY]
            ]
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'type' => Yii::t('common', 'Тип'),
        ];
    }

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);
        $attr = $this->getFilteredAttribute();
        if ($this->validate()) {
            $query->andFilterWhere([$attr => $this->type]);
        } else {
            $this->type = ReportAdcombo::TYPE_DAILY;
            $query->andFilterWhere([$attr => ReportAdcombo::TYPE_DAILY]);
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return $form->field($this, 'type')->select2List($this->types);
    }

    /**
     * @return string
     */
    protected function getFilteredAttribute()
    {
        $attributes = [
            self::ATTR_ADCOMBO_TYPE => ReportAdcombo::tableName().'.type',
            self::ATTR_CALL_CENTER_TYPE => ReportCallCenter::tableName().'.type'
        ];

        if (!array_key_exists($this->attribute, $attributes)) {
            throw new InvalidParamException(Yii::t('common', 'Неизвестный тип в условии.'));
        }

        return $attributes[$this->attribute];
    }
}
