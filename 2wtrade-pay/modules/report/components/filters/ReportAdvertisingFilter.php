<?php
namespace app\modules\report\components\filters;

use app\modules\report\widgets\filters\ReportAdvertisingFilter as ReportAdvertisingFilterWidget;
use Yii;

/**
 * Class ReportAdvertisingFilter
 * @package app\modules\report\components\filters
 */
class ReportAdvertisingFilter extends Filter
{
    public $from;
    public $to;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->from = Yii::$app->formatter->asDate(time());
        $this->to = Yii::$app->formatter->asDate(time());
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['from', 'to'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'date' => Yii::t('common', 'Дата'),
        ];
    }

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate()) {
            $timeFrom = Yii::$app->formatter->asTimestamp($this->from);
            $timeTo = Yii::$app->formatter->asTimestamp($this->to);

            if ($timeFrom) {
                $query->andFilterWhere(['>=', 'day', $timeFrom]);
            }

            if ($timeTo) {
                $query->andFilterWhere(['<=', 'day', $timeTo + 86399]);
            }
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return ReportAdvertisingFilterWidget::widget([
            'form' => $form,
            'model' => $this,
        ]);
    }
}
