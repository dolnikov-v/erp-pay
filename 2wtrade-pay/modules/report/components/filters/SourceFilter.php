<?php

namespace app\modules\report\components\filters;

use app\models\Source;
use app\modules\order\models\Order;
use app\modules\report\components\ReportForm;
use app\modules\report\widgets\filters\SourceFilter as SourceFilterWidget;
use Yii;

/**
 * Class SourceFilter
 * @package app\modules\report\components\filters
 */
class SourceFilter extends Filter
{
    /**
     * @var string
     */
    public $source;

    /**
     * @var ReportForm
     */
    public $reportForm;

    /**
     * @var array
     */
    public $sources = [];

    /**
     * @var boolean
     */
    public $allsources = false;

    /**
     * @inheritdoc
     */
    public function init()
    {
        if (Yii::$app->user->rejectedSources) {
            $this->sources = Source::find()->where(['not in', 'id', Yii::$app->user->rejectedSources])->collection();
        } else {
            $this->sources = Source::find()->collection();
        }
        parent::init();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['source'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'source' => Yii::t('common', 'Ресурс'),
        ];
    }

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);
        if ($this->validate()) {
            if ($this->source) {
                $query->andWhere([Order::tableName() . '.source_id' => $this->source]);
            }
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return SourceFilterWidget::widget([
            'form' => $form,
            'model' => $this
        ]);
    }
}
