<?php

namespace app\modules\report\components\filters;

use app\models\Country;
use app\models\User;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\models\Order;
use app\modules\report\widgets\filters\CountryDeliverySelectMultipleFilter as CountryDeliverySelectMultipleWidget;
use function GuzzleHttp\Promise\queue;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class CountryDeliverySelectMultipleFilter
 * @package app\modules\report\widgets\filters
 */
class CountryDeliverySelectMultipleFilter extends Filter
{
    const DELIVERY_TYPE_ORDER = 'delivery_order';
    const DELIVERY_TYPE_DELIVERY = 'delivery';
    const COUNTRY_TYPE_COUNTRY = 'country_type_country';
    const COUNTRY_TYPE_ORDER = 'country_type_order';
    const COUNTRY_TYPE_DELIVERY = 'country_type_delivery';

    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var \app\models\Country */
    public $countries;

    /** @var array */
    public $country_ids;

    /** @var \app\modules\delivery\models\Delivery */
    public $deliveries;

    /** @var array */
    public $delivery_ids;

    /** @var bool */
    public $hasApply;

    /**
     * Вкл/выкл автовыбора всех курьерок страны, если не выбрана ни одна
     * @var bool
     */
    public $allowNoDelivery = false;

    /**
     * Вкл/выкл мультивыбора стран
     * @var bool
     */
    public $multipleCountrySelect = true;

    /**
     * Вкл/выкл мультивыбор курьерок
     * @var bool
     */
    public $multipleDeliverySelect = true;

    /**
     * @var string
     */
    public $countryType = self::COUNTRY_TYPE_COUNTRY;
    /**
     * @var string
     */
    public $deliveryType = self::DELIVERY_TYPE_ORDER;


    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        if (!$this->hasApply) {
            $this->hasApply = $this->load($params);
        }

        $country_ids = $this->getCountryIds($params);

        if ($country_ids && $this->validate()) {
            $query->andWhere([$this->getCountryField() => $country_ids]);
        }

        $queryDeliveries = Delivery::find()
            ->joinWith(['country'], false)
            ->select([
                'id' => Delivery::tableName() . '.id',
                'name' => 'concat(' . Country::tableName() . '.name, " ", ' . Delivery::tableName() . '.name)',
            ])
            ->where([Delivery::tableName() . '.country_id' => $country_ids]);

        $this->deliveries = $queryDeliveries;

        if ($this->delivery_ids && $this->validate()) {
            if ($this->deliveryType == self::DELIVERY_TYPE_ORDER) {
                $query->andWhere(['COALESCE(' . DeliveryRequest::tableName() . '.delivery_id, ' . Order::tableName() . '.pending_delivery_id)' => $this->delivery_ids]);
            } else {
                $query->andWhere([$this->getDeliveryField() => $this->delivery_ids]);
            }
        }
    }

    public function prepareDeliveryList($params)
    {
        if (!$this->hasApply) {
            $this->hasApply = $this->load($params);
        }

        $country_ids = $this->getCountryIds($params);

        $queryDeliveries = Delivery::find()
            ->joinWith(['country'], false)
            ->select([
                'id' => Delivery::tableName() . '.id',
                'name' => 'concat(' . Country::tableName() . '.name, " ", ' . Delivery::tableName() . '.name)',
            ])
            ->where([Delivery::tableName() . '.country_id' => $country_ids]);

        $this->deliveries = $queryDeliveries;
    }

    /**
     * @param array $params
     * @return array
     */
    public function applyParams($params)
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        $allowCountries = array_keys(User::getAllowCountries());
        $this->countries = Country::find()
            ->where(['IN', Country::tableName() . '.id', $allowCountries])
            ->active()
            ->orderBy(['name' => SORT_ASC]);
        $this->deliveries = Delivery::find()
            ->joinWith(['country'], false)
            ->select([
                'id' => Delivery::tableName() . '.id',
                'name' => 'concat(' . Country::tableName() . '.name, " ", ' . Delivery::tableName() . '.name)',
            ])
            ->where(['IN', Delivery::tableName() . '.country_id', $allowCountries])
            ->byCountryId(Yii::$app->user->getCountry()->id)
            ->orderBy(['name' => SORT_ASC]);
        parent::init();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['country_ids', 'delivery_ids'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'country_ids' => Yii::t('common', 'Страна'),
            'delivery_ids' => Yii::t('common', 'Служба доставки'),
        ];
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return CountryDeliverySelectMultipleWidget::widget([
            'form' => $form,
            'model' => $this,
        ]);
    }

    /**
     * @param array $params
     * @return array
     */
    public function getCountryIds($params)
    {
        if (!$this->hasApply) {
            $this->hasApply = $this->load($params);
        }
        return $this->country_ids ?: ($this->multipleCountrySelect ? array_keys(User::getAllowCountries()) : [Yii::$app->user->country->id]);
    }

    /**
     * @param array $params
     * @return array
     */
    public function getDeliveryIds($params)
    {
        if (!$this->hasApply) {
            $this->hasApply = $this->load($params);
        }

        $countryIds = $this->getCountryIds($params);

        $deliveryIds = $this->delivery_ids;
        if (!$this->delivery_ids && !$this->allowNoDelivery) {
            $queryDeliveries = Delivery::find()
                ->joinWith(['country'], false)
                ->select([
                    'id' => Delivery::tableName() . '.id',
                ])->where([Delivery::tableName() . '.country_id' => $countryIds]);
            $deliveryIds = $queryDeliveries->column();
        }

        return !is_array($deliveryIds) ? [$deliveryIds] : $deliveryIds;
    }

    /**
     * @return string
     */
    protected function getCountryField()
    {
        switch ($this->countryType) {
            case self::COUNTRY_TYPE_DELIVERY:
                return Delivery::tableName() . '.country_id';
            case self::COUNTRY_TYPE_ORDER:
                return Order::tableName() . '.country_id';
            default:
                return Country::tableName() . '.id';
        }
    }

    /**
     * @return string
     */
    protected function getDeliveryField()
    {
        switch ($this->deliveryType) {
            case self::DELIVERY_TYPE_DELIVERY:
                return Delivery::tableName() . '.id';
            default:
                return DeliveryRequest::tableName() . '.delivery_id';
        }
    }
}
