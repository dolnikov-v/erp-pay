<?php
namespace app\modules\report\components\filters;

use app\modules\order\models\Order;
use app\modules\storage\models\StoragePart;
use app\modules\report\widgets\filters\CostPriceFilter as CostPriceFilterWidget;
use Yii;
use app\models\Product;

/**
 * Class CostPriceFilter
 * @package app\modules\report\components\filters
 */
class CostPriceFilter extends Filter
{
    public $from;
    public $to;

    /**
     * @var integer
     */
    public $product;

    /**
     * @var array
     */
    public $products = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->from = Yii::$app->formatter->asDate(time());
        $this->to = Yii::$app->formatter->asDate(time());

        $this->products = Product::find()
            ->orderBy('name')
            ->collection();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['from', 'to', 'product'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'date' => Yii::t('common', 'Дата'),
        ];
    }

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate()) {
            $timeFrom = Yii::$app->formatter->asTimestamp($this->from);
            $timeTo = Yii::$app->formatter->asTimestamp($this->to);

            if ($timeFrom) {
                $query->andFilterWhere(['>=', Order::tableName() . '.created_at', $timeFrom]);
            }

            if ($timeTo) {
                $query->andFilterWhere(['<=', Order::tableName() . '.created_at', $timeTo + 86399]);
            }

            if ($this->product) {
                $query->andFilterWhere([Product::tableName() . '.id' => $this->product]);
            }
        }
    }

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function applySubQuery($query, $params)
    {
        $this->load($params);

        if ($this->validate()) {

            if ($this->product) {
                $query->andFilterWhere([StoragePart::tableName() . '.product_id' => $this->product]);
            }
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return CostPriceFilterWidget::widget([
            'form' => $form,
            'model' => $this,
        ]);
    }
}
