<?php

namespace app\modules\report\components\filters;

use app\helpers\i18n\DateTimePicker as DateTimePickerHelper;
use app\modules\report\models\ReportDeliveryDebtsOld;
use Yii;

/**
 * Class DebtsMonthFilterOld
 * @package app\modules\report\components\filters
 */
class DebtsMonthFilterOld extends Filter
{
    /**
     * @var integer
     */
    public $from;
    /**
     * @var integer
     */
    public $to;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['from', 'to'], 'string'],
        ];
    }

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);
        if ($this->validate()) {
            $this->prepare();
            if ($this->from) {
                $query->andWhere(['>=', ReportDeliveryDebtsOld::tableName() . '.month', date('Y-m-d', $this->from)]);
            }

            if ($this->to) {
                $query->andWhere(['<=', ReportDeliveryDebtsOld::tableName() . '.month', date('Y-m-d', $this->to)]);
            }
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $this->prepare();
        return $form->field($this, 'date')->dateRangePicker('from', 'to', [
            'format' => DateTimePickerHelper::getMonthFormat(),
            'ranges' => [
                'hide' => true
            ],
            'formatFunc' => 'asMonth',
        ]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'from' => Yii::t('common', 'Период'),
            'to' => 'as',
            'date' => Yii::t('common', 'Период'),
        ];
    }

    /**
     * @param $date
     * @return array
     */
    public function parseDate($date)
    {
        return [
            'month' => substr($date, 0, 2),
            'year' => substr($date, 3)
        ];
    }


    protected function prepare()
    {
        if ($this->from && is_string($this->from)) {
            $date = $this->parseDate($this->from);
            $this->from = strtotime("{$date['year']}-{$date['month']}-01 00:00:00");
        }

        if ($this->to && is_string($this->to)) {
            $date = $this->parseDate($this->to);
            $timeTo = strtotime("{$date['year']}-{$date['month']}-01 00:00:00");
            $this->to = strtotime("+1month", $timeTo) - 1;
        }
    }
}
