<?php
namespace app\modules\report\components\filters;

use app\models\Product;
use app\modules\order\models\Order;
use app\modules\order\models\OrderProduct;
use app\modules\report\components\ReportForm;
use app\modules\report\widgets\filters\ProductFilter as ProductFilterWidget;
use Yii;
use yii\db\Expression;
use yii\db\Query;

/**
 * Class ProductFilter
 * @package app\modules\report\components\filters
 */
class ProductFilter extends Filter
{
    /**
     * @var integer
     */
    public $product;

    /**
     * @var ReportForm
     */
    public $reportForm;

    /**
     * @var array
     */
    public $products = [];

    /**
     * @var boolean
     */
    public $allProducts = false;

    /**
     * @inheritdoc
     */
    public function init()
    {
        if ($this->reportForm->getCountryFilter()->all || $this->allProducts) {
            $this->products = Product::find()->collection();
        } else {
            $countryStr = Yii::$app->user->country->id;
            $countryFilter = $this->reportForm->getCountryFilter();
            if (isset($countryFilter->ext_country_id) && is_numeric($countryFilter->ext_country_id)) {
                $countryStr = $countryFilter->ext_country_id;
            } else {
                $countryFilter = $this->reportForm->getCountrySelectFilter();
                if (is_array($countryFilter->country_ids)) {
                    $countryStr = implode(',', $countryFilter->country_ids);
                }
            }
            $cacheKey = 'products.country.' . $countryStr;
            $this->products = Yii::$app->cache->get($cacheKey);

            if ($this->products === false) {
                $this->products = [];

                $tableOrder = Order::tableName();
                $tableProduct = Product::tableName();
                $tableOrderProduct = OrderProduct::tableName();

                $items = (new Query())
                    ->select([
                        "product_id" => "DISTINCT($tableOrderProduct.product_id)",
                        "product_name" => "$tableProduct.name",
                    ])
                    ->from($tableOrder)
                    ->leftJoin($tableOrderProduct, "$tableOrderProduct.order_id = $tableOrder.id")
                    ->leftJoin($tableProduct, "$tableProduct.id = $tableOrderProduct.product_id")
                    ->where("$tableOrder.country_id in ($countryStr)")
                    ->andWhere(["is not", "$tableOrderProduct.product_id", null])
                    ->orderBy(["$tableOrderProduct.product_id" => SORT_ASC])
                    ->all();

                foreach ($items as $item) {
                    $this->products[$item['product_id']] = $item['product_name'];
                }

                Yii::$app->cache->set($cacheKey, $this->products, 10800);
            }
        }

        parent::init();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['product'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'product' => Yii::t('common', 'Товар'),
        ];
    }

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate()) {
            if ($this->product) {
                $queryFrom = OrderProduct::find()->select(new Expression('1'));
                $queryFrom->where([
                    OrderProduct::tableName().'.order_id' => new Expression(Order::tableName() . '.id'),
                    OrderProduct::tableName().'.product_id' => $this->product
                ]);
                $queryFrom->limit(1);
                $query->andWhere(['exists', $queryFrom]);
            }
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return ProductFilterWidget::widget([
            'form' => $form,
            'model' => $this
        ]);
    }
}
