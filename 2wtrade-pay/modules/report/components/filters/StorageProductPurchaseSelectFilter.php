<?php

namespace app\modules\report\components\filters;

use app\modules\storage\models\StorageProductPurchase;

/**
 * Class StorageProductPurchaseSelectFilter
 * @package app\modules\report\components\filters
 */
class StorageProductPurchaseSelectFilter extends ProductSelectFilter
{
    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->product_ids && $this->validate()) {
            $query->andWhere([StorageProductPurchase::tableName() . '.product_id' => $this->product_ids]);
        }
    }
}