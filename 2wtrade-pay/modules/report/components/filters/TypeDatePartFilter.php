<?php
namespace app\modules\report\components\filters;

use app\models\Country;
use app\modules\report\widgets\filters\TypeDatePartFilter as TypeDatePartFilterWidget;
use Yii;

/**
 * Class TypeDatePartFilter
 * @package app\modules\report\widgets\filters
 */
class TypeDatePartFilter extends Filter
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var array */
    public $typeDateFilter;

    /** @var string */
    public $type_date_part;

    const TYPE_DATE_PART_DAY = 'day';
    const TYPE_DATE_PART_WEEK = 'week';
    const TYPE_DATE_PART_MONTH = 'month';

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if (!$this->validate()) {
            $this->type_date_part = self::TYPE_DATE_PART_DAY;
        }

        if ($this->type_date_part == self::TYPE_DATE_PART_DAY) {
            $query->groupBy(Country::tableName() . '.id, yearcreatedat, monthcreatedat, weekcreatedat, daycreatedat');
        } elseif ($this->type_date_part == self::TYPE_DATE_PART_WEEK) {
            $query->groupBy(Country::tableName() . '.id, yearcreatedat, weekcreatedat');
        } elseif ($this->type_date_part == self::TYPE_DATE_PART_MONTH) {
            $query->groupBy(Country::tableName() . '.id, yearcreatedat, monthcreatedat');
        }
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->typeDateFilter = [
            self::TYPE_DATE_PART_DAY => Yii::t('common', 'День'),
            self::TYPE_DATE_PART_WEEK => Yii::t('common', 'Неделя'),
            self::TYPE_DATE_PART_MONTH => Yii::t('common', 'Месяц'),
        ];

        parent::init();
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'type_date_part' => Yii::t('common', 'Тип разбивки'),
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['type_date_part'], 'safe'],
        ];
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return TypeDatePartFilterWidget::widget([
            'form' => $form,
            'model' => $this,
        ]);
    }
}
