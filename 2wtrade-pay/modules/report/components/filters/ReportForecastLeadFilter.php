<?php
namespace app\modules\report\components\filters;

use app\modules\report\widgets\filters\ReportForecastLeadFilter as ReportForecastLeadFilterWidget;
use Yii;

/**
 * Class ReportForecastLeadFilter
 * @package app\modules\report\components\filters
 */
class ReportForecastLeadFilter extends DateFilter
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->from = Yii::$app->formatter->asDate(time());
        $this->to = Yii::$app->formatter->asDate(time());
    }

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return ReportForecastLeadFilterWidget::widget([
            'form' => $form,
            'model' => $this,
        ]);
    }
}
