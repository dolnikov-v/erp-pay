<?php
namespace app\modules\report\components\filters;

use app\modules\order\models\Order;
use app\modules\order\models\OrderLogisticList;
use app\modules\report\widgets\filters\OrderLogisticListFilter as ListWidget;
use Yii;

/**
 * Class OrderLogisticListFilter
 * @package app\modules\report\components\filters
 */
class OrderLogisticListFilter extends Filter
{
    public $listId;
    public $orderIds = [];

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['listId', 'integer'],
            ['listId',
                'exist',
                'skipOnError' => false,
                'targetClass' => OrderLogisticList::className(),
                'targetAttribute' => [
                    'listId' => 'id',
                ],
            ],
            ['listId', function($attribute){
                if (OrderLogisticList::findOne($this->listId)->country_id != Yii::$app->user->country->id) {
                    $this->addError($attribute, Yii::t('common', 'Лист из другой страны'));
                }
            }],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'listId' => Yii::t('common', 'Лист заказов'),
        ];
    }

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate()) {
            if ($this->listId) {
                $this->getOrderIds();
                $query->andFilterWhere([Order::tableName() . '.id' => $this->orderIds]);
            }
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return ListWidget::widget([
            'form' => $form,
            'model' => $this
        ]);
    }

    public function getOrderIds()
    {
        $this->orderIds = explode(',',OrderLogisticList::findOne($this->listId)->ids);
    }
}
