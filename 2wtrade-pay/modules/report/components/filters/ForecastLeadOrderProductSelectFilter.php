<?php

namespace app\modules\report\components\filters;

/**
 * Class ForecastLeadOrderProductSelectFilter
 * @package app\modules\report\widgets\filters
 */
class ForecastLeadOrderProductSelectFilter extends OrderProductSelectFilter
{
    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->product_ids && $this->validate()) {
            $query->andWhere(['product_id' => $this->product_ids]);
        }
    }
}