<?php
namespace app\modules\report\components\filters;

use app\modules\report\widgets\filters\DateFilterTopProduct as DateFilterWidget;
use Yii;

/**
 * Class Filter
 * @package app\modules\report\components\filters
 */
class DateFilterTopProduct extends Filter
{

    public $from;
    public $to;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->from = Yii::$app->formatter->asDate(time());
        $this->to = Yii::$app->formatter->asDate(time());

        if (Yii::$app->user->language->locale == 'es-ES') {                         // исключение для испанского представления даты
            $this->from = str_replace('/', '.', $this->from);
            $this->to = str_replace('/', '.', $this->to);
        }
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['from', 'to'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'date' => Yii::t('common', 'Дата'),
            'type' => Yii::t('common', 'Тип'),
        ];
    }

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */

    public function apply($query, $params)
    {
        $this->load($params);

        if (Yii::$app->user->language->locale == 'es-ES') {                         // исключение для испанского представления даты
            $this->from = str_replace('/', '.', $this->from);
            $this->to = str_replace('/', '.', $this->to);
        }

        if ($this->validate()) {
            $timeFrom = Yii::$app->formatter->asTimestamp($this->from);
            $timeTo = Yii::$app->formatter->asTimestamp($this->to);
            $filteredAttribute = '`order`.`created_at`';

            if ($timeFrom) {
                $query->andFilterWhere(['>=', $filteredAttribute, $timeFrom]);
            }

            if ($timeTo) {
                $query->andFilterWhere(['<=', $filteredAttribute, $timeTo + 86399]);
            }
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return DateFilterWidget::widget([
            'form' => $form,
            'model' => $this,
        ]);
    }

}
