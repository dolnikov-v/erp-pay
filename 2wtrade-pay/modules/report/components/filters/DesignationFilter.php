<?php
namespace app\modules\report\components\filters;

use app\modules\salary\models\Designation;
use app\modules\salary\models\Person;
use app\modules\report\components\ReportForm;
use app\modules\report\widgets\filters\DesignationFilter as DesignationFilterWidget;
use Yii;
use yii\base\InvalidParamException;

/**
 * Class DesignationFilter
 * @package app\modules\report\components\filters
 */
class DesignationFilter extends Filter
{
    const TYPE_CALL_CENTER_USER = 'cc_user';

    /**
     * @var integer
     */
    public $designation;

    /**
     * @var ReportForm
     */
    public $reportForm;

    /**
     * @var array
     */
    public $designations = [];

    /**
     * @var integer
     */
    public $officeId = 0;

    /**
     * @inheritdoc
     */
    public function init()
    {

        $this->designations = Designation::find()
            ->active()
            ->byOfficeId($this->officeId)
            ->orderBy([Designation::tableName() . '.name' => SORT_ASC])
            ->collection();

        parent::init();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['designation', 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'designation' => Yii::t('common', 'Должность')
        ];
    }

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate()) {
            if ($this->designation) {
                $query->andWhere([Person::tableName() . '.designation_id' => $this->designation]);
            }
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return DesignationFilterWidget::widget([
            'form' => $form,
            'model' => $this
        ]);
    }
}
