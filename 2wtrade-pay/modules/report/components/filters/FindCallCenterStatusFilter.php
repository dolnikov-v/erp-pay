<?php

namespace app\modules\report\components\filters;

use app\modules\callcenter\models\CallCenterFullRequest;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class FindCallCenterStatusFilter
 * @package app\modules\report\components\filters
 */
class FindCallCenterStatusFilter extends Filter
{
    public $status;

    /**
     * @var array
     */
    public $statuses;

    public function init()
    {
        $this->statuses = CallCenterFullRequest::statusLabels();
        parent::init();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['status', 'in', 'range' => array_keys($this->statuses)]
        ];
    }

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);
        if ($this->validate()) {
            if (!empty($this->status)) {
                $query->andWhere([CallCenterFullRequest::tableName() . '.status' => $this->status]);
            }
        }
    }

    /**
     * @param $params
     * @param $orders
     * @return mixed
     */
    public function applyParams($params, $orders)
    {
        $this->load($params);
        if ($this->validate()) {
            $orders = array_filter($orders, function ($item) {
                return $item->status == $this->status;
            });
        }
        return $orders;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return $form->field($this, 'status')->select2List($this->statuses, ['prompt' => Yii::t('common', 'Все')]);
    }
}
