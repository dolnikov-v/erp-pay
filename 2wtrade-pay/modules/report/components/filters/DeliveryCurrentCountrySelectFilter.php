<?php
namespace app\modules\report\components\filters;

use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\report\widgets\filters\DeliverySelectFilter as DeliverySelectWidget;
use Yii;

/**
 * Class DeliveryCurrentCountrySelectFilter
 * @package app\modules\report\widgets\filters
 */
class DeliveryCurrentCountrySelectFilter extends Filter
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var \app\modules\delivery\models\Delivery */
    public $deliveries;

    /** @var array */
    public $delivery_ids;

    /** @var bool */
    public $hasApply;

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->hasApply = $this->load($params);

        $delivery_ids = $this->delivery_ids;
        if ($delivery_ids && $this->validate()) {
            $query->andWhere([DeliveryRequest::tableName() . '.delivery_id' => $delivery_ids]);
        }
    }

    /**
     * @param array $params
     * @return array
     */
    public function applyParams($params)
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->deliveries = Delivery::find()
            ->andWhere([Delivery::tableName() . '.country_id' => Yii::$app->user->country->id])
            ->addSelect([
                Delivery::tableName() . '.id',
                'name' => Delivery::tableName() . '.name'
            ]);

        parent::init();

    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['delivery_ids'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'delivery_ids' => Yii::t('common', 'Курьерка'),
        ];
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return DeliverySelectWidget::widget([
            'form' => $form,
            'model' => $this,
        ]);
    }
}