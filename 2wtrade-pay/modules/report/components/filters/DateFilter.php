<?php

namespace app\modules\report\components\filters;

use app\modules\callcenter\models\CallCenterRequest;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\models\Lead;
use app\modules\order\models\Order;
use app\modules\callcenter\models\CallCenterCheckRequest;
use app\modules\report\models\Invoice;
use app\models\Timezone;
use app\modules\report\widgets\filters\DateFilter as DateFilterWidget;
use Yii;
use yii\base\InvalidParamException;

/**
 * Class Filter
 * @package app\modules\report\components\filters
 *
 * @property string $filteredAttribute
 */
class DateFilter extends Filter
{
    const TYPE_IGNORE = '';
    const TYPE_CREATED_AT = 'created_at';
    const TYPE_UPDATED_AT = 'updated_at';
    const TYPE_CALL_CENTER_SENT_AT = 'c_sent_at';
    const TYPE_CALL_CENTER_APPROVED_AT = 'c_approved_at';
    const TYPE_DELIVERY_CREATED_AT = 'd_created_at';
    const TYPE_DELIVERY_SENT_AT_NOT_EMPTY = 'd_sent_at_not_empty';
    const TYPE_DELIVERY_SENT_AT = 'd_sent_at';
    const TYPE_DELIVERY_FIRST_SENT_AT = 'd_first_sent_at';
    const TYPE_DELIVERY_TIME_FROM = 'delivery_time_from';
    const TYPE_DELIVERY_RETURNED_AT = 'd_returned_at';
    const TYPE_DELIVERY_APPROVED_AT = 'd_approved_at';
    const TYPE_DELIVERY_ACCEPTED_AT = 'd_accepted_at';
    const TYPE_DELIVERY_PAID_AT = 'd_paid_at';
    const TYPE_CHECK_SENT_AT = 'check_sent_at';
    const TYPE_CHECK_DONE_AT = 'check_done_at';
    const TYPE_INVOICE_CREATE = 'invoice_create';
    const TYPE_TS_SPAWN = 'ts_spawn';
    const TYPE_INVOICE_PERIOD = 'invoice_period';

    public $from;
    public $to;
    public $type = self::TYPE_CREATED_AT;
    public $timezone = Timezone::DEFAULT_OFFSET;

    public $types = [];
    public $possibleTypes = [];
    public $timezones = [];

    /**
     * Вкл/выкл показ селектора для вывод типа фильтра
     * @var bool
     */
    public $showSelectType = true;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if (is_null($this->from)) {
            $this->from = Yii::$app->formatter->asDate(time());
        }
        if (is_null($this->to)) {
            $this->to = Yii::$app->formatter->asDate(time());
        }

        if (isset(Yii::$app->user) && Yii::$app->user->isImplant) {
            $this->showSelectType = false;
        }


        if (isset(Yii::$app->user) && Yii::$app->user->language->locale == 'es-ES') {                         // исключение для испанского представления даты
            $this->from = str_replace('/', '.', $this->from);
            $this->to = str_replace('/', '.', $this->to);
        }

        $this->types = [
            self::TYPE_IGNORE => Yii::t('common', 'Игнорировать'),
            self::TYPE_CREATED_AT => Yii::t('common', 'Дата создания'),
            self::TYPE_UPDATED_AT => Yii::t('common', 'Дата обновления'),
            self::TYPE_CALL_CENTER_SENT_AT => Yii::t('common', 'Дата отправки (колл-центр)'),
            self::TYPE_CALL_CENTER_APPROVED_AT => Yii::t('common', 'Дата одобрения (колл-центр)'),
            self::TYPE_DELIVERY_CREATED_AT => Yii::t('common', 'Дата создания (курьерка)'),
            self::TYPE_DELIVERY_SENT_AT_NOT_EMPTY => Yii::t('common', 'Дата передачи заказа в курьерку'),
            // self::TYPE_DELIVERY_SENT_AT => Yii::t('common', 'Дата отправки (курьерка)'),
            self::TYPE_DELIVERY_FIRST_SENT_AT => Yii::t('common', 'Дата первой отправки (курьерка)'),
            self::TYPE_DELIVERY_TIME_FROM => Yii::t('common', 'Дата предполагаемой доставки'),
            self::TYPE_DELIVERY_RETURNED_AT => Yii::t('common', 'Дата возврата (курьерка)'),
            self::TYPE_DELIVERY_APPROVED_AT => Yii::t('common', 'Дата фактической доставки (курьерка)'),
            self::TYPE_DELIVERY_ACCEPTED_AT => Yii::t('common', 'Дата принятия (курьерка)'),
            self::TYPE_DELIVERY_PAID_AT => Yii::t('common', 'Дата оплаты (курьерка)'),
            self::TYPE_TS_SPAWN => Yii::t('common', 'Дата создания у партнера (лид)'),
        ];

        $this->possibleTypes = [
            self::TYPE_IGNORE,
            self::TYPE_CREATED_AT,
            self::TYPE_UPDATED_AT,
            self::TYPE_CALL_CENTER_SENT_AT,
            self::TYPE_CALL_CENTER_APPROVED_AT,
            self::TYPE_DELIVERY_CREATED_AT,
            self::TYPE_DELIVERY_SENT_AT_NOT_EMPTY,
            self::TYPE_DELIVERY_SENT_AT,
            self::TYPE_DELIVERY_FIRST_SENT_AT,
            self::TYPE_DELIVERY_TIME_FROM,
            self::TYPE_DELIVERY_RETURNED_AT,
            self::TYPE_DELIVERY_APPROVED_AT,
            self::TYPE_DELIVERY_ACCEPTED_AT,
            self::TYPE_DELIVERY_PAID_AT,
            self::TYPE_TS_SPAWN,
            self::TYPE_INVOICE_CREATE,
            self::TYPE_INVOICE_PERIOD,
        ];

        $this->timezones = Timezone::find()->customCollection('time_offset', 'name');
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['type', 'in', 'range' => $this->possibleTypes],
            [['from', 'to', 'timezone'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'date' => Yii::t('common', 'Дата'),
            'type' => Yii::t('common', 'Тип'),
            'timezone' => Yii::t('common', 'Временная зона'),
        ];
    }

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if (isset(Yii::$app->user) && Yii::$app->user->language->locale == 'es-ES') {                         // исключение для испанского представления даты
            $this->from = str_replace('/', '.', $this->from);
            $this->to = str_replace('/', '.', $this->to);
        }

        if ($this->type == self::TYPE_TS_SPAWN) {
            $query->leftJoin(Lead::tableName(), Lead::tableName() . '.order_id=' . Order::tableName() . '.id');
        }

        if ($this->validate()) {
            if ($this->type) {
                $dateFrom = $this->from ? (Yii::$app->formatter->asTimestamp($this->from) - $this->timezone) : null;
                $dateTo = $this->to ? (Yii::$app->formatter->asTimestamp($this->to) + 86399 - $this->timezone) : null;
                if ($this->type == self::TYPE_INVOICE_PERIOD) {
                    if ($dateFrom || $dateTo) {

                        $firstCondition = Invoice::tableName() . '.period_from is not null and ' . Invoice::tableName() . '.period_to is not null';
                        $secondCondition = Invoice::tableName() . '.period_from is not null and ' . Invoice::tableName() . '.period_to is null';
                        $thirdCondition = Invoice::tableName() . '.period_from is null and ' . Invoice::tableName() . '.period_to is not null';

                        $firstCheck = '';
                        $secondCheck = '';
                        $thirdCheck = '';
                        if ($dateFrom && $dateTo) {
                            $firstCheck = Invoice::tableName() . ".period_from <= {$dateTo} AND " . Invoice::tableName() . ".period_to >= {$dateFrom}";
                            $secondCheck = Invoice::tableName() . ".period_from <= {$dateTo}";
                            $thirdCheck = Invoice::tableName() . ".period_to >= {$dateFrom}";
                        } elseif ($dateFrom && !$dateTo) {
                            $firstCheck = Invoice::tableName() . ".period_to >= {$dateFrom}";
                            $secondCheck = "1";
                            $thirdCheck = Invoice::tableName() . ".period_to >= {$dateFrom}";
                        } elseif (!$dateFrom && $dateTo) {
                            $firstCheck = Invoice::tableName() . ".period_from <= {$dateTo}";
                            $secondCheck = Invoice::tableName() . ".period_from <= {$dateTo}";
                            $thirdCheck = '1';
                        }

                        $query->andWhere("CASE WHEN {$firstCondition} THEN {$firstCheck} WHEN {$secondCondition} THEN {$secondCheck} WHEN {$thirdCheck} THEN {$thirdCondition} ELSE 1 END");
                    }
                } else {
                    if ($this->from && $dateFrom) {
                        $query->andFilterWhere(['>=', $this->getFilteredAttribute(), $dateFrom]);
                    }

                    if ($this->to && $dateTo) {
                        $query->andFilterWhere(['<=', $this->getFilteredAttribute(), $dateTo]);
                    }
                }
            }
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     *
     * @return string
     * @throws \Exception
     */
    public function restore($form)
    {
        return DateFilterWidget::widget([
            'form' => $form,
            'model' => $this,
            'showSelectType' => $this->showSelectType,
        ]);
    }

    /**
     * @param array $params
     * @return string
     */
    public function getFilteredAttribute($params = null)
    {
        if ($params) {
            $this->load($params);
        }
        $attributes = [
            self::TYPE_CREATED_AT => Order::tableName() . '.created_at',
            self::TYPE_UPDATED_AT => Order::tableName() . '.updated_at',
            self::TYPE_CALL_CENTER_SENT_AT => CallCenterRequest::tableName() . '.sent_at',
            self::TYPE_CALL_CENTER_APPROVED_AT => CallCenterRequest::tableName() . '.approved_at',
            self::TYPE_DELIVERY_CREATED_AT => DeliveryRequest::tableName() . '.created_at',
            self::TYPE_DELIVERY_SENT_AT => 'COALESCE(' . DeliveryRequest::tableName() . '.second_sent_at,'
                . DeliveryRequest::tableName() . '.sent_at)',
            self::TYPE_DELIVERY_SENT_AT_NOT_EMPTY => 'COALESCE(' . DeliveryRequest::tableName() . '.second_sent_at,'
                . DeliveryRequest::tableName() . '.sent_at,'
                . DeliveryRequest::tableName() . '.created_at)',
            self::TYPE_DELIVERY_FIRST_SENT_AT => DeliveryRequest::tableName() . '.sent_at',
            self::TYPE_DELIVERY_TIME_FROM => Order::tableName() . '.delivery_time_from',
            self::TYPE_DELIVERY_RETURNED_AT => DeliveryRequest::tableName() . '.returned_at',
            self::TYPE_DELIVERY_APPROVED_AT => DeliveryRequest::tableName() . '.approved_at',
            self::TYPE_DELIVERY_ACCEPTED_AT => DeliveryRequest::tableName() . '.accepted_at',
            self::TYPE_DELIVERY_PAID_AT => DeliveryRequest::tableName() . '.paid_at',
            self::TYPE_CHECK_DONE_AT => CallCenterCheckRequest::tableName() . '.done_at',
            self::TYPE_CHECK_SENT_AT => CallCenterCheckRequest::tableName() . '.sent_at',
            self::TYPE_INVOICE_CREATE => Invoice::tableName() . '.period_from',
            self::TYPE_TS_SPAWN => Lead::tableName() . '.ts_spawn',
        ];

        if (!array_key_exists($this->type, $attributes)) {
            // когда у нас тип даты игнорировать вернем дату создания для группировки(?)
            // но фильтрации тут не будет т.к. if ($this->type) не пустит
            return Order::tableName() . '.created_at';
            //throw new InvalidParamException(Yii::t('common', 'Неизвестный тип в условии.'));
        }

        return $attributes[$this->type];
    }

    /**
     * @param $params
     * @return array
     */
    public function getFilteredTimestamps($params)
    {
        $out = [
            'from' => 0,
            'to' => 0,
        ];
        if ($params) {
            $this->load($params);
        }

        if (isset(Yii::$app->user) && Yii::$app->user->language->locale == 'es-ES') {                         // исключение для испанского представления даты
            $this->from = str_replace('/', '.', $this->from);
            $this->to = str_replace('/', '.', $this->to);
        }

        if ($this->from && $dateFrom = Yii::$app->formatter->asTimestamp($this->from)) {
            $out['from'] = $dateFrom;
        }

        if ($this->to && $dateTo = Yii::$app->formatter->asTimestamp($this->to)) {
            $out['to'] = $dateTo + 86399;
        }

        return $out;
    }
}
