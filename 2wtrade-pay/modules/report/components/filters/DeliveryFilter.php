<?php
namespace app\modules\report\components\filters;

use app\models\UserDelivery;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\models\Order;
use app\modules\report\components\ReportForm;
use app\modules\report\widgets\filters\DeliveryFilter as DeliveryFilterWidget;
use Yii;

/**
 * Class DeliveryFilter
 * @package app\modules\report\components\filters
 */
class DeliveryFilter extends Filter
{
    const TYPE_ORDER = 'order';
    const TYPE_DELIVERY = 'delivery';

    /**
     * @var integer
     */
    public $delivery;

    /**
     * @var ReportForm
     */
    public $reportForm;

    /**
     * @var array
     */
    public $deliveries = [];

    /**
     * @var string
     */
    public $type = self::TYPE_ORDER;

    /**
     * @inheritdoc
     */
    public function init()
    {

        if (!Yii::$app->user->isImplant) {
            if ($this->reportForm->countryFilter->all) {
                $this->deliveries = Delivery::find()
                    ->collection();
            } else {
                $this->deliveries = Delivery::find()
                    ->byCountryId(Yii::$app->user->country->id)
                    ->collection();
            }
        }
        else
        {
            $accessedDeliveries = UserDelivery::find()
                ->where(['=', UserDelivery::tableName() .'.user_id', Yii::$app->user->id])
                ->all();
            $ids = [];
            foreach ($accessedDeliveries as $item) {
                $ids[] = $item->delivery_id;
            }
            $this->deliveries = Delivery::find()
                ->andWhere(['IN', Delivery::tableName() .'.id', $ids])
                ->byCountryId(Yii::$app->user->country->id)
                ->collection();
        }

        parent::init();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['delivery', 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'delivery' => Yii::t('common', 'Служба доставки')
        ];
    }

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate()) {
            if ($this->delivery) {
                if ($this->type == self::TYPE_ORDER) {
                    $query->andFilterWhere([
                        'or',
                        [
                            'and',
                            DeliveryRequest::tableName() . '.delivery_id is not null',
                            DeliveryRequest::tableName() . '.delivery_id = ' . $this->delivery
                        ],
                        [
                            'and',
                            DeliveryRequest::tableName() . '.delivery_id is null',
                            Order::tableName() . '.pending_delivery_id = ' . $this->delivery
                        ]
                    ]);
                } elseif ($this->type == self::TYPE_DELIVERY) {
                    $query->andWhere([Delivery::tableName() . '.id' => $this->delivery]);
                }
            }
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return DeliveryFilterWidget::widget([
            'form' => $form,
            'model' => $this
        ]);
    }
}
