<?php
namespace app\modules\report\components\filters;

use app\models\Product;
use app\modules\order\models\Order;
use app\modules\order\models\LeadProduct;
use app\modules\report\widgets\filters\LeadProductFilter as LeadProductFilterWidget;
use Yii;
use yii\db\Expression;
use yii\db\Query;

/**
 * Class LeadProductFilter
 * @package app\modules\report\components\filters
 */
class LeadProductFilter extends ProductFilter
{
    /**
     * @var
     */
    protected $leadProduct;

    /**
     * @inheritdoc
     */
    public function init()
    {
        if ($this->reportForm->getCountryFilter()->all || $this->allProducts) {
            $this->products = Product::find()->collection();
        } else {
            $countryStr = Yii::$app->user->country->id;
            $countryFilter = $this->reportForm->getCountryFilter();
            if (isset($countryFilter->ext_country_id) && is_numeric($countryFilter->ext_country_id)) {
                $countryStr = $countryFilter->ext_country_id;
            } else {
                $countryFilter = $this->reportForm->getCountrySelectFilter();
                if (is_array($countryFilter->country_ids)) {
                    $countryStr = implode(',', $countryFilter->country_ids);
                }
            }
            $cacheKey = 'lead.products.country.' . $countryStr;
            $this->products = Yii::$app->cache->get($cacheKey);

            if ($this->products === false) {
                $this->products = [];

                $tableOrder = Order::tableName();
                $tableProduct = Product::tableName();
                $tableOrderProduct = LeadProduct::tableName();

                $items = (new Query())
                    ->select([
                        "product_id" => "DISTINCT($tableOrderProduct.product_id)",
                        "product_name" => "$tableProduct.name",
                    ])
                    ->from($tableOrder)
                    ->leftJoin($tableOrderProduct, "$tableOrderProduct.order_id = $tableOrder.id")
                    ->leftJoin($tableProduct, "$tableProduct.id = $tableOrderProduct.product_id")
                    ->where("$tableOrder.country_id in ($countryStr)")
                    ->andWhere(["is not", "$tableOrderProduct.product_id", null])
                    ->orderBy(["$tableOrderProduct.product_id" => SORT_ASC])
                    ->all();

                foreach ($items as $item) {
                    $this->products[$item['product_id']] = $item['product_name'];
                }

                Yii::$app->cache->set($cacheKey, $this->products, 10800);
            }
        }
    }
    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['leadProduct'], 'safe'],
        ];
    }

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate()) {
            if ($this->leadProduct) {
                $queryFrom = LeadProduct::find()->select(new Expression('1'));
                $queryFrom->where([
                    LeadProduct::tableName().'.order_id' => new Expression(Order::tableName() . '.id'),
                    LeadProduct::tableName().'.product_id' => $this->leadProduct
                ]);
                $queryFrom->limit(1);
                $query->andWhere(['exists', $queryFrom]);
            }
        }
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'leadProduct' => Yii::t('common', 'Товар лида'),
        ];
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return LeadProductFilterWidget::widget([
            'form' => $form,
            'model' => $this
        ]);
    }
}
