<?php
namespace app\modules\report\components\filters;

use app\models\Source;
use app\modules\order\models\Order;
use app\modules\report\widgets\filters\TypeOrderSourceFilter as TypeOrderSourceFilterWidget;
use Yii;

/**
 * Class TypeOrderSourceFilter
 * @package app\modules\report\widgets\filters
 */
class TypeOrderSourceFilter extends Filter
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var array */
    public $typeOrderSourceFilter;

    /** @var string */
    public $type_order_source;

    const TYPE_SOURCE_ALL = 'all';
    const TYPE_SOURCE_ADCOMBO = 'adcombo';
    const TYPE_SOURCE_JEEMPO = 'jeempo';
    const TYPE_SOURCE_2WSTORE = '2wstore';

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->type_order_source == self::TYPE_SOURCE_ADCOMBO) {
            $query->andFilterWhere(["LIKE", Order::tableName() .".source_id", Source::find()->where(['unique_system_name' => Source::SOURCE_ADCOMBO])->select('id')->scalar()]);
        } elseif ($this->type_order_source == self::TYPE_SOURCE_JEEMPO) {
            $query->andFilterWhere(["LIKE", Order::tableName() .".source_id", Source::find()->where(['unique_system_name' => Source::SOURCE_JEEMPO])->select('id')->scalar()]);
        } elseif ($this->type_order_source == self::TYPE_SOURCE_2WSTORE) {
            $query->andFilterWhere(["LIKE", Order::tableName() .".source_id", Source::find()->where(['unique_system_name' => Source::SOURCE_2WSTORE])->select('id')->scalar()]);
        }
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->typeOrderSourceFilter = [
            self::TYPE_SOURCE_ALL => Yii::t('common', 'Все'),
            self::TYPE_SOURCE_ADCOMBO => Yii::t('common', 'ADCOMBO'),
            self::TYPE_SOURCE_JEEMPO => Yii::t('common', 'JEEMPO'),
            self::TYPE_SOURCE_2WSTORE => Yii::t('common', '2WSTORE'),
        ];

        parent::init();
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'type_order_source' => Yii::t('common', 'Источник'),
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['type_order_source'], 'safe'],
        ];
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return TypeOrderSourceFilterWidget::widget([
            'form' => $form,
            'model' => $this,
        ]);
    }
}
