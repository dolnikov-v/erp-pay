<?php

namespace app\modules\report\components\filters;

use yii;
use yii\base\InvalidParamException;
use app\modules\order\models\OrderNotificationRequest;

/**
 * Class AnswerTypeSmsNotificationFilter
 * @package app\modules\report\components\filters
 */
class AnswerTypeSmsNotificationFilter extends Filter
{
    public $type_notification = OrderNotificationRequest::ANSWER_TYPE_ALL;

    public $types = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->types = [
            OrderNotificationRequest::ANSWER_TYPE_ALL => yii::t('common', 'Все'),
            OrderNotificationRequest::ANSWER_TYPE_ANSWERED => yii::t('common', 'Уведомления с ответом'),
            OrderNotificationRequest::ANSWER_TYPE_NOT_ANSWERED => yii::t('common', 'Уведомления без ответа'),
            OrderNotificationRequest::ANSWER_TYPE_READY_TO_SEND => yii::t('common', 'Готовые к отправке'),
            OrderNotificationRequest::ANSWER_TYPE_ERROR => yii::t('common', 'Ошибка отправки'),
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [
                'type_notification',
                'in',
                'range' => [
                    OrderNotificationRequest::ANSWER_TYPE_ALL,
                    OrderNotificationRequest::ANSWER_TYPE_ANSWERED,
                    OrderNotificationRequest::ANSWER_TYPE_NOT_ANSWERED,
                    OrderNotificationRequest::ANSWER_TYPE_READY_TO_SEND,
                    OrderNotificationRequest::ANSWER_TYPE_ERROR
                ]
            ]
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'type_notification' => Yii::t('common', 'Тип уведомления'),
        ];
    }

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate()) {

            if ($this->type_notification == OrderNotificationRequest::ANSWER_TYPE_ALL) {
                $query->orderBy(['id' => SORT_DESC]);
            } elseif ($this->type_notification == OrderNotificationRequest::ANSWER_TYPE_ANSWERED) {
                $query->andWhere(['is not', 'answered_at', null]);
            } elseif ($this->type_notification == OrderNotificationRequest::ANSWER_TYPE_NOT_ANSWERED) {
                $query->andWhere(['is', 'answered_at', null]);
            } else if ($this->type_notification == OrderNotificationRequest::ANSWER_TYPE_READY_TO_SEND) {
                $query->andWhere(['=', OrderNotificationRequest::tableName() . '.status', OrderNotificationRequest::SMS_STATUS_READY]);
            } else if ($this->type_notification == OrderNotificationRequest::ANSWER_TYPE_ERROR) {
                $query->andWhere(['=', OrderNotificationRequest::tableName(). '.status', OrderNotificationRequest::SMS_STATUS_ERROR]);
            } else {
                throw new InvalidParamException(Yii::t('common', 'Неизвестный тип опроса в условии.'));
            }

        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return $form->field($this, 'type_notification')->select2List($this->types);
    }

}
