<?php

namespace app\modules\report\components\filters;

use app\modules\report\models\ReportAdcombo;
use Yii;
use yii\base\InvalidParamException;
use app\modules\report\models\ReportCallCenter;

/**
 * Class AdComboDateFilter
 * @package app\modules\report\components\filters
 */
class AdcomboDateFilter extends DailyFilter
{
    const TYPE_REPORT_ADCOMBO = 'report_adcombo';
    const TYPE_REPORT_CALL_CENTER = 'report_report_adcombo';

    public $type;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->from = 0;
        $this->to = 0;

        $this->type = self::TYPE_REPORT_ADCOMBO;
    }

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate()) {
            $timeFrom = Yii::$app->formatter->asTimestamp($this->from);
            $timeTo = Yii::$app->formatter->asTimestamp($this->to);

            $filteredAttribute = $this->getFilteredAttribute();
            if ($timeFrom) {
                $query->andFilterWhere([
                    'or',
                    ['>=', $filteredAttribute . '.from', $timeFrom],
                    [
                        'and',
                        ['<', $filteredAttribute . '.from', $timeFrom],
                        ['>=', $filteredAttribute . '.to', $timeFrom]
                    ]
                ]);
            }

            if ($timeTo) {
                $query->andFilterWhere([
                    'or',
                    ['<=', $filteredAttribute . '.to', $timeTo + 86399],
                    [
                        'and',
                        ['>', $filteredAttribute . '.to', $timeTo + 86399],
                        ['<=', $filteredAttribute . '.from', $timeTo + 86399]
                    ]
                ]);
            }
        }
    }

    /**
     * @return string
     */
    protected function getFilteredAttribute()
    {
        $attributes = [
            self::TYPE_REPORT_ADCOMBO => ReportAdcombo::tableName(),
            self::TYPE_REPORT_CALL_CENTER => ReportCallCenter::tableName()
        ];

        if (!array_key_exists($this->type, $attributes)) {
            throw new InvalidParamException(Yii::t('common', 'Неизвестный тип в условии.'));
        }

        return $attributes[$this->type];
    }
}
