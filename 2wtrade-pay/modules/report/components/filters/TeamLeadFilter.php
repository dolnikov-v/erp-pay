<?php
namespace app\modules\report\components\filters;

use app\modules\salary\models\Person;
use app\modules\report\components\ReportForm;
use app\modules\report\widgets\filters\TeamLeadFilter as TeamLeadFilterWidget;
use Yii;
use yii\base\InvalidParamException;

/**
 * Class TeamLeadFilter
 * @package app\modules\report\components\filters
 */
class TeamLeadFilter extends Filter
{
    const TYPE_CALL_CENTER_USER = 'cc_user';

    /**
     * @var integer
     */
    public $teamLead;

    /**
     * @var ReportForm
     */
    public $reportForm;

    /**
     * @var array
     */
    public $teamLeads = [];

    /**
     * @var string
     */
    public $type;

    /**
     * @var integer
     */
    public $officeId = 0;

    /**
     * @inheritdoc
     */
    public function init()
    {

        $this->type = self::TYPE_CALL_CENTER_USER;

        if (!$this->isConsole()) {
            $this->teamLeads = Person::getPatentsCollection();
        }
        parent::init();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['teamLead', 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'teamLead' => Yii::t('common', 'Руководитель')
        ];
    }

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate()) {
            if ($this->teamLead) {
                $filteredAttribute = $this->getFilteredAttribute();
                $query->andWhere([
                    'or',
                    [$filteredAttribute => $this->teamLead],
                    [Person::tableName() . '.id' => $this->teamLead]
                ]);
            }
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return TeamLeadFilterWidget::widget([
            'form' => $form,
            'model' => $this
        ]);
    }

    /**
     * @return string
     */
    protected function getFilteredAttribute()
    {
        $attributes = [
            self::TYPE_CALL_CENTER_USER => Person::tableName() . '.parent_id',
        ];

        if (!array_key_exists($this->type, $attributes)) {
            throw new InvalidParamException(Yii::t('common', 'Неизвестный тип в условии.'));
        }

        return $attributes[$this->type];
    }
}
