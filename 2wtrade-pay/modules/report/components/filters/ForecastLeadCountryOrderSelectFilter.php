<?php
namespace app\modules\report\components\filters;

use app\models\User;
use app\modules\order\models\Order;

/**
 * Class ForecastLeadCountryOrderSelectFilter
 * @package app\modules\report\widgets\filters
 */
class ForecastLeadCountryOrderSelectFilter extends CountrySelectFilter
{
    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->hasApply = $this->load($params);

        $country_ids = $this->country_ids ?: array_keys(User::getAllowCountries());

        if ($country_ids && $this->validate()) {
            $query->andWhere([Order::tableName() . '.country_id' => $country_ids]);
        }
    }
}
