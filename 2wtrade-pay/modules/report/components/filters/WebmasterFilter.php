<?php
namespace app\modules\report\components\filters;

use app\modules\order\models\Order;
use app\modules\report\components\ReportForm;
use app\modules\report\widgets\filters\WebmasterFilter as WebmasterFilterWidget;
use Yii;
use yii\db\Query;

/**
 * Class WebmasterFilter
 * @package app\modules\report\components\filters
 */
class WebmasterFilter extends Filter
{
    /**
     * @var string
     */
    public $webmaster;

    /**
     * @var ReportForm
     */
    public $reportForm;

    /**
     * @var array
     */
    public $webmasters = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        $cacheKey = 'webmasters.country.' . Yii::$app->user->country->id;
        $this->webmasters = Yii::$app->cache->get($cacheKey);

        if ($this->webmasters === false) {
            $this->webmasters = [];

            $items = (new Query())
                ->select([
                    "webmaster_identifier" => "DISTINCT(webmaster_identifier)",
                ])
                ->from(Order::tableName())
                ->where(['country_id' => Yii::$app->user->country->id])
                ->orderBy(['webmaster_identifier' => SORT_ASC])
                ->all();

            foreach ($items as $item) {
                $this->webmasters[$item['webmaster_identifier']] = $item['webmaster_identifier'];
            }

            Yii::$app->cache->set($cacheKey, $this->webmasters, 3600);
        }

        parent::init();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['webmaster'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'webmaster' => Yii::t('common', 'Webmaster Id'),
        ];
    }

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate()) {
            if ($this->webmaster) {
                $query->andWhere(['webmaster_identifier' => $this->webmaster]);
            }
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return WebmasterFilterWidget::widget([
            'form' => $form,
            'model' => $this
        ]);
    }
}
