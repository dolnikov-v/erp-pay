<?php
namespace app\modules\report\components\filters;

use app\models\Country;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryChargesCalculator;
use app\modules\delivery\models\DeliveryContract;
use app\modules\report\components\ReportForm;
use app\modules\report\widgets\filters\ContractFilter as ContractWidget;
use Yii;

/**
 * Class ContractSelectFilter
 * @package app\modules\report\widgets\filters
 */
class ContractFilter extends Filter
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var DeliveryContract[] */
    public $contracts;

    /** @var integer */
    public $contract_id;

    /** @var bool */
    public $hasApply;

    /**
     * @var ReportForm
     */
    public $reportForm;

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->hasApply = $this->load($params);
    }

    /**
     * @param array $params
     * @return array
     */
    public function applyParams($params)
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->contracts = DeliveryContract::find()
            ->joinWith('chargesCalculatorModel', false)
            ->joinWith('delivery', false)
            ->joinWith('delivery.country', false)
            ->where([
                'is not',
                DeliveryContract::tableName() . '.charges_calculator_id',
                null
            ])
            ->byDates(date('Y-m-d'), date('Y-m-d'))
            ->orderBy([
                DeliveryChargesCalculator::tableName() . '.name' => SORT_ASC,
                Delivery::tableName() . '.name' => SORT_ASC,
                Country::tableName() . '.name' => SORT_ASC
            ])
            ->customCollection('id', function ($model) {
                /** @var DeliveryContract $model */
                return $model->chargesCalculatorModel->name . ' - ' . $model->delivery->name. ' - ' . Yii::t('common', $model->delivery->country->name);
            });

        parent::init();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['contract_id'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'contract_id' => Yii::t('common', 'Калькулятор контракта службы доставки'),
        ];
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return ContractWidget::widget([
            'form' => $form,
            'model' => $this,
        ]);
    }
}
