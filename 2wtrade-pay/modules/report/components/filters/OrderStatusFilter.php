<?php
namespace app\modules\report\components\filters;

use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use app\modules\report\components\ReportForm;
use app\modules\report\widgets\filters\OrderStatusFilter as OrderStatusFilterWidget;
use Yii;

/**
 * Class OrderStatusFilter
 * @package app\modules\report\components\filters
 */
class OrderStatusFilter extends Filter
{
    /**
     * @var integer | array
     */
    public $orderStatus;

    /**
     * @var ReportForm
     */
    public $reportForm;

    /**
     * @var array
     */
    public $orderStatuses = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->orderStatuses = OrderStatus::find()
            ->collection();

        parent::init();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['orderStatus', 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'orderStatus' => Yii::t('common', 'Статус заказа')
        ];
    }

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate()) {
            if ($this->orderStatus) {
                $query->andWhere([Order::tableName() . '.status_id' => $this->orderStatus]);
            }
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return OrderStatusFilterWidget::widget([
            'form' => $form,
            'model' => $this
        ]);
    }
}
