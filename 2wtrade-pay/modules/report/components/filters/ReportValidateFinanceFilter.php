<?php
namespace app\modules\report\components\filters;

use app\modules\report\widgets\filters\ReportValidateFinanceFilter as ReportValidateFinanceFilterWidget;
use Yii;

/**
 * Class ReportValidateFinanceFilter
 * @package app\modules\report\components\filters
 */
class ReportValidateFinanceFilter extends Filter
{
    public $from;
    public $to;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->from = Yii::$app->formatter->asDate(time());
        $this->to = Yii::$app->formatter->asDate(time());
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['from', 'to'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'date' => Yii::t('common', 'Дата'),
        ];
    }

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate()) {
            $timeFrom = Yii::$app->formatter->asTimestamp($this->from);
            $timeTo = Yii::$app->formatter->asTimestamp($this->to);

            if ($timeFrom) {
                $query->andFilterWhere(['>=', 'created_at', $timeFrom]);
            }

            if ($timeTo) {
                $query->andFilterWhere(['<=', 'created_at', $timeTo + 86399]);
            }
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return ReportValidateFinanceFilterWidget::widget([
            'form' => $form,
            'model' => $this,
        ]);
    }
}