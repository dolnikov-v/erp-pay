<?php
namespace app\modules\report\components\filters;

use app\modules\report\widgets\filters\ReportForecastDeliveryFilter as ReportForecastDeliveryFilterWidget;
use Yii;

/**
 * Class ReportForecastDeliveryFilter
 * @package app\modules\report\components\filters
 */
class ReportForecastDeliveryFilter extends DateFilter
{
    public $selectedType;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->from = Yii::$app->formatter->asDate(time());
        $this->to = Yii::$app->formatter->asDate(time());

        if (Yii::$app->user->language->locale == 'es-ES') {                         // исключение для испанского представления даты
            $this->from = str_replace('/', '.', $this->from);
            $this->to = str_replace('/', '.', $this->to);
        }

        $this->types = [
            self::TYPE_CREATED_AT => Yii::t('common', 'Дата создания'),
            self::TYPE_UPDATED_AT => Yii::t('common', 'Дата обновления'),
            self::TYPE_CALL_CENTER_SENT_AT => Yii::t('common', 'Дата отправки (колл-центр)'),
            self::TYPE_CALL_CENTER_APPROVED_AT => Yii::t('common', 'Дата одобрения (колл-центр)'),
            self::TYPE_DELIVERY_CREATED_AT => Yii::t('common', 'Дата создания (курьерка)'),
            self::TYPE_DELIVERY_SENT_AT => Yii::t('common', 'Дата отправки (курьерка)'),
            self::TYPE_DELIVERY_FIRST_SENT_AT => Yii::t('common', 'Дата первой отправки (курьерка)'),
            self::TYPE_DELIVERY_TIME_FROM => Yii::t('common', 'Дата доставки (курьерка)'),
            self::TYPE_DELIVERY_RETURNED_AT => Yii::t('common', 'Дата возврата (курьерка)'),
            self::TYPE_DELIVERY_APPROVED_AT => Yii::t('common', 'Дата утверждения (курьерка)'),
            self::TYPE_DELIVERY_ACCEPTED_AT => Yii::t('common', 'Дата принятия (курьерка)'),
            self::TYPE_DELIVERY_PAID_AT => Yii::t('common', 'Дата оплаты (курьерка)'),
        ];

        $this->selectedType = $this->getFilteredAttribute();
    }

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if (Yii::$app->user->language->locale == 'es-ES') {                         // исключение для испанского представления даты
            $this->from = str_replace('/', '.', $this->from);
            $this->to = str_replace('/', '.', $this->to);
        }

        if ($this->validate()) {
            if ($this->type) {
                $this->selectedType = $this->getFilteredAttribute();
                if ($this->from && $dateFrom = Yii::$app->formatter->asTimestamp($this->from)) {
                    $query->andFilterWhere(['>=', $this->selectedType, $dateFrom - 86400 * 28]);
                }

                if ($this->to && $dateTo = Yii::$app->formatter->asTimestamp($this->to)) {
                    $query->andFilterWhere(['<=', $this->selectedType, $dateTo - 86400 * 21]);
                }
                $query->addSelect(['id' => 'FROM_UNIXTIME(' . $this->selectedType . ', "%Y-%m-%d")']);
            }
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return ReportForecastDeliveryFilterWidget::widget([
            'form' => $form,
            'model' => $this,
        ]);
    }
}