<?php
namespace app\modules\report\components\filters;

use app\modules\order\models\Order;
use app\modules\report\widgets\filters\DuplicateOrderFilter as DuplicateOrderFilterWidget;
use Yii;
use \yii\db\Query;
use \yii\db\Expression;

/**
 * Class DuplicateOrderFilter
 * @package app\modules\report\components\filters
 */
class DuplicateOrderFilter extends Filter
{
    const TYPE_ALL = 'all';
    const TYPE_ONLY_DUPLICATE = 'only_duplicate';
    const TYPE_ONLY_NOT_DUPLICATE = 'only_not_duplicate';

    public $type_duplicate = self::TYPE_ALL;

    public $types_duplicate = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->types_duplicate = [
            self::TYPE_ALL => Yii::t('common', 'Все заказы'),
            self::TYPE_ONLY_DUPLICATE => Yii::t('common', 'Только дубли'),
            self::TYPE_ONLY_NOT_DUPLICATE => Yii::t('common', 'Только не дубли'),
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['type_duplicate', 'in', 'range' => array_keys($this->types_duplicate)],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'type_duplicate' => Yii::t('common', 'Учитывать заказы'),
        ];
    }

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate()) {
            switch ($this->type_duplicate) {
                case self::TYPE_ONLY_DUPLICATE:
                    $subOquery = new Query();
                    $subOquery->select([new Expression('1')]);
                    $subOquery->from(['duplicate_table' => Order::tableName()]);
                    $subOquery->where(['duplicate_table.duplicate_order_id' => new Expression(Order::tableName() . '.id')]);
                    $subOquery->limit(1);
                    $query->andWhere(['exists', $subOquery]);
                    break;
                case self::TYPE_ONLY_NOT_DUPLICATE:
                    $query->andWhere(['is', Order::tableName() . '.duplicate_order_id', null]);
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return DuplicateOrderFilterWidget::widget([
            'form' => $form,
            'model' => $this,
        ]);
    }
}