<?php
namespace app\modules\report\components\filters;

use app\modules\order\models\Order;
use app\modules\report\widgets\filters\CountryFilter as CountryFilterWidget;
use app\models\User;
use app\widgets\custom\Checkbox;
use Yii;
use yii\helpers\Html;

/**
 * Class CountryFilter
 * @package app\modules\report\components\filters
 */
class CountryFilter extends Filter
{
    public $all = false;

    public $ext_country_id = false;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['all', 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'all' => Yii::t('common', 'По всем странам'),
        ];
    }

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate()) {
            if (!$this->all) {
                if (is_numeric($this->ext_country_id)) {
                    $query->andFilterWhere([Order::tableName() . '.country_id' => $this->ext_country_id]);
                }
                else {
                    $query->andFilterWhere([Order::tableName() . '.country_id' => Yii::$app->user->country->id]);
                }
            }
        }
        if (!Yii::$app->user->isSuperadmin) {
            $query->andFilterWhere([Order::tableName() . '.country_id' => array_keys(User::getAllowCountries())]);
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return Checkbox::widget([
            'id' => Html::getInputId($this, 'all'),
            'style' => 'checkbox-inline',
            'label' => Yii::t('common', 'По всем странам'),
            'checked' => $this->all ? true : false,
            'name' => Html::getInputName($this, 'all'),
        ]);
    }
}
