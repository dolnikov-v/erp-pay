<?php
namespace app\modules\report\components\filters;

use Yii;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\report\widgets\filters\ForeignOperatorFilter as OperatorFilterWidget;

/**
 * Class ForeignOperatorFilter
 * @package app\modules\report\widgets\filters
 */
class ForeignOperatorFilter extends Filter
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var array|null */
    public $operators;

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->operators) {
            $query->andWhere([CallCenterRequest::tableName() . '.last_foreign_operator' => $this->operators]);
        }
    }

    /**
     * @param array $params
     * @return array
     */
    public function applyParams($params)
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['operators'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'operators' => Yii::t('common', 'Операторы'),
        ];
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return OperatorFilterWidget::widget([
            'form' => $form,
            'model' => $this,
        ]);
    }
}
