<?php
namespace app\modules\report\components\filters;

use app\modules\delivery\models\DeliveryRequest;
use app\modules\delivery\models\Delivery;
use app\modules\report\widgets\filters\ReportShipmentFilter as ReportShipmentFilterWidget;
use Yii;

/**
 * Class ReportShipmentFilter
 * @package app\modules\report\components\filters
 */
class ReportShipmentFilter extends Filter
{
    public $from;
    public $to;

    /**
     * @var integer
     */
    public $delivery;

    /**
     * @var array
     */
    public $deliveries = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->deliveries = Delivery::find()
            ->byCountryId(Yii::$app->user->country->id)
            ->collection();

        $this->from = Yii::$app->formatter->asDate(time());
        $this->to = Yii::$app->formatter->asDate(time());

        parent::init();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['from', 'to', 'delivery'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'date' => Yii::t('common', 'Дата'),
            'delivery' => Yii::t('common', 'Служба доставки'),
            'country' => Yii::t('common', 'Страна')
        ];
    }

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate()) {
            $timeFrom = Yii::$app->formatter->asTimestamp($this->from);
            $timeTo = Yii::$app->formatter->asTimestamp($this->to);

            if ($timeFrom) {
                $query->andFilterWhere(['>=', 'COALESCE(' . DeliveryRequest::tableName() . '.second_sent_at,' . DeliveryRequest::tableName() . '.sent_at,' . DeliveryRequest::tableName() . '.created_at)', $timeFrom]);
            }

            if ($timeTo) {
                $query->andFilterWhere(['<=', 'COALESCE(' . DeliveryRequest::tableName() . '.second_sent_at,' . DeliveryRequest::tableName() . '.sent_at,' . DeliveryRequest::tableName() . '.created_at)', $timeTo + 86399]);
            }

            if ($this->delivery) {
                $query->andFilterWhere([Delivery::tableName() . '.id' => $this->delivery]);
            }
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return ReportShipmentFilterWidget::widget([
            'form' => $form,
            'model' => $this,
        ]);
    }
}