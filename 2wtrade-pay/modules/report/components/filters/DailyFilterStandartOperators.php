<?php
namespace app\modules\report\components\filters;

use app\modules\order\models\Order;
use app\modules\report\models\AdcomboRequest;
use app\modules\report\widgets\filters\DailyFilterStandartOperators as DailyFilterWidget;
use Yii;
use yii\base\InvalidParamException;

/**
 * Class DailyFilterStandartOperators
 * @package app\modules\report\components\filters
 */
class DailyFilterStandartOperators extends Filter
{
    const TYPE_ADCOMBO_CREATED_AT = 'ad_created_at';
    const TYPE_ORDER_CREATED_AT = 'order_created_at';

    public $from;
    public $to;

    public $type;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->from = Yii::$app->formatter->asDate(time());
        $this->to = Yii::$app->formatter->asDate(time());

        if (Yii::$app->user->language->locale == 'es-ES') {                         // исключение для испанского представления даты
            $this->from = str_replace('/', '.', $this->from);
            $this->to = str_replace('/', '.', $this->to);
        }
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['from', 'to'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'date' => Yii::t('common', 'Дата'),
        ];
    }

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if (Yii::$app->user->language->locale == 'es-ES') {                         // исключение для испанского представления даты
            $this->from = str_replace('/', '.', $this->from);
            $this->to = str_replace('/', '.', $this->to);
        }

        if ($this->validate()) {
            $timeFrom = Yii::$app->formatter->asTimestamp($this->from);
            $timeTo = Yii::$app->formatter->asTimestamp($this->to);

            $filteredAttribute = $this->getFilteredAttribute();

            if ($timeFrom) {
                $query->andFilterWhere(['>=', $filteredAttribute, $timeFrom]);
            }

            if ($timeTo) {
                $query->andFilterWhere(['<=', $filteredAttribute, $timeTo + 86399]);
            }
        }
    }

    /**
     * @param array $params
     * @return array
     */
    public function applyParams($params)
    {
        $this->load($params);

        if ($this->validate()) {
            $timeFrom = Yii::$app->formatter->asTimestamp($this->from);
            $timeTo = Yii::$app->formatter->asTimestamp($this->to);

            return [
                'timeFrom' => $timeFrom,
                'timeTo' => $timeTo,
            ];
        }

        return [];
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return DailyFilterWidget::widget([
            'form' => $form,
            'model' => $this,
        ]);
    }

    /**
     * @return string
     */
    protected function getFilteredAttribute()
    {
        if(empty($this->type)){
            $this->type = self::TYPE_ORDER_CREATED_AT;
        }

        $attributes = [
            self::TYPE_ADCOMBO_CREATED_AT => AdcomboRequest::tableName() . '.created_at',
            self::TYPE_ORDER_CREATED_AT => Order::tableName().'.created_at'
        ];

        if (!array_key_exists($this->type, $attributes)) {
            throw new InvalidParamException(Yii::t('common', 'Неизвестный тип в условии.'));
        }

        return $attributes[$this->type];
    }
}
