<?php

namespace app\modules\report\components\filters;

use app\modules\delivery\models\DeliveryRequest;

/**
 * Class DebtsProcessMonthFilter
 * @package app\modules\report\components\filters
 */
class DebtsProcessMonthFilter extends DebtsInvoiceMonthFilter
{
    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);
        if ($this->validate()) {
            $this->prepare();
            if ($this->from) {
                $query->andWhere([
                    '>=',
                    'COALESCE(' . DeliveryRequest::tableName() . '.second_sent_at,'
                    . DeliveryRequest::tableName() . '.sent_at,'
                    . DeliveryRequest::tableName() . '.created_at)',
                    $this->from
                ]);
            }

            if ($this->to) {
                $query->andWhere([
                    '<',
                    'COALESCE(' . DeliveryRequest::tableName() . '.second_sent_at,'
                    . DeliveryRequest::tableName() . '.sent_at,'
                    . DeliveryRequest::tableName() . '.created_at)',
                    strtotime('+1month', $this->to)
                ]);
            }
        }
    }
}
