<?php
namespace app\modules\report\components\filters;

use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\report\components\ReportForm;
use app\models\Country;
use app\modules\report\widgets\filters\DebtFilter as DebtFilterWidget;
use Yii;

/**
 * Class DebtFilter
 * @package app\modules\report\components\filters
 */
class DebtFilter extends Filter
{
    /**
     * @var integer
     */
    public $delivery;

    /**
     * @var integer
     */
    public $country;

    /**
     * @var ReportForm
     */
    public $reportForm;

    /**
     * @var array
     */
    public $deliveries = [];

    /**
     * @var array
     */
    public $countries = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->country = Yii::$app->user->country->id;
        $this->deliveries = Delivery::find()
            ->active()
            ->collection();

        $this->countries = Country::find()
            ->active()
            ->collection();

        parent::init();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['delivery', 'country'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'delivery' => Yii::t('common', 'Служба доставки'),
            'country' => Yii::t('common', 'Страна')
        ];
    }

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate()) {
            if ($this->delivery) {
                $query->andWhere([DeliveryRequest::tableName() . '.delivery_id' => $this->delivery]);
            }
            if ($this->country) {
                $query->andWhere([Country::tableName() . '.id' => $this->country]);
            }
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return DebtFilterWidget::widget([
            'form' => $form,
            'model' => $this
        ]);
    }
}
