<?php

namespace app\modules\report\components\filters;

use app\modules\api\models\LeadStatus;
use app\modules\report\models\AdcomboRequest;
use Yii;

/**
 * Class FindAdcomboTypeFilter
 * @package app\modules\report\components\filters
 */
class FindAdcomboTypeFilter extends Filter
{
    public $state;

    public $states = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->states = LeadStatus::stateLabels();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['state', 'in', 'range' => array_keys(LeadStatus::stateLabels())]
        ];
    }

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);
        if ($this->validate()) {
            if (!empty($this->state)) {
                $query->andWhere([AdcomboRequest::tableName() . '.state' => $this->state]);
            }
        }
    }

    /**
     * @param array $params
     * @return string
     */
    public function applyParams($params)
    {
        $this->load($params);
        if ($this->validate()) {
            return $this->state;
        }
        return '';
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return $form->field($this, 'state')->select2List($this->states, ['prompt' => Yii::t('common', 'Все')]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'state' => Yii::t('common', 'Статус'),
        ];
    }
}
