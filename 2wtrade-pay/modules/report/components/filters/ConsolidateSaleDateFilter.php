<?php

namespace app\modules\report\components\filters;

use Yii;

/**
 * Class ConsolidateSaleDateFilter
 * @package app\modules\report\components\filters
 */
class ConsolidateSaleDateFilter extends DateFilter
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->types = [
            self::TYPE_CREATED_AT => Yii::t('common', 'Дата создания'),
            self::TYPE_UPDATED_AT => Yii::t('common', 'Дата обновления'),
            self::TYPE_CALL_CENTER_SENT_AT => Yii::t('common', 'Дата отправки (колл-центр)'),
            self::TYPE_CALL_CENTER_APPROVED_AT => Yii::t('common', 'Дата одобрения (колл-центр)'),
            self::TYPE_DELIVERY_CREATED_AT => Yii::t('common', 'Дата создания (курьерка)'),
            self::TYPE_DELIVERY_SENT_AT => Yii::t('common', 'Дата отправки (курьерка)'),
            self::TYPE_DELIVERY_RETURNED_AT => Yii::t('common', 'Дата возврата (курьерка)'),
            self::TYPE_DELIVERY_APPROVED_AT => Yii::t('common', 'Дата утверждения (курьерка)'),
            self::TYPE_DELIVERY_ACCEPTED_AT => Yii::t('common', 'Дата принятия (курьерка)'),
            self::TYPE_DELIVERY_PAID_AT => Yii::t('common', 'Дата оплаты (курьерка)'),
        ];
    }
}