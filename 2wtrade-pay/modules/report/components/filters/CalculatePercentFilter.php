<?php
namespace app\modules\report\components\filters;

use app\modules\report\components\ReportForm;
use app\widgets\custom\Checkbox;
use Yii;
use yii\helpers\Html;

/**
 * Class CalculatePercentFilter
 * @package app\modules\report\components\filters
 */
class CalculatePercentFilter extends Filter
{
    /**
     * @var string
     */
    public $percentOff;

    /**
     * @var ReportForm
     */
    public $reportForm;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['percentOff', 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'percentOff' => Yii::t('common', 'Не считать процент'),
        ];
    }

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate()) {
            $this->reportForm->withPercent = $this->percentOff;
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return Checkbox::widget([
            'id' => Html::getInputId($this, 'percentOff'),
            'style' => 'checkbox-inline',
            'label' => Yii::t('common', 'Не считать процент'),
            'checked' => $this->percentOff ? true : false,
            'name' => Html::getInputName($this, 'percentOff'),
        ]);
    }
}
