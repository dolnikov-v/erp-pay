<?php

namespace app\modules\report\components\filters;

use app\helpers\i18n\DateTimePicker as DateTimePickerHelper;
use app\modules\deliveryreport\models\PaymentOrder;
use Yii;

/**
 * Class DeliveryPaymentMonthFilter
 * @package app\modules\report\components\filters
 */
class DeliveryPaymentMonthFilter extends Filter
{
    /**
     * @var integer
     */
    public $from;
    /**
     * @var integer
     */
    public $to;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['from', 'to'], 'string'],
        ];
    }

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);
        if ($this->validate()) {
            $this->prepare();
            if ($this->from) {
                $query->andWhere([
                    '>=',
                    PaymentOrder::tableName() . '.paid_at',
                    $this->from
                ]);
            }

            if ($this->to) {
                $query->andWhere([
                    '<',
                    PaymentOrder::tableName() . '.paid_at',
                    $this->to + 86400
                ]);
            }
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $this->prepare();
        return $form->field($this, 'date')->dateRangePicker('from', 'to', [
            'format' => DateTimePickerHelper::getMonthFormat(),
            'ranges' => [
                'hide' => true
            ],
            'formatFunc' => 'asMonth',
        ]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'from' => Yii::t('common', 'Период'),
            'to' => 'as',
            'date' => Yii::t('common', 'Период'),
        ];
    }

    /**
     * @param $date
     * @return array
     */
    public function parseDate($date)
    {
        return [
            'month' => substr($date, 0, 2),
            'year' => substr($date, 3)
        ];
    }


    protected function prepare()
    {
        if ($this->from && is_string($this->from)) {
            $date = $this->parseDate($this->from);
            $d = new \DateTime($date['year'].'-'.$date['month'].'-01');
            $this->from = $d->getTimestamp();
        }

        if ($this->to && is_string($this->to)) {
            $date = $this->parseDate($this->to);

            $d = new \DateTime($date['year'].'-'.$date['month'].'-01');
            $d->modify('last day of this month 00:00:00');
            $this->to = $d->getTimestamp();
        }
    }
}
