<?php

namespace app\modules\report\components\filters;

use app\modules\callcenter\models\CallCenterFullRequest;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\report\models\AdcomboRequest;
use Yii;
use yii\base\InvalidParamException;

/**
 * Class FindAdcomboIdFilter
 * @package app\modules\report\components\filters
 */
class FindAdcomboIdFilter extends Filter
{
    const TYPE_ADCOMBO_FOREIGN_ID = 'adcombo_foreign_id';
    const TYPE_CALL_CENTER_FULL_REQUEST_FOREIGN_ID = 'cc_full_foreign_id';
    const TYPE_CALL_CENTER_REQUEST_FOREIGN_ID = 'cc_foreign_id';

    public $ids;

    public $type;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->type = self::TYPE_ADCOMBO_FOREIGN_ID;
        parent::init();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['ids'], 'string']
        ];
    }

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);
        if ($this->validate() && !empty($this->ids)) {
            $buffer = str_replace(' ', '', $this->ids);
            $buffer = explode(',', $buffer);
            if (count($buffer) > 0) {
                $filteredAttribute = $this->getFilteredAttribute();
                $query->andWhere([$filteredAttribute => $buffer]);
            }
        }

    }

    /**
     * @param array $params
     * @return array
     */
    public function applyParams($params)
    {
        $this->load($params);
        if ($this->validate() && !empty($this->ids)) {
            $buffer = str_replace(' ', '', $this->ids);
            return explode(',', $buffer);
        }
        return [];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'ids' => Yii::t('common', 'Внешний номер'),
        ];
    }


    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return $form->field($this, 'ids')->textInput();
    }

    /**
     * @return string
     */
    protected function getFilteredAttribute()
    {
        $attributes = [
            self::TYPE_ADCOMBO_FOREIGN_ID => AdcomboRequest::tableName() . '.foreign_id',
            self::TYPE_CALL_CENTER_FULL_REQUEST_FOREIGN_ID => CallCenterFullRequest::tableName() . '.foreign_id',
            self::TYPE_CALL_CENTER_REQUEST_FOREIGN_ID => CallCenterRequest::tableName() . '.foreign_id',
        ];

        if (!array_key_exists($this->type, $attributes)) {
            throw new InvalidParamException(Yii::t('common', 'Неизвестный тип в условии.'));
        }

        return $attributes[$this->type];
    }
}
