<?php
namespace app\modules\report\components\filters;

use app\models\User;
use app\modules\report\widgets\filters\CuratorUserFilter as CuratorUserWidget;
use Yii;
use yii\base\Widget;

/**
 * Class CuratorUserFilter
 * @package app\modules\report\widgets\filters
 */
class CuratorUserFilter extends Filter
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var \app\models\Country */
    public $curatorUsers;

    /** @var array */
    public $user_ids;

    const AUTH_ITEM_COUNTRY_CURATOR = 'country.curator';
    const AUTH_ITEM_CALLCENTER_MANAGER = 'callcenter.manager';

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        $this->validate();
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        $users = User::find()
            ->join('LEFT JOIN', 'auth_assignment', 'auth_assignment.user_id = user.id')
            ->where(['auth_assignment.item_name' => [self::AUTH_ITEM_COUNTRY_CURATOR, self::AUTH_ITEM_CALLCENTER_MANAGER]])
            ->andWhere(["=", USER::tableName() .".status", 0])
            ->all();

        foreach ($users as $user) {
            if (Yii::$app->user->isSuperadmin || Yii::$app->user->isBusinessAnalyst) {
                $this->curatorUsers[$user->id] = $user->username;
            }
            else {
                if ($user->id == Yii::$app->user->id) {
                    $this->curatorUsers[$user->id] = $user->username;
                }
            }
        }

        parent::init();
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'user_ids' => Yii::t('common', 'Пользователи'),
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['user_ids'], 'safe'],
        ];
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return CuratorUserWidget::widget([
            'form' => $form,
            'model' => $this,
        ]);
    }
}
