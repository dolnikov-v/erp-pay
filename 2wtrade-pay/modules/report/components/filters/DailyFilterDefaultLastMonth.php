<?php
namespace app\modules\report\components\filters;

use Yii;

/**
 * Class DailyFilter
 * @package app\modules\report\components\filters
 */
class DailyFilterDefaultLastMonth extends DailyFilter
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['from', 'to'], function(){
                if ($this->from == $this->to){
                    $this->from = date('d-m-Y', strtotime($this->to) - 60 * 60 * 24 * 31);
                }
            }],
        ];
    }

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if (Yii::$app->user->language->locale == 'es-ES') {                         // исключение для испанского представления даты
            $this->from = str_replace('/', '.', $this->from);
            $this->to = str_replace('/', '.', $this->to);
        }

        $this->validate();

        parent::apply($query, $params);

    }
}
