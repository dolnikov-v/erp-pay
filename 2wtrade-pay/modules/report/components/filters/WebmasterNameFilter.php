<?php

namespace app\modules\report\components\filters;

use app\modules\order\models\Order;

/**
 * Class WebmasterNameFilter
 * @package app\modules\report\components\filters
 */
class WebmasterNameFilter extends Filter
{
    /**
     * @var null|string
     */
    public $name = null;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['name', 'string']
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'name' => \Yii::t('common', 'Id web-мастера'),
        ];
    }

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate()) {
            $query->andFilterWhere(['like', Order::tableName() . '.webmaster_identifier', $this->name]);
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return $form->field($this, 'name')->input('text');
    }
}