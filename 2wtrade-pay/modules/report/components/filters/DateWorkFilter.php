<?php

namespace app\modules\report\components\filters;

use app\modules\callcenter\models\CallCenterWorkTime;
use Yii;
use yii\base\InvalidParamException;
use app\modules\report\widgets\filters\DailyFilter as DailyFilterWidget;

/**
 * Class DateWorkFilter
 * @package app\modules\report\components\filters
 */
class DateWorkFilter extends DailyFilter
{
    const TYPE_WORK_DATE = 'work_date';

    public $type;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->from = Yii::$app->formatter->asDate(date('Y-m-01'));
        $this->to = Yii::$app->formatter->asDate(date('Y-m-d'));

        $this->type = self::TYPE_WORK_DATE;
    }

    /**
     * @param \yii\db\Query $query
     * @param array $params
     * @return array
     */
    public function apply($query, $params)
    {
        $this->load($params);

        $return = [];

        if ($this->validate()) {
            $dateFrom = Yii::$app->formatter->asDate($this->from, 'php:Y-m-d');
            $dateTo = Yii::$app->formatter->asDate($this->to, 'php:Y-m-d');

            if ($this->validate()) {
                if ($this->type) {
                    if ($dateFrom) {
                        $return[] = ['>=', $this->getFilteredAttribute(), $dateFrom];
                    }
                    if ($dateTo) {
                        $return[] = ['<=', $this->getFilteredAttribute(), $dateTo];
                    }
                }
            }
        }

        return $return;
    }

    /**
     * @return string
     */
    protected function getFilteredAttribute()
    {
        $attributes = [
            self::TYPE_WORK_DATE => CallCenterWorkTime::tableName() . '.date',
        ];

        if (!array_key_exists($this->type, $attributes)) {
            throw new InvalidParamException(Yii::t('common', 'Неизвестный тип в условии.'));
        }

        return $attributes[$this->type];
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {

        $rangeList = [
            'current_month' => Yii::t('common', 'Текущий месяц'),
            'before_month' => Yii::t('common', 'Прошлый месяц'),
        ];

        for ($i = 1; $i <= 7; $i++) {
            $rangeList['before_month_' . $i] = Yii::$app->formatter->asDate(strtotime('-' . $i . ' month'), 'php:M Y');
        }


        return DailyFilterWidget::widget([
            'form' => $form,
            'model' => $this,
            'options' => [
                'ranges' => [
                    'list' => $rangeList
                ]
            ]
        ]);
    }
}
