<?php
namespace app\modules\report\components\filters;

use app\models\Product;
use app\modules\order\models\Order;
use app\modules\order\models\OrderProduct;
use app\modules\report\widgets\filters\ProductSelectFilter as ProductSelectWidget;
use Yii;
use yii\db\Expression;

/**
 * Class ProductSelectFilter
 * @package app\modules\report\widgets\filters
 */
class ProductSelectFilter extends Filter
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var \app\models\Product */
    public $products;

    /** @var array */
    public $product_ids;

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->product_ids && $this->validate()) {

            $subQuery = OrderProduct::find()->select(new Expression(1));
            $subQuery->where([
                'order_id' => new Expression(Order::tableName() . '.id'),
                'product_id' => $this->product_ids
            ]);
            $subQuery->limit(1);
            $query->andWhere(['exists', $subQuery]);
        }
    }

    /**
     * @param array $params
     * @return array
     */
    public function applyParams($params)
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->products = Product::find();
        parent::init();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['product_ids'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'product_ids' => Yii::t('common', 'Товар'),
        ];
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return ProductSelectWidget::widget([
            'form' => $form,
            'model' => $this,
        ]);
    }
}
