<?php
namespace app\modules\report\components\filters;

use app\modules\access\widgets\Country as CountryWidget;
use app\modules\delivery\models\Delivery;
use app\models\Country;
use app\modules\report\widgets\filters\DeliverySelectFilter as DeliverySelectWidget;
use Yii;
use app\models\User;

/**
 * Class DelievrySelectFilter
 * @package app\modules\report\widgets\filters
 */
class DeliverySelectFilter extends Filter
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var \app\modules\delivery\models\Delivery */
    public $deliveries;

    /** @var array */
    public $delivery_ids;

    /** @var bool */
    public $hasApply;

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->hasApply = $this->load($params);

        $delivery_ids = $this->delivery_ids;
        if ($delivery_ids && $this->validate()) {
            $query->andWhere([Delivery::tableName() . '.id' => $delivery_ids]);
        }
    }

    /**
     * @param array $params
     * @return array
     */
    public function applyParams($params)
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->deliveries = Delivery::find()
            ->leftJoin(Country::tableName(), Country::tableName() . '.id=' . Delivery::tableName() . '.country_id')
            ->andWhere(['in', Delivery::tableName() . '.country_id', array_keys(User::getAllowCountries())])
            ->addSelect([
                Delivery::tableName() . '.id',
                'name' => 'CONCAT_WS(" ", ' . Country::tableName() . '.name' . ', ' . Delivery::tableName() . '.name)'
            ]);

        parent::init();

    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['delivery_ids'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'delivery_ids' => Yii::t('common', 'Курьерка'),
        ];
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return DeliverySelectWidget::widget([
            'form' => $form,
            'model' => $this,
        ]);
    }
}
