<?php
namespace app\modules\report\components\filters;

use app\modules\report\widgets\filters\ReceivableFilter as ReceivableFilterWidget;
use Yii;

/**
 * Class ReceivableFilter
 * @package app\modules\report\components\filters
 */
class ReceivableFilter extends Filter
{
    public $from;
    public $to;
    public $country;
    public $delivery;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->from = Yii::$app->formatter->asDate(time() - 2 * 86400);
        $this->to = Yii::$app->formatter->asDate(time() - 86400);
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['from', 'to'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'date' => Yii::t('common', 'Дата'),
            'country' => Yii::t('common', 'Страна'),
            'delivery' => Yii::t('common', 'Служба доставки'),
        ];
    }

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        ;
    }

    /**
     * @param array $params
     * @return array|boolean
     */
    public function applyParams($params)
    {
        $this->load($params);

        if ($this->validate()) {
            $timeFrom = Yii::$app->formatter->asTimestamp($this->from);
            $timeTo = Yii::$app->formatter->asTimestamp($this->to);

            if ($timeFrom && $timeTo) {
                return [
                    'from' => $timeFrom,
                    'to' => $timeTo,
                ];
            }
        }
        return false;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return ReceivableFilterWidget::widget([
            'form' => $form,
            'model' => $this,
        ]);
    }
}
