<?php
namespace app\modules\report\components\filters;

use app\modules\report\components\ReportForm;
use app\widgets\custom\Checkbox;
use Yii;
use yii\helpers\Html;

/**
 * Class CalculateDollarFilter
 * @package app\modules\report\components\filters
 */
class CalculateDollarFilter extends Filter
{
    /**
     * @var string
     */
    public $dollar;

    /**
     * @var ReportForm
     */
    public $reportForm;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['dollar', 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'dollar' => Yii::t('common', 'Конвертировать в доллар'),
        ];
    }

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate()) {
            $this->reportForm->dollar = $this->dollar;
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return Checkbox::widget([
            'id' => Html::getInputId($this, 'dollar'),
            'style' => 'checkbox-inline',
            'label' => Yii::t('common', 'Конвертировать в доллар'),
            'checked' => $this->dollar ? true : false,
            'name' => Html::getInputName($this, 'dollar'),
        ]);
    }
}
