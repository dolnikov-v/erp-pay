<?php

namespace app\modules\report\components\filters;

use app\modules\deliveryreport\models\PaymentOrder;

/**
 * Class PaymentNameFilter
 * @package app\modules\report\components\filters
 */
class PaymentNameFilter extends Filter
{
    /**
     * @var null|string
     */
    public $name = null;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['name', 'string']
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'name' => \Yii::t('common', 'Назначение платежа'),
        ];
    }

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate()) {
            $query->andFilterWhere(['like', PaymentOrder::tableName() . '.name', "%{$this->name}%", false]);
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return $form->field($this, 'name')->input('text');
    }
}