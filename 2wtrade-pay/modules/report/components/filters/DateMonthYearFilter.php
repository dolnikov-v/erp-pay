<?php
namespace app\modules\report\components\filters;

use Yii;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\report\widgets\filters\DateMonthYearFilter as DateMonthYearFilterWidget;

/**
 * Class DateMonthYearFilter
 * @package app\modules\report\components\filters
 */
class DateMonthYearFilter extends Filter
{
    public $date;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->date = Yii::$app->formatter->asMonth(time());
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['date'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'date' => Yii::t('common', 'Дата'),
        ];
    }

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate() && $this->date) {
            $field = 'COALESCE(' . DeliveryRequest::tableName() . '.second_sent_at,' . DeliveryRequest::tableName() . '.sent_at)';
            $data = $this->parseDate();
            $timeFrom = strtotime($data['year'] . '-' . $data['month'] . '-01');
            $timeTo = strtotime('+1 month', $timeFrom);
            $query->andFilterWhere(['>=', $field, $timeFrom]);
            $query->andFilterWhere(['<=', $field, $timeTo]);
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return DateMonthYearFilterWidget::widget([
            'form' => $form,
            'model' => $this,
        ]);
    }

    public function parseDate()
    {
        return [
            'month' => substr($this->date, 0, 2),
            'year' => substr($this->date, 3)
        ];
    }
}
