<?php
namespace app\modules\report\components\filters;

use app\models\Country;
use app\models\User;
use app\modules\report\widgets\filters\CountrySelectFilter as CountrySelectWidget;
use Yii;

/**
 * Class CountrySelectFilter
 * @package app\modules\report\widgets\filters
 */
class CountrySelectFilter extends Filter
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var \app\models\Country */
    public $countries;

    /** @var array */
    public $country_ids;

    /** @var bool */
    public $hasApply;

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->hasApply = $this->load($params);

        $country_ids = $this->country_ids ?: array_keys(User::getAllowCountries());

        if ($country_ids && $this->validate()) {
            $query->andWhere([Country::tableName() . '.id' => $country_ids]);
        }
    }

    /**
     * @param array $params
     * @return array
     */
    public function applyParams($params)
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        $allowCountries = array_keys(User::getAllowCountries());
        $this->countries = Country::find()
            ->where(['IN', Country::tableName() . '.id', $allowCountries])
            ->andWhere(['active' => 1])
            ->orderBy(['name' => SORT_ASC]);
        parent::init();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['country_ids'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'country_ids' => Yii::t('common', 'Страна'),
        ];
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return CountrySelectWidget::widget([
            'form' => $form,
            'model' => $this,
        ]);
    }
}
