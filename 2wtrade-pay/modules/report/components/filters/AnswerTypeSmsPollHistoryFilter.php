<?php

namespace app\modules\report\components\filters;

use yii;
use yii\base\InvalidParamException;
use app\modules\report\models\SmsPollHistory;

/**
 * Class AnswerTypeSmsPollHistoryFilter
 * @package app\modules\report\components\filters
 */
class AnswerTypeSmsPollHistoryFilter extends Filter
{
    public $type_poll = SmsPollHistory::ANSWER_TYPE_ALL;

    public $types_poll = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->types_poll = [
            SmsPollHistory::ANSWER_TYPE_ALL => yii::t('common', 'Все'),
            SmsPollHistory::ANSWER_TYPE_ANSWERED => yii::t('common', 'Опросы с ответом'),
            SmsPollHistory::ANSWER_TYPE_NOT_ANSWERED => yii::t('common', 'Опросы без ответа'),
            SmsPollHistory::ANSWER_TYPE_READY_TO_SEND => yii::t('common', 'Готовые к отправке'),
            SmsPollHistory::ANSWER_TYPE_ERROR => yii::t('common', 'Ошибка отправки'),
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [
                'type_poll',
                'in',
                'range' => [
                    SmsPollHistory::ANSWER_TYPE_ALL,
                    SmsPollHistory::ANSWER_TYPE_ANSWERED,
                    SmsPollHistory::ANSWER_TYPE_NOT_ANSWERED,
                    SmsPollHistory::ANSWER_TYPE_READY_TO_SEND,
                    SmsPollHistory::ANSWER_TYPE_ERROR
                ]
            ]
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'type_poll' => Yii::t('common', 'Тип опроса'),
        ];
    }

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate()) {
            if ($this->type_poll == SmsPollHistory::ANSWER_TYPE_ALL) {
                $query->orderBy(['id' => SORT_DESC]);
            } elseif ($this->type_poll == SmsPollHistory::ANSWER_TYPE_ANSWERED) {
                $query->andWhere(['>', 'answered_at', 0]);
            } elseif ($this->type_poll == SmsPollHistory::ANSWER_TYPE_NOT_ANSWERED) {
                $query->andWhere(['=', 'answered_at', 0]);
            } else if ($this->type_poll == SmsPollHistory::ANSWER_TYPE_READY_TO_SEND) {
                $query->andWhere(['=', SmsPollHistory::tableName() . '.status', SmsPollHistory::HISTORY_STATUS_READY]);
            } else if ($this->type_poll == SmsPollHistory::ANSWER_TYPE_ERROR) {
                $query->andWhere(['=', SmsPollHistory::tableName(). '.status', SmsPollHistory::HISTORY_STATUS_ERROR]);
            } else {
                throw new InvalidParamException(Yii::t('common', 'Неизвестный тип опроса в условии.'));
            }

        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return $form->field($this, 'type_poll')->select2List($this->types_poll);
    }

}
