<?php

namespace app\modules\report\components\filters;

use Yii;
use yii\base\Model;

/**
 * Class Filter
 * @package app\modules\report\components\filters
 */
abstract class Filter extends Model
{
    public $formName = 's';

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public abstract function apply($query, $params);

    /**
     * @param \app\components\widgets\ActiveForm $form
     */
    public abstract function restore($form);

    /**
     * @return string
     */
    public function formName()
    {
        return $this->formName;
    }

    /**
     * @return bool
     */
    public function isConsole()
    {
        return Yii::$app instanceof \yii\console\Application;
    }
}
