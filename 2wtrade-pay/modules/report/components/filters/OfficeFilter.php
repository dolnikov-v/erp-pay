<?php
namespace app\modules\report\components\filters;

use app\modules\report\components\ReportForm;
use app\modules\report\widgets\filters\OfficeFilter as OfficeFilterWidget;
use app\modules\salary\models\Office;
use app\modules\salary\models\Person;
use Yii;
use yii\base\InvalidParamException;

/**
 * Class OfficeFilter
 * @package app\modules\report\components\filters
 */
class OfficeFilter extends Filter
{
    const TYPE_CALL_CENTER_OFFICE_ID = 'office_id';

    /**
     * @var array
     */
    public $office;

    /**
     * @var ReportForm
     */
    public $reportForm;

    /**
     * @var array
     */
    public $offices = [];

    /**
     * @var string
     */
    public $type;

    public $onlyType;

    /**
     * @var bool
     */
    public $selectFirst = false;

    /**
     * @inheritdoc
     */
    public function init()
    {
        if (!$this->isConsole()) {
            $query = Office::find()
                ->active()
                ->bySystemUserCountries();

            if ($this->onlyType) {
                $query->andWhere(['type' => $this->onlyType]);
            }
            $this->offices = $query->collection();

            foreach ($this->offices as &$office) {
                $office = Yii::t('common', $office);
            }

            if ($this->selectFirst) {
                reset($this->offices);
                $firstCallCenter = key($this->offices);

                $this->office[] = $firstCallCenter;
            }
        }

        $this->type = self::TYPE_CALL_CENTER_OFFICE_ID;
        parent::init();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['office', 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'office' => Yii::t('common', 'Офис')
        ];
    }

    /**
     * @param \yii\db\Query $query
     * @param array $params
     * @param boolean $applyToQuery фильтировать в запрос или только для смежных фильтров
     */
    public function apply($query, $params, $applyToQuery = true)
    {
        $this->load($params);
        $filteredAttribute = $this->getFilteredAttribute();
        if ($this->validate()) {
            if ($this->office && $applyToQuery) {
                $query->andWhere([$filteredAttribute => $this->office]);
            }
        }
        if (!$this->isConsole() && $applyToQuery) {
            $query->andWhere([$filteredAttribute => array_keys($this->offices)]);
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return OfficeFilterWidget::widget([
            'form' => $form,
            'model' => $this
        ]);
    }

    /**
     * @return string
     */
    protected function getFilteredAttribute()
    {
        $attributes = [
            self::TYPE_CALL_CENTER_OFFICE_ID => Person::tableName() . '.office_id',
        ];

        if (!array_key_exists($this->type, $attributes)) {
            throw new InvalidParamException(Yii::t('common', 'Неизвестный тип в условии.'));
        }

        return $attributes[$this->type];
    }
}
