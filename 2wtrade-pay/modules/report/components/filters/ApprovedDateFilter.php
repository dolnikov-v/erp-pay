<?php
namespace app\modules\report\components\filters;

use app\modules\callcenter\models\CallCenterRequest;
use app\modules\order\models\Order;
use Yii;
use yii\base\InvalidParamException;

/**
 * Class ApprovedDateFilter
 * @package app\modules\report\components\filters
 */
class ApprovedDateFilter extends DateFilter
{
    public $type = self::TYPE_CALL_CENTER_APPROVED_AT;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // начиная с 8:00 и по местному времени
        $this->from = Yii::$app->formatter->asDate(time() - 86400 + 60*60*8 + Yii::$app->user->timezone->time_offset);
        $this->to = Yii::$app->formatter->asDate(time() + Yii::$app->user->timezone->time_offset);

        if (Yii::$app->user->language->locale == 'es-ES') {                         // исключение для испанского представления даты
            $this->from = str_replace('/', '.', $this->from);
            $this->to = str_replace('/', '.', $this->to);
        }

        $this->types = [
            self::TYPE_CREATED_AT => Yii::t('common', 'Дата создания'),
            self::TYPE_UPDATED_AT => Yii::t('common', 'Дата обновления'),
            self::TYPE_CALL_CENTER_SENT_AT => Yii::t('common', 'Дата отправки (колл-центр)'),
            self::TYPE_CALL_CENTER_APPROVED_AT => Yii::t('common', 'Дата одобрения (колл-центр)'),
        ];
    }

    /**
     * @return string
     */
    public function getFilteredAttribute()
    {
        $attributes = [
            self::TYPE_CREATED_AT => Order::tableName() . '.created_at',
            self::TYPE_UPDATED_AT => Order::tableName() . '.updated_at',
            self::TYPE_CALL_CENTER_SENT_AT => CallCenterRequest::tableName() . '.sent_at',
            self::TYPE_CALL_CENTER_APPROVED_AT => CallCenterRequest::tableName() . '.approved_at',
        ];

        if (!array_key_exists($this->type, $attributes)) {
            throw new InvalidParamException(Yii::t('common', 'Неизвестный тип в условии.'));
        }

        return $attributes[$this->type];
    }
}
