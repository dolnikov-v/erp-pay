<?php

namespace app\modules\report\components\filters;

use app\models\Country;
use app\models\User;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryPartner;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\models\Order;
use app\modules\report\widgets\filters\CountryDeliveryPartnerSelectMultipleFilter as CountryDeliveryPartnerSelectMultipleWidget;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class CountryDeliveryPartnerSelectMultipleFilter
 * @package app\modules\report\components\filters
 */
class CountryDeliveryPartnerSelectMultipleFilter extends Filter
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var \app\models\Country */
    public $countries;

    /** @var array */
    public $country_ids;

    /** @var \app\modules\delivery\models\Delivery */
    public $deliveries;

    /** @var array */
    public $delivery_ids;

    /** @var array */
    public $partners;

    /** @var array */
    public $partners_values = [];

    /** @var bool */
    public $hasApply;

    public $allowNoDelivery = false;

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->hasApply = $this->load($params);

        $country_ids = $this->country_ids ?: array_keys(User::getAllowCountries());

        if ($country_ids && $this->validate()) {
            $query->andWhere([Order::tableName() . '.country_id' => $country_ids]);
        }

        $queryDeliveries = Delivery::find()
            ->joinWith(['country'])
            ->select([
                'id' => Delivery::tableName() . '.id',
                'name' => 'concat(' . Country::tableName() . '.name, " ", ' . Delivery::tableName() . '.name)',
            ])
            ->where([Delivery::tableName() . '.country_id' => $country_ids]);

        $this->deliveries = $queryDeliveries;

        $queryPartners = DeliveryPartner::find()
            ->joinWith('delivery')
            ->where([Delivery::tableName() . '.country_id' => $country_ids])
            ->orderBy([DeliveryPartner::tableName() . '.name' => SORT_ASC])
            ->asArray();

        if ($this->delivery_ids && $this->validate()) {
            $query->andWhere(['COALESCE(' . DeliveryRequest::tableName() . '.delivery_id, ' . Order::tableName() . '.pending_delivery_id)' => $this->delivery_ids]);

            $queryPartners->andWhere(["IN", DeliveryPartner::tableName() . '.delivery_id', $this->delivery_ids]);
        }

        $this->partners = ArrayHelper::map($queryPartners->all(), 'id', 'name');

        if (!empty($this->partners_values)) {
            $query->andWhere([DeliveryRequest::tableName() . '.delivery_partner_id' => $this->partners_values]);
        }
    }

    /**
     * @param array $params
     * @return array
     */
    public function applyParams($params)
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        $allowCountries = array_keys(User::getAllowCountries());
        $this->countries = Country::find()
            ->where(['IN', Country::tableName() . '.id', $allowCountries])
            ->active()
            ->orderBy(['name' => SORT_ASC]);
        $this->deliveries = Delivery::find()
            ->joinWith(['country'])
            ->select([
                'id' => Delivery::tableName() . '.id',
                'name' => 'concat(' . Country::tableName() . '.name, " ", ' . Delivery::tableName() . '.name)',
            ])
            ->where(['IN', Delivery::tableName() . '.country_id', $allowCountries])
            ->byCountryId(Yii::$app->user->getCountry()->id)
            ->orderBy(['name' => SORT_ASC]);

        $delivery_ids = ArrayHelper::getColumn($this->deliveries->all(), 'id');

        $partners = DeliveryPartner::find()
            ->where([DeliveryPartner::tableName() . '.delivery_id' => $delivery_ids])
            ->orderBy(['name' => SORT_ASC])
            ->asArray()
            ->all();

        $this->partners = ArrayHelper::map($partners, 'id', 'name');

        parent::init();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['country_ids', 'delivery_ids', 'partners_values'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'country_ids' => Yii::t('common', 'Страна'),
            'delivery_ids' => Yii::t('common', 'Служба доставки'),
            'partners_values' => Yii::t('common', 'Партнер КС'),
        ];
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return CountryDeliveryPartnerSelectMultipleWidget::widget([
            'form' => $form,
            'model' => $this,
        ]);
    }
}
