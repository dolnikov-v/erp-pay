<?php
namespace app\modules\report\components\filters;

use app\modules\order\models\Order;
use app\modules\report\components\ReportForm;
use app\modules\report\widgets\filters\OrderNumberFilter as OrderNumberWidget;
use Yii;

/**
 * Class OrderNumberFilter
 * @package app\modules\report\widgets\filters
 */
class OrderNumberFilter extends Filter
{
    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var array */
    public $number;

    /** @var bool */
    public $hasApply;

    /**
     * @var ReportForm
     */
    public $reportForm;

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate()) {
            if ($this->number) {
                $query->andWhere([Order::tableName() . '.id' =>  array_map('trim', explode(',', $this->number))]);
            }
        }
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['number'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'number' => Yii::t('common', 'Номер заказа'),
        ];
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return OrderNumberWidget::widget([
            'form' => $form,
            'model' => $this,
        ]);
    }
}
