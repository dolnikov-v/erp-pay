<?php

namespace app\modules\report\components\filters;

use app\modules\delivery\models\DeliveryPartner;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\report\components\ReportForm;
use app\modules\report\widgets\filters\DeliveryPartnerFilter as DeliveryPartnerFilterWidget;
use Yii;

/**
 * Class DeliveryPartnerFilter
 * @package app\modules\report\components\filters
 */
class DeliveryPartnerFilter extends Filter
{
    /**
     * @var integer
     */
    public $partner;

    /**
     * @var ReportForm
     */
    public $reportForm;

    /**
     * @var array
     */
    public $partners = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        $query = DeliveryPartner::find();
        if (!$this->reportForm->getCountryFilter()->all) {
            $query->byCountryId(Yii::$app->user->country->id);
        }
        $this->partners = $query->orderBy([DeliveryPartner::tableName() . '.name' => SORT_ASC])->collection();

        parent::init();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['partner', 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'partner' => Yii::t('common', 'Партнер КС')
        ];
    }

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate()) {
            if ($this->partner) {
                $query->andWhere([DeliveryRequest::tableName() . '.delivery_partner_id' => $this->partner]);
            }
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return DeliveryPartnerFilterWidget::widget([
            'form' => $form,
            'model' => $this
        ]);
    }
}
