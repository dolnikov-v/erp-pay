<?php
namespace app\modules\report\components\filters;

use app\models\Country;
use app\modules\callcenter\models\CallCenter;
use app\modules\callcenter\models\CallCenterFullRequest;
use app\modules\salary\models\Office;
use app\modules\salary\models\Person;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\callcenter\models\CallCenterUser;
use app\modules\callcenter\models\CallCenterCheckRequest;
use app\modules\report\components\ReportForm;
use app\modules\report\widgets\filters\CallCenterFilter as CallCenterFilterWidget;
use Yii;
use yii\base\InvalidParamException;

/**
 * Class CallCenterFilter
 * @package app\modules\report\components\filters
 */
class CallCenterFilter extends Filter
{
    const TYPE_CALL_CENTER_REQUEST_CC_ID = 'cc_request_cc_id';
    const TYPE_CALL_CENTER_FULL_REQUEST_CC_ID = 'cc_full_request_cc_id';
    const TYPE_CALL_CENTER_CHECK_REQUEST_CC_ID = 'call_center_check_request_cc_id';
    const TYPE_CALL_CENTER_USER = 'cc_user';

    /**
     * @var integer
     */
    public $callCenter;

    /**
     * @var ReportForm
     */
    public $reportForm;

    /**
     * @var array
     */
    public $callCenters = [];

    /**
     * @var string
     */
    public $type;

    /**
     * фильтруем Persons
     * @var boolean
     */
    public $toUser;

    /**
     * фильтруем CallCenterUsers
     * @var boolean
     */
    public $toCallCenterUser;

    /**
     * @var bool
     */
    public $multiple = false;

    /**
     * @var string
     */
    public $label = 'Колл-центр';

    /**
     * @var integer
     */
    public $officeId = 0;

    /**
     * @inheritdoc
     */
    public function init()
    {

        if ($this->toUser || $this->toCallCenterUser) {
            $this->type = self::TYPE_CALL_CENTER_USER;

            $callCenterQuery = CallCenter::find()
                ->active()
                ->byOfficeId($this->officeId);

            if (!$this->isConsole()) {
                if ((Yii::$app->user->can('limit_call_center_persons_by_office') &&
                        !Yii::$app->user->can('limit_call_center_persons_by_direction'))
                    ||
                    Yii::$app->user->can('limit_salary_persons_by_storage')
                ) {
                    $officeIds = array_keys(Office::find()->bySystemUserCountries()->collection());
                    $callCenterQuery->byOfficeId($officeIds);
                } else if (Yii::$app->user->can('limit_call_center_persons_by_direction')) {
                    $callCenterQuery->bySystemUserCountries();
                }
            }

            $this->callCenters = $callCenterQuery->collection(true);

        } else {

            $query = $this->callCenters = CallCenter::find()
                ->active();

            $countryId = Yii::$app->user->country->id;
            $withCountry = false;

            if ($this->reportForm && $this->reportForm->getCountrySelectFilter()) {
                $countryId = $this->reportForm->getCountrySelectFilter()->country_ids;
                $withCountry = true;
            }

            if (isset($this->reportForm->countryFilter) && $this->reportForm->countryFilter->all) {
                $countryId = null;
                $withCountry = true;
            }

            if ($countryId) {
                $query->byCountryId($countryId);
            }
            $this->callCenters = $query->collection($withCountry);

            $this->type = self::TYPE_CALL_CENTER_REQUEST_CC_ID;
        }
        parent::init();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['callCenter', 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'callCenter' => Yii::t('common', $this->label)
        ];
    }

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);
        $filteredAttribute = $this->getFilteredAttribute();
        if ($this->validate()) {
            if ($this->callCenter) {
                $query->andWhere([$filteredAttribute => $this->callCenter]);
            }
        }

        if (!$this->isConsole() && $this->toUser) {
            if (!Yii::$app->user->can('limit_call_center_persons_by_office') &&
                Yii::$app->user->can('limit_call_center_persons_by_direction')
            ) {
                $officeIds = array_keys(Office::find()->bySystemUserCountries()->collection());
                $query->andWhere([
                    'or',
                    [$filteredAttribute => array_keys($this->callCenters)],
                    [
                        'and',
                        [Person::tableName() . '.office_id' => $officeIds],
                        [$filteredAttribute => null],
                    ]
                ]);
            }
        }

        if ($this->toCallCenterUser) {
            $query->andWhere([$filteredAttribute => array_keys($this->callCenters)]);
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return CallCenterFilterWidget::widget([
            'form' => $form,
            'model' => $this
        ]);
    }

    /**
     * @return string
     */
    protected function getFilteredAttribute()
    {
        $attributes = [
            self::TYPE_CALL_CENTER_REQUEST_CC_ID => CallCenterRequest::tableName() . '.call_center_id',
            self::TYPE_CALL_CENTER_FULL_REQUEST_CC_ID => CallCenterFullRequest::tableName() . '.call_center_id',
            self::TYPE_CALL_CENTER_CHECK_REQUEST_CC_ID => CallCenterCheckRequest::tableName() . '.call_center_id',
            self::TYPE_CALL_CENTER_USER => CallCenterUser::tableName() . '.callcenter_id',
        ];

        if (!array_key_exists($this->type, $attributes)) {
            throw new InvalidParamException(Yii::t('common', 'Неизвестный тип в условии.'));
        }

        return $attributes[$this->type];
    }
}
