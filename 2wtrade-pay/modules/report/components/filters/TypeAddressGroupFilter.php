<?php
namespace app\modules\report\components\filters;

use app\modules\report\widgets\filters\TypeAddressGroupFilter as TypeAddressGroupFilterWidget;
use Yii;

/**
 * Class TypeAddressGroupFilter
 * @package app\modules\report\components\filters
 */
class TypeAddressGroupFilter extends Filter
{

    const TYPE_ADDRESS_PROVINCE = 'customer_province';
    const TYPE_ADDRESS_CITY = 'customer_city';
    const TYPE_ADDRESS_ZIP = 'customer_zip';


    /** @var \app\components\widgets\ActiveForm */
    public $form;

    /** @var array */
    public $typeAdressGroupFilter;

    /** @var string */
    public $type_address_group;


    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);
        if (is_null($this->type_address_group)) {
            $this->type_address_group = $this::TYPE_ADDRESS_PROVINCE;
        }
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->typeAdressGroupFilter = [
            self::TYPE_ADDRESS_PROVINCE => Yii::t('common', 'Провинция'),
            self::TYPE_ADDRESS_CITY  => Yii::t('common', 'Город'),
            self::TYPE_ADDRESS_ZIP => Yii::t('common', 'ZIP-код'),
        ];

        parent::init();
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'type_address_group' => Yii::t('common', 'Группировать по'),
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['type_address_group'], 'safe'],
        ];
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return TypeAddressGroupFilterWidget::widget([
            'form' => $form,
            'model' => $this,
        ]);
    }
}
