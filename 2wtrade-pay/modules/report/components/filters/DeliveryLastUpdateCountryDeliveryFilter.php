<?php

namespace app\modules\report\components\filters;

use app\models\Country;
use app\models\User;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\report\widgets\filters\CountryDeliverySelectMultipleFilter as CountryDeliverySelectMultipleWidget;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class DeliveryLastUpdateCountryDeliveryFilter
 * @package app\modules\report\components\filters
 */
class DeliveryLastUpdateCountryDeliveryFilter extends CountryDeliverySelectMultipleFilter
{
    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->hasApply = $this->load($params);

        $country_ids = $this->country_ids ?: array_keys(User::getAllowCountries());

        if ($country_ids && $this->validate()) {
            $query->andWhere([Country::tableName() . '.id' => $country_ids]);
        }

        $queryDeliveries = Delivery::find()
            ->joinWith(['country'])
            ->select([
                'id' => Delivery::tableName() . '.id',
                'name' => 'concat(' . Country::tableName() . '.name, " ", ' . Delivery::tableName() . '.name)',
            ])
            ->where([Delivery::tableName() . '.country_id' => $country_ids])
            ->andWhere([Delivery::tableName() . '.active' => 1]);

        $this->deliveries = $queryDeliveries;

        if (!$this->delivery_ids && !$this->allowNoDelivery) {
            $this->delivery_ids = array_keys(ArrayHelper::map($queryDeliveries->all(), 'id', 'name'));
        }

        if ($this->delivery_ids && $this->validate()) {
            $query->andWhere([Delivery::tableName() . '.id' => $this->delivery_ids]);
        }
    }
}