<?php

namespace app\modules\report\components\filters;


use app\modules\report\models\Invoice;

/**
 * Class InvoiceStatusFilter
 * @package app\modules\report\components\filters
 */
class InvoiceStatusFilter extends Filter
{
    public $status;

    public $statuses = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->statuses = Invoice::getStatusLabels();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['status', 'in', 'range' => array_keys(Invoice::getStatusLabels())]
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'status' => \Yii::t('common', 'Статус'),
        ];
    }

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);

        if ($this->validate()) {
            $query->andFilterWhere([Invoice::tableName() . '.status' => $this->status]);
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return $form->field($this, 'status')->select2List($this->statuses, ['length' => false, 'prompt' => '—']);
    }
}