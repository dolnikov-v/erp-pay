<?php
namespace app\modules\report\components\filters;

use Yii;
use app\modules\order\models\Lead;
use app\modules\report\components\ReportForm;
use app\modules\report\widgets\filters\PackageFilter as PackageFilterWidget;

/**
 * Class PackageFilter
 * @package app\modules\report\components\filters
 */
class PackageFilter extends Filter
{

    /**
     * @var integer
     */
    public $package;

    /**
     * @var ReportForm
     */
    public $reportForm;

    /**
     * @var array
     */
    public $packages = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->packages = Lead::PACKAGE_LABELS;
        parent::init();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['package', 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'package' => Yii::t('common', 'Маркетинговая акция'),
        ];
    }

    /**
     * @param \yii\db\Query $query
     * @param array $params
     */
    public function apply($query, $params)
    {
        $this->load($params);
        if ($this->package) {
            if ($this->package == 1) {
                $query->andWhere([
                    'OR',
                    [
                        'IN', Lead::tableName() . '.package_id', [0,1]
                    ],
                    [
                        'IS', Lead::tableName() . '.package_id', null
                    ],
                    ]);
            } else {
                $query->andFilterWhere([Lead::tableName() . '.package_id' => $this->package]);
            }
        }
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return PackageFilterWidget::widget([
            'form' => $form,
            'model' => $this
        ]);
    }
}
