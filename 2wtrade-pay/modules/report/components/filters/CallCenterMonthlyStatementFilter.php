<?php
namespace app\modules\report\components\filters;

use app\modules\salary\models\MonthlyStatement;
use app\modules\report\components\ReportForm;
use app\modules\report\widgets\filters\CallCenterMonthlyStatementFilterWidget as CallCenterMonthlyStatementFilterWidget;
use Yii;

/**
 * Class CallCenterMonthlyStatementFilter
 * @package app\modules\report\components\filters
 */
class CallCenterMonthlyStatementFilter extends Filter
{
    /**
     * @var integer
     */
    public $monthlyStatement;

    /**
     * @var ReportForm
     */
    public $reportForm;

    /**
     * @var array
     */
    public $monthlyStatements = [];

    /**
     * @var string
     */
    public $label = 'График';

    /**
     * @var integer
     */
    public $officeId = 0;

    /**
     * @inheritdoc
     */
    public function init()
    {
        if (!$this->isConsole()) {
            $this->monthlyStatements = MonthlyStatement::find()
                ->active()
                ->byOfficeId($this->officeId)
                ->bySystemUserCountries()
                ->orderBy(['id' => SORT_DESC])
                ->collection();
        }

        parent::init();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['monthlyStatement', 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'monthlyStatement' => Yii::t('common', $this->label)
        ];
    }

    /**
     * @param \yii\db\Query $query
     * @param array $params
     * @return integer
     */
    public function apply($query, $params)
    {
        $this->load($params);
        if ($this->validate()) {
            return $this->monthlyStatement;
        }
        return null;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return CallCenterMonthlyStatementFilterWidget::widget([
            'form' => $form,
            'model' => $this
        ]);
    }
}
