<?php

namespace app\modules\report\components;

use app\modules\report\components\filters\AdcomboTypeFilter;
use app\modules\report\models\ReportAdcombo;
use app\modules\report\components\filters\AdcomboDateFilter;
use yii\data\ActiveDataProvider;

/**
 * Class ReportFormAdCombo
 * @package app\modules\report\components
 */
class ReportFormAdcombo extends ReportForm
{
    /**
     * @var AdcomboDateFilter
     */
    protected $dailyFilter;

    /**
     * @var AdcomboTypeFilter
     */
    protected $typeFilter;

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function apply($params)
    {
        $this->query = ReportAdcombo::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $this->query,
        ]);

        $this->query->where([ReportAdcombo::tableName() . '.country_id' => \Yii::$app->user->country->id]);

        $this->applyFilters($params);

        $this->query->orderBy('from desc');

        return $dataProvider;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $output = '';
        $output .= $this->getDailyFilter()->restore($form);
        $output .= $this->getTypeFilter()->restore($form);
        return $output;
    }

    /**
     * @param array $params
     */
    protected function applyFilters($params)
    {
        $this->getDailyFilter()->apply($this->query, $params);
        $this->getTypeFilter()->apply($this->query, $params);
    }

    /**
     * @return AdcomboDateFilter
     */
    public function getDailyFilter()
    {
        if (is_null($this->dailyFilter)) {
            $this->dailyFilter = new AdcomboDateFilter();
        }

        return $this->dailyFilter;
    }

    /**
     * @return AdcomboTypeFilter
     */
    public function getTypeFilter()
    {
        if (is_null($this->typeFilter)) {
            $this->typeFilter = new AdcomboTypeFilter();
        }

        return $this->typeFilter;
    }
}
