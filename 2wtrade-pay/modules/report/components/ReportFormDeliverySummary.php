<?php

namespace app\modules\report\components;

use app\models\Currency;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\models\Lead;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use app\modules\report\components\filters\DateFilter;
use app\modules\report\extensions\DataProvider;
use app\modules\report\extensions\Query;
use app\models\CurrencyRate;
use app\models\Country;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class ReportFormDeliverySummary
 * @package app\modules\report\components
 */
class ReportFormDeliverySummary extends ReportFormDelivery
{
    /**
     * @param array $params
     * @return DataProvider
     */
    public function apply($params)
    {
        if (empty($params)) {
            $params[$this->formName()]['country_ids'] = [Yii::$app->user->getCountry()->id];
            $params[$this->formName()]['delivery_ids'] = ArrayHelper::getColumn(Delivery::getUserDeliveries(), 'id');
            $params[$this->formName()]['groupBy'] = self::GROUP_BY_COUNTRY_DELIVERY;
        }
        $mapStatuses = $this->getMapStatuses();

        $this->getDateFilter()->type = DateFilter::TYPE_DELIVERY_SENT_AT_NOT_EMPTY;
        if (Yii::$app->user->isImplant) {
            $this->getDateFilter()->type = DateFilter::TYPE_DELIVERY_CREATED_AT;
        }

        if ($params[$this->formName()]['groupBy'] == ReportForm::GROUP_BY_DATE_COUNTRY_DELIVERY) {
            $params[$this->formName()]['groupBy'] = ReportForm::GROUP_BY_COUNTRY_DELIVERY;
        }
        if ($params[$this->formName()]['groupBy'] == ReportForm::GROUP_BY_DATE_PRODUCTS) {
            $params[$this->formName()]['groupBy'] = ReportForm::GROUP_BY_PRODUCTS;
        }
        if ($params[$this->formName()]['groupBy'] == ReportForm::GROUP_BY_CREATED_AT ||
            $params[$this->formName()]['groupBy'] == ReportForm::GROUP_BY_UPDATED_AT ||
            $params[$this->formName()]['groupBy'] == ReportForm::GROUP_BY_CALL_CENTER_SENT_AT ||
            $params[$this->formName()]['groupBy'] == ReportForm::GROUP_BY_CALL_CENTER_APPROVED_AT ||
            $params[$this->formName()]['groupBy'] == ReportForm::GROUP_BY_DELIVERY_CREATED_AT ||
            $params[$this->formName()]['groupBy'] == ReportForm::GROUP_BY_DELIVERY_SENT_AT ||
            $params[$this->formName()]['groupBy'] == ReportForm::GROUP_BY_DELIVERY_SENT_AT_NOT_EMPTY ||
            $params[$this->formName()]['groupBy'] == ReportForm::GROUP_BY_DELIVERY_FIRST_SENT_AT ||
            $params[$this->formName()]['groupBy'] == ReportForm::GROUP_BY_DELIVERY_RETURNED_AT ||
            $params[$this->formName()]['groupBy'] == ReportForm::GROUP_BY_DELIVERY_APPROVED_AT ||
            $params[$this->formName()]['groupBy'] == ReportForm::GROUP_BY_DELIVERY_ACCEPTED_AT ||
            $params[$this->formName()]['groupBy'] == ReportForm::GROUP_BY_YEAR_MONTH ||
            $params[$this->formName()]['groupBy'] == ReportForm::GROUP_BY_DELIVERY_PAID_AT
        ) {
            $params[$this->formName()]['groupBy'] = ReportForm::GROUP_BY_DELIVERY;
            $this->groupBy = ReportForm::GROUP_BY_DELIVERY;
        }

        $this->load($params);

        $this->query = new Query();
        $this->query->from([Order::tableName()]);

        if ($this->searchQuery[$this->formName()]['package']??null) {
            $this->query->leftJoin(Lead::tableName(), Order::tableName() . '.id=' . Lead::tableName() . '.order_id');
        }

        $toDollar = '';

        if ($this->dollar) {
            $toDollar = '/ ' . CurrencyRate::tableName() . '.rate';
            $this->query->leftJoin(Country::tableName(), Order::tableName() . '.country_id=' . Country::tableName() . '.id');
            $this->query->leftJoin(CurrencyRate::tableName(), Country::tableName() . '.currency_id = ' . CurrencyRate::tableName() . '.currency_id');
        }

        $this->query->addInConditionCount(Order::tableName() . '.status_id', OrderStatus::getApproveList(), 'approved_cnt');

        $convertPriceTotalToCurrencyId = Country::tableName() . '.currency_id';
        if ($this->dollar) {
            $usdCurrency = Currency::getUSD();
            $convertPriceTotalToCurrencyId = $usdCurrency->id;
        }
        //https://2wtrade-tasks.atlassian.net/browse/ERP-689
        $caseApprove = $this->query->buildCaseCondition(
            'convertToCurrencyByCountry(' . Order::tableName() . '.price_total + ' . Order::tableName() . '.delivery, ' . Order::tableName() . '.price_currency,' . $convertPriceTotalToCurrencyId . ')',
            Order::tableName() . '.status_id',
            OrderStatus::getApproveList()
        );
        $caseBuyout = $this->query->buildCaseCondition(
            'convertToCurrencyByCountry(' . Order::tableName() . '.price_total + ' . Order::tableName() . '.delivery, ' . Order::tableName() . '.price_currency,' . $convertPriceTotalToCurrencyId . ')',
            Order::tableName() . '.status_id',
            $mapStatuses[yii::t('common', 'Выкуплено')]
        );

        $this->query->addAvgCondition($caseApprove, Yii::t('common', 'Средний чек апрувленный'));
        $this->query->addAvgCondition($caseBuyout, Yii::t('common', 'Средний чек выкупной'));

        $condition = 'if(' . DeliveryRequest::tableName() . '.approved_at > 0 and (COALESCE(' . DeliveryRequest::tableName() . '.second_sent_at,'
            . DeliveryRequest::tableName() . '.sent_at) > 0 or ' . DeliveryRequest::tableName() . '.accepted_at > 0), '
            . '(' . DeliveryRequest::tableName() . '.approved_at - if(' . DeliveryRequest::tableName() . '.accepted_at > 0, '
            . DeliveryRequest::tableName() . '.accepted_at, COALESCE(' . DeliveryRequest::tableName() . '.second_sent_at,' . DeliveryRequest::tableName() . '.sent_at))) / (3600*24), null)';


        $select['avg_delivery_time'] = 'round(avg(' . $condition . ' ))';

        if ($this->groupBy == self::GROUP_BY_COUNTRY_DELIVERY) {
            $select['country'] = Country::tableName() . '.name';
            $select['country_id'] = Country::tableName() . '.id';
            $select['delivery'] = Delivery::tableName() . '.name';
            $select['delivery_id'] = Delivery::tableName() . '.id';
        } elseif ($this->groupBy == self::GROUP_BY_PROVINCE_CITY_SUBDISTRICT_ZIP) {
            $select['province'] = Order::tableName() . '.customer_province';
            $select['city'] = Order::tableName() . '.customer_city';
            $select['subdistrict'] = Order::tableName() . '.customer_subdistrict';
            $select['zip'] = Order::tableName() . '.customer_zip';
        } elseif ($this->groupBy == self::GROUP_BY_SUBDISTRICT_ZIP) {
            $select['subdistrict'] = Order::tableName() . '.customer_subdistrict';
            $select['zip'] = Order::tableName() . '.customer_zip';
        }


        $this->query->addSelect($select);

        $this->buildSelect()
            ->applyFilters($params);

        // Исключаем запрещенные источники заказов
        $this->query->andWhere(['NOT IN', Order::tableName() . '.source_id', Yii::$app->user->rejectedSources]);

        //echo $this->query->createCommand()->getRawSql(); exit;

        $dataProvider = new DataProvider([
            'form' => $this,
            'query' => $this->query,
        ]);

        $dataProvider->pagination->pageSize = $dataProvider->pagination->totalCount;

        return $dataProvider;
    }
}
