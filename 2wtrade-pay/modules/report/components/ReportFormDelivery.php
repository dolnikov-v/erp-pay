<?php

namespace app\modules\report\components;

use app\models\Currency;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\models\Lead;
use app\modules\order\models\Order;
use app\modules\order\models\OrderFinanceFact;
use app\modules\order\models\OrderFinancePretension;
use app\modules\order\models\OrderStatus;
use app\modules\report\components\filters\DateFilter;
use app\modules\report\extensions\DataProvider;
use app\modules\report\extensions\Query;
use app\models\CurrencyRate;
use app\models\Country;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class ReportFormDelivery
 * @package app\modules\report\components
 */
class ReportFormDelivery extends ReportForm
{
    /**
     * @var bool
     */
    public $dollar = true;

    /**
     * @var string Группировка по умолчанию
     */
    public $groupBy = self::GROUP_BY_DELIVERY_SENT_AT_NOT_EMPTY;

    /**
     * @var array Маппинг статусов
     */
    protected $mapStatuses = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->groupByCollection[self::GROUP_BY_PRODUCT_CATEGORY] = Yii::t('common', 'Категория товаров');
        $this->groupByCollection[self::GROUP_BY_PRODUCTS] = Yii::t('common', 'Товар');
        $this->groupByCollection[self::GROUP_BY_DELIVERY] = Yii::t('common', 'Курьерская служба');
        $this->groupByCollection[self::GROUP_BY_DATE_COUNTRY_DELIVERY] = Yii::t('common', 'Дата, страна, служба доставки');
        $this->groupByCollection[self::GROUP_BY_PROVINCE_CITY_SUBDISTRICT_ZIP] = Yii::t('common', 'Провинция, город, квартал, ZIP код');
        $this->groupByCollection[self::GROUP_BY_SUBDISTRICT_ZIP] = Yii::t('common', 'Квартал, ZIP код');
        $this->groupByCollection[self::GROUP_BY_DATE_PRODUCTS] = Yii::t('common', 'Дата, товар');
        $this->groupByCollection[self::GROUP_BY_YEAR_MONTH] = Yii::t('common', 'Год, месяц');
        if (Yii::$app->user->isImplant) {
            $this->groupBy = self::GROUP_BY_DELIVERY_CREATED_AT;
        }
    }

    /**
     * @param array $params
     * @return DataProvider
     */
    public function apply($params)
    {
        if (empty($params)) {
            $params[$this->formName()]['country_ids'] = [Yii::$app->user->getCountry()->id];
            $params[$this->formName()]['delivery_ids'] = ArrayHelper::getColumn(Delivery::getUserDeliveries(), 'id');
            $params[$this->formName()]['groupBy'] = self::GROUP_BY_DATE_COUNTRY_DELIVERY;
        }

        $mapStatuses = $this->getMapStatuses();

        $this->getDateFilter()->type = DateFilter::TYPE_DELIVERY_SENT_AT_NOT_EMPTY;
        if (Yii::$app->user->isImplant) {
            $this->getDateFilter()->type = DateFilter::TYPE_DELIVERY_CREATED_AT;
        }

        $this->load($params);

        $this->query = new Query();
        $this->query->from([Order::tableName()]);

        if ($this->searchQuery[$this->formName()]['package'] ?? null) {
            $this->query->leftJoin(Lead::tableName(), Order::tableName() . '.id=' . Lead::tableName() . '.order_id');
        }

        $this->query->leftJoin(Country::tableName(), Order::tableName() . '.country_id=' . Country::tableName() . '.id');
        $convertPriceTotalToCurrencyId = Country::tableName() . '.currency_id';
        if ($this->dollar) {
            $usdCurrency = Currency::getUSD();
            $convertPriceTotalToCurrencyId = $usdCurrency->id;
        }

        $this->query->addInConditionCount(Order::tableName() . '.status_id', OrderStatus::getApproveList(), 'approved_cnt');

        //https://2wtrade-tasks.atlassian.net/browse/ERP-689
        $caseApprove = $this->query->buildCaseCondition(
            'convertToCurrencyByCountry(' . Order::tableName() . '.price_total + ' . Order::tableName() . '.delivery, ' . Order::tableName() . '.price_currency,' . $convertPriceTotalToCurrencyId . ')',
            Order::tableName() . '.status_id',
            OrderStatus::getApproveList()
        );
        $caseBuyout = $this->query->buildCaseCondition(
            'convertToCurrencyByCountry(' . Order::tableName() . '.price_total + ' . Order::tableName() . '.delivery, ' . Order::tableName() . '.price_currency,' . $convertPriceTotalToCurrencyId . ')',
            Order::tableName() . '.status_id',
            $mapStatuses[yii::t('common', 'Выкуплено')]
        );

        $this->query->addAvgCondition($caseApprove, Yii::t('common', 'Средний чек апрувленный'));
        $this->query->addAvgCondition($caseBuyout, Yii::t('common', 'Средний чек выкупной'));

        $condition = 'if(' . DeliveryRequest::tableName() . '.approved_at > 0 and (COALESCE(' . DeliveryRequest::tableName() . '.second_sent_at,'
            . DeliveryRequest::tableName() . '.sent_at) > 0 or ' . DeliveryRequest::tableName() . '.accepted_at > 0), '
            . '(' . DeliveryRequest::tableName() . '.approved_at - if(' . DeliveryRequest::tableName() . '.accepted_at > 0, '
            . DeliveryRequest::tableName() . '.accepted_at, COALESCE(' . DeliveryRequest::tableName() . '.second_sent_at,' . DeliveryRequest::tableName() . '.sent_at))) / (3600*24), null)';


        $select['avg_delivery_time'] = 'round(avg(' . $condition . ' ))';

        if ($this->groupBy == self::GROUP_BY_DATE_COUNTRY_DELIVERY) {
            $select['country'] = Country::tableName() . '.name';
            $select['country_id'] = Country::tableName() . '.id';
            $select['delivery'] = Delivery::tableName() . '.name';
            $select['delivery_id'] = Delivery::tableName() . '.id';
        } elseif ($this->groupBy == self::GROUP_BY_PROVINCE_CITY_SUBDISTRICT_ZIP) {
            $select['province'] = Order::tableName() . '.customer_province';
            $select['city'] = Order::tableName() . '.customer_city';
            $select['subdistrict'] = Order::tableName() . '.customer_subdistrict';
            $select['zip'] = Order::tableName() . '.customer_zip';
        } elseif ($this->groupBy == self::GROUP_BY_SUBDISTRICT_ZIP) {
            $select['subdistrict'] = Order::tableName() . '.customer_subdistrict';
            $select['zip'] = Order::tableName() . '.customer_zip';
        }

        $this->query->addSelect($select);


        $this->buildSelect()
            ->applyFilters($params);

        // Исключаем запрещенные источники заказов
        $this->query->andWhere(['NOT IN', Order::tableName() . '.source_id', Yii::$app->user->rejectedSources]);

        $dataProvider = new DataProvider([
            'form' => $this,
            'query' => $this->query,
        ]);

        $dataProvider->pagination->pageSize = $dataProvider->pagination->totalCount;

        return $dataProvider;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $output = '';

        return $output;
    }

    /**
     * @return array
     */
    public function getMapStatuses()
    {
        if (empty($this->mapStatuses)) {
            $this->mapStatuses = [
                Yii::t('common', 'Выкуплено') => OrderStatus::getBuyoutList(),
                //                Yii::t('common', 'Апрувлено') => OrderStatus::getApproveList(),
                Yii::t('common', 'В процессе') => OrderStatus::getProcessDeliveryAndLogisticList(),
                Yii::t('common', 'Не выкуплено') => OrderStatus::getNotBuyoutDeliveryList(),
                Yii::t('common', 'Деньги получены') => OrderStatus::getOnlyMoneyReceivedList(),
                Yii::t('common', 'Деньги не получены') => OrderStatus::getOnlyBuyoutList(),
            ];
        }

        return $this->mapStatuses;
    }

    /**
     * @inheritdoc
     */
    protected function buildSelect()
    {
        switch ($this->groupBy) {
            case self::GROUP_BY_PRODUCTS:
                $this->buildSelectMapProducts();
                break;
            default:
                $this->buildSelectMapStatuses();
                break;
        }

        /*$this->query->leftJoin(OrderFinanceFact::tableName(),
            OrderFinanceFact::tableName() . '.order_id=' . Order::tableName() . '.id');

        $this->query->leftJoin(OrderFinancePretension::tableName(),
            OrderFinancePretension::tableName() . '.delivery_report_id=' . OrderFinanceFact::tableName() . '.delivery_report_id and ' . OrderFinancePretension::tableName() . '.type is not null');

        $this->query->addCountDistinctCondition(OrderFinanceFact::tableName() . '.order_id', Yii::t('common', 'Претензия'));*/


        return $this;
    }

    /**
     * @param array $params
     * @return ReportForm
     */
    protected function applyFilters($params)
    {
        $this->getDateFilter()->apply($this->query, $params);
        $this->getProductFilter()->apply($this->query, $params);
        $this->getPackageFilter()->apply($this->query, $params);
        $this->getCountryDeliverySelectMultipleFilter()->apply($this->query, $params);
        $this->getProductCategoryFilter()->apply($this->query, $params);
//        $this->getDeliveryPartnerFilter()->apply($this->query, $params);

        $this->getCalculatePercentFilter()->apply($this->query, $params);
        $this->getPaymentAdvertisingFilter()->apply($this->query, $params);
        $this->getCalculateDollarFilter()->apply($this->query, $params);
        $this->getSourceFilter()->apply($this->query, $params);
        return $this;
    }

}
