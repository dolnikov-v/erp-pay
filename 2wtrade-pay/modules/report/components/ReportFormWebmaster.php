<?php

namespace app\modules\report\components;

use app\models\Country;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\callcenter\models\CallCenterRequestCallData;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use app\modules\report\components\filters\CountryDeliverySelectMultipleFilter;
use app\modules\report\components\filters\LeadProductFilter;
use app\modules\report\components\filters\ProductFilter;
use app\modules\report\extensions\Query;
use Yii;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;

/**
 * Class ReportFormWebmaster
 * @package app\modules\report\components
 */
class ReportFormWebmaster extends ReportForm
{
    /**
     * @var string Группировка по умолчанию
     */
    public $groupBy = self::GROUP_BY_WEBMASTER_IDENTIFIER;

    /**
     * @var array Маппинг статусов
     */
    protected $mapStatuses = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->groupByCollection[self::GROUP_BY_WEBMASTER_IDENTIFIER] = Yii::t('common', 'Идентификатор вебмастера');
    }

    /**
     * @param array $params
     * @return ArrayDataProvider
     */
    public function apply($params)
    {
        if (empty($params)) {
            $params[$this->formName()]['country_ids'] = [Yii::$app->user->getCountry()->id];
        }
        $this->load($params);

        $this->query = new Query();
        $this->query->from([Order::tableName()]);
        $this->query->leftJoin(DeliveryRequest::tableName(), Order::tableName() . '.id=' . DeliveryRequest::tableName() . '.order_id');
        $this->query->leftJoin(Country::tableName(), Order::tableName() . '.country_id=' . Country::tableName() . '.id');
        $this->query->leftJoin(CallCenterRequest::tableName(), Order::tableName() . '.id=' . CallCenterRequest::tableName() . '.order_id');


        $subQuery = new Query();
        $subQuery->select([
            'calls_count' => 'COUNT(*)',
            'min_called_at' => 'MIN(called_at)',
            'call_center_request_id' => 'call_center_request_id',
        ])
            ->from(CallCenterRequestCallData::tableName())
            ->leftJoin(CallCenterRequest::tableName(), CallCenterRequest::tableName() . '.id = ' . CallCenterRequestCallData::tableName() . '.call_center_request_id')
            ->groupBy(
                CallCenterRequestCallData::tableName() . '.call_center_request_id'
            );

        $this->query->leftJoin(['ccrcd' => $subQuery], CallCenterRequest::tableName() . '.id=ccrcd.call_center_request_id');

        $this->buildSelect()->applyFilters($params);
        $this->query->andWhere(['is not', Order::tableName() . '.webmaster_identifier', null]);
        $this->query->groupBy([Order::tableName() . '.webmaster_identifier']);

        $allModels = $this->query->all();
        foreach ($allModels as &$model) {
            $model['buyout_percent'] = $model['vsego'] ? $model['buyout'] * 100 / $model['vsego'] : 0;


            if (floatval($model['buyout_percent']) < 20) {
                $model['percent_group'] = Yii::t('common', 'до 20%');
            }
            if (floatval($model['buyout_percent']) >= 20) {
                $model['percent_group'] = Yii::t('common', 'от 20%');
            }
            if (floatval($model['buyout_percent']) >= 50) {
                $model['percent_group'] = Yii::t('common', 'от 50%');
            }
            if (floatval($model['buyout_percent']) >= 70) {
                $model['percent_group'] = Yii::t('common', 'от 70%');
            }

        }

        ArrayHelper::multisort($allModels, 'buyout_percent', SORT_DESC);

        $dataProvider = new ArrayDataProvider([
            'allModels' => $allModels,
        ]);
        $dataProvider->pagination->pageSize = $dataProvider->pagination->totalCount;

        return $dataProvider;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $output = '';

        return $output;
    }

    /**
     * @return array
     */
    public function getMapStatuses()
    {
        if (empty($this->mapStatuses)) {
            $this->mapStatuses = [
                'approve' => OrderStatus::getApproveList(),
                'buyout' => OrderStatus::getBuyoutList(),
                'hold' => OrderStatus::getHoldStatusList(),
                'reject' => OrderStatus::getCCRejectedStatusList(),
                'vsego' => OrderStatus::getAllList(),
            ];
        }

        return $this->mapStatuses;
    }

    /**
     * @inheritdoc
     */
    protected function buildSelect()
    {
        $this->query->select([
            'webmaster_identifier' => Order::tableName() . '.webmaster_identifier',
            'id' => Order::tableName() . '.webmaster_identifier',
            'count_lead' => 'COUNT(' . Order::tableName() . '.id)',
            'groupbyname' => Order::tableName() . '.webmaster_identifier',
            'country_id' => Order::tableName() . '.country_id',
            'try_call_count' => 'AVG(ccrcd.calls_count)',
            'first_call' => 'AVG((ccrcd.min_called_at - ' . Order::tableName() . '.created_at) / 60)',
        ]);
        $this->buildSelectMapStatuses();

        return $this;
    }

    /**
     * @return ProductFilter
     */
    public function getProductFilter()
    {
        if (is_null($this->productFilter)) {
            $this->productFilter = new LeadProductFilter(['reportForm' => $this, 'allProducts' => true]);
        }

        return $this->productFilter;
    }

    /**
     * @return CountryDeliverySelectMultipleFilter
     */
    public function getCountryDeliverySelectMultipleFilter()
    {
        if (is_null($this->countryDeliverySelectMultipleFilter)) {
            $this->countryDeliverySelectMultipleFilter = new CountryDeliverySelectMultipleFilter(['allowNoDelivery' => true]);
        }

        return $this->countryDeliverySelectMultipleFilter;
    }

    /**
     * @param array $params
     * @return ReportForm
     */
    protected function applyFilters($params)
    {
        $this->getDateFilter()->apply($this->query, $params);
        $this->getProductFilter()->apply($this->query, $params);
        $this->getCountryDeliverySelectMultipleFilter()->apply($this->query, $params);
        $this->getCalculatePercentFilter()->apply($this->query, $params);
        return $this;
    }
}
