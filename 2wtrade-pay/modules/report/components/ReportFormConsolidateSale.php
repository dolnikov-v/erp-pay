<?php
namespace app\modules\report\components;

use app\models\Country;
use app\models\CurrencyRate;
use app\models\User;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use app\modules\report\components\filters\CountryFilter;
use app\modules\report\extensions\DataProvider;
use app\modules\report\extensions\Query;
use app\modules\report\components\filters\ConsolidateSaleDateFilter;
use Yii;

/**
 * Class ReportFormConsolidateSale
 * @package app\modules\report\components
 */
class ReportFormConsolidateSale extends ReportForm
{

    /**
     * @var string Группировка по умолчанию
     */
    public $groupBy = self::GROUP_BY_COUNTRY_DATE;

    /**
     * @var ConsolidateSaleDateFilter
     */
    protected $dateFilter;

    /**
     * @var array Маппинг статусов
     */
    protected $mapStatuses = [];

    /*
     * "Галочка" конвертировать в доллар всегда отмечена
     * @var string
     */
    public $dollar = 'on';

    public $countryFilter;

    /**
     * @param array $params
     * @return DataProvider
     */
    public function apply($params)
    {
        $this->load($params);

        $this->query = new Query();
        $this->query->from([Order::tableName()]);

        $this->prepareCurrencyRate();
        $this->applyFilters($params);
        $this->buildSelect();
        $countries_ids = array_keys(User::getAllowCountries());
        $this->query->andWhere([Order::tableName() . '.country_id' => $countries_ids]);
        $this->query->orderBy([Country::tableName() . '.name' => SORT_ASC, 'date' => SORT_ASC]);

        $dataProvider = new DataProvider([
            'form' => $this,
            'query' => $this->query,
        ]);

        return $dataProvider;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $output = '';

        return $output;
    }

    /**
     * @return array
     */
    public function getMapStatuses()
    {
        if (empty($this->mapStatuses)) {
            $this->mapStatuses = [
                Yii::t('common', 'Всего') => OrderStatus::getAllList(),
                Yii::t('common', 'Заказы') => OrderStatus::getApproveList(),
                Yii::t('common', 'Выкуплено') => OrderStatus::getBuyoutList(),
            ];
        }

        return $this->mapStatuses;
    }

    /**
     * @inheritdoc
     */
    protected function buildSelect()
    {

        $selectDate = $this->getDateFilter()->getFilteredAttribute();

        $alterVikupProcent = '((COUNT(`order`.status_id IN(' . join(', ', OrderStatus::getBuyoutList()) . ') OR NULL) / COUNT(`order`.status_id IN(' . join(', ', OrderStatus::getApproveList()) . ') OR NULL)) * 100)';

        $this->query->select([
            'date' => 'DATE_FORMAT(FROM_UNIXTIME(' . $selectDate . '), "%Y-%m-%d")',
            'country' => Country::tableName() . '.name',
            'count_lead' => 'COUNT(' . Order::tableName() . '.id)',
            'vikup_alter_procent' => $alterVikupProcent,
        ]);

        $mapStatuses = $this->getMapStatuses();

        $this->query->leftJoin(Country::tableName(), Order::tableName() . '.country_id=' . Country::tableName() . '.id');

        $toDollar = '';
        if ($this->dollar || $this->dollar == 'on') {
            $toDollar = '/ ' . CurrencyRate::tableName() . '.rate';
            $this->query->leftJoin(CurrencyRate::tableName(), Country::tableName() . '.currency_id = ' . CurrencyRate::tableName() . '.currency_id');
        }

        $buyoutStatuses = $mapStatuses[Yii::t('common', 'Выкуплено')];
        $buyoutCase = $this->query->buildCaseCondition(
            '(' . Order::tableName() . '.price_total ' . $toDollar . ' + ' . Order::tableName() . '.delivery ' . $toDollar . ' )',
            Order::tableName() . '.status_id',
            $buyoutStatuses
        );
        $this->query->addSumCondition($buyoutCase, Yii::t('common', 'Доход'));

        $case = $this->query->buildCaseCondition(
            Order::tableName() . '.price_total ' . $toDollar,
            Order::tableName() . '.status_id',
            OrderStatus::getApproveList()
        );
        $this->query->addAvgCondition($case, Yii::t('common', 'Средний чек апрувленный'));

        $case = $this->query->buildCaseCondition(
            Order::tableName() . '.price_total ' . $toDollar,
            Order::tableName() . '.status_id',
            OrderStatus::getBuyoutList()
        );
        $this->query->addAvgCondition($case, Yii::t('common', 'Средний чек выкупной'));


        $caseOper = 'CASE WHEN (' . CallCenterRequest::tableName() . '.last_foreign_operator<>-1 and 
                                ' . CallCenterRequest::tableName() . '.last_foreign_operator is not null) 
                          THEN ' . CallCenterRequest::tableName() . '.last_foreign_operator ELSE NULL END';

        $this->query->addCountDistinctCondition($caseOper, Yii::t('common', 'Операторы'));

        $this->query->addInConditionCount(DeliveryRequest::tableName() . '.status', [
            '"' . DeliveryRequest::STATUS_IN_PROGRESS . '"',
            '"' . DeliveryRequest::STATUS_DONE . '"',
            '"' . DeliveryRequest::STATUS_IN_LIST . '"'
        ], Yii::t('common', 'Отгрузки'));

        $this->buildSelectMapStatuses();

        return $this;
    }

    /**
     * @return ConsolidateSaleDateFilter
     */
    public function getDateFilter()
    {
        if (is_null($this->dateFilter)) {
            $this->dateFilter = new ConsolidateSaleDateFilter();
        }

        return $this->dateFilter;
    }

    /**
     * @param array $params
     * @return ReportForm
     */
    protected function applyFilters($params)
    {
        $this->getDateFilter()->apply($this->query, $params);
        return $this;
    }

    /**
     * @return CountryFilter
     */
    public function getCountryFilter()
    {
        if (is_null($this->countryFilter)) {
            $this->countryFilter = new CountryFilter();
        }
        //чтобы была надпись 'Осуществлялась конвертация валют в USD'
        //чтобы не было перехода в заказы иначе ссыслки не работают
        $this->countryFilter->all = 'true';
        return $this->countryFilter;
    }
}
