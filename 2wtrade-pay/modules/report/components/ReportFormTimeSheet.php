<?php
namespace app\modules\report\components;

use app\models\Country;
use app\modules\callcenter\models\CallCenter;
use app\modules\crocotime\models\CrocotimeWorkTime;
use app\modules\salary\models\Designation;
use app\modules\salary\models\Holiday;
use app\modules\salary\models\Office;
use app\modules\salary\models\Person;
use app\modules\callcenter\models\CallCenterUser;
use app\modules\callcenter\models\CallCenterWorkTime;
use app\modules\salary\models\PersonLink;
use app\modules\salary\models\WorkTimePlan;
use app\modules\report\extensions\Query;
use app\widgets\Panel;
use yii\data\ArrayDataProvider;
use app\modules\report\components\filters\DateWorkFilter;
use app\modules\report\components\filters\OfficeFilter;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class ReportFormTimeSheet
 * @package app\modules\report\components
 */
class ReportFormTimeSheet extends ReportForm
{
    public $groupBy = self::GROUP_BY_LAST_FOREIGN_OPERATOR;

    /**
     * @var DateWorkFilter
     */
    protected $dailyFilter;

    /**
     * @var OfficeFilter
     */
    protected $officeFilter;

    /**
     * @param array $params
     * @return ArrayDataProvider
     */
    public function apply($params)
    {
        if (!$params || empty($params['s']['office'])) {
            $this->panelAlert = Yii::t('common', 'Выберите офис');
            $this->panelAlertStyle = Panel::ALERT_DANGER;
        }

        $this->load($params);

        $this->query = new Query();

        $this->query->from([Person::tableName()]);
        $this->query->leftJoin(CallCenterUser::tableName(), CallCenterUser::tableName() . '.person_id=' . Person::tableName() . '.id');

        $this->getDateWorkFilter()->apply($this->query, $params);

        $dateFrom = $this->getDateWorkFilter()->from;
        $dateTo = $this->getDateWorkFilter()->to;

        $allModels = [];
        $dates = [];

        if (!$this->panelAlert) {

            $this->query->leftJoin(Designation::tableName(), Designation::tableName() . '.id=' . Person::tableName() . '.designation_id');
            $this->query->leftJoin(CallCenter::tableName(), CallCenter::tableName() . '.id=' . CallCenterUser::tableName() . '.callcenter_id');
            $this->query->leftJoin(Country::tableName(), Country::tableName() . '.id=' . CallCenter::tableName() . '.country_id');
            $this->query->leftJoin(Person::tableName() . ' as parent', Person::tableName() . '.parent_id=parent.id');
            $this->query->leftJoin(Designation::tableName() . ' as parent_designation', 'parent.designation_id=parent_designation.id');
            $this->query->leftJoin(PersonLink::tableName(), PersonLink::tableName() . '.person_id = ' . Person::tableName() . '.id and ' . PersonLink::tableName() . '.type = "' . PersonLink::TYPE_CROCOTIME . '"');
            $this->buildSelect()->applyFilters($params);
            $this->query->andWhere([
                'or',
                ['<=', Person::tableName() . '.start_date', date("Y-m-d", strtotime($dateTo))],
                ['is', Person::tableName() . '.start_date', null]
            ]);
            $this->query->andWhere([
                'or',
                ['>=', 'DATE_FORMAT(' . Person::tableName() . '.dismissal_date, "%Y-%m")', date("Y-m", strtotime($dateTo))],
                ['is', Person::tableName() . '.dismissal_date', null]
            ]);
            $this->query->groupBy([Person::tableName() . '.id']);
            $this->query->orderBy([
                Person::tableName() . '.parent_id' => SORT_ASC,
                Person::tableName() . '.active' => SORT_DESC,
                Person::tableName() . '.name' => SORT_ASC,
            ]);

            $allModels = $this->query->all();

            $calcSalaryLocal = 1;
            // праздники по стране
            $holidays = [];
            if ($this->getOfficeFilter()->office) {
                $office = Office::findOne($this->getOfficeFilter()->office);
                if ($office) {
                    $calcSalaryLocal = $office->calc_salary_local;
                    $holidays = ArrayHelper::index(
                        Holiday::find()
                            ->where(['country_id' => $office->country_id])
                            ->andWhere(['>=', 'date', date('Y-m-d', strtotime($dateFrom))])
                            ->andWhere(['<=', 'date', date('Y-m-d', strtotime($dateTo))])
                            ->asArray()
                            ->all(),
                        'date'
                    );
                }
            }

            $totalCount = 0;
            // по всем сотрудникам
            foreach ($allModels as &$model) {

                // Тим лидов группируем в его же команду
                if ($model['designation_team_lead']) {
                    $model['parent_id'] = $model['id'];
                    $model['parent_name'] = $model['fio'];
                    $model['parent_designation'] = $model['designation'];
                }

                $times = [];
                if ($model['call_center_user_ids'] && $model['designation_calc_work_time']) {
                    // у кого есть логины и он оператор

                    // для новых КЦ нужно взять только один логин а не все... это костыльное решение
                    $ids = CallCenterUser::getUniqueIdList($model['id']);

                    // может быть позже будем брать всех
                    // $ids = explode(',', $model['call_center_user_ids']);

                    $times = CallCenterWorkTime::getWorkTimes($ids, $dateFrom, $dateTo);
                }
                if (!$model['designation_calc_work_time'] && $model['crocotime_employee_id']) {
                    // для не операторов, попробуем по CrocoTime

                    $times = ArrayHelper::map(CrocotimeWorkTime::find()
                        ->select([
                            'time' => '(summary_time)/3600',
                            'date' => 'date',
                        ])
                        ->where(['crocotime_employee_id' => $model['crocotime_employee_id']])
                        ->andWhere(['>=', 'date', date('Y-m-d', strtotime($dateFrom))])
                        ->andWhere(['<=', 'date', date('Y-m-d', strtotime($dateTo))])
                        ->asArray()
                        ->all(), 'date', 'time');
                }

                $planTimes = WorkTimePlan::getPlanList($model['id'], $dateFrom, $dateTo, $model['start_date'], $model['dismissal_date']);

                $date = $dateFrom;
                $dateEnd = $dateTo;

                $model['total'] = 0;
                while (strtotime($date) <= strtotime($dateEnd)) {
                    $curDate = date("Y-m-d", strtotime($date));
                    $model['dates'][$curDate] =
                        (isset($times[$curDate]) &&
                            ($model['start_date'] <= $curDate || $model['start_date'] == '') &&
                            ($model['dismissal_date'] >= $curDate || $model['dismissal_date'] == '')) ? $times[$curDate] : 0;

                    if ($model['dates'][$curDate] > 16) {
                        // если больше 16 часов в день работает, то это явно косяк и пишем норму (если ее нет то 8) часов
                        $model['dates'][$curDate] = $planTimes[$curDate] ?? 8;
                    }

                    $model['total'] += $model['dates'][$curDate];
                    $date = date("Y-m-d", strtotime("+1 day", strtotime($date)));
                }
                $model['plan'] = $planTimes;
            }

            $dates = $allModels[0]['dates'] ?? [];
            foreach ($dates as $k => $v) {
                $dates[$k] = isset($holidays[$k]) ? 1 : 0;
            }

            ArrayHelper::multisort($allModels,
                ['parent_id', $calcSalaryLocal ? 'salary_local' : 'salary_usd', 'active', 'fio'],
                [SORT_ASC, SORT_DESC, SORT_DESC, SORT_ASC]);
        }


        $dataProvider = new ArrayDataProvider([
            'allModels' => ['data' => $allModels, 'dates' => $dates]
        ]);
        $dataProvider->pagination->pageSize = $dataProvider->pagination->totalCount;


        return $dataProvider;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $output = '';

        return $output;
    }


    /**
     * @inheritdoc
     */
    protected function buildSelect()
    {
        $this->query->select([
            'id' => Person::tableName() . '.id',
            'fio' => Person::tableName() . '.name',
            'start_date' => Person::tableName() . '.start_date',
            'dismissal_date' => Person::tableName() . '.dismissal_date',
            'active' => Person::tableName() . '.active',
            'parent_id' => Person::tableName() . '.parent_id',
            'parent_name' => 'parent.name',
            'parent_designation' => 'parent_designation.name',
            'designation' => Designation::tableName() . '.name',
            'designation_id' => Designation::tableName() . '.id',
            'designation_team_lead' => Designation::tableName() . '.team_lead',
            'designation_calc_work_time' => Designation::tableName() . '.calc_work_time',
            'salary_local' => Person::tableName() . '.salary_local',
            'salary_usd' => Person::tableName() . '.salary_usd',
            'call_center_user_ids' => 'GROUP_CONCAT(DISTINCT ' . CallCenterUser::tableName() . '.id)',
            'call_center_user_logins' => 'GROUP_CONCAT(DISTINCT ' . CallCenterUser::tableName() . '.user_login)',
            'country_names' => 'GROUP_CONCAT(' . Country::tableName() . '.name)',
            'crocotime_employee_id' => PersonLink::tableName() . '.foreign_id',
        ]);
        return $this;
    }


    public function getDateWorkFilter()
    {
        if (is_null($this->dailyFilter)) {
            $this->dailyFilter = new DateWorkFilter();
        }
        return $this->dailyFilter;
    }

    public function getOfficeFilter()
    {
        if (is_null($this->officeFilter)) {
            $this->officeFilter = new OfficeFilter();
        }
        return $this->officeFilter;
    }

    /**
     * @param array $params
     * @return ReportForm
     */
    protected function applyFilters($params)
    {
        $this->getOfficeFilter()->apply($this->query, $params);
        return $this;
    }
}
