<?php

namespace app\modules\report\components;

use app\models\CurrencyRate;
use app\modules\order\models\Order;
use app\modules\order\models\OrderProduct;
use app\modules\order\models\OrderStatus;
use app\modules\report\components\filters\CalculateDollarFilter;
use app\modules\report\components\filters\CalculatePercentFilter;
use app\modules\report\components\filters\CallCenterFilter;
use app\modules\report\components\filters\CountryFilter;
use app\modules\report\components\filters\CountrySelectFilter;
use app\modules\report\components\filters\CountryDeliverySelectMultipleFilter;
use app\modules\report\components\filters\CountryDeliveryPartnerSelectMultipleFilter;
use app\modules\report\components\filters\DateFilter;
use app\modules\report\components\filters\DateMonthYearFilter;
use app\modules\report\components\filters\DeliveryFilter;
use app\modules\report\components\filters\ForeignOperatorFilter;
use app\modules\report\components\filters\PackageFilter;
use app\modules\report\components\filters\PaymentAdvertisingFilter;
use app\modules\report\components\filters\ProductCategoryFilter;
use app\modules\report\components\filters\ProductFilter;
use app\modules\report\components\filters\OrderStatusFilter;
use app\modules\report\components\filters\OrderLogisticListFilter;
use app\modules\report\components\filters\SourceFilter;
use app\modules\report\widgets\ReportGroupBy;
use app\widgets\Panel;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\report\components\filters\DeliveryPartnerFilter;
use app\modules\report\components\filters\AnswerTypeSmsPollHistoryFilter;
use app\modules\report\components\filters\AnswerTypeSmsNotificationFilter;
use app\modules\report\components\filters\DateSmsPollHistoryFilter;
use app\modules\report\components\filters\DateSmsNotificationFilter;

/**
 * Class ReportForm
 * @package app\modules\report\components
 *
 * @property array $mapStatusesAsArray
 */
abstract class ReportForm extends Model
{
    const GROUP_BY_CREATED_AT = 'created_at';
    const GROUP_BY_UPDATED_AT = 'updated_at';
    const GROUP_BY_CALL_CENTER_SENT_AT = 'c_sent_at';
    const GROUP_BY_CALL_CENTER_APPROVED_AT = 'c_approved_at';
    const GROUP_BY_DELIVERY = 'courier';
    const GROUP_BY_DELIVERY_CREATED_AT = 'd_created_at';
    const GROUP_BY_DELIVERY_SENT_AT = 'd_sent_at';
    //нужна для заказов где нет даты отправки, есть только дата создания
    const GROUP_BY_DELIVERY_SENT_AT_NOT_EMPTY = 'd_sent_at_not_empty';
    const GROUP_BY_DELIVERY_FIRST_SENT_AT = 'd_first_sent_at';
    const GROUP_BY_DELIVERY_RETURNED_AT = 'd_returned_at';
    const GROUP_BY_DELIVERY_APPROVED_AT = 'd_approved_at';
    const GROUP_BY_DELIVERY_ACCEPTED_AT = 'd_accepted_at';
    const GROUP_BY_DELIVERY_PAID_AT = 'd_paid_at';
    const GROUP_BY_PRODUCTS = 'products';
    const GROUP_BY_LEAD_PRODUCTS = 'lead_products';
    const GROUP_BY_CUSTOMER_DISTRIT = 'customer_district';
    const GROUP_BY_CUSTOMER_CITY = 'customer_city';
    const GROUP_BY_CUSTOMER_PROVINCE = 'customer_province';
    const GROUP_BY_CUSTOMER_ZIP = 'customer_zip';
    const GROUP_BY_WEBMASTER_IDENTIFIER = 'webmaster_identifier';
    const GROUP_BY_LAST_FOREIGN_OPERATOR = 'last_foreign_operator';
    const GROUP_BY_YEAR_MONTH = 'year_month';
    const GROUP_BY_COUNTRY_DATE = 'country, date';
    const GROUP_BY_COUNTRY = 'country';
    const GROUP_BY_COUNTRY_PRODUCTS = 'country, products';
    const GROUP_BY_COUNTRY_LEAD_PRODUCTS = 'country, lead_products';
    const GROUP_BY_DELIVERY_ZIPCODES = 'zipcodes';
    const GROUP_BY_LANDING = 'landing';
    const GROUP_BY_CHECK_SENT_AT = 'check_sent_at';
    const GROUP_BY_CHECK_DONE_AT = 'check_done_at';
    const GROUPBY_SMS_CREATED_AT = 'sms_created_at';
    const GROUPBY_SMS_ANSWERED_AT = 'sms_answered_at';
    const GROUPBY_NOTIFICATION_CREATED_AT = 'notification_created_at';
    const GROUPBY_NOTIFICATION_ANSWERED_AT = 'notification_answered_at';
    const GROUP_BY_DATE_COUNTRY_DELIVERY = 'date, country_id, delivery_id';
    const GROUP_BY_COUNTRY_DELIVERY = 'country_id, delivery_id';
    const GROUP_BY_PROVINCE_CITY_SUBDISTRICT_ZIP = 'province_city_subdistrict_zip';
    const GROUP_BY_SUBDISTRICT_ZIP = 'subdistrict_zip';
    const GROUP_BY_TS_SPAWN = 'ts_spawn';
    const GROUP_BY_DATE_PRODUCTS = 'date, product_id';
    const GROUP_BY_PRODUCT_CATEGORY = 'product_categories';


    /**
     * @var string
     */
    public $panelAlert;

    /**
     * @var string
     */
    public $panelAlertStyle;

    /**
     * @var \app\modules\report\extensions\Query
     */
    public $query;

    /**
     * @var string
     */
    public $groupBy;

    /**
     * @var string
     */
    public $withPercent;

    /**
     * @var string
     */
    public $dollar;

    /**
     * @var array
     */
    public $groupByCollection = [];

    /**
     * @var string|array
     */
    public $searchQuery;

    /**
     * @var DateFilter
     */
    protected $dateFilter;

    /**
     * @var DateMonthYearFilter
     */
    protected $dateMonthYearFilter;

    /**
     * @var ProductCategoryFilter
     */
    protected $productCategoryFilter;

    /**
     * @var ProductFilter
     */
    protected $productFilter;

    /**
     * @var PackageFilter
     */
    protected $packageFilter;

    /**
     * @var CallCenterFilter
     */
    protected $callCenterFilter;

    /**
     * @var DeliveryFilter
     */
    protected $deliveryFilter;

    /**
     * @var CountryFilter
     */
    protected $countryFilter;

    /**
     * @var CountrySelectFilter
     */
    protected $countrySelectFilter;

    /**
     * @var CountryDeliverySelectMultipleFilter
     */
    protected $countryDeliverySelectMultipleFilter;

    /**
     * @var CountryDeliveryPartnerSelectMultipleFilter
     */
    protected $countryDeliveryPartnerSelectMultipleFilter;

    /**
     * @var OrderLogisticListFilter
     */
    protected $orderLogisticListFilter;

    /**
     * @var CalculatePercentFilter
     */
    protected $calculatePercentFilter;

    /**
     * @var PaymentAdvertisingFilter
     */
    protected $paymentAdvertisingFilter;

    /**
     * @var CalculateDollarFilter
     */
    protected $calculateDollarFilter;

    /**
     * @var OrderStatusFilter
     */
    protected $orderStatusFilter;

    /**
     * @var DeliveryPartnerFilter
     */
    protected $deliveryPartnerFilter;

    /**
     * @var ForeignOperatorFilter
     */
    protected $foreignOperatorFilter;

    /**
     * @var SourceFilter
     */
    protected $sourceFilter;

    /**
     * @var array
     */
    protected $collectionStatuses;

    /**
     * @var AnswerTypeSmsPollHistoryFilter
     */
    protected $answerTypeSmsPollHistoryFilter;

    /**
     * @var AnswerTypeSmsNotificationFilter
     */
    protected $answerTypeSmsNotificationFilter;

    /**
     * @var DateSmsPollHistoryFilter
     */
    protected $dateSmsPollHistoryFilter;

    /**
     * @var DateSmsNotificationFilter
     */
    protected $dateSmsNotificationFilter;

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public abstract function apply($params);

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public abstract function restore($form);

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->groupByCollection = [
            self::GROUP_BY_CREATED_AT => Yii::t('common', 'Дата создания'),
            self::GROUP_BY_UPDATED_AT => Yii::t('common', 'Дата обновления'),
            self::GROUP_BY_CALL_CENTER_SENT_AT => Yii::t('common', 'Дата отправки (колл-центр)'),
            self::GROUP_BY_CALL_CENTER_APPROVED_AT => Yii::t('common', 'Дата одобрения (колл-центр)'),
            self::GROUP_BY_DELIVERY_CREATED_AT => Yii::t('common', 'Дата создания (курьерка)'),
            //self::GROUP_BY_DELIVERY_SENT_AT => Yii::t('common', 'Дата отправки (курьерка)'),
            self::GROUP_BY_DELIVERY_SENT_AT_NOT_EMPTY => Yii::t('common', 'Дата передачи заказа в курьерку'),
            self::GROUP_BY_DELIVERY_FIRST_SENT_AT => Yii::t('common', 'Дата первой отправки (курьерка)'),
            self::GROUP_BY_DELIVERY_RETURNED_AT => Yii::t('common', 'Дата возврата (курьерка)'),
            self::GROUP_BY_DELIVERY_APPROVED_AT => Yii::t('common', 'Дата утверждения (курьерка)'),
            self::GROUP_BY_DELIVERY_ACCEPTED_AT => Yii::t('common', 'Дата принятия (курьерка)'),
            self::GROUP_BY_DELIVERY_PAID_AT => Yii::t('common', 'Дата оплаты (курьерка)'),
            self::GROUP_BY_TS_SPAWN => Yii::t('common', 'Дата создания у партнера (лид)'),
            self::GROUP_BY_CUSTOMER_DISTRIT => Yii::t('common', 'Район'),
            self::GROUP_BY_CUSTOMER_CITY => Yii::t('common', 'Город'),
            self::GROUP_BY_CUSTOMER_PROVINCE => Yii::t('common', 'Провинция'),
            self::GROUP_BY_CUSTOMER_ZIP => Yii::t('common', 'ZIP код'),
            self::GROUP_BY_COUNTRY => Yii::t('common', 'Страна'),
        ];
    }

    /**
     * @return string
     */
    public function formName()
    {
        return 's';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['groupBy', 'in', 'range' => array_keys($this->groupByCollection)],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'groupBy' => Yii::t('common', 'Группировка'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function load($data, $formName = null)
    {
        $this->searchQuery = $data;

        return parent::load($data, $formName);
    }

    /**
     * @return DateFilter
     */
    public function getDateFilter()
    {
        if (is_null($this->dateFilter)) {
            $this->dateFilter = new DateFilter();
        }

        return $this->dateFilter;
    }

    /**
     * @return DateMonthYearFilter
     */
    public function getDateMonthYearFilter()
    {
        if (is_null($this->dateMonthYearFilter)) {
            $this->dateMonthYearFilter = new DateMonthYearFilter();
        }

        return $this->dateMonthYearFilter;
    }

    /**
     * @return ProductFilter
     */
    public function getProductFilter()
    {
        if (is_null($this->productFilter)) {
            $this->productFilter = new ProductFilter(['reportForm' => $this]);
        }

        return $this->productFilter;
    }

    /**
     * @return PackageFilter
     */
    public function getPackageFilter()
    {
        if (is_null($this->packageFilter)) {
            $this->packageFilter = new PackageFilter(['reportForm' => $this]);
        }

        return $this->packageFilter;
    }

    /**
     * @return ProductCategoryFilter
     */
    public function getProductCategoryFilter()
    {
        if (is_null($this->productCategoryFilter)) {
            $this->productCategoryFilter = new ProductCategoryFilter(['reportForm' => $this]);
        }

        return $this->productCategoryFilter;
    }

    /**
     * @return CallCenterFilter
     */
    public function getCallCenterFilter()
    {
        if (is_null($this->callCenterFilter)) {
            $this->callCenterFilter = new CallCenterFilter(['reportForm' => $this]);
        }

        return $this->callCenterFilter;
    }

    /**
     * @return DeliveryFilter
     */
    public function getDeliveryFilter()
    {
        if (is_null($this->deliveryFilter)) {
            $this->deliveryFilter = new DeliveryFilter(['reportForm' => $this]);
        }

        return $this->deliveryFilter;
    }

    /**
     * @return CountryFilter
     */
    public function getCountryFilter()
    {
        if (is_null($this->countryFilter)) {
            $this->countryFilter = new CountryFilter();
        }

        return $this->countryFilter;
    }

    /**
     * @return CountrySelectFilter
     */
    public function getCountrySelectFilter()
    {
        if (is_null($this->countrySelectFilter)) {
            $this->countrySelectFilter = new CountrySelectFilter();
        }

        return $this->countrySelectFilter;
    }

    /**
     * @return CountryDeliverySelectMultipleFilter
     */
    public function getCountryDeliverySelectMultipleFilter()
    {
        if (is_null($this->countryDeliverySelectMultipleFilter)) {
            $this->countryDeliverySelectMultipleFilter = new CountryDeliverySelectMultipleFilter();
        }

        return $this->countryDeliverySelectMultipleFilter;
    }

    /**
     * @return CountryDeliveryPartnerSelectMultipleFilter
     */
    public function getCountryDeliveryPartnerSelectMultipleFilter()
    {
        if (is_null($this->countryDeliveryPartnerSelectMultipleFilter)) {
            $this->countryDeliveryPartnerSelectMultipleFilter = new CountryDeliveryPartnerSelectMultipleFilter();
        }

        return $this->countryDeliveryPartnerSelectMultipleFilter;
    }

    /**
     * @return OrderLogisticListFilter
     */
    public function getOrderLogisticListFilter()
    {
        if (is_null($this->orderLogisticListFilter)) {
            $this->orderLogisticListFilter = new OrderLogisticListFilter();
        }

        return $this->orderLogisticListFilter;
    }

    /**
     * @return CalculatePercentFilter
     */
    public function getCalculatePercentFilter()
    {
        if (is_null($this->calculatePercentFilter)) {
            $this->calculatePercentFilter = new CalculatePercentFilter(['reportForm' => $this]);
        }

        return $this->calculatePercentFilter;
    }

    /**
     * @return PaymentAdvertisingFilter
     */
    public function getPaymentAdvertisingFilter()
    {
        if (is_null($this->paymentAdvertisingFilter)) {
            $this->paymentAdvertisingFilter = new PaymentAdvertisingFilter();
        }

        return $this->paymentAdvertisingFilter;
    }

    /**
     * @return CalculateDollarFilter
     */
    public function getCalculateDollarFilter()
    {
        if (is_null($this->calculateDollarFilter)) {
            $this->calculateDollarFilter = new CalculateDollarFilter(['reportForm' => $this]);
        }

        return $this->calculateDollarFilter;
    }

    /**
     * @return OrderStatusFilter
     */
    public function getOrderStatusFilter()
    {
        if (is_null($this->orderStatusFilter)) {
            $this->orderStatusFilter = new OrderStatusFilter(['reportForm' => $this]);
        }

        return $this->orderStatusFilter;
    }

    /**
     * @return DeliveryPartnerFilter
     */
    public function getDeliveryPartnerFilter()
    {
        if (is_null($this->deliveryPartnerFilter)) {
            $this->deliveryPartnerFilter = new DeliveryPartnerFilter(['reportForm' => $this]);
        }

        return $this->deliveryPartnerFilter;
    }

    /**
     * @return ForeignOperatorFilter
     */
    public function getForeignOperatorFilter()
    {
        if (is_null($this->foreignOperatorFilter)) {
            $this->foreignOperatorFilter = new ForeignOperatorFilter();
        }

        return $this->foreignOperatorFilter;
    }

    /**
     * @return AnswerTypeSmsPollHistoryFilter
     */
    public function getAnswerTypeSmsPollHistoryFilter()
    {
        if (is_null($this->answerTypeSmsPollHistoryFilter)) {
            $this->answerTypeSmsPollHistoryFilter = new AnswerTypeSmsPollHistoryFilter();
        }

        return $this->answerTypeSmsPollHistoryFilter;
    }

    /**
     * @return AnswerTypeSmsNotificationFilter
     */
    public function getAnswerTypeSmsNotificationFilter()
    {
        if (is_null($this->answerTypeSmsNotificationFilter)) {
            $this->answerTypeSmsNotificationFilter = new AnswerTypeSmsNotificationFilter();
        }

        return $this->answerTypeSmsNotificationFilter;
    }

    /**
     * @return DateSmsPollHistoryFilter
     */
    public function getDateSmsPollHistoryFilter()
    {
        if (is_null($this->dateSmsPollHistoryFilter)) {
            $this->dateSmsPollHistoryFilter = new DateSmsPollHistoryFilter();
        }

        return $this->dateSmsPollHistoryFilter;
    }

    /**
     * @return DateSmsNotificationFilter
     */
    public function getDateSmsNotificationFilter()
    {
        if (is_null($this->dateSmsNotificationFilter)) {
            $this->dateSmsNotificationFilter = new DateSmsNotificationFilter();
        }

        return $this->dateSmsNotificationFilter;
    }

    /**
     * @return SourceFilter
     */
    public function getSourceFilter()
    {
        if (is_null($this->sourceFilter)) {
            $this->sourceFilter = new SourceFilter(['reportForm' => $this]);
        }

        return $this->sourceFilter;
    }


    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restoreGroupBy($form)
    {
        return ReportGroupBy::widget([
            'form' => $form,
            'reportForm' => $this,
        ]);
    }

    /**
     * @return array
     */
    protected function getCollectionStatuses()
    {
        if (is_null($this->collectionStatuses)) {
            $this->collectionStatuses = OrderStatus::find()->collection();
        }

        return $this->collectionStatuses;
    }

    /**
     * @return array
     */
    public function getMapStatusesAsArray()
    {
        $ids = [];

        if (method_exists($this, 'getMapStatuses')) {
            $mapStatuses = $this->getMapStatuses();

            foreach ($mapStatuses as $statuses) {
                $ids = array_merge($ids, $statuses);
            }

            asort($ids);
        }
        $ids = array_unique($ids);
        asort($ids);

        return $ids;
    }

    /**
     * Подготовка курса валюты. Просто вывод информации о курсе
     */
    protected function prepareCurrencyRate()
    {
        if ($this->getCalculateDollarFilter()->dollar || $this->dollar == 'on') {
            if ($this->getCountryFilter()->all) {
                $this->panelAlert = Yii::t('common', 'Осуществлялась конвертация валют в USD');
                $this->panelAlertStyle = Panel::ALERT_PRIMARY;
            } else {
                if (Yii::$app->user->country->currency_id) {
                    /** @var CurrencyRate $currencyRate */
                    $currencyRate = CurrencyRate::find()
                        ->joinWith('currency')
                        ->where([CurrencyRate::tableName() . '.currency_id' => Yii::$app->user->country->currency_id])
                        ->one();

                    $this->panelAlert = Yii::t('common', 'Осуществлялась конвертация валют по курсу 1/{rate} (USD/{currency}).', [
                        'rate' => $currencyRate->rate,
                        'currency' => $currencyRate->currency->char_code,
                    ]);

                    $this->panelAlertStyle = Panel::ALERT_PRIMARY;
                } else {
                    $this->panelAlert = Yii::t('common', 'У выбранной страны не назначена валюта.');
                    $this->panelAlertStyle = Panel::ALERT_DANGER;
                }
            }
        }
    }

    /**
     * Подготовка "SELECT" для запроса
     * @return ReportForm
     */
    protected function buildSelect()
    {

    }

    /**
     * Подготовка "SELECT" для запроса по статусам
     */
    protected function buildSelectStatuses()
    {
        $statuses = $this->getCollectionStatuses();

        foreach ($statuses as $id => $status) {
            $this->query->addInConditionCount(Order::tableName() . '.status_id', $id, $status);
        }

        $this->query->addInConditionCount(Order::tableName() . '.status_id', array_keys($statuses), Yii::t('common', 'Всего'));
    }

    /**
     * Подготовка "SELECT" для запроса по статусам из мапинга
     */
    protected function buildSelectMapStatuses()
    {
        if (method_exists($this, 'getMapStatuses')) {
            $mapStatuses = $this->getMapStatuses();

            foreach ($mapStatuses as $name => $statuses) {
                $this->query->addInConditionCount(Order::tableName() . '.status_id', $statuses, $name);
            }

            $this->query->addInConditionCount(Order::tableName() . '.status_id', $this->getMapStatusesAsArray(), Yii::t('common', 'Всего'));

            $this->query->andWhere(['in', Order::tableName() . '.status_id', $this->getMapStatusesAsArray()]);
        }
    }

    /**
     * Подготовка "SELECT" для запроса по товарам
     */
    protected function buildSelectProducts()
    {
        $statuses = $this->getCollectionStatuses();

        foreach ($statuses as $id => $status) {
            $sumCondition = $this->query->buildIfCondition(Order::tableName() . '.status_id', $id, OrderProduct::tableName() . '.quantity', 0);
            $this->query->addSumCondition($sumCondition, $status);
        }

        $sumCondition = $this->query->buildIfCondition(Order::tableName() . '.status_id', array_keys($statuses), OrderProduct::tableName() . '.quantity', 0);
        $this->query->addSumCondition($sumCondition, Yii::t('common', 'Всего'));
    }

    /**
     * Подготовка "SELECT" для запроса по товарам из мапинга
     */
    protected function buildSelectMapProducts()
    {
        if (method_exists($this, 'getMapStatuses')) {
            $mapStatuses = $this->getMapStatuses();

            foreach ($mapStatuses as $name => $statuses) {
                $sumCondition = $this->query->buildIfCondition(Order::tableName() . '.status_id', $statuses, Order::tableName() . '.id', 'NULL');
                $this->query->addCountDistinctCondition($sumCondition, $name);
            }

            $sumCondition = $this->query->buildIfCondition(Order::tableName() . '.status_id', $this->getMapStatusesAsArray(), Order::tableName() . '.id', 'NULL');
            $this->query->addCountDistinctCondition($sumCondition, Yii::t('common', 'Всего'));

            $this->query->andWhere(['in', Order::tableName() . '.status_id', $this->getMapStatusesAsArray()]);
        }
    }

    /**
     * @param array $params
     * @return ReportForm
     */
    protected function applyFilters($params)
    {
        return $this;
    }

    /**
     * @param array $params
     * @return ReportForm
     */
    protected function applyBaseFilters($params)
    {
        $this->getCountryFilter()->apply($this->query, $params);
        $this->getCalculatePercentFilter()->apply($this->query, $params);
        $this->getPaymentAdvertisingFilter()->apply($this->query, $params);
        $this->getCalculateDollarFilter()->apply($this->query, $params);

        return $this;
    }
}
