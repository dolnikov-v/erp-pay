<?php
namespace app\modules\report\components;

use app\models\AccountingCost;
use yii\data\ArrayDataProvider;
use app\modules\report\components\filters\AccountingCostsFilter;
use Yii;

/**
 * Class ReportFormAccountingCosts
 * @package app\modules\report\components\
 */
class ReportFormAccountingCosts extends ReportForm
{
    /**
     * @var AccountingCostsFilter
     */
    protected $accountingCostsFilter;

    /**
     * @param array $params
     * @return ArrayDataProvider
     */
    public function apply($params)
    {
        $this->query = AccountingCost::find();
        $this->applyFilters($params);
        $this->query->orderBy(['date_article' => SORT_DESC]);
        $data = $this->query->asArray()->all();

        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
        ]);

        return $dataProvider;
    }

    /**
     * @return AccountingCostsFilter
     */
    public function getAccountingCostsFilter()
    {
        if (is_null($this->accountingCostsFilter)) {
            $this->accountingCostsFilter = new AccountingCostsFilter();
        }

        return $this->accountingCostsFilter;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return '';
    }

    /**
     * @param array $params
     * @return $this
     */
    protected function applyFilters($params)
    {
        $this->getAccountingCostsFilter()->apply($this->query, $params);
        return $this;
    }
}
