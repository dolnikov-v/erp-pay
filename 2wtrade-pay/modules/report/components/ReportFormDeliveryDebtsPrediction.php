<?php

namespace app\modules\report\components;

use app\models\Country;
use app\models\Currency;
use app\models\CurrencyRate;
use app\models\CurrencyRateHistory;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\models\Order;
use app\modules\order\models\OrderFinancePrediction;
use app\modules\order\models\OrderStatus;
use Yii;
use yii\data\ArrayDataProvider;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * Class ReportFormDeliveryDebtsPrediction
 * @package app\modules\report\components
 */
class ReportFormDeliveryDebtsPrediction extends ReportFormDeliveryDebts
{
    /**
     * @param array $params
     * @return ArrayDataProvider
     */
    public function apply($params)
    {
        $records = $this->prepareModels($params);

        if (isset($params['sort'])) {
            $prefix = substr($params['sort'], 0, 1) == '-';
            $sortKey = $params['sort'];
            if ($prefix) {
                $sortKey = substr($sortKey, 1);
            }
            if (in_array($sortKey, ['sum_total_dollars', 'common_charges_dollars', 'end_sum_dollars', 'charges_real_dollars', 'end_sum_done_dollars', 'buyout', 'money_received', 'charges_in_process_dollars', 'in_process', 'unbuyout', 'charges_unbuyout_dollars', 'charges_buyout_dollars'])) {
                $countrySum = [];
                $deliverySum = [];
                foreach ($records as $key => $record) {
                    if (!isset($countrySum[$record['country_id']])) {
                        $countrySum[$record['country_id']] = 0;
                    }
                    if (!isset($deliverySum[$record['delivery_id']])) {
                        $deliverySum[$record['delivery_id']] = 0;
                    }

                    $countrySum[$record['country_id']] += $record[$sortKey];
                    $deliverySum[$record['delivery_id']] += $record[$sortKey];
                }

                usort($records, function ($itemA, $itemB) use ($countrySum, $deliverySum, $prefix, $sortKey) {
                    $answer = 0;
                    if ($itemA['country_id'] != $itemB['country_id']) {
                        if ($countrySum[$itemA['country_id']] == $countrySum[$itemB['country_id']]) {
                            $answer = $itemA['country_id'] < $itemB['country_id'] ? -1 : 1;
                        } else {
                            $answer = ($countrySum[$itemA['country_id']] < $countrySum[$itemB['country_id']]) ? -1 : 1;
                        }
                    } else {
                        if ($itemA['delivery_id'] != $itemB['delivery_id']) {
                            if ($deliverySum[$itemA['delivery_id']] == $deliverySum[$itemB['delivery_id']]) {
                                $answer = $itemA['delivery_id'] < $itemB['delivery_id'] ? -1 : 1;
                            } else {
                                $answer = ($deliverySum[$itemA['delivery_id']] < $deliverySum[$itemB['delivery_id']]) ? -1 : 1;
                            }
                        } else {
                            if ($itemA[$sortKey] != $itemB[$sortKey]) {
                                $answer = $itemA[$sortKey] < $itemB[$sortKey] ? -1 : 1;
                            } elseif ($itemA['start_month_time'] != $itemB['start_month_time']) {
                                $answer = $itemA['start_month_time'] < $itemB['start_month_time'] ? -1 : 1;
                            }
                        }
                    }

                    return $prefix ? -1 * $answer : $answer;
                });
            } elseif (in_array($sortKey, ['start_month_time', 'last_date_payment'])) {
                usort($records, function ($itemA, $itemB) use ($prefix, $sortKey) {
                    $answer = 0;
                    if ($itemA['country_id'] != $itemB['country_id']) {
                        if ($itemA[$sortKey] == $itemB[$sortKey]) {
                            $answer = $itemA['country_id'] < $itemB['country_id'] ? -1 : 1;
                        } else {
                            $answer = ($itemA[$sortKey] < $itemB[$sortKey]) ? -1 : 1;
                        }
                    } else {
                        if ($itemA['delivery_id'] != $itemB['delivery_id']) {
                            if ($itemA[$sortKey] == $itemB[$sortKey]) {
                                $answer = $itemA['delivery_id'] < $itemB['delivery_id'] ? -1 : 1;
                            } else {
                                $answer = ($itemA[$sortKey] < $itemB[$sortKey]) ? -1 : 1;
                            }
                        } elseif ($itemA[$sortKey] != $itemB[$sortKey]) {
                            $answer = $itemA[$sortKey] < $itemB[$sortKey] ? -1 : 1;
                        }
                    }

                    return $prefix ? -1 * $answer : $answer;
                });
            }
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $records,
            'pagination' => false,
            'sort' => [
                'attributes' => [
                    'start_month_time' => [
                        'asc' => [],
                        'desc' => [],
                    ],
                    'last_date_payment' => [
                        'asc' => [],
                        'desc' => [],
                    ],
                    'end_sum_done_dollars' => [
                        'asc' => [],
                        'desc' => [],
                    ],
                    'charges_in_process_dollars' => [
                        'asc' => [],
                        'desc' => [],
                    ],
                    'end_sum_dollars' => [
                        'asc' => [],
                        'desc' => [],
                    ],
                    'sum_total_dollars' => [
                        'asc' => [],
                        'desc' => [],
                    ],
                    'buyout' => [
                        'asc' => [],
                        'desc' => [],
                    ],
                    'money_received' => [
                        'asc' => [],
                        'desc' => [],
                    ],
                    'in_process' => [
                        'asc' => [],
                        'desc' => [],
                    ],
                    'unbuyout' => [
                        'asc' => [],
                        'desc' => [],
                    ],
                    'charges_buyout_dollars' => [
                        'asc' => [],
                        'desc' => [],
                    ],
                    'charges_unbuyout_dollars' => [
                        'asc' => [],
                        'desc' => [],
                    ],
                ]
            ]
        ]);

        return $dataProvider;
    }

    /**
     * @param $params
     * @return ArrayDataProvider
     */
    public function applyCommonTable($params)
    {
        $records = $this->prepareModels($params, false);

        $dataProvider = new ArrayDataProvider([
            'allModels' => $records,
            'pagination' => false,
            'sort' => false
        ]);

        return $dataProvider;
    }

    /**
     * @param array $params
     * @param bool $groupByMonth
     * @return array
     */
    public function prepareModels($params, $groupByMonth = true)
    {
        $chargesAttributes = [
            'IFNULL(' . OrderFinancePrediction::tableName() . '.price_storage, 0)',
            'IFNULL(' . OrderFinancePrediction::tableName() . '.price_fulfilment, 0)',
            'IFNULL(' . OrderFinancePrediction::tableName() . '.price_packing, 0)',
            'IFNULL(' . OrderFinancePrediction::tableName() . '.price_package, 0)',
            'IFNULL(' . OrderFinancePrediction::tableName() . '.price_address_correction, 0)',
            'IFNULL(' . OrderFinancePrediction::tableName() . '.price_delivery, 0)',
            'IFNULL(' . OrderFinancePrediction::tableName() . '.price_redelivery, 0)',
            'IFNULL(' . OrderFinancePrediction::tableName() . '.price_delivery_back, 0)',
            'IFNULL(' . OrderFinancePrediction::tableName() . '.price_delivery_return, 0)',
            'IFNULL(' . OrderFinancePrediction::tableName() . '.price_cod_service, 0)',
            'IFNULL(' . OrderFinancePrediction::tableName() . '.price_vat, 0)',
        ];

        $sumCase = "SUM(CASE WHEN " . Order::tableName() . '.status_id in (' . implode(',', OrderStatus::getOnlyBuyoutList()) . ') THEN ' . OrderFinancePrediction::tableName() . '.price_cod ELSE NULL END)';
        $chargesSum = "SUM(" . implode(" + ", $chargesAttributes) . ")";


        $query = DeliveryRequest::find()->joinWith(['order.financePrediction', 'delivery.country.currencyRate.currency']);
        $query->where([Order::tableName() . '.status_id' => OrderStatus::getInDeliveryStatusList()]);

        $query->orderBy(['`country_id`' => SORT_ASC, '`delivery_id`' => SORT_ASC, '`start_month_time`' => SORT_ASC]);
        $query->groupBy(['delivery_id']);
        if ($groupByMonth) {
            $query->addGroupBy(['`month`']);
        }
        $query->select([
            'month' => 'DATE_FORMAT(FROM_UNIXTIME(COALESCE(' . DeliveryRequest::tableName() . '.second_sent_at, ' . DeliveryRequest::tableName() . '.sent_at, ' . DeliveryRequest::tableName() . '.created_at)), "%m/%Y")',
            'delivery_id' => DeliveryRequest::tableName() . '.delivery_id',
            'country_id' => Delivery::tableName() . '.country_id',
            'delivery_name' => Delivery::tableName() . '.name',
            'country_name' => Country::tableName() . '.name',
            'currency_rate' => CurrencyRate::tableName() . '.rate',
            'start_month_time' => 'MIN(COALESCE(' . DeliveryRequest::tableName() . '.second_sent_at, ' . DeliveryRequest::tableName() . '.sent_at, ' . DeliveryRequest::tableName() . '.created_at))',
            'currency' => Currency::tableName() . '.char_code',
            'country_slug' => Country::tableName() . '.slug',
            'in_process' => 'COUNT(' . Order::tableName() . '.status_id IN (' . implode(',', OrderStatus::getProcessDeliveryAndLogisticList()) . ') OR NULL)',
            'unbuyout' => 'COUNT(' . Order::tableName() . '.status_id IN (' . implode(',', OrderStatus::getOnlyNotBuyoutInDeliveryList()) . ') OR NULL)',
            'vsego' => 'COUNT(' . DeliveryRequest::tableName() . '.id)',
            'buyout' => 'COUNT(' . Order::tableName() . '.status_id IN (' . implode(',', OrderStatus::getBuyoutList()) . ') OR NULL)',
            'money_received' => 'COUNT(' . Order::tableName() . '.status_id IN (' . implode(',', OrderStatus::getOnlyMoneyReceivedList()) . ') OR NULL)',
            'charges_real' => new Expression('0'),
            'sum_total' => $sumCase,
            'charges_done' => new Expression('0'),
            'sum_total_done' => new Expression('0'),
            'charges_done_dollars' => new Expression('0'),
            'charges_buyout_dollars' => new Expression('0'),
            'charges_unbuyout_dollars' => new Expression('0'),
            'sum_total_done_dollars' => new Expression('0'),
            'charges_buyout' => 'SUM(IF (' . Order::tableName() . '.status_id IN (' . implode(',', OrderStatus::getOnlyBuyoutList()) . '), ' . implode('+', $chargesAttributes) . ', 0))',
            'charges_unbuyout' => 'SUM(IF (' . Order::tableName() . '.status_id IN (' . implode(',', OrderStatus::getNotBuyoutInDeliveryProcessList()) . '), ' . implode('+', $chargesAttributes) . ', 0))',
            'charges_in_process' => 'SUM(IF (' . Order::tableName() . '.status_id IN (' . implode(',', OrderStatus::getProcessDeliveryAndLogisticList()) . '), ' . implode('+', $chargesAttributes) . ', 0))',
        ]);



        $query->having('`charges_in_process` > 0 or `charges_buyout` > 0 or `charges_unbuyout` > 0 or `sum_total` > 0');
        $this->applyFiltersForQuery($query, $params, $groupByMonth);

        $records = $query->createCommand()->queryAll();

        $sumCase = "SUM(CASE WHEN " . Order::tableName() . '.status_id in (' . implode(',', OrderStatus::getOnlyMoneyReceivedList()) . ') THEN ' . OrderFinancePrediction::tableName() . '.price_cod ELSE NULL END)';
        $query = DeliveryRequest::find()->joinWith(['order.financePrediction', 'order.country']);
        $query->where([Order::tableName() . '.status_id' => array_merge(OrderStatus::getOnlyMoneyReceivedList(), OrderStatus::getOnlyNotBuyoutDoneList())]);
        $query->groupBy(['delivery_id', '`month`, `paid_date`']);
        $query->select([
            'month' => 'DATE_FORMAT(FROM_UNIXTIME(COALESCE(' . DeliveryRequest::tableName() . '.second_sent_at, ' . DeliveryRequest::tableName() . '.sent_at, ' . DeliveryRequest::tableName() . '.created_at)), "%m/%Y")',
            'charges_buyout' => 'SUM(IF (' . Order::tableName() . '.status_id IN (' . implode(',', OrderStatus::getOnlyMoneyReceivedList()) . '), ' . implode('+', $chargesAttributes) . ', 0))',
            'charges_unbuyout' => 'SUM(IF (' . Order::tableName() . '.status_id IN (' . implode(',', OrderStatus::getOnlyNotBuyoutDoneList()) . '), ' . implode('+', $chargesAttributes) . ', 0))',
            'sum_total_done' => $sumCase,
            'delivery_id' => DeliveryRequest::tableName() . '.delivery_id',
            'start_month_time' => 'MIN(COALESCE(' . DeliveryRequest::tableName() . '.second_sent_at, ' . DeliveryRequest::tableName() . '.sent_at, ' . DeliveryRequest::tableName() . '.created_at))',
            'paid_date' => 'DATE_FORMAT(FROM_UNIXTIME(' . DeliveryRequest::tableName() . '.paid_at), "%d")',
            'paid_at' => DeliveryRequest::tableName() . '.paid_at',
        ]);

        $query->having('`charges_buyout` > 0 or `charges_unbuyout` > 0 or `sum_total_done` > 0');

        // Исключаем запрещенные источники заказов
        $query->andWhere(['NOT IN', Order::tableName() . '.source_id', Yii::$app->user->rejectedSources]);
        $this->applyFiltersForQuery($query, $params, $groupByMonth);

        $tempModelsMoneyReceivedTemp = $query->createCommand()->queryAll();

        $tempModelsMoneyReceivedTemp = ArrayHelper::index($tempModelsMoneyReceivedTemp, null, ['delivery_id', 'month']);

        /**
         * @var Delivery[] $deliveries
         */
        $deliveries = Delivery::find()->where(['id' => ArrayHelper::getColumn($records, 'delivery_id')])->with([/*'chargesCalculatorModel',*/ 'country'])->indexBy('id')->all();

        $tempModelsMoneyReceived = [];

        foreach ($tempModelsMoneyReceivedTemp as $deliveryId => $data) {
            $tempModelsMoneyReceived[$deliveryId] = [];
            foreach ($data as $month => $values) {
                if (empty($month) || !isset($deliveries[$deliveryId])) {
                    $currencyRates = [];
                } else {
                    $currencyRates = CurrencyRateHistory::find()->where(['currency_id' => $deliveries[$deliveryId]->country->currency_id])
                        ->select([
                            'created_at' => 'DATE_FORMAT(FROM_UNIXTIME(`created_at`), "%d")',
                            'rate'
                        ])
                        ->andWhere(['>=', 'created_at', new Expression(strtotime('midnight first day of this month', $values[0]['start_month_time']))])
                        ->andWhere(['<=', 'created_at', new Expression(strtotime('midnight last day of this month', $values[0]['start_month_time']) + 86399)])
                        ->orderBy(['created_at' => SORT_DESC])
                        ->indexBy('created_at')
                        ->all();
                }
                if ($groupByMonth) {
                    $tempModelsMoneyReceived[$deliveryId][$month] = ['month' => $month, 'charges_done' => 0, 'sum_total_done' => 0, 'delivery_id' => $deliveryId, 'start_month_time' => $values[0]['start_month_time'], 'charges_done_dollars' => 0, 'sum_total_done_dollars' => 0, 'charges_buyout' => 0, 'charges_unbuyout' => 0, 'charges_buyout_dollars' => 0, 'charges_unbuyout_dollars' => 0];
                } else {
                    $tempModelsMoneyReceived[$deliveryId] = ['charges_done' => 0, 'sum_total_done' => 0, 'delivery_id' => $deliveryId, 'start_month_time' => $values[0]['start_month_time'], 'charges_done_dollars' => 0, 'sum_total_done_dollars' => 0, 'charges_buyout' => 0, 'charges_unbuyout' => 0, 'charges_buyout_dollars' => 0, 'charges_unbuyout_dollars' => 0];
                }

                foreach ($values as $value) {
                    if ($groupByMonth) {
                        $tempModelsMoneyReceived[$deliveryId][$month]['charges_done'] += $value['charges_buyout'] + $value['charges_unbuyout'];
                        $tempModelsMoneyReceived[$deliveryId][$month]['charges_buyout'] += $value['charges_buyout'];
                        $tempModelsMoneyReceived[$deliveryId][$month]['charges_unbuyout'] += $value['charges_unbuyout'];
                        $tempModelsMoneyReceived[$deliveryId][$month]['sum_total_done'] += $value['sum_total_done'];
                    } else {
                        $tempModelsMoneyReceived[$deliveryId]['charges_done'] += $value['charges_buyout'] + $value['charges_unbuyout'];
                        $tempModelsMoneyReceived[$deliveryId]['charges_buyout'] += $value['charges_buyout'];
                        $tempModelsMoneyReceived[$deliveryId]['charges_unbuyout'] += $value['charges_unbuyout'];
                        $tempModelsMoneyReceived[$deliveryId]['sum_total_done'] += $value['sum_total_done'];
                    }
                    $rate = null;
                    foreach ($currencyRates as $currencyRate) {
                        if ($currencyRate['created_at'] < $value['paid_date'] && !is_null($rate)) {
                            break;
                        }
                        $rate = $currencyRate['rate'];
                    }
                    if (!is_null($rate)) {
                        if ($groupByMonth) {
                            $tempModelsMoneyReceived[$deliveryId][$month]['charges_done_dollars'] += ($value['charges_buyout'] + $value['charges_unbuyout']) / $rate;
                            $tempModelsMoneyReceived[$deliveryId][$month]['charges_buyout_dollars'] += $value['charges_buyout'] / $rate;
                            $tempModelsMoneyReceived[$deliveryId][$month]['charges_unbuyout_dollars'] += $value['charges_unbuyout'] / $rate;
                            $tempModelsMoneyReceived[$deliveryId][$month]['sum_total_done_dollars'] += $value['sum_total_done'] / $rate;
                        } else {
                            $tempModelsMoneyReceived[$deliveryId]['charges_done_dollars'] += ($value['charges_buyout'] + $value['charges_unbuyout']) / $rate;
                            $tempModelsMoneyReceived[$deliveryId]['charges_buyout_dollars'] += $value['charges_buyout'] / $rate;
                            $tempModelsMoneyReceived[$deliveryId]['charges_unbuyout_dollars'] += $value['charges_unbuyout'] / $rate;
                            $tempModelsMoneyReceived[$deliveryId]['sum_total_done_dollars'] += $value['sum_total_done'] / $rate;
                        }
                    }
                }
            }
        }

        $countryTotal = [];
        foreach ($records as $key => $record) {

            if (!empty($records[$key]['currency_rate'])) {
                $records[$key]['sum_total_dollars'] = $records[$key]['sum_total'] / $records[$key]['currency_rate'];
                $records[$key]['charges_buyout_dollars'] = $records[$key]['charges_buyout'] / $records[$key]['currency_rate'];
                $records[$key]['charges_unbuyout_dollars'] = $records[$key]['charges_unbuyout'] / $records[$key]['currency_rate'];
                $records[$key]['charges_in_process_dollars'] = $records[$key]['charges_unbuyout'] / $records[$key]['currency_rate'];
            } else {
                $records[$key]['sum_total_dollars'] = 0;
                $records[$key]['charges_buyout_dollars'] = 0;
                $records[$key]['charges_unbuyout_dollars'] = 0;
                $records[$key]['charges_in_process_dollars'] = 0;
            }

            if ($groupByMonth) {
                if (isset($tempModelsMoneyReceived[$record['delivery_id']][$record['month']])) {
                    $records[$key]['sum_total'] += $tempModelsMoneyReceived[$record['delivery_id']][$record['month']]['sum_total_done'];
                    $records[$key]['sum_total_dollars'] += $tempModelsMoneyReceived[$record['delivery_id']][$record['month']]['sum_total_done_dollars'];
                    $records[$key]['charges_buyout'] += $tempModelsMoneyReceived[$record['delivery_id']][$record['month']]['charges_buyout'];
                    $records[$key]['charges_unbuyout'] += $tempModelsMoneyReceived[$record['delivery_id']][$record['month']]['charges_unbuyout'];
                    $records[$key]['charges_buyout_dollars'] += $tempModelsMoneyReceived[$record['delivery_id']][$record['month']]['charges_buyout_dollars'];
                    $records[$key]['charges_unbuyout_dollars'] += $tempModelsMoneyReceived[$record['delivery_id']][$record['month']]['charges_unbuyout_dollars'];
                }
            } else {
                if (isset($tempModelsMoneyReceived[$record['delivery_id']])) {
                    $records[$key]['sum_total'] += $tempModelsMoneyReceived[$record['delivery_id']]['sum_total_done'];
                    $records[$key]['sum_total_dollars'] += $tempModelsMoneyReceived[$record['delivery_id']]['sum_total_done_dollars'];
                    $records[$key]['charges_buyout'] += $tempModelsMoneyReceived[$record['delivery_id']]['charges_buyout'];
                    $records[$key]['charges_unbuyout'] += $tempModelsMoneyReceived[$record['delivery_id']]['charges_unbuyout'];
                    $records[$key]['charges_buyout_dollars'] += $tempModelsMoneyReceived[$record['delivery_id']]['charges_buyout_dollars'];
                    $records[$key]['charges_unbuyout_dollars'] += $tempModelsMoneyReceived[$record['delivery_id']]['charges_unbuyout_dollars'];
                }
            }

            $records[$key]['end_sum'] = $records[$key]['sum_total'] - $records[$key]['charges_buyout'] - $records[$key]['charges_in_process'] - $records[$key]['charges_unbuyout'];
            $records[$key]['end_sum_dollars'] = $records[$key]['sum_total_dollars'] - $records[$key]['charges_buyout_dollars'] - $records[$key]['charges_in_process_dollars'] - $records[$key]['charges_unbuyout_dollars'];
            /*if ($deliveries[$record['delivery_id']]->chargesCalculatorModel) {
                $tax = $deliveries[$record['delivery_id']]->chargesCalculator->calculateAdditionalChargesForCommonDebt($records[$key]['end_sum']);
                $records[$key]['charges_buyout'] += $tax;
                $records[$key]['end_sum'] -= $tax;
                $tax = $deliveries[$record['delivery_id']]->chargesCalculator->calculateAdditionalChargesForCommonDebt($records[$key]['end_sum_dollars']);
                $records[$key]['charges_buyout_dollars'] += $tax;
                $records[$key]['end_sum_dollars'] -= $tax;
            }*/

            if (!isset($countryTotal[$record['country_id']])) {
                $countryTotal[$record['country_id']] = ['total_country_common_end_sum_dollars' => 0, 'total_country_end_sum_dollars' => 0, 'total_country_charges_buyout_dollars' => 0, 'total_country_charges_unbuyout_dollars' => 0, 'total_country_in_process' => 0, 'total_country_unbuyout' => 0, 'total_country_buyout' => 0, 'total_country_vsego' => 0, 'total_country_money_received' => 0, 'total_country_start_month_time' => 0, 'total_country_end_month_time' => 0, 'total_country_charges_in_process_dollars' => 0, 'total_country_sum_total_dollars' => 0];
            }

            $countryTotal[$record['country_id']]['total_country_in_process'] += $record['in_process'];
            $countryTotal[$record['country_id']]['total_country_buyout'] += $record['buyout'];
            $countryTotal[$record['country_id']]['total_country_unbuyout'] += $record['unbuyout'];
            $countryTotal[$record['country_id']]['total_country_vsego'] += $record['vsego'];
            $countryTotal[$record['country_id']]['total_country_money_received'] += $record['money_received'];
            $countryTotal[$record['country_id']]['total_country_end_sum_dollars'] += $records[$key]['end_sum_dollars'];
            $countryTotal[$record['country_id']]['total_country_charges_buyout_dollars'] += $records[$key]['charges_buyout_dollars'];
            $countryTotal[$record['country_id']]['total_country_charges_unbuyout_dollars'] += $records[$key]['charges_unbuyout_dollars'];
            $countryTotal[$record['country_id']]['total_country_charges_in_process_dollars'] += $records[$key]['charges_in_process_dollars'];
            $countryTotal[$record['country_id']]['total_country_sum_total_dollars'] += $records[$key]['sum_total_dollars'];

            if (empty($countryTotal[$record['country_id']]['total_country_start_month_time']) || $countryTotal[$record['country_id']]['total_country_start_month_time'] > $record['start_month_time']) {
                $countryTotal[$record['country_id']]['total_country_start_month_time'] = $record['start_month_time'];
            }

            if (empty($countryTotal[$record['country_id']]['total_country_end_month_time']) || $countryTotal[$record['country_id']]['total_country_end_month_time'] < $record['start_month_time']) {
                $countryTotal[$record['country_id']]['total_country_end_month_time'] = $record['start_month_time'];
            }
        }

        foreach ($records as $key => $record) {
            $records[$key] = array_merge($countryTotal[$record['country_id']], $record);
        }

        return $records;
    }
}