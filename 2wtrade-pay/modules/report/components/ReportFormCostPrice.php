<?php
namespace app\modules\report\components;

use app\modules\order\models\Order;
use app\models\Country;
use app\modules\order\models\OrderProduct;
use app\modules\storage\models\StoragePart;
use app\models\Product;
use yii\data\ArrayDataProvider;
use app\modules\report\components\filters\CostPriceFilter;
use yii\db\Expression;

/**
 * Class ReportFormCostPrice
 * @package app\modules\report\components\
 */
class ReportFormCostPrice extends ReportForm
{
    /**
     * @var CostPriceFilter
     */
    protected $costPriceFilter;

    /**
     * @param array $params
     * @return ArrayDataProvider
     */
    public function apply($params)
    {
        $subQuery = StoragePart::find()
            ->joinWith([
                'storagePartHistory.order as order_in_part'
            ])
            ->select(['ROUND(AVG(' . StoragePart::tableName() . '.price_purchase))'])
            ->where([StoragePart::tableName() . '.product_id' => new Expression(Product::tableName() . '.id')])
            ->andWhere(['FROM_UNIXTIME(order_in_part.created_at, "%Y-%m")' => new Expression('FROM_UNIXTIME(' . Order::tableName() . '.created_at, "%Y-%m")')]);
        $this->applyFiltersSubQuery($params, $subQuery);

        $this->query = OrderProduct::find()
            ->joinWith([
                'product',
                'order',
                'order.country'
            ])
            ->select([
                'order_id' => 'concat(' . Country::tableName() . '.id, "-", FROM_UNIXTIME(' . Order::tableName() . '.created_at, "%Y-%m"), "-", ' . Product::tableName() . '.id)',
                'price_order' => 'ROUND(AVG(' . OrderProduct::tableName() . '.price))',
                'price_storage' => $subQuery,
                'product_name' => Product::tableName() . '.name',
                'product_id' => Product::tableName() . '.id',
                'create_date' => 'FROM_UNIXTIME(' . Order::tableName() . '.created_at, "%Y-%m")',
                'create_date_month' => 'FROM_UNIXTIME(' . Order::tableName() . '.created_at, "%m")',
                'create_date_year' => 'FROM_UNIXTIME(' . Order::tableName() . '.created_at, "%Y")',
                'country_id' => Country::tableName() . '.id',
                'country_name' => Country::tableName() . '.name',
            ])
            ->groupBy('country_id, create_date, product_id')
            ->orderBy('country_name, create_date desc, product_name');
        $this->applyFilters($params);
        $data = $this->query->asArray()->all();

        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
        ]);

        return $dataProvider;
    }

    /**
     * @return CostPriceFilter
     */
    public function getCostPriceFilter()
    {
        if (is_null($this->costPriceFilter)) {
            $this->costPriceFilter = new CostPriceFilter();
        }

        return $this->costPriceFilter;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return '';
    }

    /**
     * @param string $value
     * @return string
     */
    public static function monthName($value)
    {
        switch ($value) {
            case '01': return 'Январь';
            case '02': return 'Февраль';
            case '03': return 'Март';
            case '04': return 'Апрель';
            case '05': return 'Май';
            case '06': return 'Июнь';
            case '07': return 'Июль';
            case '08': return 'Август';
            case '09': return 'Сентябрь';
            case '10': return 'Октябрь';
            case '11': return 'Ноябрь';
            case '12': return 'Декабрь';
            default: return '';
        }
    }

    /**
     * @param array $params
     * @return $this
     */
    protected function applyFilters($params)
    {
        $this->getCostPriceFilter()->apply($this->query, $params);
        $this->getCountryFilter()->apply($this->query, $params);

        return $this;
    }

    /**
     * @param array $params
     * @param \yii\db\Query $subQuery
     */
    protected function applyFiltersSubQuery($params, $subQuery)
    {
        $this->getCostPriceFilter()->applySubQuery($subQuery, $params);
    }
}
