<?php

namespace app\modules\report\components;

use app\models\Country;
use app\models\Currency;
use app\models\CurrencyRate;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\deliveryreport\models\PaymentOrder;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use app\modules\report\components\filters\CountryDeliverySelectMultipleFilter;
use app\modules\report\components\filters\CountrySelectFilter;
use app\modules\report\components\filters\DateFilter;
use app\modules\report\components\filters\DebtsInvoiceMonthFilter;
use app\modules\report\components\filters\DebtsProcessMonthFilter;
use app\modules\report\components\filters\DeliveryFilter;
use app\modules\report\models\Invoice;
use app\modules\report\models\InvoiceCurrency;
use app\modules\report\models\InvoiceDebt;
use app\modules\report\models\InvoiceOrder;
use app\modules\report\models\InvoicePayment;
use Yii;
use yii\data\ArrayDataProvider;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * Class ReportFormDebtsInvoice
 * @package app\modules\report\components
 */
class ReportFormDebtsInvoice extends ReportForm
{
    /**
     * @var DebtsInvoiceMonthFilter
     */
    public $monthFilter = null;

    /**
     * @var DateFilter
     */
    public $processMonthFilter = null;

    /**
     * @var CountrySelectFilter
     */
    public $countrySelectFilter = null;

    /**
     * @param array $params
     * @return ArrayDataProvider
     */
    public function apply($params)
    {
        if (empty($params)) {
            $params[$this->getCountryDeliverySelectMultipleFilter()
                ->formName()]['country_ids'] = [Yii::$app->user->country->id];
        }

        $records = $this->loadRecordsFromTable($params);

        $dataProvider = new ArrayDataProvider([
            'allModels' => $records,
            'pagination' => false,
        ]);

        return $dataProvider;
    }

    /**
     * @param array $params
     * @return array
     */
    protected function loadRecordsFromTable($params)
    {

        $this->query = Order::find()
            ->select([
                'process' => 'COUNT(' . Order::tableName() . '.status_id IN(' . implode(',', OrderStatus::getProcessList()) . ') OR NULL)',
                'total' => 'COUNT(' . Order::tableName() . '.status_id IN(' . implode(',', OrderStatus::getAllList()) . ') OR NULL)',
                'delivery_id' => Delivery::tableName() . '.id',
                'month' => 'FROM_UNIXTIME(COALESCE(' . DeliveryRequest::tableName() . '.second_sent_at,'
                    . DeliveryRequest::tableName() . '.sent_at,'
                    . DeliveryRequest::tableName() . '.created_at), "%Y-%m-01")'
            ])
            ->joinWith(['deliveryRequest'], false)
            ->joinWith(['deliveryRequest.delivery'], false)
            ->joinWith(['country'], false)
            ->groupBy([
                Country::tableName() . '.id',
                Delivery::tableName() . '.id',
                'month'
            ]);
        $this->getProcessMonthFilter()->apply($this->query, $params);
        $this->getCountryDeliverySelectMultipleFilter()->apply($this->query, $params);

        $process = $this->query->asArray()->all();
        $process = ArrayHelper::index($process, 'month', 'delivery_id');

        $subQuery = InvoiceOrder::find()
            ->joinWith('order')
            ->select([
                'buyout' => 'COUNT(' . Order::tableName() . '.status_id IN(' . implode(',', OrderStatus::getBuyoutList()) . ') OR NULL)',
                'invoice_debt_id' => InvoiceOrder::tableName() . '.invoice_debt_id'
            ])
            ->groupBy(
                InvoiceOrder::tableName() . '.invoice_debt_id'
            );


        $this->query = InvoiceDebt::find()
            ->select([
                'country_name' => Country::tableName() . '.name',
                'delivery_id' => Delivery::tableName() . '.id',
                'delivery_name' => Delivery::tableName() . '.name',
                'debt_month' => InvoiceDebt::tableName() . '.debt_month',
                'period' => 'GROUP_CONCAT(' . InvoiceDebt::tableName() . '.period_from, "-", ' . InvoiceDebt::tableName() . '.period_to)',
                'buyout_count' => 'COALESCE(SUM(orders.buyout), 0)',
                'buyout_local' => 'SUM(' . InvoiceDebt::tableName() . '.buyout_local)',
                'buyout_usd' => 'SUM(' . InvoiceDebt::tableName() . '.buyout_usd)',
                'service_local' => 'SUM(' . InvoiceDebt::tableName() . '.service_local)',
                'service_usd' => 'SUM(' . InvoiceDebt::tableName() . '.service_usd)',
                'total_local' => 'SUM(' . InvoiceDebt::tableName() . '.total_local)',
                'total_usd' => 'SUM(' . InvoiceDebt::tableName() . '.total_usd)',
                'currency' => Currency::tableName() . '.name',
                'rate' => CurrencyRate::tableName() . '.rate',
                'invoice_ids' => 'GROUP_CONCAT(' . InvoiceDebt::tableName() . '.invoice_id)',
            ])
            ->joinWith(['invoice'], false)
            ->joinWith(['invoice.delivery'], false)
            ->joinWith(['invoice.delivery.country'], false)
            ->joinWith(['invoice.delivery.country.currency'], false)
            ->joinWith(['invoice.delivery.country.currencyRate'], false)
            ->leftJoin(['orders' => $subQuery], 'orders.invoice_debt_id = ' . InvoiceDebt::tableName() . '.id')
            ->orderBy([
                Country::tableName() . '.id' => SORT_ASC,
                Delivery::tableName() . '.id' => SORT_ASC,
                InvoiceDebt::tableName() . '.debt_month' => SORT_ASC
            ])
            ->groupBy([
                Country::tableName() . '.id',
                Delivery::tableName() . '.id',
                InvoiceDebt::tableName() . '.debt_month'
            ]);
        $this->applyFilters($params);

//        echo $this->query->createCommand()->rawSql; die();

        $data = $this->query->asArray()->all();


        $invoiceIds = [];
        foreach ($data as &$row) {
            $row['process'] = $process[$row['delivery_id']][$row['debt_month']]['process'] ?? 0;
            $row['total'] = $process[$row['delivery_id']][$row['debt_month']]['total'] ?? 0;
            $row['process_percent'] = $row['total'] ? round($row['process'] * 100 / $row['total']) : 0;
            $row['invoice_ids'] = explode(',', $row['invoice_ids']);
            $invoiceIds = array_unique(array_merge($invoiceIds, $row['invoice_ids']));
            $row['invoice_ids'] = array_combine($row['invoice_ids'], $row['invoice_ids']);
            $row['payment_usd'] = 0;
            $row['payment_local'] = 0;
            $row['debt_usd'] = $row['total_usd'];
            $row['debt_local'] = 0;
        }

        if ($invoiceIds) {
            $queryPayments = InvoicePayment::find()
                ->joinWith([
                    'payment',
                    'invoice AS invoice_sub',
                    'invoice.delivery.country'
                ], false)
                ->groupBy([InvoicePayment::tableName() . '.invoice_id']);
            $queryPayments->where([InvoicePayment::tableName() . '.invoice_id' => $invoiceIds]);
            // текущий курс валюты
            $queryPayments->leftJoin(CurrencyRate::tableName() . ' payment_currency_rate_default', 'payment_currency_rate_default.currency_id = ' . PaymentOrder::tableName() . '.currency_id');
            // курс в инвойсе
            $queryPayments->leftJoin(InvoiceCurrency::tableName() . ' payment_invoice_currency', 'payment_invoice_currency.invoice_id = ' . InvoicePayment::tableName() . '.invoice_id and payment_invoice_currency.currency_id = ' . PaymentOrder::tableName() . '.currency_id');
            $queryPayments->leftJoin(CurrencyRate::tableName(), CurrencyRate::tableName() . '.currency_id = ' . Country::tableName() . '.currency_id');
            $queryPayments->select([
                'payment_usd' => 'SUM(IFNULL(' . PaymentOrder::tableName() . '.sum, 0) / COALESCE(payment_invoice_currency.rate, payment_currency_rate_default.rate, ' . CurrencyRate::tableName() . '.rate))',
                'invoice_id' => InvoicePayment::tableName() . '.invoice_id'
            ]);
            $dataPayments = ArrayHelper::index($queryPayments->asArray()->all(), 'invoice_id');


            foreach ($data as &$row) {
                if ($dataPayments) {
                    // есть платежи по инвойсам
                    foreach ($row['invoice_ids'] as $invoiceId) {
                        // может быть несколько инвойсов
                        if (isset($dataPayments[$invoiceId])) {
                            // есть платеж по этому инвойсу
                            if ($row['total_usd'] > 0) {
                                // если нам должны

                                if ($dataPayments[$invoiceId]['payment_usd'] > $row['total_usd']) {
                                    // заплатили больше чем должны за месяц
                                    $row['payment_usd'] = $row['total_usd'];
                                } else {
                                    $row['payment_usd'] = $dataPayments[$invoiceId]['payment_usd'];
                                }

                                // уменьшаем неучтенные платежи для след месяца
                                $dataPayments[$invoiceId]['payment_usd'] -= $row['payment_usd'];
                            }
                        }
                    }
                }
                $row['payment_local'] = $row['payment_usd'] * $row['rate'];
                $row['debt_usd'] = $row['total_usd'] - $row['payment_usd'];
                $row['debt_local'] = $row['debt_usd'] * $row['rate'];
            }
        }


        return $data;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return '';
    }

    /**
     * @param array $params
     * @return $this
     */
    public function applyFilters($params)
    {
        $this->getMonthFilter()->apply($this->query, $params);
        $this->getCountryDeliverySelectMultipleFilter()->apply($this->query, $params);
        return $this;
    }

    /**
     * @return DebtsInvoiceMonthFilter
     */
    public function getMonthFilter()
    {
        if (is_null($this->monthFilter)) {
            $this->monthFilter = new DebtsInvoiceMonthFilter([
                'from' => Yii::$app->formatter->asMonth(strtotime(' - 1month', time())),
                'to' => Yii::$app->formatter->asMonth(time())
            ]);
        }
        return $this->monthFilter;
    }

    /**
     * @return DateFilter
     */
    public function getProcessMonthFilter()
    {
        if (is_null($this->processMonthFilter)) {
            $this->processMonthFilter = new DebtsProcessMonthFilter([
                'from' => Yii::$app->formatter->asMonth(strtotime(' - 1month', time())),
                'to' => Yii::$app->formatter->asMonth(time())
            ]);
        }
        return $this->processMonthFilter;
    }

    /**
     * @return CountryDeliverySelectMultipleFilter
     */
    public function getCountryDeliverySelectMultipleFilter()
    {
        if (is_null($this->countryDeliverySelectMultipleFilter)) {
            $this->countryDeliverySelectMultipleFilter = new CountryDeliverySelectMultipleFilter([
                'deliveryType' => CountryDeliverySelectMultipleFilter::DELIVERY_TYPE_DELIVERY,
            ]);
        }

        return $this->countryDeliverySelectMultipleFilter;
    }

    /**
     * @return DeliveryFilter
     */
    public function getDeliveryFilter()
    {
        if (is_null($this->deliveryFilter)) {
            $this->deliveryFilter = new DeliveryFilter([
                'type' => DeliveryFilter::TYPE_DELIVERY,
                'reportForm' => $this
            ]);
        }
        return $this->deliveryFilter;
    }
}
