<?php
namespace app\modules\report\components;

use app\helpers\Utils;
use app\modules\report\models\ReportValidateFinanceData;
use app\widgets\LinkPager;
use Yii;
use yii\data\ArrayDataProvider;
use yii\web\UploadedFile;
use app\modules\report\components\filters\ReportValidateFinanceFilter;
use PHPExcel_IOFactory;
use app\modules\deliveryreport\components\scanner\ChunkReadFilter;
use app\modules\report\models\ReportValidateFinance;
use app\modules\order\models\Order;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\delivery\models\Delivery;
use app\helpers\i18n\Formatter;

/**
 * Class ReportFormValidateFinance
 * @package app\modules\report\components
 */
class ReportFormValidateFinance extends ReportForm
{
    use ModelTrait;
    const PART_LOAD_STR = 1000;

    /**
     * @var UploadedFile
     */
    public $loadFile;

    /**
     * @var ReportValidateFinance
     */
    public $model;

    /**
     * @var ReportValidateFinanceFilter
     */
    protected $dateFilter;

    /**
     * @var string
     */
    private $fileName;

    /**
     * @var integer
     */
    private $fileId;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->model = new ReportValidateFinance();
        $this->model->row_begin = 1;
        $this->model->cell_order_id = 1;
        $this->model->cell_tracking = 2;
        $this->model->cell_delivery_cost_percent = 3;
        $this->model->cell_fulfillment_cost = 4;
        $this->model->cell_nds = 5;
        $this->model->cell_fulfillment_nds = 6;
        $this->model->cell_delivery_price = 7;
        $this->model->cell_total_price = 8;
        $this->model->cell_status = 9;
        $this->fileName = null;
        $this->fileId = null;
    }

    /**
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * @return string
     */
    public function getFileId()
    {
        return $this->fileId;
    }

    public function getInputFields()
    {
        return [
            'cell_order_id',
            'cell_tracking',
            'cell_delivery_cost_percent',
            'cell_fulfillment_cost',
            'cell_nds',
            'cell_fulfillment_nds',
            'cell_delivery_price',
            'cell_total_price',
            'cell_status',
        ];
    }

    /**
     * @param string $formDataStr
     */
    public function setFormData($formDataStr)
    {
        $formDataArray = null;
        $params = explode('&', urldecode($formDataStr));
        foreach ($params as $param) {
            $datePair = explode('=', $param);
            $formDataArray[$datePair[0]] = $datePair[1];
        }
        $this->model->row_begin = $formDataArray['ReportValidateFinance[row_begin]'];


        foreach ($this->getInputFields() as $field) {

            if (isset($formDataArray['ReportValidateFinance[' . $field . ']']) &&
                $formDataArray['ReportValidateFinance[' . $field . ']']
            ) {
                $this->model->$field = (int)$formDataArray['ReportValidateFinance[' . $field . ']'];
            } else {
                $this->model->$field = null;
            }
        }
    }

    /**
     * @param integer $fileId
     * @param integer $currentstr
     * @return array
     */
    public function parse($fileId, $currentStr)
    {
        $this->model = ReportValidateFinance::findOne($fileId);

        if (!$this->model->id) {
            return [
                'status' => 'fail',
                'message' => Yii::t('common', 'Файл не выбран для разбора'),
            ];
        }

        try {

            $rowIdx = 0;
            $loadStr = $currentStr;
            if ($currentStr == 1) { /* первую строку корректируем на строку начала данных */
                $loadStr = $this->model->row_begin;
            }
            /* загрузим из файла только партию строк self::PART_LOAD_STR для считывания */
            $objReader = PHPExcel_IOFactory::createReaderForFile($this->model->file_name);
            $chunkFilter = new ChunkReadFilter();
            $objReader->setReadFilter($chunkFilter);
            $chunkFilter->setRows($loadStr, self::PART_LOAD_STR);
            $objReader->setReadDataOnly(true);

            $objPHPExcel = $objReader->load($this->model->file_name);
            /* считываем только с 1-го листа */
            $objPHPExcel->setActiveSheetIndex(0);
            $aSheet = $objPHPExcel->getActiveSheet();

            $arrayForInsert = null;
            $arrayOrderId = null;
            $arrayTracking = null;
            /* читаем строки из файла в $arrayForInsert */
            foreach ($aSheet->getRowIterator($loadStr) as $worksheetRow) {
                $rowIdx = $worksheetRow->getRowIndex();
                $error = null;
                $warning = null;

                $row = [];
                foreach ($this->getInputFields() as $field) {
                    if (!is_null($this->model->$field)) {
                        $colIdx = $this->model->$field - 1;
                        $cell = $aSheet->getCellByColumnAndRow($colIdx, $rowIdx);
                        if ($cell instanceof \PHPExcel_Cell) {
                            $value = $cell->getValue();
                            $row[$field] = $value;
                        }
                    }
                }
                $arrayForInsert[] = [
                    'order_id_file' => $row['cell_order_id'] ?? '',
                    'tracking_file' => $row['cell_tracking'] ?? '',
                    'delivery_cost_percent_file' => $row['cell_delivery_cost_percent'] ?? '',
                    'fulfillment_cost_file' => $row['cell_fulfillment_cost'] ?? '',
                    'nds_file' => $row['cell_nds'] ?? '',
                    'fulfillment_nds_file' => $row['cell_fulfillment_nds'] ?? '',
                    'file_id' => $this->model->id,
                    'error' => $error,
                    'warning' => $warning,
                    'order_id' => '',
                    'tracking' => '',
                    'delivery_cost_percent' => '',
                    'fulfillment_cost' => '',
                    'nds' => '',
                    'fulfillment_nds' => '',
                    'delivery_price_file' => $row['cell_delivery_price'] ?? '',
                    'total_price_file' => $row['cell_total_price'] ?? '',
                    'status_file' => $row['cell_status'] ?? '',
                    'delivery_price' => '',
                    'total_price' => '',
                    'status' => '',
                    'created_at' => time(),
                    'updated_at' => time(),
                ];
                if (isset($row['cell_order_id'])) {
                    $arrayOrderId[] = $row['cell_order_id'];
                }
                if (isset($row['cell_tracking'])) {
                    $arrayTracking[] = $row['cell_tracking'];
                }
                $loadStr++;
            }


            unset($objPHPExcel);
            unset($objReader);

            /* читаем данные по номеру заказу или треку из БД для заказов из $arrayForInsert */

            $query = Order::find()
                ->joinWith(['deliveryRequest', 'deliveryRequest.delivery'])
                ->select([
                    'id' => Order::tableName() . '.id',
                    'order_id' => Order::tableName() . '.id',
                    'tracking' => DeliveryRequest::tableName() . '.tracking',
                    'price_delivery' => Order::tableName() . '.delivery',
                    'price_total' => Order::tableName() . '.price_total',
                    'status_id' => Order::tableName() . '.status_id',
                ]);

            $doQuery = true;
            if (!$arrayOrderId && !$arrayTracking) {
                $doQuery = false;
            }

            if ($arrayOrderId && $arrayTracking) {
                $query->where(['or',
                    ['in', Order::tableName() . '.id', $arrayOrderId],
                    ['in', DeliveryRequest::tableName() . '.tracking', $arrayTracking]
                ]);
            } else {
                if ($arrayOrderId) {
                    $query->where(['in', Order::tableName() . '.id', $arrayOrderId]);
                }
                if ($arrayTracking) {
                    $query->where(['in', DeliveryRequest::tableName() . '.tracking', $arrayTracking]);
                }
            }

            if ($doQuery) {
                $query->groupBy([Order::tableName() . '.id']);
                $dataDB = $query->asArray()->all();
            }

            unset($arrayOrderId);
            unset($arrayTracking);

            /* данные из БД положим в ассоциативные массивы для быстрого доступа */
            $dataDBOrderId = null;
            $dataDBOrderTracking = null;

            if ($doQuery && isset($dataDB)) {
                foreach ($dataDB as $dataDBItem) {
                    $dataDBOrderId[$dataDBItem['order_id']] = $dataDBItem;
                    $dataDBOrderTracking[$dataDBItem['tracking']] = $dataDBItem;
                }
            }
            unset($dataDB);


            $statusMap = [];
            if ($this->model->status_map) {
                $statusMap = json_decode($this->model->status_map, true);
            }

            for ($i = 0; $i < count($arrayForInsert); $i++) {
                if ($arrayForInsert[$i]['error'] == '') {

                    $index = null;
                    $dataDBSource = null;

                    if (isset($dataDBOrderId[$arrayForInsert[$i]['order_id_file']])) { /* нашли по номеру заказа */
                        $index = $arrayForInsert[$i]['order_id_file'];
                        $dataDBSource = $dataDBOrderId[$index];

                    } elseif (isset($dataDBOrderTracking[$arrayForInsert[$i]['tracking_file']])) { /* нашли по треку */;
                        $index = $arrayForInsert[$i]['tracking_file'];
                        $dataDBSource = $dataDBOrderTracking[$index];
                    }

                    if ($index && $dataDBSource) {

                        $arrayForInsert[$i]['order_id'] = $dataDBSource['order_id'];
                        $arrayForInsert[$i]['tracking'] = $dataDBSource['tracking'];

                        $arrayForInsert[$i]['delivery_price'] = $dataDBSource['price_delivery'];
                        $arrayForInsert[$i]['total_price'] = $dataDBSource['price_total'];
                        $arrayForInsert[$i]['status'] = $dataDBSource['status_id'];

                        if ($this->model->cell_delivery_price) {
                            if ((float)$dataDBSource['price_delivery'] != (float)$arrayForInsert[$i]['delivery_price_file']) {
                                $arrayForInsert[$i]['error'] .= ' ' . Yii::t('common', 'Не совпадает стоимость доставки');
                            }
                        }

                        if ($this->model->cell_total_price) {
                            if ((float)$dataDBSource['price_total'] == (float)$arrayForInsert[$i]['total_price_file']) {
                                // если суммы равны, то ок
                            }
                            if (
                                (((float)$dataDBSource['price_total'] + (float)$dataDBSource['price_delivery']) == (float)$arrayForInsert[$i]['total_price_file']) ||
                                (((float)$dataDBSource['price_total'] - (float)$dataDBSource['price_delivery']) == (float)$arrayForInsert[$i]['total_price_file']) ||
                                ((float)$dataDBSource['price_total'] == ((float)$arrayForInsert[$i]['total_price_file'] + (float)$arrayForInsert[$i]['delivery_price_file'])) ||
                                ((float)$dataDBSource['price_total'] == ((float)$arrayForInsert[$i]['total_price_file'] - (float)$arrayForInsert[$i]['delivery_price_file']))
                            ) {
                                // если суммы различаются ровно на стоимость доставки, то warning
                                $arrayForInsert[$i]['warning'] .= ' ' . Yii::t('common', 'Общая сумма различается на стоимость доставки');
                            } else {
                                // если суммы различаются как то иначе, то error
                                $arrayForInsert[$i]['error'] .= ' ' . Yii::t('common', 'Не совпадает общая сумма');
                            }
                        }
                    } else {
                        $arrayForInsert[$i]['error'] .= ' ' . Yii::t('common', 'Не нашли в базе');
                    }
                }
                if ($this->model->cell_status) {
                    $statusMap['fileStatus'][$arrayForInsert[$i]['status_file']] = $arrayForInsert[$i]['status_file'];
                }
            }

            unset($dataDBOrderId);
            unset($dataDBOrderTracking);

            /* записываем в БД результат разбора отчета */
            Yii::$app->db->createCommand()->batchInsert(
                ReportValidateFinanceData::tableName(),
                [
                    'order_id_file',
                    'tracking_file',
                    'delivery_cost_percent_file',
                    'fulfillment_cost_file',
                    'nds_file',
                    'fulfillment_nds_file',
                    'file_id',
                    'error',
                    'warning',
                    'order_id',
                    'tracking',
                    'delivery_cost_percent',
                    'fulfillment_cost',
                    'nds',
                    'fulfillment_nds',
                    'delivery_price_file',
                    'total_price_file',
                    'status_file',
                    'delivery_price',
                    'total_price',
                    'status',
                    'created_at',
                    'updated_at'
                ],
                $arrayForInsert
            )->execute();

            unset($arrayForInsert);

            $this->model->status_map = json_encode($statusMap);
            $this->model->save(false, ['status_map']);

            if ($loadStr < ($currentStr + self::PART_LOAD_STR)) {
                $response['status'] = 'success';
            } else {
                $response = [
                    'status' => 'progress',
                    'currentStr' => $loadStr,
                    'rows' => $currentStr
                ];
            }
        } catch (\Exception $e) {
            $response = [
                'status' => 'fail',
                'message' => '(' . Yii::t('common', 'Строка') . ': ' . $rowIdx . ')' . $e->getMessage(),
            ];
        }
        return $response;
    }

    /**
     * @param string $file
     * @return array
     */
    public function getHistoryOne($file)
    {
        $itemsArray = ReportValidateFinanceData::find()
            ->select([
                'id' => 'id',
                'order_id' => 'order_id',
                'order_id_file' => 'order_id_file',
                'tracking' => 'tracking',
                'tracking_file' => 'tracking_file',
                'delivery_cost_percent' => 'delivery_cost_percent',
                'delivery_cost_percent_file' => 'delivery_cost_percent_file',
                'fulfillment_cost' => 'fulfillment_cost',
                'fulfillment_cost_file' => 'fulfillment_cost_file',
                'nds' => 'nds',
                'nds_file' => 'nds_file',
                'fulfillment_nds' => 'fulfillment_nds',
                'fulfillment_nds_file' => 'fulfillment_nds_file',
                'error' => 'error',
                'warning' => 'warning',
                'delivery_price_file' => 'delivery_price_file',
                'total_price_file' => 'total_price_file',
                'status_file' => 'status_file',
                'delivery_price' => 'delivery_price',
                'total_price' => 'total_price',
                'status' => 'status',
            ])
            ->where(['file_id' => $file])
            ->orderBy('created_at')
            ->asArray()->all();

        $sumsArray = [
            'sum_delivery_cost_percent' => 0,
            'sum_delivery_cost_percent_file' => 0,
            'sum_fulfillment_cost' => 0,
            'sum_fulfillment_cost_file' => 0,
            'sum_nds' => 0,
            'sum_nds_file' => 0,
            'sum_fulfillment_nds' => 0,
            'sum_fulfillment_nds_file' => 0,
            'sum_delivery_price' => 0,
            'sum_delivery_price_file' => 0,
            'sum_total_price' => 0,
            'sum_total_price_file' => 0,
            'count_errors' => 0,
            'count_warnings' => 0,
        ];

        foreach ($itemsArray as $item) {
            $sumsArray['sum_delivery_cost_percent'] += $item['delivery_cost_percent'];
            $sumsArray['sum_delivery_cost_percent_file'] += $item['delivery_cost_percent_file'];
            $sumsArray['sum_fulfillment_cost'] += $item['fulfillment_cost'];
            $sumsArray['sum_fulfillment_cost_file'] += $item['fulfillment_cost_file'];
            $sumsArray['sum_nds'] += $item['nds'];
            $sumsArray['sum_nds_file'] += $item['nds_file'];
            $sumsArray['sum_fulfillment_nds'] += $item['fulfillment_nds'];
            $sumsArray['sum_fulfillment_nds_file'] += $item['fulfillment_nds_file'];
            $sumsArray['sum_delivery_price'] += $item['delivery_price'];
            $sumsArray['sum_delivery_price_file'] += $item['delivery_price_file'];
            $sumsArray['sum_total_price'] += $item['total_price'];
            $sumsArray['sum_total_price_file'] += $item['total_price_file'];
            if ($item['error'] != '') {
                $sumsArray['count_errors']++;
            }
            if ($item['warning'] != '') {
                $sumsArray['count_warnings']++;
            }
        }

        $dataHistory = new ArrayDataProvider([
            'allModels' => $itemsArray,
            'pagination' => [
                'pageSize' => LinkPager::getPageSize()
            ],
        ]);

        return [
            'dataHistory' => $dataHistory,
            'sumsArray' => $sumsArray,
        ];
    }

    /**
     * @param array $row
     * @return array
     */
    public static function check($row)
    {
        if ($row['error'] != '') {
            return [
                'class' => 'error'
            ];
        }
        if ($row['warning'] != '') {
            return [
                'class' => 'warning'
            ];
        }
        return [];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['loadFile'], 'file', 'checkExtensionByMimeType' => false, 'skipOnEmpty' => false, 'extensions' => 'xls, xlsx', 'message' => 'retyryt'],
        ];
    }

    /**
     * @param boolean $prepareDir
     * @return string
     */
    public function getUploadPath($prepareDir = false)
    {
        $dir = Yii::getAlias('@uploads') . DIRECTORY_SEPARATOR . 'validate-finance';

        if ($prepareDir) {
            Utils::prepareDir($dir);
        }

        return $dir;
    }

    /**
     * @return string|boolean
     */
    public function upload()
    {
        $this->loadFile = UploadedFile::getInstance($this, 'loadFile');
        $this->setFormData(Yii::$app->request->post('formData'));

        if ($this->validate()) {
            $this->fileName = $this->getUploadPath(true) . DIRECTORY_SEPARATOR . time() . '.' . $this->loadFile->extension;
            if ($this->loadFile->saveAs($this->fileName)) {
                $this->model->file_name = $this->fileName;
                if ($this->model->save()) {
                    $this->fileId = $this->model->id;
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @param array $params
     * @return ArrayDataProvider
     * @throws \Exception
     */
    public function apply($params)
    {
        $this->query = ReportValidateFinance::find()
            ->select([
                'id' => 'id',
                'file_name' => 'file_name',
                'created' => 'FROM_UNIXTIME(created_at, "' . Formatter::getMysqlDateFormat() . ' %H:%i")',
            ]);
        $this->applyFilters($params);
        $this->query->orderBy('id desc');

        $historyArray = $this->query->asArray()->all();

        for ($i = 0; $i < count($historyArray); $i++) {
            $historyArray[$i]['file_name_trim'] = substr($historyArray[$i]['file_name'], strrpos($historyArray[$i]['file_name'], '/') + 1);
        }

        $dataHistory = new ArrayDataProvider([
            'allModels' => $historyArray,
        ]);

        return $dataHistory;
    }

    /**
     * @return ReportValidateFinanceFilter
     */
    public function getDateFilter()
    {
        if (is_null($this->dateFilter)) {
            $this->dateFilter = new ReportValidateFinanceFilter();
        }

        return $this->dateFilter;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $output = '';

        $output .= $this->getDateFilter()->restore($form);

        return $output;
    }

    /**
     * @param array $params
     * @return ReportFormValidateFinance
     */
    protected function applyFilters($params)
    {
        $this->getDateFilter()->apply($this->query, $params);

        return $this;
    }
}