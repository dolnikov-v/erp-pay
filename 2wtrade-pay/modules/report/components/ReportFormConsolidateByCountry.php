<?php
namespace app\modules\report\components;

use app\models\Country;
use app\models\CurrencyRate;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use app\modules\report\extensions\DataProvider;
use app\modules\report\extensions\Query;
use Yii;

/**
 * Class ReportFormConsolidateByCountry
 * @package app\modules\report\components
 */
class ReportFormConsolidateByCountry extends ReportForm
{

    /**
     * @var string Группировка по умолчанию
     */
    public $groupBy = self::GROUP_BY_YEAR_MONTH;

    /**
     * @var array Маппинг статусов
     */
    protected $mapStatuses = [];

    /*
     * "Галочка" конвертировать в доллар всегда отмечена
     * @var string
     */
    public $dollar = 'on';

    /**
     * @param array $params
     * @return DataProvider
     */
    public function apply($params)
    {
        $this->load($params);

        $this->query = new Query();
        $this->query->from([Order::tableName()]);

        $this->prepareCurrencyRate();
        $params['s']['dollar'] = $this->dollar;
        $this->applyFilters($params);
        $this->buildSelect();

        $dataProvider = new DataProvider([
            'form' => $this,
            'query' => $this->query,
        ]);

        return $dataProvider;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $output = '';

        return $output;
    }

    /**
     * @return array
     */
    public function getMapStatuses()
    {
        if (empty($this->mapStatuses)) {
            $this->mapStatuses = [
                Yii::t('common', 'Подтверждено') => OrderStatus::getApproveList(),
                Yii::t('common', 'Выкуплено') => OrderStatus::getBuyoutList(),
                Yii::t('common', 'Выкуп') => [
                    OrderStatus::STATUS_DELIVERY_BUYOUT
                ]
            ];

            $this->mapStatuses[Yii::t('common', 'Всего')] = $this->getMapStatusesAsArray();
        }

        return $this->mapStatuses;
    }

    /**
     * @inheritdoc
     */
    protected function buildSelect()
    {
        $selectDate = $this->getDateFilter()->getFilteredAttribute();

        $this->query->select([
            'year_month' => 'DATE_FORMAT(FROM_UNIXTIME(' . $selectDate . '), "%Y-%m")',
            /*
            'expense_delivery' =>
                'SUM(IFNULL(IFNULL(' . OrderFinance::tableName() . '.price_delivery,' . Order::tableName() . '.delivery),0)+
                    IFNULL(' . OrderFinance::tableName() . '.price_storage,0)+
                    IFNULL(' . OrderFinance::tableName() . '.price_packing,0)+
                    IFNULL(' . OrderFinance::tableName() . '.price_package,0)+
                    IFNULL(' . OrderFinance::tableName() . '.price_address_correction,0)+
                    IFNULL(' . OrderFinance::tableName() . '.price_redelivery,0)+
                    IFNULL(' . OrderFinance::tableName() . '.price_delivery_back,0)+
                    IFNULL(' . OrderFinance::tableName() . '.price_delivery_return,0))'
            */
        ]);

        //Пока в OrderFinance::tableName() нет данных в колонках price_*, пусть считается сумма по Order::tableName() . '.delivery '
        //->leftJoin(OrderFinance::tableName(), Order::tableName() . '.id = ' . OrderFinance::tableName() . '.order_id');

        $mapStatuses = $this->getMapStatuses();

        $toDollar = '';
        if ($this->dollar || $this->dollar == 'on') {
            $toDollar = '/ ' . CurrencyRate::tableName() . '.rate';
            $this->query->leftJoin(Country::tableName(), Order::tableName() . '.country_id=' . Country::tableName() . '.id');
            $this->query->leftJoin(CurrencyRate::tableName(), Country::tableName() . '.currency_id = ' . CurrencyRate::tableName() . '.currency_id');
        }

        $buyoutStatuses = $mapStatuses[Yii::t('common', 'Выкуплено')];
        $buyoutCase = $this->query->buildCaseCondition(
            '(' . Order::tableName() . '.price_total ' . $toDollar . ' + ' . Order::tableName() . '.delivery ' . $toDollar . ' )',
            Order::tableName() . '.status_id',
            $buyoutStatuses
        );
        $this->query->addSumCondition($buyoutCase, Yii::t('common', 'Доход'));

        $debitStatuses = $mapStatuses[Yii::t('common', 'Выкуп')];
        $debitCase = $this->query->buildCaseCondition(
            '(' . Order::tableName() . '.price_total ' . $toDollar . ' + ' . Order::tableName() . '.delivery ' . $toDollar . ' )',
            Order::tableName() . '.status_id',
            $debitStatuses
        );
        $this->query->addSumCondition($debitCase, Yii::t('common', 'Дебиторка'));
        $this->query->addSumCondition(Order::tableName() . '.income ' . $toDollar, Yii::t('common', 'Расходы на рекламу'));
        $this->query->addSumCondition(Order::tableName() . '.delivery ' . $toDollar, Yii::t('common', 'Расходы на доставку'));
        $this->buildSelectMapStatuses();

        return $this;
    }

    /**
     * @param array $params
     * @return ReportForm
     */
    protected function applyFilters($params)
    {
        $this->getDateFilter()->apply($this->query, $params);
        $this->getDeliveryFilter()->apply($this->query, $params);
        $this->applyBaseFilters($params);
        return $this;
    }

    /**
     * @param string $value
     * @return string
     */
    public static function monthName($value)
    {
        switch ($value) {
            case '01': return 'Январь';
            case '02': return 'Февраль';
            case '03': return 'Март';
            case '04': return 'Апрель';
            case '05': return 'Май';
            case '06': return 'Июнь';
            case '07': return 'Июль';
            case '08': return 'Август';
            case '09': return 'Сентябрь';
            case '10': return 'Октябрь';
            case '11': return 'Ноябрь';
            case '12': return 'Декабрь';
            default: return '';
        }
    }
}
