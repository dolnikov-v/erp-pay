<?php
namespace app\modules\report\components;

use app\modules\callcenter\models\CallCenterUser;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\catalog\models\OperatorsPenalty;
use app\modules\report\extensions\Query;
use yii\data\ArrayDataProvider;
use app\modules\report\components\filters\DailyFilterDefaultLastMonth;
use Yii;


/**
 * Class ReportFormOperatorsPenalty
 * @package app\modules\report\components
 */
class ReportFormOperatorsPenalty extends ReportForm
{
    /**
     * @var string Группировка по умолчанию
     */
    public $groupBy = self::GROUP_BY_LAST_FOREIGN_OPERATOR;

    /*
     * "Галочка" конвертировать в доллар всегда отмечена
     * @var string
     */
    public $dollar = 'on';

    /**
     * @var DailyFilterDefaultLastMonth
     */
    protected $dailyFilter;


    public function init()
    {
        parent::init();
        $this->groupByCollection[self::GROUP_BY_LAST_FOREIGN_OPERATOR] = Yii::t('common', 'Внешний оператор');
    }

    /**
     * @param array $params
     * @return ArrayDataProvider
     */
    public function apply($params)
    {
        $this->load($params);
        $this->prepareCurrencyRate();

        $this->query = new Query();
        $this->query->from([CallCenterRequest::tableName()])
            ->innerJoin(DeliveryRequest::tableName(), DeliveryRequest::tableName() .'.order_id=' .CallCenterRequest::tableName() .'.order_id')
            ->innerJoin(OperatorsPenalty::tableName(), OperatorsPenalty::tableName() .'.call_center_id=' .CallCenterRequest::tableName() .'.call_center_id')
            ->innerJoin(CallCenterUser::tableName(), CallCenterUser::tableName() .'.callcenter_id=' .CallCenterRequest::tableName() .'.call_center_id');
        $this->buildSelect()
            ->applyFilters($params);

        $this->query->where(['NOT', [CallCenterRequest::tableName() . '.last_foreign_operator' => null]])
            ->andWhere(['NOT', [CallCenterRequest::tableName() . '.last_foreign_operator' => [0, -1]]])
            ->andWhere(['LIKE', DeliveryRequest::tableName() . '.unshipping_reason', OperatorsPenalty::INVALID_ADDRESS]);

        $this->query->groupBy(['callCenterId', 'lastForeignOperator']);
        $allModels = $this->query->all();

        $dataProvider = new ArrayDataProvider([
            'allModels' => $allModels,
            'pagination' => false,
        ]);

        return $dataProvider;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $output = '';

        return $output;
    }

    /**
     * @inheritdoc
     */
    protected function buildSelect()
    {
        $this->query->select([
            'callCenterId' => CallCenterRequest::tableName() .'.call_center_id',
            'lastForeignOperator' => CallCenterRequest::tableName() . '.last_foreign_operator',
            'operatorSupervisor' => CallCenterUser::tableName() .'.parent_id',
            'operatorLogin' => CallCenterUser::tableName() .'.user_login',
            'penalty' => 'sum(' .OperatorsPenalty::tableName() .'.penalty_value' .')',
        ]);

        return $this;
    }

    /**
     * @param array $params
     * @return ReportForm
     */
    protected function applyFilters($params)
    {
        $this->getDailyFilter()->apply($this->query, $params);
        $this->getCallCenterFilter()->apply($this->query, $params);
//        $this->getForeignOperatorFilter()->apply($this->query, $params);
        $this->applyBaseFilters($params);

        return $this;
    }

    /**
     * @return DailyFilterDefaultLastMonth
     */
    public function getDailyFilter()
    {
        if (is_null($this->dailyFilter)) {
            $this->dailyFilter = new DailyFilterDefaultLastMonth();
            $this->dailyFilter->type = DailyFilterDefaultLastMonth::TYPE_CALL_CENTER_APPROVED_AT;
        }

        return $this->dailyFilter;
    }


    /**
     * Получаем суммарные штрафы операторов по тимлиду
     * @param array $data
     * @return ArrayDataProvider
     */
    public function buildTeamLeadData($data) {

        $supervisors = [];
        foreach ($data as $item) {
            if (!in_array($item['operatorSupervisor'], $supervisors)) {
                $supervisors[] = Array('callCenterId' => $item['callCenterId'], 'id' => $item['operatorSupervisor'], 'penaltySum' => $item['penalty']);
            }
            else {
                foreach ($supervisors as &$supervisor) {
                    if ($supervisor['operatorSupervisor'] == $item['operatorSupervisor']) {
                        $supervisor['penaltySum'] .= $item['penalty'];
                    }
                }
            }
        }

        return new ArrayDataProvider([
            'allModels' => $supervisors,
            'pagination' => false,
        ]);
    }

}
