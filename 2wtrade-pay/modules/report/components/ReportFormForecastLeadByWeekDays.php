<?php
namespace app\modules\report\components;

use app\modules\order\models\Order;
use app\modules\report\extensions\Query;
use Yii;
use yii\data\ArrayDataProvider;
use app\modules\order\models\OrderStatus;

/**
 * Class ReportFormLeadByCountry
 * @package app\modules\report\components
 */
class ReportFormForecastLeadByWeekDays extends ReportForm
{
    public $period;

    /**
     * @param array $params
     * @return ArrayDataProvider
     */
    public function apply($params)
    {
        $this->load($params);

        $from = strtotime($params['s']['from']);
        $to = strtotime($params['s']['to'] ." 23:59:59");
        $this->period = $from - $to;

        $this->query = new Query();
        $this->query->from([Order::tableName()]);
        $this->query->where(["IS NOT", Order::tableName() .".foreign_id", NULL]);

        $this->buildSelect();
        $this->applyFilters($params);

        $dataArray = $this->query->all();

        if (!empty($dataArray)) {
            $dataArray = $this->buildWeekData($dataArray);
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $dataArray,
            'pagination' => false,
        ]);

        return $dataProvider;
    }

    /**
     * @inheritdoc
     */
    protected function buildSelect()
    {
        $this->query->select([
            'weekday' => "DATE_FORMAT(FROM_UNIXTIME(" .Order::tableName() .".created_at), \"%a\")",
            'yearcreatedat' => "DATE_FORMAT(FROM_UNIXTIME(" .Order::tableName() .".created_at), \"%Y\")",
            'monthcreatedat' => "DATE_FORMAT(FROM_UNIXTIME(" .Order::tableName() .".created_at), \"%m\")",
            'daycreatedat' => "DATE_FORMAT(FROM_UNIXTIME(" .Order::tableName() .".created_at), \"%d\")",
            'groupbyname' => "DATE_FORMAT(FROM_UNIXTIME(" .Order::tableName() .".created_at), \"%d.%m.%Y\")",
        ]);
        $this->query->addInConditionCount('status_id', OrderStatus::getAllList(), 'leadcount');
        $this->query->groupBy(['groupbyname']);

        return $this;
    }

    /**
     * @param array $params
     * @return $this
     */
    public function applyFilters($params)
    {
        $this->getDateFilter()->apply($this->query, $params);
        $this->getCountryFilter()->apply($this->query, $params);
        $this->getProductFilter()->apply($this->query, $params);

        return $this;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $content = '';
        return $content;
    }

    /**
     * @param $inputArray
     * @return array
     */
    public function buildWeekData($inputArray)
    {
        $weekArray = [];
        $resArray = [];

        // Принудительно строим массив от сегодняшнего для недели
        $tmp = time();
        for ($i = 0; $i < 7; $i++) {
            $weekArray[] = date("D", $tmp);
            $tmp += 86400;
        }

        foreach ($weekArray as $item) {
            $resArray[] = Array('weekday' => $item, 'leadcount' => 0, 'number' => 0);
        }

        foreach ($inputArray as $item) {
            if (in_array($item['weekday'], $weekArray)) {
                foreach ($resArray as &$res) {
                    if ($res['weekday'] == $item['weekday']) {
                        $res['leadcount'] += $item['leadcount'];
                        $res['number'] += 1;
                        break;
                    }
                }
            }
        }

        foreach ($resArray as &$item) {
            if ($item['weekday'] == 'Mon') $item['weekday'] = Yii::t('common', 'Понедельник');
            if ($item['weekday'] == 'Tue') $item['weekday'] = Yii::t('common', 'Вторник');
            if ($item['weekday'] == 'Wed') $item['weekday'] = Yii::t('common', 'Среда');
            if ($item['weekday'] == 'Thu') $item['weekday'] = Yii::t('common', 'Четверг');
            if ($item['weekday'] == 'Fri') $item['weekday'] = Yii::t('common', 'Пятница');
            if ($item['weekday'] == 'Sat') $item['weekday'] = Yii::t('common', 'Суббота');
            if ($item['weekday'] == 'Sun') $item['weekday'] = Yii::t('common', 'Воскресенье');
            if (!isset($item['number']) || $item['number'] == 0) {
                $item['leadcount'] = 0;
            }
            else {
                $item['leadcount'] = round($item['leadcount'] / $item['number']);
            }
        }

        return $resArray;
    }


}
