<?php
namespace app\modules\report\components;

use app\models\Country;
use app\models\Product;
use app\modules\order\models\Order;
use app\modules\order\models\OrderProduct;
use app\modules\order\models\OrderStatus;
use app\modules\report\components\filters\DateFilterTopProduct;
use app\modules\report\components\filters\TopProductFilter;
use app\modules\report\extensions\DataProviderTopProduct;
use app\modules\report\extensions\Query;

/**
 * Class ReportFormTopProduct
 * @package app\modules\report\components
 */
class ReportFormTopProduct extends ReportForm
{
/**
 * @var string Группировка по умолчанию
 */
    public $groupBy = self::GROUP_BY_PRODUCTS;

/**
 * @inheritdoc
 */
    public function init()
    {
        parent::init();
    }

/**
 * @param array $params
 * @return DataProviderTopProduct
 */
    public function apply($params)
    {
        $this->load($params);

        $this->query = new Query();
        $this->query->from([OrderProduct::tableName()]);

        $this->query->innerJoin(Order::tableName(), Order::tableName() . '.id = ' . OrderProduct::tableName() . '.order_id');
        $this->query->innerJoin(Country::tableName(), Country::tableName() . '.id = ' . Order::tableName() . '.country_id');
        $this->query->innerJoin(Product::tableName(), Product::tableName() . '.id = ' . OrderProduct::tableName() . '.product_id');

        $this->query->where([Order::tableName() . '.status_id' => OrderStatus::getBuyoutList()]);

        $this->query->orderBy(['countryid' => SORT_ASC, 'yearcreatedat' => SORT_ASC, 'monthcreatedat' => SORT_ASC, 'countproducts' => SORT_DESC]);

        $this->query->groupBy(['yearcreatedat', 'monthcreatedat', 'orderproductid']);

        $this->buildSelect()->applyFilters($params);
        $dataProvider = new DataProviderTopProduct([
            'form' => $this,
            'query' => $this->query,
        ]);

        $dataProvider->pagination->pageSize = $dataProvider->pagination->totalCount;

        return $dataProvider;
    }

/**
 * @param \app\components\widgets\ActiveForm $form
 * @return string
 */
    public function restore($form)
    {
        $output = '';

        return $output;
    }

/**
 * @inheritdoc
 */
    protected function buildSelect()
    {
        $this->query->addCondition(Country::tableName() . '.name', 'countryName');
        $this->query->addCondition('YEAR(FROM_UNIXTIME(' . Order::tableName() . '.created_at))', 'yearCreatedAt');
        $this->query->addCondition('MONTH(FROM_UNIXTIME(' . Order::tableName() . '.created_at))', 'monthCreatedAt');
        $this->query->addCondition(OrderProduct::tableName() . '.product_id', 'orderProductId');
        $this->query->addCondition('SUM(' . OrderProduct::tableName() . '.quantity)', 'countProducts');
        $this->query->addCondition(Country::tableName() . '.id', 'countryId');
        $this->query->addCondition(OrderProduct::tableName() . '.created_at', 'createdDate');
        $this->query->addCondition(Product::tableName() . '.name', 'productName');

        return $this;
    }

/**
 * @param array $params
 * @return ReportForm
 */
    protected function applyFilters($params)
    {
        $this->getDateFilter()->apply($this->query, $params);
        $this->getCountrySelectFilter()->apply($this->query, $params);
        $this->getProductFilter()->apply($this->query, $params);

        return $this;
    }

    /**
     * @return TopProductFilter
     */
    public function getProductFilter()
    {
        if (is_null($this->productFilter)) {
            $this->productFilter = new TopProductFilter(['reportForm' => $this]);
        }

        return $this->productFilter;
    }

/**
 * @inheritdoc
 */
    protected function prepareQuery()
    {

        $closure = $this->getQueryClosure();

        if ($closure instanceof \Closure) {
            $closure($this->query);
        }

        return $this;
    }

    /**
     * @return DateFilterTopProduct
     */
    public function getDateFilter()
    {
        if (is_null($this->dateFilter)) {
            $this->dateFilter = new DateFilterTopProduct();
        }

        return $this->dateFilter;
    }
}
