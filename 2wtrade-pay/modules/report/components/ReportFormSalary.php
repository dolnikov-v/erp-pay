<?php

namespace app\modules\report\components;

use app\helpers\Utils;
use app\models\Country;
use app\modules\callcenter\models\CallCenter;
use app\modules\callcenter\models\CallCenterToOffice;
use app\modules\crocotime\models\CrocotimeWorkTime;
use app\modules\report\components\filters\DateFilter;
use app\modules\report\components\invoice\generators\InvoiceFinanceBuilder;
use app\modules\salary\models\Designation;
use app\modules\salary\models\Holiday;
use app\modules\salary\models\MonthlyStatement;
use app\modules\salary\models\MonthlyStatementData;
use app\modules\salary\models\Office;
use app\modules\salary\models\OfficeTax;
use app\modules\salary\models\Penalty;
use app\modules\salary\models\Bonus;
use app\modules\salary\models\Person;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\callcenter\models\CallCenterUser;
use app\modules\salary\models\PersonLink;
use app\modules\salary\models\WorkingShift;
use app\modules\callcenter\models\CallCenterWorkTime;
use app\modules\salary\models\WorkTimePlan;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use app\modules\report\extensions\Query;
use app\widgets\Panel;
use kartik\mpdf\Pdf;
use keltstr\simplehtmldom\SimpleHTMLDom;
use yii\data\ArrayDataProvider;
use app\modules\report\components\filters\DateWorkFilter;
use app\modules\report\components\filters\OfficeFilter;
use app\modules\report\components\filters\CallCenterFilter;
use app\modules\report\components\filters\TeamLeadFilter;
use app\modules\report\components\filters\DesignationFilter;
use app\modules\report\components\filters\CallCenterMonthlyStatementFilter;
use Yii;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\httpclient\Client;

/**
 * Class ReportFormSalary
 * @package app\modules\report\components
 */
class ReportFormSalary extends ReportForm
{
    /**
     * @var array Маппинг статусов
     */
    protected $mapStatuses = [];

    public $groupBy = self::GROUP_BY_LAST_FOREIGN_OPERATOR;

    /*
     * "Галочка" конвертировать в доллар всегда отмечена
     * @var string
     */
    public $dollar = 'on';

    /**
     * @var DateWorkFilter
     */
    protected $dailyFilter;

    /**
     * @var OfficeFilter
     */
    public $officeFilter;

    /**
     * @var CallCenterFilter
     */
    protected $callCenterFilter;

    /**
     * @var CallCenterMonthlyStatementFilter
     */
    protected $callCenterMonthlyStatementFilter;

    /**
     * @var TeamLeadFilter
     */
    protected $teamLeadFilter;

    /**
     * @var DesignationFilter
     */
    protected $designationFilter;

    /**
     * @var integer
     */
    public $totalCount;

    /**
     * @var boolean
     */
    public $showHolidays = false;

    /**
     * @var boolean
     */
    public $stateMode = false;

    /**
     * @var boolean
     */
    public $closedMode = false;

    /**
     * @var integer
     */
    public $person = null;

    /**
     * @var boolean
     */
    public $calcSalaryLocal = false;

    /**
     * @var boolean
     */
    public $calcSalaryUSD = false;

    /**
     * @var array
     */
    public $taxRates = [];

    /**
     * Из-за проблемы с новым КЦ (один логин на все направления) будем получать данные по каждому сотруднику отдельно
     * @var boolean
     */
    protected $forceLoadWorkTime = true;

    /**
     * Часы КЗОТ по раб. плану
     * @var array
     */
    protected $planHoursByWorkingShift = [];

    /**
     * Число апрувов по КЦ
     * @var array
     */
    public $totalApprove = [];

    /**
     * @return array
     */
    public function getMapStatuses()
    {
        if (empty($this->mapStatuses)) {
            $this->mapStatuses = [
                'approve' => OrderStatus::getApproveList(),
                'total' => OrderStatus::getAllList(),
            ];
        }
        return $this->mapStatuses;
    }

    /**
     * @param array $params
     * @return ArrayDataProvider
     */
    public function apply($params)
    {
        $saveMode = $params['saveMode'] ?? false;
        if (empty($params[$this->formName()])) {
            $this->panelAlert = Yii::t('common', 'Выберите Офис или Направление');
            $this->panelAlertStyle = Panel::ALERT_DANGER;
        }

        $this->load($params);

        $person = $params['person'] ?? null;

        $this->query = new Query();
        $this->applyFilters($params);

        $monthlyStatementId = $this->getCallCenterMonthlyStatementFilter()->apply($this->query, $params);
        if ($monthlyStatementId) {
            $this->stateMode = true;
            $callCenterMonthlyStatement = MonthlyStatement::findOne($monthlyStatementId);
            if ($callCenterMonthlyStatement instanceof MonthlyStatement && $callCenterMonthlyStatement->closed_at) {
                $this->closedMode = true;
            }

            $params[$this->formName()] = [
                'from' => $callCenterMonthlyStatement->date_from,
                'to' => $callCenterMonthlyStatement->date_to
            ];
        }

        $dateParams = $this->getDateWorkFilter()->apply($this->query, $params);

        $dateFrom = date("Y-m-d", strtotime($this->getDateWorkFilter()->from));
        $dateTo = date("Y-m-d", strtotime($this->getDateWorkFilter()->to));

        $this->buildSelect();

        $allModels = [];

        if (!$this->panelAlert) {

            $subQueryTime = CallCenterWorkTime::find()
                ->select([
                    'count_time' => 'SUM(' . CallCenterWorkTime::tableName() . '.time)',
                    'count_calls' => 'SUM(' . CallCenterWorkTime::tableName() . '.number_of_calls)',
                    'call_center_user_id' => CallCenterWorkTime::tableName() . '.call_center_user_id'
                ])
                ->groupBy('call_center_user_id');
            foreach ($dateParams as $d) {
                $subQueryTime->andFilterWhere($d);
            }

            $subQueryBonus = Bonus::find()
                ->select([
                    'bonus_sum_usd' => 'SUM(' . Bonus::tableName() . '.bonus_sum_usd)',
                    'bonus_sum_local' => 'SUM(' . Bonus::tableName() . '.bonus_sum_local)',
                    'bonus_percent' => 'SUM(' . Bonus::tableName() . '.bonus_percent)',
                    'bonus_hour' => 'SUM(' . Bonus::tableName() . '.bonus_hour)',
                    'bonus_percent_hour' => 'SUM(' . Bonus::tableName() . '.bonus_percent_hour)',
                    'person_id' => Bonus::tableName() . '.person_id'
                ])
                ->where([
                    'and',
                    ['>=', Bonus::tableName() . '.date', $dateFrom],
                    ['<=', Bonus::tableName() . '.date', $dateTo]
                ])
                ->groupBy('person_id');

            $subQueryPenalty = Penalty::find()
                ->select([
                    'penalty_sum_usd' => 'SUM(' . Penalty::tableName() . '.penalty_sum_usd)',
                    'penalty_sum_local' => 'SUM(' . Penalty::tableName() . '.penalty_sum_local)',
                    'person_id' => Penalty::tableName() . '.person_id'
                ])
                ->where([
                    'and',
                    ['>=', Penalty::tableName() . '.date', $dateFrom],
                    ['<=', Penalty::tableName() . '.date', $dateTo]
                ])
                ->groupBy('person_id');


            if ($this->stateMode) {
                // сохраненный график
                $this->query->from([MonthlyStatementData::tableName()]);
                $this->query->where(['statement_id' => $monthlyStatementId]);

                $this->query->leftJoin(MonthlyStatement::tableName(), MonthlyStatement::tableName() . '.id=' . MonthlyStatementData::tableName() . '.statement_id');
                $this->query->leftJoin(Office::tableName(), Office::tableName() . '.id=' . MonthlyStatement::tableName() . '.office_id');
                $this->query->leftJoin(Person::tableName(), Person::tableName() . '.id=' . MonthlyStatementData::tableName() . '.person_id');

                // по-хорошему нужно клеить таблицу и считать в запросе
                if (!$this->forceLoadWorkTime) {
                    $this->query->leftJoin(['time' => $subQueryTime],
                        new Expression('FIND_IN_SET(time.call_center_user_id,' . MonthlyStatementData::tableName() . '.call_center_user_ids) > 0')
                    );
                }

                $groupBy = MonthlyStatementData::tableName() . ".person_id";
            } else {
                // онлайн выборка
                $this->query->from([Person::tableName()]);
                $this->query->leftJoin(Office::tableName(), Office::tableName() . '.id=' . Person::tableName() . '.office_id');
                $this->query->leftJoin(CallCenterUser::tableName(), CallCenterUser::tableName() . '.person_id=' . Person::tableName() . '.id');
                $this->query->leftJoin(Designation::tableName(), Designation::tableName() . '.id=' . Person::tableName() . '.designation_id');
                $this->query->leftJoin(CallCenter::tableName(), CallCenter::tableName() . '.id=' . CallCenterUser::tableName() . '.callcenter_id');
                $this->query->leftJoin(Country::tableName(), Country::tableName() . '.id=' . CallCenter::tableName() . '.country_id');
                $this->query->leftJoin(Person::tableName() . ' as parent', Person::tableName() . '.parent_id=parent.id');
                $this->query->leftJoin(Designation::tableName() . ' as parent_designation', 'parent.designation_id=parent_designation.id');
                $this->query->leftJoin(WorkingShift::tableName(), WorkingShift::tableName() . '.id=' . Person::tableName() . '.working_shift_id');

                // по-хорошему нужно клеить таблицу и считать в запросе
                if (!$this->forceLoadWorkTime) {
                    $this->query->leftJoin(['time' => $subQueryTime], 'time.call_center_user_id = ' . CallCenterUser::tableName() . '.id');
                }

                $groupBy = Person::tableName() . ".id";

                $this->query->andWhere([
                    'or',
                    ['<=', Person::tableName() . '.start_date', date("Y-m-d", strtotime($dateTo))],
                    ['is', Person::tableName() . '.start_date', null]
                ]);

                $this->query->andWhere([
                    'or',
                    [
                        '>=',
                        'DATE_FORMAT(' . Person::tableName() . '.dismissal_date, "%Y-%m")',
                        date("Y-m", strtotime($dateTo))
                    ],
                    ['is', Person::tableName() . '.dismissal_date', null]
                ]);

                $this->query->orderBy([
                    Person::tableName() . '.parent_id' => SORT_ASC,
                    Person::tableName() . '.active' => SORT_DESC,
                    Person::tableName() . '.name' => SORT_ASC,
                ]);
            }

            $this->query->leftJoin(['bonus' => $subQueryBonus], 'bonus.person_id = ' . $groupBy);
            $this->query->leftJoin(['penalty' => $subQueryPenalty], 'penalty.person_id = ' . $groupBy);

            if ($person) {
                $this->query->andWhere([
                    $groupBy => $person
                ]);
            }

            $this->query->groupBy([$groupBy]);

            $allModels = ArrayHelper::index($this->query->all(), 'id');

            if ($person && sizeof($allModels) == 1) {
                $this->person = $person;
            }

            // нужна возможность выбрать произвольный период в месяце, например с 1 по 15 число
            // для правильного подсчета берем полный месяц
            $startMonth = date('Y-m-01', strtotime($dateFrom));
            $endMonth = date('Y-m-t', strtotime($dateFrom));

            // праздники по всем странам в виде $holidays[country_id][dates]
            $holidayQuery = Holiday::find()
                ->where([
                    'and',
                    ['>=', 'date', $dateFrom],
                    ['<=', 'date', $dateTo]
                ])
                ->asArray();
            if (!(Yii::$app instanceof \yii\console\Application)) {
                $holidayQuery->bySystemUserCountries();
            }
            $holidays = ArrayHelper::index(
                $holidayQuery->all(),
                'date',
                'country_id'
            );

            $this->getMapStatuses();
            $this->query = new Query();
            $this->query->from([Order::tableName()]);
            $this->query->select([
                'last_foreign_operator' => CallCenterRequest::tableName() . '.last_foreign_operator',
                'call_center_id' => CallCenterRequest::tableName() . '.call_center_id',
            ]);

            $this->buildSelectMapStatuses();
            $this->query->leftJoin(CallCenterRequest::tableName(), CallCenterRequest::tableName() . '.order_id = ' . Order::tableName() . '.id');
            $this->query->leftJoin(DeliveryRequest::tableName(), DeliveryRequest::tableName() . '.order_id = ' . Order::tableName() . '.id');
            $this->query->leftJoin(Country::tableName(), Order::tableName() . '.country_id=' . Country::tableName() . '.id');
            $this->applyOrderFilters($params);



            if ($this->getOfficeFilter()->office) {
                $subQueryOffice = CallCenterToOffice::find()
                    ->select('call_center_id')
                    ->andWhere(['office_id' => $this->getOfficeFilter()->office]);
                if ($callCenterIds = $subQueryOffice->column()) {
                    $this->query->andWhere([CallCenterRequest::tableName() . '.call_center_id' => $callCenterIds]);
                }
            }

            if ($this->getCallCenterFilter()->callCenter) {
                $this->query->andWhere([CallCenterRequest::tableName() . '.call_center_id' => $this->getCallCenterFilter()->callCenter]);
            }

            if ($this->person) {
                $operIds = [];
                foreach (ArrayHelper::getColumn($allModels, 'call_center_oper_ids') as $line) {
                    if ($line) {
                        $operIds = array_merge($operIds, explode(',', $line));
                    }
                }
                $this->query->andWhere([
                    'in',
                    CallCenterRequest::tableName() . '.last_foreign_operator',
                    array_unique($operIds)
                ]);
            }
            $this->query->groupBy([
                'call_center_id',
                'last_foreign_operator'
            ]);

            $rawOrders = $this->query->all();

            $orders = ArrayHelper::index($rawOrders, 'last_foreign_operator', ['call_center_id']);

            // число недель в выборке, 4 или 2
            $weekNumbers = round((strtotime($dateTo) - strtotime($dateFrom)) / 60 / 60 / 24 / 7);

            $weekDaysNumber = $this->calcWeekDaysNumber($dateFrom, $dateTo);

            $totalCount = 0;
            // по всем сотрудникам
            foreach ($allModels as &$model) {

                $planTimes = [];

                if ($model['calc_salary_local']) {
                    $this->calcSalaryLocal = true;
                }
                if ($model['calc_salary_usd']) {
                    $this->calcSalaryUSD = true;
                }

                $model['week_numbers'] = $weekNumbers;

                // Тим лидов группируем в его же команду
                if ($model['designation_team_lead']) {
                    $model['parent_id'] = $model['id'];
                    $model['parent_name'] = $model['fio'];
                    $model['parent_designation'] = $model['designation'];
                }

                $model['dismissed'] = false;
                if ($model['dismissal_date']) {
                    if (
                        (strtotime($model['dismissal_date']) >= strtotime($startMonth) && strtotime($model['dismissal_date']) < strtotime($endMonth)) || // уволили в этом месяце
                        (strtotime($model['dismissal_date']) < strtotime($startMonth))  // уволили до начала месяца
                    ) {
                        $model['dismissed'] = true;
                    }
                }
                if ($model['start_date']) {
                    if (strtotime($model['start_date']) > strtotime($dateTo))  // устроили после конца выборки
                    {
                        $model['dismissed'] = true;
                    }
                    if (strtotime($model['start_date']) >= strtotime($dateFrom) && strtotime($model['start_date']) <= strtotime($dateTo)) { // устроили в этом периоде
                        $model['dismissed'] = true;
                    }
                }

                // если расчет еще не сохранен, то будем считать, иначе у нас это уже в базе сохранено
                if (!$this->closedMode) {

                    if ($this->forceLoadWorkTime) {
                        // костыльно получаем список IDs
                        $ids = CallCenterUser::getUniqueIdList($model['id']);
                    } else {
                        $ids = explode(',', $model['call_center_user_ids']);
                    }

                    $model['holiday_worked_time'] = 0;

                    $freeDays = [];
                    if (isset($holidays[$model['country_id']]) && $holidays[$model['country_id']]) {
                        // есть праздники в месяц
                        $this->showHolidays = true;

                        // посчитаем число праздников по дням недели
                        foreach ($holidays[$model['country_id']] as $holiday) {
                            $n = date('N', strtotime($holiday['date']));
                            $freeDays[$n] = $freeDays[$n] ?? 0;
                            $freeDays[$n]++;
                        }

                        if ($model['designation_calc_work_time']) {
                            // для операторов
                            if ($ids) {
                                // есть аккаунты
                                $model['holiday_worked_time'] = CallCenterWorkTime::find()
                                    ->where(['in', 'call_center_user_id', $ids])
                                    ->andWhere(['date' => array_keys($holidays[$model['country_id']])])
                                    ->sum('time');
                                $model['holiday_worked_time'] = $model['holiday_worked_time'] / 60 / 60;
                            }
                        } else {
                            // для остальных окладников должен быть по плану этот день в работе
                            $model['holiday_worked_time'] = WorkTimePlan::find()
                                ->where(['person_id' => $model['id']])
                                ->andWhere(['date' => array_keys($holidays[$model['country_id']])])
                                ->sum('work_hours');

                            // для тим лидов, пусть как для всех считается
                            /*
                            if ($model['designation_team_lead']) {
                                $teamLeads[$this->person] = $this->person;
                                $model['holiday_worked_time'] = $this->calcTeamLeadHolidays($model, array_keys($holidays[$model['country_id']]));
                            }
                            */
                        }
                    }

                    // число часов в интервале
                    $model['count_normal_hours'] = 0;
                    if ($model['count_fixed_norm']) {
                        $model['count_normal_hours'] = $model['week_numbers'] * $model['working_hours_per_week'];
                    } else {

                        if (empty($planTimes)) {
                            // считаем число часов по плану
                            $planTimes = WorkTimePlan::getPlanList($model['id'], $dateFrom, $dateTo, $model['start_date'], $model['dismissal_date']);
                            foreach ($planTimes as $hour) {
                                $model['count_normal_hours'] += $hour;
                            }
                        }

                        if (!$model['count_normal_hours']) {
                            // если нету часов по плану по какойто причине, считаем по старому по текущей смене
                            $model['count_normal_hours'] = $this->calcHoursInPeriod($model, $weekDaysNumber, $freeDays);
                        }
                    }

                    if ($model['dismissed']) {
                        $model['count_normal_hours'] = $this->getWorkingShiftFromPlan($model, $dateFrom, $dateTo, $weekDaysNumber, $freeDays) ?: $model['count_normal_hours'];
                    }

                    // для уволенных или принятых в этом периоде сотрудников отбросить лишние часы
                    // Из-за проблемы с новым КЦ (один логин на все направления) будем получать данные так
                    if ($this->forceLoadWorkTime ||
                        ($model['start_date'] && $model['start_date'] > $dateFrom) ||
                        ($model['dismissal_date'] && $model['dismissal_date'] < $dateTo)
                    ) {

                        $times = CallCenterWorkTime::getWorkTimes($ids, $dateFrom, $dateTo, $model['start_date'], $model['dismissal_date']);

                        if (empty($planTimes)) {
                            $planTimes = WorkTimePlan::getPlanList($model['id'], $dateFrom, $dateTo, $model['start_date'], $model['dismissal_date']);
                        }

                        $model['count_time'] = 0;
                        foreach ($times as $timeDate => $timeHours) {

                            if ($timeHours > 16) {
                                // если больше 16 часов в день работает, то это явно косяк и пишем норму (если ее нет то 8) часов
                                $timeHours = $planTimes[$timeDate] ?? 8;
                            }

                            $model['count_time'] += $timeHours;
                        }

                        // только число звонков
                        $timesQuery = CallCenterWorkTime::find()
                            ->select([
                                'count_calls' => 'SUM(' . CallCenterWorkTime::tableName() . '.number_of_calls)'
                            ])
                            ->where(['in', 'call_center_user_id', $ids])
                            ->andWhere(['>=', 'date', $dateFrom])
                            ->andWhere(['<=', 'date', $dateTo]);

                        if ($model['start_date']) {
                            $timesQuery->andWhere(['>=', 'date', $model['start_date']]);
                        }
                        if ($model['dismissal_date']) {
                            $timesQuery->andWhere(['<=', 'date', $model['dismissal_date']]);
                        }

                        $model['count_calls'] = $timesQuery->scalar();
                    }

                    // сохраним для рассчета без праздников и переработок
                    $count_time = $model['count_time'];

                    if (!$model['designation_calc_work_time']) {
                        // фиксированный оклад, считаем по плану часы всем
                        if (empty($planTimes)) {
                            $planTimes = WorkTimePlan::getPlanList($model['id'], $dateFrom, $dateTo, $model['start_date'], $model['dismissal_date']);
                        }

                        $count_time = 0;
                        foreach ($planTimes as $planTime) {
                            $count_time += $planTime;
                        }
                        $model['count_time'] = $count_time;
                    }

                    $model['count_extra_hours'] = 0;

                    if ($model['holiday_worked_time']) {
                        // выходил в праздники
                        // из рассчетных часов вычитаем праздники, т.к. их считаем отдельно
                        $count_time -= $model['holiday_worked_time'];
                    }

                    // учтем переработки и праздники
                    $extraHours = $model['count_time'] - $model['count_normal_hours'];
                    if ($extraHours > 0) {
                        // есть переработка

                        // переработка, часов
                        if ($count_time > $model['count_normal_hours']) {
                            // запоминаем переработку
                            $model['count_extra_hours'] = ($count_time - $model['count_normal_hours']);
                            // из рассчетных часов вычитаем переработку, т.к. их считаем отдельно
                            $count_time -= $model['count_extra_hours'];
                        }
                    }

                    // оклад месячный и в часах
                    if ($model['salary_scheme'] == Person::SALARY_SCHEME_HOURLY) { // высчитываем оклад месячный от почасового
                        $model['salary_local'] = $model['salary_usd'] = 0;
                        if ($model['calc_salary_local']) {
                            if ($model['count_normal_hours']) {
                                $model['salary_local'] = $model['salary_hour_local'] * $model['count_normal_hours'];
                                if ($model['week_numbers'] != 4) {
                                    $model['salary_local'] *= $model['week_numbers'] / 4;
                                }
                            }
                        }
                        if ($model['calc_salary_usd']) {
                            if ($model['count_normal_hours']) {
                                $model['salary_usd'] = $model['salary_hour_usd'] * $model['count_normal_hours'];
                                if ($model['week_numbers'] != 4) {
                                    $model['salary_usd'] *= $model['week_numbers'] / 4;
                                }
                            }
                        }
                    } else { // высчитываем оклад почасовый от месячного
                        $model['salary_hour_local'] = $model['salary_hour_usd'] = 0;
                        if ($model['calc_salary_local']) {
                            if ($model['count_normal_hours']) {
                                $model['salary_hour_local'] = $model['salary_local'] / $model['count_normal_hours'];
                                if ($model['week_numbers'] != 4) {
                                    $model['salary_hour_local'] *= $model['week_numbers'] / 4;
                                }
                            }
                        }
                        if ($model['calc_salary_usd']) {
                            if ($model['count_normal_hours']) {
                                $model['salary_hour_usd'] = $model['salary_usd'] / $model['count_normal_hours'];
                                if ($model['week_numbers'] != 4) {
                                    $model['salary_hour_usd'] *= $model['week_numbers'] / 4;
                                }
                            }
                        }
                    }

                    // бонусы в виде процента от итоговых часов
                    if ($model['bonus_percent_hour']) {
                        $model['bonus_hour'] += $count_time * $model['bonus_percent_hour'] / 100;
                    }

                    // бонусы в виде часов
                    if ($model['bonus_hour']) {
                        if ($model['calc_salary_local']) {
                            $model['bonus_sum_local'] += $model['salary_hour_local'] * $model['bonus_hour'];
                        }
                        if ($model['calc_salary_usd']) {
                            $model['bonus_sum_usd'] += $model['bonus_sum_usd'] * $model['bonus_hour'];
                        }
                    }

                    if ($model['calc_salary_local']) {
                        $model['salary_hand_local'] = $model['salary_hour_local'] * $count_time;
                        $model['salary_hand_local_extra'] = $model['extra_days_coefficient'] * $model['salary_hour_local'] * $model['count_extra_hours'];
                        $model['salary_hand_local_holiday'] = $model['holidays_coefficient'] * $model['salary_hour_local'] * $model['holiday_worked_time'];

                        $model['salary_hand_local'] += $model['salary_hand_local_extra'];
                        $model['salary_hand_local'] += $model['salary_hand_local_holiday'];
                        if ($model['bonus_percent']) {
                            // бонус в виде процента от зп на руки
                            $model['bonus_sum_local'] += $model['salary_hand_local'] * $model['bonus_percent'] / 100;
                        }
                        $model['salary_hand_local'] += $model['bonus_sum_local'];

                        if ($model['min_salary_local']) {
                            // есть минимальная зп в офисе
                            if ($model['salary_hand_local'] < $model['min_salary_local']) {
                                // минимальная зп больше чем на руки
                                $daysInPeriodByWorkingShift = $this->calcDaysInPeriod($model, $weekDaysNumber, $freeDays);
                                $daysInPeriodByPerson = sizeof($planTimes);
                                if ($daysInPeriodByWorkingShift) {
                                    // выставим минималку с учетом реально отработанного
                                    $model['salary_hand_local'] = max(
                                        $model['salary_hand_local'],
                                        $model['min_salary_local'] * $daysInPeriodByPerson / $daysInPeriodByWorkingShift);
                                }
                            }
                        }
                        $model['salary_hand_local'] -= $model['penalty_sum_local'];

                        $model['salary_tax_local'] = self::getTaxSum($model['salary_hand_local'], $model['office_id'], $model['insurance_coefficient'], $model['pension_contribution'], $dateTo);
                    }

                    if ($model['calc_salary_usd']) {
                        $model['salary_hand_usd'] = $model['salary_hour_usd'] * $count_time;
                        $model['salary_hand_usd_extra'] = $model['extra_days_coefficient'] * $model['salary_hour_usd'] * $model['count_extra_hours'];
                        $model['salary_hand_usd_holiday'] = $model['holidays_coefficient'] * $model['salary_hour_usd'] * $model['holiday_worked_time'];

                        $model['salary_hand_usd'] += $model['salary_hand_usd_extra'];
                        $model['salary_hand_usd'] += $model['salary_hand_usd_holiday'];
                        if ($model['bonus_percent']) {
                            // бонус в виде процента от зп на руки
                            $model['bonus_sum_usd'] += $model['salary_hand_usd'] * $model['bonus_percent'] / 100;
                        }
                        $model['salary_hand_usd'] += $model['bonus_sum_usd'];

                        if ($model['min_salary_usd']) {
                            if ($model['salary_hand_usd'] < $model['min_salary_usd']) {
                                $daysInPeriodByWorkingShift = $this->calcDaysInPeriod($model, $weekDaysNumber, $freeDays);
                                $daysInPeriodByPerson = sizeof($planTimes);
                                if ($daysInPeriodByWorkingShift) {
                                    $model['salary_hand_usd'] = max(
                                        $model['salary_hand_usd'],
                                        $model['min_salary_usd'] * $daysInPeriodByPerson / $daysInPeriodByWorkingShift);
                                }
                            }
                        }
                        $model['salary_hand_usd'] -= $model['penalty_sum_usd'];

                        $model['salary_tax_usd'] = self::getTaxSum($model['salary_hand_usd'], $model['office_id'], $model['insurance_coefficient'], $model['pension_contribution'], $dateTo);
                    }
                }

                $model['approve'] = $model['buyout'] = $model['vsego'] = $model['srednij_cek_apruvlennyj'] = $model['srednij_cek_vykupnoj'] = 0;

                if ($model['call_center_id_oper_id']) {
                    // есть привязанные пользователи КЦ

                    $avgA = [];
                    $avgB = [];
                    foreach (explode(',', $model['call_center_id_oper_id']) as $callCenterWithUserId) {

                        $tmp = explode('_', $callCenterWithUserId);
                        $callCenterId = $tmp[0];
                        $operatorId = $tmp[1];

                        if (isset($orders[$callCenterId][$operatorId])) {

                            $this->totalApprove[$callCenterId] = $this->totalApprove[$callCenterId] ?? 0;
                            $this->totalApprove[$callCenterId] += $orders[$callCenterId][$operatorId]['approve'] ?? 0;

                            // суммируем т.к. считаем что мог апрувить под разными аккаунтами
                            $model['approve'] += $orders[$callCenterId][$operatorId]['approve'] ?? 0;
                            $model['buyout'] += $orders[$callCenterId][$operatorId]['buyout'] ?? 0;
                            $model['vsego'] += $orders[$callCenterId][$operatorId]['vsego'] ?? 0;
                            $avgA[] = $orders[$callCenterId][$operatorId]['srednij_cek_apruvlennyj'] ?? 0;
                            $avgB[] = $orders[$callCenterId][$operatorId]['srednij_cek_vykupnoj'] ?? 0;

                            if (!isset($model['call_center_approve'][$callCenterId])) {
                                $model['call_center_approve'][$callCenterId] = 0;
                            }
                            $model['call_center_approve'][$callCenterId] += $orders[$callCenterId][$operatorId]['approve'] ?? 0;

                            $totalCount += $model['vsego'];
                        }
                    }
                    $model["approve_percent"] = $model["vsego"] ? $model["approve"] * 100 / $model["vsego"] : 0;

                    $model["srednij_cek_apruvlennyj"] = $avgA ? array_sum($avgA) / sizeof($avgA) : 0;
                    $model["srednij_cek_vykupnoj"] = $avgB ? array_sum($avgB) / sizeof($avgB) : 0;

                    if (!$model["approved"]) { // для неподтвержденных обнулим некоторые показатели
                        $model["salary_local"] = $model["salary_usd"] = $model["salary_hour_usd"] = $model["salary_hour_local"]
                            = $model["salary_hand_local"] = $model["salary_hand_usd"]
                            = $model["salary_tax_local"] = $model["salary_tax_usd"]
                            = $model["salary_hand_local_holiday"]
                            = $model["salary_hand_usd_holiday"] = $model["salary_hand_local_extra"] = $model["salary_hand_usd_extra"]
                            = $model["count_normal_hours"] = $model["count_extra_hours"] = $model["count_time"] = 0;
                    }
                }
            }

            $this->totalCount = $totalCount;

            ArrayHelper::multisort($allModels,
                ['parent_id', $this->calcSalaryLocal ? 'salary_local' : 'salary_usd', 'active', 'fio'],
                [SORT_ASC, SORT_DESC, SORT_DESC, SORT_ASC]);


            if ($this->person || $saveMode) {

                foreach ($allModels as &$model) {
                    $model['penalties'] =
                        Penalty::find()
                            ->where([
                                'and',
                                ['=', Penalty::tableName() . ".person_id", $this->person],
                                ['>=', Penalty::tableName() . ".date", date("Y-m-d", strtotime($dateFrom))],
                                ['<=', Penalty::tableName() . ".date", date("Y-m-d", strtotime($dateTo))]
                            ])
                            ->all();


                    $model['bonuses'] =
                        Bonus::find()
                            ->where([
                                'and',
                                ['=', Bonus::tableName() . ".person_id", $this->person],
                                ['>=', Bonus::tableName() . ".date", date("Y-m-d", strtotime($dateFrom))],
                                ['<=', Bonus::tableName() . ".date", date("Y-m-d", strtotime($dateTo))]
                            ])
                            ->all();

                    $dates = [];
                    $date = $dateFrom;
                    while (strtotime($date) <= strtotime($dateTo)) {
                        $curDate = date("Y-m-d", strtotime($date));
                        $date = date("Y-m-d", strtotime("+1 day", strtotime($date)));
                        $dates[$curDate] = isset($holidays[$model['country_id']][$curDate]) ? 1 : 0;
                    }
                    $model['dates'] = $dates;

                    $queryTimes = null;

                    $personLink = PersonLink::find()
                        ->byPersonId($model['id'])
                        ->isCrocoTime()
                        ->one();

                    $model['times'] = [];
                    if (!$model['designation_calc_work_time'] && $personLink instanceof PersonLink) {
                        // для не операторов, попробуем по CrocoTime
                        $queryTimes = CrocotimeWorkTime::find()
                            ->select([
                                'time' => '(summary_time)/3600',
                                'date' => 'date',
                            ])
                            ->where(['crocotime_employee_id' => $personLink->foreign_id])
                            ->andWhere(['>=', 'date', date('Y-m-d', strtotime($dateFrom))])
                            ->andWhere(['<=', 'date', date('Y-m-d', strtotime($dateTo))]);
                        if (!is_null($queryTimes)) {
                            if ($model['start_date'] != '') {
                                $queryTimes->andWhere(['>=', 'date', $model['start_date']]);
                            }
                            if ($model['dismissal_date'] != '') {
                                $queryTimes->andWhere(['<=', 'date', $model['dismissal_date']]);
                            }

                            $model['times'] =
                                ArrayHelper::map(
                                    $queryTimes
                                        ->asArray()
                                        ->all(), 'date', 'time');

                        }
                    }

                    if (empty($planTimes)) {
                        $planTimes = WorkTimePlan::getPlanList($model['id'], $dateFrom, $dateTo, $model['start_date'], $model['dismissal_date']);
                    }

                    // оператор
                    if ($model['designation_calc_work_time']) {

                        if ($this->forceLoadWorkTime) {
                            // костыльно получаем список IDs
                            $ids = CallCenterUser::getUniqueIdList($model['id']);
                        } else {
                            $ids = explode(',', $model['call_center_user_ids']);
                        }

                        $times = CallCenterWorkTime::getWorkTimes($ids, $dateFrom, $dateTo, $model['start_date'], $model['dismissal_date']);

                        foreach ($times as $timeDate => $timeHours) {
                            if ($timeHours > 16) {
                                // если больше 16 часов в день работает, то это явно косяк и пишем норму (если ее нет то 8) часов
                                $times[$timeDate] = $planTimes[$timeDate] ?? 8;
                            }
                        }
                        $model['times'] = $times;

                    }

                    $model['plan'] = $planTimes;

                    $model['times']['total'] = $model['count_time'];
                }
            }
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $allModels
        ]);
        $dataProvider->pagination->pageSize = $dataProvider->pagination->totalCount;


        return $dataProvider;
    }
    
    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $output = '';

        return $output;
    }


    /**
     * @inheritdoc
     */
    protected function buildSelect()
    {
        if ($this->stateMode) {
            $select = [
                'id' => MonthlyStatementData::tableName() . '.person_id',
                'office' => MonthlyStatementData::tableName() . '.office',
                'fio' => MonthlyStatementData::tableName() . '.name',
                'start_date' => MonthlyStatementData::tableName() . '.start_date',
                'dismissal_date' => MonthlyStatementData::tableName() . '.dismissal_date',
                'active' => MonthlyStatementData::tableName() . '.active',
                'approved' => Person::tableName() . '.approved',
                'parent_id' => MonthlyStatementData::tableName() . '.parent_id',
                'parent_name' => MonthlyStatementData::tableName() . '.parent_name',
                'parent_designation' => MonthlyStatementData::tableName() . '.parent_designation',
                'designation' => MonthlyStatementData::tableName() . '.designation',
                'designation_id' => MonthlyStatementData::tableName() . '.designation_id',
                'designation_team_lead' => MonthlyStatementData::tableName() . '.designation_team_lead',
                'designation_calc_work_time' => MonthlyStatementData::tableName() . '.designation_calc_work_time',
                'call_center_user_logins' => MonthlyStatementData::tableName() . '.call_center_user_logins',
                'call_center_user_ids' => MonthlyStatementData::tableName() . '.call_center_user_ids',
                'call_center_oper_ids' => MonthlyStatementData::tableName() . '.call_center_oper_ids',
                'call_center_id_oper_id' => MonthlyStatementData::tableName() . '.call_center_id_oper_id',
                'country_names' => MonthlyStatementData::tableName() . '.country_names',
                'salary_local' => MonthlyStatementData::tableName() . '.salary_local',
                'salary_usd' => MonthlyStatementData::tableName() . '.salary_usd',
                'salary_hour_local' => MonthlyStatementData::tableName() . '.salary_hour_local',
                'salary_hour_usd' => MonthlyStatementData::tableName() . '.salary_hour_usd',
                'salary_scheme' => MonthlyStatementData::tableName() . '.salary_scheme',
                'groupbyname' => MonthlyStatementData::tableName() . '.person_id',
                'lunch_time' => MonthlyStatementData::tableName() . '.lunch_time',
                'working_1' => MonthlyStatementData::tableName() . '.working_1',
                'working_2' => MonthlyStatementData::tableName() . '.working_2',
                'working_3' => MonthlyStatementData::tableName() . '.working_3',
                'working_4' => MonthlyStatementData::tableName() . '.working_4',
                'working_5' => MonthlyStatementData::tableName() . '.working_5',
                'working_6' => MonthlyStatementData::tableName() . '.working_6',
                'working_7' => MonthlyStatementData::tableName() . '.working_7',

                'count_normal_hours' => MonthlyStatementData::tableName() . '.count_normal_hours',
                'count_time' => MonthlyStatementData::tableName() . '.count_time',
                'count_extra_hours' => MonthlyStatementData::tableName() . '.count_extra_hours',
                'holiday_worked_time' => MonthlyStatementData::tableName() . '.holiday_worked_time',
                'salary_hand_local_extra' => MonthlyStatementData::tableName() . '.salary_hand_local_extra',
                'salary_hand_usd_extra' => MonthlyStatementData::tableName() . '.salary_hand_usd_extra',
                'salary_hand_local_holiday' => MonthlyStatementData::tableName() . '.salary_hand_local_holiday',
                'salary_hand_usd_holiday' => MonthlyStatementData::tableName() . '.salary_hand_usd_holiday',
                'penalty_sum_local' => MonthlyStatementData::tableName() . '.penalty_sum_local',
                'penalty_sum_usd' => MonthlyStatementData::tableName() . '.penalty_sum_usd',
                'bonus_sum_local' => MonthlyStatementData::tableName() . '.bonus_sum_local',
                'bonus_sum_usd' => MonthlyStatementData::tableName() . '.bonus_sum_usd',
                'salary_hand_local' => MonthlyStatementData::tableName() . '.salary_hand_local',
                'salary_tax_local' => MonthlyStatementData::tableName() . '.salary_tax_local',
                'salary_hand_usd' => MonthlyStatementData::tableName() . '.salary_hand_usd',
                'salary_tax_usd' => MonthlyStatementData::tableName() . '.salary_tax_usd',
                'insurance_coefficient' => Person::tableName() . '.insurance_coefficient',
                'pension_contribution' => Person::tableName() . '.pension_contribution',
            ];
        } else {
            $select = [
                'id' => Person::tableName() . '.id',
                'office' => Office::tableName() . '.name',
                'fio' => Person::tableName() . '.name',
                'start_date' => Person::tableName() . '.start_date',
                'dismissal_date' => Person::tableName() . '.dismissal_date',
                'active' => Person::tableName() . '.active',
                'approved' => Person::tableName() . '.approved',
                'parent_id' => Person::tableName() . '.parent_id',
                'parent_name' => 'parent.name',
                'parent_designation' => 'parent_designation.name',
                'designation' => Designation::tableName() . '.name',
                'designation_id' => Designation::tableName() . '.id',
                'designation_team_lead' => Designation::tableName() . '.team_lead',
                'designation_calc_work_time' => Designation::tableName() . '.calc_work_time',
                'call_center_user_logins' => 'GROUP_CONCAT(DISTINCT ' . CallCenterUser::tableName() . '.user_login)',
                'call_center_user_ids' => 'GROUP_CONCAT(DISTINCT ' . CallCenterUser::tableName() . '.id)',
                'call_center_oper_ids' => 'GROUP_CONCAT(DISTINCT ' . CallCenterUser::tableName() . '.user_id)',
                'call_center_id_oper_id' => 'GROUP_CONCAT(DISTINCT CONCAT(' . CallCenterUser::tableName() . '.callcenter_id,"_",' . CallCenterUser::tableName() . '.user_id))',
                'country_names' => 'GROUP_CONCAT(DISTINCT ' . Country::tableName() . '.name)',
                'salary_local' => Person::tableName() . '.salary_local',
                'salary_usd' => Person::tableName() . '.salary_usd',
                'salary_hour_local' => Person::tableName() . '.salary_hour_local',
                'salary_hour_usd' => Person::tableName() . '.salary_hour_usd',
                'salary_scheme' => Person::tableName() . '.salary_scheme',
                'lunch_time' => WorkingShift::tableName() . '.lunch_time',
                'working_1' => WorkingShift::tableName() . '.working_mon',
                'working_2' => WorkingShift::tableName() . '.working_tue',
                'working_3' => WorkingShift::tableName() . '.working_wed',
                'working_4' => WorkingShift::tableName() . '.working_thu',
                'working_5' => WorkingShift::tableName() . '.working_fri',
                'working_6' => WorkingShift::tableName() . '.working_sat',
                'working_7' => WorkingShift::tableName() . '.working_sun',
                'insurance_coefficient' => Person::tableName() . '.insurance_coefficient',
                'pension_contribution' => Person::tableName() . '.pension_contribution',
            ];
        }

        $select['account_number'] = Person::tableName() . '.account_number';
        $select['passport_id_number'] = Person::tableName() . '.passport_id_number';
        $select['office_id'] = Office::tableName() . '.id';
        $select['office_country_id'] = Office::tableName() . '.country_id';
        $select['working_hours_per_week'] = Office::tableName() . '.working_hours_per_week';
        $select['count_fixed_norm'] = Office::tableName() . '.count_fixed_norm';
        $select['country_id'] = Office::tableName() . '.country_id';
        $select['calc_salary_local'] = Office::tableName() . '.calc_salary_local';
        $select['calc_salary_usd'] = Office::tableName() . '.calc_salary_usd';
        $select['holidays_coefficient'] = Office::tableName() . '.holidays_coefficient';
        $select['extra_days_coefficient'] = Office::tableName() . '.extra_days_coefficient';
        $select['min_salary_local'] = Office::tableName() . '.min_salary_local';
        $select['min_salary_usd'] = Office::tableName() . '.min_salary_usd';
        $select['holiday_hours'] = Office::tableName() . '.holiday_hours';

        $select['penalty_sum_usd'] = 'penalty.penalty_sum_usd';
        $select['penalty_sum_local'] = 'penalty.penalty_sum_local';
        $select['bonus_sum_usd'] = 'bonus.bonus_sum_usd';
        $select['bonus_sum_local'] = 'bonus.bonus_sum_local';
        $select['bonus_percent'] = 'bonus.bonus_percent';
        $select['bonus_hour'] = 'bonus.bonus_hour';
        $select['bonus_percent_hour'] = 'bonus.bonus_percent_hour';
        if (!$this->forceLoadWorkTime) {
            $select['count_time'] = 'SUM(time.count_time)';
            $select['count_calls'] = 'SUM(time.count_calls)';
        }
        $this->query->select($select);

        return $this;
    }


    public function getDateWorkFilter()
    {
        if (is_null($this->dailyFilter)) {
            $this->dailyFilter = new DateWorkFilter();
        }
        return $this->dailyFilter;
    }

    public function getOfficeFilter()
    {
        if (is_null($this->officeFilter)) {
            $this->officeFilter = new OfficeFilter();
        }
        return $this->officeFilter;
    }

    public function getCallCenterMonthlyStatementFilter()
    {
        if (is_null($this->callCenterMonthlyStatementFilter)) {
            $this->callCenterMonthlyStatementFilter = new CallCenterMonthlyStatementFilter([
                'officeId' => $this->officeFilter->office
            ]);
        }
        return $this->callCenterMonthlyStatementFilter;
    }

    public function getCallCenterFilter()
    {
        if (is_null($this->callCenterFilter)) {
            $this->callCenterFilter = new CallCenterFilter([
                'toUser' => true,
                'multiple' => true,
                'label' => 'Направление',
                'officeId' => $this->officeFilter->office
            ]);
        }
        return $this->callCenterFilter;
    }

    public function getTeamLeadFilter()
    {
        if (is_null($this->teamLeadFilter)) {
            $this->teamLeadFilter = new TeamLeadFilter([
                'officeId' => $this->officeFilter->office
            ]);
        }
        return $this->teamLeadFilter;
    }


    public function getDesignationFilter()
    {
        if (is_null($this->designationFilter)) {
            $this->designationFilter = new DesignationFilter([
                'officeId' => $this->officeFilter->office
            ]);
        }
        return $this->designationFilter;
    }

    public function getShowHolidays()
    {
        return $this->showHolidays;
    }

    public function getCalcSalaryLocal()
    {
        return $this->calcSalaryLocal;
    }

    public function getCalcSalaryUSD()
    {
        return $this->calcSalaryUSD;
    }

    /**
     * @param array $params
     * @return ReportForm
     */
    protected function applyFilters($params)
    {
        $this->getOfficeFilter()->apply($this->query, $params);
        $this->getCallCenterFilter()->apply($this->query, $params);
        $this->getTeamLeadFilter()->apply($this->query, $params);
        $this->getDesignationFilter()->apply($this->query, $params);
        return $this;
    }

    /**
     * Считает часы из строки вида 10:00-19:00
     * @param string $time
     * @param float $lunch
     * @return float|int
     */
    public static function calcHours($time, $lunch = null)
    {
        if ($time == '') {
            return 0;
        }
        preg_match('/(\d+):(\d+)\D*(\d+):(\d+)/', $time, $matches);
        list(, $startHour, $startMin, $endHour, $endMin) = $matches;
        if ($endHour < $startHour) {
            $endHour = 24 + $endHour;
        }
        $totalMinutes = ($endHour * 60 + $endMin - ($startHour * 60 + $startMin));
        $return = $totalMinutes / 60;
        if ($lunch) {
            $return -= $lunch;
        }
        return $return;
    }

    /**
     * Сколько часов вообще рабочих в этом интервале
     * @param $model
     * @param $weekDaysNumber
     * @return float|int
     */
    public function calcHoursInPeriod($model, $weekDaysNumber, $freeDays)
    {
        $return = 0;
        // время обеда
        $lunchTime = $this->calcHours($model['lunch_time']);
        $n = 1;
        // по дням недели
        while ($n <= 7) {
            // сколько должен был отработать в этот день часов
            $hours = $this->calcHours($model['working_' . $n], $lunchTime);
            // число недель
            $w = $weekDaysNumber[$n] ?? 0;
            if (isset($freeDays[$n]) && $freeDays[$n]) {
                // уменьшаем на число праздников попавших на этот день недели
                $w -= $freeDays[$n];
            }
            if ($w) {
                // норма часов в интервале сумма часов в день на число недель
                $return += $hours * $w;
            }
            $n++;
        }
        return $return;
    }

    /**
     * Сколько часов вообще рабочих в этом интервале
     * @param $model
     * @param $weekDaysNumber
     * @return float|int
     */
    public function calcDaysInPeriod($model, $weekDaysNumber, $freeDays)
    {
        $return = 0;
        // время обеда
        $lunchTime = $this->calcHours($model['lunch_time']);
        $n = 1;
        // по дням недели
        while ($n <= 7) {
            // сколько должен был отработать в этот день часов
            $hours = $this->calcHours($model['working_' . $n], $lunchTime);
            // число недель
            $w = $weekDaysNumber[$n] ?? 0;
            if (isset($freeDays[$n]) && $freeDays[$n]) {
                // уменьшаем на число праздников попавших на этот день недели
                $w -= $freeDays[$n];
            }
            if ($w && $hours) {
                // норма часов в интервале сумма часов в день на число недель
                $return += $w;
            }
            $n++;
        }
        return $return;
    }


    /**
     * Считает разницу дней
     * @param string $from
     * @param string $to
     * @return int
     */
    public static function calcDays($from, $to)
    {
        return round((strtotime($to) - strtotime($from)) / 60 / 60 / 24) + 1;
    }

    /**
     * @param array $params
     * @return ReportForm
     */
    protected function applyOrderFilters($params)
    {
        $params['s']['type'] = DateFilter::TYPE_CALL_CENTER_APPROVED_AT;
        $this->getDateFilter()->apply($this->query, $params);
        return $this;
    }

    /**
     * @return integer
     */
    public function getTotalCount()
    {
        return $this->totalCount;
    }


    /**
     * Для тим лидом считает часы в праздники если есть время отработанное его подчиненными
     * @param array $model
     * @param array $holidaysDates
     * @return integer
     */
    public function calcTeamLeadHolidays($model, $holidaysDates)
    {
        $myTeamHolidaysWorkedDays = ArrayHelper::getColumn(CallCenterWorkTime::find()
            ->select(['date'])
            ->distinct()
            ->leftJoin(CallCenterUser::tableName(), CallCenterUser::tableName() . '.id=' . CallCenterWorkTime::tableName() . '.call_center_user_id')
            ->leftJoin(Person::tableName(), Person::tableName() . '.id=' . CallCenterUser::tableName() . '.person_id')
            ->where([Person::tableName() . '.parent_id' => $model['id']])
            ->andWhere([CallCenterWorkTime::tableName() . '.date' => $holidaysDates])
            ->asArray()
            ->all(),
            'date'
        );

        $lunchTime = $this->calcHours($model['lunch_time']);

        $myExtraHolidayHours = 0;
        foreach ($myTeamHolidaysWorkedDays as $date) {
            $n = date('N', strtotime($date));
            $myExtraHolidayHours += $this->calcHours($model['working_' . $n], $lunchTime);
        }

        return $myExtraHolidayHours;
    }

    /**
     * кол-во каждого дня недели в периоде
     * @param string $date
     * @param string $end
     * @return array
     */
    public function calcWeekDaysNumber($date, $end)
    {
        $return = [];
        while (strtotime($date) <= strtotime($end)) {
            $n = date('N', strtotime($date));
            $return[$n] = $return[$n] ?? 0;
            $return[$n]++;
            $date = date("Y-m-d", strtotime("+1 day", strtotime($date)));
        }
        return $return;
    }

    /**
     * Сумма с учетом налогов по офису
     *
     * @param $sum float
     * @param $officeId integer
     * @param null $insuranceCoefficient
     * @param null $pensionContribution
     * @param null $dateTo
     * @return float
     */
    public function getTaxSum($sum, $officeId, $insuranceCoefficient = null, $pensionContribution, $dateTo = null)
    {
        $return = [];
        if (!$this->taxRates) {
            $q = OfficeTax::find()
                ->select([
                    'id' => OfficeTax::tableName() . '.id',
                    'office_id',
                    'tax_type',
                    'sum_from',
                    'sum_to',
                    'type' => OfficeTax::tableName() . '.type',
                    'tax_type' => OfficeTax::tableName() . '.tax_type',
                    'tax_type_sort' => 'IF (' . OfficeTax::tableName() . '.tax_type' . ' IN ("' . OfficeTax::TAX_TYPE_INCOME . '"), 1, 2)',
                    'rate',
                    'amount',
                    'unpaid_amount',
                    'country' => Country::tableName() . '.char_code'
                ])
                ->joinWith('office.country', false)
                ->orderBy(['office_id' => SORT_ASC, 'tax_type_sort' => SORT_ASC])
                ->asArray()->all();
            $this->taxRates = ArrayHelper::index($q, 'id', 'office_id');
        }
        if ($sum && isset($this->taxRates[$officeId])) {

            $salary113 = 0;
            foreach ($this->taxRates[$officeId] as $range) {
                if ((float)$range['sum_from'] <= (float)$sum &&
                    ((float)$range['sum_to'] > (float)$sum || !(float)$range['sum_to'])
                ) {
                    if ($range['country'] == 'CL' && $range['tax_type'] == OfficeTax::TAX_TYPE_INSURANCE && $insuranceCoefficient > 1) {
                        //5. добавить костыль для офисов в чили:
                        //при расчете налога если налог типа "страховой взнос" и страна офиса чили и у сотрудника "коэф. страхового взноса" больше 1

                        $UnitOfAccount = $this->getChiliBankUnitOfAccount($dateTo);
                        if ($UnitOfAccount) {
                            $return[] = $insuranceCoefficient * $UnitOfAccount;
                            continue; // по этой статье налог посчитан
                        }
                    }

                    if ($range['tax_type'] == OfficeTax::TAX_TYPE_PENSION && (float)$pensionContribution) {
                        // если пенсионный взнос и есть чтото у сотрудника, то берем от сотрудника
                        $range['rate'] = (float)$pensionContribution;
                    }

                    if ($range['tax_type'] == OfficeTax::TAX_TYPE_INCOME &&
                        $range['type'] == OfficeTax::TYPE_PERCENT
                    ) {
                        // ЗП с учетом подоходного налога
                        $salary113 = 100 * ($sum - (float)$range['unpaid_amount']) / (100 - $range['rate']);
                        $return[] = $salary113 - $sum;
                    }
                    else {
                        if ($range['type'] == OfficeTax::TYPE_PERCENT) {
                            if ($range['rate'] < 100) {
                                if ($salary113) {
                                    // если есть зп с учетом подоходного, все остальное считаем от нее
                                    $return[] = $salary113 * $range['rate'] / 100;
                                }
                                else {
                                    $return[] = ($sum - (float)$range['unpaid_amount']) * $range['rate'] / (100 - $range['rate']);
                                }
                            }
                        }
                    }
                    if ($range['type'] == OfficeTax::TYPE_FIXED) {
                        $return[] = $range['amount'];
                    }
                }
            }
        }
        return $sum + array_sum($return);
    }


    /**
     * @param array $model
     * @param string $lang
     * @param string $dateFormat
     * @return array
     */
    private function prepareExportData($model, $lang = null, $dateFormat = '')
    {
        $data = [];
        $data[] = [
            'label' => Yii::t('common', 'Дата', [], $lang),
            'value' => Yii::$app->formatter->asDate($this->getDateWorkFilter()->from, $dateFormat) . ' - ' . Yii::$app->formatter->asDate($this->getDateWorkFilter()->to, $dateFormat)
        ];
        $data[] = [
            'label' => Yii::t('common', 'ФИО', [], $lang),
            'value' => $model['fio']
        ];
        $data[] = [
            'label' => Yii::t('common', 'Должность', [], $lang),
            'value' => isset($model['designation']) ? Yii::t('common', $model['designation'], [], $lang) : ''
        ];
        if ($model['start_date']) {
            $data[] = [
                'label' => Yii::t('common', 'Дата приема на работу', [], $lang),
                'value' => Yii::$app->formatter->asDate($model['start_date'], $dateFormat),
            ];
        }
        if ($model['dismissal_date']) {
            $data[] = [
                'label' => Yii::t('common', 'Дата увольнения', [], $lang),
                'value' => Yii::$app->formatter->asDate($model['dismissal_date'], $dateFormat),
            ];
        }
        if (Yii::$app->user->can('view_salary_person_numbers')) {
            $data[] = [
                'label' => Yii::t('common', 'Номер счета', [], $lang),
                'value' => $model['account_number'],
            ];
            $data[] = [
                'label' => Yii::t('common', 'Номер ID/Паспорта', [], $lang),
                'value' => $model['passport_id_number'],
            ];
        }
        $data[] = [
            'label' => Yii::t('common', 'Руководитель', [], $lang),
            'value' => (!empty($model['parent_id']) ? Yii::t('common', $model['parent_designation'], [], $lang) . ': ' . $model['parent_name'] : Yii::t('common', 'Руководитель не задан', [], $lang))
        ];
        $data[] = [
            'label' => Yii::t('common', 'Офис', [], $lang),
            'value' => Yii::t('common', ($model['office'] ?? '-'), [], $lang)
        ];

        $countries = explode(",", $model['country_names']);
        foreach ($countries as &$country) {
            $country = Yii::t('common', $country, [], $lang);
        }
        $data[] = [
            'label' => Yii::t('common', 'Направление', [], $lang),
            'value' => implode(", ", $countries)
        ];
        $data[] = [
            'label' => Yii::t('common', 'Логин', [], $lang),
            'value' => isset($model['call_center_user_logins']) ? str_replace(',', ', ', $model['call_center_user_logins']) : ''
        ];
        if ($this->getCalcSalaryLocal()) {
            $data[] = [
                'label' => Yii::t('common', 'Оклад в местной валюте в месяц', [], $lang),
                'value' => round($model['salary_local'] ?? 0, 2),
            ];
        }
        if ($this->getCalcSalaryUSD()) {
            $data[] = [
                'label' => Yii::t('common', 'Оклад в USD', [], $lang),
                'value' => round($model['salary_usd'] ?? 0, 2),
            ];
        }
        if ($this->getCalcSalaryLocal()) {
            $data[] = [
                'label' => Yii::t('common', 'Оклад в местной валюте в час', [], $lang),
                'value' => round($model['salary_hour_local'] ?? 0, 2),
            ];
        }
        if ($this->getCalcSalaryUSD()) {
            $data[] = [
                'label' => Yii::t('common', 'Оклад в USD в час', [], $lang),
                'value' => round($model['salary_hour_usd'] ?? 0, 2),
            ];
        }
        $data[] = [
            'label' => Yii::t('common', 'Норма часов по КЗоТ', [], $lang),
            'value' => round($model['count_normal_hours'] ?? 0, 2),
        ];
        $data[] = [
            'label' => Yii::t('common', 'Отработанных часов', [], $lang),
            'value' => round($model['count_time'] ?? 0, 2),
        ];
        $data[] = [
            'label' => Yii::t('common', 'Переработка часов', [], $lang),
            'value' => round($model['count_extra_hours'] ?? 0, 2),
        ];
        if ($this->getShowHolidays()) {
            $data[] = [
                'label' => Yii::t('common', 'Праздники часов', [], $lang),
                'value' => round($model['holiday_worked_time'] ?? 0, 2),
            ];
        }
        if ($this->getCalcSalaryLocal()) {
            $data[] = [
                'label' => Yii::t('common', 'ЗП переработка', [], $lang),
                'value' => round($model['salary_hand_local_extra'] ?? 0, 2),
            ];
        }
        if ($this->getCalcSalaryUSD()) {
            $data[] = [
                'label' => Yii::t('common', 'ЗП переработка', [], $lang),
                'value' => round($model['salary_hand_usd_extra'] ?? 0, 2),
            ];
        }
        if ($this->getShowHolidays() && $this->getCalcSalaryLocal()) {
            $data[] = [
                'label' => Yii::t('common', 'ЗП праздники', [], $lang),
                'value' => round($model['salary_hand_local_holiday'] ?? 0, 2),
            ];
        }
        if ($this->getShowHolidays() && $this->getCalcSalaryUSD()) {
            $data[] = [
                'label' => Yii::t('common', 'ЗП праздники', [], $lang),
                'value' => round($model['salary_hand_usd_holiday'] ?? 0, 2),
            ];
        }
        if ($this->getCalcSalaryLocal()) {
            $data[] = [
                'label' => Yii::t('common', 'Штрафы', [], $lang),
                'value' => round($model['penalty_sum_local'] ?? 0, 2),
            ];
        }
        if ($this->getCalcSalaryUSD()) {
            $data[] = [
                'label' => Yii::t('common', 'Штрафы', [], $lang),
                'value' => round($model['penalty_sum_usd'] ?? 0, 2),
            ];
        }
        if ($this->getCalcSalaryLocal()) {
            $data[] = [
                'label' => Yii::t('common', 'Бонусы', [], $lang),
                'value' => round($model['bonus_sum_local'] ?? 0, 2),
            ];
        }
        if ($this->getCalcSalaryUSD()) {
            $data[] = [
                'label' => Yii::t('common', 'Бонусы', [], $lang),
                'value' => round($model['bonus_sum_usd'] ?? 0, 2),
            ];
        }
        if ($this->getCalcSalaryLocal()) {
            $data[] = [
                'label' => Yii::t('common', 'ЗП в местной валюте "на руки"', [], $lang),
                'value' => round($model['salary_hand_local'] ?? 0, 2),
            ];
        }
        if ($this->getCalcSalaryLocal()) {
            $data[] = [
                'label' => Yii::t('common', 'ЗП в местной валюте с учетом налогов', [], $lang),
                'value' => round($model['salary_tax_local'] ?? 0, 2),
            ];
        }
        if ($this->getCalcSalaryUSD()) {
            $data[] = [
                'label' => Yii::t('common', 'ЗП в USD "на руки"', [], $lang),
                'value' => round($model['salary_hand_usd'] ?? 0, 2),
            ];
        }
        if ($this->getCalcSalaryUSD()) {
            $data[] = [
                'label' => Yii::t('common', 'ЗП в USD с учетом налогов', [], $lang),
                'value' => round($model['salary_tax_usd'] ?? 0, 2),
            ];
        }
        $mapStatuses = $this->getMapStatuses();
        foreach ($mapStatuses as $name => $statuses) {

            if ($name == 'total') continue;

            $label = '';
            if ($name == 'approve') $label = 'Подтверждено';
            if ($name == 'buyout') $label = 'Выкуплено';

            $data[] = [
                'label' => Yii::t('common', $label, [], $lang),
                'value' => $model[$name] ?? 0,
            ];
            if (isset($model['vsego']) && $model['vsego']) {
                $data[] = [
                    'label' => Yii::t('common', $label . ', %', [], $lang),
                    'value' => round(100 * $model[$name] / $model['vsego'], 2),
                ];
            }
        }
        $data[] = [
            'label' => Yii::t('common', 'Средний чек апрувленный', [], $lang),
            'value' => round($model['srednij_cek_apruvlennyj'] ?? 0, 2),
        ];
        $data[] = [
            'label' => Yii::t('common', 'Средний чек выкупной', [], $lang),
            'value' => round($model['srednij_cek_vykupnoj'] ?? 0, 2),
        ];
        $data[] = [
            'label' => Yii::t('common', 'Всего звонков', [], $lang),
            'value' => $model['count_calls'] ?? 0,
        ];
        return $data;
    }

    /**
     * @param $model
     * @return bool
     */
    public function exportPersonExcel($model)
    {
        $excel = new \PHPExcel();
        $sheetIndex = 0;
        $sheet = $excel->getSheet($sheetIndex);
        $sheet->setTitle(Yii::t('common', 'Расчет ЗП'));

        $data = $this->prepareExportData($model);

        $row = 1;
        foreach ($data as $line) {
            $sheet->setCellValueByColumnAndRow(0, $row, $line['label']);
            $sheet->setCellValueByColumnAndRow(1, $row, $line['value']);
            $row++;
        }
        $excel->getActiveSheet()->getStyle('A1:A' . $row)->getFont()->setBold(true);

        foreach (range('A', 'B') as $columnID) {
            $excel->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        if ($model['penalties']) {

            $sheetIndex++;
            $sheet = $excel->createSheet($sheetIndex);
            $sheet->setTitle(Yii::t('common', 'Штрафы'));
            $excel->setActiveSheetIndex($sheetIndex);

            $col = 0;
            $row = 1;
            $sheet->setCellValueByColumnAndRow($col++, $row, Yii::t('common', 'Тип штрафа'));
            $sheet->setCellValueByColumnAndRow($col++, $row, Yii::t('common', 'Дата штрафа'));
            $sheet->setCellValueByColumnAndRow($col++, $row, Yii::t('common', 'Комментарий'));
            if ($this->getCalcSalaryUSD()) {
                $sheet->setCellValueByColumnAndRow($col++, $row, Yii::t('common', 'Сумма штрафа в USD'));
            }
            if ($this->getCalcSalaryLocal()) {
                $sheet->setCellValueByColumnAndRow($col++, $row, Yii::t('common', 'Сумма штрафа в местной валюте'));
            }
            $row++;

            foreach ($model['penalties'] as $penalty) {
                $col = 0;
                $sheet->setCellValueByColumnAndRow($col++, $row, (!empty($penalty->penalty_type_id)) ? $penalty->penaltyType->name : '');
                $sheet->setCellValueByColumnAndRow($col++, $row, Yii::$app->formatter->asDate($penalty->date));
                $sheet->setCellValueByColumnAndRow($col++, $row, $penalty->comment);
                if ($this->getCalcSalaryUSD()) {
                    $sheet->setCellValueByColumnAndRow($col++, $row, round($penalty->penalty_sum_usd, 2));
                }
                if ($this->getCalcSalaryLocal()) {
                    $sheet->setCellValueByColumnAndRow($col++, $row, round($penalty->penalty_sum_local, 2));
                }
                $row++;
            }

            $excel->getActiveSheet()->getStyle('A1:F1')->getFont()->setBold(true);

            foreach (range('A', 'D') as $columnID) {
                $excel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }

        }

        if ($model['bonuses']) {

            $sheetIndex++;
            $sheet = $excel->createSheet($sheetIndex);
            $sheet->setTitle(Yii::t('common', 'Бонусы'));
            $excel->setActiveSheetIndex($sheetIndex);

            $col = 0;
            $row = 1;
            $sheet->setCellValueByColumnAndRow($col++, $row, Yii::t('common', 'Тип бонуса'));
            $sheet->setCellValueByColumnAndRow($col++, $row, Yii::t('common', 'Дата бонуса'));
            $sheet->setCellValueByColumnAndRow($col++, $row, Yii::t('common', 'Комментарий'));
            $sheet->setCellValueByColumnAndRow($col++, $row, Yii::t('common', 'Процент'));
            $sheet->setCellValueByColumnAndRow($col++, $row, Yii::t('common', 'Процент часа'));
            $sheet->setCellValueByColumnAndRow($col++, $row, Yii::t('common', 'Час'));
            if ($this->getCalcSalaryUSD()) {
                $sheet->setCellValueByColumnAndRow($col++, $row, Yii::t('common', 'Сумма бонуса в USD'));
            }
            if ($this->getCalcSalaryLocal()) {
                $sheet->setCellValueByColumnAndRow($col++, $row, Yii::t('common', 'Сумма бонуса в местной валюте'));
            }
            $row++;

            foreach ($model['bonuses'] as $bonus) {
                $col = 0;
                $sheet->setCellValueByColumnAndRow($col++, $row, (!empty($bonus->bonus_type_id)) ? $bonus->bonusType->name : '');
                $sheet->setCellValueByColumnAndRow($col++, $row, Yii::$app->formatter->asDate($bonus->date));
                $sheet->setCellValueByColumnAndRow($col++, $row, $bonus->comment);
                $sheet->setCellValueByColumnAndRow($col++, $row, round($bonus->bonus_percent, 2));
                $sheet->setCellValueByColumnAndRow($col++, $row, round($bonus->bonus_percent_hour, 2));
                $sheet->setCellValueByColumnAndRow($col++, $row, round($bonus->bonus_hour, 2));
                if ($this->getCalcSalaryUSD()) {
                    $sheet->setCellValueByColumnAndRow($col++, $row, round($bonus->bonus_sum_usd, 2));
                }
                if ($this->getCalcSalaryLocal()) {
                    $sheet->setCellValueByColumnAndRow($col++, $row, round($bonus->bonus_sum_local, 2));
                }
                $row++;
            }

            $excel->getActiveSheet()->getStyle('A1:G1')->getFont()->setBold(true);

            foreach (range('A', 'G') as $columnID) {
                $excel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }
        }

        if ($model['times']) {
            $sheetIndex++;
            $sheet = $excel->createSheet($sheetIndex);
            $sheet->setTitle(Yii::t('common', 'Табель'));
            $excel->setActiveSheetIndex($sheetIndex);

            $col = 0;
            $weekDays = ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'];
            foreach ($model['dates'] as $date => $isHoliday) {
                $sheet->setCellValueByColumnAndRow($col, 1, Yii::$app->formatter->asDate($date));
                $sheet->setCellValueByColumnAndRow($col, 2, Yii::t('common', $weekDays[date('w', strtotime($date))]));
                $sheet->setCellValueByColumnAndRow($col, 3, round($model['times'][$date] ?? 0, 2));
                $sheet->setCellValueByColumnAndRow($col, 4, round($model['plan'][$date] ?? 0, 2));
                $col++;
            }

            $excel->getActiveSheet()->getStyle('A1:AE1')->getFont()->setBold(true);
            $style = array(
                'alignment' => array(
                    'horizontal' => 'center',
                )
            );
            $sheet->getStyle("A2:Z2")->applyFromArray($style);
            $sheet->getStyle("AA2:AE2")->applyFromArray($style);

            foreach (array_merge(range('A', 'Z'), ['AA', 'AB', 'AC', 'AD', 'AE']) as $columnID) {
                $excel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }
        }

        $excel->setActiveSheetIndex(0);

        $dir = Yii::getAlias("@runtime") . DIRECTORY_SEPARATOR . "salary";
        Utils::prepareDir($dir);

        $filename = $dir . DIRECTORY_SEPARATOR . date('Y-m-d') . '_persons_' . $this->person . '_' . date('Y-m-d', strtotime($this->getDateWorkFilter()->from)) . ' - ' . date('Y-m-d', strtotime($this->getDateWorkFilter()->to)) . ".xlsx";

        $objWriter = \PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        $objWriter->save($filename);

        Yii::$app->response->sendFile($filename);

        return true;
    }

    /**
     * @param $model
     * @param bool $saveFile
     * @return mixed
     */
    public function exportPersonPdf($model, $saveFile = false)
    {
        $data = $this->prepareExportData($model, 'en-US', 'php:m/d/Y');

        $logoPath = Yii::getAlias('@templates') . DIRECTORY_SEPARATOR . InvoiceFinanceBuilder::FOLDER_FILES . DIRECTORY_SEPARATOR . 'logo.png';
        $logoContent = file_get_contents($logoPath);
        $logoBase64 = 'data:image/png;base64,' . base64_encode($logoContent);

        $content = Yii::$app->controller->renderFile(Yii::getAlias('@templates') . DIRECTORY_SEPARATOR . 'salary' . DIRECTORY_SEPARATOR . 'template.php', [
            'data' => $data,
            'penalties' => $model['penalties'] ?? [],
            'bonuses' => $model['bonuses'] ?? [],
            'times' => $model['times'] ?? [],
            'dates' => $model['dates'] ?? [],
            'plan' => $model['plan'] ?? [],
            'logoBase64' => $logoBase64,
            'calcSalaryUSD' => $this->getCalcSalaryUSD(),
            'calcSalaryLocal' => $this->getCalcSalaryLocal(),
            'dateFrom' => $this->getDateWorkFilter()->from,
            'dateTo' => $this->getDateWorkFilter()->to,
            'lang' => 'en-US'
        ]);

        $pdfOptions = [
            'mode' => Pdf::MODE_UTF8,
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'content' => $content,
            'options' => ['title' => Yii::t('common', 'Расчет ЗП', [], 'en-US')],
            'cssInline' => '
                table.head{width: 100%; font-family: "AvantGarde"; font-size: 9pt;}
                table td{vertical-align:top}                                
                table.calendar { width: 100%; font-family: "AvantGarde"; font-size: 9pt; border-collapse: collapse }                
                table.calendar td.wrap { font-size:11px; border:1px solid #000; width: 14%}
                table.calendar td.np { background:#eee;}
                table.calendar th.h { background:#ccc; font-weight:bold; text-align:center; padding:5px; border:1px solid #000; width: 14% }                
                table.day {width: 100%}
                table.day th {font-weight: normal; text-align: right; font-style: italic}
                table.day td {text-align: center;}
                ',
        ];
        $filename = '';
        if ($saveFile) {
            $tempPath = MonthlyStatement::getSalaryFilePath();
            $filePath = MonthlyStatement::getSalaryFilePath($model['id']);
            $filename = Utils::uid();

            $pdfOptions['destination'] = Pdf::DEST_FILE;
            $pdfOptions['filename'] = $filePath . DIRECTORY_SEPARATOR . $filename . '.pdf';
            $pdfOptions['tempPath'] = $tempPath;
        }

        $pdf = new Pdf($pdfOptions);
        $pdf->render();
        if ($saveFile) {
            return $filename . '.pdf';
        } else {
            return true;
        }
    }

    /**
     * смена в которую скорее всего и работал бы человек до даты устройства или после увольнения
     * @param $model
     * @param $dateFrom
     * @param $dateTo
     * @param $weekDaysNumber
     * @param $freeDays
     * @return float|int
     */
    public function getWorkingShiftFromPlan($model, $dateFrom, $dateTo, $weekDaysNumber, $freeDays)
    {
        $defaultWorkingShift = WorkTimePlan::find()->select(['id' => 'working_shift_id', 'count' => 'count(*)'])
            ->byPersonId($model['id'])
            ->andWhere(['>=', 'date', $dateFrom])
            ->andWhere(['<=', 'date', $dateTo])
            ->groupBy(['working_shift_id'])
            ->orderBy(['count' => SORT_DESC])
            ->limit(1)
            ->asArray()
            ->one();
        if ($defaultWorkingShift) {
            if (isset($this->planHoursByWorkingShift[$defaultWorkingShift['id']])) {
                return $this->planHoursByWorkingShift[$defaultWorkingShift['id']];
            }
            $workingShift = WorkingShift::findOne($defaultWorkingShift['id']);
            if ($workingShift) {
                $this->planHoursByWorkingShift[$defaultWorkingShift['id']] = $this->calcHoursInPeriod([
                    'lunch_time' => $workingShift->lunch_time,
                    'working_1' => $workingShift->working_mon,
                    'working_2' => $workingShift->working_tue,
                    'working_3' => $workingShift->working_wed,
                    'working_4' => $workingShift->working_thu,
                    'working_5' => $workingShift->working_fri,
                    'working_6' => $workingShift->working_sat,
                    'working_7' => $workingShift->working_sun,
                ], $weekDaysNumber, $freeDays);
                return $this->planHoursByWorkingShift[$defaultWorkingShift['id']];
            }
        }
        return $this->calcHoursInPeriod($model, $weekDaysNumber, $freeDays);
    }


    /*
     *
     * @param $dateTo string
     * @return bool|float
     */
    public function getChiliBankUnitOfAccount($dateTo)
    {
        $cacheKey = 'chili.bank.unit.account.' . $dateTo;
        $UnitOfAccount = Yii::$app->cache->get($cacheKey);

        if ($UnitOfAccount === false) {

            $tmp = explode('-', $dateTo);
            $months = [
                '01' => 'enero',
                '02' => 'febrero',
                '03' => 'marzo',
                '04' => 'abril',
                '05' => 'mayo',
                '06' => 'junio',
                '07' => 'julio',
                '08' => 'agosto',
                '09' => 'septiembre',
                '10' => 'octubre',
                '11' => 'noviembre',
                '12' => 'diciembre',
            ];

            $domain = 'https://valoruf.cl/valor_uf_' . $tmp[2] . '_' . $months[$tmp[1]] . '_' . $tmp[0] . '.html';
            $client = new Client();
            $client->setTransport('yii\httpclient\CurlTransport');
            $response = $client->createRequest()
                ->setUrl($domain)
                ->setMethod('get')
                ->setOptions([
                    'sslVerifyPeer' => false,
                ])
                ->send();


            if ($response->isOk) {

                $dom = new SimpleHTMLDom();
                $html = $dom->str_get_html($response->content);

                $val = $html->find('.valor_principal', 0);
                if ($val) {
                    $UnitOfAccount = (float)str_replace(',', '.', str_replace('$', '', str_replace('.', '', trim($val->plaintext))));
                    if ($UnitOfAccount) {
                        Yii::$app->cache->set($cacheKey, $UnitOfAccount, 60 * 60 * 24);
                        return $UnitOfAccount;
                    }
                }
                $html->clear();
                unset($html);

            }
        } else {
            return $UnitOfAccount;
        }
        return false;
    }
}