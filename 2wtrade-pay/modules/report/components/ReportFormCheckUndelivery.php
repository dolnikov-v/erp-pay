<?php

namespace app\modules\report\components;

use app\modules\callcenter\models\CallCenter;
use app\modules\order\models\Order;
use app\modules\callcenter\models\CallCenterCheckRequest;
use app\modules\report\components\filters\CallCenterFilter;
use app\modules\report\components\filters\DateFilter;
use app\modules\report\extensions\DataProvider;
use app\modules\report\extensions\Query;
use Yii;

/**
 * Class ReportFormCheckUndelivery
 * @package app\modules\report\components
 */
class ReportFormCheckUndelivery extends ReportForm
{
    /**
     * @var string Группировка по умолчанию
     */
    public $groupBy = self::GROUP_BY_CREATED_AT;


    /**
     * @param array $params
     * @return DataProvider
     */
    public function apply($params)
    {
        $this->load($params);
        $this->query = new Query();
        $this->query->from(CallCenterCheckRequest::tableName());
        $this->query->leftJoin(CallCenter::tableName(),
            CallCenter::tableName() . '.id = ' . CallCenterCheckRequest::tableName() . '.call_center_id');
        $this->query->leftJoin(Order::tableName(),
            Order::tableName() . '.id = ' . CallCenterCheckRequest::tableName() . '.order_id');

        $substatuses = CallCenterCheckRequest::getSubStatusesForCheckUndelivery();

        $this->query->where([
            CallCenter::tableName() . '.country_id' => Yii::$app->user->country->id,
            CallCenterCheckRequest::tableName() . '.status' => CallCenterCheckRequest::STATUS_DONE,
            CallCenterCheckRequest::tableName() . '.type' => CallCenterCheckRequest::TYPE_CHECKUNDELIVERY,
            CallCenterCheckRequest::tableName() . '.foreign_information' => array_keys($substatuses)
        ]);

        foreach ($substatuses as $sub_status_id => $label) {
            $sumCondition = $this->query->buildIfCondition(CallCenterCheckRequest::tableName() . '.foreign_information',
                $sub_status_id, CallCenterCheckRequest::tableName() . '.id', 'NULL');
            $this->query->addCountDistinctCondition($sumCondition, $label);
        }

        $sumCondition = $this->query->buildIfCondition(CallCenterCheckRequest::tableName() . '.foreign_information',
            array_keys($substatuses), CallCenterCheckRequest::tableName() . '.id', 'NULL');
        $this->query->addCountDistinctCondition($sumCondition, Yii::t('common', 'Всего'));

        $this->applyFilters($params);
        $dataProvider = new DataProvider([
            'form' => $this,
            'query' => $this->query,
        ]);

        return $dataProvider;

    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $output = '';

        return $output;
    }

    /**
     * @return CallCenterFilter
     */
    public function getCallCenterFilter()
    {
        if (is_null($this->callCenterFilter)) {
            $this->callCenterFilter = new CallCenterFilter([
                'reportForm' => $this,
                'type' => CallCenterFilter::TYPE_CALL_CENTER_CHECK_REQUEST_CC_ID
            ]);
        }

        return $this->callCenterFilter;
    }

    /**
     * @return DateFilter
     */
    public function getDateFilter()
    {
        if (is_null($this->dateFilter)) {
            $this->dateFilter = new DateFilter();
        }

        return $this->dateFilter;
    }

    /**
     * @param array $params
     * @return $this
     */
    public function applyFilters($params)
    {
        $this->getDateFilter()->apply($this->query, $params);
        $this->getCallCenterFilter()->apply($this->query, $params);
        $this->getProductFilter()->apply($this->query, $params);
        $this->getDeliveryFilter()->apply($this->query, $params);
        $this->getDeliveryPartnerFilter()->apply($this->query, $params);
        return $this;
    }
}
