<?php
namespace app\modules\report\components;

use app\modules\order\models\Order;
use app\models\Country;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\order\models\OrderStatus;
use yii\data\ArrayDataProvider;
use app\modules\report\components\filters\DailyFilter;

/**
 * Class ReportFormDaily
 * @package app\modules\report\components\
 */
class ReportFormDaily extends ReportForm
{
    /**
     * @var DailyFilter
     */
    protected $dailyFilter;

    /**
     * @param array $params
     * @return ArrayDataProvider
     */
    public function apply($params)
    {
        $loadParams = $this->getDailyFilter()->applyParams($params);
        $this->query = Order::find()
            ->joinWith([
                'country',
            ])
            ->select([
                'created' => 'COUNT(1)',
                'country_id' => Country::tableName() . '.id',
                'id' => Country::tableName() . '.id'
            ])
            ->where(['>=', Order::tableName() . '.created_at', $loadParams['timeFrom']])
            ->andWhere(['<=', Order::tableName() . '.created_at', ($loadParams['timeTo'] + 86399)]);
        $this->applyFilters($params);
        $dataOrders = $this->query->asArray()->all();

        $this->query = Order::find()
            ->joinWith([
                'country',
                'callCenterRequest',
            ])
            ->select([
                'approved' => 'COUNT(1)',
                'country_id' => Country::tableName() . '.id',
                'id' => Country::tableName() . '.id'
            ])
            ->where(['>=', CallCenterRequest::tableName() . '.approved_at', $loadParams['timeFrom']])
            ->andWhere(['<=', CallCenterRequest::tableName() . '.approved_at', ($loadParams['timeTo'] + 86399)]);
        $this->applyFilters($params);
        $dataCallCenterApproved = $this->query->asArray()->all();

        $this->query = Order::find()
            ->joinWith([
                'country',
                'callCenterRequest',
            ])
            ->select([
                'sent' => 'COUNT(1)',
                'country_id' => Country::tableName() . '.id',
                'id' => Country::tableName() . '.id'
            ])
            ->where(['>=', CallCenterRequest::tableName() . '.sent_at', $loadParams['timeFrom']])
            ->andWhere(['<=', CallCenterRequest::tableName() . '.sent_at', ($loadParams['timeTo'] + 86399)]);
        $this->applyFilters($params);
        $dataCallCenterSent = $this->query->asArray()->all();

        $this->query = Order::find()
            ->joinWith([
                'country',
                'deliveryRequest',
            ])
            ->select([
                'sent' => 'COUNT(1)',
                'country_id' => Country::tableName() . '.id',
                'id' => Country::tableName() . '.id'
            ])
            ->where(['>=', DeliveryRequest::tableName() . '.sent_at', $loadParams['timeFrom']])
            ->andWhere(['<=', DeliveryRequest::tableName() . '.sent_at', ($loadParams['timeTo'] + 86399)]);
        $this->applyFilters($params);
        $dataDeliverySent = $this->query->asArray()->all();

        $this->query = Order::find()
            ->joinWith([
                'country',
                'deliveryRequest',
            ])
            ->select([
                'error' => 'COUNT(1)',
                'country_id' => Country::tableName() . '.id',
                'id' => Country::tableName() . '.id'
            ])
            ->where(['>=', DeliveryRequest::tableName() . '.created_at', $loadParams['timeFrom']])
            ->andWhere(['<=', DeliveryRequest::tableName() . '.created_at', ($loadParams['timeTo'] + 86399)])
            ->andWhere([DeliveryRequest::tableName() . '.status' => 'error'])
            ->andWhere([Order::tableName() . '.status_id' => OrderStatus::STATUS_DELIVERY_REJECTED]);
        $this->applyFilters($params);
        $dataDeliveryError = $this->query->asArray()->all();

        $dataProvider = new ArrayDataProvider([
            'allModels' => [[
                'created' => $dataOrders[0]['created'],
                'call_approved' => $dataCallCenterApproved[0]['approved'],
                'call_sent' => $dataCallCenterSent[0]['sent'],
                'delivery_sent' => $dataDeliverySent[0]['sent'],
                'delivery_error' => $dataDeliveryError[0]['error'],
                'timeFrom' => $loadParams['timeFrom'],
                'timeTo' => $loadParams['timeTo']
            ]],
        ]);

        return $dataProvider;
    }

    /**
     * @return DailyFilter
     */
    public function getDailyFilter()
    {
        if (is_null($this->dailyFilter)) {
            $this->dailyFilter = new DailyFilter();
        }

        return $this->dailyFilter;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return '';
    }

    /**
     * @param array $params
     * @return $this
     */
    protected function applyFilters($params)
    {
        $this->getCountryFilter()->apply($this->query, $params);

        return $this;
    }
}
