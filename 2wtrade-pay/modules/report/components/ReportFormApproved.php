<?php
namespace app\modules\report\components;

use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
//use app\models\Country;
use yii\data\ArrayDataProvider;
use app\modules\report\components\filters\ApprovedDateFilter;
use Yii;

/**
 * Class ReportFormApproved
 * @package app\modules\report\components\
 */
class ReportFormApproved extends ReportForm
{
    /**
     * @return ApprovedDateFilter
     */
    public function getDateFilter()
    {
        if (is_null($this->dateFilter)) {
            $this->dateFilter = new ApprovedDateFilter();
        }

        return $this->dateFilter;
    }

    /**
     * @param array $params
     * @return ArrayDataProvider
     */
    public function apply($params)
    {
        $this->query = Order::find()
            ->joinWith([
                'callCenterRequest',
                //'country',
            ])
            ->select([
                'approved' => 'count(' . Order::tableName() . '.status_id in (' . join(', ', OrderStatus::getApproveList()) . ') OR NULL)',
                'all' => 'count(' . Order::tableName() . '.status_id in (' . join(', ', OrderStatus::getAllList()) . ') OR NULL)',
                //'country_id' => Country::tableName() . '.id',
               // 'country_name' => Country::tableName() . '.name',
            ]);
        $this->applyFilters($params);
        $this->query->addSelect(['day' => 'FROM_UNIXTIME((' . $this->getDateFilter()->getFilteredAttribute() . '), "%Y-%m-%d")']);
        $this->query->addSelect(['period' => 'FROM_UNIXTIME((' . $this->getDateFilter()->getFilteredAttribute() . '), "%H")',]);
        $this->query->addSelect(['id' => 'FROM_UNIXTIME((' . $this->getDateFilter()->getFilteredAttribute() . '), "%Y-%m-%d")',]);
        $this->query->groupBy([/*Country::tableName() . '.id', */'FROM_UNIXTIME((' . $this->getDateFilter()->getFilteredAttribute() . '), "%Y-%m-%d %H")']);
        $this->query->orderBy(/*Country::tableName() . '.name, ' . */$this->getDateFilter()->getFilteredAttribute() . ' DESC');

        $data = $this->query->asArray()->all();

        $columnsData = [];
        foreach ($data as $row) {
            if (isset($columnsData[/*$row['country_id'] . '_' . */$row['day']])) {
                $columnsData[/*$row['country_id'] . '_' . */$row['day']]['hour_' . $row['period']] += $row['approved'];
                $columnsData[/*$row['country_id'] . '_' . */$row['day']]['hour_leads_' . $row['period']] += $row['all'];
                $columnsData[/*$row['country_id'] . '_' . */$row['day']]['all'] += $row['all'];
            } else {
                $from = Yii::$app->formatter->asTimestamp($this->getDateFilter()->from);
                $to = Yii::$app->formatter->asTimestamp($this->getDateFilter()->to) + 86399;
                while ($to >= $from) {
                    $dateDay = date("Y-m-d", $to);
                    $columnsData[/*$row['country_id'] . '_' . */$dateDay] = [
                        //'country_name' => $row['country_name'],
                        'day' => Yii::$app->formatter->asDate($to),
                        'all' => 0,
                        /*потому что попросили с 8:00 показывать (https://2wtrade-tasks.atlassian.net/browse/NPAY-1933) */
                        //'hour_00' => 0,
                        //'hour_01' => 0,
                        //'hour_02' => 0,
                        //'hour_03' => 0,
                        //'hour_04' => 0,
                        //'hour_05' => 0,
                        //'hour_06' => 0,
                        //'hour_07' => 0,
                        'hour_08' => 0,
                        'hour_09' => 0,
                        'hour_10' => 0,
                        'hour_11' => 0,
                        'hour_12' => 0,
                        'hour_13' => 0,
                        'hour_14' => 0,
                        'hour_15' => 0,
                        'hour_16' => 0,
                        'hour_17' => 0,
                        'hour_18' => 0,
                        'hour_19' => 0,
                        'hour_20' => 0,
                        'hour_21' => 0,
                        'hour_22' => 0,
                        'hour_23' => 0,

                        'hour_leads_08' => 0,
                        'hour_leads_09' => 0,
                        'hour_leads_10' => 0,
                        'hour_leads_11' => 0,
                        'hour_leads_12' => 0,
                        'hour_leads_13' => 0,
                        'hour_leads_14' => 0,
                        'hour_leads_15' => 0,
                        'hour_leads_16' => 0,
                        'hour_leads_17' => 0,
                        'hour_leads_18' => 0,
                        'hour_leads_19' => 0,
                        'hour_leads_20' => 0,
                        'hour_leads_21' => 0,
                        'hour_leads_22' => 0,
                        'hour_leads_23' => 0,
                    ];
                    $to -= 86400;
                }
                $columnsData[/*$row['country_id'] . '_' . */$row['day']]['hour_' . $row['period']] = $row['approved'];
                $columnsData[/*$row['country_id'] . '_' . */$row['day']]['hour_leads_' . $row['period']] = $row['all'];
                $columnsData[/*$row['country_id'] . '_' . */$row['day']]['all'] = $row['all'];
            }
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $columnsData,
        ]);

        return $dataProvider;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return '';
    }

    /**
     * @param array $params
     * @return $this
     */
    protected function applyFilters($params)
    {
        $this->getCountryFilter()->apply($this->query, $params);
        $this->getDateFilter()->apply($this->query, $params);

        return $this;
    }
}
