<?php

namespace app\modules\report\components;

use app\models\Country;
use app\models\Currency;
use app\models\CurrencyRate;
use app\models\CurrencyRateHistory;
use app\modules\delivery\components\charges\ChargesCalculatorAbstract;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryContract;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\models\Order;
use app\modules\order\models\OrderFinancePrediction;
use app\modules\order\models\OrderStatus;
use app\modules\report\components\filters\CountryDeliverySelectMultipleFilter;
use app\modules\report\components\filters\CountrySelectFilter;
use app\modules\report\components\filters\DebtsMonthFilterOld;
use app\modules\report\components\filters\DeliveryFilter;
use app\modules\report\models\InvoiceOrder;
use app\modules\report\models\ReportDeliveryDebtsOld;
use Yii;
use yii\data\ArrayDataProvider;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * Class ReportFormDeliveryDebts
 * @package app\modules\report\components
 */
class ReportFormDeliveryDebtsOld extends ReportForm
{
    /**
     * @var DebtsMonthFilterOld
     */
    public $monthFilter = null;

    /**
     * @var CountrySelectFilter
     */
    public $countrySelectFilter = null;

    /**
     * @param array $params
     * @return ArrayDataProvider
     */
    public function apply($params)
    {
        if (empty($params)) {
            $params[$this->getCountryDeliverySelectMultipleFilter()
                ->formName()]['country_ids'] = [Yii::$app->user->country->id];
        }

        $records = $this->loadRecordsFromTable($params);

        $records = static::calculateTotalsForRecords($records);

        if (isset($params['sort'])) {
            $records = static::sortRecords($records, $params['sort']);
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $records,
            'pagination' => false,
            'sort' => [
                'attributes' => [
                    'month' => [
                        'asc' => [],
                        'desc' => [],
                    ],
                    'end_sum_dollars' => [
                        'asc' => [],
                        'desc' => [],
                    ],
                    'money_received_sum_total_dollars' => [
                        'asc' => [],
                        'desc' => [],
                    ],
                    'buyout_sum_total_dollars' => [
                        'asc' => [],
                        'desc' => [],
                    ],
                    'orders_in_process' => [
                        'asc' => [],
                        'desc' => [],
                    ],
                    'orders_unbuyout' => [
                        'asc' => [],
                        'desc' => [],
                    ],
                ]
            ]
        ]);

        return $dataProvider;
    }

    /**
     * @param array $params
     * @return array
     */
    protected function loadRecordsFromTable($params)
    {
        $this->query = ReportDeliveryDebtsOld::find()
            ->joinWith(['delivery.country'], false)
            ->orderBy([
                Country::tableName() . '.id' => SORT_ASC,
                Delivery::tableName() . '.id' => SORT_ASC,
                ReportDeliveryDebtsOld::tableName() . '.month' => SORT_ASC
            ]);
        $this->applyFilters($params);
        $this->query->andWhere([
            'is not', ReportDeliveryDebtsOld::tableName().'.created_at', null
        ]);

        return ArrayHelper::getColumn($this->query->all(), 'decodedData');
    }

    /**
     * @return Query
     */
    protected static function prepareQuery()
    {
        $query = DeliveryRequest::find()->joinWith([
            'order.financePrediction.invoiceOrder',
            'delivery.country.currency.currencyRate',
            'deliveryContract',
        ], false);
        $query->joinWith([
            'order.financePrediction.priceCodCurrencyRate',
            'order.financePrediction.priceStorageCurrencyRate',
            'order.financePrediction.priceFulfilmentCurrencyRate',
            'order.financePrediction.pricePackingCurrencyRate',
            'order.financePrediction.pricePackageCurrencyRate',
            'order.financePrediction.priceAddressCorrectionCurrencyRate',
            'order.financePrediction.priceDeliveryCurrencyRate',
            'order.financePrediction.priceRedeliveryCurrencyRate',
            'order.financePrediction.priceDeliveryReturnCurrencyRate',
            'order.financePrediction.priceDeliveryBackCurrencyRate',
            'order.financePrediction.priceCodServiceCurrencyRate',
            'order.financePrediction.priceVatCurrencyRate',
        ], false);

        $select = [
            'income' => [],
            'costs' => [],
        ];
        $currencyFields = OrderFinancePrediction::getCurrencyFields();
        $currencyRateRelationMap = OrderFinancePrediction::currencyRateRelationMap();
        foreach (array_keys($currencyFields) as $key) {
            $relationName = $currencyRateRelationMap[$currencyFields[$key]];
            $query->leftJoin("invoice_currency as {$relationName}_temp", "{$relationName}_temp.invoice_id = " . InvoiceOrder::tableName() . ".invoice_id and {$relationName}_temp.currency_id = " . OrderFinancePrediction::tableName() . ".{$currencyFields[$key]}");
            $query->leftJoin(CurrencyRateHistory::tableName() . " as {$relationName}_history", "{$relationName}_history.currency_id = " . OrderFinancePrediction::tableName() . ".{$currencyFields[$key]} AND {$relationName}_history.date = DATE(FROM_UNIXTIME(COALESCE(" . DeliveryRequest::tableName() . '.paid_at,' . DeliveryRequest::tableName() . '.done_at)))');

            $type = OrderFinancePrediction::getFinanceTypeOfField($key) == OrderFinancePrediction::FINANCE_TYPE_INCOME ? 'income' : 'costs';
            $select[$type][] = "IFNULL(" . OrderFinancePrediction::tableName() . ".{$key} / COALESCE({$relationName}_temp.rate, {$relationName}_history.rate,{$relationName}.rate, " . CurrencyRate::tableName() . '.rate), 0)';
        }

        $query->select([
            'month' => 'FROM_UNIXTIME(COALESCE(' . DeliveryRequest::tableName() . '.second_sent_at, ' . DeliveryRequest::tableName() . '.sent_at, ' . DeliveryRequest::tableName() . '.created_at), "%Y-%m-01")',
            'delivery_id' => DeliveryRequest::tableName() . '.delivery_id',
            'contract_id' => DeliveryContract::tableName() . '.id',
            'country_id' => Delivery::tableName() . '.country_id',
            'delivery_name' => Delivery::tableName() . '.name',
            'country_name' => Country::tableName() . '.name',
            'country_slug' => Country::tableName() . '.slug',
            'currency' => Currency::tableName() . '.char_code',
            'orders_in_process' => 'COUNT(' . Order::tableName() . '.status_id IN (' . implode(',', OrderStatus::getProcessDeliveryAndLogisticList()) . ') OR NULL)',
            'orders_unbuyout' => 'COUNT(' . Order::tableName() . '.status_id IN (' . implode(',', OrderStatus::getOnlyNotBuyoutInDeliveryList()) . ') OR NULL)',
            'orders_total' => 'COUNT(' . DeliveryRequest::tableName() . '.id)',
            'orders_buyout' => 'COUNT(' . Order::tableName() . '.status_id IN (' . implode(',', OrderStatus::getBuyoutList()) . ') OR NULL)',
            'orders_money_received' => 'COUNT(' . Order::tableName() . '.status_id IN (' . implode(',', OrderStatus::getOnlyMoneyReceivedList()) . ') OR NULL)',
            'money_received_sum_total_dollars' => "SUM(IF (" . Order::tableName() . '.status_id in (' . implode(',', OrderStatus::getOnlyMoneyReceivedList()) . '), ' . implode('+', $select['income']) . ', 0))',
            'charges_accepted_dollars' => 'SUM(IF (' . Order::tableName() . '.status_id IN (' . implode(',', array_merge(OrderStatus::getOnlyMoneyReceivedList(), OrderStatus::getOnlyNotBuyoutDoneList())) . '), ' . implode('+', $select['costs']) . ', 0))',
            'buyout_sum_total_dollars' => "SUM(IF (" . Order::tableName() . '.status_id in (' . implode(',', OrderStatus::getOnlyBuyoutList()) . '), ' . implode('+', $select['income']) . ', 0))',
            'charges_not_accepted_dollars' => 'SUM(IF (' . Order::tableName() . '.status_id IN (' . implode(',', array_merge(OrderStatus::getOnlyBuyoutList(), OrderStatus::getNotBuyoutInDeliveryProcessList())) . '), ' . implode('+', $select['costs']) . ', 0))',
        ]);
        $query->where([Order::tableName() . '.status_id' => OrderStatus::getInDeliveryStatusList()]);

        $query->groupBy(['delivery_id', 'month', 'contract_id']);
        $query->orderBy(['country_id' => SORT_ASC, 'delivery_id' => SORT_ASC, 'month' => SORT_ASC]);
        return $query;
    }

    /**
     * @param Query $query
     * @return array
     */
    protected static function getRecordsFromQuery($query)
    {
        $currencyFields = OrderFinancePrediction::getCurrencyFields();
        $tempRecords = ArrayHelper::index($query->createCommand()->queryAll(), null, ['delivery_id', 'month']);
        static $contracts = null;
        if (is_null($contracts)) {
            $contracts = DeliveryContract::find()->with(['chargesCalculatorModel'])->indexBy('id')->all();
        }
        $records = [];
        foreach ($tempRecords as $months) {
            foreach ($months as $contractsData) {
                $monthData = [];
                foreach ($contractsData as $data) {
                    if ($data['contract_id']) {
                        $contract = $contracts[$data['contract_id']];
                        if ($contract->chargesCalculatorModel) {
                            foreach ($currencyFields as $fieldName => $currencyField) {
                                $type = $contract->chargesCalculator->getCalculatingTypeForField($fieldName);
                                if ($type["type"] == ChargesCalculatorAbstract::CALCULATING_TYPE_MONTHLY_CHARGE) {
                                    $data['charges_accepted_dollars'] += $type['charge'] / $type['currency_rate'];
                                }
                            }
                        }
                    }
                    if (empty($monthData)) {
                        $monthData = $data;
                    } else {
                        $sumFields = [
                            'orders_in_process',
                            'orders_unbuyout',
                            'orders_total',
                            'orders_buyout',
                            'orders_money_received',
                            'money_received_sum_total_dollars',
                            'charges_accepted_dollars',
                            'buyout_sum_total_dollars',
                            'charges_not_accepted_dollars',
                        ];
                        foreach ($sumFields as $fieldName) {
                            $monthData[$fieldName] += $data[$fieldName];
                        }
                    }
                }

                $monthData['sum_total_dollars'] = $monthData['buyout_sum_total_dollars'] + $monthData['money_received_sum_total_dollars'];
                $monthData['charges_total_dollars'] = $monthData['charges_not_accepted_dollars'] + $monthData['charges_accepted_dollars'];
                $monthData['end_sum_dollars'] = $monthData['sum_total_dollars'] - $monthData['charges_total_dollars'];
                $records[] = $monthData;
            }
        }
        return $records;
    }

    /**
     * @param array $records
     * @return array
     */
    protected static function calculateTotalsForRecords($records)
    {
        $columnsForCalculatingTotals = [
            'end_sum_dollars',
            'charges_not_accepted_dollars',
            'charges_accepted_dollars',
            'orders_in_process',
            'orders_unbuyout',
            'orders_buyout',
            'orders_total',
            'orders_money_received',
            'buyout_sum_total_dollars',
            'money_received_sum_total_dollars',
        ];
        $countryTotal = $deliveryTotal = [];
        foreach ($records as $record) {
            foreach ($columnsForCalculatingTotals as $column) {
                $countryTotal[$record['country_id']]["total_country_{$column}"] = ($countryTotal[$record['country_id']]["total_country_{$column}"] ?? 0) + $record[$column];
                $deliveryTotal[$record['delivery_id']]["total_delivery_{$column}"] = ($deliveryTotal[$record['delivery_id']]["total_delivery_{$column}"] ?? 0) + $record[$column];
            }
            if (!isset($countryTotal[$record['country_id']]['total_country_min_month']) || $countryTotal[$record['country_id']]['total_country_min_month'] > $record['month']) {
                $countryTotal[$record['country_id']]['total_country_min_month'] = $record['month'];
            }
            if (!isset($deliveryTotal[$record['delivery_id']]['total_delivery_min_month']) || $deliveryTotal[$record['delivery_id']]['total_delivery_min_month'] > $record['month']) {
                $deliveryTotal[$record['delivery_id']]['total_delivery_min_month'] = $record['month'];
            }

            if (!isset($countryTotal[$record['country_id']]['total_country_max_month']) || $countryTotal[$record['country_id']]['total_country_max_month'] < $record['month']) {
                $countryTotal[$record['country_id']]['total_country_max_month'] = $record['month'];
            }
            if (!isset($deliveryTotal[$record['delivery_id']]['total_delivery_max_month']) || $deliveryTotal[$record['delivery_id']]['total_delivery_max_month'] < $record['month']) {
                $deliveryTotal[$record['delivery_id']]['total_delivery_max_month'] = $record['month'];
            }
        }

        foreach ($records as $key => $record) {
            $records[$key] = array_merge($countryTotal[$record['country_id']], $deliveryTotal[$record['delivery_id']], $record);
        }

        return $records;
    }

    /**
     * @param array $records
     * @param string $sortKey
     * @param string $defaultSortKey
     * @return array
     */
    public static function sortRecords($records, $sortKey, $defaultSortKey = 'month')
    {
        $prefix = substr($sortKey, 0, 1) == '-';
        if ($prefix) {
            $sortKey = substr($sortKey, 1);
        }
        if (in_array($sortKey, [
            'end_sum_dollars',
            'money_received_sum_total_dollars',
            'buyout_sum_total_dollars',
            'orders_in_process',
            'orders_unbuyout',
        ])) {
            $totalKeyType = 'sum';
        } else {
            $totalKeyType = 'maxValue';
        }

        $countrySum = [];
        $deliverySum = [];
        foreach ($records as $key => $record) {
            if ($totalKeyType == 'sum') {
                if (!isset($countrySum[$record['country_id']])) {
                    $countrySum[$record['country_id']] = 0;
                }
                if (!isset($deliverySum[$record['delivery_id']])) {
                    $deliverySum[$record['delivery_id']] = 0;
                }

                $countrySum[$record['country_id']] += $record[$sortKey];
                $deliverySum[$record['delivery_id']] += $record[$sortKey];
            } else {
                if (!isset($countrySum[$record['country_id']]) || (($countrySum[$record['country_id']] > $record[$sortKey]) ^ $prefix)) {
                    $countrySum[$record['country_id']] = $record[$sortKey];
                }
                if (!isset($deliverySum[$record['delivery_id']]) || (($deliverySum[$record['delivery_id']] > $record[$sortKey]) ^ $prefix)) {
                    $deliverySum[$record['delivery_id']] = $record[$sortKey];
                }
            }
        }

        usort($records, function ($itemA, $itemB) use ($countrySum, $deliverySum, $prefix, $sortKey, $defaultSortKey) {
            $answer = 0;
            if ($itemA['country_id'] != $itemB['country_id']) {
                if ($countrySum[$itemA['country_id']] == $countrySum[$itemB['country_id']]) {
                    $answer = $itemA['country_id'] < $itemB['country_id'] ? -1 : 1;
                } else {
                    $answer = ($countrySum[$itemA['country_id']] < $countrySum[$itemB['country_id']]) ? -1 : 1;
                }
            } else {
                if ($itemA['delivery_id'] != $itemB['delivery_id']) {
                    if ($deliverySum[$itemA['delivery_id']] == $deliverySum[$itemB['delivery_id']]) {
                        $answer = $itemA['delivery_id'] < $itemB['delivery_id'] ? -1 : 1;
                    } else {
                        $answer = ($deliverySum[$itemA['delivery_id']] < $deliverySum[$itemB['delivery_id']]) ? -1 : 1;
                    }
                } else {
                    if ($itemA[$sortKey] != $itemB[$sortKey]) {
                        $answer = $itemA[$sortKey] < $itemB[$sortKey] ? -1 : 1;
                    } elseif ($itemA[$defaultSortKey] != $itemB[$defaultSortKey]) {
                        $answer = $itemA[$defaultSortKey] < $itemB[$defaultSortKey] ? -1 : 1;
                    }
                }
            }

            return $prefix ? -1 * $answer : $answer;
        });

        return $records;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return '';
    }

    /**
     * @param array $params
     * @return $this
     */
    public function applyFilters($params)
    {
        $this->getMonthFilter()->apply($this->query, $params);
        $this->getCountryDeliverySelectMultipleFilter()->apply($this->query, $params);
        return $this;
    }

    /**
     * @param $query
     * @param $params
     * @param $groupByMonth
     * @return $this
     */
    public function applyFiltersForQuery($query, $params, $groupByMonth = true)
    {
        if ($groupByMonth) {
            $this->getMonthFilter()->apply($query, $params);
        }
        $this->getCountryDeliverySelectMultipleFilter()->apply($query, $params);
        return $this;
    }

    /**
     * @return DebtsMonthFilterOld
     */
    public function getMonthFilter()
    {
        if (is_null($this->monthFilter)) {
            $this->monthFilter = new DebtsMonthFilterOld([
                'from' => Yii::$app->formatter->asMonth(strtotime(' - 1month', time())),
                'to' => Yii::$app->formatter->asMonth(time())
            ]);
        }
        return $this->monthFilter;
    }

    /**
     * @return CountryDeliverySelectMultipleFilter
     */
    public function getCountryDeliverySelectMultipleFilter()
    {
        if (is_null($this->countryDeliverySelectMultipleFilter)) {
            $this->countryDeliverySelectMultipleFilter = new CountryDeliverySelectMultipleFilter([
                'deliveryType' => CountryDeliverySelectMultipleFilter::DELIVERY_TYPE_DELIVERY,
            ]);
        }

        return $this->countryDeliverySelectMultipleFilter;
    }

    /**
     * @return DeliveryFilter
     */
    public function getDeliveryFilter()
    {
        if (is_null($this->deliveryFilter)) {
            $this->deliveryFilter = new DeliveryFilter([
                'type' => DeliveryFilter::TYPE_DELIVERY,
                'reportForm' => $this
            ]);
        }
        return $this->deliveryFilter;
    }

    /**
     * Подготовка данных и сохранение их в таблицу ReportDeliveryDebtsOld
     * @param integer $deliveryId
     * @param integer $countryId
     */
    public static function prepareData($deliveryId = null, $countryId = null)
    {
        $oldTime = ReportDeliveryDebtsOld::find()->select('created_at')->scalar();
        $queryDeliveries = Delivery::find()
            ->with(['deliveryContracts.chargesCalculatorModel'])
            ->orderBy(['country_id' => SORT_ASC, 'id' => SORT_ASC]);
        if (!is_null($deliveryId)) {
            $queryDeliveries->andWhere(['id' => $deliveryId]);
        }
        if (!is_null($countryId)) {
            $queryDeliveries->andWhere(['country_id' => $countryId]);
        }

        $deliveries = $queryDeliveries->all();

        foreach ($deliveries as $delivery) {
            $query = static::prepareQuery();
            $query->andWhere([Delivery::tableName() . '.id' => $delivery->id]);
            $records = static::getRecordsFromQuery($query);
            static::saveRecordsToTable($records);
        }
        if ($oldTime) {
            ReportDeliveryDebtsOld::deleteAll([
                'created_at' => $oldTime
            ]);
        }
        ReportDeliveryDebtsOld::updateAll([
            'created_at' => time(),
        ]);
    }

    /**
     * @param array $records
     */
    protected static function saveRecordsToTable($records)
    {
        if ($records) {
            $records = array_map(function ($item) {
                return [
                    'delivery_id' => $item['delivery_id'],
                    'month' => $item['month'],
                    'data' => json_encode($item, JSON_UNESCAPED_UNICODE),
                ];
            }, $records);

            Yii::$app->db->createCommand()->batchInsert(ReportDeliveryDebtsOld::tableName(), [
                'delivery_id',
                'month',
                'data'
            ], $records)->execute();
        }
    }
}
