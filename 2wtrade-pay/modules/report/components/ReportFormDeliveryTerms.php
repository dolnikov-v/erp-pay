<?php
namespace app\modules\report\components;

use app\modules\callcenter\models\CallCenterRequest;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\delivery\models\DeliveryZipcodes;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use app\modules\report\components\filters\DateFilter;
use app\modules\report\components\filters\WebmasterFilter;
use app\modules\report\extensions\Query;
use Yii;
use yii\data\ArrayDataProvider;
use yii\db\Expression;
use app\widgets\Panel;

/**
 * Class ReportFormDeliveryTerms
 * @package app\modules\report\components
 */
class ReportFormDeliveryTerms extends ReportForm
{

    /**
     * @var string Группировка по умолчанию
     */
    public $groupBy = self::GROUP_BY_CUSTOMER_ZIP;


    /**
     * @var WebmasterFilter
     */
    protected $webmasterFilter;


    /**
     * @var array Маппинг статусов
     */
    protected $mapStatuses = [];

    /**
     * @param array $params
     * @return ArrayDataProvider
     */
    public function apply($params)
    {
        $this->load($params);

        $this->query = new Query();
        $this->query->from([Order::tableName()]);
        $this->query->leftJoin(DeliveryRequest::tableName(), DeliveryRequest::tableName() . '.order_id =  ' . Order::tableName() . '.id');

        if (isset($params['s']['type']) && in_array($params['s']['type'], [DateFilter::TYPE_CALL_CENTER_SENT_AT, DateFilter::TYPE_CALL_CENTER_APPROVED_AT])) {
            $this->query->leftJoin(CallCenterRequest::tableName(), CallCenterRequest::tableName() . '.order_id =  ' . Order::tableName() . '.id');
        }

        $this->buildSelect()
            ->applyFilters($params);

        $deliveryZipcodes = [];
        if ($this->deliveryFilter->delivery) {

            $orderDaysData = $this->query->all();

            $deliveryZipcodes = DeliveryZipcodes::find()
                ->byDeliveryId($this->deliveryFilter->delivery)
                ->asArray()
                ->all();

            $oldGroup = sizeof($deliveryZipcodes);

            $deliveryZipcodes[$oldGroup] = [
                'id' => 'old',
                'name' => 'Старое распределение',
                'max_term_days' => DeliveryZipcodes::OLD_MAX_TERM_DAYS,
                'zipcodes' => ''
            ];

            foreach ($deliveryZipcodes as &$deliveryZipcode) {
                $zipCodes = array_flip(explode(",", $deliveryZipcode['zipcodes']));
                $deliveryZipcode['days']['clarification'] = 0;
                $deliveryZipcode['total'] = 0;

                foreach ($orderDaysData as $key => $data) {
                    if (isset($zipCodes[$data['zip']]) || ($oldGroup == 1 &&  trim($deliveryZipcode['zipcodes']) == '')) {
                        //zipкод входит в группу или группа всего одна и она пуста (т.е. по всей стране)
                        if ($data['clarification']) {
                            //отправили на уточнение
                            $deliveryZipcode['days']['clarification'] += $data['cnt'];
                        } else {
                            //на уточнение не отправляли
                            if (!isset($deliveryZipcode['days'][$data['days']])) {
                                //число дней не превысило максимальное
                                $deliveryZipcode['days'][$data['days']] = $data['cnt'];
                            } else
                            $deliveryZipcode['days'][$data['days']] += $data['cnt'];
                        }
                        $deliveryZipcode['total'] += $data['cnt'];
                        unset($orderDaysData[$key]); //заказ пристроили в группу
                    }
                }
            }

            if (sizeof($orderDaysData) > 0) {
                //и есть заказы не попавшие ни в одну группу, то все такие заказы определим в Старое распределение
                foreach ($orderDaysData as $key => $data) {
                    if ($data['clarification']) {
                        //отправили на уточнение
                        $deliveryZipcodes[$oldGroup]['days']['clarification'] += $data['cnt'];
                    } else {
                        //на уточнение не отправляли
                        if (!isset($deliveryZipcodes[$oldGroup]['days'][$data['days']])) {
                            $deliveryZipcodes[$oldGroup]['days'][$data['days']] = $data['cnt'];
                        }
                        $deliveryZipcodes[$oldGroup]['days'][$data['days']] += $data['cnt'];
                    }
                    $deliveryZipcodes[$oldGroup]['total'] += $data['cnt'];
                }
            } else {
                unset($deliveryZipcodes[$oldGroup]);
            }

            foreach ($deliveryZipcodes as &$deliveryZipcode) {
                $tmp = $deliveryZipcode['days']['clarification'];
                unset($deliveryZipcode['days']['clarification']);
                $deliveryZipcode['days']['clarification'] = $tmp;
            }


        } else {
            $this->panelAlert = Yii::t('common', 'Выберите службу доставки');
            $this->panelAlertStyle = Panel::ALERT_DANGER;
        }
        $dataProvider = new ArrayDataProvider([
            'allModels' => $deliveryZipcodes
        ]);

        return $dataProvider;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $output = '';

        return $output;
    }

    /**
     * @inheritdoc
     */
    protected function buildSelect()
    {
        $statuses = $this->getMapStatuses();

        $this->query->select([
            'cnt' => 'COUNT(*)',
            'zip' => Order::tableName() . '.customer_zip',
            'days' => new Expression('CEILING((' . strtotime('today midnight UTC') . ' - ' . Order::tableName() . '.delivery_time_from)/86400)'),
            'clarification' => new Expression('IF(' . DeliveryRequest::tableName() . '.sent_clarification_at,1,0)')
        ]);
        $this->query->where(['is not', Order::tableName() . '.delivery_time_from', null]);
        $this->query->andWhere(['is not', Order::tableName() . '.customer_zip', null]);
        $this->query->andWhere(['<>', Order::tableName() . '.customer_zip', '']);
        $this->query->andWhere(['in', Order::tableName() . '.status_id',
            $statuses['in_process']
        ]);
        $this->query->addGroupBy(
            [
                'zip',
                'days',
                'clarification'
            ]
        );
        $this->query->having(['>', 'days', 0]);
        $this->query->orderBy(['days' => SORT_ASC]);
        return $this;
    }

    /**
     * @return WebmasterFilter
     */
    public function getWebmasterFilter()
    {
        if (is_null($this->webmasterFilter)) {
            $this->webmasterFilter = new WebmasterFilter();
        }

        return $this->webmasterFilter;
    }

    /**
     * @param array $params
     * @return ReportForm
     */
    protected function applyFilters($params)
    {
        $this->getDeliveryFilter()->apply($this->query, $params);
        $this->getProductFilter()->apply($this->query, $params);
        $this->getWebmasterFilter()->apply($this->query, $params);
        $this->applyBaseFilters($params);

        return $this;
    }

    /**
     * @return array
     */
    public function getMapStatuses()
    {
        if (empty($this->mapStatuses)) {
            $this->mapStatuses = [
                'in_process' => OrderStatus::getProcessDeliveryAndLogisticList(),
            ];
        }

        return $this->mapStatuses;
    }
}
