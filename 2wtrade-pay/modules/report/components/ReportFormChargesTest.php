<?php
namespace app\modules\report\components;

use app\models\Country;
use app\modules\order\models\Order;
use app\modules\report\components\filters\ContractFilter;
use app\modules\report\components\filters\DateFilter;
use app\modules\report\components\filters\OrderNumberFilter;
use app\modules\report\extensions\DataProvider;
use app\modules\report\extensions\Query;
use yii\data\ActiveDataProvider;

/**
 * Class ReportFormChargesTest
 * @package app\modules\report\components\
 */
class ReportFormChargesTest extends ReportForm
{

    const ORDER_LIMIT = 1000;
    
    /**
     * @var ContractFilter;
     */
    protected $contractFilter;

    /**
     * @var OrderNumberFilter;
     */
    protected $orderNumberFilter;

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function apply($params)
    {
        if (empty($params)) {
            $params[$this->formName()]['country_ids'] = [\Yii::$app->user->getCountry()->id];
        }

        $this->getDateFilter()->type = DateFilter::TYPE_DELIVERY_SENT_AT_NOT_EMPTY;

        $this->query = Order::find()
            ->joinWith('country', false)
            ->joinWith('deliveryRequest', false);
        $this->applyFilters($params);
        $this->query->limit(self::ORDER_LIMIT);
        $dataProvider = new ActiveDataProvider([
            'query' => $this->query,
        ]);

        return $dataProvider;
    }

    /**
     * @return ReportForm
     */
    protected function buildSelect()
    {
        $this->query->select([
            'id' => Order::tableName() . '.id',
            'status_id' => Order::tableName() . '.status_id',
            'customer_zip' => Order::tableName() . '.customer_zip',
            'customer_city' => Order::tableName() . '.customer_city',
            'customer_province' => Order::tableName() . '.customer_province',
            'price_total' => Order::tableName() . '.price_total',
        ]);

        return $this;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return '';
    }

    /**
     * @return ContractFilter
     */
    public function getContractFilter()
    {
        if (is_null($this->contractFilter)) {
            $this->contractFilter = new ContractFilter(['reportForm' => $this]);
        }
        return $this->contractFilter;
    }

    /**
     * @return OrderNumberFilter
     */
    public function getOrderNumberFilter()
    {
        if (is_null($this->orderNumberFilter)) {
            $this->orderNumberFilter = new OrderNumberFilter(['reportForm' => $this]);
        }
        return $this->orderNumberFilter;
    }

    /**
     * @param array $params
     * @return ReportForm
     */
    protected function applyFilters($params)
    {
        $this->getDateFilter()->apply($this->query, $params);
        $this->getCountryDeliverySelectMultipleFilter()->apply($this->query, $params);
        $this->getContractFilter()->apply($this->query, $params);
        $this->getOrderStatusFilter()->apply($this->query, $params);
        $this->getOrderNumberFilter()->apply($this->query, $params);
        return $this;
    }
}
