<?php

namespace app\modules\report\components;

use app\models\Country;

use app\models\Product;
use app\modules\callcenter\models\CallCenter;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\callcenter\models\CallCenterRequestCallData;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\models\LeadProduct;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use app\modules\report\components\filters\CallCenterFilter;
use app\modules\report\components\filters\CountrySelectFilter;
use app\modules\report\components\filters\DateFilter;
use app\modules\report\components\filters\LeadProductFilter;
use app\modules\report\components\filters\ProductFilter;
use app\modules\report\components\filters\WebmasterNameFilter;
use app\modules\report\extensions\Query;
use Yii;
use yii\data\ArrayDataProvider;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * Class ReportFormWebmaster
 * @package app\modules\report\components
 */
class ReportFormWebmasterAnalytics extends ReportForm
{
    const MAX_CALLS = 9;

    /**
     * @var WebmasterNameFilter
     */
    protected $webmasterFilter;

    /**
     * @var array Маппинг статусов
     */
    protected $mapStatuses = [];

    /**
     * @var array
     */
    public $numberCalls = [];

    /**
     * @var array
     */
    public $models = [];

    /**
     * @var int
     */
    public $totalCount = 0;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->groupByCollection[self::GROUP_BY_WEBMASTER_IDENTIFIER] = Yii::t('common', 'Идентификатор вебмастера');
    }

    private function buildCaseStatus()
    {

        $lines = [];
        foreach ($this->getMapStatuses() as $name => $statuses) {
            $lines[] = 'WHEN `order`.status_id IN(' . implode(",", $statuses) . ') THEN "' . $name . '"';
        }

        $this->query->addSelect([
            'status' => 'CASE
                ' . implode(" ", $lines) . '
                ELSE "other"
            END'
        ]);
    }

    /**
     * @param $params
     * @throws \yii\web\HttpException
     */
    private function setQuery($params)
    {
        if (empty($params)) {
            $params[$this->formName()]['country_ids'] = [Yii::$app->user->getCountry()->id];
        }
        $this->load($params);

        $this->query = new Query();
        $this->query->from([Order::tableName()]);
        $this->query->select([
            'webmaster_identifier' => Order::tableName() . '.webmaster_identifier',
            'country_name' => Country::tableName() . '.name',
            'country_id' => Country::tableName() . '.id',
            'call_center_name' => CallCenter::tableName() . '.name',
            'call_center_id' => CallCenter::tableName() . '.id',
            'vsego' => 'COUNT(' . Order::tableName() . '.id)',
            'calls' => 'IFNULL(ccrcd.calls_count, 0)',
        ]);
        $this->query->addSelect([
            'lead_product_id' => LeadProduct::find()
                ->select([
                    LeadProduct::tableName() . '.product_id'
                ])
                ->where([LeadProduct::tableName() . '.order_id' => new Expression(Order::tableName() . '.id')])
                ->limit(1)
        ]);
        $this->query->addSelect([
            'lead_product_name' => LeadProduct::find()
                ->joinWith(['product'])
                ->select([
                    Product::tableName() . '.name'
                ])
                ->where([LeadProduct::tableName() . '.order_id' => new Expression(Order::tableName() . '.id')])
                ->limit(1)
        ]);
        $this->buildCaseStatus();

        $this->query->leftJoin(Country::tableName(), Order::tableName() . '.country_id=' . Country::tableName() . '.id');
        $this->query->leftJoin(CallCenterRequest::tableName(), Order::tableName() . '.id=' . CallCenterRequest::tableName() . '.order_id');
        $this->query->leftJoin(DeliveryRequest::tableName(), Order::tableName() . '.id=' . DeliveryRequest::tableName() . '.order_id');
        $this->query->leftJoin(CallCenter::tableName(), CallCenterRequest::tableName() . '.call_center_id=' . CallCenter::tableName() . '.id');
        $this->query->leftJoin(LeadProduct::tableName(), Order::tableName() . '.id=' . LeadProduct::tableName() . '.order_id');
        $this->query->leftJoin(Product::tableName(), LeadProduct::tableName() . '.product_id=' . Product::tableName() . '.id');

        $subQuery = new Query();
        $subQuery->select([
            'calls_count' => 'COUNT(*)',
            'call_center_request_id' => 'call_center_request_id',
        ])
            ->from(CallCenterRequestCallData::tableName())
            ->leftJoin(CallCenterRequest::tableName(), CallCenterRequest::tableName() . '.id = ' . CallCenterRequestCallData::tableName() . '.call_center_request_id')
            ->groupBy(
                CallCenterRequestCallData::tableName() . '.call_center_request_id'
            );
        $this->query->leftJoin(['ccrcd' => $subQuery], CallCenterRequest::tableName() . '.id=ccrcd.call_center_request_id');

        $this->applyFilters($params);
        $this->query->andWhere(['is not', Order::tableName() . '.webmaster_identifier', null]);
        $this->query->groupBy([
            Order::tableName() . '.webmaster_identifier',
            Order::tableName() . '.country_id',
            CallCenterRequest::tableName() . '.call_center_id',
            LeadProduct::tableName() . '.product_id',
            'status',
            'calls',
        ]);
        $this->query->orderBy([
            'webmaster_identifier' => SORT_ASC,
            'country_name' => SORT_ASC,
            'call_center_name' => SORT_ASC,
            'lead_product_name' => SORT_ASC,
        ]);

    }

    /**
     * @param $params
     * @return array
     * @throws \yii\web\HttpException
     */
    private function getModels($params)
    {
        if (!empty($this->models)) {
            return $this->models;
        }
        $this->setQuery($params);

        $this->models = $this->query->all();

        return $this->models;
    }

    /**
     * @param array $params
     * @return \yii\data\ActiveDataProvider|ArrayDataProvider
     * @throws \yii\web\HttpException
     */
    public function apply($params)
    {

        $allModels = [];
        $numberCalls = [];
        $totals = [];
        foreach ($this->getModels($params) as $model) {

            $key = $model['webmaster_identifier'] . $model['country_id'] . $model['call_center_id'] . $model['lead_product_id'];

            if (!isset($allModels[$key])) {
                $allModels[$key] = $model;
            } else {
                $allModels[$key]['vsego'] += $model['vsego'];
            }

            if ($model['calls'] > self::MAX_CALLS) {
                $model['calls'] = self::MAX_CALLS;
            }
            if (!isset($allModels[$key]['call_' . $model['calls']])) {
                $allModels[$key]['call_' . $model['calls']] = 0;
            }

            $allModels[$key]['call_' . $model['calls']] += $model['vsego'];

            if (!isset($totals[$model['webmaster_identifier']])) {
                $totals[$model['webmaster_identifier']] = 0;
            }
            $totals[$model['webmaster_identifier']] += $model['vsego'];

            $numberCalls[$model['calls']] = $model['calls'];

            unset($allModels[$key]['calls']);
            unset($allModels[$key]['status']);
        }

        sort($numberCalls);
        $this->numberCalls = $numberCalls;

        foreach ($allModels as &$model) {
            $model['total'] = $totals[$model['webmaster_identifier']];
        }

        $this->totalCount = array_sum($totals);

        ArrayHelper::multisort($allModels, ['total', 'vsego', 'country_name', 'call_center_name', 'lead_product_name'], [SORT_DESC, SORT_DESC, SORT_ASC, SORT_ASC, SORT_ASC]);

        $dataProvider = new ArrayDataProvider([
            'allModels' => $allModels,
        ]);
        $dataProvider->pagination->pageSize = $dataProvider->pagination->totalCount;

        return $dataProvider;
    }


    /**
     * @param array $params
     * @return \yii\data\ActiveDataProvider|ArrayDataProvider
     * @throws \yii\web\HttpException
     */
    public function applyStatus($params)
    {

        $allModels = [];
        $totals = [];
        foreach ($this->getModels($params) as $model) {

            $key = $model['webmaster_identifier'] . $model['country_id'] . $model['call_center_id'] . $model['lead_product_id'];

            if (!isset($allModels[$key])) {
                $allModels[$key] = $model;
                $allModels[$key]['status_calls'] = [];
            } else {
                $allModels[$key]['vsego'] += $model['vsego'];
            }

            if ($model['calls'] > self::MAX_CALLS) {
                $model['calls'] = self::MAX_CALLS;
            }

            $allModels[$key]['status_calls'][$model['status']][$model['calls']] = $allModels[$key]['status_calls'][$model['status']][$model['calls']] ?? 0;
            $allModels[$key]['status_calls'][$model['status']][$model['calls']] += $model['vsego'];


            $allModels[$key][$model['status']] = $allModels[$key][$model['status']] ?? 0;
            $allModels[$key][$model['status']] += $model['vsego'];


            if (!isset($totals[$model['webmaster_identifier']])) {
                $totals[$model['webmaster_identifier']] = 0;
            }
            $totals[$model['webmaster_identifier']] += $model['vsego'];

            unset($allModels[$key]['calls']);
            unset($allModels[$key]['status']);
        }


        foreach ($allModels as &$model) {
            $model['total'] = $totals[$model['webmaster_identifier']];
        }

        ArrayHelper::multisort($allModels, ['total', 'vsego', 'country_name', 'call_center_name', 'lead_product_name'], [SORT_DESC, SORT_DESC, SORT_ASC, SORT_ASC, SORT_ASC]);


        $dataProvider = new ArrayDataProvider([
            'allModels' => $allModels,
        ]);
        $dataProvider->pagination->pageSize = $dataProvider->pagination->totalCount;

        return $dataProvider;
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        $output = '';

        return $output;
    }

    /**
     * @return array
     */
    public function getMapStatuses()
    {
        if (empty($this->mapStatuses)) {
            $this->mapStatuses = [
                Yii::t('common', 'Обработка') => OrderStatus::getInitialList(),
                Yii::t('common', 'Перезвоны') => [
                    OrderStatus::STATUS_CC_RECALL,
                ],
                Yii::t('common', 'Недозвон') => [
                    OrderStatus::STATUS_CC_FAIL_CALL,
                ],
                Yii::t('common', 'Подтверждено') => OrderStatus::getApproveCCList(),
                Yii::t('common', 'Отклонено') => [
                    OrderStatus::STATUS_CC_REJECTED,
                ],
                Yii::t('common', 'Брак') => [
                    OrderStatus::STATUS_CC_TRASH,
                    OrderStatus::STATUS_CC_DOUBLE,
                ],
                Yii::t('common', 'На проверке КЦ') => [
                    OrderStatus::STATUS_CC_CHECKING,
                ],
            ];
        }

        return $this->mapStatuses;
    }

    /**
     * @return ProductFilter
     */
    public function getProductFilter()
    {
        if (is_null($this->productFilter)) {
            $this->productFilter = new LeadProductFilter(['reportForm' => $this, 'allProducts' => true]);
        }

        return $this->productFilter;
    }

    /**
     * @return WebmasterNameFilter
     */
    public function getWebmasterFilter()
    {
        if (is_null($this->webmasterFilter)) {
            $this->webmasterFilter = new WebmasterNameFilter();
        }

        return $this->webmasterFilter;
    }

    /**
     * @return CallCenterFilter
     */
    public function getCallCenterFilter()
    {
        if (is_null($this->callCenterFilter)) {
            $this->callCenterFilter = new CallCenterFilter([
                'multiple' => true,
                'label' => 'Колл-центр',
                'reportForm' => $this
            ]);
        }
        return $this->callCenterFilter;
    }


    /**
     * @param array $params
     * @return ReportForm
     */
    protected function applyFilters($params)
    {
        $this->getCountrySelectFilter()->apply($this->query, $params);
        $this->getDateFilter()->apply($this->query, $params);
        $this->getCallCenterFilter()->apply($this->query, $params);
        $this->getProductFilter()->apply($this->query, $params);
        $this->getWebmasterFilter()->apply($this->query, $params);
        return $this;
    }

    /**
     * @return int
     */
    public function getTotalCount(): int 
    {
        return $this->totalCount;
    }
}
