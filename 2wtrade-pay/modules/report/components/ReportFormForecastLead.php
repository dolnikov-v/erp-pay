<?php
namespace app\modules\report\components;

use app\models\Product;
use app\models\Country;
use app\modules\order\models\Order;
use app\modules\order\models\OrderProduct;
use app\modules\order\models\OrderStatus;
use app\modules\storage\models\StorageProduct;
use app\modules\storage\models\Storage;
use app\modules\callcenter\models\CallCenter;
use yii\data\ArrayDataProvider;
use app\modules\report\components\filters\ReportForecastLeadFilter;
use app\modules\report\components\filters\ForecastLeadOrderProductSelectFilter;
use app\modules\report\components\filters\ForecastLeadCountrySelectFilter;
use app\modules\report\components\filters\ForecastLeadCountryOrderSelectFilter;
use app\models\CalculatedParam;
use Yii;
use yii\db\Expression;

/**
 * Class ReportFormForecastLead
 * @package app\modules\report\components
 */
class ReportFormForecastLead extends ReportForm
{
    const PRODUCT_NORM_DAY = 30;

    /**
     * @var ReportForecastLeadFilter
     */
    protected $forecastLeadFilter;

    /**
     * @var ForecastLeadCountrySelectFilter
     */
    protected $forecastLeadCountrySelectFilter;

    /**
     * @var ForecastLeadCountryOrderSelectFilter
     */
    protected $forecastLeadCountryOrderSelectFilter;

    /**
     * @var ForecastLeadOrderProductSelectFilter
     */
    protected $productSelectFilter;

    public static $products;
    public static $callCenters;
    public static $countries;

    public function init()
    {
        parent::init();
        self::$products = Product::find()->collection();
        self::$callCenters = CallCenter::find()->collection();
        self::$countries = Country::find()->collection();
    }

    /**
     * @param array $params
     * @return array
     */
    public function apply($params)
    {
        if (empty($params)) {
            $params['s']['country_ids'] = [Yii::$app->user->getCountry()->id];
        }

        $queryProducts = StorageProduct::find()
            ->joinWith(['storage'])
            ->select([
                'id' => 'concat(' . Storage::tableName() . '.country_id, "_", ' . StorageProduct::tableName() . '.product_id)',
                'country_id' => Storage::tableName() . '.country_id',
                'product_id' => StorageProduct::tableName() . '.product_id',
                'storage_id' => Storage::tableName() . '.id',
                'balance' => new Expression('sum(ifnull(' . StorageProduct::tableName() . '.balance, 0))'),
            ]);
        $this->applyFiltersQueryProductCountry($queryProducts, $params);
        $queryProducts = $queryProducts->groupBy(['id']);
        $arrayProducts = $queryProducts->asArray()->all();

        $queryNeedCalled = Order::find()
            ->select([
                'count(' . Order::tableName() . '.status_id in (' . join(', ', [
                    OrderStatus::STATUS_SOURCE_LONG_FORM,
                    OrderStatus::STATUS_SOURCE_SHORT_FORM,
                    OrderStatus::STATUS_CC_POST_CALL,
                    OrderStatus::STATUS_CC_INPUT_QUEUE,
                    OrderStatus::STATUS_CC_RECALL,
                    OrderStatus::STATUS_CC_FAIL_CALL,
                ]) . ') OR NULL)'
            ])
            ->where([Order::tableName() . '.country_id' => new Expression(CallCenter::tableName() . '.country_id')])
            ->andWhere(['NOT IN', Order::tableName() . '.source_id', Yii::$app->user->rejectedSources]);
        $this->applyFiltersQueryOrder($queryNeedCalled, $params);

        $queryOpers = CallCenter::find()
            ->select([
                'id' => 'concat(' . CallCenter::tableName() . '.country_id, "_", ' . CallCenter::tableName() . '.id)',
                'country_id' => CallCenter::tableName() . '.country_id',
                'call_center_id' => CallCenter::tableName() . '.id',
                // заказы, по которым необходимо совершить звонки (статус 1,2,3,4,5,30)
                'need_called' => $queryNeedCalled,
            ]);
        $this->applyFiltersQueryCountry($queryOpers, $params);
        $queryOpers = $queryOpers->groupBy(['id']);
        $arrayOpers = $queryOpers->asArray()->all();

        $resultArrayOpers = null;
        if (is_array($arrayOpers)) {
            foreach ($arrayOpers as $arrayOpersOne) {
                $item = null;
                $item['id'] = $arrayOpersOne['id'];
                $item['country_id'] = $arrayOpersOne['country_id'];
                $item['call_center_id'] = $arrayOpersOne['call_center_id'];
                $item['need_called'] = $arrayOpersOne['need_called'];
                $item['operators_fact'] = CalculatedParam::getParam(CalculatedParam::OPERATORS_DAY, $item['country_id'], null, $item['call_center_id']);
                $item['avg_calls_order'] = (float)Yii::$app->request->get('avg_calls_order_' . $item['country_id'] . '_' . $item['call_center_id']);
                if (is_null($item['avg_calls_order']) || $item['avg_calls_order'] == '') {
                    $item['avg_calls_order'] = CalculatedParam::getParam(CalculatedParam::AVG_CALLS_ORDER, $item['country_id'], null, $item['call_center_id']);
                }
                $item['leads_day'] = (float)Yii::$app->request->get('leads_day_all_' . $item['country_id']);
                if (is_null($item['leads_day']) || $item['leads_day'] == '') {
                    $item['leads_day'] = CalculatedParam::getParam(CalculatedParam::COUNT_LEADS_ALL, $item['country_id']);
                }
                $item['avg_calls_oper'] = (float)Yii::$app->request->get('avg_calls_oper_' . $item['country_id'] . '_' . $item['call_center_id']);
                if (is_null($item['avg_calls_oper']) || $item['avg_calls_oper'] == '') {
                    $item['avg_calls_oper'] = CalculatedParam::getParam(CalculatedParam::AVG_CALLS_OPERATOR, $item['country_id'], null, $item['call_center_id']);
                }
                $resultArrayOpers[] = $item;
            }
        }

        $queryAvgCallsOrder = CalculatedParam::find()
            ->select([
                'avg(ifnull(' . CalculatedParam::tableName() . '.value, 0))',
            ])
            ->where([CalculatedParam::tableName() . '.name' => CalculatedParam::AVG_CALLS_ORDER])
            ->andWhere([CalculatedParam::tableName() . '.country_id' => new Expression(Order::tableName() . '.country_id')]);
        $queryAvgCallsOper = CalculatedParam::find()
            ->select([
                'avg(ifnull(' . CalculatedParam::tableName() . '.value, 0))',
            ])
            ->where([CalculatedParam::tableName() . '.name' => CalculatedParam::AVG_CALLS_OPERATOR])
            ->andWhere([CalculatedParam::tableName() . '.country_id' => new Expression(Order::tableName() . '.country_id')]);
        $queryOperatorsFact = CalculatedParam::find()
            ->select([
                'avg(ifnull(' . CalculatedParam::tableName() . '.value, 0))',
            ])
            ->where([CalculatedParam::tableName() . '.name' => CalculatedParam::OPERATORS_DAY])
            ->andWhere([CalculatedParam::tableName() . '.country_id' => new Expression(Order::tableName() . '.country_id')]);

        $queryLead = OrderProduct::find()
            ->joinWith([
                'order'
            ])
            ->select([
                'id' => 'concat(' . Order::tableName() . '.country_id, "_", ' . OrderProduct::tableName() . '.product_id)',
                'country_id' => Order::tableName() . '.country_id',
                'product_id' => OrderProduct::tableName() . '.product_id',
                'order_id' => Order::tableName() . '.id',
                // общее количество товара в заказах
                'all' => 'sum(ifnull(' . OrderProduct::tableName() . '.quantity, 0))',
                'holds' =>'count(' . Order::tableName() . '.status_id in (' . join(', ', OrderStatus::getHoldStatusList()) . ') OR NULL)',
                // отправлено в КЦ
                /*'in_call_center' => 'sum(if((' . Order::tableName() . '.status_id not in (' . join(', ', [
                        OrderStatus::STATUS_CC_INPUT_QUEUE,
                        OrderStatus::STATUS_SOURCE_LONG_FORM,
                        OrderStatus::STATUS_SOURCE_SHORT_FORM,
                        OrderStatus::STATUS_AUTOTRASH,
                        OrderStatus::STATUS_AUTOTRASH_DOUBLE,
                    ]) . ')), ' . OrderProduct::tableName() . '.quantity, 0))',*/
                'need_product' => 'sum(if((' . Order::tableName() . '.status_id in (' . join(', ', [
                        OrderStatus::STATUS_CC_APPROVED,
                        OrderStatus::STATUS_LOG_ACCEPTED,
                        OrderStatus::STATUS_LOG_GENERATED,
                    ]) . ')), ' . OrderProduct::tableName() . '.quantity, 0))',
                // одобрено в КЦ
                //'approved' => 'sum(if((' . Order::tableName() . '.status_id in (' . join(', ', OrderStatus::getApproveList()) . ')), ' . OrderProduct::tableName() . '.quantity, 0))',
                // среднее количество товара в заказе
                'avg_quantity' => 'avg(ifnull(' . OrderProduct::tableName() . '.quantity, 0))',
                'avg_calls_order' => $queryAvgCallsOrder,
                'avg_calls_oper' => $queryAvgCallsOper,
                'operators_fact' => $queryOperatorsFact,
            ])
            ->where(['>', OrderProduct::tableName() . '.price', 0])
            ->andWhere(['is not', OrderProduct::tableName() . '.price', null])
            ->andWhere(['NOT IN', Order::tableName() . '.source_id', Yii::$app->user->rejectedSources]);
        $this->applyFiltersQuery($queryLead, $params);
        $queryLead = $queryLead->groupBy(['id']);
        $arrayLead = $queryLead->asArray()->all();

        $resultArrayProducts = null;
        if (is_array($arrayProducts)) {
            foreach ($arrayProducts as $arrayProductsOne) {
                $item = null;
                $item['id'] = $arrayProductsOne['id'];
                $item['country_id'] = $arrayProductsOne['country_id'];
                $item['product_id'] = $arrayProductsOne['product_id'];
                $item['storage_id'] = $arrayProductsOne['storage_id'];
                $item['balance'] = $arrayProductsOne['balance'];
                $item['count_leads'] = (float)Yii::$app->request->get('leads_day_' . $item['country_id'] . '_' . $item['product_id']);
                if (is_null($item['count_leads']) || $item['count_leads'] == '') {
                    $item['count_leads'] = CalculatedParam::getParam(CalculatedParam::COUNT_LEADS, $item['country_id'], $item['product_id']);
                }
                $item['approve_percent'] = (float)Yii::$app->request->get('percent_approved_' . $item['country_id'] . '_' . $item['product_id']);
                if (is_null($item['approve_percent']) || $item['approve_percent'] == '') {
                    $item['approve_percent'] = CalculatedParam::getParam(CalculatedParam::PERCENT_APPROVED, $item['country_id'], $item['product_id']);
                }
                $item['avg_count_product'] = CalculatedParam::getParam(CalculatedParam::AVG_COUNT_PRODUCT, $arrayProductsOne['country_id'], $arrayProductsOne['product_id']);
                if ($item['count_leads'] === false || $item['approve_percent'] === false || $item['avg_count_product'] === false) {
                    $item['need_stock_day'] = false;
                    $item['stock_days'] = false;
                } else {
                    $item['need_stock_day'] = $item['count_leads'] * $item['approve_percent'] * $item['avg_count_product'];
                    if ($item['need_stock_day'] == 0) {
                        $item['stock_days'] = 0;
                    } else {
                        $item['stock_days'] = $item['balance'] / $item['need_stock_day'];
                    }
                }
                $resultArrayProducts[$arrayProductsOne['id']] = $item;
            }
        }

        $resultArrayLead = null;
        if (is_array($arrayLead)) {
            foreach ($arrayLead as $arrayLeadOne) {
                $item = null;
                $item['id'] = $arrayLeadOne['id'];
                $item['country_id'] = $arrayLeadOne['country_id'];
                $item['product_id'] = $arrayLeadOne['product_id'];
                $item['holds'] = $arrayLeadOne['holds'];
                $item['avg_calls_order'] = (float)Yii::$app->request->get('avg_calls_order_' . $item['country_id']);
                if (is_null($item['avg_calls_order']) || $item['avg_calls_order'] == '') {
                    $item['avg_calls_order'] = $arrayLeadOne['avg_calls_order'];
                }
                $item['avg_calls_oper'] = (float)Yii::$app->request->get('avg_calls_oper_' . $item['country_id']);
                if (is_null($item['avg_calls_oper']) || $item['avg_calls_oper'] == '') {
                    $item['avg_calls_oper'] = $arrayLeadOne['avg_calls_oper'];
                }
                $item['operators_fact'] = $arrayLeadOne['operators_fact'];
                //$item['in_call_center'] = $arrayLeadOne['in_call_center'];
                //$item['approved'] = $arrayLeadOne['approved'];
                $item['need_product'] = $arrayLeadOne['need_product'];
                $item['percent_approve'] = (float)Yii::$app->request->get('percent_approved_' . $item['country_id'] . '_' . $item['product_id']);
                if (is_null($item['percent_approve']) || $item['percent_approve'] == '') {
                    $item['percent_approve'] = CalculatedParam::getParam(CalculatedParam::PERCENT_APPROVED, $item['country_id'], $item['product_id']);
                }
                $item['avg_quantity'] = (float)Yii::$app->request->get('avg_quantity_' . $item['country_id'] . '_' . $item['product_id']);
                if (is_null($item['avg_quantity']) || $item['avg_quantity'] == '') {
                    $item['avg_quantity'] = CalculatedParam::getParam(CalculatedParam::AVG_COUNT_PRODUCT, $item['country_id'], $item['product_id']);
                }
                $item['part_quantity'] = (float)Yii::$app->request->get('part_quantity_' . $item['country_id'] . '_' . $item['product_id']);
                if (is_null($item['part_quantity']) || $item['part_quantity'] == '') {
                    $item['part_quantity'] = CalculatedParam::getParam(CalculatedParam::PART_PRODUCT_IN_ORDER, $item['country_id'], $item['product_id']);
                }
                $item['product_balance'] = 0;
                if (isset($resultArrayProducts[$item['country_id'] . '_' . $item['product_id']])) {
                    $item['product_balance'] = $resultArrayProducts[$item['country_id'] . '_' . $item['product_id']]['balance'];
                }
                if (is_null($item['avg_calls_order']) || $item['avg_calls_order'] == 0) {
                    $item['forecast_oper'] = false;
                } else {
                    $item['forecast_oper'] = $item['avg_calls_oper'] * $item['operators_fact'] / $item['avg_calls_order'];
                }

                $resultArrayLead[] = $item;
            }
        }

        $dataProviderLead = new ArrayDataProvider([
            'allModels' => $resultArrayLead,
            'pagination' => false
        ]);
        $dataProviderOpers = new ArrayDataProvider([
            'allModels' => $resultArrayOpers,
            'pagination' => false
        ]);
        $dataProviderProducts = new ArrayDataProvider([
            'allModels' => $resultArrayProducts,
            'pagination' => false
        ]);

        return [
            'dataProviderLead' => $dataProviderLead,
            'dataProviderOpers' => $dataProviderOpers,
            'dataProviderProducts' => $dataProviderProducts,
        ];
    }

    /**
     * @param \app\components\widgets\ActiveForm $form
     * @return string
     */
    public function restore($form)
    {
        return '';
    }

    /**
     * @return ReportForecastLeadFilter
     */
    public function getForecastLeadFilter()
    {
        if (is_null($this->forecastLeadFilter)) {
            $this->forecastLeadFilter = new ReportForecastLeadFilter();
        }

        return $this->forecastLeadFilter;
    }

    /**
     * @return ForecastLeadOrderProductSelectFilter
     */
    public function getOrderProductSelectFilter()
    {
        if (is_null($this->productSelectFilter)) {
            $this->productSelectFilter = new ForecastLeadOrderProductSelectFilter();
        }
        return $this->productSelectFilter;
    }

    /**
     * @return ForecastLeadCountrySelectFilter
     */
    public function getCountrySelectFilter()
    {
        if (is_null($this->forecastLeadCountrySelectFilter)) {
            $this->forecastLeadCountrySelectFilter = new ForecastLeadCountrySelectFilter();
        }

        return $this->forecastLeadCountrySelectFilter;
    }

    /**
     * @return ForecastLeadCountryOrderSelectFilter
     */
    public function getCountryOrderSelectFilter()
    {
        if (is_null($this->forecastLeadCountryOrderSelectFilter)) {
            $this->forecastLeadCountryOrderSelectFilter = new ForecastLeadCountryOrderSelectFilter();
        }

        return $this->forecastLeadCountryOrderSelectFilter;
    }

    /**
     * @param array $params
     * @param \yii\db\ActiveQuery $query
     */
    protected function applyFiltersQuery($query, $params)
    {
        $this->getForecastLeadFilter()->apply($query, $params);
        $this->getCountrySelectFilter()->apply($query, $params);
        $this->getOrderProductSelectFilter()->apply($query, $params);
    }

    /**
     * @param array $params
     * @param \yii\db\ActiveQuery $query
     */
    protected function applyFiltersQueryProductCountry($query, $params)
    {
        $this->getCountrySelectFilter()->apply($query, $params);
        $this->getOrderProductSelectFilter()->apply($query, $params);
    }

    /**
     * @param array $params
     * @param \yii\db\ActiveQuery $query
     */
    protected function applyFiltersQueryCountry($query, $params)
    {
        $this->getCountrySelectFilter()->apply($query, $params);
        $this->getForecastLeadFilter()->apply($query, $params);
    }

    /**
     * @param array $params
     * @param \yii\db\ActiveQuery $query
     */
    protected function applyFiltersQueryOrder($query, $params)
    {
        $this->getForecastLeadFilter()->apply($query, $params);
    }
}