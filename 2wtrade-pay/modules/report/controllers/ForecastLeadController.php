<?php
namespace app\modules\report\controllers;

use app\components\web\Controller;
use app\modules\report\components\ReportFormForecastLead;
use Yii;

/**
 * Class ForecastLeadController
 * @package app\modules\report\controllers
 */
class ForecastLeadController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $reportForm = new ReportFormForecastLead();
        $result = $reportForm->apply(Yii::$app->request->queryParams);

        $totalByRatio = [];
        $totalByOneType = [];
        foreach ($result['dataProviderLead']->allModels as $model) {
            //Сумма столбца Прогноз лидов по товару (По % соотношению)
            if (!($model['percent_approve'] === false || $model['avg_quantity'] === false || $model['part_quantity'] === false || is_null($model['part_quantity']) || $model['part_quantity'] == 0)) {
                $totalByRatio[$model['country_id']] +=
                    (($model['product_balance'] - $model['need_product'] - ($model['holds'] * $model['percent_approve'] * $model['avg_quantity'] * $model['part_quantity'])) - $model['avg_quantity']) / $model['part_quantity'];
            }

            //Массив всех значений столбца Прогноз лидов по товару (Max по одному виду)
            if (!($model['percent_approve'] === false || $model['avg_quantity'] === false || $model['part_quantity'] === false || is_null($model['avg_quantity']) || $model['avg_quantity'] == 0)) {
                $totalByOneType[$model['country_id']][] .=
                    ($model['product_balance'] - $model['need_product'] - ($model['holds'] * $model['percent_approve'] * $model['avg_quantity'] * $model['part_quantity'])) / $model['avg_quantity'];
            }
        }

        foreach ($totalByOneType as $key => $value) {
            $totalByOneType[$key] = min($totalByOneType[$key]);
         }


        return $this->render('index', [
            'reportForm' => $reportForm,
            'dataProviderLead' => $result['dataProviderLead'],
            'dataProviderOpers' => $result['dataProviderOpers'],
            'dataProviderProducts' => $result['dataProviderProducts'],
            'totalByRatio' => $totalByRatio,
            'totalByOneType' => $totalByOneType
        ]);
    }
}