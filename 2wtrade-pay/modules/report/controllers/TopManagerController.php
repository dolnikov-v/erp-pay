<?php
namespace app\modules\report\controllers;

use app\components\web\Controller;
use app\modules\report\components\ReportFormTopManager;
use Yii;

/**
 * Class TopManagerController
 * @package app\modules\report\controllers
 */
class TopManagerController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $reportFormTopManager = new ReportFormTopManager();
        $dataProvider = $reportFormTopManager->apply(Yii::$app->request->queryParams);

        return $this->render('index', [
            'reportFormTopManager' => $reportFormTopManager,
            'dataProvider' => $dataProvider,
        ]);
    }
}