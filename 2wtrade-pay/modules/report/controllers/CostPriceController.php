<?php
namespace app\modules\report\controllers;

use app\components\web\Controller;
use app\modules\report\components\ReportFormCostPrice;
use Yii;

/**
 * Class CostPriceController
 * @package app\modules\report\controllers
 */
class CostPriceController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $reportForm = new ReportFormCostPrice();
        $dataProvider = $reportForm->apply(Yii::$app->request->queryParams);

        return $this->render('index', [
            'reportForm' => $reportForm,
            'dataProvider' => $dataProvider,
        ]);
    }
}
