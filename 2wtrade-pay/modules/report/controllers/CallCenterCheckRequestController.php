<?php
namespace app\modules\report\controllers;

use app\components\web\Controller;
use app\helpers\Utils;
use app\modules\callcenter\models\CallCenterCheckRequest;
use app\modules\report\components\ReportFormCallCenterCheckRequest;
use app\modules\report\extensions\DataProvider;
use Yii;

/**
 * Class CallCenterCheckRequestController
 * @package app\modules\report\controllers
 */
class CallCenterCheckRequestController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $reportForm = new ReportFormCallCenterCheckRequest();
        $dataProviderReject = $reportForm->applyReject(Yii::$app->request->queryParams);
        $dataProviderHold = $reportForm->applyHold(Yii::$app->request->queryParams);
        $dataProviderBuyout = $reportForm->applyBuyout(Yii::$app->request->queryParams);

        $export = Yii::$app->request->get('export');
        if ($export == 'excel') {
            return $this->exportExcel($reportForm, $dataProviderReject, $dataProviderHold, $dataProviderBuyout);
        }


        return $this->render('index', [
            'reportForm' => $reportForm,
            'dataProviderReject' => $dataProviderReject,
            'dataProviderHold' => $dataProviderHold,
            'dataProviderBuyout' => $dataProviderBuyout,
        ]);
    }

    /***
     * @param $reportForm ReportFormCallCenterCheckRequest
     * @param $dataProviderReject DataProvider
     * @param $dataProviderHold DataProvider
     * @param $dataProviderBuyout DataProvider
     * @return bool
     */
    private function exportExcel($reportForm, $dataProviderReject, $dataProviderHold, $dataProviderBuyout)
    {
        $excel = new \PHPExcel();

        $sheetIndex = 0;
        $sheet = $excel->getSheet($sheetIndex);
        $sheet->setTitle(Yii::t('common', 'Отказы КС'));

        $this->exportTab($reportForm, $sheet, $dataProviderReject->allModels);

        $sheetIndex++;
        $sheet = $excel->createSheet($sheetIndex);
        $sheet->setTitle(Yii::t('common', 'Заказы в процессе'));
        $excel->setActiveSheetIndex($sheetIndex);

        $this->exportTab($reportForm, $sheet, $dataProviderHold->allModels);

        $sheetIndex++;
        $sheet = $excel->createSheet($sheetIndex);
        $sheet->setTitle(Yii::t('common', 'Выкуп'));
        $excel->setActiveSheetIndex($sheetIndex);

        $this->exportTab($reportForm, $sheet, $dataProviderBuyout->allModels);

        $excel->setActiveSheetIndex(0);

        $dir = Yii::getAlias("@runtime") . DIRECTORY_SEPARATOR . "call-center-check-request";
        Utils::prepareDir($dir);

        $filename = $dir . DIRECTORY_SEPARATOR . 'call-center-check-request_' . date('Y-m-d', strtotime($reportForm->getDateFilter()->from)) . ' - ' . date('Y-m-d', strtotime($reportForm->getDateFilter()->to)) . ".xlsx";

        $objWriter = \PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        $objWriter->save($filename);

        Yii::$app->response->sendFile($filename);

        return true;
    }

    /**
     * @param $reportForm ReportFormCallCenterCheckRequest
     * @param $sheet \PHPExcel_Worksheet
     * @param $models array
     */
    private function exportTab($reportForm, $sheet, $models)
    {

        $row = 1;
        $col = 0;
        $sheet->setCellValueByColumnAndRow($col++, $row, $reportForm->groupByCollection[$reportForm->groupBy]);
        $sheet->setCellValueByColumnAndRow($col++, $row, Yii::t('common', 'Всего'));
        $sheet->setCellValueByColumnAndRow($col++, $row, '%');
        $sheet->setCellValueByColumnAndRow($col++, $row, Yii::t('common', 'Ожидает заказ'));
        $sheet->setCellValueByColumnAndRow($col++, $row, '%');
        $sheet->setCellValueByColumnAndRow($col++, $row, Yii::t('common', 'Заказ доставлен'));
        $sheet->setCellValueByColumnAndRow($col++, $row, '%');
        $sheet->setCellValueByColumnAndRow($col++, $row, Yii::t('common', 'Клиент отказался'));
        $sheet->setCellValueByColumnAndRow($col++, $row, '%');
        $sheet->setCellValueByColumnAndRow($col++, $row, Yii::t('common', 'КС не связалась с клиентом'));
        $sheet->setCellValueByColumnAndRow($col++, $row, '%');
        $sheet->setCellValueByColumnAndRow($col++, $row, Yii::t('common', 'Не обработано'));
        $sheet->setCellValueByColumnAndRow($col++, $row, '%');

        $row++;

        $totals = [];
        $totals['total'] = 0;
        $totals['waiting'] = 0;
        $totals['received'] = 0;
        $totals['notneeded'] = 0;
        $totals['notdone'] = 0;

        foreach ($models as $model) {
            $col = 0;
            $sheet->setCellValueByColumnAndRow($col++, $row, $model['groupbyname']);
            $sheet->setCellValueByColumnAndRow($col++, $row, $model[Yii::t('common', 'Всего')]);
            $sheet->setCellValueByColumnAndRow($col++, $row, self::percent(1, 1));
            $sheet->setCellValueByColumnAndRow($col++, $row, $model['waiting']);
            $sheet->setCellValueByColumnAndRow($col++, $row, self::percent($model[Yii::t('common', 'Всего')], $model['waiting']));
            $sheet->setCellValueByColumnAndRow($col++, $row, $model['received']);
            $sheet->setCellValueByColumnAndRow($col++, $row, self::percent($model[Yii::t('common', 'Всего')], $model['received']));
            $sheet->setCellValueByColumnAndRow($col++, $row, $model['notneeded']);
            $sheet->setCellValueByColumnAndRow($col++, $row, self::percent($model[Yii::t('common', 'Всего')], $model['notneeded']));
            $sheet->setCellValueByColumnAndRow($col++, $row, $model['notcontacted']);
            $sheet->setCellValueByColumnAndRow($col++, $row, self::percent($model[Yii::t('common', 'Всего')], $model['notcontacted']));
            $sheet->setCellValueByColumnAndRow($col++, $row, $model['notdone']);
            $sheet->setCellValueByColumnAndRow($col++, $row, self::percent($model[Yii::t('common', 'Всего')], $model['notdone']));

            $totals['total'] += $model[Yii::t('common', 'Всего')];
            $totals['waiting'] += $model['waiting'];
            $totals['received'] += $model['received'];
            $totals['notneeded'] += $model['notneeded'];
            $totals['notdone'] += $model['notdone'];
            $row++;
        }

        $col = 0;
        $sheet->setCellValueByColumnAndRow($col++, $row, Yii::t('common', 'Всего'));
        $sheet->setCellValueByColumnAndRow($col++, $row, $totals['total']);
        $sheet->setCellValueByColumnAndRow($col++, $row, self::percent(1, 1));
        $sheet->setCellValueByColumnAndRow($col++, $row, $totals['waiting']);
        $sheet->setCellValueByColumnAndRow($col++, $row, self::percent($totals['total'], $totals['waiting']));
        $sheet->setCellValueByColumnAndRow($col++, $row, $totals['received']);
        $sheet->setCellValueByColumnAndRow($col++, $row, self::percent($totals['total'], $totals['received']));
        $sheet->setCellValueByColumnAndRow($col++, $row, $totals['notneeded']);
        $sheet->setCellValueByColumnAndRow($col++, $row, self::percent($totals['total'], $totals['notneeded']));
        $sheet->setCellValueByColumnAndRow($col++, $row, $totals['notcontacted']);
        $sheet->setCellValueByColumnAndRow($col++, $row, self::percent($totals['total'], $totals['notcontacted']));
        $sheet->setCellValueByColumnAndRow($col++, $row, $totals['notdone']);
        $sheet->setCellValueByColumnAndRow($col++, $row, self::percent($totals['total'], $totals['notdone']));

        foreach (range('A', 'M') as $columnID) {
            $sheet->getColumnDimension($columnID)->setAutoSize(true);
        }

        $sheet->getStyle('A1:M1')->getFont()->setBold(true);
        $sheet->getStyle('A' . $row . ':M' . $row)->getFont()->setBold(true);
    }

    /**
     * @param $all
     * @param $val
     * @return float|int
     */
    public static function percent($all, $val)
    {
        return number_format($all ? $val * 100 / $all : 0, 2);
    }
}
