<?php
namespace app\modules\report\controllers;

use app\components\web\Controller;
use app\modules\report\components\ReportFormCallCenter;
use Yii;

/**
 * Class CallCenterController
 * @package app\modules\report\controllers
 */
class CallCenterController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {

        ini_set('memory_limit', '1024M');
        $reportForm = new ReportFormCallCenter();
        $dataProvider = $reportForm->apply(Yii::$app->request->queryParams);

        return $this->render('index', [
            'reportForm' => $reportForm,
            'dataProvider' => $dataProvider,
        ]);
    }
}
