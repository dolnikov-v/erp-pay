<?php
namespace app\modules\report\controllers;

use app\components\filters\AjaxFilter;
use app\components\web\Controller;
use app\modules\callcenter\models\CallCenter;
use app\modules\salary\models\Person;
use app\modules\report\components\ReportFormTeam;
use yii\data\ArrayDataProvider;
use Yii;

/**
 * Class TeamController
 * @package app\modules\report\controllers
 */
class TeamController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => [
                    'get-call-center',
                    'get-team-lead',
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $reportForm = new ReportFormTeam();
        $dataProvider = $reportForm->apply(Yii::$app->request->queryParams);

        $params = [
            'reportForm' => $reportForm,
            'dataProvider' => $dataProvider,
            'dataProviderDetail' => false,
            'dataProviderDetailPrevious' => false,
            'dataProviderDetailPrevious2' => false,
            'previousDates' => [],
            'previousDates2' => [],
        ];

        if ($reportForm->person) {

            $params['dataProviderDetail'] = new ArrayDataProvider([
                'allModels' => $dataProvider->allModels[0]['orders'],
                'sort' => $dataProvider->sort
            ]);

            $params['dataProviderDetailPrevious'] = new ArrayDataProvider([
                'allModels' => $dataProvider->allModels[0]['ordersPrevious'],
                'sort' => $dataProvider->sort
            ]);
            $params['dataProviderDetailPrevious2'] = new ArrayDataProvider([
                'allModels' => $dataProvider->allModels[0]['ordersPrevious2'],
                'sort' => $dataProvider->sort
            ]);
            $params['previousDates'] = $dataProvider->allModels[0]['ordersPreviousDates'];
            $params['previousDates2'] = $dataProvider->allModels[0]['ordersPreviousDates2'];

        }

        return $this->render('index', $params);
    }

    /**
     * @return string
     */
    public function actionGetCallCenter()
    {
        $response = [
            'status' => 'fail',
            'message' => '',
        ];

        $callCenters = [];
        if (Yii::$app->request->get('office')) {
            $officeId = Yii::$app->request->get('office');
            $callCenters = CallCenter::find()->byOfficeId($officeId)->collection(true);
        }

        $response['status'] = 'true';
        $response['message'] = $callCenters;

        return $response;
    }

    /**
     * @return string
     */
    public function actionGetTeamLead()
    {
        $response = [
            'status' => 'fail',
            'message' => '',
        ];

        $teamLeads = [];
        if (Yii::$app->request->get('office')) {
            $officeId = Yii::$app->request->get('office');
            $teamLeads = Person::find()
                ->active()
                ->byOfficeId($officeId)
                ->isTeamLead()
                ->collection();
        }

        $response['status'] = 'true';
        $response['message'] = $teamLeads;

        return $response;
    }
}