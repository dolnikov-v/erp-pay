<?php

namespace app\modules\report\controllers;

use app\components\web\Controller;
use app\modules\report\components\ReportFormShipment;
use Yii;

/**
 * Class ShipmentController
 * @package app\modules\report\controllers
 */
class ShipmentController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $reportForm = new ReportFormShipment();
        $dataProvider = $reportForm->apply(Yii::$app->request->queryParams);

        return $this->render('index', [
            'reportForm' => $reportForm,
            'dataProvider' => $dataProvider,
        ]);
    }
}
