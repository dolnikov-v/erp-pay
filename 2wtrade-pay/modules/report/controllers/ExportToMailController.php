<?php
namespace app\modules\report\controllers;

use app\components\web\Controller;
use app\components\filters\AjaxFilter;
use app\helpers\Utils;
use kartik\mpdf\Pdf;

use Yii;


/**
 * Class ExportToMailController
 * @package app\modules\report\controllers
 */
class ExportToMailController extends Controller
{

    private $report;
    private $content;


    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => [
                    'sent',
                ],
            ]
        ];
    }

    /**
     * @return string
     */
    public function actionSent()
    {
        $request = Yii::$app->request;
        $this->report = $request->post('report');
        $this->content = $request->post('export_content');

        $mailsStr = $request->post('mails', Yii::t('common', 'No email found'));
        $mailsBeforeValidate = explode('×', $mailsStr);
        $mails = [];

        foreach ($mailsBeforeValidate as $item) {
            if (mb_strpos($item, '@')) {
                $mails[] = $item;
            }
        }

        $fileName = $this->createPDFFile();
        $result = $this->sender($fileName, $mails);
        unlink($fileName);

        if (empty($result)) {
            return 'success';
        }

        return $result;
    }


    private function createPDFFile() {

        $tempPath = Yii::getAlias('@runtime') .DIRECTORY_SEPARATOR .'mpdf';
        $filePath = Yii::getAlias('@runtime') .DIRECTORY_SEPARATOR .'mpdf' .DIRECTORY_SEPARATOR .Yii::$app->controller->id;
        $filename = Utils::uid();
        Utils::prepareDir($filePath);

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_LANDSCAPE,
            'destination' => Pdf::DEST_FILE,
            'filename' => $filePath .DIRECTORY_SEPARATOR .$filename .'.pdf',
            'tempPath' => $tempPath,
            'content' => $this->content,
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'cssInline' => '
                .kv-wrap{padding:20px;}
			    .kv-align-center{text-align:center;}
			    .kv-align-left{text-align:left;}
			    .kv-align-right{text-align:right;}
			    .kv-align-top{vertical-align:top!important;}
			    .kv-align-bottom{vertical-align:bottom!important;}
			    .kv-align-middle{vertical-align:middle!important;}
			    .kv-page-summary{border-top:4px double #ddd;font-weight: bold;}
			    .kv-table-footer{border-top:4px double #ddd;font-weight: bold;}
			    .kv-table-caption{font-size:1.5em;padding:8px;border:1px solid #ddd;border-bottom:none;}
			    ',
            'options' => ['title' => $this->report],
            'methods' => [
                'SetHeader' => [$this->report],
                'SetFooter' => ['{PAGENO}'],
            ]
        ]);

        $pdf->render();
        return $pdf->filename;
    }

    private function sender($filename, $mails) {

        $errorList = [];

        foreach ($mails as $recipient) {
            $sent = Yii::$app->mailer->compose()
                ->setFrom('no-reply@2wtrade-pay.com')
                ->setTo($recipient)
                ->setSubject(Yii::t('common', $this->report .'_' .Yii::$app->formatter->asDate(time())))
                ->setTextBody(Yii::t('common', 'Вам был отправлен ') .$this->report .Yii::t('common', ' (во вложенном файле)'))
                ->setHtmlBody('<b>' .Yii::t('common', 'Вам был отправлен ') .$this->report .Yii::t('common', ' (во вложенном файле)') .'</b>')
                ->attach($filename)
                ->send();
            if (!$sent) {
                $errorList[] = $recipient;
            }
        }

        return $errorList;
    }

}
