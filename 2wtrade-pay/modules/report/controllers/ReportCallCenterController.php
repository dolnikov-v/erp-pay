<?php

namespace app\modules\report\controllers;

use app\components\web\Controller;
use app\components\Notifier;
use app\components\curl\CurlFactory;
use app\modules\callcenter\models\CallCenter;
use app\modules\callcenter\models\CallCenterFullRequest;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\report\components\ReportCallCenterComparator;
use app\modules\report\components\ReportFormCallCenterCompare;
use app\modules\report\components\ReportFormFindCallCenter;
use app\modules\report\models\ReportCallCenter;
use app\modules\report\widgets\ReportCallCenterShowerColumns;
use Yii;
use yii\base\Exception;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Response;

/**
 * Class ReportCallCenterController
 * @package app\modules\report\controllers
 */
class ReportCallCenterController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $reportForm = new ReportFormCallCenterCompare();
        $dataProvider = $reportForm->apply(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'reportForm' => $reportForm
        ]);
    }

    /**
     * @return string
     */
    public function actionFindInCallCenter()
    {
        $reportForm = new ReportFormFindCallCenter();
        $params = Yii::$app->request->queryParams;

        if (isset(Yii::$app->request->queryParams['query-from-session']) && Yii::$app->session->has('report-report-call-center-query-params')) {
            $params = Yii::$app->session->get('report-report-call-center-query-params');
        }

        $models = $reportForm->apply($params);

        $dataProvider = new ArrayDataProvider([
            'allModels' => $models,
            'pagination' => []
        ]);

        $showerColumns = new ReportCallCenterShowerColumns();

        $data = [
            'dataProvider' => $dataProvider,
            'reportForm' => $reportForm,
            'showerColumns' => $showerColumns
        ];

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $column = Yii::$app->request->post('column');
            $choose = Yii::$app->request->post('choose');

            $showerColumns->toggleColumn($column, $choose);

            return [
                'content' => $this->renderPartial('_find-in-call-center-content', $data),
            ];
        }

        return $this->render('find-in-call-center', $data);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionShowErrors($id)
    {
        $type = Yii::$app->request->get('type');
        $model = ReportCallCenter::findOne($id);

        $errors = ReportCallCenterComparator::getIdsWithError($model, $type);

        if (in_array($type, [
            ReportCallCenter::SHOW_ERROR_OUR_POST_CALL,
            ReportCallCenter::SHOW_ERROR_OUR_FAIL_CALL,
            ReportCallCenter::SHOW_ERROR_OUR_RECALL,
            ReportCallCenter::SHOW_ERROR_OUR_REJECTED,
            ReportCallCenter::SHOW_ERROR_OUR_DOUBLE,
            ReportCallCenter::SHOW_ERROR_OUR_TRASH,
            ReportCallCenter::SHOW_ERROR_OUR_APPROVED
        ])) {
            $elements = ArrayHelper::getColumn($errors, 'order_id');
            Yii::$app->session->set('order-index-query-params', [
                'NumberFilter' => [
                    'number' => implode(',', $elements),
                    'entity' => 'id'
                ]
            ]);
            return $this->redirect(Url::toRoute([
                '/order/index/index',
                'query-from-session' => true
            ]));
        } elseif (in_array($type, [
            ReportCallCenter::SHOW_ERROR_CC_POST_CALL,
            ReportCallCenter::SHOW_ERROR_CC_APPROVED,
            ReportCallCenter::SHOW_ERROR_CC_FAIL_CALL,
            ReportCallCenter::SHOW_ERROR_CC_RECALL,
            ReportCallCenter::SHOW_ERROR_CC_REJECTED,
            ReportCallCenter::SHOW_ERROR_CC_DOUBLE,
            ReportCallCenter::SHOW_ERROR_CC_TRASH,
            ReportCallCenter::SHOW_ERROR_NOT_FOUND_PRODUCTS,
            ReportCallCenter::SHOW_ERROR_NOT_EQUAL_PRODUCT_QUANTITY,
            ReportCallCenter::SHOW_ERROR_NOT_EQUAL_PRODUCT_PRICE,
        ])) {
            $elements = ArrayHelper::getColumn($errors, 'foreign_id');

            Yii::$app->session->set('report-report-call-center-query-params', [
                's' => [
                    'ids' => implode(',', $elements),
                    'from' => '',
                    'to' => ''
                ],
                'search-in-base' => 'on'
            ]);

            return $this->redirect(Url::toRoute([
                'find-in-call-center',
                'query-from-session' => true
            ]));
        }


        return $this->redirect(Url::to(['index']));
    }

    /**
     * @param $id
     * @return Response
     */
    public function actionRecheckLog($id)
    {
        $model = ReportCallCenter::findOne($id);

        ReportCallCenterComparator::compareWithCallCenters($model);

        $model->save();

        $options = Yii::$app->request->queryParams;
        unset($options['id']);
        return $this->redirect(ArrayHelper::merge(['index'], $options));
    }

    /**
     * @param $id
     * @return Response
     */
    public function actionUpdateManually($id)
    {
        $error = false;
        $resArray = [];

        $mapStatuses = [
            1 => OrderStatus::STATUS_CC_POST_CALL,
            4 => OrderStatus::STATUS_CC_APPROVED,
            5 => OrderStatus::STATUS_CC_FAIL_CALL,
            6 => OrderStatus::STATUS_CC_RECALL,
            7 => OrderStatus::STATUS_CC_REJECTED,
            8 => OrderStatus::STATUS_CC_REJECTED,
            9 => OrderStatus::STATUS_CC_APPROVED,
            10 => OrderStatus::STATUS_CC_REJECTED,
            11 => OrderStatus::STATUS_CC_REJECTED,
            12 => OrderStatus::STATUS_CC_APPROVED,
            13 => OrderStatus::STATUS_CC_REJECTED,
            15 => OrderStatus::STATUS_CC_DOUBLE,
            17 => OrderStatus::STATUS_CC_TRASH,
            19 => OrderStatus::STATUS_CC_TRASH,
        ];

        $ccr = CallCenterRequest::find()
            ->byOrderId($id)
            ->one();

        $cc = CallCenter::find()
            ->where(["=", CallCenter::tableName() .".id", $ccr->call_center_id])
            ->one();

        $order = Order::find()
            ->byId($id)
            ->one();

        $cc_type = $order->call_center_type;

        $ccfr = null;

        // Делаем запрос к КоллЦентру
        try {
            $time = time();
            $token = md5($cc->api_key . $time . $cc->api_pid);
            $url = rtrim($cc->url,
                    '/') . '/api/method/GetOrdersByIds?token=' . $token . '&time=' . $time . '&pid=' . $cc->api_pid . '&order_ids=' . $id . '&type=' . $cc_type;

            $curl = CurlFactory::build(CurlFactory::METHOD_GET, $url);
            $сс_answer = $curl->query();
            $answer = json_decode($сс_answer, true);
        }
        catch (exception $e) {
            $error = true;
            $resArray = Array('result_text' => 'Ошибка: не удалось получить данные из Колл-Центра', 'result_type' => Notifier::TYPE_DANGER);
        }

        // Обрабатываем результат запроса
        if (!$error) {
            if (!empty($answer)) {
                if ($answer['success']) {
                    $transaction = Yii::$app->db->beginTransaction();
                    $answer_status = $answer['response'][$id]['status'];
                    // Обновляем CallCenterRequest
                    if ($answer_status != $ccr->foreign_status) {
                        try {
                            $ccr->foreign_status = $answer_status;
                            $ccr->save();
                        }
                        catch (exception $e) {
                            $error = true;
                            $resArray = Array('result_text' => 'Ошибка: не удалось сохранить статус в CallCenterRequest', 'result_type' => Notifier::TYPE_DANGER);
                            $transaction->rollBack();
                        }
                    }
                    // Обновляем Order
                    if (!$error) {
                        $status_id = $mapStatuses[$answer_status];
                        if ($status_id != $order->status_id) {
                            if ($order->canChangeStatusTo($status_id, $order->status_id)) {
                                try {
                                    $order->status_id = $status_id;
                                    $order->save();
                                } catch (exception $e) {
                                    $error = true;
                                    $resArray = Array('result_text' => 'Ошибка: не удалось сохранить статус в Order', 'result_type' => Notifier::TYPE_DANGER);
                                    $transaction->rollBack();
                                }
                            }
                            else {
                                $error = true;
                                $resArray = Array('result_text' => 'Ошибка: статус в Order продвинулся дальше чем в CallCenterRequest', 'result_type' => Notifier::TYPE_DANGER);
                                $transaction->rollBack();
                            }
                        }
                    }
                    // Обновляем CallCenterFullRequest
                    if (!$error) {
                        $ccfr = CallCenterFullRequest::find()
                            ->where(["=", CallCenterFullRequest::tableName() .".order_id", $id])
                            ->andWhere(["=", CallCenterFullRequest::tableName() .".foreign_id", CallCenterRequest::tableName() .".id"])
                            ->one();
                        if (!empty($ccfr)) {            // Если запись есть - обновляем
                            try {
                                $ccfr->status = $answer_status;
                                $ccfr->sub_status = $answer['response'][$id]['sub_status'];
                                $ccfr->name = $answer['response'][$id]['name'];
                                $ccfr->phone = $answer['response'][$id]['phone'];
                                $ccfr->address = $answer['response'][$id]['address'];
                                $ccfr->address_components = json_encode($answer['response'][$id]['address_components']);
                                $ccfr->products = json_encode($answer['response'][$id]['products']);
                                $ccfr->shipping = json_encode($answer['response'][$id]['shipping']);
                                $ccfr->history = json_encode($answer['response'][$id]['history']);
                                $ccfr->delivery = $answer['response'][$id]['delivery'];
                                $ccfr->comment = $answer['response'][$id]['comment'];
                                $ccfr->last_update = time();
                                $ccfr->updated_at = time();
                                $ccfr->response_message = json_encode($answer['response'][$id]);
                                $ccfr->save();
                            }
                            catch (exception $e) {
                                $error = true;
                                $resArray = Array('result_text' => 'Ошибка: не удалось сохранить статус в CallCenterFullRequest', 'result_type' => Notifier::TYPE_DANGER);
                                $transaction->rollBack();
                            }
                        }
                        else {                          // Если записи нет - создаем новую
                            $ccfr = new CallCenterFullRequest();
                            $ccfr->call_center_id = $ccr->call_center_id;
                            $ccfr->order_id = $ccr->order_id;
                            $ccfr->foreign_id = $ccr->id;
                            $ccfr->status = $answer_status;
                            $ccfr->sub_status = $answer['response'][$id]['sub_status'];
                            $ccfr->name = $answer['response'][$id]['name'];
                            $ccfr->phone = $answer['response'][$id]['phone'];
                            $ccfr->address = $answer['response'][$id]['address'];
                            $ccfr->address_components = json_encode($answer['response'][$id]['address_components']);
                            $ccfr->products = json_encode($answer['response'][$id]['products']);
                            $ccfr->shipping = json_encode($answer['response'][$id]['shipping']);
                            $ccfr->history = json_encode($answer['response'][$id]['history']);
                            $ccfr->delivery = $answer['response'][$id]['delivery'];
                            $ccfr->comment = $answer['response'][$id]['comment'];
                            $ccfr->last_update = time();
                            $ccfr->created_at = time();
                            $ccfr->updated_at = time();
                            $ccfr->response_message = json_encode($answer['response'][$id]);

                            try {
                                $res = $ccfr->save();
                                if ($res === false) $error = true;
                            }
                            catch (exception $e) {
                                $error = true;
                                $resArray = Array('result_text' => 'Ошибка: не удалось сохранить статус в CallCenterFullRequest', 'result_type' => Notifier::TYPE_DANGER);
                                $transaction->rollBack();
                            }
                        }
                    }

                    if (!$error) {
                        try {
                            $transaction->commit();
                            $resArray = Array('result_text' => 'Статус успешно обновлен', 'result_type' => Notifier::TYPE_SUCCESS);
                        }
                        catch (exception $e) {
                            $error = true;
                            $resArray = Array('result_text' => 'Ошибка: не удалось применить транзакцию', 'result_type' => Notifier::TYPE_DANGER);
                            $transaction->rollBack();
                        }
                    }
                }
                else {
                    $error = true;
                    $resArray = Array('result_text' => 'Ошибка: не удалось получить данные из Колл-Центра', 'result_type' => Notifier::TYPE_DANGER);
                }
            }
            else {
                $error = true;
                $resArray = Array('result_text' => 'Ошибка: не удалось получить данные из Колл-Центра', 'result_type' => Notifier::TYPE_DANGER);
            }
        }


        Yii::$app->session->set('income-from-update-manually', [
            'result' => $resArray
        ]);

        return $this->redirect(Url::toRoute([
            'find-in-call-center',
            'query-from-session' => true
        ]));
    }

}