<?php
namespace app\modules\report\controllers;

use app\components\web\Controller;
use app\modules\report\components\ReportFormRatingBuyoutCheck;
use Yii;

/**
 * Class RatingBuyoutCheckController
 * @package app\modules\report\controllers
 */
class RatingBuyoutCheckController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $reportForm = new ReportFormRatingBuyoutCheck();
        $dataProvider = $reportForm->apply(Yii::$app->request->queryParams);

        return $this->render('index', [
            'reportForm' => $reportForm,
            'dataProvider' => $dataProvider,
        ]);
    }
}