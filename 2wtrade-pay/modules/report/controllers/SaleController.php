<?php

namespace app\modules\report\controllers;

use app\components\web\Controller;
use app\modules\report\components\ReportFormSale;
use Yii;

/**
 * Class SaleController
 * @package app\modules\report\controllers
 */
class SaleController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $reportForm = new ReportFormSale();
        $dataProvider = $reportForm->apply(Yii::$app->request->queryParams);

        return $this->render('index', [
            'reportForm' => $reportForm,
            'dataProvider' => $dataProvider,
        ]);
    }
}
