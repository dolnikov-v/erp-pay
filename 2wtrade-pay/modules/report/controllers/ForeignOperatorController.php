<?php
namespace app\modules\report\controllers;

use app\components\web\Controller;
use app\modules\report\components\ReportFormForeignOperator;
use app\widgets\Panel;
use Yii;

/**
 * Class ForeignOperatorController
 * @package app\modules\report\controllers
 */
class ForeignOperatorController extends Controller
{
    const NEWBIE_DAYS = 31;
    const APPROVE_GOOD_PERCENT = 45;
    /**
     * @return string
     */
    public function actionIndex()
    {
        $reportForm = new ReportFormForeignOperator();
        $dataProvider = $reportForm->apply(Yii::$app->request->queryParams);

        $working_days = $reportForm->getWorkingDays($reportForm->getDateFilter()->from);


        $dataProviderOld = clone $dataProvider;
        $dataProviderNew = clone $dataProvider;

        $showAlert = false;
        //вкладка опытные сотрудники
        foreach ($dataProviderOld->allModels as $k => $item) {
            //исключаем тех кто работает меньше месяца
            if ($working_days[$item['last_foreign_operator']] <= self::NEWBIE_DAYS) {
                unset($dataProviderOld->allModels[$k]);
            }

            //показать уведомление, если отчёт пытаемся сгенерить за период, который не закрыт.
            if ($item[Yii::t('common', 'В процессе')] || $item[Yii::t('common', 'В курьерке')]) {
                $showAlert = true;
            }
        }

        //вкладка сотрудники-новички
        foreach ($dataProviderNew->allModels as $k => $item) {
            //исключаем тех кто работает больще месяца
            if ($working_days[$item['last_foreign_operator']] > self::NEWBIE_DAYS) {
                unset($dataProviderNew->allModels[$k]);
            }
        }

        if ($showAlert) {
            $reportForm->panelAlert = Yii::t('common', 'Выбранный период не закрыт. Данные в отчёте могут быть неполными или некорректными');
            $reportForm->panelAlertStyle = Panel::ALERT_PRIMARY;
        }

        $approvePlan = Yii::$app->user->country->cc_operator_daily_approve_plan;

        return $this->render('index', [
            'reportForm' => $reportForm,
            'approvePlan' => $approvePlan,
            'dataProviderOld' => $dataProviderOld,
            'dataProviderNew' => $dataProviderNew,
        ]);
    }
}