<?php
namespace app\modules\report\controllers;

use app\components\web\Controller;
use app\modules\report\components\ReportFormOperatorsPenalty;
use Yii;
use yii\data\ArrayDataProvider;

/**
 * Class OperatorsPenaltyController
 * @package app\modules\report\controllers
 */
class OperatorsPenaltyController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $reportForm = new ReportFormOperatorsPenalty();
        $dataProviderAll = $reportForm->apply(Yii::$app->request->queryParams);
        $dataProviderTeamLead = $reportForm->buildTeamLeadData($dataProviderAll->allModels);

        return $this->render('index', [
            'reportForm' => $reportForm,
            'dataProviderAll' => $dataProviderAll,
            'dataProviderTeamLead' => $dataProviderTeamLead,
        ]);
    }
}