<?php
namespace app\modules\report\controllers;

use app\components\web\Controller;
use app\modules\report\components\ReportFormDeliveryTerms;
use Yii;

/**
 * Class DeliveryTermsController
 * @package app\modules\report\controllers
 */
class DeliveryTermsController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $reportForm = new ReportFormDeliveryTerms();

        if (empty(Yii::$app->request->queryParams))
            $inputRequest['s'] = $this->getDefault();
        else
            $inputRequest = Yii::$app->request->queryParams;

        $dataProvider = $reportForm->apply($inputRequest);

        return $this->render('index', [
            'reportForm' => $reportForm,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return array
     */
    private function getDefault() {
        $s = [];
        $s['to'] = date("d.m.Y", time());
        $s['from'] = date("d.m.Y", strtotime("-7 days"));
        $s['type'] = "";
        return $s;
    }
}
