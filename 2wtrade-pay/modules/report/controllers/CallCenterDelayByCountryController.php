<?php
namespace app\modules\report\controllers;

use app\components\filters\AjaxFilter;
use app\components\web\Controller;
use app\models\UserCountry;
use app\models\Country;
use app\modules\report\components\ReportFormCallCenterDelayByCountry;
use Yii;

/**
 * Class CallCenterDelayByCountryController
 * @package app\modules\report\controllers
 */
class CallCenterDelayByCountryController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => [
                    'get-country',
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $inputRequest = [];
        $reportForm = new ReportFormCallCenterDelayByCountry();

        if (empty(Yii::$app->request->queryParams)) {
            $inputRequest['s'] = $this->getDefault();
        }
        else {
            $inputRequest = Yii::$app->request->queryParams;
        }

        if ($inputRequest['s']['all_countries'] == 1) {
            $countries = Country::find()->select(['id'])->asArray()->all();
            foreach ($countries as $item) {
                $inputRequest['s']['country_ids'][] = (string)$item['id'];
            }
        }

        $dataProvider = $reportForm->apply($inputRequest);

        return $this->render('index', [
            'reportForm' => $reportForm,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return array
     */
    public function actionGetCountry()
    {
        $response = [
            'status' => 'fail',
            'message' => '',
        ];

        $countryIds = [];
        if (Yii::$app->request->get('curator-users')) {
            $userIds = Yii::$app->request->get('curator-users');

            $userCountryArray = UserCountry::find();
            foreach ($userIds as $userId) {
                $userCountryArray->orWhere(['user_id' => $userId]);
            }

            foreach ($userCountryArray->all() as $userCountry) {
                $countryIds[] = $userCountry->country_id;
            }
        }

        $response['status'] = 'true';
        $response['message'] = $countryIds;

        return $response;
    }

    /**
     * @return array
     */
    private function getDefault () {
        $s = [];
        $s['country_ids'] = Array(0 => (string)Yii::$app->user->getCountry()->id);

        return $s;
    }
}
