<?php
namespace app\modules\report\controllers;

use app\modules\profile\controllers\ViewController;
use app\modules\report\components\ReportFormConsolidateByCountry;
use Yii;

/**
 * Class ConsolidateByCountryController
 * @package app\modules\report\controllers
 */
class ConsolidateByCountryController extends ViewController
{
    /**
     * @return string
     */
    public function actionIndex()
    {

        set_time_limit(120);

        $reportForm = new ReportFormConsolidateByCountry();
        $dataProvider = $reportForm->apply(Yii::$app->request->queryParams);

        return $this->render('index', [
            'reportForm' => $reportForm,
            'dataProvider' => $dataProvider,
        ]);
    }

}
