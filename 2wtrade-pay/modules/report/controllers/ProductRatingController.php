<?php
namespace app\modules\report\controllers;

use app\components\web\Controller;
use app\modules\report\components\ReportFormProductRating;
use Yii;

/**
 * Class ProductRatingController
 * @package app\modules\report\controllers
 */
class ProductRatingController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $reportForm = new ReportFormProductRating();
        $dataProvider = $reportForm->apply(Yii::$app->request->queryParams);

        return $this->render('index', [
            'reportForm' => $reportForm,
            'dataProvider' => $dataProvider,
        ]);
    }
}