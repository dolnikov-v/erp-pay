<?php

namespace app\modules\report\controllers;


use app\components\filters\AjaxFilter;
use app\components\Notifier;
use app\components\web\Controller;
use app\models\CurrencyRate;
use app\models\CurrencyRateHistory;
use app\modules\report\components\invoice\ReportFormInvoice;
use app\modules\report\components\invoice\ReportFormInvoiceList;
use app\modules\report\models\Invoice;
use app\modules\report\widgets\invoice\InvoiceModalClose;
use app\modules\report\widgets\invoice\InvoiceModalEmailSender;
use app\widgets\LinkPager;
use Yii;
use yii\db\Expression;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class InvoiceController
 * @package app\modules\report\controllers
 */
class InvoiceController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => [
                    'create-invoice',
                    'regenerate-invoice',
                    'currency-rate',
                    'reload-invoice-list',
                    'get-finance-reports',
                    'approve-invoice',
                ],
            ]
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $reportForm = new ReportFormInvoice();
        $result = $reportForm->apply(Yii::$app->request->queryParams);

        $searchFormInvoices = new ReportFormInvoiceList();
        $dataProvider = $searchFormInvoices->apply(Yii::$app->request->queryParams);

        LinkPager::setPageSize($dataProvider->pagination);

        return $this->render('index', [
            'reportForm' => $reportForm,
            'allData' => $result['output'],
            'delivery' => $result['delivery'],
            'invoicesDataProvider' => $dataProvider,
            'invoicesReportForm' => $searchFormInvoices,
        ]);
    }

    /**
     * @param $contract
     * @param $from
     * @param $to
     * @param $includeDoneOrders
     * @return array
     */
    public function actionCreateInvoice($contract, $from, $to, $includeDoneOrders)
    {
        $response = [
            'status' => 'fail',
            'message' => '',
        ];

        $payment_due_date = Yii::$app->request->post('payment_due_date');
        if ($payment_due_date) {
            $payment_due_date = strtotime($payment_due_date);
        }
        $requisite_delivery_id = Yii::$app->request->post('requisite_delivery_id')  ;

        $reportForm = new ReportFormInvoice(['includeDoneOrders' => $includeDoneOrders]);
        $post = Yii::$app->request->post();
        $reportForm->load($post);
        try {
            $reportForm->createInvoice($contract, $from, $to, $payment_due_date, $requisite_delivery_id, $post);
            $response['status'] = 'success';
            $response['message'] = Yii::t('common', 'Инвойс успешно создан, ожидайте генерации файлов.');
            if ($post) {
                $response['finance_table_content'] = $this->renderPartialFinanceTable($post);
                $response['invoice_table_content'] = $this->renderPartialInvoicesTable($post);
            }
        } catch (\Throwable $e) {
            $response['message'] = $e->getMessage();
        }
        return $response;
    }

    /**
     * @param $id
     * @return array
     */
    public function actionRegenerateInvoice($id)
    {
        $response = [
            'status' => 'fail',
            'message' => '',
        ];
        $invoice = $this->findInvoice($id);

        if (!in_array($invoice->status, [Invoice::STATUS_GENERATED])) {
            $response['message'] = Yii::t('common', 'Перегенерация инвойса {name} в текущем статусе невозможна.');
        } else {
            $invoice->status = Invoice::STATUS_QUEUE;
            if (!$invoice->save()) {
                $response['message'] = $invoice->getFirstErrorAsString();
            } else {
                $response['invoice_table_content'] = $this->renderPartialInvoicesTable(Yii::$app->request->get());
                $response['status'] = 'success';
            }
        }

        return $response;
    }

    /**
     * @param $id
     * @return array
     */
    public function actionApproveInvoice($id)
    {
        $response = [
            'status' => 'fail',
            'message' => '',
        ];

        try {
            $invoice = $this->findInvoice($id);
            if (!in_array($invoice->status, [Invoice::STATUS_GENERATED])) {
                $response['message'] = Yii::t('common', 'Подтверждение инвойса {name} в текущем статусе невозможно.');
            } else {
                $invoice->status = Invoice::STATUS_APPROVED;
                if (!$invoice->save()) {
                    $response['message'] = $invoice->getFirstErrorAsString();
                } else {
                    $response['invoice_table_content'] = $this->renderPartialInvoicesTable(Yii::$app->request->get());
                    $response['status'] = 'success';
                }
            }
        } catch (\Throwable $e) {
            $response['message'] = $e->getMessage();
        }

        return $response;
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionDeleteInvoice($id)
    {
        $invoice = $this->findInvoice($id);
        $name = $invoice->name;
        if ($invoice->delete()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Инвойс {name} успешно удален.', ['name' => $name]), 'success');
        } else {
            Yii::$app->notifier->addNotificationsByModel($invoice);
        }
        $queryParams = Yii::$app->request->queryParams;
        unset($queryParams['id']);
        return $this->redirect(Url::toRoute(array_merge($queryParams, ['index'])));
    }

    /**
     * @param $id
     * @param $type
     * @return $this | \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionDownloadInvoice($id, $type)
    {
        $invoice = $this->findInvoice($id);

        if (!in_array($type, array_keys(Invoice::getFileTypes()))) {
            throw new NotFoundHttpException(Yii::t('common', 'Указан неизвестный тип файла инвойса.'), 404);
        }

        $field = Invoice::getFileTypes()[$type];
        $dir = rtrim(trim(Invoice::getUploadPath($invoice->$field)), DIRECTORY_SEPARATOR);
        $path = $dir . DIRECTORY_SEPARATOR . $invoice->$field;
        if (empty($invoice->$field) || !file_exists($path)) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Файлы для инвойса {name} не были сгенерированы.', ['name' => $invoice->name]), 'danger');
            $queryParams = Yii::$app->request->queryParams;
            unset($queryParams['id']);
            unset($queryParams['type']);
            return $this->redirect(Url::toRoute(array_merge($queryParams, ['index'])));
        }

        $info = pathinfo($invoice->$field);
        return Yii::$app->response->sendFile($path, $invoice->name . (!empty($info['extension']) ? '.' . $info['extension'] : ''));
    }

    /**
     * @return array
     * @throws \yii\db\Exception
     */
    public function actionCurrencyRate()
    {
        $response = [
            'status' => 'fail',
            'message' => '',
            'rate' => '',
        ];
        $currency_id = Yii::$app->request->get('currency_id');
        $invoiceCurrencyId = Yii::$app->request->get('invoice_currency_id');
        $date = Yii::$app->formatter->asTimestamp(Yii::$app->request->get('date'));

        if (empty($currency_id) || empty($invoiceCurrencyId)) {
            $response['status'] = 'success';
            return $response;
        }

        $rate = Yii::$app->db->createCommand('SELECT convert_value_to_currency_by_date(1, :currency_from, :currency_to, :date)', ['currency_from' => $invoiceCurrencyId, 'currency_to' => $currency_id, 'date' => empty($date) ? null : date('Y-m-d', $date)])->queryScalar();
        if (empty($rate)) {
            $response['status'] = 'fail';
            $response['message'] = Yii::t('common', 'Не удается определить курс валют для выбранной даты');
        } else {
            $response['rate'] = $rate;
            $response['status'] = 'success';
        }

        return $response;
    }

    /**
     * @return array
     */
    public function actionReloadInvoiceList()
    {
        $response = [
            'status' => 'success',
            'invoice_table_content' => $this->renderPartialInvoicesTable(Yii::$app->request->get()),
            'message' => '',
        ];

        return $response;
    }

    public function actionCloseInvoice($id)
    {
        $invoice = $this->findInvoice($id);
        $queryParams = Yii::$app->request->queryParams;
        if (Yii::$app->request->get('get_invoice_information') && Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $response = [
                'status' => 'fail',
                'message' => '',
                'content' => '',
            ];
            try {
                $widget = new InvoiceModalClose(['invoice' => $invoice]);
                $response['content'] = $widget->renderSettingsBlock();
                $response['status'] = 'success';
            } catch (\Throwable $e) {
                $response['message'] = $e->getMessage();
            }
            return $response;
        }
        $response = [
            'status' => 'fail',
            'message' => '',
        ];
        try {
            $invoice->close(Yii::$app->request->post(), false);
            $response['message'] = Yii::t('common', 'Инвойс {name} отправлен в очередь на закрытие.', ['name' => $invoice->name]);
            $response['status'] = 'success';
        } catch (\Throwable $e) {
            $response['message'] = $e->getMessage();
        }
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $response;
        } else {
            if ($response['status'] != 'success') {
                Yii::$app->notifier->addNotification($response['message']);
            } else {
                Yii::$app->notifier->addNotification($response['message'], Notifier::TYPE_SUCCESS);
            }
        }
        unset($queryParams['id']);
        return $this->redirect(Url::toRoute(array_merge($queryParams, ['index'])));
    }

    /**
     * @param $id
     * @return array|Response
     */
    public function actionSendInvoice($id)
    {
        $invoice = $this->findInvoice($id);


        Invoice::generateInvoiceByRequisite($invoice);

        $queryParams = Yii::$app->request->queryParams;
        if (Yii::$app->request->get('get_invoice_information') && Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $response = [
                'status' => 'fail',
                'message' => '',
                'content' => '',
            ];
            try {
                $widget = new InvoiceModalEmailSender(['invoice' => $invoice]);
                $response['content'] = $widget->renderSettingsBlock();
                $response['status'] = 'success';
            } catch (\Throwable $e) {
                $response['message'] = $e->getMessage();
            }
            return $response;
        }

        $response = [
            'status' => 'fail',
            'message' => '',
        ];
        try {
            if (!Yii::$app->request->post('orders_file') && !Yii::$app->request->post('invoice_file')) {
                throw new \Exception(Yii::t('common', 'Укажите файлы для отправки.'));
            }
            if (!Yii::$app->request->post('emails')) {
                throw new \Exception(Yii::t('common', 'Выберите e-mail\'ы для отправки.'));
            }
            $invoice->sendToEmail(Yii::$app->request->post('emails'), Yii::$app->request->post('invoice_file'), Yii::$app->request->post('orders_file'));
            $response['message'] = Yii::t('common', 'Инвойс {name} был успешно отправлен.', ['name' => $invoice->name]);
            $response['status'] = 'success';
        } catch (\Throwable $e) {
            $response['message'] = $e->getMessage();
        }
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $response;
        } else {
            if ($response['status'] != 'success') {
                Yii::$app->notifier->addNotification($response['message']);
            } else {
                Yii::$app->notifier->addNotification($response['message'], Notifier::TYPE_SUCCESS);
            }
        }
        unset($queryParams['id']);
        return $this->redirect(Url::toRoute(array_merge($queryParams, ['index'])));
    }

    /**
     * @param $id
     * @return null|Invoice
     * @throws NotFoundHttpException
     */
    protected function findInvoice($id)
    {
        $model = Invoice::findOne($id);

        if (!$model) {
            throw new NotFoundHttpException(Yii::t('common', 'Инвойс не найден.'), 404);
        }

        return $model;
    }

    /**
     * @param $queryParams
     * @return string
     */
    protected function renderPartialInvoicesTable($queryParams)
    {
        $searchFormInvoices = new ReportFormInvoiceList();
        $dataProvider = $searchFormInvoices->apply($queryParams);
        LinkPager::setPageSize($dataProvider->pagination);
        return $this->renderPartial('_index-invoices-table', [
            'dataProvider' => $dataProvider,
            'reportForm' => $searchFormInvoices,
            'queryParams' => $queryParams,
        ]);
    }

    /**
     * @param $queryParams
     * @return string
     */
    protected function renderPartialFinanceTable($queryParams)
    {
        $reportForm = new ReportFormInvoice();
        $result = $reportForm->apply($queryParams);
        return $this->renderPartial('_index-finance-table', [
            'reportForm' => $reportForm,
            'allData' => $result['output'],
            'delivery' => $result['delivery'],
        ]);
    }

    /**
     * @return array
     */
    public function actionGetFinanceReports()
    {
        $reportForm = new ReportFormInvoice();
        $reportForm->delivery_id = yii::$app->request->post('delivery_id');
        $reportForm->country_id = yii::$app->request->post('country_id');

        return $reportForm->getFinanceReportsCollection();
    }
}