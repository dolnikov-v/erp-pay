<?php
namespace app\modules\report\controllers;

use app\components\web\Controller;
use app\models\CurrencyRate;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\order\models\Order;
use app\modules\order\models\OrderFinance;
use app\modules\report\components\ReportFormDeliveryClosedPeriods;
use Yii;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;

/**
 * Class DeliveryClosedPeriodsController
 * @package app\modules\report\controllers
 */
class DeliveryClosedPeriodsController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $reportForm = new ReportFormDeliveryClosedPeriods();
        $dataProvider = $reportForm->apply(Yii::$app->request->queryParams);

        return $this->render('index', [
            'reportForm' => $reportForm,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionPaymentPerPeriod()
    {
        $year = Yii::$app->request->post('year');
        $month = Yii::$app->request->post('month');

        $number = cal_days_in_month(CAL_GREGORIAN, $month, $year);

        $month = str_pad($month, 2, '0', STR_PAD_LEFT);
        $number = str_pad($number, 2, '0', STR_PAD_LEFT);

        $date = join('-', [$year, $month,  '01']) . ' 00:00:00';
        $timeFrom = Yii::$app->formatter->asTimestamp($date);

        $date = join('-', [$year, $month, $number]) . ' 00:00:00';
        $timeTo = Yii::$app->formatter->asTimestamp($date);


        $queryPerPeriod = OrderFinance::find()
            ->joinWith(['order', 'order.deliveryRequest', 'order.country.currencyRate'], false)
            ->select([
                'payment' => OrderFinance::tableName() . '.payment',
            ])
            ->where([
                Order::tableName() . '.country_id' => Yii::$app->user->country->id,
            ])
            ->groupBy(['payment'])
            ->asArray()
            ->indexBy('payment');

        $queryExceptPeriod = clone $queryPerPeriod;

        $dataPerPeriod = $queryPerPeriod
            ->addSelect(['amount_per_period' => 'sum(' . Order::tableName(). '.price / ' . CurrencyRate::tableName() . '.rate)',])
            ->andWhere(['>=', DeliveryRequest::tableName() . '.sent_at', $timeFrom])
            ->andWhere(['<', DeliveryRequest::tableName() . '.sent_at', $timeTo])
            ->orderBy(['payment' => SORT_ASC])
            ->all();

        $dataExceptPeriod = $queryExceptPeriod
            ->addSelect(['amount_except_period' => 'sum(' . Order::tableName(). '.price / ' . CurrencyRate::tableName() . '.rate)',])
            ->andWhere([OrderFinance::tableName() . '.payment' => array_keys($dataPerPeriod)])
            ->andFilterWhere(['or',
                ['<', DeliveryRequest::tableName() . '.sent_at', $timeFrom],
                ['>=', DeliveryRequest::tableName() . '.sent_at', $timeTo]
            ])
            ->all();


        $data = [];
        foreach ($dataPerPeriod as $payment => $item) {

            $item['amount_except_period'] = ArrayHelper::getValue($dataExceptPeriod, $payment . '.amount_except_period', 0);
            $data[] = $item;
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'pagination' => false,
        ]);

        return $this->renderPartial('payment-per-period', [
            'dataProvider' => $dataProvider,
        ]);
    }

}
