<?php
namespace app\modules\report\controllers;

use app\components\web\Controller;

/**
 * Class WebmasterController
 * @package app\modules\report\controllers
 */
class TrendsController extends Controller
{
    /**
     * @return string
     */
    public function actionLead()
    {
        return $this->render('index', [
            'title' => 'Тренды по лидам',
            'taskNumber' => 'ERP-990',
        ]);
    }

    /**
     * @return string
     */
    public function actionApprove()
    {
        return $this->render('index', [
            'title' => 'Тренды по апруву',
            'taskNumber' => 'ERP-990',
        ]);
    }

    /**
     * @return string
     */
    public function actionBuyout()
    {
        return $this->render('index', [
            'title' => 'Тренды по выкупу',
            'taskNumber' => 'ERP-990',
        ]);
    }
}
