<?php
namespace app\modules\report\controllers;

use app\components\web\Controller;
use app\models\CurrencyRate;
use app\modules\delivery\components\charges\ChargesCalculatorAbstract;
use app\modules\delivery\models\DeliveryContract;
use app\modules\order\models\Order;
use app\modules\order\models\OrderFinancePrediction;
use app\modules\report\components\filters\DateFilter;
use app\modules\report\components\invoice\ReportFormInvoice;
use app\modules\report\components\ReportFormChargesTest;
use app\widgets\Panel;
use Yii;
use yii\data\ArrayDataProvider;
use yii\db\QueryInterface;
use yii\helpers\ArrayHelper;

/**
 * Class ChargesTestController
 * @package app\modules\delivery\controllers
 */
class ChargesTestController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $reportForm = new ReportFormChargesTest();

        $dataProvider = $reportForm->apply(Yii::$app->request->queryParams);

        $contract = DeliveryContract::findOne($reportForm->getContractFilter()->contract_id);

        $data = [
            'reportForm' => $reportForm,
            'dataProvider' => $dataProvider,
            'predictions' => []
        ];

        if ($contract) {
            $predictions = self::getPredictionData($dataProvider->query, $contract);

            $fields = ReportFormInvoice::getDisplayedFields($contract, $contract->delivery->country->currency_id, $contract->delivery->country->currencyRate->rate);

            $monthlyChargesCount = self::getMonthDiff($reportForm->getDateFilter());

            $totals = self::calcTotals($predictions, $fields, $monthlyChargesCount);

            $data['predictions'] = $predictions;
            $data['totals'] = $totals['totalData'];
            $data['invoiceDataProvider'] = new ArrayDataProvider([
                'allModels' => $totals['rows'],
                'sort' => false,
            ]);
        }
        else {
            $reportForm->panelAlert = Yii::t('common', 'Выберите калькулятор контракта службы доставки');
            $reportForm->panelAlertStyle = Panel::ALERT_DANGER;
        }

        return $this->render('index', $data);
    }

    /**
     * @param QueryInterface $query
     * @param DeliveryContract $contract
     * @return bool|array
     */
    protected function getPredictionData($query, $contract = null)
    {
        if (!$contract) {
            return false;
        }

        if (!$contract->chargesCalculatorModel) {
            return false;
        }

        $return = [];
        foreach ($query->all() as $order) {
            /** @var Order $order */
            $return[$order->id] = $contract->chargesCalculator->calculate($order);
            $return[$order->id]->price_cod_currency_id = $order->price_currency ?: $order->country->currency_id;
        }
        return $return;
    }

    /**
     * @param OrderFinancePrediction[] $predictions
     * @param array $fields
     * @param integer $monthlyChargesCount
     * @return array
     */
    protected function calcTotals($predictions, $fields, $monthlyChargesCount)
    {
        $rates = ArrayHelper::map(CurrencyRate::find()->all(), 'currency_id', 'rate');

        $items = [];
        foreach ($fields as $name => $properties) {
            $items[$name]['count'] = $items[$name]['count'] ?? 0;
            $items[$name]['sum'] = $items[$name]['sum'] ?? 0;
        }

        foreach ($predictions as $prediction) {
            foreach ($items as $key => $total) {
                if (!empty($prediction->$key)) {
                    $keyCurrency = $key . '_currency_id';
                    if (isset($prediction->$keyCurrency)) {
                        $items[$key]['sum'] += (isset($rates[$prediction->$keyCurrency]) && $rates[$prediction->$keyCurrency]) ? $prediction->$key / $rates[$prediction->$keyCurrency] : 0;
                        $items[$key]['count']++;
                    }
                }
            }
        }

        $rows = [];
        $totalData = [
            'total_sum_usd' => 0,
            'orders_count' => sizeof($predictions),
            'total_cod_sum' => 0,
        ];

        foreach ($fields as $name => $properties) {
            if ($properties['calculating_type']['type'] == ChargesCalculatorAbstract::CALCULATING_TYPE_MONTHLY_CHARGE) {
                $items[$name]['sum'] = $items[$name]['sum'] * $properties['calculating_type']['charge'] * $monthlyChargesCount;
            }
            if ($properties['finance_type'] == OrderFinancePrediction::FINANCE_TYPE_INCOME) {
                $totalData['total_cod_sum'] += $items[$name]['sum'];
            }
        }

        foreach ($fields as $name => $properties) {
            if (empty($items[$name]['sum'])) {
                continue;
            }
            if ($name != OrderFinancePrediction::COLUMN_VAT_INCLUDED) {
                // не включаем значения поля "НДС включен" в итоги
                $totalData['total_sum_usd'] += $items[$name]['sum'] * ($properties['finance_type'] == OrderFinancePrediction::FINANCE_TYPE_COST ? -1 : 1);
            }
            $rows[$name] = [
                'usd_sum' => $items[$name]['sum'],
                'label' => $properties['label'],
                'quantity' => '',
                'formula' => '',
                'finance_type' => $properties['finance_type'],
                'calculating_type' => $properties['calculating_type']['type'],
            ];
            if (isset($properties['calculating_type']['type'])) {
                switch ($properties['calculating_type']['type']) {
                    case ChargesCalculatorAbstract::CALCULATING_TYPE_PERCENT:
                        $rows[$name]['quantity'] = $items[$name]['sum'];
                        $rows[$name]['formula'] = $properties['calculating_type']['visible_formula'];
                        break;
                    case ChargesCalculatorAbstract::CALCULATING_TYPE_NUMBER:
                        if ($items[$name]['count']) {
                            $rows[$name]['quantity'] = $items[$name]['count'];
                            $rows[$name]['formula'] = $items[$name]['sum'] / $rows[$name]['quantity'];
                        }
                        break;
                    case ChargesCalculatorAbstract::CALCULATING_TYPE_MONTHLY_CHARGE:
                        $rows[$name]['quantity'] = $monthlyChargesCount;
                        $rows[$name]['formula'] = $properties['calculating_type']['charge'] / ($rates[$properties['calculating_type']] ?? $properties['calculating_type']['currency_rate']);
                        break;
                }
            }
        }

        return [
            'rows' => $rows,
            'totalData' => $totalData,
        ];
    }

    /**
     * @param DateFilter $dateFilter
     * @return int
     */
    public function getMonthDiff($dateFilter)
    {
        if ($dateFilter->from && $dateFilter->to) {
            $dateFrom = new \DateTime($dateFilter->from);
            $dateTo = new \DateTime($dateFilter->to);
            $interval = $dateFrom->diff($dateTo);
            return $interval->m + ($interval->d ? 1 : 0);
        }
        return 1;
    }
}
