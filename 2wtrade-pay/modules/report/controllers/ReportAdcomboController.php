<?php

namespace app\modules\report\controllers;

use app\components\web\Controller;
use app\modules\api\models\LeadStatus;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\order\models\Order;
use app\modules\report\components\ReportAdcomboComparator;
use app\modules\report\components\ReportFormAdcombo;
use app\modules\report\models\ReportAdcombo;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;

/**
 * Class ReportAdcomboController
 * @package app\modules\report\controllers
 */
class ReportAdcomboController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $form = new ReportFormAdcombo();
        $dataProvider = $form->apply(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'reportForm' => $form
        ]);
    }


    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionRecheckRecord($id)
    {
        $model = ReportAdcombo::findOne($id);

        ReportAdcomboComparator::compareWithAdcombo($model);

        $model->save();

        $options = Yii::$app->request->queryParams;
        unset($options['id']);
        return $this->redirect(ArrayHelper::merge(['index'], $options));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws BadRequestHttpException
     */
    public function actionShowErrors($id)
    {
        $type = Yii::$app->request->get('type');
        $model = ReportAdcombo::findOne($id);
        $errors = ReportAdcomboComparator::getCompareErrors($id);

        switch ($type) {
            case ReportAdcombo::SHOW_ERROR_TYPE_TOTAL_ADCOMBO:
                $elements = array_filter($errors, function ($item) {
                    return $item['type'] == ReportAdcombo::ERROR_OUR_ORDER_NOT_FOUND;
                });
                $redirectType = 'adcombo';
                break;
            case ReportAdcombo::SHOW_ERROR_TYPE_CONFIRMED_ADCOMBO:
                $elements = array_filter($errors, function ($item) {
                    return ($item['type'] == ReportAdcombo::ERROR_OUR_ORDER_NOT_FOUND || $item['type'] == ReportAdcombo::ERROR_STATUS_NOT_EQUAL) && $item['foreign_state'] == LeadStatus::STATUS_CONFIRMED;
                });
                $redirectType = 'adcombo';
                break;
            case ReportAdcombo::SHOW_ERROR_TYPE_HOLD_ADCOMBO:
                $elements = array_filter($errors, function ($item) {
                    return ($item['type'] == ReportAdcombo::ERROR_OUR_ORDER_NOT_FOUND || $item['type'] == ReportAdcombo::ERROR_STATUS_NOT_EQUAL) && $item['foreign_state'] == LeadStatus::STATUS_HOLD;
                });
                $redirectType = 'adcombo';
                break;
            case ReportAdcombo::SHOW_ERROR_TYPE_TRASH_ADCOMBO:
                $elements = array_filter($errors, function ($item) {
                    return ($item['type'] == ReportAdcombo::ERROR_OUR_ORDER_NOT_FOUND || $item['type'] == ReportAdcombo::ERROR_STATUS_NOT_EQUAL) && $item['foreign_state'] == LeadStatus::STATUS_TRASH;
                });
                $redirectType = 'adcombo';
                break;
            case ReportAdcombo::SHOW_ERROR_TYPE_CANCELLED_ADCOMBO:
                $elements = array_filter($errors, function ($item) {
                    return ($item['type'] == ReportAdcombo::ERROR_OUR_ORDER_NOT_FOUND || $item['type'] == ReportAdcombo::ERROR_STATUS_NOT_EQUAL) && $item['foreign_state'] == LeadStatus::STATUS_CANCELLED;
                });
                $redirectType = 'adcombo';
                break;
            case ReportAdcombo::SHOW_ERROR_TYPE_TOTAL_OUR:
                $elements = array_filter($errors, function ($item) {
                    return $item['type'] == ReportAdcombo::ERROR_ADCOMBO_ORDER_NOT_FOUND;
                });
                $redirectType = 'our';
                break;
            case ReportAdcombo::SHOW_ERROR_TYPE_CONFIRMED_OUR:
                $elements = array_filter($errors, function ($item) {
                    return ($item['type'] == ReportAdcombo::ERROR_ADCOMBO_ORDER_NOT_FOUND || $item['type'] == ReportAdcombo::ERROR_STATUS_NOT_EQUAL) && $item['order_state'] == LeadStatus::STATUS_CONFIRMED;
                });
                $redirectType = 'our';
                break;
            case ReportAdcombo::SHOW_ERROR_TYPE_HOLD_OUR:
                $elements = array_filter($errors, function ($item) {
                    return ($item['type'] == ReportAdcombo::ERROR_ADCOMBO_ORDER_NOT_FOUND || $item['type'] == ReportAdcombo::ERROR_STATUS_NOT_EQUAL) && $item['order_state'] == LeadStatus::STATUS_HOLD;
                });
                $redirectType = 'our';
                break;
            case ReportAdcombo::SHOW_ERROR_TYPE_TRASH_OUR:
                $elements = array_filter($errors, function ($item) {
                    return ($item['type'] == ReportAdcombo::ERROR_ADCOMBO_ORDER_NOT_FOUND || $item['type'] == ReportAdcombo::ERROR_STATUS_NOT_EQUAL) && $item['order_state'] == LeadStatus::STATUS_TRASH;
                });
                $redirectType = 'our';
                break;
            case ReportAdcombo::SHOW_ERROR_TYPE_CANCELLED_OUR:
                $elements = array_filter($errors, function ($item) {
                    return ($item['type'] == ReportAdcombo::ERROR_ADCOMBO_ORDER_NOT_FOUND || $item['type'] == ReportAdcombo::ERROR_STATUS_NOT_EQUAL) && $item['order_state'] == LeadStatus::STATUS_CANCELLED;
                });
                $redirectType = 'our';
                break;
            case ReportAdcombo::SHOW_ERROR_TYPE_CC:
                $elements = Order::find()->select([Order::tableName() . '.id as order_id'])->joinWith('callCenterRequest')->where([
                    '>=',
                    Order::tableName() . '.created_at',
                    $model->from
                ])->andWhere([
                    '<=',
                    Order::tableName() . '.created_at',
                    $model->to
                ])->andWhere([
                    'is',
                    CallCenterRequest::tableName() . '.id',
                    null
                ])->byCountryId(Yii::$app->user->country->id)->column();
                $redirectType = 'our';
                break;
            default:
                throw new BadRequestHttpException(Yii::t('common', 'Невозможно определить тип запроса.'));
        }

        if (empty($elements)) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Заказы с выбранными параметрами не найдены.'));
            return $this->redirect(['index']);
        }

        if ($redirectType == 'adcombo') {
            Yii::$app->session->set('report-advertising-query-params', [
                'query-type' => 'adcombo',
                's' => [
                    'ids' => implode(',', ArrayHelper::getColumn($elements, 'foreign_id')),
                    'from' => '',
                    'to' => ''
                ]
            ]);
            return $this->redirect(Url::toRoute([
                '/report/advertising/index',
                'query-from-session' => true,
                '#' => 'tab3'
            ]));
        } elseif ($redirectType == 'our') {
            if ($type != ReportAdcombo::SHOW_ERROR_TYPE_CC) {
                $elements = ArrayHelper::getColumn($elements, 'order_id');
            }
            Yii::$app->session->set('order-index-query-params', [
                'NumberFilter' => [
                    'number' => implode(',', $elements),
                    'entity' => 'id'
                ]
            ]);
            return $this->redirect(Url::toRoute([
                '/order/index/index',
                'query-from-session' => true
            ]));
        }

        throw new BadRequestHttpException(Yii::t('common', 'Невозможно определить тип запроса.'));
    }
}
