<?php
namespace app\modules\report\controllers;

use app\components\web\Controller;
use app\modules\report\components\ReportFormWebmaster;
use app\modules\report\components\ReportFormWebmasterAnalytics;
use Yii;

/**
 * Class WebmasterController
 * @package app\modules\report\controllers
 */
class WebmasterController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $reportForm = new ReportFormWebmaster();
        $dataProvider = $reportForm->apply(Yii::$app->request->queryParams);

        return $this->render('index', [
            'reportForm' => $reportForm,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionAnalytics()
    {
        $reportForm = new ReportFormWebmasterAnalytics();
        $dataProvider = $reportForm->apply(Yii::$app->request->queryParams);
        $dataProviderStatus = $reportForm->applyStatus(Yii::$app->request->queryParams);

        return $this->render('analytics', [
            'reportForm' => $reportForm,
            'dataProvider' => $dataProvider,
            'dataProviderStatus' => $dataProviderStatus,
        ]);
    }
}
