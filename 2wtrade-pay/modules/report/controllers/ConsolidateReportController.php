<?php
namespace app\modules\report\controllers;

use app\components\filters\AjaxFilter;
use app\components\web\Controller;
use app\models\Timezone;
use app\modules\profile\controllers\ViewController;
use app\modules\report\components\ReportFormConsolidateInfo;
use app\modules\report\widgets\ConsolidateInfo;
use yii\helpers\ArrayHelper;
use app\models\Country;
use Yii;

/**
 * Class ApproveByCountryController
 * @package app\modules\report\controllers
 */
class ConsolidateReportController extends ViewController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => [
                    'get-country',
                ],
            ],
        ];
    }

    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionIndex()
    {
        $model = $this->findModel();

        $reportForm = new ReportFormConsolidateInfo();

        $reportForm->apply(Yii::$app->request->queryParams);

        if (!empty($reportForm->getCountrySelectFilter()->country_ids)){
            $countries = [];
            foreach($reportForm->getCountrySelectFilter()->country_ids as $countryId){
                $countries[] = Country::find()->where(['id' => $countryId])->one();
            }
        }

        if (empty($countries)){
            $countries = $model->countries;
        }

        $stats = new ConsolidateInfo($countries, strtotime($reportForm->getDailyFilter()->from . ' 00:00:00 GMT'), $model->id);

        return $this->render('index', [
            'model' => $model,
            'reportForm' => $reportForm,
            'stats' => $stats->prepareStats(),
            'timezones' => ArrayHelper::map(Timezone::find()->all(), 'id', 'name'),
        ]);
    }

}
