<?php
namespace app\modules\report\controllers;

use app\components\filters\AjaxFilter;
use app\components\web\Controller;
use app\models\UserCountry;
use app\modules\callcenter\models\CallCenter;
use app\modules\report\components\ReportFormCallCenterOperatorsOnline;
use app\modules\salary\models\Person;
use Yii;

/**
 * Class CallCenterOperatorsOnlineController
 * @package app\modules\report\controllers
 *
 * @property array $default
 */
class CallCenterOperatorsOnlineController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => [
                    'get-call-center',
                    'get-team-lead',
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $inputRequest = [];
        $reportForm = new ReportFormCallCenterOperatorsOnline();

        if (empty(Yii::$app->request->queryParams)) {
            $inputRequest['s'] = $this->getDefault();
        }
        else {
            $inputRequest = Yii::$app->request->queryParams;
        }

        $dataProvider = $reportForm->apply($inputRequest);

        return $this->render('index', [
            'reportForm' => $reportForm,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return string
     */
    public function actionGetCallCenter()
    {
        $response = [
            'status' => 'fail',
        ];

        if (Yii::$app->request->get('office')) {
            $officeId = Yii::$app->request->get('office');

            $callCenters = CallCenter::find()
                ->byOfficeId($officeId)
                ->bySystemUserCountries()
                ->list();

            $teamLeads = Person::find()
                ->active()
                ->byOfficeId($officeId)
                ->bySystemUserCountries()
                ->isTeamLead()
                ->orderBy([Person::tableName() . '.name' => SORT_ASC])
                ->list();

            $response['status'] = 'true';
            $response['centers'] = $callCenters;
            $response['leads'] = $teamLeads;
        }
        return $response;
    }

    /**
     * @return string
     */
    public function actionGetTeamLead()
    {
        $response = [
            'status' => 'fail',
            'message' => '',
        ];

        $teamLeads = [];
        if (Yii::$app->request->get('office')) {
            $officeId = Yii::$app->request->get('office');
            $teamLeads = Person::find()
                ->active()
                ->byOfficeId($officeId)
                ->isTeamLead()
                ->collection();
        }

        $response['status'] = 'true';
        $response['message'] = $teamLeads;

        return $response;
    }

    /**
     * @return array
     */
    private function getDefault () {
        $s = [];
        $s['to'] = date("d.m.Y", time());
        $s['from'] = date("d.m.Y", strtotime($s['to']) - 60*60*24*31);
        $s['type_date_part'] = "day";
        return $s;
    }
}
