<?php

namespace app\modules\report\controllers;

use app\components\filters\AjaxFilter;
use app\components\web\Controller;
use app\models\Country;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryPartner;
use app\modules\delivery\models\DeliveryRequest;
use yii\web\BadRequestHttpException;
use Yii;

/**
 * Class FilterController
 * @package app\modules\report\controllers
 */
class FilterController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => [
                    'get-select-options-delivery',
                    'get-select-options-partners',
                ],
            ],
        ];
    }

    /**
     * @throws \yii\web\HttpException
     * @return string|array
     */
    public function actionGetSelectOptionsDelivery()
    {
        $countryIds = Yii::$app->request->post('country_ids');

        if (!empty($countryIds)) {
            if (!is_array($countryIds)) {
                $countryIds = [$countryIds];
            }
            foreach ($countryIds as $countryId) {
                $country = Country::findOne($countryId);
                if (!$country) {
                    throw new BadRequestHttpException(Yii::t('common', 'Обращение к несуществущей стране.'));
                }
            }

            $deliveries = Delivery::find()
                ->joinWith(['country'])
                ->select([
                    'id' => Delivery::tableName() . '.id',
                    'name' => 'concat(' . Country::tableName() . '.name, " ", ' . Delivery::tableName() . '.name)',
                ])
                ->where(['country_id' => $countryIds])
                ->orderBy('name')
                ->collection();

            if (count($deliveries) > 0) {
                $resultString = '';
                foreach ($deliveries as $key => $value) {
                    $resultString .= '<option value="' . $key . '">' . $value . '</option>';
                }
                return $resultString;
            }
        }

        return [
            'error' => true
        ];
    }

    /**
     * @throws \yii\web\HttpException
     * @return string|array
     */
    public function actionGetSelectOptionsPartners()
    {
        $deliveryIds = Yii::$app->request->post('delivery_ids');
        if (!empty($deliveryIds)) {
            if (!is_array($deliveryIds)) {
                $deliveryIds = [$deliveryIds];
            }
            foreach ($deliveryIds as $deliveryId) {
                $delivery = Delivery::findOne($deliveryId);
                if (!$delivery) {
                    throw new BadRequestHttpException(Yii::t('common', 'Обращение к несуществущей службе доставки.'));
                }
            }

            $partners = DeliveryPartner::find()
                ->where(['delivery_id' => $deliveryIds])
                ->orderBy(['name' => SORT_ASC])
                ->all();

            if (count($partners) > 0) {
                $resultString = '';
                foreach ($partners as $value) {
                    /** @var $value DeliveryPartner */
                    $resultString .= '<option value="' . $value->id . '">' . $value->name . '</option>';
                }
                return $resultString;
            }
        }

        return [
            'error' => true
        ];
    }
}
