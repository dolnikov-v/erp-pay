<?php
namespace app\modules\report\controllers;

use app\components\web\Controller;
use app\models\Country;
use app\modules\order\models\Order;
use app\modules\catalog\models\SmsPollQuestions;
use app\modules\report\components\ReportFormSmsPollHistory;
use app\modules\report\models\SmsPollHistory;
use Yii;
use yii\data\ArrayDataProvider;
use app\modules\rating\models\query\CountryCurators;

/**
 * Class SmsPollHistoryController
 * @package app\modules\report\controllers
 */
class SmsPollHistoryController extends Controller
{
    /**
     * @param Order $order
     * @param SmsPollQuestions $smsPollQuestion
     * @return []
     */
    public static function sendSmsPollOnOrder($order, $smsPollQuestion)
    {
        $response = [
            'status' => SmsPollHistory::HISTORY_STATUS_ERROR
        ];

        //отправка на номера мобильных телефонов
        $phoneTo = !empty($order->customer_mobile) ? $order->customer_mobile : false;

        if (!$phoneTo) {
            $response = [
                'message' => Yii::t('common', 'У заказа №{order_id} отсутствует номер мобильного телефона', ['order_id' => $order->id])
            ];
        } else {
            //Именно так - сначала сохраняем опрос себе (предварительно), чтобы получить ID опроса, который нужен нам для формирования короткой ссылке в смс
            //Сохранить отправку или попытку отправки смс на заказ
            //в Бд пишем оригинал сообщения
            $preResult = self::saveHistory($order, $smsPollQuestion, ['status' => SmsPollHistory::HISTORY_STATUS_NEW, 'code' => 0, 'message' => '']);

            if (!$preResult) {
                $response['message'] = Yii::t('common', 'Не удалось сохранить историю смс опроса. Отправка смс отменена.');
            } else {
                $response['status'] = SmsPollHistory::HISTORY_STATUS_SUCCESS;
                $response['message'] = Yii::t('common', 'Смс поставлены в очередь на отправку');
            }
        }

        return $response;
    }

    /**
     * Запись результатов отправки SMS опроса
     * @param Order $order
     * @param SmsPollQuestions $smsPollQuestion
     * @param [] $result $this->sendSmsPoll
     * @param $id integer | bool
     * @return []
     */
    public static function saveHistory($order, $smsPollQuestion, $response, $id = false)
    {
        if (!isset($response['code'])) {
            $response['code'] = 0;
        }

        if (!$id) {
            $model = new SmsPollHistory();
            $model->order_id = $order->id;
            $model->customer_mobile = $order->customer_mobile;
            $model->question_id = $smsPollQuestion->id;
            $model->question_text = $smsPollQuestion->question_text;
            $model->status = $response['status'];
            $model->api_code = $response['code'];
            $model->api_error = $response['message'];
            $model->created_at = time();
            $model->updated_at = time();
        } else {
            $model = SmsPollHistory::find()->where(['id' => $id])->one();
            $model->question_text = $smsPollQuestion->question_text;
            $model->status = $response['status'];
            $model->api_code = $response['code'];
            $model->api_error = $response['message'];
        }

        if (!$model->save()) {
            return false;
        }

        return $model;
    }

    /**
     * Отправка SMS опросов для выбранных заказов (несколько вопросов за раз)
     * @param integer $orders_id
     * @param integer $questions_id
     * @return [] $response
     */
    public static function sendSmsPollQuestions($orders_id, $questions_id)
    {
        $response = [
            'status' => SmsPollHistory::HISTORY_STATUS_SUCCESS,
            'message' => yii::t('common', 'Отправка СМС опроса прошла успешно')
        ];

        $order = Order::find()
            ->where(['id' => $orders_id])
            ->one();

        $question = SmsPollQuestions::find()
            ->where(['id' => $questions_id])
            ->one();

        //Получить список опросов, которые успешно были отправлены на данный заказ
        $smsPollHistoryByOrder = SmsPollHistory::find()
            ->select(SmsPollHistory::tableName() . '.question_id')
            ->where(['question_id' => $questions_id])
            ->andWhere([SmsPollHistory::tableName() . '.order_id' => $order->id])
            ->andWhere([SmsPollHistory::tableName() . '.status' => SmsPollHistory::HISTORY_STATUS_SUCCESS])
            ->asArray()
            ->one();


        //не отсылать повторные опросы для данного заказа
        if (!is_array($smsPollHistoryByOrder) || !in_array($question->id, $smsPollHistoryByOrder)) {
            $response = self::sendSmsPollonOrder($order, $question);
        }

        return $response;
    }

    public function actionIndex()
    {
        $reportForm = new ReportFormSmsPollHistory();

        if (empty(Yii::$app->request->queryParams)) {
            $inputRequest['s'] = $this->getDefault();
        } else {
            $inputRequest = Yii::$app->request->queryParams;
        }

        $dataProvider = $reportForm->apply($inputRequest);

        $country = Country::findOne(Yii::$app->user->country->id);

        $dataCuratorProvider = new ArrayDataProvider([
            'allModels' => [$country->curatorUser]
        ]);

        return $this->render('index', [
            'reportForm' => $reportForm,
            'dataProvider' => $dataProvider,
            'dataCuratorProvider' => $dataCuratorProvider
        ]);
    }


    /**
     * @return array
     */
    private function getDefault()
    {
        $s = [];
        $s['to'] = date("d.m.Y", time());
        $s['from'] = date("d.m.Y", time());
        $s['type_poll'] = SmsPollHistory::ANSWER_TYPE_ALL;
        return $s;
    }

}