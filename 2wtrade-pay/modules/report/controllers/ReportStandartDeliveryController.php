<?php
namespace app\modules\report\controllers;

use app\components\web\Controller;
use app\modules\report\components\ReportFormStandartDelivery;
use Yii;

/**
 * Class ReportStandartDeliveryController
 * @package app\modules\report\controllers
 */
class ReportStandartDeliveryController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $reportForm = new ReportFormStandartDelivery();

        if (empty(Yii::$app->request->queryParams)) {
            $inputRequest['s'] = $this->getDefault();
        }
        else {
            $inputRequest = Yii::$app->request->queryParams;
        }

        $dataProvider = $reportForm->apply($inputRequest);

        return $this->render('index', [
            'reportForm' => $reportForm,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return array
     */
    private function getDefault() {
        $s = [];
        $s['to'] = date("d.m.Y", time());
        $s['from'] = date("d.m.Y", strtotime($s['to']) - 60*60*24*31);
        return $s;
    }
}
