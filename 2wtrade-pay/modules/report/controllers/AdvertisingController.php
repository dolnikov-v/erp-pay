<?php
namespace app\modules\report\controllers;

use app\components\web\Controller;
use app\modules\report\components\ReportFormAdvertising;
use app\modules\report\components\ReportFormFindAdcombo;
use Yii;
use yii\base\Exception;
use yii\base\InvalidParamException;
use yii\data\ArrayDataProvider;
use app\modules\report\models\ReportAdvertising;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;

/**
 * Class AdvertisingController
 * @package app\modules\report\controllers
 */
class AdvertisingController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $model = new ReportAdvertising();
        $labels = $model->attributeLabels();
        $reportForm = new ReportFormAdvertising();
        $dataProvider = null;
        $dataHistoryProvider = null;
        $daysProvider = null;
        $day = null;

        $findAdcomboForm = new ReportFormFindAdcombo();
        $adcomboOrderProvider = new ArrayDataProvider();

        $queryParams = Yii::$app->request->queryParams;
        if (Yii::$app->request->get('query-from-session') && Yii::$app->session->has('report-advertising-query-params')) {
            $queryParams = Yii::$app->session->get('report-advertising-query-params');
        }

        if (Yii::$app->request->isGet && count($queryParams) > 0) {
            if (isset($queryParams['query-type']) && $queryParams['query-type'] == 'adcombo') {
                $models = [];
                try {
                    $models = $findAdcomboForm->apply($queryParams);
                } catch (InvalidParamException $e) {
                    Yii::$app->notifier->addNotification($e->getMessage());
                } catch (BadRequestHttpException $e) {
                    Yii::$app->notifier->addNotification($e->getMessage());
                }
                $adcomboOrderProvider = new ArrayDataProvider([
                    'allModels' => $models,
                    'pagination' => [
                        'params' => ArrayHelper::merge(Yii::$app->request->queryParams, [
                            '#' => 'tab3'
                        ])
                    ]
                ]);
            }
            $day = isset($queryParams['day']) ? $queryParams['day'] : null;
            $resultHistory = $reportForm->getHistory($queryParams);
            $daysProvider = new ArrayDataProvider([
                'allModels' => $resultHistory['daysArray'],
            ]);

            if ($day) {
                $dataHistoryProvider = new ArrayDataProvider([
                    'allModels' => $resultHistory['dataHistory'][$day]['allModels'],
                ]);
            }
        }

        if (Yii::$app->request->isPost) {
            if ($reportForm->upload()) {
                try {
                    $dataProvider = $reportForm->parse();
                } catch (Exception $e) {
                    Yii::$app->notifier->addNotification($e->getMessage());
                }
            } else {
                Yii::$app->notifier->addNotificationsByModel($reportForm);
            }
        }

        return $this->render('index', [
            'reportForm' => $reportForm,
            'dataProvider' => $dataProvider,
            'dataHistory' => $dataHistoryProvider,
            'daysProvider' => $daysProvider,
            'day' => $day,
            'labels' => $labels,
            'findAdcomboForm' => $findAdcomboForm,
            'adcomboOrderProvider' => $adcomboOrderProvider
        ]);
    }
}
