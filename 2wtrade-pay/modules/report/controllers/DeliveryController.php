<?php

namespace app\modules\report\controllers;

use app\components\web\Controller;
use app\modules\report\components\ReportFormDelivery;
use app\modules\report\components\ReportFormDeliverySummary;
use app\modules\order\models\Order;
use app\modules\report\components\ReportFormDeliveryLastUpdate;
use Yii;

/**
 * Class DeliveryController
 * @package app\modules\report\controllers
 */
class DeliveryController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $reportForm = new ReportFormDelivery();
        $dataProvider = $reportForm->apply(Yii::$app->request->queryParams);

        $updateTimeReportForm = new ReportFormDeliveryLastUpdate();
        $lastUpdateDataProvider = $updateTimeReportForm->apply(Yii::$app->request->queryParams);

        $summaryForm = new ReportFormDeliverySummary();
        $summaryProvider = $summaryForm->apply(Yii::$app->request->queryParams);

        return $this->render('index', [
            'reportForm' => $reportForm,
            'dataProvider' => $dataProvider,
            'updateTimeReportForm' => $updateTimeReportForm,
            'lastUpdateDataProvider' => $lastUpdateDataProvider,
            'summaryProvider' => $summaryProvider,
        ]);
    }

    /**
     * строим список ZIP-кодов по ссылке
     * @return string
     */
    public function actionZip()
    {

        $geoType = Yii::$app->request->get('geoType');
        $geoName = Yii::$app->request->get('geoName');

        $zips = null;
        if ($geoType == ReportFormDelivery::GROUP_BY_COUNTRY) {
            $zips = Order::find()
                ->select(['customer_zip'])
                ->where(['LIKE', Order::tableName() . '.country_id', $geoName])
                ->andWhere(['NOT IN', Order::tableName() . '.source_id', Yii::$app->user->rejectedSources])// Исключаем запрещенные источники заказов
                ->asArray()
                ->distinct()
                ->all();
        }
        if ($geoType == ReportFormDelivery::GROUP_BY_CUSTOMER_DISTRIT) {
            $zips = Order::find()
                ->select(['customer_zip'])
                ->where(['LIKE', Order::tableName() . '.customer_district', $geoName])
                ->andWhere(['NOT IN', Order::tableName() . '.source_id', Yii::$app->user->rejectedSources])// Исключаем запрещенные источники заказов
                ->asArray()
                ->distinct()
                ->all();
        }
        if ($geoType == ReportFormDelivery::GROUP_BY_CUSTOMER_CITY) {
            $zips = Order::find()
                ->select(['customer_zip'])
                ->where(['LIKE', Order::tableName() . '.customer_city', $geoName])
                ->andWhere(['NOT IN', Order::tableName() . '.source_id', Yii::$app->user->rejectedSources])// Исключаем запрещенные источники заказов
                ->asArray()
                ->distinct()
                ->all();
        }

        $zipStr = '';
        $counter = 0;
        foreach ($zips as $zip) {
            if (!empty($zip['customer_zip'])) {
                if ($counter > 0) {
                    $zipStr .= ',';
                }
                $zipStr .= $zip['customer_zip'];
                $counter++;
            }
        }

        return $zipStr;
    }

}
