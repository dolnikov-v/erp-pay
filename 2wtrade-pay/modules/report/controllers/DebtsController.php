<?php

namespace app\modules\report\controllers;

use app\components\filters\AjaxFilter;
use app\components\web\Controller;
use app\jobs\DeliveryDebtsDynamic;
use app\models\User;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\deliveryreport\models\PaymentOrder;
use app\modules\order\models\Order;
use app\modules\report\components\debts\DebtsDaily;
use app\modules\report\components\debts\models\DebtsDailyRequest;
use app\modules\report\components\DeliveryDebtsExcelXlsxBuilder;
use app\modules\report\components\ReportFormDeliveryDebts;
use app\modules\report\components\ReportFormDeliveryDebtsPrediction;
use app\modules\report\components\ReportFormDeliveryDynamicDebts;
use app\modules\report\components\ReportFormDeliveryNewDebts;
use app\modules\report\models\ReportDeliveryDebts;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\db\Query;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;


/**
 * Class AdvertisingController
 * @package app\modules\report\controllers
 */
class DebtsController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => [
                    'get-payments',
                    'dynamic-reload',
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionGetPayments()
    {
        $countryId = Yii::$app->request->get('country_id');
        $deliveryId = Yii::$app->request->get('delivery_id');
        $month = Yii::$app->request->get('month');
        $from = Yii::$app->request->get('from', $month);
        $to = Yii::$app->request->get('to', $month);

        $query = PaymentOrder::find()
            ->joinWith('orderFinanceFacts', false)
            ->joinWith('orderFinanceFacts.order', false)
            ->joinWith('orderFinanceFacts.order.deliveryRequest', false)
            ->where([
                Order::tableName() . '.country_id' => $countryId,
                DeliveryRequest::tableName() . '.delivery_id' => $deliveryId,
            ]);

        if (!Yii::$app->user->isSuperadmin) {
            $query->andWhere([Order::tableName() . '.country_id' => array_keys(User::getAllowCountries())]);
        }

        if (Yii::$app->user->getIsImplant()) {
            $query->andWhere([DeliveryRequest::tableName() . '.delivery_id' => array_keys(Yii::$app->user->getDeliveriesIds())]);
        }

        $query->andWhere([
            'between',
            'COALESCE(' . DeliveryRequest::tableName() . '.second_sent_at, ' . DeliveryRequest::tableName() . '.sent_at, ' . DeliveryRequest::tableName() . '.created_at)',
            strtotime('first day of this month', strtotime($from)),
            strtotime('last day of this month', strtotime($to))
        ]);

        $query->groupBy([PaymentOrder::tableName() . '.id']);
        $query->orderBy([PaymentOrder::tableName() . '.paid_at' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $dataProvider->pagination->pageSize = $dataProvider->totalCount;

        return $this->renderPartial('_modal-payments', [
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $reportForm = new ReportFormDeliveryDebts();
        $dataProvider = $reportForm->apply(Yii::$app->request->queryParams, true);
        $oldTime = ReportDeliveryDebts::find()->select('created_at')->where(['is_old_data' => 0])->scalar();

        $totalCounter = $reportForm->getTotalCounter();

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'reportForm' => $reportForm,
            'errors' => $reportForm->reportsErrors,
            'oldTime' => $oldTime,
            'totalCounterCurrent' => $totalCounter['current'],
            'totalCounter' => $totalCounter['all'],
        ]);
    }

    /**
     * @return string
     */
    public function actionIndexTwo()
    {
        $reportForm = new ReportFormDeliveryNewDebts();
        $dataProvider = $reportForm->apply(Yii::$app->request->queryParams);
        $oldTime = ReportDeliveryDebts::find()->select('created_at')->where(['is_old_data' => 0])->scalar();

        $totalCounter = $reportForm->getTotalCounter();

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'reportForm' => $reportForm,
            'oldTime' => $oldTime,
            'totalCounterCurrent' => $totalCounter['current'],
            'title' => Yii::t('common', 'Взаиморасчеты с КС (по дате передачи в КС)')
        ]);
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function actionDynamic()
    {
        $reportForm = new ReportFormDeliveryDynamicDebts();

        $reportForm->query = new Query(); // фейковый query для работы фильтров
        $params = Yii::$app->request->queryParams;
        if (empty($params)) {
            $params[$reportForm->getCountryDeliverySelectMultipleFilter()
                ->formName()]['country_ids'] = [Yii::$app->user->country->id];
        }
        $reportForm->applyFilters($params);

        $debtsDailyRequest = new DebtsDailyRequest([
            'country_id' => $reportForm->getCountryDeliverySelectMultipleFilter()->country_ids,
            'delivery_id' => $reportForm->getCountryDeliverySelectMultipleFilter()->delivery_ids,
            'date_from' => date('Y-m-d', $reportForm->getMonthFilter()->from),
            'date_to' => date('Y-m-d', $reportForm->getMonthFilter()->to),
        ]);

        $debtsDaily = new DebtsDaily();

        $data = $debtsDaily->getSavedData($debtsDailyRequest);

        $createdAt = $debtsDaily->getCreatedAt($debtsDailyRequest);

        $dataProvider = new ArrayDataProvider([
            'allModels' => $data
        ]);

        return $this->render('dynamic', [
            'reportForm' => $reportForm,
            'dataProvider' => $dataProvider,
            'debtsDailyRequest' => $debtsDailyRequest,
            'createdAt' => $createdAt,
        ]);
    }

    public function actionDynamicReload()
    {

        $debtsDailyRequest = new DebtsDailyRequest([
            'country_id' => Yii::$app->request->get('country_id'),
            'delivery_id' => Yii::$app->request->get('delivery_id'),
            'date_from' => Yii::$app->request->get('from'),
            'date_to' => Yii::$app->request->get('to'),
        ]);

        $reportForm = new ReportFormDeliveryDebts();

        Yii::$app->queue->push(new DeliveryDebtsDynamic([
            'debtsDailyRequest' => $debtsDailyRequest,
            'userId' => Yii::$app->user->identity->id,
            'backUrl' => Url::to([
                '/report/debts/dynamic/',
                $reportForm->formName() => [
                    'from' => Yii::$app->formatter->asMonth($debtsDailyRequest->date_from),
                    'to' => Yii::$app->formatter->asMonth($debtsDailyRequest->date_to),
                    'country_ids' => $debtsDailyRequest->country_id,
                    'delivery_ids' => $debtsDailyRequest->delivery_id,
                ]
            ])
        ]));

        return [
            'status' => 'success',
            'message' => Yii::t('common', 'Запущен пересчет данных по выбранному фильтру. По окончании расчетов вы получите уведомление, обновите страницу')
        ];
    }

    /**
     * @return string
     */
    public function actionPredictionDebts()
    {
        if (!yii::$app->user->isSuperadmin) {
            return $this->redirect(['/report/debts-old/index']);
        }

        $reportForm = new ReportFormDeliveryDebtsPrediction();
        $dataProvider = $reportForm->apply(Yii::$app->request->queryParams);

        return $this->render('prediction-debts', [
            'dataProvider' => $dataProvider,
            'reportForm' => $reportForm,
        ]);
    }

    public function actionExport()
    {
        $reportForm = new ReportFormDeliveryDebts();
        $dataProvider = $reportForm->apply(Yii::$app->request->queryParams);
        $exporter = new DeliveryDebtsExcelXlsxBuilder($dataProvider->getModels());
        $exporter->sendFile();
    }

    /**
     * @param $id
     * @return null|ReportDeliveryDebts
     * @throws BadRequestHttpException
     */
    protected function findModel($id)
    {
        $model = ReportDeliveryDebts::findOne($id);
        if (is_null($model)) {
            throw new BadRequestHttpException("Запись отсутствует.");
        }
        return $model;
    }
}
