<?php

namespace app\modules\report\controllers;

use app\components\web\Controller;
use app\modules\report\components\ReportFormDebtsInvoice;
use Yii;


/**
 * Class DebtsInvoiceController
 * @package app\modules\report\controllers
 */
class DebtsInvoiceController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $reportForm = new ReportFormDebtsInvoice();
        $dataProvider = $reportForm->apply(Yii::$app->request->queryParams);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'reportForm' => $reportForm,
        ]);
    }
}