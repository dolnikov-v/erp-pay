<?php
namespace app\modules\report\controllers;

use app\components\web\Controller;
use app\modules\report\components\ReportFormWorkflow;
use Yii;

/**
 * Class ReportWorkflowController
 * @package app\modules\report\controllers
 */
class ReportWorkflowController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $reportForm = new ReportFormWorkflow();
        $dataProviderToCallCenter = $reportForm->apply(Yii::$app->request->queryParams);
        $dataProviderToDelivery = $reportForm->applyToDelivery(Yii::$app->request->queryParams);
        $dataProviderToDeliveryCallCenterApproved = $reportForm->applyToDeliveryCallCenterApproved(Yii::$app->request->queryParams);
        $dataProviderToDeliveryApproved = $reportForm->applyToDeliveryApproved(Yii::$app->request->queryParams);
        $dataProviderToDeliveryReturned = $reportForm->applyToDeliveryReturned(Yii::$app->request->queryParams);
        $dataProviderToDeliveryPaid = $reportForm->applyToDeliveryPaid(Yii::$app->request->queryParams);

        $data = $reportForm->getDataGraph($dataProviderToCallCenter,
            $dataProviderToDelivery,
            $dataProviderToDeliveryCallCenterApproved,
            $dataProviderToDeliveryApproved,
            $dataProviderToDeliveryReturned,
            $dataProviderToDeliveryPaid
        );

        return $this->render('index', [
            'reportForm' => $reportForm,
            'dataProviderToCallCenter' => $dataProviderToCallCenter,
            'dataProviderToDelivery' => $dataProviderToDelivery,
            'dataProviderToDeliveryCallCenterApproved' => $dataProviderToDeliveryCallCenterApproved,
            'dataProviderToDeliveryApproved' => $dataProviderToDeliveryApproved,
            'dataProviderToDeliveryReturned' => $dataProviderToDeliveryReturned,
            'dataProviderToDeliveryPaid' => $dataProviderToDeliveryPaid,
            'countries' => $data['countries'],
            'series' => $data['series'],
        ]);
    }
}
