<?php
namespace app\modules\report\controllers;

use app\components\web\Controller;
use app\modules\report\components\ReportFormForecastDelivery;
use Yii;

/**
 * Class ForecastDeliveryController
 * @package app\modules\report\controllers
 */
class ForecastDeliveryController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $reportForm = new ReportFormForecastDelivery();
        $dataArray = $reportForm->apply(Yii::$app->request->queryParams);

        return $this->render('index', [
            'reportForm' => $reportForm,
            'dataProvider' => $dataArray['dataProvider'],
            'series' => $dataArray['series'],
            'intervals' => $dataArray['intervals'],
        ]);
    }
}