<?php
namespace app\modules\report\controllers;

use app\modules\profile\controllers\ViewController;
use app\modules\report\components\ReportFormConsolidateSale;
use Yii;

/**
 * Class ConsolidateSaleController
 * @package app\modules\report\controllers
 */
class ConsolidateSaleController extends ViewController
{
    /**
     * @return string
     */
    public function actionIndex()
    {

        set_time_limit(300);

        $reportForm = new ReportFormConsolidateSale();

        if (empty(Yii::$app->request->queryParams))
            $inputRequest['s'] = $this->getDefault();
        else
            $inputRequest = Yii::$app->request->queryParams;

        $dataProvider = $reportForm->apply($inputRequest);

        return $this->render('index', [
            'reportForm' => $reportForm,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * @return array
     */
    private function getDefault() {
        $s = [];
        $s['to'] = date("d.m.Y", time());
        $s['from'] = date("d.m.Y", strtotime("-7 days"));
        $s['type'] = "c_sent_at";
        return $s;
    }
}
