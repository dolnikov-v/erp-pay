<?php
namespace app\modules\report\controllers;

use app\components\web\Controller;
use app\modules\report\components\ReportFormTimeSheet;
use Yii;

/**
 * Class TimeSheetController
 * @package app\modules\report\controllers
 */
class TimeSheetController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $reportForm = new ReportFormTimeSheet();
        $dataProvider = $reportForm->apply(Yii::$app->request->queryParams);

        $data = $dataProvider->allModels['data'];
        $dates = $dataProvider->allModels['dates'];
        $dataProvider->allModels = $data;

        return $this->render('index', [
            'reportForm' => $reportForm,
            'dataProvider' => $dataProvider,
            'dates' => $dates,
        ]);
    }
}
