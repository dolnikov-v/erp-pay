<?php
namespace app\modules\report\controllers;

use app\components\web\Controller;
use app\modules\report\components\ReportFormPurchaseStorage;
use Yii;

/**
 * Class PurchaseStorageController
 * @package app\modules\report\controllers
 */
class PurchaseStorageController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $reportForm = new ReportFormPurchaseStorage();
        $dataProvider = $reportForm->apply(Yii::$app->request->queryParams);

        return $this->render('index', [
            'reportForm' => $reportForm,
            'dataProvider' => $dataProvider,
        ]);
    }
}