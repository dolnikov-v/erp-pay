<?php
namespace app\modules\report\controllers;

use app\components\web\Controller;
use app\modules\report\components\ReportFormForecastWorkloadOperatorsByHours;
use Yii;

/**
 * Class ForecastLeadByWeekDaysController
 * @package app\modules\report\controllers
 */
class ForecastWorkloadOperatorsByHoursController extends Controller
{

    /**
     * @return string
     */
    public function actionIndex()
    {
        $inputRequest = [];

        $reportForm = new ReportFormForecastWorkloadOperatorsByHours();
        if (empty(Yii::$app->request->queryParams)) {
            $inputRequest['s'] = $this->getDefault();
        }
        else {
            $inputRequest = Yii::$app->request->queryParams;
        }

        $dataProvider = $reportForm->apply($inputRequest);

        return $this->render('index', [
            'reportForm' => $reportForm,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return array
     */
    private function getDefault () {
        $s = [];
        $s['to'] = date("d.m.Y", time());
        $s['from'] = date("d.m.Y", strtotime($s['to']) - 60*60*24*28);
        $s['country_ids'] = Array(0 => (string)Yii::$app->user->getCountry()->id);

        return $s;
    }

}
