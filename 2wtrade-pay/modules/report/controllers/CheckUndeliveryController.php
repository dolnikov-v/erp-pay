<?php

namespace app\modules\report\controllers;


use app\components\web\Controller;
use app\modules\report\components\ReportFormCheckUndelivery;
use Yii;

/**
 * Class CheckUndeliveryController
 * @package app\modules\report\controllers
 */
class CheckUndeliveryController extends Controller
{
    public function actionIndex()
    {
        $reportForm = new ReportFormCheckUndelivery();
        $dataProvider = $reportForm->apply(Yii::$app->request->queryParams);

        return $this->render('index', [
            'reportForm' => $reportForm,
            'dataProvider' => $dataProvider
        ]);
    }
}
