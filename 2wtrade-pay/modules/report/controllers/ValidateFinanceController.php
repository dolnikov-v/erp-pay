<?php
namespace app\modules\report\controllers;

use app\components\web\Controller;
use app\helpers\Utils;
use app\modules\order\models\Order;
use app\modules\report\components\ReportFormValidateFinance;
use app\components\filters\AjaxFilter;
use app\modules\report\models\ReportValidateFinance;
use app\modules\report\models\ReportValidateFinanceData;
use Yii;
use yii\helpers\Url;

/**
 * Class ValidateFinanceController
 * @package app\modules\report\controllers
 */
class ValidateFinanceController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => [
                    'load',
                    'parse'
                ],
            ]
        ];
    }

    /**
     * @var ReportFormValidateFinance
     */
    public $reportForm;

    /**
     * @var ReportValidateFinance
     */
    public $reportModel;


    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->reportForm = new ReportFormValidateFinance();
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionStatus($id = null)
    {
        $reportValidateFinance = ReportValidateFinance::findOne($id);

        if ($reportValidateFinance) {

            $statusMap = json_decode($reportValidateFinance->status_map, true);
            $map = [];
            $post = Yii::$app->request->post('satatus');
            if (isset($statusMap['fileStatus'])) {
                $i = 0;
                foreach ($statusMap['fileStatus'] as $fileStatus) {
                    $map[$fileStatus] = $post[$i] ?? 0;
                    $i++;
                }
            }
            $statusMap['map'] = $map;

            $reportValidateFinance->status_map = json_encode($statusMap);
            $reportValidateFinance->save(false, ['status_map']);

            $dataToSave = ReportValidateFinanceData::findAll(['file_id' => $id]);

            foreach ($dataToSave as $data) {

                if (!isset($map[$data->status_file])) {
                    $data->error = $data->error . ' ' . Yii::t('common', 'Статус не найден');
                } else {
                    if ((int)$data->status == (int)$map[$data->status_file]) {
                        // статус не менялся
                    } else {
                        $order = Order::findOne($data->order_id);
                        if ($order) {
                            if ($order->canChangeStatusTo((int)$map[$data->status_file])) {
                                // статус можно поменять
                            } else {
                                $data->error = $data->error . ' ' . Yii::t('common', 'Неразрешенное значение статуса') . ' (' . $order->status_id . '->' . (int)$map[$data->status_file] . ')';
                            }
                        }
                    }
                }
                $data->save(false, ['error']);
            }

            return $this->redirect(Url::toRoute('index?file=' . $id));
        } else {
            return $this->redirect(Url::toRoute('index'));
        }
    }

    /**
     * @param integer $file
     * @return string
     */
    public function actionIndex($file = null)
    {
        $reportValidateFinance = ReportValidateFinance::findOne($file);

        if ($reportValidateFinance) {
            $dataArray = $this->reportForm->getHistoryOne($file);
            return $this->render('index', [
                'reportForm' => $this->reportForm,
                'dataHistory' => $dataArray['dataHistory'],
                'sumsArray' => $dataArray['sumsArray'],
                'file' => $file,
                'model' => $reportValidateFinance,
            ]);
        } else {
            $dataHistory = $this->reportForm->apply(Yii::$app->request->queryParams);
            return $this->render('index', [
                'reportForm' => $this->reportForm,
                'dataHistory' => $dataHistory,
                'file' => $file,
            ]);
        }
    }

    /**
     * @return array
     */
    public function actionLoad()
    {
        if ($this->reportForm->upload()) {
            return [
                'status' => 'success',
                'file' => $this->reportForm->getFileId(),
            ];
        }
        return [
            'status' => 'fail',
            'message' => implode(',', $this->reportForm->getErrorsAsArray())
        ];
    }

    /**
     * @return array
     */
    public function actionParse()
    {
        return $this->reportForm->parse(Yii::$app->request->post('file'), Yii::$app->request->post('currentStr'));
    }


    /**
     * Экспорт в эксель
     */
    public function actionExportToExcel()
    {

        $file = Yii::$app->request->get('file');
        $type = Yii::$app->request->get('type');

        $reportValidateFinance = ReportValidateFinance::findOne($file);

        if ($reportValidateFinance) {
            $dataArray = $this->reportForm->getHistoryOne($file);
            $dataProvider = $dataArray['dataHistory'];

            $excel = new \PHPExcel();
            $sheet = $excel->getSheet(0);
            $sheet->setTitle(Yii::t('common', 'Сверка фин.отчета'));

            $row = 1;
            $sheet->setCellValueByColumnAndRow(0, $row, Yii::t('common', 'Номер заказа'));
            $sheet->setCellValueByColumnAndRow(1, $row, Yii::t('common', 'Номер заказа (файл)'));
            $sheet->setCellValueByColumnAndRow(2, $row, Yii::t('common', 'Трек номер'));
            $sheet->setCellValueByColumnAndRow(3, $row, Yii::t('common', 'Трек номер (файл)'));
            $sheet->setCellValueByColumnAndRow(4, $row, Yii::t('common', 'Процент от заказа для КС'));
            $sheet->setCellValueByColumnAndRow(5, $row, Yii::t('common', 'Процент от заказа для КС (файл)'));
            $sheet->setCellValueByColumnAndRow(6, $row, Yii::t('common', 'Стоимость фулфиллмента'));
            $sheet->setCellValueByColumnAndRow(7, $row, Yii::t('common', 'Стоимость фулфиллмента (файл)'));
            $sheet->setCellValueByColumnAndRow(8, $row, Yii::t('common', 'НДС на доставку'));
            $sheet->setCellValueByColumnAndRow(9, $row, Yii::t('common', 'НДС на доставку (файл)'));
            $sheet->setCellValueByColumnAndRow(10, $row, Yii::t('common', 'НДС на фулфиллмент'));
            $sheet->setCellValueByColumnAndRow(11, $row, Yii::t('common', 'НДС на фулфиллмент (файл)'));
            $sheet->setCellValueByColumnAndRow(12, $row, Yii::t('common', 'Стоимость доставки'));
            $sheet->setCellValueByColumnAndRow(13, $row, Yii::t('common', 'Стоимость доставки (файл)'));
            $sheet->setCellValueByColumnAndRow(14, $row, Yii::t('common', 'Общая сумма'));
            $sheet->setCellValueByColumnAndRow(15, $row, Yii::t('common', 'Общая сумма (файл)'));
            $sheet->setCellValueByColumnAndRow(16, $row, Yii::t('common', 'Статус'));
            $sheet->setCellValueByColumnAndRow(17, $row, Yii::t('common', 'Статус (файл)'));
            if ($type == 'error') {
                $sheet->setCellValueByColumnAndRow(18, $row, Yii::t('common', 'Текст ошибки'));
            }

            $sheet->getStyleByColumnAndRow(0, $row, 18, $row)->getFont()->setBold(true);
            $row++;

            $sum_delivery_cost_percent = 0;
            $sum_delivery_cost_percent_file = 0;
            $sum_fulfillment_cost = 0;
            $sum_fulfillment_cost_file = 0;
            $sum_nds = 0;
            $sum_nds_file = 0;
            $sum_fulfillment_nds = 0;
            $sum_fulfillment_nds_file = 0;
            $sum_delivery_price = 0;
            $sum_delivery_price_file = 0;
            $sum_total_price = 0;
            $sum_total_price_file = 0;
            foreach ($dataProvider->allModels as $item) {
                if ($type == 'correct' && !empty($item['error'])) {
                    continue;
                }
                if ($type == 'error' && empty($item['error'])) {
                    continue;
                }
                $sheet->setCellValueByColumnAndRow(0, $row, $item['order_id']);
                $sheet->setCellValueByColumnAndRow(1, $row, $item['order_id_file']);
                $sheet->setCellValueByColumnAndRow(2, $row, $item['tracking']);
                $sheet->setCellValueByColumnAndRow(3, $row, $item['tracking_file']);
                $sheet->setCellValueByColumnAndRow(4, $row, $item['delivery_cost_percent']);
                $sheet->setCellValueByColumnAndRow(5, $row, $item['delivery_cost_percent_file']);
                $sheet->setCellValueByColumnAndRow(6, $row, $item['fulfillment_cost']);
                $sheet->setCellValueByColumnAndRow(7, $row, $item['fulfillment_cost_file']);
                $sheet->setCellValueByColumnAndRow(8, $row, $item['nds']);
                $sheet->setCellValueByColumnAndRow(9, $row, $item['nds_file']);
                $sheet->setCellValueByColumnAndRow(10, $row, $item['fulfillment_nds']);
                $sheet->setCellValueByColumnAndRow(11, $row, $item['fulfillment_nds_file']);
                $sheet->setCellValueByColumnAndRow(12, $row, $item['delivery_price']);
                $sheet->setCellValueByColumnAndRow(13, $row, $item['delivery_price_file']);
                $sheet->setCellValueByColumnAndRow(14, $row, $item['total_price']);
                $sheet->setCellValueByColumnAndRow(15, $row, $item['total_price_file']);
                $sheet->setCellValueByColumnAndRow(16, $row, $item['status']);
                $sheet->setCellValueByColumnAndRow(17, $row, $item['status_file']);
                if ($type == 'error') {
                    $sheet->setCellValueByColumnAndRow(18, $row, $item['error']);
                }

                $sum_delivery_cost_percent += $item['delivery_cost_percent'];
                $sum_delivery_cost_percent_file += $item['delivery_cost_percent_file'];
                $sum_fulfillment_cost += $item['fulfillment_cost'];
                $sum_fulfillment_cost_file += $item['fulfillment_cost_file'];
                $sum_nds += $item['nds'];
                $sum_nds_file += $item['nds_file'];
                $sum_fulfillment_nds += $item['fulfillment_nds'];
                $sum_fulfillment_nds_file += $item['fulfillment_nds_file'];
                $sum_delivery_price += $item['delivery_price'];
                $sum_delivery_price_file += $item['delivery_price_file'];
                $sum_total_price += $item['total_price'];
                $sum_total_price_file += $item['total_price_file'];

                $row++;
            }

            $row++;
            $sheet->setCellValueByColumnAndRow(0, $row, '-');
            $sheet->setCellValueByColumnAndRow(1, $row, '-');
            $sheet->setCellValueByColumnAndRow(2, $row, '-');
            $sheet->setCellValueByColumnAndRow(3, $row, '-');
            $sheet->setCellValueByColumnAndRow(4, $row, $sum_delivery_cost_percent);
            $sheet->setCellValueByColumnAndRow(5, $row, $sum_delivery_cost_percent_file);
            $sheet->setCellValueByColumnAndRow(6, $row, $sum_fulfillment_cost);
            $sheet->setCellValueByColumnAndRow(7, $row, $sum_fulfillment_cost_file);
            $sheet->setCellValueByColumnAndRow(8, $row, $sum_nds);
            $sheet->setCellValueByColumnAndRow(9, $row, $sum_nds_file);
            $sheet->setCellValueByColumnAndRow(10, $row, $sum_fulfillment_nds);
            $sheet->setCellValueByColumnAndRow(11, $row, $sum_fulfillment_nds_file);
            $sheet->setCellValueByColumnAndRow(12, $row, $sum_delivery_price);
            $sheet->setCellValueByColumnAndRow(13, $row, $sum_delivery_price_file);
            $sheet->setCellValueByColumnAndRow(14, $row, $sum_total_price);
            $sheet->setCellValueByColumnAndRow(15, $row, $sum_total_price_file);
            $sheet->setCellValueByColumnAndRow(16, $row, '-');
            $sheet->setCellValueByColumnAndRow(17, $row, '-');
            if ($type == 'error') {
                $sheet->setCellValueByColumnAndRow(18, $row, '-');
            }
            $sheet->getStyleByColumnAndRow(0, $row, 18, $row)->getFont()->setBold(true);

            $dir = Yii::getAlias("@runtime") .DIRECTORY_SEPARATOR ."validate-finance";
            Utils::prepareDir($dir);
            $endFileName = 'corrects';
            if ($type == 'error') {
                $endFileName = 'errors';
            }

            $filename = $dir .DIRECTORY_SEPARATOR .date('Y-m-d', time()) .'_' .Yii::t('common', 'validate-finance_') .$endFileName .".xlsx";

            $objWriter = \PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
            $objWriter->save($filename);

            Yii::$app->response->sendFile($filename);

        }
    }

}
