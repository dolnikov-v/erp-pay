<?php
namespace app\modules\report\controllers;

use app\components\web\Controller;
use app\modules\report\components\ReportFormTopProduct;
use Yii;

/**
 * Class TopProductController
 * @package app\modules\report\controllers
 */
class TopProductController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $reportFormTopProduct = new ReportFormTopProduct();
        if (empty(Yii::$app->request->queryParams)) {
            $dataProvider = $reportFormTopProduct->apply($this->defaultParams());
        } else {
            $dataProvider = $reportFormTopProduct->apply(Yii::$app->request->queryParams);
        }


        return $this->render('index', [
            'reportFormTopProduct' => $reportFormTopProduct,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return array
     */
    private function defaultParams () {
        $s = [];
        $s['to'] = date("d.m.Y", time());
        $s['from'] = date("d.m.Y", strtotime($s['to']) - 60*60*24*31);
        $s['country_ids'] = [0 => (string)Yii::$app->user->getCountry()->id];

        return ['s' => $s];
    }
}
