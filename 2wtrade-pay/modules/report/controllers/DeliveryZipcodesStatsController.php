<?php
namespace app\modules\report\controllers;

use app\components\web\Controller;
use app\modules\report\components\ReportFormDeliveryZipcodesStats;
use Yii;

/**
 * Class DeliveryZipcodesStatsController
 * @package app\modules\report\controllers
 */
class DeliveryZipcodesStatsController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $reportForm = new ReportFormDeliveryZipcodesStats();
        $dataProvider = $reportForm->apply(Yii::$app->request->queryParams);

        return $this->render('index', [
            'reportForm' => $reportForm,
            'dataProvider' => $dataProvider,
        ]);
    }
}
