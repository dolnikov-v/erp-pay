<?php

namespace app\modules\report\controllers;

use app\components\web\Controller;
use app\modules\report\components\ReportFormCompanyStandards;
use app\modules\report\models\CompanyStandards;
use app\modules\report\models\CompanyStandardsDelivery;
use Yii;

/**
 * Class CompanyStandardsController
 * @package app\modules\report\controllers
 */
class CompanyStandardsController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $reportForm = new ReportFormCompanyStandards();
        $dataProvider = $reportForm->apply(Yii::$app->request->queryParams);

        $totalData = $totalCountryData = $total = $totalByCountry = $deliveryDataByMonth = [];
        foreach ($dataProvider->getModels() as $model) {
            /** @var CompanyStandards $model */
            foreach ($model->getAttributes() as $key => $val) {
                if ($val) {
                    $totalData[$key][] = $val;
                    $totalCountryData[$model->country_id][$key][] = $val;
                    if (in_array($key, ['costs', 'income', 'lead_cost', 'call_center_costs', 'product_costs'])) {
                        $valPercent = !isset($model->$key) || empty($model->costs) ? null : round(100 * $model->$key / $model->costs, CompanyStandards::PRECISION);
                        if ($valPercent !== null) {
                            $totalData[$key . '_percent'][] = $valPercent;
                            $totalCountryData[$model->country_id][$key . '_percent'][] = $valPercent;
                        }
                    }
                }
            }
            $deliveryData = [];
            foreach ($model->companyStandardsDelivery as $deliveryStandart) {
                foreach ($deliveryStandart->getAttributes() as $key => $val) {
                    if ($val) {
                        $deliveryData[$key][] = $val;
                    }
                }
            }

            $deliveryDataByMonth[$model->country_id][$model->month]['accepted_time'] = !empty($deliveryData['accepted_time']) ? round((array_sum($deliveryData['accepted_time'] ?? []) / count($deliveryData['accepted_time'])) / 3600, CompanyStandardsDelivery::PRECISION) : null;
            if ($deliveryDataByMonth[$model->country_id][$model->month]['accepted_time']) {
                $totalData['accepted_time'][] = $totalCountryData[$model->country_id]['accepted_time'][] = $deliveryDataByMonth[$model->country_id][$model->month]['accepted_time'];
            }
            $totalData['payment_time'][] = $totalCountryData[$model->country_id]['payment_time'][] = $deliveryDataByMonth[$model->country_id][$model->month]['payment_time'] = !empty($deliveryData['payment_time']) ? round((array_sum($deliveryData['payment_time']) / count($deliveryData['payment_time'])) / 216000, CompanyStandardsDelivery::PRECISION) : null;
        }

        foreach ($totalCountryData as $countryID => $data) {
            $totalByCountry[$countryID] = $this->calculateTotal($data);
        }
        unset($totalCountryData);

        $total = $this->calculateTotal($totalData);
        unset($totalData);

        return $this->render('index', [
            'reportForm' => $reportForm,
            'dataProvider' => $dataProvider,
            'total' => $total,
            'totalByCountry' => $totalByCountry,
            'deliveryDataByMonth' => $deliveryDataByMonth,
        ]);
    }

    /**
     * Подсчет тоталов для вьюхи
     * @param array $data
     * @return array
     */
    private function calculateTotal(array $data): array
    {
        $result['admission_lead'] = !empty($data['admission_lead']) ? round(array_sum($data['admission_lead']) / count($data['admission_lead']), CompanyStandards::PRECISION) : null;
        $result['first_sms'] = !empty($data['first_sms']) ? round(array_sum($data['first_sms']) / count($data['first_sms']), CompanyStandards::PRECISION) : null;
        $result['send_to_delivery'] = !empty($data['send_to_delivery']) ? round((array_sum($data['send_to_delivery']) / count($data['send_to_delivery'])) / 3600, CompanyStandards::PRECISION) : null;
        $result['first_call'] = !empty($data['first_call']) ? round((array_sum($data['first_call']) / count($data['first_call'])) / 60, CompanyStandards::PRECISION) : null;
        $result['try_call_count'] = !empty($data['try_call_count']) ? round(array_sum($data['try_call_count']) / count($data['try_call_count']), CompanyStandards::PRECISION) : null;
        $result['try_call_days'] = !empty($data['try_call_days']) ? round(array_sum($data['try_call_days']) / count($data['try_call_days']), CompanyStandards::PRECISION) : null;
        $result['approve_percent'] = !empty($data['approve_percent']) ? round(array_sum($data['approve_percent']) / count($data['approve_percent']), CompanyStandards::PRECISION) : null;
        $result['avg_approve_check'] = !empty($data['avg_approve_check']) ? round(array_sum($data['avg_approve_check']) / count($data['avg_approve_check']), CompanyStandards::PRECISION) : null;
        $result['error_in_address'] = !empty($data['error_in_address']) ? round(array_sum($data['error_in_address']) / count($data['error_in_address']), CompanyStandards::PRECISION) : null;
        $result['costs'] = !empty($data['costs']) ? round(array_sum($data['costs']) / count($data['costs']), CompanyStandards::PRECISION) : null;

        $result['income'] = !empty($data['income']) ? round(array_sum($data['income']) / count($data['income']), CompanyStandards::PRECISION) : null;
        $result['income_percent'] = !empty($data['income_percent']) ? round(array_sum($data['income_percent']) / count($data['income_percent']), CompanyStandards::PRECISION) : null;
        $result['lead_cost'] = !empty($data['lead_cost']) ? round(array_sum($data['lead_cost']) / count($data['lead_cost']), CompanyStandards::PRECISION) : null;
        $result['lead_cost_percent'] = !empty($data['lead_cost_percent']) ? round(array_sum($data['lead_cost_percent']) / count($data['lead_cost_percent']), CompanyStandards::PRECISION) : null;
        $result['call_center_costs'] = !empty($data['call_center_costs']) ? round(array_sum($data['call_center_costs']) / count($data['call_center_costs']), CompanyStandards::PRECISION) : null;
        $result['call_center_costs_percent'] = !empty($data['call_center_costs_percent']) ? round(array_sum($data['call_center_costs_percent']) / count($data['call_center_costs_percent']), CompanyStandards::PRECISION) : null;
        $result['product_costs'] = !empty($data['product_costs']) ? round(array_sum($data['product_costs']) / count($data['product_costs']), CompanyStandards::PRECISION) : null;
        $result['product_costs_percent'] = !empty($data['product_costs_percent']) ? round(array_sum($data['product_costs_percent']) / count($data['product_costs_percent']), CompanyStandards::PRECISION) : null;

        $result['other_costs'] = !empty($result['costs']) ? $result['costs'] - $result['lead_cost'] - $result['call_center_costs'] - $result['product_costs'] : null;
        $result['other_costs_percent'] = !empty($result['costs']) ? 100 - $result['lead_cost_percent'] - $result['call_center_costs_percent'] - $result['product_costs_percent'] : null;

        $result['accepted_time'] = !empty($data['accepted_time']) ? round(array_sum($data['accepted_time']) / count($data['accepted_time']), CompanyStandardsDelivery::PRECISION) : null;
        $result['payment_time'] = !empty($data['payment_time']) ? round(array_sum($data['payment_time']) / count($data['payment_time']), CompanyStandardsDelivery::PRECISION) : null;
        return $result;
    }
}
