<?php

namespace app\modules\report\controllers;

use app\components\web\Controller;
use app\modules\report\components\DeliveryDebtsExcelXlsxBuilder;
use app\modules\report\components\ReportFormDeliveryDebts;
use app\modules\report\components\ReportFormDeliveryDebtsOld;
use app\modules\report\components\ReportFormDeliveryDebtsPrediction;
use app\modules\report\models\ReportDeliveryDebts;
use Yii;
use yii\web\BadRequestHttpException;


/**
 * Class DebtsOldController
 * @package app\modules\report\controllers
 */
class DebtsOldController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $reportForm = new ReportFormDeliveryDebtsOld();
        $dataProvider = $reportForm->apply(Yii::$app->request->queryParams);
        $oldTime = ReportDeliveryDebts::find()->select('created_at')->scalar();
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'reportForm' => $reportForm,
            'oldTime' => $oldTime,
        ]);
    }

    /**
     * @return string
     */
    public function actionPredictionDebts()
    {
        $reportForm = new ReportFormDeliveryDebtsPrediction();
        $dataProvider = $reportForm->apply(Yii::$app->request->queryParams);

        return $this->render('prediction-debts', [
            'dataProvider' => $dataProvider,
            'reportForm' => $reportForm,
        ]);
    }

    public function actionExport()
    {
        $reportForm = new ReportFormDeliveryDebtsOld();
        $dataProvider = $reportForm->apply(Yii::$app->request->queryParams);
        $exporter = new DeliveryDebtsExcelXlsxBuilder($dataProvider->getModels());
        $exporter->sendFile();
    }

    /**
     * @param $id
     * @return null|ReportDeliveryDebts
     * @throws BadRequestHttpException
     */
    protected function findModel($id)
    {
        $model = ReportDeliveryDebts::findOne($id);
        if (is_null($model)) {
            throw new BadRequestHttpException("Запись отсутствует.");
        }
        return $model;
    }
}
