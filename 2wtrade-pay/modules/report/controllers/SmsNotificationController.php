<?php
namespace app\modules\report\controllers;

use app\components\web\Controller;
use app\modules\order\models\Order;
use app\modules\report\components\ReportFormSmsNotification;
use app\modules\order\models\OrderNotificationRequest;
use Yii;

/**
 * Class SmsNotificationController
 * @package app\modules\report\controllers
 */
class SmsNotificationController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $reportForm = new ReportFormSmsNotification();

        if (empty(Yii::$app->request->queryParams)) {
            $inputRequest['s'] = $this->getDefault();
        }
        else {
            $inputRequest = Yii::$app->request->queryParams;
        }

        $dataProvider = $reportForm->apply($inputRequest);

        return $this->render('index', [
            'reportForm' => $reportForm,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * @return array
     */
    private function getDefault() {
        $s = [];
        $s['to'] = date("d.m.Y", time());
        $s['from'] = date("d.m.Y", time());
        $s['type_poll'] = OrderNotificationRequest::ANSWER_TYPE_ALL;
        return $s;
    }

}