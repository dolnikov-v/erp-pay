<?php
namespace app\modules\report\controllers;

use app\components\web\Controller;
use app\modules\report\components\ReportFormBuyoutAndApproveByCountry;
use Yii;

/**
 * Class BuyoutAndApproveByCountryController
 * @package app\modules\report\controllers
 *
 */
class BuyoutAndApproveByCountryController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {

        ini_set('memory_limit', '1024M');
        $reportForm = new ReportFormBuyoutAndApproveByCountry();
        $dataProvider = $reportForm->apply(Yii::$app->request->queryParams);

        return $this->render('index', [
            'reportForm' => $reportForm,
            'dataProvider' => $dataProvider,
        ]);
    }
}
