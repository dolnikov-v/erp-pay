<?php
namespace app\modules\report\controllers;

use app\components\filters\AjaxFilter;
use app\components\web\Controller;
use app\modules\callcenter\models\CallCenter;
use app\modules\salary\models\MonthlyStatement;
use app\modules\salary\models\Office;
use app\modules\salary\models\Person;
use app\modules\report\components\ReportFormSalary;
use Yii;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * Class SalaryController
 * @package app\modules\report\controllers
 */
class SalaryController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => [
                    'get-call-center',
                    'get-team-lead',
                    'get-total',
                ],
            ],
        ];
    }

    /**
     * @param ArrayDataProvider $dataProvider
     * @param $dateFrom
     * @param $dateTo
     * @param ReportFormSalary $reportForm
     * @return array
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    public static function generateTotalData($dataProvider, $dateFrom, $dateTo, $reportForm)
    {
        $offices = [];
        foreach ($dataProvider->allModels as $model) {
            $def = [
                'office' => $model['office'],
                'office_id' => $model['office_id'],
                'office_country_id' => $model['office_country_id'],
                'calc_salary_local' => $model['calc_salary_local'],
                'calc_salary_usd' => $model['calc_salary_usd'],
                'staff_salary_hand_local' => 0,
                'staff_salary_hand_usd' => 0,
                'operators_salary_hand_local' => 0,
                'operators_salary_hand_usd' => 0,
                'staff_salary_tax_local' => 0,
                'staff_salary_tax_usd' => 0,
                'operators_salary_tax_local' => 0,
                'operators_salary_tax_usd' => 0,
                'expenses' => [],
                'expense_amount' => 0,
                'rate' => 0,
                'call_centers' => [],
                'count_opers' => 0,
            ];
            $office = ArrayHelper::getValue($offices, $model['office_id'], $def);

            $office['count_opers']++;
            $operatorWithCC = false;

            // оператор
            if ($model['designation_calc_work_time']) {

                $callCenters = null;
                // есть логины
                if ($model['call_center_id_oper_id']) {
                    $callCenters = [];
                    $items = explode(',', $model['call_center_id_oper_id']);
                    foreach ($items as $item) {
                        if ($tmp = explode('_', $item)) {
                            $callCenters[] = $tmp[0];
                        }
                    }

                    // выделили колл-центры оператора
                    if ($callCenters) {
                        $operatorWithCC = true;

                        // посчитали долю апрувов по направлениям для оператора
                        $rates = [];
                        foreach ($callCenters as $id) {
                            $rates[$id] = 0;
                            if (isset($model['call_center_approve'])) {
                                $rates[$id] = ArrayHelper::getValue($model['call_center_approve'], $id) ?: 0;
                            }
                        }

                        $sum = array_sum($rates);
                        $count = count($rates);
                        foreach ($rates as &$rate) {
                            $rate /= $sum ?: $count;
                        }
                        unset($rate);

                        foreach ($callCenters as $id) {
                            $def = [
                                'direction' => '',
                                'operators_salary_hand_local' => 0,
                                'operators_salary_hand_usd' => 0,
                                'operators_salary_tax_local' => 0,
                                'operators_salary_tax_usd' => 0,
                                'operator_count' => 0,
                                'operators_approve_count' => 0,
                                'operators_utility' => 0,
                                'operators_utility_normalize' => 0,
                                'operators_inutility_normalize' => 0,
                                'expenses' => [],
                                'expense_amount' => 0,
                                'staff_salary_hand_usd' => 0,
                                'staff_salary_hand_local' => 0,
                                'staff_salary_tax_usd' => 0,
                                'staff_salary_tax_local' => 0,
                                'rate' => 0,
                                'count_opers' => 0,
                            ];
                            $callCenter = ArrayHelper::getValue($office['call_centers'], $id, $def);
                            $rate = $rates[$id];

                            if ($model['calc_salary_local'] && $model['salary_hand_local'] > 0) {
                                $callCenter['operators_salary_hand_local'] += $model['salary_hand_local'] * $rate;
                                $callCenter['operators_salary_tax_local'] += $model['salary_tax_local'] * $rate;
                            }
                            if ($model['calc_salary_usd'] && $model['salary_hand_usd'] > 0) {
                                $callCenter['operators_salary_hand_usd'] += $model['salary_hand_usd'] * $rate;
                                $callCenter['operators_salary_tax_usd'] += $model['salary_tax_usd'] * $rate;
                            }
                            $callCenter['count_opers']++;

                            $office['call_centers'][$id] = $callCenter;
                        }
                    }
                }
            }

            if (!$operatorWithCC) {
                if ($model['calc_salary_local'] && $model['salary_hand_local'] > 0) {
                    $office['staff_salary_hand_local'] += $model['salary_hand_local'];
                    $office['staff_salary_tax_local'] += $model['salary_tax_local'];
                }
                if ($model['calc_salary_usd'] && $model['salary_hand_usd'] > 0) {
                    $office['staff_salary_hand_usd'] += $model['salary_hand_usd'];
                    $office['staff_salary_tax_usd'] += $model['salary_tax_usd'];
                }
            }

            $offices[$model['office_id']] = $office;
        }

        foreach ($offices as &$item) {
            $office = Office::findOne($item['office_id']);
            $currencyRate = ($office->currency) ? $office->currency->currencyRate : $office->country->currencyRate;
            if ($currencyRate) {
                if ($item['calc_salary_local'] && $currencyRate->rate) {
                    $item['rate'] = $currencyRate->rate;
                    $item['staff_salary_hand_usd'] = $item['staff_salary_hand_local'] / $currencyRate->rate;
                    $item['staff_salary_tax_usd'] = $item['staff_salary_tax_local'] / $currencyRate->rate;
                }
            }

            foreach (Office::expensesFields() as $field) {
                $item['expenses'][$field] = $office->getExpenseValue($field, $dateFrom, $dateTo);
            }
            $item['expense_amount'] = array_sum($item['expenses']);

            $rates = [];
            foreach ($item['call_centers'] as $id => &$callCenter) {

                $callcenter = CallCenter::findOne($id);
                if ($callcenter) {
                    $callCenter['direction'] = $callcenter->country->name ?? '';
                }

                if ($currencyRate) {
                    if ($item['calc_salary_local'] && $currencyRate->rate) {
                        $callCenter['rate'] = $currencyRate->rate;
                        $callCenter['operators_salary_hand_usd'] = $callCenter['operators_salary_hand_local'] / $currencyRate->rate;
                        $callCenter['operators_salary_tax_usd'] = $callCenter['operators_salary_tax_local'] / $currencyRate->rate;
                    }
                }

                // костыль для МСК и НСК офиса не учитывать апрувы в распределении
                if ($item['office_id'] != Office::MOSCOW_ID &&
                    $item['office_id'] != Office::NSK_ID &&
                    isset($reportForm->totalApprove[$id])) {
                    $callCenter['operators_approve_count'] = $reportForm->totalApprove[$id];
                }

                $callCenter['operators_utility'] = $callCenter['operators_approve_count'];

                $rates[$id] = $callCenter['operators_utility'];

            }
            unset($callCenter);

            $operatorsUtilities = ArrayHelper::getColumn($item['call_centers'], 'operators_utility');
            $operatorsUtilitySum = array_sum($operatorsUtilities);
            if ($count = count($rates)) {
                foreach ($item['call_centers'] as &$callCenter) {
                    $value = (!$operatorsUtilitySum) ? (1 / $count) : ($callCenter['operators_utility'] / $operatorsUtilitySum);
                    $callCenter['operators_utility_normalize'] = $value;
                    $callCenter['operators_inutility_normalize'] = 1 - $value;
                }
                unset($callCenter);
            }

            foreach ($item['call_centers'] as &$callCenter) {

                $rate = $callCenter['operators_utility_normalize'];

                $callCenter['staff_salary_hand_usd'] = $rate * $item['staff_salary_hand_usd'];
                $callCenter['staff_salary_tax_usd'] = $rate * $item['staff_salary_tax_usd'];
                $callCenter['staff_salary_hand_local'] = $rate * $item['staff_salary_hand_local'];
                $callCenter['staff_salary_tax_local'] = $rate * $item['staff_salary_tax_local'];
                $callCenter['expense_amount'] = $rate * $item['expense_amount'];
                foreach ($item['expenses'] as $expense => $cost) {
                    $callCenter['expenses'][$expense] = $cost * $rate;
                }

                $item['operators_salary_hand_local'] += $callCenter['operators_salary_hand_local'];
                $item['operators_salary_tax_local'] += $callCenter['operators_salary_tax_local'];
                $item['operators_salary_hand_usd'] += $callCenter['operators_salary_hand_usd'];
                $item['operators_salary_tax_usd'] += $callCenter['operators_salary_tax_usd'];

            }
            unset($callCenter);
        }
        unset($item);

        return $offices;
    }

    /**
     * @param array $data
     * @param array $directions
     * @return array
     */
    public static function prepareTotalData($data, $directions = null)
    {
        $totals = [];
        foreach ($data as $office) {
            $tmp = [
                'office' => $office['office'],
                'office_id' => $office['office'],
                'count_opers' => $office['count_opers'],
                'staff_salary_hand_local' => $office['staff_salary_hand_local'],
                'staff_salary_tax_local' => $office['staff_salary_tax_local'],
                'staff_salary_hand_usd' => $office['staff_salary_hand_usd'],
                'staff_salary_tax_usd' => $office['staff_salary_tax_usd'],
                'operators_salary_hand_local' => $office['operators_salary_hand_local'],
                'operators_salary_tax_local' => $office['operators_salary_tax_local'],
                'operators_salary_hand_usd' => $office['operators_salary_hand_usd'],
                'operators_salary_tax_usd' => $office['operators_salary_tax_usd'],
                'direction' => null,
                'rate' => $office['rate'],
                'expense_amount' => $office['expense_amount'],
                'operators_utility_normalize' => 1,
                'operators_approve_count' => 0,
                'operators_utility' => 0,
            ];

            foreach ($office['call_centers'] as $id => $callCenter) {
                if ($directions && !in_array($id, $directions)) {
                    continue;
                }
                $total = $tmp;
                $total['count_opers'] = $callCenter['count_opers'];
                $total['staff_salary_hand_local'] = $callCenter['staff_salary_hand_local'];
                $total['staff_salary_tax_local'] = $callCenter['staff_salary_tax_local'];
                $total['staff_salary_hand_usd'] = $callCenter['staff_salary_hand_usd'];
                $total['staff_salary_tax_usd'] = $callCenter['staff_salary_tax_usd'];
                $total['operators_salary_hand_local'] = $callCenter['operators_salary_hand_local'];
                $total['operators_salary_tax_local'] = $callCenter['operators_salary_tax_local'];
                $total['operators_salary_hand_usd'] = $callCenter['operators_salary_hand_usd'];
                $total['operators_salary_tax_usd'] = $callCenter['operators_salary_tax_usd'];
                $total['direction'] = $callCenter['direction'];
                $total['expense_amount'] = $callCenter['expense_amount'];
                $total['operators_utility_normalize'] = $callCenter['operators_utility_normalize'];
                $total['operators_approve_count'] = $callCenter['operators_approve_count'];
                $total['operators_utility'] = $callCenter['operators_utility'];
                $tmp['operators_approve_count'] += $callCenter['operators_approve_count'];
                $tmp['operators_utility'] += $callCenter['operators_utility'];

                $totals[] = $total;
            }

            if (!$directions) {
                $totals[] = $tmp;
            }
        }

        ArrayHelper::multisort($totals, ['office', 'direction'], [SORT_ASC, SORT_ASC]);

        return $totals;
    }

    public function actionIndex()
    {
        $reportForm = new ReportFormSalary();
        $params = Yii::$app->request->queryParams;
        $state = Yii::$app->request->get('state') ?? '';
        $officeId = $params['s']['office'][0] ?? 0;
        $dateFrom = isset($params['s']['from']) ? Yii::$app->formatter->asDate($params['s']['from'], 'php:Y-m-d') : null;
        $dateTo = isset($params['s']['to']) ? Yii::$app->formatter->asDate($params['s']['to'], 'php:Y-m-d') : null;

        $params['saveMode'] = false;
        if ($state == 'save' &&
            $dateFrom &&
            $dateTo &&
            sizeof($params['s']['office']) == 1 &&
            $officeId &&
            Office::find()->bySystemUserCountries()->byId($officeId)->exists() &&
            !$reportForm->closedMode &&
            !$reportForm->person &&
            Yii::$app->user->can('report.salary.close')
        ) {
            $params['saveMode'] = true;
        }
        $dataProvider = $reportForm->apply($params);

        $export = Yii::$app->request->get('export');
        if ($export && $reportForm->person && isset($dataProvider->allModels[0])) {
            switch ($export) {
                case 'excel':
                    return $reportForm->exportPersonExcel($dataProvider->allModels[0]);
                    break;
                case 'pdf':
                    return $reportForm->exportPersonPdf($dataProvider->allModels[0]);
                    break;
                }
        }

        if ($params['saveMode']) {
            $monthlyStatement = MonthlyStatement::find()
                ->byOfficeId($officeId)
                ->andWhere(['date_from' => $dateFrom])
                ->andWhere(['date_to' => $dateTo])
                ->one();

            $error = false;

            if ($monthlyStatement) {
                if ($monthlyStatement->closed_at) {
                    $error = true;
                    Yii::$app->notifier->addNotification(Yii::t('common', 'Расчет уже сохранен.'), 'error');
                }
            }
            else {
                $monthlyStatement = new MonthlyStatement();
                $monthlyStatement->office_id = $officeId;
                $monthlyStatement->date_from = $dateFrom;
                $monthlyStatement->date_to = $dateTo;
            }
            $monthlyStatement->active = 1;
            $monthlyStatement->closed_by = Yii::$app->user->id;
            $monthlyStatement->closed_at = time();


            $transaction = Yii::$app->db->beginTransaction();
            try {
                if (!$monthlyStatement->save()) {
                    Yii::$app->notifier->addNotificationsByModel($monthlyStatement);
                    $error = true;
                } else {

                    foreach ($dataProvider->allModels as &$model) {
                        $fileName = $reportForm->exportPersonPdf($model, true);
                        if ($fileName) {
                            $model['salary_sheet_file'] = $fileName;
                        }
                    }

                    $persons = $monthlyStatement->formatPersons($dataProvider->allModels, true);
                    $monthlyStatement->savePersons($persons, true);
                }
            } catch (\yii\web\HttpException $e) {
                $transaction->rollBack();
                Yii::$app->notifier->addNotificationsByModel($monthlyStatement);
                throw $e;
            }

            if (!$error) {
                $transaction->commit();
                Yii::$app->notifier->addNotification(Yii::t('common', 'Расчет успешно сохранен.'), 'success');

                $params['state'] = null;
                $params['s']['monthlyStatement'] = $monthlyStatement->id;
                return $this->redirect(Url::current($params));
            }
        }


        $hasDirection = isset($params['s']['callCenter']);
        $hasTeamLead = isset($params['s']['teamLead']);
        if ($hasDirection || $hasTeamLead) {
            $totalParams = $params;
            if ($hasDirection) {
                unset($totalParams['s']['callCenter']);
            }
            if ($hasTeamLead) {
                unset($totalParams['s']['teamLead']);
            }
            $totalDataProvider = (new ReportFormSalary())->apply($totalParams);
        } else {
            $totalDataProvider = $dataProvider;
        }

        $directions = ($hasDirection) ? $params['s']['callCenter'] : null;
        $data = self::generateTotalData($totalDataProvider, $dateFrom, $dateTo, $reportForm);
        $totals = self::prepareTotalData($data, $directions);

        return $this->render('index', [
            'reportForm' => $reportForm,
            'dataProvider' => $dataProvider,
            'totals' => $totals,
        ]);
    }

    /**
     * @return string
     */
    public function actionGetCallCenter()
    {
        $response = [
            'status' => 'fail',
        ];

        if (Yii::$app->request->get('office')) {
            $officeId = Yii::$app->request->get('office');

            $callCenters = CallCenter::find()
                ->byOfficeId($officeId)
                ->bySystemUserCountries()
                ->list();

            $teamLeads = Person::find()
                ->active()
                ->byOfficeId($officeId)
                ->bySystemUserCountries()
                ->isTeamLead()
                ->orderBy([Person::tableName() . '.name' => SORT_ASC])
                ->list();

            $monthlyStatements = MonthlyStatement::find()
                ->active()
                ->byOfficeId($officeId)
                ->bySystemUserCountries()
                ->orderBy(['id' => SORT_DESC])
                ->list();

            $response['status'] = 'true';
            $response['centers'] = $callCenters;
            $response['leads'] = $teamLeads;
            $response['statements'] = $monthlyStatements;
        }
        return $response;
    }

    /**
     * @return string
     */
    public function actionGetTeamLead()
    {
        $response = [
            'status' => 'fail',
            'message' => '',
        ];

        $teamLeads = [];
        if (Yii::$app->request->get('office')) {
            $officeId = Yii::$app->request->get('office');
            $teamLeads = Person::find()
                ->active()
                ->byOfficeId($officeId)
                ->isTeamLead()
                ->collection();
        }

        $response['status'] = 'true';
        $response['message'] = $teamLeads;

        return $response;
    }

    public static function getTotal($dateFrom, $dateTo, $countryId)
    {
        $office = Office::find()->byCountryId($countryId)->one();
        if (!$office) {
            $return = [
                'status' => 'false',
                'message' => Yii::t('common', 'Офис не найден')
            ];
        } else {
            $officeID = $office->id;

            $params = [
                's' => [
                    'from' => $dateFrom,
                    'to' => $dateTo,
                    'office' => [
                        $officeID
                    ]
                ]
            ];

            $callCenterMonthlyStatement = MonthlyStatement::find()
                ->byOfficeId($officeID)
                ->andWhere([
                    'and',
                    ['date_from' => $dateFrom],
                    ['date_to' => $dateTo]
                ])
                ->one();

            if ($callCenterMonthlyStatement) {
                $params['s']['monthlyStatement'] = $callCenterMonthlyStatement->id;
            }

            $reportForm = new ReportFormSalary();
            $dataProvider = $reportForm->apply($params);

            $totalSalaryHandLocal = 0;
            $totalSalaryHandUSD = 0;
            foreach ($dataProvider->allModels as $model) {
                if ($reportForm->getCalcSalaryLocal()) {
                    $totalSalaryHandLocal += $model['salary_hand_local'];
                }
                if ($reportForm->getCalcSalaryUSD()) {
                    $totalSalaryHandUSD += $model['salary_hand_usd'];
                }
            }

            $return = [
                'status' => 'true',
            ];
            if ($reportForm->getCalcSalaryLocal()) {
                $return['salary_local'] = round($totalSalaryHandLocal, 2);
            }
            if ($reportForm->getCalcSalaryUSD()) {
                $return['salary_usd'] = round($totalSalaryHandUSD, 2);
            }

        }

        return $return;
    }

    /**
     * @return string
     */
    public function actionGetTotal()
    {

        $return = [];
        $dateFrom = $dateTo = null;
        $countryId = Yii::$app->request->get('country');
        if (!$countryId) {
            $return = [
                'status' => 'false',
                'message' => Yii::t('common', 'Страна не задана')
            ];
        }

        $date = Yii::$app->request->get('date');
        if (!$date) {
            $return = [
                'status' => 'false',
                'message' => Yii::t('common', 'Период не задан')
            ];
        } else {
            $dateFrom = date('Y-m-01', strtotime($date));
            $dateTo = date('Y-m-t', strtotime($date));
        }

        if ($dateFrom && $dateTo) {
            $return = self::getTotal($dateFrom, $dateTo, $countryId);
        }

        return $return;
    }
}
