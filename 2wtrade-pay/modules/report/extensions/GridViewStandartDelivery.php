<?php
namespace app\modules\report\extensions;

use app\modules\report\components\ReportFormStandartDelivery;
use Yii;
use yii\helpers\ArrayHelper;


/**
 * Class GridViewStandartDelivery
 * @package app\modules\report\extensions
 */
class GridViewStandartDelivery extends GridView
{
    /**
     * @var ReportFormStandartDelivery
     */
    public $reportForm;

    /**
     * @param $key
     * @param $deliveryNames
     * @return string
     */
    private function getDeliveryName($key, $deliveryNames) {

        $name = '';
        foreach ($deliveryNames as $item) {
            if ($item['id'] == $key) {
                $name = $item['name'];
                break;
            }
        }

        return $name;
    }


    /**
     * @return array
     */
    private function subTotalsCalculate()
    {
        $subTotals = [];

        $models = $this->dataProvider->getModels();
        if ($models) {

            $deliveryIds = array_keys($models[0][0]);
            $subTotals = [];
            foreach ($deliveryIds as $id) {
                $subTotals[$id] = [
                    'fast' => 0,
                    'fastCount' => 0,
                    'slow' => 0,
                    'slowCount' => 0,
                    'count' => 0,
                    'countCount' => 0,
                ];
            }

            foreach ($models as $model) {
                foreach ($model[0] as $id => $item) {
                    if (!is_numeric($item['fast'])) $item['fast'] = 0;
                    if (!is_numeric($item['slow'])) $item['slow'] = 0;
                    if (!is_numeric($item['count'])) $item['count'] = 0;
                    $subTotals[$id]['fast'] += intval($item['fast']);
                    if ($item['fast'] != 0) $subTotals[$id]['fastCount']++;
                    $subTotals[$id]['slow'] += intval($item['slow']);
                    if ($item['slow'] != 0) $subTotals[$id]['slowCount']++;
                    $subTotals[$id]['count'] += intval($item['count']);
                    if ($item['count'] != 0) $subTotals[$id]['countCount']++;
                }
            }

            foreach ($subTotals as &$total) {
                if ($total['fastCount'] != 0) {
                    $total['fast'] = number_format(round($total['fast'] / $total['fastCount'], 2), 2);
                }
                else {
                    $total['fast'] = '0.00';
                }
                if ($total['slowCount'] != 0) {
                    $total['slow'] = number_format(round($total['slow'] / $total['slowCount'], 2), 2);
                }
                else {
                    $total['slow'] = '0.00';
                }
                if ($total['countCount'] != 0) {
                    $total['count'] = number_format(round($total['count'] / $total['countCount'], 2), 2);
                }
                else {
                    $total['count'] = '0.00';
                }
            }
        }

        return $subTotals;
    }

    /**
     * @inheritdoc
     */
    public function initColumns()
    {
        $deliveryNames = $this->reportForm->getDeliveriesNames($this->dataProvider->allModels[0][0]);
        $subTotals = $this->subTotalsCalculate();

        $group = $this->reportForm->groupBy;
        $type = Yii::t('common', 'Локация');
        if ($group == 'customer_province') {
            $type = Yii::t('common', 'Провинция');
        }
        else if ($group == 'customer_city') {
            $type =Yii::t('common', 'Город');
        }
        else if ($group == 'customer_zip') {
            $type = Yii::t('common', 'ZIP-код');
        }

        $keyArray = [];
        if (!empty($this->dataProvider->allModels[0][0])) {
            $keyArray = array_keys($this->dataProvider->allModels[0][0]);
        }

        $this->columns[] = [
            'attribute' => 'geo',
            'label' => $type,
            'format' => 'raw',
            'header' => Yii::t('common', 'Среднее итого, фактическое:'),
            'headerOptions' => ['class' => 'panel-footer text-left', 'style' => 'background-color: #ffdfb9; font-weight: bold;'],
            'footer' => Yii::t('common', 'Среднее итого, фактическое:'),
            'footerOptions' => ['class' => 'panel-footer text-left', 'style' => 'background-color: #ffdfb9'],
        ];

        foreach ($keyArray as $key) {
            $this->columns[] = [
                'label' => $this->getDeliveryName($key, $deliveryNames),
                'contentOptions' => ['style'=>'text-align:center'],
                'value' => function($model) use ($key, $subTotals)  {
                    return '(' .$model[0][$key]['fast'] .') / ' .$model[0][$key]['slow'] .' / ' .$model[0][$key]['count'];
                },
                'header' => $this->getDeliveryName($key, $deliveryNames) .'<br>(' .$subTotals[$key]['fast'] .') / ' .$subTotals[$key]['slow'] .' / ' .$subTotals[$key]['count'],
                'headerOptions' => ['class' => 'panel-footer text-center', 'style' => 'background-color: #ffdfb9; font-weight: bold;'],
                'footer' => $this->getDeliveryName($key, $deliveryNames) .'<br>(' .$subTotals[$key]['fast'] .') / ' .$subTotals[$key]['slow'] .' / ' .$subTotals[$key]['count'],
                'footerOptions' => ['class' => 'panel-footer text-center', 'style' => 'background-color: #ffdfb9'],
            ];
        }

        parent::initColumns();
    }

}
