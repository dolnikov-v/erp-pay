<?php
namespace app\modules\report\extensions;

use app\modules\report\components\ReportFormProduct;
use Yii;

/**
 * Class GridViewProduct
 * @package app\modules\report\extensions
 */
class GridViewProduct extends GridView
{
    /**
     * @var ReportFormProduct
     */
    public $reportForm;

    /**
     * @inheritdoc
     */
    public function initColumns()
    {
        $this->columns[] = [
            'label' => $this->reportForm->groupByCollection[$this->reportForm->groupBy],
            'attribute' => Query::GROUP_BY_LABEL,
            'pageSummary' => Yii::t('common', 'Всего'),
            'width' => '110px',
        ];

        $this->columns[] = [
            'class' => DataColumnCount::className(),
            'attribute' => Yii::t('common', 'Всего заказов'),
            'width' => '110px',
        ];

        $this->columns[] = [
            'class' => DataColumnCount::className(),
            'attribute' => Yii::t('common', 'Всего товаров'),
            'width' => '110px',
        ];

        parent::initColumns();
    }
}
