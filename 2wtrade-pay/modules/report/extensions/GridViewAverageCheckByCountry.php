<?php
namespace app\modules\report\extensions;

use Yii;

/**
 * Class GridViewAverageCheckByCountry
 * @package app\modules\report\extensions
 */
class GridViewAverageCheckByCountry extends GridView
{
    /**
     * @var ReportFormBuyoutByCountry
     */
    public $reportForm;

    /**
     * @inheritdoc
     */
    public function initColumns()
    {

        $this->columns[] = [
            'label' => Yii::t('common', 'Страна'),
            'attribute' => 'country_name',
        ];
        $this->columns[] = [
            'label' => Yii::t('common', 'Дата'),
            'attribute' => 'yearCreatedAt',
            'format' => 'raw',
            'value' => function ($model) {
                return date('d m Y', strtotime($model['yearcreatedat'] . '-' . $model['monthcreatedat'] . '-' . $model['daycreatedat'] . ' 00:00:00')) . '<span style = "display: none;">' . $model['country_name'] . '</span>';
            },
        ];
        $this->columns[] = [
            'label' => Yii::t('common', 'Средний чек'),
            'attribute' => 'podtverzdeno',
        ];

        parent::initColumns();
    }
}
