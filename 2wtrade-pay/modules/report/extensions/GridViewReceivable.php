<?php
namespace app\modules\report\extensions;

use yii\helpers\Html;

/**
 * Class GridViewReceivable
 * @package app\modules\report\extensions
 */
class GridViewReceivable extends GridView
{
    /**
     * @var array[]
     */
    public $columnsHead;

    /**
     * @return string
     */
    public function renderTableHeader()
    {
        $cellsString = '';

        foreach ($this->columnsHead as $column) {

            $cellsString .= Html::tag('th', $column['label'], $column['options']);
        }

        $content = Html::tag('tr', $cellsString, $this->headerRowOptions);

        $cells = [];

        foreach ($this->columns as $index => $column) {
            if ($this->resizableColumns && $this->persistResize) {
                $column->headerOptions['data-resizable-column-id'] = "kv-col-{$index}";
            }

            $cells[] = $column->renderHeaderCell();
        }

        $content .= Html::tag('tr', implode('', $cells), $this->headerRowOptions);

        if ($this->filterPosition == self::FILTER_POS_HEADER) {
            $content = $this->renderFilters() . $content;
        } elseif ($this->filterPosition == self::FILTER_POS_BODY) {
            $content .= $this->renderFilters();
        }

        // Показывать суммарную информацию сверху, только если больше 10 записей
        if ($this->pageSummaryRowOptions !== false) {
            if ($this->dataProvider->getCount() > 10) {
                $cells = [];

                foreach ($this->columns as $column) {
                    $cells[] = $column->renderPageSummaryCell();
                }

                $content .= Html::tag('tr', implode('', $cells), $this->pageSummaryRowOptions);
            }
        }

        return "<thead>" . PHP_EOL .
        $this->generateRows($this->beforeHeader) . PHP_EOL .
        $content . PHP_EOL .
        $this->generateRows($this->afterHeader) . PHP_EOL .
        "</thead>";
    }
}
