<?php
namespace app\modules\report\extensions;

use Yii;

class DataColumnInProgress extends DataColumn
{
    /**
     * @return float|int
     */
    protected function calculateSummary()
    {
        $type = $this->pageSummaryFunc;

        switch ($type) {
            case GridViewInProgress::F_I_AVG:
                $columnRansomCount = $this->getColumnByLabel(Yii::t('common', 'Выкуплено'));
                $columnIncomeRedemption = $this->getColumnByLabel(Yii::t('common', 'Доход по выкупам'));

                $columnRansomCount = $columnRansomCount->calculateSummary();
                $columnIncomeRedemption = $columnIncomeRedemption->calculateSummary();

                if ($columnRansomCount == 0 || $columnIncomeRedemption == 0) {
                    return 0;
                } else {
                    return $columnIncomeRedemption / $columnRansomCount;
                }
            default:
                parent::calculateSummary();
        }
    }


}
