<?php
namespace app\modules\report\extensions;

use app\modules\order\models\OrderStatus;
use app\modules\report\components\ReportFormStatus;
use Yii;

/**
 * Class GridViewStatus
 * @package app\modules\report\extensions
 */
class GridViewStatus extends GridView
{
    /**
     * @var ReportFormStatus
     */
    public $reportForm;

    /**
     * @inheritdoc
     */
    public function initColumns()
    {
        $this->columns[] = [
            'label' => $this->reportForm->groupByCollection[$this->reportForm->groupBy],
            'attribute' => Query::GROUP_BY_LABEL,
            'pageSummary' => Yii::t('common', 'Всего'),
            'width' => '110px',
        ];

        $statuses = OrderStatus::find()->collection();

        foreach ($statuses as $id => $status) {
            $this->columns[] = [
                'class' => DataColumnCount::className(),
                'attribute' => $status,
                'label' => $id . '. ' . $status,
                'columnLinkParams' => $id,
                'width' => '110px',
            ];
        }

        $this->columns[] = [
            'class' => DataColumnCount::className(),
            'attribute' => Yii::t('common', 'Всего'),
            'columnLinkParams' => implode(',', array_keys($statuses)),
            'width' => '110px',
        ];

        parent::initColumns();
    }
}
