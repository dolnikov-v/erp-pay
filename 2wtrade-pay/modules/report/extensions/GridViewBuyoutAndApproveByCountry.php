<?php
namespace app\modules\report\extensions;

use app\modules\report\components\ReportFormBuyoutAndApproveByCountry;
use Yii;
use yii\helpers\Html;

/**
 * Class GridViewBuyoutAndApproveByCountry
 * @package app\modules\report\extensions
 */
class GridViewBuyoutAndApproveByCountry extends GridView
{
    /**
     * @var ReportFormBuyoutAndApproveByCountry
     */
    public $reportForm;

    /**
     * @inheritdoc
     */
    public function initColumns()
    {
        $this->showPageSummary = false;

        $this->columns[] = [
            'label' => $this->reportForm->groupLabel == 'country' ? Yii::t('common', 'Страна') : Yii::t('common', 'Служба доставки'),
            'attribute' => $this->reportForm->groupLabel,
            'group' => true,
            'content' => function ($data) {
                if ($this->reportForm->groupLabel == 'delivery_ids') {

                    if (!empty($data['delivery_name'])) return $data['delivery_name'];
                    if (!empty($data['delivery_ids']))  return $data['delivery_ids'];
                    return '-';

                    return $data['delivery_name'] ?? ($data[$this->reportForm->groupLabel] ?? '-');
                }

                if ($this->reportForm->groupLabel == 'country') {
                    $getData = $this->reportForm->searchQuery;
                    $getData['s']['country_ids'] = $data['country_id'];
                    $getData['s']['groupLabel'] = 'delivery_ids';

                    return Html::a($data['country'],
                        [
                            '/report/buyout-and-approve-by-country/index',
                            's' => $getData['s'],
                        ],
                        [
                            'target' => '_blank',
                            'title' => Yii::t('common', 'Детальная информация'),
                        ]);
                }

                return '';
            },
        ];

        $this->columns[] = [
            'label' => Yii::t('common', 'Товар'),
            'attribute' => 'product',
        ];

        $this->columns[] = [
            'label' => Yii::t('common', 'Одобрено заказов'),
            'attribute' => 'approved',
        ];

        $this->columns[] = [
            'label' => Yii::t('common', 'Выкуплено заказов'),
            'attribute' => 'buyout',
        ];

        $this->columns[] = [
            'label' => Yii::t('common', 'Всего заказов'),
            'attribute' => 'allorders',
        ];


        parent::initColumns();
    }
}
