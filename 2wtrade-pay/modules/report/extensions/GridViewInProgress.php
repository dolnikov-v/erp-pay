<?php
namespace app\modules\report\extensions;

use app\modules\order\models\OrderStatus;
use app\modules\report\components\ReportForm;
use app\modules\report\components\ReportFormInProgress;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * Class GridViewInProgress
 * @package app\modules\report\extensions
 */
class GridViewInProgress extends GridView
{
    /**
     * Для подсчёта среднего дохода по выкупам
     */
    const F_I_AVG = 'f_i_avg';
    /**
     * @var ReportFormInProgress
     */
    public $reportForm;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if (in_array($this->reportForm->groupBy, [ReportForm::GROUP_BY_COUNTRY_DATE, ReportForm::GROUP_BY_COUNTRY_PRODUCTS])
            && $subTotals = $this->subTotalsCalculate()
        ) {
            $this->afterRow = function ($model, $key) use ($subTotals) {
                $row = null;
                if ($subTotal = ArrayHelper::getValue($subTotals, $key)) {
                    $options = $this->rowOptions; // save options
                    $this->rowOptions = ['class' => 'info kv-group-footer'];
                    $row = $this->renderTableRow($subTotal, null, null);
                    $this->rowOptions = $options; // load options
                }
                return $row;
            };
        }
    }

    /**
     * @param $value
     * @return DataColumn|null
     */
    private function findColumnByLabel($value)
    {
        foreach ($this->columns as $key => $column) {
            if ($column->attribute != null) {
                if ($value == $column->attribute) {
                    return $this->columns[$key];
                }
            } else {
                if ($value == $column->label) {
                    return $this->columns[$key];
                }
            }
        }

        return null;
    }

    /**
     * @param $group
     * @param $name
     * @return mixed
     */
    private function subTotalSummary($group, $name)
    {
        $value = '';
        $column = $this->findColumnByLabel($name);
        if ($column && $column->pageSummary) {
            $operator = $column->pageSummaryFunc ?: GridView::F_SUM;
            $values = ArrayHelper::getColumn($group, $name);
            switch ($operator) {
                case GridView::F_SUM: $value = array_sum($values);
                    break;
                case GridView::F_AVG: $value = array_sum($values) / count($values);
                    break;
                case GridView::F_MAX: $value = max($values);
                    break;
                case GridView::F_MIN: $value = min($values);
                    break;
                case GridViewInProgress::F_I_AVG:
                    $buyout = $this->subTotalSummary($group, $this->findColumnByLabel(Yii::t('common', 'Выкуплено')));
                    $income = $this->subTotalSummary($group, $this->findColumnByLabel(Yii::t('common', 'Доход по выкупам')));
                    $value = ($buyout > 0) ? ($income / $buyout) : '';
                    break;
            }
        }

        return $value;
    }

    /**
     * @return array
     */
    private function subTotalsCalculate()
    {
        $subTotals = [];

        $models = $this->dataProvider->getModels();
        if ($models) {
            $groups = ArrayHelper::index($models, null, ['country_id']);
            if (count($groups) > 1) {
                $fields = array_keys(reset($models));
                foreach ($groups as $countryId => $group) {
                    foreach ($fields as $field) {
                        $subTotals[$countryId][$field] = $this->subTotalSummary($group, $field);
                    }
                }

                $map = [];
                foreach($models as $rowKey => $model) {
                    $map[$model['country_id']] = $rowKey;
                }

                $tmp = [];
                foreach ($subTotals as $key => $subTotal) {
                    if ($rowKey = ArrayHelper::getValue($map, $key)) {
                        $tmp[$rowKey] = $subTotal;
                    }
                }
                $subTotals = $tmp;
            }
        }

        return $subTotals;
    }

    /**
     * @inheritdoc
     */
    public function initColumns()
    {

        if ($this->reportForm->groupBy == ReportForm::GROUP_BY_COUNTRY_PRODUCTS) {

            $this->columns[] = [
                'attribute' => 'country_name',
                'label' => Yii::t('common', 'Страна'),
                'group' => true,
                'pageSummary' => false,
            ];

            $this->columns[] = [
                'attribute' => 'product_name',
                'pageSummary' => false,
                'label' => Yii::t('common', 'Товар'),
            ];

        } elseif ($this->reportForm->groupBy == ReportForm::GROUP_BY_COUNTRY_DATE) {

            $this->columns[] = [
                'attribute' => 'country_name',
                'label' => Yii::t('common', 'Страна'),
                'group' => true,
                'pageSummary' => false,
            ];

            $this->columns[] = [
                'attribute' => 'date',
                'pageSummary' => false,
                'label' => Yii::t('common', 'Дата'),
            ];

        } else {

            $this->columns[] = [
                'label' => $this->reportForm->groupByCollection[$this->reportForm->groupBy],
                'attribute' => Query::GROUP_BY_LABEL,
                'pageSummary' => Yii::t('common', 'Всего'),
                'content' => function ($data) {
                    if ($this->reportForm->groupByCollection[$this->reportForm->groupBy] == Yii::t('common', 'Страна')) {
                        $getData = $this->reportForm->searchQuery;
                        $getData['s']['country_ids'] = $data['groupbyname'];
                        $getData['s']['groupBy'] = $getData['s']['type'];

                        return Html::a($data['groupbylabel'], [
                            '/report/in-progress/index',
                            's' => $getData['s'],
                        ],
                            [
                                'target' => '_blank',
                                'title' => Yii::t('common','Детальная информация'),
                            ]);
                    }
                    return $data['groupbylabel'];
                },
            ];
        }

        $mapStatuses = $this->reportForm->getMapStatuses();
        $orderStatuses = OrderStatus::find()->collection();

        foreach ($mapStatuses as $name => $statuses) {
            $innerStatuses = [];

            foreach ($statuses as $status) {
                $innerStatuses[] = $status . '. ' . $orderStatuses[$status];
            }

            $this->columns[] = [
                'class' => DataColumnCount::className(),
                'attribute' => $name,
                'columnLinkParams' => implode(',', $statuses),
                'headerOptions' => [
                    'data-container' => 'body',
                    'data-toggle' => 'tooltip',
                    'title' => Yii::t('common', 'Статусы: {statuses}', ['statuses' => implode(', ', $innerStatuses)]),
                ],
            ];
        }

        $this->columns[] = [
            'label' => Yii::t('common', 'Средний чек апрувленный'),
            'value' => function ($data) {
                return round($data[Yii::t('common', 'Средний чек апрувленный')], 2);
            },
            'pageSummaryFunc' => self::F_AVG,
            'format' => ['decimal', 2],
        ];
        $this->columns[] = [
            'label' => Yii::t('common', 'Средний чек выкупной'),
            'value' => function ($data) {
                return round($data[Yii::t('common', 'Средний чек выкупной')], 2);
            },
            'pageSummaryFunc' => self::F_AVG,
            'format' => ['decimal', 2],
        ];

        $this->columns[] = [
            'class' => DataColumnInProgress::className(),
            'label' => Yii::t('common', 'Ср. доход по выкупам'),
            'value' => function ($data) {
                $buyout = $data[Yii::t('common', 'Выкуплено')];
                $all = $data[Yii::t('common', 'Доход по выкупам')];

                return round($buyout != 0 ? $all / $buyout : 0, 2);
            },
            'pageSummaryFunc' => self::F_I_AVG,
            'format' => ['decimal', 2],
        ];

        $this->columns[] = [
            'label' => Yii::t('common', 'Доход по выкупам'),
            'value' => function ($data) {
                return round($data[Yii::t('common', 'Доход по выкупам')], 2);
            },
            'format' => ['decimal', 2],
        ];

        $this->columns[] = [
            'label' => Yii::t('common', 'Цена за лиды'),
            'value' => function ($data) {
                return round($data[Yii::t('common', 'Цена за лиды')], 2);
            },
            'format' => ['decimal', 2],
        ];

        $this->columns[] = [
            'label' => Yii::t('common', 'Расходы'),
            'value' => function () {
                return 0;
            },
            'format' => ['decimal', 2],
        ];

        $this->columns[] = [
            'label' => Yii::t('common', 'Прибыль'),
            'value' => function ($data) {
                return $this->getValueIncome($data);
            },
            'content' => function ($data) {
                $value = $this->getValueIncome($data);
                $value = Yii::$app->formatter->asDecimal($value, 2);

                if ($value < 0) {
                    $value = Html::tag('span', $value, ['class' => 'text-danger']);
                }

                return $value;
            },
            'format' => ['decimal', 2],
        ];

        parent::initColumns();
    }

    /**
     * @param $data
     * @return integer
     */
    protected function getValueIncome($data)
    {
        $buyout = $data[Yii::t('common', 'Доход по выкупам')];
        $leads = $data[Yii::t('common', 'Цена за лиды')];
        $consumptions = 0;

        return $buyout - $leads - $consumptions;
    }
}
