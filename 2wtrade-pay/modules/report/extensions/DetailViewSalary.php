<?php
namespace app\modules\report\extensions;

use app\modules\report\components\ReportFormSalary;
use Yii;
use app\modules\order\models\OrderStatus;
use yii\widgets\DetailView;

/**
 * Class DetailViewSalary
 * @package app\modules\report\extensions
 */
class DetailViewSalary extends DetailView
{
    /**
     * @var ReportFormSalary
     */
    public $reportForm;

    /**
     * @inheritdoc
     */
    public function init()
    {

        $model = $this->model;

        $this->attributes[] = [
            'label' => Yii::t('common', 'ФИО'),
            'value' => $model['fio']
        ];
        $this->attributes[] = [
            'label' => Yii::t('common', 'Должность'),
            'value' => isset($model['designation']) ? Yii::t('common', $model['designation']) : ''
        ];
        $this->attributes[] = [
            'label' => Yii::t('common', 'Дата приема на работу'),
            'value' => $model['start_date'],
            'format' => ['date'],
            'visible' => $model['start_date'] != ''
        ];
        $this->attributes[] = [
            'label' => Yii::t('common', 'Дата увольнения'),
            'value' => $model['dismissal_date'],
            'format' => ['date'],
            'visible' => $model['dismissal_date'] != ''
        ];
        if (Yii::$app->user->can('view_salary_person_numbers')) {
            $this->attributes[] = [
                'label' => Yii::t('common', 'Номер счета'),
                'value' => $model['account_number'],
            ];
            $this->attributes[] = [
                'label' => Yii::t('common', 'Номер ID/Паспорта'),
                'value' => $model['passport_id_number'],
            ];
        }
        $this->attributes[] = [
            'label' => Yii::t('common', 'Руководитель'),
            'value' => (!empty($model['parent_id']) ? Yii::t('common', $model['parent_designation']) . ': ' . $model['parent_name'] : '-')
        ];
        $this->attributes[] = [
            'label' => Yii::t('common', 'Офис'),
            'value' => isset($model['office']) ? Yii::t('common', $model['office']) : '-'
        ];
        $this->attributes[] = [
            'label' => Yii::t('common', 'Направление'),
            'value' => str_replace(',', ', ', $model['country_names'])
        ];
        $this->attributes[] = [
            'label' => Yii::t('common', 'Логин'),
            'value' => isset($model['call_center_user_logins']) ? str_replace(',', ', ', $model['call_center_user_logins']) : ''
        ];
        $this->attributes[] = [
            'label' => Yii::t('common', 'Оклад в местной валюте в месяц'),
            'format' => ['decimal', 2],
            'value' => $model['salary_local'] ?? 0,
            'visible' => $this->reportForm->getCalcSalaryLocal()
        ];
        $this->attributes[] = [
            'attribute' => 'salary_usd',
            'label' => Yii::t('common', 'Оклад в USD'),
            'format' => ['decimal', 2],
            'value' => $model['salary_usd'] ?? 0,
            'visible' => $this->reportForm->getCalcSalaryUSD()
        ];
        $this->attributes[] = [
            'attribute' => 'salary_hour_local',
            'label' => Yii::t('common', 'Оклад в местной валюте в час'),
            'format' => ['decimal', 2],
            'value' => $model['salary_hour_local'] ?? 0,
            'visible' => $this->reportForm->getCalcSalaryLocal()
        ];
        $this->attributes[] = [
            'attribute' => 'salary_hour_usd',
            'label' => Yii::t('common', 'Оклад в USD в час'),
            'format' => ['decimal', 2],
            'value' => $model['salary_hour_usd'] ?? 0,
            'visible' => $this->reportForm->getCalcSalaryUSD()
        ];
        $this->attributes[] = [
            'attribute' => 'count_normal_hours',
            'label' => Yii::t('common', 'Норма часов по КЗоТ'),
            'format' => ['decimal', 2],
            'value' => $model['count_normal_hours'] ?? 0,
        ];
        $this->attributes[] = [
            'attribute' => 'count_time',
            'label' => Yii::t('common', 'Отработанных часов'),
            'format' => ['decimal', 2],
            'value' => $model['count_time'] ?? 0,
        ];
        $this->attributes[] = [
            'attribute' => 'count_extra_hours',
            'label' => Yii::t('common', 'Переработка часов'),
            'format' => ['decimal', 2],
            'value' => $model['count_extra_hours'] ?? 0,
        ];
        $this->attributes[] = [
            'attribute' => 'holiday_worked_time',
            'label' => Yii::t('common', 'Праздники часов'),
            'format' => ['decimal', 2],
            'value' => $model['holiday_worked_time'] ?? 0,
            'visible' => $this->reportForm->getShowHolidays()
        ];
        $this->attributes[] = [
            'attribute' => 'salary_hand_local_extra',
            'label' => Yii::t('common', 'ЗП переработка'),
            'format' => ['decimal', 2],
            'value' => $model['salary_hand_local_extra'] ?? 0,
            'visible' => $this->reportForm->getCalcSalaryLocal()
        ];
        $this->attributes[] = [
            'attribute' => 'salary_hand_usd_extra',
            'label' => Yii::t('common', 'ЗП переработка'),
            'format' => ['decimal', 2],
            'value' => $model['salary_hand_usd_extra'] ?? 0,
            'visible' => $this->reportForm->getCalcSalaryUSD()
        ];
        $this->attributes[] = [
            'attribute' => 'salary_hand_local_holiday',
            'label' => Yii::t('common', 'ЗП праздники'),
            'format' => ['decimal', 2],
            'value' => $model['salary_hand_local_holiday'] ?? 0,
            'visible' => $this->reportForm->getShowHolidays() && $this->reportForm->getCalcSalaryLocal()
        ];
        $this->attributes[] = [
            'attribute' => 'salary_hand_usd_holiday',
            'label' => Yii::t('common', 'ЗП праздники'),
            'format' => ['decimal', 2],
            'value' => $model['salary_hand_usd_holiday'] ?? 0,
            'visible' => $this->reportForm->getShowHolidays() && $this->reportForm->getCalcSalaryUSD()
        ];
        $this->attributes[] = [
            'attribute' => 'penalty_sum_local',
            'label' => Yii::t('common', 'Штрафы'),
            'format' => ['decimal', 2],
            'value' => $model['penalty_sum_local'] ?? 0,
            'visible' => $this->reportForm->getCalcSalaryLocal()
        ];
        $this->attributes[] = [
            'attribute' => 'penalty_sum_usd',
            'label' => Yii::t('common', 'Штрафы'),
            'format' => ['decimal', 2],
            'value' => $model['penalty_sum_usd'] ?? 0,
            'visible' => $this->reportForm->getCalcSalaryUSD()
        ];
        $this->attributes[] = [
            'attribute' => 'bonus_sum_local',
            'label' => Yii::t('common', 'Бонусы'),
            'format' => ['decimal', 2],
            'value' => $model['bonus_sum_local'] ?? 0,
            'visible' => $this->reportForm->getCalcSalaryLocal()
        ];
        $this->attributes[] = [
            'attribute' => 'bonus_sum_usd',
            'label' => Yii::t('common', 'Бонусы'),
            'format' => ['decimal', 2],
            'value' => $model['bonus_sum_usd'] ?? 0,
            'visible' => $this->reportForm->getCalcSalaryUSD()
        ];
        $this->attributes[] = [
            'attribute' => 'salary_hand_local',
            'label' => Yii::t('common', 'ЗП в местной валюте "на руки"'),
            'format' => ['decimal', 2],
            'value' => $model['salary_hand_local'] ?? 0,
            'visible' => $this->reportForm->getCalcSalaryLocal()
        ];
        $this->attributes[] = [
            'attribute' => 'salary_tax_local',
            'label' => Yii::t('common', 'ЗП в местной валюте с учетом налогов'),
            'format' => ['decimal', 2],
            'value' => $model['salary_tax_local'] ?? 0,
            'visible' => $this->reportForm->getCalcSalaryLocal()
        ];
        $this->attributes[] = [
            'attribute' => 'salary_hand_usd',
            'label' => Yii::t('common', 'ЗП в USD "на руки"'),
            'format' => ['decimal', 2],
            'value' => $model['salary_hand_usd'] ?? 0,
            'visible' => $this->reportForm->getCalcSalaryUSD()
        ];
        $this->attributes[] = [
            'attribute' => 'salary_tax_usd',
            'label' => Yii::t('common', 'ЗП в USD с учетом налогов'),
            'format' => ['decimal', 2],
            'value' => $model['salary_tax_usd'] ?? 0,
            'visible' => $this->reportForm->getCalcSalaryUSD()
        ];
        $mapStatuses = $this->reportForm->getMapStatuses();
        $orderStatuses = OrderStatus::find()->collection();
        foreach ($mapStatuses as $name => $statuses) {

            if ($name == 'total') continue;

            $innerStatuses = [];

            foreach ($statuses as $status) {
                $innerStatuses[] = $status . '. ' . $orderStatuses[$status];
            }

            $label = '';
            if ($name == 'approve') $label = 'Подтверждено';
            if ($name == 'buyout') $label = 'Выкуплено';

            $value = $model[$name] ?? 0;

            if (isset($model['vsego']) && $model['vsego']) {
                $value .= ' (' . number_format(100 * $model[$name] / $model['vsego'], 2, Yii::$app->get('formatter')->decimalSeparator, Yii::$app->get('formatter')->thousandSeparator) . '%)';
            }

            $this->attributes[] = [
                'attribute' => $name,
                'label' => Yii::t('common', $label),
                'makeLinkToOrders' => false,
                'value' => $value,
            ];
        }
        $this->attributes[] = [
            'attribute' => 'count_calls',
            'label' => Yii::t('common', 'Всего звонков'),
            'format' => ['decimal'],
            'value' => $model['count_calls'] ?? 0,
        ];
        parent::init();
    }
}