<?php

namespace app\modules\report\extensions;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\delivery\models\Delivery;
use app\modules\order\models\Order;
use app\helpers\i18n\Formatter;
use app\models\Country;
use app\modules\report\components\ReportFormWorkflow;
use Yii;
use yii\base\InvalidParamException;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;


class DataProviderWorkflow extends ArrayDataProvider
{
    /**
     * @var ReportFormWorkflow
     */
    private $form;

    /**
     * @var Query
     */
    private $query;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->pagination->pageSize = 0;

        $this->prepareQuery()
            ->prepareAllModels();
    }

    /**
     * @param ReportFormWorkflow $form
     */
    public function setForm(ReportFormWorkflow $form)
    {
        $this->form = $form;
    }

    /**
     * @param Query $query
     */
    public function setQuery(Query $query)
    {
        $this->query = $query;
    }

    /**
     * @return $this
     */
    protected function prepareQuery()
    {
        $closure = $this->getQueryClosure();

        if ($closure instanceof \Closure) {
            $closure($this->query);
        }

        return $this;
    }

    /**
     * @return $this
     */
    protected function prepareAllModels()
    {
        $allModels = $this->query->all();

        $closure = $this->getDataClosure();

        if ($closure instanceof \Closure) {
            $closure($this->query, $allModels);
        }

        $result = [];

        foreach ($allModels as $rowNumber => $row) {
            foreach ($row as $label => $data) {
                $newLabel = $this->query->getLabel($label);

                if ($newLabel) {
                    $result[$rowNumber][$newLabel] = $data;
                } else {
                    $result[$rowNumber][$label] = $data;
                }
            }
        }

        $this->allModels = $result;

        return $this;
    }

    /**
     * @return \Closure
     */
    protected function getQueryClosure()
    {
        $this->query->groupBy(Query::GROUP_BY_NAME);

        $closures = self::getQueryClosures($this->form);

        if (!array_key_exists($this->form->groupBy, $closures)) {
            throw new InvalidParamException(Yii::t('common', 'Неправильный параметр группировки.'));
        }

        return $closures[$this->form->groupBy];
    }

    /**
     * @param ReportFormWorkflow $form
     * @return array
     */
    public static function getQueryClosures($form)
    {
        return [
            ReportFormWorkflow::GROUP_BY_CREATED_AT => function (Query $query) {
                self::applyQueryClosureDate('order.created_at', $query);
            },
            ReportFormWorkflow::GROUP_BY_UPDATED_AT => function (Query $query) {
                self::applyQueryClosureDate('order.updated_at', $query);
            },
            ReportFormWorkflow::GROUP_BY_CALL_CENTER_SENT_AT => function (Query $query) {
                self::applyQueryClosureDate('call_center_request.sent_at', $query);
            },
            ReportFormWorkflow::GROUP_BY_CALL_CENTER_APPROVED_AT => function (Query $query) {
                self::applyQueryClosureDate('call_center_request.approved_at', $query);
            },
            ReportFormWorkflow::GROUP_BY_DELIVERY_CREATED_AT => function (Query $query) {
                self::applyQueryClosureDate('delivery_request.created_at', $query);
            },
            ReportFormWorkflow::GROUP_BY_DELIVERY_SENT_AT => function (Query $query) {
                self::applyQueryClosureDate('COALESCE(`delivery_request`.second_sent_at,`delivery_request`.sent_at)', $query);
            },
            ReportFormWorkflow::GROUP_BY_DELIVERY_FIRST_SENT_AT => function (Query $query) {
                self::applyQueryClosureDate('`delivery_request`.sent_at', $query);
            },
            ReportFormWorkflow::GROUP_BY_DELIVERY_RETURNED_AT => function (Query $query) {
                self::applyQueryClosureDate('delivery_request.returned_at', $query);
            },
            ReportFormWorkflow::GROUP_BY_DELIVERY_APPROVED_AT => function (Query $query) {
                self::applyQueryClosureDate('delivery_request.approved_at', $query);
            },
            ReportFormWorkflow::GROUP_BY_DELIVERY_ACCEPTED_AT => function (Query $query) {
                self::applyQueryClosureDate('delivery_request.accepted_at', $query);
            },
            ReportFormWorkflow::GROUP_BY_DELIVERY_PAID_AT => function (Query $query) {
                self::applyQueryClosureDate('delivery_request.paid_at', $query);
            },
            ReportFormWorkflow::GROUP_BY_CUSTOMER_DISTRIT => function (Query $query) {
                self::applyQueryClosureOrder('order.customer_district', $query);
            },
            ReportFormWorkflow::GROUP_BY_CUSTOMER_CITY => function (Query $query) {
                self::applyQueryClosureOrder('order.customer_city', $query);
            },
            ReportFormWorkflow::GROUP_BY_CUSTOMER_PROVINCE => function (Query $query) {
                self::applyQueryClosureOrder('order.customer_province', $query);
            },
            ReportFormWorkflow::GROUP_BY_CUSTOMER_ZIP => function (Query $query) {
                self::applyQueryClosureOrder('order.customer_zip', $query);
            },
            ReportFormWorkflow::GROUP_BY_COUNTRY => function (Query $query) {
                $attribute = Country::tableName() . '.id';
                $attributeLabel = Country::tableName() . '.name';

                $joins = $query->join;
                $joins = ArrayHelper::getColumn($joins, '1');
                $shouldJoin = true;
                foreach ($joins as $table) {
                    if (in_array($table, [Country::tableName(), '`country`', 'country'])) {
                        $shouldJoin = false;
                        break;
                    }
                }
                if ($shouldJoin) {
                    $query->leftJoin(Country::tableName(),
                        Country::tableName() . '.id = ' . Order::tableName() . '.country_id');
                }
                $query->addCondition($attribute, Query::GROUP_BY_NAME);
                $query->addCondition($attributeLabel, Query::GROUP_BY_LABEL);
                $query->groupBy([Order::tableName() . '.country_id']);
                $query->orderBy($attributeLabel);
            },
            ReportFormWorkflow::GROUP_BY_DELIVERY => function (Query $query) {
                $attribute = Delivery::tableName() . '.id';
                $attributeLabel = Delivery::tableName() . '.name';

                $joins = $query->join;
                $joins = ArrayHelper::getColumn($joins, '1');
                $shouldJoin = true;
                foreach ($joins as $table) {
                    if (in_array($table, [Delivery::tableName(), '`delivery`', 'delivery'])) {
                        $shouldJoin = false;
                        break;
                    }
                }
                if ($shouldJoin) {
                    $query->leftJoin(Delivery::tableName(),
                        Delivery::tableName() . '.id = ' . DeliveryRequest::tableName() . '.delivery_id');
                }

                $query->addCondition($attribute, Query::GROUP_BY_NAME);
                $query->addCondition($attributeLabel, Query::GROUP_BY_LABEL);
                $query->groupBy([Order::tableName() . '.country_id']);
                $query->orderBy($attributeLabel);
            },
            ReportFormWorkflow::GROUP_BY_COUNTRY_DELIVERY => function (Query $query) {
                $joins = $query->join;
                $joins = ArrayHelper::getColumn($joins, '1');
                $shouldJoin = true;
                foreach ($joins as $table) {
                    if (in_array($table, [Delivery::tableName(), '`delivery`', 'delivery'])) {
                        $shouldJoin = false;
                        break;
                    }
                }
                if ($shouldJoin) {
                    $query->leftJoin(Delivery::tableName(),
                        Delivery::tableName() . '.id = ' . DeliveryRequest::tableName() . '.delivery_id');
                }

                $shouldJoin = true;
                foreach ($joins as $table) {
                    if (in_array($table, [Country::tableName(), '`country`', 'country'])) {
                        $shouldJoin = false;
                        break;
                    }
                }
                if ($shouldJoin) {
                    $query->leftJoin(Country::tableName(),
                        Country::tableName() . '.id = ' . Order::tableName() . '.country_id');
                }

                $query->addCondition('CONCAT(country.id, " ", delivery_request.delivery_id)', Query::GROUP_BY_NAME);
                $query->addCondition('CONCAT(country.name, " ", delivery.name)', Query::GROUP_BY_LABEL);
                $query->orderBy(Query::GROUP_BY_LABEL);
            },
        ];
    }

    /**
     * @return \Closure
     */
    protected function getDataClosure()
    {
        $closures = [
            ReportFormWorkflow::GROUP_BY_CREATED_AT => function (Query $query, $data) {

            },
            ReportFormWorkflow::GROUP_BY_UPDATED_AT => function (Query $query, $data) {

            },
            ReportFormWorkflow::GROUP_BY_CALL_CENTER_SENT_AT => function (Query $query, $data) {

            },
            ReportFormWorkflow::GROUP_BY_CALL_CENTER_APPROVED_AT => function (Query $query, $data) {

            },
            ReportFormWorkflow::GROUP_BY_DELIVERY_CREATED_AT => function (Query $query, $data) {

            },
            ReportFormWorkflow::GROUP_BY_DELIVERY_SENT_AT => function (Query $query, $data) {

            },

            ReportFormWorkflow::GROUP_BY_DELIVERY_FIRST_SENT_AT => function (Query $query, $data) {

            },
            ReportFormWorkflow::GROUP_BY_DELIVERY_RETURNED_AT => function (Query $query, $data) {

            },
            ReportFormWorkflow::GROUP_BY_DELIVERY_APPROVED_AT => function (Query $query, $data) {

            },
            ReportFormWorkflow::GROUP_BY_DELIVERY_ACCEPTED_AT => function (Query $query, $data) {

            },
            ReportFormWorkflow::GROUP_BY_DELIVERY_PAID_AT => function (Query $query, $data) {

            },
            ReportFormWorkflow::GROUP_BY_CUSTOMER_DISTRIT => function (Query $query, $data) {

            },
            ReportFormWorkflow::GROUP_BY_CUSTOMER_CITY => function (Query $query, $data) {

            },
            ReportFormWorkflow::GROUP_BY_CUSTOMER_PROVINCE => function (Query $query, $data) {

            },
            ReportFormWorkflow::GROUP_BY_CUSTOMER_ZIP => function (Query $query, $data) {

            },
            ReportFormWorkflow::GROUP_BY_COUNTRY => function (Query $query, $data) {

            },
            ReportFormWorkflow::GROUP_BY_DELIVERY => function (Query $query, $data) {

            },
            ReportFormWorkflow::GROUP_BY_COUNTRY_DELIVERY => function (Query $query, $data) {

            },
        ];

        if (!array_key_exists($this->form->groupBy, $closures)) {
            throw new InvalidParamException(Yii::t('common', 'Неправильный параметр группировки.'));
        }

        return $closures[$this->form->groupBy];
    }

    /**
     * @param string $attribute
     * @param Query $query
     */
    protected static function applyQueryClosureDate($attribute, $query)
    {
        $condition = 'DATE_FORMAT(FROM_UNIXTIME(' . $attribute . '), "' . Formatter::getMysqlDateFormat() . '")';

        $query->addCondition($condition, Query::GROUP_BY_NAME);
        $query->addCondition($condition, Query::GROUP_BY_LABEL);

        $query->orderBy([$attribute => SORT_DESC]);
    }

    /**
     * @param string $attribute
     * @param Query $query
     */
    protected static function applyQueryClosureOrder($attribute, $query)
    {
        $query->addCondition($attribute, Query::GROUP_BY_NAME);
        $query->addCondition($attribute, Query::GROUP_BY_LABEL);

        $query->orderBy([$attribute => SORT_DESC]);
    }
}