<?php

namespace app\modules\report\extensions;

use app\helpers\i18n\Formatter;
use app\models\Country;
use app\models\Landing;
use app\models\Product;
use app\models\ProductCategory;
use app\models\ProductLinkCategory;
use app\modules\callcenter\models\CallCenterRequest;
use app\modules\delivery\models\Delivery;
use app\modules\delivery\models\DeliveryRequest;
use app\modules\delivery\models\DeliveryZipcodes;
use app\modules\order\models\Lead;
use app\modules\order\models\LeadProduct;
use app\modules\order\models\Order;
use app\modules\callcenter\models\CallCenterCheckRequest;
use app\modules\order\models\OrderNotificationRequest;
use app\modules\order\models\OrderProduct;
use app\modules\order\models\OrderStatus;
use app\modules\report\components\ReportForm;
use app\modules\report\components\ReportFormFinance;
use app\modules\report\models\SmsPollHistory;
use Yii;
use yii\base\InvalidParamException;
use yii\data\ArrayDataProvider;
use yii\db\Expression;
use yii\db\Query as BaseQuery;
use yii\helpers\ArrayHelper;

/**
 * Class DataProvider
 * @package app\modules\report\extensions
 */
class DataProvider extends ArrayDataProvider
{
    /**
     * @var ReportForm
     */
    private $form;

    /**
     * @var Query
     */
    private $query;

    /**
     * @var integer
     */
    private $totalCount;

    /**
     * @var integer
     */
    private $totalCountFinance;

    /**
     * @var integer
     */
    private $totalCountFinanceMoneyReceived;

    /**
     * @var integer
     */
    private $totalCountProducts;

    /**
     * @var array
     */
    private $products;

    /**
     * @var array
     */
    public $statuses;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->pagination->pageSize = 0;

        $this->prepareQuery()
            ->prepareAllModels();
    }

    /**
     * @param ReportForm $form
     */
    public function setForm(ReportForm $form)
    {
        $this->form = $form;
    }

    /**
     * @param Query $query
     */
    public function setQuery(Query $query)
    {
        $this->query = $query;
    }

    /**
     * @return integer
     */
    public function getTotalCount()
    {
        if ($this->totalCount === null) {
            $this->totalCount = 0;

            $query = clone $this->query;

            $result = $query
                ->select(['COUNT(1) as totalCount'])
                ->groupBy(null)
                ->orderBy(null)
                ->all();

            $this->totalCount = $result[0]['totalCount'];
        }

        return $this->totalCount;
    }

    /**
     * @return integer
     */
    public function getTotalCountFinance()
    {
        if ($this->totalCountFinance === null) {
            $this->totalCountFinance = 0;

            /** @var ReportFormFinance $form */
            $form = $this->form;

            $queryStatuses = [];
            $mapStatuses = $form->getMapStatuses();

            foreach ($mapStatuses as $name => $statuses) {
                $queryStatuses = array_merge($queryStatuses, $statuses);
            }

            $queryStatuses = array_unique($queryStatuses);

            $query = clone $this->query;

            $result = $query
                ->select(['COUNT(1) as totalCount'])
                ->andWhere(['in', Order::tableName() . '.status_id', $queryStatuses])
                ->groupBy(null)
                ->orderBy(null)
                ->all();

            $this->totalCountFinance = $result[0]['totalCount'];
        }

        return $this->totalCountFinance;
    }

    /**
     * @return integer
     */
    public function getTotalCountFinanceMoneyReceived()
    {
        if ($this->totalCountFinanceMoneyReceived === null) {
            $this->totalCountFinanceMoneyReceived = 0;

            $statuses = [
                OrderStatus::STATUS_DELIVERY_BUYOUT,
                OrderStatus::STATUS_FINANCE_MONEY_RECEIVED,
            ];

            $query = clone $this->query;

            $result = $query
                ->select(['COUNT(1) as totalCount'])
                ->andWhere(['in', Order::tableName() . '.status_id', $statuses])
                ->groupBy(null)
                ->orderBy(null)
                ->all();

            $this->totalCountFinanceMoneyReceived = $result[0]['totalCount'];
        }

        return $this->totalCountFinanceMoneyReceived;
    }

    /**
     * @return integer
     */
    public function getTotalCountProducts()
    {
        if ($this->totalCountProducts === null) {
            $this->totalCountProducts = 0;

            $query = new BaseQuery();
            $tmpQuery = clone $this->query;
            $query->select(['SUM(quantity) as totalCount']);
            $query->from(['tmp' => $tmpQuery]);
            $result = $query->all();

            $this->totalCountProducts = $result[0]['totalCount'];
        }

        return $this->totalCountProducts;
    }

    /**
     * @return integer
     */
    public function getTotalCountProductsForCallCenter()
    {
        if ($this->totalCountProducts === null) {
            $this->totalCountProducts = 0;

            $query = new BaseQuery();
            $tmpQuery = clone $this->query;
            $sumCondition = $tmpQuery->buildIfCondition(Order::tableName() . '.status_id',
                $this->form->getMapStatusesAsArray(),
                Order::tableName() . '.id', 'NULL');
            $tmpQuery->addCountDistinctCondition($sumCondition, 'totalcount');
            $tmpQuery->groupBy('product_id');

            $query->select(['SUM(totalcount) as totalCount']);
            $query->from(['tmp' => $tmpQuery]);

            $result = $query->all();

            $this->totalCountProducts = $result[0]['totalCount'];
        }

        return $this->totalCountProducts;
    }

    /**
     * @return array
     */
    public function getProducts()
    {
        if ($this->products === null) {
            $this->products = [];

            $query = new BaseQuery();
            $tmpQuery = clone $this->query;
            $query->select(['DISTINCT(product_id) as product']);
            $query->from(['tmp' => $tmpQuery]);

            $records = $query->all();

            foreach ($records as $record) {
                if ($record['product']) {
                    $this->products[] = $record['product'];
                }
            }
        }

        return $this->products;
    }

    /**
     * @return $this
     */
    protected function prepareQuery()
    {
        $closure = $this->getQueryClosure();

        if ($closure instanceof \Closure) {
            $closure($this->query);
        }

        return $this;
    }

    /**
     * @return $this
     */
    protected function prepareAllModels()
    {
        $allModels = $this->query->all();

        $closure = $this->getDataClosure();

        if ($closure instanceof \Closure) {
            $closure($this->query, $allModels);
        }

        $result = [];

        foreach ($allModels as $rowNumber => $row) {
            foreach ($row as $label => $data) {
                $newLabel = $this->query->getLabel($label);

                if ($newLabel) {
                    $result[$rowNumber][$newLabel] = $data;
                } else {
                    $result[$rowNumber][$label] = $data;
                }
            }
        }

        $this->allModels = $result;

        return $this;
    }

    /**
     * @return \Closure
     */
    protected function getQueryClosure()
    {
        // Нафига Артем запихал это сюда? Если это убрать отсюда что-нибудь закрашиться или нет?
        //=-=-=-=-=
        if (!$this->checkTableJoin([CallCenterRequest::tableName(), '`call_center_request`', 'call_center_request'])) {
            $this->query->leftJoin('`call_center_request`', '`call_center_request`.`order_id` = `order`.`id`');
        }
        if (!$this->checkTableJoin([DeliveryRequest::tableName(), '`delivery_request`', 'delivery_request'])) {
            $this->query->leftJoin(DeliveryRequest::tableName(), DeliveryRequest::tableName() . '.`order_id` = `order`.`id`');
        }
        //=-=-=-=-=

        if ($this->form->groupBy == ReportForm::GROUP_BY_TS_SPAWN) {
            if (!$this->checkTableJoin([Lead::tableName(), '`lead`', 'lead'])) {
                $this->query->leftJoin(Lead::tableName(), Lead::tableName() . '.order_id=' . Order::tableName() . '.id');
            }
        }

        $this->query->groupBy(Query::GROUP_BY_NAME);

        $closures = self::getQueryClosures($this->form);

        if (!array_key_exists($this->form->groupBy, $closures)) {
            throw new InvalidParamException(Yii::t('common', 'Неправильный параметр группировки.'));
        }

        return $closures[$this->form->groupBy];
    }

    /**
     * @param array $possibleTableNames
     * @return bool
     */
    protected function checkTableJoin(array $possibleTableNames)
    {
        $joins = $this->query->join;
        if (!$joins) {
            return false;
        }
        $joins = ArrayHelper::getColumn($joins, '1');
        $shouldJoin = true;
        foreach ($joins as $table) {
            if (in_array($table, $possibleTableNames)) {
                $shouldJoin = false;
                break;
            }
        }

        return !$shouldJoin;
    }

    /**
     * @param string $tableName
     * @param array $joins
     * @return bool
     */
    public static function isTableJoined($tableName, $joins)
    {
        $possibleTableNames = [$tableName, "`$tableName`"];
        if (!$joins) {
            return false;
        }
        $joins = ArrayHelper::getColumn($joins, '1');
        foreach ($joins as $table) {
            if (in_array($table, $possibleTableNames)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param ReportForm $form
     * @return array
     */
    public static function getQueryClosures($form)
    {
        return [
            ReportForm::GROUP_BY_CREATED_AT => function (Query $query) {
                static::applyQueryClosureDate('order.created_at', $query);
            },
            ReportForm::GROUP_BY_UPDATED_AT => function (Query $query) {
                static::applyQueryClosureDate('order.updated_at', $query);
            },
            ReportForm::GROUP_BY_CALL_CENTER_SENT_AT => function (Query $query) {
                static::applyQueryClosureDate('call_center_request.sent_at', $query);
            },
            ReportForm::GROUP_BY_CALL_CENTER_APPROVED_AT => function (Query $query) {
                static::applyQueryClosureDate('call_center_request.approved_at', $query);
            },
            ReportForm::GROUP_BY_DELIVERY => function (Query $query) {
                $attribute = DeliveryRequest::tableName() . '.delivery_id';
                $attributeLabel = 'CONCAT(' . Country::tableName() . '.name' . ',\' \',' . Delivery::tableName() . '.name)';

                if (!self::isTableJoined(Delivery::tableName(), $query->join)) {
                    $query->leftJoin(Delivery::tableName(),
                        DeliveryRequest::tableName() . '.delivery_id = ' . Delivery::tableName() . '.id');
                };

                if (!self::isTableJoined(Country::tableName(), $query->join)) {
                    $query->leftJoin(Country::tableName(),
                        Country::tableName() . '.id = ' . Order::tableName() . '.country_id');
                };

                $query->addSelect([
                    'delivery_id' => $attribute,
                    'country_id' => Country::tableName() . '.id'
                ]);
                $query->addCondition($attribute, Query::GROUP_BY_NAME);
                $query->addCondition($attributeLabel, Query::GROUP_BY_LABEL);
                $query->orderBy(Query::GROUP_BY_LABEL);
                $query->groupBy([Query::GROUP_BY_NAME]);
            },
            ReportForm::GROUP_BY_DELIVERY_CREATED_AT => function (Query $query) {
                static::applyQueryClosureDate('delivery_request.created_at', $query);
            },
            ReportForm::GROUP_BY_DELIVERY_SENT_AT => function (Query $query) {
                static::applyQueryClosureDate(
                    'COALESCE(`delivery_request`.second_sent_at,`delivery_request`.sent_at)', $query);
            },
            ReportForm::GROUP_BY_DELIVERY_SENT_AT_NOT_EMPTY => function (Query $query) {
                static::applyQueryClosureDate(
                    'COALESCE(
                    `delivery_request`.second_sent_at,
                    `delivery_request`.sent_at, 
                    `delivery_request`.created_at)', $query);
            },
            ReportForm::GROUP_BY_DELIVERY_FIRST_SENT_AT => function (Query $query) {
                static::applyQueryClosureDate('`delivery_request`.sent_at', $query);
            },
            ReportForm::GROUP_BY_DELIVERY_RETURNED_AT => function (Query $query) {
                static::applyQueryClosureDate('delivery_request.returned_at', $query);
            },
            ReportForm::GROUP_BY_DELIVERY_APPROVED_AT => function (Query $query) {
                static::applyQueryClosureDate('delivery_request.approved_at', $query);
            },
            ReportForm::GROUP_BY_DELIVERY_ACCEPTED_AT => function (Query $query) {
                static::applyQueryClosureDate('delivery_request.accepted_at', $query);
            },
            ReportForm::GROUP_BY_DELIVERY_PAID_AT => function (Query $query) {
                static::applyQueryClosureDate('delivery_request.paid_at', $query);
            },
            ReportForm::GROUP_BY_TS_SPAWN => function (Query $query) {
                static::applyQueryClosureDate('lead.ts_spawn', $query);
            },
            ReportForm::GROUP_BY_CUSTOMER_DISTRIT => function (Query $query) {
                static::applyQueryClosureOrder('customer_district', $query);
            },
            ReportForm::GROUP_BY_CUSTOMER_CITY => function (Query $query) {
                static::applyQueryClosureOrder('customer_city', $query);
            },
            ReportForm::GROUP_BY_CUSTOMER_PROVINCE => function (Query $query) {
                static::applyQueryClosureOrder('customer_province', $query);
            },
            ReportForm::GROUP_BY_CUSTOMER_ZIP => function (Query $query) {
                static::applyQueryClosureOrder('customer_zip', $query);
            },
            ReportForm::GROUP_BY_LANDING => function (Query $query) {
                $attribute = Order::tableName() . '.landing_id';
                $attributeLabel = Landing::tableName() . '.url';

                $query->leftJoin(Landing::tableName(),
                    Order::tableName() . '.landing_id = ' . Landing::tableName() . '.id');

                $query->addCondition($attribute, Query::GROUP_BY_NAME);
                $query->addCondition($attributeLabel, Query::GROUP_BY_LABEL);
            },
            ReportForm::GROUP_BY_PRODUCTS => function (Query $query) {
                $subQuery = clone $query;
                if (!self::isTableJoined(OrderProduct::tableName(), $subQuery->join)) {
                    $subQuery->leftJoin(OrderProduct::tableName(), OrderProduct::tableName().'.order_id = ' . Order::tableName().'.id');
                }
                $subQuery->select([
                    OrderProduct::tableName() . '.product_id',
                    OrderProduct::tableName() . '.order_id',
                    OrderProduct::tableName() . '.price',
                    'quantity' => 'SUM(' . OrderProduct::tableName() . '.quantity)'
                ])->groupBy(['order_id', 'product_id']);
                $query->leftJoin(['group_by_product_join_order_product' => $subQuery],
                    '`group_by_product_join_order_product`.order_id = ' . Order::tableName() . '.id');
                $query->leftJoin(['group_by_product_join_product' => Product::tableName()], '`group_by_product_join_product`.id = `group_by_product_join_order_product`.product_id');

                $query->addSelect([
                    Query::GROUP_BY_NAME => '`group_by_product_join_order_product`.product_id',
                    'quantity' => 'SUM(`group_by_product_join_order_product`.quantity)',
                    'product_id' => '`group_by_product_join_order_product`.product_id',
                    Query::GROUP_BY_LABEL => '`group_by_product_join_product`.name'
                ]);
                $query->groupBy([Query::GROUP_BY_NAME]);
                $query->orderBy(Query::GROUP_BY_LABEL);
            },
            ReportForm::GROUP_BY_LEAD_PRODUCTS => function (Query $query) {
                $query->addSelect([
                    Query::GROUP_BY_NAME => LeadProduct::find()
                        ->select([
                            LeadProduct::tableName() . '.product_id'
                        ])
                        ->where([LeadProduct::tableName() . '.order_id' => new Expression(Order::tableName() . '.id')])
                        ->limit(1)
                ]);

                $query->addSelect([
                    'quantity' => LeadProduct::find()
                        ->select([
                            'sum(' . LeadProduct::tableName() . '.quantity)'
                        ])
                        ->where([LeadProduct::tableName() . '.order_id' => new Expression(Order::tableName() . '.id')])
                        ->limit(1)
                ]);

                $query->addSelect([
                    'product_id' => LeadProduct::find()
                        ->select([
                            LeadProduct::tableName() . '.product_id'
                        ])
                        ->where([LeadProduct::tableName() . '.order_id' => new Expression(Order::tableName() . '.id')])
                        ->limit(1)
                ]);

                $query->addSelect([
                    Query::GROUP_BY_LABEL => LeadProduct::find()
                        ->joinWith(['product'])
                        ->select([
                            Product::tableName() . '.name'
                        ])
                        ->where([LeadProduct::tableName() . '.order_id' => new Expression(Order::tableName() . '.id')])
                        ->limit(1)
                ]);
                $query->groupBy([Query::GROUP_BY_NAME]);
                $query->orderBy(Query::GROUP_BY_LABEL);
            },
            ReportForm::GROUP_BY_WEBMASTER_IDENTIFIER => function (Query $query) {
                $attribute = Order::tableName() . '.webmaster_identifier';
                $query->addCondition($attribute, Query::GROUP_BY_NAME);
                $query->addCondition($attribute, Query::GROUP_BY_LABEL);
            },
            ReportForm::GROUP_BY_LAST_FOREIGN_OPERATOR => function (Query $query) {
                $attribute = CallCenterRequest::tableName() . '.last_foreign_operator';
                $query->addCondition($attribute, Query::GROUP_BY_NAME);
                $query->addCondition($attribute, Query::GROUP_BY_LABEL);
            },
            ReportForm::GROUP_BY_YEAR_MONTH => function (Query $query) use ($form) {
                $selectDate = $form->getDateFilter()->getFilteredAttribute();
                $attribute = 'DATE_FORMAT(FROM_UNIXTIME(' . $selectDate . '), "%Y-%m")';
                $query->addCondition($attribute, Query::GROUP_BY_NAME);
                $query->addCondition($attribute, Query::GROUP_BY_LABEL);
            },
            ReportForm::GROUP_BY_COUNTRY_DATE => function (Query $query) use ($form) {
                $selectDate = $form->getDateFilter()->getFilteredAttribute();
                $query->addCondition('DATE_FORMAT(FROM_UNIXTIME(' . $selectDate . '), "%Y-%m-%d")', 'date');
                $joins = $query->join;
                $joins = ArrayHelper::getColumn($joins, '1');
                $shouldJoin = true;
                foreach ($joins as $table) {
                    if (in_array($table, [Country::tableName(), '`country`', 'country'])) {
                        $shouldJoin = false;
                        break;
                    }
                }
                if ($shouldJoin) {
                    $query->leftJoin(Country::tableName(),
                        Country::tableName() . '.id = ' . Order::tableName() . '.country_id');
                }
                $query->addCondition(Country::tableName() . '.name', 'country_name');
                $query->addCondition(Country::tableName() . '.id', Query::GROUP_BY_NAME);
                $query->addCondition(Country::tableName() . '.name', Query::GROUP_BY_LABEL);

                $query->groupBy([Order::tableName() . '.country_id', 'date']);
                $query->orderBy(Country::tableName() . '.name, date');
            },
            ReportForm::GROUP_BY_COUNTRY => function (Query $query) {
                $attribute = Country::tableName() . '.id';
                $attributeLabel = Country::tableName() . '.name';

                $joins = $query->join;
                $joins = ArrayHelper::getColumn($joins, '1');
                $shouldJoin = true;
                foreach ($joins as $table) {
                    if (in_array($table, [Country::tableName(), '`country`', 'country'])) {
                        $shouldJoin = false;
                        break;
                    }
                }
                if ($shouldJoin) {
                    $query->leftJoin(Country::tableName(),
                        Country::tableName() . '.id = ' . Order::tableName() . '.country_id');
                }
                $query->addSelect(['country_id' => Country::tableName() . '.id']);
                $query->addCondition($attribute, Query::GROUP_BY_NAME);
                $query->addCondition($attributeLabel, Query::GROUP_BY_LABEL);
                $query->groupBy([Order::tableName() . '.country_id']);
            },
            ReportForm::GROUP_BY_COUNTRY_PRODUCTS => function (Query $query) {
                $joins = $query->join;
                $joins = ArrayHelper::getColumn($joins, '1');
                $shouldJoin = true;
                foreach ($joins as $table) {
                    if (in_array($table, [Country::tableName(), '`country`', 'country'])) {
                        $shouldJoin = false;
                        break;
                    }
                }
                if ($shouldJoin) {
                    $query->leftJoin(Country::tableName(),
                        Country::tableName() . '.id = ' . Order::tableName() . '.country_id');
                }

                /*$query->addSelect([
                    Query::GROUP_BY_NAME => OrderProduct::find()
                        ->select([
                            'CONCAT(' . Order::tableName() . '.country_id, "-",' . OrderProduct::tableName() . '.product_id)'
                        ])
                        ->where([OrderProduct::tableName() . '.order_id' => new Expression(Order::tableName() . '.id')])
                        ->limit(1)
                ]);

                $query->addSelect([
                    Query::GROUP_BY_LABEL => OrderProduct::find()
                        ->joinWith(['product', 'order', 'order.country'])
                        ->select([
                            'CONCAT(' . Country::tableName() . '.name, "-",' . Product::tableName() . '.name)'
                        ])
                        ->where([OrderProduct::tableName() . '.order_id' => new Expression(Order::tableName() . '.id')])
                        ->limit(1)
                ]);*/
                $query->addSelect([
                    'product_id' => OrderProduct::find()
                        ->select([
                            OrderProduct::tableName() . '.product_id'
                        ])
                        ->where([OrderProduct::tableName() . '.order_id' => new Expression(Order::tableName() . '.id')])
                        ->limit(1)
                ]);

                $query->addSelect([
                    'product_name' => OrderProduct::find()
                        ->joinWith(['product'])
                        ->select([
                            Product::tableName() . '.name'
                        ])
                        ->where([OrderProduct::tableName() . '.order_id' => new Expression(Order::tableName() . '.id')])
                        ->limit(1)
                ]);

                $query->addSelect([
                    'quantity' => OrderProduct::find()
                        ->select([
                            'sum(' . OrderProduct::tableName() . '.quantity)'
                        ])
                        ->where([OrderProduct::tableName() . '.order_id' => new Expression(Order::tableName() . '.id')])
                        ->limit(1)
                ]);

                $query->addCondition(Country::tableName() . '.id', 'country_id');
                $query->addCondition(Country::tableName() . '.name', 'country_name');

                //$query->groupBy([Query::GROUP_BY_NAME]);
                //$query->orderBy(Query::GROUP_BY_LABEL);
                $query->groupBy([Order::tableName() . '.country_id', 'product_id']);
                $query->orderBy(Country::tableName() . '.name, product_name');
            },
            ReportForm::GROUP_BY_COUNTRY_LEAD_PRODUCTS => function (Query $query) {
                $joins = $query->join;
                $joins = ArrayHelper::getColumn($joins, '1');
                $shouldJoin = true;
                foreach ($joins as $table) {
                    if (in_array($table, [Country::tableName(), '`country`', 'country'])) {
                        $shouldJoin = false;
                        break;
                    }
                }
                if ($shouldJoin) {
                    $query->leftJoin(Country::tableName(),
                        Country::tableName() . '.id = ' . Order::tableName() . '.country_id');
                }
                $query->addSelect([
                    'product_id' => LeadProduct::find()
                        ->select([
                            LeadProduct::tableName() . '.product_id'
                        ])
                        ->where([LeadProduct::tableName() . '.order_id' => new Expression(Order::tableName() . '.id')])
                        ->limit(1)
                ]);
                $query->addSelect([
                    'product_name' => LeadProduct::find()
                        ->joinWith(['product'])
                        ->select([
                            Product::tableName() . '.name'
                        ])
                        ->where([LeadProduct::tableName() . '.order_id' => new Expression(Order::tableName() . '.id')])
                        ->limit(1)
                ]);
                $query->addSelect([
                    'quantity' => LeadProduct::find()
                        ->select([
                            'sum(' . LeadProduct::tableName() . '.quantity)'
                        ])
                        ->where([LeadProduct::tableName() . '.order_id' => new Expression(Order::tableName() . '.id')])
                        ->limit(1)
                ]);
                //$query->addCondition(Country::tableName() . '.id', 'country_id');
                $query->addCondition(Country::tableName() . '.name', 'country_name');
                $query->groupBy([Order::tableName() . '.country_id', 'product_id']);
                $query->orderBy(Country::tableName() . '.name, product_name');
            },
            ReportForm::GROUP_BY_DELIVERY_ZIPCODES => function (Query $query) {

                $query->leftJoin(DeliveryZipcodes::tableName(),
                    new Expression('FIND_IN_SET(' . Order::tableName() . '.customer_zip, ' . DeliveryZipcodes::tableName() . '.zipcodes)>0 and ' . DeliveryZipcodes::tableName() . '.delivery_id=' . DeliveryRequest::tableName() . '.delivery_id'));

                $attribute = DeliveryZipcodes::tableName() . '.id';
                $query->addCondition($attribute, Query::GROUP_BY_NAME);
                $query->addCondition($attribute, Query::GROUP_BY_LABEL);
                $query->groupBy([DeliveryZipcodes::tableName() . '.id']);
            },
            ReportForm::GROUP_BY_CHECK_SENT_AT => function (Query $query) {
                $froms = $query->from;
                $shouldJoin = true;

                foreach ($froms as $from) {
                    if (in_array($from,
                        [
                            CallCenterCheckRequest::tableName(),
                            'call_center_check_request',
                            '`call_center_check_request`'
                        ])) {
                        $shouldJoin = false;
                        break;
                    }
                }

                if ($shouldJoin) {
                    $joins = $query->join;
                    $joins = ArrayHelper::getColumn($joins, '1');
                    foreach ($joins as $table) {
                        if (is_array($table)) {
                            reset($table);
                            $table = key($table);
                        }
                        if (in_array($table,
                            [
                                CallCenterCheckRequest::tableName(),
                                'call_center_check_request',
                                '`call_center_check_request`'
                            ])) {
                            $shouldJoin = false;
                            break;
                        }
                    }
                }
                if ($shouldJoin) {
                    $query->leftJoin(CallCenterCheckRequest::tableName(),
                        CallCenterCheckRequest::tableName() . '.order_id = ' . Order::tableName() . '.id');
                }

                self::applyQueryClosureDate('call_center_check_request.sent_at', $query);
            },
            ReportForm::GROUP_BY_CHECK_DONE_AT => function (Query $query) {
                $froms = $query->from;
                $shouldJoin = false;

                foreach ($froms as $from) {
                    if (in_array($from,
                        [
                            CallCenterCheckRequest::tableName(),
                            'call_center_check_request',
                            '`call_center_check_request`'
                        ])) {
                        $shouldJoin = true;
                        break;
                    }
                }

                if ($shouldJoin) {
                    $joins = $query->join;
                    $joins = ArrayHelper::getColumn($joins, '1');
                    $shouldJoin = true;
                    foreach ($joins as $table) {
                        if (is_array($table)) {
                            reset($table);
                            $table = key($table);
                        }
                        if (in_array($table,
                            [
                                CallCenterCheckRequest::tableName(),
                                'call_center_check_request',
                                '`call_center_check_request`'
                            ])) {
                            $shouldJoin = false;
                            break;
                        }
                    }
                }
                if ($shouldJoin) {
                    $query->leftJoin(CallCenterCheckRequest::tableName(),
                        CallCenterCheckRequest::tableName() . '.order_id = ' . Order::tableName() . '.id');
                }

                self::applyQueryClosureDate('call_center_check_request.done_at', $query);
            },
            ReportForm::GROUPBY_SMS_CREATED_AT => function (Query $query) {
                $attribute = SmsPollHistory::tableName() . '.id';
                $query->addCondition($attribute, Query::GROUP_BY_NAME);
                $query->addCondition($attribute, Query::GROUP_BY_LABEL);
            },
            ReportForm::GROUPBY_SMS_ANSWERED_AT => function (Query $query) {
                $attribute = SmsPollHistory::tableName() . '.id';
                $query->addCondition($attribute, Query::GROUP_BY_NAME);
                $query->addCondition($attribute, Query::GROUP_BY_LABEL);
            },
            ReportForm::GROUPBY_NOTIFICATION_CREATED_AT => function (Query $query) {
                $attribute = OrderNotificationRequest::tableName() . '.id';
                $query->addCondition($attribute, Query::GROUP_BY_NAME);
                $query->addCondition($attribute, Query::GROUP_BY_LABEL);
            },
            ReportForm::GROUPBY_NOTIFICATION_ANSWERED_AT => function (Query $query) {
                $attribute = OrderNotificationRequest::tableName() . '.id';
                $query->addCondition($attribute, Query::GROUP_BY_NAME);
                $query->addCondition($attribute, Query::GROUP_BY_LABEL);
            },
            ReportForm::GROUP_BY_DATE_COUNTRY_DELIVERY => function (Query $query) use ($form) {

                if (!self::isTableJoined(Delivery::tableName(), $query->join)) {
                    $query->leftJoin(Delivery::tableName(),
                        DeliveryRequest::tableName() . '.delivery_id = ' . Delivery::tableName() . '.id');
                };

                if ($form->getDateFilter()->type) {
                    $selectDate = $form->getDateFilter()->getFilteredAttribute();
                    $attribute = 'DATE_FORMAT(FROM_UNIXTIME(' . $selectDate . '), "' . Formatter::getMysqlDateFormat() . '")';
                    $query->addCondition($attribute, Query::GROUP_BY_NAME);
                    $query->addCondition($attribute, Query::GROUP_BY_LABEL);
                    $query->addCondition($attribute, 'date');
                    $query->groupBy([
                        'date',
                        Delivery::tableName() . '.country_id',
                        Delivery::tableName() . '.id',
                    ]);
                } else {
                    $attribute = Delivery::tableName() . '.country_id';
                    $query->addCondition($attribute, Query::GROUP_BY_NAME);
                    $query->addCondition($attribute, Query::GROUP_BY_LABEL);
                    $query->groupBy([
                        Delivery::tableName() . '.country_id',
                        Delivery::tableName() . '.id',
                    ]);
                }

            },
            ReportForm::GROUP_BY_PRODUCT_CATEGORY => function (Query $query) use ($form) {
                $subQuery = clone $query;

                if (!self::isTableJoined(ProductCategory::tableName(), $subQuery->join)) {
                    $subQuery->leftJoin(OrderProduct::tableName(), OrderProduct::tableName() . '.order_id = ' . Order::tableName() . '.id');
                    $subQuery->leftJoin(ProductLinkCategory::tableName(), ProductLinkCategory::tableName() . '.product_id = ' . OrderProduct::tableName() . '.product_id');
                    $subQuery->leftJoin(ProductCategory::tableName(), ProductCategory::tableName() . '.id = ' . ProductLinkCategory::tableName() . '.category_id');
                }
                $subQuery->select([
                    'order_id' => OrderProduct::tableName() . '.order_id',
                    'category_id' => ProductLinkCategory::tableName() . '.category_id'
                ])->groupBy(['order_id', 'category_id']);
                $query->leftJoin(['group_by_product_join_order_product' => $subQuery],
                    '`group_by_product_join_order_product`.order_id = ' . Order::tableName() . '.id');
                $query->leftJoin(ProductCategory::tableName(), ProductCategory::tableName() . '.id = `group_by_product_join_order_product`.category_id');

                $query->addSelect([
                    Query::GROUP_BY_NAME => ProductCategory::tableName() . '.id',
                    'category_id' => '`group_by_product_join_order_product`.category_id',
                    Query::GROUP_BY_LABEL => ProductCategory::tableName() . '.name'
                ]);

                $query->groupBy([Query::GROUP_BY_NAME]);
                $query->orderBy(Query::GROUP_BY_LABEL);
            },
            ReportForm::GROUP_BY_PROVINCE_CITY_SUBDISTRICT_ZIP => function (Query $query) use ($form) {
                $attribute = Order::tableName() . '.customer_province';
                $query->addCondition($attribute, Query::GROUP_BY_NAME);
                $query->addCondition($attribute, Query::GROUP_BY_LABEL);
                $query->groupBy([
                    $attribute,
                    Order::tableName() . '.customer_city',
                    Order::tableName() . '.customer_subdistrict',
                    Order::tableName() . '.customer_zip',
                ]);
                $query->orderBy([
                    $attribute => SORT_ASC,
                    Order::tableName() . '.customer_city' => SORT_ASC,
                    Order::tableName() . '.customer_subdistrict' => SORT_ASC,
                    Order::tableName() . '.customer_zip' => SORT_ASC,
                ]);
            },
            ReportForm::GROUP_BY_SUBDISTRICT_ZIP => function (Query $query) use ($form) {
                $attribute = Order::tableName() . '.customer_subdistrict';
                $query->addCondition($attribute, Query::GROUP_BY_NAME);
                $query->addCondition($attribute, Query::GROUP_BY_LABEL);
                $query->groupBy([
                    $attribute,
                    Order::tableName() . '.customer_zip',
                ]);
                $query->orderBy([
                    $attribute => SORT_ASC,
                    Order::tableName() . '.customer_zip' => SORT_ASC,
                ]);
            },
            ReportForm::GROUP_BY_COUNTRY_DELIVERY => function (Query $query) use ($form) {

                if (!self::isTableJoined(Delivery::tableName(), $query->join)) {
                    $query->leftJoin(Delivery::tableName(),
                        DeliveryRequest::tableName() . '.delivery_id = ' . Delivery::tableName() . '.id');
                };

                $attribute = Delivery::tableName() . '.country_id';
                $query->addCondition($attribute, Query::GROUP_BY_NAME);
                $query->addCondition($attribute, Query::GROUP_BY_LABEL);
                $query->groupBy([
                    Delivery::tableName() . '.country_id',
                    Delivery::tableName() . '.id',
                ]);
            },
            ReportForm::GROUP_BY_DATE_PRODUCTS => function (Query $query) use ($form) {
                $selectDate = $form->getDateFilter()->getFilteredAttribute();
                $attribute = 'DATE_FORMAT(FROM_UNIXTIME(' . $selectDate . '), "%Y-%m-%d")';

                $query->addCondition($attribute, Query::GROUP_BY_NAME);
                $query->addCondition($attribute, Query::GROUP_BY_LABEL);
                $query->addCondition($attribute, 'date');

                $subQuery = clone $query;
                if (!self::isTableJoined(OrderProduct::tableName(), $subQuery->join)) {
                    $subQuery->leftJoin(OrderProduct::tableName(), OrderProduct::tableName().'.order_id = ' . Order::tableName().'.id');
                }
                $subQuery->select([
                    OrderProduct::tableName() . '.product_id',
                    OrderProduct::tableName() . '.order_id',
                    OrderProduct::tableName() . '.price',
                    'quantity' => 'SUM(' . OrderProduct::tableName() . '.quantity)'
                ])->groupBy(['order_id', 'product_id']);
                $query->leftJoin(['group_by_product_join_order_product' => $subQuery],
                    '`group_by_product_join_order_product`.order_id = ' . Order::tableName() . '.id');
                $query->leftJoin(['group_by_product_join_product' => Product::tableName()], '`group_by_product_join_product`.id = `group_by_product_join_order_product`.product_id');

                $query->addSelect([
                    'quantity' => 'SUM(`group_by_product_join_order_product`.quantity)',
                    'product_id' => '`group_by_product_join_order_product`.product_id',
                    'product_name' => '`group_by_product_join_product`.name'
                ]);

                $query->groupBy([
                    'date',
                    '`group_by_product_join_order_product`.product_id',
                ]);

                $query->orderBy([
                    'date' => SORT_ASC,
                    'product_name' => SORT_ASC,
                ]);
            }
        ];
    }

    /**
     * @return \Closure
     */
    protected function getDataClosure()
    {
        $closures = [
            ReportForm::GROUP_BY_CREATED_AT => function (Query $query, $data) {

            },
            ReportForm::GROUP_BY_UPDATED_AT => function (Query $query, $data) {

            },
            ReportForm::GROUP_BY_CALL_CENTER_SENT_AT => function (Query $query, $data) {

            },
            ReportForm::GROUP_BY_CALL_CENTER_APPROVED_AT => function (Query $query, $data) {

            },
            ReportForm::GROUP_BY_DELIVERY => function (Query $query, $data) {

            },
            ReportForm::GROUP_BY_DELIVERY_CREATED_AT => function (Query $query, $data) {

            },
            ReportForm::GROUP_BY_DELIVERY_SENT_AT => function (Query $query, $data) {

            },
            ReportForm::GROUP_BY_DELIVERY_SENT_AT_NOT_EMPTY => function (Query $query, $data) {

            },

            ReportForm::GROUP_BY_DELIVERY_FIRST_SENT_AT => function (Query $query, $data) {

            },
            ReportForm::GROUP_BY_DELIVERY_RETURNED_AT => function (Query $query, $data) {

            },
            ReportForm::GROUP_BY_DELIVERY_APPROVED_AT => function (Query $query, $data) {

            },
            ReportForm::GROUP_BY_DELIVERY_ACCEPTED_AT => function (Query $query, $data) {

            },
            ReportForm::GROUP_BY_DELIVERY_PAID_AT => function (Query $query, $data) {

            },
            ReportForm::GROUP_BY_PRODUCTS => function (Query $query, $data) {

            },
            ReportForm::GROUP_BY_LEAD_PRODUCTS => function (Query $query, $data) {

            },
            ReportForm::GROUP_BY_CUSTOMER_DISTRIT => function (Query $query, $data) {

            },
            ReportForm::GROUP_BY_CUSTOMER_CITY => function (Query $query, $data) {

            },
            ReportForm::GROUP_BY_CUSTOMER_PROVINCE => function (Query $query, $data) {

            },
            ReportForm::GROUP_BY_CUSTOMER_ZIP => function (Query $query, $data) {

            },
            ReportForm::GROUP_BY_WEBMASTER_IDENTIFIER => function (Query $query, $data) {

            },
            ReportForm::GROUP_BY_LAST_FOREIGN_OPERATOR => function (Query $query, $data) {

            },
            ReportForm::GROUP_BY_YEAR_MONTH => function (Query $query, $data) {

            },
            ReportForm::GROUP_BY_COUNTRY_DATE => function (Query $query, $data) {

            },
            ReportForm::GROUP_BY_COUNTRY => function (Query $query, $data) {

            },
            ReportForm::GROUP_BY_COUNTRY_PRODUCTS => function (Query $query, $data) {

            },
            ReportForm::GROUP_BY_COUNTRY_LEAD_PRODUCTS => function (Query $query, $data) {

            },
            ReportForm::GROUP_BY_DELIVERY_ZIPCODES => function (Query $query, $data) {

            },
            ReportForm::GROUP_BY_LANDING => function (Query $query, $data) {

            },
            ReportForm::GROUP_BY_CHECK_SENT_AT => function (Query $query, $data) {

            },
            ReportForm::GROUP_BY_CHECK_DONE_AT => function (Query $query, $data) {

            },
            ReportForm::GROUPBY_SMS_CREATED_AT => function (Query $query, $data) {

            },
            ReportForm::GROUPBY_SMS_ANSWERED_AT => function (Query $query, $data) {

            },
            ReportForm::GROUPBY_NOTIFICATION_CREATED_AT => function (Query $query, $data) {

            },
            ReportForm::GROUPBY_NOTIFICATION_ANSWERED_AT => function (Query $query, $data) {

            },
            ReportForm::GROUP_BY_DATE_COUNTRY_DELIVERY => function (Query $query, $data) {

            },
            ReportForm::GROUP_BY_PROVINCE_CITY_SUBDISTRICT_ZIP => function (Query $query, $data) {

            },
            ReportForm::GROUP_BY_SUBDISTRICT_ZIP => function (Query $query, $data) {

            },
            ReportForm::GROUP_BY_COUNTRY_DELIVERY => function (Query $query, $data) {

            },
            ReportForm::GROUP_BY_TS_SPAWN => function (Query $query, $data) {

            },
            ReportForm::GROUP_BY_DATE_PRODUCTS => function (Query $query, $data) {

            },
            ReportForm::GROUP_BY_PRODUCT_CATEGORY => function (Query $query, $data) {

            },
        ];

        if (!array_key_exists($this->form->groupBy, $closures)) {
            throw new InvalidParamException(Yii::t('common', 'Неправильный параметр группировки.'));
        }

        return $closures[$this->form->groupBy];
    }

    /**
     * @param string $attribute
     * @param Query $query
     */
    protected static function applyQueryClosureDate($attribute, $query)
    {
        $condition = 'DATE_FORMAT(FROM_UNIXTIME(' . $attribute . '), "' . Formatter::getMysqlDateFormat() . '")';

        $query->addCondition($condition, Query::GROUP_BY_NAME);
        $query->addCondition($condition, Query::GROUP_BY_LABEL);

        $query->orderBy([$attribute => SORT_DESC]);
    }

    /**
     * @param string $attribute
     * @param Query $query
     */
    protected static function applyQueryClosureOrder($attribute, $query)
    {
        $query->addCondition($attribute, Query::GROUP_BY_NAME);
        $query->addCondition($attribute, Query::GROUP_BY_LABEL);

        $query->orderBy([$attribute => SORT_DESC]);
    }
}
