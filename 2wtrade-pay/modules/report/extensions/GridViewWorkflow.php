<?php

namespace app\modules\report\extensions;

use app\modules\report\components\ReportFormWorkflow;
use Yii;

class GridViewWorkflow extends GridView
{
    /**
     * @inheritdoc
     */
    public function initColumns()
    {

        $this->columns[] = [
            'label' => $this->reportForm->groupByCollection[$this->reportForm->groupBy],
            'attribute' => Query::GROUP_BY_LABEL,
        ];
        $this->columns[] = [
            'label' => Yii::t('common', 'Шт'),
            'attribute' => 'calcparamcount',
        ];
        $this->columns[] = [
            'label' => Yii::t('common', 'Минимальное'),
            'value' => function ($model) {
                $days = intval($model['calcparammin'] / 86400);
                $hours = intval(($model['calcparammin'] - $days * 86400) / 3600);
                $minutes = intval(($model['calcparammin'] - $days * 86400 - $hours * 3600) / 60);
                $sec = $model['calcparammin'] - $days * 86400 - $hours * 3600 - $minutes * 60;
                return $days . ' ' . Yii::t('common', 'дней') . ' ' . ReportFormWorkflow::getTime($hours, $minutes, $sec);
            }
        ];
        $this->columns[] = [
            'label' => Yii::t('common', 'Среднее'),
            'value' => function ($model) {
                $days = intval($model['calcparamavg'] / 86400);
                $hours = intval(($model['calcparamavg'] - $days * 86400) / 3600);
                $minutes = intval(($model['calcparamavg'] - $days * 86400 - $hours * 3600) / 60);
                $sec = $model['calcparamavg'] - $days * 86400 - $hours * 3600 - $minutes * 60;
                return $days . ' ' . Yii::t('common', 'дней') . ' ' . ReportFormWorkflow::getTime($hours, $minutes, $sec);
            }
        ];
        $this->columns[] = [
            'label' => Yii::t('common', 'Максимальное'),
            'value' => function ($model) {
                $days = intval($model['calcparammax'] / 86400);
                $hours = intval(($model['calcparammax'] - $days * 86400) / 3600);
                $minutes = intval(($model['calcparammax'] - $days * 86400 - $hours * 3600) / 60);
                $sec = $model['calcparammax'] - $days * 86400 - $hours * 3600 - $minutes * 60;
                return $days . ' ' . Yii::t('common', 'дней') . ' ' . ReportFormWorkflow::getTime($hours, $minutes, $sec);
            }
        ];

        parent::initColumns();
    }
}
