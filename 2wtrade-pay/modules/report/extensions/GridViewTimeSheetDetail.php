<?php
namespace app\modules\report\extensions;

use app\modules\report\components\ReportFormTimeSheet;
use Yii;
use yii\helpers\Html;

/**
 * Class GridViewTimeSheetDetail
 * @package app\modules\report\extensions
 */
class GridViewTimeSheetDetail extends GridView
{
    /**
     * @var ReportFormTimeSheet
     */
    public $reportForm;

    /**
     * @var array
     */
    public $dates;

    public $export = false;

    /**
     * @var array
     */
    public $plan;

    /**
     * @var array
     */
    public $person;

    /**
     * @inheritdoc
     */
    public function initColumns()
    {
        $weekDays = ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'];
        foreach ($this->dates as $date => $isHoliday) {
            $column = [
                'attribute' => 'dates',
                'header' => date('j', strtotime($date)) . '<br>' . Yii::t('common', $weekDays[date('w', strtotime($date))]),
                'format' => ['decimal', 1],
                'hAlign' => 'center',
                'value' => function ($model) use ($date) {
                    return $model[$date] ?? 0;
                },
                'content' => function ($model) use ($date) {
                    return Yii::$app->formatter->asDecimal($model[$date] ?? 0, 1) . '<br>' .
                           Yii::$app->formatter->asDecimal(($this->plan[$date] ?? 0), 1);
                },
                'contentOptions' => function ($model) use ($date, $isHoliday) {
                    $r = ['class' => 'day'];
                    if ($isHoliday) {
                        $r = ['class' => 'holiday'];
                    } else {
                        if ($this->person['designation_calc_work_time']) {
                            if (!isset($this->plan[$date]) || !$this->plan[$date]) {
                                $r = ['class' => 'weekend'];
                            }
                        } else {
                            if (date('w', strtotime($date)) == 0) {
                                $r = ['class' => 'weekend'];
                            }
                        }
                    }
                    return $r;
                },
            ];
            $this->columns[] = $column;
        }
        parent::initColumns();
    }
}