<?php

namespace app\modules\report\extensions;

use app\modules\callcenter\models\CallCenterRequest;
use app\modules\order\models\OrderStatus;
use Yii;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class GridViewCallCenterDelayByCountry
 * @package app\modules\report\extensions
 */
class GridViewCallCenterDelayByCountry extends GridView
{
    public $reportForm;

    function outDelayStr($diff)
    {

        if (empty($diff)) $diff = 0;

        $days = floor($diff / 86400);
        $hours = floor(($diff % 86400) / 3600);
        $minutes = floor(($diff % 3600) / 60);
        $seconds = $diff % 60;

        if ($hours < 10) $hours = '0' . $hours;
        if ($minutes < 10) $minutes = '0' . $minutes;
        if ($seconds < 10) $seconds = '0' . $seconds;

        return $days . Yii::t("common", " дн.") . ', ' . $hours . ':' . $minutes . ':' . $seconds;
    }


    /**
     * @inheritdoc
     */
    public function initColumns()
    {
        $this->showPageSummary = false;


        $this->rowOptions = function ($model) {
            $color = 'white';
            $d = strtotime($model['yearofdate'] . '-' . $model['monthofdate'] . '-' . $model['dayofdate'] . ' 00:00:00');
            if ($d < (time() - 60 * 60 * 24 * 3)) {
                $color = '#FFE7E7';
            }

            return ['style' => 'background-color: ' . $color];
        };

        $this->columns[] = [
            'label' => Yii::t('common', 'Страна'),
            'attribute' => 'country_name',
            'contentOptions' => ['style' => 'background-color: white'],
        ];
        $this->columns[] = [
            'label' => Yii::t('common', 'Дата'),
            'attribute' => 'yearCreatedAt',
            'format' => 'raw',
            'value' => function ($model) {
                $date = strtotime($model['yearofdate'] . '-' . $model['monthofdate'] . '-' . $model['dayofdate'] . ' 00:00:00');
                $start_date = strtotime('now 00:00:00');
                if ($date != $start_date) {
                    $interval_text = '';
                } else {
                    switch ($model['interval']) {
                        case 0:
                            {
                                $interval_text = ' | 00:00 - 3:59';
                                break;
                            }
                        case 1:
                            {
                                $interval_text = ' | 04:00 - 7:59';
                                break;
                            }
                        case 2:
                            {
                                $interval_text = ' | 08:00 - 11:59';
                                break;
                            }
                        case 3:
                            {
                                $interval_text = ' | 12:00 - 15:59';
                                break;
                            }
                        case 4:
                            {
                                $interval_text = ' | 16:00 - 19:59';
                                break;
                            }
                        case 5:
                            {
                                $interval_text = ' | 20:00 - 23:59';
                                break;
                            }
                        default:
                            {
                                $interval_text = '';
                            }
                    }
                }

                if ($date == 0) {
                    $date_text = 'Без даты';
                } else {
                    $date_text = date('d.m.Y', strtotime($model['yearofdate'] . '-' . $model['monthofdate'] . '-' . $model['dayofdate'])) . $interval_text;
                }

                return $date_text . '<span style = "display: none;">' . $model['country_name'] . '</span>';
            },
        ];
        $this->columns[] = [
            'label' => Yii::t('common', 'Колл-Центр'),
            'attribute' => 'callcenter_name',
        ];
        $this->columns[] = [
            'label' => Yii::t('common', 'Кол-во заказов'),
            'attribute' => 'order_count',
            'content' => function ($model) {
                $date = $model['dayofdate'] . '.' . $model['monthofdate'] . '.' . $model['yearofdate'];
                return Html::a($model['order_count'], Url::toRoute([
                    '/order/index/index',
                    'force_country_slug' => $model['slug'],
                    'DateFilter' => [
                        'dateType' => 'c_update_at',
                        'dateFrom' => $date,
                        'dateTo' => $date,
                    ],
                    'CallCenterFilter' => [
                        'callCenter' => $model['rep_callcenter_id']
                    ],
                    'StatusFilter' => [
                        'status' => OrderStatus::getSentToCCList(),
                    ],
                    'StatusCallCenterRequestFilter' => [
                        'statusCallCenterRequest' => [CallCenterRequest::STATUS_IN_PROGRESS, CallCenterRequest::STATUS_ERROR],
                    ],
                ]));
            },
        ];
        $this->columns[] = [
            'label' => Yii::t('common', 'Последнее обновление, минимум'),
            'attribute' => 'min_delay',
            'value' => function ($model) {
                return $this->outDelayStr($model['min_delay']);
            },
        ];
        $this->columns[] = [
            'label' => Yii::t('common', 'Последнее обновление, среднее'),
            'attribute' => 'avg_delay',
            'value' => function ($model) {
                return $this->outDelayStr($model['avg_delay']);
            },
        ];
        $this->columns[] = [
            'label' => Yii::t('common', 'Последнее обновление, максимум'),
            'attribute' => 'max_delay',
            'value' => function ($model) {
                return $this->outDelayStr($model['max_delay']);
            },
        ];

        parent::initColumns();
    }
}
