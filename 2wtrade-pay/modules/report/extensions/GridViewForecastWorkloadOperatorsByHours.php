<?php
namespace app\modules\report\extensions;

use Yii;

/**
 * Class GridViewForecastWorkloadOperatorsByHours
 * @package app\modules\report\extensions
 */
class GridViewForecastWorkloadOperatorsByHours extends GridView
{
    /**
     * @var
     */
    public $reportForm;

    /**
     * @inheritdoc
     */
    public function initColumns()
    {
        $this->showPageSummary = false;

        $this->columns[] = [
            'label' => Yii::t('common', 'Рабочие часы'),
            'attribute' => 'hour',
        ];
        $this->columns[] = [
            'label' => Yii::t('common', 'Прогноз загруженности операторов'),
            'attribute' => 'leadcount',
        ];

        parent::initColumns();
    }
}
