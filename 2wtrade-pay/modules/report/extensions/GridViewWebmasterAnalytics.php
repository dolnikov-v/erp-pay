<?php

namespace app\modules\report\extensions;

use app\modules\order\models\OrderStatus;
use app\modules\report\components\ReportFormWebmasterAnalytics;
use Yii;

/**
 * Class GridViewWebmasterAnalytics
 * @package app\modules\report\extensions
 */
class GridViewWebmasterAnalytics extends GridView
{
    /**
     * @var ReportFormWebmasterAnalytics
     */
    public $reportForm;

    /**
     * @var bool
     */
    public $byStatus = false;

    public $resizableColumns = false;

    /**
     * @inheritdoc
     */
    public function initColumns()
    {
        $this->columns[] = [
            'attribute' => 'webmaster_identifier',
            'label' => Yii::t('common', 'ID вебмастера'),
            'pageSummary' => Yii::t('common', 'Всего'),
            'group' => true,
        ];
        $this->columns[] = [
            'attribute' => 'country_name',
            'label' => Yii::t('common', 'Страна'),
            'pageSummary' => false,
            'group' => true,
            'width' => '100px',
        ];
        $this->columns[] = [
            'attribute' => 'call_center_name',
            'label' => Yii::t('common', 'Колл-центр'),
            'pageSummary' => false,
            'group' => true,
            'width' => '100px',
        ];
        $this->columns[] = [
            'attribute' => 'lead_product_name',
            'label' => Yii::t('common', 'Продукты'),
            'pageSummary' => false,
            'group' => false,
            'width' => '100px',
        ];
        $this->columns[] = [
            'attribute' => 'vsego',
            'class' => DataColumnCount::className(),
            'label' => Yii::t('common', 'Всего лидов'),
            'width' => '100px',
        ];

        if (!$this->byStatus) {
            foreach ($this->reportForm->numberCalls as $i) {
                $this->columns[] = [
                    'attribute' => 'call_' . $i,
                    'class' => DataColumnCount::className(),
                    'label' => Yii::t(
                        'common',
                        '{n, plural, =0{нет звонков} =1{1 звонок} one{# звонок} few{# звонка} many{# звонков} other{# звонков} =9{9+ звонков}}',
                        ['n' => $i]
                    ),
                    'width' => '100px',
                    'columnLinkParams' => ['calls_count' => $i],
                ];
            }
        }

        if ($this->byStatus) {

            $mapStatuses = $this->reportForm->getMapStatuses();
            $orderStatuses = OrderStatus::find()->collection();

            foreach ($mapStatuses as $name => $statuses) {

                $innerStatuses = [];

                foreach ($statuses as $status) {
                    $innerStatuses[] = $status . '. ' . $orderStatuses[$status];
                }

                $this->columns[] = [
                    'label' => Yii::t('common', $name),
                    'attribute' => Yii::t('common', $name),
                    'class' => DataColumnCount::className(),
                    'headerOptions' => [
                        'data-container' => 'body',
                        'data-toggle' => 'tooltip',
                        'title' => Yii::t('common', 'Статусы: {statuses}', ['statuses' => implode(', ', $innerStatuses)]),
                    ],
                    'contentOptions' => [
                        'class' => 'status_calls_wrap'
                    ],
                    'beforeContent' => function ($model) use ($name) {
                        $return = '';
                        if (isset($model['status_calls'][$name])) {
                            $return .= '<div class="status_calls">';
                            ksort($model['status_calls'][$name]);
                            $tmp = [];
                            foreach ($this->reportForm->numberCalls as $i) {
                                if (isset($model['status_calls'][$name][$i])) {
                                    $tmp[] = '<span title="' . Yii::t(
                                            'common',
'{s, plural, =0{нет заказов} =1{1 заказ} one{# заказ} few{# заказа} many{# заказов} other{# заказов}} {n, plural, =0{без звонков} =1{с 1 звонком} one{с # звонком} few{с # звонками} many{с # звонками} other{с # звонками} =9{с 9+ звонками}}',
                                            [
                                                's' => $model['status_calls'][$name][$i],
                                                'n' => $i
                                            ]
                                        ) . '">' . $model['status_calls'][$name][$i] . '</span>';
                                } else {
                                    $tmp[] = '.';
                                }
                            }
                            if ($tmp) {
                                $return .= preg_replace('/\D+$/', '', implode('-', $tmp));
                            }
                            $return .= '</div>';
                        }
                        return $return;
                    },
                    'width' => '150px',
                    'makeLinkToOrders' => true,
                    'columnLinkParams' => ['statuses' => implode(',', $statuses)],
                ];
            }
        }

        parent::initColumns();
    }
}
