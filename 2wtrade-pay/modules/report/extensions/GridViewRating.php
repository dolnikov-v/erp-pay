<?php
namespace app\modules\report\extensions;

use app\modules\rating\components\ReportFormRatingProduct;
use Yii;

/**
 * Class GridViewRating
 * @package app\modules\report\extensions
 */
class GridViewRating extends GridView
{
    public $symbol;
    public $toDollar;

    /**
     * @inheritdoc
     */
    public function initColumns()
    {
        $this->columns = [
            [
                'label' => Yii::t('common', 'Страна'),
                'group' => true,
                'groupedRow' => true,
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'value' => function ($model) {
                    return ReportFormRatingProduct::$countries[$model['country_id']];
                },
            ]
        ];
        foreach (ReportFormRatingProduct::$days as $day) {
            $this->columns[] = [
                'label' => Yii::t('common', '{day} дней', ['day' => $day]),
                'attribute' => 'value' . $day,
                'value' => function ($model) use ($day) {
                    if ($this->toDollar && !isset(ReportFormRatingProduct::$currencies[$model['country_id']])) {
                        return null;
                    }
                    if (isset($model["product".$day])) {
                        return ReportFormRatingProduct::$products[$model["product".$day]] . ' (' . ($this->toDollar ? round($model['value' . $day] / ReportFormRatingProduct::$currencies[$model['country_id']], 2) : round($model['value' . $day], 2)) . " " . $this->symbol . ')';
                    }
                    return null;
                },
            ];
        }

        parent::initColumns();
    }
}