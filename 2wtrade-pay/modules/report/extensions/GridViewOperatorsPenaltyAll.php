<?php
namespace app\modules\report\extensions;

use app\modules\report\components\ReportFormOperatorsPenalty;
use Yii;

/**
 * Class GridViewOperatorsPenaltyAll
 * @package app\modules\report\extensions
 */
class GridViewOperatorsPenaltyAll extends GridView
{
    /**
     * @var ReportFormOperatorsPenalty
     */
    public $reportForm;

    /**
     * @inheritdoc
     */
    public function initColumns()
    {
        $this->columns[] = [
            'label' => Yii::t('common', 'Колл-центр'),
            'attribute' => 'callCenterId',
        ];
        $this->columns[] = [
            'label' => Yii::t('common', 'Оператор'),
            'attribute' => 'operatorLogin',
        ];
        $this->columns[] = [
            'label' => Yii::t('common', 'Сумма').", USD",
            'attribute' => 'penalty',
        ];

        parent::initColumns();
    }
}
