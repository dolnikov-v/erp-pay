<?php

namespace app\modules\report\extensions;

use Yii;
use app\modules\report\components\filters\TypeDatePartFilter;
use app\modules\report\components\ReportFormBuyoutByCountry;
use kartik\grid\GridView;

/**
 * Class GridViewBuyoutByCountry
 * @package app\modules\report\extensions
 */
class GridViewBuyoutByCountry extends GridView
{
    /** @var ReportFormBuyoutByCountry */
    public $reportForm;


    /**
     * @param array $model
     * @return string
     */
    private function formatDate($model)
    {
        $year = $model['yearcreatedat'];
        $week = $model['weekcreatedat'];
        $month = $model['monthcreatedat'];
        $day = $model['daycreatedat'];

        $date = '';
        switch ($this->reportForm->getTypeDatePartFilter()->type_date_part) {

            case TypeDatePartFilter::TYPE_DATE_PART_DAY:
                $date = Yii::$app->formatter->asDate(strtotime($year . '-' . $month . '-' . $day));
                break;

            case TypeDatePartFilter::TYPE_DATE_PART_MONTH:
                $date = Yii::$app->formatter->asMonth(strtotime($year . '-' . $month . '-01'));
                break;

            case TypeDatePartFilter::TYPE_DATE_PART_WEEK:
                if ($week == 0) {
                    $time = strtotime($year . '-01-01');
                } else {
                    $timestamp = mktime(0, 0, 0, 1, 1, $year) + ($week * 7 * 86400);
                    $time = $timestamp - 86400 * (date('N', $timestamp) - 1);
                }
                $date = Yii::$app->formatter->asDate($time);
                break;
        }

        return $date;
    }


    /**
     * @inheritdoc
     */
    public function initColumns()
    {
        $this->showPageSummary = false;

        $this->columns[] = [
            'label' => Yii::t('common', 'Страна'),
            'attribute' => 'country_name',
            'group' => true,
        ];
        $this->columns[] = [
            'label' => Yii::t('common', 'Дата'),
            'attribute' => 'yearCreatedAt',
            'format' => 'raw',
            'value' => function ($model) {
                return $this->formatDate($model) . '<span style = "display: none;">' . $model['country_name'] . '</span>';
            },
            'group' => true,
        ];
        $this->columns[] = [
            'label' => Yii::t('common', 'Количество'),
            'attribute' => 'vykupleno',
        ];
        $this->columns[] = [
            'label' => Yii::t('common', '% от апрувов'),
            'attribute' => 'podtverzdeno',
            'value' => function ($model) {
                if ($model['podtverzdeno'] != 0) {
                    return round(($model['vykupleno'] / $model['podtverzdeno'] * 100), 2) . "%";
                } else {
                    return 0 . ".00%";
                }
            },
        ];

        parent::initColumns();
    }
}
