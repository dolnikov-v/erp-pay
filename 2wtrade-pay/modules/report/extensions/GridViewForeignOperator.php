<?php
namespace app\modules\report\extensions;

use app\modules\order\models\OrderStatus;
use app\modules\report\components\ReportFormForeignOperator;
use Yii;

/**
 * Class GridViewForeignOperator
 * @package app\modules\report\extensions
 */
class GridViewForeignOperator extends GridView
{
    /**
     * @var ReportFormForeignOperator
     */
    public $reportForm;

    /**
     * @inheritdoc
     */
    public function initColumns()
    {
        $this->columns[] = [
            'attribute' => 'id',
            'label' => Yii::t('common', 'Внешний оператор'),
            'pageSummary' => Yii::t('common', 'Всего'),
            'width' => '110px',
        ];
        $this->columns[] = [
            'attribute' => 'count_lead',
            'class' => DataColumnCount::className(),
            'label' => Yii::t('common', 'Количество лидов'),
            'width' => '110px',
        ];

        $mapStatuses = $this->reportForm->getMapStatuses();
        $orderStatuses = OrderStatus::find()->collection();

        foreach ($mapStatuses as $name => $statuses) {

            if ($name == Yii::t('common', 'Всего')) continue;

            $innerStatuses = [];

            foreach ($statuses as $status) {
                $innerStatuses[] = $status . '. ' . $orderStatuses[$status];
            }

            if ($name != Yii::t('common', 'Выкуплено')) {
                $this->columns[] = [
                    'class' => DataColumnCount::className(),
                    'attribute' => $name,
                    'columnLinkParams' => implode(',', $statuses),
                    'headerOptions' => [
                        'data-container' => 'body',
                        'data-toggle' => 'tooltip',
                        'title' => Yii::t('common', 'Статусы: {statuses}', ['statuses' => implode(', ', $innerStatuses)]),
                    ],
                    'width' => '110px',
                ];
            }
            else {
                $this->columns[] = [
                    'attribute' => $name,
                    'value' => function ($data) {
                        return $data[Yii::t('common', 'Выкуплено')] ." (" .round($data[Yii::t('common', 'vikup_alter_procent')], 2) ."%)";
                    },
                    'headerOptions' => [
                        'data-container' => 'body',
                        'data-toggle' => 'tooltip',
                        'title' => Yii::t('common', 'Статусы: {statuses}', ['statuses' => implode(', ', $innerStatuses)]),
                    ],
                    'width' => '110px',
                ];
            }

        }

        if (Yii::$app->user->can('view_finance_director')) {
            $this->columns[] = [
                'attribute' => 'sum',
                'value' => function ($data) {
                    if (!$data['sum']) return "-";
                    return number_format($data['sum'], 2);
                },
                'label' => Yii::t('common', 'Сумма') . ", USD",
                'headerOptions' => [
                    'data-container' => 'body',
                    'data-toggle' => 'tooltip',
                    'title' => Yii::t('common', 'Сумма выкупленных заказов'),
                ],
                'width' => '110px',
            ];
        }

        $this->columns[] = [
            'attribute' => 'sr_cek_kc',
            'value' => function ($data) {
                if (!$data['sr_cek_kc']) return "-";
                return round($data['sr_cek_kc'], 2);
            },
            'label' => Yii::t('common', 'Ср. чек КЦ').", USD",
            'width' => '110px',
        ];

        $this->columns[] = [
            'attribute' => 'sr_cek_buy',
            'value' => function ($data) {
                if (!$data['sr_cek_buy']) return "-";
                return round($data['sr_cek_buy'], 2);
            },
            'label' => Yii::t('common', 'Ср. чек по выкупам').", USD",
            'width' => '110px',
        ];

        $this->columns[] = [
            'value' => function ($data) {
                if (!$data['count_lead']) return "-";
                return round($data['sum'] / $data['count_lead'], 2);
            },
            'label' => Yii::t('common', 'Доходность на лид'),
            'width' => '110px',
        ];

        $this->columns[] = [
            'value' => function ($data) {
                if (!$data[Yii::t('common', 'Подтверждено')]) return "-";
                return round($data['sum'] / $data[Yii::t('common', 'Подтверждено')], 2);
            },
            'label' => Yii::t('common', 'Доходность на апрув'),
            'width' => '110px',
        ];
        parent::initColumns();
    }
}
