<?php
namespace app\modules\report\extensions;

use app\modules\report\components\ReportFormCallCenterOperatorsOnline;
use Yii;

/**
 * Class GridViewCallCenterOperatorsOnline
 * @package app\modules\report\extensions
 */
class GridViewCallCenterOperatorsOnline extends GridView
{
    const DATE_DAY = 'day';
    const DATE_MOUTH = 'month';
    const DATE_WEEK = 'week';
    const DATE_YEAR = 'year';

    /**
     * @var ReportFormCallCenterOperatorsOnline
     */
    public $reportForm;

    /**
     * @inheritdoc
     */
    public function initColumns()
    {
        $this->showPageSummary = false;

        $get = Yii::$app->request->get();
        $type_date_part = isset($get['s']['type_date_part']) ? $get['s']['type_date_part'] : self::DATE_DAY;

        switch ($type_date_part) {
            case self::DATE_DAY :
                $columnTpl = ['columnName' => Yii::t('common', 'Дата'), 'tpl' => 'd.m.Y'];
                break;
            case self::DATE_MOUTH :
                $columnTpl = ['columnName' => Yii::t('common', 'Месяц'), 'tpl' => 'm.Y'];
                break;
            case self::DATE_YEAR :
                $columnTpl = ['columnName' => Yii::t('common', 'Год'), 'tpl' => 'Y'];
                break;
            case self::DATE_WEEK :
                $columnTpl = ['columnName' => Yii::t('common', 'Неделя'), 'tpl' => 'd.m.Y'];
                break;
            default :
                $columnTpl = ['columnName' => Yii::t('common', 'Дата'), 'tpl' => 'd.m.Y'];
                break;
        }

        $this->columns[] = [
            'label' => Yii::t('common', 'Направление'),
            'headerOptions' => ['style' => 'text-align:center;', 'class' => 'width-150'],
            'attribute' => 'country_name',
        ];
        $this->columns[] = [
            'label' => $columnTpl['columnName'],
            'headerOptions' => ['style' => 'text-align:center;', 'class' => 'width-100'],
            'attribute' => 'yearCreatedAt',
            'format' => 'raw',
            'value' => function ($model) use ($columnTpl) {
                return date($columnTpl['tpl'], strtotime($model['year'] . '-' . $model['month'] . '-' . $model['day'] . ' 00:00:00')) . '<span style = "display: none;">' . $model['country_name'] . '</span>';
            },
        ];
        $this->columns[] = [
            'label' => Yii::t('common', 'Операторов онлайн'),
            'headerOptions' => ['style' => 'text-align:center;', 'class' => 'width-100'],
            'attribute' => 'countOperators',
            'format' => 'raw',
            'value' => function ($model) {
                return $model['countOperators'];
            }
        ];
        $this->columns[] = [
            'label' => Yii::t('common', 'Список операторов'),
            'attribute' => 'listOperators',
            'headerOptions' => ['style' => 'text-align:center;', 'class' => 'width-600'],
            'format' => 'raw',
            'value' => function ($model) {
                return $model['listOperators'];
            }
        ];

        parent::initColumns();
    }
}
