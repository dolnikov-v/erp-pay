<?php
namespace app\modules\report\extensions;

use app\modules\report\components\filters\DateFilter;
use app\modules\report\components\ReportFormOperatorWorkdays;
use app\helpers\i18n\Formatter;
use Yii;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class GridViewOperatorWorkdays
 * @package app\modules\report\extensions
 */
class GridViewOperatorWorkdays extends GridView
{
    /**
     * @var ReportFormOperatorWorkdays
     */
    public $reportForm;

    /**
     * @inheritdoc
     */
    public function initColumns()
    {
        $statuses = $this->reportForm->getMapStatuses();
        $this->reportForm->withPercent = 'off';

        $this->columns[] = [
            'label' => Yii::t('common', 'Операторы'),
            'value' => function ($model) {
                return $model['operator'];
            },
        ];

        if (isset($this->dataProvider->allModels[0])) {
            foreach ($this->dataProvider->allModels[0]['data'] as $key => $hours) {
                $this->columns[] = [
                    'label' => Yii::$app->formatter->asDate($key, Formatter::getShortDateFormat()),
                    'format' => 'raw',
                    'contentOptions' => function ($model) use ($key) {
                        if (is_numeric($model['data'][$key])) {
                            return ['style' => 'text-align:center; background-color:#ddfade;'];
                        }
                        return ['style' => 'text-align:center'];
                    },
                    'value' => function ($model, $statuses) use ($key) {
                        $operatorId = $this->getOperatorId($model['operator']);
                        $route = [
                            '/order/index/index',
                            'DateFilter' => [
                                'dateFrom' => Yii::$app->formatter->asDate($key, Formatter::getDateFormat()),
                                'dateTo' => Yii::$app->formatter->asDate($key, Formatter::getDateFormat()),
                                'dateType' => DateFilter::TYPE_CALL_CENTER_APPROVED_AT
                            ],
                            'NumberFilter' => [
                                'entity' => 'last_foreign_operator',
                                'number' => $operatorId,
                            ]
                        ];
                        $route['StatusFilter'] = ['status' => $statuses[Yii::t('common', 'Подтверждено')]];

                        return Html::a($model['data'][$key], Url::toRoute($route), ['target' => '_blank', 'title' => Yii::t('common', 'Подтверждено')]);
                    },
                ];
            }
        }

        $this->columns[] = [
            'label' => Yii::t('common', 'Дней'),
            'contentOptions' => ['style' => 'text-align:center; font-weight:bold; background-color:#fadddd;'],
            'value' => function ($model) {
                return $model['workdays'];
            },
        ];


        parent::initColumns();
    }

    /**
     * @param $str
     * @return int|string
     */
    private function getOperatorId($str)
    {

        $res = explode('-', $str);
        if (!empty($res)) {
            $res = $res[count($res) - 1];
            if (is_numeric($res)) {
                return intval($res);
            }
        }

        return '';
    }
}
