<?php
namespace app\modules\report\extensions;

use app\modules\report\components\ReportFormSalary;
use Yii;
use app\modules\order\models\OrderStatus;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class GridViewSalary
 * @package app\modules\report\extensions
 */
class GridViewSalary extends GridView
{
    /**
     * @var ReportFormSalary
     */
    public $reportForm;

    public function init()
    {
        $officeFilter = $this->reportForm->getOfficeFilter();
        $text = '';
        if ($officeFilter->office) {
            foreach ($officeFilter->office as $office) {
                $text .= $officeFilter->offices[$office] ?? '';
            }
            $text = Yii::t('common', $text, [], "en-US");
            $text = preg_replace('/[^\w]+/', '_', $text);
        }
        $this->exportFilename = $text . '_' . Yii::$app->formatter->asDate($this->reportForm->getDateWorkFilter()->from, 'php:Y-m-d') . '_' . Yii::$app->formatter->asDate($this->reportForm->getDateWorkFilter()->to, 'php:Y-m-d');
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function initColumns()
    {
        $this->rowOptions = function ($model) {
            if (empty($model['active'])) {
                return [
                    'class' => 'tr-vertical-align-middle text-muted',
                ];
            }
            if (empty($model['approved'])) {
                return [
                    'class' => 'tr-vertical-align-middle text-danger',
                ];
            }
            return [
                'class' => 'tr-vertical-align-middle',
            ];
        };

        $this->columns[] = ['class' => 'kartik\grid\SerialColumn'];

        $this->columns[] = [
            'attribute' => 'parent_id',
            'label' => Yii::t('common', 'Руководитель'),
            'pageSummary' => false,
            'pageSummaryOptions' => ['hidden' => true],
            'hiddenFromExport' => true,
            'group' => true,  // enable grouping,
            'groupedRow' => true,                    // move grouped column to a single grouped row
            'groupOddCssClass' => 'kv-grouped-row',  // configure odd group cell css class
            'groupEvenCssClass' => 'kv-grouped-row', // configure even group cell css class
            'value' => function ($model) {
                return (!empty($model['parent_id']) ? Yii::t('common', $model['parent_designation']) . ': ' . $model['parent_name'] : Yii::t('common', 'Руководитель не задан')) . ' - ' . Yii::t('common', $model['office']);
            },
            'groupFooter' => function ($model, $key, $index, $widget) { // Closure method
                $col1 = $col2 = $col3 = 7;
                return [
                    'mergeColumns' => [[2, $col1]], // columns to merge in summary
                    'content' => [             // content to show in each summary cell
                        2 => Yii::t('common', 'Итого по региону') . (!empty($model['parent_id']) ? ' ' . $model['parent_name'] : ''),
                        ++$col1 => GridView::F_SUM,
                        ++$col1 => GridView::F_SUM,
                        ++$col1 => GridView::F_SUM,
                        ++$col1 => GridView::F_SUM,
                        ++$col1 => GridView::F_SUM,
                        ++$col1 => GridView::F_SUM,
                        ++$col1 => GridView::F_SUM,
                        ++$col1 => GridView::F_SUM,
                        ++$col1 => GridView::F_SUM,
                        ++$col1 => GridView::F_SUM,
                        ++$col1 => GridView::F_SUM,
                        ++$col1 => GridView::F_SUM,
                        ++$col1 => GridView::F_SUM,
                        ++$col1 => GridView::F_SUM,
                        ++$col1 => GridView::F_SUM,
                        ++$col1 => GridView::F_SUM,
                        ++$col1 => GridView::F_SUM,
                        ++$col1 => GridView::F_SUM,
                        ++$col1 => GridView::F_SUM,
                        ++$col1 => GridView::F_SUM,
                        ++$col1 => GridView::F_SUM,
                        ++$col1 => GridView::F_AVG,
                        ++$col1 => GridView::F_AVG,
                        ++$col1 => GridView::F_SUM,
                    ],
                    'contentFormats' => [      // content reformatting for each summary cell
                        ++$col2 => ['format' => 'number', 'decimals' => 2, 'decPoint' => Yii::$app->get('formatter')->decimalSeparator, 'thousandSep' => Yii::$app->get('formatter')->thousandSeparator],
                        ++$col2 => ['format' => 'number', 'decimals' => 2, 'decPoint' => Yii::$app->get('formatter')->decimalSeparator, 'thousandSep' => Yii::$app->get('formatter')->thousandSeparator],
                        ++$col2 => ['format' => 'number', 'decimals' => 2, 'decPoint' => Yii::$app->get('formatter')->decimalSeparator, 'thousandSep' => Yii::$app->get('formatter')->thousandSeparator],
                        ++$col2 => ['format' => 'number', 'decimals' => 2, 'decPoint' => Yii::$app->get('formatter')->decimalSeparator, 'thousandSep' => Yii::$app->get('formatter')->thousandSeparator],
                        ++$col2 => ['format' => 'number', 'decimals' => 2, 'decPoint' => Yii::$app->get('formatter')->decimalSeparator, 'thousandSep' => Yii::$app->get('formatter')->thousandSeparator],
                        ++$col2 => ['format' => 'number', 'decimals' => 2, 'decPoint' => Yii::$app->get('formatter')->decimalSeparator, 'thousandSep' => Yii::$app->get('formatter')->thousandSeparator],
                        ++$col2 => ['format' => 'number', 'decimals' => 2, 'decPoint' => Yii::$app->get('formatter')->decimalSeparator, 'thousandSep' => Yii::$app->get('formatter')->thousandSeparator],
                        ++$col2 => ['format' => 'number', 'decimals' => 2, 'decPoint' => Yii::$app->get('formatter')->decimalSeparator, 'thousandSep' => Yii::$app->get('formatter')->thousandSeparator],
                        ++$col2 => ['format' => 'number', 'decimals' => 2, 'decPoint' => Yii::$app->get('formatter')->decimalSeparator, 'thousandSep' => Yii::$app->get('formatter')->thousandSeparator],
                        ++$col2 => ['format' => 'number', 'decimals' => 2, 'decPoint' => Yii::$app->get('formatter')->decimalSeparator, 'thousandSep' => Yii::$app->get('formatter')->thousandSeparator],
                        ++$col2 => ['format' => 'number', 'decimals' => 2, 'decPoint' => Yii::$app->get('formatter')->decimalSeparator, 'thousandSep' => Yii::$app->get('formatter')->thousandSeparator],
                        ++$col2 => ['format' => 'number', 'decimals' => 2, 'decPoint' => Yii::$app->get('formatter')->decimalSeparator, 'thousandSep' => Yii::$app->get('formatter')->thousandSeparator],
                        ++$col2 => ['format' => 'number', 'decimals' => 2, 'decPoint' => Yii::$app->get('formatter')->decimalSeparator, 'thousandSep' => Yii::$app->get('formatter')->thousandSeparator],
                        ++$col2 => ['format' => 'number', 'decimals' => 2, 'decPoint' => Yii::$app->get('formatter')->decimalSeparator, 'thousandSep' => Yii::$app->get('formatter')->thousandSeparator],
                        ++$col2 => ['format' => 'number', 'decimals' => 2, 'decPoint' => Yii::$app->get('formatter')->decimalSeparator, 'thousandSep' => Yii::$app->get('formatter')->thousandSeparator],
                        ++$col2 => ['format' => 'number', 'decimals' => 2, 'decPoint' => Yii::$app->get('formatter')->decimalSeparator, 'thousandSep' => Yii::$app->get('formatter')->thousandSeparator],
                        ++$col2 => ['format' => 'number', 'decimals' => 2, 'decPoint' => Yii::$app->get('formatter')->decimalSeparator, 'thousandSep' => Yii::$app->get('formatter')->thousandSeparator],
                        ++$col2 => ['format' => 'number', 'decimals' => 2, 'decPoint' => Yii::$app->get('formatter')->decimalSeparator, 'thousandSep' => Yii::$app->get('formatter')->thousandSeparator],
                        ++$col2 => ['format' => 'number', 'decimals' => 2, 'decPoint' => Yii::$app->get('formatter')->decimalSeparator, 'thousandSep' => Yii::$app->get('formatter')->thousandSeparator],
                        ++$col2 => ['format' => 'number', 'decimals' => 2, 'decPoint' => Yii::$app->get('formatter')->decimalSeparator, 'thousandSep' => Yii::$app->get('formatter')->thousandSeparator],
                        ++$col2 => ['format' => 'number'],
                        ++$col2 => ['format' => 'number', 'decimals' => 2, 'decPoint' => Yii::$app->get('formatter')->decimalSeparator, 'thousandSep' => Yii::$app->get('formatter')->thousandSeparator],
                        ++$col2 => ['format' => 'number', 'decimals' => 2, 'decPoint' => Yii::$app->get('formatter')->decimalSeparator, 'thousandSep' => Yii::$app->get('formatter')->thousandSeparator],
                        ++$col2 => ['format' => 'number'],
                    ],
                    'contentOptions' => [      // content html attributes for each summary cell
                        1 => ['style' => 'font-variant:small-caps'],
                        ++$col3 => ['style' => 'text-align:right'],
                        ++$col3 => ['style' => 'text-align:right'],
                        ++$col3 => ['style' => 'text-align:right'],
                        ++$col3 => ['style' => 'text-align:right'],
                        ++$col3 => ['style' => 'text-align:right'],
                        ++$col3 => ['style' => 'text-align:right'],
                        ++$col3 => ['style' => 'text-align:right'],
                        ++$col3 => ['style' => 'text-align:right'],
                        ++$col3 => ['style' => 'text-align:right'],
                        ++$col3 => ['style' => 'text-align:right'],
                        ++$col3 => ['style' => 'text-align:right'],
                        ++$col3 => ['style' => 'text-align:right'],
                        ++$col3 => ['style' => 'text-align:right'],
                        ++$col3 => ['style' => 'text-align:right'],
                        ++$col3 => ['style' => 'text-align:right'],
                        ++$col3 => ['style' => 'text-align:right'],
                        ++$col3 => ['style' => 'text-align:right'],
                        ++$col3 => ['style' => 'text-align:right'],
                        ++$col3 => ['style' => 'text-align:right'],
                        ++$col3 => ['style' => 'text-align:right'],
                        ++$col3 => ['style' => 'text-align:left'],
                        ++$col3 => ['style' => 'text-align:right'],
                        ++$col3 => ['style' => 'text-align:right'],
                        ++$col3 => ['style' => 'text-align:right'],
                    ],
                    // html attributes for group summary row
                    'options' => ['class' => 'info', 'style' => 'font-style:italic;']
                ];
            }
        ];
        $this->columns[] = [
            'attribute' => 'country_names',
            'label' => Yii::t('common', 'Направление'),
            'pageSummary' => Yii::t('common', 'Итого'),
            'content' => function ($model) {
                $r = [];
                if (isset($model['country_names'])) {
                    foreach (explode(',', $model['country_names']) as $name) {
                        $r[$name] = Yii::t('common', $name);
                    }
                }
                return implode('<br>', $r);
            },
        ];
        $this->columns[] = [
            'attribute' => 'fio',
            'label' => Yii::t('common', 'ФИО'),
            'content' => function ($model) {
                $route = [
                    '/report/salary/index',
                    's' => [
                        'from' => $this->reportForm->getDateWorkFilter()->from,
                        'to' => $this->reportForm->getDateWorkFilter()->to,
                        'office' => $this->reportForm->getOfficeFilter()->office,
                        'monthlyStatement' => $this->reportForm->getCallCenterMonthlyStatementFilter()->monthlyStatement,
                    ],
                    'person' => $model['id'] ?? 0
                ];
                return Html::a($model['fio'], Url::toRoute($route), [
                    'target' => '_blank',
                    'title' => Yii::t('common', 'Подробнее')
                ]);
            },
            'pageSummary' => '',
        ];
        $this->columns[] = [
            'attribute' => 'designation',
            'label' => Yii::t('common', 'Должность'),
            'pageSummary' => '',
            'value' => function ($model) {
                return isset($model['designation']) ? Yii::t('common', $model['designation']) : '';
            },
        ];
        $this->columns[] = [
            'attribute' => 'account_number',
            'label' => Yii::t('common', 'Номер счета'),
            'pageSummary' => false,
            'hiddenFromExport' => false,
            'format' => 'raw',
            'value' => function ($model) {
                return $model['account_number'] . '&nbsp;';
            },
            'visible' => Yii::$app->user->can('view_salary_person_numbers')
        ];
        $this->columns[] = [
            'attribute' => 'passport_id_number',
            'label' => Yii::t('common', 'Паспорт'),
            'pageSummary' => false,
            'hiddenFromExport' => false,
            'format' => 'raw',
            'value' => function ($model) {
                return $model['passport_id_number'] . '&nbsp;';
            },
            'visible' => Yii::$app->user->can('view_salary_person_numbers')
        ];
        $this->columns[] = [
            'attribute' => 'call_center_user_logins',
            'label' => Yii::t('common', 'Логин'),
            'pageSummary' => '',
            'content' => function ($model) {
                return isset($model['call_center_user_logins']) ? str_replace(',', '<br>', $model['call_center_user_logins']) : '';
            },
        ];
        $this->columns[] = [
            'attribute' => 'salary_local',
            'label' => Yii::t('common', 'Оклад в местной валюте в месяц'),
            'format' => ['decimal', 2],
            'hAlign' => 'right',
            'value' => function ($model) {
                return $model['salary_local'] ?? 0;
            },
            'visible' => $this->reportForm->getCalcSalaryLocal()
        ];
        $this->columns[] = [
            'attribute' => 'salary_usd',
            'label' => Yii::t('common', 'Оклад в USD'),
            'format' => ['decimal', 2],
            'hAlign' => 'right',
            'value' => function ($model) {
                return $model['salary_usd'] ?? 0;
            },
            'visible' => $this->reportForm->getCalcSalaryUSD()
        ];
        $this->columns[] = [
            'attribute' => 'salary_hour_local',
            'label' => Yii::t('common', 'Оклад в местной валюте в час'),
            'format' => ['decimal', 2],
            'hAlign' => 'right',
            'value' => function ($model) {
                return $model['salary_hour_local'] ?? 0;
            },
            'visible' => $this->reportForm->getCalcSalaryLocal()
        ];
        $this->columns[] = [
            'attribute' => 'salary_hour_usd',
            'label' => Yii::t('common', 'Оклад в USD в час'),
            'format' => ['decimal', 2],
            'hAlign' => 'right',
            'value' => function ($model) {
                return $model['salary_hour_usd'] ?? 0;
            },
            'visible' => $this->reportForm->getCalcSalaryUSD()
        ];
        $this->columns[] = [
            'attribute' => 'count_normal_hours',
            'label' => Yii::t('common', 'Норма часов по КЗоТ'),
            'format' => ['decimal', 2],
            'hAlign' => 'right',
            'value' => function ($model) {
                return $model['count_normal_hours'] ?? 0;
            },
        ];
        $this->columns[] = [
            'attribute' => 'count_time',
            'label' => Yii::t('common', 'Отработанных часов'),
            'format' => ['decimal', 2],
            'hAlign' => 'right',
            'value' => function ($model) {
                return $model['count_time'] ?? 0;
            },
            'content' => function ($model) {

                $time = $model['count_time'] ?? 0;
                $time = number_format($time, 2, Yii::$app->get('formatter')->decimalSeparator, Yii::$app->get('formatter')->thousandSeparator);

                if (Yii::$app->user->can('report.timesheet.index')) {
                    $route = [
                        '/report/time-sheet/index',
                        's' => [
                            'from' => $this->reportForm->getDateWorkFilter()->from,
                            'to' => $this->reportForm->getDateWorkFilter()->to,
                            'office' => $this->reportForm->getOfficeFilter()->office,
                        ],
                        'person' => $model['id'] ?? 0
                    ];

                    $time = Html::a($time, Url::toRoute($route) . '#person' . ($model['id'] ?? 0), [
                        'target' => '_blank',
                        'title' => Yii::t('common', 'Подробнее')
                    ]);
                }

                return $time;
            },
        ];
        $this->columns[] = [
            'attribute' => 'count_extra_hours',
            'label' => Yii::t('common', 'Переработка часов'),
            'format' => ['decimal', 2],
            'hAlign' => 'right',
            'value' => function ($model) {
                return $model['count_extra_hours'] ?? 0;
            },
        ];
        $this->columns[] = [
            'attribute' => 'holiday_worked_time',
            'label' => Yii::t('common', 'Праздники часов'),
            'format' => ['decimal', 2],
            'hAlign' => 'right',
            'value' => function ($model) {
                return $model['holiday_worked_time'] ?? 0;
            },
            'visible' => $this->reportForm->getShowHolidays()
        ];
        $this->columns[] = [
            'attribute' => 'salary_hand_local_extra',
            'label' => Yii::t('common', 'ЗП переработка'),
            'format' => ['decimal', 2],
            'hAlign' => 'right',
            'value' => function ($model) {
                return $model['salary_hand_local_extra'] ?? 0;
            },
            'visible' => $this->reportForm->getCalcSalaryLocal()
        ];
        $this->columns[] = [
            'attribute' => 'salary_hand_usd_extra',
            'label' => Yii::t('common', 'ЗП переработка'),
            'format' => ['decimal', 2],
            'hAlign' => 'right',
            'value' => function ($model) {
                return $model['salary_hand_usd_extra'] ?? 0;
            },
            'visible' => $this->reportForm->getCalcSalaryUSD()
        ];
        $this->columns[] = [
            'attribute' => 'salary_hand_local_holiday',
            'label' => Yii::t('common', 'ЗП праздники'),
            'format' => ['decimal', 2],
            'hAlign' => 'right',
            'value' => function ($model) {
                return $model['salary_hand_local_holiday'] ?? 0;
            },
            'visible' => $this->reportForm->getShowHolidays() && $this->reportForm->getCalcSalaryLocal()
        ];
        $this->columns[] = [
            'attribute' => 'salary_hand_usd_holiday',
            'label' => Yii::t('common', 'ЗП праздники'),
            'format' => ['decimal', 2],
            'hAlign' => 'right',
            'value' => function ($model) {
                return $model['salary_hand_usd_holiday'] ?? 0;
            },
            'visible' => $this->reportForm->getShowHolidays() && $this->reportForm->getCalcSalaryUSD()
        ];

        $this->columns[] = [
            'attribute' => 'penalty_sum_local',
            'label' => Yii::t('common', 'Штрафы'),
            'format' => ['decimal', 2],
            'hAlign' => 'right',
            'value' => function ($model) {
                return $model['penalty_sum_local'] ?? 0;
            },
            'visible' => $this->reportForm->getCalcSalaryLocal()
        ];
        $this->columns[] = [
            'attribute' => 'penalty_sum_usd',
            'label' => Yii::t('common', 'Штрафы'),
            'format' => ['decimal', 2],
            'hAlign' => 'right',
            'value' => function ($model) {
                return $model['penalty_sum_usd'] ?? 0;
            },
            'visible' => $this->reportForm->getCalcSalaryUSD()
        ];

        $this->columns[] = [
            'attribute' => 'bonus_sum_local',
            'label' => Yii::t('common', 'Бонусы'),
            'format' => ['decimal', 2],
            'hAlign' => 'right',
            'value' => function ($model) {
                return $model['bonus_sum_local'] ?? 0;
            },
            'visible' => $this->reportForm->getCalcSalaryLocal()
        ];
        $this->columns[] = [
            'attribute' => 'bonus_sum_usd',
            'label' => Yii::t('common', 'Бонусы'),
            'format' => ['decimal', 2],
            'hAlign' => 'right',
            'value' => function ($model) {
                return $model['bonus_sum_usd'] ?? 0;
            },
            'visible' => $this->reportForm->getCalcSalaryUSD()
        ];

        $this->columns[] = [
            'attribute' => 'salary_hand_local',
            'label' => Yii::t('common', 'ЗП в местной валюте "на руки"'),
            'format' => ['decimal', 2],
            'hAlign' => 'right',
            'value' => function ($model) {
                return $model['salary_hand_local'] ?? 0;
            },
            'visible' => $this->reportForm->getCalcSalaryLocal()
        ];
        $this->columns[] = [
            'attribute' => 'salary_tax_local',
            'label' => Yii::t('common', 'ЗП в местной валюте с учетом налогов'),
            'format' => ['decimal', 2],
            'hAlign' => 'right',
            'value' => function ($model) {
                return $model['salary_tax_local'] ?? 0;
            },
            'visible' => $this->reportForm->getCalcSalaryLocal()
        ];
        $this->columns[] = [
            'attribute' => 'salary_hand_usd',
            'label' => Yii::t('common', 'ЗП в USD "на руки"'),
            'format' => ['decimal', 2],
            'hAlign' => 'right',
            'value' => function ($model) {
                return $model['salary_hand_usd'] ?? 0;
            },
            'visible' => $this->reportForm->getCalcSalaryUSD()
        ];
        $this->columns[] = [
            'attribute' => 'salary_tax_usd',
            'label' => Yii::t('common', 'ЗП в USD с учетом налогов'),
            'format' => ['decimal', 2],
            'hAlign' => 'right',
            'value' => function ($model) {
                return $model['salary_tax_usd'] ?? 0;
            },
            'visible' => $this->reportForm->getCalcSalaryUSD()
        ];

        $mapStatuses = $this->reportForm->getMapStatuses();
        $orderStatuses = OrderStatus::find()->collection();
        foreach ($mapStatuses as $name => $statuses) {

            if ($name == 'total') continue;

            $innerStatuses = [];

            foreach ($statuses as $status) {
                $innerStatuses[] = $status . '. ' . $orderStatuses[$status];
            }

            // без этого не подставлялось значение в ячейку. в MapStatuses пришлось поместить названия на английском, так они идут в $model
            $label = '';
            if ($name == 'approve') $label = 'Подтверждено';
            if ($name == 'buyout') $label = 'Выкуплено';

            $this->columns[] = [
                'class' => DataColumnCount::className(),
                'attribute' => $name,
                'label' => Yii::t('common', $label),
                'columnLinkParams' => implode(',', $statuses),
                'makeLinkToOrders' => false,
                'headerOptions' => [
                    'data-container' => 'body',
                    'data-toggle' => 'tooltip',
                    'title' => Yii::t('common', 'Статусы: {statuses}', ['statuses' => implode(', ', $innerStatuses)]),
                ],
                'value' => function ($model) use ($name) {
                    return $model[$name] ?? 0;
                },
            ];
        }
        $this->columns[] = [
            'attribute' => 'count_calls',
            'label' => Yii::t('common', 'Всего звонков'),
            'format' => ['decimal'],
            'hAlign' => 'right',
            'pageSummaryFunc' => GridView::F_SUM,
            'value' => function ($model) {
                return $model['count_calls'] ?? 0;
            },
        ];
        parent::initColumns();
    }
}