<?php
namespace app\modules\report\extensions;

use app\helpers\i18n\Formatter;
use app\modules\report\components\ReportForm;
use Yii;
use yii\base\InvalidParamException;
use yii\data\ArrayDataProvider;

/**
 * Class DataProvider
 * @package app\modules\report\extensions
 */
class DataProviderTopProduct extends ArrayDataProvider
{
    /**
     * @var ReportForm
     */
    private $form;

    /**
     * @var Query
     */
    private $query;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->pagination->pageSize = 0;

        $this->prepareAllModels();
    }

    /**
     * @param ReportForm $form
     */
    public function setForm(ReportForm $form)
    {
        $this->form = $form;
    }

    /**
     * @param Query $query
     */
    public function setQuery(Query $query)
    {
        $this->query = $query;
    }

    /**
     * @return $this
     */
    protected function prepareQuery()
    {
        $closure = $this->getQueryClosure();

        if ($closure instanceof \Closure) {
            $closure($this->query);
        }

        return $this;
    }

    /**
     * @return $this
     */
    protected function prepareAllModels()
    {
        $allModels = $this->query->all();

        $closure = $this->getDataClosure();

        if ($closure instanceof \Closure) {
            $closure($this->query, $allModels);
        }

        $result = [];

        foreach ($allModels as $rowNumber => $row) {
            foreach ($row as $label => $data) {
                $newLabel = $this->query->getLabel($label);

                if ($newLabel) {
                    $result[$rowNumber][$newLabel] = $data;
                } else {
                    $result[$rowNumber][$label] = $data;
                }
            }
        }

        $this->allModels = $result;

        return $this;
    }

    /**
     * @return \Closure
     */
    protected function getDataClosure()
    {
        $closures = [
            ReportForm::GROUP_BY_PRODUCTS => function (Query $query, $data) {

            },
        ];

        if (!array_key_exists($this->form->groupBy, $closures)) {
            throw new InvalidParamException(Yii::t('common', 'Неправильный параметр группировки.'));
        }

        return $closures[$this->form->groupBy];
    }

    /**
     * @param string $attribute
     * @param Query $query
     */
    protected function applyQueryClosureDate($attribute, $query)
    {
        $condition = 'DATE_FORMAT(FROM_UNIXTIME(' . $attribute . '), "' . Formatter::getMysqlDateFormat() . '")';

        $query->orderBy([$attribute => SORT_DESC]);
    }
}
