<?php
namespace app\modules\report\extensions;

class DataColumnActiveLanding extends DataColumn
{
    /**
     * @param integer $count
     * @param integer $totalCount
     * @return string
     */
    protected function calculatePercent($count, $totalCount)
    {
        if (is_numeric($totalCount) && (int) $totalCount > 0) {
            $percent = number_format($count / $totalCount * 100, 2) . '%';
        } else {
            $percent = number_format(0, 2) . '%';
        }

        return $percent;
    }

    /**
     * @inheritdoc
     */
    protected function renderDataCellContent($model, $key, $index)
    {
        $dataCellContent = parent::renderDataCellContent($model, $key, $index);

        if ($this->attribute == "count_lead") {
            $percent = $this->calculatePercent($dataCellContent, $this->calculateSummary());
            $dataCellContent .= ' (' . $percent . ')';
        }

        return $dataCellContent;
    }

    /**
     * @inheritdoc
     */
    protected function renderPageSummaryCellContent()
    {
        $pageSummaryCellContent = parent::renderPageSummaryCellContent();

        if ($this->attribute == "count_lead" && $this->calculateSummary()) {
            $pageSummaryCellContent .= ' (100.00%)';
        }

        return $pageSummaryCellContent;
    }
}
