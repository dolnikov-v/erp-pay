<?php
namespace app\modules\report\extensions;

use app\modules\report\components\ReportFormProduct;
use Yii;

/**
 * Class GridViewProduct
 * @package app\modules\report\extensions
 */
class GridViewTopProduct extends GridView
{
    /**
     * @var ReportFormProduct
     */
    public $reportForm;

    /**
     * @inheritdoc
     */
    public function initColumns()
    {

        $this->columns[] = [
            'label' => Yii::t('common', 'Страна'),
            'attribute' => 'countryName',
        ];
        $this->columns[] = [
            'label' => Yii::t('common', 'Дата'),
            'attribute' => 'yearCreatedAt',
            'format' => 'raw',
            'value' => function ($model) {
                return date('M Y', strtotime($model['yearCreatedAt'] . '-' . $model['monthCreatedAt'] . '-' . '00' . ' 00:00:00')) . '<span style = "display: none;">' . $model['countryName'] . '</span>';
            },
        ];
        $this->columns[] = [
            'label' => Yii::t('common', 'Товар'),
            'attribute' => 'productName',
        ];
        $this->columns[] = [
            'label' => Yii::t('common', 'Количество'),
            'attribute' => 'countProducts',
        ];

        parent::initColumns();
    }
}
