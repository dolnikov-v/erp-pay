<?php
namespace app\modules\report\extensions;

use app\modules\report\components\ReportFormTopManager;
use Yii;

/**
 * Class GridViewTopManager
 * @package app\modules\report\extensions
 */
class GridViewTopManager extends GridView
{
    /**
     * @var ReportFormTopManager
     */
    public $reportForm;

    /**
     * @inheritdoc
     */
    public function initColumns()
    {
        $this->export = false;
        $this->showPageSummary = false;
        $this->columns[] = [
            'label' => Yii::t('common', 'Менеджер'),
            'attribute' => 'userName',
        ];

        $this->columns[] = [
            'label' => Yii::t('common', 'Доход') . ', $',
            'attribute' => 'income',
            'value' => function ($model) {
                if ($model['income']) {
                    return round($model['income']);
                }
                else return false;
            }
        ];

        $this->columns[] = [
            'label' => Yii::t('common', 'Процент апрувов'),
            'attribute' => 'approvePercent',
            'value' => function ($model) {
                if ($model['approvePercent']) {
                    return $model['approvePercent'] . "%";
                }
                else return 0 .".00%";
            }
        ];

        parent::initColumns();
    }
}
