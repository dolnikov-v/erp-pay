<?php
namespace app\modules\report\extensions;

use app\modules\report\components\ReportFormProduct;
use yii\helpers\Html;
use Yii;

/**
 * Class GridViewActiveLanding
 * @package app\modules\report\extensions
 */
class GridViewActiveLanding extends GridView
{
    /**
     * @var ReportFormProduct
     */
    public $reportForm;

    /**
     * @inheritdoc
     */
    public function initColumns()
    {
        $this->columns[] = [
            'attribute' => 'landing_url',
            'label' => Yii::t('common', 'Лендинг'),
            'pageSummary' => false,
            'format' => 'raw',
            'value' => function ($model) {
                return Html::a($model['landing_url'], $model['landing_url'], ['target' => '_blank']);
            }
        ];

        $this->columns[] = [
            'class' => DataColumnActiveLanding::className(),
            'label' => Yii::t('common', 'Кол-во лидов'),
            'attribute' => 'count_lead'
        ];

        parent::initColumns();
    }
}
