<?php

namespace app\modules\report\extensions;


use app\modules\callcenter\models\CallCenterCheckRequest;
use app\modules\report\components\ReportFormCheckUndelivery;
use Yii;

class GridViewCheckUndelivery extends GridView
{
    /**
     * @var ReportFormCheckUndelivery
     */
    public $reportForm;

    /**
     * @inheritdoc
     */
    public function initColumns()
    {
        $this->columns[] = [
            'label' => $this->reportForm->groupByCollection[$this->reportForm->groupBy],
            'attribute' => Query::GROUP_BY_LABEL,
            'pageSummary' => Yii::t('common', 'Всего'),
            'width' => '110px',
        ];

        $statuses = CallCenterCheckRequest::getSubStatusesForCheckUndelivery();

        foreach ($statuses as $id => $status) {
            $this->columns[] = [
                'class' => DataColumnCount::className(),
                'attribute' => $status,
                'columnLinkParams' => $id,
                'width' => '110px',
            ];
        }

        $this->columns[] = [
            'class' => DataColumnCount::className(),
            'attribute' => Yii::t('common', 'Всего'),
            'columnLinkParams' => implode(',', array_keys($statuses)),
            'width' => '110px',
        ];

        parent::initColumns();
    }
}
